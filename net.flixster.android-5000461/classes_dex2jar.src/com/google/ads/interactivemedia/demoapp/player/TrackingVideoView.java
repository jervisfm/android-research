package com.google.ads.interactivemedia.demoapp.player;

import android.content.Context;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.AttributeSet;
import android.widget.VideoView;
import com.google.ads.interactivemedia.api.player.VideoAdPlayer.VideoAdPlayerCallback;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TrackingVideoView extends VideoView
{
  private final List<VideoAdPlayer.VideoAdPlayerCallback> callbacks = new ArrayList(1);
  private boolean isTrackingEnabled;
  private VideoViewProgressThread progressThread;
  private PlaybackState state = PlaybackState.STOPPED;

  public TrackingVideoView(Context paramContext)
  {
    super(paramContext);
  }

  public TrackingVideoView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public TrackingVideoView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  private void onStop()
  {
    if (this.state == PlaybackState.STOPPED);
    do
    {
      return;
      this.state = PlaybackState.STOPPED;
    }
    while (!this.isTrackingEnabled);
    this.progressThread.quit();
    try
    {
      this.progressThread.join();
      this.progressThread = null;
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      throw new RuntimeException(localInterruptedException);
    }
  }

  public void addCallback(VideoAdPlayer.VideoAdPlayerCallback paramVideoAdPlayerCallback)
  {
    this.callbacks.add(paramVideoAdPlayerCallback);
  }

  public void disableTracking()
  {
    this.isTrackingEnabled = false;
  }

  public void enableTracking()
  {
    this.isTrackingEnabled = true;
  }

  public void onClick()
  {
    Iterator localIterator = this.callbacks.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((VideoAdPlayer.VideoAdPlayerCallback)localIterator.next()).onClick();
    }
  }

  public void onCompletion()
  {
    onStop();
    Iterator localIterator = this.callbacks.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((VideoAdPlayer.VideoAdPlayerCallback)localIterator.next()).onComplete();
    }
  }

  public void pause()
  {
    super.pause();
    this.state = PlaybackState.PAUSED;
    Iterator localIterator = this.callbacks.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((VideoAdPlayer.VideoAdPlayerCallback)localIterator.next()).onPause();
    }
  }

  public void removeCallback(VideoAdPlayer.VideoAdPlayerCallback paramVideoAdPlayerCallback)
  {
    this.callbacks.remove(paramVideoAdPlayerCallback);
  }

  public void setOnCompletionListener(MediaPlayer.OnCompletionListener paramOnCompletionListener)
  {
    super.setOnCompletionListener(paramOnCompletionListener);
  }

  public void start()
  {
    super.start();
    PlaybackState localPlaybackState = this.state;
    this.state = PlaybackState.PLAYING;
    if (this.isTrackingEnabled)
      switch ($SWITCH_TABLE$com$google$ads$interactivemedia$demoapp$player$TrackingVideoView$PlaybackState()[localPlaybackState.ordinal()])
      {
      default:
      case 1:
      case 2:
      }
    while (true)
    {
      return;
      this.progressThread = new VideoViewProgressThread(this, this.callbacks);
      this.progressThread.start();
      Iterator localIterator2 = this.callbacks.iterator();
      while (localIterator2.hasNext())
        ((VideoAdPlayer.VideoAdPlayerCallback)localIterator2.next()).onPlay();
      continue;
      Iterator localIterator1 = this.callbacks.iterator();
      while (localIterator1.hasNext())
        ((VideoAdPlayer.VideoAdPlayerCallback)localIterator1.next()).onResume();
    }
  }

  public void stopPlayback()
  {
    super.stopPlayback();
    onStop();
  }

  private static enum PlaybackState
  {
    static
    {
      PAUSED = new PlaybackState("PAUSED", 1);
      PLAYING = new PlaybackState("PLAYING", 2);
      PlaybackState[] arrayOfPlaybackState = new PlaybackState[3];
      arrayOfPlaybackState[0] = STOPPED;
      arrayOfPlaybackState[1] = PAUSED;
      arrayOfPlaybackState[2] = PLAYING;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.demoapp.player.TrackingVideoView
 * JD-Core Version:    0.6.2
 */