package com.google.ads.interactivemedia.demoapp.player;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.widget.VideoView;
import com.flixster.android.utils.Logger;
import com.google.ads.interactivemedia.api.player.VideoAdPlayer.VideoAdPlayerCallback;
import java.util.Iterator;
import java.util.List;

public class VideoViewProgressThread extends Thread
{
  private static final int QUIT = 2;
  private static final int UPDATE = 1;
  private final List<VideoAdPlayer.VideoAdPlayerCallback> callbacks;
  private Handler threadHandler;
  private Handler uiHandler;
  private final VideoView video;

  public VideoViewProgressThread(VideoView paramVideoView, List<VideoAdPlayer.VideoAdPlayerCallback> paramList)
  {
    this.video = paramVideoView;
    this.callbacks = paramList;
    setName("VideoViewProgressThread");
    this.uiHandler = new Handler(new Handler.Callback()
    {
      public boolean handleMessage(Message paramAnonymousMessage)
      {
        return VideoViewProgressThread.this.handleUiMessage(paramAnonymousMessage);
      }
    });
  }

  private void update()
  {
    try
    {
      if (!this.video.isPlaying())
        return;
      this.uiHandler.sendMessage(Message.obtain(this.uiHandler, 1, this.video.getCurrentPosition(), this.video.getDuration()));
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      Logger.w("FlxAd", "VideoViewProgressThread", localIllegalStateException);
    }
  }

  protected boolean handleThreadMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default:
      return false;
    case 1:
      update();
      this.threadHandler.sendEmptyMessageDelayed(1, 1000L);
      return true;
    case 2:
    }
    this.threadHandler.removeCallbacksAndMessages(null);
    Looper.myLooper().quit();
    return true;
  }

  protected boolean handleUiMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default:
      return false;
    case 1:
    }
    Iterator localIterator = this.callbacks.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return true;
      ((VideoAdPlayer.VideoAdPlayerCallback)localIterator.next()).onProgress(paramMessage.arg1, paramMessage.arg2);
    }
  }

  public void quit()
  {
    this.threadHandler.sendMessageAtFrontOfQueue(Message.obtain(this.threadHandler, 2));
  }

  public void run()
  {
    Looper.prepare();
    this.threadHandler = new Handler(new Handler.Callback()
    {
      public boolean handleMessage(Message paramAnonymousMessage)
      {
        return VideoViewProgressThread.this.handleThreadMessage(paramAnonymousMessage);
      }
    });
    this.threadHandler.sendEmptyMessage(1);
    Looper.loop();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.demoapp.player.VideoViewProgressThread
 * JD-Core Version:    0.6.2
 */