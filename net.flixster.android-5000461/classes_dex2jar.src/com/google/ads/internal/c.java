package com.google.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.l;
import com.google.ads.l.a;
import com.google.ads.m;
import com.google.ads.searchads.SearchAdRequest;
import com.google.ads.util.AdUtil;
import com.google.ads.util.AdUtil.a;
import com.google.ads.util.i.b;
import com.google.ads.util.i.c;
import com.google.ads.util.i.d;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;

public class c
  implements Runnable
{
  boolean a;
  private String b;
  private String c;
  private String d;
  private String e;
  private boolean f;
  private f g;
  private d h;
  private AdRequest i;
  private WebView j;
  private String k;
  private LinkedList<String> l;
  private String m;
  private AdSize n;
  private volatile boolean o;
  private boolean p;
  private AdRequest.ErrorCode q;
  private boolean r;
  private int s;
  private Thread t;
  private boolean u;
  private d v = d.b;

  protected c()
  {
  }

  public c(d paramd)
  {
    this.h = paramd;
    this.k = null;
    this.b = null;
    this.c = null;
    this.d = null;
    this.l = new LinkedList();
    this.q = null;
    this.r = false;
    this.s = -1;
    this.f = false;
    this.p = false;
    this.m = null;
    this.n = null;
    if ((Activity)paramd.h().e.a() != null)
    {
      this.j = new AdWebView(paramd.h(), null);
      this.j.setWebViewClient(i.a(paramd, a.b, false, false));
      this.j.setVisibility(8);
      this.j.setWillNotDraw(true);
      this.g = new f(this, paramd);
      return;
    }
    this.j = null;
    this.g = null;
    com.google.ads.util.b.e("activity was null while trying to create an AdLoader.");
  }

  static void a(String paramString, com.google.ads.c paramc, com.google.ads.d paramd)
  {
    if (paramString == null);
    while ((paramString.contains("no-store")) || (paramString.contains("no-cache")))
      return;
    Matcher localMatcher = Pattern.compile("max-age\\s*=\\s*(\\d+)").matcher(paramString);
    if (localMatcher.find())
      try
      {
        int i1 = Integer.parseInt(localMatcher.group(1));
        paramd.a(paramc, i1);
        Locale localLocale = Locale.US;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(i1);
        com.google.ads.util.b.c(String.format(localLocale, "Caching gWhirl configuration for: %d seconds", arrayOfObject));
        return;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        com.google.ads.util.b.b("Caught exception trying to parse cache control directive. Overflow?", localNumberFormatException);
        return;
      }
    com.google.ads.util.b.c("Unrecognized cacheControlDirective: '" + paramString + "'. Not caching configuration.");
  }

  private void b(String paramString1, String paramString2)
  {
    this.h.a(new c(this.j, paramString2, paramString1));
  }

  private String d()
  {
    if ((this.i instanceof SearchAdRequest))
      return "AFMA_buildAdURL";
    return "AFMA_buildAdURL";
  }

  private String e()
  {
    if ((this.i instanceof SearchAdRequest))
      return "AFMA_getSdkConstants();";
    return "AFMA_getSdkConstants();";
  }

  private String f()
  {
    if ((this.i instanceof SearchAdRequest))
      return "http://www.gstatic.com/safa/";
    return "http://media.admob.com/";
  }

  private String g()
  {
    if ((this.i instanceof SearchAdRequest))
      return "<html><head><script src=\"http://www.gstatic.com/safa/sdk-core-v40.js\"></script><script>";
    return "<html><head><script src=\"http://media.admob.com/sdk-core-v40.js\"></script><script>";
  }

  private String h()
  {
    if ((this.i instanceof SearchAdRequest))
      return "</script></head><body></body></html>";
    return "</script></head><body></body></html>";
  }

  private void i()
  {
    AdWebView localAdWebView = this.h.k();
    this.h.l().c(true);
    this.h.m().h();
    this.h.a(new c(localAdWebView, this.b, this.c));
  }

  private void j()
  {
    this.h.a(new e(this.h, this.j, this.l, this.s, this.p, this.m, this.n));
  }

  public String a(Map<String, Object> paramMap, Activity paramActivity)
    throws c.b
  {
    int i1 = 0;
    Context localContext = paramActivity.getApplicationContext();
    g localg = this.h.m();
    long l1 = localg.m();
    if (l1 > 0L)
      paramMap.put("prl", Long.valueOf(l1));
    long l2 = localg.n();
    if (l2 > 0L)
      paramMap.put("prnl", Long.valueOf(l2));
    String str1 = localg.l();
    if (str1 != null)
      paramMap.put("ppcl", str1);
    String str2 = localg.k();
    if (str2 != null)
      paramMap.put("pcl", str2);
    long l3 = localg.j();
    if (l3 > 0L)
      paramMap.put("pcc", Long.valueOf(l3));
    paramMap.put("preqs", Long.valueOf(localg.o()));
    paramMap.put("oar", Long.valueOf(localg.p()));
    paramMap.put("bas_on", Long.valueOf(localg.s()));
    paramMap.put("bas_off", Long.valueOf(localg.v()));
    if (localg.y())
      paramMap.put("aoi_timeout", "true");
    if (localg.A())
      paramMap.put("aoi_nofill", "true");
    String str3 = localg.D();
    if (str3 != null)
      paramMap.put("pit", str3);
    paramMap.put("ptime", Long.valueOf(g.E()));
    localg.a();
    localg.i();
    String str4;
    if (this.h.h().b())
    {
      paramMap.put("format", "interstitial_mb");
      paramMap.put("slotname", this.h.h().d.a());
      paramMap.put("js", "afma-sdk-a-v6.2.1");
      str4 = localContext.getPackageName();
    }
    while (true)
    {
      StringBuilder localStringBuilder;
      try
      {
        PackageInfo localPackageInfo = localContext.getPackageManager().getPackageInfo(str4, 0);
        int i2 = localPackageInfo.versionCode;
        String str5 = AdUtil.f(localContext);
        if (!TextUtils.isEmpty(str5))
          paramMap.put("mv", str5);
        paramMap.put("msid", localContext.getPackageName());
        paramMap.put("app_name", i2 + ".android." + localContext.getPackageName());
        paramMap.put("isu", AdUtil.a(localContext));
        String str6 = AdUtil.d(localContext);
        if (str6 == null)
          str6 = "null";
        paramMap.put("net", str6);
        String str7 = AdUtil.e(localContext);
        if ((str7 != null) && (str7.length() != 0))
          paramMap.put("cap", str7);
        paramMap.put("u_audio", Integer.valueOf(AdUtil.g(localContext).ordinal()));
        DisplayMetrics localDisplayMetrics1 = AdUtil.a(paramActivity);
        paramMap.put("u_sd", Float.valueOf(localDisplayMetrics1.density));
        paramMap.put("u_h", Integer.valueOf(AdUtil.a(localContext, localDisplayMetrics1)));
        paramMap.put("u_w", Integer.valueOf(AdUtil.b(localContext, localDisplayMetrics1)));
        paramMap.put("hl", Locale.getDefault().getLanguage());
        if ((this.h.h().i != null) && (this.h.h().i.a() != null))
        {
          AdView localAdView = (AdView)this.h.h().i.a();
          if (localAdView.getParent() != null)
          {
            int[] arrayOfInt = new int[2];
            localAdView.getLocationOnScreen(arrayOfInt);
            int i4 = arrayOfInt[0];
            int i5 = arrayOfInt[1];
            DisplayMetrics localDisplayMetrics2 = ((Context)this.h.h().f.a()).getResources().getDisplayMetrics();
            int i6 = localDisplayMetrics2.widthPixels;
            int i7 = localDisplayMetrics2.heightPixels;
            if ((!localAdView.isShown()) || (i4 + localAdView.getWidth() <= 0) || (i5 + localAdView.getHeight() <= 0) || (i4 > i6) || (i5 > i7))
              break label1511;
            i8 = 1;
            HashMap localHashMap2 = new HashMap();
            localHashMap2.put("x", Integer.valueOf(i4));
            localHashMap2.put("y", Integer.valueOf(i5));
            localHashMap2.put("width", Integer.valueOf(localAdView.getWidth()));
            localHashMap2.put("height", Integer.valueOf(localAdView.getHeight()));
            localHashMap2.put("visible", Integer.valueOf(i8));
            paramMap.put("ad_pos", localHashMap2);
          }
        }
        localStringBuilder = new StringBuilder();
        AdSize[] arrayOfAdSize = (AdSize[])this.h.h().l.a();
        if (arrayOfAdSize == null)
          break label1224;
        int i3 = arrayOfAdSize.length;
        if (i1 < i3)
        {
          AdSize localAdSize2 = arrayOfAdSize[i1];
          if (localStringBuilder.length() != 0)
            localStringBuilder.append("|");
          localStringBuilder.append(localAdSize2.getWidth() + "x" + localAdSize2.getHeight());
          i1++;
          continue;
          AdSize localAdSize1 = ((h)this.h.h().k.a()).b();
          if (localAdSize1.isFullWidth())
            paramMap.put("smart_w", "full");
          if (localAdSize1.isAutoHeight())
            paramMap.put("smart_h", "auto");
          if (!localAdSize1.isCustomAdSize())
          {
            paramMap.put("format", localAdSize1.toString());
            break;
          }
          HashMap localHashMap1 = new HashMap();
          localHashMap1.put("w", Integer.valueOf(localAdSize1.getWidth()));
          localHashMap1.put("h", Integer.valueOf(localAdSize1.getHeight()));
          paramMap.put("ad_frame", localHashMap1);
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        throw new b("NameNotFoundException");
      }
      paramMap.put("sz", localStringBuilder.toString());
      label1224: TelephonyManager localTelephonyManager = (TelephonyManager)localContext.getSystemService("phone");
      paramMap.put("carrier", localTelephonyManager.getNetworkOperator());
      paramMap.put("gnt", Integer.valueOf(localTelephonyManager.getNetworkType()));
      if (AdUtil.c())
        paramMap.put("simulator", Integer.valueOf(1));
      paramMap.put("session_id", com.google.ads.b.a().b().toString());
      paramMap.put("seq_num", com.google.ads.b.a().c().toString());
      String str8 = AdUtil.a(paramMap);
      if (((Boolean)((l.a)((l)this.h.h().a.a()).a.a()).l.a()).booleanValue());
      for (String str9 = g() + d() + "(" + str8 + ");" + h(); ; str9 = g() + e() + d() + "(" + str8 + ");" + h())
      {
        com.google.ads.util.b.c("adRequestUrlHtml: " + str9);
        return str9;
      }
      label1511: int i8 = 0;
    }
  }

  protected void a()
  {
    com.google.ads.util.b.a("AdLoader cancelled.");
    if (this.j != null)
    {
      this.j.stopLoading();
      this.j.destroy();
    }
    if (this.t != null)
    {
      this.t.interrupt();
      this.t = null;
    }
    if (this.g != null)
      this.g.a();
    this.o = true;
  }

  public void a(int paramInt)
  {
    try
    {
      this.s = paramInt;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void a(AdRequest.ErrorCode paramErrorCode)
  {
    try
    {
      this.q = paramErrorCode;
      notify();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void a(AdRequest.ErrorCode paramErrorCode, boolean paramBoolean)
  {
    this.h.a(new a(this.h, this.j, this.g, paramErrorCode, paramBoolean));
  }

  protected void a(AdRequest paramAdRequest)
  {
    this.i = paramAdRequest;
    this.o = false;
    this.t = new Thread(this);
    this.t.start();
  }

  public void a(AdSize paramAdSize)
  {
    try
    {
      this.n = paramAdSize;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void a(d paramd)
  {
    try
    {
      this.v = paramd;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void a(String paramString)
  {
    try
    {
      this.l.add(paramString);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void a(String paramString1, String paramString2)
  {
    try
    {
      this.b = paramString2;
      this.c = paramString1;
      notify();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void a(boolean paramBoolean)
  {
    try
    {
      this.f = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void b()
  {
    try
    {
      if (TextUtils.isEmpty(this.e))
      {
        com.google.ads.util.b.b("Got a mediation response with no content type. Aborting mediation.");
        a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
        return;
      }
      if (!this.e.startsWith("application/json"))
      {
        com.google.ads.util.b.b("Got a mediation response with a content type: '" + this.e + "'. Expected something starting with 'application/json'. Aborting mediation.");
        a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
        return;
      }
    }
    catch (JSONException localJSONException)
    {
      com.google.ads.util.b.b("AdLoader can't parse gWhirl server configuration.", localJSONException);
      a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
      return;
    }
    final com.google.ads.c localc = com.google.ads.c.a(this.c);
    a(this.d, localc, this.h.i());
    this.h.a(new Runnable()
    {
      public void run()
      {
        if (c.a(c.this) != null)
        {
          c.a(c.this).stopLoading();
          c.a(c.this).destroy();
        }
        c.c(c.this).a(c.b(c.this));
        if (c.d(c.this) != null)
          ((h)c.c(c.this).h().k.a()).b(c.d(c.this));
        c.c(c.this).a(localc);
      }
    });
  }

  protected void b(String paramString)
  {
    try
    {
      this.e = paramString;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void b(boolean paramBoolean)
  {
    try
    {
      this.p = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void c()
  {
    try
    {
      this.r = true;
      notify();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void c(String paramString)
  {
    try
    {
      this.d = paramString;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void c(boolean paramBoolean)
  {
    try
    {
      this.u = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void d(String paramString)
  {
    try
    {
      this.k = paramString;
      notify();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void d(boolean paramBoolean)
  {
    try
    {
      this.a = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void e(String paramString)
  {
    try
    {
      this.m = paramString;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  // ERROR //
  public void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 105	com/google/ads/internal/c:j	Landroid/webkit/WebView;
    //   6: ifnull +10 -> 16
    //   9: aload_0
    //   10: getfield 136	com/google/ads/internal/c:g	Lcom/google/ads/internal/f;
    //   13: ifnonnull +20 -> 33
    //   16: ldc_w 750
    //   19: invokestatic 143	com/google/ads/util/b:e	(Ljava/lang/String;)V
    //   22: aload_0
    //   23: getstatic 710	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   26: iconst_0
    //   27: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   30: aload_0
    //   31: monitorexit
    //   32: return
    //   33: aload_0
    //   34: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   37: invokevirtual 86	com/google/ads/internal/d:h	()Lcom/google/ads/m;
    //   40: getfield 91	com/google/ads/m:e	Lcom/google/ads/util/i$d;
    //   43: invokevirtual 96	com/google/ads/util/i$d:a	()Ljava/lang/Object;
    //   46: checkcast 98	android/app/Activity
    //   49: astore_3
    //   50: aload_3
    //   51: ifnonnull +25 -> 76
    //   54: ldc_w 752
    //   57: invokestatic 143	com/google/ads/util/b:e	(Ljava/lang/String;)V
    //   60: aload_0
    //   61: getstatic 710	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   64: iconst_0
    //   65: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   68: aload_0
    //   69: monitorexit
    //   70: return
    //   71: astore_2
    //   72: aload_0
    //   73: monitorexit
    //   74: aload_2
    //   75: athrow
    //   76: aload_0
    //   77: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   80: invokevirtual 753	com/google/ads/internal/d:o	()J
    //   83: lstore 4
    //   85: invokestatic 758	android/os/SystemClock:elapsedRealtime	()J
    //   88: lstore 6
    //   90: aload_0
    //   91: getfield 242	com/google/ads/internal/c:i	Lcom/google/ads/AdRequest;
    //   94: aload_0
    //   95: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   98: invokevirtual 86	com/google/ads/internal/d:h	()Lcom/google/ads/m;
    //   101: getfield 495	com/google/ads/m:f	Lcom/google/ads/util/i$b;
    //   104: invokevirtual 374	com/google/ads/util/i$b:a	()Ljava/lang/Object;
    //   107: checkcast 380	android/content/Context
    //   110: invokevirtual 764	com/google/ads/AdRequest:getRequestMap	(Landroid/content/Context;)Ljava/util/Map;
    //   113: astore 8
    //   115: aload 8
    //   117: ldc_w 766
    //   120: invokeinterface 770 2 0
    //   125: astore 9
    //   127: aload 9
    //   129: instanceof 299
    //   132: ifeq +142 -> 274
    //   135: aload 9
    //   137: checkcast 299	java/util/Map
    //   140: astore 31
    //   142: aload 31
    //   144: ldc_w 772
    //   147: invokeinterface 770 2 0
    //   152: astore 32
    //   154: aload 32
    //   156: instanceof 151
    //   159: ifeq +12 -> 171
    //   162: aload_0
    //   163: aload 32
    //   165: checkcast 151	java/lang/String
    //   168: putfield 58	com/google/ads/internal/c:b	Ljava/lang/String;
    //   171: aload 31
    //   173: ldc_w 774
    //   176: invokeinterface 770 2 0
    //   181: astore 33
    //   183: aload 33
    //   185: instanceof 151
    //   188: ifeq +12 -> 200
    //   191: aload_0
    //   192: aload 33
    //   194: checkcast 151	java/lang/String
    //   197: putfield 56	com/google/ads/internal/c:k	Ljava/lang/String;
    //   200: aload 31
    //   202: ldc_w 776
    //   205: invokeinterface 770 2 0
    //   210: astore 34
    //   212: aload 34
    //   214: instanceof 151
    //   217: ifeq +19 -> 236
    //   220: aload 34
    //   222: ldc_w 777
    //   225: invokevirtual 780	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   228: ifeq +125 -> 353
    //   231: aload_0
    //   232: iconst_1
    //   233: putfield 73	com/google/ads/internal/c:s	I
    //   236: aload 31
    //   238: ldc_w 782
    //   241: invokeinterface 770 2 0
    //   246: astore 35
    //   248: aload 35
    //   250: instanceof 151
    //   253: ifeq +21 -> 274
    //   256: aload 35
    //   258: ldc_w 783
    //   261: invokevirtual 780	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   264: ifeq +10 -> 274
    //   267: aload_0
    //   268: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   271: invokevirtual 785	com/google/ads/internal/d:d	()V
    //   274: aload_0
    //   275: getfield 58	com/google/ads/internal/c:b	Ljava/lang/String;
    //   278: ifnonnull +590 -> 868
    //   281: aload_0
    //   282: getfield 56	com/google/ads/internal/c:k	Ljava/lang/String;
    //   285: astore 15
    //   287: aload 15
    //   289: ifnonnull +235 -> 524
    //   292: aload_0
    //   293: aload 8
    //   295: aload_3
    //   296: invokevirtual 787	com/google/ads/internal/c:a	(Ljava/util/Map;Landroid/app/Activity;)Ljava/lang/String;
    //   299: astore 24
    //   301: aload_0
    //   302: aload 24
    //   304: aload_0
    //   305: invokespecial 789	com/google/ads/internal/c:f	()Ljava/lang/String;
    //   308: invokespecial 791	com/google/ads/internal/c:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   311: invokestatic 758	android/os/SystemClock:elapsedRealtime	()J
    //   314: lstore 25
    //   316: lload 4
    //   318: lload 25
    //   320: lload 6
    //   322: lsub
    //   323: lsub
    //   324: lstore 27
    //   326: lload 27
    //   328: lconst_0
    //   329: lcmp
    //   330: ifle +9 -> 339
    //   333: aload_0
    //   334: lload 27
    //   336: invokevirtual 795	java/lang/Object:wait	(J)V
    //   339: aload_0
    //   340: getfield 675	com/google/ads/internal/c:o	Z
    //   343: istore 29
    //   345: iload 29
    //   347: ifeq +110 -> 457
    //   350: aload_0
    //   351: monitorexit
    //   352: return
    //   353: aload 34
    //   355: ldc_w 796
    //   358: invokevirtual 780	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   361: ifeq -125 -> 236
    //   364: aload_0
    //   365: iconst_0
    //   366: putfield 73	com/google/ads/internal/c:s	I
    //   369: goto -133 -> 236
    //   372: astore_1
    //   373: ldc_w 798
    //   376: aload_1
    //   377: invokestatic 213	com/google/ads/util/b:b	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   380: aload_0
    //   381: getstatic 710	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   384: iconst_1
    //   385: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   388: aload_0
    //   389: monitorexit
    //   390: return
    //   391: astore 23
    //   393: new 215	java/lang/StringBuilder
    //   396: dup
    //   397: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   400: ldc_w 800
    //   403: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   406: aload 23
    //   408: invokevirtual 803	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   411: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   414: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   417: aload_0
    //   418: getstatic 710	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   421: iconst_0
    //   422: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   425: aload_0
    //   426: monitorexit
    //   427: return
    //   428: astore 30
    //   430: new 215	java/lang/StringBuilder
    //   433: dup
    //   434: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   437: ldc_w 805
    //   440: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   443: aload 30
    //   445: invokevirtual 803	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   448: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   451: invokestatic 659	com/google/ads/util/b:a	(Ljava/lang/String;)V
    //   454: aload_0
    //   455: monitorexit
    //   456: return
    //   457: aload_0
    //   458: getfield 69	com/google/ads/internal/c:q	Lcom/google/ads/AdRequest$ErrorCode;
    //   461: ifnull +15 -> 476
    //   464: aload_0
    //   465: aload_0
    //   466: getfield 69	com/google/ads/internal/c:q	Lcom/google/ads/AdRequest$ErrorCode;
    //   469: iconst_0
    //   470: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   473: aload_0
    //   474: monitorexit
    //   475: return
    //   476: aload_0
    //   477: getfield 56	com/google/ads/internal/c:k	Ljava/lang/String;
    //   480: ifnonnull +44 -> 524
    //   483: new 215	java/lang/StringBuilder
    //   486: dup
    //   487: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   490: ldc_w 807
    //   493: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   496: lload 4
    //   498: invokevirtual 810	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   501: ldc_w 812
    //   504: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   507: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   510: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   513: aload_0
    //   514: getstatic 815	com/google/ads/AdRequest$ErrorCode:NETWORK_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   517: iconst_0
    //   518: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   521: aload_0
    //   522: monitorexit
    //   523: return
    //   524: aload_0
    //   525: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   528: invokevirtual 269	com/google/ads/internal/d:m	()Lcom/google/ads/internal/g;
    //   531: astore 16
    //   533: getstatic 820	com/google/ads/internal/c$2:a	[I
    //   536: aload_0
    //   537: getfield 51	com/google/ads/internal/c:v	Lcom/google/ads/internal/c$d;
    //   540: invokevirtual 821	com/google/ads/internal/c$d:ordinal	()I
    //   543: iaload
    //   544: tableswitch	default:+32 -> 576, 1:+109->653, 2:+133->677, 3:+147->691, 4:+166->710
    //   577: getfield 743	com/google/ads/internal/c:a	Z
    //   580: ifne +254 -> 834
    //   583: ldc_w 823
    //   586: invokestatic 659	com/google/ads/util/b:a	(Ljava/lang/String;)V
    //   589: aload_0
    //   590: getfield 136	com/google/ads/internal/c:g	Lcom/google/ads/internal/f;
    //   593: aload_0
    //   594: getfield 741	com/google/ads/internal/c:u	Z
    //   597: invokevirtual 825	com/google/ads/internal/f:a	(Z)V
    //   600: aload_0
    //   601: getfield 136	com/google/ads/internal/c:g	Lcom/google/ads/internal/f;
    //   604: aload_0
    //   605: getfield 56	com/google/ads/internal/c:k	Ljava/lang/String;
    //   608: invokevirtual 826	com/google/ads/internal/f:a	(Ljava/lang/String;)V
    //   611: invokestatic 758	android/os/SystemClock:elapsedRealtime	()J
    //   614: lstore 17
    //   616: lload 4
    //   618: lload 17
    //   620: lload 6
    //   622: lsub
    //   623: lsub
    //   624: lstore 19
    //   626: lload 19
    //   628: lconst_0
    //   629: lcmp
    //   630: ifle +9 -> 639
    //   633: aload_0
    //   634: lload 19
    //   636: invokevirtual 795	java/lang/Object:wait	(J)V
    //   639: aload_0
    //   640: getfield 675	com/google/ads/internal/c:o	Z
    //   643: istore 21
    //   645: iload 21
    //   647: ifeq +120 -> 767
    //   650: aload_0
    //   651: monitorexit
    //   652: return
    //   653: aload 16
    //   655: invokevirtual 828	com/google/ads/internal/g:r	()V
    //   658: aload 16
    //   660: invokevirtual 830	com/google/ads/internal/g:u	()V
    //   663: aload 16
    //   665: invokevirtual 832	com/google/ads/internal/g:x	()V
    //   668: ldc_w 834
    //   671: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   674: goto -98 -> 576
    //   677: aload 16
    //   679: invokevirtual 836	com/google/ads/internal/g:t	()V
    //   682: ldc_w 838
    //   685: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   688: goto -112 -> 576
    //   691: aload 16
    //   693: invokevirtual 840	com/google/ads/internal/g:w	()V
    //   696: aload 16
    //   698: invokevirtual 842	com/google/ads/internal/g:q	()V
    //   701: ldc_w 844
    //   704: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   707: goto -131 -> 576
    //   710: aload 16
    //   712: invokevirtual 842	com/google/ads/internal/g:q	()V
    //   715: ldc_w 846
    //   718: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   721: ldc_w 848
    //   724: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   727: aload_0
    //   728: getstatic 815	com/google/ads/AdRequest$ErrorCode:NETWORK_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   731: iconst_0
    //   732: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   735: aload_0
    //   736: monitorexit
    //   737: return
    //   738: astore 22
    //   740: new 215	java/lang/StringBuilder
    //   743: dup
    //   744: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   747: ldc_w 850
    //   750: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   753: aload 22
    //   755: invokevirtual 803	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   758: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   761: invokestatic 659	com/google/ads/util/b:a	(Ljava/lang/String;)V
    //   764: aload_0
    //   765: monitorexit
    //   766: return
    //   767: aload_0
    //   768: getfield 69	com/google/ads/internal/c:q	Lcom/google/ads/AdRequest$ErrorCode;
    //   771: ifnull +15 -> 786
    //   774: aload_0
    //   775: aload_0
    //   776: getfield 69	com/google/ads/internal/c:q	Lcom/google/ads/AdRequest$ErrorCode;
    //   779: iconst_0
    //   780: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   783: aload_0
    //   784: monitorexit
    //   785: return
    //   786: aload_0
    //   787: getfield 60	com/google/ads/internal/c:c	Ljava/lang/String;
    //   790: ifnonnull +78 -> 868
    //   793: new 215	java/lang/StringBuilder
    //   796: dup
    //   797: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   800: ldc_w 807
    //   803: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   806: lload 4
    //   808: invokevirtual 810	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   811: ldc_w 852
    //   814: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   817: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   820: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   823: aload_0
    //   824: getstatic 815	com/google/ads/AdRequest$ErrorCode:NETWORK_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   827: iconst_0
    //   828: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   831: aload_0
    //   832: monitorexit
    //   833: return
    //   834: aload_0
    //   835: aload_0
    //   836: getfield 56	com/google/ads/internal/c:k	Ljava/lang/String;
    //   839: putfield 58	com/google/ads/internal/c:b	Ljava/lang/String;
    //   842: new 215	java/lang/StringBuilder
    //   845: dup
    //   846: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   849: ldc_w 854
    //   852: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   855: aload_0
    //   856: getfield 58	com/google/ads/internal/c:b	Ljava/lang/String;
    //   859: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   862: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   865: invokestatic 659	com/google/ads/util/b:a	(Ljava/lang/String;)V
    //   868: aload_0
    //   869: getfield 743	com/google/ads/internal/c:a	Z
    //   872: ifne +227 -> 1099
    //   875: aload_0
    //   876: getfield 75	com/google/ads/internal/c:f	Z
    //   879: ifeq +18 -> 897
    //   882: aload_0
    //   883: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   886: iconst_1
    //   887: invokevirtual 856	com/google/ads/internal/d:b	(Z)V
    //   890: aload_0
    //   891: invokevirtual 858	com/google/ads/internal/c:b	()V
    //   894: aload_0
    //   895: monitorexit
    //   896: return
    //   897: aload_0
    //   898: getfield 701	com/google/ads/internal/c:e	Ljava/lang/String;
    //   901: ifnull +72 -> 973
    //   904: aload_0
    //   905: getfield 701	com/google/ads/internal/c:e	Ljava/lang/String;
    //   908: ldc_w 714
    //   911: invokevirtual 718	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   914: ifne +16 -> 930
    //   917: aload_0
    //   918: getfield 701	com/google/ads/internal/c:e	Ljava/lang/String;
    //   921: ldc_w 860
    //   924: invokevirtual 718	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   927: ifeq +46 -> 973
    //   930: new 215	java/lang/StringBuilder
    //   933: dup
    //   934: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   937: ldc_w 862
    //   940: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   943: aload_0
    //   944: getfield 701	com/google/ads/internal/c:e	Ljava/lang/String;
    //   947: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   950: ldc_w 864
    //   953: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   956: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   959: invokestatic 705	com/google/ads/util/b:b	(Ljava/lang/String;)V
    //   962: aload_0
    //   963: getstatic 710	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   966: iconst_0
    //   967: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   970: aload_0
    //   971: monitorexit
    //   972: return
    //   973: aload_0
    //   974: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   977: invokevirtual 86	com/google/ads/internal/d:h	()Lcom/google/ads/m;
    //   980: getfield 537	com/google/ads/m:l	Lcom/google/ads/util/i$c;
    //   983: invokevirtual 540	com/google/ads/util/i$c:a	()Ljava/lang/Object;
    //   986: ifnull +95 -> 1081
    //   989: aload_0
    //   990: getfield 81	com/google/ads/internal/c:n	Lcom/google/ads/AdSize;
    //   993: ifnonnull +20 -> 1013
    //   996: ldc_w 866
    //   999: invokestatic 705	com/google/ads/util/b:b	(Ljava/lang/String;)V
    //   1002: aload_0
    //   1003: getstatic 710	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   1006: iconst_0
    //   1007: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   1010: aload_0
    //   1011: monitorexit
    //   1012: return
    //   1013: aload_0
    //   1014: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   1017: invokevirtual 86	com/google/ads/internal/d:h	()Lcom/google/ads/m;
    //   1020: getfield 537	com/google/ads/m:l	Lcom/google/ads/util/i$c;
    //   1023: invokevirtual 540	com/google/ads/util/i$c:a	()Ljava/lang/Object;
    //   1026: checkcast 868	[Ljava/lang/Object;
    //   1029: invokestatic 874	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
    //   1032: aload_0
    //   1033: getfield 81	com/google/ads/internal/c:n	Lcom/google/ads/AdSize;
    //   1036: invokeinterface 878 2 0
    //   1041: ifne +58 -> 1099
    //   1044: new 215	java/lang/StringBuilder
    //   1047: dup
    //   1048: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   1051: ldc_w 880
    //   1054: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1057: aload_0
    //   1058: getfield 81	com/google/ads/internal/c:n	Lcom/google/ads/AdSize;
    //   1061: invokevirtual 803	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1064: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1067: invokestatic 705	com/google/ads/util/b:b	(Ljava/lang/String;)V
    //   1070: aload_0
    //   1071: getstatic 710	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   1074: iconst_0
    //   1075: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   1078: aload_0
    //   1079: monitorexit
    //   1080: return
    //   1081: aload_0
    //   1082: getfield 81	com/google/ads/internal/c:n	Lcom/google/ads/AdSize;
    //   1085: ifnull +14 -> 1099
    //   1088: ldc_w 882
    //   1091: invokestatic 143	com/google/ads/util/b:e	(Ljava/lang/String;)V
    //   1094: aload_0
    //   1095: aconst_null
    //   1096: putfield 81	com/google/ads/internal/c:n	Lcom/google/ads/AdSize;
    //   1099: aload_0
    //   1100: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   1103: iconst_0
    //   1104: invokevirtual 856	com/google/ads/internal/d:b	(Z)V
    //   1107: aload_0
    //   1108: invokespecial 883	com/google/ads/internal/c:i	()V
    //   1111: invokestatic 758	android/os/SystemClock:elapsedRealtime	()J
    //   1114: lstore 10
    //   1116: lload 4
    //   1118: lload 10
    //   1120: lload 6
    //   1122: lsub
    //   1123: lsub
    //   1124: lstore 12
    //   1126: lload 12
    //   1128: lconst_0
    //   1129: lcmp
    //   1130: ifle +9 -> 1139
    //   1133: aload_0
    //   1134: lload 12
    //   1136: invokevirtual 795	java/lang/Object:wait	(J)V
    //   1139: aload_0
    //   1140: getfield 71	com/google/ads/internal/c:r	Z
    //   1143: ifeq +39 -> 1182
    //   1146: aload_0
    //   1147: invokespecial 885	com/google/ads/internal/c:j	()V
    //   1150: goto -762 -> 388
    //   1153: astore 14
    //   1155: new 215	java/lang/StringBuilder
    //   1158: dup
    //   1159: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   1162: ldc_w 887
    //   1165: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1168: aload 14
    //   1170: invokevirtual 803	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1173: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1176: invokestatic 659	com/google/ads/util/b:a	(Ljava/lang/String;)V
    //   1179: aload_0
    //   1180: monitorexit
    //   1181: return
    //   1182: new 215	java/lang/StringBuilder
    //   1185: dup
    //   1186: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   1189: ldc_w 807
    //   1192: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1195: lload 4
    //   1197: invokevirtual 810	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1200: ldc_w 889
    //   1203: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1206: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1209: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   1212: aload_0
    //   1213: getstatic 815	com/google/ads/AdRequest$ErrorCode:NETWORK_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   1216: iconst_1
    //   1217: invokevirtual 712	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   1220: goto -832 -> 388
    //
    // Exception table:
    //   from	to	target	type
    //   2	16	71	finally
    //   16	30	71	finally
    //   30	32	71	finally
    //   33	50	71	finally
    //   54	68	71	finally
    //   68	70	71	finally
    //   72	74	71	finally
    //   76	171	71	finally
    //   171	200	71	finally
    //   200	236	71	finally
    //   236	274	71	finally
    //   274	287	71	finally
    //   292	301	71	finally
    //   301	316	71	finally
    //   333	339	71	finally
    //   339	345	71	finally
    //   350	352	71	finally
    //   353	369	71	finally
    //   373	388	71	finally
    //   388	390	71	finally
    //   393	425	71	finally
    //   425	427	71	finally
    //   430	454	71	finally
    //   454	456	71	finally
    //   457	473	71	finally
    //   473	475	71	finally
    //   476	521	71	finally
    //   521	523	71	finally
    //   524	576	71	finally
    //   576	616	71	finally
    //   633	639	71	finally
    //   639	645	71	finally
    //   650	652	71	finally
    //   653	674	71	finally
    //   677	688	71	finally
    //   691	707	71	finally
    //   710	735	71	finally
    //   735	737	71	finally
    //   740	764	71	finally
    //   764	766	71	finally
    //   767	783	71	finally
    //   783	785	71	finally
    //   786	831	71	finally
    //   831	833	71	finally
    //   834	868	71	finally
    //   868	894	71	finally
    //   894	896	71	finally
    //   897	930	71	finally
    //   930	970	71	finally
    //   970	972	71	finally
    //   973	1010	71	finally
    //   1010	1012	71	finally
    //   1013	1078	71	finally
    //   1078	1080	71	finally
    //   1081	1099	71	finally
    //   1099	1116	71	finally
    //   1133	1139	71	finally
    //   1139	1150	71	finally
    //   1155	1179	71	finally
    //   1179	1181	71	finally
    //   1182	1220	71	finally
    //   2	16	372	java/lang/Throwable
    //   16	30	372	java/lang/Throwable
    //   33	50	372	java/lang/Throwable
    //   54	68	372	java/lang/Throwable
    //   76	171	372	java/lang/Throwable
    //   171	200	372	java/lang/Throwable
    //   200	236	372	java/lang/Throwable
    //   236	274	372	java/lang/Throwable
    //   274	287	372	java/lang/Throwable
    //   292	301	372	java/lang/Throwable
    //   301	316	372	java/lang/Throwable
    //   333	339	372	java/lang/Throwable
    //   339	345	372	java/lang/Throwable
    //   353	369	372	java/lang/Throwable
    //   393	425	372	java/lang/Throwable
    //   430	454	372	java/lang/Throwable
    //   457	473	372	java/lang/Throwable
    //   476	521	372	java/lang/Throwable
    //   524	576	372	java/lang/Throwable
    //   576	616	372	java/lang/Throwable
    //   633	639	372	java/lang/Throwable
    //   639	645	372	java/lang/Throwable
    //   653	674	372	java/lang/Throwable
    //   677	688	372	java/lang/Throwable
    //   691	707	372	java/lang/Throwable
    //   710	735	372	java/lang/Throwable
    //   740	764	372	java/lang/Throwable
    //   767	783	372	java/lang/Throwable
    //   786	831	372	java/lang/Throwable
    //   834	868	372	java/lang/Throwable
    //   868	894	372	java/lang/Throwable
    //   897	930	372	java/lang/Throwable
    //   930	970	372	java/lang/Throwable
    //   973	1010	372	java/lang/Throwable
    //   1013	1078	372	java/lang/Throwable
    //   1081	1099	372	java/lang/Throwable
    //   1099	1116	372	java/lang/Throwable
    //   1133	1139	372	java/lang/Throwable
    //   1139	1150	372	java/lang/Throwable
    //   1155	1179	372	java/lang/Throwable
    //   1182	1220	372	java/lang/Throwable
    //   292	301	391	com/google/ads/internal/c$b
    //   333	339	428	java/lang/InterruptedException
    //   633	639	738	java/lang/InterruptedException
    //   1133	1139	1153	java/lang/InterruptedException
  }

  private static class a
    implements Runnable
  {
    private final d a;
    private final WebView b;
    private final f c;
    private final AdRequest.ErrorCode d;
    private final boolean e;

    public a(d paramd, WebView paramWebView, f paramf, AdRequest.ErrorCode paramErrorCode, boolean paramBoolean)
    {
      this.a = paramd;
      this.b = paramWebView;
      this.c = paramf;
      this.d = paramErrorCode;
      this.e = paramBoolean;
    }

    public void run()
    {
      if (this.b != null)
      {
        this.b.stopLoading();
        this.b.destroy();
      }
      if (this.c != null)
        this.c.a();
      if (this.e)
      {
        AdWebView localAdWebView = this.a.k();
        localAdWebView.stopLoading();
        localAdWebView.setVisibility(8);
      }
      this.a.a(this.d);
    }
  }

  private class b extends Exception
  {
    public b(String arg2)
    {
      super();
    }
  }

  private class c
    implements Runnable
  {
    private final String b;
    private final String c;
    private final WebView d;

    public c(WebView paramString1, String paramString2, String arg4)
    {
      this.d = paramString1;
      this.b = paramString2;
      Object localObject;
      this.c = localObject;
    }

    public void run()
    {
      if (this.c != null)
      {
        this.d.loadDataWithBaseURL(this.b, this.c, "text/html", "utf-8", null);
        return;
      }
      this.d.loadUrl(this.b);
    }
  }

  public static enum d
  {
    public String e;

    static
    {
      d[] arrayOfd = new d[4];
      arrayOfd[0] = a;
      arrayOfd[1] = b;
      arrayOfd[2] = c;
      arrayOfd[3] = d;
    }

    private d(String paramString)
    {
      this.e = paramString;
    }
  }

  private static class e
    implements Runnable
  {
    private final d a;
    private final WebView b;
    private final LinkedList<String> c;
    private final int d;
    private final boolean e;
    private final String f;
    private final AdSize g;

    public e(d paramd, WebView paramWebView, LinkedList<String> paramLinkedList, int paramInt, boolean paramBoolean, String paramString, AdSize paramAdSize)
    {
      this.a = paramd;
      this.b = paramWebView;
      this.c = paramLinkedList;
      this.d = paramInt;
      this.e = paramBoolean;
      this.f = paramString;
      this.g = paramAdSize;
    }

    public void run()
    {
      if (this.b != null)
      {
        this.b.stopLoading();
        this.b.destroy();
      }
      this.a.a(this.c);
      this.a.a(this.d);
      this.a.a(this.e);
      this.a.a(this.f);
      if (this.g != null)
      {
        ((h)this.a.h().k.a()).b(this.g);
        this.a.k().setAdSize(this.g);
      }
      this.a.C();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.internal.c
 * JD-Core Version:    0.6.2
 */