package com.google.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.google.ads.Ad;
import com.google.ads.AdActivity;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.AppEventListener;
import com.google.ads.InterstitialAd;
import com.google.ads.ac;
import com.google.ads.ae;
import com.google.ads.ag;
import com.google.ads.f;
import com.google.ads.l;
import com.google.ads.l.a;
import com.google.ads.m;
import com.google.ads.util.AdUtil;
import com.google.ads.util.i.b;
import com.google.ads.util.i.c;
import com.google.ads.util.i.d;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class d
{
  private static final Object a = new Object();
  private final m b;
  private c c;
  private AdRequest d;
  private g e;
  private AdWebView f;
  private i g;
  private Handler h;
  private long i;
  private boolean j;
  private boolean k;
  private boolean l;
  private boolean m;
  private boolean n;
  private SharedPreferences o;
  private long p;
  private ae q;
  private boolean r;
  private LinkedList<String> s;
  private LinkedList<String> t;
  private int u = -1;
  private Boolean v;
  private com.google.ads.d w;
  private com.google.ads.e x;
  private f y;
  private String z = null;

  public d(Ad paramAd, Activity paramActivity, AdSize paramAdSize, String paramString, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    this.r = paramBoolean;
    this.e = new g();
    this.c = null;
    this.d = null;
    this.k = false;
    this.h = new Handler();
    this.p = 60000L;
    this.l = false;
    this.n = false;
    this.m = true;
    if (paramActivity == null)
    {
      l locall2 = l.a();
      AdView localAdView2;
      InterstitialAd localInterstitialAd2;
      if ((paramAd instanceof AdView))
      {
        localAdView2 = (AdView)paramAd;
        if (!(paramAd instanceof InterstitialAd))
          break label156;
        localInterstitialAd2 = (InterstitialAd)paramAd;
        label114: if (paramAdSize != null)
          break label162;
      }
      label156: label162: for (h localh2 = h.a; ; localh2 = h.a(paramAdSize))
      {
        this.b = new m(locall2, paramAd, localAdView2, localInterstitialAd2, paramString, null, null, paramViewGroup, localh2);
        return;
        localAdView2 = null;
        break;
        localInterstitialAd2 = null;
        break label114;
      }
    }
    while (true)
    {
      synchronized (a)
      {
        this.o = paramActivity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
        if (paramBoolean)
        {
          long l1 = this.o.getLong("Timeout" + paramString, -1L);
          if (l1 < 0L)
          {
            this.i = 5000L;
            l locall1 = l.a();
            if (!(paramAd instanceof AdView))
              break label437;
            localAdView1 = (AdView)paramAd;
            if (!(paramAd instanceof InterstitialAd))
              break label443;
            localInterstitialAd1 = (InterstitialAd)paramAd;
            Context localContext = paramActivity.getApplicationContext();
            if (paramAdSize != null)
              break label449;
            localh1 = h.a;
            this.b = new m(locall1, paramAd, localAdView1, localInterstitialAd1, paramString, paramActivity, localContext, paramViewGroup, localh1);
            this.q = new ae(this);
            this.s = new LinkedList();
            this.t = new LinkedList();
            a();
            AdUtil.h((Context)this.b.f.a());
            this.w = new com.google.ads.d();
            this.x = new com.google.ads.e(this);
            this.v = null;
            this.y = null;
            return;
          }
          this.i = l1;
        }
      }
      this.i = 60000L;
      continue;
      label437: AdView localAdView1 = null;
      continue;
      label443: InterstitialAd localInterstitialAd1 = null;
      continue;
      label449: h localh1 = h.a(paramAdSize, paramActivity.getApplicationContext());
    }
  }

  private void a(f paramf, Boolean paramBoolean)
  {
    Object localObject = paramf.d();
    if (localObject == null)
    {
      localObject = new ArrayList();
      ((List)localObject).add("http://e.admob.com/imp?ad_loc=@gw_adlocid@&qdata=@gw_qdata@&ad_network_id=@gw_adnetid@&js=@gw_sdkver@&session_id=@gw_sessid@&seq_num=@gw_seqnum@&nr=@gw_adnetrefresh@&adt=@gw_adt@&aec=@gw_aec@");
    }
    String str = paramf.b();
    a((List)localObject, paramf.a(), str, paramf.c(), paramBoolean, this.e.d(), this.e.e());
  }

  private void a(List<String> paramList, String paramString)
  {
    Object localObject;
    if (paramList == null)
    {
      localObject = new ArrayList();
      ((List)localObject).add("http://e.admob.com/nofill?ad_loc=@gw_adlocid@&qdata=@gw_qdata@&js=@gw_sdkver@&session_id=@gw_sessid@&seq_num=@gw_seqnum@&adt=@gw_adt@&aec=@gw_aec@");
    }
    while (true)
    {
      a((List)localObject, null, null, paramString, null, this.e.d(), this.e.e());
      return;
      localObject = paramList;
    }
  }

  private void a(List<String> paramList, String paramString1, String paramString2, String paramString3, Boolean paramBoolean, String paramString4, String paramString5)
  {
    String str1 = AdUtil.a((Context)this.b.f.a());
    com.google.ads.b localb = com.google.ads.b.a();
    String str2 = localb.b().toString();
    String str3 = localb.c().toString();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
      new Thread(new ac(com.google.ads.g.a((String)localIterator.next(), (String)this.b.d.a(), paramBoolean, str1, paramString1, paramString2, paramString3, str2, str3, paramString4, paramString5), (Context)this.b.f.a())).start();
    this.e.b();
  }

  private void b(f paramf, Boolean paramBoolean)
  {
    Object localObject = paramf.e();
    if (localObject == null)
    {
      localObject = new ArrayList();
      ((List)localObject).add("http://e.admob.com/clk?ad_loc=@gw_adlocid@&qdata=@gw_qdata@&ad_network_id=@gw_adnetid@&js=@gw_sdkver@&session_id=@gw_sessid@&seq_num=@gw_seqnum@&nr=@gw_adnetrefresh@");
    }
    String str = paramf.b();
    a((List)localObject, paramf.a(), str, paramf.c(), paramBoolean, null, null);
  }

  public void A()
  {
    try
    {
      if (this.c != null)
      {
        this.c.a();
        this.c = null;
      }
      if (this.f != null)
        this.f.stopLoading();
      return;
    }
    finally
    {
    }
  }

  protected void B()
  {
    try
    {
      Activity localActivity = (Activity)this.b.e.a();
      if (localActivity == null)
        com.google.ads.util.b.e("activity was null while trying to ping click tracking URLs.");
      while (true)
      {
        return;
        Iterator localIterator = this.t.iterator();
        while (localIterator.hasNext())
          new Thread(new ac((String)localIterator.next(), localActivity.getApplicationContext())).start();
      }
    }
    finally
    {
    }
  }

  protected void C()
  {
    try
    {
      this.c = null;
      this.k = true;
      this.f.setVisibility(0);
      if (this.b.a())
        a(this.f);
      this.e.g();
      if (this.b.a())
        x();
      com.google.ads.util.b.c("onReceiveAd()");
      AdListener localAdListener = (AdListener)this.b.m.a();
      if (localAdListener != null)
        localAdListener.onReceiveAd((Ad)this.b.h.a());
      return;
    }
    finally
    {
    }
  }

  public void a()
  {
    try
    {
      this.f = new AdWebView(this.b, ((h)this.b.k.a()).b());
      this.f.setVisibility(8);
      this.g = i.a(this, a.c, true, this.b.b());
      this.f.setWebViewClient(this.g);
      l.a locala = (l.a)((l)this.b.a.a()).a.a();
      if ((AdUtil.a < ((Integer)locala.a.a()).intValue()) && (!((h)this.b.k.a()).a()))
      {
        com.google.ads.util.b.a("Disabling hardware acceleration for a banner.");
        this.f.b();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void a(float paramFloat)
  {
    try
    {
      long l1 = this.p;
      this.p = (()(1000.0F * paramFloat));
      if ((s()) && (this.p != l1))
      {
        e();
        f();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void a(int paramInt)
  {
    try
    {
      this.u = paramInt;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void a(long paramLong)
  {
    synchronized (a)
    {
      SharedPreferences.Editor localEditor = this.o.edit();
      localEditor.putLong("Timeout" + this.b.d, paramLong);
      localEditor.commit();
      if (this.r)
        this.i = paramLong;
      return;
    }
  }

  public void a(View paramView)
  {
    ((ViewGroup)this.b.g.a()).removeAllViews();
    ((ViewGroup)this.b.g.a()).addView(paramView);
  }

  public void a(View paramView, com.google.ads.h paramh, f paramf, boolean paramBoolean)
  {
    try
    {
      com.google.ads.util.b.a("AdManager.onReceiveGWhirlAd() called.");
      this.k = true;
      this.y = paramf;
      if (this.b.a())
      {
        a(paramView);
        a(paramf, Boolean.valueOf(paramBoolean));
      }
      this.x.d(paramh);
      AdListener localAdListener = (AdListener)this.b.m.a();
      if (localAdListener != null)
        localAdListener.onReceiveAd((Ad)this.b.h.a());
      return;
    }
    finally
    {
    }
  }

  public void a(AdRequest.ErrorCode paramErrorCode)
  {
    try
    {
      this.c = null;
      if (paramErrorCode == AdRequest.ErrorCode.NETWORK_ERROR)
      {
        a(60.0F);
        if (!s())
          g();
      }
      if (this.b.b())
      {
        if (paramErrorCode != AdRequest.ErrorCode.NO_FILL)
          break label126;
        this.e.B();
      }
      while (true)
      {
        com.google.ads.util.b.c("onFailedToReceiveAd(" + paramErrorCode + ")");
        AdListener localAdListener = (AdListener)this.b.m.a();
        if (localAdListener != null)
          localAdListener.onFailedToReceiveAd((Ad)this.b.h.a(), paramErrorCode);
        return;
        label126: if (paramErrorCode == AdRequest.ErrorCode.NETWORK_ERROR)
          this.e.z();
      }
    }
    finally
    {
    }
  }

  public void a(AdRequest paramAdRequest)
  {
    while (true)
    {
      try
      {
        if (p())
        {
          com.google.ads.util.b.e("loadAd called while the ad is already loading, so aborting.");
          return;
        }
        if (AdActivity.isShowing())
        {
          com.google.ads.util.b.e("loadAd called while an interstitial or landing page is displayed, so aborting");
          continue;
        }
      }
      finally
      {
      }
      if ((AdUtil.c((Context)this.b.f.a())) && (AdUtil.b((Context)this.b.f.a())))
      {
        long l1 = this.o.getLong("GoogleAdMobDoritosLife", 60000L);
        if (ag.a((Context)this.b.f.a(), l1))
          ag.a((Activity)this.b.e.a());
        this.k = false;
        this.s.clear();
        this.d = paramAdRequest;
        if (this.w.a())
        {
          this.x.a(this.w.b(), paramAdRequest);
        }
        else
        {
          this.c = new c(this);
          this.c.a(paramAdRequest);
        }
      }
    }
  }

  public void a(com.google.ads.c paramc)
  {
    try
    {
      this.c = null;
      if (paramc.d())
      {
        a(paramc.e());
        if (!this.l)
          f();
      }
      while (true)
      {
        this.x.a(paramc, this.d);
        return;
        if (this.l)
          e();
      }
    }
    finally
    {
    }
  }

  public void a(f paramf, boolean paramBoolean)
  {
    try
    {
      Locale localLocale = Locale.US;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Boolean.valueOf(paramBoolean);
      com.google.ads.util.b.a(String.format(localLocale, "AdManager.onGWhirlAdClicked(%b) called.", arrayOfObject));
      b(paramf, Boolean.valueOf(paramBoolean));
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void a(Runnable paramRunnable)
  {
    this.h.post(paramRunnable);
  }

  public void a(String paramString)
  {
    Uri localUri = new Uri.Builder().encodedQuery(paramString).build();
    StringBuilder localStringBuilder = new StringBuilder();
    HashMap localHashMap = AdUtil.b(localUri);
    Iterator localIterator = localHashMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localStringBuilder.append(str).append(" = ").append((String)localHashMap.get(str)).append("\n");
    }
    this.z = localStringBuilder.toString().trim();
    if (TextUtils.isEmpty(this.z))
      this.z = null;
  }

  public void a(String paramString1, String paramString2)
  {
    try
    {
      AppEventListener localAppEventListener = (AppEventListener)this.b.n.a();
      if (localAppEventListener != null)
        localAppEventListener.onAppEvent((Ad)this.b.h.a(), paramString1, paramString2);
      return;
    }
    finally
    {
    }
  }

  protected void a(LinkedList<String> paramLinkedList)
  {
    try
    {
      Iterator localIterator = paramLinkedList.iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        com.google.ads.util.b.a("Adding a click tracking URL: " + str);
      }
    }
    finally
    {
    }
    this.t = paramLinkedList;
  }

  public void a(boolean paramBoolean)
  {
    try
    {
      this.j = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void b()
  {
    try
    {
      if (this.x != null)
        this.x.b();
      this.b.m.a(null);
      this.b.n.a(null);
      A();
      if (this.f != null)
        this.f.destroy();
      return;
    }
    finally
    {
    }
  }

  public void b(long paramLong)
  {
    if (paramLong > 0L);
    try
    {
      this.o.edit().putLong("GoogleAdMobDoritosLife", paramLong).commit();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void b(com.google.ads.c paramc)
  {
    try
    {
      com.google.ads.util.b.a("AdManager.onGWhirlNoFill() called.");
      a(paramc.i(), paramc.c());
      AdListener localAdListener = (AdListener)this.b.m.a();
      if (localAdListener != null)
        localAdListener.onFailedToReceiveAd((Ad)this.b.h.a(), AdRequest.ErrorCode.NO_FILL);
      return;
    }
    finally
    {
    }
  }

  protected void b(String paramString)
  {
    try
    {
      com.google.ads.util.b.a("Adding a tracking URL: " + paramString);
      this.s.add(paramString);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void b(boolean paramBoolean)
  {
    this.v = Boolean.valueOf(paramBoolean);
  }

  public String c()
  {
    return this.z;
  }

  public void d()
  {
    try
    {
      this.m = false;
      com.google.ads.util.b.a("Refreshing is no longer allowed on this AdView.");
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void e()
  {
    try
    {
      if (this.l)
      {
        com.google.ads.util.b.a("Disabling refreshing.");
        this.h.removeCallbacks(this.q);
        this.l = false;
      }
      while (true)
      {
        return;
        com.google.ads.util.b.a("Refreshing is already disabled.");
      }
    }
    finally
    {
    }
  }

  public void f()
  {
    while (true)
    {
      try
      {
        this.n = false;
        if (!this.b.a())
          break label110;
        if (this.m)
        {
          if (!this.l)
          {
            com.google.ads.util.b.a("Enabling refreshing every " + this.p + " milliseconds.");
            this.h.postDelayed(this.q, this.p);
            this.l = true;
            return;
          }
          com.google.ads.util.b.a("Refreshing is already enabled.");
          continue;
        }
      }
      finally
      {
      }
      com.google.ads.util.b.a("Refreshing disabled on this AdView");
      continue;
      label110: com.google.ads.util.b.a("Tried to enable refreshing on something other than an AdView.");
    }
  }

  public void g()
  {
    f();
    this.n = true;
  }

  public m h()
  {
    return this.b;
  }

  public com.google.ads.d i()
  {
    try
    {
      com.google.ads.d locald = this.w;
      return locald;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public c j()
  {
    try
    {
      c localc = this.c;
      return localc;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public AdWebView k()
  {
    try
    {
      AdWebView localAdWebView = this.f;
      return localAdWebView;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public i l()
  {
    try
    {
      i locali = this.g;
      return locali;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public g m()
  {
    return this.e;
  }

  public int n()
  {
    try
    {
      int i1 = this.u;
      return i1;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public long o()
  {
    return this.i;
  }

  public boolean p()
  {
    try
    {
      c localc = this.c;
      if (localc != null)
      {
        bool = true;
        return bool;
      }
      boolean bool = false;
    }
    finally
    {
    }
  }

  public boolean q()
  {
    try
    {
      boolean bool = this.j;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public boolean r()
  {
    try
    {
      boolean bool = this.k;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public boolean s()
  {
    try
    {
      boolean bool = this.l;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void t()
  {
    try
    {
      this.e.C();
      com.google.ads.util.b.c("onDismissScreen()");
      AdListener localAdListener = (AdListener)this.b.m.a();
      if (localAdListener != null)
        localAdListener.onDismissScreen((Ad)this.b.h.a());
      return;
    }
    finally
    {
    }
  }

  public void u()
  {
    try
    {
      com.google.ads.util.b.c("onPresentScreen()");
      AdListener localAdListener = (AdListener)this.b.m.a();
      if (localAdListener != null)
        localAdListener.onPresentScreen((Ad)this.b.h.a());
      return;
    }
    finally
    {
    }
  }

  public void v()
  {
    try
    {
      com.google.ads.util.b.c("onLeaveApplication()");
      AdListener localAdListener = (AdListener)this.b.m.a();
      if (localAdListener != null)
        localAdListener.onLeaveApplication((Ad)this.b.h.a());
      return;
    }
    finally
    {
    }
  }

  public void w()
  {
    this.e.f();
    B();
  }

  public void x()
  {
    try
    {
      Activity localActivity = (Activity)this.b.e.a();
      if (localActivity == null)
        com.google.ads.util.b.e("activity was null while trying to ping tracking URLs.");
      while (true)
      {
        return;
        Iterator localIterator = this.s.iterator();
        while (localIterator.hasNext())
          new Thread(new ac((String)localIterator.next(), localActivity.getApplicationContext())).start();
      }
    }
    finally
    {
    }
  }

  public void y()
  {
    while (true)
    {
      try
      {
        if (this.d == null)
          break label114;
        if (!this.b.a())
          break label105;
        if ((((AdView)this.b.i.a()).isShown()) && (AdUtil.d()))
        {
          com.google.ads.util.b.c("Refreshing ad.");
          a(this.d);
          if (this.n)
            e();
        }
        else
        {
          com.google.ads.util.b.a("Not refreshing because the ad is not visible.");
          continue;
        }
      }
      finally
      {
      }
      this.h.postDelayed(this.q, this.p);
      continue;
      label105: com.google.ads.util.b.a("Tried to refresh an ad that wasn't an AdView.");
      continue;
      label114: com.google.ads.util.b.a("Tried to refresh before calling loadAd().");
    }
  }

  public void z()
  {
    while (true)
    {
      try
      {
        com.google.ads.util.a.a(this.b.b());
        if (!this.k)
          break label101;
        this.k = false;
        if (this.v == null)
        {
          com.google.ads.util.b.b("isMediationFlag is null in show() with isReady() true. we should have an ad and know whether this is a mediation request or not. ");
          return;
        }
        if (this.v.booleanValue())
        {
          if (!this.x.c())
            continue;
          a(this.y, Boolean.valueOf(false));
          continue;
        }
      }
      finally
      {
      }
      AdActivity.launchAdActivity(this, new e("interstitial"));
      x();
      continue;
      label101: com.google.ads.util.b.c("Cannot show interstitial because it is not loaded and ready.");
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.internal.d
 * JD-Core Version:    0.6.2
 */