package com.google.ads.mediation.customevent;

public abstract interface CustomEvent
{
  public abstract void destroy();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.mediation.customevent.CustomEvent
 * JD-Core Version:    0.6.2
 */