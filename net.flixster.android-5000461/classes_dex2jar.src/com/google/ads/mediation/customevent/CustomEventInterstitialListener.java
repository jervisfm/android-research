package com.google.ads.mediation.customevent;

public abstract interface CustomEventInterstitialListener extends CustomEventListener
{
  public abstract void onReceivedAd();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.mediation.customevent.CustomEventInterstitialListener
 * JD-Core Version:    0.6.2
 */