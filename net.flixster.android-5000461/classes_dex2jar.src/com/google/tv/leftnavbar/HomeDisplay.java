package com.google.tv.leftnavbar;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.flixster.android.utils.Logger;

@TargetApi(9)
final class HomeDisplay
{
  private static final String TAG = "LeftNavBar-Home";
  private final Context mContext;
  private boolean mExpanded;
  private Drawable mIcon;
  private Drawable mLogo;
  private Mode mMode;
  private View mView;

  HomeDisplay(Context paramContext, ViewGroup paramViewGroup, TypedArray paramTypedArray)
  {
    this.mContext = paramContext;
    this.mMode = Mode.ICON;
    ApplicationInfo localApplicationInfo = paramContext.getApplicationInfo();
    PackageManager localPackageManager = paramContext.getPackageManager();
    loadLogo(paramTypedArray, localPackageManager, localApplicationInfo);
    loadIcon(paramTypedArray, localPackageManager, localApplicationInfo);
    createView(paramViewGroup, paramTypedArray);
  }

  private void createView(ViewGroup paramViewGroup, TypedArray paramTypedArray)
  {
    this.mView = LayoutInflater.from(this.mContext).inflate(2130903097, paramViewGroup, false);
  }

  private void loadIcon(TypedArray paramTypedArray, PackageManager paramPackageManager, ApplicationInfo paramApplicationInfo)
  {
    if ((this.mContext instanceof Activity));
    try
    {
      this.mIcon = paramPackageManager.getActivityIcon(((Activity)this.mContext).getComponentName());
      if (this.mIcon == null)
        this.mIcon = paramApplicationInfo.loadIcon(paramPackageManager);
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
        Logger.e("LeftNavBar-Home", "Failed to load app icon.", localNameNotFoundException);
    }
  }

  private void loadLogo(TypedArray paramTypedArray, PackageManager paramPackageManager, ApplicationInfo paramApplicationInfo)
  {
    if ((this.mContext instanceof Activity));
    try
    {
      this.mLogo = paramPackageManager.getActivityLogo(((Activity)this.mContext).getComponentName());
      if (this.mLogo == null)
        this.mLogo = paramApplicationInfo.loadLogo(paramPackageManager);
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
        Logger.e("LeftNavBar-Home", "Failed to load app logo.", localNameNotFoundException);
    }
  }

  private void updateImage()
  {
    int i;
    ImageView localImageView;
    if ((this.mMode != Mode.ICON) && (this.mLogo != null) && ((this.mMode != Mode.BOTH) || (this.mExpanded)))
    {
      i = 0;
      localImageView = (ImageView)this.mView.findViewById(2131165399);
      if (i == 0)
        break label69;
    }
    label69: for (Drawable localDrawable = this.mIcon; ; localDrawable = this.mLogo)
    {
      localImageView.setImageDrawable(localDrawable);
      return;
      i = 1;
      break;
    }
  }

  View getView()
  {
    return this.mView;
  }

  boolean isVisible()
  {
    return this.mView.getVisibility() == 0;
  }

  HomeDisplay setAsUp(boolean paramBoolean)
  {
    View localView = this.mView.findViewById(2131165400);
    if (paramBoolean);
    for (int i = 0; ; i = 8)
    {
      localView.setVisibility(i);
      return this;
    }
  }

  HomeDisplay setExpanded(boolean paramBoolean)
  {
    this.mExpanded = paramBoolean;
    updateImage();
    return this;
  }

  HomeDisplay setImageMode(Mode paramMode)
  {
    this.mMode = paramMode;
    updateImage();
    return this;
  }

  public void setOnClickHomeListener(View.OnClickListener paramOnClickListener)
  {
    this.mView.setOnClickListener(paramOnClickListener);
  }

  HomeDisplay setVisible(boolean paramBoolean)
  {
    View localView = this.mView;
    if (paramBoolean);
    for (int i = 0; ; i = 8)
    {
      localView.setVisibility(i);
      return this;
    }
  }

  static enum Mode
  {
    static
    {
      BOTH = new Mode("BOTH", 2);
      Mode[] arrayOfMode = new Mode[3];
      arrayOfMode[0] = ICON;
      arrayOfMode[1] = LOGO;
      arrayOfMode[2] = BOTH;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.HomeDisplay
 * JD-Core Version:    0.6.2
 */