package com.google.tv.leftnavbar;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.ActionBar.OnMenuVisibilityListener;
import android.app.ActionBar.OnNavigationListener;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.SpinnerAdapter;

@TargetApi(11)
public final class LeftNavBar extends ActionBar
{
  public static final int DEFAULT_DISPLAY_OPTIONS = 43;
  public static final int DISPLAY_ALWAYS_EXPANDED = 32;
  public static final int DISPLAY_AUTO_EXPAND = 64;
  public static final int DISPLAY_SHOW_INDETERMINATE_PROGRESS = 256;
  public static final int DISPLAY_USE_LOGO_WHEN_EXPANDED = 128;
  private View mContent;
  private Context mContext;
  private boolean mIsOverlay;
  private LeftNavView mLeftNav;
  private TitleBarView mTitleBar;

  public LeftNavBar(Activity paramActivity)
  {
    initialize(paramActivity.getWindow());
  }

  public LeftNavBar(Dialog paramDialog)
  {
    initialize(paramDialog.getWindow());
  }

  private TabImpl convertTab(ActionBar.Tab paramTab)
  {
    if (paramTab == null)
      return null;
    if (!(paramTab instanceof TabImpl))
      throw new IllegalArgumentException("Invalid tab object.");
    return (TabImpl)paramTab;
  }

  private static boolean has(int paramInt1, int paramInt2)
  {
    return (paramInt1 & paramInt2) != 0;
  }

  private void initialize(Window paramWindow)
  {
    View localView = paramWindow.getDecorView();
    this.mContext = localView.getContext();
    this.mIsOverlay = paramWindow.hasFeature(9);
    this.mTitleBar = ((TitleBarView)localView.findViewById(2131165362));
    this.mLeftNav = ((LeftNavView)localView.findViewById(2131165363));
    this.mContent = localView.findViewById(2131165360);
    if ((this.mTitleBar == null) || (this.mLeftNav == null))
      throw new IllegalStateException(getClass().getSimpleName() + ": incompatible window decor!");
    setDisplayOptions(43);
    showOptionsMenu(true);
  }

  private void setLeftMargin(View paramView, int paramInt)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    localMarginLayoutParams.leftMargin = paramInt;
    paramView.setLayoutParams(localMarginLayoutParams);
  }

  private void setTopMargin(View paramView, int paramInt)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    localMarginLayoutParams.topMargin = paramInt;
    paramView.setLayoutParams(localMarginLayoutParams);
  }

  private void setVisible(boolean paramBoolean)
  {
    boolean bool = this.mIsOverlay;
    if (this.mLeftNav.setVisible(paramBoolean, bool))
      updateWindowLayout(bool);
  }

  private void updateTitleBar(boolean paramBoolean)
  {
    int i = getDisplayOptions();
    boolean bool1 = has(i, 8);
    boolean bool2 = has(i, 256);
    boolean bool3 = this.mTitleBar.isHorizontalProgressVisible();
    TitleBarView localTitleBarView = this.mTitleBar;
    if ((isShowing()) && ((bool1) || (bool2) || (bool3)));
    for (boolean bool4 = true; ; bool4 = false)
    {
      localTitleBarView.setVisible(bool4, paramBoolean);
      this.mTitleBar.setProgressVisible(bool2);
      return;
    }
  }

  private void updateWindowLayout(boolean paramBoolean)
  {
    updateTitleBar(paramBoolean);
    setLeftMargin(this.mTitleBar, this.mLeftNav.getApparentWidth(true));
    if (!this.mIsOverlay)
    {
      setLeftMargin(this.mContent, this.mLeftNav.getApparentWidth(false));
      setTopMargin(this.mContent, this.mTitleBar.getApparentHeight());
    }
  }

  public void addOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener paramOnMenuVisibilityListener)
  {
  }

  public void addTab(ActionBar.Tab paramTab)
  {
    addTab(paramTab, -2);
  }

  public void addTab(ActionBar.Tab paramTab, int paramInt)
  {
    if (getTabCount() == 0);
    for (boolean bool = true; ; bool = false)
    {
      addTab(paramTab, paramInt, bool);
      return;
    }
  }

  public void addTab(ActionBar.Tab paramTab, int paramInt, boolean paramBoolean)
  {
    this.mLeftNav.getTabs().add(convertTab(paramTab), paramInt, paramBoolean);
  }

  public void addTab(ActionBar.Tab paramTab, boolean paramBoolean)
  {
    addTab(paramTab, -2, paramBoolean);
  }

  public void dispatchMenuVisibilityChanged(boolean paramBoolean)
  {
  }

  public View getCustomView()
  {
    return this.mLeftNav.getCustomView();
  }

  public int getDisplayOptions()
  {
    return this.mLeftNav.getDisplayOptions();
  }

  public int getHeight()
  {
    return this.mLeftNav.getApparentWidth(true);
  }

  public int getNavigationItemCount()
  {
    switch (getNavigationMode())
    {
    default:
      throw new IllegalStateException("No count available for mode: " + getNavigationMode());
    case 2:
      return getTabCount();
    case 1:
    }
    return this.mLeftNav.getSpinner().getCount();
  }

  public int getNavigationMode()
  {
    return this.mLeftNav.getNavigationMode();
  }

  public int getSelectedNavigationIndex()
  {
    switch (getNavigationMode())
    {
    default:
      throw new IllegalStateException("No selection available for mode: " + getNavigationMode());
    case 2:
      ActionBar.Tab localTab = getSelectedTab();
      if (localTab != null)
        return localTab.getPosition();
      return -1;
    case 1:
    }
    return this.mLeftNav.getSpinner().getSelected();
  }

  public ActionBar.Tab getSelectedTab()
  {
    return this.mLeftNav.getTabs().getSelected();
  }

  public CharSequence getSubtitle()
  {
    return this.mTitleBar.getSubtitle();
  }

  public ActionBar.Tab getTabAt(int paramInt)
  {
    return this.mLeftNav.getTabs().get(paramInt);
  }

  public int getTabCount()
  {
    return this.mLeftNav.getTabs().getCount();
  }

  public CharSequence getTitle()
  {
    return this.mTitleBar.getTitle();
  }

  public void hide()
  {
    setVisible(false);
  }

  public boolean isShowing()
  {
    return this.mLeftNav.isVisible();
  }

  public ActionBar.Tab newTab()
  {
    return new TabImpl(this.mContext)
    {
      public void select()
      {
        LeftNavBar.this.selectTab(this);
      }
    };
  }

  public void removeAllTabs()
  {
    this.mLeftNav.getTabs().removeAll();
  }

  public void removeOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener paramOnMenuVisibilityListener)
  {
  }

  public void removeTab(ActionBar.Tab paramTab)
  {
    this.mLeftNav.getTabs().remove(convertTab(paramTab));
  }

  public void removeTabAt(int paramInt)
  {
    this.mLeftNav.getTabs().remove(paramInt);
  }

  public void selectTab(ActionBar.Tab paramTab)
  {
    this.mLeftNav.getTabs().select(convertTab(paramTab));
  }

  public void setBackgroundDrawable(Drawable paramDrawable)
  {
    this.mLeftNav.setBackgroundDrawable(paramDrawable);
  }

  public void setCustomView(int paramInt)
  {
    setCustomView(LayoutInflater.from(this.mContext).inflate(paramInt, this.mLeftNav, false));
  }

  public void setCustomView(View paramView)
  {
    this.mLeftNav.setCustomView(paramView);
  }

  public void setCustomView(View paramView, ActionBar.LayoutParams paramLayoutParams)
  {
    paramView.setLayoutParams(paramLayoutParams);
    setCustomView(paramView);
  }

  public void setDisplayHomeAsUpEnabled(boolean paramBoolean)
  {
    if (paramBoolean);
    for (int i = 4; ; i = 0)
    {
      setDisplayOptions(i, 4);
      return;
    }
  }

  public void setDisplayOptions(int paramInt)
  {
    int i = this.mLeftNav.setDisplayOptions(paramInt);
    if ((has(i, 32)) || (has(i, 64)) || (has(i, 8)) || (has(i, 256)))
      updateWindowLayout(false);
  }

  public void setDisplayOptions(int paramInt1, int paramInt2)
  {
    int i = getDisplayOptions();
    setDisplayOptions(paramInt1 & paramInt2 | i & (paramInt2 ^ 0xFFFFFFFF));
  }

  public void setDisplayShowCustomEnabled(boolean paramBoolean)
  {
    if (paramBoolean);
    for (int i = 16; ; i = 0)
    {
      setDisplayOptions(i, 16);
      return;
    }
  }

  public void setDisplayShowHomeEnabled(boolean paramBoolean)
  {
    if (paramBoolean);
    for (int i = 2; ; i = 0)
    {
      setDisplayOptions(i, 2);
      return;
    }
  }

  public void setDisplayShowTitleEnabled(boolean paramBoolean)
  {
    if (paramBoolean);
    for (int i = 8; ; i = 0)
    {
      setDisplayOptions(i, 8);
      return;
    }
  }

  public void setDisplayUseLogoEnabled(boolean paramBoolean)
  {
    if (paramBoolean);
    for (int i = 1; ; i = 0)
    {
      setDisplayOptions(i, 1);
      return;
    }
  }

  public void setIcon(int paramInt)
  {
  }

  public void setIcon(Drawable paramDrawable)
  {
  }

  public void setListNavigationCallbacks(SpinnerAdapter paramSpinnerAdapter, ActionBar.OnNavigationListener paramOnNavigationListener)
  {
    this.mLeftNav.getSpinner().setContent(paramSpinnerAdapter, paramOnNavigationListener);
  }

  public void setLogo(int paramInt)
  {
  }

  public void setLogo(Drawable paramDrawable)
  {
  }

  public void setNavigationMode(int paramInt)
  {
    this.mLeftNav.setNavigationMode(paramInt);
  }

  public void setOnClickHomeListener(View.OnClickListener paramOnClickListener)
  {
    this.mLeftNav.setOnClickHomeListener(paramOnClickListener);
  }

  public void setSelectedNavigationItem(int paramInt)
  {
    switch (getNavigationMode())
    {
    default:
      throw new IllegalStateException("Cannot set selection on mode: " + getNavigationMode());
    case 2:
      selectTab(getTabAt(paramInt));
      return;
    case 1:
    }
    this.mLeftNav.getSpinner().setSelected(paramInt);
  }

  public void setShowHideAnimationEnabled(boolean paramBoolean)
  {
    this.mLeftNav.setAnimationsEnabled(paramBoolean);
    this.mTitleBar.setAnimationsEnabled(paramBoolean);
  }

  public void setShowHorizontalProgress(int paramInt)
  {
    this.mTitleBar.setHorizontalProgress(paramInt);
    updateWindowLayout(false);
  }

  public void setSubtitle(int paramInt)
  {
    setSubtitle(this.mContext.getString(paramInt));
  }

  public void setSubtitle(CharSequence paramCharSequence)
  {
    this.mTitleBar.setSubtitle(paramCharSequence);
  }

  public void setTitle(int paramInt)
  {
    setTitle(this.mContext.getString(paramInt));
  }

  public void setTitle(CharSequence paramCharSequence)
  {
    this.mTitleBar.setTitle(paramCharSequence);
  }

  public void show()
  {
    setVisible(true);
  }

  public void showOptionsMenu(boolean paramBoolean)
  {
    this.mLeftNav.showOptionsMenu(Boolean.valueOf(paramBoolean));
  }

  public ActionMode startActionMode(ActionMode.Callback paramCallback)
  {
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.LeftNavBar
 * JD-Core Version:    0.6.2
 */