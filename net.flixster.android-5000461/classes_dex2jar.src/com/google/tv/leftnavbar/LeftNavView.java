package com.google.tv.leftnavbar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.TargetApi;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.FocusFinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import java.util.ArrayList;

@TargetApi(11)
public final class LeftNavView extends LinearLayout
{
  private final int mAnimationDuration;
  private boolean mAnimationsEnabled;
  private final int mApparentWidthCollapsed;
  private final int mApparentWidthExpanded;
  private int mDisplayOptions;
  private boolean mExpanded;
  private final HomeDisplay mHome;
  private int mNavigationMode;
  private final OptionsDisplay mOptions;
  private final SpinnerDisplay mSpinner;
  private final TabDisplay mTabs;
  private final VisibilityController mVisibilityController = new VisibilityController(this);
  private ValueAnimator mWidthAnimator;
  private final int mWidthCollapsed;
  private final int mWidthExpanded;

  public LeftNavView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    LayoutInflater.from(paramContext).inflate(2130903096, this, true);
    setOrientation(1);
    this.mHome = new HomeDisplay(paramContext, this, null).setVisible(false);
    this.mTabs = new TabDisplay(paramContext, this, null).setVisible(false);
    this.mOptions = new OptionsDisplay(paramContext, this, null).setVisible(false);
    this.mSpinner = new SpinnerDisplay(paramContext, this, null).setVisible(false);
    Resources localResources = paramContext.getResources();
    this.mWidthCollapsed = localResources.getDimensionPixelSize(2131361891);
    this.mWidthExpanded = localResources.getDimensionPixelSize(2131361893);
    this.mApparentWidthCollapsed = localResources.getDimensionPixelSize(2131361892);
    this.mApparentWidthExpanded = localResources.getDimensionPixelSize(2131361894);
    this.mAnimationDuration = localResources.getInteger(17694720);
    this.mNavigationMode = 0;
    setNavigationMode(0);
  }

  private CustomViewWrapper getCustomViewWrapper()
  {
    ViewGroup localViewGroup = getMainSection();
    if (localViewGroup.getChildCount() == 3)
      return (CustomViewWrapper)localViewGroup.getChildAt(2);
    return null;
  }

  private ViewGroup getMainSection()
  {
    return (ViewGroup)findViewById(2131165397);
  }

  private static boolean has(int paramInt1, int paramInt2)
  {
    return (paramInt1 & paramInt2) != 0;
  }

  private boolean hasCustomView()
  {
    return getCustomViewWrapper() != null;
  }

  private boolean hasVisibleCustomView()
  {
    return (hasCustomView()) && (getCustomViewWrapper().getVisibility() == 0);
  }

  private void setContentExpanded(boolean paramBoolean)
  {
    this.mTabs.setExpanded(paramBoolean);
    this.mOptions.setExpanded(paramBoolean);
    this.mHome.setExpanded(paramBoolean);
    this.mSpinner.setExpanded(paramBoolean);
    if (hasCustomView())
      getCustomView().setActivated(paramBoolean);
  }

  private void setCustomViewVisibility(boolean paramBoolean)
  {
    CustomViewWrapper localCustomViewWrapper = getCustomViewWrapper();
    if (localCustomViewWrapper != null)
      if (!paramBoolean)
        break label21;
    label21: for (int i = 0; ; i = 8)
    {
      localCustomViewWrapper.setVisibility(i);
      return;
    }
  }

  private void setExpanded(boolean paramBoolean)
  {
    if ((this.mAnimationsEnabled) && (isVisible()));
    for (boolean bool = true; ; bool = false)
    {
      setExpanded(paramBoolean, bool);
      return;
    }
  }

  private void setExpanded(final boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.mExpanded == paramBoolean1)
      return;
    if (paramBoolean2)
    {
      if (this.mWidthAnimator != null)
        this.mWidthAnimator.cancel();
      int[] arrayOfInt = new int[2];
      arrayOfInt[0] = getLayoutParams().width;
      if (paramBoolean1);
      for (int j = this.mWidthExpanded; ; j = this.mWidthCollapsed)
      {
        arrayOfInt[1] = j;
        this.mWidthAnimator = ValueAnimator.ofInt(arrayOfInt);
        this.mWidthAnimator.setDuration(this.mAnimationDuration);
        this.mWidthAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
          public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
          {
            LeftNavView.this.setViewWidth(((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue());
          }
        });
        this.mWidthAnimator.addListener(new AnimatorListenerAdapter()
        {
          public void onAnimationEnd(Animator paramAnonymousAnimator)
          {
            if (paramBoolean1)
              LeftNavView.this.setContentExpanded(true);
          }

          public void onAnimationStart(Animator paramAnonymousAnimator)
          {
            if (!paramBoolean1)
              LeftNavView.this.setContentExpanded(false);
          }
        });
        this.mWidthAnimator.start();
        this.mExpanded = paramBoolean1;
        return;
      }
    }
    if (paramBoolean1);
    for (int i = this.mWidthExpanded; ; i = this.mWidthCollapsed)
    {
      setViewWidth(i);
      setContentExpanded(paramBoolean1);
      break;
    }
  }

  private void setExpandedState()
  {
    if (has(this.mDisplayOptions, 64))
    {
      setExpanded(hasFocus(), false);
      return;
    }
    setExpanded(has(this.mDisplayOptions, 32), false);
  }

  private void setHomeMode()
  {
    HomeDisplay.Mode localMode;
    if (has(this.mDisplayOptions, 128))
      localMode = HomeDisplay.Mode.BOTH;
    while (true)
    {
      this.mHome.setImageMode(localMode);
      return;
      if (has(this.mDisplayOptions, 1))
        localMode = HomeDisplay.Mode.LOGO;
      else
        localMode = HomeDisplay.Mode.ICON;
    }
  }

  private void setNavigationModeVisibility(int paramInt, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default:
      return;
    case 2:
      this.mTabs.setVisible(paramBoolean);
      return;
    case 1:
    }
    this.mSpinner.setVisible(paramBoolean);
  }

  private void setViewWidth(int paramInt)
  {
    ViewGroup.LayoutParams localLayoutParams = getLayoutParams();
    localLayoutParams.width = paramInt;
    setLayoutParams(localLayoutParams);
  }

  public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2)
  {
    if (paramInt1 == 2)
      super.addFocusables(paramArrayList, paramInt1, paramInt2);
    while ((paramInt1 != 17) && (!hasFocus()))
      return;
    int i;
    if (!hasFocus())
    {
      i = paramArrayList.size();
      switch (this.mNavigationMode)
      {
      default:
        if (hasCustomView())
          getCustomView().addFocusables(paramArrayList, paramInt1, paramInt2);
        break;
      case 2:
      case 1:
      }
    }
    while (paramArrayList.size() <= i)
    {
      super.addFocusables(paramArrayList, paramInt1, paramInt2);
      return;
      this.mTabs.getView().addFocusables(paramArrayList, paramInt1, paramInt2);
      continue;
      this.mSpinner.getView().addFocusables(paramArrayList, paramInt1, paramInt2);
    }
  }

  public View focusSearch(View paramView, int paramInt)
  {
    if ((hasFocus()) && (paramInt != 66))
      return FocusFinder.getInstance().findNextFocus(this, paramView, paramInt);
    return super.focusSearch(paramView, paramInt);
  }

  public int getApparentWidth(boolean paramBoolean)
  {
    if ((!isVisible()) && (!paramBoolean))
      return 0;
    if ((!has(this.mDisplayOptions, 64)) && (has(this.mDisplayOptions, 32)));
    for (int i = 0; i != 0; i = 1)
      return this.mApparentWidthCollapsed;
    return this.mApparentWidthExpanded;
  }

  public View getCustomView()
  {
    CustomViewWrapper localCustomViewWrapper = getCustomViewWrapper();
    if (localCustomViewWrapper != null)
      return localCustomViewWrapper.getView();
    return null;
  }

  public int getDisplayOptions()
  {
    return this.mDisplayOptions;
  }

  public int getNavigationMode()
  {
    return this.mNavigationMode;
  }

  public SpinnerDisplay getSpinner()
  {
    return this.mSpinner;
  }

  public TabDisplay getTabs()
  {
    return this.mTabs;
  }

  public boolean isVisible()
  {
    return this.mVisibilityController.isVisible();
  }

  protected void onDescendantFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if (has(this.mDisplayOptions, 64))
      setExpanded(paramBoolean);
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    addView(this.mHome.getView(), 0);
    addView(this.mOptions.getView(), 2);
    ViewGroup localViewGroup = getMainSection();
    localViewGroup.addView(this.mTabs.getView());
    localViewGroup.addView(this.mSpinner.getView());
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (hasVisibleCustomView())
      getCustomViewWrapper().onPostLayout(this);
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    if (hasVisibleCustomView())
      getCustomViewWrapper().onPostMeasure(this);
  }

  public void setAnimationsEnabled(boolean paramBoolean)
  {
    this.mAnimationsEnabled = paramBoolean;
  }

  public void setCustomView(View paramView)
  {
    ViewGroup localViewGroup = getMainSection();
    CustomViewWrapper localCustomViewWrapper = getCustomViewWrapper();
    if (localCustomViewWrapper != null)
    {
      localCustomViewWrapper.detach();
      localViewGroup.removeView(localCustomViewWrapper);
    }
    if (paramView != null)
    {
      paramView.setActivated(this.mExpanded);
      localViewGroup.addView(new CustomViewWrapper(getContext(), paramView));
      setCustomViewVisibility(has(this.mDisplayOptions, 16));
    }
  }

  public int setDisplayOptions(int paramInt)
  {
    int i = paramInt ^ this.mDisplayOptions;
    this.mDisplayOptions = paramInt;
    if (has(i, 2))
      this.mHome.setVisible(has(paramInt, 2));
    if ((has(i, 1)) || (has(i, 128)))
      setHomeMode();
    if (has(i, 4))
      this.mHome.setAsUp(has(paramInt, 4));
    if (has(i, 16))
      setCustomViewVisibility(has(this.mDisplayOptions, 16));
    if ((has(i, 64)) || (has(i, 32)))
      setExpandedState();
    return i;
  }

  public void setNavigationMode(int paramInt)
  {
    if (this.mNavigationMode == paramInt)
      return;
    setNavigationModeVisibility(this.mNavigationMode, false);
    setNavigationModeVisibility(paramInt, true);
    this.mNavigationMode = paramInt;
  }

  public void setOnClickHomeListener(View.OnClickListener paramOnClickListener)
  {
    this.mHome.setOnClickHomeListener(paramOnClickListener);
  }

  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
  {
    VisibilityController localVisibilityController = this.mVisibilityController;
    if ((paramBoolean2) && (this.mAnimationsEnabled));
    for (boolean bool = true; ; bool = false)
      return localVisibilityController.setVisible(paramBoolean1, bool);
  }

  public void showOptionsMenu(Boolean paramBoolean)
  {
    this.mOptions.setVisible(paramBoolean.booleanValue());
  }

  private static final class CustomViewWrapper extends ViewGroup
  {
    private final View mView;

    CustomViewWrapper(Context paramContext, View paramView)
    {
      super();
      setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
      this.mView = paramView;
      if (!(paramView.getLayoutParams() instanceof ActionBar.LayoutParams))
        paramView.setLayoutParams(generateDefaultLayoutParams());
      addView(paramView);
    }

    private void checkDimensionsConsistency(int paramInt1, int paramInt2)
    {
      if (paramInt1 != paramInt2)
        throw new IllegalStateException("Inconsistent dimensions!");
    }

    private int findBottomOfAvailableSpace(LeftNavView paramLeftNavView)
    {
      int i = paramLeftNavView.getMeasuredHeight() - paramLeftNavView.getPaddingBottom();
      if (paramLeftNavView.mOptions.isVisible())
        i -= paramLeftNavView.mOptions.getView().getMeasuredHeight();
      return i;
    }

    private int findTopOfAvailableSpace(LeftNavView paramLeftNavView)
    {
      int i = paramLeftNavView.getPaddingTop();
      if (paramLeftNavView.mHome.isVisible())
        i += paramLeftNavView.mHome.getView().getMeasuredHeight();
      switch (paramLeftNavView.mNavigationMode)
      {
      default:
        return i;
      case 2:
        return i + paramLeftNavView.mTabs.getView().getMeasuredHeight();
      case 1:
      }
      return i + paramLeftNavView.mSpinner.getView().getMeasuredHeight();
    }

    void detach()
    {
      removeView(this.mView);
    }

    protected ViewGroup.LayoutParams generateDefaultLayoutParams()
    {
      return new ActionBar.LayoutParams(-1, -1);
    }

    View getView()
    {
      return this.mView;
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
    }

    void onPostLayout(LeftNavView paramLeftNavView)
    {
      int i = this.mView.getMeasuredWidth();
      int j = this.mView.getMeasuredHeight();
      ActionBar.LayoutParams localLayoutParams = (ActionBar.LayoutParams)this.mView.getLayoutParams();
      int k = getRight() - getLeft();
      int m = 0x7 & localLayoutParams.gravity;
      int n = 0;
      int i1;
      int i2;
      int i3;
      int i4;
      label190: int i5;
      int i6;
      switch (m)
      {
      case 2:
      case 4:
      default:
        i1 = 0x70 & localLayoutParams.gravity;
        i2 = (paramLeftNavView.getBottom() - paramLeftNavView.getTop() - paramLeftNavView.getPaddingTop() - paramLeftNavView.getPaddingBottom() - j) / 2 + paramLeftNavView.getPaddingTop();
        i3 = findTopOfAvailableSpace(paramLeftNavView);
        i4 = findBottomOfAvailableSpace(paramLeftNavView);
        if (getBottom() - getTop() != 0)
          checkDimensionsConsistency(i4 - i3, getBottom() - getTop());
        if (i1 == 16)
        {
          if (i2 < i3)
            i1 = 48;
        }
        else
        {
          i5 = i4 - i3;
          i6 = 0;
          switch (i1)
          {
          default:
          case 16:
          case 48:
          case 80:
          }
        }
        break;
      case 1:
      case 3:
      case 5:
      }
      while (true)
      {
        this.mView.layout(n, i6, n + i, i6 + j);
        return;
        n = (k - i) / 2;
        break;
        n = localLayoutParams.leftMargin;
        break;
        n = k - i - localLayoutParams.rightMargin;
        break;
        if (i2 + j <= i4)
          break label190;
        i1 = 80;
        break label190;
        i6 = i2 - i3;
        continue;
        i6 = localLayoutParams.topMargin;
        continue;
        i6 = i5 - j - localLayoutParams.bottomMargin;
      }
    }

    void onPostMeasure(LeftNavView paramLeftNavView)
    {
      int i = paramLeftNavView.getMeasuredWidth();
      int j = paramLeftNavView.getMeasuredHeight();
      int k = findTopOfAvailableSpace(paramLeftNavView);
      int m = findBottomOfAvailableSpace(paramLeftNavView);
      int n = i - paramLeftNavView.getPaddingLeft() - paramLeftNavView.getPaddingRight();
      int i1 = m - k;
      int i2 = j / 2 - k;
      int i3 = m - j / 2;
      if (getMeasuredWidth() != 0)
        checkDimensionsConsistency(n, getMeasuredWidth());
      if (getMeasuredHeight() != 0)
        checkDimensionsConsistency(i1, getMeasuredHeight());
      ActionBar.LayoutParams localLayoutParams = (ActionBar.LayoutParams)this.mView.getLayoutParams();
      int i4 = localLayoutParams.leftMargin + localLayoutParams.rightMargin;
      int i5 = localLayoutParams.topMargin + localLayoutParams.bottomMargin;
      int i6;
      int i7;
      label166: int i8;
      int i9;
      if (localLayoutParams.width != -2)
      {
        i6 = 1073741824;
        if (localLayoutParams.width < 0)
          break label294;
        i7 = Math.min(localLayoutParams.width, n);
        i8 = Math.max(0, i7 - i4);
        if (localLayoutParams.height == -2)
          break label301;
        i9 = 1073741824;
        label191: if (localLayoutParams.height < 0)
          break label308;
      }
      label294: label301: label308: for (int i10 = Math.min(localLayoutParams.height, i1); ; i10 = i1)
      {
        int i11 = Math.max(0, i10 - i5);
        if (((0x70 & localLayoutParams.gravity) == 16) && (localLayoutParams.height == -1) && (i2 > 0) && (i3 > 0))
          i11 = 2 * Math.min(i2, i3);
        this.mView.measure(View.MeasureSpec.makeMeasureSpec(i8, i6), View.MeasureSpec.makeMeasureSpec(i11, i9));
        return;
        i6 = -2147483648;
        break;
        i7 = n;
        break label166;
        i9 = -2147483648;
        break label191;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.LeftNavView
 * JD-Core Version:    0.6.2
 */