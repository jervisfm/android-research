package com.google.tv.leftnavbar;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

final class OptionsDisplay
{
  private final Context mContext;
  private boolean mExpanded;
  private ViewGroup mView;

  OptionsDisplay(Context paramContext, ViewGroup paramViewGroup, TypedArray paramTypedArray)
  {
    this.mContext = paramContext;
    createView(paramViewGroup, paramTypedArray);
  }

  private View configureOption(View paramView, CharSequence paramCharSequence, boolean paramBoolean)
  {
    getOptionIcon(paramView).setEnabled(paramBoolean);
    getOptionTitle(paramView).setText(paramCharSequence);
    return paramView;
  }

  private void createView(ViewGroup paramViewGroup, TypedArray paramTypedArray)
  {
    this.mView = ((ViewGroup)LayoutInflater.from(this.mContext).inflate(2130903099, paramViewGroup, false));
    View localView = this.mView.findViewById(2131165402);
    configureOption(localView, this.mContext.getResources().getString(2131493309), true);
    localView.setClickable(true);
    localView.setFocusable(true);
    localView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if ((OptionsDisplay.this.mContext instanceof Activity))
          ((Activity)OptionsDisplay.this.mContext).openOptionsMenu();
      }
    });
    setDuplicateParentState(getOptionIcon(localView));
    setDuplicateParentState(getOptionTitle(localView));
  }

  private static ImageView getOptionIcon(View paramView)
  {
    return (ImageView)paramView.findViewById(2131165281);
  }

  private static TextView getOptionTitle(View paramView)
  {
    return (TextView)paramView.findViewById(2131165295);
  }

  private ViewGroup getOptionsContainer()
  {
    return (ViewGroup)this.mView.findViewById(2131165401);
  }

  private void refreshExpandedState()
  {
    setOptionExpanded(this.mView.getChildAt(1), this.mExpanded);
    ViewGroup localViewGroup = getOptionsContainer();
    for (int i = 0; ; i++)
    {
      if (i >= localViewGroup.getChildCount())
        return;
      setOptionExpanded(localViewGroup.getChildAt(i), this.mExpanded);
    }
  }

  private void setDuplicateParentState(View paramView)
  {
    paramView.setDuplicateParentStateEnabled(true);
    ViewGroup localViewGroup = (ViewGroup)paramView.getParent();
    if (localViewGroup == null)
      return;
    int i = localViewGroup.indexOfChild(paramView);
    localViewGroup.removeViewAt(i);
    localViewGroup.addView(paramView, i);
  }

  private static void setOptionExpanded(View paramView, boolean paramBoolean)
  {
    TextView localTextView = getOptionTitle(paramView);
    if (paramBoolean);
    for (int i = 0; ; i = 8)
    {
      localTextView.setVisibility(i);
      return;
    }
  }

  View getView()
  {
    return this.mView;
  }

  boolean isVisible()
  {
    return this.mView.getVisibility() == 0;
  }

  OptionsDisplay setExpanded(boolean paramBoolean)
  {
    this.mExpanded = paramBoolean;
    refreshExpandedState();
    return this;
  }

  OptionsDisplay setVisible(boolean paramBoolean)
  {
    ViewGroup localViewGroup = this.mView;
    if (paramBoolean);
    for (int i = 0; ; i = 8)
    {
      localViewGroup.setVisibility(i);
      return this;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.OptionsDisplay
 * JD-Core Version:    0.6.2
 */