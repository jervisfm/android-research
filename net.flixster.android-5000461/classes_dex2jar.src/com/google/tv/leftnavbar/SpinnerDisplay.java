package com.google.tv.leftnavbar;

import android.annotation.TargetApi;
import android.app.ActionBar.OnNavigationListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

@TargetApi(11)
final class SpinnerDisplay
{
  private final Context mContext;
  private boolean mExpanded;
  private ActionBar.OnNavigationListener mListener;
  private Spinner mView;

  SpinnerDisplay(Context paramContext, ViewGroup paramViewGroup, TypedArray paramTypedArray)
  {
    this.mContext = paramContext;
    createView(paramViewGroup, paramTypedArray);
  }

  private void createView(ViewGroup paramViewGroup, TypedArray paramTypedArray)
  {
    this.mView = ((Spinner)LayoutInflater.from(this.mContext).inflate(2130903100, paramViewGroup, false));
    this.mView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
    {
      public void onItemSelected(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        if (SpinnerDisplay.this.mListener != null)
          SpinnerDisplay.this.mListener.onNavigationItemSelected(paramAnonymousInt, paramAnonymousLong);
        SpinnerDisplay.this.refreshSelectedItem();
      }

      public void onNothingSelected(AdapterView<?> paramAnonymousAdapterView)
      {
      }
    });
  }

  private void refreshSelectedItem()
  {
    View localView = this.mView.getSelectedView();
    if (localView == null)
      return;
    localView.setActivated(this.mExpanded);
  }

  int getCount()
  {
    return this.mView.getCount();
  }

  int getSelected()
  {
    return this.mView.getSelectedItemPosition();
  }

  View getView()
  {
    return this.mView;
  }

  void setContent(SpinnerAdapter paramSpinnerAdapter, ActionBar.OnNavigationListener paramOnNavigationListener)
  {
    this.mListener = paramOnNavigationListener;
    this.mView.setAdapter(paramSpinnerAdapter);
    refreshSelectedItem();
  }

  SpinnerDisplay setExpanded(boolean paramBoolean)
  {
    this.mExpanded = paramBoolean;
    refreshSelectedItem();
    return this;
  }

  void setSelected(int paramInt)
  {
    this.mView.setSelection(paramInt);
  }

  SpinnerDisplay setVisible(boolean paramBoolean)
  {
    Spinner localSpinner = this.mView;
    if (paramBoolean);
    for (int i = 0; ; i = 8)
    {
      localSpinner.setVisibility(i);
      return this;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.SpinnerDisplay
 * JD-Core Version:    0.6.2
 */