package com.google.tv.leftnavbar;

import android.annotation.TargetApi;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@TargetApi(11)
final class TabDisplay
{
  public static final int LAST_POSITION = -2;
  private static final TabImpl NONE = null;
  private final TabAdapter mAdapter;
  private final Context mContext;
  private boolean mExpanded;
  private TabListView mList;

  TabDisplay(Context paramContext, ViewGroup paramViewGroup, TypedArray paramTypedArray)
  {
    this.mContext = paramContext;
    this.mAdapter = new TabAdapter(paramContext);
    createView(paramViewGroup);
  }

  private void createView(ViewGroup paramViewGroup)
  {
    this.mList = ((TabListView)LayoutInflater.from(this.mContext).inflate(2130903102, paramViewGroup, false));
    this.mList.setAdapter(this.mAdapter);
    this.mList.setItemsCanFocus(true);
    this.mList.setDescendantFocusability(131072);
  }

  private static void detachFromParent(View paramView)
  {
    if (paramView == null);
    ViewGroup localViewGroup;
    do
    {
      return;
      localViewGroup = (ViewGroup)paramView.getParent();
    }
    while (localViewGroup == null);
    localViewGroup.removeView(paramView);
  }

  private void onSelectionChanged(TabImpl paramTabImpl1, TabImpl paramTabImpl2)
  {
    boolean bool = this.mContext instanceof Activity;
    FragmentTransaction localFragmentTransaction = null;
    if (bool)
      localFragmentTransaction = ((Activity)this.mContext).getFragmentManager().beginTransaction().disallowAddToBackStack();
    if (paramTabImpl1 == paramTabImpl2)
      if ((paramTabImpl2 != NONE) && (paramTabImpl2.getCallback() != null))
        paramTabImpl2.getCallback().onTabReselected(paramTabImpl2, localFragmentTransaction);
    while (true)
    {
      if ((localFragmentTransaction != null) && (!localFragmentTransaction.isEmpty()))
        localFragmentTransaction.commit();
      this.mList.setHighlighted(this.mAdapter.getPosition(paramTabImpl2));
      return;
      if ((paramTabImpl1 != NONE) && (paramTabImpl1.getCallback() != null))
        paramTabImpl1.getCallback().onTabUnselected(paramTabImpl1, localFragmentTransaction);
      if ((paramTabImpl2 != NONE) && (paramTabImpl2.getCallback() != null))
        paramTabImpl2.getCallback().onTabSelected(paramTabImpl2, localFragmentTransaction);
    }
  }

  void add(TabImpl paramTabImpl, int paramInt, boolean paramBoolean)
  {
    if (paramInt == -2)
      paramInt = this.mAdapter.getCount();
    this.mAdapter.insert(paramTabImpl, paramInt);
    if (paramBoolean)
      select(paramTabImpl);
  }

  TabImpl get(int paramInt)
  {
    return (TabImpl)this.mAdapter.getItem(paramInt);
  }

  int getCount()
  {
    return this.mAdapter.getCount();
  }

  TabImpl getSelected()
  {
    return this.mAdapter.getSelected();
  }

  View getView()
  {
    return this.mList;
  }

  void remove(int paramInt)
  {
    remove((TabImpl)this.mAdapter.getItem(paramInt));
  }

  void remove(TabImpl paramTabImpl)
  {
    this.mAdapter.remove(paramTabImpl);
  }

  void removeAll()
  {
    this.mAdapter.clear();
  }

  void select(TabImpl paramTabImpl)
  {
    this.mAdapter.setSelected(paramTabImpl);
  }

  TabDisplay setExpanded(boolean paramBoolean)
  {
    this.mExpanded = paramBoolean;
    this.mAdapter.refresh();
    return this;
  }

  TabDisplay setVisible(boolean paramBoolean)
  {
    TabListView localTabListView = this.mList;
    if (paramBoolean);
    for (int i = 0; ; i = 8)
    {
      localTabListView.setVisibility(i);
      this.mAdapter.setSelectionActive(paramBoolean);
      return this;
    }
  }

  private final class TabAdapter extends ArrayAdapter<TabImpl>
  {
    private final Map<TabImpl, TabFrame> mCachedViews = new HashMap();
    private boolean mIsSelectionActive = true;
    private TabImpl mSavedSelection = TabDisplay.NONE;
    private TabImpl mSelection = TabDisplay.NONE;

    TabAdapter(Context arg2)
    {
      super(0);
    }

    private boolean isSelected(TabImpl paramTabImpl)
    {
      return (paramTabImpl != TabDisplay.NONE) && (paramTabImpl == getSelected());
    }

    private void setSelectionState(TabImpl paramTabImpl, boolean paramBoolean)
    {
      if ((paramTabImpl != TabDisplay.NONE) && (this.mCachedViews.containsKey(paramTabImpl)))
        ((TabFrame)this.mCachedViews.get(paramTabImpl)).select(paramBoolean);
    }

    private void updatePositions(boolean paramBoolean)
    {
      int i = 0;
      if (i >= getCount())
        return;
      TabImpl localTabImpl = (TabImpl)getItem(i);
      if (paramBoolean);
      for (int j = -1; ; j = i)
      {
        localTabImpl.setPosition(j);
        i++;
        break;
      }
    }

    public void clear()
    {
      updatePositions(true);
      for (int i = 0; ; i++)
      {
        if (i >= getCount())
        {
          this.mCachedViews.clear();
          setSelected(TabDisplay.NONE);
          super.clear();
          return;
        }
        TabDisplay.detachFromParent(((TabImpl)getItem(i)).getCustomView());
      }
    }

    public int getItemViewType(int paramInt)
    {
      return -1;
    }

    public TabImpl getSelected()
    {
      if (this.mIsSelectionActive)
        return this.mSelection;
      return this.mSavedSelection;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      final TabImpl localTabImpl = (TabImpl)getItem(paramInt);
      TabFrame localTabFrame2;
      if (!this.mCachedViews.containsKey(localTabImpl))
      {
        localTabFrame2 = (TabFrame)LayoutInflater.from(getContext()).inflate(2130903101, paramViewGroup, false);
        if (!localTabImpl.hasCustomView())
          break label133;
        localTabFrame2.configureCustom(localTabImpl.getCustomView());
      }
      while (true)
      {
        localTabFrame2.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            TabDisplay.TabAdapter.this.setSelected(localTabImpl);
          }
        });
        this.mCachedViews.put(localTabImpl, localTabFrame2);
        setSelectionState(localTabImpl, isSelected(localTabImpl));
        TabFrame localTabFrame1 = (TabFrame)this.mCachedViews.get(localTabImpl);
        localTabFrame1.expand(TabDisplay.this.mExpanded);
        return localTabFrame1;
        label133: localTabFrame2.configureNormal(localTabImpl.getIcon(), localTabImpl.getText());
      }
    }

    public void insert(TabImpl paramTabImpl, int paramInt)
    {
      super.insert(paramTabImpl, paramInt);
      updatePositions(false);
    }

    public void refresh()
    {
      Iterator localIterator = this.mCachedViews.values().iterator();
      while (true)
      {
        if (!localIterator.hasNext())
          return;
        ((TabFrame)localIterator.next()).expand(TabDisplay.this.mExpanded);
      }
    }

    public void remove(TabImpl paramTabImpl)
    {
      TabDisplay.detachFromParent(paramTabImpl.getCustomView());
      this.mCachedViews.remove(paramTabImpl);
      super.remove(paramTabImpl);
      updatePositions(false);
      if (isSelected(paramTabImpl))
        if (getCount() != 0)
          break label58;
      label58: for (TabImpl localTabImpl = TabDisplay.NONE; ; localTabImpl = (TabImpl)getItem(Math.max(0, -1 + paramTabImpl.getPosition())))
      {
        setSelected(localTabImpl);
        paramTabImpl.setPosition(-1);
        return;
      }
    }

    public void setSelected(TabImpl paramTabImpl)
    {
      if (!this.mIsSelectionActive)
      {
        this.mSavedSelection = paramTabImpl;
        return;
      }
      TabImpl localTabImpl = this.mSelection;
      this.mSelection = paramTabImpl;
      if (localTabImpl != this.mSelection)
      {
        setSelectionState(localTabImpl, false);
        setSelectionState(this.mSelection, true);
      }
      TabDisplay.this.onSelectionChanged(localTabImpl, this.mSelection);
    }

    public void setSelectionActive(boolean paramBoolean)
    {
      if (paramBoolean == this.mIsSelectionActive)
        return;
      if (paramBoolean)
      {
        this.mIsSelectionActive = true;
        setSelected(this.mSavedSelection);
        this.mSavedSelection = TabDisplay.NONE;
        return;
      }
      this.mSavedSelection = this.mSelection;
      setSelected(TabDisplay.NONE);
      this.mIsSelectionActive = false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.TabDisplay
 * JD-Core Version:    0.6.2
 */