package com.google.tv.leftnavbar;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@TargetApi(11)
final class TabFrame extends LinearLayout
{
  private boolean mConfigured;
  private boolean mIsCustom;

  public TabFrame(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  private ImageView getIcon()
  {
    return (ImageView)findViewById(2131165281);
  }

  private TextView getTitle()
  {
    return (TextView)findViewById(2131165295);
  }

  private void markConfigured(boolean paramBoolean)
  {
    if (this.mConfigured)
      throw new IllegalStateException("Frame already configured.");
    this.mConfigured = true;
    this.mIsCustom = paramBoolean;
  }

  public void configureCustom(View paramView)
  {
    markConfigured(true);
    setBackgroundDrawable(null);
    paramView.setFocusable(false);
    paramView.setFocusableInTouchMode(false);
    paramView.setClickable(false);
    paramView.setDuplicateParentStateEnabled(true);
    removeAllViews();
    addView(paramView);
  }

  public void configureNormal(Drawable paramDrawable, CharSequence paramCharSequence)
  {
    markConfigured(false);
    getIcon().setImageDrawable(paramDrawable);
    getTitle().setText(paramCharSequence);
  }

  public void expand(boolean paramBoolean)
  {
    TextView localTextView;
    if (!this.mIsCustom)
    {
      localTextView = getTitle();
      if (!paramBoolean)
        break label29;
    }
    label29: for (int i = 0; ; i = 8)
    {
      localTextView.setVisibility(i);
      setActivated(paramBoolean);
      return;
    }
  }

  public void select(boolean paramBoolean)
  {
    super.setSelected(paramBoolean);
  }

  public void setSelected(boolean paramBoolean)
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.TabFrame
 * JD-Core Version:    0.6.2
 */