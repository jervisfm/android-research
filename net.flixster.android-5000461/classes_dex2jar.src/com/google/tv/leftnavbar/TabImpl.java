package com.google.tv.leftnavbar;

import android.annotation.TargetApi;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;

@TargetApi(11)
abstract class TabImpl extends ActionBar.Tab
{
  private ActionBar.TabListener mCallback;
  private final Context mContext;
  private View mCustomView;
  private Drawable mIcon;
  private int mPosition;
  private Object mTag;
  private CharSequence mText;

  public TabImpl(Context paramContext)
  {
    this.mContext = paramContext;
  }

  public ActionBar.TabListener getCallback()
  {
    return this.mCallback;
  }

  public CharSequence getContentDescription()
  {
    return null;
  }

  public View getCustomView()
  {
    return this.mCustomView;
  }

  public Drawable getIcon()
  {
    return this.mIcon;
  }

  public int getPosition()
  {
    return this.mPosition;
  }

  public Object getTag()
  {
    return this.mTag;
  }

  public CharSequence getText()
  {
    return this.mText;
  }

  boolean hasCustomView()
  {
    return this.mCustomView != null;
  }

  public ActionBar.Tab setContentDescription(int paramInt)
  {
    return null;
  }

  public ActionBar.Tab setContentDescription(CharSequence paramCharSequence)
  {
    return null;
  }

  public ActionBar.Tab setCustomView(int paramInt)
  {
    return setCustomView(LayoutInflater.from(this.mContext).inflate(paramInt, null));
  }

  public ActionBar.Tab setCustomView(View paramView)
  {
    this.mCustomView = paramView;
    return this;
  }

  public ActionBar.Tab setIcon(int paramInt)
  {
    return setIcon(this.mContext.getResources().getDrawable(paramInt));
  }

  public ActionBar.Tab setIcon(Drawable paramDrawable)
  {
    this.mIcon = paramDrawable;
    return this;
  }

  public void setPosition(int paramInt)
  {
    this.mPosition = paramInt;
  }

  public ActionBar.Tab setTabListener(ActionBar.TabListener paramTabListener)
  {
    this.mCallback = paramTabListener;
    return this;
  }

  public ActionBar.Tab setTag(Object paramObject)
  {
    this.mTag = paramObject;
    return this;
  }

  public ActionBar.Tab setText(int paramInt)
  {
    return setText(this.mContext.getResources().getText(paramInt));
  }

  public ActionBar.Tab setText(CharSequence paramCharSequence)
  {
    this.mText = paramCharSequence;
    return this;
  }

  public String toString()
  {
    Object localObject;
    StringBuilder localStringBuilder;
    if (this.mTag != null)
    {
      localObject = this.mTag;
      localStringBuilder = new StringBuilder("Tab:");
      if (localObject == null)
        break label48;
    }
    label48: for (String str = localObject.toString(); ; str = "<no id>")
    {
      return str;
      localObject = this.mText;
      break;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.TabImpl
 * JD-Core Version:    0.6.2
 */