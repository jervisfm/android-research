package com.google.tv.leftnavbar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;
import java.util.ArrayList;

public final class TabListView extends ListView
{
  private boolean mClearingFocus;
  private int mHighlighted;

  public TabListView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2)
  {
    int i = this.mHighlighted - getFirstVisiblePosition();
    if ((!hasFocus()) && (i >= 0) && (i < getChildCount()) && (paramInt1 == 17))
    {
      setSelection(this.mHighlighted);
      getChildAt(i).addFocusables(paramArrayList, paramInt1, paramInt2);
      return;
    }
    super.addFocusables(paramArrayList, paramInt1, paramInt2);
  }

  public void clearChildFocus(View paramView)
  {
    if (this.mClearingFocus)
    {
      super.clearChildFocus(paramView);
      return;
    }
    post(new Runnable()
    {
      public void run()
      {
        TabListView.this.setSelection(TabListView.this.mHighlighted);
      }
    });
  }

  public void clearFocus()
  {
    this.mClearingFocus = true;
    super.clearFocus();
    this.mClearingFocus = false;
  }

  protected void dispatchDraw(Canvas paramCanvas)
  {
    super.dispatchDraw(paramCanvas);
    Drawable localDrawable = getDivider();
    if (localDrawable == null)
      return;
    Rect localRect = new Rect();
    localRect.left = getPaddingLeft();
    localRect.right = (getRight() - getLeft() - getPaddingRight());
    localRect.top = getPaddingTop();
    localRect.bottom = (getPaddingTop() + getDividerHeight());
    localDrawable.setBounds(localRect);
    localDrawable.draw(paramCanvas);
    localRect.top = (getBottom() - getTop() - getPaddingBottom() - getDividerHeight());
    localRect.bottom = (getBottom() - getTop() - getPaddingBottom());
    localDrawable.setBounds(localRect);
    localDrawable.draw(paramCanvas);
  }

  public void setHighlighted(int paramInt)
  {
    this.mHighlighted = paramInt;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.TabListView
 * JD-Core Version:    0.6.2
 */