package com.google.tv.leftnavbar;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public final class TitleBarView extends RelativeLayout
{
  private boolean mAnimationsEnabled;
  private ProgressBar mCircularProgress;
  private ProgressBar mHorizontalProgress;
  private boolean mIsLegacy;
  private ImageView mLeftIcon;
  private ImageView mRightIcon;
  private TextView mSubtitle;
  private int mSubtitleResource;
  private TextView mTitle;
  private int mTitleResource;
  private final VisibilityController mVisibilityController = new VisibilityController(this);

  public TitleBarView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet, 0);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, new int[] { 16842843, 16843245 });
    this.mIsLegacy = localTypedArray.getBoolean(localTypedArray.getIndex(1), false);
    if (this.mIsLegacy)
      this.mTitleResource = localTypedArray.getResourceId(localTypedArray.getIndex(0), 0);
    while (true)
    {
      localTypedArray.recycle();
      return;
      localTypedArray.recycle();
      localTypedArray = paramContext.obtainStyledAttributes(null, new int[] { 16843512, 16843513 }, 16843470, 0);
      this.mTitleResource = localTypedArray.getResourceId(localTypedArray.getIndex(0), 0);
      this.mSubtitleResource = localTypedArray.getResourceId(localTypedArray.getIndex(1), 0);
    }
  }

  private void disableSubtitle()
  {
    removeFromParent(this.mSubtitle);
    this.mSubtitle = null;
  }

  private static void removeFromParent(View paramView)
  {
    if (paramView == null);
    ViewParent localViewParent;
    do
    {
      return;
      localViewParent = paramView.getParent();
    }
    while (localViewParent == null);
    ((ViewGroup)localViewParent).removeView(paramView);
  }

  private void setIcon(ImageView paramImageView, Drawable paramDrawable, int paramInt)
  {
    if (paramImageView == null)
      return;
    if (paramDrawable != null)
    {
      paramDrawable.setAlpha(paramInt);
      paramImageView.setImageDrawable(paramDrawable);
      paramImageView.setVisibility(0);
      return;
    }
    paramImageView.setVisibility(8);
  }

  private void setTextStyle(TextView paramTextView, int paramInt)
  {
    if (paramInt != 0)
      paramTextView.setTextAppearance(getContext(), paramInt);
  }

  public void disableCircularProgress()
  {
    removeFromParent(this.mCircularProgress);
    this.mCircularProgress = null;
  }

  public void disableHorizontalProgress()
  {
    removeFromParent(this.mHorizontalProgress);
    this.mHorizontalProgress = null;
  }

  public void disableLeftIcon()
  {
    removeFromParent(this.mLeftIcon);
    this.mLeftIcon = null;
  }

  public void disableRightIcon()
  {
    removeFromParent(this.mRightIcon);
    this.mRightIcon = null;
  }

  public int getApparentHeight()
  {
    if (isVisible())
      return getContext().getResources().getDimensionPixelSize(2131361897);
    return 0;
  }

  public CharSequence getSubtitle()
  {
    return this.mSubtitle.getText();
  }

  public CharSequence getTitle()
  {
    return this.mTitle.getText();
  }

  public boolean isHorizontalProgressVisible()
  {
    return (this.mHorizontalProgress != null) && (this.mHorizontalProgress.getVisibility() == 0);
  }

  public boolean isVisible()
  {
    return this.mVisibilityController.isVisible();
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    if (getChildCount() == 0)
      LayoutInflater.from(getContext()).inflate(2130903178, this, true);
    this.mTitle = ((TextView)findViewById(2131165295));
    this.mSubtitle = ((TextView)findViewById(2131165897));
    this.mLeftIcon = ((ImageView)findViewById(2131165896));
    this.mRightIcon = ((ImageView)findViewById(2131165899));
    this.mCircularProgress = ((ProgressBar)findViewById(2131165898));
    if (this.mCircularProgress != null)
      this.mCircularProgress.setIndeterminate(true);
    this.mHorizontalProgress = ((ProgressBar)findViewById(2131165900));
    if (this.mIsLegacy)
    {
      setTextStyle(this.mTitle, this.mTitleResource);
      disableSubtitle();
      return;
    }
    setTextStyle(this.mTitle, this.mTitleResource);
    setTextStyle(this.mSubtitle, this.mSubtitleResource);
    disableLeftIcon();
    disableRightIcon();
  }

  public void setAnimationsEnabled(boolean paramBoolean)
  {
    this.mAnimationsEnabled = paramBoolean;
  }

  public void setCircularProgress(int paramInt)
  {
    if (this.mCircularProgress == null)
      return;
    switch (paramInt)
    {
    default:
      return;
    case -2:
      this.mCircularProgress.setVisibility(8);
      return;
    case -1:
    }
    this.mCircularProgress.setVisibility(0);
  }

  public void setHorizontalProgress(int paramInt)
  {
    if (this.mHorizontalProgress == null);
    do
    {
      return;
      switch (paramInt)
      {
      default:
        if ((paramInt >= 0) && (paramInt <= 10000))
        {
          this.mHorizontalProgress.setProgress(paramInt + 0);
          return;
        }
        break;
      case -1:
        this.mHorizontalProgress.setVisibility(0);
        return;
      case -2:
        this.mHorizontalProgress.setVisibility(8);
        return;
      case -3:
        this.mHorizontalProgress.setIndeterminate(true);
        return;
      case -4:
        this.mHorizontalProgress.setIndeterminate(false);
        return;
      }
    }
    while ((20000 > paramInt) || (paramInt > 30000));
    this.mHorizontalProgress.setSecondaryProgress(paramInt - 20000);
  }

  public void setLeftIcon(Drawable paramDrawable, int paramInt)
  {
    setIcon(this.mLeftIcon, paramDrawable, paramInt);
  }

  public void setProgressVisible(boolean paramBoolean)
  {
    if (paramBoolean);
    for (int i = -1; ; i = -2)
    {
      setCircularProgress(i);
      return;
    }
  }

  public void setRightIcon(Drawable paramDrawable, int paramInt)
  {
    setIcon(this.mRightIcon, paramDrawable, paramInt);
  }

  public void setSubtitle(CharSequence paramCharSequence)
  {
    this.mSubtitle.setText(paramCharSequence);
    TextView localTextView = this.mSubtitle;
    if (TextUtils.isEmpty(paramCharSequence));
    for (int i = 8; ; i = 0)
    {
      localTextView.setVisibility(i);
      return;
    }
  }

  public void setTitle(CharSequence paramCharSequence)
  {
    this.mTitle.setText(paramCharSequence);
  }

  public void setTitleColor(int paramInt)
  {
    this.mTitle.setTextColor(paramInt);
  }

  public void setVisible(boolean paramBoolean1, boolean paramBoolean2)
  {
    VisibilityController localVisibilityController = this.mVisibilityController;
    if ((paramBoolean2) && (this.mAnimationsEnabled));
    for (boolean bool = true; ; bool = false)
    {
      localVisibilityController.setVisible(paramBoolean1, bool);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.TitleBarView
 * JD-Core Version:    0.6.2
 */