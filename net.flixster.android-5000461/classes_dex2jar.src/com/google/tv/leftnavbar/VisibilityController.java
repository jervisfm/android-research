package com.google.tv.leftnavbar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewPropertyAnimator;

@TargetApi(12)
public final class VisibilityController
{
  private final int mAnimationDuration;
  private final View mView;
  private boolean mVisible;

  VisibilityController(View paramView)
  {
    this.mView = paramView;
    this.mAnimationDuration = paramView.getContext().getResources().getInteger(17694720);
    if (paramView.getVisibility() == 0);
    for (boolean bool = true; ; bool = false)
    {
      this.mVisible = bool;
      return;
    }
  }

  private void setViewVisible(boolean paramBoolean)
  {
    View localView = this.mView;
    if (paramBoolean);
    for (int i = 0; ; i = 8)
    {
      localView.setVisibility(i);
      return;
    }
  }

  boolean isVisible()
  {
    return this.mVisible;
  }

  boolean setVisible(final boolean paramBoolean1, boolean paramBoolean2)
  {
    if (isVisible() == paramBoolean1)
      return false;
    this.mVisible = paramBoolean1;
    float f;
    if (paramBoolean2)
      if (paramBoolean1)
      {
        f = 1.0F;
        this.mView.animate().alpha(f).setDuration(this.mAnimationDuration).setListener(new AnimatorListenerAdapter()
        {
          public void onAnimationEnd(Animator paramAnonymousAnimator)
          {
            if (!paramBoolean1)
              VisibilityController.this.setViewVisible(false);
          }

          public void onAnimationStart(Animator paramAnonymousAnimator)
          {
            if (paramBoolean1)
              VisibilityController.this.setViewVisible(true);
          }
        });
      }
    while (true)
    {
      return true;
      f = 0.0F;
      break;
      setViewVisible(paramBoolean1);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.tv.leftnavbar.VisibilityController
 * JD-Core Version:    0.6.2
 */