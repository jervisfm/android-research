package com.jayway.android.robotium.solo;

import android.app.Activity;
import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.util.Log;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import junit.framework.Assert;

class ActivityUtils
{
  private final String LOG_TAG = "Robotium";
  private final int MINISLEEP = 100;
  private Activity activity;
  private LinkedHashSet<Activity> activityList;
  private Instrumentation.ActivityMonitor activityMonitor;
  private final Instrumentation inst;
  private final Sleeper sleeper;

  public ActivityUtils(Instrumentation paramInstrumentation, Activity paramActivity, Sleeper paramSleeper)
  {
    this.inst = paramInstrumentation;
    this.activity = paramActivity;
    this.sleeper = paramSleeper;
    this.activityList = new LinkedHashSet();
    setupActivityMonitor();
  }

  private void finishActivity(Activity paramActivity)
  {
    try
    {
      paramActivity.finish();
      return;
    }
    catch (Throwable localThrowable)
    {
      localThrowable.printStackTrace();
    }
  }

  private void setupActivityMonitor()
  {
    try
    {
      this.activityMonitor = this.inst.addMonitor(null, null, false);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  private final void waitForActivityIfNotAvailable()
  {
    if (this.activity == null)
    {
      if (this.activityMonitor != null)
        while (this.activityMonitor.getLastActivity() == null)
          this.sleeper.sleepMini();
      this.sleeper.sleepMini();
      setupActivityMonitor();
      waitForActivityIfNotAvailable();
    }
  }

  public void finalize()
    throws Throwable
  {
    try
    {
      if (this.activityMonitor != null)
        this.inst.removeMonitor(this.activityMonitor);
      label18: super.finalize();
      return;
    }
    catch (Exception localException)
    {
      break label18;
    }
  }

  public void finishOpenedActivities()
  {
    ArrayList localArrayList = getAllOpenedActivities();
    for (int i = -1 + localArrayList.size(); i >= 0; i--)
    {
      this.sleeper.sleep(100);
      finishActivity((Activity)localArrayList.get(i));
    }
    finishActivity(getCurrentActivity());
    this.sleeper.sleepMini();
    try
    {
      this.inst.sendKeyDownUpSync(4);
      this.sleeper.sleep(100);
      this.inst.sendKeyDownUpSync(4);
      label83: this.activityList.clear();
      return;
    }
    catch (Throwable localThrowable)
    {
      break label83;
    }
  }

  public Instrumentation.ActivityMonitor getActivityMonitor()
  {
    return this.activityMonitor;
  }

  public ArrayList<Activity> getAllOpenedActivities()
  {
    return new ArrayList(this.activityList);
  }

  public Activity getCurrentActivity()
  {
    return getCurrentActivity(true);
  }

  public Activity getCurrentActivity(boolean paramBoolean)
  {
    if (paramBoolean)
      this.sleeper.sleep();
    waitForActivityIfNotAvailable();
    if ((this.activityMonitor != null) && (this.activityMonitor.getLastActivity() != null))
      this.activity = this.activityMonitor.getLastActivity();
    this.activityList.add(this.activity);
    return this.activity;
  }

  public String getString(int paramInt)
  {
    return getCurrentActivity(false).getString(paramInt);
  }

  public void goBackToActivity(String paramString)
  {
    ArrayList localArrayList = getAllOpenedActivities();
    for (int i = 0; ; i++)
    {
      int j = localArrayList.size();
      int k = 0;
      if (i < j)
      {
        if (((Activity)localArrayList.get(i)).getClass().getSimpleName().equals(paramString))
          k = 1;
      }
      else
      {
        if (k == 0)
          break;
        while (!getCurrentActivity().getClass().getSimpleName().equals(paramString))
          try
          {
            this.inst.sendKeyDownUpSync(4);
          }
          catch (SecurityException localSecurityException)
          {
          }
      }
    }
    for (int m = 0; m < localArrayList.size(); m++)
      Log.d("Robotium", "Activity priorly opened: " + ((Activity)localArrayList.get(m)).getClass().getSimpleName());
    Assert.assertTrue("No Activity named " + paramString + " has been priorly opened", false);
  }

  public void setActivityOrientation(int paramInt)
  {
    getCurrentActivity().setRequestedOrientation(paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.ActivityUtils
 * JD-Core Version:    0.6.2
 */