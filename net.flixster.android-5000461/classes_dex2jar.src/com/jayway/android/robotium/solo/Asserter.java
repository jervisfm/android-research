package com.jayway.android.robotium.solo;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import java.util.ArrayList;
import junit.framework.Assert;

class Asserter
{
  private final ActivityUtils activityUtils;
  private final Waiter waiter;

  public Asserter(ActivityUtils paramActivityUtils, Waiter paramWaiter)
  {
    this.activityUtils = paramActivityUtils;
    this.waiter = paramWaiter;
  }

  public void assertCurrentActivity(String paramString, Class<? extends Activity> paramClass)
  {
    this.waiter.waitForActivity(paramClass.getSimpleName());
    Assert.assertEquals(paramString, paramClass.getName(), this.activityUtils.getCurrentActivity().getClass().getName());
  }

  public void assertCurrentActivity(String paramString, Class<? extends Activity> paramClass, boolean paramBoolean)
  {
    boolean bool = false;
    assertCurrentActivity(paramString, paramClass);
    Activity localActivity = this.activityUtils.getCurrentActivity(false);
    for (int i = 0; i < -1 + this.activityUtils.getAllOpenedActivities().size(); i++)
      if (((Activity)this.activityUtils.getAllOpenedActivities().get(i)).toString().equals(localActivity.toString()))
        bool = true;
    Assert.assertNotSame(paramString + ", isNewInstance: actual and ", Boolean.valueOf(paramBoolean), Boolean.valueOf(bool));
  }

  public void assertCurrentActivity(String paramString1, String paramString2)
  {
    this.waiter.waitForActivity(paramString2);
    Assert.assertEquals(paramString1, paramString2, this.activityUtils.getCurrentActivity().getClass().getSimpleName());
  }

  public void assertCurrentActivity(String paramString1, String paramString2, boolean paramBoolean)
  {
    assertCurrentActivity(paramString1, paramString2);
    assertCurrentActivity(paramString1, this.activityUtils.getCurrentActivity().getClass(), paramBoolean);
  }

  public void assertMemoryNotLow()
  {
    ActivityManager.MemoryInfo localMemoryInfo = new ActivityManager.MemoryInfo();
    ((ActivityManager)this.activityUtils.getCurrentActivity().getSystemService("activity")).getMemoryInfo(localMemoryInfo);
    Assert.assertFalse("Low memory available: " + localMemoryInfo.availMem + " bytes", localMemoryInfo.lowMemory);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Asserter
 * JD-Core Version:    0.6.2
 */