package com.jayway.android.robotium.solo;

import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;

class Checker
{
  private final ViewFetcher viewFetcher;
  private final Waiter waiter;

  public Checker(ViewFetcher paramViewFetcher, Waiter paramWaiter)
  {
    this.viewFetcher = paramViewFetcher;
    this.waiter = paramWaiter;
  }

  public <T extends CompoundButton> boolean isButtonChecked(Class<T> paramClass, int paramInt)
  {
    return ((CompoundButton)this.waiter.waitForAndGetView(paramInt, paramClass)).isChecked();
  }

  public <T extends CompoundButton> boolean isButtonChecked(Class<T> paramClass, String paramString)
  {
    this.waiter.waitForText(paramString, 0, 10000L);
    Iterator localIterator = this.viewFetcher.getCurrentViews(paramClass).iterator();
    CompoundButton localCompoundButton;
    do
    {
      boolean bool1 = localIterator.hasNext();
      bool2 = false;
      if (!bool1)
        break;
      localCompoundButton = (CompoundButton)localIterator.next();
    }
    while ((!localCompoundButton.getText().equals(paramString)) || (!localCompoundButton.isChecked()));
    boolean bool2 = true;
    return bool2;
  }

  public boolean isCheckedTextChecked(String paramString)
  {
    this.waiter.waitForText(paramString, 0, 10000L);
    Iterator localIterator = this.viewFetcher.getCurrentViews(CheckedTextView.class).iterator();
    CheckedTextView localCheckedTextView;
    do
    {
      boolean bool1 = localIterator.hasNext();
      bool2 = false;
      if (!bool1)
        break;
      localCheckedTextView = (CheckedTextView)localIterator.next();
    }
    while ((!localCheckedTextView.getText().equals(paramString)) || (!localCheckedTextView.isChecked()));
    boolean bool2 = true;
    return bool2;
  }

  public boolean isSpinnerTextSelected(int paramInt, String paramString)
  {
    boolean bool1 = ((TextView)((Spinner)this.waiter.waitForAndGetView(paramInt, Spinner.class)).getChildAt(0)).getText().equals(paramString);
    boolean bool2 = false;
    if (bool1)
      bool2 = true;
    return bool2;
  }

  public boolean isSpinnerTextSelected(String paramString)
  {
    this.waiter.waitForAndGetView(0, Spinner.class);
    ArrayList localArrayList = this.viewFetcher.getCurrentViews(Spinner.class);
    for (int i = 0; ; i++)
    {
      int j = localArrayList.size();
      boolean bool = false;
      if (i < j)
      {
        if (isSpinnerTextSelected(i, paramString))
          bool = true;
      }
      else
        return bool;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Checker
 * JD-Core Version:    0.6.2
 */