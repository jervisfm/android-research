package com.jayway.android.robotium.solo;

import android.app.Instrumentation;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import junit.framework.Assert;

class Clicker
{
  private final String LOG_TAG = "Robotium";
  private final int MINISLEEP = 100;
  private final int TIMEOUT = 10000;
  private final Instrumentation inst;
  private final RobotiumUtils robotiumUtils;
  private final Scroller scroller;
  private final Sleeper sleeper;
  Set<TextView> uniqueTextViews;
  private final ViewFetcher viewFetcher;
  private final Waiter waiter;

  public Clicker(ViewFetcher paramViewFetcher, Scroller paramScroller, RobotiumUtils paramRobotiumUtils, Instrumentation paramInstrumentation, Sleeper paramSleeper, Waiter paramWaiter)
  {
    this.viewFetcher = paramViewFetcher;
    this.scroller = paramScroller;
    this.robotiumUtils = paramRobotiumUtils;
    this.inst = paramInstrumentation;
    this.sleeper = paramSleeper;
    this.waiter = paramWaiter;
    this.uniqueTextViews = new HashSet();
  }

  public ArrayList<TextView> clickInList(int paramInt)
  {
    return clickInList(paramInt, 0, false, 0);
  }

  public ArrayList<TextView> clickInList(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3)
  {
    int i = paramInt1 - 1;
    if (i < 0)
      i = 0;
    ArrayList localArrayList = new ArrayList();
    ListView localListView = (ListView)this.waiter.waitForAndGetView(paramInt2, ListView.class);
    if (localListView == null)
      Assert.assertTrue("ListView is null!", false);
    View localView = localListView.getChildAt(i);
    if (localView != null)
    {
      localArrayList = RobotiumUtils.removeInvisibleViews(this.viewFetcher.getViews(localView, true));
      clickOnScreen(localView, paramBoolean, paramInt3);
    }
    return RobotiumUtils.filterViews(TextView.class, localArrayList);
  }

  public void clickLongOnScreen(float paramFloat1, float paramFloat2, int paramInt)
  {
    long l = SystemClock.uptimeMillis();
    MotionEvent localMotionEvent1 = MotionEvent.obtain(l, SystemClock.uptimeMillis(), 0, paramFloat1, paramFloat2, 0);
    try
    {
      this.inst.sendPointerSync(localMotionEvent1);
      MotionEvent localMotionEvent2 = MotionEvent.obtain(l, SystemClock.uptimeMillis(), 2, paramFloat1 + ViewConfiguration.getTouchSlop() / 2, paramFloat2 + ViewConfiguration.getTouchSlop() / 2, 0);
      this.inst.sendPointerSync(localMotionEvent2);
      if (paramInt > 0)
      {
        this.sleeper.sleep(paramInt);
        MotionEvent localMotionEvent3 = MotionEvent.obtain(l, SystemClock.uptimeMillis(), 1, paramFloat1, paramFloat2, 0);
        this.inst.sendPointerSync(localMotionEvent3);
        this.sleeper.sleep();
        return;
      }
    }
    catch (SecurityException localSecurityException)
    {
      while (true)
      {
        Assert.assertTrue("Click can not be completed! Something is in the way e.g. the keyboard.", false);
        continue;
        this.sleeper.sleep((int)(2.5F * ViewConfiguration.getLongPressTimeout()));
      }
    }
  }

  public void clickLongOnTextAndPress(String paramString, int paramInt)
  {
    clickOnText(paramString, true, 0, true, 0);
    try
    {
      this.inst.sendKeyDownUpSync(20);
      for (int i = 0; i < paramInt; i++)
      {
        this.sleeper.sleepMini();
        this.inst.sendKeyDownUpSync(20);
      }
    }
    catch (SecurityException localSecurityException)
    {
      while (true)
        Assert.assertTrue("Can not press the context menu!", false);
      this.inst.sendKeyDownUpSync(66);
    }
  }

  public <T extends View> void clickOn(Class<T> paramClass, int paramInt)
  {
    clickOnScreen(this.waiter.waitForAndGetView(paramInt, paramClass));
  }

  public <T extends TextView> void clickOn(Class<T> paramClass, String paramString)
  {
    Pattern localPattern = Pattern.compile(paramString);
    this.waiter.waitForText(paramString, 0, 10000L, true, true);
    ArrayList localArrayList = RobotiumUtils.removeInvisibleViews(this.viewFetcher.getCurrentViews(paramClass));
    Object localObject = null;
    Iterator localIterator1 = localArrayList.iterator();
    do
    {
      TextView localTextView2;
      do
      {
        if (!localIterator1.hasNext())
          break;
        localTextView2 = (TextView)localIterator1.next();
      }
      while (!localPattern.matcher(localTextView2.getText().toString()).matches());
      localObject = localTextView2;
    }
    while (!localObject.isShown());
    if (localObject != null)
    {
      clickOnScreen(localObject);
      return;
    }
    if (this.scroller.scroll(0))
    {
      clickOn(paramClass, paramString);
      return;
    }
    Iterator localIterator2 = localArrayList.iterator();
    while (localIterator2.hasNext())
    {
      TextView localTextView1 = (TextView)localIterator2.next();
      Log.d("Robotium", paramString + " not found. Have found: " + localTextView1.getText());
    }
    Assert.assertTrue(paramClass.getSimpleName() + " with the text: " + paramString + " is not found!", false);
  }

  public void clickOnMenuItem(String paramString)
  {
    this.sleeper.sleep();
    try
    {
      this.robotiumUtils.sendKeyCode(82);
      clickOnText(paramString, false, 1, true, 0);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      while (true)
        Assert.assertTrue("Can not open the menu!", false);
    }
  }

  public void clickOnMenuItem(String paramString, boolean paramBoolean)
  {
    this.sleeper.sleep();
    int[] arrayOfInt = new int[2];
    try
    {
      this.robotiumUtils.sendKeyCode(82);
      localObject = null;
      if (paramBoolean)
      {
        int i = this.viewFetcher.getCurrentViews(TextView.class).size();
        localObject = null;
        if (i > 5)
        {
          boolean bool = this.waiter.waitForText(paramString, 1, 1500L, false);
          localObject = null;
          if (!bool)
          {
            Iterator localIterator = this.viewFetcher.getCurrentViews(TextView.class).iterator();
            while (localIterator.hasNext())
            {
              TextView localTextView = (TextView)localIterator.next();
              int j = arrayOfInt[0];
              int k = arrayOfInt[1];
              localTextView.getLocationOnScreen(arrayOfInt);
              if ((arrayOfInt[0] > j) || (arrayOfInt[1] > k))
                localObject = localTextView;
            }
          }
        }
      }
    }
    catch (SecurityException localSecurityException)
    {
      Object localObject;
      while (true)
        Assert.assertTrue("Can not open the menu!", false);
      if (localObject != null)
        clickOnScreen(localObject);
      clickOnText(paramString, false, 1, true, 0);
    }
  }

  public void clickOnScreen(float paramFloat1, float paramFloat2)
  {
    long l1 = SystemClock.uptimeMillis();
    long l2 = SystemClock.uptimeMillis();
    MotionEvent localMotionEvent1 = MotionEvent.obtain(l1, l2, 0, paramFloat1, paramFloat2, 0);
    MotionEvent localMotionEvent2 = MotionEvent.obtain(l1, l2, 1, paramFloat1, paramFloat2, 0);
    try
    {
      this.inst.sendPointerSync(localMotionEvent1);
      this.inst.sendPointerSync(localMotionEvent2);
      this.sleeper.sleep(100);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      Assert.assertTrue("Click can not be completed!", false);
    }
  }

  public void clickOnScreen(View paramView)
  {
    clickOnScreen(paramView, false, 0);
  }

  public void clickOnScreen(View paramView, boolean paramBoolean, int paramInt)
  {
    if (paramView == null)
      Assert.assertTrue("View is null and can therefore not be clicked!", false);
    int[] arrayOfInt = new int[2];
    paramView.getLocationOnScreen(arrayOfInt);
    int i = paramView.getWidth();
    int j = paramView.getHeight();
    float f1 = arrayOfInt[0] + i / 2.0F;
    float f2 = arrayOfInt[1] + j / 2.0F;
    if (paramBoolean)
    {
      clickLongOnScreen(f1, f2, paramInt);
      return;
    }
    clickOnScreen(f1, f2);
  }

  public void clickOnText(String paramString, boolean paramBoolean1, int paramInt1, boolean paramBoolean2, int paramInt2)
  {
    this.waiter.waitForText(paramString, 0, 10000L, paramBoolean2, true);
    ArrayList localArrayList = RobotiumUtils.removeInvisibleViews(this.viewFetcher.getCurrentViews(TextView.class));
    if (paramInt1 == 0)
      paramInt1 = 1;
    Iterator localIterator1 = localArrayList.iterator();
    TextView localTextView2;
    do
    {
      boolean bool = localIterator1.hasNext();
      localObject = null;
      if (!bool)
        break;
      localTextView2 = (TextView)localIterator1.next();
    }
    while (RobotiumUtils.checkAndGetMatches(paramString, localTextView2, this.uniqueTextViews) != paramInt1);
    this.uniqueTextViews.clear();
    Object localObject = localTextView2;
    if (localObject != null)
    {
      clickOnScreen(localObject, paramBoolean1, paramInt2);
      return;
    }
    if ((paramBoolean2) && (this.scroller.scroll(0)))
    {
      clickOnText(paramString, paramBoolean1, paramInt1, paramBoolean2, paramInt2);
      return;
    }
    int i = this.uniqueTextViews.size();
    this.uniqueTextViews.clear();
    if (i > 0)
    {
      Assert.assertTrue("There are only " + i + " matches of " + paramString, false);
      return;
    }
    Iterator localIterator2 = localArrayList.iterator();
    while (localIterator2.hasNext())
    {
      TextView localTextView1 = (TextView)localIterator2.next();
      Log.d("Robotium", paramString + " not found. Have found: " + localTextView1.getText());
    }
    Assert.assertTrue("The text: " + paramString + " is not found!", false);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Clicker
 * JD-Core Version:    0.6.2
 */