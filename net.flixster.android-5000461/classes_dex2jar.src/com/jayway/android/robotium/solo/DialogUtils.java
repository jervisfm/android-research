package com.jayway.android.robotium.solo;

class DialogUtils
{
  private final Sleeper sleeper;
  private final ViewFetcher viewFetcher;

  public DialogUtils(ViewFetcher paramViewFetcher, Sleeper paramSleeper)
  {
    this.viewFetcher = paramViewFetcher;
    this.sleeper = paramSleeper;
  }

  public boolean waitForDialogToClose(long paramLong)
  {
    this.sleeper.sleepMini();
    int i = this.viewFetcher.getWindowDecorViews().length;
    long l1 = System.currentTimeMillis();
    long l2 = l1 + paramLong;
    while (true)
    {
      if (l1 < l2)
      {
        int j = this.viewFetcher.getWindowDecorViews().length;
        if (i < j)
          i = j;
        if (i <= j);
      }
      else
      {
        if (l1 <= l2)
          break;
        return false;
      }
      this.sleeper.sleepMini();
      l1 = System.currentTimeMillis();
    }
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.DialogUtils
 * JD-Core Version:    0.6.2
 */