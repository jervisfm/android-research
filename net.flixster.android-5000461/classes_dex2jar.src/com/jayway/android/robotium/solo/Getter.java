package com.jayway.android.robotium.solo;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import junit.framework.Assert;

class Getter
{
  private final ActivityUtils activityUtils;
  private final ViewFetcher viewFetcher;
  private final Waiter waiter;

  public Getter(ActivityUtils paramActivityUtils, ViewFetcher paramViewFetcher, Waiter paramWaiter)
  {
    this.activityUtils = paramActivityUtils;
    this.viewFetcher = paramViewFetcher;
    this.waiter = paramWaiter;
  }

  public View getView(int paramInt)
  {
    View localView = this.activityUtils.getCurrentActivity(false).findViewById(paramInt);
    if (localView != null)
      return localView;
    return this.waiter.waitForView(paramInt);
  }

  public <T extends View> T getView(Class<T> paramClass, int paramInt)
  {
    return this.waiter.waitForAndGetView(paramInt, paramClass);
  }

  public <T extends TextView> T getView(Class<T> paramClass, String paramString, boolean paramBoolean)
  {
    this.waiter.waitForText(paramString, 0, 10000L, false, paramBoolean);
    ArrayList localArrayList = this.viewFetcher.getCurrentViews(paramClass);
    if (paramBoolean)
      localArrayList = RobotiumUtils.removeInvisibleViews(localArrayList);
    Object localObject = null;
    Iterator localIterator = localArrayList.iterator();
    while (localIterator.hasNext())
    {
      TextView localTextView = (TextView)localIterator.next();
      if (localTextView.getText().toString().equals(paramString))
        localObject = localTextView;
    }
    if (localObject == null)
      Assert.assertTrue("No " + paramClass.getSimpleName() + " with text " + paramString + " is found!", false);
    return localObject;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Getter
 * JD-Core Version:    0.6.2
 */