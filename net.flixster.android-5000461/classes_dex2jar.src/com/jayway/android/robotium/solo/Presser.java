package com.jayway.android.robotium.solo;

import android.app.Instrumentation;
import android.widget.Spinner;
import junit.framework.Assert;

class Presser
{
  private final Clicker clicker;
  private final Instrumentation inst;
  private final Sleeper sleeper;
  private final Waiter waiter;

  public Presser(Clicker paramClicker, Instrumentation paramInstrumentation, Sleeper paramSleeper, Waiter paramWaiter)
  {
    this.clicker = paramClicker;
    this.inst = paramInstrumentation;
    this.sleeper = paramSleeper;
    this.waiter = paramWaiter;
  }

  public void pressMenuItem(int paramInt)
  {
    pressMenuItem(paramInt, 3);
  }

  public void pressMenuItem(int paramInt1, int paramInt2)
  {
    int[] arrayOfInt = new int[4];
    for (int i = 1; i <= 3; i++)
      arrayOfInt[i] = (paramInt2 * i);
    this.sleeper.sleep();
    try
    {
      this.inst.sendKeyDownUpSync(82);
      this.sleeper.sleepMini();
      this.inst.sendKeyDownUpSync(19);
      this.inst.sendKeyDownUpSync(19);
      if (paramInt1 < arrayOfInt[1])
        for (int m = 0; m < paramInt1; m++)
        {
          this.sleeper.sleepMini();
          this.inst.sendKeyDownUpSync(22);
        }
    }
    catch (SecurityException localSecurityException1)
    {
      while (true)
        Assert.assertTrue("Can not press the menu!", false);
      int k;
      if ((paramInt1 >= arrayOfInt[1]) && (paramInt1 < arrayOfInt[2]))
      {
        this.inst.sendKeyDownUpSync(20);
        k = arrayOfInt[1];
      }
      while (k < paramInt1)
      {
        this.sleeper.sleepMini();
        this.inst.sendKeyDownUpSync(22);
        k++;
        continue;
        if (paramInt1 >= arrayOfInt[2])
        {
          this.inst.sendKeyDownUpSync(20);
          this.inst.sendKeyDownUpSync(20);
          for (int j = arrayOfInt[2]; j < paramInt1; j++)
          {
            this.sleeper.sleepMini();
            this.inst.sendKeyDownUpSync(22);
          }
        }
      }
      try
      {
        this.inst.sendKeyDownUpSync(66);
        return;
      }
      catch (SecurityException localSecurityException2)
      {
      }
    }
  }

  public void pressSpinnerItem(int paramInt1, int paramInt2)
  {
    this.clicker.clickOnScreen(this.waiter.waitForAndGetView(paramInt1, Spinner.class));
    this.sleeper.sleep();
    try
    {
      this.inst.sendKeyDownUpSync(20);
      label33: int i = 1;
      if (paramInt2 < 0)
      {
        i = 0;
        paramInt2 *= -1;
      }
      int j = 0;
      while (true)
      {
        if (j < paramInt2)
        {
          this.sleeper.sleepMini();
          if (i == 0);
        }
        try
        {
          this.inst.sendKeyDownUpSync(20);
          while (true)
          {
            label77: j++;
            break;
            try
            {
              this.inst.sendKeyDownUpSync(19);
            }
            catch (SecurityException localSecurityException3)
            {
            }
          }
          try
          {
            this.inst.sendKeyDownUpSync(66);
            return;
          }
          catch (SecurityException localSecurityException2)
          {
          }
        }
        catch (SecurityException localSecurityException4)
        {
          break label77;
        }
      }
    }
    catch (SecurityException localSecurityException1)
    {
      break label33;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Presser
 * JD-Core Version:    0.6.2
 */