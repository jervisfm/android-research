package com.jayway.android.robotium.solo;

import android.app.Instrumentation;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import junit.framework.Assert;

class RobotiumUtils
{
  private final Instrumentation inst;
  private final Sleeper sleeper;

  public RobotiumUtils(Instrumentation paramInstrumentation, Sleeper paramSleeper)
  {
    this.inst = paramInstrumentation;
    this.sleeper = paramSleeper;
  }

  public static int checkAndGetMatches(String paramString, TextView paramTextView, Set<TextView> paramSet)
  {
    Pattern localPattern = Pattern.compile(paramString);
    if (localPattern.matcher(paramTextView.getText().toString()).find())
      paramSet.add(paramTextView);
    if ((paramTextView.getError() != null) && (localPattern.matcher(paramTextView.getError().toString()).find()))
      paramSet.add(paramTextView);
    if ((paramTextView.getText().toString().equals("")) && (paramTextView.getHint() != null) && (localPattern.matcher(paramTextView.getHint().toString()).find()))
      paramSet.add(paramTextView);
    return paramSet.size();
  }

  public static <T extends View> ArrayList<T> filterViews(Class<T> paramClass, ArrayList<View> paramArrayList)
  {
    ArrayList localArrayList = new ArrayList(paramArrayList.size());
    Iterator localIterator = paramArrayList.iterator();
    while (localIterator.hasNext())
    {
      View localView = (View)localIterator.next();
      if ((localView != null) && (paramClass.isAssignableFrom(localView.getClass())))
        localArrayList.add(paramClass.cast(localView));
    }
    return localArrayList;
  }

  public static <T extends View> ArrayList<T> removeInvisibleViews(ArrayList<T> paramArrayList)
  {
    ArrayList localArrayList = new ArrayList(paramArrayList.size());
    Iterator localIterator = paramArrayList.iterator();
    while (localIterator.hasNext())
    {
      View localView = (View)localIterator.next();
      if ((localView != null) && (localView.isShown()))
        localArrayList.add(localView);
    }
    return localArrayList;
  }

  public void goBack()
  {
    this.sleeper.sleep();
    try
    {
      this.inst.sendKeyDownUpSync(4);
      this.sleeper.sleep();
      return;
    }
    catch (Throwable localThrowable)
    {
    }
  }

  public void sendKeyCode(int paramInt)
  {
    this.sleeper.sleep();
    try
    {
      this.inst.sendCharacterSync(paramInt);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      Assert.assertTrue("Can not complete action!", false);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.RobotiumUtils
 * JD-Core Version:    0.6.2
 */