package com.jayway.android.robotium.solo;

import android.app.Activity;
import android.app.Instrumentation;
import android.os.SystemClock;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ScrollView;
import java.util.ArrayList;

class Scroller
{
  public static final int DOWN = 0;
  public static final int UP = 1;
  private final ActivityUtils activityUtils;
  private final Instrumentation inst;
  private final Sleeper sleeper;
  private final ViewFetcher viewFetcher;

  public Scroller(Instrumentation paramInstrumentation, ActivityUtils paramActivityUtils, ViewFetcher paramViewFetcher, Sleeper paramSleeper)
  {
    this.inst = paramInstrumentation;
    this.activityUtils = paramActivityUtils;
    this.viewFetcher = paramViewFetcher;
    this.sleeper = paramSleeper;
  }

  private <T extends AbsListView> void scrollListToLine(final T paramT, int paramInt)
  {
    if ((paramT instanceof GridView));
    for (final int i = paramInt + 1; ; i = paramInt)
    {
      this.inst.runOnMainSync(new Runnable()
      {
        public void run()
        {
          paramT.setSelection(i);
        }
      });
      return;
    }
  }

  private boolean scrollScrollView(int paramInt, ArrayList<ScrollView> paramArrayList)
  {
    ScrollView localScrollView = (ScrollView)this.viewFetcher.getView(ScrollView.class, paramArrayList);
    int i;
    int j;
    if (localScrollView != null)
    {
      i = -1 + localScrollView.getHeight();
      if (paramInt != 0)
        break label59;
      j = i;
    }
    while (true)
    {
      int k = localScrollView.getScrollY();
      scrollScrollViewTo(localScrollView, 0, j);
      if (k != localScrollView.getScrollY())
        break;
      return false;
      label59: j = 0;
      if (paramInt == 1)
        j = -i;
    }
    return true;
  }

  private void scrollScrollViewTo(final ScrollView paramScrollView, final int paramInt1, final int paramInt2)
  {
    this.inst.runOnMainSync(new Runnable()
    {
      public void run()
      {
        paramScrollView.scrollBy(paramInt1, paramInt2);
      }
    });
  }

  public void drag(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt)
  {
    long l1 = SystemClock.uptimeMillis();
    long l2 = SystemClock.uptimeMillis();
    float f1 = (paramFloat4 - paramFloat3) / paramInt;
    float f2 = (paramFloat2 - paramFloat1) / paramInt;
    MotionEvent localMotionEvent1 = MotionEvent.obtain(l1, l2, 0, paramFloat1, paramFloat3, 0);
    try
    {
      this.inst.sendPointerSync(localMotionEvent1);
      label51: int i = 0;
      float f3 = paramFloat1;
      float f4 = paramFloat3;
      while (true)
      {
        MotionEvent localMotionEvent3;
        if (i < paramInt)
        {
          f4 += f1;
          f3 += f2;
          localMotionEvent3 = MotionEvent.obtain(l1, SystemClock.uptimeMillis(), 2, f3, f4, 0);
        }
        try
        {
          this.inst.sendPointerSync(localMotionEvent3);
          label106: i++;
          continue;
          MotionEvent localMotionEvent2 = MotionEvent.obtain(l1, SystemClock.uptimeMillis(), 1, paramFloat2, paramFloat4, 0);
          try
          {
            this.inst.sendPointerSync(localMotionEvent2);
            return;
          }
          catch (SecurityException localSecurityException2)
          {
          }
        }
        catch (SecurityException localSecurityException3)
        {
          break label106;
        }
      }
    }
    catch (SecurityException localSecurityException1)
    {
      break label51;
    }
  }

  public boolean scroll(int paramInt)
  {
    ArrayList localArrayList1 = RobotiumUtils.removeInvisibleViews(this.viewFetcher.getViews(null, true));
    ArrayList localArrayList2 = RobotiumUtils.filterViews(ListView.class, localArrayList1);
    if (localArrayList2.size() > 0)
      return scrollList(ListView.class, null, paramInt, localArrayList2);
    ArrayList localArrayList3 = RobotiumUtils.filterViews(GridView.class, localArrayList1);
    if (localArrayList3.size() > 0)
      return scrollList(GridView.class, null, paramInt, localArrayList3);
    ArrayList localArrayList4 = RobotiumUtils.filterViews(ScrollView.class, localArrayList1);
    if (localArrayList4.size() > 0)
      return scrollScrollView(paramInt, localArrayList4);
    return false;
  }

  public <T extends AbsListView> boolean scrollList(Class<T> paramClass, T paramT, int paramInt, ArrayList<T> paramArrayList)
  {
    if (paramT == null)
      paramT = (AbsListView)this.viewFetcher.getView(paramClass, paramArrayList);
    if (paramT == null)
      return false;
    if (paramInt == 0)
    {
      if (paramT.getLastVisiblePosition() >= -1 + paramT.getCount())
      {
        scrollListToLine(paramT, paramT.getLastVisiblePosition());
        return false;
      }
      if (paramT.getFirstVisiblePosition() != paramT.getLastVisiblePosition())
        scrollListToLine(paramT, paramT.getLastVisiblePosition());
    }
    while (true)
    {
      this.sleeper.sleep();
      return true;
      scrollListToLine(paramT, 1 + paramT.getFirstVisiblePosition());
      continue;
      if (paramInt == 1)
      {
        if (paramT.getFirstVisiblePosition() < 2)
        {
          scrollListToLine(paramT, 0);
          return false;
        }
        int i = paramT.getLastVisiblePosition() - paramT.getFirstVisiblePosition();
        int j = paramT.getFirstVisiblePosition() - i;
        if (j == paramT.getLastVisiblePosition())
          j--;
        if (j < 0)
          j = 0;
        scrollListToLine(paramT, j);
      }
    }
  }

  public void scrollToSide(Side paramSide)
  {
    int i = this.activityUtils.getCurrentActivity().getWindowManager().getDefaultDisplay().getHeight();
    float f1 = this.activityUtils.getCurrentActivity(false).getWindowManager().getDefaultDisplay().getWidth() / 2.0F;
    float f2 = i / 2.0F;
    if (paramSide == Side.LEFT)
      drag(0.0F, f1, f2, f2, 40);
    while (paramSide != Side.RIGHT)
      return;
    drag(f1, 0.0F, f2, f2, 40);
  }

  public static enum Direction
  {
    static
    {
      DOWN = new Direction("DOWN", 1);
      Direction[] arrayOfDirection = new Direction[2];
      arrayOfDirection[0] = UP;
      arrayOfDirection[1] = DOWN;
    }
  }

  public static enum Side
  {
    static
    {
      Side[] arrayOfSide = new Side[2];
      arrayOfSide[0] = LEFT;
      arrayOfSide[1] = RIGHT;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Scroller
 * JD-Core Version:    0.6.2
 */