package com.jayway.android.robotium.solo;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Callable;

class Searcher
{
  private final String LOG_TAG = "Robotium";
  private final int TIMEOUT = 5000;
  private int numberOfUniqueViews;
  private final Scroller scroller;
  private final Sleeper sleeper;
  Set<TextView> uniqueTextViews;
  private final ViewFetcher viewFetcher;

  public Searcher(ViewFetcher paramViewFetcher, Scroller paramScroller, Sleeper paramSleeper)
  {
    this.viewFetcher = paramViewFetcher;
    this.scroller = paramScroller;
    this.sleeper = paramSleeper;
    this.uniqueTextViews = new HashSet();
  }

  private boolean logMatchesFoundAndReturnFalse(String paramString)
  {
    if (this.uniqueTextViews.size() > 0)
      Log.d("Robotium", " There are only " + this.uniqueTextViews.size() + " matches of " + paramString);
    this.uniqueTextViews.clear();
    return false;
  }

  private <T extends View> boolean setArrayToNullAndReturn(boolean paramBoolean, ArrayList<T> paramArrayList)
  {
    return paramBoolean;
  }

  public int getNumberOfUniqueViews()
  {
    return this.numberOfUniqueViews;
  }

  public <T extends View> int getNumberOfUniqueViews(Set<T> paramSet, ArrayList<T> paramArrayList)
  {
    for (int i = 0; i < paramArrayList.size(); i++)
      paramSet.add(paramArrayList.get(i));
    this.numberOfUniqueViews = paramSet.size();
    return this.numberOfUniqueViews;
  }

  public <T extends View> boolean searchFor(View paramView)
  {
    Iterator localIterator = this.viewFetcher.getAllViews(true).iterator();
    while (localIterator.hasNext())
      if (((View)localIterator.next()).equals(paramView))
        return true;
    return false;
  }

  public <T extends TextView> boolean searchFor(final Class<T> paramClass, String paramString, int paramInt, boolean paramBoolean1, final boolean paramBoolean2)
  {
    Callable local1 = new Callable()
    {
      public Collection<T> call()
        throws Exception
      {
        Searcher.this.sleeper.sleep();
        if (paramBoolean2)
          return RobotiumUtils.removeInvisibleViews(Searcher.this.viewFetcher.getCurrentViews(paramClass));
        return Searcher.this.viewFetcher.getCurrentViews(paramClass);
      }
    };
    try
    {
      boolean bool = searchFor(local1, paramString, paramInt, paramBoolean1);
      return bool;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  public <T extends View> boolean searchFor(Set<T> paramSet, Class<T> paramClass, int paramInt)
  {
    ArrayList localArrayList = RobotiumUtils.removeInvisibleViews(this.viewFetcher.getCurrentViews(paramClass));
    int i = getNumberOfUniqueViews(paramSet, localArrayList);
    if ((i > 0) && (paramInt < i))
      return setArrayToNullAndReturn(true, localArrayList);
    if ((i > 0) && (paramInt == 0))
      return setArrayToNullAndReturn(true, localArrayList);
    return setArrayToNullAndReturn(false, localArrayList);
  }

  public <T extends TextView> boolean searchFor(Callable<Collection<T>> paramCallable, String paramString, int paramInt, boolean paramBoolean)
    throws Exception
  {
    if (paramInt < 1)
      paramInt = 1;
    while (true)
    {
      Iterator localIterator = ((Collection)paramCallable.call()).iterator();
      while (localIterator.hasNext())
        if (RobotiumUtils.checkAndGetMatches(paramString, (TextView)localIterator.next(), this.uniqueTextViews) == paramInt)
        {
          this.uniqueTextViews.clear();
          return true;
        }
      if ((paramBoolean) && (!this.scroller.scroll(0)))
        return logMatchesFoundAndReturnFalse(paramString);
      if (!paramBoolean)
        return logMatchesFoundAndReturnFalse(paramString);
      this.sleeper.sleep();
    }
  }

  public boolean searchWithTimeoutFor(Class<? extends TextView> paramClass, String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    long l = 5000L + System.currentTimeMillis();
    while (System.currentTimeMillis() < l)
    {
      this.sleeper.sleep();
      if (searchFor(paramClass, paramString, paramInt, paramBoolean1, paramBoolean2))
        return true;
    }
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Searcher
 * JD-Core Version:    0.6.2
 */