package com.jayway.android.robotium.solo;

import android.app.Activity;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.SlidingDrawer;
import android.widget.TimePicker;

class Setter
{
  private final int CLOSED = 0;
  private final int OPENED = 1;
  private final ActivityUtils activityUtils;

  public Setter(ActivityUtils paramActivityUtils)
  {
    this.activityUtils = paramActivityUtils;
  }

  public void setDatePicker(final DatePicker paramDatePicker, final int paramInt1, final int paramInt2, final int paramInt3)
  {
    if (paramDatePicker != null)
      this.activityUtils.getCurrentActivity(false).runOnUiThread(new Runnable()
      {
        public void run()
        {
          try
          {
            paramDatePicker.updateDate(paramInt1, paramInt2, paramInt3);
            return;
          }
          catch (Exception localException)
          {
          }
        }
      });
  }

  public void setProgressBar(final ProgressBar paramProgressBar, final int paramInt)
  {
    if (paramProgressBar != null)
      this.activityUtils.getCurrentActivity(false).runOnUiThread(new Runnable()
      {
        public void run()
        {
          try
          {
            paramProgressBar.setProgress(paramInt);
            return;
          }
          catch (Exception localException)
          {
          }
        }
      });
  }

  public void setSlidingDrawer(final SlidingDrawer paramSlidingDrawer, final int paramInt)
  {
    if (paramSlidingDrawer != null)
      this.activityUtils.getCurrentActivity(false).runOnUiThread(new Runnable()
      {
        public void run()
        {
          try
          {
            switch (paramInt)
            {
            case 0:
              paramSlidingDrawer.close();
              return;
            case 1:
              paramSlidingDrawer.open();
            }
            return;
          }
          catch (Exception localException)
          {
          }
        }
      });
  }

  public void setTimePicker(final TimePicker paramTimePicker, final int paramInt1, final int paramInt2)
  {
    if (paramTimePicker != null)
      this.activityUtils.getCurrentActivity(false).runOnUiThread(new Runnable()
      {
        public void run()
        {
          try
          {
            paramTimePicker.setCurrentHour(Integer.valueOf(paramInt1));
            paramTimePicker.setCurrentMinute(Integer.valueOf(paramInt2));
            return;
          }
          catch (Exception localException)
          {
          }
        }
      });
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Setter
 * JD-Core Version:    0.6.2
 */