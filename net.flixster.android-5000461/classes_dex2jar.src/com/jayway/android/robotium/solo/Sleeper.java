package com.jayway.android.robotium.solo;

class Sleeper
{
  private final int MINIPAUSE = 300;
  private final int PAUSE = 500;

  public void sleep()
  {
    sleep(500);
  }

  public void sleep(int paramInt)
  {
    long l = paramInt;
    try
    {
      Thread.sleep(l);
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
    }
  }

  public void sleepMini()
  {
    sleep(300);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Sleeper
 * JD-Core Version:    0.6.2
 */