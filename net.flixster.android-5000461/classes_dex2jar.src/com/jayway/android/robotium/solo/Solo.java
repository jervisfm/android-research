package com.jayway.android.robotium.solo;

import android.app.Activity;
import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.SlidingDrawer;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;
import java.util.ArrayList;

public class Solo
{
  public static final int CLOSED = 0;
  public static final int DELETE = 67;
  public static final int DOWN = 20;
  public static final int ENTER = 66;
  public static final int LANDSCAPE = 0;
  public static final int LEFT = 21;
  public static final int MENU = 82;
  public static final int OPENED = 1;
  public static final int PORTRAIT = 1;
  public static final int RIGHT = 22;
  public static final int UP = 19;
  private final int SMALLTIMEOUT = 10000;
  private final int TIMEOUT = 20000;
  private final ActivityUtils activityUtils = new ActivityUtils(paramInstrumentation, paramActivity, this.sleeper);
  private final Asserter asserter = new Asserter(this.activityUtils, this.waiter);
  private final Checker checker = new Checker(this.viewFetcher, this.waiter);
  private final Clicker clicker = new Clicker(this.viewFetcher, this.scroller, this.robotiumUtils, paramInstrumentation, this.sleeper, this.waiter);
  private final DialogUtils dialogUtils = new DialogUtils(this.viewFetcher, this.sleeper);
  private final Getter getter = new Getter(this.activityUtils, this.viewFetcher, this.waiter);
  private final Presser presser = new Presser(this.clicker, paramInstrumentation, this.sleeper, this.waiter);
  private final RobotiumUtils robotiumUtils = new RobotiumUtils(paramInstrumentation, this.sleeper);
  private final Scroller scroller = new Scroller(paramInstrumentation, this.activityUtils, this.viewFetcher, this.sleeper);
  private final Searcher searcher = new Searcher(this.viewFetcher, this.scroller, this.sleeper);
  private final Setter setter = new Setter(this.activityUtils);
  private final Sleeper sleeper = new Sleeper();
  private final TextEnterer textEnterer;
  private final ViewFetcher viewFetcher = new ViewFetcher(this.activityUtils);
  private final Waiter waiter = new Waiter(this.activityUtils, this.viewFetcher, this.searcher, this.scroller, this.sleeper);

  public Solo(Instrumentation paramInstrumentation)
  {
    this(paramInstrumentation, null);
  }

  public Solo(Instrumentation paramInstrumentation, Activity paramActivity)
  {
    this.textEnterer = new TextEnterer(paramInstrumentation);
  }

  public void assertCurrentActivity(String paramString, Class paramClass)
  {
    this.asserter.assertCurrentActivity(paramString, paramClass);
  }

  public void assertCurrentActivity(String paramString, Class paramClass, boolean paramBoolean)
  {
    this.asserter.assertCurrentActivity(paramString, paramClass, paramBoolean);
  }

  public void assertCurrentActivity(String paramString1, String paramString2)
  {
    this.asserter.assertCurrentActivity(paramString1, paramString2);
  }

  public void assertCurrentActivity(String paramString1, String paramString2, boolean paramBoolean)
  {
    this.asserter.assertCurrentActivity(paramString1, paramString2, paramBoolean);
  }

  public void assertMemoryNotLow()
  {
    this.asserter.assertMemoryNotLow();
  }

  public void clearEditText(int paramInt)
  {
    this.textEnterer.setEditText((EditText)this.waiter.waitForAndGetView(paramInt, EditText.class), "");
  }

  public void clearEditText(EditText paramEditText)
  {
    this.waiter.waitForView(paramEditText, 10000);
    this.textEnterer.setEditText(paramEditText, "");
  }

  public ArrayList<TextView> clickInList(int paramInt)
  {
    return this.clicker.clickInList(paramInt);
  }

  public ArrayList<TextView> clickInList(int paramInt1, int paramInt2)
  {
    return this.clicker.clickInList(paramInt1, paramInt2, false, 0);
  }

  public ArrayList<TextView> clickLongInList(int paramInt)
  {
    return this.clicker.clickInList(paramInt, 0, true, 0);
  }

  public ArrayList<TextView> clickLongInList(int paramInt1, int paramInt2)
  {
    return this.clicker.clickInList(paramInt1, paramInt2, true, 0);
  }

  public ArrayList<TextView> clickLongInList(int paramInt1, int paramInt2, int paramInt3)
  {
    return this.clicker.clickInList(paramInt1, paramInt2, true, paramInt3);
  }

  public void clickLongOnScreen(float paramFloat1, float paramFloat2)
  {
    this.clicker.clickLongOnScreen(paramFloat1, paramFloat2, 0);
  }

  public void clickLongOnScreen(float paramFloat1, float paramFloat2, int paramInt)
  {
    this.clicker.clickLongOnScreen(paramFloat1, paramFloat2, paramInt);
  }

  public void clickLongOnText(String paramString)
  {
    this.clicker.clickOnText(paramString, true, 1, true, 0);
  }

  public void clickLongOnText(String paramString, int paramInt)
  {
    this.clicker.clickOnText(paramString, true, paramInt, true, 0);
  }

  public void clickLongOnText(String paramString, int paramInt1, int paramInt2)
  {
    this.clicker.clickOnText(paramString, true, paramInt1, true, paramInt2);
  }

  public void clickLongOnText(String paramString, int paramInt, boolean paramBoolean)
  {
    this.clicker.clickOnText(paramString, true, paramInt, paramBoolean, 0);
  }

  public void clickLongOnTextAndPress(String paramString, int paramInt)
  {
    this.clicker.clickLongOnTextAndPress(paramString, paramInt);
  }

  public void clickLongOnView(View paramView)
  {
    this.clicker.clickOnScreen(paramView, true, 0);
  }

  public void clickLongOnView(View paramView, int paramInt)
  {
    this.clicker.clickOnScreen(paramView, true, paramInt);
  }

  public void clickOnButton(int paramInt)
  {
    this.clicker.clickOn(Button.class, paramInt);
  }

  public void clickOnButton(String paramString)
  {
    this.clicker.clickOn(Button.class, paramString);
  }

  public void clickOnCheckBox(int paramInt)
  {
    this.clicker.clickOn(CheckBox.class, paramInt);
  }

  public void clickOnEditText(int paramInt)
  {
    this.clicker.clickOn(EditText.class, paramInt);
  }

  public void clickOnImage(int paramInt)
  {
    this.clicker.clickOn(ImageView.class, paramInt);
  }

  public void clickOnImageButton(int paramInt)
  {
    this.clicker.clickOn(ImageButton.class, paramInt);
  }

  public void clickOnMenuItem(String paramString)
  {
    this.clicker.clickOnMenuItem(paramString);
  }

  public void clickOnMenuItem(String paramString, boolean paramBoolean)
  {
    this.clicker.clickOnMenuItem(paramString, paramBoolean);
  }

  public void clickOnRadioButton(int paramInt)
  {
    this.clicker.clickOn(RadioButton.class, paramInt);
  }

  public void clickOnScreen(float paramFloat1, float paramFloat2)
  {
    this.sleeper.sleep();
    this.clicker.clickOnScreen(paramFloat1, paramFloat2);
  }

  public void clickOnText(String paramString)
  {
    this.clicker.clickOnText(paramString, false, 1, true, 0);
  }

  public void clickOnText(String paramString, int paramInt)
  {
    this.clicker.clickOnText(paramString, false, paramInt, true, 0);
  }

  public void clickOnText(String paramString, int paramInt, boolean paramBoolean)
  {
    this.clicker.clickOnText(paramString, false, paramInt, paramBoolean, 0);
  }

  public void clickOnToggleButton(String paramString)
  {
    this.clicker.clickOn(ToggleButton.class, paramString);
  }

  public void clickOnView(View paramView)
  {
    this.waiter.waitForView(paramView, 10000);
    this.clicker.clickOnScreen(paramView);
  }

  public void drag(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt)
  {
    this.scroller.drag(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramInt);
  }

  public void enterText(int paramInt, String paramString)
  {
    this.textEnterer.setEditText((EditText)this.waiter.waitForAndGetView(paramInt, EditText.class), paramString);
  }

  public void enterText(EditText paramEditText, String paramString)
  {
    this.waiter.waitForView(paramEditText, 10000);
    this.textEnterer.setEditText(paramEditText, paramString);
  }

  public void finalize()
    throws Throwable
  {
    this.activityUtils.finalize();
  }

  public void finishOpenedActivities()
  {
    this.activityUtils.finishOpenedActivities();
  }

  public Instrumentation.ActivityMonitor getActivityMonitor()
  {
    return this.activityUtils.getActivityMonitor();
  }

  public ArrayList<Activity> getAllOpenedActivities()
  {
    return this.activityUtils.getAllOpenedActivities();
  }

  public Button getButton(int paramInt)
  {
    return (Button)this.getter.getView(Button.class, paramInt);
  }

  public Button getButton(String paramString)
  {
    return (Button)this.getter.getView(Button.class, paramString, false);
  }

  public Button getButton(String paramString, boolean paramBoolean)
  {
    return (Button)this.getter.getView(Button.class, paramString, paramBoolean);
  }

  public Activity getCurrentActivity()
  {
    return this.activityUtils.getCurrentActivity();
  }

  public ArrayList<Button> getCurrentButtons()
  {
    return this.viewFetcher.getCurrentViews(Button.class);
  }

  public ArrayList<CheckBox> getCurrentCheckBoxes()
  {
    return this.viewFetcher.getCurrentViews(CheckBox.class);
  }

  public ArrayList<DatePicker> getCurrentDatePickers()
  {
    return this.viewFetcher.getCurrentViews(DatePicker.class);
  }

  public ArrayList<EditText> getCurrentEditTexts()
  {
    return this.viewFetcher.getCurrentViews(EditText.class);
  }

  public ArrayList<GridView> getCurrentGridViews()
  {
    return this.viewFetcher.getCurrentViews(GridView.class);
  }

  public ArrayList<ImageButton> getCurrentImageButtons()
  {
    return this.viewFetcher.getCurrentViews(ImageButton.class);
  }

  public ArrayList<ImageView> getCurrentImageViews()
  {
    return this.viewFetcher.getCurrentViews(ImageView.class);
  }

  public ArrayList<ListView> getCurrentListViews()
  {
    return this.viewFetcher.getCurrentViews(ListView.class);
  }

  public ArrayList<ProgressBar> getCurrentProgressBars()
  {
    return this.viewFetcher.getCurrentViews(ProgressBar.class);
  }

  public ArrayList<RadioButton> getCurrentRadioButtons()
  {
    return this.viewFetcher.getCurrentViews(RadioButton.class);
  }

  public ArrayList<ScrollView> getCurrentScrollViews()
  {
    return this.viewFetcher.getCurrentViews(ScrollView.class);
  }

  public ArrayList<SlidingDrawer> getCurrentSlidingDrawers()
  {
    return this.viewFetcher.getCurrentViews(SlidingDrawer.class);
  }

  public ArrayList<Spinner> getCurrentSpinners()
  {
    return this.viewFetcher.getCurrentViews(Spinner.class);
  }

  public ArrayList<TextView> getCurrentTextViews(View paramView)
  {
    return this.viewFetcher.getCurrentViews(TextView.class, paramView);
  }

  public ArrayList<TimePicker> getCurrentTimePickers()
  {
    return this.viewFetcher.getCurrentViews(TimePicker.class);
  }

  public ArrayList<ToggleButton> getCurrentToggleButtons()
  {
    return this.viewFetcher.getCurrentViews(ToggleButton.class);
  }

  public ArrayList<View> getCurrentViews()
  {
    return this.viewFetcher.getViews(null, true);
  }

  public EditText getEditText(int paramInt)
  {
    return (EditText)this.getter.getView(EditText.class, paramInt);
  }

  public EditText getEditText(String paramString)
  {
    return (EditText)this.getter.getView(EditText.class, paramString, false);
  }

  public EditText getEditText(String paramString, boolean paramBoolean)
  {
    return (EditText)this.getter.getView(EditText.class, paramString, paramBoolean);
  }

  public ImageView getImage(int paramInt)
  {
    return (ImageView)this.getter.getView(ImageView.class, paramInt);
  }

  public ImageButton getImageButton(int paramInt)
  {
    return (ImageButton)this.getter.getView(ImageButton.class, paramInt);
  }

  public String getString(int paramInt)
  {
    return this.activityUtils.getString(paramInt);
  }

  public TextView getText(int paramInt)
  {
    return (TextView)this.getter.getView(TextView.class, paramInt);
  }

  public TextView getText(String paramString)
  {
    return this.getter.getView(TextView.class, paramString, false);
  }

  public TextView getText(String paramString, boolean paramBoolean)
  {
    return this.getter.getView(TextView.class, paramString, paramBoolean);
  }

  public View getTopParent(View paramView)
  {
    return this.viewFetcher.getTopParent(paramView);
  }

  public View getView(int paramInt)
  {
    return this.getter.getView(paramInt);
  }

  public <T extends View> View getView(Class<T> paramClass, int paramInt)
  {
    return this.waiter.waitForAndGetView(paramInt, paramClass);
  }

  public ArrayList<View> getViews()
  {
    try
    {
      ArrayList localArrayList = this.viewFetcher.getViews(null, false);
      return localArrayList;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  public ArrayList<View> getViews(View paramView)
  {
    try
    {
      ArrayList localArrayList = this.viewFetcher.getViews(paramView, false);
      return localArrayList;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  public void goBack()
  {
    this.robotiumUtils.goBack();
  }

  public void goBackToActivity(String paramString)
  {
    this.activityUtils.goBackToActivity(paramString);
  }

  public boolean isCheckBoxChecked(int paramInt)
  {
    return this.checker.isButtonChecked(CheckBox.class, paramInt);
  }

  public boolean isCheckBoxChecked(String paramString)
  {
    return this.checker.isButtonChecked(CheckBox.class, paramString);
  }

  public boolean isRadioButtonChecked(int paramInt)
  {
    return this.checker.isButtonChecked(RadioButton.class, paramInt);
  }

  public boolean isRadioButtonChecked(String paramString)
  {
    return this.checker.isButtonChecked(RadioButton.class, paramString);
  }

  public boolean isSpinnerTextSelected(int paramInt, String paramString)
  {
    return this.checker.isSpinnerTextSelected(paramInt, paramString);
  }

  public boolean isSpinnerTextSelected(String paramString)
  {
    return this.checker.isSpinnerTextSelected(paramString);
  }

  public boolean isTextChecked(String paramString)
  {
    this.waiter.waitForViews(CheckedTextView.class, CompoundButton.class);
    if ((this.viewFetcher.getCurrentViews(CheckedTextView.class).size() > 0) && (this.checker.isCheckedTextChecked(paramString)));
    while ((this.viewFetcher.getCurrentViews(CompoundButton.class).size() > 0) && (this.checker.isButtonChecked(CompoundButton.class, paramString)))
      return true;
    return false;
  }

  public boolean isToggleButtonChecked(int paramInt)
  {
    return this.checker.isButtonChecked(ToggleButton.class, paramInt);
  }

  public boolean isToggleButtonChecked(String paramString)
  {
    return this.checker.isButtonChecked(ToggleButton.class, paramString);
  }

  public void pressMenuItem(int paramInt)
  {
    this.presser.pressMenuItem(paramInt);
  }

  public void pressMenuItem(int paramInt1, int paramInt2)
  {
    this.presser.pressMenuItem(paramInt1, paramInt2);
  }

  public void pressSpinnerItem(int paramInt1, int paramInt2)
  {
    this.presser.pressSpinnerItem(paramInt1, paramInt2);
  }

  public boolean scrollDown()
  {
    this.waiter.waitForViews(AbsListView.class, ScrollView.class);
    return this.scroller.scroll(0);
  }

  public boolean scrollDownList(int paramInt)
  {
    return this.scroller.scrollList(ListView.class, (AbsListView)this.waiter.waitForAndGetView(paramInt, ListView.class), 0, null);
  }

  public void scrollToSide(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return;
    case 22:
      this.scroller.scrollToSide(Scroller.Side.RIGHT);
      return;
    case 21:
    }
    this.scroller.scrollToSide(Scroller.Side.LEFT);
  }

  public boolean scrollUp()
  {
    this.waiter.waitForViews(AbsListView.class, ScrollView.class);
    return this.scroller.scroll(1);
  }

  public boolean scrollUpList(int paramInt)
  {
    return this.scroller.scrollList(ListView.class, (AbsListView)this.waiter.waitForAndGetView(paramInt, ListView.class), 1, null);
  }

  public boolean searchButton(String paramString)
  {
    return this.searcher.searchWithTimeoutFor(Button.class, paramString, 0, true, false);
  }

  public boolean searchButton(String paramString, int paramInt)
  {
    return this.searcher.searchWithTimeoutFor(Button.class, paramString, paramInt, true, false);
  }

  public boolean searchButton(String paramString, int paramInt, boolean paramBoolean)
  {
    return this.searcher.searchWithTimeoutFor(Button.class, paramString, paramInt, true, paramBoolean);
  }

  public boolean searchButton(String paramString, boolean paramBoolean)
  {
    return this.searcher.searchWithTimeoutFor(Button.class, paramString, 0, true, paramBoolean);
  }

  public boolean searchEditText(String paramString)
  {
    return this.searcher.searchWithTimeoutFor(EditText.class, paramString, 1, true, false);
  }

  public boolean searchText(String paramString)
  {
    return this.searcher.searchWithTimeoutFor(TextView.class, paramString, 0, true, false);
  }

  public boolean searchText(String paramString, int paramInt)
  {
    return this.searcher.searchWithTimeoutFor(TextView.class, paramString, paramInt, true, false);
  }

  public boolean searchText(String paramString, int paramInt, boolean paramBoolean)
  {
    return this.searcher.searchWithTimeoutFor(TextView.class, paramString, paramInt, paramBoolean, false);
  }

  public boolean searchText(String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    return this.searcher.searchWithTimeoutFor(TextView.class, paramString, paramInt, paramBoolean1, paramBoolean2);
  }

  public boolean searchText(String paramString, boolean paramBoolean)
  {
    return this.searcher.searchWithTimeoutFor(TextView.class, paramString, 0, true, paramBoolean);
  }

  public boolean searchToggleButton(String paramString)
  {
    return this.searcher.searchWithTimeoutFor(ToggleButton.class, paramString, 0, true, false);
  }

  public boolean searchToggleButton(String paramString, int paramInt)
  {
    return this.searcher.searchWithTimeoutFor(ToggleButton.class, paramString, paramInt, true, false);
  }

  public void sendKey(int paramInt)
  {
    switch (paramInt)
    {
    default:
      this.robotiumUtils.sendKeyCode(paramInt);
      return;
    case 22:
      this.robotiumUtils.sendKeyCode(22);
      return;
    case 21:
      this.robotiumUtils.sendKeyCode(21);
      return;
    case 19:
      this.robotiumUtils.sendKeyCode(19);
      return;
    case 20:
      this.robotiumUtils.sendKeyCode(20);
      return;
    case 66:
      this.robotiumUtils.sendKeyCode(66);
      return;
    case 82:
      this.robotiumUtils.sendKeyCode(82);
      return;
    case 67:
    }
    this.robotiumUtils.sendKeyCode(67);
  }

  public void setActivityOrientation(int paramInt)
  {
    this.activityUtils.setActivityOrientation(paramInt);
  }

  public void setDatePicker(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    setDatePicker((DatePicker)this.waiter.waitForAndGetView(paramInt1, DatePicker.class), paramInt2, paramInt3, paramInt4);
  }

  public void setDatePicker(DatePicker paramDatePicker, int paramInt1, int paramInt2, int paramInt3)
  {
    this.waiter.waitForView(paramDatePicker, 10000);
    this.setter.setDatePicker(paramDatePicker, paramInt1, paramInt2, paramInt3);
  }

  public void setProgressBar(int paramInt1, int paramInt2)
  {
    setProgressBar((ProgressBar)this.waiter.waitForAndGetView(paramInt1, ProgressBar.class), paramInt2);
  }

  public void setProgressBar(ProgressBar paramProgressBar, int paramInt)
  {
    this.waiter.waitForView(paramProgressBar, 10000);
    this.setter.setProgressBar(paramProgressBar, paramInt);
  }

  public void setSlidingDrawer(int paramInt1, int paramInt2)
  {
    setSlidingDrawer((SlidingDrawer)this.waiter.waitForAndGetView(paramInt1, SlidingDrawer.class), paramInt2);
  }

  public void setSlidingDrawer(SlidingDrawer paramSlidingDrawer, int paramInt)
  {
    this.waiter.waitForView(paramSlidingDrawer, 10000);
    this.setter.setSlidingDrawer(paramSlidingDrawer, paramInt);
  }

  public void setTimePicker(int paramInt1, int paramInt2, int paramInt3)
  {
    setTimePicker((TimePicker)this.waiter.waitForAndGetView(paramInt1, TimePicker.class), paramInt2, paramInt3);
  }

  public void setTimePicker(TimePicker paramTimePicker, int paramInt1, int paramInt2)
  {
    this.waiter.waitForView(paramTimePicker, 10000);
    this.setter.setTimePicker(paramTimePicker, paramInt1, paramInt2);
  }

  public void sleep(int paramInt)
  {
    this.sleeper.sleep(paramInt);
  }

  public boolean waitForActivity(String paramString)
  {
    return this.waiter.waitForActivity(paramString, 20000);
  }

  public boolean waitForActivity(String paramString, int paramInt)
  {
    return this.waiter.waitForActivity(paramString, paramInt);
  }

  public boolean waitForDialogToClose(long paramLong)
  {
    return this.dialogUtils.waitForDialogToClose(paramLong);
  }

  public boolean waitForText(String paramString)
  {
    return this.waiter.waitForText(paramString);
  }

  public boolean waitForText(String paramString, int paramInt, long paramLong)
  {
    return this.waiter.waitForText(paramString, paramInt, paramLong);
  }

  public boolean waitForText(String paramString, int paramInt, long paramLong, boolean paramBoolean)
  {
    return this.waiter.waitForText(paramString, paramInt, paramLong, paramBoolean);
  }

  public boolean waitForText(String paramString, int paramInt, long paramLong, boolean paramBoolean1, boolean paramBoolean2)
  {
    return this.waiter.waitForText(paramString, paramInt, paramLong, paramBoolean1, paramBoolean2);
  }

  public <T extends View> boolean waitForView(View paramView)
  {
    return this.waiter.waitForView(paramView);
  }

  public <T extends View> boolean waitForView(View paramView, int paramInt, boolean paramBoolean)
  {
    return this.waiter.waitForView(paramView, paramInt, paramBoolean);
  }

  public <T extends View> boolean waitForView(Class<T> paramClass)
  {
    return this.waiter.waitForView(paramClass, 0, 20000, true);
  }

  public <T extends View> boolean waitForView(Class<T> paramClass, int paramInt1, int paramInt2)
  {
    int i = paramInt1 - 1;
    if (i < 1)
      i = 0;
    return this.waiter.waitForView(paramClass, i, paramInt2, true);
  }

  public <T extends View> boolean waitForView(Class<T> paramClass, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int i = paramInt1 - 1;
    if (i < 1)
      i = 0;
    return this.waiter.waitForView(paramClass, i, paramInt2, paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Solo
 * JD-Core Version:    0.6.2
 */