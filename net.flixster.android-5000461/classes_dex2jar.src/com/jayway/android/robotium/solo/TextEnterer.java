package com.jayway.android.robotium.solo;

import android.app.Instrumentation;
import android.widget.EditText;
import junit.framework.Assert;

class TextEnterer
{
  private final Instrumentation inst;

  public TextEnterer(Instrumentation paramInstrumentation)
  {
    this.inst = paramInstrumentation;
  }

  public void setEditText(final EditText paramEditText, final String paramString)
  {
    if (paramEditText != null)
    {
      final String str = paramEditText.getText().toString();
      if (!paramEditText.isEnabled())
        Assert.assertTrue("Edit text is not enabled!", false);
      this.inst.runOnMainSync(new Runnable()
      {
        public void run()
        {
          paramEditText.setInputType(0);
          paramEditText.performClick();
          if (paramString.equals(""))
          {
            paramEditText.setText(paramString);
            return;
          }
          paramEditText.setText(str + paramString);
          paramEditText.setCursorVisible(false);
        }
      });
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.TextEnterer
 * JD-Core Version:    0.6.2
 */