package com.jayway.android.robotium.solo;

import android.app.Activity;
import android.os.Build.VERSION;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ScrollView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class ViewFetcher
{
  private static Class<?> windowManager;
  private final ActivityUtils activityUtils;
  private String windowManagerString;

  static
  {
    try
    {
      windowManager = Class.forName("android.view.WindowManagerImpl");
      return;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new RuntimeException(localClassNotFoundException);
    }
    catch (SecurityException localSecurityException)
    {
      localSecurityException.printStackTrace();
    }
  }

  public ViewFetcher(ActivityUtils paramActivityUtils)
  {
    this.activityUtils = paramActivityUtils;
    setWindowManagerString();
  }

  private void addChildren(ArrayList<View> paramArrayList, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    int i = 0;
    if (i < paramViewGroup.getChildCount())
    {
      View localView = paramViewGroup.getChildAt(i);
      if ((paramBoolean) && (isViewSufficientlyShown(localView)))
        paramArrayList.add(localView);
      while (true)
      {
        if ((localView instanceof ViewGroup))
          addChildren(paramArrayList, (ViewGroup)localView, paramBoolean);
        i++;
        break;
        if (!paramBoolean)
          paramArrayList.add(localView);
      }
    }
  }

  private final View[] getNonDecorViews(View[] paramArrayOfView)
  {
    View[] arrayOfView = new View[paramArrayOfView.length];
    int i = 0;
    for (int j = 0; j < paramArrayOfView.length; j++)
    {
      View localView = paramArrayOfView[j];
      if (!localView.getClass().getName().equals("com.android.internal.policy.impl.PhoneWindow$DecorView"))
      {
        arrayOfView[i] = localView;
        i++;
      }
    }
    return arrayOfView;
  }

  private final View getRecentContainer(View[] paramArrayOfView)
  {
    Object localObject = null;
    long l = 0L;
    for (int i = 0; i < paramArrayOfView.length; i++)
    {
      View localView = paramArrayOfView[i];
      if ((localView != null) && (localView.isShown()) && (localView.hasWindowFocus()) && (localView.getDrawingTime() > l))
      {
        localObject = localView;
        l = localView.getDrawingTime();
      }
    }
    return localObject;
  }

  private void setWindowManagerString()
  {
    if (Build.VERSION.SDK_INT >= 13)
    {
      this.windowManagerString = "sWindowManager";
      return;
    }
    this.windowManagerString = "mWindowManager";
  }

  public ArrayList<View> getAllViews(boolean paramBoolean)
  {
    View[] arrayOfView1 = getWindowDecorViews();
    ArrayList localArrayList = new ArrayList();
    View[] arrayOfView2 = getNonDecorViews(arrayOfView1);
    int i;
    if ((arrayOfView1 != null) && (arrayOfView1.length > 0))
      i = 0;
    while (true)
    {
      View localView2;
      if (i < arrayOfView2.length)
        localView2 = arrayOfView2[i];
      try
      {
        addChildren(localArrayList, (ViewGroup)localView2, paramBoolean);
        label58: i++;
        continue;
        View localView1 = getRecentDecorView(arrayOfView1);
        try
        {
          addChildren(localArrayList, (ViewGroup)localView1, paramBoolean);
          return localArrayList;
        }
        catch (Exception localException1)
        {
          return localArrayList;
        }
      }
      catch (Exception localException2)
      {
        break label58;
      }
    }
  }

  public <T extends View> ArrayList<T> getCurrentViews(Class<T> paramClass)
  {
    return getCurrentViews(paramClass, null);
  }

  public <T extends View> ArrayList<T> getCurrentViews(Class<T> paramClass, View paramView)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = getViews(paramView, true).iterator();
    while (localIterator.hasNext())
    {
      View localView = (View)localIterator.next();
      if ((localView != null) && (paramClass.isAssignableFrom(localView.getClass())))
        localArrayList.add(paramClass.cast(localView));
    }
    return localArrayList;
  }

  public final View getRecentDecorView(View[] paramArrayOfView)
  {
    View[] arrayOfView = new View[paramArrayOfView.length];
    int i = 0;
    for (int j = 0; j < paramArrayOfView.length; j++)
    {
      View localView = paramArrayOfView[j];
      if (localView.getClass().getName().equals("com.android.internal.policy.impl.PhoneWindow$DecorView"))
      {
        arrayOfView[i] = localView;
        i++;
      }
    }
    return getRecentContainer(arrayOfView);
  }

  public float getScrollListWindowHeight(View paramView)
  {
    int[] arrayOfInt = new int[2];
    View localView = getScrollOrListParent(paramView);
    if (localView == null);
    for (float f = this.activityUtils.getCurrentActivity(false).getWindowManager().getDefaultDisplay().getHeight(); ; f = arrayOfInt[1] + localView.getHeight())
    {
      return f;
      localView.getLocationOnScreen(arrayOfInt);
    }
  }

  public View getScrollOrListParent(View paramView)
  {
    if ((!(paramView instanceof AbsListView)) && (!(paramView instanceof ScrollView)));
    try
    {
      View localView = getScrollOrListParent((View)paramView.getParent());
      paramView = localView;
      return paramView;
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  public View getTopParent(View paramView)
  {
    if ((paramView.getParent() != null) && (!paramView.getParent().getClass().getName().equals("android.view.ViewRoot")))
      paramView = getTopParent((View)paramView.getParent());
    return paramView;
  }

  public final <T extends View> T getView(Class<T> paramClass, ArrayList<T> paramArrayList)
  {
    Object localObject = null;
    long l = 0L;
    if (paramArrayList == null)
      paramArrayList = RobotiumUtils.removeInvisibleViews(getCurrentViews(paramClass));
    Iterator localIterator = paramArrayList.iterator();
    while (localIterator.hasNext())
    {
      View localView = (View)localIterator.next();
      if ((localView.getDrawingTime() > l) && (localView.getHeight() > 0))
      {
        l = localView.getDrawingTime();
        localObject = localView;
      }
    }
    return localObject;
  }

  public ArrayList<View> getViews(View paramView, boolean paramBoolean)
  {
    this.activityUtils.getCurrentActivity(false);
    ArrayList localArrayList = new ArrayList();
    if (paramView == null)
      localArrayList = getAllViews(paramBoolean);
    do
    {
      return localArrayList;
      localArrayList.add(paramView);
    }
    while (!(paramView instanceof ViewGroup));
    addChildren(localArrayList, (ViewGroup)paramView, paramBoolean);
    return localArrayList;
  }

  public View[] getWindowDecorViews()
  {
    try
    {
      Field localField1 = windowManager.getDeclaredField("mViews");
      Field localField2 = windowManager.getDeclaredField(this.windowManagerString);
      localField1.setAccessible(true);
      localField2.setAccessible(true);
      View[] arrayOfView = (View[])localField1.get(localField2.get(null));
      return arrayOfView;
    }
    catch (SecurityException localSecurityException)
    {
      localSecurityException.printStackTrace();
      return null;
    }
    catch (NoSuchFieldException localNoSuchFieldException)
    {
      while (true)
        localNoSuchFieldException.printStackTrace();
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      while (true)
        localIllegalArgumentException.printStackTrace();
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      while (true)
        localIllegalAccessException.printStackTrace();
    }
  }

  public final boolean isViewSufficientlyShown(View paramView)
  {
    int[] arrayOfInt1 = new int[2];
    int[] arrayOfInt2 = new int[2];
    if (paramView == null);
    while (true)
    {
      return false;
      float f = paramView.getHeight();
      View localView = getScrollOrListParent(paramView);
      paramView.getLocationOnScreen(arrayOfInt1);
      if (localView == null)
        arrayOfInt2[1] = 0;
      while ((arrayOfInt1[1] + f / 2.0F <= getScrollListWindowHeight(paramView)) && (arrayOfInt1[1] + f / 2.0F >= arrayOfInt2[1]))
      {
        return true;
        localView.getLocationOnScreen(arrayOfInt2);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.ViewFetcher
 * JD-Core Version:    0.6.2
 */