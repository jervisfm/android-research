package com.jayway.android.robotium.solo;

import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import junit.framework.Assert;

class Waiter
{
  private final int SMALLTIMEOUT = 10000;
  private final int TIMEOUT = 20000;
  private final ActivityUtils activityUtils;
  private final Scroller scroller;
  private final Searcher searcher;
  private final Sleeper sleeper;
  private final ViewFetcher viewFetcher;

  public Waiter(ActivityUtils paramActivityUtils, ViewFetcher paramViewFetcher, Searcher paramSearcher, Scroller paramScroller, Sleeper paramSleeper)
  {
    this.activityUtils = paramActivityUtils;
    this.viewFetcher = paramViewFetcher;
    this.searcher = paramSearcher;
    this.scroller = paramScroller;
    this.sleeper = paramSleeper;
  }

  public boolean waitForActivity(String paramString)
  {
    return waitForActivity(paramString, 10000);
  }

  public boolean waitForActivity(String paramString, int paramInt)
  {
    long l1 = System.currentTimeMillis();
    long l2 = l1 + paramInt;
    while ((!this.activityUtils.getCurrentActivity().getClass().getSimpleName().equals(paramString)) && (l1 < l2))
      l1 = System.currentTimeMillis();
    return l1 < l2;
  }

  public <T extends View> T waitForAndGetView(int paramInt, Class<T> paramClass)
  {
    long l = 10000L + System.currentTimeMillis();
    while ((System.currentTimeMillis() <= l) && (!waitForView(paramClass, paramInt, true, true)));
    int i = this.searcher.getNumberOfUniqueViews();
    ArrayList localArrayList = RobotiumUtils.removeInvisibleViews(this.viewFetcher.getCurrentViews(paramClass));
    if (localArrayList.size() < i)
    {
      int j = paramInt - (i - localArrayList.size());
      if (j >= 0)
        paramInt = j;
    }
    try
    {
      localView = (View)localArrayList.get(paramInt);
      return localView;
    }
    catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
    {
      while (true)
      {
        Assert.assertTrue(paramClass.getSimpleName() + " with index " + paramInt + " is not available!", false);
        View localView = null;
      }
    }
  }

  public boolean waitForText(String paramString)
  {
    return waitForText(paramString, 0, 20000L, true);
  }

  public boolean waitForText(String paramString, int paramInt)
  {
    return waitForText(paramString, paramInt, 20000L, true);
  }

  public boolean waitForText(String paramString, int paramInt, long paramLong)
  {
    return waitForText(paramString, paramInt, paramLong, true);
  }

  public boolean waitForText(String paramString, int paramInt, long paramLong, boolean paramBoolean)
  {
    return waitForText(paramString, paramInt, paramLong, paramBoolean, false);
  }

  public boolean waitForText(String paramString, int paramInt, long paramLong, boolean paramBoolean1, boolean paramBoolean2)
  {
    long l = paramLong + System.currentTimeMillis();
    do
    {
      if (System.currentTimeMillis() > l);
      for (int i = 1; i != 0; i = 0)
        return false;
      this.sleeper.sleep();
    }
    while (!this.searcher.searchFor(TextView.class, paramString, paramInt, paramBoolean1, paramBoolean2));
    return true;
  }

  public View waitForView(int paramInt)
  {
    new ArrayList();
    View localView;
    do
    {
      long l = 10000L + System.currentTimeMillis();
      Iterator localIterator;
      while (!localIterator.hasNext())
      {
        if (System.currentTimeMillis() > l)
          break;
        this.sleeper.sleep();
        localIterator = this.viewFetcher.getAllViews(false).iterator();
      }
      localView = (View)localIterator.next();
    }
    while (localView.getId() != paramInt);
    return localView;
    return null;
  }

  public boolean waitForView(View paramView)
  {
    return waitForView(paramView, 20000, true);
  }

  public boolean waitForView(View paramView, int paramInt)
  {
    return waitForView(paramView, paramInt, true);
  }

  public boolean waitForView(View paramView, int paramInt, boolean paramBoolean)
  {
    long l = System.currentTimeMillis() + paramInt;
    while (true)
    {
      boolean bool1 = System.currentTimeMillis() < l;
      boolean bool2 = false;
      if (bool1)
      {
        this.sleeper.sleep();
        if (this.searcher.searchFor(paramView))
          bool2 = true;
      }
      else
      {
        return bool2;
      }
      if (paramBoolean)
        this.scroller.scroll(0);
    }
  }

  public <T extends View> boolean waitForView(Class<T> paramClass, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    HashSet localHashSet = new HashSet();
    long l = System.currentTimeMillis() + paramInt2;
    while (true)
    {
      boolean bool1 = System.currentTimeMillis() < l;
      boolean bool2 = false;
      if (bool1)
      {
        this.sleeper.sleep();
        if (this.searcher.searchFor(localHashSet, paramClass, paramInt1))
          bool2 = true;
      }
      else
      {
        return bool2;
      }
      if (paramBoolean)
        this.scroller.scroll(0);
    }
  }

  public <T extends View> boolean waitForView(Class<T> paramClass, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    HashSet localHashSet = new HashSet();
    do
    {
      if (paramBoolean1)
        this.sleeper.sleep();
      boolean bool2;
      if (this.searcher.searchFor(localHashSet, paramClass, paramInt))
        bool2 = true;
      boolean bool1;
      do
      {
        return bool2;
        if (!paramBoolean2)
          break;
        bool1 = this.scroller.scroll(0);
        bool2 = false;
      }
      while (!bool1);
    }
    while (paramBoolean2);
    return false;
  }

  public <T extends View> boolean waitForViews(Class<T> paramClass, Class<? extends View> paramClass1)
  {
    long l = 10000L + System.currentTimeMillis();
    while (System.currentTimeMillis() < l)
    {
      if (waitForView(paramClass, 0, false, false));
      while (waitForView(paramClass1, 0, false, false))
        return true;
      this.scroller.scroll(0);
      this.sleeper.sleep();
    }
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.jayway.android.robotium.solo.Waiter
 * JD-Core Version:    0.6.2
 */