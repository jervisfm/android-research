package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVStatus;
import java.nio.ByteBuffer;
import java.util.HashMap;

public class JNI
{
  private static JNI a;

  public static JNI a()
  {
    if (a == null)
      a = new JNI();
    return a;
  }

  private native String[] cs(int paramInt1, int paramInt2, String paramString, int paramInt3);

  private native String[] d(int paramInt1, int paramInt2, ByteBuffer paramByteBuffer, int paramInt3);

  private native void da(String paramString);

  private native void gms(int paramInt1, int paramInt2, int[] paramArrayOfInt);

  private native int gn(int paramInt1, int paramInt2);

  private native int gsid();

  private native void hhe(int paramInt1, int paramInt2, String paramString);

  private native int hhr(int paramInt, String paramString);

  private native boolean ir();

  private native String[] os(int paramInt1, int paramInt2, String paramString, int paramInt3, boolean paramBoolean);

  private native int qa(int paramInt1, int paramInt2, String paramString);

  private native int qas(int paramInt1, int paramInt2);

  private native String[] qra(int paramInt1, int paramInt2, String paramString);

  private native String[] qras(int paramInt1, int paramInt2);

  private native int ra(int paramInt1, int paramInt2, String paramString);

  private native int rl(int paramInt1, int paramInt2, long paramLong1, long paramLong2, long paramLong3);

  private native void rsid(int paramInt);

  private native int sr(int paramInt1, int paramInt2, int[] paramArrayOfInt);

  private native String sr();

  private native int ss(String paramString);

  private native int ua(int paramInt1, int paramInt2, String paramString);

  private native String[] uc(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, int paramInt2, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, String paramString13);

  public final int a(int paramInt1, int paramInt2)
  {
    return gn(paramInt1, paramInt2);
  }

  public final int a(int paramInt1, int paramInt2, long paramLong1, long paramLong2, long paramLong3)
  {
    return rl(paramInt1, paramInt2, paramLong1, paramLong2, paramLong3);
  }

  public final int a(int paramInt1, int paramInt2, String paramString)
  {
    return ra(paramInt1, paramInt2, paramString);
  }

  public final int a(int paramInt1, int paramInt2, int[] paramArrayOfInt)
  {
    return sr(paramInt1, paramInt2, paramArrayOfInt);
  }

  public final int a(int paramInt, String paramString)
  {
    return hhr(paramInt, paramString);
  }

  public final int a(String paramString)
  {
    return ss(paramString);
  }

  public final void a(int paramInt)
  {
    rsid(paramInt);
  }

  public final String[] a(int paramInt1, int paramInt2, String paramString, int paramInt3)
  {
    return cs(paramInt1, paramInt2, paramString, paramInt3);
  }

  public final String[] a(int paramInt1, int paramInt2, String paramString, int paramInt3, boolean paramBoolean)
  {
    return os(paramInt1, paramInt2, paramString, paramInt3, paramBoolean);
  }

  public final String[] a(int paramInt1, int paramInt2, ByteBuffer paramByteBuffer, int paramInt3)
  {
    return d(paramInt1, paramInt2, paramByteBuffer, paramInt3);
  }

  public final String[] a(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, int paramInt2, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, String paramString13)
  {
    return uc(paramInt1, paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramInt2, paramString7, paramString8, paramString9, paramString10, paramString11, paramString12, paramString13);
  }

  public final int b()
  {
    return gsid();
  }

  public final int b(int paramInt1, int paramInt2)
  {
    return qas(paramInt1, paramInt2);
  }

  public final int b(int paramInt1, int paramInt2, String paramString)
  {
    return ua(paramInt1, paramInt2, paramString);
  }

  public final void b(int paramInt1, int paramInt2, int[] paramArrayOfInt)
  {
    gms(paramInt1, paramInt2, paramArrayOfInt);
  }

  public final void b(String paramString)
  {
    da(paramString);
  }

  public final int c(int paramInt1, int paramInt2, String paramString)
  {
    return qa(paramInt1, paramInt2, paramString);
  }

  public final String c()
  {
    return sr();
  }

  public final String[] c(int paramInt1, int paramInt2)
  {
    return qras(paramInt1, paramInt2);
  }

  public final boolean d()
  {
    return ir();
  }

  public final String[] d(int paramInt1, int paramInt2, String paramString)
  {
    return qra(paramInt1, paramInt2, paramString);
  }

  public final void e(int paramInt1, int paramInt2, String paramString)
  {
    hhe(paramInt1, paramInt2, paramString);
  }

  public void httpRequestCallback(int paramInt, String paramString1, String paramString2)
  {
    new h(paramInt, paramString1, paramString2);
  }

  public void keyReceivedCallback(int paramInt1, int paramInt2)
  {
    x localx = a.a().a(paramInt1, paramInt2);
    if (localx == null)
    {
      m.b("No task associated with event " + paramInt1 + "-" + paramInt2 + " (hd:krc)");
      a.a().toString();
      return;
    }
    localx.d();
  }

  public void reportEventCallback(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, boolean paramBoolean1, int paramInt5, boolean paramBoolean2, long paramLong1, long paramLong2, long paramLong3, boolean paramBoolean3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, String paramString2)
  {
    x localx = a.a().a(paramInt1, paramInt2);
    if (localx == null)
    {
      m.b("No task associated with event " + paramInt3 + "-" + paramInt4 + "-" + paramInt1 + "-" + paramInt2 + ", " + paramString2 + " (hd:rec)");
      a.a().toString();
      return;
    }
    t localt = t.a(paramInt3);
    HashMap localHashMap = new HashMap();
    if (paramBoolean3)
    {
      localHashMap.put("WVLicenseDurationRemainingKey", Long.valueOf(paramLong6));
      localHashMap.put("WVPurchaseDurationRemainingKey", Long.valueOf(paramLong5));
      localHashMap.put("WVPlaybackElapsedTimeKey", Long.valueOf(paramLong7));
    }
    if (paramBoolean2)
    {
      localHashMap.put("WVSystemIDKey", Long.valueOf(paramLong1));
      localHashMap.put("WVAssetIDKey", Long.valueOf(paramLong2));
      localHashMap.put("WVKeyIDKey", Long.valueOf(paramLong3));
      localHashMap.put("WVIsEncryptedKey", Boolean.valueOf(paramBoolean1));
    }
    if (paramString1.length() > 0)
    {
      localHashMap.put("WVAssetPathKey", paramString1);
      if (!paramString1.startsWith("http"))
        break label303;
      localHashMap.put("WVAssetTypeKey", Integer.valueOf(2));
    }
    while (true)
    {
      if ((paramInt5 > 0) && (paramInt5 <= 3))
        localHashMap.put("WVLicenseTypeKey", Integer.valueOf(paramInt5));
      localx.a(paramBoolean1);
      WVStatus localWVStatus = WVStatus.a(paramInt4);
      localx.a(localx.a(localt, localWVStatus), localWVStatus, paramString2, localHashMap);
      return;
      label303: localHashMap.put("WVAssetTypeKey", Integer.valueOf(1));
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.JNI
 * JD-Core Version:    0.6.2
 */