package com.widevine.drm.internal;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class a
{
  private static a b;
  private Set<x> a = Collections.synchronizedSet(new HashSet());

  public static a a()
  {
    try
    {
      if (b == null)
        b = new a();
      a locala = b;
      return locala;
    }
    finally
    {
    }
  }

  public final x a(int paramInt1, int paramInt2)
  {
    try
    {
      Iterator localIterator = this.a.iterator();
      x localx;
      int i;
      do
      {
        do
        {
          if (!localIterator.hasNext())
            break;
          localx = (x)localIterator.next();
        }
        while (localx.g() != paramInt1);
        i = localx.hashCode();
      }
      while (i != paramInt2);
      while (true)
      {
        return localx;
        localx = null;
      }
    }
    finally
    {
    }
  }

  public final boolean a(x paramx)
  {
    try
    {
      this.a.add(paramx);
      return true;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final boolean b(x paramx)
  {
    try
    {
      boolean bool = this.a.remove(paramx);
      return bool;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final String toString()
  {
    Iterator localIterator = this.a.iterator();
    x localx;
    for (String str = "\n===== ActiveTasks ====="; localIterator.hasNext(); str = str + "\nSession Id: 0x" + Integer.toHexString(localx.g()) + ", Task Hash: 0x" + Integer.toHexString(localx.hashCode()))
      localx = (x)localIterator.next();
    return str + "\n=======================";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.a
 * JD-Core Version:    0.6.2
 */