package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVStatus;

public final class aa extends Exception
{
  WVStatus a;
  private String b;

  public aa(WVStatus paramWVStatus, String paramString)
  {
    this.a = paramWVStatus;
    this.b = paramString;
  }

  public final WVStatus a()
  {
    return this.a;
  }

  public final String getMessage()
  {
    return this.b;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.aa
 * JD-Core Version:    0.6.2
 */