package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVStatus;
import java.nio.ByteBuffer;

public abstract class c extends x
{
  private String a;

  protected c(u paramu)
  {
    super(paramu);
  }

  protected final WVStatus a(String paramString, t paramt)
  {
    String[] arrayOfString = JNI.a().a(g(), hashCode(), paramString, paramt.ordinal(), h());
    if (arrayOfString.length < 2)
      return WVStatus.SystemCallError;
    this.a = arrayOfString[1];
    return WVStatus.a(Integer.parseInt(arrayOfString[0]));
  }

  protected final String a()
  {
    return this.a;
  }

  protected final String[] a(ByteBuffer paramByteBuffer, int paramInt)
  {
    return JNI.a().a(g(), hashCode(), paramByteBuffer, paramInt);
  }

  protected final WVStatus b(String paramString, t paramt)
  {
    String[] arrayOfString = JNI.a().a(g(), hashCode(), paramString, paramt.ordinal());
    if (arrayOfString.length < 2)
      return WVStatus.SystemCallError;
    this.a = arrayOfString[1];
    return WVStatus.a(Integer.parseInt(arrayOfString[0]));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.c
 * JD-Core Version:    0.6.2
 */