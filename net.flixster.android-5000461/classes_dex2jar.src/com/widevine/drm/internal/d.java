package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVStatus;
import com.widevine.drmapi.android.a;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public final class d extends l
{
  private RandomAccessFile o;
  private a p;
  private Thread q;
  private long r;

  public d(u paramu, a parama)
    throws aa
  {
    super(paramu, "ChunkedRandomAccessFile");
    this.p = parama;
    this.r = 0L;
    this.q = new e(this, this);
    this.q.start();
  }

  public d(u paramu, String paramString, long paramLong)
    throws aa
  {
    super(paramu, paramString);
    try
    {
      if (paramString.startsWith("file://"))
        paramString = paramString.substring("file://".length());
      this.o = new RandomAccessFile(paramString, "r");
      this.r = paramLong;
      this.q = new e(this, this);
      this.q.start();
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      throw new aa(WVStatus.FileNotPresent, localFileNotFoundException.getMessage());
    }
  }

  private void a(long paramLong)
    throws IOException
  {
    if (this.o != null)
      this.o.seek(paramLong);
  }

  private long i()
    throws IOException
  {
    if (this.o != null)
      return this.o.length();
    if (this.p != null)
      return this.p.b();
    return 0L;
  }

  public final void a(WVStatus paramWVStatus, String paramString)
  {
    super.a(paramWVStatus, paramString);
    try
    {
      if (this.o != null)
        this.o.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.d
 * JD-Core Version:    0.6.2
 */