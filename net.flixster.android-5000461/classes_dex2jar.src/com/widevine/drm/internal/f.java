package com.widevine.drm.internal;

import android.net.Uri;
import com.widevine.drmapi.android.WVStatus;

public final class f extends l
{
  private Uri o;
  private Thread p;
  private long q = 0L;
  private long r = 0L;
  private long s = 0L;

  public f(u paramu, String paramString)
    throws aa
  {
    super(paramu, paramString);
    this.o = Uri.parse(paramString);
    if (!h())
    {
      m.b("Unable to connect to remote server. Device is offline");
      throw new aa(WVStatus.CantConnectToMediaServer, "Network inaccessible (hrp:hrp)");
    }
    if (this.o.getHost() == null)
    {
      m.b("Invalid host in content url: " + paramString);
      throw new aa(WVStatus.CantConnectToMediaServer, "Invalid host in content url: " + paramString + " (hrp:hrp)");
    }
    this.p = new g(this, this);
    this.p.start();
  }

  public static int a(byte[] paramArrayOfByte, int paramInt)
  {
    if ((paramArrayOfByte[0] != 72) || (paramArrayOfByte[1] != 84) || (paramArrayOfByte[2] != 84) || (paramArrayOfByte[3] != 80));
    while (true)
    {
      return 0;
      for (int i = 4; i < paramInt - 3; i++)
        if ((paramArrayOfByte[i] == 13) && (paramArrayOfByte[(i + 1)] == 10) && (paramArrayOfByte[(i + 2)] == 13) && (paramArrayOfByte[(i + 3)] == 10))
          return i + 4;
    }
  }

  public static String a(String paramString)
  {
    int i = paramString.indexOf("\r\n");
    if (i == -1);
    int j;
    do
    {
      do
        return null;
      while (!paramString.substring(0, i).matches(".*HTTP/\\d.\\d\\s30\\d.*"));
      j = paramString.indexOf("Location:");
    }
    while (j == -1);
    for (int k = j + "Location:".length(); paramString.charAt(k) == ' '; k++);
    int m = paramString.indexOf(" ", k);
    int n = paramString.indexOf('\r', k);
    if ((n != -1) && ((m == -1) || (n < m)))
      m = n;
    if (m == -1)
      m = paramString.length();
    return paramString.substring(k, m);
  }

  private String a(String paramString, boolean paramBoolean, long[] paramArrayOfLong)
  {
    String str1 = "127.0.0.1";
    int i = paramString.indexOf(str1);
    if (i >= 0)
    {
      int i1 = i + str1.length();
      if ((paramString.length() > i1) && (paramString.charAt(i1) == ':'))
      {
        int i2 = i1 + 1;
        int i3 = 0;
        int i4 = 1;
        if ((i3 < 5) && (i4 != 0))
        {
          if ((paramString.length() > i2) && (Character.isDigit(paramString.charAt(i2))))
            i2++;
          while (true)
          {
            i3++;
            break;
            i4 = 0;
          }
        }
        str1 = paramString.substring(i, i2);
      }
    }
    Object localObject1 = paramString.replaceFirst("HEAD", "GET").replace("/" + this.b, this.o.getPath()).replace(str1, this.o.getHost());
    int m;
    int n;
    String str2;
    if (paramBoolean)
    {
      int k = ((String)localObject1).indexOf("Range: bytes=");
      if (k != -1)
      {
        m = k + 13;
        n = ((String)localObject1).indexOf('\r', m);
        if (n == -1)
          break label383;
        str2 = "";
        if (paramArrayOfLong[1] > 1L)
          str2 = String.valueOf(paramArrayOfLong[1]);
      }
    }
    label383: for (Object localObject2 = ((String)localObject1).substring(0, m) + paramArrayOfLong[0] + "-" + str2 + ((String)localObject1).substring(n, ((String)localObject1).length()); ; localObject2 = localObject1)
    {
      localObject1 = localObject2;
      int j;
      do
      {
        do
        {
          return localObject1;
          m.b("HTTP server header malformed - missing termination");
          return localObject1;
        }
        while (paramArrayOfLong[0] == 0L);
        j = ((String)localObject1).lastIndexOf("\r\n");
      }
      while (j < 0);
      return ((String)localObject1).substring(0, j) + "Range: bytes=" + paramArrayOfLong[0] + "-" + paramArrayOfLong[1] + "\r\n" + ((String)localObject1).substring(j);
    }
  }

  public final void a(WVStatus paramWVStatus, String paramString)
  {
    super.a(paramWVStatus, paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.f
 * JD-Core Version:    0.6.2
 */