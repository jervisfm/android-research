package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVStatus;
import java.net.MalformedURLException;
import java.net.URL;

public final class h
{
  private int a;
  private URL b;
  private String c;
  private Thread d;

  public h()
  {
  }

  public h(int paramInt, String paramString1, String paramString2)
  {
    this.a = paramInt;
    this.c = paramString2;
    try
    {
      this.b = new URL(paramString1);
      this.d = new i(this);
      this.d.start();
      return;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      JNI.a().e(WVStatus.CantConnectToDrmServer.ordinal(), this.a, "Malformed URL (hr:hr)");
    }
  }

  public static String a(int paramInt)
  {
    byte[] arrayOfByte = new byte[paramInt];
    int i = 0;
    if (i < paramInt)
    {
      int j = (byte)(int)(62.0D * Math.random());
      int k;
      if (j < 26)
        k = (byte)(j + 97);
      while (true)
      {
        arrayOfByte[i] = k;
        i++;
        break;
        if (j < 52)
          k = (byte)(65 + (j - 26));
        else
          k = (byte)(48 + (j - 52));
      }
    }
    return new String(arrayOfByte);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.h
 * JD-Core Version:    0.6.2
 */