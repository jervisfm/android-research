package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVEvent;
import com.widevine.drmapi.android.WVStatus;
import java.util.HashMap;
import java.util.Random;
import java.util.zip.CRC32;

public final class j extends x
{
  private HashMap<String, Object> a;

  public j(u paramu, HashMap<String, Object> paramHashMap)
  {
    super(paramu);
    this.a = paramHashMap;
  }

  private static char a(int paramInt)
  {
    int i = paramInt % 62;
    int j;
    if (i < 26)
      j = i + 65;
    while (true)
    {
      return (char)j;
      if (i < 52)
        j = i + 71;
      else
        j = i - 4;
    }
  }

  private void a(String paramString)
    throws aa
  {
    String str1;
    String str2;
    String str3;
    String str4;
    String str5;
    String str6;
    if (this.a.containsKey("WVPortalKey"))
    {
      str1 = (String)this.a.get("WVPortalKey");
      if (!this.a.containsKey("WVDRMServer"))
        break label452;
      str2 = (String)this.a.get("WVDRMServer");
      str3 = "";
      if (this.a.containsKey("WVAssetDBPathKey"))
        str3 = (String)this.a.get("WVAssetDBPathKey");
      str4 = "";
      if (this.a.containsKey("WVCAUserDataKey"))
        str4 = (String)this.a.get("WVCAUserDataKey");
      str5 = "";
      if (this.a.containsKey("WVDeviceIDKey"))
        str5 = (String)this.a.get("WVDeviceIDKey");
      str6 = "";
      if (this.a.containsKey("WVStreamIDKey"))
        str6 = (String)this.a.get("WVStreamIDKey");
      if (!this.a.containsKey("WVLicenseTypeKey"))
        break label465;
    }
    String[] arrayOfString;
    label452: label465: for (int i = ((Integer)this.a.get("WVLicenseTypeKey")).intValue(); ; i = 0)
    {
      String str7 = "";
      if (this.a.containsKey("WVAndroidIDKey"))
        str7 = (String)this.a.get("WVAndroidIDKey");
      String str8 = "";
      if (this.a.containsKey("WVIMEIKey"))
        str8 = (String)this.a.get("WVIMEIKey");
      String str9 = "";
      if (this.a.containsKey("WVWifiMacKey"))
        str9 = (String)this.a.get("WVWifiMacKey");
      String str10 = "";
      if (this.a.containsKey("WVHWDeviceKey"))
        str10 = (String)this.a.get("WVHWDeviceKey");
      String str11 = "";
      if (this.a.containsKey("WVHWModelKey"))
        str11 = (String)this.a.get("WVHWModelKey");
      String str12 = "";
      if (this.a.containsKey("WVUIDKey"))
        str12 = (String)this.a.get("WVUIDKey");
      arrayOfString = JNI.a().a(this.n.g(), str2, str3, str1, str4, str5, str6, i, str7, str8, str9, str10, str11, str12, paramString);
      if (arrayOfString.length >= 2)
        break;
      throw new aa(WVStatus.SystemCallError, "");
      throw new aa(WVStatus.MandatorySettingAbsent, "WVPortalKey absent");
      throw new aa(WVStatus.MandatorySettingAbsent, "WVDRMServer absent");
    }
    WVStatus localWVStatus = WVStatus.a(Integer.parseInt(arrayOfString[0]));
    if (localWVStatus != WVStatus.OK)
      throw new aa(localWVStatus, arrayOfString[1]);
  }

  protected final WVEvent a(t paramt, WVStatus paramWVStatus)
  {
    return WVEvent.Initialized;
  }

  public final void a()
    throws aa
  {
    Random localRandom = new Random();
    StringBuffer localStringBuffer = new StringBuffer();
    for (int i = 0; i < 14; i++)
      localStringBuffer.append(a(localRandom.nextInt(62)));
    a(localStringBuffer.toString());
  }

  public final void run()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("WVVersionKey", "5.0.0.5582");
    v localv = new v();
    Random localRandom = new Random();
    if (this.a.containsKey("WVAndroidIDKey"))
    {
      byte[] arrayOfByte = ((String)this.a.get("WVAndroidIDKey")).getBytes();
      CRC32 localCRC32 = new CRC32();
      localCRC32.update(arrayOfByte, 0, arrayOfByte.length);
      localRandom.setSeed(0xFFFFFFFF & localCRC32.getValue());
    }
    int i = JNI.a().a(this.n.g(), localRandom.nextInt());
    int j;
    try
    {
      j = localv.a(i);
      if (j == 0)
      {
        a(WVEvent.InitializeFailed, WVStatus.TamperDetected, "serror (11) ", localHashMap);
        return;
      }
    }
    catch (Exception localException1)
    {
      a(WVEvent.InitializeFailed, WVStatus.TamperDetected, "serror (12) " + localException1.getMessage(), localHashMap);
      return;
    }
    int k;
    try
    {
      k = localv.a(i, this.n.e());
      if (k == 0)
      {
        a(WVEvent.InitializeFailed, WVStatus.TamperDetected, "serror (13) ", localHashMap);
        return;
      }
    }
    catch (Exception localException2)
    {
      a(WVEvent.InitializeFailed, WVStatus.TamperDetected, "serror (14) " + localException2.getMessage(), localHashMap);
      return;
    }
    try
    {
      StringBuffer localStringBuffer = new StringBuffer();
      localStringBuffer.append(a(k & 0xF));
      localStringBuffer.append(a(localRandom.nextInt(62)));
      localStringBuffer.append(a((j & 0xF000000) >>> 24));
      localStringBuffer.append(a((j & 0xF0) >>> 4));
      localStringBuffer.append(a(localRandom.nextInt(62)));
      localStringBuffer.append(a((k & 0xF000000) >>> 24));
      localStringBuffer.append(a((j & 0x3F00) >>> 8));
      localStringBuffer.append(a((k & 0xF0) >>> 4));
      localStringBuffer.append(a((j & 0xF0000000) >>> 28));
      localStringBuffer.append(a(localRandom.nextInt(62)));
      localStringBuffer.append(a((k & 0x3F00) >>> 8));
      localStringBuffer.append(a(j & 0xF));
      localStringBuffer.append(a((k & 0xF0000000) >>> 28));
      localStringBuffer.append(a(localRandom.nextInt(62)));
      a(localStringBuffer.toString());
      this.n.c();
      a(WVEvent.Initialized, WVStatus.OK, null, localHashMap);
      return;
    }
    catch (aa localaa)
    {
      a(WVEvent.InitializeFailed, localaa.a, localaa.getMessage(), localHashMap);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.j
 * JD-Core Version:    0.6.2
 */