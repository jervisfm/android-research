package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVEvent;
import com.widevine.drmapi.android.WVStatus;
import java.util.HashMap;

public abstract class k extends c
{
  private boolean a = false;

  public k(u paramu)
  {
    super(paramu);
  }

  protected final void a(String paramString, long paramLong1, long paramLong2, long paramLong3)
  {
    if (!h())
      a(WVEvent.LicenseRequestFailed, WVStatus.CantConnectToDrmServer, "Network inaccessible", c());
    WVStatus localWVStatus;
    int i;
    do
    {
      return;
      localWVStatus = a(paramString, t.b);
      if (localWVStatus != WVStatus.OK)
      {
        a(WVEvent.LicenseRequestFailed, localWVStatus, a(), c());
        return;
      }
      this.a = true;
      i = JNI.a().a(g(), hashCode(), paramLong1, paramLong2, paramLong3);
    }
    while (localWVStatus == WVStatus.OK);
    a(WVEvent.LicenseRequestFailed, WVStatus.a(i), "License refresh failed (rlt:rl)", c());
  }

  // ERROR //
  protected final void a(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: sipush 5120
    //   3: newarray byte
    //   5: astore_3
    //   6: sipush 5120
    //   9: invokestatic 92	java/nio/ByteBuffer:allocateDirect	(I)Ljava/nio/ByteBuffer;
    //   12: astore 4
    //   14: aload_0
    //   15: invokevirtual 17	com/widevine/drm/internal/k:h	()Z
    //   18: ifne +20 -> 38
    //   21: aload_0
    //   22: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   25: getstatic 29	com/widevine/drmapi/android/WVStatus:CantConnectToDrmServer	Lcom/widevine/drmapi/android/WVStatus;
    //   28: ldc 94
    //   30: aload_0
    //   31: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   34: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   37: return
    //   38: aload_1
    //   39: ldc 96
    //   41: invokevirtual 102	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   44: ifeq +900 -> 944
    //   47: aload_1
    //   48: invokestatic 108	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   51: astore 18
    //   53: aload 18
    //   55: invokevirtual 111	android/net/Uri:getPort	()I
    //   58: istore 19
    //   60: iload 19
    //   62: iconst_m1
    //   63: if_icmpne +7 -> 70
    //   66: bipush 80
    //   68: istore 19
    //   70: new 113	java/net/Socket
    //   73: dup
    //   74: aload 18
    //   76: invokevirtual 116	android/net/Uri:getHost	()Ljava/lang/String;
    //   79: iload 19
    //   81: invokespecial 119	java/net/Socket:<init>	(Ljava/lang/String;I)V
    //   84: astore 20
    //   86: new 121	java/lang/StringBuilder
    //   89: dup
    //   90: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   93: ldc 126
    //   95: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   98: aload 18
    //   100: invokevirtual 133	android/net/Uri:getPath	()Ljava/lang/String;
    //   103: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   106: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   109: astore 21
    //   111: aload 18
    //   113: invokevirtual 139	android/net/Uri:getQuery	()Ljava/lang/String;
    //   116: ifnull +33 -> 149
    //   119: new 121	java/lang/StringBuilder
    //   122: dup
    //   123: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   126: aload 21
    //   128: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   131: ldc 141
    //   133: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   136: aload 18
    //   138: invokevirtual 139	android/net/Uri:getQuery	()Ljava/lang/String;
    //   141: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   144: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   147: astore 21
    //   149: new 121	java/lang/StringBuilder
    //   152: dup
    //   153: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   156: aload 21
    //   158: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   161: ldc 143
    //   163: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   166: aload 18
    //   168: invokevirtual 116	android/net/Uri:getHost	()Ljava/lang/String;
    //   171: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   174: ldc 145
    //   176: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   179: sipush 4096
    //   182: invokevirtual 148	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   185: ldc 150
    //   187: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   190: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   193: astore 22
    //   195: aload 20
    //   197: invokevirtual 154	java/net/Socket:getOutputStream	()Ljava/io/OutputStream;
    //   200: astore 24
    //   202: aload 24
    //   204: aload 22
    //   206: invokevirtual 158	java/lang/String:getBytes	()[B
    //   209: iconst_0
    //   210: aload 22
    //   212: invokevirtual 161	java/lang/String:length	()I
    //   215: invokevirtual 167	java/io/OutputStream:write	([BII)V
    //   218: aload 20
    //   220: invokevirtual 171	java/net/Socket:getInputStream	()Ljava/io/InputStream;
    //   223: astore 27
    //   225: iconst_0
    //   226: istore 28
    //   228: iconst_0
    //   229: istore 29
    //   231: iload 29
    //   233: sipush 3072
    //   236: if_icmpge +1343 -> 1579
    //   239: aload 27
    //   241: invokevirtual 176	java/io/InputStream:available	()I
    //   244: ifle +25 -> 269
    //   247: aload 27
    //   249: aload_3
    //   250: iload 29
    //   252: aload_3
    //   253: arraylength
    //   254: iload 29
    //   256: isub
    //   257: invokevirtual 180	java/io/InputStream:read	([BII)I
    //   260: istore 42
    //   262: iload 29
    //   264: iload 42
    //   266: iadd
    //   267: istore 29
    //   269: iload 29
    //   271: ifle +416 -> 687
    //   274: aload_3
    //   275: iload 29
    //   277: invokestatic 185	com/widevine/drm/internal/f:a	([BI)I
    //   280: istore 40
    //   282: iload 40
    //   284: ifle +403 -> 687
    //   287: new 98	java/lang/String
    //   290: dup
    //   291: aload_3
    //   292: iconst_0
    //   293: iload 40
    //   295: invokespecial 187	java/lang/String:<init>	([BII)V
    //   298: invokestatic 190	com/widevine/drm/internal/f:a	(Ljava/lang/String;)Ljava/lang/String;
    //   301: astore 41
    //   303: aload 41
    //   305: ifnull +382 -> 687
    //   308: iconst_0
    //   309: istore 29
    //   311: aload 41
    //   313: astore_1
    //   314: iconst_1
    //   315: istore 30
    //   317: aload 24
    //   319: invokevirtual 193	java/io/OutputStream:close	()V
    //   322: aload 27
    //   324: invokevirtual 194	java/io/InputStream:close	()V
    //   327: iload 30
    //   329: ifne -282 -> 47
    //   332: aload_3
    //   333: iload 29
    //   335: invokestatic 185	com/widevine/drm/internal/f:a	([BI)I
    //   338: istore 32
    //   340: iload 29
    //   342: iload 32
    //   344: isub
    //   345: istore 8
    //   347: iload 32
    //   349: ifgt +417 -> 766
    //   352: aload_0
    //   353: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   356: getstatic 197	com/widevine/drmapi/android/WVStatus:BadMedia	Lcom/widevine/drmapi/android/WVStatus;
    //   359: new 121	java/lang/StringBuilder
    //   362: dup
    //   363: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   366: ldc 199
    //   368: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   371: iload 32
    //   373: invokevirtual 148	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   376: ldc 201
    //   378: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   381: iload 8
    //   383: invokevirtual 148	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   386: ldc 203
    //   388: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   391: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   394: aload_0
    //   395: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   398: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   401: return
    //   402: astore 44
    //   404: aload_0
    //   405: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   408: getstatic 206	com/widevine/drmapi/android/WVStatus:CantConnectToMediaServer	Lcom/widevine/drmapi/android/WVStatus;
    //   411: new 121	java/lang/StringBuilder
    //   414: dup
    //   415: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   418: ldc 208
    //   420: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   423: aload 44
    //   425: invokevirtual 211	java/net/UnknownHostException:getMessage	()Ljava/lang/String;
    //   428: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   431: ldc 213
    //   433: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   436: aload_1
    //   437: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   440: ldc 215
    //   442: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   445: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   448: aload_0
    //   449: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   452: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   455: return
    //   456: astore 43
    //   458: aload_0
    //   459: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   462: getstatic 206	com/widevine/drmapi/android/WVStatus:CantConnectToMediaServer	Lcom/widevine/drmapi/android/WVStatus;
    //   465: new 121	java/lang/StringBuilder
    //   468: dup
    //   469: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   472: ldc 217
    //   474: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   477: aload 43
    //   479: invokevirtual 218	java/io/IOException:getMessage	()Ljava/lang/String;
    //   482: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   485: ldc 213
    //   487: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   490: aload_1
    //   491: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   494: ldc 215
    //   496: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   499: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   502: aload_0
    //   503: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   506: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   509: return
    //   510: astore 23
    //   512: aload_0
    //   513: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   516: getstatic 221	com/widevine/drmapi/android/WVStatus:LostConnection	Lcom/widevine/drmapi/android/WVStatus;
    //   519: new 121	java/lang/StringBuilder
    //   522: dup
    //   523: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   526: ldc 223
    //   528: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   531: aload 23
    //   533: invokevirtual 218	java/io/IOException:getMessage	()Ljava/lang/String;
    //   536: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   539: ldc 213
    //   541: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   544: aload_1
    //   545: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   548: ldc 215
    //   550: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   553: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   556: aload_0
    //   557: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   560: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   563: return
    //   564: astore 25
    //   566: aload 24
    //   568: invokevirtual 193	java/io/OutputStream:close	()V
    //   571: aload_0
    //   572: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   575: getstatic 221	com/widevine/drmapi/android/WVStatus:LostConnection	Lcom/widevine/drmapi/android/WVStatus;
    //   578: new 121	java/lang/StringBuilder
    //   581: dup
    //   582: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   585: ldc 225
    //   587: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   590: aload 25
    //   592: invokevirtual 218	java/io/IOException:getMessage	()Ljava/lang/String;
    //   595: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   598: ldc 213
    //   600: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   603: aload_1
    //   604: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   607: ldc 215
    //   609: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   612: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   615: aload_0
    //   616: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   619: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   622: return
    //   623: astore 35
    //   625: aload 24
    //   627: invokevirtual 193	java/io/OutputStream:close	()V
    //   630: aload 27
    //   632: invokevirtual 194	java/io/InputStream:close	()V
    //   635: aload_0
    //   636: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   639: getstatic 221	com/widevine/drmapi/android/WVStatus:LostConnection	Lcom/widevine/drmapi/android/WVStatus;
    //   642: new 121	java/lang/StringBuilder
    //   645: dup
    //   646: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   649: ldc 227
    //   651: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   654: aload 35
    //   656: invokevirtual 218	java/io/IOException:getMessage	()Ljava/lang/String;
    //   659: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   662: ldc 213
    //   664: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   667: aload_1
    //   668: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   671: ldc 215
    //   673: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   676: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   679: aload_0
    //   680: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   683: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   686: return
    //   687: ldc2_w 228
    //   690: invokestatic 233	com/widevine/drm/internal/k:sleep	(J)V
    //   693: iload 28
    //   695: bipush 10
    //   697: iadd
    //   698: istore 38
    //   700: iload 38
    //   702: sipush 10000
    //   705: if_icmple +880 -> 1585
    //   708: aload 24
    //   710: invokevirtual 193	java/io/OutputStream:close	()V
    //   713: aload 27
    //   715: invokevirtual 194	java/io/InputStream:close	()V
    //   718: aload_0
    //   719: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   722: getstatic 221	com/widevine/drmapi/android/WVStatus:LostConnection	Lcom/widevine/drmapi/android/WVStatus;
    //   725: new 121	java/lang/StringBuilder
    //   728: dup
    //   729: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   732: ldc 235
    //   734: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   737: aload_1
    //   738: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   741: ldc 215
    //   743: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   746: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   749: aload_0
    //   750: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   753: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   756: return
    //   757: astore 37
    //   759: iload 28
    //   761: istore 38
    //   763: goto -63 -> 700
    //   766: iload 8
    //   768: ifgt +43 -> 811
    //   771: aload_0
    //   772: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   775: getstatic 197	com/widevine/drmapi/android/WVStatus:BadMedia	Lcom/widevine/drmapi/android/WVStatus;
    //   778: new 121	java/lang/StringBuilder
    //   781: dup
    //   782: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   785: ldc 199
    //   787: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   790: iload 8
    //   792: invokevirtual 148	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   795: ldc 203
    //   797: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   800: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   803: aload_0
    //   804: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   807: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   810: return
    //   811: iload 8
    //   813: aload_3
    //   814: arraylength
    //   815: iload 32
    //   817: isub
    //   818: if_icmple +10 -> 828
    //   821: aload_3
    //   822: arraylength
    //   823: iload 32
    //   825: isub
    //   826: istore 8
    //   828: iload 8
    //   830: aload 4
    //   832: invokevirtual 238	java/nio/ByteBuffer:remaining	()I
    //   835: if_icmple +10 -> 845
    //   838: aload 4
    //   840: invokevirtual 238	java/nio/ByteBuffer:remaining	()I
    //   843: istore 8
    //   845: aload 4
    //   847: aload_3
    //   848: iload 32
    //   850: iload 8
    //   852: invokevirtual 242	java/nio/ByteBuffer:put	([BII)Ljava/nio/ByteBuffer;
    //   855: pop
    //   856: aload_0
    //   857: aload_1
    //   858: getstatic 44	com/widevine/drm/internal/t:b	Lcom/widevine/drm/internal/t;
    //   861: invokevirtual 47	com/widevine/drm/internal/k:a	(Ljava/lang/String;Lcom/widevine/drm/internal/t;)Lcom/widevine/drmapi/android/WVStatus;
    //   864: astore 11
    //   866: aload 11
    //   868: getstatic 50	com/widevine/drmapi/android/WVStatus:OK	Lcom/widevine/drmapi/android/WVStatus;
    //   871: if_acmpeq +466 -> 1337
    //   874: aload_0
    //   875: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   878: aload 11
    //   880: aload_0
    //   881: invokevirtual 53	com/widevine/drm/internal/k:a	()Ljava/lang/String;
    //   884: aload_0
    //   885: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   888: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   891: return
    //   892: astore 33
    //   894: aload_0
    //   895: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   898: getstatic 197	com/widevine/drmapi/android/WVStatus:BadMedia	Lcom/widevine/drmapi/android/WVStatus;
    //   901: new 121	java/lang/StringBuilder
    //   904: dup
    //   905: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   908: ldc 244
    //   910: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   913: iload 32
    //   915: invokevirtual 148	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   918: ldc 201
    //   920: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   923: iload 8
    //   925: invokevirtual 148	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   928: ldc 203
    //   930: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   933: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   936: aload_0
    //   937: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   940: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   943: return
    //   944: aload_1
    //   945: ldc 246
    //   947: invokevirtual 102	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   950: ifne +59 -> 1009
    //   953: aload_2
    //   954: invokevirtual 161	java/lang/String:length	()I
    //   957: ifeq +18 -> 975
    //   960: aload_2
    //   961: iconst_m1
    //   962: aload_2
    //   963: invokevirtual 161	java/lang/String:length	()I
    //   966: iadd
    //   967: invokevirtual 250	java/lang/String:charAt	(I)C
    //   970: bipush 47
    //   972: if_icmpeq +137 -> 1109
    //   975: aload_1
    //   976: iconst_0
    //   977: invokevirtual 250	java/lang/String:charAt	(I)C
    //   980: bipush 47
    //   982: if_icmpeq +127 -> 1109
    //   985: new 121	java/lang/StringBuilder
    //   988: dup
    //   989: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   992: aload_2
    //   993: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   996: bipush 47
    //   998: invokevirtual 253	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   1001: aload_1
    //   1002: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1005: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1008: astore_1
    //   1009: aload_1
    //   1010: ldc 246
    //   1012: invokevirtual 102	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   1015: ifeq +13 -> 1028
    //   1018: aload_1
    //   1019: ldc 246
    //   1021: invokevirtual 161	java/lang/String:length	()I
    //   1024: invokevirtual 257	java/lang/String:substring	(I)Ljava/lang/String;
    //   1027: astore_1
    //   1028: new 259	java/io/RandomAccessFile
    //   1031: dup
    //   1032: aload_1
    //   1033: ldc_w 261
    //   1036: invokespecial 263	java/io/RandomAccessFile:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   1039: astore 7
    //   1041: aload 7
    //   1043: aload_3
    //   1044: invokevirtual 266	java/io/RandomAccessFile:read	([B)I
    //   1047: istore 8
    //   1049: aload 7
    //   1051: invokevirtual 267	java/io/RandomAccessFile:close	()V
    //   1054: iload 8
    //   1056: ifgt +188 -> 1244
    //   1059: aload_0
    //   1060: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   1063: getstatic 197	com/widevine/drmapi/android/WVStatus:BadMedia	Lcom/widevine/drmapi/android/WVStatus;
    //   1066: new 121	java/lang/StringBuilder
    //   1069: dup
    //   1070: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   1073: ldc_w 269
    //   1076: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1079: aload_1
    //   1080: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1083: ldc 213
    //   1085: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1088: iload 8
    //   1090: invokevirtual 148	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1093: ldc 215
    //   1095: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1098: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1101: aload_0
    //   1102: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   1105: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   1108: return
    //   1109: new 121	java/lang/StringBuilder
    //   1112: dup
    //   1113: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   1116: aload_2
    //   1117: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1120: aload_1
    //   1121: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1124: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1127: astore_1
    //   1128: goto -119 -> 1009
    //   1131: astore 6
    //   1133: aload_0
    //   1134: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   1137: getstatic 272	com/widevine/drmapi/android/WVStatus:FileNotPresent	Lcom/widevine/drmapi/android/WVStatus;
    //   1140: new 121	java/lang/StringBuilder
    //   1143: dup
    //   1144: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   1147: ldc_w 274
    //   1150: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1153: aload_1
    //   1154: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1157: ldc_w 276
    //   1160: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1163: aload 6
    //   1165: invokevirtual 277	java/io/FileNotFoundException:getMessage	()Ljava/lang/String;
    //   1168: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1171: ldc 215
    //   1173: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1176: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1179: aload_0
    //   1180: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   1183: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   1186: return
    //   1187: astore 5
    //   1189: aload_0
    //   1190: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   1193: getstatic 272	com/widevine/drmapi/android/WVStatus:FileNotPresent	Lcom/widevine/drmapi/android/WVStatus;
    //   1196: new 121	java/lang/StringBuilder
    //   1199: dup
    //   1200: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   1203: ldc_w 279
    //   1206: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1209: aload 5
    //   1211: invokevirtual 218	java/io/IOException:getMessage	()Ljava/lang/String;
    //   1214: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1217: ldc_w 281
    //   1220: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1223: aload_1
    //   1224: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1227: ldc_w 283
    //   1230: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1233: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1236: aload_0
    //   1237: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   1240: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   1243: return
    //   1244: iload 8
    //   1246: aload_3
    //   1247: arraylength
    //   1248: if_icmple +7 -> 1255
    //   1251: aload_3
    //   1252: arraylength
    //   1253: istore 8
    //   1255: iload 8
    //   1257: aload 4
    //   1259: invokevirtual 238	java/nio/ByteBuffer:remaining	()I
    //   1262: if_icmple +10 -> 1272
    //   1265: aload 4
    //   1267: invokevirtual 238	java/nio/ByteBuffer:remaining	()I
    //   1270: istore 8
    //   1272: aload 4
    //   1274: aload_3
    //   1275: iconst_0
    //   1276: iload 8
    //   1278: invokevirtual 242	java/nio/ByteBuffer:put	([BII)Ljava/nio/ByteBuffer;
    //   1281: pop
    //   1282: goto -426 -> 856
    //   1285: astore 9
    //   1287: aload_0
    //   1288: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   1291: getstatic 197	com/widevine/drmapi/android/WVStatus:BadMedia	Lcom/widevine/drmapi/android/WVStatus;
    //   1294: new 121	java/lang/StringBuilder
    //   1297: dup
    //   1298: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   1301: ldc_w 285
    //   1304: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1307: aload_1
    //   1308: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1311: ldc 213
    //   1313: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1316: iload 8
    //   1318: invokevirtual 148	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1321: ldc 215
    //   1323: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1326: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1329: aload_0
    //   1330: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   1333: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   1336: return
    //   1337: aload_0
    //   1338: iconst_1
    //   1339: putfield 12	com/widevine/drm/internal/k:a	Z
    //   1342: aload_0
    //   1343: aload 4
    //   1345: iload 8
    //   1347: invokevirtual 288	com/widevine/drm/internal/k:a	(Ljava/nio/ByteBuffer;I)[Ljava/lang/String;
    //   1350: astore 12
    //   1352: aload 12
    //   1354: arraylength
    //   1355: iconst_3
    //   1356: if_icmpge +21 -> 1377
    //   1359: aload_0
    //   1360: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   1363: getstatic 197	com/widevine/drmapi/android/WVStatus:BadMedia	Lcom/widevine/drmapi/android/WVStatus;
    //   1366: ldc_w 290
    //   1369: aload_0
    //   1370: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   1373: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   1376: return
    //   1377: aload 12
    //   1379: iconst_0
    //   1380: aaload
    //   1381: invokestatic 296	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   1384: istore 13
    //   1386: aload 12
    //   1388: iconst_1
    //   1389: aaload
    //   1390: invokestatic 296	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   1393: istore 14
    //   1395: aload 12
    //   1397: iconst_2
    //   1398: aaload
    //   1399: astore 15
    //   1401: iload 13
    //   1403: tableswitch	default:+21 -> 1424, 0:+77->1480, 1:+138->1541
    //   1425: nop
    //   1426: lshl
    //   1427: dup
    //   1428: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   1431: ldc_w 298
    //   1434: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1437: iload 13
    //   1439: invokevirtual 148	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1442: ldc_w 300
    //   1445: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1448: aload 15
    //   1450: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1453: ldc 215
    //   1455: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1458: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1461: astore 17
    //   1463: aload_0
    //   1464: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   1467: getstatic 197	com/widevine/drmapi/android/WVStatus:BadMedia	Lcom/widevine/drmapi/android/WVStatus;
    //   1470: aload 17
    //   1472: aload_0
    //   1473: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   1476: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   1479: return
    //   1480: iload 14
    //   1482: ifge -1445 -> 37
    //   1485: new 121	java/lang/StringBuilder
    //   1488: dup
    //   1489: invokespecial 124	java/lang/StringBuilder:<init>	()V
    //   1492: ldc_w 298
    //   1495: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1498: iload 13
    //   1500: invokevirtual 148	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1503: ldc_w 300
    //   1506: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1509: aload 15
    //   1511: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1514: ldc 215
    //   1516: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1519: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1522: astore 16
    //   1524: aload_0
    //   1525: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   1528: getstatic 197	com/widevine/drmapi/android/WVStatus:BadMedia	Lcom/widevine/drmapi/android/WVStatus;
    //   1531: aload 16
    //   1533: aload_0
    //   1534: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   1537: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   1540: return
    //   1541: aload_0
    //   1542: getstatic 23	com/widevine/drmapi/android/WVEvent:LicenseRequestFailed	Lcom/widevine/drmapi/android/WVEvent;
    //   1545: getstatic 197	com/widevine/drmapi/android/WVStatus:BadMedia	Lcom/widevine/drmapi/android/WVStatus;
    //   1548: ldc_w 302
    //   1551: aload_0
    //   1552: invokevirtual 35	com/widevine/drm/internal/k:c	()Ljava/util/HashMap;
    //   1555: invokevirtual 38	com/widevine/drm/internal/k:a	(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    //   1558: return
    //   1559: astore 26
    //   1561: goto -990 -> 571
    //   1564: astore 31
    //   1566: goto -1239 -> 327
    //   1569: astore 39
    //   1571: goto -853 -> 718
    //   1574: astore 36
    //   1576: goto -941 -> 635
    //   1579: iconst_0
    //   1580: istore 30
    //   1582: goto -1265 -> 317
    //   1585: iload 38
    //   1587: istore 28
    //   1589: goto -1358 -> 231
    //
    // Exception table:
    //   from	to	target	type
    //   70	86	402	java/net/UnknownHostException
    //   70	86	456	java/io/IOException
    //   195	218	510	java/io/IOException
    //   218	225	564	java/io/IOException
    //   239	262	623	java/io/IOException
    //   687	693	757	java/lang/InterruptedException
    //   845	856	892	java/lang/IndexOutOfBoundsException
    //   1009	1028	1131	java/io/FileNotFoundException
    //   1028	1054	1131	java/io/FileNotFoundException
    //   1009	1028	1187	java/io/IOException
    //   1028	1054	1187	java/io/IOException
    //   1272	1282	1285	java/lang/IndexOutOfBoundsException
    //   566	571	1559	java/io/IOException
    //   317	327	1564	java/io/IOException
    //   708	718	1569	java/io/IOException
    //   625	635	1574	java/io/IOException
  }

  protected final boolean b()
  {
    return this.a;
  }

  protected abstract HashMap<String, Object> c();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.k
 * JD-Core Version:    0.6.2
 */