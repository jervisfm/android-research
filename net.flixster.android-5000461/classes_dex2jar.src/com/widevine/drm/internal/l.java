package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVEvent;
import com.widevine.drmapi.android.WVStatus;
import java.io.IOException;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Random;

public abstract class l extends c
{
  protected ServerSocket a;
  protected String b;
  protected boolean c = false;
  protected boolean d = false;
  protected boolean e = false;
  protected boolean f = false;
  protected boolean g = true;
  protected String h;
  protected boolean i;
  protected boolean j = false;
  protected long k = 0L;
  protected WVStatus l = WVStatus.OK;
  protected String m = "";
  private String o;
  private long p = 0L;
  private ByteBuffer q;
  private long r = 0L;
  private long s = 20L;
  private boolean t = false;

  public l(u paramu, String paramString)
    throws aa
  {
    super(paramu);
    try
    {
      this.a = new ServerSocket();
      this.a.bind(null);
      if (paramu.h() >= 0)
        this.a.setSoTimeout(paramu.h());
      while (true)
      {
        this.h = paramString;
        this.b = h.a(20);
        h.a(10);
        h.a(10);
        this.o = ("http://127.0.0.1:" + this.a.getLocalPort() + "/" + this.b);
        this.q = ByteBuffer.allocateDirect(65536);
        return;
        this.a.setSoTimeout(5000);
      }
    }
    catch (IOException localIOException)
    {
      String str = "Server socket error. " + localIOException.getMessage();
      throw new aa(WVStatus.SystemCallError, str);
    }
  }

  // ERROR //
  private long a(String paramString)
  {
    // Byte code:
    //   0: ldc2_w 149
    //   3: lstore_2
    //   4: ldc 152
    //   6: invokestatic 158	java/util/regex/Pattern:compile	(Ljava/lang/String;)Ljava/util/regex/Pattern;
    //   9: aload_1
    //   10: invokevirtual 162	java/util/regex/Pattern:split	(Ljava/lang/CharSequence;)[Ljava/lang/String;
    //   13: astore 4
    //   15: aload 4
    //   17: iconst_2
    //   18: aaload
    //   19: astore 5
    //   21: aload 4
    //   23: iconst_3
    //   24: aaload
    //   25: astore 6
    //   27: aload 4
    //   29: iconst_4
    //   30: aaload
    //   31: astore 7
    //   33: aload 4
    //   35: iconst_5
    //   36: aaload
    //   37: astore 8
    //   39: aload 4
    //   41: bipush 12
    //   43: aaload
    //   44: astore 9
    //   46: aload 5
    //   48: ldc 164
    //   50: invokevirtual 170	java/lang/String:compareToIgnoreCase	(Ljava/lang/String;)I
    //   53: ifeq +13 -> 66
    //   56: aload 5
    //   58: ldc 172
    //   60: invokevirtual 170	java/lang/String:compareToIgnoreCase	(Ljava/lang/String;)I
    //   63: ifne +87 -> 150
    //   66: aload 7
    //   68: ldc 164
    //   70: invokevirtual 170	java/lang/String:compareToIgnoreCase	(Ljava/lang/String;)I
    //   73: ifeq +13 -> 86
    //   76: aload 7
    //   78: ldc 172
    //   80: invokevirtual 170	java/lang/String:compareToIgnoreCase	(Ljava/lang/String;)I
    //   83: ifne +67 -> 150
    //   86: sipush 1013
    //   89: invokestatic 176	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   92: astore 10
    //   94: invokestatic 181	android/os/Process:myUid	()I
    //   97: invokestatic 176	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   100: astore 11
    //   102: aload_0
    //   103: getfield 80	com/widevine/drm/internal/l:a	Ljava/net/ServerSocket;
    //   106: invokevirtual 114	java/net/ServerSocket:getLocalPort	()I
    //   109: invokestatic 184	java/lang/Integer:toHexString	(I)Ljava/lang/String;
    //   112: astore 12
    //   114: aload 9
    //   116: aload 10
    //   118: invokevirtual 170	java/lang/String:compareToIgnoreCase	(Ljava/lang/String;)I
    //   121: ifne +31 -> 152
    //   124: aload 8
    //   126: aload 12
    //   128: invokevirtual 170	java/lang/String:compareToIgnoreCase	(Ljava/lang/String;)I
    //   131: ifne +19 -> 150
    //   134: aload 6
    //   136: bipush 16
    //   138: invokestatic 190	java/lang/Long:parseLong	(Ljava/lang/String;I)J
    //   141: lstore 17
    //   143: lload 17
    //   145: ldc2_w 191
    //   148: lor
    //   149: lstore_2
    //   150: lload_2
    //   151: lreturn
    //   152: aload 9
    //   154: aload 11
    //   156: invokevirtual 170	java/lang/String:compareToIgnoreCase	(Ljava/lang/String;)I
    //   159: ifne -9 -> 150
    //   162: aload 6
    //   164: aload 12
    //   166: invokevirtual 170	java/lang/String:compareToIgnoreCase	(Ljava/lang/String;)I
    //   169: ifne -19 -> 150
    //   172: aload 8
    //   174: bipush 16
    //   176: invokestatic 190	java/lang/Long:parseLong	(Ljava/lang/String;I)J
    //   179: lstore 14
    //   181: lload 14
    //   183: ldc2_w 193
    //   186: lor
    //   187: lreturn
    //   188: astore 16
    //   190: lload_2
    //   191: lreturn
    //   192: astore 13
    //   194: lload_2
    //   195: lreturn
    //
    // Exception table:
    //   from	to	target	type
    //   134	143	188	java/lang/NumberFormatException
    //   172	181	192	java/lang/NumberFormatException
  }

  protected static boolean a(String paramString, long[] paramArrayOfLong)
  {
    int n = paramString.indexOf("Range: bytes=");
    if (n == -1)
      return false;
    int i1 = n + 13;
    int i2 = paramString.indexOf('-', i1);
    int i3 = paramString.indexOf('\r', i1);
    if (i2 != 1)
    {
      paramArrayOfLong[0] = Long.parseLong(paramString.substring(i1, i2));
      if (i2 + 1 < i3)
      {
        long l1 = Long.parseLong(paramString.substring(i2 + 1, i3));
        if ((paramArrayOfLong[0] < l1) && (l1 < paramArrayOfLong[1]))
          paramArrayOfLong[1] = l1;
      }
    }
    while (true)
    {
      return true;
      paramArrayOfLong[0] = Long.parseLong(paramString.substring(i1, i3));
    }
  }

  protected final WVEvent a(t paramt, WVStatus paramWVStatus)
  {
    if (paramWVStatus == WVStatus.OK)
      return WVEvent.Playing;
    return WVEvent.PlayFailed;
  }

  public void a(WVStatus paramWVStatus, String paramString)
  {
    this.t = true;
    this.f = true;
    if (paramWVStatus == WVStatus.AlreadyPlaying)
      this.g = false;
    this.e = false;
    this.l = paramWVStatus;
    this.m = paramString;
  }

  public final void a(boolean paramBoolean)
  {
    this.i = paramBoolean;
  }

  protected final void a(long[] paramArrayOfLong)
  {
    int[] arrayOfInt = new int[4];
    arrayOfInt[0] = ((int)(0xFFFFFFFF & paramArrayOfLong[0] >> 32));
    arrayOfInt[1] = ((int)(0xFFFFFFFF & paramArrayOfLong[0]));
    arrayOfInt[2] = ((int)(0xFFFFFFFF & paramArrayOfLong[1] >> 32));
    arrayOfInt[3] = ((int)(0xFFFFFFFF & paramArrayOfLong[1]));
    JNI.a().a(g(), hashCode(), arrayOfInt);
    paramArrayOfLong[0] = (((0xFFFFFFFF & arrayOfInt[0]) << 32) + (0xFFFFFFFF & arrayOfInt[1]));
    paramArrayOfLong[1] = (((0xFFFFFFFF & arrayOfInt[2]) << 32) + (0xFFFFFFFF & arrayOfInt[3]));
  }

  // ERROR //
  protected final boolean a_()
  {
    // Byte code:
    //   0: iconst_2
    //   1: anewarray 166	java/lang/String
    //   4: dup
    //   5: iconst_0
    //   6: ldc 247
    //   8: aastore
    //   9: dup
    //   10: iconst_1
    //   11: ldc 249
    //   13: aastore
    //   14: astore_1
    //   15: ldc2_w 149
    //   18: lstore_2
    //   19: ldc2_w 149
    //   22: lstore 4
    //   24: new 104	java/lang/StringBuilder
    //   27: dup
    //   28: invokespecial 105	java/lang/StringBuilder:<init>	()V
    //   31: ldc 251
    //   33: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   36: invokestatic 254	android/os/Process:myPid	()I
    //   39: invokevirtual 117	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   42: ldc_w 256
    //   45: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   48: invokestatic 181	android/os/Process:myUid	()I
    //   51: invokevirtual 117	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   54: ldc_w 258
    //   57: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   60: aload_0
    //   61: getfield 80	com/widevine/drm/internal/l:a	Ljava/net/ServerSocket;
    //   64: invokevirtual 114	java/net/ServerSocket:getLocalPort	()I
    //   67: invokevirtual 117	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   70: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   73: pop
    //   74: iconst_0
    //   75: istore 7
    //   77: iload 7
    //   79: aload_1
    //   80: arraylength
    //   81: if_icmpge +222 -> 303
    //   84: new 260	java/io/RandomAccessFile
    //   87: dup
    //   88: aload_1
    //   89: iload 7
    //   91: aaload
    //   92: ldc_w 261
    //   95: invokespecial 264	java/io/RandomAccessFile:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   98: astore 8
    //   100: aload 8
    //   102: invokevirtual 267	java/io/RandomAccessFile:readLine	()Ljava/lang/String;
    //   105: astore 11
    //   107: aload 11
    //   109: ifnull +99 -> 208
    //   112: new 104	java/lang/StringBuilder
    //   115: dup
    //   116: invokespecial 105	java/lang/StringBuilder:<init>	()V
    //   119: ldc_w 269
    //   122: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   125: aload 11
    //   127: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   133: pop
    //   134: aload_0
    //   135: aload 11
    //   137: invokespecial 271	com/widevine/drm/internal/l:a	(Ljava/lang/String;)J
    //   140: lstore 13
    //   142: lload 13
    //   144: lconst_0
    //   145: lcmp
    //   146: iflt +221 -> 367
    //   149: ldc2_w 191
    //   152: lload 13
    //   154: land
    //   155: lconst_0
    //   156: lcmp
    //   157: ifeq +204 -> 361
    //   160: lload 13
    //   162: ldc2_w 191
    //   165: lxor
    //   166: lstore 17
    //   168: ldc2_w 193
    //   171: lload 13
    //   173: land
    //   174: lconst_0
    //   175: lcmp
    //   176: ifeq +178 -> 354
    //   179: ldc2_w 193
    //   182: lload 13
    //   184: lxor
    //   185: lstore 19
    //   187: aload 8
    //   189: invokevirtual 267	java/io/RandomAccessFile:readLine	()Ljava/lang/String;
    //   192: astore 23
    //   194: lload 19
    //   196: lstore 4
    //   198: lload 17
    //   200: lstore_2
    //   201: aload 23
    //   203: astore 11
    //   205: goto -98 -> 107
    //   208: aload 8
    //   210: invokevirtual 274	java/io/RandomAccessFile:close	()V
    //   213: iinc 7 1
    //   216: goto -139 -> 77
    //   219: astore 10
    //   221: new 104	java/lang/StringBuilder
    //   224: dup
    //   225: invokespecial 105	java/lang/StringBuilder:<init>	()V
    //   228: aload_0
    //   229: invokevirtual 278	java/lang/Object:getClass	()Ljava/lang/Class;
    //   232: invokevirtual 283	java/lang/Class:getName	()Ljava/lang/String;
    //   235: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   238: ldc_w 285
    //   241: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   244: aload 10
    //   246: invokevirtual 286	java/io/FileNotFoundException:getMessage	()Ljava/lang/String;
    //   249: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   252: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   255: invokestatic 291	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   258: goto -45 -> 213
    //   261: astore 9
    //   263: new 104	java/lang/StringBuilder
    //   266: dup
    //   267: invokespecial 105	java/lang/StringBuilder:<init>	()V
    //   270: aload_0
    //   271: invokevirtual 278	java/lang/Object:getClass	()Ljava/lang/Class;
    //   274: invokevirtual 283	java/lang/Class:getName	()Ljava/lang/String;
    //   277: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   280: ldc_w 285
    //   283: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   286: aload 9
    //   288: invokevirtual 139	java/io/IOException:getMessage	()Ljava/lang/String;
    //   291: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   294: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   297: invokestatic 291	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   300: goto -87 -> 213
    //   303: lload_2
    //   304: ldc2_w 149
    //   307: lcmp
    //   308: ifeq +12 -> 320
    //   311: lload_2
    //   312: lload 4
    //   314: lcmp
    //   315: ifne +5 -> 320
    //   318: iconst_1
    //   319: ireturn
    //   320: iconst_0
    //   321: ireturn
    //   322: astore 22
    //   324: lload 19
    //   326: lstore 4
    //   328: lload 17
    //   330: lstore_2
    //   331: aload 22
    //   333: astore 9
    //   335: goto -72 -> 263
    //   338: astore 21
    //   340: lload 19
    //   342: lstore 4
    //   344: lload 17
    //   346: lstore_2
    //   347: aload 21
    //   349: astore 10
    //   351: goto -130 -> 221
    //   354: lload 4
    //   356: lstore 19
    //   358: goto -171 -> 187
    //   361: lload_2
    //   362: lstore 17
    //   364: goto -196 -> 168
    //   367: lload 4
    //   369: lstore 15
    //   371: lload_2
    //   372: lstore 17
    //   374: lload 15
    //   376: lstore 19
    //   378: goto -191 -> 187
    //
    // Exception table:
    //   from	to	target	type
    //   84	107	219	java/io/FileNotFoundException
    //   112	142	219	java/io/FileNotFoundException
    //   208	213	219	java/io/FileNotFoundException
    //   84	107	261	java/io/IOException
    //   112	142	261	java/io/IOException
    //   208	213	261	java/io/IOException
    //   187	194	322	java/io/IOException
    //   187	194	338	java/io/FileNotFoundException
  }

  protected final int b(byte[] paramArrayOfByte, int paramInt)
  {
    int n = -1;
    long l1 = this.r;
    this.r = (1L + l1);
    v localv;
    if (l1 > this.s)
      localv = new v();
    String str;
    try
    {
      int i2 = new Random().nextInt();
      int i3 = localv.a(i2);
      if (i3 == 0)
        a(WVStatus.TamperDetected, "serror (21)");
      int i4 = (i3 & 0x3F00) >>> 8;
      if (i4 % 5 != 1)
        a(WVStatus.TamperDetected, "serror (25)");
      int i5 = (0 + ((0xFF00 & i2) >>> 8)) % 255;
      int i6 = (i5 + 0) % 255;
      int i7 = (i5 + ((0xFF0000 & i2) >>> 16)) % 255;
      int i8 = (i6 + i7) % 255;
      int i9 = (i7 + (i2 & 0xFF)) % 255;
      int i10 = (i8 + i9) % 255;
      int i11 = (i9 + ((i2 & 0xFF000000) >>> 24)) % 255;
      int i12 = (i10 + i11) % 255;
      int i13 = (i11 + (i4 & 0xFF)) % 255;
      int i14 = (i12 + i13) % 255;
      int i15 = (i13 + 123) % 255;
      int i16 = (i14 + i15) % 255;
      if ((i3 & 0xFF000000) >>> 24 != i15)
        a(WVStatus.TamperDetected, "serror (26)");
      if ((i3 & 0xFF) != i16)
        a(WVStatus.TamperDetected, "serror (27)");
      if (this.s < 159L)
        this.s = (2L * this.s);
      this.r = 0L;
      this.q.rewind();
      this.q.limit(this.q.capacity());
      if (paramInt < 0)
        return n;
    }
    catch (Exception localException)
    {
      String[] arrayOfString;
      do
      {
        while (true)
        {
          new StringBuilder().append(getClass().getName()).append(", ").append(localException.getMessage()).toString();
          a(WVStatus.TamperDetected, "serror (22): " + localException.getMessage());
        }
        if (paramInt > paramArrayOfByte.length)
          paramInt = paramArrayOfByte.length;
        if (paramInt > this.q.remaining())
          paramInt = this.q.remaining();
        this.q.put(paramArrayOfByte, 0, paramInt);
        arrayOfString = a(this.q, paramInt);
      }
      while (arrayOfString.length < 3);
      int i1 = Integer.parseInt(arrayOfString[0]);
      n = Integer.parseInt(arrayOfString[1]);
      str = arrayOfString[2];
      new StringBuilder().append("decrypt: parseResult: ").append(i1).append(", bytes: ").append(n).toString();
      switch (i1)
      {
      case 16:
      default:
        m.a("decrypt returned error: " + i1 + " (lhp:d)");
        return n;
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 7:
      case 9:
      case 8:
      case 100:
      case 101:
      case 102:
      case 103:
      case 104:
      case 106:
      }
    }
    if ((n > this.q.capacity()) || (n < 0))
    {
      m.a("decrypt error: " + n + " (lhp:d)");
      this.q.rewind();
      this.q.limit(0);
      n = 0;
    }
    while (true)
    {
      this.c = false;
      return n;
      this.q.rewind();
      this.q.limit(n);
      this.q.get(paramArrayOfByte, 0, n);
    }
    a(WVStatus.BadMedia, "Unsupported file format (lhp:d)");
    return n;
    a(WVStatus.BadMedia, "Unsupported data format (lhp:d)");
    return n;
    a(WVStatus.BadMedia, "Decode error (lhp:d)");
    return n;
    a(WVStatus.TamperDetected, "serror (23) (lhp:d)");
    return n;
    this.c = true;
    return n;
    a(WVStatus.OutOfMemoryError, "Write error (lhp:d)");
    return n;
    a(WVStatus.OutOfMemoryError, "Unable to reserve bytes (lhp:d)");
    return n;
    a(WVStatus.LicenseExpired, str);
    return n;
    a(WVStatus.ClockTamperDetected, "serror (24) (lhp:d)");
    return n;
    a(WVStatus.HeartbeatError, str);
    return n;
    a(WVStatus.AlreadyPlaying, "Another playback command has been requested");
    return n;
  }

  protected final long b()
  {
    if (this.p == 0L)
    {
      int[] arrayOfInt = { 0, 0 };
      JNI.a().b(g(), hashCode(), arrayOfInt);
      this.p = ((arrayOfInt[0] << 32) + arrayOfInt[1]);
    }
    return this.p;
  }

  public final void b(WVStatus paramWVStatus, String paramString)
  {
    this.t = true;
    HashMap localHashMap = new HashMap();
    localHashMap.put("WVAssetPathKey", this.h);
    localHashMap.put("WVIsEncryptedKey", Boolean.valueOf(this.i));
    a(WVEvent.Stopped, paramWVStatus, paramString, localHashMap);
  }

  public final String c()
  {
    return this.o;
  }

  protected final void d()
  {
    this.d = true;
  }

  public final boolean f()
  {
    return (!this.t) && (!this.j) && (this.k <= 10000L);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.l
 * JD-Core Version:    0.6.2
 */