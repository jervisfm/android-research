package com.widevine.drm.internal;

import android.util.Log;

public final class m
{
  private static n a = n.b;

  private static String a(n paramn, String paramString)
  {
    if (paramn.ordinal() <= a.ordinal())
    {
      String str1 = java.lang.Thread.currentThread().getStackTrace()[3].getClassName();
      String str2 = str1.substring(1 + str1.lastIndexOf("."));
      String str3 = java.lang.Thread.currentThread().getStackTrace()[3].getMethodName();
      int i = java.lang.Thread.currentThread().getStackTrace()[3].getLineNumber();
      return str2 + "." + str3 + "():" + i + ", " + paramString;
    }
    return null;
  }

  public static void a(String paramString)
  {
    String str = a(n.a, paramString);
    if (str != null)
      Log.e("WVDRM:", str);
  }

  public static void b(String paramString)
  {
    String str = a(n.b, paramString);
    if (str != null)
      Log.e("WVDRM:", str);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.m
 * JD-Core Version:    0.6.2
 */