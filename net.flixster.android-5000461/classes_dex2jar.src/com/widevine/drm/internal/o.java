package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVEvent;
import com.widevine.drmapi.android.WVStatus;
import java.util.HashMap;

public final class o extends k
{
  private String a = "";
  private boolean b = false;
  private String c;
  private long d;
  private long e;
  private long f;

  public o(u paramu)
  {
    super(paramu);
  }

  protected final WVEvent a(t paramt, WVStatus paramWVStatus)
  {
    if (paramWVStatus == WVStatus.OK)
      return WVEvent.LicenseReceived;
    return WVEvent.LicenseRequestFailed;
  }

  protected final HashMap<String, Object> c()
  {
    HashMap localHashMap = new HashMap();
    if ((this.c != null) && (this.c.length() > 0))
    {
      localHashMap.put("WVAssetPathKey", this.c);
      if (this.c.startsWith("http"))
        localHashMap.put("WVAssetTypeKey", Integer.valueOf(2));
      while (true)
      {
        if ((this.d != 0L) || (this.e != 0L) || (this.f != 0L))
        {
          localHashMap.put("WVSystemIDKey", Long.valueOf(this.d));
          localHashMap.put("WVAssetIDKey", Long.valueOf(this.e));
          localHashMap.put("WVKeyIDKey", Long.valueOf(this.f));
        }
        return localHashMap;
        localHashMap.put("WVAssetTypeKey", Integer.valueOf(1));
      }
    }
    return null;
  }

  protected final void e()
  {
    this.b = true;
  }

  public final void run()
  {
    a.a().a(this);
    String[] arrayOfString = JNI.a().c(g(), hashCode());
    if ((arrayOfString == null) || (arrayOfString.length < 2))
    {
      a(WVEvent.EndOfList, WVStatus.OutOfMemoryError, "JNI call failed( rlt:r)", null);
      a.a().b(this);
      return;
    }
    m.b("NowOnlineTask: results = " + arrayOfString.length);
    if (arrayOfString.length < 5)
    {
      a(WVEvent.EndOfList, WVStatus.a(Integer.parseInt(arrayOfString[0])), arrayOfString[1], null);
      a.a().b(this);
      return;
    }
    int i = 0;
    while (true)
    {
      int j;
      label162: label229: int k;
      if (i < arrayOfString.length)
        if (arrayOfString.length - i >= 5)
        {
          this.b = false;
          this.c = arrayOfString[(i + 0)];
          if (Integer.parseInt(arrayOfString[(i + 1)]) != 0)
          {
            j = 1;
            if (j == 0)
              break label265;
            this.d = Integer.parseInt(arrayOfString[(i + 2)]);
            this.e = Integer.parseInt(arrayOfString[(i + 3)]);
            this.f = Integer.parseInt(arrayOfString[(i + 4)]);
            a(this.c, this.d, this.e, this.f);
            k = 0;
            label232: if ((k >= 1200) || (this.b))
              break label295;
          }
        }
      try
      {
        sleep(100L);
        label253: k++;
        break label232;
        j = 0;
        break label162;
        label265: this.d = 0L;
        this.e = 0L;
        this.f = 0L;
        a(this.c, this.a);
        break label229;
        label295: if (b())
          b(this.c, t.b);
        i += 5;
        continue;
        a(WVEvent.EndOfList, WVStatus.OK, null, null);
        a.a().b(this);
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        break label253;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.o
 * JD-Core Version:    0.6.2
 */