package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVEvent;
import com.widevine.drmapi.android.WVStatus;
import java.util.HashMap;

public final class p extends x
{
  private String a;

  public p(u paramu, String paramString)
  {
    super(paramu);
    this.a = paramString;
  }

  protected final WVEvent a(t paramt, WVStatus paramWVStatus)
  {
    return WVEvent.QueryStatus;
  }

  public final void run()
  {
    a.a().a(this);
    WVStatus localWVStatus = WVStatus.a(JNI.a().c(g(), hashCode(), this.a));
    if (localWVStatus != WVStatus.OK)
    {
      HashMap localHashMap = new HashMap();
      localHashMap.put("WVAssetPathKey", this.a);
      a(WVEvent.QueryStatus, localWVStatus, null, localHashMap);
      a.a().b(this);
      return;
    }
    a.a().b(this);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.p
 * JD-Core Version:    0.6.2
 */