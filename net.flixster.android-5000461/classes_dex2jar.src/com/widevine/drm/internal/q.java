package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVEvent;
import com.widevine.drmapi.android.WVStatus;

public final class q extends x
{
  public q(u paramu)
  {
    super(paramu);
  }

  protected final WVEvent a(t paramt, WVStatus paramWVStatus)
  {
    if (paramt == t.c)
      return WVEvent.EndOfList;
    return WVEvent.QueryStatus;
  }

  public final void run()
  {
    a.a().a(this);
    WVStatus localWVStatus = WVStatus.a(JNI.a().b(g(), hashCode()));
    if (localWVStatus != WVStatus.OK)
    {
      a(WVEvent.EndOfList, localWVStatus, null, null);
      a.a().b(this);
      return;
    }
    a.a().b(this);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.q
 * JD-Core Version:    0.6.2
 */