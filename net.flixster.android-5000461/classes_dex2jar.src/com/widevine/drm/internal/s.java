package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVEvent;
import com.widevine.drmapi.android.WVStatus;
import java.util.HashMap;

public final class s extends k
{
  private String a;
  private String b;
  private long c;
  private long d;
  private long e;
  private boolean f;

  public s(u paramu, long paramLong1, long paramLong2, long paramLong3)
  {
    super(paramu);
    this.c = paramLong1;
    this.d = paramLong2;
    this.e = paramLong3;
    this.a = "";
    this.b = "";
    this.f = false;
  }

  public s(u paramu, String paramString)
  {
    super(paramu);
    this.a = paramString;
    this.b = "";
    this.f = false;
  }

  protected final WVEvent a(t paramt, WVStatus paramWVStatus)
  {
    if (paramWVStatus == WVStatus.OK)
      return WVEvent.LicenseReceived;
    return WVEvent.LicenseRequestFailed;
  }

  protected final HashMap<String, Object> c()
  {
    HashMap localHashMap = new HashMap();
    if ((this.a != null) && (this.a.length() > 0))
    {
      localHashMap.put("WVAssetPathKey", this.a);
      if (!this.a.startsWith("http"))
        break label130;
      localHashMap.put("WVAssetTypeKey", Integer.valueOf(2));
    }
    while (true)
    {
      if ((this.c != 0L) || (this.d != 0L) || (this.e != 0L))
      {
        localHashMap.put("WVSystemIDKey", Long.valueOf(this.c));
        localHashMap.put("WVAssetIDKey", Long.valueOf(this.d));
        localHashMap.put("WVKeyIDKey", Long.valueOf(this.e));
      }
      return localHashMap;
      label130: localHashMap.put("WVAssetTypeKey", Integer.valueOf(1));
    }
  }

  protected final void e()
  {
    this.f = true;
  }

  public final void run()
  {
    int i = 1;
    a.a().a(this);
    String[] arrayOfString;
    if (this.a.length() > 0)
    {
      arrayOfString = JNI.a().d(g(), hashCode(), this.a);
      if ((arrayOfString == null) || (arrayOfString.length < 2))
      {
        a(WVEvent.LicenseRequestFailed, WVStatus.OutOfMemoryError, "JNI call failed( rlt:r)", c());
        a.a().b(this);
        return;
      }
      if (arrayOfString.length < 5)
      {
        a(WVEvent.LicenseRequestFailed, WVStatus.a(Integer.parseInt(arrayOfString[0])), arrayOfString[i], c());
        a.a().b(this);
        return;
      }
      if (Integer.parseInt(arrayOfString[i]) == 0)
        break label243;
    }
    while (true)
    {
      if (i != 0)
      {
        this.c = Integer.parseInt(arrayOfString[2]);
        this.d = Integer.parseInt(arrayOfString[3]);
        this.e = Integer.parseInt(arrayOfString[4]);
      }
      label215: int j;
      if ((this.c != 0L) || (this.d != 0L) || (this.e != 0L))
      {
        a(this.a, this.c, this.d, this.e);
        j = 0;
        label217: if ((j >= 1200) || (this.f))
          break label263;
      }
      try
      {
        sleep(100L);
        label237: j++;
        break label217;
        label243: i = 0;
        continue;
        a(this.a, this.b);
        break label215;
        label263: if (b())
          b(this.a, t.b);
        a.a().b(this);
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        break label237;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.s
 * JD-Core Version:    0.6.2
 */