package com.widevine.drm.internal;

public enum t
{
  static
  {
    a = new t("Play", 3);
    g = new t("Stop", 4);
    h = new t("Register", 5);
    i = new t("RegisterLicense", 6);
    j = new t("Unregister", 7);
    k = new t("NowOnlineAllFirst", 8);
    l = new t("NowOnlineAll", 9);
    m = new t("NowOnlineAllEndOfList", 10);
    b = new t("RequestLicense", 11);
    n = new t("QueryAll", 12);
    c = new t("QueryAllEndOfList", 13);
    o = new t("Query", 14);
    p = new t("SecureStore", 15);
    q = new t("SecureRetrieve", 16);
    r = new t("Unknown", 17);
    t[] arrayOft = new t[18];
    arrayOft[0] = d;
    arrayOft[1] = e;
    arrayOft[2] = f;
    arrayOft[3] = a;
    arrayOft[4] = g;
    arrayOft[5] = h;
    arrayOft[6] = i;
    arrayOft[7] = j;
    arrayOft[8] = k;
    arrayOft[9] = l;
    arrayOft[10] = m;
    arrayOft[11] = b;
    arrayOft[12] = n;
    arrayOft[13] = c;
    arrayOft[14] = o;
    arrayOft[15] = p;
    arrayOft[16] = q;
    arrayOft[17] = r;
  }

  public static t a(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return r;
    case 0:
      return d;
    case 1:
      return e;
    case 2:
      return f;
    case 3:
      return a;
    case 4:
      return g;
    case 5:
      return h;
    case 6:
      return i;
    case 7:
      return j;
    case 8:
      return k;
    case 9:
      return l;
    case 10:
      return m;
    case 11:
      return b;
    case 12:
      return n;
    case 13:
      return c;
    case 14:
      return o;
    case 15:
      return p;
    case 16:
    }
    return q;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.t
 * JD-Core Version:    0.6.2
 */