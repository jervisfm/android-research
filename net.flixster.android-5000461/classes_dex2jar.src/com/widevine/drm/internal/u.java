package com.widevine.drm.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.widevine.drmapi.android.WVEventListener;

public final class u
{
  private static int h;
  private boolean a = false;
  private ConnectivityManager b;
  private ApplicationInfo c;
  private WVEventListener d;
  private int e;
  private int f = JNI.a().b();
  private int g = -1;

  static
  {
    try
    {
      System.loadLibrary("WVphoneAPI");
      return;
    }
    catch (Exception localException)
    {
      m.a("Load library:" + localException.getMessage());
    }
  }

  public u(WVEventListener paramWVEventListener, Context paramContext)
  {
    this.d = paramWVEventListener;
    this.b = ((ConnectivityManager)paramContext.getSystemService("connectivity"));
    this.c = paramContext.getApplicationInfo();
  }

  public static void j()
  {
    h = 1 + h;
  }

  public final void a()
  {
    JNI.a().a(this.e);
    this.a = false;
    this.e = 0;
    this.d = null;
  }

  public final void a(int paramInt)
  {
    if (paramInt >= 0)
      this.g = paramInt;
  }

  public final boolean b()
  {
    try
    {
      boolean bool = this.a;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final void c()
  {
    try
    {
      this.e = this.f;
      this.a = true;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final WVEventListener d()
  {
    try
    {
      WVEventListener localWVEventListener = this.d;
      return localWVEventListener;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final ApplicationInfo e()
  {
    try
    {
      ApplicationInfo localApplicationInfo = this.c;
      return localApplicationInfo;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final int f()
  {
    try
    {
      int i = this.e;
      return i;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final int g()
  {
    try
    {
      int i = this.f;
      return i;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final int h()
  {
    return this.g;
  }

  public final boolean i()
  {
    NetworkInfo localNetworkInfo = this.b.getActiveNetworkInfo();
    return (localNetworkInfo != null) && (localNetworkInfo.isConnected());
  }

  public final String toString()
  {
    return "\n===== Session =====\nSession::isInitialized = " + this.a + "\nSession::mId = 0x" + Integer.toHexString(this.e) + "\n===================";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.u
 * JD-Core Version:    0.6.2
 */