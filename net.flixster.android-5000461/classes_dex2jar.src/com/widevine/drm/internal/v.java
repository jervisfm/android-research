package com.widevine.drm.internal;

import android.content.pm.ApplicationInfo;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Properties;
import java.util.Random;
import java.util.jar.JarFile;
import java.util.zip.CRC32;
import java.util.zip.ZipException;

public final class v
{
  // ERROR //
  private int a(int paramInt, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, X509Certificate paramX509Certificate)
    throws Exception
  {
    // Byte code:
    //   0: aload 4
    //   2: aload_0
    //   3: invokespecial 31	com/widevine/drm/internal/v:a	()Ljava/security/PublicKey;
    //   6: invokevirtual 37	java/security/cert/X509Certificate:verify	(Ljava/security/PublicKey;)V
    //   9: aload 4
    //   11: invokevirtual 40	java/security/cert/X509Certificate:getPublicKey	()Ljava/security/PublicKey;
    //   14: astore 15
    //   16: ldc 42
    //   18: invokestatic 48	javax/crypto/Cipher:getInstance	(Ljava/lang/String;)Ljavax/crypto/Cipher;
    //   21: astore 16
    //   23: aload 16
    //   25: iconst_2
    //   26: aload 15
    //   28: invokevirtual 52	javax/crypto/Cipher:init	(ILjava/security/Key;)V
    //   31: aload 16
    //   33: aload_3
    //   34: invokevirtual 56	javax/crypto/Cipher:doFinal	([B)[B
    //   37: astore 17
    //   39: aload_2
    //   40: arraylength
    //   41: aload 17
    //   43: arraylength
    //   44: if_icmpne +684 -> 728
    //   47: iconst_0
    //   48: istore 18
    //   50: iload 18
    //   52: aload_2
    //   53: arraylength
    //   54: if_icmpge +445 -> 499
    //   57: aload_2
    //   58: iload 18
    //   60: baload
    //   61: aload 17
    //   63: iload 18
    //   65: baload
    //   66: if_icmpeq +427 -> 493
    //   69: new 12	java/lang/Exception
    //   72: dup
    //   73: ldc 58
    //   75: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   78: athrow
    //   79: astore 14
    //   81: new 63	java/lang/StringBuilder
    //   84: dup
    //   85: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   88: aload_0
    //   89: invokevirtual 68	java/lang/Object:getClass	()Ljava/lang/Class;
    //   92: invokevirtual 74	java/lang/Class:getName	()Ljava/lang/String;
    //   95: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   98: ldc 80
    //   100: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: aload 14
    //   105: invokevirtual 83	java/security/NoSuchAlgorithmException:getMessage	()Ljava/lang/String;
    //   108: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   111: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   114: invokestatic 90	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   117: new 12	java/lang/Exception
    //   120: dup
    //   121: new 63	java/lang/StringBuilder
    //   124: dup
    //   125: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   128: ldc 92
    //   130: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: aload 14
    //   135: invokevirtual 83	java/security/NoSuchAlgorithmException:getMessage	()Ljava/lang/String;
    //   138: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   141: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   144: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   147: athrow
    //   148: astore 9
    //   150: new 63	java/lang/StringBuilder
    //   153: dup
    //   154: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   157: aload_0
    //   158: invokevirtual 68	java/lang/Object:getClass	()Ljava/lang/Class;
    //   161: invokevirtual 74	java/lang/Class:getName	()Ljava/lang/String;
    //   164: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   167: ldc 80
    //   169: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: aload 9
    //   174: invokevirtual 93	java/security/cert/CertificateException:getMessage	()Ljava/lang/String;
    //   177: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   180: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   183: invokestatic 90	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   186: new 12	java/lang/Exception
    //   189: dup
    //   190: new 63	java/lang/StringBuilder
    //   193: dup
    //   194: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   197: ldc 95
    //   199: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   202: aload 9
    //   204: invokevirtual 93	java/security/cert/CertificateException:getMessage	()Ljava/lang/String;
    //   207: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   210: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   213: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   216: athrow
    //   217: astore 8
    //   219: new 63	java/lang/StringBuilder
    //   222: dup
    //   223: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   226: aload_0
    //   227: invokevirtual 68	java/lang/Object:getClass	()Ljava/lang/Class;
    //   230: invokevirtual 74	java/lang/Class:getName	()Ljava/lang/String;
    //   233: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   236: ldc 80
    //   238: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   241: aload 8
    //   243: invokevirtual 83	java/security/NoSuchAlgorithmException:getMessage	()Ljava/lang/String;
    //   246: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   249: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   252: invokestatic 90	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   255: new 12	java/lang/Exception
    //   258: dup
    //   259: new 63	java/lang/StringBuilder
    //   262: dup
    //   263: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   266: ldc 97
    //   268: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   271: aload 8
    //   273: invokevirtual 83	java/security/NoSuchAlgorithmException:getMessage	()Ljava/lang/String;
    //   276: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   279: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   282: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   285: athrow
    //   286: astore 7
    //   288: new 63	java/lang/StringBuilder
    //   291: dup
    //   292: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   295: aload_0
    //   296: invokevirtual 68	java/lang/Object:getClass	()Ljava/lang/Class;
    //   299: invokevirtual 74	java/lang/Class:getName	()Ljava/lang/String;
    //   302: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   305: ldc 80
    //   307: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   310: aload 7
    //   312: invokevirtual 98	java/security/InvalidKeyException:getMessage	()Ljava/lang/String;
    //   315: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   318: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   321: invokestatic 90	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   324: new 12	java/lang/Exception
    //   327: dup
    //   328: new 63	java/lang/StringBuilder
    //   331: dup
    //   332: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   335: ldc 100
    //   337: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   340: aload 7
    //   342: invokevirtual 98	java/security/InvalidKeyException:getMessage	()Ljava/lang/String;
    //   345: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   348: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   351: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   354: athrow
    //   355: astore 6
    //   357: new 63	java/lang/StringBuilder
    //   360: dup
    //   361: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   364: aload_0
    //   365: invokevirtual 68	java/lang/Object:getClass	()Ljava/lang/Class;
    //   368: invokevirtual 74	java/lang/Class:getName	()Ljava/lang/String;
    //   371: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   374: ldc 80
    //   376: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   379: aload 6
    //   381: invokevirtual 101	java/security/NoSuchProviderException:getMessage	()Ljava/lang/String;
    //   384: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   387: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   390: invokestatic 90	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   393: new 12	java/lang/Exception
    //   396: dup
    //   397: new 63	java/lang/StringBuilder
    //   400: dup
    //   401: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   404: ldc 103
    //   406: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   409: aload 6
    //   411: invokevirtual 101	java/security/NoSuchProviderException:getMessage	()Ljava/lang/String;
    //   414: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   417: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   420: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   423: athrow
    //   424: astore 5
    //   426: new 63	java/lang/StringBuilder
    //   429: dup
    //   430: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   433: aload_0
    //   434: invokevirtual 68	java/lang/Object:getClass	()Ljava/lang/Class;
    //   437: invokevirtual 74	java/lang/Class:getName	()Ljava/lang/String;
    //   440: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   443: ldc 80
    //   445: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   448: aload 5
    //   450: invokevirtual 104	java/security/SignatureException:getMessage	()Ljava/lang/String;
    //   453: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   456: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   459: invokestatic 90	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   462: new 12	java/lang/Exception
    //   465: dup
    //   466: new 63	java/lang/StringBuilder
    //   469: dup
    //   470: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   473: ldc 106
    //   475: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   478: aload 5
    //   480: invokevirtual 104	java/security/SignatureException:getMessage	()Ljava/lang/String;
    //   483: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   486: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   489: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   492: athrow
    //   493: iinc 18 1
    //   496: goto -446 -> 50
    //   499: new 108	java/util/Random
    //   502: dup
    //   503: invokespecial 109	java/util/Random:<init>	()V
    //   506: astore 19
    //   508: aload 19
    //   510: iload_1
    //   511: i2l
    //   512: invokevirtual 113	java/util/Random:setSeed	(J)V
    //   515: aload 19
    //   517: invokevirtual 117	java/util/Random:nextInt	()I
    //   520: pop
    //   521: aload 19
    //   523: bipush 62
    //   525: invokevirtual 120	java/util/Random:nextInt	(I)I
    //   528: istore 21
    //   530: iconst_1
    //   531: iload 21
    //   533: iload 21
    //   535: iconst_3
    //   536: irem
    //   537: isub
    //   538: iadd
    //   539: istore 22
    //   541: iconst_0
    //   542: ldc 121
    //   544: iload_1
    //   545: iand
    //   546: bipush 8
    //   548: iushr
    //   549: iadd
    //   550: sipush 255
    //   553: irem
    //   554: istore 23
    //   556: iload 23
    //   558: iconst_0
    //   559: iadd
    //   560: sipush 255
    //   563: irem
    //   564: istore 24
    //   566: iload 23
    //   568: ldc 122
    //   570: iload_1
    //   571: iand
    //   572: bipush 16
    //   574: iushr
    //   575: iadd
    //   576: sipush 255
    //   579: irem
    //   580: istore 25
    //   582: iload 24
    //   584: iload 25
    //   586: iadd
    //   587: sipush 255
    //   590: irem
    //   591: istore 26
    //   593: iload 25
    //   595: iload_1
    //   596: sipush 255
    //   599: iand
    //   600: iadd
    //   601: sipush 255
    //   604: irem
    //   605: istore 27
    //   607: iload 26
    //   609: iload 27
    //   611: iadd
    //   612: sipush 255
    //   615: irem
    //   616: istore 28
    //   618: iload 27
    //   620: iload_1
    //   621: ldc 123
    //   623: iand
    //   624: bipush 24
    //   626: iushr
    //   627: iadd
    //   628: sipush 255
    //   631: irem
    //   632: istore 29
    //   634: iload 28
    //   636: iload 29
    //   638: iadd
    //   639: sipush 255
    //   642: irem
    //   643: istore 30
    //   645: iload 29
    //   647: iload 22
    //   649: sipush 255
    //   652: iand
    //   653: iadd
    //   654: sipush 255
    //   657: irem
    //   658: istore 31
    //   660: iload 30
    //   662: iload 31
    //   664: iadd
    //   665: sipush 255
    //   668: irem
    //   669: istore 32
    //   671: iload 31
    //   673: bipush 71
    //   675: iadd
    //   676: sipush 255
    //   679: irem
    //   680: istore 33
    //   682: iload 32
    //   684: iload 33
    //   686: iadd
    //   687: sipush 255
    //   690: irem
    //   691: istore 34
    //   693: ldc 124
    //   695: aload 19
    //   697: invokevirtual 117	java/util/Random:nextInt	()I
    //   700: iand
    //   701: ldc 123
    //   703: iload 33
    //   705: bipush 24
    //   707: ishl
    //   708: iand
    //   709: ior
    //   710: sipush 16128
    //   713: iload 22
    //   715: bipush 8
    //   717: ishl
    //   718: iand
    //   719: ior
    //   720: iload 34
    //   722: sipush 255
    //   725: iand
    //   726: ior
    //   727: ireturn
    //   728: new 12	java/lang/Exception
    //   731: dup
    //   732: ldc 126
    //   734: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   737: athrow
    //   738: astore 13
    //   740: new 63	java/lang/StringBuilder
    //   743: dup
    //   744: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   747: aload_0
    //   748: invokevirtual 68	java/lang/Object:getClass	()Ljava/lang/Class;
    //   751: invokevirtual 74	java/lang/Class:getName	()Ljava/lang/String;
    //   754: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   757: ldc 80
    //   759: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   762: aload 13
    //   764: invokevirtual 127	javax/crypto/NoSuchPaddingException:getMessage	()Ljava/lang/String;
    //   767: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   770: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   773: invokestatic 90	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   776: new 12	java/lang/Exception
    //   779: dup
    //   780: new 63	java/lang/StringBuilder
    //   783: dup
    //   784: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   787: ldc 129
    //   789: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   792: aload 13
    //   794: invokevirtual 127	javax/crypto/NoSuchPaddingException:getMessage	()Ljava/lang/String;
    //   797: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   800: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   803: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   806: athrow
    //   807: astore 12
    //   809: new 63	java/lang/StringBuilder
    //   812: dup
    //   813: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   816: aload_0
    //   817: invokevirtual 68	java/lang/Object:getClass	()Ljava/lang/Class;
    //   820: invokevirtual 74	java/lang/Class:getName	()Ljava/lang/String;
    //   823: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   826: ldc 80
    //   828: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   831: aload 12
    //   833: invokevirtual 98	java/security/InvalidKeyException:getMessage	()Ljava/lang/String;
    //   836: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   839: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   842: invokestatic 90	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   845: new 12	java/lang/Exception
    //   848: dup
    //   849: new 63	java/lang/StringBuilder
    //   852: dup
    //   853: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   856: ldc 131
    //   858: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   861: aload 12
    //   863: invokevirtual 98	java/security/InvalidKeyException:getMessage	()Ljava/lang/String;
    //   866: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   869: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   872: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   875: athrow
    //   876: astore 11
    //   878: new 63	java/lang/StringBuilder
    //   881: dup
    //   882: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   885: aload_0
    //   886: invokevirtual 68	java/lang/Object:getClass	()Ljava/lang/Class;
    //   889: invokevirtual 74	java/lang/Class:getName	()Ljava/lang/String;
    //   892: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   895: ldc 80
    //   897: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   900: aload 11
    //   902: invokevirtual 132	javax/crypto/BadPaddingException:getMessage	()Ljava/lang/String;
    //   905: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   908: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   911: invokestatic 90	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   914: new 12	java/lang/Exception
    //   917: dup
    //   918: new 63	java/lang/StringBuilder
    //   921: dup
    //   922: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   925: ldc 129
    //   927: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   930: aload 11
    //   932: invokevirtual 132	javax/crypto/BadPaddingException:getMessage	()Ljava/lang/String;
    //   935: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   938: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   941: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   944: athrow
    //   945: astore 10
    //   947: new 63	java/lang/StringBuilder
    //   950: dup
    //   951: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   954: aload_0
    //   955: invokevirtual 68	java/lang/Object:getClass	()Ljava/lang/Class;
    //   958: invokevirtual 74	java/lang/Class:getName	()Ljava/lang/String;
    //   961: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   964: ldc 80
    //   966: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   969: aload 10
    //   971: invokevirtual 133	javax/crypto/IllegalBlockSizeException:getMessage	()Ljava/lang/String;
    //   974: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   977: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   980: invokestatic 90	com/widevine/drm/internal/m:a	(Ljava/lang/String;)V
    //   983: new 12	java/lang/Exception
    //   986: dup
    //   987: new 63	java/lang/StringBuilder
    //   990: dup
    //   991: invokespecial 64	java/lang/StringBuilder:<init>	()V
    //   994: ldc 135
    //   996: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   999: aload 10
    //   1001: invokevirtual 133	javax/crypto/IllegalBlockSizeException:getMessage	()Ljava/lang/String;
    //   1004: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1007: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1010: invokespecial 61	java/lang/Exception:<init>	(Ljava/lang/String;)V
    //   1013: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   9	47	79	java/security/NoSuchAlgorithmException
    //   50	79	79	java/security/NoSuchAlgorithmException
    //   499	530	79	java/security/NoSuchAlgorithmException
    //   693	728	79	java/security/NoSuchAlgorithmException
    //   728	738	79	java/security/NoSuchAlgorithmException
    //   0	9	148	java/security/cert/CertificateException
    //   0	9	217	java/security/NoSuchAlgorithmException
    //   0	9	286	java/security/InvalidKeyException
    //   0	9	355	java/security/NoSuchProviderException
    //   0	9	424	java/security/SignatureException
    //   9	47	738	javax/crypto/NoSuchPaddingException
    //   50	79	738	javax/crypto/NoSuchPaddingException
    //   499	530	738	javax/crypto/NoSuchPaddingException
    //   693	728	738	javax/crypto/NoSuchPaddingException
    //   728	738	738	javax/crypto/NoSuchPaddingException
    //   9	47	807	java/security/InvalidKeyException
    //   50	79	807	java/security/InvalidKeyException
    //   499	530	807	java/security/InvalidKeyException
    //   693	728	807	java/security/InvalidKeyException
    //   728	738	807	java/security/InvalidKeyException
    //   9	47	876	javax/crypto/BadPaddingException
    //   50	79	876	javax/crypto/BadPaddingException
    //   499	530	876	javax/crypto/BadPaddingException
    //   693	728	876	javax/crypto/BadPaddingException
    //   728	738	876	javax/crypto/BadPaddingException
    //   9	47	945	javax/crypto/IllegalBlockSizeException
    //   50	79	945	javax/crypto/IllegalBlockSizeException
    //   499	530	945	javax/crypto/IllegalBlockSizeException
    //   693	728	945	javax/crypto/IllegalBlockSizeException
    //   728	738	945	javax/crypto/IllegalBlockSizeException
  }

  private PublicKey a()
    throws Exception
  {
    try
    {
      X509EncodedKeySpec localX509EncodedKeySpec = new X509EncodedKeySpec(b.a("MIIEIjANBgkqhkiG9w0BAQEFAAOCBA8AMIIECgKCBAEAtsUF62b/riY/U947T2A7FHEYuulzEZc3v0HjTuSDphMy3KhHpgFQX23Ui7X99HLFbH0yflOtwsXN4JFUk3C36bjT4MyiGalx4BA9ykzSJ9Ctzu6/n4IsV3eOoaS9wI8QTI7RLGqNAeVnCkqXHmQuf4mXIR/yfyjd45fGhaCTTdreia2Gve+B5jNeaINHNezjBjX428r9TfvM3/FOWIfNKNqYbUsYUMUrIMddC6VaDhUP/j614wxrwHfjn0l85yZCj4Q2Ls8YkIhKVkB5DJshkVOOpG1KxgAouHsEDYCojqQNCDzDl+UBx/68JClmjPCYiC2BUNdXnl5fO5myKkw393ZWr4RvNPPS/n5CmY+Ox7ifeydgedy9lJy9N5OyA52j41lYASWii1R5pimHM04kEXyhPdUvB+pWZy19D+1+tnZkuB6FqolAn922gWIEcoAyBzWOtS+gWMXyg9Kd9v8VGWIELvvJoQQXp2uwYIvudye+HBvGtTNm3cPO4opIc5MswHDcdpsrUhWL+ViyXHgau3iQ6LItujL2ftDX139Js8orEkK+czVsFVZHev+uROzVG0WWSUrTGcIvKHqS+bew88opXOt4fiuCYMt3uQNM+1TNeiG0704y9UnbMiAsJlnr4n4uUF734QyxlILH6bnJkjwfp0FM3xArhMHg0pi2GAoQBzCHe59mCefuQn40QMCYPithI1vAUEt+wb1lK3YIwDlcshhe8ttSsQi2AXd8TqBdvBKGYTZ/ZibWTzx9gGhf3OFEEBlFN9WEeHv54Er9/DuL3FSGYzOYSVg9C0fwH40EtjD4Pq4ol+WDII21MFGgCch3sx7jp+n8WfKt6tQq9jdiKeSRrD6xtHfAB3HP6mCDkU6m+ulwOIcx2xHATSEtXSD9B7Vtl4Z1oVOWoveXSEua2yC+5mziLEEhqn5BSJF0kK2YCXuYITrM0vulFqiFRynQ/PesYcqacFQmI6mpHp2tlxfVfH0Zfd1Lv3xCLZMRoV2AleAoCrcBFtk3QGRE7opQMs/w8WiKg/DfXb4ysPqqt5KLMJnJXvthf/tETxLQlUhrRibIioBE4rVGXozuJBypEjWe1f2Z9UcC3S19/hTh9q7ITH/YxqCGLWkrsJ9IQ4RQD+AOy6tJJrPaopmtAN2p21IrBgDyQPaVDL0vsnZZ8xwGZ4GkRKa5fGBhtFqwWypibT7HKnBknoS4CxlFNAxLYQG5Bmpom5W3rjcAfpcVBbdjBunYiV4k//zn4gEoHiGIMda3GUfcQY8rFt1YiEwF0ewPamD+OvrtxNXRohgP5JeyDBp7QWklFEqv5pWRIdp9s31zhn3qbPDNgD+qcXHEM8ZKpm/K3WOmvWcwmwIDAQAB"));
      PublicKey localPublicKey = KeyFactory.getInstance("RSA").generatePublic(localX509EncodedKeySpec);
      return localPublicKey;
    }
    catch (InvalidKeySpecException localInvalidKeySpecException)
    {
      m.a(getClass().getName() + ", " + localInvalidKeySpecException.getMessage());
      throw new Exception("Root CA parsing errors. " + localInvalidKeySpecException.getMessage());
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      m.a(getClass().getName() + ", " + localNoSuchAlgorithmException.getMessage());
      throw new Exception("Root CA algo errors. " + localNoSuchAlgorithmException.getMessage());
    }
    catch (Exception localException)
    {
      m.a(getClass().getName() + ", " + localException.getMessage());
      throw new Exception("Root CA errors. " + localException.getMessage());
    }
  }

  private X509Certificate a(String paramString)
    throws Exception
  {
    try
    {
      X509Certificate localX509Certificate = (X509Certificate)CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(paramString.getBytes("UTF-8")));
      return localX509Certificate;
    }
    catch (CertificateException localCertificateException)
    {
      m.a(getClass().getName() + ", " + localCertificateException.getMessage());
      throw new Exception("Certificate parsing Error. " + localCertificateException.getMessage());
    }
  }

  private Properties a(ApplicationInfo paramApplicationInfo)
    throws Exception
  {
    try
    {
      Properties localProperties = new Properties();
      JarFile localJarFile = new JarFile(paramApplicationInfo.sourceDir);
      int[] arrayOfInt = { 112, 217, 97, 121, 109, 66, 39, 20, 104, 238, 85, 63, 105, 207, 31, 54, 101, 157, 116, 46, 42, 8, 106, 163, 106, 100, 101, 173, 100, 236, 87, 112, 98, 167, 98, 78, 103, 239, 97, 57, 109, 210 };
      StringBuffer localStringBuffer = new StringBuffer();
      for (int i = 0; i < arrayOfInt.length; i += 2)
        localStringBuffer.append((char)(2 + (arrayOfInt[i] + i % 18)));
      InputStream localInputStream = localJarFile.getInputStream(localJarFile.getEntry(localStringBuffer.toString()));
      localProperties.load(localInputStream);
      localInputStream.close();
      return localProperties;
    }
    catch (ZipException localZipException)
    {
      m.a(getClass().getName() + ", " + localZipException.getMessage());
      throw new Exception("Property retrieval zip file error. " + localZipException.getMessage());
    }
    catch (IOException localIOException)
    {
      m.a(getClass().getName() + ", " + localIOException.getMessage());
      throw new Exception("Property retrieval io error. " + localIOException.getMessage());
    }
  }

  private byte[] a(w paramw, ApplicationInfo paramApplicationInfo)
    throws Exception
  {
    CRC32 localCRC32 = null;
    int i = 0;
    try
    {
      int[] arrayOfInt = { 97, 200, 104, 67, 91, 54, 107, 22, 105, 172, 89, 134, 101, 245, 30, 211, 82, 107, 99, 145, 116, 60 };
      StringBuffer localStringBuffer = new StringBuffer();
      for (int j = 0; j < arrayOfInt.length; j += 2)
        localStringBuffer.append((char)(2 + (arrayOfInt[j] + j % 18)));
      JarFile localJarFile = new JarFile(paramApplicationInfo.sourceDir);
      InputStream localInputStream = localJarFile.getInputStream(localJarFile.getEntry(localStringBuffer.toString()));
      if (paramw == w.b);
      for (MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-256"); ; localMessageDigest = null)
      {
        arrayOfByte1 = new byte[8192];
        do
        {
          k = localInputStream.read(arrayOfByte1);
          i += k;
          if (k > 0)
          {
            if (paramw != w.b)
              break;
            localMessageDigest.update(arrayOfByte1, 0, k);
          }
          new StringBuilder().append("read in len: ").append(Integer.toString(k)).append(", totalLen: ").append(Integer.toString(i)).toString();
        }
        while (k > 0);
        new StringBuilder().append("Total Hash Len: ").append(Integer.toString(i)).toString();
        localInputStream.close();
        localJarFile.close();
        if (paramw != w.b)
          break label613;
        return localMessageDigest.digest();
        if (paramw != w.a)
          break;
        localCRC32 = new CRC32();
      }
      m.a("Unknown hash type: " + Integer.toString(paramw.ordinal()));
      throw new Exception("Unknown hash type: " + Integer.toString(paramw.ordinal()));
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      while (true)
      {
        byte[] arrayOfByte1;
        int k;
        m.a(getClass().getName() + ", " + localNoSuchAlgorithmException.getMessage());
        throw new Exception("Hashing algo error: " + localNoSuchAlgorithmException.getMessage());
        if (paramw == w.a)
          localCRC32.update(arrayOfByte1, 0, k);
      }
    }
    catch (IOException localIOException)
    {
      m.a(getClass().getName() + ", " + localIOException.getMessage());
      throw new Exception("Hashing io error: " + localIOException.getMessage());
    }
    label613: if (paramw == w.a)
    {
      long l = localCRC32.getValue();
      new StringBuilder().append("CRC: ").append(l).toString();
      byte[] arrayOfByte2 = new byte[8];
      arrayOfByte2[0] = ((byte)(int)(l >>> 56));
      arrayOfByte2[1] = ((byte)(int)(l >>> 48));
      arrayOfByte2[2] = ((byte)(int)(l >>> 40));
      arrayOfByte2[3] = ((byte)(int)(l >>> 32));
      arrayOfByte2[4] = ((byte)(int)(l >>> 24));
      arrayOfByte2[5] = ((byte)(int)(l >>> 16));
      arrayOfByte2[6] = ((byte)(int)(l >>> 8));
      arrayOfByte2[7] = ((byte)(int)l);
      return arrayOfByte2;
    }
    throw new Exception("Unexpected hash type: " + Integer.toString(paramw.ordinal()));
  }

  public final int a(int paramInt)
    throws Exception
  {
    Random localRandom = new Random();
    localRandom.setSeed(paramInt);
    int i = localRandom.nextInt(62);
    int j = 1 + (i - i % 5);
    int k = (0 + ((0xFF00 & paramInt) >>> 8)) % 255;
    int m = (k + 0) % 255;
    int n = (k + ((0xFF0000 & paramInt) >>> 16)) % 255;
    int i1 = (m + n) % 255;
    int i2 = (n + (paramInt & 0xFF)) % 255;
    int i3 = (i1 + i2) % 255;
    int i4 = (i2 + ((paramInt & 0xFF000000) >>> 24)) % 255;
    int i5 = (i3 + i4) % 255;
    int i6 = (i4 + (j & 0xFF)) % 255;
    int i7 = (i5 + i6) % 255;
    int i8 = (i6 + 123) % 255;
    int i9 = (i7 + i8) % 255;
    int i10 = 0xFFC000 & localRandom.nextInt() | 0xFF000000 & i8 << 24 | 0x3F00 & j << 8 | i9 & 0xFF;
    while (true)
    {
      String str1;
      int i17;
      int i18;
      try
      {
        int[] arrayOfInt1 = { 113, 88, 100, 77 };
        StringBuffer localStringBuffer1 = new StringBuffer();
        int i11 = 0;
        if (i11 < arrayOfInt1.length)
        {
          localStringBuffer1.append((char)(2 + (arrayOfInt1[i11] + i11 % 18)));
          i11 += 2;
          continue;
        }
        localProcess = Runtime.getRuntime().exec(localStringBuffer1.toString());
        if (localProcess == null)
        {
          m.b("Warning: 0x801");
          throw new Exception("Error: 0x801");
        }
      }
      catch (IOException localIOException)
      {
        Process localProcess;
        new StringBuilder().append(getClass().getName()).append(", ").append(localIOException.getMessage()).toString();
        m.b("Warning: 0x805");
        return i10;
        DataOutputStream localDataOutputStream = new DataOutputStream(localProcess.getOutputStream());
        BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localProcess.getInputStream()));
        int[] arrayOfInt2 = { 99, 23, 95, 187, 98, 210, 103, 171, 22, 74, 24, 246, 66, 29, 49, 135, 66, 8, 70, 73, 6, 55 };
        StringBuffer localStringBuffer2 = new StringBuffer();
        int i12 = 0;
        if (i12 < arrayOfInt2.length)
        {
          localStringBuffer2.append((char)(2 + (arrayOfInt2[i12] + i12 % 18)));
          i12 += 2;
          continue;
        }
        localDataOutputStream.writeBytes(localStringBuffer2.toString());
        localDataOutputStream.flush();
        str1 = localBufferedReader.readLine();
        if (str1 != null)
          break label1604;
        int[] arrayOfInt3 = { 45, 179, 111, 230, 92, 131, 97, 88, 100, 149, 46, 158, 33, 207, 99, 137, 103, 106, 113, 9, 112, 79, 95, 219, 101, 42, 37, 215, 103, 156, 84, 250, 89, 115, 92, 255, 56, 203, 43, 242, 109, 118, 113, 168, 105, 172, 104, 66, 87, 194, 93, 27, 29, 56, 96, 130, 101, 151, 104, 93, 50, 143, 37, 68, 103, 246, 107, 226, 99, 175, 98, 52, 99, 196, 105, 142, 41, 36, 112, 107, 88, 42, 93, 6, 96, 190, 42, 111 };
        StringBuffer localStringBuffer3 = new StringBuffer();
        int i13 = 0;
        if (i13 < arrayOfInt3.length)
        {
          localStringBuffer3.append((char)(2 + (arrayOfInt3[i13] + i13 % 18)));
          i13 += 2;
          continue;
        }
        str2 = localStringBuffer3.toString();
        String[] arrayOfString1 = new String[2];
        int[] arrayOfInt4 = { 113, 10, 113, 200 };
        StringBuffer localStringBuffer4 = new StringBuffer();
        int i14 = 0;
        if (i14 < arrayOfInt4.length)
        {
          localStringBuffer4.append((char)(2 + (arrayOfInt4[i14] + i14 % 18)));
          i14 += 2;
          continue;
        }
        arrayOfString1[0] = localStringBuffer4.toString();
        int[] arrayOfInt5 = { 114, 21, 95, 67, 106, 8, 92, 53, 107, 217, 97, 129, 98, 36 };
        StringBuffer localStringBuffer5 = new StringBuffer();
        int i15 = 0;
        if (i15 < arrayOfInt5.length)
        {
          localStringBuffer5.append((char)(2 + (arrayOfInt5[i15] + i15 % 18)));
          i15 += 2;
          continue;
        }
        arrayOfString1[1] = localStringBuffer5.toString();
        String[] arrayOfString2 = str2.split(":");
        int i16 = arrayOfString2.length;
        i17 = 0;
        if (i17 >= i16)
          continue;
        String str3 = arrayOfString2[i17];
        i18 = 0;
        if (i18 >= arrayOfString1.length)
          break label1598;
        if (new File(str3, arrayOfString1[i18]).exists())
          throw new Exception("Error: " + Integer.toHexString(i18 + 2064));
      }
      catch (SecurityException localSecurityException)
      {
        new StringBuilder().append(getClass().getName()).append(", ").append(localSecurityException.getMessage()).toString();
        m.b("Warning: 0x806");
        return i10;
      }
      i18++;
      continue;
      label1598: i17++;
      continue;
      label1604: String str2 = str1;
    }
  }

  public final int a(int paramInt, ApplicationInfo paramApplicationInfo)
    throws Exception
  {
    int i = 0;
    w localw = w.b;
    byte[] arrayOfByte1 = a(localw, paramApplicationInfo);
    if (arrayOfByte1 == null)
      throw new Exception("Hash computation failed.");
    Properties localProperties = a(paramApplicationInfo);
    if (localProperties == null)
      return 0;
    String str1;
    if (localw == w.a)
    {
      int[] arrayOfInt1 = { 113, 138, 101, 175, 97, 183, 91, 89 };
      StringBuffer localStringBuffer1 = new StringBuffer();
      for (int j = 0; j < arrayOfInt1.length; j += 2)
        localStringBuffer1.append((char)(2 + (arrayOfInt1[j] + j % 18)));
      str1 = localProperties.getProperty(localStringBuffer1.toString());
    }
    while (str1 == null)
    {
      throw new Exception("Signature absent.");
      if (localw == w.b)
      {
        int[] arrayOfInt3 = { 113, 157, 101, 230, 97, 75, 107, 1 };
        StringBuffer localStringBuffer3 = new StringBuffer();
        for (int k = 0; k < arrayOfInt3.length; k += 2)
          localStringBuffer3.append((char)(2 + (arrayOfInt3[k] + k % 18)));
        str1 = localProperties.getProperty(localStringBuffer3.toString());
      }
      else
      {
        m.a("Unrecognized hash type: " + Integer.toString(localw.ordinal()));
        throw new Exception("Unknown hash type.");
      }
    }
    byte[] arrayOfByte2 = b.a(str1);
    if (arrayOfByte2.length == 0)
      throw new Exception("Empty signature decoded.");
    int[] arrayOfInt2 = { 97, 168, 97, 251, 108, 8, 108, 26 };
    StringBuffer localStringBuffer2 = new StringBuffer();
    while (i < arrayOfInt2.length)
    {
      localStringBuffer2.append((char)(2 + (arrayOfInt2[i] + i % 18)));
      i += 2;
    }
    String str2 = localProperties.getProperty(localStringBuffer2.toString());
    if (str2 == null)
      throw new Exception("Certificate absent.");
    return a(paramInt, arrayOfByte1, arrayOfByte2, a(str2));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.v
 * JD-Core Version:    0.6.2
 */