package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVEvent;
import com.widevine.drmapi.android.WVEventListener;
import com.widevine.drmapi.android.WVStatus;
import java.util.HashMap;

public abstract class x extends Thread
{
  protected u n;

  public x(u paramu)
  {
    this.n = paramu;
  }

  protected abstract WVEvent a(t paramt, WVStatus paramWVStatus);

  protected final void a(WVEvent paramWVEvent, WVStatus paramWVStatus, String paramString, HashMap<String, Object> paramHashMap)
  {
    if (paramHashMap == null)
      paramHashMap = new HashMap();
    paramHashMap.put("WVStatusKey", paramWVStatus);
    if ((paramString != null) && (paramString.length() > 0))
      paramHashMap.put("WVErrorKey", paramString);
    if (this.n.d() != null)
      this.n.d().onEvent(paramWVEvent, paramHashMap);
    e();
  }

  protected void a(boolean paramBoolean)
  {
  }

  protected void d()
  {
  }

  protected void e()
  {
  }

  public final int g()
  {
    return this.n.f();
  }

  protected final boolean h()
  {
    return this.n.i();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.x
 * JD-Core Version:    0.6.2
 */