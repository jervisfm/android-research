package com.widevine.drm.internal;

import com.widevine.drmapi.android.WVEvent;
import com.widevine.drmapi.android.WVStatus;

public final class y extends x
{
  public y(u paramu)
  {
    super(paramu);
  }

  protected final WVEvent a(t paramt, WVStatus paramWVStatus)
  {
    return WVEvent.Terminated;
  }

  public final void run()
  {
    a(WVEvent.Terminated, WVStatus.OK, null, null);
    this.n.a();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drm.internal.y
 * JD-Core Version:    0.6.2
 */