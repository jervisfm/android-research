package com.widevine.drmapi.android;

public enum WVEvent
{
  static
  {
    LicenseReceived = new WVEvent("LicenseReceived", 1);
    LicenseRequestFailed = new WVEvent("LicenseRequestFailed", 2);
    Playing = new WVEvent("Playing", 3);
    PlayFailed = new WVEvent("PlayFailed", 4);
    Stopped = new WVEvent("Stopped", 5);
    QueryStatus = new WVEvent("QueryStatus", 6);
    EndOfList = new WVEvent("EndOfList", 7);
    Initialized = new WVEvent("Initialized", 8);
    InitializeFailed = new WVEvent("InitializeFailed", 9);
    Terminated = new WVEvent("Terminated", 10);
    LicenseRemoved = new WVEvent("LicenseRemoved", 11);
    Registered = new WVEvent("Registered", 12);
    Unregistered = new WVEvent("Unregistered", 13);
    SecureStore = new WVEvent("SecureStore", 14);
    WVEvent[] arrayOfWVEvent = new WVEvent[15];
    arrayOfWVEvent[0] = NullEvent;
    arrayOfWVEvent[1] = LicenseReceived;
    arrayOfWVEvent[2] = LicenseRequestFailed;
    arrayOfWVEvent[3] = Playing;
    arrayOfWVEvent[4] = PlayFailed;
    arrayOfWVEvent[5] = Stopped;
    arrayOfWVEvent[6] = QueryStatus;
    arrayOfWVEvent[7] = EndOfList;
    arrayOfWVEvent[8] = Initialized;
    arrayOfWVEvent[9] = InitializeFailed;
    arrayOfWVEvent[10] = Terminated;
    arrayOfWVEvent[11] = LicenseRemoved;
    arrayOfWVEvent[12] = Registered;
    arrayOfWVEvent[13] = Unregistered;
    arrayOfWVEvent[14] = SecureStore;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drmapi.android.WVEvent
 * JD-Core Version:    0.6.2
 */