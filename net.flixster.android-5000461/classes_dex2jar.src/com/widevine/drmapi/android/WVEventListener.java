package com.widevine.drmapi.android;

import java.util.HashMap;

public abstract interface WVEventListener
{
  public abstract WVStatus onEvent(WVEvent paramWVEvent, HashMap<String, Object> paramHashMap);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drmapi.android.WVEventListener
 * JD-Core Version:    0.6.2
 */