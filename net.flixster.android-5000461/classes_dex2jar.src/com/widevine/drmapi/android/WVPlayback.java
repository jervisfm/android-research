package com.widevine.drmapi.android;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Process;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import com.widevine.drm.internal.JNI;
import com.widevine.drm.internal.aa;
import com.widevine.drm.internal.d;
import com.widevine.drm.internal.f;
import com.widevine.drm.internal.j;
import com.widevine.drm.internal.l;
import com.widevine.drm.internal.o;
import com.widevine.drm.internal.p;
import com.widevine.drm.internal.q;
import com.widevine.drm.internal.r;
import com.widevine.drm.internal.s;
import com.widevine.drm.internal.u;
import com.widevine.drm.internal.x;
import com.widevine.drm.internal.y;
import com.widevine.drm.internal.z;
import java.io.File;
import java.util.HashMap;

public class WVPlayback
{
  private l a;
  private String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private u g;

  private WVStatus a(long paramLong1, long paramLong2, long paramLong3, boolean paramBoolean)
  {
    if ((this.g == null) || (!this.g.b()))
      return WVStatus.NotInitialized;
    if ((paramLong1 < 0L) || (paramLong2 < 0L) || (paramLong3 < 0L))
      return WVStatus.NotLicensed;
    s locals = new s(this.g, paramLong1, paramLong2, paramLong3);
    if (paramBoolean)
      locals.start();
    while (true)
    {
      return WVStatus.OK;
      locals.run();
    }
  }

  private WVStatus a(Context paramContext, HashMap<String, Object> paramHashMap, WVEventListener paramWVEventListener, boolean paramBoolean)
  {
    if (this.g != null)
      return WVStatus.AlreadyInitialized;
    this.c = c(((TelephonyManager)paramContext.getSystemService("phone")).getDeviceId());
    new StringBuilder().append("IMEI: ").append(this.c).toString();
    this.b = c(Settings.Secure.getString(paramContext.getContentResolver(), "android_id"));
    new StringBuilder().append("Android ID: ").append(this.b).toString();
    this.d = c(((WifiManager)paramContext.getSystemService("wifi")).getConnectionInfo().getMacAddress());
    new StringBuilder().append("Wifi MAC: ").append(this.d).toString();
    this.f = paramContext.getFilesDir().getAbsolutePath();
    new StringBuilder().append("Default asset DB Path: ").append(this.f).toString();
    a(paramHashMap);
    this.g = new u(paramWVEventListener, paramContext);
    j localj = new j(this.g, b(paramHashMap));
    if (paramHashMap.containsKey("WVInitialMediaplayerConnectionTimeout"))
      this.g.a(1000 * ((Integer)paramHashMap.get("WVInitialMediaplayerConnectionTimeout")).intValue());
    if (paramBoolean)
      localj.start();
    while (true)
    {
      return WVStatus.OK;
      localj.run();
    }
  }

  private WVStatus a(String paramString, boolean paramBoolean)
  {
    if ((this.g == null) || (!this.g.b()))
      return WVStatus.NotInitialized;
    r localr = new r(this.g, paramString);
    if (paramBoolean)
      localr.start();
    while (true)
    {
      return WVStatus.OK;
      localr.run();
    }
  }

  private WVStatus a(boolean paramBoolean)
  {
    if (this.a != null)
    {
      this.a.a(WVStatus.OK, "Terminate command received");
      this.a = null;
    }
    if ((this.g == null) || (!this.g.b()))
      return WVStatus.NotInitialized;
    y localy = new y(this.g);
    if (paramBoolean)
      localy.start();
    while (true)
    {
      this.g = null;
      return WVStatus.OK;
      localy.run();
    }
  }

  private void a(HashMap<String, Object> paramHashMap)
  {
    if (paramHashMap.containsKey("WVAssetRootKey"))
    {
      this.e = ((String)paramHashMap.get("WVAssetRootKey"));
      return;
    }
    this.e = "/sdcard/media/";
  }

  private boolean a(String paramString)
  {
    if (this.g == null)
      return false;
    if (!this.g.b())
    {
      HashMap localHashMap1 = new HashMap();
      localHashMap1.put("WVStatusKey", WVStatus.NotInitialized);
      localHashMap1.put("WVAssetPathKey", paramString);
      localHashMap1.put("WVErrorKey", "Unable to play. Not initialized (wp:p)");
      this.g.d().onEvent(WVEvent.PlayFailed, localHashMap1);
      return false;
    }
    if ((this.a != null) && (this.a.f()))
    {
      HashMap localHashMap2 = new HashMap();
      localHashMap2.put("WVStatusKey", WVStatus.NotPlaying);
      localHashMap2.put("WVAssetPathKey", paramString);
      localHashMap2.put("WVErrorKey", "Unable to play. Previous command still being processed (wp:p)");
      this.g.d().onEvent(WVEvent.PlayFailed, localHashMap2);
      return false;
    }
    return true;
  }

  private WVStatus b(String paramString, boolean paramBoolean)
  {
    if ((this.g == null) || (!this.g.b()))
      return WVStatus.NotInitialized;
    z localz = new z(this.g, paramString);
    if (paramBoolean)
      localz.start();
    while (true)
    {
      return WVStatus.OK;
      localz.run();
    }
  }

  private WVStatus b(boolean paramBoolean)
  {
    if ((this.g == null) || (!this.g.b()))
      return WVStatus.NotInitialized;
    q localq = new q(this.g);
    if (paramBoolean)
      localq.start();
    while (true)
    {
      return WVStatus.OK;
      localq.run();
    }
  }

  private String b(String paramString)
  {
    if ((!paramString.startsWith("http://")) && (!paramString.startsWith("file://")))
    {
      if (((this.e.length() == 0) || (this.e.charAt(-1 + this.e.length()) != '/')) && (paramString.charAt(0) != '/'))
        paramString = this.e + '/' + paramString;
    }
    else
      return paramString;
    return this.e + paramString;
  }

  private HashMap<String, Object> b(HashMap<String, Object> paramHashMap)
  {
    HashMap localHashMap = new HashMap(paramHashMap);
    localHashMap.put("WVAndroidIDKey", this.b);
    localHashMap.put("WVIMEIKey", this.c);
    localHashMap.put("WVWifiMacKey", this.d);
    localHashMap.put("WVHWDeviceKey", c(Build.DEVICE));
    localHashMap.put("WVHWModelKey", c(Build.MODEL));
    localHashMap.put("WVUIDKey", Integer.toString(Process.myUid()));
    if (!localHashMap.containsKey("WVAssetDBPathKey"))
      localHashMap.put("WVAssetDBPathKey", this.f);
    return localHashMap;
  }

  private WVStatus c(String paramString, boolean paramBoolean)
  {
    if ((this.g == null) || (!this.g.b()))
      return WVStatus.NotInitialized;
    p localp = new p(this.g, paramString);
    if (paramBoolean)
      localp.start();
    while (true)
    {
      return WVStatus.OK;
      localp.run();
    }
  }

  private WVStatus c(boolean paramBoolean)
  {
    if ((this.g == null) || (!this.g.b()))
      return WVStatus.NotInitialized;
    o localo = new o(this.g);
    if (paramBoolean)
      localo.start();
    while (true)
    {
      return WVStatus.OK;
      localo.run();
    }
  }

  private static String c(String paramString)
  {
    if (paramString == null)
      return "";
    return paramString.replaceAll("[^a-zA-Z0-9]", "");
  }

  private WVStatus d(String paramString, boolean paramBoolean)
  {
    if ((this.g == null) || (!this.g.b()))
      return WVStatus.NotInitialized;
    s locals = new s(this.g, paramString);
    if (paramBoolean)
      locals.start();
    while (true)
    {
      return WVStatus.OK;
      locals.run();
    }
  }

  public static WVStatus deleteAssetDB(Context paramContext)
  {
    return deleteAssetDB(paramContext.getFilesDir().getAbsolutePath());
  }

  public static WVStatus deleteAssetDB(String paramString)
  {
    if ((paramString == null) || (paramString.length() == 0))
      return WVStatus.FileSystemError;
    JNI.a().b(paramString);
    return WVStatus.OK;
  }

  public WVStatus initialize(Context paramContext, HashMap<String, Object> paramHashMap, WVEventListener paramWVEventListener)
  {
    return a(paramContext, paramHashMap, paramWVEventListener, true);
  }

  public WVStatus initializeSynchronous(Context paramContext, HashMap<String, Object> paramHashMap, WVEventListener paramWVEventListener)
  {
    return a(paramContext, paramHashMap, paramWVEventListener, false);
  }

  public boolean isRooted()
  {
    if ((this.g == null) || (!this.g.b()))
      u.j();
    return JNI.a().d();
  }

  public void logDebugInfo()
  {
  }

  public WVStatus nowOnline()
  {
    return c(true);
  }

  public WVStatus nowOnlineSynchronous()
  {
    return c(false);
  }

  public String play(a parama)
  {
    if (!a("ChunkedRandomAccessFile"))
      return null;
    try
    {
      this.a = new d(this.g, parama);
      return this.a.c();
    }
    catch (aa localaa)
    {
      HashMap localHashMap = new HashMap();
      localHashMap.put("WVStatusKey", WVStatus.NotPlaying);
      localHashMap.put("WVAssetPathKey", "ChunkedRandomAccessFile");
      localHashMap.put("WVErrorKey", localaa.getMessage() + " (wp:p)");
      this.g.d().onEvent(WVEvent.PlayFailed, localHashMap);
    }
    return null;
  }

  public String play(String paramString)
  {
    if (!a(paramString))
      return null;
    String str = b(paramString);
    try
    {
      if (str.startsWith("http://"));
      for (this.a = new f(this.g, str); ; this.a = new d(this.g, str, 0L))
        return this.a.c();
    }
    catch (aa localaa)
    {
      HashMap localHashMap = new HashMap();
      localHashMap.put("WVStatusKey", WVStatus.NotPlaying);
      localHashMap.put("WVAssetPathKey", str);
      localHashMap.put("WVErrorKey", localaa.getMessage() + " (wp:p)");
      this.g.d().onEvent(WVEvent.PlayFailed, localHashMap);
    }
    return null;
  }

  public String play(String paramString, long paramLong)
  {
    if (!a(paramString))
      return null;
    String str = b(paramString);
    try
    {
      if (str.startsWith("http://"));
      for (this.a = new f(this.g, str); ; this.a = new d(this.g, str, paramLong))
        return this.a.c();
    }
    catch (aa localaa)
    {
      HashMap localHashMap = new HashMap();
      localHashMap.put("WVStatusKey", WVStatus.NotPlaying);
      localHashMap.put("WVAssetPathKey", str);
      localHashMap.put("WVErrorKey", localaa.getMessage() + " (wp:p)");
      this.g.d().onEvent(WVEvent.PlayFailed, localHashMap);
    }
    return null;
  }

  public WVStatus queryAssetStatus(String paramString)
  {
    return c(paramString, true);
  }

  public WVStatus queryAssetStatusSynchronous(String paramString)
  {
    return c(paramString, false);
  }

  public WVStatus queryAssetsStatus()
  {
    return b(true);
  }

  public WVStatus queryAssetsStatusSynchronous()
  {
    return b(false);
  }

  public WVStatus registerAsset(String paramString)
  {
    return a(paramString, true);
  }

  public WVStatus registerAssetSynchronous(String paramString)
  {
    return a(paramString, false);
  }

  public WVStatus requestLicense(long paramLong1, long paramLong2, long paramLong3)
  {
    return a(paramLong1, paramLong2, paramLong3, true);
  }

  public WVStatus requestLicense(String paramString)
  {
    return d(paramString, true);
  }

  public WVStatus requestLicenseSynchronous(long paramLong1, long paramLong2, long paramLong3)
  {
    return a(paramLong1, paramLong2, paramLong3, false);
  }

  public WVStatus requestLicenseSynchronous(String paramString)
  {
    return d(paramString, false);
  }

  public String secureRetrieve()
  {
    if (this.g == null);
    HashMap localHashMap;
    do
    {
      return null;
      if (this.g.b())
        break;
      localHashMap = new HashMap();
      localHashMap.put("WVStatusKey", WVStatus.NotInitialized);
      localHashMap.put("WVErrorKey", "Not initialized (wp:sr)");
    }
    while (this.g.d() == null);
    this.g.d().onEvent(WVEvent.SecureStore, localHashMap);
    return null;
    return JNI.a().c();
  }

  public WVStatus secureStore(String paramString)
  {
    if ((this.g == null) || (!this.g.b()))
      return WVStatus.NotInitialized;
    return WVStatus.a(JNI.a().a(paramString));
  }

  public WVStatus setCredentials(HashMap<String, Object> paramHashMap)
  {
    if ((this.g == null) || (!this.g.b()))
      return WVStatus.NotInitialized;
    a(paramHashMap);
    if (paramHashMap.containsKey("WVInitialMediaplayerConnectionTimeout"))
      this.g.a(((Integer)paramHashMap.get("WVInitialMediaplayerConnectionTimeout")).intValue());
    j localj = new j(this.g, b(paramHashMap));
    try
    {
      localj.a();
      return WVStatus.OK;
    }
    catch (aa localaa)
    {
      return localaa.a();
    }
  }

  public WVStatus stop()
  {
    if (this.a == null)
      return WVStatus.NotPlaying;
    this.a.a(WVStatus.OK, "Stop command received");
    this.a = null;
    return WVStatus.OK;
  }

  public WVStatus terminate()
  {
    return a(true);
  }

  public WVStatus terminateSynchronous()
  {
    return a(false);
  }

  public WVStatus unregisterAsset(String paramString)
  {
    return b(paramString, true);
  }

  public WVStatus unregisterAssetSynchronous(String paramString)
  {
    return b(paramString, false);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drmapi.android.WVPlayback
 * JD-Core Version:    0.6.2
 */