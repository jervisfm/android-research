package com.widevine.drmapi.android;

public enum WVStatus
{
  static
  {
    NotInitialized = new WVStatus("NotInitialized", 1);
    AlreadyInitialized = new WVStatus("AlreadyInitialized", 2);
    CantConnectToMediaServer = new WVStatus("CantConnectToMediaServer", 3);
    BadMedia = new WVStatus("BadMedia", 4);
    CantConnectToDrmServer = new WVStatus("CantConnectToDrmServer", 5);
    NotLicensed = new WVStatus("NotLicensed", 6);
    LicenseDenied = new WVStatus("LicenseDenied", 7);
    LostConnection = new WVStatus("LostConnection", 8);
    LicenseExpired = new WVStatus("LicenseExpired", 9);
    AssetExpired = new WVStatus("AssetExpired", 10);
    NotLicensedByRegion = new WVStatus("NotLicensedByRegion", 11);
    LicenseRequestLimitReached = new WVStatus("LicenseRequestLimitReached", 12);
    BadURL = new WVStatus("BadURL", 13);
    FileNotPresent = new WVStatus("FileNotPresent", 14);
    NotRegistered = new WVStatus("NotRegistered", 15);
    AlreadyRegistered = new WVStatus("AlreadyRegistered", 16);
    NotPlaying = new WVStatus("NotPlaying", 17);
    AlreadyPlaying = new WVStatus("AlreadyPlaying", 18);
    FileSystemError = new WVStatus("FileSystemError", 19);
    AssetDBWasCorrupted = new WVStatus("AssetDBWasCorrupted", 20);
    ClockTamperDetected = new WVStatus("ClockTamperDetected", 21);
    MandatorySettingAbsent = new WVStatus("MandatorySettingAbsent", 22);
    SystemCallError = new WVStatus("SystemCallError", 23);
    OutOfMemoryError = new WVStatus("OutOfMemoryError", 24);
    TamperDetected = new WVStatus("TamperDetected", 25);
    PendingServerNotification = new WVStatus("PendingServerNotification", 26);
    HardwareIDAbsent = new WVStatus("HardwareIDAbsent", 27);
    OutOfRange = new WVStatus("OutOfRange", 28);
    HeartbeatError = new WVStatus("HeartbeatError", 29);
    WVStatus[] arrayOfWVStatus = new WVStatus[30];
    arrayOfWVStatus[0] = OK;
    arrayOfWVStatus[1] = NotInitialized;
    arrayOfWVStatus[2] = AlreadyInitialized;
    arrayOfWVStatus[3] = CantConnectToMediaServer;
    arrayOfWVStatus[4] = BadMedia;
    arrayOfWVStatus[5] = CantConnectToDrmServer;
    arrayOfWVStatus[6] = NotLicensed;
    arrayOfWVStatus[7] = LicenseDenied;
    arrayOfWVStatus[8] = LostConnection;
    arrayOfWVStatus[9] = LicenseExpired;
    arrayOfWVStatus[10] = AssetExpired;
    arrayOfWVStatus[11] = NotLicensedByRegion;
    arrayOfWVStatus[12] = LicenseRequestLimitReached;
    arrayOfWVStatus[13] = BadURL;
    arrayOfWVStatus[14] = FileNotPresent;
    arrayOfWVStatus[15] = NotRegistered;
    arrayOfWVStatus[16] = AlreadyRegistered;
    arrayOfWVStatus[17] = NotPlaying;
    arrayOfWVStatus[18] = AlreadyPlaying;
    arrayOfWVStatus[19] = FileSystemError;
    arrayOfWVStatus[20] = AssetDBWasCorrupted;
    arrayOfWVStatus[21] = ClockTamperDetected;
    arrayOfWVStatus[22] = MandatorySettingAbsent;
    arrayOfWVStatus[23] = SystemCallError;
    arrayOfWVStatus[24] = OutOfMemoryError;
    arrayOfWVStatus[25] = TamperDetected;
    arrayOfWVStatus[26] = PendingServerNotification;
    arrayOfWVStatus[27] = HardwareIDAbsent;
    arrayOfWVStatus[28] = OutOfRange;
    arrayOfWVStatus[29] = HeartbeatError;
  }

  public static WVStatus a(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return NotLicensedByRegion;
    case 0:
      return OK;
    case 1:
      return NotInitialized;
    case 2:
      return AlreadyInitialized;
    case 3:
      return CantConnectToMediaServer;
    case 4:
      return BadMedia;
    case 5:
      return CantConnectToDrmServer;
    case 6:
      return NotLicensed;
    case 7:
      return LicenseDenied;
    case 8:
      return LostConnection;
    case 9:
      return LicenseExpired;
    case 10:
      return AssetExpired;
    case 11:
      return NotLicensedByRegion;
    case 12:
      return LicenseRequestLimitReached;
    case 13:
      return BadURL;
    case 14:
      return FileNotPresent;
    case 15:
      return NotRegistered;
    case 16:
      return AlreadyRegistered;
    case 17:
      return NotPlaying;
    case 18:
      return AlreadyPlaying;
    case 19:
      return FileSystemError;
    case 20:
      return AssetDBWasCorrupted;
    case 21:
      return ClockTamperDetected;
    case 22:
      return MandatorySettingAbsent;
    case 23:
      return SystemCallError;
    case 24:
      return OutOfMemoryError;
    case 25:
      return TamperDetected;
    case 26:
      return PendingServerNotification;
    case 27:
      return HardwareIDAbsent;
    case 28:
      return OutOfRange;
    case 29:
    }
    return HeartbeatError;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.widevine.drmapi.android.WVStatus
 * JD-Core Version:    0.6.2
 */