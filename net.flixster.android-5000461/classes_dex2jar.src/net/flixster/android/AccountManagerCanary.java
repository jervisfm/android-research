package net.flixster.android;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

public class AccountManagerCanary
{
  private static Account[] accounts;
  private static String name;

  public static String getFirstAccountName(Context paramContext)
  {
    accounts = AccountManager.get(paramContext).getAccounts();
    int i = accounts.length;
    String str = null;
    if (i > 0)
    {
      name = accounts[0].name;
      accounts = null;
      str = name;
    }
    return str;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.AccountManagerCanary
 * JD-Core Version:    0.6.2
 */