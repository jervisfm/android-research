package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.PhotoDao;
import net.flixster.android.model.Actor;
import net.flixster.android.model.Photo;

public class ActorGalleryPage extends FlixsterActivity
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  private static final int MAX_PHOTOS = 50;
  private Actor actor;
  private AdView mDefaultAd;
  private View.OnClickListener photoClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Photo localPhoto = (Photo)paramAnonymousView.getTag();
      if (localPhoto != null)
      {
        int i = ActorGalleryPage.this.photos.indexOf(localPhoto);
        if (i < 0)
          i = 0;
        Trackers.instance().track("/actor/photo", "Actor Photo Page for photo:" + localPhoto.thumbnailUrl);
        Intent localIntent = new Intent("PHOTO", null, ActorGalleryPage.this.getApplicationContext(), ScrollGalleryPage.class);
        localIntent.putExtra("PHOTO_INDEX", i);
        localIntent.putExtra("ACTOR_ID", ActorGalleryPage.this.actor.id);
        localIntent.putExtra("ACTOR_NAME", ActorGalleryPage.this.actor.name);
        ActorGalleryPage.this.startActivity(localIntent);
      }
    }
  };
  private AdapterView.OnItemClickListener photoItemClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      paramAnonymousView.performClick();
    }
  };
  private ArrayList<Object> photos;
  private Timer timer;
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "ActorGalleryPage.updateHandler");
      if (ActorGalleryPage.this.actor.photos != null)
        ActorGalleryPage.this.updatePage();
      while (ActorGalleryPage.this.isFinishing())
        return;
      ActorGalleryPage.this.showDialog(1);
    }
  };

  private void scheduleUpdatePageTask()
  {
    TimerTask local5 = new TimerTask()
    {
      public void run()
      {
        long l;
        if (ActorGalleryPage.this.actor.photos == null)
          l = ActorGalleryPage.this.actor.id;
        try
        {
          ActorGalleryPage.this.actor.photos = PhotoDao.getActorPhotos(l);
          ActorGalleryPage.this.updateHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          while (true)
          {
            Logger.e("FlxMain", "ActorGalleryPage.scheduleUpdatePageTask:failed to get photos", localDaoException);
            ActorGalleryPage.this.actor.photos = null;
          }
        }
      }
    };
    if (this.timer != null)
      this.timer.schedule(local5, 100L);
  }

  private void updatePage()
  {
    GridView localGridView;
    if (this.actor != null)
    {
      if (this.actor.name != null)
        setActionBarTitle(getString(2131492925) + " - " + this.actor.name);
      if (this.actor.photos != null)
      {
        localGridView = (GridView)findViewById(2131165242);
        if (localGridView.getAdapter() == null)
          this.photos = new ArrayList();
      }
    }
    for (int i = 0; ; i++)
    {
      if ((i >= this.actor.photos.size()) || (i >= 50))
      {
        localGridView.setAdapter(new PhotosListAdapter(this, this.photos, this.photoClickListener));
        localGridView.setOnItemClickListener(this.photoItemClickListener);
        localGridView.setClickable(true);
        Trackers.instance().track("/photo/gallery/actor", "Photo Gallery - Actor - " + this.actor.name);
        return;
      }
      Object localObject = this.actor.photos.get(i);
      this.photos.add(localObject);
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903065);
    createActionBar();
    this.mDefaultAd = ((AdView)findViewById(2131165226));
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.actor = new Actor();
      this.actor.id = localBundle.getLong("ACTOR_ID");
      this.actor.name = localBundle.getString("ACTOR_NAME");
    }
    updatePage();
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        ActorGalleryPage.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder.create();
  }

  protected void onDestroy()
  {
    super.onDestroy();
    if (this.timer != null)
    {
      this.timer.cancel();
      this.timer.purge();
    }
    this.timer = null;
  }

  protected void onResume()
  {
    super.onResume();
    if (this.timer == null)
      this.timer = new Timer();
    if (this.actor != null)
      this.actor.photos = null;
    scheduleUpdatePageTask();
    this.mDefaultAd.refreshAds();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ActorGalleryPage
 * JD-Core Version:    0.6.2
 */