package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import java.util.Date;
import net.flixster.android.ads.AdManager;

@Deprecated
public class AdAdminPage extends DecoratedSherlockActivity
  implements View.OnClickListener
{
  private static final int DIALOG_DOUBLECLICK_TEST = 1001;
  private CheckBox disableLaunchCap;
  private CheckBox disablePrerollCap;
  private Button mDoubleClickTestButton;
  private TextView mPosixInstallStamp;

  private void setDelaySubheader()
  {
    double d = (FlixsterApplication.sToday.getTime() - FlixsterApplication.sInstallMsPosixTime) / 86400000.0D;
    TextView localTextView = this.mPosixInstallStamp;
    StringBuilder localStringBuilder = new StringBuilder("Days Installed: ");
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Double.valueOf(d);
    localTextView.setText(String.format("%2.2f", arrayOfObject));
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    case 2131165247:
    case 2131165249:
    default:
      return;
    case 2131165248:
      showDialog(1001);
      return;
    case 2131165245:
      FlixsterApplication.setPosixInstallTime(FlixsterApplication.sInstallMsPosixTime - 259200000L);
      setDelaySubheader();
      return;
    case 2131165246:
      FlixsterApplication.setPosixInstallTime(43200000L + FlixsterApplication.sInstallMsPosixTime);
      setDelaySubheader();
      return;
    case 2131165250:
      FlixsterApplication.setAdAdminDisableLaunchCap(((CheckBox)paramView).isChecked());
      return;
    case 2131165251:
    }
    FlixsterApplication.setAdAdminDisablePrerollCap(((CheckBox)paramView).isChecked());
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903066);
    createActionBar();
    setActionBarTitle("Ads Admin Page");
    this.mDoubleClickTestButton = ((Button)findViewById(2131165248));
    this.mDoubleClickTestButton.setOnClickListener(this);
    this.mDoubleClickTestButton.setText(FlixsterApplication.getAdAdminDoubleClickTest());
    ((Button)findViewById(2131165245)).setOnClickListener(this);
    ((Button)findViewById(2131165246)).setOnClickListener(this);
    this.mPosixInstallStamp = ((TextView)findViewById(2131165244));
    setDelaySubheader();
    ((TextView)findViewById(2131165252)).setText(AdManager.instance().getPayloadDump());
    this.disableLaunchCap = ((CheckBox)findViewById(2131165250));
    this.disableLaunchCap.setChecked(FlixsterApplication.isAdAdminLaunchCapDisabled());
    this.disableLaunchCap.setOnClickListener(this);
    this.disablePrerollCap = ((CheckBox)findViewById(2131165251));
    this.disablePrerollCap.setChecked(FlixsterApplication.isAdAdminPrerollCapDisabled());
    this.disablePrerollCap.setOnClickListener(this);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1001:
    }
    final View localView = LayoutInflater.from(this).inflate(2130903075, null);
    localView.setMinimumWidth(300);
    return new AlertDialog.Builder(this).setView(localView).setPositiveButton(2131492937, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        EditText localEditText = (EditText)localView.findViewById(2131165291);
        FlixsterApplication.setAdAdminDoubleclickTest(localEditText.getText().toString());
        AdAdminPage.this.mDoubleClickTestButton.setText(localEditText.getText().toString());
      }
    }).setNegativeButton(2131492938, null).create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    populatePage();
  }

  void populatePage()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.AdAdminPage
 * JD-Core Version:    0.6.2
 */