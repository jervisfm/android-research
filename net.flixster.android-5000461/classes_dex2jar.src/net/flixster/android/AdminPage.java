package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.bootstrap.BootstrapActivity;
import com.flixster.android.utils.Logger;
import java.util.Date;
import net.flixster.android.ads.AdManager;

public class AdminPage extends DecoratedSherlockActivity
  implements View.OnClickListener
{
  private static final int DIALOG_DOUBLECLICK_TEST = 1001;
  private RadioButton mApiSourceProduction;
  private RadioButton mApiSourceQaPreview;
  private RadioButton mApiSourceQaTrunk;
  private RadioButton mApiSourceStaging;
  private Button mDoubleClickTestButton;
  private TextView mPosixInstallStamp;

  private void populatePage()
  {
    switch (FlixsterApplication.getAdminApiSource())
    {
    default:
      return;
    case 0:
      this.mApiSourceProduction.setChecked(true);
      return;
    case 1:
      this.mApiSourceStaging.setChecked(true);
      return;
    case 2:
      this.mApiSourceQaTrunk.setChecked(true);
      return;
    case 3:
    }
    this.mApiSourceQaPreview.setChecked(true);
  }

  private void setDelaySubheader()
  {
    double d = (FlixsterApplication.sToday.getTime() - FlixsterApplication.sInstallMsPosixTime) / 86400000.0D;
    TextView localTextView = this.mPosixInstallStamp;
    StringBuilder localStringBuilder = new StringBuilder("Days Installed: ");
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Double.valueOf(d);
    localTextView.setText(String.format("%2.2f", arrayOfObject));
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    case 2131165247:
    case 2131165249:
    case 2131165252:
    case 2131165255:
    case 2131165260:
    default:
      return;
    case 2131165256:
      Logger.d("FlxMain", "AdminPage.onClick setAdminState:0");
      FlixsterApplication.setAdminApiSource(0);
      return;
    case 2131165257:
      Logger.d("FlxMain", "AdminPage.onClick setAdminState:1");
      FlixsterApplication.setAdminApiSource(1);
      return;
    case 2131165258:
      Logger.d("FlxMain", "AdminPage.onClick setAdminState:2");
      FlixsterApplication.setAdminApiSource(2);
      return;
    case 2131165259:
      Logger.d("FlxMain", "AdminPage.onClick setAdminState:3");
      FlixsterApplication.setAdminApiSource(3);
      return;
    case 2131165253:
      startActivity(new Intent("ADADMIN", null, this, AdAdminPage.class));
      return;
    case 2131165254:
      NavUtils.navigateUpTo(this, BootstrapActivity.getMainIntent(this));
      Starter.forceClose();
      return;
    case 2131165248:
      showDialog(1001);
      return;
    case 2131165245:
      FlixsterApplication.setPosixInstallTime(FlixsterApplication.sInstallMsPosixTime - 259200000L);
      setDelaySubheader();
      return;
    case 2131165246:
      FlixsterApplication.setPosixInstallTime(43200000L + FlixsterApplication.sInstallMsPosixTime);
      setDelaySubheader();
      return;
    case 2131165250:
      FlixsterApplication.setAdAdminDisableLaunchCap(((CheckBox)paramView).isChecked());
      return;
    case 2131165251:
      FlixsterApplication.setAdAdminDisablePrerollCap(((CheckBox)paramView).isChecked());
      return;
    case 2131165261:
    }
    FlixsterApplication.setAdAdminDisablePostitialCap(((CheckBox)paramView).isChecked());
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903067);
    createActionBar();
    setActionBarTitle("Admin Page");
    this.mApiSourceProduction = ((RadioButton)findViewById(2131165256));
    this.mApiSourceProduction.setOnClickListener(this);
    this.mApiSourceStaging = ((RadioButton)findViewById(2131165257));
    this.mApiSourceStaging.setOnClickListener(this);
    this.mApiSourceQaTrunk = ((RadioButton)findViewById(2131165258));
    this.mApiSourceQaTrunk.setOnClickListener(this);
    this.mApiSourceQaPreview = ((RadioButton)findViewById(2131165259));
    this.mApiSourceQaPreview.setOnClickListener(this);
    ((Button)findViewById(2131165253)).setOnClickListener(this);
    ((Button)findViewById(2131165254)).setOnClickListener(this);
    this.mDoubleClickTestButton = ((Button)findViewById(2131165248));
    this.mDoubleClickTestButton.setOnClickListener(this);
    this.mDoubleClickTestButton.setText(FlixsterApplication.getAdAdminDoubleClickTest());
    ((Button)findViewById(2131165245)).setOnClickListener(this);
    ((Button)findViewById(2131165246)).setOnClickListener(this);
    this.mPosixInstallStamp = ((TextView)findViewById(2131165244));
    setDelaySubheader();
    ((TextView)findViewById(2131165252)).setText(AdManager.instance().getPayloadDump());
    CheckBox localCheckBox1 = (CheckBox)findViewById(2131165250);
    localCheckBox1.setChecked(FlixsterApplication.isAdAdminLaunchCapDisabled());
    localCheckBox1.setOnClickListener(this);
    CheckBox localCheckBox2 = (CheckBox)findViewById(2131165251);
    localCheckBox2.setChecked(FlixsterApplication.isAdAdminPrerollCapDisabled());
    localCheckBox2.setOnClickListener(this);
    CheckBox localCheckBox3 = (CheckBox)findViewById(2131165261);
    localCheckBox3.setChecked(FlixsterApplication.isAdAdminPostitialCapDisabled());
    localCheckBox3.setOnClickListener(this);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1001:
    }
    final View localView = LayoutInflater.from(this).inflate(2130903075, null);
    localView.setMinimumWidth(300);
    return new AlertDialog.Builder(this).setView(localView).setPositiveButton(2131492937, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        EditText localEditText = (EditText)localView.findViewById(2131165291);
        FlixsterApplication.setAdAdminDoubleclickTest(localEditText.getText().toString());
        AdminPage.this.mDoubleClickTestButton.setText(localEditText.getText().toString());
      }
    }).setNegativeButton(2131492938, null).create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    populatePage();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.AdminPage
 * JD-Core Version:    0.6.2
 */