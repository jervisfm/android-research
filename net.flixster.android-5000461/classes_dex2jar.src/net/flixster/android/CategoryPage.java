package net.flixster.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ListView;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.lvi.LviAd;
import net.flixster.android.lvi.LviFooter;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.model.Movie;

public class CategoryPage extends LviActivity
{
  public static final String CATEGORY_FILTER = "CATEGORY_FILTER";
  public static final String CATEGORY_TITLE = "CATEGORY_TITLE";
  private final ArrayList<Movie> mCategory = new ArrayList();
  private String mCategoryFilter = "&filter=flixster-top-100";
  private String mCategoryTitle = "Flixster Top 100";

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      this.throbberHandler.sendEmptyMessage(1);
      TimerTask local1 = new TimerTask()
      {
        public void run()
        {
          try
          {
            ArrayList localArrayList;
            String str;
            if (CategoryPage.this.mCategory.isEmpty())
            {
              localArrayList = CategoryPage.this.mCategory;
              str = CategoryPage.this.mCategoryFilter;
              if (CategoryPage.this.mRetryCount != 0)
                break label84;
            }
            label84: for (boolean bool2 = true; ; bool2 = false)
            {
              MovieDao.fetchDvdCategory(localArrayList, str, bool2);
              boolean bool1 = CategoryPage.this.shouldSkipBackgroundTask(this.val$currResumeCtr);
              if (!bool1)
                break;
              return;
            }
            CategoryPage.this.setMovieLviList();
            CategoryPage.this.mUpdateHandler.sendEmptyMessage(0);
            return;
          }
          catch (DaoException localDaoException)
          {
            Logger.w("FlxMain", "CategoryPage.ScheduleLoadItemsTask.run() mRetryCount:" + CategoryPage.this.mRetryCount);
            localDaoException.printStackTrace();
            CategoryPage localCategoryPage = CategoryPage.this;
            localCategoryPage.mRetryCount = (1 + localCategoryPage.mRetryCount);
            if (CategoryPage.this.mRetryCount < 3)
              CategoryPage.this.ScheduleLoadItemsTask(1000L);
            while (true)
            {
              return;
              CategoryPage.this.mRetryCount = 0;
              CategoryPage.this.mShowDialogHandler.sendEmptyMessage(2);
            }
          }
          finally
          {
            CategoryPage.this.throbberHandler.sendEmptyMessage(0);
          }
        }
      };
      Logger.d("FlxMain", "CategoryPage.ScheduleLoadItemsTask() loading item");
      if (this.mPageTimer != null)
        this.mPageTimer.schedule(local1, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void setMovieLviList()
  {
    Logger.d("FlxMain", "CategoryPage.setPopularLviList ");
    this.mDataHolder.clear();
    destroyExistingLviAd();
    this.lviAd = new LviAd();
    this.lviAd.mAdSlot = "Category";
    this.mDataHolder.add(this.lviAd);
    Iterator localIterator = this.mCategory.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        LviFooter localLviFooter = new LviFooter();
        this.mDataHolder.add(localLviFooter);
        return;
      }
      Movie localMovie = (Movie)localIterator.next();
      LviMovie localLviMovie = new LviMovie();
      localLviMovie.mMovie = localMovie;
      localLviMovie.mTrailerClick = getTrailerOnClickListener();
      this.mDataHolder.add(localLviMovie);
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    createActionBar();
    this.mListView.setOnItemClickListener(getMovieItemClickListener());
    this.mStickyTopAd.setSlot("CategoryStickyTop");
    this.mStickyBottomAd.setSlot("CategoryStickyBottom");
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.mCategoryFilter = localBundle.getString("CATEGORY_FILTER");
      this.mCategoryTitle = localBundle.getString("CATEGORY_TITLE");
      setActionBarTitle(this.mCategoryTitle);
    }
  }

  public void onResume()
  {
    super.onResume();
    Trackers.instance().track("/dvds/browse/genre", "Category " + this.mCategoryTitle);
    ScheduleLoadItemsTask(100L);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.CategoryPage
 * JD-Core Version:    0.6.2
 */