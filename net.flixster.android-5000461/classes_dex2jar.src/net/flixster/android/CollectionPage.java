package net.flixster.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.flixster.android.activity.NonTabbedActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.data.AccountManager;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackLogic;
import com.flixster.android.net.DownloadHelper;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.view.UvFooter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.User;

public class CollectionPage extends NonTabbedActivity
{
  private static volatile Thread sProgressMonitorThread;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      CollectionPage.this.throbber.setVisibility(8);
      CollectionPage.this.findViewById(2131165458).setVisibility(0);
      if ((paramAnonymousMessage.obj instanceof DaoException))
        ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, CollectionPage.this);
    }
  };
  private GridView gridView;
  private List<LockerRight> movieAndSeasonRights;
  private final AdapterView.OnItemClickListener movieDetailClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      LockerRight localLockerRight = (LockerRight)CollectionPage.this.movieAndSeasonRights.get(paramAnonymousInt);
      if (localLockerRight.isSeason())
      {
        Starter.launchSeasonDetail(localLockerRight.assetId, localLockerRight.rightId, CollectionPage.this);
        return;
      }
      Starter.launchMovieDetail(localLockerRight.assetId, localLockerRight.rightId, CollectionPage.this);
    }
  };
  private LinearLayout scrollLayout;
  private final Handler successHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      CollectionPage.this.throbber.setVisibility(8);
      View localView = CollectionPage.this.findViewById(2131165454);
      int i;
      int j;
      int k;
      int m;
      Iterator localIterator;
      label73: TextView localTextView;
      ImageView localImageView;
      String str;
      if (FlixsterApplication.isConnected())
      {
        i = 8;
        localView.setVisibility(i);
        j = 0;
        k = 0;
        m = 0;
        CollectionPage.this.movieAndSeasonRights.clear();
        localIterator = AccountManager.instance().getUser().getLockerRights().iterator();
        if (localIterator.hasNext())
          break label343;
        CollectionPage.this.gridView.setAdapter(new LockerRightGridViewAdapter(CollectionPage.this, CollectionPage.this.movieAndSeasonRights));
        CollectionPage.this.gridView.setClickable(true);
        CollectionPage.this.gridView.setOnItemClickListener(CollectionPage.this.movieDetailClickListener);
        localTextView = (TextView)CollectionPage.this.findViewById(2131165456);
        localImageView = (ImageView)CollectionPage.this.findViewById(2131165457);
        if (m <= 0)
          break label505;
        if (m != 1)
          break label470;
        str = CollectionPage.this.getString(2131492979);
        label189: localTextView.setText(str);
        localTextView.setOnClickListener(CollectionPage.this.unfulfillablePanelClickListener);
        localTextView.setVisibility(0);
        localImageView.setVisibility(0);
      }
      while (true)
      {
        int n = CollectionPage.this.scrollLayout.getChildCount();
        if (n > 5)
          CollectionPage.this.scrollLayout.removeViews(5, n - 5);
        if (FlixsterApplication.isConnected())
          CollectionPage.this.scrollLayout.addView(new UvFooter(CollectionPage.this));
        if (Drm.logic().isDeviceDownloadCompatible())
        {
          CollectionPage.sProgressMonitorThread = new Thread(new Runnable()
          {
            public void run()
            {
              Thread localThread = Thread.currentThread();
              while (true)
              {
                if (CollectionPage.sProgressMonitorThread != localThread)
                  return;
                try
                {
                  Thread.sleep(2000L);
                  for (int i = 0; i < CollectionPage.this.gridView.getChildCount(); i++)
                  {
                    Handler localHandler = ((MovieCollectionItem)CollectionPage.this.gridView.getChildAt(i)).progressHandler;
                    localHandler.sendMessage(localHandler.obtainMessage());
                  }
                }
                catch (InterruptedException localInterruptedException)
                {
                  localInterruptedException.printStackTrace();
                }
              }
            }
          });
          CollectionPage.sProgressMonitorThread.start();
        }
        if ((!FlixsterApplication.isConnected()) && (!FlixsterApplication.hasMigratedToRights()))
          CollectionPage.this.showDialog(1000000207, null);
        return;
        i = 0;
        break;
        label343: LockerRight localLockerRight = (LockerRight)localIterator.next();
        if (((localLockerRight.isMovie()) && ((localLockerRight.isStreamingSupported) || (localLockerRight.isDownloadSupported))) || (localLockerRight.isSeason()))
        {
          if ((!FlixsterApplication.isConnected()) && (!DownloadHelper.isMovieDownloadInProgress(localLockerRight)) && (DownloadHelper.isDownloaded(localLockerRight)))
          {
            CollectionPage.this.movieAndSeasonRights.add(k, localLockerRight);
            k++;
            j++;
            break label73;
          }
          CollectionPage.this.movieAndSeasonRights.add(j, localLockerRight);
          j++;
          break label73;
        }
        if (!localLockerRight.isUnfulfillable())
          break label73;
        m++;
        break label73;
        label470: CollectionPage localCollectionPage = CollectionPage.this;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(m);
        str = localCollectionPage.getString(2131492980, arrayOfObject);
        break label189;
        label505: localTextView.setVisibility(8);
        localImageView.setVisibility(8);
      }
    }
  };
  private ProgressBar throbber;
  private final View.OnClickListener unfulfillablePanelClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Intent localIntent = new Intent(CollectionPage.this, UnfulfillablePage.class);
      CollectionPage.this.startActivity(localIntent);
    }
  };

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903119);
    this.scrollLayout = ((LinearLayout)findViewById(2131165299));
    this.gridView = ((GridView)findViewById(2131165455));
    this.throbber = ((ProgressBar)findViewById(2131165241));
    this.movieAndSeasonRights = new ArrayList();
  }

  public void onPause()
  {
    super.onPause();
    sProgressMonitorThread = null;
  }

  protected void onResume()
  {
    super.onResume();
    User localUser = AccountFacade.getSocialUser();
    if (localUser != null)
    {
      if (!localUser.isLockerRightsFetched())
        this.throbber.setVisibility(0);
      ProfileDao.getUserLockerRights(this.successHandler, this.errorHandler);
    }
    Trackers.instance().track("/mymovies/mycollection", "My Movies - My Collection");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.CollectionPage
 * JD-Core Version:    0.6.2
 */