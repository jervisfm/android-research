package net.flixster.android;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;

public class ConnectRatePage extends FlixsterActivity
  implements View.OnClickListener
{
  protected static final int REQUESTCODE_FRIENDPROMT = 2;
  protected static final int REQUESTCODE_RATEPROMT = 1;
  protected static final int REQUESTCODE_WTSPROMT = 3;
  int mRequestCode = 1;

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Logger.d("FlxMain", "ConnectRatePage requestCode:" + paramInt1 + " resultCode:" + paramInt2);
    if ((paramInt2 == -1) && (paramInt1 == 123))
    {
      setResult(-1);
      finish();
    }
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
      return;
    case 2131165272:
      startActivityForResult(new Intent(this, FacebookAuth.class), 123);
      return;
    case 2131165273:
    }
    startActivityForResult(new Intent(this, FlixsterLoginPage.class), 123);
  }

  public void onCreate(Bundle paramBundle)
  {
    Logger.d("FlxMain", "ConnectRatePage.onCreate");
    super.onCreate(paramBundle);
    setContentView(2130903070);
    createActionBar();
    setActionBarTitle(2131492965);
    ((RelativeLayout)findViewById(2131165266)).getBackground().setDither(true);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      Integer localInteger = Integer.valueOf(localBundle.getInt("net.flixster.RequestCode"));
      if (localInteger != null)
        this.mRequestCode = localInteger.intValue();
    }
    TextView localTextView1 = (TextView)findViewById(2131165267);
    TextView localTextView2 = (TextView)findViewById(2131165269);
    TextView localTextView3 = (TextView)findViewById(2131165270);
    TextView localTextView4 = (TextView)findViewById(2131165271);
    ImageView localImageView = (ImageView)findViewById(2131165268);
    switch (this.mRequestCode)
    {
    case 1:
    default:
    case 3:
    case 2:
    }
    while (true)
    {
      ((ImageButton)findViewById(2131165272)).setOnClickListener(this);
      ((TextView)findViewById(2131165273)).setOnClickListener(this);
      return;
      localImageView.setImageResource(2130837884);
      localTextView1.setText(2131493033);
      localTextView2.setBackgroundResource(2130837633);
      localTextView2.setText(2131492999);
      localTextView2.setPadding(12, 0, 0, 0);
      localTextView3.setBackgroundResource(2130837634);
      localTextView3.setText(2131493000);
      localTextView3.setPadding(12, 0, 0, 0);
      localTextView4.setBackgroundResource(2130837635);
      localTextView4.setText(2131493001);
      localTextView4.setPadding(12, 0, 0, 0);
      continue;
      localImageView.setImageResource(2130837883);
      localTextView1.setText(2131493034);
      localTextView2.setBackgroundResource(2130837604);
      localTextView2.setText(2131492998);
      localTextView2.setPadding(20, 0, 0, 0);
      ((RelativeLayout.LayoutParams)localTextView2.getLayoutParams()).topMargin = 30;
      localTextView3.setBackgroundResource(2130837603);
      localTextView3.setText(2131492970);
      localTextView3.setPadding(20, 0, 0, 0);
      ((RelativeLayout.LayoutParams)localTextView3.getLayoutParams()).topMargin = 15;
      localTextView4.setVisibility(4);
      localTextView4.setHeight(1);
      localTextView4.setMaxHeight(1);
      localTextView4.setPadding(0, 0, 0, 0);
      ((RelativeLayout.LayoutParams)localTextView4.getLayoutParams()).topMargin = 0;
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  public void onResume()
  {
    Logger.d("FlxMain", "ConnectRatePage.onResume");
    super.onResume();
    switch (this.mRequestCode)
    {
    default:
      return;
    case 1:
      Trackers.instance().track("/mymovies/promo/rating", "Promo - Rating");
      return;
    case 3:
      Trackers.instance().track("/mymovies/promo/wanttosee", "Promo - WTS");
      return;
    case 2:
    }
    Trackers.instance().track("/mymovies/promo/friendsrating", "Promo - Friends");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ConnectRatePage
 * JD-Core Version:    0.6.2
 */