package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import com.actionbarsherlock.view.Menu;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.SessionEvents;
import com.facebook.android.Util;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.data.AccountManager;
import com.flixster.android.msk.MskController;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder;
import com.flixster.android.view.DialogBuilder.DialogListener;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.User;
import org.json.JSONException;
import org.json.JSONObject;

public class FacebookAuth extends DecoratedSherlockActivity
{
  private static final int DIALOGKEY_FB_CANCELED = 8;
  private static final int DIALOGKEY_FB_CONNECTED = 1;
  private static final int DIALOGKEY_FB_DENIED = 7;
  private static final int DIALOGKEY_FB_ERROR = 3;
  private static final int DIALOGKEY_FB_FUBAR = 6;
  private static final String[] GTV_PERMISSIONS = { "offline_access" };
  public static final String KEY_SHOW_CONNECTED_DIALOG = "KEY_SHOW_CONNECTED_DIALOG";
  private static final String[] PERMISSIONS = { "publish_actions", "email", "offline_access" };
  public static final int REQUESTCODE_CONNECT = 123;
  private Handler mFacebookConnectedDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "FacebookAuth  mFlixsterConnectedHandler yay... connect ");
      if (!FacebookAuth.this.isFinishing())
      {
        if (MskController.isRequestCodeValid(FacebookAuth.this.requestCode))
          MskController.instance().trackFbLoginSuccess();
        if (FacebookAuth.this.showConnectedDialog)
          FacebookAuth.this.showDialog(1);
      }
      else
      {
        return;
      }
      FlixsterApplication.sFacebook.release();
      FacebookAuth.this.setResult(-1);
      FacebookAuth.this.finish();
    }
  };
  private Handler mFacebookErrorDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "FacebookAuth  DIALOGKEY_FB_ERROR");
      if (!FacebookAuth.this.isFinishing())
        FacebookAuth.this.showDialog(3);
    }
  };
  private Handler mFacebookFubarDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "FacebookAuth  DIALOGKEY_FB_FUBAR");
      if (!FacebookAuth.this.isFinishing())
        FacebookAuth.this.showDialog(6);
    }
  };
  private Bundle mSavedInstanceState;
  private User mUser;
  private int requestCode;
  private boolean showConnectedDialog = true;
  private final DialogBuilder.DialogListener tosDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
      FacebookAuth.this.finish();
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      FlixsterApplication.setTosAccepted(true);
      Logger.d("FlxMain", "FacebookAuth.onCreate pre .authorize");
      Facebook localFacebook = FlixsterApplication.sFacebook;
      FacebookAuth localFacebookAuth = FacebookAuth.this;
      if (Properties.instance().isGoogleTv());
      for (String[] arrayOfString = FacebookAuth.GTV_PERMISSIONS; ; arrayOfString = FacebookAuth.PERMISSIONS)
      {
        localFacebook.authorize(localFacebookAuth, arrayOfString, new FacebookAuth.LoginDialogListener(FacebookAuth.this, null));
        Logger.d("FlxMain", "FacebookAuth.onCreate post .authorize");
        return;
      }
    }
  };

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Logger.d("FlxMain", "FacebookAuth.onActivityResult pre .authorizeCallback requestCode:" + paramInt1);
    FlixsterApplication.sFacebook.authorizeCallback(paramInt1, paramInt2, paramIntent);
    Logger.d("FlxMain", "FacebookAuth.onActivityResult post .authorizeCallback ");
    if (FlixsterApplication.sFacebook.isSessionValid())
    {
      Logger.d("FlxMain", "FacebookAuth.onActivityResult session valid");
      return;
    }
    Logger.w("FlxMain", "FacebookAuth.onActivityResult session not valid");
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    Logger.d("FlxMain", "FacebookAuth.onConfigurationChanged");
    FlixsterApplication.sFacebook.dismissDialog();
    onCreate(this.mSavedInstanceState);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mSavedInstanceState = paramBundle;
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.showConnectedDialog = localBundle.getBoolean("KEY_SHOW_CONNECTED_DIALOG", this.showConnectedDialog);
      this.requestCode = localBundle.getInt("MskEntryActivity.REQUEST_CODE");
    }
    setContentView(2130903068);
    createActionBar();
    setActionBarTitle(2131492965);
    if (FlixsterApplication.hasAcceptedTos())
      this.tosDialogListener.onPositiveButtonClick(0);
    while (true)
    {
      if (MskController.isRequestCodeValid(this.requestCode))
        MskController.instance().trackFbLoginAttempt();
      return;
      DialogBuilder.showTermsOfService(this, this.tosDialogListener);
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    case 2:
    case 4:
    case 5:
    default:
      Logger.e("FlixsterCache", "onCreateDialog() default:fell through dialog switch");
      return null;
    case 1:
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(this).setIcon(null).setMessage(getResources().getString(2131493096));
      String str = getResources().getString(2131493169);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = this.mUser.firstName;
      return localBuilder.setTitle(String.format(str, arrayOfObject)).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          FlixsterApplication.sFacebook.release();
          FacebookAuth.this.setResult(-1);
          FacebookAuth.this.finish();
        }
      }).create();
    case 7:
      return new AlertDialog.Builder(this).setIcon(null).setMessage(getResources().getString(2131493098)).setTitle(getResources().getString(2131493097)).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          FlixsterApplication.sFacebook.release();
          FacebookAuth.this.setResult(0);
          FacebookAuth.this.finish();
        }
      }).create();
    case 6:
      return new AlertDialog.Builder(this).setIcon(null).setMessage("We were unable to log you in to Facebook.").setTitle("Connection Error").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          FlixsterApplication.sFacebook.release();
          FacebookAuth.this.setResult(0);
          FacebookAuth.this.finish();
        }
      }).create();
    case 3:
      return new AlertDialog.Builder(this).setIcon(null).setMessage(getResources().getString(2131493100)).setTitle(getResources().getString(2131493099)).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          FlixsterApplication.sFacebook.release();
          FacebookAuth.this.setResult(0);
          FacebookAuth.this.finish();
        }
      }).create();
    case 8:
    }
    return new AlertDialog.Builder(this).setIcon(null).setMessage(getResources().getString(2131493095)).setTitle("Facebook Canceled").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        FlixsterApplication.sFacebook.release();
        FacebookAuth.this.setResult(0);
        FacebookAuth.this.finish();
      }
    }).create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      setResult(0);
      finish();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onPause()
  {
    super.onPause();
    Logger.d("FlxMain", "FacebookAuth.onPause");
  }

  protected void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "FacebookAuth.onResume");
  }

  public class FbLoginRequestListener extends BaseRequestListener
  {
    public FbLoginRequestListener()
    {
    }

    public void onComplete(String paramString, Object paramObject)
    {
      try
      {
        Logger.d("FlxMain", "FacebookAuth.FbLoginRequestListener Response: " + paramString.toString());
        String str = Util.parseJson(paramString).getString("id");
        AccountManager.instance().onFbLogin(FlixsterApplication.sFacebook, str);
        ProfileDao.resetUser();
        FacebookAuth.this.mUser = ProfileDao.fetchUser();
        AccountManager.instance().onFbLogin(FacebookAuth.this.mUser.getProperty("displayName"), FacebookAuth.this.mUser.id);
        FacebookAuth.this.mFacebookConnectedDialogHandler.sendEmptyMessage(0);
        return;
      }
      catch (JSONException localJSONException)
      {
        Logger.w("FlxMain", "JSON Error in response");
        return;
      }
      catch (FacebookError localFacebookError)
      {
        Logger.w("FlxMain", "Facebook Error: " + localFacebookError.getMessage());
        FacebookAuth.this.mFacebookFubarDialogHandler.sendEmptyMessage(0);
        return;
      }
      catch (Exception localException)
      {
        Logger.w("FlxMain", "Exception Facebook Error: " + localException.getMessage());
        FacebookAuth.this.mFacebookErrorDialogHandler.sendEmptyMessage(0);
      }
    }
  }

  private final class LoginDialogListener
    implements Facebook.DialogListener
  {
    private LoginDialogListener()
    {
    }

    public void onCancel()
    {
      SessionEvents.onLoginError("Action Canceled");
      Logger.d("FlxMain", "FacebookAuth.LoginDialogListener.onCancel");
      FacebookAuth.this.showDialog(8);
    }

    public void onComplete(Bundle paramBundle)
    {
      SessionEvents.onLoginSuccess();
      Facebook localFacebook = FlixsterApplication.sFacebook;
      Logger.sd("FlxMain", "FacebookAuth.LoginDialogListener.onComplete " + localFacebook.getAccessToken());
      new AsyncFacebookRunner(localFacebook).request("me", new FacebookAuth.FbLoginRequestListener(FacebookAuth.this));
    }

    public void onError(DialogError paramDialogError)
    {
      SessionEvents.onLoginError(paramDialogError.getMessage());
      Logger.d("FlxMain", "FacebookAuth.LoginDialogListener.onError");
      FacebookAuth.this.showDialog(6);
    }

    public void onFacebookError(FacebookError paramFacebookError)
    {
      SessionEvents.onLoginError(paramFacebookError.getMessage());
      Logger.d("FlxMain", "FacebookAuth.LoginDialogListener.onFacebookError code:" + paramFacebookError.getErrorCode() + " type:" + paramFacebookError.getErrorType() + " message:" + paramFacebookError.getMessage());
      if (paramFacebookError.getMessage().contentEquals("user_denied"))
      {
        FacebookAuth.this.showDialog(7);
        return;
      }
      FacebookAuth.this.showDialog(6);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FacebookAuth
 * JD-Core Version:    0.6.2
 */