package net.flixster.android;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import net.flixster.android.data.ApiBuilder;

public class FeedbackPage extends Activity
  implements View.OnClickListener
{
  String[] mBodyArray;
  String[] mLine1Array;
  String[] mLine2Array;

  public void onClick(View paramView)
  {
    Logger.d("FlxMain", "FeedbackPage.onClick");
    Integer localInteger = (Integer)paramView.getTag();
    this.mBodyArray = getResources().getStringArray(2131623951);
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setFlags(16777216);
    localIntent.setFlags(33554432);
    localIntent.setType("message/rfc822");
    String[] arrayOfString = new String[1];
    arrayOfString[0] = ApiBuilder.supportEmail();
    localIntent.putExtra("android.intent.extra.EMAIL", arrayOfString);
    localIntent.putExtra("android.intent.extra.SUBJECT", "Android Feedback -" + this.mLine1Array[localInteger.intValue()]);
    String str = FlixsterApplication.getCurrentZip();
    localIntent.putExtra("android.intent.extra.TEXT", this.mBodyArray[localInteger.intValue()] + "\n\n\n\n_____________________\n" + FlixsterApplication.getUserAgent() + "," + str);
    Starter.tryLaunch(this, localIntent);
    finish();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903083);
    this.mLine1Array = getResources().getStringArray(2131623949);
    this.mLine2Array = getResources().getStringArray(2131623950);
    int i = this.mLine1Array.length;
    LayoutInflater localLayoutInflater = LayoutInflater.from(this);
    LinearLayout localLinearLayout = (LinearLayout)findViewById(2131165312);
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return;
      View localView = localLayoutInflater.inflate(2130903161, localLinearLayout, false);
      ((TextView)localView.findViewById(2131165655)).setText(this.mLine1Array[j]);
      ((TextView)localView.findViewById(2131165750)).setText(this.mLine2Array[j]);
      localView.setOnClickListener(this);
      localView.setTag(Integer.valueOf(j));
      localLinearLayout.addView(localView);
    }
  }

  protected void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "SettingsPage.onResume()");
    Trackers.instance().track("/mymovies/feedback", "Feedback Page");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FeedbackPage
 * JD-Core Version:    0.6.2
 */