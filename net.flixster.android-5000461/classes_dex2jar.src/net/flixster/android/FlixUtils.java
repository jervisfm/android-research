package net.flixster.android;

public class FlixUtils
{
  public static String copyString(String paramString)
  {
    if (paramString == null)
      return null;
    return String.valueOf(paramString.toCharArray());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FlixUtils
 * JD-Core Version:    0.6.2
 */