package net.flixster.android;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.DeepLink;
import com.flixster.android.activity.common.DecoratedSherlockTabActivity;
import com.flixster.android.activity.decorator.TimerDecorator;
import com.flixster.android.ads.AdsArbiter;
import com.flixster.android.ads.AdsArbiter.LaunchAdsArbiter;
import com.flixster.android.ads.DfpLaunchInterstitial;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.bootstrap.Startup;
import com.flixster.android.data.AccountManager;
import com.flixster.android.utils.ActivityHolder;
import com.flixster.android.utils.AppRater;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder.DialogListener;
import com.flixster.android.widget.WidgetProvider;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TimerTask;
import net.flixster.android.ads.AdManager;
import net.flixster.android.ads.data.GenericAdsDao.AdsListener;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.LocationDao;
import net.flixster.android.data.NetflixDao;
import net.flixster.android.model.Location;
import net.flixster.android.model.User;
import net.flixster.android.util.ErrorHandler;
import net.flixster.android.util.PostalCodeUtils;

public class Flixster extends DecoratedSherlockTabActivity
  implements TabHost.OnTabChangeListener, GenericAdsDao.AdsListener
{
  private static final String KEY_DL_THEATERS_TAB = "KEY_DL_THEATERS";
  private static final String KEY_DL_WIDGET_LOGO = "KEY_DL_LOGO";
  public static final String KEY_MOVIE_ID = "net.flixster.android.EXTRA_MOVIE_ID";
  public static final String KEY_THEATER_ID = "net.flixster.android.EXTRA_THEATER_ID";
  public static final int[] RATING_LARGE_R = { 2130837917, 2130837918, 2130837919, 2130837920, 2130837921, 2130837922, 2130837923, 2130837924, 2130837925, 2130837926, 2130837927, 2130837869, 2130837865 };
  public static final int[] RATING_SMALL_R = { 2130837928, 2130837929, 2130837930, 2130837931, 2130837932, 2130837933, 2130837934, 2130837935, 2130837936, 2130837937, 2130837938, 2130837866, 2130837862 };
  public static final String TAB_BOXOFFICE = "boxoffice";
  public static final String TAB_DVD = "dvd";
  public static final String TAB_HOME = "home";
  public static final String TAB_MYMOVIES = "mymovies";
  public static final String TAB_THEATERS = "theaters";
  public static final String TAB_UPCOMING = "upcoming";
  private static int instanceCount = 0;
  private final LaunchAdHandler launchAdHandler = new LaunchAdHandler(null);
  private TabHost mTabHost;
  private boolean skipRatePrompt;

  private Set<String> checkDeepLink()
  {
    HashSet localHashSet = new HashSet();
    Uri localUri = getIntent().getData();
    Logger.d("FlxMain", "Flixster.checkDeepLink " + localUri);
    int i = 1;
    String str1 = null;
    String str2;
    String str3;
    String str4;
    String str5;
    String str6;
    if (localUri != null)
    {
      str2 = localUri.getQueryParameter("movie");
      str1 = localUri.getQueryParameter("page");
      str3 = localUri.getQueryParameter("theater");
      str4 = localUri.getQueryParameter("postalcode");
      str5 = localUri.getQueryParameter("actor");
      str6 = localUri.getQueryParameter("search");
    }
    try
    {
      Logger.d("FlxMain", "Flixster.onCreate() postalcode:" + str4);
      if (str4 != null)
      {
        String str7 = PostalCodeUtils.parseZipcodeForShowtimes(str4);
        if (str7 != null)
        {
          Logger.d("FlxMain", "Flixster.onCreate() yay! cleanZip:" + str7);
          ArrayList localArrayList = LocationDao.getLocations(str7);
          if (!localArrayList.isEmpty())
          {
            Location localLocation = (Location)localArrayList.get(0);
            FlixsterApplication.setUserLatitude(localLocation.latitude);
            FlixsterApplication.setUserLongitude(localLocation.longitude);
            FlixsterApplication.setUserZip(localLocation.zip);
            FlixsterApplication.setUserCity(localLocation.city);
            FlixsterApplication.setUserLocation(localLocation.zip);
            FlixsterApplication.setUseLocationServiceFlag(false);
            FlixsterApplication.setLocationPolicy(0);
          }
        }
      }
      if (str3 != null)
      {
        removeSplashScreen();
        Intent localIntent1 = new Intent(this, TheaterInfoPage.class);
        localIntent1.putExtra("net.flixster.android.EXTRA_THEATER_ID", Long.valueOf(str3));
        StartActivityHandler localStartActivityHandler1 = new StartActivityHandler(localIntent1);
        localStartActivityHandler1.sendEmptyMessage(0);
      }
      if ((str1 != null) && (str1.contentEquals("actor")) && (str5 != null))
      {
        removeSplashScreen();
        Intent localIntent2 = new Intent(this, ActorPage.class);
        localIntent2.putExtra("ACTOR_ID", Long.valueOf(str5));
        StartActivityHandler localStartActivityHandler2 = new StartActivityHandler(localIntent2);
        localStartActivityHandler2.sendEmptyMessage(0);
      }
      if (str2 != null)
      {
        removeSplashScreen();
        Intent localIntent3 = new Intent(this, MovieDetails.class);
        boolean bool1 = false;
        if (str1 != null)
        {
          boolean bool2 = str1.contentEquals("trailer");
          bool1 = false;
          if (bool2)
            bool1 = true;
        }
        WidgetProvider.propagateIntentExtras(getIntent(), localIntent3);
        i = 0;
        localIntent3.putExtra("net.flixster.android.EXTRA_MOVIE_ID", Long.valueOf(str2));
        localIntent3.putExtra("net.flixster.PLAY_TRAILER", bool1);
        StartActivityHandler localStartActivityHandler3 = new StartActivityHandler(localIntent3);
        localStartActivityHandler3.sendEmptyMessage(0);
      }
      if (str6 != null)
      {
        removeSplashScreen();
        Intent localIntent4 = new Intent(this, SearchPage.class);
        WidgetProvider.propagateIntentExtras(getIntent(), localIntent4);
        i = 0;
        StartActivityHandler localStartActivityHandler4 = new StartActivityHandler(localIntent4);
        localStartActivityHandler4.sendEmptyMessage(0);
      }
      if ((str1 != null) && (str1.contentEquals("theaters")))
        localHashSet.add("KEY_DL_THEATERS");
      if (i != 0)
        localHashSet.add("KEY_DL_LOGO");
      return localHashSet;
    }
    catch (DaoException localDaoException)
    {
      while (true)
        localDaoException.printStackTrace();
    }
  }

  private void checkFlixsterDeepLinks(final Uri paramUri)
  {
    if (DeepLink.isFlixsterDeepLink(paramUri))
    {
      removeSplashScreen();
      new Handler()
      {
        public void handleMessage(Message paramAnonymousMessage)
        {
          DeepLink.launchFlixsterDeepLink(paramUri, Flixster.this);
        }
      }
      .sendEmptyMessage(0);
    }
  }

  public static int getInstanceCount()
  {
    return instanceCount;
  }

  private void removeSplashScreen()
  {
    Properties.instance().splashRemoved();
    tryRemoveDialog(999999999);
  }

  private void setupTabsAndCheckLegacyDeepLinks()
  {
    Intent localIntent1 = new Intent(this, BoxOfficePage.class);
    Intent localIntent2 = new Intent(this, MyMoviesPage.class);
    Intent localIntent3 = new Intent(this, DvdPage.class);
    Intent localIntent4 = new Intent(this, TheaterListPage.class);
    Intent localIntent5 = new Intent(this, Homepage.class);
    LayoutInflater localLayoutInflater = getLayoutInflater();
    RelativeLayout localRelativeLayout1 = (RelativeLayout)localLayoutInflater.inflate(2130903170, null);
    ((ImageView)localRelativeLayout1.findViewById(2131165789)).setImageResource(2130837949);
    ((TextView)localRelativeLayout1.findViewById(2131165790)).setText(2131493140);
    TabHost.TabSpec localTabSpec1 = this.mTabHost.newTabSpec("home");
    localTabSpec1.setIndicator(localRelativeLayout1);
    localTabSpec1.setContent(localIntent5);
    this.mTabHost.addTab(localTabSpec1);
    RelativeLayout localRelativeLayout2 = (RelativeLayout)localLayoutInflater.inflate(2130903170, null);
    ((ImageView)localRelativeLayout2.findViewById(2131165789)).setImageResource(2130837947);
    ((TextView)localRelativeLayout2.findViewById(2131165790)).setText(2131493135);
    TabHost.TabSpec localTabSpec2 = this.mTabHost.newTabSpec("boxoffice");
    localTabSpec2.setIndicator(localRelativeLayout2);
    localTabSpec2.setContent(localIntent1);
    this.mTabHost.addTab(localTabSpec2);
    RelativeLayout localRelativeLayout3 = (RelativeLayout)localLayoutInflater.inflate(2130903170, null);
    ((ImageView)localRelativeLayout3.findViewById(2131165789)).setImageResource(2130837950);
    ((TextView)localRelativeLayout3.findViewById(2131165790)).setText(2131493139);
    TabHost.TabSpec localTabSpec3 = this.mTabHost.newTabSpec("mymovies");
    localTabSpec3.setIndicator(localRelativeLayout3);
    localTabSpec3.setContent(localIntent2);
    this.mTabHost.addTab(localTabSpec3);
    RelativeLayout localRelativeLayout4 = (RelativeLayout)localLayoutInflater.inflate(2130903170, null);
    ((ImageView)localRelativeLayout4.findViewById(2131165789)).setImageResource(2130837951);
    ((TextView)localRelativeLayout4.findViewById(2131165790)).setText(2131493136);
    TabHost.TabSpec localTabSpec4 = this.mTabHost.newTabSpec("theaters");
    localTabSpec4.setIndicator(localRelativeLayout4);
    localTabSpec4.setContent(localIntent4);
    this.mTabHost.addTab(localTabSpec4);
    RelativeLayout localRelativeLayout5 = (RelativeLayout)localLayoutInflater.inflate(2130903171, null);
    ((ImageView)localRelativeLayout5.findViewById(2131165789)).setImageResource(2130837948);
    ((TextView)localRelativeLayout5.findViewById(2131165790)).setText(2131493138);
    TabHost.TabSpec localTabSpec5 = this.mTabHost.newTabSpec("dvd");
    localTabSpec5.setIndicator(localRelativeLayout5);
    localTabSpec5.setContent(localIntent3);
    this.mTabHost.addTab(localTabSpec5);
    getTabWidget().setBackgroundResource(2131296301);
    this.mTabHost.setOnTabChangedListener(this);
    String str = FlixsterApplication.getLastTab();
    Set localSet = checkDeepLink();
    if (localSet.contains("KEY_DL_THEATERS"))
      str = "theaters";
    Intent localIntent6;
    if (localSet.contains("KEY_DL_LOGO"))
    {
      if (!"boxoffice".equals(str))
        break label579;
      localIntent6 = localIntent1;
    }
    while (true)
    {
      if (localIntent6 != null)
        WidgetProvider.propagateIntentExtras(getIntent(), localIntent6);
      this.mTabHost.setCurrentTabByTag(str);
      return;
      label579: if ("theaters".equals(str))
      {
        localIntent6 = localIntent4;
      }
      else if ("mymovies".equals(str))
      {
        localIntent6 = localIntent2;
      }
      else if ("dvd".equals(str))
      {
        localIntent6 = localIntent3;
      }
      else
      {
        boolean bool = "home".equals(str);
        localIntent6 = null;
        if (bool)
          localIntent6 = localIntent5;
      }
    }
  }

  private void showDiagnosticsOrRateDialog()
  {
    if (Properties.instance().hasUserSessionExpired())
      showDialog(1000000904, null);
    do
    {
      return;
      if ((Properties.instance().isMskEnabled()) && (!"mymovies".equals(FlixsterApplication.getLastTab())) && (!FlixsterApplication.hasMskPromptShown()) && (!Properties.instance().isMskFinishedDeepLinkInitiated()))
      {
        showDialog(1000001001, new DialogBuilder.DialogListener()
        {
          public void onNegativeButtonClick(int paramAnonymousInt)
          {
            FlixsterApplication.setMskPromptShown();
            Flixster.this.removeDialog(1000001001);
            Trackers.instance().trackEvent("/msk/tooltip", "MSK Tooltip", "MskTooltip", "Dismiss");
          }

          public void onNeutralButtonClick(int paramAnonymousInt)
          {
            FlixsterApplication.setMskPromptShown();
            Flixster.this.removeDialog(1000001001);
            Trackers.instance().trackEvent("/msk/tooltip", "MSK Tooltip", "MskTooltip", "Dismiss");
          }

          public void onPositiveButtonClick(int paramAnonymousInt)
          {
            FlixsterApplication.setMskPromptShown();
            Flixster.this.removeDialog(1000001001);
            Trackers.instance().trackEvent("/msk/tooltip", "MSK Tooltip", "MskTooltip", "Click");
            Starter.launchMsk(Flixster.this);
          }
        });
        Trackers.instance().trackEvent("/msk/tooltip", "MSK Tooltip", "MskTooltip", "Impression");
        this.timerDecorater.scheduleTask(new RemoveMskPromptTask(null), 6800L);
        return;
      }
      final Intent localIntent = ErrorHandler.instance().checkForAppCrash(getApplicationContext());
      if (localIntent != null)
      {
        this.skipRatePrompt = true;
        showDialog(1000001000, new DialogBuilder.DialogListener()
        {
          public void onNegativeButtonClick(int paramAnonymousInt)
          {
          }

          public void onNeutralButtonClick(int paramAnonymousInt)
          {
          }

          public void onPositiveButtonClick(int paramAnonymousInt)
          {
            Starter.tryLaunch(Flixster.this, localIntent);
          }
        });
        return;
      }
    }
    while (this.skipRatePrompt);
    AppRater.showRateDialog(this);
  }

  private void showSplashScreen()
  {
    if (Properties.instance().shouldShowSplash())
    {
      Properties.instance().splashShown();
      showDialog(999999999);
      this.timerDecorater.scheduleTask(new RemoveSplashTask(null), 1800L);
    }
  }

  private void tryRemoveDialog(int paramInt)
  {
    try
    {
      removeDialog(paramInt);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      Logger.w("FlxMain", "Flixster.tryRemoveDialog id " + paramInt, localIllegalArgumentException);
    }
  }

  public void checkAndShowLaunchAd()
  {
    if (AdsArbiter.launch().shouldShow())
      this.launchAdHandler.sendEmptyMessage(0);
  }

  public void onAdsLoaded()
  {
    Logger.i("FlxAd", "Flixster.onAdsLoaded");
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    instanceCount = 1 + instanceCount;
    boolean bool;
    Intent localIntent;
    StringBuilder localStringBuilder;
    if (getLastNonConfigurationInstance() != null)
    {
      bool = true;
      this.skipRatePrompt = bool;
      localIntent = getIntent();
      localStringBuilder = new StringBuilder("Flixster.onCreate intent:").append(localIntent.filterHashCode()).append(" ");
      if (localIntent.getExtras() != null)
        break label159;
    }
    label159: for (Object localObject = "null"; ; localObject = localIntent.getExtras().keySet())
    {
      Logger.d("FlxMain", localObject + " " + localIntent);
      createActionBar();
      DeepLink.checkMskFinished(localIntent.getData());
      showDiagnosticsOrRateDialog();
      showSplashScreen();
      Startup.instance().onStartup(this);
      AdManager.instance().addDaoListener(this);
      this.mTabHost = getTabHost();
      setupTabsAndCheckLegacyDeepLinks();
      checkFlixsterDeepLinks(localIntent.getData());
      localIntent.setData(null);
      return;
      bool = false;
      break;
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    if ((!"mymovies".equals(this.mTabHost.getCurrentTabTag())) || (!AccountManager.instance().hasUserSession()))
      paramMenu.removeItem(2131165959);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Logger.d("FlxMain", "Flixster.onDestroy");
    instanceCount = -1 + instanceCount;
    AdManager.instance().removeDaoListener(this);
    FlixsterApplication.stopLocationRequest();
    Activity localActivity = ActivityHolder.instance().getTopLevelActivity();
    if (localActivity == null)
      Logger.i("FlxMain", "ActivityHolder sanity check passed");
    int i;
    while (true)
    {
      i = ObjectHolder.instance().size();
      if (i != 0)
        break;
      Logger.i("FlxMain", "ObjectHolder sanity check passed");
      return;
      Logger.w("FlxMain", "ActivityHolder sanity check failed: " + localActivity);
      ActivityHolder.instance().clear();
    }
    Logger.w("FlxMain", "ObjectHolder sanity check failed: " + i);
    ObjectHolder.instance().clear();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getAction() == 0) && (paramInt == 4))
    {
      Logger.d("FlxMain", "Flixster.onKeyDown - call finish");
      finish();
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    StringBuilder localStringBuilder = new StringBuilder("Flixster.onNewIntent intent:").append(paramIntent.filterHashCode()).append(" ");
    if (paramIntent.getExtras() == null);
    for (Object localObject = "null"; ; localObject = paramIntent.getExtras().keySet())
    {
      Logger.i("FlxMain", localObject + " " + paramIntent);
      setIntent(paramIntent);
      DeepLink.checkMskFinished(paramIntent.getData());
      checkFlixsterDeepLinks(paramIntent.getData());
      paramIntent.setData(null);
      return;
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    boolean bool = true;
    switch (paramMenuItem.getItemId())
    {
    default:
      bool = super.onOptionsItemSelected(paramMenuItem);
    case 2131165959:
    }
    do
      return bool;
    while ((!"mymovies".equals(this.mTabHost.getCurrentTabTag())) || (!AccountManager.instance().hasUserSession()));
    User localUser = AccountManager.instance().getUser();
    localUser.resetLockerRights();
    localUser.refreshRequired = bool;
    ((MyMoviesPage)getLocalActivityManager().getActivity("mymovies")).onResume();
    return bool;
  }

  protected void onPause()
  {
    super.onPause();
    Logger.d("FlxMain", "Flixster.onPause");
    removeSplashScreen();
  }

  protected void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "Flixster.onResume");
    showSplashScreen();
    if (!FlixsterApplication.sNetflixStartupValidate)
      FlixsterApplication.sNetflixStartupValidate = true;
    try
    {
      if (FlixsterApplication.getNetflixUserId() != null)
      {
        NetflixDao.fetchTitleState("12");
        Trackers.instance().track("/netflix/startupvalidate", "User token valid");
      }
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public Object onRetainNonConfigurationInstance()
  {
    if (this.skipRatePrompt)
      return new Object();
    return null;
  }

  protected void onStart()
  {
    super.onStart();
    Logger.d("FlxMain", "Flixster.onStart");
    FlixsterApplication.sToday = new Date();
  }

  protected void onStop()
  {
    super.onStop();
    Logger.d("FlxMain", "Flixster.onStop");
  }

  public void onTabChanged(String paramString)
  {
    Logger.i("FlxMain", "Flixster.onTabChanged:" + paramString);
    FlixsterApplication.setLastTab(paramString);
    invalidateOptionsMenu();
  }

  private class LaunchAdHandler extends Handler
  {
    private LaunchAdHandler()
    {
    }

    public void handleMessage(Message paramMessage)
    {
      if ((Flixster.this.isFinishing()) || (!FlixsterApplication.isConnected()));
      do
      {
        return;
        Logger.d("FlxMain", "Flixster.LaunchAdHandler.handleMessage");
      }
      while (!AdsArbiter.launch().shouldShow());
      Flixster.this.removeSplashScreen();
      AdsArbiter.launch().shown();
      new DfpLaunchInterstitial().loadAd(Flixster.this, null);
    }
  }

  private class RemoveMskPromptTask extends TimerTask
  {
    private RemoveMskPromptTask()
    {
    }

    public void run()
    {
      Logger.d("FlxMain", "Flixster.RemoveMskPromptTask.run");
      Flixster.this.tryRemoveDialog(1000001001);
    }
  }

  private class RemoveSplashTask extends TimerTask
  {
    private RemoveSplashTask()
    {
    }

    public void run()
    {
      Logger.d("FlxMain", "Flixster.RemoveSplashTask.run");
      Flixster.this.removeSplashScreen();
    }
  }

  private class StartActivityHandler extends Handler
  {
    private final Intent intent;

    StartActivityHandler(Intent arg2)
    {
      Object localObject;
      this.intent = localObject;
    }

    public void handleMessage(Message paramMessage)
    {
      Flixster.this.startActivity(this.intent);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.Flixster
 * JD-Core Version:    0.6.2
 */