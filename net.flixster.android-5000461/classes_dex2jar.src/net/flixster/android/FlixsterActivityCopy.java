package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import com.flixster.android.activity.TabbedActivity;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.ImageTask;
import com.flixster.android.utils.ImageTaskImpl;
import com.flixster.android.utils.ListHelper;
import com.flixster.android.utils.Logger;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import net.flixster.android.ads.AdManager;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;
import net.flixster.android.util.DialogUtils;

public class FlixsterActivityCopy extends TabbedActivity
  implements ImageTask
{
  protected static final int ACTIVITY_RESULT_EMAIL = 0;
  protected static final int ACTIVITY_RESULT_INFO = 1;
  public static final int DIALOGKEY_DATESELECT = 6;
  public static final int DIALOGKEY_NETWORKERROR = 2;
  public static final int DIALOGKEY_SORTSELECT = 5;
  protected static final String KEY_PLATFORM_ID = "PLATFORM_ID";
  protected static final String LISTSTATE_POSITION = "LISTSTATE_POSITION";
  protected static final int SWIPE_MAX_OFF_PATH = 250;
  protected static final int SWIPE_MIN_DISTANCE = 120;
  protected static final int SWIPE_THRESHOLD_VELOCITY = 200;
  private View.OnClickListener mDateSelectOnClickListener = null;
  private ImageTaskImpl mImageTask;
  protected Dialog mLoadingDialog = null;
  protected Timer mPageTimer;
  protected final Handler mRemoveDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!FlixsterActivityCopy.this.isFinishing());
      try
      {
        FlixsterActivityCopy.this.removeDialog(paramAnonymousMessage.what);
        return;
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        Logger.w("FlxMain", "FlixsterActivity.mRemoveDialogHandler", localIllegalArgumentException);
      }
    }
  };
  protected int mRetryCount = 0;
  protected final Handler mShowDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!FlixsterActivityCopy.this.isFinishing());
      try
      {
        FlixsterActivityCopy.this.showDialog(paramAnonymousMessage.what);
        return;
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        Logger.w("FlxMain", "FlixsterActivity.mShowDialogHandler", localIllegalArgumentException);
      }
    }
  };
  private View.OnClickListener mStartSettingsClickListener = null;
  protected AdView mStickyBottomAd;
  protected AdView mStickyTopAd;

  protected View.OnClickListener getDateSelectOnClickListener()
  {
    try
    {
      if (this.mDateSelectOnClickListener == null)
        this.mDateSelectOnClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            FlixsterActivityCopy.this.showDialog(6);
          }
        };
      View.OnClickListener localOnClickListener = this.mDateSelectOnClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected List<Movie> getFeaturedMovies()
  {
    return null;
  }

  protected String getResourceString(int paramInt)
  {
    return getResources().getString(paramInt);
  }

  public View.OnClickListener getStartSettingsClickListener()
  {
    try
    {
      if (this.mStartSettingsClickListener == null)
        this.mStartSettingsClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            Intent localIntent = new Intent(FlixsterActivityCopy.this, SettingsPage.class);
            FlixsterActivityCopy.this.startActivity(localIntent);
          }
        };
      View.OnClickListener localOnClickListener = this.mStartSettingsClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    FlixsterApplication.setCurrentDimensions(this);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 6:
      return new AlertDialog.Builder(this).setTitle(getResourceString(2131493167)).setItems(DialogUtils.getShowtimesDateOptions(this), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Logger.d("Flixster", "ShowtimesList DIALOG_DATE_SELECT which:" + paramAnonymousInt);
          Calendar localCalendar = Calendar.getInstance();
          localCalendar.add(5, paramAnonymousInt);
          Logger.d("Flixster", "ShowtimesList DIALOG_DATE_SELECT date:" + localCalendar.getTime());
          FlixsterApplication.setShowtimesDate(localCalendar.getTime());
          Trackers.instance().track("/showtimes/selectDate", "Select Date");
          FlixsterActivityCopy.this.onResume();
          FlixsterActivityCopy.this.removeDialog(6);
        }
      }).create();
    case 2:
    }
    return new AlertDialog.Builder(this).setMessage("The network connection failed. Press Retry to make another attempt.").setTitle("Network Error").setCancelable(true).setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        FlixsterActivityCopy.this.onResume();
      }
    }).setNegativeButton(getResources().getString(2131492938), null).create();
  }

  protected void onDestroy()
  {
    if (this.mStickyTopAd != null)
      this.mStickyTopAd.destroy();
    if (this.mStickyBottomAd != null)
      this.mStickyBottomAd.destroy();
    super.onDestroy();
  }

  public void onLowMemory()
  {
    super.onLowMemory();
    Logger.w("FlxMain", "FlixsterActivity.onLowMemory");
    ProfileDao.clearBitmapCache();
  }

  protected void onPause()
  {
    super.onPause();
    if (this.mPageTimer != null)
    {
      this.mPageTimer.cancel();
      this.mPageTimer.purge();
      this.mPageTimer = null;
    }
    if (this.mImageTask != null)
    {
      this.mImageTask.stopTask();
      this.mImageTask = null;
    }
  }

  protected void onResume()
  {
    super.onResume();
    if (this.mPageTimer == null)
      this.mPageTimer = new Timer();
    if (this.mImageTask == null)
    {
      Logger.d("FlxMain", this.className + ".onResume start ImageTask");
      this.mImageTask = new ImageTaskImpl();
      this.mImageTask.startTask();
    }
  }

  public void orderImage(ImageOrder paramImageOrder)
  {
    orderImageLifo(paramImageOrder);
  }

  public void orderImageFifo(ImageOrder paramImageOrder)
  {
    if (this.mImageTask != null)
      this.mImageTask.orderImageFifo(paramImageOrder);
  }

  public void orderImageLifo(ImageOrder paramImageOrder)
  {
    if (this.mImageTask != null)
      this.mImageTask.orderImageLifo(paramImageOrder);
  }

  public void refreshSticky()
  {
    if (FlixsterApplication.isLandscape(this))
    {
      this.mStickyTopAd.setVisibility(8);
      this.mStickyBottomAd.setVisibility(8);
      return;
    }
    this.mStickyTopAd.refreshAds();
    this.mStickyBottomAd.refreshAds();
  }

  protected void retryAction()
  {
  }

  protected void retryLogic(DaoException paramDaoException)
  {
    if (!FlixsterApplication.isConnected())
    {
      this.mRetryCount = 0;
      ErrorDialog.handleException(paramDaoException, this, this.dialogDecorator);
      return;
    }
    if (this.mRetryCount < 3)
    {
      this.mRetryCount = (1 + this.mRetryCount);
      retryAction();
      return;
    }
    this.mRetryCount = 0;
    ErrorDialog.handleException(paramDaoException, this, this.dialogDecorator);
  }

  protected boolean shouldSkipBackgroundTask(int paramInt)
  {
    if ((this.topLevelDecorator.isPausing()) || (paramInt != TopLevelDecorator.getResumeCtr()))
    {
      Logger.w("FlxMain", this.className + ".shouldSkipBackgroundTask #" + paramInt);
      return true;
    }
    return false;
  }

  protected void trackPage()
  {
    super.trackPage();
    List localList = getFeaturedMovies();
    Iterator localIterator;
    if ((localList != null) && (!localList.isEmpty()))
      localIterator = ListHelper.copy(localList).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Movie localMovie = (Movie)localIterator.next();
      AdManager.instance().trackEvent(localMovie.getFeaturedId().intValue(), "Impression", "");
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FlixsterActivityCopy
 * JD-Core Version:    0.6.2
 */