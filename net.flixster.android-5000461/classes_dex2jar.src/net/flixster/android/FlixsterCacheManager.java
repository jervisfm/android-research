package net.flixster.android;

import android.content.Context;
import android.os.Environment;
import com.flixster.android.utils.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.util.HttpHelper;

public class FlixsterCacheManager
{
  public static final long CACHE_TRIMPADDING = 5000L;
  public static final int POLICY_LARGE = 3;
  public static final int POLICY_MEDIUM = 2;
  public static final int POLICY_OFF = 0;
  public static final int POLICY_SMALL = 1;
  public static final int STATE_INVALID = 0;
  public static final int STATE_VALID = 2;
  public static final int STATE_WRITING = 1;
  private static long sCacheByteSize = 0L;
  private static File sCacheDir;
  private static HashMap<Integer, CacheItem> sCacheHash;
  private static int sCacheItemSize;
  public static long sCacheLimit = 0L;
  private static FileInputStream sFileInputStream;
  private static FileOutputStream sFileOutputStream;
  private static CacheItem sHead = null;
  private static boolean sIsActive = false;
  private static CacheItem sTail = null;
  private static Timer sTimer;
  private static TimerTask sTimerTask;
  private Context mContext;

  static
  {
    sCacheItemSize = 0;
  }

  FlixsterCacheManager(Context paramContext)
  {
    this.mContext = paramContext;
    sCacheHash = new LinkedHashMap(10, 0.5F, true);
    sTimer = new Timer();
    sTimerTask = new TimerTask()
    {
      public void run()
      {
        boolean bool1 = Environment.getExternalStorageState().contentEquals("mounted");
        boolean bool2 = false;
        if (bool1)
        {
          File localFile = Environment.getExternalStorageDirectory();
          Logger.d("FlxMain", "FlixsterCacheManager sdDir:" + localFile);
          FlixsterCacheManager.sCacheDir = new File(localFile + "/data/flixster/");
          if ((!FlixsterCacheManager.sCacheDir.exists()) || (!FlixsterCacheManager.sCacheDir.isDirectory()) || (!FlixsterCacheManager.sCacheDir.canWrite()))
            break label124;
          Logger.v("FlxMain", "FlixsterCacheManager  mCacheDir exists == true");
          FlixsterCacheManager.this.buildCacheIndex();
          bool2 = true;
        }
        while (true)
        {
          FlixsterCacheManager.SetCacheLimit();
          FlixsterCacheManager.TrimCache(0L);
          FlixsterCacheManager.sIsActive = bool2;
          return;
          label124: Logger.v("FlxMain", "FlixsterCacheManager mCacheDir mkdirs()");
          boolean bool3 = FlixsterCacheManager.sCacheDir.mkdirs();
          bool2 = false;
          if (bool3)
            bool2 = true;
        }
      }
    };
    sTimer.schedule(sTimerTask, 10L);
  }

  private void AppendCacheItem(CacheItem paramCacheItem)
  {
    if (sHead == null)
      sHead = paramCacheItem;
    for (sTail = sHead; ; sTail = paramCacheItem)
    {
      sCacheHash.put(Integer.valueOf(paramCacheItem.key), sTail);
      sCacheByteSize += paramCacheItem.size;
      sCacheItemSize = 1 + sCacheItemSize;
      return;
      paramCacheItem.next = null;
      paramCacheItem.prev = sTail;
      sTail.next = paramCacheItem;
    }
  }

  private static void DecapCacheItem(CacheItem paramCacheItem)
  {
    if (paramCacheItem == null)
    {
      Logger.e("FlxMain", "FlixsterCacheManager.DecapCacheItem topItem is null");
      return;
    }
    if (sHead == sTail)
    {
      Logger.v("FlxMain", "FlixsterCacheManager.DecapCacheItem empty? mHead:" + sHead + " tail:" + sTail);
      sHead = null;
      sTail = null;
    }
    while (true)
    {
      sCacheHash.remove(paramCacheItem);
      sCacheByteSize -= paramCacheItem.size;
      sCacheItemSize = -1 + sCacheItemSize;
      return;
      sHead = paramCacheItem.next;
      sHead.prev = null;
    }
  }

  public static int FilenameToHash(String paramString)
  {
    return Integer.valueOf(paramString.substring(5).replace("_", "-")).intValue();
  }

  public static String HashToFilename(int paramInt)
  {
    return "cache" + Integer.toString(paramInt).replace('-', '_');
  }

  public static void SetCacheLimit()
  {
    Logger.v("FlxMain", "FlixsterCacheManager SetCacheLimit() before sCacheLimit:" + sCacheLimit + " getCachePolicy:" + FlixsterApplication.getCachePolicy());
    switch (FlixsterApplication.getCachePolicy())
    {
    default:
      sCacheLimit = 0L;
    case 0:
    case 1:
    case 2:
    case 3:
    }
    while (true)
    {
      Logger.v("FlxMain", "FlixsterCacheManager SetCacheLimit() after sCacheLimit:" + sCacheLimit);
      return;
      sCacheLimit = 0L;
      continue;
      sCacheLimit = 2000000L;
      continue;
      sCacheLimit = 4000000L;
      continue;
      sCacheLimit = 8000000L;
    }
  }

  public static void TrimCache(long paramLong)
  {
    while (true)
    {
      try
      {
        if (paramLong > sCacheLimit)
        {
          Logger.w("FlxMain", "trimSize size exceeds cache size!!");
          return;
        }
        if (5000L + (paramLong + sCacheByteSize) <= sCacheLimit)
          continue;
        if ((5000L + (paramLong + sCacheByteSize) <= sCacheLimit) || (sHead == null))
        {
          Logger.v("FlxMain", "FlixsterCacheManager.put TRIM, mCacheByteSize:" + sCacheByteSize + " mCacheItemSize:" + sCacheItemSize + " sCacheLimit:" + sCacheLimit);
          continue;
        }
      }
      finally
      {
      }
      new File(sCacheDir, HashToFilename(sHead.key)).delete();
      DecapCacheItem(sHead);
    }
  }

  public void buildCacheIndex()
  {
    Logger.v("FlxMain", "FlixsterCacheManager.buildCacheIndex start");
    sCacheDir = this.mContext.getFilesDir();
    String[] arrayOfString = sCacheDir.list();
    Logger.v("FlxMain", "FlixsterCacheManager.buildCacheIndex files:" + arrayOfString);
    int i;
    if (arrayOfString != null)
      i = arrayOfString.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        Logger.v("FlxMain", "FlixsterCacheManager.buildCacheIndex mCacheByteSize:" + sCacheByteSize + " mCacheItemSize:" + sCacheItemSize + " sCacheLimit:" + sCacheLimit);
        return;
      }
      String str = arrayOfString[j];
      if (str.startsWith("cache"))
      {
        File localFile = new File(sCacheDir, str);
        CacheItem localCacheItem = new CacheItem();
        localCacheItem.size = localFile.length();
        localCacheItem.state = 2;
        localCacheItem.key = FilenameToHash(str);
        AppendCacheItem(localCacheItem);
      }
    }
  }

  public byte[] get(int paramInt)
  {
    String str = "cache" + Integer.toString(paramInt).replace('-', '_');
    if (!sIsActive)
    {
      Logger.e("FlxMain", "FlixsterCacheManager.get NOT ACTIVE YET");
      return null;
    }
    try
    {
      if (sCacheHash.containsKey(Integer.valueOf(paramInt)))
      {
        sFileInputStream = new FileInputStream(new File(sCacheDir, str));
        byte[] arrayOfByte = HttpHelper.streamToByteArray(sFileInputStream);
        sFileInputStream.close();
        return arrayOfByte;
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      Logger.w("FlxMain", "FlixsterCacheManager.get", localFileNotFoundException);
      return null;
    }
    catch (IOException localIOException)
    {
      while (true)
        Logger.w("FlxMain", "FlixsterCacheManager.get", localIOException);
    }
  }

  // ERROR //
  public void put(int paramInt, byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 130	java/lang/StringBuilder
    //   5: dup
    //   6: ldc 181
    //   8: invokespecial 135	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   11: iload_1
    //   12: invokestatic 183	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   15: bipush 45
    //   17: bipush 95
    //   19: invokevirtual 186	java/lang/String:replace	(CC)Ljava/lang/String;
    //   22: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   25: invokevirtual 148	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   28: astore 4
    //   30: getstatic 57	net/flixster/android/FlixsterCacheManager:sIsActive	Z
    //   33: ifne +29 -> 62
    //   36: ldc 120
    //   38: new 130	java/lang/StringBuilder
    //   41: dup
    //   42: ldc_w 311
    //   45: invokespecial 135	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   48: aload_2
    //   49: arraylength
    //   50: invokevirtual 202	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   53: invokevirtual 148	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   56: invokestatic 151	com/flixster/android/utils/Logger:v	(Ljava/lang/String;Ljava/lang/String;)V
    //   59: aload_0
    //   60: monitorexit
    //   61: return
    //   62: getstatic 47	net/flixster/android/FlixsterCacheManager:sCacheLimit	J
    //   65: lconst_0
    //   66: lcmp
    //   67: ifeq -8 -> 59
    //   70: ldc2_w 7
    //   73: aload_2
    //   74: arraylength
    //   75: i2l
    //   76: ladd
    //   77: invokestatic 313	net/flixster/android/FlixsterCacheManager:TrimCache	(J)V
    //   80: new 315	java/io/FileOutputStream
    //   83: dup
    //   84: new 225	java/io/File
    //   87: dup
    //   88: getstatic 227	net/flixster/android/FlixsterCacheManager:sCacheDir	Ljava/io/File;
    //   91: aload 4
    //   93: invokespecial 232	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   96: invokespecial 316	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   99: putstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   102: aload_0
    //   103: getfield 63	net/flixster/android/FlixsterCacheManager:mContext	Landroid/content/Context;
    //   106: aload 4
    //   108: iconst_1
    //   109: invokevirtual 322	android/content/Context:openFileOutput	(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    //   112: putstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   115: getstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   118: aload_2
    //   119: iconst_0
    //   120: aload_2
    //   121: arraylength
    //   122: invokevirtual 326	java/io/FileOutputStream:write	([BII)V
    //   125: getstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   128: invokevirtual 329	java/io/FileOutputStream:flush	()V
    //   131: new 93	net/flixster/android/FlixsterCacheManager$CacheItem
    //   134: dup
    //   135: aload_0
    //   136: invokespecial 265	net/flixster/android/FlixsterCacheManager$CacheItem:<init>	(Lnet/flixster/android/FlixsterCacheManager;)V
    //   139: astore 14
    //   141: aload 14
    //   143: iload_1
    //   144: putfield 96	net/flixster/android/FlixsterCacheManager$CacheItem:key	I
    //   147: aload 14
    //   149: iconst_2
    //   150: putfield 272	net/flixster/android/FlixsterCacheManager$CacheItem:state	I
    //   153: aload 14
    //   155: aload_2
    //   156: arraylength
    //   157: i2l
    //   158: putfield 111	net/flixster/android/FlixsterCacheManager$CacheItem:size	J
    //   161: aload_0
    //   162: aload 14
    //   164: invokespecial 276	net/flixster/android/FlixsterCacheManager:AppendCacheItem	(Lnet/flixster/android/FlixsterCacheManager$CacheItem;)V
    //   167: getstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   170: astore 15
    //   172: aload 15
    //   174: ifnull -115 -> 59
    //   177: getstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   180: invokevirtual 330	java/io/FileOutputStream:close	()V
    //   183: goto -124 -> 59
    //   186: astore 16
    //   188: goto -129 -> 59
    //   191: astore 11
    //   193: ldc 120
    //   195: ldc_w 332
    //   198: invokestatic 151	com/flixster/android/utils/Logger:v	(Ljava/lang/String;Ljava/lang/String;)V
    //   201: aload 11
    //   203: invokevirtual 335	java/io/FileNotFoundException:printStackTrace	()V
    //   206: getstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   209: astore 12
    //   211: aload 12
    //   213: ifnull -154 -> 59
    //   216: getstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   219: invokevirtual 330	java/io/FileOutputStream:close	()V
    //   222: goto -163 -> 59
    //   225: astore 13
    //   227: goto -168 -> 59
    //   230: astore 8
    //   232: ldc 120
    //   234: ldc_w 337
    //   237: invokestatic 151	com/flixster/android/utils/Logger:v	(Ljava/lang/String;Ljava/lang/String;)V
    //   240: aload 8
    //   242: invokevirtual 338	java/io/IOException:printStackTrace	()V
    //   245: getstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   248: astore 9
    //   250: aload 9
    //   252: ifnull -193 -> 59
    //   255: getstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   258: invokevirtual 330	java/io/FileOutputStream:close	()V
    //   261: goto -202 -> 59
    //   264: astore 10
    //   266: goto -207 -> 59
    //   269: astore 5
    //   271: getstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   274: astore 6
    //   276: aload 6
    //   278: ifnull +9 -> 287
    //   281: getstatic 318	net/flixster/android/FlixsterCacheManager:sFileOutputStream	Ljava/io/FileOutputStream;
    //   284: invokevirtual 330	java/io/FileOutputStream:close	()V
    //   287: aload 5
    //   289: athrow
    //   290: astore_3
    //   291: aload_0
    //   292: monitorexit
    //   293: aload_3
    //   294: athrow
    //   295: astore 7
    //   297: goto -10 -> 287
    //
    // Exception table:
    //   from	to	target	type
    //   177	183	186	java/io/IOException
    //   80	167	191	java/io/FileNotFoundException
    //   216	222	225	java/io/IOException
    //   80	167	230	java/io/IOException
    //   255	261	264	java/io/IOException
    //   80	167	269	finally
    //   193	206	269	finally
    //   232	245	269	finally
    //   2	59	290	finally
    //   62	80	290	finally
    //   167	172	290	finally
    //   177	183	290	finally
    //   206	211	290	finally
    //   216	222	290	finally
    //   245	250	290	finally
    //   255	261	290	finally
    //   271	276	290	finally
    //   281	287	290	finally
    //   287	290	290	finally
    //   281	287	295	java/io/IOException
  }

  class CacheItem
  {
    int key;
    CacheItem next = null;
    CacheItem prev = null;
    long size = 0L;
    int state = 0;

    CacheItem()
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FlixsterCacheManager
 * JD-Core Version:    0.6.2
 */