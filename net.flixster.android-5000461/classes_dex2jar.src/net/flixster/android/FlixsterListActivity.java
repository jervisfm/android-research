package net.flixster.android;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.decorator.ActivityDecorator;
import com.flixster.android.activity.decorator.DialogDecorator;
import com.flixster.android.bootstrap.BootstrapActivity;
import com.flixster.android.utils.ImageTask;
import com.flixster.android.utils.ImageTaskImpl;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.DialogBuilder.DialogListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;

public class FlixsterListActivity extends SherlockListActivity
  implements ImageTask
{
  private final String className = getClass().getSimpleName();
  private Collection<ActivityDecorator> decorators;
  protected DialogDecorator dialogDecorator;
  private ImageTaskImpl mImageTask;
  protected HashMap<String, Movie> mMovieHash;

  protected void addDecorator(ActivityDecorator paramActivityDecorator)
  {
    this.decorators.add(paramActivityDecorator);
  }

  protected void createActionBar()
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayHomeAsUpEnabled(true);
    localActionBar.setDisplayShowTitleEnabled(false);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.decorators = new ArrayList();
    DialogDecorator localDialogDecorator = new DialogDecorator(this);
    this.dialogDecorator = localDialogDecorator;
    addDecorator(localDialogDecorator);
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onCreate();
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = this.dialogDecorator.onCreateDialog(paramInt);
    if (localDialog != null)
      return localDialog;
    return super.onCreateDialog(paramInt);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        this.mMovieHash = null;
        return;
      }
      ((ActivityDecorator)localIterator.next()).onDestroy();
    }
  }

  public void onLowMemory()
  {
    super.onLowMemory();
    Logger.w("FlxMain", "Low memory!");
    ProfileDao.clearBitmapCache();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 16908332:
    }
    NavUtils.navigateUpTo(this, BootstrapActivity.getMainIntent(this));
    return true;
  }

  protected void onPause()
  {
    super.onPause();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        if (this.mImageTask != null)
        {
          this.mImageTask.stopTask();
          this.mImageTask = null;
        }
        return;
      }
      ((ActivityDecorator)localIterator.next()).onPause();
    }
  }

  protected void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "FlixsterListActivity.onResume");
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        if (this.mMovieHash == null)
          this.mMovieHash = new HashMap();
        if (this.mImageTask == null)
        {
          Logger.d("FlxMain", this.className + ".onResume start ImageTask");
          this.mImageTask = new ImageTaskImpl();
          this.mImageTask.startTask();
        }
        return;
      }
      ((ActivityDecorator)localIterator.next()).onResume();
    }
  }

  protected void onStop()
  {
    super.onStop();
  }

  public void orderImage(ImageOrder paramImageOrder)
  {
    orderImageLifo(paramImageOrder);
  }

  public void orderImageFifo(ImageOrder paramImageOrder)
  {
    if (this.mImageTask != null)
      this.mImageTask.orderImageFifo(paramImageOrder);
  }

  public void orderImageLifo(ImageOrder paramImageOrder)
  {
    if (this.mImageTask != null)
      this.mImageTask.orderImageLifo(paramImageOrder);
  }

  protected void setActionBarTitle(int paramInt)
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayShowTitleEnabled(true);
    localActionBar.setTitle(paramInt);
  }

  public void showDialog(int paramInt, DialogBuilder.DialogListener paramDialogListener)
  {
    this.dialogDecorator.showDialog(paramInt, paramDialogListener);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FlixsterListActivity
 * JD-Core Version:    0.6.2
 */