package net.flixster.android;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.utils.Logger;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import net.flixster.android.ads.AdView;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;
import net.flixster.android.model.User;

public abstract class FlixsterListAdapter extends BaseAdapter
{
  protected static final int VIEW_TYPE_ADVIEW = 7;
  protected static final int VIEW_TYPE_LETTER = 8;
  protected static final int VIEW_TYPE_MOVIE = 4;
  protected static final int VIEW_TYPE_REVIEW = 5;
  protected static final int VIEW_TYPE_TEXT = 2;
  protected static final int VIEW_TYPE_TITLE = 1;
  protected static final int VIEW_TYPE_UNSUPPORTED = 0;
  protected static final int VIEW_TYPE_USER = 3;
  protected static final int VIEW_TYPE_VIEW = 6;
  protected static ArrayList<Object> data;
  protected FlixsterListActivity context;
  protected LayoutInflater inflater;
  protected View.OnClickListener movieClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Movie localMovie = (Movie)paramAnonymousView.getTag();
      if (localMovie != null)
      {
        Intent localIntent = FlixsterListAdapter.this.getMovieIntent(localMovie);
        FlixsterListAdapter.this.context.startActivity(localIntent);
      }
    }
  };
  private final Handler movieThumbnailHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      ImageView localImageView = (ImageView)paramAnonymousMessage.obj;
      if (localImageView != null)
      {
        Movie localMovie = (Movie)localImageView.getTag();
        if (localMovie != null)
        {
          localImageView.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
          localImageView.invalidate();
        }
      }
    }
  };
  protected Resources resources;
  protected View.OnClickListener reviewClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Review localReview = (Review)paramAnonymousView.getTag();
      if ((localReview != null) && (localReview.getMovie() != null))
      {
        Movie localMovie = localReview.getMovie();
        Intent localIntent = FlixsterListAdapter.this.getMovieIntent(localMovie);
        FlixsterListAdapter.this.context.startActivity(localIntent);
      }
    }
  };

  public FlixsterListAdapter(FlixsterListActivity paramFlixsterListActivity, ArrayList<Object> paramArrayList)
  {
    this.context = paramFlixsterListActivity;
    this.inflater = LayoutInflater.from(paramFlixsterListActivity);
    this.resources = paramFlixsterListActivity.getResources();
    data = paramArrayList;
    ListView localListView = paramFlixsterListActivity.getListView();
    localListView.setAddStatesFromChildren(false);
    localListView.setItemsCanFocus(true);
  }

  private Intent getMovieIntent(Movie paramMovie)
  {
    Intent localIntent = new Intent("DETAILS", null, this.context, MovieDetails.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramMovie.getId());
    localIntent.putExtra("MOVIE_IN_THEATER_FLAG", paramMovie.isMIT);
    localIntent.putExtra("MOVIE_THUMBNAIL", (Parcelable)paramMovie.thumbnailSoftBitmap.get());
    String[] arrayOfString = { "title", "high", "mpaa", "MOVIE_ACTORS", "thumbnail", "runningTime", "directors", "theaterReleaseDate", "genre", "status", "MOVIE_ACTORS_SHORT", "meta" };
    int i = arrayOfString.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        localIntent.putExtra("boxOffice", paramMovie.getIntProperty("boxOffice"));
        if (paramMovie.checkIntProperty("popcornScore"))
          localIntent.putExtra("popcornScore", paramMovie.getIntProperty("popcornScore"));
        if (paramMovie.checkIntProperty("rottenTomatoes"))
          localIntent.putExtra("rottenTomatoes", paramMovie.getIntProperty("rottenTomatoes"));
        return localIntent;
      }
      String str = arrayOfString[j];
      localIntent.putExtra(str, paramMovie.getProperty(str));
    }
  }

  public boolean areAllItemsEnabled()
  {
    return false;
  }

  public int getCount()
  {
    return data.size();
  }

  public Object getItem(int paramInt)
  {
    return data.get(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public int getItemViewType(int paramInt)
  {
    Object localObject = data.get(paramInt);
    if ((localObject instanceof String))
    {
      if (paramInt == 0)
        return 1;
      return 2;
    }
    if ((localObject instanceof User))
      return 3;
    if ((localObject instanceof Review))
      return 5;
    if ((localObject instanceof AdView))
      return 7;
    if ((localObject instanceof View))
      return 6;
    Logger.e("FlxMain", "FlixsterListAdapter.getItemViewType UNSUPPORTED TYPE position:" + paramInt);
    return 0;
  }

  protected View getProfileView(User paramUser, View paramView, ViewGroup paramViewGroup)
  {
    if ((paramView == null) || (!(paramView.getTag() instanceof ProfileViewHolder)))
      paramView = this.inflater.inflate(2130903150, paramViewGroup, false);
    ProfileViewHolder localProfileViewHolder = (ProfileViewHolder)paramView.getTag();
    if (localProfileViewHolder == null)
    {
      localProfileViewHolder = new ProfileViewHolder(null);
      localProfileViewHolder.thumbnailView = ((ImageView)paramView.findViewById(2131165706));
      localProfileViewHolder.thumbnailFrameView = ((ImageView)paramView.findViewById(2131165707));
      localProfileViewHolder.titleView = ((TextView)paramView.findViewById(2131165708));
      localProfileViewHolder.friendView = ((TextView)paramView.findViewById(2131165349));
      localProfileViewHolder.ratingView = ((TextView)paramView.findViewById(2131165348));
      localProfileViewHolder.wtsView = ((TextView)paramView.findViewById(2131165347));
      localProfileViewHolder.collectionsView = ((TextView)paramView.findViewById(2131165350));
      paramView.setTag(localProfileViewHolder);
    }
    String str1 = FlixsterApplication.getPlatform();
    if ("FLX".equals(str1))
      localProfileViewHolder.thumbnailFrameView.setBackgroundResource(2130837692);
    while (true)
    {
      Bitmap localBitmap = paramUser.getProfileBitmap(localProfileViewHolder.thumbnailView);
      if (localBitmap != null)
        localProfileViewHolder.thumbnailView.setImageBitmap(localBitmap);
      String str2 = paramUser.displayName;
      if (str2 != null)
      {
        localProfileViewHolder.titleView.setText(str2);
        localProfileViewHolder.titleView.setVisibility(0);
      }
      int i = paramUser.friendCount;
      localProfileViewHolder.friendView.setText(new StringBuilder("").append(i).append(" ").append(this.resources.getString(2131492994)));
      int j = paramUser.ratingCount;
      localProfileViewHolder.ratingView.setText(new StringBuilder("").append(j).append(" ").append(this.resources.getString(2131492995)));
      int k = paramUser.wtsCount;
      localProfileViewHolder.wtsView.setText(new StringBuilder("").append(k).append(" ").append(this.resources.getString(2131492996)));
      int m = paramUser.collectionsCount;
      if (m > 0)
      {
        localProfileViewHolder.collectionsView.setText(new StringBuilder("").append(m).append(" ").append(this.resources.getString(2131492997)));
        localProfileViewHolder.collectionsView.setVisibility(0);
      }
      return paramView;
      if ("FBK".equals(str1))
        localProfileViewHolder.thumbnailFrameView.setBackgroundResource(2130837833);
    }
  }

  protected View getReviewView(Review paramReview, boolean paramBoolean, View paramView, ViewGroup paramViewGroup, View.OnClickListener paramOnClickListener)
  {
    if ((paramView == null) || (!(paramView.getTag() instanceof ReviewViewHolder)))
      paramView = this.inflater.inflate(2130903129, paramViewGroup, false);
    ReviewViewHolder localReviewViewHolder = (ReviewViewHolder)paramView.getTag();
    if (localReviewViewHolder == null)
    {
      localReviewViewHolder = new ReviewViewHolder();
      localReviewViewHolder.reviewLayout = ((RelativeLayout)paramView.findViewById(2131165497));
      localReviewViewHolder.authorView = ((TextView)paramView.findViewById(2131165502));
      localReviewViewHolder.starsView = ((ImageView)paramView.findViewById(2131165501));
      localReviewViewHolder.commentView = ((TextView)paramView.findViewById(2131165503));
      localReviewViewHolder.titleView = ((TextView)paramView.findViewById(2131165499));
      localReviewViewHolder.movieView = ((ImageView)paramView.findViewById(2131165498));
      paramView.setTag(localReviewViewHolder);
    }
    if (paramBoolean)
    {
      localReviewViewHolder.authorView.setText("by " + paramReview.name);
      localReviewViewHolder.authorView.setVisibility(0);
    }
    localReviewViewHolder.starsView.setImageResource(Flixster.RATING_SMALL_R[((int)(2.0D * paramReview.stars))]);
    localReviewViewHolder.commentView.setText(paramReview.comment);
    Movie localMovie = paramReview.getMovie();
    if (localMovie != null)
    {
      String str1 = localMovie.getProperty("title");
      localReviewViewHolder.titleView.setText(str1);
      if (localMovie.thumbnailSoftBitmap.get() == null)
        break label318;
      localReviewViewHolder.movieView.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
    }
    while (true)
    {
      localReviewViewHolder.reviewLayout.setTag(paramReview);
      localReviewViewHolder.reviewLayout.setId(2130903129);
      if (paramOnClickListener == null)
        break;
      localReviewViewHolder.reviewLayout.setOnClickListener(paramOnClickListener);
      return paramView;
      label318: String str2 = localMovie.getProperty("thumbnail");
      if (str2 != null)
      {
        localReviewViewHolder.movieView.setImageResource(2130837844);
        localReviewViewHolder.movieView.setTag(localMovie);
        ImageOrder localImageOrder = new ImageOrder(0, localMovie, str2, localReviewViewHolder.movieView, this.movieThumbnailHandler);
        this.context.orderImage(localImageOrder);
      }
      else
      {
        localReviewViewHolder.movieView.setImageResource(2130837839);
      }
    }
    localReviewViewHolder.reviewLayout.setOnClickListener(this.reviewClickListener);
    return paramView;
  }

  protected View getTextView(String paramString, View paramView)
  {
    if ((paramView == null) || (!(paramView instanceof TextView)))
    {
      TextView localTextView = new TextView(this.context);
      localTextView.setText(paramString);
      localTextView.setTextColor(this.context.getResources().getColor(2131296268));
      localTextView.setPadding(10, 10, 10, 10);
      paramView = localTextView;
    }
    return paramView;
  }

  protected View getTitleView(String paramString, View paramView)
  {
    if ((paramView == null) || (!(paramView instanceof TextView)))
    {
      TextView localTextView = new TextView(this.context);
      localTextView.setText(paramString);
      localTextView.setBackgroundResource(2130837972);
      localTextView.setTextAppearance(this.context, 2131558526);
      localTextView.setPadding(10, 10, 10, 10);
      paramView = localTextView;
    }
    return paramView;
  }

  public int getViewTypeCount()
  {
    return 9;
  }

  public boolean isEnabled(int paramInt)
  {
    return getItemViewType(paramInt) != 0;
  }

  protected static final class MovieViewHolder
  {
    TextView actorsView;
    TextView friendScore;
    TextView metaView;
    RelativeLayout movieLayout;
    TextView releaseView;
    TextView scoreView;
    ImageView thumbnailView;
    TextView titleView;
    ImageView trailerView;
  }

  private static final class ProfileViewHolder
  {
    TextView collectionsView;
    TextView friendView;
    TextView ratingView;
    ImageView thumbnailFrameView;
    ImageView thumbnailView;
    TextView titleView;
    TextView wtsView;
  }

  protected static final class ReviewViewHolder
  {
    TextView authorView;
    TextView commentView;
    ImageView movieView;
    RelativeLayout reviewLayout;
    ImageView starsView;
    TextView titleView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FlixsterListAdapter
 * JD-Core Version:    0.6.2
 */