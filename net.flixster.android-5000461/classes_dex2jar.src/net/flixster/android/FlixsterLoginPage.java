package net.flixster.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.msk.MskController;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.DialogBuilder;
import com.flixster.android.view.DialogBuilder.DialogListener;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.DaoException.Type;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.User;

public class FlixsterLoginPage extends DecoratedSherlockActivity
{
  private TextView errorText;
  private boolean isLoggingIn;
  private final Handler loginHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      String str1 = ((EditText)FlixsterLoginPage.this.findViewById(2131165319)).getText().toString();
      String str2;
      String str3;
      if (str1 != null)
      {
        str2 = str1.trim();
        str3 = ((EditText)FlixsterLoginPage.this.findViewById(2131165320)).getText().toString();
        if (str3 == null)
          break label140;
      }
      label140: for (String str4 = str3.trim(); ; str4 = "")
      {
        if (!FlixsterLoginPage.this.isLoggingIn)
        {
          if (!FlixsterApplication.isConnected())
            break label147;
          FlixsterLoginPage.this.isLoggingIn = true;
          final String str5 = str2;
          final String str6 = str4;
          FlixsterLoginPage.this.errorText.setVisibility(8);
          FlixsterLoginPage.this.throbber.setVisibility(0);
          new Thread()
          {
            public void run()
            {
              Message localMessage = Message.obtain();
              try
              {
                User localUser = ProfileDao.loginUser(str5, str6, "FLX");
                if ((localUser != null) && (localUser.id != null) && (localUser.id.length() > 0))
                  localMessage.obj = Boolean.valueOf(true);
                ProfileDao.fetchUser();
                FlixsterLoginPage.this.loginUserHandler.sendMessage(localMessage);
                return;
              }
              catch (DaoException localDaoException)
              {
                while (true)
                  localMessage.obj = localDaoException;
              }
            }
          }
          .start();
        }
        return;
        str2 = "";
        break;
      }
      label147: FlixsterLoginPage.this.errorText.setVisibility(0);
      FlixsterLoginPage.this.errorText.setText("The device has no connection to the Internet. Please verify the settings.");
      FlixsterLoginPage.this.errorText.invalidate();
      FlixsterLoginPage.this.isLoggingIn = false;
    }
  };
  private final Handler loginUserHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      FlixsterLoginPage.this.throbber.setVisibility(8);
      if (Boolean.valueOf(true).equals(paramAnonymousMessage.obj))
      {
        FlixsterLoginPage.this.errorText.setVisibility(8);
        FlixsterLoginPage.this.setResult(-1);
        Trackers.instance().track("/flixster/login/dialog", "Flixster - Login");
        if (MskController.isRequestCodeValid(FlixsterLoginPage.this.requestCode))
          MskController.instance().trackFlxLoginSuccess();
        FlixsterLoginPage.this.finish();
      }
      while (!(paramAnonymousMessage.obj instanceof DaoException))
      {
        FlixsterLoginPage.this.isLoggingIn = false;
        return;
      }
      DaoException localDaoException = (DaoException)paramAnonymousMessage.obj;
      if (localDaoException.getType() == DaoException.Type.USER_ACCT);
      for (String str = localDaoException.getMessage(); ; str = FlixsterLoginPage.this.getString(2131493217))
      {
        FlixsterLoginPage.this.errorText.setVisibility(0);
        FlixsterLoginPage.this.errorText.setText(str);
        FlixsterLoginPage.this.setResult(0);
        break;
      }
    }
  };
  private int requestCode;
  private ProgressBar throbber;
  private final DialogBuilder.DialogListener tosDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      FlixsterApplication.setTosAccepted(true);
      FlixsterLoginPage.this.loginHandler.handleMessage(null);
    }
  };

  private void login()
  {
    if (FlixsterApplication.hasAcceptedTos())
    {
      this.tosDialogListener.onPositiveButtonClick(0);
      return;
    }
    DialogBuilder.showTermsOfService(this, this.tosDialogListener);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxMain", "FlixsterLoginPage.onCreate");
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.requestCode = localBundle.getInt("MskEntryActivity.REQUEST_CODE");
    setContentView(2130903085);
    createActionBar();
    setActionBarTitle(2131492961);
    this.errorText = ((TextView)findViewById(2131165321));
    this.throbber = ((ProgressBar)findViewById(2131165241));
    if (MskController.isRequestCodeValid(this.requestCode))
      MskController.instance().trackFlxLoginAttempt();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getSupportMenuInflater().inflate(2131689480, paramMenu);
    return true;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165954:
    }
    login();
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FlixsterLoginPage
 * JD-Core Version:    0.6.2
 */