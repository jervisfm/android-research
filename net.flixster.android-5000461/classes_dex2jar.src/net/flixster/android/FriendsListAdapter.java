package net.flixster.android;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.SubHeader;
import java.util.ArrayList;
import net.flixster.android.ads.AdView;
import net.flixster.android.model.User;

public class FriendsListAdapter extends FlixsterListAdapter
{
  private static final int MAX_LENGTH_FULL_NAME = 15;
  private View.OnClickListener friendClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      User localUser = (User)paramAnonymousView.getTag();
      if (localUser != null)
      {
        Trackers.instance().track("/mymovies/profile", "User Profile Page for user:" + localUser.id);
        String str = String.valueOf(localUser.id);
        Intent localIntent = new Intent(FriendsListAdapter.this.context, UserProfilePage.class);
        localIntent.putExtra("PLATFORM_ID", str);
        FriendsListAdapter.this.context.startActivity(localIntent);
      }
    }
  };
  private AdapterView.OnItemClickListener friendItemClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      if (3 == FriendsListAdapter.this.getItemViewType(paramAnonymousInt))
        ((FriendsListAdapter.FriendViewHolder)paramAnonymousView.getTag()).friendLayout.performClick();
    }
  };
  private boolean mRefreshAd = true;

  public FriendsListAdapter(FlixsterListActivity paramFlixsterListActivity, ArrayList<Object> paramArrayList)
  {
    super(paramFlixsterListActivity, paramArrayList);
    paramFlixsterListActivity.getListView().setOnItemClickListener(this.friendItemClickListener);
  }

  private View getFriendView(User paramUser, View paramView, ViewGroup paramViewGroup)
  {
    if ((paramView == null) || (!(paramView.getTag() instanceof FriendViewHolder)))
      paramView = this.inflater.inflate(2130903087, paramViewGroup, false);
    FriendViewHolder localFriendViewHolder = (FriendViewHolder)paramView.getTag();
    if (localFriendViewHolder == null)
    {
      localFriendViewHolder = new FriendViewHolder(null);
      localFriendViewHolder.friendLayout = ((RelativeLayout)paramView.findViewById(2131165331));
      localFriendViewHolder.friendImage = ((ImageView)paramView.findViewById(2131165332));
      localFriendViewHolder.firstNameView = ((TextView)paramView.findViewById(2131165333));
      localFriendViewHolder.lastNameView = ((TextView)paramView.findViewById(2131165334));
      localFriendViewHolder.ratedView = ((TextView)paramView.findViewById(2131165335));
      paramView.setTag(localFriendViewHolder);
    }
    Bitmap localBitmap = paramUser.getThumbnailBitmap(localFriendViewHolder.friendImage);
    if (localBitmap != null)
    {
      localFriendViewHolder.friendImage.setImageBitmap(localBitmap);
      String str1 = paramUser.firstName;
      int i = 0;
      if (str1 != null)
      {
        i = 0 + str1.length();
        localFriendViewHolder.firstNameView.setText(str1);
        localFriendViewHolder.firstNameView.setVisibility(0);
      }
      String str2 = paramUser.lastName;
      if (str2 != null)
      {
        int k = str2.length();
        int m = -15 + (i + k);
        if ((m > 0) && (k > m))
          str2 = str2.substring(0, k - m).concat("...");
        localFriendViewHolder.lastNameView.setText(str2);
        localFriendViewHolder.lastNameView.setVisibility(0);
      }
      int j = paramUser.ratedCount;
      if (j <= 0)
        break label347;
      localFriendViewHolder.ratedView.setText(String.valueOf(j));
      localFriendViewHolder.ratedView.setVisibility(0);
    }
    while (true)
    {
      localFriendViewHolder.friendLayout.setTag(paramUser);
      localFriendViewHolder.friendLayout.setOnClickListener(this.friendClickListener);
      return paramView;
      if (paramUser.isThumbnailUrlAvailable())
        break;
      localFriendViewHolder.friendImage.setImageResource(2130837978);
      break;
      label347: localFriendViewHolder.ratedView.setVisibility(4);
    }
  }

  private View getLetterView(String paramString, View paramView)
  {
    if ((paramView == null) || (!(paramView instanceof TextView)))
      paramView = new SubHeader(this.context);
    ((TextView)paramView).setText(paramString);
    return paramView;
  }

  public int getItemViewType(int paramInt)
  {
    Object localObject = data.get(paramInt);
    if ((localObject instanceof String))
    {
      String str = (String)localObject;
      if ((paramInt == 0) && (str.length() > 1))
        return 1;
      if (str.length() == 1)
        return 8;
      return 2;
    }
    if ((localObject instanceof User))
      return 3;
    if ((localObject instanceof AdView))
      return 7;
    return 0;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    int i = getItemViewType(paramInt);
    Object localObject1 = data.get(paramInt);
    switch (i)
    {
    case 4:
    case 5:
    case 6:
    default:
    case 7:
    case 1:
    case 8:
    case 3:
    case 2:
    }
    while (true)
    {
      Object localObject2 = paramView;
      do
      {
        return localObject2;
        localObject2 = (AdView)paramView;
        if (localObject2 == null)
        {
          localObject2 = new AdView(this.context, "MoviesIveRated");
          ((AdView)localObject2).setGravity(17);
          ((AdView)localObject2).setBackgroundColor(-4473925);
        }
        Logger.i("FlxMain", "FriendsListAdapter.getView: " + localObject2);
      }
      while (!this.mRefreshAd);
      ((AdView)localObject2).refreshAds();
      this.mRefreshAd = false;
      Logger.i("FlxMain", "FriendsListAdapter.refresh: " + localObject2);
      return localObject2;
      paramView = getTitleView((String)localObject1, paramView);
      continue;
      paramView = getLetterView((String)localObject1, paramView);
      continue;
      paramView = getFriendView((User)localObject1, paramView, paramViewGroup);
      continue;
      paramView = getTextView((String)localObject1, paramView);
    }
  }

  public void setAdRefresh(boolean paramBoolean)
  {
    this.mRefreshAd = paramBoolean;
  }

  private static final class FriendViewHolder
  {
    TextView firstNameView;
    ImageView friendImage;
    RelativeLayout friendLayout;
    TextView lastNameView;
    TextView ratedView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FriendsListAdapter
 * JD-Core Version:    0.6.2
 */