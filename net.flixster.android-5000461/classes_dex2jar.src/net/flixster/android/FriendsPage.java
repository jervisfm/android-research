package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ListView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.LastNameComparator;
import net.flixster.android.model.RatedCountComparator;
import net.flixster.android.model.User;

public class FriendsPage extends FlixsterListActivity
{
  private static final int DIALOG_FRIENDS = 2;
  private static final int DIALOG_NETWORK_FAIL = 1;
  public static final String KEY_SORT_FRIENDS = "SORT_FRIENDS";
  public static final int SORT_ALPHABETICAL = 1;
  public static final int SORT_REVIEW_COUNT = 2;
  private Boolean isConnected;
  private ArrayList<Object> mData;
  private ListView mFriendsList;
  private int sortType = 1;
  private Timer timer;
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "FriendsPage.updateHandler");
      if ((FriendsPage.this.user != null) && (FriendsPage.this.user.friends != null))
        FriendsPage.this.updatePage();
      while (FriendsPage.this.isFinishing())
        return;
      FriendsPage.this.showDialog(1);
    }
  };
  private User user;

  private void scheduleUpdatePageTask()
  {
    TimerTask local3 = new TimerTask()
    {
      public void run()
      {
        if ((FriendsPage.this.user == null) || (FriendsPage.this.user.friends == null));
        try
        {
          FriendsPage.this.user = ProfileDao.fetchUser();
          if ((FriendsPage.this.user == null) || (FriendsPage.this.user.id == null) || (FriendsPage.this.user.friends != null));
        }
        catch (DaoException localDaoException1)
        {
          try
          {
            FriendsPage.this.user.friends = ProfileDao.getFriends(FriendsPage.this.user.id, -1);
            FriendsPage.this.updateHandler.sendEmptyMessage(0);
            return;
            localDaoException1 = localDaoException1;
            Logger.e("FlxMain", "FriendsPage.scheduleUpdatePageTask (failed to get user data)", localDaoException1);
            FriendsPage.this.user = null;
          }
          catch (DaoException localDaoException2)
          {
            while (true)
              Logger.e("FlxMain", "FriendsPage.scheduleUpdatePageTask (failed to get review data)", localDaoException2);
          }
        }
      }
    };
    if (this.timer != null)
      this.timer.schedule(local3, 100L);
  }

  private List<?> sortUsers(List<User> paramList)
  {
    if (2 == this.sortType)
    {
      Collections.sort(paramList, new RatedCountComparator());
      return paramList;
    }
    Collections.sort(paramList, new LastNameComparator());
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    Object localObject = "#";
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        if (!localArrayList2.isEmpty())
        {
          localArrayList1.add("#");
          localArrayList1.addAll(localArrayList2);
        }
        return localArrayList1;
      }
      User localUser = (User)localIterator.next();
      if ((localUser.lastName != null) && (localUser.lastName.length() > 0))
      {
        String str = localUser.lastName.toUpperCase().substring(0, 1);
        if (Character.isLetter(str.charAt(0)))
        {
          if (!str.equals(localObject))
          {
            localObject = str;
            localArrayList1.add(localObject);
          }
          localArrayList1.add(localUser);
        }
        else
        {
          localArrayList2.add(localUser);
        }
      }
      else
      {
        localArrayList2.add(localUser);
      }
    }
  }

  private void updatePage()
  {
    this.mData.clear();
    this.mData.add(new AdView(this, "MyFriends"));
    if ((this.user.friends != null) && (!this.user.friends.isEmpty()))
    {
      List localList = sortUsers(this.user.friends);
      this.mData.addAll(localList);
    }
    while (true)
    {
      getListView().invalidateViews();
      removeDialog(2);
      Trackers.instance().track("/friendsmovies/friends", "Friends Page");
      return;
      this.mData.add(getResources().getString(2131493004));
    }
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt2 == -1) && (paramInt1 == 2))
    {
      this.isConnected = Boolean.valueOf(true);
      getIntent().putExtra("net.flixster.IsConnected", Boolean.TRUE);
      return;
    }
    setResult(0);
    finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxMain", "FriendsPage.onCreate");
    createActionBar();
    setActionBarTitle(2131492975);
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.containsKey("SORT_FRIENDS")))
    {
      this.sortType = localBundle.getInt("SORT_FRIENDS");
      if (this.sortType <= 0)
        this.sortType = 1;
    }
    this.mFriendsList = getListView();
    this.mFriendsList.setBackgroundResource(2131296267);
    this.mFriendsList.setCacheColorHint(getResources().getColor(2131296267));
    this.mFriendsList.setDividerHeight(0);
    this.mData = new ArrayList();
    setListAdapter(new FriendsListAdapter(this, this.mData));
    if ((localBundle != null) && (this.isConnected == null))
    {
      Logger.d("FlxMain", "FriendsPage.onCreate has extras");
      this.isConnected = Boolean.valueOf(localBundle.getBoolean("net.flixster.IsConnected"));
      if ((this.isConnected == null) || (!this.isConnected.booleanValue()))
      {
        Intent localIntent = new Intent(this, ConnectRatePage.class);
        localIntent.putExtra("net.flixster.RequestCode", 2);
        startActivityForResult(localIntent, 2);
      }
      return;
    }
    updatePage();
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      ProgressDialog localProgressDialog2 = new ProgressDialog(this);
      localProgressDialog2.setMessage(getResources().getString(2131493173));
      localProgressDialog2.setIndeterminate(true);
      localProgressDialog2.setCancelable(true);
      localProgressDialog2.setCanceledOnTouchOutside(true);
      return localProgressDialog2;
    case 2:
      ProgressDialog localProgressDialog1 = new ProgressDialog(this);
      localProgressDialog1.setMessage(getResources().getString(2131493173));
      localProgressDialog1.setIndeterminate(true);
      localProgressDialog1.setCancelable(true);
      localProgressDialog1.setCanceledOnTouchOutside(true);
      return localProgressDialog1;
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        FriendsPage.this.showDialog(2);
        FriendsPage.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder.create();
  }

  public void onDestroy()
  {
    Logger.d("FlxMain", "FriendsPage.onDestroy");
    super.onDestroy();
    if (this.timer != null)
    {
      this.timer.cancel();
      this.timer.purge();
    }
    this.timer = null;
  }

  protected void onPause()
  {
    Logger.d("FlxMain", "FriendsPage.onPause");
    super.onPause();
  }

  public void onResume()
  {
    Logger.d("FlxMain", "FriendsPage.onResume");
    super.onResume();
    if (!this.isConnected.booleanValue())
      return;
    if (this.timer == null)
      this.timer = new Timer();
    showDialog(2);
    scheduleUpdatePageTask();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FriendsPage
 * JD-Core Version:    0.6.2
 */