package net.flixster.android;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.data.AccountManager;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.FriendActivity;
import com.flixster.android.view.LoadMore;
import java.util.List;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.Review;
import net.flixster.android.model.User;

public class FriendsRatedPage extends DecoratedSherlockActivity
{
  protected static final int MAX_REVIEWS = 10;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.e("FlxMain", "FriendsRatedPage.errorHandler: " + paramAnonymousMessage);
      if ((paramAnonymousMessage.obj instanceof DaoException))
        ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, FriendsRatedPage.this);
    }
  };
  private final Handler friendsActivitySuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      FriendsRatedPage.this.friendsRatedLayout.setVisibility(0);
      FriendsRatedPage.this.friendsRatedLayout.setFocusable(true);
      FriendsRatedPage.this.reviews = AccountManager.instance().getUser().friendsActivity;
      for (int i = 0; ; i++)
      {
        if ((i >= FriendsRatedPage.this.reviews.size()) || (i >= 10))
        {
          if (FriendsRatedPage.this.reviews.size() > 10)
          {
            LoadMore localLoadMore = new LoadMore(FriendsRatedPage.this);
            localLoadMore.setId(2131165404);
            ((TextView)localLoadMore.findViewById(2131165405)).setText(2131492907);
            FriendsRatedPage.this.friendsRatedLayout.addView(localLoadMore);
            String str = FriendsRatedPage.this.getString(2131492993);
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Integer.valueOf(FriendsRatedPage.this.reviews.size());
            localLoadMore.load(null, String.format(str, arrayOfObject));
            localLoadMore.setFocusable(true);
            localLoadMore.setOnClickListener(FriendsRatedPage.this.moreReviewsListener);
          }
          if (FriendsRatedPage.this.reviews.size() == 0)
            ((TextView)FriendsRatedPage.this.findViewById(2131165352)).setVisibility(0);
          return;
        }
        Review localReview = (Review)FriendsRatedPage.this.reviews.get(i);
        FriendActivity localFriendActivity = new FriendActivity(FriendsRatedPage.this);
        localFriendActivity.setFocusable(true);
        FriendsRatedPage.this.friendsRatedLayout.addView(localFriendActivity);
        localFriendActivity.load(localReview);
      }
    }
  };
  private LinearLayout friendsRatedLayout;
  private View.OnClickListener moreReviewsListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      FriendsRatedPage.this.friendsRatedLayout.removeViewAt(10);
      for (int i = 10; ; i++)
      {
        if (i >= FriendsRatedPage.this.reviews.size())
          return;
        Review localReview = (Review)FriendsRatedPage.this.reviews.get(i);
        FriendActivity localFriendActivity = new FriendActivity(FriendsRatedPage.this);
        localFriendActivity.setFocusable(true);
        FriendsRatedPage.this.friendsRatedLayout.addView(localFriendActivity);
        localFriendActivity.load(localReview);
      }
    }
  };
  private List<Review> reviews;
  private User user;

  private void initializeViews()
  {
    Logger.d("FlxMain", "FriendsRatedPage.initializeViews");
    ProfileDao.getFriendsActivity(this.friendsActivitySuccessHandler, this.errorHandler);
    RelativeLayout localRelativeLayout = (RelativeLayout)findViewById(2131165342);
    this.user = AccountFacade.getSocialUser();
    localRelativeLayout.setVisibility(0);
    ((TextView)localRelativeLayout.findViewById(2131165346)).setText(this.user.displayName);
    ((TextView)localRelativeLayout.findViewById(2131165349)).setText(this.user.friendCount + " " + getString(2131492994));
    ((TextView)localRelativeLayout.findViewById(2131165348)).setText(this.user.ratingCount + " " + getString(2131492995));
    ((TextView)localRelativeLayout.findViewById(2131165347)).setText(this.user.wtsCount + " " + getString(2131492996));
    int i = this.user.collectionsCount;
    if (i > 0)
    {
      TextView localTextView = (TextView)localRelativeLayout.findViewById(2131165350);
      localTextView.setText(i + " " + getString(2131492997));
      localTextView.setVisibility(0);
    }
    ImageView localImageView1 = (ImageView)localRelativeLayout.findViewById(2131165344);
    if (AccountManager.instance().isPlatformFlixster())
      localImageView1.setBackgroundResource(2130837692);
    while (true)
    {
      ImageView localImageView2 = (ImageView)localRelativeLayout.findViewById(2131165343);
      Bitmap localBitmap = this.user.getProfileBitmap(localImageView2);
      if (localBitmap != null)
        localImageView2.setImageBitmap(localBitmap);
      return;
      if (AccountManager.instance().isPlatformFacebook())
        localImageView1.setBackgroundResource(2130837833);
    }
  }

  protected String getAnalyticsTag()
  {
    return "/friendsmovies/friendsrated";
  }

  protected String getAnalyticsTitle()
  {
    return "Friends Rated Page for user:" + this.user.id;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903089);
    createActionBar();
    setActionBarTitle(2131493228);
    this.friendsRatedLayout = ((LinearLayout)findViewById(2131165351));
    initializeViews();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    trackPage();
  }

  protected void trackPage()
  {
    Trackers.instance().track(getAnalyticsTag(), getAnalyticsTitle());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.FriendsRatedPage
 * JD-Core Version:    0.6.2
 */