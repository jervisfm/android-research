package net.flixster.android;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.facebook.android.AsyncFacebookRunner;
import com.flixster.android.activity.DeepLink;
import com.flixster.android.activity.TabbedActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.data.PromoDao;
import com.flixster.android.model.CarouselItem;
import com.flixster.android.model.PromoItem;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.LocationFacade;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.StringHelper;
import com.flixster.android.view.Carousel;
import com.flixster.android.view.Headline;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.NetflixDao;
import net.flixster.android.model.Movie;
import net.flixster.android.model.NetflixQueue;
import net.flixster.android.model.NetflixQueueItem;
import net.flixster.android.model.Season;

public class Homepage extends TabbedActivity
{
  private static final int MAX_MOVIES_DISPLAYED = 10;
  private AdView adView;
  private Carousel carousel;
  private final View.OnClickListener carouselOnClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      CarouselItem localCarouselItem = (CarouselItem)paramAnonymousView.getTag();
      String str = localCarouselItem.getLinkUrl();
      Homepage.this.onPromoLinkClick(str);
      Trackers.instance().trackEvent("/homepage", "Homepage", "Homepage", "FeaturedRotator", StringHelper.getFirstTwoWords(localCarouselItem.getTitle()), 0);
    }
  };
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.e("FlxMain", "Homepage.errorHandler: " + paramAnonymousMessage);
      switch (paramAnonymousMessage.what)
      {
      default:
        Homepage.this.throbber.setVisibility(8);
      case 3:
      case 4:
      }
      while (true)
      {
        if ((paramAnonymousMessage.obj instanceof DaoException))
          ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, Homepage.this, Homepage.this.dialogDecorator);
        return;
        Homepage.this.headerNetflixDvd.setVisibility(8);
        Homepage.this.throbberNetflixDvd.setVisibility(8);
        continue;
        Homepage.this.headerNetflixInstant.setVisibility(8);
        Homepage.this.throbberNetflixInstant.setVisibility(8);
      }
    }
  };
  private final List<CarouselItem> featuredItems = new ArrayList();
  private LinearLayout galleryNetflixDvd;
  private LinearLayout galleryNetflixInstant;
  private boolean hasCreated;
  private TextView headerNetflixDvd;
  private TextView headerNetflixInstant;
  private TextView headerNetflixLogin;
  private final View.OnClickListener headlineOnClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      PromoItem localPromoItem = (PromoItem)paramAnonymousView.getTag();
      String str = localPromoItem.getLinkUrl();
      Homepage.this.onPromoLinkClick(str);
      Trackers.instance().trackEvent("/homepage", "Homepage", "Homepage", "HotToday", StringHelper.getFirstTwoWords(localPromoItem.getTitle()), 0);
    }
  };
  private final View.OnClickListener homepageOnClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (FlixsterApplication.getPlatformUsername() != null);
      for (boolean bool = true; ; bool = false)
        switch (paramAnonymousView.getId())
        {
        default:
          return;
        case 2131165386:
        case 2131165387:
        case 2131165404:
        case 2131165380:
        }
      Intent localIntent4 = new Intent(Homepage.this, TopPhotosPage.class);
      localIntent4.putExtra("KEY_PHOTO_FILTER", "top-actresses");
      Homepage.this.startActivity(localIntent4);
      Trackers.instance().trackEvent("/homepage", "Homepage", "Homepage", "FunStuffTopPhotos");
      return;
      Intent localIntent3 = new Intent(Homepage.this, TopActorsPage.class);
      Homepage.this.startActivity(localIntent3);
      Trackers.instance().trackEvent("/homepage", "Homepage", "Homepage", "FunStuffTopActors");
      return;
      Intent localIntent2 = new Intent(Homepage.this, FriendsRatedPage.class);
      localIntent2.putExtra("net.flixster.IsConnected", bool);
      Homepage.this.startActivity(localIntent2);
      Trackers.instance().trackEvent("/homepage", "Homepage", "Homepage", "FriendActivitySeeMore");
      return;
      Trackers.instance().track("/mymovies/netflix/login", "My Movies - Netflix Login");
      Intent localIntent1 = new Intent(Homepage.this, NetflixAuth.class);
      Homepage.this.startActivity(localIntent1);
    }
  };
  private final List<PromoItem> hotTodayItems = new ArrayList();
  private LinearLayout hotTodayLayout;
  private String lastNetflixUserId;
  AsyncFacebookRunner mAsyncRunner;
  private final View.OnClickListener movieGalleryClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if ((paramAnonymousView.getTag() instanceof Movie))
      {
        Movie localMovie = (Movie)paramAnonymousView.getTag();
        if ((localMovie instanceof Season))
        {
          Starter.launchSeasonDetail(localMovie.getId(), Homepage.this);
          return;
        }
        Starter.launchMovieDetail(localMovie.getId(), Homepage.this);
        return;
      }
      Intent localIntent = new Intent(Homepage.this, NetflixQueuePage.class);
      localIntent.setFlags(131072);
      Homepage.this.startActivity(localIntent);
    }
  };
  private final Handler netflixDvdQueueSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Homepage.this.throbberNetflixDvd.setVisibility(8);
      Homepage.this.galleryNetflixDvd.removeAllViews();
      NetflixQueue localNetflixQueue = (NetflixQueue)paramAnonymousMessage.obj;
      int i = 0;
      ArrayList localArrayList = localNetflixQueue.getNetflixQueueItemList();
      if (localArrayList.isEmpty())
      {
        Homepage.this.headerNetflixDvd.setVisibility(8);
        return;
      }
      Iterator localIterator = localArrayList.iterator();
      if (!localIterator.hasNext());
      while (true)
      {
        MovieCollectionItem localMovieCollectionItem2 = new MovieCollectionItem(Homepage.this);
        ((MovieCollectionItem)localMovieCollectionItem2).load(null, 3);
        localMovieCollectionItem2.setTag(Integer.valueOf(3));
        localMovieCollectionItem2.setOnClickListener(Homepage.this.movieGalleryClickListener);
        Homepage.this.galleryNetflixDvd.addView(localMovieCollectionItem2);
        return;
        Movie localMovie = ((NetflixQueueItem)localIterator.next()).mMovie;
        if (localMovie != null)
        {
          MovieCollectionItem localMovieCollectionItem1 = new MovieCollectionItem(Homepage.this);
          ((MovieCollectionItem)localMovieCollectionItem1).load(localMovie, 3);
          localMovieCollectionItem1.setTag(localMovie);
          localMovieCollectionItem1.setOnClickListener(Homepage.this.movieGalleryClickListener);
          Homepage.this.galleryNetflixDvd.addView(localMovieCollectionItem1);
          i++;
        }
        if (i < 10)
          break;
      }
    }
  };
  private final Handler netflixInstantQueueSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Homepage.this.throbberNetflixInstant.setVisibility(8);
      Homepage.this.galleryNetflixInstant.removeAllViews();
      NetflixQueue localNetflixQueue = (NetflixQueue)paramAnonymousMessage.obj;
      int i = 0;
      ArrayList localArrayList = localNetflixQueue.getNetflixQueueItemList();
      if (localArrayList.isEmpty())
      {
        Homepage.this.headerNetflixInstant.setVisibility(8);
        return;
      }
      Iterator localIterator = localArrayList.iterator();
      if (!localIterator.hasNext());
      while (true)
      {
        MovieCollectionItem localMovieCollectionItem2 = new MovieCollectionItem(Homepage.this);
        ((MovieCollectionItem)localMovieCollectionItem2).load(null, 4);
        localMovieCollectionItem2.setTag(Integer.valueOf(4));
        localMovieCollectionItem2.setOnClickListener(Homepage.this.movieGalleryClickListener);
        Homepage.this.galleryNetflixInstant.addView(localMovieCollectionItem2);
        return;
        Movie localMovie = ((NetflixQueueItem)localIterator.next()).mMovie;
        if (localMovie != null)
        {
          MovieCollectionItem localMovieCollectionItem1 = new MovieCollectionItem(Homepage.this);
          ((MovieCollectionItem)localMovieCollectionItem1).load(localMovie, 4);
          localMovieCollectionItem1.setTag(localMovie);
          localMovieCollectionItem1.setOnClickListener(Homepage.this.movieGalleryClickListener);
          Homepage.this.galleryNetflixInstant.addView(localMovieCollectionItem1);
          i++;
        }
        if (i < 10)
          break;
      }
    }
  };
  private TextView netflixLoginDescription;
  private final Handler promoSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "Homepage.promoSuccessHandler");
      Homepage.this.throbber.setVisibility(8);
      Homepage.this.carousel.load(Homepage.this.featuredItems, Homepage.this.carouselOnClickListener);
      Iterator localIterator = Homepage.this.hotTodayItems.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          Homepage.this.checkAndShowLaunchAd();
          return;
        }
        PromoItem localPromoItem = (PromoItem)localIterator.next();
        Headline localHeadline = new Headline(Homepage.this);
        localHeadline.setFocusable(true);
        Homepage.this.hotTodayLayout.addView(localHeadline);
        localHeadline.setTag(localPromoItem);
        localHeadline.setOnClickListener(Homepage.this.headlineOnClickListener);
        localHeadline.load(localPromoItem);
      }
    }
  };
  private ProgressBar throbber;
  private ProgressBar throbberNetflixDvd;
  private ProgressBar throbberNetflixInstant;

  private void initializeNetflixViews()
  {
    ImageView localImageView1 = (ImageView)findViewById(2131165380);
    if ((LocationFacade.isUsLocale()) || (LocationFacade.isCanadaLocale()))
    {
      localImageView1.setOnClickListener(this.homepageOnClickListener);
      TextView localTextView = (TextView)findViewById(2131165385);
      ImageView localImageView2 = (ImageView)findViewById(2131165384);
      String str = AccountFacade.getNetflixUserId();
      this.lastNetflixUserId = str;
      if ((str != null) && (FlixsterApplication.isConnected()))
      {
        this.headerNetflixDvd.setVisibility(0);
        this.throbberNetflixDvd.setVisibility(0);
        NetflixDao.fetchQueue(this.netflixDvdQueueSuccessHandler, this.errorHandler, "/queues/disc/available");
        this.headerNetflixInstant.setVisibility(0);
        this.throbberNetflixInstant.setVisibility(0);
        NetflixDao.fetchQueue(this.netflixInstantQueueSuccessHandler, this.errorHandler, "/queues/instant/available");
        this.headerNetflixLogin.setVisibility(8);
        localTextView.setText(getResources().getString(2131493072));
        localImageView2.setImageResource(2130837818);
        localImageView1.setVisibility(8);
        this.netflixLoginDescription.setVisibility(8);
        return;
      }
      this.headerNetflixDvd.setVisibility(8);
      this.headerNetflixInstant.setVisibility(8);
      this.galleryNetflixDvd.removeAllViews();
      this.galleryNetflixInstant.removeAllViews();
      this.headerNetflixLogin.setVisibility(0);
      localImageView1.setVisibility(0);
      this.netflixLoginDescription.setVisibility(0);
      return;
    }
    this.headerNetflixDvd.setVisibility(8);
    this.headerNetflixInstant.setVisibility(8);
    this.headerNetflixLogin.setVisibility(8);
    localImageView1.setVisibility(8);
    this.netflixLoginDescription.setVisibility(8);
  }

  private void initializeStaticViews()
  {
    Logger.d("FlxMain", "Homepage.initializeStaticViews");
    TextView localTextView1 = (TextView)findViewById(2131165386);
    localTextView1.setOnClickListener(this.homepageOnClickListener);
    localTextView1.setFocusable(true);
    TextView localTextView2 = (TextView)findViewById(2131165387);
    localTextView2.setOnClickListener(this.homepageOnClickListener);
    localTextView2.setFocusable(true);
    ((TextView)findViewById(2131165388)).setVisibility(8);
    initializeNetflixViews();
  }

  private void onPromoLinkClick(String paramString)
  {
    if (DeepLink.isValid(paramString))
    {
      DeepLink.launch(paramString, this);
      return;
    }
    Starter.launchFlxHtmlPage(paramString, getString(2131493173), this);
  }

  private void onRealCreate()
  {
    if (!this.hasCreated)
    {
      this.hasCreated = true;
      Logger.d("FlxMain", "Homepage.onRealCreate");
      setContentView(2130903093);
      this.adView = ((AdView)findViewById(2131165369));
      this.adView.setSlot("Homepage");
      this.carousel = ((Carousel)findViewById(2131165370));
      this.throbber = ((ProgressBar)findViewById(2131165241));
      this.hotTodayLayout = ((LinearLayout)findViewById(2131165371));
      this.headerNetflixDvd = ((TextView)findViewById(2131165372));
      this.headerNetflixInstant = ((TextView)findViewById(2131165375));
      this.headerNetflixLogin = ((TextView)findViewById(2131165378));
      this.netflixLoginDescription = ((TextView)findViewById(2131165379));
      this.throbberNetflixDvd = ((ProgressBar)findViewById(2131165373));
      this.throbberNetflixInstant = ((ProgressBar)findViewById(2131165376));
      this.galleryNetflixDvd = ((LinearLayout)findViewById(2131165374));
      this.galleryNetflixInstant = ((LinearLayout)findViewById(2131165377));
      this.featuredItems.clear();
      this.hotTodayItems.clear();
      this.throbber.setVisibility(0);
      PromoDao.get(this.featuredItems, this.hotTodayItems, this.promoSuccessHandler, this.errorHandler);
      initializeStaticViews();
    }
  }

  protected String getAnalyticsAction()
  {
    return "Logo";
  }

  protected String getAnalyticsCategory()
  {
    return "WidgetEntrance";
  }

  protected String getAnalyticsTag()
  {
    return "/homepage";
  }

  protected String getAnalyticsTitle()
  {
    return "Homepage";
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxMain", "Homepage.onDummyCreate");
  }

  public void onDestroy()
  {
    if (this.adView != null)
      this.adView.destroy();
    super.onDestroy();
  }

  protected void onResume()
  {
    onRealCreate();
    super.onResume();
    Logger.d("FlxMain", "Homepage.onResume");
    String str = AccountFacade.getNetflixUserId();
    if (((str == null) && (this.lastNetflixUserId != null)) || ((str != null) && (!str.equals(this.lastNetflixUserId))))
    {
      this.lastNetflixUserId = str;
      initializeNetflixViews();
    }
    trackPage();
    this.adView.refreshAds();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.Homepage
 * JD-Core Version:    0.6.2
 */