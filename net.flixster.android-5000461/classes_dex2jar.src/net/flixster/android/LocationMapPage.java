package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.flixster.android.activity.common.DecoratedSherlockMapActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.LocationDao;
import net.flixster.android.model.Location;

public class LocationMapPage extends DecoratedSherlockMapActivity
  implements View.OnClickListener, TextView.OnEditorActionListener
{
  private static final int DIALOG_LOCATION = 2;
  private static final int DIALOG_LOCATION_CHOICES = 3;
  private static final int DIALOG_NETWORK_FAIL = 1;
  private static final int MAP_ZOOM_DEFAULT = 16;
  private Pattern geoPointPattern = Pattern.compile("[(][-+]?[0-9]*\\.?[0-9]*[,] [-+]?[0-9]*\\.?[0-9]*[)]");
  private Handler locationSelectionHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      LocationMapPage.this.locations = ((ArrayList)paramAnonymousMessage.obj);
      if (LocationMapPage.this.locations.isEmpty())
        Toast.makeText(LocationMapPage.this.getApplicationContext(), "Please type a more specific location and try again", 0).show();
      do
      {
        return;
        if (LocationMapPage.this.locations.size() == 1)
        {
          Location localLocation = (Location)LocationMapPage.this.locations.get(0);
          FlixsterApplication.setUserLatitude(localLocation.latitude);
          FlixsterApplication.setUserLongitude(localLocation.longitude);
          FlixsterApplication.setUserZip(localLocation.zip);
          FlixsterApplication.setUserCity(localLocation.city);
          FlixsterApplication.setUserLocation(LocationMapPage.this.getLocationItemDisplay(localLocation));
          FlixsterApplication.setUseLocationServiceFlag(false);
          FlixsterApplication.setLocationPolicy(0);
          LocationMapPage.this.setResult(-1, LocationMapPage.this.getIntent());
          LocationMapPage.this.finish();
          return;
        }
      }
      while ((LocationMapPage.this.locations.size() <= 1) || (LocationMapPage.this.isFinishing()));
      LocationMapPage.this.showDialog(3);
    }
  };
  private TextView locationView;
  private ArrayList<Location> locations;
  private Button mGoButton;
  private TextView mLocationLabel;
  private EditText mLocationText;
  private MapController mapController;
  private MapView mapView;
  private Timer timer;
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "LocationPage.updateHandler");
      LocationMapPage.this.showDialog(2);
      LocationMapPage.this.updatePage();
      LocationMapPage.this.removeDialog(2);
    }
  };

  private String getLocationItemDisplay(Location paramLocation)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((paramLocation.city != null) && (!"".equals(paramLocation.city)))
      localStringBuilder.append(paramLocation.city);
    if ((paramLocation.stateCode != null) && (!"".equals(paramLocation.stateCode)))
    {
      if (localStringBuilder.length() > 0)
        localStringBuilder.append(", ");
      localStringBuilder.append(paramLocation.stateCode);
    }
    if ((paramLocation.zip != null) && (!"".equals(paramLocation.zip)))
    {
      if (localStringBuilder.length() > 0)
        localStringBuilder.append(", ");
      localStringBuilder.append(paramLocation.zip);
    }
    if ((paramLocation.country != null) && (!"".equals(paramLocation.country)) && (!"United States".equals(paramLocation.country)))
    {
      if (localStringBuilder.indexOf(",") < 0)
        localStringBuilder.append(",");
      localStringBuilder.append(" ").append(paramLocation.country);
    }
    return localStringBuilder.toString();
  }

  private void locationSelection()
  {
    final String str = this.mLocationText.getText().toString().trim();
    new Thread()
    {
      public void run()
      {
        try
        {
          if (this.val$isGeoPointMatched);
          ArrayList localArrayList;
          for (Object localObject = LocationDao.getLocations(this.val$latitude, str); localObject != null; localObject = localArrayList)
          {
            Message localMessage = Message.obtain();
            localMessage.obj = localObject;
            LocationMapPage.this.locationSelectionHandler.sendMessage(localMessage);
            return;
            localArrayList = LocationDao.getLocations(this.val$query);
          }
        }
        catch (DaoException localDaoException)
        {
          Logger.e("FlxMain", "problem retrieving location", localDaoException);
        }
      }
    }
    .start();
  }

  private void showCurrentLocation(double paramDouble1, double paramDouble2, String paramString)
  {
    if ((paramDouble1 == 0.0D) && (paramDouble2 == 0.0D))
    {
      Toast.makeText(getApplicationContext(), "Please enable a My Location source in system settings", 0).show();
      return;
    }
    NumberFormat.getInstance().setMaximumFractionDigits(4);
    try
    {
      this.mLocationLabel.setText(paramString);
      GeoPoint localGeoPoint = new GeoPoint((int)(1000000.0D * paramDouble1), (int)(1000000.0D * paramDouble2));
      this.mapController.setCenter(localGeoPoint);
      TheaterItemizedOverlay localTheaterItemizedOverlay = new TheaterItemizedOverlay(this, getResources().getDrawable(2130837677));
      localTheaterItemizedOverlay.addOverlay(new OverlayItem(localGeoPoint, "Current Location", null));
      List localList = this.mapView.getOverlays();
      localList.clear();
      localList.add(localTheaterItemizedOverlay);
      this.mapView.invalidate();
      this.locationView.setVisibility(8);
      return;
    }
    catch (Exception localException)
    {
      Logger.e("problem retrieving location", localException.getMessage());
    }
  }

  private void updatePage()
  {
    double d1;
    double d2;
    if (FlixsterApplication.getUseLocationServiceFlag())
    {
      d1 = FlixsterApplication.getCurrentLatitude();
      d2 = FlixsterApplication.getCurrentLongitude();
    }
    for (String str = FlixsterApplication.getCurrentLocationDisplay(); ; str = FlixsterApplication.getCurrentLocationDisplay())
    {
      do
      {
        showCurrentLocation(d1, d2, str);
        return;
        d1 = FlixsterApplication.getUserLatitude();
        d2 = FlixsterApplication.getUserLongitude();
        str = FlixsterApplication.getUserLocation();
      }
      while ((d1 != 0.0D) || (d2 != 0.0D));
      d1 = FlixsterApplication.getCurrentLatitude();
      d2 = FlixsterApplication.getCurrentLongitude();
    }
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    case 2131165408:
    default:
      return;
    case 2131165409:
    }
    locationSelection();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903105);
    createActionBar();
    setActionBarTitle(2131492930);
    this.mLocationLabel = ((TextView)findViewById(2131165407));
    this.mLocationText = ((EditText)findViewById(2131165408));
    this.mLocationLabel.setText("?");
    this.mLocationText.setHint(getResources().getString(2131493168));
    this.mLocationText.setOnEditorActionListener(this);
    this.locationView = ((TextView)findViewById(2131165410));
    this.mapView = ((MapView)findViewById(2131165412));
    ((LinearLayout)findViewById(2131165413)).addView(this.mapView.getZoomControls());
    this.mGoButton = ((Button)findViewById(2131165409));
    this.mGoButton.setOnClickListener(this);
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      ProgressDialog localProgressDialog2 = new ProgressDialog(this);
      localProgressDialog2.setMessage(getResources().getString(2131493173));
      localProgressDialog2.setIndeterminate(true);
      localProgressDialog2.setCancelable(true);
      localProgressDialog2.setCanceledOnTouchOutside(true);
      return localProgressDialog2;
    case 2:
      ProgressDialog localProgressDialog1 = new ProgressDialog(this);
      localProgressDialog1.setMessage(getResources().getString(2131493173));
      localProgressDialog1.setIndeterminate(true);
      localProgressDialog1.setCancelable(true);
      localProgressDialog1.setCanceledOnTouchOutside(true);
      return localProgressDialog1;
    case 3:
      AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(this);
      localBuilder2.setTitle("Select a Location");
      CharSequence[] arrayOfCharSequence = new CharSequence[this.locations.size()];
      for (int i = 0; ; i++)
      {
        if (i >= this.locations.size())
        {
          localBuilder2.setItems(arrayOfCharSequence, new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              Location localLocation = (Location)LocationMapPage.this.locations.get(paramAnonymousInt);
              FlixsterApplication.setUserLatitude(localLocation.latitude);
              FlixsterApplication.setUserLongitude(localLocation.longitude);
              FlixsterApplication.setUserZip(localLocation.zip);
              FlixsterApplication.setUserCity(localLocation.city);
              FlixsterApplication.setUserLocation(LocationMapPage.this.getLocationItemDisplay(localLocation));
              FlixsterApplication.setUseLocationServiceFlag(false);
              LocationMapPage.this.setResult(-1, LocationMapPage.this.getIntent());
              FlixsterApplication.setLocationPolicy(0);
              LocationMapPage.this.finish();
            }
          });
          localBuilder2.setNegativeButton(getResources().getString(2131492938), new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              LocationMapPage.this.removeDialog(3);
            }
          });
          return localBuilder2.create();
        }
        arrayOfCharSequence[i] = getLocationItemDisplay((Location)this.locations.get(i));
      }
    case 1:
    }
    AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(this);
    localBuilder1.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder1.setTitle("Network Error");
    localBuilder1.setCancelable(false);
    localBuilder1.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        LocationMapPage.this.updateHandler.sendEmptyMessage(0);
      }
    });
    localBuilder1.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder1.create();
  }

  public void onDestroy()
  {
    Logger.d("FlxMain", "LocationPage.onDestroy");
    super.onDestroy();
    if (this.timer != null)
    {
      this.timer.cancel();
      this.timer.purge();
    }
    this.timer = null;
  }

  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    locationSelection();
    return false;
  }

  public void onResume()
  {
    super.onResume();
    if (this.timer == null)
      this.timer = new Timer();
    this.mapController = this.mapView.getController();
    this.mapController.setZoom(16);
    this.mapView.displayZoomControls(true);
    Trackers.instance().track("/showtimes/location", "Movie - ");
    this.updateHandler.sendEmptyMessage(0);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.LocationMapPage
 * JD-Core Version:    0.6.2
 */