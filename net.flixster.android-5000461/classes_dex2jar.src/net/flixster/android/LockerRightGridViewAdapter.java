package net.flixster.android;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.List;
import net.flixster.android.model.LockerRight;

public class LockerRightGridViewAdapter extends BaseAdapter
{
  private final Context context;
  private final List<LockerRight> rights;

  LockerRightGridViewAdapter(Context paramContext, List<LockerRight> paramList)
  {
    this.context = paramContext;
    this.rights = paramList;
  }

  public int getCount()
  {
    return this.rights.size();
  }

  public Object getItem(int paramInt)
  {
    return this.rights.get(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if ((paramView == null) || (!(paramView instanceof MovieCollectionItem)))
      paramView = new MovieCollectionItem(this.context);
    while (true)
    {
      ((MovieCollectionItem)paramView).load((LockerRight)this.rights.get(paramInt));
      return paramView;
      ((MovieCollectionItem)paramView).reset();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.LockerRightGridViewAdapter
 * JD-Core Version:    0.6.2
 */