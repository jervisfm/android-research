package net.flixster.android;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ListView;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.ads.AdsArbiter;
import com.flixster.android.ads.AdsArbiter.TrailerAdsArbiter;
import com.flixster.android.ads.DfpPostTrailerInterstitial;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import net.flixster.android.ads.AdManager;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.MovieDao;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviAd;
import net.flixster.android.lvi.LviAdapter;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviShowtimes;
import net.flixster.android.lvi.LviTheater.TheaterItemHolder;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Showtimes;
import net.flixster.android.model.Theater;

public class LviActivityCopy extends FlixsterActivityCopy
{
  protected static final int HIDE_THROBBER = 0;
  public static final String LISTSTATE_NAV = "LISTSTATE_NAV";
  protected static final int SHOW_THROBBER = 1;
  protected LviAd lviAd;
  protected final ArrayList<Lvi> mData = new ArrayList();
  protected final ArrayList<Lvi> mDataHolder = new ArrayList();
  protected ListView mListView;
  protected LviAdapter mLviListPageAdapter;
  private AdapterView.OnItemClickListener mMovieItemClickListener;
  protected int mPositionLast;
  protected boolean mPositionRecover;
  private View.OnClickListener mShowtimesClickListener = null;
  private View.OnClickListener mStarTheaterClickListener = null;
  private View.OnClickListener mTheaterClickListener = null;
  protected View.OnClickListener mTrailerClick;
  protected final Handler mUpdateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!LviActivityCopy.this.isFinishing())
      {
        int i = LviActivityCopy.this.mDataHolder.size();
        Logger.d("FlxMain", LviActivityCopy.this.className + ".mUpdateHandler mDataHolder.size: " + i);
        if ((i <= 0) || (LviActivityCopy.this.topLevelDecorator.isPausing()))
          break label128;
        LviActivityCopy.this.mData.clear();
        LviActivityCopy.this.mData.addAll(LviActivityCopy.this.mDataHolder);
        LviActivityCopy.this.mDataHolder.clear();
        LviActivityCopy.this.mLviListPageAdapter.notifyDataSetChanged();
        LviActivityCopy.this.refreshSticky();
      }
      label128: 
      while (!LviActivityCopy.this.topLevelDecorator.isPausing())
        return;
      Logger.w("FlxMain", LviActivityCopy.this.className + ".mUpdateHandler pausing");
    }
  };
  private View throbber;
  protected final Handler throbberHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
        LviActivityCopy.this.hideLoading();
        return;
      case 1:
      }
      LviActivityCopy.this.showLoading();
    }
  };
  private View throbberLayout;

  private void hideLoading()
  {
    this.throbber.setVisibility(8);
    this.throbberLayout.setVisibility(8);
    this.mListView.setVisibility(0);
  }

  private void showLoading()
  {
    this.mListView.setVisibility(8);
    this.throbberLayout.setVisibility(0);
    this.throbber.setVisibility(0);
  }

  protected void destroyExistingLviAd()
  {
    if (this.lviAd != null)
      this.lviAd.destroy();
  }

  protected AdapterView.OnItemClickListener getMovieItemClickListener()
  {
    try
    {
      if (this.mMovieItemClickListener == null)
        this.mMovieItemClickListener = new AdapterView.OnItemClickListener()
        {
          public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
          {
            Lvi localLvi = (Lvi)LviActivityCopy.this.mData.get(paramAnonymousInt);
            if (localLvi.getItemViewType() == Lvi.VIEW_TYPE_MOVIE)
            {
              Movie localMovie = ((LviMovie)localLvi).mMovie;
              if (localMovie != null)
              {
                if (localMovie.getIntProperty("featuredId") != null)
                  AdManager.instance().trackEvent(localMovie.getIntProperty("featuredId").intValue(), "Click", "MoviesTab");
                LviActivityCopy.this.mPositionLast = LviActivityCopy.this.mListView.getFirstVisiblePosition();
                LviActivityCopy.this.mPositionRecover = true;
                localIntent2 = new Intent("DETAILS", null, LviActivityCopy.this, MovieDetails.class);
                localIntent2.putExtra("net.flixster.android.EXTRA_MOVIE_ID", localMovie.getId());
                LviActivityCopy.this.startActivity(localIntent2);
              }
              while (FlixsterApplication.getMovieRatingType() != 1)
              {
                Intent localIntent2;
                return;
              }
              Intent localIntent1 = new Intent("android.intent.action.VIEW", Uri.parse(LviActivityCopy.this.getResources().getString(2131492948)));
              LviActivityCopy.this.startActivity(localIntent1);
              return;
            }
            localLvi.getItemViewType();
          }
        };
      AdapterView.OnItemClickListener localOnItemClickListener = this.mMovieItemClickListener;
      return localOnItemClickListener;
    }
    finally
    {
    }
  }

  protected View.OnClickListener getShowtimesClickListener()
  {
    try
    {
      if (this.mShowtimesClickListener == null)
        this.mShowtimesClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            Logger.d("FlxMain", LviActivityCopy.this.className + ".getShowtimesClickListener.onClick");
            LviShowtimes localLviShowtimes = (LviShowtimes)paramAnonymousView.getTag();
            if (localLviShowtimes != null)
            {
              Intent localIntent = new Intent(LviActivityCopy.this, TicketInfoPage.class);
              localIntent.putExtra("MOVIE_ID", localLviShowtimes.mShowtimes.mMovieId);
              localIntent.putExtra("id", localLviShowtimes.mTheater.getId());
              localIntent.putExtra("SHOWTIMES_TITLE", localLviShowtimes.mShowtimes.mShowtimesTitle);
              LviActivityCopy.this.startActivity(localIntent);
            }
          }
        };
      View.OnClickListener localOnClickListener = this.mShowtimesClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected View.OnClickListener getStarTheaterClickListener()
  {
    try
    {
      if (this.mStarTheaterClickListener == null)
        this.mStarTheaterClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            CheckBox localCheckBox = (CheckBox)paramAnonymousView;
            Theater localTheater = (Theater)paramAnonymousView.getTag();
            String str;
            if (localTheater != null)
            {
              str = Long.toString(localTheater.getId());
              if (localCheckBox.isChecked())
              {
                Logger.v("FlxMain", LviActivityCopy.this.className + ".StarTheaterListener selected");
                FlixsterApplication.addFavoriteTheater(str);
              }
            }
            else
            {
              return;
            }
            Logger.v("FlxMain", LviActivityCopy.this.className + ".StarTheaterListener unselected");
            FlixsterApplication.removeFavoriteTheater(str);
          }
        };
      View.OnClickListener localOnClickListener = this.mStarTheaterClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected View.OnClickListener getTheaterClickListener()
  {
    try
    {
      if (this.mTheaterClickListener == null)
        this.mTheaterClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            LviTheater.TheaterItemHolder localTheaterItemHolder = (LviTheater.TheaterItemHolder)paramAnonymousView.getTag();
            if (localTheaterItemHolder != null)
            {
              Intent localIntent = new Intent(LviActivityCopy.this, TheaterInfoPage.class);
              localIntent.putExtra("net.flixster.android.EXTRA_THEATER_ID", localTheaterItemHolder.theater.getId());
              LviActivityCopy.this.startActivity(localIntent);
            }
          }
        };
      View.OnClickListener localOnClickListener = this.mTheaterClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected View.OnClickListener getTrailerOnClickListener()
  {
    try
    {
      if (this.mTrailerClick == null)
        this.mTrailerClick = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            LviActivityCopy.this.mPositionLast = LviActivityCopy.this.mListView.getFirstVisiblePosition();
            LviActivityCopy.this.mPositionRecover = true;
            Logger.d("FlxMain", LviActivityCopy.this.className + ".mTrailerClick.onClick");
            Movie localMovie = (Movie)paramAnonymousView.getTag();
            if ((localMovie != null) && (localMovie.hasTrailer()))
              Starter.launchTrailerForResult(localMovie, LviActivityCopy.this, (int)localMovie.getId());
          }
        };
      View.OnClickListener localOnClickListener = this.mTrailerClick;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (AdsArbiter.trailer().shouldShowPostitial())
    {
      AdsArbiter.trailer().postitialShown();
      new DfpPostTrailerInterstitial().loadAd(this, MovieDao.getMovie(paramInt1));
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903113);
    this.throbber = findViewById(2131165241);
    this.throbberLayout = findViewById(2131165435);
    this.mStickyTopAd = ((AdView)findViewById(2131165434));
    this.mStickyBottomAd = ((AdView)findViewById(2131165436));
    this.mListView = ((ListView)findViewById(2131165428));
    this.mListView.setDividerHeight(0);
    this.mLviListPageAdapter = new LviAdapter(this, this.mData);
    this.mListView.setAdapter(this.mLviListPageAdapter);
  }

  protected void onDestroy()
  {
    this.mListView.clearFocus();
    this.mListView.removeAllViewsInLayout();
    destroyExistingLviAd();
    super.onDestroy();
  }

  public void onPause()
  {
    super.onPause();
    this.mData.clear();
    this.mDataHolder.clear();
    this.mLviListPageAdapter.notifyDataSetInvalidated();
    getIntent().putExtra("PositionLast", this.mPositionLast);
    getIntent().putExtra("PositionRecover", this.mPositionRecover);
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    Logger.d("FlxMain", this.className + ".onRestoreInstanceState");
    this.mPositionLast = paramBundle.getInt("LISTSTATE_POSITION");
    getIntent().putExtra("PositionLast", this.mPositionLast);
    this.mPositionRecover = true;
  }

  public void onResume()
  {
    super.onResume();
    this.mPositionLast = getIntent().getIntExtra("PositionLast", 0);
    this.mPositionRecover = getIntent().getBooleanExtra("PositionRecover", true);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if ((this.mListView != null) && (this.mListView.getCount() > 1))
    {
      this.mPositionLast = this.mListView.getFirstVisiblePosition();
      this.mPositionRecover = true;
      paramBundle.putInt("LISTSTATE_POSITION", this.mPositionLast);
    }
  }

  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    this.mLviListPageAdapter.notifyDataSetChanged();
    if ((paramBoolean) && (this.mPositionRecover))
    {
      this.mListView.setSelection(this.mPositionLast);
      if (this.mListView.getCount() > 1)
        this.mPositionRecover = false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.LviActivityCopy
 * JD-Core Version:    0.6.2
 */