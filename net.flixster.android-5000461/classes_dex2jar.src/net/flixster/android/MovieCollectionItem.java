package net.flixster.android;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.drm.DownloadRight;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackLogic;
import com.flixster.android.net.DownloadHelper;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;

public class MovieCollectionItem extends RelativeLayout
{
  public static final int MOVIE_TYPE_COLLECTION = 0;
  public static final int MOVIE_TYPE_NETFLIXDVD = 3;
  public static final int MOVIE_TYPE_NETFLIXINSTANT = 4;
  public static final int MOVIE_TYPE_RATED = 2;
  public static final int MOVIE_TYPE_WTS = 1;
  private final Context context;
  private ProgressBar downloadProgress;
  private final Handler downloadSizeHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      int i = ((Integer)paramAnonymousMessage.obj).intValue();
      MovieCollectionItem.this.right.setDownloadAssetSize(i);
    }
  };
  private TextView downloadStatus;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      (paramAnonymousMessage.obj instanceof DaoException);
    }
  };
  private ImageView poster;
  final Handler progressHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((MovieCollectionItem.this.right != null) && ((DownloadHelper.isMovieDownloadInProgress(MovieCollectionItem.this.right)) || (DownloadHelper.isDownloaded(MovieCollectionItem.this.right)) || (MovieCollectionItem.this.downloadStatus.getVisibility() == 0)))
        MovieCollectionItem.this.setDownloadControls();
    }
  };
  private ImageView rating;
  private final Handler ratingSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Review localReview = (Review)paramAnonymousMessage.obj;
      if ((localReview != null) && (localReview.stars != 0.0D))
      {
        MovieCollectionItem.this.rating.setImageResource(Flixster.RATING_SMALL_R[((int)(2.0D * localReview.stars))]);
        MovieCollectionItem.this.rating.setVisibility(0);
      }
    }
  };
  private TextView rentalExpiration;
  private LockerRight right;
  private TextView title;

  public MovieCollectionItem(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903120, this);
    setBackgroundResource(17301602);
    this.poster = ((ImageView)findViewById(2131165459));
    this.downloadProgress = ((ProgressBar)findViewById(2131165460));
    this.title = ((TextView)findViewById(2131165461));
    this.rentalExpiration = ((TextView)findViewById(2131165463));
    this.downloadStatus = ((TextView)findViewById(2131165464));
    this.rating = ((ImageView)findViewById(2131165462));
  }

  private void setDownloadControls()
  {
    if (((this.right.isMovie()) && (this.right.isDownloadSupported)) || (this.right.isSeason()))
    {
      if ((DownloadHelper.isMovieDownloadInProgress(this.right.rightId)) && (!this.right.isSeason()))
      {
        String str1 = this.right.getDownloadAssetSize();
        if ((str1 == null) || ("".equals(str1)))
        {
          String str2 = this.right.getDownloadUri();
          if (str2 == null)
            str2 = new DownloadRight(this.right.rightId).getDownloadUri();
          DownloadHelper.getRemoteFileSize(str2, this.downloadSizeHandler);
          this.downloadProgress.setVisibility(8);
          this.downloadStatus.setVisibility(4);
          return;
        }
        int i = (int)(100L * DownloadHelper.findDownloadedMovieSize(this.right.rightId) / this.right.getDownloadAssetSizeRaw());
        this.downloadProgress.setProgress(i);
        this.downloadProgress.setMax(100);
        this.downloadProgress.setVisibility(0);
        this.downloadStatus.setText(DownloadHelper.getMovieDownloadProgress(this.right));
        this.downloadStatus.setCompoundDrawables(null, null, null, null);
        this.downloadStatus.setVisibility(0);
        return;
      }
      if (DownloadHelper.isDownloaded(this.right))
      {
        this.downloadProgress.setVisibility(8);
        this.downloadStatus.setText(getResources().getString(2131493264, new Object[] { "" }));
        Drawable localDrawable = getResources().getDrawable(2130837681);
        localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
        this.downloadStatus.setCompoundDrawables(localDrawable, null, null, null);
        this.downloadStatus.setVisibility(0);
        return;
      }
      this.downloadProgress.setVisibility(8);
      this.downloadStatus.setVisibility(4);
      return;
    }
    this.downloadProgress.setVisibility(8);
    this.downloadStatus.setVisibility(4);
  }

  public void load(LockerRight paramLockerRight)
  {
    this.rating.setVisibility(8);
    this.right = paramLockerRight;
    if ((paramLockerRight == null) || (paramLockerRight.getAsset() == null))
    {
      this.poster.setImageResource(2130837980);
      this.title.setText("");
      this.downloadProgress.setVisibility(8);
      this.downloadStatus.setVisibility(4);
      return;
    }
    if (paramLockerRight.getProfilePoster() == null)
    {
      this.poster.setImageResource(2130837839);
      this.title.setText(paramLockerRight.getTitle());
      String str1 = paramLockerRight.getViewingExpirationString();
      if (str1 == null)
        break label169;
      this.rentalExpiration.setText(str1);
      this.rentalExpiration.setTextColor(getResources().getColor(2131296299));
      this.rentalExpiration.setVisibility(0);
    }
    while (true)
    {
      if (!Drm.logic().isDeviceDownloadCompatible())
        break label230;
      setDownloadControls();
      return;
      Bitmap localBitmap = paramLockerRight.getProfileBitmap(this.poster);
      if (localBitmap == null)
        break;
      this.poster.setImageBitmap(localBitmap);
      break;
      label169: String str2 = paramLockerRight.getRentalExpirationString(true);
      if (str2 != null)
      {
        this.rentalExpiration.setText(str2);
        this.rentalExpiration.setTextColor(getResources().getColor(2131296286));
        this.rentalExpiration.setVisibility(0);
      }
      else
      {
        this.rentalExpiration.setVisibility(8);
      }
    }
    label230: this.downloadProgress.setVisibility(8);
    this.downloadStatus.setVisibility(4);
  }

  public void load(Movie paramMovie, int paramInt)
  {
    this.downloadProgress.setVisibility(8);
    if (paramMovie == null)
    {
      this.poster.setImageResource(2130837980);
      this.title.setText("");
      this.rentalExpiration.setVisibility(8);
      this.downloadStatus.setVisibility(4);
      this.rating.setVisibility(8);
      return;
    }
    if (paramMovie.getProfilePoster() == null)
      this.poster.setImageResource(2130837839);
    while (true)
    {
      this.title.setText(paramMovie.getTitle());
      this.rentalExpiration.setVisibility(8);
      this.downloadStatus.setVisibility(8);
      if (paramInt != 2)
        break;
      ProfileDao.getUserMovieReview(this.ratingSuccessHandler, this.errorHandler, Long.toString(paramMovie.getId()));
      return;
      Bitmap localBitmap = paramMovie.getProfileBitmap(this.poster);
      if (localBitmap != null)
        this.poster.setImageBitmap(localBitmap);
    }
    this.rating.setVisibility(8);
  }

  public void reset()
  {
    this.poster.setImageResource(2130837844);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.MovieCollectionItem
 * JD-Core Version:    0.6.2
 */