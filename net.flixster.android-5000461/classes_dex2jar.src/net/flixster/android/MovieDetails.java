package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.text.style.StyleSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.ads.AdsArbiter;
import com.flixster.android.ads.AdsArbiter.TrailerAdsArbiter;
import com.flixster.android.ads.DfpPostTrailerInterstitial;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountManager;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackLogic;
import com.flixster.android.drm.PlaybackManager;
import com.flixster.android.net.DownloadHelper;
import com.flixster.android.storage.ExternalStorage;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder.DialogListener;
import com.flixster.android.view.DownloadPanel;
import com.flixster.android.view.SynopsisView;
import com.flixster.android.widget.WidgetProvider;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.ads.model.DfpAd.DfpCustomTarget;
import net.flixster.android.data.ApiBuilder;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.NetflixDao;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.Actor;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;
import net.flixster.android.model.NetflixException;
import net.flixster.android.model.NetflixTitleState;
import net.flixster.android.model.Photo;
import net.flixster.android.model.Property;
import net.flixster.android.model.Review;
import net.flixster.android.model.Season;
import net.flixster.android.model.User;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

public class MovieDetails extends FlixsterActivity
  implements View.OnClickListener
{
  private static final int DIALOG_NETFLIX_ACTION = 0;
  private static final int DIALOG_NETFLIX_EXCEPTION = 1;
  public static final String KEY_IS_SEASON = "KEY_IS_SEASON";
  public static final String KEY_RIGHT_ID = "KEY_RIGHT_ID";
  private static final int MAX_ACTORS = 3;
  private static final int MAX_DIRECTORS = 3;
  private static final int MAX_PHOTOS = 15;
  private static final int MAX_PHOTO_COUNT = 50;
  private static final int NETFLIX_ACTION_CANCEL = 0;
  private static final int NETFLIX_ACTION_DVD_ADD_BOTTOM = 4;
  private static final int NETFLIX_ACTION_DVD_ADD_TOP = 3;
  private static final int NETFLIX_ACTION_DVD_MOVE_BOTTOM = 10;
  private static final int NETFLIX_ACTION_DVD_MOVE_TOP = 9;
  private static final int NETFLIX_ACTION_DVD_REMOVE = 2;
  private static final int NETFLIX_ACTION_DVD_SAVE = 5;
  private static final int NETFLIX_ACTION_INSTANT_ADD_BOTTOM = 8;
  private static final int NETFLIX_ACTION_INSTANT_ADD_TOP = 7;
  private static final int NETFLIX_ACTION_INSTANT_MOVE_BOTTOM = 12;
  private static final int NETFLIX_ACTION_INSTANT_MOVE_TOP = 11;
  private static final int NETFLIX_ACTION_INSTANT_REMOVE = 6;
  private static final int NETFLIX_ACTION_VIEW_QUEUE = 1;
  private static final int REQUEST_CODE_REVIEW = 1010;
  private static final int REQUEST_CODE_TRAILER = 1000;
  private final int REVIEW_SUBSET_COUNT = 3;
  private View.OnClickListener actorClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Actor localActor = (Actor)paramAnonymousView.getTag();
      if (localActor != null)
      {
        Intent localIntent = new Intent("TOP_ACTOR", null, MovieDetails.this.getApplicationContext(), ActorPage.class);
        localIntent.putExtra("ACTOR_ID", localActor.id);
        localIntent.putExtra("ACTOR_NAME", localActor.name);
        MovieDetails.this.startActivity(localIntent);
      }
    }
  };
  private Handler actorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      ImageView localImageView = (ImageView)paramAnonymousMessage.obj;
      if (localImageView != null)
      {
        Actor localActor = (Actor)localImageView.getTag();
        if (localActor != null)
        {
          localImageView.setImageBitmap(localActor.bitmap);
          localImageView.invalidate();
        }
      }
    }
  };
  private LinearLayout actorsLayout;
  private final Handler canDownloadSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (MovieDetails.this.isFinishing())
        return;
      if (MovieDetails.this.downloadInitDialog != null);
      try
      {
        MovieDetails.this.downloadInitDialog.dismiss();
        if (MovieDetails.this.right.getDownloadsRemain() > 0)
        {
          DownloadHelper.downloadMovie(MovieDetails.this.right);
          MovieDetails.this.delayedStreamingUiUpdate();
          return;
        }
      }
      catch (Exception localException)
      {
        while (true)
          localException.printStackTrace();
        MovieDetails.this.showDialog(1000000204, null);
      }
    }
  };
  private final Handler collectedMoviesSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      MovieDetails.this.populateStreamingUi();
    }
  };
  private LinearLayout directorsLayout;
  private final DialogBuilder.DialogListener downloadCancelDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      DownloadHelper.cancelMovieDownload(MovieDetails.this.right.rightId);
      MovieDetails.this.delayedStreamingUiUpdate();
      Trackers.instance().trackEvent("/download", "Download", "Download", "Cancel");
    }
  };
  private final View.OnClickListener downloadClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if ((!DownloadHelper.isMovieDownloadInProgress(MovieDetails.this.right.rightId)) && (!DownloadHelper.isDownloaded(MovieDetails.this.right.rightId)))
      {
        if (!ExternalStorage.isWriteable())
          break label101;
        if (DownloadHelper.findRemainingSpace() > MovieDetails.this.right.getDownloadAssetSizeRaw())
        {
          ObjectHolder.instance().put(String.valueOf(1000000200), MovieDetails.this.right);
          MovieDetails.this.showDialog(1000000200, MovieDetails.this.downloadConfirmDialogListener);
        }
      }
      else
      {
        return;
      }
      MovieDetails.this.showDialog(1000000205, null);
      return;
      label101: MovieDetails.this.showDialog(1000000203, null);
    }
  };
  private final DialogBuilder.DialogListener downloadConfirmDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000200));
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000200));
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000200));
      Trackers.instance().trackEvent("/download", "Download", "Download", "Confirm");
      if (MovieDetails.this.downloadInitDialog == null)
      {
        MovieDetails.this.downloadInitDialog = new ProgressDialog(MovieDetails.this);
        MovieDetails.this.downloadInitDialog.setMessage(MovieDetails.this.getResources().getString(2131493172));
      }
      MovieDetails.this.downloadInitDialog.show();
      ProfileDao.canDownload(MovieDetails.this.canDownloadSuccessHandler, MovieDetails.this.errorHandler, MovieDetails.this.right);
    }
  };
  private final View.OnClickListener downloadDeleteClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (DownloadHelper.isMovieDownloadInProgress(MovieDetails.this.right.rightId))
        MovieDetails.this.showDialog(1000000201, MovieDetails.this.downloadCancelDialogListener);
      while (!DownloadHelper.isDownloaded(MovieDetails.this.right.rightId))
        return;
      ObjectHolder.instance().put(String.valueOf(1000000202), MovieDetails.this.right);
      MovieDetails.this.showDialog(1000000202, MovieDetails.this.downloadDeleteDialogListener);
    }
  };
  private final DialogBuilder.DialogListener downloadDeleteDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000202));
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000202));
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000202));
      DownloadHelper.deleteDownloadedMovie(MovieDetails.this.right);
      MovieDetails.this.delayedStreamingUiUpdate();
      Trackers.instance().trackEvent("/download", "Download", "Download", "Delete");
    }
  };
  private ProgressDialog downloadInitDialog;
  private DownloadPanel downloadPanel;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (MovieDetails.this.isFinishing());
      while (true)
      {
        return;
        if (MovieDetails.this.downloadInitDialog != null);
        try
        {
          MovieDetails.this.downloadInitDialog.dismiss();
          if (!(paramAnonymousMessage.obj instanceof DaoException))
            continue;
          ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, MovieDetails.this);
          return;
        }
        catch (Exception localException)
        {
          while (true)
            localException.printStackTrace();
        }
      }
    }
  };
  private LayoutInflater inflater;
  private boolean isSeason;
  private Bundle mExtras;
  private boolean mFromRating = false;
  private Movie mMovie;
  private MovieDetails mMovieDetails;
  private long mMovieId;
  private int[] mNetflixMenuToAction = null;
  private NetflixTitleState mNetflixTitleState;
  private Handler mNetflixTitleStateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      TextView localTextView;
      ArrayList localArrayList;
      label175: label197: int i;
      if (MovieDetails.this.mNetflixTitleState != null)
      {
        localTextView = (TextView)MovieDetails.this.findViewById(2131165529);
        localArrayList = new ArrayList();
        localArrayList.add(Integer.valueOf(1));
        Logger.d("FlxMain", "MovieDetails.mNetflixTitleStateHandler mNetflixTitleState:" + MovieDetails.this.mNetflixTitleState);
        localTextView.setText(2131493073);
        if ((0x8 & MovieDetails.this.mNetflixTitleState.mCategoriesMask) == 0)
          break label276;
        localTextView.setText(2131493075);
        localArrayList.add(Integer.valueOf(6));
        localArrayList.add(Integer.valueOf(11));
        localArrayList.add(Integer.valueOf(12));
        if ((0x1 & MovieDetails.this.mNetflixTitleState.mCategoriesMask) == 0)
          break label315;
        localTextView.setText(2131493075);
        localArrayList.add(Integer.valueOf(2));
        localArrayList.add(Integer.valueOf(9));
        localArrayList.add(Integer.valueOf(10));
        if ((0x9 & MovieDetails.this.mNetflixTitleState.mCategoriesMask) == 0)
          break label378;
        localTextView.setText(2131493075);
        localArrayList.add(Integer.valueOf(0));
        localTextView.setVisibility(0);
        localTextView.setOnClickListener(MovieDetails.this.mMovieDetails);
        localTextView.setFocusable(true);
        i = localArrayList.size();
        Logger.d("FlxMain", "menuActions:" + localArrayList);
        MovieDetails.this.mNetflixMenuToAction = new int[i];
      }
      for (int j = 0; ; j++)
      {
        if (j >= i)
        {
          return;
          label276: if ((0x10 & MovieDetails.this.mNetflixTitleState.mCategoriesMask) == 0)
            break;
          localArrayList.add(Integer.valueOf(7));
          localArrayList.add(Integer.valueOf(8));
          break;
          label315: if ((0x2 & MovieDetails.this.mNetflixTitleState.mCategoriesMask) != 0)
          {
            localArrayList.add(Integer.valueOf(3));
            localArrayList.add(Integer.valueOf(4));
            break label175;
          }
          if ((0x4 & MovieDetails.this.mNetflixTitleState.mCategoriesMask) == 0)
            break label175;
          localArrayList.add(Integer.valueOf(5));
          break label175;
          label378: if ((0x4 & MovieDetails.this.mNetflixTitleState.mCategoriesMask) != 0)
          {
            localTextView.setText(2131493074);
            break label197;
          }
          if ((0x12 & MovieDetails.this.mNetflixTitleState.mCategoriesMask) == 0)
            break label197;
          localTextView.setText(2131493073);
          break label197;
        }
        MovieDetails.this.mNetflixMenuToAction[j] = ((Integer)localArrayList.get(j)).intValue();
      }
    }
  };
  private Boolean mPlayTrailer = null;
  private Review mReview;
  private AdView mScrollAd;
  private RelativeLayout moreActorsLayout;
  private View.OnClickListener moreActorsListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      View localView;
      if ((MovieDetails.this.actorsLayout != null) && (MovieDetails.this.mMovie != null) && (MovieDetails.this.mMovie.mActors != null) && (MovieDetails.this.mMovie.mActors.size() > 3))
        localView = null;
      for (int i = 3; ; i++)
      {
        if (i >= MovieDetails.this.mMovie.mActors.size())
        {
          if (MovieDetails.this.moreActorsLayout != null)
            MovieDetails.this.moreActorsLayout.setVisibility(8);
          return;
        }
        Actor localActor = (Actor)MovieDetails.this.mMovie.mActors.get(i);
        localView = MovieDetails.this.getActorView(localActor, localView);
        ImageView localImageView = new ImageView(MovieDetails.this);
        localImageView.setBackgroundResource(2131296282);
        localImageView.setLayoutParams(new ViewGroup.LayoutParams(-1, MovieDetails.this.getResources().getDimensionPixelOffset(2131361829)));
        MovieDetails.this.actorsLayout.addView(localImageView);
        MovieDetails.this.actorsLayout.addView(localView);
      }
    }
  };
  private RelativeLayout moreDirectorsLayout;
  private View.OnClickListener moreDirectorsListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      View localView;
      if ((MovieDetails.this.directorsLayout != null) && (MovieDetails.this.mMovie != null) && (MovieDetails.this.mMovie.mDirectors != null) && (MovieDetails.this.mMovie.mDirectors.size() > 3))
        localView = null;
      for (int i = 3; ; i++)
      {
        if (i >= MovieDetails.this.mMovie.mDirectors.size())
        {
          if (MovieDetails.this.moreDirectorsLayout != null)
            MovieDetails.this.moreDirectorsLayout.setVisibility(8);
          return;
        }
        Actor localActor = (Actor)MovieDetails.this.mMovie.mDirectors.get(i);
        localView = MovieDetails.this.getActorView(localActor, localView);
        ImageView localImageView = new ImageView(MovieDetails.this);
        localImageView.setBackgroundResource(2131296282);
        localImageView.setLayoutParams(new ViewGroup.LayoutParams(-1, MovieDetails.this.getResources().getDimensionPixelOffset(2131361829)));
        MovieDetails.this.directorsLayout.addView(localImageView);
        MovieDetails.this.directorsLayout.addView(localView);
      }
    }
  };
  private Handler netflixHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!MovieDetails.this.isFinishing())
        MovieDetails.this.showDialog(1);
    }
  };
  private View.OnClickListener photoClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Photo localPhoto = (Photo)paramAnonymousView.getTag();
      if (localPhoto != null)
      {
        int i = MovieDetails.this.mMovie.mPhotos.indexOf(localPhoto);
        if (i < 0)
          i = 0;
        Trackers.instance().track(MovieDetails.this.getGaTagPrefix() + "/photo", MovieDetails.this.getGaTitlePrefix() + "Photo Page for photo:" + localPhoto.thumbnailUrl);
        Intent localIntent = new Intent("PHOTO", null, MovieDetails.this.getApplicationContext(), ScrollGalleryPage.class);
        localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", MovieDetails.this.mMovie.getId());
        localIntent.putExtra("PHOTO_INDEX", i);
        localIntent.putExtra("title", MovieDetails.this.mMovie.getProperty("title"));
        MovieDetails.this.startActivity(localIntent);
      }
    }
  };
  private Handler photoHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      ImageView localImageView = (ImageView)paramAnonymousMessage.obj;
      if (localImageView != null)
      {
        Photo localPhoto = (Photo)localImageView.getTag();
        if ((localPhoto != null) && (localPhoto.bitmap != null))
        {
          localImageView.setImageBitmap(localPhoto.bitmap);
          localImageView.invalidate();
        }
      }
    }
  };
  private final Handler populateStreamingUiHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      MovieDetails.this.populateStreamingUi();
    }
  };
  private Handler refreshHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((MovieDetails.this.topLevelDecorator.isPausing()) || (MovieDetails.this.isFinishing()))
        Logger.w("FlxMain", "MovieDetails.mUpdateHandler pausing");
      String str;
      do
      {
        do
        {
          do
          {
            return;
            Logger.d("FlxMain", "MovieDetails.refreshHandler run...");
            MovieDetails.this.mMovieDetails.populatePage();
            Logger.d("FlxMain", "MovieDetails.refreshHandler check deep trailer link playTrailer:" + MovieDetails.this.mPlayTrailer);
          }
          while (!MovieDetails.this.mPlayTrailer.booleanValue());
          Logger.d("FlxMain", "MovieDetails.refreshHandler check deep trailer link mMovie:" + MovieDetails.this.mMovie);
        }
        while (MovieDetails.this.mMovie == null);
        str = MovieDetails.this.mMovie.getProperty("high");
        Logger.d("FlxMain", "MovieDetails.refreshHandler check deep trailer link movieTrailerUrl:" + str);
      }
      while ((str == null) || (!str.startsWith("http:")));
      MovieDetails.this.mPlayTrailer = Boolean.valueOf(false);
      Starter.launchTrailer(MovieDetails.this.mMovie, MovieDetails.this);
    }
  };
  private LockerRight right;
  private boolean showGetGlue;
  private final View.OnClickListener watchNowClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if ((!DownloadHelper.isMovieDownloadInProgress(MovieDetails.this.right.rightId)) && (DownloadHelper.isDownloaded(MovieDetails.this.right.rightId)));
      for (int i = 1; (i == 0) && (DownloadHelper.isMovieDownloadInProgress()); i = 0)
      {
        MovieDetails.this.showDialog(1000000300, MovieDetails.this.watchNowConfirmDialogListener);
        return;
      }
      Drm.manager().playMovie(MovieDetails.this.right);
    }
  };
  private final DialogBuilder.DialogListener watchNowConfirmDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      Drm.manager().playMovie(MovieDetails.this.right);
    }
  };
  private TextView watchNowPanel;

  private void ScheduleAddToQueue(final int paramInt, final String paramString)
  {
    TimerTask local23 = new TimerTask()
    {
      public void run()
      {
        try
        {
          NetflixDao.postQueueItem(MovieDetails.this.mMovie.getProperty("netflix"), paramInt, paramString);
          MovieDetails.this.ScheduleNetflixTitleState();
          Trackers.instance().track("/netflix/addtoqueue" + paramString, "position:" + paramInt);
          return;
        }
        catch (OAuthMessageSignerException localOAuthMessageSignerException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleAddToQueue OAuthMessageSignerException", localOAuthMessageSignerException);
          return;
        }
        catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleAddToQueue OAuthExpectationFailedException", localOAuthExpectationFailedException);
          return;
        }
        catch (ClientProtocolException localClientProtocolException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleAddToQueue ClientProtocolException", localClientProtocolException);
          return;
        }
        catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleAddToQueue OAuthNotAuthorizedException", localOAuthNotAuthorizedException);
          return;
        }
        catch (NetflixException localNetflixException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleAddToQueue NetflixException message:" + localNetflixException.mMessage, localNetflixException);
          MovieDetails.this.netflixHandler.sendEmptyMessage(0);
          return;
        }
        catch (IOException localIOException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleAddToQueue IOException", localIOException);
          return;
        }
        catch (OAuthCommunicationException localOAuthCommunicationException)
        {
          localOAuthCommunicationException.printStackTrace();
          return;
        }
        catch (JSONException localJSONException)
        {
          localJSONException.printStackTrace();
        }
      }
    };
    if (this.mPageTimer != null)
      this.mPageTimer.schedule(local23, 100L);
  }

  private void ScheduleLoadMovieTask(long paramLong)
  {
    TimerTask local26 = new TimerTask()
    {
      // ERROR //
      public void run()
      {
        // Byte code:
        //   0: ldc 27
        //   2: new 29	java/lang/StringBuilder
        //   5: dup
        //   6: ldc 31
        //   8: invokespecial 34	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
        //   11: aload_0
        //   12: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   15: invokestatic 38	net/flixster/android/MovieDetails:access$32	(Lnet/flixster/android/MovieDetails;)J
        //   18: invokevirtual 42	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //   21: invokevirtual 46	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //   24: invokestatic 52	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
        //   27: aload_0
        //   28: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   31: astore_2
        //   32: aload_0
        //   33: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   36: invokestatic 56	net/flixster/android/MovieDetails:access$33	(Lnet/flixster/android/MovieDetails;)Z
        //   39: ifeq +34 -> 73
        //   42: aload_0
        //   43: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   46: invokestatic 38	net/flixster/android/MovieDetails:access$32	(Lnet/flixster/android/MovieDetails;)J
        //   49: invokestatic 62	net/flixster/android/data/MovieDao:getSeasonDetailLegacy	(J)Lnet/flixster/android/model/Season;
        //   52: astore_3
        //   53: aload_2
        //   54: aload_3
        //   55: invokestatic 66	net/flixster/android/MovieDetails:access$34	(Lnet/flixster/android/MovieDetails;Lnet/flixster/android/model/Movie;)V
        //   58: aload_0
        //   59: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   62: aload_0
        //   63: getfield 19	net/flixster/android/MovieDetails$26:val$currResumeCtr	I
        //   66: invokevirtual 70	net/flixster/android/MovieDetails:shouldSkipBackgroundTask	(I)Z
        //   69: ifeq +18 -> 87
        //   72: return
        //   73: aload_0
        //   74: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   77: invokestatic 38	net/flixster/android/MovieDetails:access$32	(Lnet/flixster/android/MovieDetails;)J
        //   80: invokestatic 74	net/flixster/android/data/MovieDao:getMovieDetail	(J)Lnet/flixster/android/model/Movie;
        //   83: astore_3
        //   84: goto -31 -> 53
        //   87: invokestatic 79	net/flixster/android/FlixsterApplication:getNetflixUserId	()Ljava/lang/String;
        //   90: ifnull +10 -> 100
        //   93: aload_0
        //   94: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   97: invokestatic 83	net/flixster/android/MovieDetails:access$30	(Lnet/flixster/android/MovieDetails;)V
        //   100: aload_0
        //   101: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   104: iconst_0
        //   105: putfield 86	net/flixster/android/MovieDetails:mRetryCount	I
        //   108: aload_0
        //   109: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   112: invokestatic 90	net/flixster/android/MovieDetails:access$12	(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/MovieDetails;
        //   115: invokestatic 93	net/flixster/android/MovieDetails:access$35	(Lnet/flixster/android/MovieDetails;)Z
        //   118: ifne +36 -> 154
        //   121: invokestatic 96	net/flixster/android/FlixsterApplication:getPlatformUsername	()Ljava/lang/String;
        //   124: ifnull +30 -> 154
        //   127: invokestatic 99	net/flixster/android/FlixsterApplication:getFlixsterId	()Ljava/lang/String;
        //   130: astore 5
        //   132: aload_0
        //   133: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   136: aload 5
        //   138: aload_0
        //   139: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   142: invokestatic 38	net/flixster/android/MovieDetails:access$32	(Lnet/flixster/android/MovieDetails;)J
        //   145: invokestatic 104	java/lang/Long:toString	(J)Ljava/lang/String;
        //   148: invokestatic 110	net/flixster/android/data/ProfileDao:getUserMovieReview	(Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/Review;
        //   151: invokestatic 114	net/flixster/android/MovieDetails:access$36	(Lnet/flixster/android/MovieDetails;Lnet/flixster/android/model/Review;)V
        //   154: aload_0
        //   155: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   158: invokestatic 90	net/flixster/android/MovieDetails:access$12	(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/MovieDetails;
        //   161: invokestatic 118	net/flixster/android/MovieDetails:access$37	(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;
        //   164: iconst_0
        //   165: invokevirtual 123	android/os/Handler:sendEmptyMessage	(I)Z
        //   168: pop
        //   169: return
        //   170: astore_1
        //   171: ldc 27
        //   173: ldc 125
        //   175: aload_1
        //   176: invokestatic 129	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   179: aload_0
        //   180: getfield 17	net/flixster/android/MovieDetails$26:this$0	Lnet/flixster/android/MovieDetails;
        //   183: aload_1
        //   184: invokevirtual 133	net/flixster/android/MovieDetails:retryLogic	(Lnet/flixster/android/data/DaoException;)V
        //   187: return
        //   188: astore 6
        //   190: ldc 27
        //   192: new 29	java/lang/StringBuilder
        //   195: dup
        //   196: ldc 135
        //   198: invokespecial 34	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
        //   201: aload 6
        //   203: invokevirtual 138	net/flixster/android/data/DaoException:getMessage	()Ljava/lang/String;
        //   206: invokevirtual 141	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   209: invokevirtual 46	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //   212: invokestatic 144	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;)V
        //   215: goto -61 -> 154
        //
        // Exception table:
        //   from	to	target	type
        //   27	53	170	net/flixster/android/data/DaoException
        //   53	72	170	net/flixster/android/data/DaoException
        //   73	84	170	net/flixster/android/data/DaoException
        //   87	100	170	net/flixster/android/data/DaoException
        //   100	132	170	net/flixster/android/data/DaoException
        //   154	169	170	net/flixster/android/data/DaoException
        //   190	215	170	net/flixster/android/data/DaoException
        //   132	154	188	net/flixster/android/data/DaoException
      }
    };
    if (this.mPageTimer != null)
      this.mPageTimer.schedule(local26, paramLong);
  }

  private void ScheduleMoveToBottom(final String paramString)
  {
    TimerTask local24 = new TimerTask()
    {
      public void run()
      {
        try
        {
          String str = MovieDetails.this.mMovie.getProperty("netflix");
          Logger.d("FlxMain", "MovieDetails.ScheduleMoveToBottom movieUrl:" + str + " queueType:" + paramString);
          NetflixDao.deleteQueueItem(str.substring(37, str.length()), paramString);
          NetflixDao.postQueueItem(MovieDetails.this.mMovie.getProperty("netflix"), 0, paramString);
          MovieDetails.this.ScheduleNetflixTitleState();
          Trackers.instance().track("/netflix/movetobottom" + paramString, "position:");
          return;
        }
        catch (OAuthMessageSignerException localOAuthMessageSignerException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleMoveToBottom OAuthMessageSignerException", localOAuthMessageSignerException);
          return;
        }
        catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleMoveToBottom OAuthExpectationFailedException", localOAuthExpectationFailedException);
          return;
        }
        catch (ClientProtocolException localClientProtocolException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleMoveToBottom ClientProtocolException", localClientProtocolException);
          return;
        }
        catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleMoveToBottom OAuthNotAuthorizedException", localOAuthNotAuthorizedException);
          return;
        }
        catch (NetflixException localNetflixException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleAddToQueue NetflixException message:" + localNetflixException.mMessage, localNetflixException);
          MovieDetails.this.netflixHandler.sendEmptyMessage(0);
          return;
        }
        catch (IOException localIOException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleMoveToBottom IOException", localIOException);
          return;
        }
        catch (OAuthCommunicationException localOAuthCommunicationException)
        {
          localOAuthCommunicationException.printStackTrace();
          return;
        }
        catch (JSONException localJSONException)
        {
          localJSONException.printStackTrace();
        }
      }
    };
    if (this.mPageTimer != null)
      this.mPageTimer.schedule(local24, 100L);
  }

  private void ScheduleNetflixTitleState()
  {
    TimerTask local22 = new TimerTask()
    {
      public void run()
      {
        try
        {
          MovieDetails.this.mNetflixTitleState = NetflixDao.fetchTitleState(MovieDetails.this.mMovie.getProperty("netflix"));
          MovieDetails.this.mNetflixTitleStateHandler.sendEmptyMessage(0);
          return;
        }
        catch (OAuthMessageSignerException localOAuthMessageSignerException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleNetflixTitleState OAuthMessageSignerException", localOAuthMessageSignerException);
          return;
        }
        catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleNetflixTitleState OAuthExpectationFailedException", localOAuthExpectationFailedException);
          return;
        }
        catch (IOException localIOException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleNetflixTitleState IOException", localIOException);
          return;
        }
        catch (JSONException localJSONException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleNetflixTitleState JSONException", localJSONException);
          return;
        }
        catch (OAuthCommunicationException localOAuthCommunicationException)
        {
          localOAuthCommunicationException.printStackTrace();
        }
      }
    };
    if (this.mPageTimer != null)
      this.mPageTimer.schedule(local22, 100L);
  }

  private void ScheduleRemoveFromQueue(final String paramString)
  {
    TimerTask local25 = new TimerTask()
    {
      public void run()
      {
        try
        {
          String str1 = MovieDetails.this.mMovie.getProperty("netflix");
          String str2 = str1.substring(37, str1.length());
          Logger.d("FlxMain", "MovieDetails.ScheduleRemoveFromQueue movieUrl:" + str1 + " movieUrlId:" + str2);
          NetflixDao.deleteQueueItem(str2, paramString);
          Trackers.instance().track("/netflix/removefromqueue" + paramString, "Delete");
          MovieDetails.this.ScheduleNetflixTitleState();
          return;
        }
        catch (OAuthMessageSignerException localOAuthMessageSignerException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleRemoveFromQueue OAuthMessageSignerException", localOAuthMessageSignerException);
          return;
        }
        catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleRemoveFromQueue OAuthExpectationFailedException", localOAuthExpectationFailedException);
          return;
        }
        catch (ClientProtocolException localClientProtocolException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleRemoveFromQueue ClientProtocolException", localClientProtocolException);
          return;
        }
        catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleRemoveFromQueue OAuthNotAuthorizedException", localOAuthNotAuthorizedException);
          return;
        }
        catch (IOException localIOException)
        {
          Logger.e("FlxMain", "MovieDetails.ScheduleRemoveFromQueue IOException", localIOException);
          return;
        }
        catch (OAuthCommunicationException localOAuthCommunicationException)
        {
          localOAuthCommunicationException.printStackTrace();
        }
      }
    };
    if (this.mPageTimer != null)
      this.mPageTimer.schedule(local25, 100L);
  }

  protected static String createMovieSummary(Movie paramMovie)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramMovie.getTitle()).append("\n");
    if (paramMovie.checkProperty("MOVIE_ACTORS_SHORT"))
      localStringBuilder.append(paramMovie.getProperty("MOVIE_ACTORS_SHORT")).append("\n");
    if (paramMovie.checkProperty("dvdReleaseDate"))
      localStringBuilder.append("DVD release: " + paramMovie.getProperty("dvdReleaseDate")).append("\n");
    while (true)
    {
      if (paramMovie.checkProperty("meta"))
        localStringBuilder.append(paramMovie.getProperty("meta")).append("\n");
      localStringBuilder.append("\n");
      return localStringBuilder.toString();
      if (paramMovie.checkProperty("theaterReleaseDate"))
        localStringBuilder.append("Theater release: " + paramMovie.getProperty("theaterReleaseDate")).append("\n");
    }
  }

  private void delayedStreamingUiUpdate()
  {
    if (this.mPageTimer != null)
      this.mPageTimer.schedule(new TimerTask()
      {
        public void run()
        {
          MovieDetails.this.populateStreamingUiHandler.sendEmptyMessage(0);
        }
      }
      , 2000L);
  }

  private View getActorView(Actor paramActor, View paramView)
  {
    View localView = this.inflater.inflate(2130903117, null);
    ActorViewHolder localActorViewHolder = new ActorViewHolder(null);
    localActorViewHolder.actorLayout = ((RelativeLayout)localView.findViewById(2131165224));
    localActorViewHolder.nameView = ((TextView)localView.findViewById(2131165228));
    localActorViewHolder.charsView = ((TextView)localView.findViewById(2131165444));
    localActorViewHolder.imageView = ((ImageView)localView.findViewById(2131165227));
    if (paramActor != null)
    {
      String str1 = paramActor.name;
      localActorViewHolder.nameView.setText(str1);
      String str2 = paramActor.chars;
      if (str2 != null)
      {
        localActorViewHolder.charsView.setText(str2);
        localActorViewHolder.charsView.setVisibility(0);
      }
      localActorViewHolder.imageView.setTag(paramActor);
      if (paramActor.bitmap == null)
        break label183;
      localActorViewHolder.imageView.setImageBitmap(paramActor.bitmap);
    }
    while (true)
    {
      localActorViewHolder.actorLayout.setTag(paramActor);
      localActorViewHolder.actorLayout.setOnClickListener(this.actorClickListener);
      return localView;
      label183: if (paramActor.thumbnailUrl != null)
      {
        localActorViewHolder.imageView.setImageResource(2130837978);
        orderImageFifo(new ImageOrder(4, paramActor, paramActor.thumbnailUrl, localActorViewHolder.imageView, this.actorHandler));
      }
      else
      {
        localActorViewHolder.imageView.setImageResource(2130837978);
      }
    }
  }

  private String getGaTagPrefix()
  {
    if (this.isSeason)
      return "/tv-season";
    return "/movie";
  }

  private String getGaTitlePrefix()
  {
    if (this.isSeason)
      return "TV Season ";
    return "Movie ";
  }

  private void loadCriticReviews(int paramInt1, int paramInt2, int paramInt3, LinearLayout paramLinearLayout, ArrayList<Review> paramArrayList)
  {
    int i = paramInt3;
    LayoutInflater localLayoutInflater = LayoutInflater.from(this);
    for (int j = paramInt1; ; j++)
    {
      if (j >= paramInt2)
        return;
      Review localReview = (Review)paramArrayList.get(j);
      View localView = localLayoutInflater.inflate(2130903074, paramLinearLayout, false);
      localView.setId(2130903074);
      localView.setFocusable(true);
      ((ImageView)localView.findViewById(2131165288)).setVisibility(8);
      ((TextView)localView.findViewById(2131165285)).setText(localReview.name);
      ((TextView)localView.findViewById(2131165287)).setText(", " + localReview.source);
      TextView localTextView = (TextView)localView.findViewById(2131165289);
      localTextView.setMaxLines(10);
      localTextView.setText("     " + localReview.comment);
      if (localReview.score < 60)
        ((ImageView)localView.findViewById(2131165286)).setImageResource(2130837741);
      localView.setTag(Integer.valueOf(i));
      Logger.v("FlxMain", "MovieDetails.loadCriticReviews iTagIndex:" + i);
      i++;
      localView.setOnClickListener(this);
      ImageView localImageView = (ImageView)localView.findViewById(2131165284);
      Bitmap localBitmap = localReview.getReviewerBitmap(localImageView);
      if (localBitmap != null)
        localImageView.setImageBitmap(localBitmap);
      paramLinearLayout.addView(localView);
    }
  }

  private void loadFriendReviews(int paramInt1, int paramInt2, int paramInt3, LinearLayout paramLinearLayout, ArrayList<Review> paramArrayList)
  {
    int i = paramInt3;
    for (int j = paramInt1; ; j++)
    {
      if (j >= paramInt2)
        return;
      Review localReview = (Review)paramArrayList.get(j);
      View localView = this.inflater.inflate(2130903074, paramLinearLayout, false);
      localView.setId(2130903074);
      localView.setFocusable(true);
      ((ImageView)localView.findViewById(2131165288)).setImageResource(Flixster.RATING_SMALL_R[((int)(2.0D * localReview.stars))]);
      ((TextView)localView.findViewById(2131165285)).setText(localReview.name);
      ((TextView)localView.findViewById(2131165287)).setVisibility(8);
      ((TextView)localView.findViewById(2131165289)).setText(localReview.comment);
      ((ImageView)localView.findViewById(2131165286)).setVisibility(8);
      localView.setTag(Integer.valueOf(i));
      i++;
      localView.setOnClickListener(this);
      ImageView localImageView = (ImageView)localView.findViewById(2131165284);
      Bitmap localBitmap = localReview.getReviewerBitmap(localImageView);
      if (localBitmap != null)
        localImageView.setImageBitmap(localBitmap);
      paramLinearLayout.addView(localView);
    }
  }

  private void loadUserReviews(int paramInt1, int paramInt2, int paramInt3, LinearLayout paramLinearLayout, ArrayList<Review> paramArrayList)
  {
    int i = paramInt3;
    LayoutInflater localLayoutInflater = LayoutInflater.from(this);
    for (int j = paramInt1; ; j++)
    {
      if (j >= paramInt2)
        return;
      Review localReview = (Review)paramArrayList.get(j);
      View localView = localLayoutInflater.inflate(2130903074, paramLinearLayout, false);
      localView.setId(2130903074);
      localView.setFocusable(true);
      ((ImageView)localView.findViewById(2131165288)).setImageResource(Flixster.RATING_SMALL_R[((int)(2.0D * localReview.stars))]);
      ((TextView)localView.findViewById(2131165285)).setText(localReview.name);
      ((TextView)localView.findViewById(2131165287)).setVisibility(8);
      ((TextView)localView.findViewById(2131165289)).setText(localReview.comment);
      ((ImageView)localView.findViewById(2131165286)).setVisibility(8);
      localView.setTag(Integer.valueOf(i));
      i++;
      localView.setOnClickListener(this);
      ImageView localImageView = (ImageView)localView.findViewById(2131165284);
      Bitmap localBitmap = localReview.getReviewerBitmap(localImageView);
      if (localBitmap != null)
        localImageView.setImageBitmap(localBitmap);
      paramLinearLayout.addView(localView);
    }
  }

  private void populatePage()
  {
    Logger.d("FlxMain", "MovieDetails.populatePage()");
    if (this.mMovie == null)
    {
      Logger.d("FlxMain", "MovieDetails.populatePage() mMovie == null, skip return early from populatePage()");
      return;
    }
    if (this.inflater == null)
      this.inflater = LayoutInflater.from(this);
    String str1;
    label103: TextView localTextView2;
    label131: TextView localTextView3;
    label168: LinearLayout localLinearLayout1;
    TextView localTextView4;
    ArrayList localArrayList1;
    label221: ImageView localImageView2;
    label258: View localView2;
    int i11;
    label322: View localView1;
    int i9;
    label526: String[] arrayOfString1;
    int[] arrayOfInt;
    String[] arrayOfString2;
    int j;
    TextView localTextView6;
    ImageView localImageView3;
    TextView localTextView7;
    label1038: int k;
    label1067: int i5;
    label1117: Object localObject;
    String str11;
    label1160: int i6;
    label1185: int i7;
    label1279: TextView localTextView8;
    label1286: StringBuilder localStringBuilder1;
    label1417: label1433: TextView localTextView9;
    label1479: TextView localTextView10;
    label1525: TextView localTextView11;
    label1604: TextView localTextView12;
    int m;
    label1678: TextView localTextView13;
    label1722: TextView localTextView14;
    label1766: TextView localTextView15;
    label1789: RelativeLayout localRelativeLayout2;
    RelativeLayout localRelativeLayout3;
    LinearLayout localLinearLayout6;
    LinearLayout localLinearLayout8;
    LinearLayout localLinearLayout9;
    RelativeLayout localRelativeLayout7;
    label2094: RelativeLayout localRelativeLayout6;
    label2269: RelativeLayout localRelativeLayout5;
    label2451: label2461: RelativeLayout localRelativeLayout4;
    label2620: TextView localTextView18;
    if (this.right != null)
    {
      str1 = this.right.getTitle();
      if (str1 != null)
      {
        ((TextView)findViewById(2131165511)).setText(str1);
        setActionBarTitle(str1);
      }
      RelativeLayout localRelativeLayout1 = (RelativeLayout)findViewById(2131165525);
      if (!this.isSeason)
        break label2746;
      localRelativeLayout1.setVisibility(8);
      localTextView2 = (TextView)findViewById(2131165523);
      if (!this.mMovie.isMIT)
        break label2861;
      localTextView2.setVisibility(0);
      localTextView3 = (TextView)findViewById(2131165528);
      if (!this.showGetGlue)
        break label2871;
      localTextView3.setVisibility(0);
      localTextView3.setFocusable(true);
      localTextView3.setOnClickListener(this);
      localLinearLayout1 = (LinearLayout)findViewById(2131165538);
      localTextView4 = (TextView)findViewById(2131165524);
      localArrayList1 = this.mMovie.mPhotos;
      if (localArrayList1 == null)
        break label3204;
      if (!localArrayList1.isEmpty())
        break label2881;
      localTextView4.setVisibility(8);
      localImageView2 = (ImageView)findViewById(2131165509);
      if (this.right == null)
        break label3247;
      if (this.right.getProfilePoster() != null)
        break label3221;
      localImageView2.setImageResource(2130837839);
      this.directorsLayout = ((LinearLayout)findViewById(2131165540));
      if ((this.mMovie.mDirectors != null) && (!this.mMovie.mDirectors.isEmpty()))
      {
        ((TextView)findViewById(2131165539)).setVisibility(0);
        localView2 = null;
        this.directorsLayout.removeAllViews();
        i11 = 0;
        int i12 = this.mMovie.mDirectors.size();
        if ((i11 < i12) && (i11 < 3))
          break label3294;
        if (this.mMovie.mDirectors.size() > 3)
        {
          this.moreDirectorsLayout = ((RelativeLayout)findViewById(2131165541));
          this.moreDirectorsLayout.setVisibility(0);
          StringBuilder localStringBuilder4 = new StringBuilder();
          localStringBuilder4.append(this.mMovie.mDirectors.size()).append(" total");
          ((TextView)this.moreDirectorsLayout.findViewById(2131165543)).setText(localStringBuilder4.toString());
          this.moreDirectorsLayout.setFocusable(true);
          this.moreDirectorsLayout.setClickable(true);
          this.moreDirectorsLayout.setOnClickListener(this.moreDirectorsListener);
        }
      }
      this.actorsLayout = ((LinearLayout)findViewById(2131165451));
      if ((this.mMovie.mActors != null) && (!this.mMovie.mActors.isEmpty()))
      {
        ((TextView)findViewById(2131165544)).setVisibility(0);
        localView1 = null;
        this.actorsLayout.removeAllViews();
        i9 = 0;
        int i10 = this.mMovie.mActors.size();
        if ((i9 < i10) && (i9 < 3))
          break label3332;
        if (this.mMovie.mActors.size() > 3)
        {
          this.moreActorsLayout = ((RelativeLayout)findViewById(2131165545));
          this.moreActorsLayout.setVisibility(0);
          StringBuilder localStringBuilder3 = new StringBuilder();
          localStringBuilder3.append(this.mMovie.mActors.size()).append(" total");
          ((TextView)this.moreActorsLayout.findViewById(2131165547)).setText(localStringBuilder3.toString());
          this.moreActorsLayout.setFocusable(true);
          this.moreActorsLayout.setClickable(true);
          this.moreActorsLayout.setOnClickListener(this.moreActorsListener);
        }
      }
      if (this.mMovie.isDetailsApiParsed())
      {
        arrayOfString1 = new String[] { "genre", "runningTime", "theaterReleaseDate", "dvdReleaseDate", "mpaa" };
        arrayOfInt = new int[] { 2131165534, 2131165535, 2131165536, 2131165537, 2131165533 };
        Resources localResources = getResources();
        arrayOfString2 = new String[5];
        arrayOfString2[0] = localResources.getString(2131492929);
        arrayOfString2[1] = localResources.getString(2131492899);
        arrayOfString2[2] = localResources.getString(2131492900);
        arrayOfString2[3] = localResources.getString(2131492904);
        arrayOfString2[4] = localResources.getString(2131492898);
        int i = arrayOfString1.length;
        j = 0;
        if (j < i)
          break label3370;
        ((SynopsisView)findViewById(2131165532)).load(this.mMovie.getProperty("synopsis"), 170, true);
      }
      localTextView6 = (TextView)findViewById(2131165513);
      localImageView3 = (ImageView)findViewById(2131165563);
      localTextView7 = (TextView)findViewById(2131165562);
      if (!this.mMovie.checkIntProperty("rottenTomatoes"))
        break label3502;
      int i8 = this.mMovie.getIntProperty("rottenTomatoes").intValue();
      localTextView6.setText(i8 + getResources().getString(2131493218));
      localTextView7.setText(i8 + "%");
      if (i8 < 60)
      {
        Drawable localDrawable4 = getResources().getDrawable(2130837741);
        localDrawable4.setBounds(0, 0, localDrawable4.getIntrinsicWidth(), localDrawable4.getIntrinsicHeight());
        localTextView6.setCompoundDrawables(localDrawable4, null, null, null);
        localImageView3.setImageResource(2130837740);
      }
      if ((this.mMovie.getTheaterReleaseDate() == null) || (FlixsterApplication.sToday.after(this.mMovie.getTheaterReleaseDate())))
        break label3526;
      k = 0;
      if (!this.mMovie.checkIntProperty("popcornScore"))
        break label3587;
      TextView localTextView25 = (TextView)findViewById(2131165512);
      int i4 = this.mMovie.getIntProperty("popcornScore").intValue();
      if (i4 >= 60)
        break label3532;
      i5 = 1;
      localObject = getResources().getString(2131493219);
      str11 = getResources().getString(2131493220);
      StringBuilder localStringBuilder2 = new StringBuilder(String.valueOf(i4));
      if (k == 0)
        break label3538;
      localTextView25.setText((String)localObject);
      if (k != 0)
        break label3545;
      i6 = 2130837750;
      Drawable localDrawable3 = getResources().getDrawable(i6);
      localDrawable3.setBounds(0, 0, localDrawable3.getIntrinsicWidth(), localDrawable3.getIntrinsicHeight());
      localTextView25.setCompoundDrawables(localDrawable3, null, null, null);
      ((TextView)findViewById(2131165571)).setText(i4 + "%");
      ImageView localImageView4 = (ImageView)findViewById(2131165572);
      if (k != 0)
        break label3566;
      i7 = 2130837749;
      localImageView4.setImageResource(i7);
      localTextView8 = (TextView)findViewById(2131165514);
      if ((this.mMovie.getFriendRatedReviewsList() == null) || (this.mMovie.getFriendRatedReviewsList().isEmpty()))
        break label3638;
      int i3 = this.mMovie.getFriendRatedReviewsList().size();
      Drawable localDrawable2 = getResources().getDrawable(2130837739);
      localDrawable2.setBounds(0, 0, localDrawable2.getIntrinsicWidth(), localDrawable2.getIntrinsicHeight());
      localTextView8.setCompoundDrawables(localDrawable2, null, null, null);
      localStringBuilder1 = new StringBuilder();
      localStringBuilder1.append(i3).append(" ");
      if (i3 <= 1)
        break label3619;
      localStringBuilder1.append(getResources().getString(2131493008));
      localTextView8.setText(localStringBuilder1.toString());
      localTextView8.setVisibility(0);
      localTextView9 = (TextView)findViewById(2131165515);
      if (!this.mMovie.checkProperty("MOVIE_ACTORS_SHORT"))
        break label3819;
      localTextView9.setText(this.mMovie.getProperty("MOVIE_ACTORS_SHORT"));
      localTextView9.setVisibility(0);
      localTextView10 = (TextView)findViewById(2131165516);
      if (!this.mMovie.checkProperty("meta"))
        break label3829;
      localTextView10.setText(this.mMovie.getProperty("meta"));
      localTextView10.setVisibility(0);
      localTextView11 = (TextView)findViewById(2131165517);
      if (this.right == null)
        break label3913;
      String str6 = this.right.getViewingExpirationString();
      if (str6 == null)
        break label3839;
      localTextView11.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(2130837689), null, null, null);
      localTextView11.setText(str6);
      localTextView11.setTextColor(getResources().getColor(2131296299));
      localTextView11.setVisibility(0);
      if ((AccountManager.instance().hasUserSession()) && (this.right != null))
        ProfileDao.getUserLockerRights(this.collectedMoviesSuccessHandler, this.errorHandler);
      populateSeasonUi();
      localTextView12 = (TextView)findViewById(2131165579);
      if (0 == 0)
        break label3923;
      m = 1;
      localTextView12.setClickable(true);
      localTextView12.setFocusable(true);
      localTextView12.setOnClickListener(this);
      localTextView12.setVisibility(0);
      localTextView13 = (TextView)findViewById(2131165580);
      if (m == 0)
        break label3936;
      m = 1;
      localTextView13.setClickable(true);
      localTextView13.setFocusable(true);
      localTextView13.setOnClickListener(this);
      localTextView13.setVisibility(0);
      localTextView14 = (TextView)findViewById(2131165581);
      if (m == 0)
        break label3946;
      m = 1;
      localTextView14.setClickable(true);
      localTextView14.setFocusable(true);
      localTextView14.setOnClickListener(this);
      localTextView14.setVisibility(0);
      localTextView15 = (TextView)findViewById(2131165578);
      if (m == 0)
        break label3956;
      localTextView15.setVisibility(0);
      localRelativeLayout2 = (RelativeLayout)findViewById(2131165560);
      TextView localTextView16 = (TextView)findViewById(2131165548);
      LinearLayout localLinearLayout2 = (LinearLayout)findViewById(2131165549);
      LinearLayout localLinearLayout3 = (LinearLayout)findViewById(2131165550);
      TextView localTextView17 = (TextView)findViewById(2131165554);
      LinearLayout localLinearLayout4 = (LinearLayout)findViewById(2131165555);
      LinearLayout localLinearLayout5 = (LinearLayout)findViewById(2131165556);
      localRelativeLayout3 = (RelativeLayout)findViewById(2131165569);
      localLinearLayout6 = (LinearLayout)findViewById(2131165573);
      LinearLayout localLinearLayout7 = (LinearLayout)findViewById(2131165574);
      localLinearLayout8 = (LinearLayout)findViewById(2131165564);
      localLinearLayout9 = (LinearLayout)findViewById(2131165565);
      ArrayList localArrayList2 = this.mMovie.getFriendWantToSeeList();
      int n = 0;
      if (localArrayList2 != null)
      {
        int i1 = localArrayList2.size();
        n = 0;
        if (i1 > 0)
        {
          if (localLinearLayout2.getChildCount() == 0)
          {
            localLinearLayout2.setVisibility(0);
            localLinearLayout3.setVisibility(8);
            localTextView16.setVisibility(0);
            loadFriendReviews(0, Math.min(localArrayList2.size(), 3), 0, localLinearLayout2, localArrayList2);
            localRelativeLayout7 = (RelativeLayout)findViewById(2131165551);
            localRelativeLayout7.setVisibility(8);
            if (localArrayList2.size() <= 3)
              break label3966;
            ((TextView)findViewById(2131165553)).setText(localArrayList2.size() + " friends total");
            localRelativeLayout7.setOnClickListener(this);
            localRelativeLayout7.setFocusable(true);
            localRelativeLayout7.setVisibility(0);
          }
          n = 0 + localArrayList2.size();
        }
      }
      ArrayList localArrayList3 = this.mMovie.getFriendRatedReviewsList();
      if ((localArrayList3 != null) && (localArrayList3.size() > 0))
      {
        if (localLinearLayout4.getChildCount() == 0)
        {
          localLinearLayout4.setVisibility(0);
          localLinearLayout5.setVisibility(8);
          localTextView17.setVisibility(0);
          loadFriendReviews(0, Math.min(localArrayList3.size(), 3), n, localLinearLayout4, localArrayList3);
          localRelativeLayout6 = (RelativeLayout)findViewById(2131165557);
          localRelativeLayout6.setVisibility(8);
          if (localArrayList3.size() <= 3)
            break label3976;
          TextView localTextView24 = (TextView)findViewById(2131165559);
          String str5 = getResourceString(2131492993);
          Object[] arrayOfObject3 = new Object[1];
          arrayOfObject3[0] = Integer.valueOf(localArrayList3.size());
          localTextView24.setText(String.format(str5, arrayOfObject3));
          localRelativeLayout6.setOnClickListener(this);
          localRelativeLayout6.setFocusable(true);
          localRelativeLayout6.setVisibility(0);
        }
        n += localArrayList3.size();
      }
      ArrayList localArrayList4 = this.mMovie.getUserReviewsList();
      if ((localArrayList4 == null) || (localArrayList4.size() == 0))
        break label3996;
      if (localLinearLayout6.getChildCount() == 0)
      {
        localRelativeLayout3.setVisibility(0);
        localLinearLayout6.setVisibility(0);
        localLinearLayout7.setVisibility(8);
        localRelativeLayout2.setVisibility(0);
        loadUserReviews(0, Math.min(localArrayList4.size(), 3), n, localLinearLayout6, localArrayList4);
        localRelativeLayout5 = (RelativeLayout)findViewById(2131165575);
        localRelativeLayout5.setVisibility(8);
        if (localArrayList4.size() <= 3)
          break label3986;
        TextView localTextView23 = (TextView)findViewById(2131165577);
        String str4 = getResourceString(2131492993);
        Object[] arrayOfObject2 = new Object[1];
        arrayOfObject2[0] = Integer.valueOf(localArrayList4.size());
        localTextView23.setText(String.format(str4, arrayOfObject2));
        localRelativeLayout5.setOnClickListener(this);
        localRelativeLayout5.setFocusable(true);
        localRelativeLayout5.setVisibility(0);
      }
      n += localArrayList4.size();
      localRelativeLayout4 = (RelativeLayout)findViewById(2131165566);
      if (localLinearLayout8.getChildCount() == 0)
      {
        ArrayList localArrayList5 = this.mMovie.getCriticReviewsList();
        if ((localArrayList5 == null) || (localArrayList5.size() == 0))
          break label4023;
        localLinearLayout8.setVisibility(0);
        localLinearLayout9.setVisibility(8);
        localRelativeLayout2.setVisibility(0);
        loadCriticReviews(0, Math.min(3, localArrayList5.size()), n, localLinearLayout8, localArrayList5);
        if (localArrayList5.size() <= 3)
          break label4013;
        TextView localTextView22 = (TextView)findViewById(2131165568);
        String str3 = getResourceString(2131492993);
        Object[] arrayOfObject1 = new Object[1];
        arrayOfObject1[0] = Integer.valueOf(localArrayList5.size());
        localTextView22.setText(String.format(str3, arrayOfObject1));
        localRelativeLayout4.setOnClickListener(this);
        localRelativeLayout4.setFocusable(true);
        localRelativeLayout4.setVisibility(0);
      }
      localTextView18 = (TextView)findViewById(2131165522);
      TextView localTextView19 = (TextView)findViewById(2131165519);
      if ((!this.mMovie.hasTrailer()) || (localTextView19.getVisibility() == 0))
        break label4054;
      localTextView18.setVisibility(0);
      localTextView18.setFocusable(true);
      localTextView18.setOnClickListener(this);
    }
    TextView localTextView21;
    while (true)
    {
      TextView localTextView20 = (TextView)findViewById(2131165523);
      localTextView20.setFocusable(true);
      localTextView20.setOnClickListener(this);
      localTextView21 = (TextView)findViewById(2131165529);
      if (FlixsterApplication.getNetflixUserId() == null)
        break label4064;
      localTextView21.setOnClickListener(this);
      localTextView21.setFocusable(true);
      return;
      str1 = this.mMovie.getTitle();
      break;
      label2746: TextView localTextView1 = (TextView)findViewById(2131165526);
      localTextView1.setFocusable(true);
      localTextView1.setOnClickListener(this);
      ImageView localImageView1 = (ImageView)this.mMovieDetails.findViewById(2131165527);
      if ((this.mReview != null) && (this.mReview.stars != 0.0D) && (FlixsterApplication.getPlatformUsername() != null))
      {
        localImageView1.setVisibility(0);
        localImageView1.setImageResource(Flixster.RATING_SMALL_R[((int)(2.0D * this.mReview.stars))]);
        localTextView1.setText(2131492927);
        break label103;
      }
      localImageView1.setVisibility(8);
      localTextView1.setText(2131492926);
      break label103;
      label2861: localTextView2.setVisibility(8);
      break label131;
      label2871: localTextView3.setVisibility(8);
      break label168;
      label2881: localLinearLayout1.removeAllViews();
      int i13 = 0;
      int i14 = localArrayList1.size();
      if ((i13 >= i14) || (i13 >= 15))
      {
        localLinearLayout1.setVisibility(0);
        StringBuilder localStringBuilder5 = new StringBuilder(getResources().getString(2131492928));
        int i15 = this.mMovie.photoCount;
        if (i15 > 50)
          i15 = 50;
        localStringBuilder5.append(" (").append(i15).append(")");
        localTextView4.setText(localStringBuilder5.toString());
        localTextView4.setVisibility(0);
        localTextView4.setFocusable(true);
        localTextView4.setOnClickListener(this);
        break label221;
      }
      Photo localPhoto = (Photo)localArrayList1.get(i13);
      ImageView localImageView5 = new ImageView(this);
      localImageView5.setLayoutParams(new ViewGroup.LayoutParams(getResources().getDimensionPixelOffset(2131361847), getResources().getDimensionPixelOffset(2131361847)));
      localImageView5.setPadding(0, getResources().getDimensionPixelOffset(2131361836), getResources().getDimensionPixelOffset(2131361835), 0);
      localImageView5.setTag(localPhoto);
      if (localPhoto.bitmap != null)
        localImageView5.setImageBitmap(localPhoto.bitmap);
      while (true)
      {
        localImageView5.setClickable(true);
        localImageView5.setFocusable(true);
        localImageView5.setOnClickListener(this.photoClickListener);
        localLinearLayout1.addView(localImageView5);
        i13++;
        break;
        localImageView5.setImageResource(2130837841);
        if ((localPhoto.thumbnailUrl != null) && (localPhoto.thumbnailUrl.startsWith("http://")))
          orderImageFifo(new ImageOrder(3, localPhoto, localPhoto.thumbnailUrl, localImageView5, this.photoHandler));
      }
      label3204: localLinearLayout1.setVisibility(8);
      localTextView4.setVisibility(8);
      break label221;
      label3221: Bitmap localBitmap2 = this.right.getProfileBitmap(localImageView2);
      if (localBitmap2 == null)
        break label258;
      localImageView2.setImageBitmap(localBitmap2);
      break label258;
      label3247: if (this.mMovie.getThumbnailPoster() == null)
      {
        localImageView2.setImageResource(2130837839);
        break label258;
      }
      Bitmap localBitmap1 = this.mMovie.getThumbnailBackedProfileBitmap(localImageView2);
      if (localBitmap1 == null)
        break label258;
      localImageView2.setImageBitmap(localBitmap1);
      break label258;
      label3294: localView2 = getActorView((Actor)this.mMovie.mDirectors.get(i11), localView2);
      this.directorsLayout.addView(localView2);
      i11++;
      break label322;
      label3332: localView1 = getActorView((Actor)this.mMovie.mActors.get(i9), localView1);
      this.actorsLayout.addView(localView1);
      i9++;
      break label526;
      label3370: String str2 = this.mMovie.getProperty(arrayOfString1[j]);
      TextView localTextView5 = (TextView)findViewById(arrayOfInt[j]);
      if ((str2 != null) && (str2.length() != 0))
      {
        localTextView5.setText(arrayOfString2[j] + " " + str2, TextView.BufferType.SPANNABLE);
        ((Spannable)localTextView5.getText()).setSpan(new StyleSpan(1), 0, arrayOfString2[j].length(), 33);
        localTextView5.setVisibility(0);
      }
      while (true)
      {
        j++;
        break;
        localTextView5.setVisibility(8);
      }
      label3502: localTextView6.setVisibility(8);
      localImageView3.setVisibility(8);
      localTextView7.setVisibility(8);
      break label1038;
      label3526: k = 1;
      break label1067;
      label3532: i5 = 0;
      break label1117;
      label3538: localObject = str11;
      break label1160;
      label3545: if (i5 != 0)
      {
        i6 = 2130837743;
        break label1185;
      }
      i6 = 2130837738;
      break label1185;
      label3566: if (i5 != 0)
      {
        i7 = 2130837742;
        break label1279;
      }
      i7 = 2130837737;
      break label1279;
      label3587: ((TextView)findViewById(2131165512)).setVisibility(8);
      ((TextView)findViewById(2131165571)).setVisibility(4);
      break label1286;
      label3619: localStringBuilder1.append(getResources().getString(2131493007));
      break label1417;
      label3638: if ((this.mMovie.getFriendWantToSeeList() != null) && (!this.mMovie.getFriendWantToSeeList().isEmpty()))
      {
        int i2 = this.mMovie.getFriendWantToSeeList().size();
        Drawable localDrawable1 = getResources().getDrawable(2130837751);
        localDrawable1.setBounds(0, 0, localDrawable1.getIntrinsicWidth(), localDrawable1.getIntrinsicHeight());
        localTextView8.setCompoundDrawables(localDrawable1, null, null, null);
        String str10;
        Object[] arrayOfObject5;
        if (i2 > 1)
        {
          str10 = getResources().getString(2131493222);
          arrayOfObject5 = new Object[1];
          arrayOfObject5[0] = Integer.valueOf(i2);
        }
        String str8;
        Object[] arrayOfObject4;
        for (String str9 = String.format(str10, arrayOfObject5); ; str9 = String.format(str8, arrayOfObject4))
        {
          localTextView8.setText(str9);
          localTextView8.setVisibility(0);
          break;
          str8 = getResources().getString(2131493221);
          arrayOfObject4 = new Object[1];
          arrayOfObject4[0] = Integer.valueOf(i2);
        }
      }
      localTextView8.setVisibility(8);
      break label1433;
      label3819: localTextView9.setVisibility(8);
      break label1479;
      label3829: localTextView10.setVisibility(8);
      break label1525;
      label3839: String str7 = this.right.getRentalExpirationString(false);
      if (str7 != null)
      {
        localTextView11.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(2130837939), null, null, null);
        localTextView11.setText(str7);
        localTextView11.setTextColor(getResources().getColor(2131296286));
        localTextView11.setVisibility(0);
        break label1604;
      }
      localTextView11.setVisibility(8);
      break label1604;
      label3913: localTextView11.setVisibility(8);
      break label1604;
      label3923: localTextView12.setVisibility(8);
      m = 0;
      break label1678;
      label3936: localTextView13.setVisibility(8);
      break label1722;
      label3946: localTextView14.setVisibility(8);
      break label1766;
      label3956: localTextView15.setVisibility(8);
      break label1789;
      label3966: localRelativeLayout7.setVisibility(8);
      break label2094;
      label3976: localRelativeLayout6.setVisibility(8);
      break label2269;
      label3986: localRelativeLayout5.setVisibility(8);
      break label2451;
      label3996: localRelativeLayout3.setVisibility(8);
      localLinearLayout6.setVisibility(8);
      break label2461;
      label4013: localRelativeLayout4.setVisibility(8);
      break label2620;
      label4023: localLinearLayout8.setVisibility(8);
      localLinearLayout9.setVisibility(8);
      localRelativeLayout2.setVisibility(8);
      localRelativeLayout4.setVisibility(8);
      break label2620;
      label4054: localTextView18.setVisibility(8);
    }
    label4064: localTextView21.setVisibility(8);
  }

  private void populateSeasonUi()
  {
    TextView localTextView1 = (TextView)findViewById(2131165521);
    if (this.isSeason)
    {
      localTextView1.setVisibility(0);
      localTextView1.setOnClickListener(this);
      ((TextView)findViewById(2131165530)).setText(2131493289);
      TextView localTextView2 = (TextView)findViewById(2131165516);
      String str = ((Season)this.mMovie).getNetwork();
      if (str != null)
      {
        localTextView2.setText(str);
        localTextView2.setVisibility(0);
        return;
      }
      localTextView2.setVisibility(8);
      return;
    }
    localTextView1.setVisibility(8);
  }

  private void populateStreamingUi()
  {
    this.watchNowPanel = ((TextView)findViewById(2131165519));
    this.downloadPanel = ((DownloadPanel)findViewById(2131165520));
    if (Drm.logic().isAssetPlayable(this.right))
    {
      this.watchNowPanel.setVisibility(0);
      this.watchNowPanel.setOnClickListener(this.watchNowClickListener);
    }
    while (Drm.logic().isAssetDownloadable(this.right))
    {
      this.downloadPanel.setVisibility(0);
      this.downloadPanel.load(this.right, this.downloadClickListener, this.downloadDeleteClickListener);
      return;
      this.watchNowPanel.setVisibility(8);
    }
    this.downloadPanel.setVisibility(8);
  }

  private void readExtras()
  {
    this.mExtras = getIntent().getExtras();
    Object localObject;
    if (this.mExtras != null)
    {
      this.mMovieId = this.mExtras.getLong("net.flixster.android.EXTRA_MOVIE_ID");
      this.isSeason = this.mExtras.getBoolean("KEY_IS_SEASON");
      long l = this.mExtras.getLong("KEY_RIGHT_ID");
      if (this.mPlayTrailer == null)
        this.mPlayTrailer = Boolean.valueOf(this.mExtras.getBoolean("net.flixster.PLAY_TRAILER"));
      Logger.d("FlxMain", "MovieDetails.readExtras mMovieId:" + this.mMovieId);
      if (!this.isSeason)
        break label285;
      localObject = MovieDao.getSeason(this.mMovieId);
      this.mMovie = ((Movie)localObject);
      User localUser = AccountManager.instance().getUser();
      if ((localUser != null) && (l > 0L))
        this.right = localUser.getLockerRightFromRightId(l);
      Property localProperty = Properties.instance().getProperties();
      if ((this.right != null) || (localProperty == null) || (!localProperty.isGetGlueEnabled))
        break label297;
    }
    label285: label297: for (boolean bool = true; ; bool = false)
    {
      this.showGetGlue = bool;
      populatePage();
      DfpAd.DfpCustomTarget localDfpCustomTarget = new DfpAd.DfpCustomTarget(this.mMovie);
      this.mStickyTopAd.dfpCustomTarget = localDfpCustomTarget;
      this.mStickyBottomAd.dfpCustomTarget = localDfpCustomTarget;
      this.mScrollAd.dfpCustomTarget = localDfpCustomTarget;
      this.mStickyTopAd.mAdContextId = Long.valueOf(this.mMovie.getId());
      this.mStickyBottomAd.mAdContextId = Long.valueOf(this.mMovie.getId());
      this.mScrollAd.mAdContextId = Long.valueOf(this.mMovie.getId());
      return;
      localObject = MovieDao.getMovie(this.mMovieId);
      break;
    }
  }

  protected void NetworkAttemptsCancelled()
  {
    Logger.d("FlxMain", "MovieDetails.NetworkAttemptsCancelled()");
  }

  protected Intent createShareIntent()
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("text/plain");
    StringBuilder localStringBuilder = new StringBuilder(String.valueOf(createMovieSummary(this.mMovie)));
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = ApiBuilder.mobileUpsell();
    localIntent.putExtra("android.intent.extra.TEXT", getString(2131493297, arrayOfObject));
    return localIntent;
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt2 == -1) && (paramInt1 == 1010))
    {
      this.mFromRating = true;
      localBundle = paramIntent.getExtras();
      if (this.mReview == null)
      {
        this.mReview = new Review();
        this.mReview.type = 1;
      }
      this.mReview.comment = localBundle.getString("net.flixster.ReviewComment");
      Logger.d("FlxMain", "MovieDetails onActivityResult comment:" + this.mReview.comment);
      this.mReview.stars = localBundle.getDouble("net.flixster.ReviewStars");
      str = localBundle.getString("net.flixster.FeedUrl");
      if (str != null)
      {
        localIntent = new Intent(this, FeedPermissionPage.class);
        localIntent.putExtra("net.flixster.FeedUrl", str);
        startActivity(localIntent);
      }
    }
    while ((paramInt1 != 1000) || (!AdsArbiter.trailer().shouldShowPostitial()))
    {
      Bundle localBundle;
      String str;
      Intent localIntent;
      return;
    }
    AdsArbiter.trailer().postitialShown();
    new DfpPostTrailerInterstitial().loadAd(this, this.mMovie);
  }

  public void onClick(View paramView)
  {
    if (this.mMovie == null);
    do
    {
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                return;
                switch (paramView.getId())
                {
                default:
                  return;
                case 2130903074:
                  Trackers.instance().track(getGaTagPrefix() + "/criticreview", "Critic Review - " + this.mMovie.getProperty("title"));
                  Integer localInteger = (Integer)paramView.getTag();
                  Intent localIntent7 = new Intent("REVIEW_PAGE", null, this, ReviewPage.class);
                  if (localInteger == null)
                    localInteger = Integer.valueOf(0);
                  Logger.d("FlxMain", "MovieDetails.onClick criticreview  reviewIndex:" + localInteger);
                  this.mExtras.putInt("REVIEW_INDEX", localInteger.intValue());
                  this.mExtras.putLong("net.flixster.android.EXTRA_MOVIE_ID", this.mMovie.getId());
                  localIntent7.replaceExtras(this.mExtras);
                  startActivity(localIntent7);
                  return;
                case 2131165579:
                  Trackers.instance().track(getGaTagPrefix() + "/website/flixster", getGaTitlePrefix() + "Info (Flixster) - " + this.mMovie.getProperty("title"));
                  Intent localIntent6 = new Intent("android.intent.action.VIEW", Uri.parse(this.mMovie.getProperty("flixster")));
                  startActivity(localIntent6);
                  return;
                case 2131165580:
                  Trackers.instance().track(getGaTagPrefix() + "/website/rottentomatoes", getGaTitlePrefix() + "Website (RT) - " + this.mMovie.getProperty("title"));
                  Intent localIntent5 = new Intent("android.intent.action.VIEW", Uri.parse(this.mMovie.getProperty("rottentomatoes")));
                  startActivity(localIntent5);
                  return;
                case 2131165581:
                  Trackers.instance().track(getGaTagPrefix() + "/website/imdb", getGaTitlePrefix() + "Info (IMDB) - " + this.mMovie.getProperty("title"));
                  Intent localIntent4 = new Intent("android.intent.action.VIEW", Uri.parse(this.mMovie.getProperty("imdb")));
                  startActivity(localIntent4);
                  return;
                case 2131165522:
                case 2131165523:
                case 2131165528:
                case 2131165526:
                case 2131165524:
                case 2131165538:
                case 2131165566:
                case 2131165575:
                case 2131165551:
                case 2131165557:
                case 2131165529:
                case 2131165521:
                }
              }
              while (this.mMovie == null);
              Starter.launchTrailerForResult(this.mMovie, this, 1000);
              return;
            }
            while (this.mMovie == null);
            Intent localIntent3 = new Intent("SHOWTIMES", null, this, ShowtimesPage.class);
            localIntent3.putExtra("net.flixster.android.EXTRA_MOVIE_ID", this.mMovie.getId());
            startActivity(localIntent3);
            return;
          }
          while (this.mMovie == null);
          Trackers.instance().trackEvent("/getglue", null, "GetGlue", "Click");
          String str = ApiBuilder.getGlue(this.mMovie);
          Logger.d("FlxMain", "MovieDetails.onClick GetGlue " + str);
          Starter.launchBrowser(str, this);
          return;
        }
        while (this.mMovie == null);
        Intent localIntent2 = new Intent("MOVIE_ID", null, this, RateMoviePage.class);
        localIntent2.putExtra("net.flixster.android.EXTRA_MOVIE_ID", this.mMovieId);
        localIntent2.putExtra("title", this.mMovie.getProperty("title"));
        if (this.mReview != null)
        {
          localIntent2.putExtra("net.flixster.ReviewComment", this.mReview.comment);
          localIntent2.putExtra("net.flixster.ReviewStars", this.mReview.stars);
        }
        if (FlixsterApplication.getPlatformUsername() != null)
          localIntent2.putExtra("net.flixster.IsConnected", true);
        Logger.d("FlxMain", "MovieDetails.onClick() launch RateMoviePage mMovieId:" + this.mMovieId);
        startActivityForResult(localIntent2, 1010);
        return;
      }
      while (this.mMovie == null);
      Intent localIntent1 = new Intent(this, MovieGalleryPage.class);
      localIntent1.putExtra("net.flixster.android.EXTRA_MOVIE_ID", this.mMovie.getId());
      localIntent1.putExtra("title", this.mMovie.getProperty("title"));
      startActivity(localIntent1);
      return;
      LinearLayout localLinearLayout4 = (LinearLayout)findViewById(2131165565);
      ArrayList localArrayList2 = this.mMovie.getCriticReviewsList();
      int k = 3 + (this.mMovie.getFriendWantToSeeListSize() + this.mMovie.getFriendRatedReviewsListSize() + this.mMovie.getUserReviewsList().size());
      loadCriticReviews(3, localArrayList2.size(), k, localLinearLayout4, localArrayList2);
      localLinearLayout4.setVisibility(0);
      paramView.setVisibility(8);
      return;
      int j = 3 + (this.mMovie.getFriendWantToSeeListSize() + this.mMovie.getFriendRatedReviewsListSize());
      LinearLayout localLinearLayout3 = (LinearLayout)findViewById(2131165574);
      ArrayList localArrayList1 = this.mMovie.getUserReviewsList();
      loadUserReviews(3, localArrayList1.size(), j, localLinearLayout3, localArrayList1);
      localLinearLayout3.setVisibility(0);
      paramView.setVisibility(8);
      return;
      LinearLayout localLinearLayout2 = (LinearLayout)findViewById(2131165550);
      loadFriendReviews(3, this.mMovie.getFriendWantToSeeListSize(), 3, localLinearLayout2, this.mMovie.getFriendWantToSeeList());
      localLinearLayout2.setVisibility(0);
      paramView.setVisibility(8);
      return;
      int i = 3 + this.mMovie.getFriendWantToSeeListSize();
      LinearLayout localLinearLayout1 = (LinearLayout)findViewById(2131165556);
      loadFriendReviews(3, this.mMovie.getFriendRatedReviewsListSize(), i, localLinearLayout1, this.mMovie.getFriendRatedReviewsList());
      localLinearLayout1.setVisibility(0);
      paramView.setVisibility(8);
      return;
    }
    while (this.mNetflixMenuToAction == null);
    showDialog(0);
    return;
    if (this.right == null)
    {
      Starter.launchEpisodes(this.mMovieId, this);
      return;
    }
    Starter.launchEpisodes(this.mMovieId, this.right.rightId, this);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903131);
    createActionBar();
    this.mStickyTopAd = ((AdView)findViewById(2131165505));
    this.mStickyBottomAd = ((AdView)findViewById(2131165582));
    this.mScrollAd = ((AdView)findViewById(2131165226));
    this.mMovieDetails = this;
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog;
    switch (paramInt)
    {
    default:
      localDialog = super.onCreateDialog(paramInt);
    case 0:
      String[] arrayOfString1;
      int[] arrayOfInt;
      do
      {
        return localDialog;
        arrayOfString1 = getResources().getStringArray(2131623947);
        arrayOfInt = this.mNetflixMenuToAction;
        localDialog = null;
      }
      while (arrayOfInt == null);
      int i = this.mNetflixMenuToAction.length;
      String[] arrayOfString2 = new String[i];
      for (int j = 0; ; j++)
      {
        if (j >= i)
          return new AlertDialog.Builder(this).setTitle(2131493070).setItems(arrayOfString2, new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              Logger.d("FlxMain", "MovieDetails.onCreateDialog mNetflixMenuToAction[which]:" + MovieDetails.this.mNetflixMenuToAction[paramAnonymousInt]);
              switch (MovieDetails.this.mNetflixMenuToAction[paramAnonymousInt])
              {
              default:
                Logger.d("FlxMain", "MovieDetails.onCreateDialog -default-");
              case 0:
              case 1:
              case 3:
              case 9:
              case 4:
              case 10:
              case 7:
              case 11:
              case 8:
              case 12:
              case 5:
              case 2:
              case 6:
              }
              while (true)
              {
                MovieDetails.this.removeDialog(0);
                return;
                Intent localIntent = new Intent(MovieDetails.this.mMovieDetails, NetflixQueuePage.class);
                localIntent.setFlags(131072);
                MovieDetails.this.startActivity(localIntent);
                continue;
                MovieDetails.this.ScheduleAddToQueue(1, "/queues/disc/available");
                FlixsterApplication.sNetflixListIsDirty[1] = true;
                continue;
                MovieDetails.this.ScheduleAddToQueue(0, "/queues/disc/available");
                FlixsterApplication.sNetflixListIsDirty[1] = true;
                continue;
                MovieDetails.this.ScheduleMoveToBottom("/queues/disc/available");
                FlixsterApplication.sNetflixListIsDirty[1] = true;
                continue;
                MovieDetails.this.ScheduleAddToQueue(1, "/queues/instant/available");
                FlixsterApplication.sNetflixListIsDirty[2] = true;
                continue;
                MovieDetails.this.ScheduleAddToQueue(0, "/queues/instant/available");
                FlixsterApplication.sNetflixListIsDirty[2] = true;
                continue;
                MovieDetails.this.ScheduleMoveToBottom("/queues/instant/available");
                FlixsterApplication.sNetflixListIsDirty[2] = true;
                continue;
                MovieDetails.this.ScheduleAddToQueue(0, "/queues/disc/saved");
                FlixsterApplication.sNetflixListIsDirty[3] = true;
                continue;
                MovieDetails.this.ScheduleRemoveFromQueue("/queues/disc/available");
                FlixsterApplication.sNetflixListIsDirty[1] = true;
                FlixsterApplication.sNetflixListIsDirty[3] = true;
                continue;
                MovieDetails.this.ScheduleRemoveFromQueue("/queues/instant/available");
                FlixsterApplication.sNetflixListIsDirty[2] = true;
              }
            }
          }).create();
        arrayOfString2[j] = arrayOfString1[this.mNetflixMenuToAction[j]];
      }
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setTitle("Netflix Error");
    localBuilder.setMessage("You can't add movies to your Netflix Queue because it is full.");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("OK", null);
    return localBuilder.create();
  }

  public void onDestroy()
  {
    this.mScrollAd.destroy();
    super.onDestroy();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getAction() == 0) && (paramInt == 4))
    {
      Logger.d("FlxMain", "MovieDetails.onKeyDown - call finish");
      finish();
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165955:
    }
    Tracker localTracker = Trackers.instance();
    String str1 = getGaTagPrefix() + "/info";
    StringBuilder localStringBuilder = new StringBuilder(String.valueOf(getGaTitlePrefix())).append("Info - ");
    if (this.mMovie != null);
    for (String str2 = this.mMovie.getTitle(); ; str2 = "")
    {
      localTracker.trackEvent(str1, str2, "ActionBar", "Share");
      break;
    }
  }

  protected void onResume()
  {
    super.onResume();
    if (this.mMovie == null)
      readExtras();
    String str2;
    if (this.mMovie != null)
    {
      String str3 = this.mMovie.getProperty("title");
      setActionBarTitle(str3);
      Trackers.instance().track(getGaTagPrefix() + "/info", getGaTitlePrefix() + "Info - " + str3);
      if (this.showGetGlue)
        Trackers.instance().trackEvent("/getglue", null, "GetGlue", "Impression");
      if (WidgetProvider.isOriginatedFromWidget(getIntent()))
      {
        Tracker localTracker = Trackers.instance();
        String str1 = getGaTagPrefix() + "/info";
        StringBuilder localStringBuilder = new StringBuilder(String.valueOf(getGaTitlePrefix())).append("Info - ");
        if (this.mMovie == null)
          break label304;
        str2 = this.mMovie.getTitle();
        label199: localTracker.trackEvent(str1, str2, "WidgetEntrance", "Movie");
      }
      if (this.right != null)
        break label312;
      refreshSticky();
      this.mScrollAd.refreshAds();
    }
    while (true)
    {
      ScheduleLoadMovieTask(100L);
      return;
      Trackers.instance().track(getGaTagPrefix() + "/info", getGaTitlePrefix() + "Info - ");
      break;
      label304: str2 = "";
      break label199;
      label312: Logger.d("FlxMain", "MovieDetails.onResume movie in user's collection, ads disabled");
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    removeDialog(0);
  }

  protected void retryAction()
  {
    this.mMovieDetails.ScheduleLoadMovieTask(1000L);
  }

  private static final class ActorViewHolder
  {
    RelativeLayout actorLayout;
    TextView charsView;
    ImageView imageView;
    TextView nameView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.MovieDetails
 * JD-Core Version:    0.6.2
 */