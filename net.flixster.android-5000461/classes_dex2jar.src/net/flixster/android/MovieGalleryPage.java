package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.PhotoDao;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Photo;

public class MovieGalleryPage extends FlixsterActivity
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  private static final int MAX_PHOTOS = 50;
  private AdView mDefaultAd;
  private Movie mMovie;
  private PhotosListAdapter mPhotosListAdapter;
  private View.OnClickListener photoClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Photo localPhoto = (Photo)paramAnonymousView.getTag();
      if (localPhoto != null)
      {
        int i = MovieGalleryPage.this.photos.indexOf(localPhoto);
        if (i < 0)
          i = 0;
        Intent localIntent = new Intent("PHOTO", null, MovieGalleryPage.this.getApplicationContext(), ScrollGalleryPage.class);
        localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", MovieGalleryPage.this.mMovie.getId());
        localIntent.putExtra("PHOTO_INDEX", i);
        localIntent.putExtra("PHOTO_COUNT", MovieGalleryPage.this.photos.size());
        localIntent.putExtra("title", MovieGalleryPage.this.mMovie.getProperty("title"));
        MovieGalleryPage.this.startActivity(localIntent);
      }
    }
  };
  private AdapterView.OnItemClickListener photoItemClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      paramAnonymousView.performClick();
    }
  };
  private ArrayList<Object> photos;
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "MovieGalleryPage.updateHandler");
      if (MovieGalleryPage.this.mMovie.mPhotos != null)
        MovieGalleryPage.this.updatePage();
      while (MovieGalleryPage.this.isFinishing())
        return;
      MovieGalleryPage.this.showDialog(1);
    }
  };

  private void scheduleUpdatePageTask()
  {
    TimerTask local5 = new TimerTask()
    {
      public void run()
      {
        long l = MovieGalleryPage.this.mMovie.getId();
        try
        {
          MovieGalleryPage.this.mMovie.mPhotos = PhotoDao.getMoviePhotos(l);
          MovieGalleryPage.this.updateHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          while (true)
          {
            Logger.e("FlxMain", "MovieGalleryPage.scheduleUpdatePageTask:failed to get photos", localDaoException);
            MovieGalleryPage.this.mMovie.mPhotos = null;
          }
        }
      }
    };
    if (this.mPageTimer != null)
      this.mPageTimer.schedule(local5, 100L);
  }

  private void updatePage()
  {
    GridView localGridView;
    if (this.mMovie != null)
    {
      if (this.mMovie.getTitle() != null)
        setActionBarTitle(getString(2131492925) + " - " + this.mMovie.getTitle());
      if (this.mMovie.mPhotos != null)
      {
        localGridView = (GridView)findViewById(2131165496);
        if (localGridView.getAdapter() == null)
          this.photos = new ArrayList();
      }
    }
    for (int i = 0; ; i++)
    {
      if ((i >= this.mMovie.mPhotos.size()) || (i >= 50))
      {
        this.mPhotosListAdapter = new PhotosListAdapter(this, this.photos, this.photoClickListener);
        localGridView.setAdapter(this.mPhotosListAdapter);
        localGridView.setOnItemClickListener(this.photoItemClickListener);
        localGridView.setClickable(true);
        return;
      }
      Object localObject = this.mMovie.mPhotos.get(i);
      this.photos.add(localObject);
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903128);
    createActionBar();
    this.mDefaultAd = ((AdView)findViewById(2131165226));
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.mMovie = MovieDao.getMovie(localBundle.getLong("net.flixster.android.EXTRA_MOVIE_ID"));
      if (this.mMovie.getProperty("title") == null)
      {
        String str = localBundle.getString("title");
        this.mMovie.setProperty("title", str);
      }
    }
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        MovieGalleryPage.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder.create();
  }

  public void onDestroy()
  {
    this.mDefaultAd.destroy();
    super.onDestroy();
  }

  protected void onResume()
  {
    super.onResume();
    Trackers.instance().track("/photo/gallery/movie", "Photo Gallery - Movie - " + this.mMovie.getTitle());
    scheduleUpdatePageTask();
    this.mDefaultAd.refreshAds();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.MovieGalleryPage
 * JD-Core Version:    0.6.2
 */