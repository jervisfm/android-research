package net.flixster.android;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.List;
import net.flixster.android.model.Movie;

public class MovieGridViewAdapter extends BaseAdapter
{
  private final Context context;
  private final int movieType;
  private final List<Movie> movies;

  MovieGridViewAdapter(Context paramContext, List<Movie> paramList, int paramInt)
  {
    this.context = paramContext;
    this.movies = paramList;
    this.movieType = paramInt;
  }

  public int getCount()
  {
    return this.movies.size();
  }

  public Object getItem(int paramInt)
  {
    return this.movies.get(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if ((paramView == null) || (!(paramView instanceof MovieCollectionItem)))
      paramView = new MovieCollectionItem(this.context);
    while (true)
    {
      ((MovieCollectionItem)paramView).load((Movie)this.movies.get(paramInt), this.movieType);
      return paramView;
      ((MovieCollectionItem)paramView).reset();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.MovieGridViewAdapter
 * JD-Core Version:    0.6.2
 */