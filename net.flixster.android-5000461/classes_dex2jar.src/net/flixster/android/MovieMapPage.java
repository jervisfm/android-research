package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.flixster.android.activity.common.DecoratedSherlockMapActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Theater;
import net.flixster.android.util.PostalCodeUtils;

public class MovieMapPage extends DecoratedSherlockMapActivity
{
  private static final int DIALOG_NETWORK_FAIL = 2;
  private static final int DIALOG_THEATERS = 1;
  private static final int DIALOG_THEATERS_NONE = 3;
  private static final int DIALOG_ZIP = 4;
  private static final int DIALOG_ZIP_ERROR = 5;
  private static final int DIALOG_ZIP_NONE = 6;
  private static final int MAP_ZOOM_DEFAULT = 13;
  private Handler dismissProgressHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((MovieMapPage.this.progressDialog != null) && (MovieMapPage.this.progressDialog.isShowing()) && (!MovieMapPage.this.isFinishing()))
        MovieMapPage.this.removeDialog(1);
    }
  };
  private MapController mapController;
  private MapView mapView;
  private Movie movie;
  private ProgressDialog progressDialog;
  private View.OnClickListener settingsClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      switch (paramAnonymousView.getId())
      {
      default:
        return;
      case 2131165800:
      }
      Intent localIntent = new Intent(MovieMapPage.this.getApplicationContext(), SettingsPage.class);
      MovieMapPage.this.startActivity(localIntent);
    }
  };
  private Handler showProgressHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((MovieMapPage.this.progressDialog == null) || (!MovieMapPage.this.progressDialog.isShowing())) && (!MovieMapPage.this.isFinishing()))
        MovieMapPage.this.showDialog(1);
    }
  };
  private List<Theater> theaters;
  private Timer timer;
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "MyWantToSeePage.updateHandler");
      if (MovieMapPage.this.theaters != null)
        MovieMapPage.this.updatePage();
      while (MovieMapPage.this.isFinishing())
        return;
      MovieMapPage.this.showDialog(2);
    }
  };

  private List<Theater> getTheaters()
  {
    HashMap localHashMap = FlixsterApplication.getFavoriteTheatersList();
    try
    {
      if (FlixsterApplication.getUseLocationServiceFlag())
        return TheaterDao.findTheatersByMovieLocation(FlixsterApplication.getCurrentLatitude(), FlixsterApplication.getCurrentLongitude(), FlixsterApplication.getShowtimesDate(), this.movie.getId(), localHashMap);
      if ((FlixsterApplication.getUserLatitude() != 0.0D) && (FlixsterApplication.getUserLongitude() != 0.0D))
        return TheaterDao.findTheatersByMovieLocation(FlixsterApplication.getUserLatitude(), FlixsterApplication.getUserLongitude(), FlixsterApplication.getShowtimesDate(), this.movie.getId(), localHashMap);
      List localList = TheaterDao.findTheatersByMovieLocation(FlixsterApplication.getUserLatitude(), FlixsterApplication.getUserLongitude(), FlixsterApplication.getShowtimesDate(), this.movie.getId(), localHashMap);
      return localList;
    }
    catch (DaoException localDaoException)
    {
      Logger.e("FlxMain", "problem loading theaters: " + localDaoException.getMessage());
    }
    return null;
  }

  private void scheduleUpdatePageTask()
  {
    TimerTask local10 = new TimerTask()
    {
      public void run()
      {
        MovieMapPage.this.theaters = MovieMapPage.this.getTheaters();
        MovieMapPage.this.updateHandler.sendEmptyMessage(0);
      }
    };
    this.timer.schedule(local10, 100L);
  }

  private void updatePage()
  {
    double d1;
    double d2;
    if (this.theaters != null)
      if (FlixsterApplication.getUseLocationServiceFlag())
      {
        d1 = FlixsterApplication.getCurrentLatitude();
        d2 = FlixsterApplication.getCurrentLongitude();
        if ((d1 != 0.0D) || (d2 != 0.0D))
          break label93;
        setContentView(2130903173);
        ((Button)findViewById(2131165800)).setOnClickListener(this.settingsClickListener);
        ((TextView)findViewById(2131165799)).setText("No theaters found");
      }
    while (true)
    {
      this.dismissProgressHandler.sendEmptyMessage(0);
      return;
      d1 = FlixsterApplication.getUserLatitude();
      d2 = FlixsterApplication.getUserLongitude();
      break;
      label93: LinearLayout localLinearLayout = (LinearLayout)findViewById(2131165796);
      if (localLinearLayout != null)
        localLinearLayout.setVisibility(4);
      GeoPoint localGeoPoint1 = new GeoPoint((int)(1000000.0D * d1), (int)(1000000.0D * d2));
      this.mapController.setCenter(localGeoPoint1);
      Resources localResources = getResources();
      TheaterItemizedOverlay localTheaterItemizedOverlay1 = new TheaterItemizedOverlay(this, localResources.getDrawable(2130837677));
      localTheaterItemizedOverlay1.addOverlay(new OverlayItem(localGeoPoint1, "Current Location", null));
      TheaterItemizedOverlay localTheaterItemizedOverlay2 = new TheaterItemizedOverlay(this, localResources.getDrawable(2130837842));
      for (int i = 0; ; i++)
      {
        if (i >= this.theaters.size())
        {
          List localList = this.mapView.getOverlays();
          localList.clear();
          localList.add(localTheaterItemizedOverlay1);
          if (localTheaterItemizedOverlay2.size() > 0)
            localList.add(localTheaterItemizedOverlay2);
          this.mapView.invalidate();
          break;
        }
        Theater localTheater = (Theater)this.theaters.get(i);
        if (localTheater != null)
        {
          int j = (int)(1000000.0D * localTheater.latitude);
          int k = (int)(1000000.0D * localTheater.longitude);
          GeoPoint localGeoPoint2 = new GeoPoint(j, k);
          OverlayItem localOverlayItem = new OverlayItem(localGeoPoint2, localTheater.getProperty("name") + "\n" + localTheater.getProperty("address"), String.valueOf(localTheater.getId()));
          localTheaterItemizedOverlay2.addOverlay(localOverlayItem);
        }
      }
      showDialog(2);
    }
  }

  public boolean isRouteDisplayed()
  {
    return false;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxMain", "MovieMapPage.onCreate()");
    setContentView(2130903127);
    createActionBar();
    this.timer = new Timer();
    this.mapView = ((MapView)findViewById(2131165412));
    ((LinearLayout)findViewById(2131165413)).addView(this.mapView.getZoomControls());
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1:
      this.progressDialog = new ProgressDialog(this);
      this.progressDialog.setMessage(getResources().getString(2131493173));
      this.progressDialog.setIndeterminate(true);
      this.progressDialog.setCancelable(true);
      this.progressDialog.setCanceledOnTouchOutside(true);
      return this.progressDialog;
    case 2:
      AlertDialog.Builder localBuilder5 = new AlertDialog.Builder(this);
      localBuilder5.setMessage("The network connection failed. Press Retry to make another attempt.");
      localBuilder5.setTitle("Network Error");
      localBuilder5.setCancelable(true);
      localBuilder5.setPositiveButton("Retry", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          MovieMapPage.this.showProgressHandler.sendEmptyMessage(0);
          MovieMapPage.this.scheduleUpdatePageTask();
        }
      });
      localBuilder5.setNegativeButton(getResources().getString(2131492938), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          MovieMapPage.this.dismissProgressHandler.sendEmptyMessage(0);
        }
      });
      return localBuilder5.create();
    case 3:
      AlertDialog.Builder localBuilder4 = new AlertDialog.Builder(this);
      localBuilder4.setMessage("No theaters were found for this postal code.");
      localBuilder4.setTitle("No Theaters.");
      localBuilder4.setCancelable(false);
      localBuilder4.setPositiveButton("OK", null);
      return localBuilder4.create();
    case 4:
      final View localView = LayoutInflater.from(this).inflate(2130903189, null);
      localView.setMinimumWidth(300);
      AlertDialog.Builder localBuilder3 = new AlertDialog.Builder(this);
      localBuilder3.setView(localView);
      localBuilder3.setPositiveButton(2131492937, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          String str = PostalCodeUtils.parseZipcodeForShowtimes(((EditText)localView.findViewById(2131165945)).getText().toString());
          if (str != null)
          {
            FlixsterApplication.setUserLocation(str);
            FlixsterApplication.setUseLocationServiceFlag(false);
            MovieMapPage.this.scheduleUpdatePageTask();
            return;
          }
          MovieMapPage.this.showDialog(5);
        }
      });
      localBuilder3.setNegativeButton(2131492938, null);
      return localBuilder3.create();
    case 5:
      AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(this);
      localBuilder2.setMessage("Invalid Postal Code");
      localBuilder2.setTitle("Invalid Postal Code");
      localBuilder2.setCancelable(false).setPositiveButton("OK", null);
      return localBuilder2.create();
    case 6:
    }
    AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(this);
    localBuilder1.setTitle("Postal Code Warning");
    localBuilder1.setMessage("Can't detect postal code, please retry or manually enter postal code.");
    localBuilder1.setCancelable(true);
    localBuilder1.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        MovieMapPage.this.scheduleUpdatePageTask();
        Logger.d("FlxMain", "retry on emptyzip");
      }
    });
    localBuilder1.setNeutralButton("Manual", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        MovieMapPage.this.showDialog(4);
      }
    });
    localBuilder1.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder1.create();
  }

  public void onDestroy()
  {
    super.onDestroy();
    if (this.timer != null)
    {
      this.timer.cancel();
      this.timer.purge();
    }
    this.timer = null;
  }

  public void onPause()
  {
    super.onPause();
  }

  public void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "MovieMapPage.onResume()");
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.movie = new Movie(localBundle.getLong("net.flixster.android.EXTRA_MOVIE_ID"));
      this.movie.setProperty("title", localBundle.getString("title"));
      this.movie.setProperty("runningTime", localBundle.getString("runningTime"));
      this.movie.setProperty("MOVIE_ACTORS_SHORT", localBundle.getString("MOVIE_ACTORS_SHORT"));
      this.movie.setProperty("mpaa", localBundle.getString("mpaa"));
      this.movie.setProperty("thumbnail", localBundle.getString("thumbnail"));
      this.movie.setIntProperty("popcornScore", Integer.valueOf(localBundle.getInt("popcornScore")));
      this.movie.setIntProperty("rottenTomatoes", Integer.valueOf(localBundle.getInt("rottenTomatoes")));
      setActionBarTitle(getString(2131493136) + " - " + this.movie.getTitle());
    }
    Trackers.instance().track("/movie/showtimes/map", "Movie Info - " + this.movie.getProperty("title"));
    this.mapController = this.mapView.getController();
    this.mapController.setZoom(13);
    this.mapView.displayZoomControls(true);
    this.showProgressHandler.sendEmptyMessage(0);
    scheduleUpdatePageTask();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.MovieMapPage
 * JD-Core Version:    0.6.2
 */