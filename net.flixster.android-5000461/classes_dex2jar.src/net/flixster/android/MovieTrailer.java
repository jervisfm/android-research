package net.flixster.android;

import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.flixster.android.activity.common.DecoratedActivity;
import com.flixster.android.activity.decorator.TimerDecorator;
import com.flixster.android.ads.AdsArbiter;
import com.flixster.android.ads.AdsArbiter.TrailerAdsArbiter;
import com.flixster.android.ads.DfpVideoAdRunner;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.F;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.OsInfo;
import com.flixster.android.utils.OsInfo.NetworkSpeed;
import com.flixster.android.utils.Properties;
import com.flixster.android.utils.VersionedViewHelper;
import com.google.ads.interactivemedia.api.AdsManager.AdEvent;
import com.google.ads.interactivemedia.api.AdsManager.AdEventListener;
import com.google.ads.interactivemedia.api.player.VideoAdPlayer;
import com.google.ads.interactivemedia.api.player.VideoAdPlayer.VideoAdPlayerCallback;
import com.google.ads.interactivemedia.demoapp.player.TrackingVideoView;
import net.flixster.android.ads.AdManager;
import net.flixster.android.ads.ImageManager;
import net.flixster.android.ads.model.Ad;
import net.flixster.android.ads.model.DfpVideoAd;
import net.flixster.android.ads.model.FlixsterAd;
import net.flixster.android.data.MovieDao;
import net.flixster.android.model.ImageOrder;

public class MovieTrailer extends DecoratedActivity
  implements MediaPlayer.OnPreparedListener, MediaPlayer.OnInfoListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener, View.OnClickListener, VideoAdPlayer, AdsManager.AdEventListener
{
  private static final int SKIP_BUTTON_DELAY = 3000;
  private TextView adLabel;
  private TextView adSkipButton;
  private TextView adVisitButton;
  private AdaptiveSwitchLogic adaptiveSwitchLogic;
  private FlixsterAd backgroundAd;
  private ImageView backgroundImage;
  private String contentUrlHigh;
  private String contentUrlLow;
  private String contentUrlMed;
  private MediaController controls;
  private DfpVideoAdRunner dfpVideoAdRunner;
  private boolean isAdStarted;
  private boolean isBackPressed;
  private boolean isDfpVideoAvailable;
  private long movieId;
  private int savedContentPosition;
  private ProgressBar throbber;
  private TimerDecorator timerDecorator;
  private String title;
  private TrackingVideoView video;
  private int videoPosition;

  private String getContentUrlHigh()
  {
    if (this.contentUrlHigh == null)
      return getContentUrlMed();
    return this.contentUrlHigh;
  }

  private String getContentUrlMed()
  {
    if (this.contentUrlMed == null)
      return this.contentUrlLow;
    return this.contentUrlMed;
  }

  private int getRemainingTime()
  {
    return (this.video.getDuration() - this.video.getCurrentPosition()) / 1000;
  }

  private String getRemainingTimeAdLabel()
  {
    return getText(2131493302) + ": " + getRemainingTime();
  }

  private String getVideoUrl()
  {
    OsInfo.NetworkSpeed localNetworkSpeed = OsInfo.instance().getNetworkSpeed();
    if ((Properties.instance().shouldUseLowBitrateForTrailer()) && (this.contentUrlLow != null))
      return this.contentUrlLow;
    if (Properties.instance().isGoogleTv())
      return getContentUrlHigh();
    if (Properties.instance().isHoneycombTablet())
    {
      switch ($SWITCH_TABLE$com$flixster$android$utils$OsInfo$NetworkSpeed()[localNetworkSpeed.ordinal()])
      {
      default:
        return this.contentUrlLow;
      case 5:
        if (Properties.instance().isScreenXlarge())
          return getContentUrlHigh();
        return getContentUrlMed();
      case 4:
      }
      return getContentUrlMed();
    }
    if ((Properties.instance().isAtLeastHdpi()) || (Properties.instance().isScreenLarge()))
    {
      switch ($SWITCH_TABLE$com$flixster$android$utils$OsInfo$NetworkSpeed()[localNetworkSpeed.ordinal()])
      {
      default:
        return this.contentUrlLow;
      case 5:
        if (F.API_LEVEL >= 9)
          return getContentUrlHigh();
        return getContentUrlMed();
      case 3:
      case 4:
      }
      return getContentUrlMed();
    }
    return this.contentUrlLow;
  }

  private void loadContent()
  {
    Logger.d("FlxMain", "MovieTrailer.loadContent");
    this.backgroundAd = ((FlixsterAd)AdManager.instance().getAd("TrailerBG", null, Long.valueOf(this.movieId)));
    if ((this.backgroundAd != null) && (AdsArbiter.trailer().shouldShowSponsor()) && (this.backgroundAd.imageUrl != null))
    {
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
      localLayoutParams.addRule(12);
      localLayoutParams.addRule(14);
      localLayoutParams.setMargins(0, 0, 0, 15);
      this.throbber.setLayoutParams(localLayoutParams);
      ImageManager.getInstance().enqueImage(new ImageOrder(0, this.backgroundAd, this.backgroundAd.imageUrl, this.backgroundImage, new Handler(new BackgroundAdImageCallback(null))));
    }
    if (!this.isAdStarted)
      this.video.setMediaController(this.controls);
    this.video.setVisibility(0);
    String str = getVideoUrl();
    this.adaptiveSwitchLogic.videoUrlSet(str);
    this.video.setVideoPath(str);
    if (this.savedContentPosition > 0)
      this.video.seekTo(this.savedContentPosition);
    showLoading();
    Trackers.instance().track("/trailer", "Trailer - " + this.title);
  }

  private void pauseContent()
  {
    this.savedContentPosition = this.video.getCurrentPosition();
    this.video.stopPlayback();
  }

  private void postTrailer()
  {
    finish();
  }

  private void removeLoading()
  {
    this.throbber.setVisibility(8);
  }

  private void resumeContent()
  {
    loadContent();
  }

  private void scheduleRemainingTime()
  {
    this.timerDecorator.scheduleTask(new RemainingTimeCallback(null), 0L, 1000L);
  }

  private void scheduleSkipButton()
  {
    this.timerDecorator.scheduleTask(new SkipButtonCallback(null), 3000L);
  }

  private void showLoading()
  {
    this.throbber.setVisibility(0);
  }

  private void trackEventPrerollStart()
  {
    Trackers.instance().trackEvent("/trailer", "Trailer - " + this.title, "TrailerWithPreroll", "PrerollStart");
  }

  private void trackEventTrailerEnd()
  {
    Tracker localTracker = Trackers.instance();
    String str1 = "Trailer - " + this.title;
    if (this.isDfpVideoAvailable);
    for (String str2 = "TrailerWithPreroll"; ; str2 = "TrailerWithoutPreroll")
    {
      localTracker.trackEvent("/trailer", str1, str2, "TrailerEnd");
      return;
    }
  }

  private void trackEventTrailerStart()
  {
    Tracker localTracker = Trackers.instance();
    String str1 = "Trailer - " + this.title;
    if (this.isDfpVideoAvailable);
    for (String str2 = "TrailerWithPreroll"; ; str2 = "TrailerWithoutPreroll")
    {
      localTracker.trackEvent("/trailer", str1, str2, "TrailerStart");
      return;
    }
  }

  public void addCallback(VideoAdPlayer.VideoAdPlayerCallback paramVideoAdPlayerCallback)
  {
    Logger.d("FlxAd", "MovieTrailer.addCallback DFP");
    this.video.addCallback(paramVideoAdPlayerCallback);
  }

  public void onAdEvent(AdsManager.AdEvent paramAdEvent)
  {
    Logger.d("FlxAd", "MovieTrailer.onAdEvent DFP " + paramAdEvent.getEventType());
    switch ($SWITCH_TABLE$com$google$ads$interactivemedia$api$AdsManager$AdEventType()[paramAdEvent.getEventType().ordinal()])
    {
    case 2:
    default:
    case 3:
    case 4:
      do
      {
        do
          return;
        while (!this.video.isPlaying());
        pauseContent();
        return;
      }
      while (this.isBackPressed);
      this.adLabel.setVisibility(8);
      this.adVisitButton.setVisibility(8);
      this.adSkipButton.setVisibility(8);
      this.timerDecorator.cancelTimer();
      this.isAdStarted = false;
      resumeContent();
      return;
    case 1:
    }
    this.dfpVideoAdRunner.unloadAd();
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
      return;
    case 2131165921:
      this.dfpVideoAdRunner.unloadAd();
      return;
    case 2131165920:
    }
    this.video.onClick();
  }

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    Logger.d("FlxMain", "MovieTrailer.onCompletion");
    if (this.isAdStarted)
    {
      this.video.onCompletion();
      return;
    }
    trackEventTrailerEnd();
    this.video.stopPlayback();
    this.video.setVideoURI(null);
    this.video.setVisibility(4);
    postTrailer();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    TimerDecorator localTimerDecorator = new TimerDecorator(this);
    this.timerDecorator = localTimerDecorator;
    addDecorator(localTimerDecorator);
    this.timerDecorator.onCreate();
    String str;
    if (F.API_LEVEL < 9)
    {
      setRequestedOrientation(0);
      VersionedViewHelper.dimSystemNavigation(this);
      Bundle localBundle = getIntent().getExtras();
      if (localBundle != null)
      {
        this.movieId = localBundle.getLong("net.flixster.android.EXTRA_MOVIE_ID");
        this.title = localBundle.getString("title");
        this.contentUrlHigh = localBundle.getString("high");
        this.contentUrlMed = localBundle.getString("med");
        this.contentUrlLow = localBundle.getString("low");
        Logger.d("FlxMain", "MovieTrailer.onCreate high: " + this.contentUrlHigh);
        Logger.d("FlxMain", "MovieTrailer.onCreate med: " + this.contentUrlMed);
        Logger.d("FlxMain", "MovieTrailer.onCreate low: " + this.contentUrlLow);
        if (this.contentUrlLow == null)
        {
          if (this.contentUrlMed == null)
            break label541;
          str = this.contentUrlMed;
        }
      }
    }
    Ad localAd;
    while (true)
    {
      this.contentUrlLow = str;
      Logger.w("FlxMain", "MovieTrailer.onCreate substitue for null contentUrlLow");
      Logger.d("FlxMain", "MovieTrailer.onCreate high: " + this.contentUrlHigh);
      Logger.d("FlxMain", "MovieTrailer.onCreate med: " + this.contentUrlMed);
      Logger.d("FlxMain", "MovieTrailer.onCreate low: " + this.contentUrlLow);
      setContentView(2130903184);
      this.backgroundImage = ((ImageView)findViewById(2131165918));
      this.throbber = ((ProgressBar)findViewById(2131165241));
      this.adLabel = ((TextView)findViewById(2131165919));
      this.adSkipButton = ((TextView)findViewById(2131165921));
      this.adVisitButton = ((TextView)findViewById(2131165920));
      this.video = ((TrackingVideoView)findViewById(2131165917));
      this.video.setOnPreparedListener(this);
      this.video.setOnErrorListener(this);
      this.video.setOnCompletionListener(this);
      this.video.requestFocus();
      this.controls = new MediaController(this);
      this.adaptiveSwitchLogic = new AdaptiveSwitchLogic(this.contentUrlLow, null);
      if (!AdsArbiter.trailer().shouldShowPreroll())
        break label595;
      localAd = AdManager.instance().getAd("Preroll");
      if (localAd == null)
        break label595;
      if (!(localAd instanceof DfpVideoAd))
        break label563;
      AdManager.instance().trackEvent((DfpVideoAd)localAd, "Impression");
      showLoading();
      DfpVideoAdRunner localDfpVideoAdRunner = new DfpVideoAdRunner(this, this, this, MovieDao.getMovie(this.movieId));
      this.dfpVideoAdRunner = localDfpVideoAdRunner;
      localDfpVideoAdRunner.showAd();
      return;
      setRequestedOrientation(6);
      break;
      label541: if (this.contentUrlHigh != null)
        str = this.contentUrlHigh;
      else
        str = null;
    }
    label563: Logger.w("FlxMain", "MovieTrailer.onCreate unknown preroll type " + localAd);
    trackEventTrailerStart();
    loadContent();
    return;
    label595: trackEventTrailerStart();
    loadContent();
  }

  public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    removeLoading();
    return false;
  }

  public boolean onInfo(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    boolean bool = true;
    switch (paramInt1)
    {
    default:
      bool = false;
    case 701:
      do
      {
        return bool;
        Logger.d("FlxMain", "MovieTrailer.onInfo buffering started");
      }
      while ((this.isAdStarted) || (this.contentUrlLow == null));
      if (this.adaptiveSwitchLogic.shouldSwitch())
      {
        Logger.i("FlxMain", "MovieTrailer.onInfo adaptive switching");
        showLoading();
        int i = this.video.getCurrentPosition();
        this.video.stopPlayback();
        this.video.setVideoURI(null);
        this.video.setVideoPath(this.contentUrlLow);
        this.video.seekTo(i);
        return bool;
      }
      this.adaptiveSwitchLogic.bufferStarted(this.video.getCurrentPosition());
      return bool;
    case 702:
    }
    Logger.d("FlxMain", "MovieTrailer.onInfo buffering ended");
    this.adaptiveSwitchLogic.bufferEnded();
    return bool;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getAction() == 0) && (paramInt == 4))
    {
      Logger.d("FlxMain", "MovieDetails.onKeyDown back");
      this.isBackPressed = true;
      if (this.isAdStarted)
        this.dfpVideoAdRunner.unloadAd();
      this.video.stopPlayback();
      this.video.setVideoURI(null);
      this.video.setVisibility(4);
      postTrailer();
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onPause()
  {
    super.onPause();
    Logger.d("FlxMain", "MovieTrailer.onPause");
    if (this.video.isPlaying())
    {
      this.videoPosition = this.video.getCurrentPosition();
      this.video.pause();
    }
  }

  public void onPrepared(MediaPlayer paramMediaPlayer)
  {
    Logger.d("FlxMain", "MovieTrailer.onPrepared");
    this.backgroundImage.setVisibility(8);
    removeLoading();
    if (this.isAdStarted)
    {
      this.isDfpVideoAvailable = true;
      AdsArbiter.trailer().prerollShown();
      Trackers.instance().trackEvent("/preroll/dfp", "Preroll - DFP", "Preroll", "Impression");
      trackEventPrerollStart();
      this.adLabel.setText(getRemainingTimeAdLabel());
      this.adLabel.setVisibility(0);
      this.adVisitButton.setVisibility(0);
      this.adVisitButton.setOnClickListener(this);
      scheduleSkipButton();
      scheduleRemainingTime();
      paramMediaPlayer.setOnInfoListener(null);
    }
    while (true)
    {
      this.video.start();
      return;
      VersionedViewHelper.hideSystemNavigation(this);
      paramMediaPlayer.setOnInfoListener(this);
      this.adaptiveSwitchLogic.videoPrepared();
    }
  }

  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    Logger.v("FlxMain", "MovieTrailer.onWindowFocusChanged hasFocus:" + paramBoolean + " isPlaying:" + this.video.isPlaying() + " mCurrentPosition:" + this.videoPosition);
    if ((paramBoolean) && (this.video.isPlaying()))
      VersionedViewHelper.hideSystemNavigation(this);
    if (paramBoolean);
    try
    {
      if ((!this.video.isPlaying()) && (this.videoPosition > 0))
      {
        Logger.d("FlxMain", "MovieTrailer.onWindowFocusChanged start");
        this.video.start();
        if (this.video.getCurrentPosition() < this.videoPosition)
        {
          this.video.seekTo(this.videoPosition);
          this.videoPosition = 0;
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.e("FlxMain", "MovieTrailer.onWindowFocusChanged exception", localException);
    }
  }

  public void playAd(String paramString)
  {
    Logger.i("FlxAd", "MovieTrailer.playAd DFP");
    this.isAdStarted = true;
    this.video.enableTracking();
    this.video.setVideoPath(paramString);
  }

  public void removeCallback(VideoAdPlayer.VideoAdPlayerCallback paramVideoAdPlayerCallback)
  {
    Logger.d("FlxAd", "MovieTrailer.removeCallback DFP");
    this.video.removeCallback(paramVideoAdPlayerCallback);
  }

  public void stopAd()
  {
    Logger.i("FlxAd", "MovieTrailer.stopAd DFP");
    this.video.stopPlayback();
    this.video.disableTracking();
    this.video.setVisibility(4);
    this.adLabel.setVisibility(8);
    this.adVisitButton.setVisibility(8);
    this.adSkipButton.setVisibility(8);
    this.timerDecorator.cancelTimer();
    this.isAdStarted = false;
  }

  private static class AdaptiveSwitchLogic
  {
    private long bufferEndedTime;
    private long bufferStartedTime;
    private boolean hasAdaptiveSwitched = Properties.instance().shouldUseLowBitrateForTrailer();
    private boolean ignoreBufferEnd;
    private boolean isBufferTimeTooLong;
    private int lastPlaybackPosition;
    private String urlLow;

    private AdaptiveSwitchLogic(String paramString)
    {
      this.urlLow = paramString;
    }

    private void bufferEnded()
    {
      this.bufferEndedTime = System.currentTimeMillis();
      if (this.ignoreBufferEnd)
      {
        Logger.d("FlxMain", "MovieTrailer.AdaptiveSwitchLogic.bufferEnded ignored");
        return;
      }
      if (this.hasAdaptiveSwitched)
      {
        Logger.d("FlxMain", "MovieTrailer.AdaptiveSwitchLogic.bufferEnded already switched or low");
        return;
      }
      long l = this.bufferEndedTime - this.bufferStartedTime;
      boolean bool;
      StringBuilder localStringBuilder;
      if (l > 5000L)
      {
        bool = true;
        this.isBufferTimeTooLong = bool;
        localStringBuilder = new StringBuilder("MovieTrailer.AdaptiveSwitchLogic.bufferEnded took ").append(l);
        if (!this.isBufferTimeTooLong)
          break label109;
      }
      label109: for (String str = " too long"; ; str = "")
      {
        Logger.d("FlxMain", str);
        return;
        bool = false;
        break;
      }
    }

    private void bufferStarted(int paramInt)
    {
      this.bufferStartedTime = System.currentTimeMillis();
      int i = (int)(this.bufferStartedTime - this.bufferEndedTime);
      int j = paramInt - this.lastPlaybackPosition;
      this.lastPlaybackPosition = paramInt;
      float f = j / (1.0F + i);
      if ((f <= 1.0F) && (f >= 0.0F) && (j != 0));
      for (boolean bool = false; ; bool = true)
      {
        this.ignoreBufferEnd = bool;
        Logger.d("FlxMain", "MovieTrailer.AdaptiveSwitchLogic.bufferStarted " + this.bufferStartedTime + " " + this.lastPlaybackPosition + " " + j + " " + i + " " + f);
        return;
      }
    }

    private boolean shouldSwitch()
    {
      if (this.hasAdaptiveSwitched)
      {
        Logger.d("FlxMain", "MovieTrailer.AdaptiveSwitchLogic.shouldSwitch already switched or low");
        return false;
      }
      boolean bool1 = this.isBufferTimeTooLong;
      boolean bool2 = false;
      if (bool1)
      {
        bool2 = true;
        this.hasAdaptiveSwitched = bool2;
        Properties.instance().setUseLowBitrateForTrailer();
      }
      Logger.d("FlxMain", "MovieTrailer.AdaptiveSwitchLogic.shouldSwitch " + bool2);
      return bool2;
    }

    private void videoPrepared()
    {
      long l = System.currentTimeMillis();
      this.bufferEndedTime = l;
      this.bufferStartedTime = l;
    }

    private void videoUrlSet(String paramString)
    {
      Logger.d("FlxMain", "MovieTrailer.AdaptiveSwitchLogic.videoUrlSet " + paramString);
      this.hasAdaptiveSwitched = paramString.equals(this.urlLow);
    }
  }

  private final class BackgroundAdImageCallback
    implements Handler.Callback
  {
    private BackgroundAdImageCallback()
    {
    }

    public boolean handleMessage(Message paramMessage)
    {
      Logger.d("FlxMain", "MovieTrailer.BackgroundAdImageCallback");
      MovieTrailer.this.backgroundImage.setImageBitmap(MovieTrailer.this.backgroundAd.bitmap);
      AdManager.instance().trackEvent(MovieTrailer.this.backgroundAd, "Impression");
      return true;
    }
  }

  private class RemainingTimeCallback
    implements Handler.Callback
  {
    private RemainingTimeCallback()
    {
    }

    public boolean handleMessage(Message paramMessage)
    {
      if (!MovieTrailer.this.isFinishing())
        MovieTrailer.this.adLabel.setText(MovieTrailer.this.getRemainingTimeAdLabel());
      return true;
    }
  }

  private class SkipButtonCallback
    implements Handler.Callback
  {
    private SkipButtonCallback()
    {
    }

    public boolean handleMessage(Message paramMessage)
    {
      if (!MovieTrailer.this.isFinishing())
      {
        MovieTrailer.this.adSkipButton.setVisibility(0);
        MovieTrailer.this.adSkipButton.setOnClickListener(MovieTrailer.this);
        MovieTrailer.this.adSkipButton.startAnimation(AnimationUtils.loadAnimation(MovieTrailer.this, 2130968590));
      }
      return true;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.MovieTrailer
 * JD-Core Version:    0.6.2
 */