package net.flixster.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.lvi.LviAd;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviMovie.MovieViewItemHolder;
import net.flixster.android.lvi.LviUserProfile;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;
import net.flixster.android.model.User;

public class MoviesIWantToSeePage extends LviActivity
{
  private Boolean isConnected = null;
  private User mUser;
  private View.OnClickListener mUserReviewClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      LviMovie.MovieViewItemHolder localMovieViewItemHolder = (LviMovie.MovieViewItemHolder)paramAnonymousView.getTag();
      Logger.d("FlxMain", "MoviesIWantToSee.mUserReviewClickListener holder:" + localMovieViewItemHolder + " review:" + localMovieViewItemHolder.movieReview + " mUser:" + MoviesIWantToSeePage.this.mUser);
      if ((localMovieViewItemHolder != null) && (localMovieViewItemHolder.movieReview != null) && (MoviesIWantToSeePage.this.mUser != null))
      {
        Movie localMovie = localMovieViewItemHolder.movieReview.getMovie();
        Trackers.instance().track("/mymovies/wanttosee", "User Review - " + localMovie.getProperty("title"));
        int i = MoviesIWantToSeePage.this.mUser.wantToSeeReviews.indexOf(localMovieViewItemHolder.movieReview);
        if (i < 0)
          i = 0;
        Intent localIntent = new Intent("USER_REVIEW_PAGE", null, MoviesIWantToSeePage.this, UserReviewPage.class);
        localIntent.putExtra("REVIEW_INDEX", i);
        localIntent.putExtra("REVIEW_TYPE", 0);
        MoviesIWantToSeePage.this.startActivity(localIntent);
      }
    }
  };

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      this.throbberHandler.sendEmptyMessage(1);
      TimerTask local2 = new TimerTask()
      {
        // ERROR //
        public void run()
        {
          // Byte code:
          //   0: aload_0
          //   1: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   4: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   7: ifnull +51 -> 58
          //   10: aload_0
          //   11: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   14: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   17: getfield 33	net/flixster/android/model/User:wantToSeeReviews	Ljava/util/List;
          //   20: ifnull +38 -> 58
          //   23: aload_0
          //   24: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   27: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   30: getfield 33	net/flixster/android/model/User:wantToSeeReviews	Ljava/util/List;
          //   33: invokeinterface 39 1 0
          //   38: ifeq +30 -> 68
          //   41: aload_0
          //   42: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   45: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   48: getfield 43	net/flixster/android/model/User:wtsCount	I
          //   51: istore 12
          //   53: iload 12
          //   55: ifle +13 -> 68
          //   58: aload_0
          //   59: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   62: invokestatic 49	net/flixster/android/data/ProfileDao:fetchUser	()Lnet/flixster/android/model/User;
          //   65: invokestatic 53	net/flixster/android/MoviesIWantToSeePage:access$1	(Lnet/flixster/android/MoviesIWantToSeePage;Lnet/flixster/android/model/User;)V
          //   68: aload_0
          //   69: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   72: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   75: ifnull +89 -> 164
          //   78: aload_0
          //   79: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   82: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   85: getfield 57	net/flixster/android/model/User:id	Ljava/lang/String;
          //   88: ifnull +76 -> 164
          //   91: aload_0
          //   92: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   95: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   98: getfield 33	net/flixster/android/model/User:wantToSeeReviews	Ljava/util/List;
          //   101: ifnull +38 -> 139
          //   104: aload_0
          //   105: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   108: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   111: getfield 33	net/flixster/android/model/User:wantToSeeReviews	Ljava/util/List;
          //   114: invokeinterface 39 1 0
          //   119: ifeq +45 -> 164
          //   122: aload_0
          //   123: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   126: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   129: getfield 43	net/flixster/android/model/User:wtsCount	I
          //   132: istore 11
          //   134: iload 11
          //   136: ifle +28 -> 164
          //   139: aload_0
          //   140: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   143: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   146: aload_0
          //   147: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   150: invokestatic 27	net/flixster/android/MoviesIWantToSeePage:access$0	(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
          //   153: getfield 57	net/flixster/android/model/User:id	Ljava/lang/String;
          //   156: bipush 50
          //   158: invokestatic 61	net/flixster/android/data/ProfileDao:getWantToSeeReviews	(Ljava/lang/String;I)Ljava/util/List;
          //   161: putfield 33	net/flixster/android/model/User:wantToSeeReviews	Ljava/util/List;
          //   164: aload_0
          //   165: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   168: invokestatic 64	net/flixster/android/MoviesIWantToSeePage:access$2	(Lnet/flixster/android/MoviesIWantToSeePage;)V
          //   171: aload_0
          //   172: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   175: getfield 68	net/flixster/android/MoviesIWantToSeePage:mUpdateHandler	Landroid/os/Handler;
          //   178: iconst_0
          //   179: invokevirtual 74	android/os/Handler:sendEmptyMessage	(I)Z
          //   182: pop
          //   183: aload_0
          //   184: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   187: getfield 77	net/flixster/android/MoviesIWantToSeePage:throbberHandler	Landroid/os/Handler;
          //   190: iconst_0
          //   191: invokevirtual 74	android/os/Handler:sendEmptyMessage	(I)Z
          //   194: pop
          //   195: return
          //   196: astore 7
          //   198: ldc 79
          //   200: ldc 81
          //   202: aload 7
          //   204: invokestatic 87	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
          //   207: aload_0
          //   208: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   211: aconst_null
          //   212: invokestatic 53	net/flixster/android/MoviesIWantToSeePage:access$1	(Lnet/flixster/android/MoviesIWantToSeePage;Lnet/flixster/android/model/User;)V
          //   215: goto -147 -> 68
          //   218: astore_3
          //   219: ldc 79
          //   221: new 89	java/lang/StringBuilder
          //   224: dup
          //   225: ldc 91
          //   227: invokespecial 94	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
          //   230: aload_0
          //   231: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   234: getfield 97	net/flixster/android/MoviesIWantToSeePage:mRetryCount	I
          //   237: invokevirtual 101	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
          //   240: invokevirtual 105	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   243: invokestatic 109	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;)V
          //   246: aload_3
          //   247: invokevirtual 112	java/lang/Exception:printStackTrace	()V
          //   250: aload_0
          //   251: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   254: astore 4
          //   256: aload 4
          //   258: iconst_1
          //   259: aload 4
          //   261: getfield 97	net/flixster/android/MoviesIWantToSeePage:mRetryCount	I
          //   264: iadd
          //   265: putfield 97	net/flixster/android/MoviesIWantToSeePage:mRetryCount	I
          //   268: aload_0
          //   269: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   272: getfield 97	net/flixster/android/MoviesIWantToSeePage:mRetryCount	I
          //   275: iconst_3
          //   276: if_icmpge +55 -> 331
          //   279: aload_0
          //   280: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   283: ldc2_w 113
          //   286: invokestatic 118	net/flixster/android/MoviesIWantToSeePage:access$3	(Lnet/flixster/android/MoviesIWantToSeePage;J)V
          //   289: aload_0
          //   290: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   293: getfield 77	net/flixster/android/MoviesIWantToSeePage:throbberHandler	Landroid/os/Handler;
          //   296: iconst_0
          //   297: invokevirtual 74	android/os/Handler:sendEmptyMessage	(I)Z
          //   300: pop
          //   301: return
          //   302: astore 10
          //   304: ldc 79
          //   306: ldc 120
          //   308: aload 10
          //   310: invokestatic 87	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
          //   313: goto -149 -> 164
          //   316: astore_1
          //   317: aload_0
          //   318: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   321: getfield 77	net/flixster/android/MoviesIWantToSeePage:throbberHandler	Landroid/os/Handler;
          //   324: iconst_0
          //   325: invokevirtual 74	android/os/Handler:sendEmptyMessage	(I)Z
          //   328: pop
          //   329: aload_1
          //   330: athrow
          //   331: aload_0
          //   332: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   335: iconst_0
          //   336: putfield 97	net/flixster/android/MoviesIWantToSeePage:mRetryCount	I
          //   339: aload_0
          //   340: getfield 15	net/flixster/android/MoviesIWantToSeePage$2:this$0	Lnet/flixster/android/MoviesIWantToSeePage;
          //   343: getfield 123	net/flixster/android/MoviesIWantToSeePage:mShowDialogHandler	Landroid/os/Handler;
          //   346: iconst_2
          //   347: invokevirtual 74	android/os/Handler:sendEmptyMessage	(I)Z
          //   350: pop
          //   351: goto -62 -> 289
          //
          // Exception table:
          //   from	to	target	type
          //   58	68	196	net/flixster/android/data/DaoException
          //   0	53	218	java/lang/Exception
          //   58	68	218	java/lang/Exception
          //   68	134	218	java/lang/Exception
          //   139	164	218	java/lang/Exception
          //   164	183	218	java/lang/Exception
          //   198	215	218	java/lang/Exception
          //   304	313	218	java/lang/Exception
          //   139	164	302	net/flixster/android/data/DaoException
          //   0	53	316	finally
          //   58	68	316	finally
          //   68	134	316	finally
          //   139	164	316	finally
          //   164	183	316	finally
          //   198	215	316	finally
          //   219	289	316	finally
          //   304	313	316	finally
          //   331	351	316	finally
        }
      };
      Logger.d("FlxMain", "MoviesIWantToSeePage.ScheduleLoadItemsTask() loading item");
      if (this.mPageTimer != null)
        this.mPageTimer.schedule(local2, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void setWtsLviList()
  {
    Logger.d("FlxMain", "MoviesIWantToSeePage.setPopularLviList ");
    this.mDataHolder.clear();
    destroyExistingLviAd();
    this.lviAd = new LviAd();
    this.lviAd.mAdSlot = "MoviesIWantToSee";
    this.mDataHolder.add(this.lviAd);
    LviUserProfile localLviUserProfile = new LviUserProfile();
    localLviUserProfile.mUser = this.mUser;
    this.mDataHolder.add(localLviUserProfile);
    Iterator localIterator = this.mUser.wantToSeeReviews.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Review localReview = (Review)localIterator.next();
      LviMovie localLviMovie = new LviMovie();
      localLviMovie.mMovie = localReview.getMovie();
      localLviMovie.mReview = localReview;
      localLviMovie.mDetailsClick = this.mUserReviewClickListener;
      this.mDataHolder.add(localLviMovie);
    }
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt2 == -1) && (paramInt1 == 3))
    {
      this.isConnected = Boolean.valueOf(true);
      getIntent().putExtra("net.flixster.IsConnected", Boolean.TRUE);
      return;
    }
    setResult(0);
    finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    createActionBar();
    setActionBarTitle(2131492973);
    this.mStickyTopAd.setSlot("MoviesIWantToSeeStickyTop");
    this.mStickyBottomAd.setSlot("MoviesIWantToSeeStickyBottom");
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (this.isConnected == null))
    {
      Logger.d("FlxMain", "MyWantToSeePage.onCreate ");
      this.isConnected = Boolean.valueOf(localBundle.getBoolean("net.flixster.IsConnected"));
      if ((this.isConnected == null) || (!this.isConnected.booleanValue()))
      {
        Intent localIntent = new Intent(this, ConnectRatePage.class);
        localIntent.putExtra("net.flixster.RequestCode", 3);
        startActivityForResult(localIntent, 3);
      }
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  public void onResume()
  {
    super.onResume();
    Trackers.instance().track("/mymovies/wanttosee", "My Movies - Want To See");
    ScheduleLoadItemsTask(100L);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.MoviesIWantToSeePage
 * JD-Core Version:    0.6.2
 */