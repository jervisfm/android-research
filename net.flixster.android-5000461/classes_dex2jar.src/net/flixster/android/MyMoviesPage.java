package net.flixster.android;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.flixster.android.activity.TabbedActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.data.AccountManager;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackLogic;
import com.flixster.android.drm.PlaybackManager;
import com.flixster.android.model.Image;
import com.flixster.android.net.DownloadHelper;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder;
import com.flixster.android.view.FriendActivity;
import com.flixster.android.view.LoadMore;
import com.flixster.android.view.UvFooter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.data.ApiBuilder;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;
import net.flixster.android.model.Season;
import net.flixster.android.model.User;

public class MyMoviesPage extends TabbedActivity
{
  private static final int MAX_FRIENDS_DISPLAYED = 6;
  private static final int MAX_FRIENDS_REVIEWS = 3;
  private static final int MAX_MOVIES_PER_ROW_LANDSCAPE = 4;
  private static final int MAX_MOVIES_PER_ROW_PORTRAIT = 3;
  private static final int MAX_REVIEWS_DISPLAYED = 50;
  private static final int NAV_COLLECTED = 1;
  private static final int NAV_FRIENDS = 4;
  private static final int NAV_RATINGS = 3;
  private static final int NAV_WTS = 2;
  private static boolean isNoticeShown;
  private static volatile Thread sProgressMonitorThread;
  private TextView collectedCountView;
  private final Handler collectedMoviesSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      MyMoviesPage.this.collectionThrobber.setVisibility(8);
      User localUser = AccountManager.instance().getUser();
      if (localUser == null)
        return;
      localUser.collectionsCount = localUser.getLockerNonEpisodesCount();
      MyMoviesPage.this.collectedCountView.setText(String.valueOf(localUser.collectionsCount));
      if (MyMoviesPage.this.lastIsRewardsEligibleState)
        MyMoviesPage.this.updateRewardsLayout();
      MyMoviesPage.this.rentalRights.clear();
      MyMoviesPage.this.movieAndSeasonRights.clear();
      int i = 0;
      int j = 0;
      int k = 0;
      Iterator localIterator = localUser.getLockerRights().iterator();
      String str;
      if (!localIterator.hasNext())
      {
        MyMoviesPage.this.populateRentalRights();
        MyMoviesPage.this.populateMovieAndSeasonRights();
        if (k <= 0)
          break label448;
        TextView localTextView = MyMoviesPage.this.unfulfillablePanel;
        if (k != 1)
          break label413;
        str = MyMoviesPage.this.getString(2131492979);
        label164: localTextView.setText(str);
        MyMoviesPage.this.unfulfillablePanel.setOnClickListener(MyMoviesPage.this.unfulfillablePanelClickListener);
        MyMoviesPage.this.unfulfillablePanel.setVisibility(0);
        MyMoviesPage.this.unfulfillableDivider.setVisibility(0);
        label210: MyMoviesPage.this.initializeProgressMonitorThread();
        if (!MyMoviesPage.isNoticeShown)
        {
          MyMoviesPage.isNoticeShown = true;
          if (!Drm.manager().isRooted())
            break label475;
          DialogBuilder.showStreamUnsupportedOnRootedDevices(MyMoviesPage.this);
        }
      }
      while (true)
      {
        if ((!FlixsterApplication.isConnected()) && (!FlixsterApplication.hasMigratedToRights()))
          MyMoviesPage.this.showDialog(1000000207, null);
        MyMoviesPage.this.invalidActionBarItems();
        return;
        LockerRight localLockerRight = (LockerRight)localIterator.next();
        if (((localLockerRight.isMovie()) && ((localLockerRight.isStreamingSupported) || (localLockerRight.isDownloadSupported))) || (localLockerRight.isSeason()))
        {
          if (localLockerRight.isRental)
          {
            MyMoviesPage.this.rentalRights.add(localLockerRight);
            break;
          }
          if (DownloadHelper.isDownloaded(localLockerRight))
          {
            MyMoviesPage.this.movieAndSeasonRights.add(j, localLockerRight);
            j++;
            i++;
            break;
          }
          MyMoviesPage.this.movieAndSeasonRights.add(i, localLockerRight);
          i++;
          break;
        }
        if (!localLockerRight.isUnfulfillable())
          break;
        k++;
        break;
        label413: MyMoviesPage localMyMoviesPage = MyMoviesPage.this;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(k);
        str = localMyMoviesPage.getString(2131492980, arrayOfObject);
        break label164;
        label448: MyMoviesPage.this.unfulfillablePanel.setVisibility(8);
        MyMoviesPage.this.unfulfillableDivider.setVisibility(8);
        break label210;
        label475: if ((Drm.logic().isDeviceNonWifiStreamBlacklisted()) && (!FlixsterApplication.isWifi()))
          DialogBuilder.showNonWifiStreamUnsupported(MyMoviesPage.this);
        else if ((Drm.logic().isDeviceStreamBlacklisted()) || (!Drm.logic().isDeviceStreamingCompatible()))
          DialogBuilder.showStreamUnsupported(MyMoviesPage.this);
      }
    }
  };
  private TextView collectionHeader;
  private ProgressBar collectionThrobber;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
      case 0:
      case 1:
      case 2:
      }
      while (true)
      {
        if ((paramAnonymousMessage.obj instanceof DaoException))
          ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, MyMoviesPage.this, MyMoviesPage.this.dialogDecorator);
        return;
        MyMoviesPage.this.collectionThrobber.setVisibility(8);
        continue;
        MyMoviesPage.this.wtsHeader.setVisibility(8);
        MyMoviesPage.this.wtsThrobber.setVisibility(8);
        continue;
        MyMoviesPage.this.ratedHeader.setVisibility(8);
        MyMoviesPage.this.ratedThrobber.setVisibility(8);
      }
    }
  };
  private ImageView facebookLogin;
  private ImageView flixsterLogin;
  private TextView flixsterLoginDescription;
  private TextView flixsterLoginHeader;
  private TextView friendsActivityHeader;
  private LinearLayout friendsActivityLayout;
  private final Handler friendsActivitySuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      MyMoviesPage.this.friendsActivityHeader.setVisibility(0);
      MyMoviesPage.this.friendsActivityLayout.setVisibility(0);
      MyMoviesPage.this.friendsActivityLayout.removeAllViews();
      MyMoviesPage.this.friendsActivityLayout.setFocusable(true);
      List localList = AccountManager.instance().getUser().friendsActivity;
      int i = localList.size();
      int j = Math.min(i, 3);
      for (int k = 0; ; k++)
      {
        if (k >= j)
        {
          if (i > 3)
          {
            LoadMore localLoadMore = new LoadMore(MyMoviesPage.this);
            localLoadMore.setId(2131165404);
            MyMoviesPage.this.friendsActivityLayout.addView(localLoadMore);
            localLoadMore.load(MyMoviesPage.this.getResources().getString(2131492991), null);
            localLoadMore.setFocusable(true);
            localLoadMore.setOnClickListener(MyMoviesPage.this.myMoviesOnClickListener);
          }
          if (i == 0)
            ((TextView)MyMoviesPage.this.findViewById(2131165626)).setVisibility(0);
          return;
        }
        Review localReview = (Review)localList.get(k);
        FriendActivity localFriendActivity = new FriendActivity(MyMoviesPage.this);
        localFriendActivity.setFocusable(true);
        MyMoviesPage.this.friendsActivityLayout.addView(localFriendActivity);
        localFriendActivity.load(localReview);
      }
    }
  };
  private TextView friendsCountView;
  private TextView friendsHeader;
  private LinearLayout friendsLayout;
  private final Handler friendsSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      MyMoviesPage.this.friendsHeader.setVisibility(0);
      MyMoviesPage.this.friendsLayout.setVisibility(0);
      MyMoviesPage.this.friendsLayout.removeAllViews();
      MyMoviesPage.this.friendsLayout.setOnClickListener(MyMoviesPage.this.myMoviesOnClickListener);
      MyMoviesPage.this.friendsLayout.setFocusable(true);
      List localList = AccountManager.instance().getUser().friends;
      int i = localList.size();
      TextView localTextView = MyMoviesPage.this.friendsHeader;
      String str = MyMoviesPage.this.getResources().getString(2131493183);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(i);
      localTextView.setText(String.format(str, arrayOfObject));
      int j = 0;
      int k = MyMoviesPage.this.getResources().getDimensionPixelSize(2131361861);
      LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(k, k);
      localLayoutParams.rightMargin = MyMoviesPage.this.getResources().getDimensionPixelSize(2131361836);
      for (int m = 0; ; m++)
      {
        if (m >= i);
        User localUser;
        do
        {
          return;
          localUser = (User)localList.get(m);
        }
        while (j >= 6);
        if (!localUser.profileImage.contains("user.none"))
        {
          ImageView localImageView = new ImageView(MyMoviesPage.this);
          localImageView.setLayoutParams(localLayoutParams);
          localImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
          Bitmap localBitmap = localUser.getProfileBitmap(localImageView);
          if (localBitmap != null)
            localImageView.setImageBitmap(localBitmap);
          MyMoviesPage.this.friendsLayout.addView(localImageView);
          j++;
        }
      }
    }
  };
  private ScrollView friendsView;
  private final Handler getUserSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      User localUser = AccountFacade.getSocialUser();
      String str = AccountFacade.getSocialUserId();
      Logger.d("FlxMain", "MyMoviesPage.getUserSuccessHandler curr=" + str + " last=" + MyMoviesPage.this.lastSocialUserId);
      MyMoviesPage localMyMoviesPage;
      if ((str != null) && (!str.equals(MyMoviesPage.this.lastSocialUserId)))
      {
        MyMoviesPage.this.lastSocialUserId = str;
        MyMoviesPage.this.lastIsMskEligibleState = localUser.isMskEligible;
        localMyMoviesPage = MyMoviesPage.this;
        if ((!localUser.isRewardsEligible()) || (localUser.isMskEligible))
          break label119;
      }
      label119: for (boolean bool = true; ; bool = false)
      {
        localMyMoviesPage.lastIsRewardsEligibleState = bool;
        MyMoviesPage.this.initializeSocialViews();
        return;
      }
    }
  };
  private boolean lastIsConnectedState;
  private boolean lastIsMskEligibleState = true;
  private boolean lastIsMskEnabledState;
  private boolean lastIsRewardsEligibleState;
  private String lastSocialUserId;
  private final View.OnClickListener lockerRightClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      LockerRight localLockerRight = (LockerRight)paramAnonymousView.getTag();
      if (localLockerRight.isSeason())
        Starter.launchSeasonDetail(localLockerRight.assetId, localLockerRight.rightId, MyMoviesPage.this);
      while (!localLockerRight.isMovie())
        return;
      Starter.launchMovieDetail(localLockerRight.assetId, localLockerRight.rightId, MyMoviesPage.this);
    }
  };
  private View.OnClickListener mNavListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      User localUser = AccountFacade.getSocialUser();
      switch (paramAnonymousView.getId())
      {
      case 2131165666:
      case 2131165668:
      case 2131165670:
      default:
      case 2131165665:
      case 2131165667:
      case 2131165669:
      case 2131165671:
      }
      while (true)
      {
        MyMoviesPage.this.buttonShade();
        MyMoviesPage.this.trackPage();
        MyMoviesPage.this.initializeStaticViews();
        return;
        MyMoviesPage.this.mNavSelect = 1;
        MyMoviesPage.this.trackPromoImpressions();
        continue;
        if (localUser.wtsCount == 0)
        {
          Intent localIntent2 = new Intent(MyMoviesPage.this, QuickRatePage.class);
          localIntent2.putExtra("KEY_IS_WTS", true);
          MyMoviesPage.this.startActivity(localIntent2);
        }
        while (true)
        {
          MyMoviesPage.sProgressMonitorThread = null;
          break;
          MyMoviesPage.this.mNavSelect = 2;
        }
        if (localUser.ratingCount == 0)
        {
          Intent localIntent1 = new Intent(MyMoviesPage.this, QuickRatePage.class);
          localIntent1.putExtra("KEY_IS_WTS", false);
          MyMoviesPage.this.startActivity(localIntent1);
        }
        else
        {
          MyMoviesPage.this.mNavSelect = 3;
          continue;
          MyMoviesPage.this.mNavSelect = 4;
        }
      }
    }
  };
  private int mNavSelect = 1;
  private LinearLayout mNavbar;
  private LinearLayout mNavbarHolder;
  private List<LockerRight> movieAndSeasonRights;
  private final AdapterView.OnItemClickListener movieDetailClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      if (paramAnonymousAdapterView == MyMoviesPage.this.wtsGridView);
      for (Movie localMovie = (Movie)MyMoviesPage.this.wtsMovies.get(paramAnonymousInt); (localMovie instanceof Season); localMovie = (Movie)MyMoviesPage.this.ratedMovies.get(paramAnonymousInt))
      {
        Starter.launchSeasonDetail(localMovie.getId(), MyMoviesPage.this);
        return;
      }
      Starter.launchMovieDetail(localMovie.getId(), MyMoviesPage.this);
    }
  };
  private LinearLayout myMoviesCollectionLayout;
  private final View.OnClickListener myMoviesOnClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (FlixsterApplication.getPlatformUsername() != null);
      for (boolean bool = true; ; bool = false)
        switch (paramAnonymousView.getId())
        {
        default:
          return;
        case 2131165623:
        case 2131165404:
        case 2131165592:
        case 2131165597:
        case 2131165603:
        case 2131165604:
        case 2131165605:
        }
      Intent localIntent5 = new Intent(MyMoviesPage.this, FriendsPage.class);
      localIntent5.putExtra("net.flixster.IsConnected", bool);
      MyMoviesPage.this.startActivity(localIntent5);
      return;
      Intent localIntent4 = new Intent(MyMoviesPage.this, FriendsRatedPage.class);
      localIntent4.putExtra("net.flixster.IsConnected", bool);
      MyMoviesPage.this.startActivity(localIntent4);
      return;
      Trackers.instance().trackEvent("/msk/promo", "MSK Promo", "MskPromo", "Click");
      Starter.launchMsk(MyMoviesPage.this);
      return;
      Trackers.instance().trackEvent("/rewards", "Flixster Rewards", "FlixsterRewards", "RewardsPromo", "Click", 0);
      Intent localIntent3 = new Intent(MyMoviesPage.this, RewardsPage.class);
      MyMoviesPage.this.startActivity(localIntent3);
      return;
      Trackers.instance().track("/mymovies/facebook/login", "My Movies - Facebook Login");
      Intent localIntent2 = new Intent(MyMoviesPage.this, FacebookAuth.class);
      MyMoviesPage.this.startActivity(localIntent2);
      return;
      Trackers.instance().track("/mymovies/flixster/login", "My Movies - Flixster Login");
      Intent localIntent1 = new Intent(MyMoviesPage.this, FlixsterLoginPage.class);
      MyMoviesPage.this.startActivity(localIntent1);
      return;
      Trackers.instance().track("/mymovies/flixster/register", "My Movies - Flixster Register");
      Starter.launchFlxHtmlPage(ApiBuilder.registerAccount(), MyMoviesPage.this.getString(2131493306), MyMoviesPage.this);
    }
  };
  private LinearLayout myMoviesRatingsLayout;
  private LinearLayout myMoviesRentalsLayout;
  private ScrollView myMoviesView;
  private LinearLayout myMoviesWtsLayout;
  private TextView offlineAlert;
  private ImageView pickYourMovie;
  private GridView ratedGridView;
  private TextView ratedHeader;
  private List<Movie> ratedMovies;
  private final Handler ratedMoviesSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      MyMoviesPage.this.ratedThrobber.setVisibility(8);
      User localUser = AccountManager.instance().getUser();
      if (localUser == null)
        return;
      List localList = localUser.ratedReviews;
      if (localUser.ratingCount <= 50)
      {
        localUser.ratingCount = localList.size();
        MyMoviesPage.this.ratingsCountView.setText(String.valueOf(localUser.ratingCount));
      }
      MyMoviesPage.this.ratedMovies.clear();
      Iterator localIterator = localList.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          MyMoviesPage.this.ratedGridView.setAdapter(new MovieGridViewAdapter(MyMoviesPage.this, MyMoviesPage.this.ratedMovies, 2));
          MyMoviesPage.this.ratedGridView.setClickable(true);
          MyMoviesPage.this.ratedGridView.setOnItemClickListener(MyMoviesPage.this.movieDetailClickListener);
          MyMoviesPage.this.ratedGridView.setVisibility(0);
          return;
        }
        Movie localMovie = ((Review)localIterator.next()).getMovie();
        MyMoviesPage.this.ratedMovies.add(localMovie);
      }
    }
  };
  private ProgressBar ratedThrobber;
  private TextView ratingsCountView;
  private List<LockerRight> rentalRights;
  private TextView rentalsHeader;
  private TextView rewardsEarned;
  private RelativeLayout rewardsLayout;
  private TextView rewardsMore;
  private TextView startCollectionHeader;
  private View subnavFriends;
  private View subnavOwn;
  private View subnavRate;
  private View subnavWts;
  private ImageView unfulfillableDivider;
  private TextView unfulfillablePanel;
  private final View.OnClickListener unfulfillablePanelClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Intent localIntent = new Intent(MyMoviesPage.this, UnfulfillablePage.class);
      MyMoviesPage.this.startActivity(localIntent);
    }
  };
  private UvFooter uvFooter;
  private ImageView uvFooterDivider;
  private TextView wtsCountView;
  private GridView wtsGridView;
  private TextView wtsHeader;
  private List<Movie> wtsMovies;
  private final Handler wtsMoviesSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      MyMoviesPage.this.wtsThrobber.setVisibility(8);
      User localUser = AccountManager.instance().getUser();
      if (localUser == null)
        return;
      List localList = localUser.wantToSeeReviews;
      if (localUser.wtsCount <= 50)
      {
        localUser.wtsCount = localList.size();
        MyMoviesPage.this.wtsCountView.setText(String.valueOf(localUser.wtsCount));
      }
      MyMoviesPage.this.wtsMovies.clear();
      Iterator localIterator = localList.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          MyMoviesPage.this.wtsGridView.setAdapter(new MovieGridViewAdapter(MyMoviesPage.this, MyMoviesPage.this.wtsMovies, 1));
          MyMoviesPage.this.wtsGridView.setClickable(true);
          MyMoviesPage.this.wtsGridView.setOnItemClickListener(MyMoviesPage.this.movieDetailClickListener);
          MyMoviesPage.this.wtsGridView.setVisibility(0);
          return;
        }
        Movie localMovie = ((Review)localIterator.next()).getMovie();
        MyMoviesPage.this.wtsMovies.add(localMovie);
      }
    }
  };
  private ProgressBar wtsThrobber;

  private void buttonShade()
  {
    int i = 1;
    View localView1 = this.subnavOwn;
    label39: label63: View localView4;
    if (this.mNavSelect == i)
    {
      int k = i;
      localView1.setSelected(k);
      View localView2 = this.subnavWts;
      if (this.mNavSelect != 2)
        break label96;
      int n = i;
      localView2.setSelected(n);
      View localView3 = this.subnavRate;
      if (this.mNavSelect != 3)
        break label102;
      int i2 = i;
      localView3.setSelected(i2);
      localView4 = this.subnavFriends;
      if (this.mNavSelect != 4)
        break label108;
    }
    while (true)
    {
      localView4.setSelected(i);
      return;
      int m = 0;
      break;
      label96: int i1 = 0;
      break label39;
      label102: int i3 = 0;
      break label63;
      label108: int j = 0;
    }
  }

  private void initializeProgressMonitorThread()
  {
    if (Drm.logic().isDeviceDownloadCompatible())
    {
      sProgressMonitorThread = new Thread(new Runnable()
      {
        public void run()
        {
          Thread localThread = Thread.currentThread();
          if (MyMoviesPage.sProgressMonitorThread != localThread)
            return;
          while (true)
          {
            int m;
            try
            {
              Thread.sleep(2000L);
              int i = 0;
              LinearLayout localLinearLayout1;
              int k;
              if (i >= MyMoviesPage.this.myMoviesRentalsLayout.getChildCount())
              {
                int j = 0;
                if (j >= MyMoviesPage.this.myMoviesCollectionLayout.getChildCount())
                  break;
                localLinearLayout1 = (LinearLayout)MyMoviesPage.this.myMoviesCollectionLayout.getChildAt(j);
                k = 0;
                if (k < localLinearLayout1.getChildCount())
                  continue;
                j++;
                continue;
              }
              LinearLayout localLinearLayout2 = (LinearLayout)MyMoviesPage.this.myMoviesRentalsLayout.getChildAt(i);
              m = 0;
              if (m >= localLinearLayout2.getChildCount())
              {
                i++;
                continue;
              }
              View localView2 = localLinearLayout2.getChildAt(m);
              if (!(localView2 instanceof MovieCollectionItem))
                break label216;
              Handler localHandler2 = ((MovieCollectionItem)localView2).progressHandler;
              localHandler2.sendMessage(localHandler2.obtainMessage());
              break label216;
              View localView1 = localLinearLayout1.getChildAt(k);
              if ((localView1 instanceof MovieCollectionItem))
              {
                Handler localHandler1 = ((MovieCollectionItem)localView1).progressHandler;
                localHandler1.sendMessage(localHandler1.obtainMessage());
              }
              k++;
              continue;
            }
            catch (InterruptedException localInterruptedException)
            {
              localInterruptedException.printStackTrace();
            }
            break;
            label216: m++;
          }
        }
      });
      sProgressMonitorThread.start();
    }
  }

  private void initializeSocialViews()
  {
    this.rentalsHeader.setVisibility(8);
    this.collectionHeader.setVisibility(8);
    this.wtsGridView.setVisibility(8);
    this.ratedGridView.setVisibility(8);
    this.rewardsLayout.setVisibility(8);
    this.myMoviesRentalsLayout.removeAllViews();
    this.myMoviesCollectionLayout.removeAllViews();
    this.unfulfillableDivider.setVisibility(8);
    this.unfulfillablePanel.setVisibility(8);
    this.uvFooterDivider.setVisibility(8);
    this.uvFooter.setVisibility(8);
    View localView = findViewById(2131165605);
    localView.setOnClickListener(this.myMoviesOnClickListener);
    User localUser = AccountFacade.getSocialUser();
    if (localUser == null)
    {
      this.mNavbarHolder.setVisibility(8);
      this.mNavSelect = 1;
      initializeStaticViews();
      this.flixsterLoginHeader.setVisibility(0);
      this.flixsterLoginDescription.setVisibility(0);
      this.flixsterLogin.setVisibility(0);
      this.facebookLogin.setVisibility(0);
      localView.setVisibility(0);
      this.wtsHeader.setVisibility(8);
      this.ratedHeader.setVisibility(8);
      this.collectionThrobber.setVisibility(8);
      this.wtsThrobber.setVisibility(8);
      this.ratedThrobber.setVisibility(8);
      if ((this.lastIsMskEnabledState) && (!Properties.instance().isMskFinishedDeepLinkInitiated()))
        updateMskPromo();
      while (true)
      {
        invalidActionBarItems();
        return;
        this.startCollectionHeader.setVisibility(8);
        this.pickYourMovie.setVisibility(8);
      }
    }
    this.mNavbarHolder.setVisibility(0);
    this.flixsterLoginHeader.setVisibility(8);
    this.flixsterLoginDescription.setVisibility(8);
    this.flixsterLogin.setVisibility(8);
    this.facebookLogin.setVisibility(8);
    localView.setVisibility(8);
    this.collectedCountView.setText(String.valueOf(localUser.collectionsCount));
    this.wtsCountView.setText(String.valueOf(localUser.wtsCount));
    this.ratingsCountView.setText(String.valueOf(localUser.ratingCount));
    this.friendsCountView.setText(String.valueOf(localUser.friendCount));
    this.flixsterLoginHeader.setVisibility(8);
    this.flixsterLoginDescription.setVisibility(8);
    this.flixsterLogin.setVisibility(8);
    this.facebookLogin.setVisibility(8);
    this.collectionThrobber.setVisibility(0);
    ProfileDao.getUserLockerRights(this.collectedMoviesSuccessHandler, this.errorHandler);
    if ((FlixsterApplication.isConnected()) || (localUser.wantToSeeReviews != null))
    {
      this.wtsHeader.setVisibility(0);
      this.wtsThrobber.setVisibility(0);
      ProfileDao.getWantToSeeReviews(this.wtsMoviesSuccessHandler, this.errorHandler);
      if ((!FlixsterApplication.isConnected()) && (localUser.ratedReviews == null))
        break label590;
      this.ratedHeader.setVisibility(0);
      this.ratedThrobber.setVisibility(0);
      ProfileDao.getUserRatedReviews(this.ratedMoviesSuccessHandler, this.errorHandler);
      label506: if ((!FlixsterApplication.isConnected()) || (!this.lastIsMskEnabledState) || (!this.lastIsMskEligibleState) || (Properties.instance().isMskFinishedDeepLinkInitiated()))
        break label618;
      if (localUser.collectionsCount != 0)
        break label611;
      updateMskPromo();
    }
    while (true)
    {
      ProfileDao.getFriends(this.friendsSuccessHandler, this.errorHandler);
      ProfileDao.getFriendsActivity(this.friendsActivitySuccessHandler, this.errorHandler);
      return;
      this.wtsHeader.setVisibility(8);
      this.wtsThrobber.setVisibility(8);
      break;
      label590: this.ratedHeader.setVisibility(8);
      this.ratedThrobber.setVisibility(8);
      break label506;
      label611: updateRewardsLayout();
      continue;
      label618: this.startCollectionHeader.setVisibility(8);
      this.pickYourMovie.setVisibility(8);
    }
  }

  private void initializeStaticViews()
  {
    this.myMoviesView.setVisibility(8);
    this.myMoviesWtsLayout.setVisibility(8);
    this.myMoviesRatingsLayout.setVisibility(8);
    this.friendsView.setVisibility(8);
    if (this.mNavSelect == 1)
    {
      this.myMoviesView.setVisibility(0);
      return;
    }
    if (this.mNavSelect == 2)
    {
      this.myMoviesWtsLayout.setVisibility(0);
      return;
    }
    if (this.mNavSelect == 3)
    {
      this.myMoviesRatingsLayout.setVisibility(0);
      return;
    }
    this.friendsView.setVisibility(0);
  }

  private void populateMovieAndSeasonRights()
  {
    LinearLayout localLinearLayout;
    int i;
    int j;
    int k;
    if (!this.movieAndSeasonRights.isEmpty())
    {
      this.myMoviesCollectionLayout.removeAllViews();
      localLinearLayout = new LinearLayout(this);
      if (getResources().getConfiguration().orientation != 1)
        break label135;
      i = 3;
      j = 0;
      if (j < this.movieAndSeasonRights.size())
        break label140;
      k = localLinearLayout.getChildCount();
      if (k > 0)
        if (k >= -1 + i * 2);
    }
    for (int m = k; ; m += 2)
    {
      if (m >= -1 + i * 2)
      {
        this.myMoviesCollectionLayout.addView(localLinearLayout);
        this.collectionHeader.setVisibility(0);
        this.myMoviesCollectionLayout.setVisibility(0);
        this.uvFooterDivider.setVisibility(0);
        this.uvFooter.setVisibility(0);
        return;
        label135: i = 4;
        break;
        label140: LockerRight localLockerRight = (LockerRight)this.movieAndSeasonRights.get(j);
        MovieCollectionItem localMovieCollectionItem1 = new MovieCollectionItem(this);
        ((MovieCollectionItem)localMovieCollectionItem1).load(localLockerRight);
        localMovieCollectionItem1.setTag(localLockerRight);
        localMovieCollectionItem1.setOnClickListener(this.lockerRightClickListener);
        if (j % i != 0)
        {
          View localView1 = new View(this);
          localView1.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0F));
          localLinearLayout.addView(localView1);
        }
        while (true)
        {
          localLinearLayout.addView(localMovieCollectionItem1);
          j++;
          break;
          if (j > 0)
          {
            this.myMoviesCollectionLayout.addView(localLinearLayout);
            localLinearLayout = new LinearLayout(this);
          }
        }
      }
      View localView2 = new View(this);
      localView2.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0F));
      localLinearLayout.addView(localView2);
      MovieCollectionItem localMovieCollectionItem2 = new MovieCollectionItem(this);
      localMovieCollectionItem2.setVisibility(4);
      localLinearLayout.addView(localMovieCollectionItem2);
    }
  }

  private void populateRentalRights()
  {
    LinearLayout localLinearLayout;
    int i;
    int j;
    int k;
    if (!this.rentalRights.isEmpty())
    {
      this.myMoviesRentalsLayout.removeAllViews();
      localLinearLayout = new LinearLayout(this);
      if (getResources().getConfiguration().orientation != 1)
        break label129;
      i = 3;
      j = 0;
      if (j < this.rentalRights.size())
        break label134;
      k = localLinearLayout.getChildCount();
      if (k > 0)
        if (k >= -1 + i * 2);
    }
    for (int m = k; ; m += 2)
    {
      if (m >= -1 + i * 2)
      {
        this.myMoviesRentalsLayout.addView(localLinearLayout);
        if (this.myMoviesRentalsLayout.getChildCount() <= 0)
          break label396;
        this.rentalsHeader.setVisibility(0);
        this.myMoviesRentalsLayout.setVisibility(0);
        return;
        label129: i = 4;
        break;
        label134: LockerRight localLockerRight = (LockerRight)this.rentalRights.get(j);
        if ((localLockerRight.getViewingExpirationString() != null) || (localLockerRight.getRentalExpirationString(true) != null))
        {
          MovieCollectionItem localMovieCollectionItem1 = new MovieCollectionItem(this);
          ((MovieCollectionItem)localMovieCollectionItem1).load(localLockerRight);
          localMovieCollectionItem1.setTag(localLockerRight);
          localMovieCollectionItem1.setOnClickListener(this.lockerRightClickListener);
          if (j % i != 0)
          {
            View localView1 = new View(this);
            localView1.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0F));
            localLinearLayout.addView(localView1);
            label240: localLinearLayout.addView(localMovieCollectionItem1);
          }
        }
        while (true)
        {
          j++;
          break;
          if (j <= 0)
            break label240;
          this.myMoviesRentalsLayout.addView(localLinearLayout);
          localLinearLayout = new LinearLayout(this);
          break label240;
          Object[] arrayOfObject = new Object[1];
          arrayOfObject[0] = localLockerRight.getTitle();
          Toast.makeText(this, getString(2131493250, arrayOfObject), 1).show();
          AccountManager.instance().getUser().removeLockerRight(localLockerRight);
          this.rentalRights.remove(j);
          j--;
        }
      }
      View localView2 = new View(this);
      localView2.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0F));
      localLinearLayout.addView(localView2);
      MovieCollectionItem localMovieCollectionItem2 = new MovieCollectionItem(this);
      localMovieCollectionItem2.setVisibility(4);
      localLinearLayout.addView(localMovieCollectionItem2);
    }
    label396: this.rentalsHeader.setVisibility(8);
    this.myMoviesRentalsLayout.setVisibility(8);
  }

  private void trackPromoImpressions()
  {
    if (this.pickYourMovie.getVisibility() == 0)
      Trackers.instance().trackEvent("/msk/promo", "MSK Promo", "MskPromo", "Impression");
    do
    {
      do
        return;
      while (this.rewardsLayout.getVisibility() != 0);
      if (this.lastIsMskEligibleState)
      {
        Trackers.instance().trackEvent("/msk/promo", "MSK Promo", "MskPromo", "Impression");
        return;
      }
    }
    while (!this.lastIsRewardsEligibleState);
    Trackers.instance().trackEvent("/rewards", "Flixster Rewards", "FlixsterRewards", "RewardsPromo", "Impression", 0);
  }

  private void updateMskPromo()
  {
    this.startCollectionHeader.setVisibility(0);
    this.pickYourMovie.setVisibility(0);
    this.pickYourMovie.setOnClickListener(this.myMoviesOnClickListener);
    User localUser = AccountManager.instance().getUser();
    if ((AccountManager.instance().hasUserSession()) && (localUser.collectionsCount != 0))
      this.pickYourMovie.setImageResource(2130837831);
    for (String str = FlixsterApplication.getMskUserPromoUrl(); ; str = FlixsterApplication.getMskAnonPromoUrl())
    {
      if ((str != null) && (str.length() > 0))
        new Image(str).getBitmap(this.pickYourMovie);
      return;
      this.pickYourMovie.setImageResource(2130837830);
    }
  }

  private void updateRewardsLayout()
  {
    this.startCollectionHeader.setVisibility(8);
    this.pickYourMovie.setVisibility(8);
    User localUser = AccountFacade.getSocialUser();
    if (FlixsterApplication.isConnected())
    {
      if (this.lastIsMskEligibleState)
      {
        this.rewardsEarned.setText(2131493188);
        this.rewardsMore.setText(2131493189);
        this.rewardsLayout.setId(2131165592);
        this.rewardsLayout.setOnClickListener(this.myMoviesOnClickListener);
        this.rewardsLayout.setVisibility(0);
        return;
      }
      if (this.lastIsRewardsEligibleState)
      {
        trackPromoImpressions();
        TextView localTextView = this.rewardsEarned;
        Resources localResources = getResources();
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(localUser.getGiftMoviesEarnedCount());
        arrayOfObject[1] = Integer.valueOf(localUser.getGiftMoviesEarnedCount() + localUser.getRewardsEligibleCount());
        localTextView.setText(localResources.getString(2131493186, arrayOfObject));
        this.rewardsMore.setText(2131493187);
        this.rewardsLayout.setId(2131165597);
        this.rewardsLayout.setOnClickListener(this.myMoviesOnClickListener);
        this.rewardsLayout.setVisibility(0);
        return;
      }
      this.rewardsLayout.setVisibility(8);
      return;
    }
    this.rewardsLayout.setVisibility(8);
  }

  protected String getAnalyticsTag()
  {
    if (AccountFacade.getSocialUser() == null)
      return "/mymovies/anon";
    if (this.mNavSelect == 1)
      return "/mymovies/collected";
    if (this.mNavSelect == 2)
      return "/mymovies/wts";
    if (this.mNavSelect == 3)
      return "/mymovies/ratings";
    return "/mymovies/friends";
  }

  protected String getAnalyticsTitle()
  {
    if (AccountFacade.getSocialUser() == null)
      return "My Movies - Anon";
    if (this.mNavSelect == 1)
      return "My Movies - Collected";
    if (this.mNavSelect == 2)
      return "My Movies - Want To See";
    if (this.mNavSelect == 3)
      return "My Movies - Ratings";
    return "My Movies - Friends";
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903134);
    this.mNavbar = ((LinearLayout)LayoutInflater.from(this).inflate(2130903139, null));
    this.mNavbarHolder = ((LinearLayout)findViewById(2131165594));
    this.mNavbarHolder.addView(this.mNavbar, 0);
    this.subnavOwn = this.mNavbar.findViewById(2131165665);
    this.subnavOwn.setOnClickListener(this.mNavListener);
    this.subnavWts = this.mNavbar.findViewById(2131165667);
    this.subnavWts.setOnClickListener(this.mNavListener);
    this.subnavRate = this.mNavbar.findViewById(2131165669);
    this.subnavRate.setOnClickListener(this.mNavListener);
    this.subnavFriends = this.mNavbar.findViewById(2131165671);
    this.subnavFriends.setOnClickListener(this.mNavListener);
    this.myMoviesView = ((ScrollView)findViewById(2131165367));
    this.friendsView = ((ScrollView)findViewById(2131165621));
    this.myMoviesRentalsLayout = ((LinearLayout)findViewById(2131165608));
    this.myMoviesCollectionLayout = ((LinearLayout)findViewById(2131165610));
    this.myMoviesWtsLayout = ((LinearLayout)findViewById(2131165613));
    this.myMoviesRatingsLayout = ((LinearLayout)findViewById(2131165617));
    this.friendsLayout = ((LinearLayout)findViewById(2131165623));
    this.friendsActivityLayout = ((LinearLayout)findViewById(2131165625));
    this.collectedCountView = ((TextView)this.mNavbar.findViewById(2131165666));
    this.wtsCountView = ((TextView)this.mNavbar.findViewById(2131165668));
    this.ratingsCountView = ((TextView)this.mNavbar.findViewById(2131165670));
    this.friendsCountView = ((TextView)this.mNavbar.findViewById(2131165672));
    this.flixsterLoginHeader = ((TextView)findViewById(2131165601));
    this.flixsterLoginDescription = ((TextView)findViewById(2131165602));
    this.flixsterLogin = ((ImageView)findViewById(2131165604));
    this.flixsterLogin.setOnClickListener(this.myMoviesOnClickListener);
    this.facebookLogin = ((ImageView)findViewById(2131165603));
    this.facebookLogin.setOnClickListener(this.myMoviesOnClickListener);
    this.offlineAlert = ((TextView)findViewById(2131165595));
    this.startCollectionHeader = ((TextView)findViewById(2131165596));
    this.collectionHeader = ((TextView)findViewById(2131165609));
    this.rentalsHeader = ((TextView)findViewById(2131165607));
    this.wtsHeader = ((TextView)findViewById(2131165614));
    this.ratedHeader = ((TextView)findViewById(2131165618));
    this.friendsHeader = ((TextView)findViewById(2131165622));
    this.friendsActivityHeader = ((TextView)findViewById(2131165624));
    this.pickYourMovie = ((ImageView)findViewById(2131165592));
    this.collectionThrobber = ((ProgressBar)findViewById(2131165606));
    this.wtsThrobber = ((ProgressBar)findViewById(2131165615));
    this.ratedThrobber = ((ProgressBar)findViewById(2131165619));
    this.wtsGridView = ((GridView)findViewById(2131165616));
    this.ratedGridView = ((GridView)findViewById(2131165620));
    this.rewardsLayout = ((RelativeLayout)findViewById(2131165597));
    this.rewardsEarned = ((TextView)findViewById(2131165599));
    this.rewardsMore = ((TextView)findViewById(2131165600));
    this.unfulfillableDivider = ((ImageView)findViewById(2131165457));
    this.unfulfillablePanel = ((TextView)findViewById(2131165456));
    this.uvFooterDivider = ((ImageView)findViewById(2131165611));
    this.uvFooter = ((UvFooter)findViewById(2131165612));
    this.rentalRights = new ArrayList();
    this.movieAndSeasonRights = new ArrayList();
    this.wtsMovies = new ArrayList();
    this.ratedMovies = new ArrayList();
    initializeStaticViews();
    boolean bool1 = isNoticeShown;
    boolean bool2;
    boolean bool4;
    if (FlixsterApplication.isConnected())
    {
      bool2 = false;
      isNoticeShown = bool2 | bool1;
      boolean bool3 = FlixsterApplication.isConnected();
      bool4 = false;
      if (!bool3)
        break label821;
    }
    while (true)
    {
      this.lastIsConnectedState = bool4;
      if ((Properties.instance().isMskEnabled()) && (!FlixsterApplication.hasMskPromptShown()))
        FlixsterApplication.setMskPromptShown();
      checkAndShowLaunchAd();
      return;
      bool2 = true;
      break;
      label821: bool4 = true;
    }
  }

  public void onPause()
  {
    super.onPause();
    sProgressMonitorThread = null;
  }

  protected void onResume()
  {
    boolean bool1 = true;
    super.onResume();
    buttonShade();
    boolean bool2 = FlixsterApplication.isConnected();
    String str = AccountFacade.fetchSocialUserId(this.getUserSuccessHandler, this.errorHandler);
    User localUser = AccountFacade.getSocialUser();
    boolean bool4;
    if (((str != null) || (this.lastSocialUserId == null)) && ((str == null) || (str.equals(this.lastSocialUserId))) && (this.lastIsMskEnabledState == Properties.instance().isMskEnabled()) && ((str == null) || (this.lastIsMskEligibleState == localUser.isMskEligible)))
      if (str != null)
      {
        boolean bool3 = this.lastIsRewardsEligibleState;
        if ((!localUser.isRewardsEligible()) || (localUser.isMskEligible))
          break label265;
        bool4 = bool1;
        if (bool3 != bool4);
      }
      else
      {
        if (((str == null) || ((!localUser.refreshRequired) && (localUser.ratedReviews != null) && (localUser.wantToSeeReviews != null))) && (this.lastIsConnectedState == bool2))
          break label276;
      }
    this.lastSocialUserId = str;
    this.lastIsMskEnabledState = Properties.instance().isMskEnabled();
    if (str != null)
    {
      this.lastIsMskEligibleState = localUser.isMskEligible;
      if ((localUser.isRewardsEligible()) && (!localUser.isMskEligible))
      {
        label202: this.lastIsRewardsEligibleState = bool1;
        localUser.refreshRequired = false;
      }
    }
    else
    {
      this.lastIsConnectedState = bool2;
      initializeSocialViews();
    }
    while (true)
    {
      TextView localTextView = this.offlineAlert;
      int i;
      if (!FlixsterApplication.isConnected())
      {
        i = 0;
        if (str != null);
      }
      else
      {
        i = 8;
      }
      localTextView.setVisibility(i);
      initializeProgressMonitorThread();
      trackPromoImpressions();
      trackPage();
      return;
      label265: bool4 = false;
      break;
      bool1 = false;
      break label202;
      label276: if ((str != null) && (!this.rentalRights.isEmpty()))
        populateRentalRights();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.MyMoviesPage
 * JD-Core Version:    0.6.2
 */