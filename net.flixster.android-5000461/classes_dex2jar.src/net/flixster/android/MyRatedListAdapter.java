package net.flixster.android;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.List;
import net.flixster.android.ads.AdManager;
import net.flixster.android.ads.AdView;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;
import net.flixster.android.model.User;

public class MyRatedListAdapter extends FlixsterListAdapter
{
  private boolean mRefreshAd = true;
  private View.OnClickListener reviewClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Review localReview = (Review)paramAnonymousView.getTag();
      if ((localReview != null) && (MyRatedListAdapter.this.user != null))
      {
        Movie localMovie = localReview.getMovie();
        Trackers.instance().track("/mymovies/rated", "User Review - " + localMovie.getProperty("title"));
        int i = MyRatedListAdapter.this.user.ratedReviews.indexOf(localReview);
        if (i < 0)
          i = 0;
        Intent localIntent = new Intent("USER_REVIEW_PAGE", null, MyRatedListAdapter.this.context, UserReviewPage.class);
        localIntent.putExtra("REVIEW_INDEX", i);
        localIntent.putExtra("REVIEW_TYPE", 1);
        MyRatedListAdapter.this.context.startActivity(localIntent);
      }
    }
  };
  private AdapterView.OnItemClickListener reviewItemClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      if (5 == MyRatedListAdapter.this.getItemViewType(paramAnonymousInt))
        ((FlixsterListAdapter.ReviewViewHolder)paramAnonymousView.getTag()).reviewLayout.performClick();
    }
  };
  private User user;

  public MyRatedListAdapter(FlixsterListActivity paramFlixsterListActivity, User paramUser, ArrayList<Object> paramArrayList)
  {
    super(paramFlixsterListActivity, paramArrayList);
    this.user = paramUser;
    paramFlixsterListActivity.getListView().setOnItemClickListener(this.reviewItemClickListener);
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    int i = getItemViewType(paramInt);
    Object localObject1 = data.get(paramInt);
    switch (i)
    {
    case 4:
    case 6:
    default:
    case 7:
    case 1:
    case 3:
    case 5:
    case 2:
    }
    while (true)
    {
      Object localObject2 = paramView;
      do
      {
        return localObject2;
        localObject2 = (AdView)paramView;
        if (localObject2 == null)
        {
          localObject2 = new AdView(this.context, "MoviesIveRated");
          ((AdView)localObject2).setGravity(17);
          ((AdView)localObject2).setBackgroundColor(-4473925);
        }
        Logger.i(AdManager.class.getName(), "getView: " + localObject2);
      }
      while (!this.mRefreshAd);
      ((AdView)localObject2).refreshAds();
      this.mRefreshAd = false;
      Logger.i(AdManager.class.getName(), "refresh: " + localObject2);
      return localObject2;
      paramView = getTitleView((String)localObject1, paramView);
      continue;
      paramView = getProfileView((User)localObject1, paramView, paramViewGroup);
      continue;
      Review localReview = (Review)localObject1;
      View.OnClickListener localOnClickListener = this.reviewClickListener;
      paramView = getReviewView(localReview, false, paramView, paramViewGroup, localOnClickListener);
      continue;
      paramView = getTextView((String)localObject1, paramView);
    }
  }

  public void setAdRefresh(boolean paramBoolean)
  {
    this.mRefreshAd = paramBoolean;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.MyRatedListAdapter
 * JD-Core Version:    0.6.2
 */