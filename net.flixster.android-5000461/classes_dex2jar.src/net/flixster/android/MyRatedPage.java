package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.User;

public class MyRatedPage extends FlixsterListActivity
{
  private static final int DIALOG_MOVIES = 2;
  private static final int DIALOG_NETWORK_FAIL = 1;
  private Boolean isConnected;
  private Timer timer;
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "MyRatedPage.updateHandler");
      if ((MyRatedPage.this.user != null) && (MyRatedPage.this.user.ratedReviews != null) && ((!MyRatedPage.this.user.ratedReviews.isEmpty()) || (MyRatedPage.this.user.ratingCount <= 0)))
        MyRatedPage.this.updatePage();
      while (MyRatedPage.this.isFinishing())
        return;
      MyRatedPage.this.showDialog(1);
    }
  };
  private User user;

  private void scheduleUpdatePageTask()
  {
    TimerTask local3 = new TimerTask()
    {
      public void run()
      {
        if ((MyRatedPage.this.user == null) || (MyRatedPage.this.user.ratedReviews == null) || ((MyRatedPage.this.user.ratedReviews.isEmpty()) && (MyRatedPage.this.user.ratingCount > 0)));
        try
        {
          MyRatedPage.this.user = ProfileDao.fetchUser();
          if ((MyRatedPage.this.user != null) && (MyRatedPage.this.user.id != null))
            if (MyRatedPage.this.user.ratedReviews != null)
              if ((!MyRatedPage.this.user.ratedReviews.isEmpty()) || (MyRatedPage.this.user.ratingCount <= 0))
                break label156;
        }
        catch (DaoException localDaoException1)
        {
          try
          {
            MyRatedPage.this.user.ratedReviews = ProfileDao.getUserRatedReviews(MyRatedPage.this.user.id, 50);
            label156: MyRatedPage.this.updateHandler.sendEmptyMessage(0);
            return;
            localDaoException1 = localDaoException1;
            Logger.e("FlxMain", "MyRatedPage.scheduleUpdatePageTask (failed to get user data)", localDaoException1);
            MyRatedPage.this.user = null;
          }
          catch (DaoException localDaoException2)
          {
            while (true)
              Logger.e("FlxMain", "MyRatedPage.scheduleUpdatePageTask (failed to get review data)", localDaoException2);
          }
        }
      }
    };
    if (this.timer != null)
      this.timer.schedule(local3, 100L);
  }

  private void updatePage()
  {
    ArrayList localArrayList;
    if (this.user != null)
    {
      localArrayList = new ArrayList();
      localArrayList.add(new AdView(this, "MoviesIveRated"));
      localArrayList.add(this.user);
      if ((this.user.ratedReviews == null) || (this.user.ratedReviews.isEmpty()))
        break label148;
      localArrayList.addAll(this.user.ratedReviews);
    }
    while (true)
    {
      if (getListAdapter() == null)
      {
        findViewById(2131165627).setVisibility(4);
        setListAdapter(new MyRatedListAdapter(this, this.user, localArrayList));
      }
      removeDialog(2);
      Trackers.instance().track("/mymovies/rated", "My Rated Page for user:" + this.user.id);
      return;
      label148: localArrayList.add(getResources().getString(2131493002));
    }
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt2 == -1) && (paramInt1 == 1))
    {
      this.isConnected = Boolean.valueOf(true);
      getIntent().putExtra("net.flixster.IsConnected", Boolean.TRUE);
      return;
    }
    setResult(0);
    finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    Logger.d("FlxMain", "MyRatedPage.onCreate");
    super.onCreate(paramBundle);
    setContentView(2130903135);
    createActionBar();
    setActionBarTitle(2131492974);
    ListView localListView = getListView();
    int i = getResources().getColor(2131296267);
    localListView.setCacheColorHint(i);
    localListView.setBackgroundColor(i);
    localListView.setDivider(getResources().getDrawable(2130837698));
    localListView.setDividerHeight(getResources().getDimensionPixelOffset(2131361829));
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (this.isConnected == null))
    {
      Logger.d("FlxMain", "MyRatedPage.onCreate ");
      this.isConnected = Boolean.valueOf(localBundle.getBoolean("net.flixster.IsConnected"));
      if ((this.isConnected == null) || (!this.isConnected.booleanValue()))
      {
        Intent localIntent = new Intent(this, ConnectRatePage.class);
        localIntent.putExtra("net.flixster.RequestCode", 1);
        startActivityForResult(localIntent, 1);
      }
      return;
    }
    updatePage();
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      ProgressDialog localProgressDialog2 = new ProgressDialog(this);
      localProgressDialog2.setMessage(getResources().getString(2131493173));
      localProgressDialog2.setIndeterminate(true);
      localProgressDialog2.setCancelable(true);
      localProgressDialog2.setCanceledOnTouchOutside(true);
      return localProgressDialog2;
    case 2:
      ProgressDialog localProgressDialog1 = new ProgressDialog(this);
      localProgressDialog1.setMessage(getResources().getString(2131493173));
      localProgressDialog1.setIndeterminate(true);
      localProgressDialog1.setCancelable(true);
      localProgressDialog1.setCanceledOnTouchOutside(true);
      return localProgressDialog1;
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        MyRatedPage.this.showDialog(2);
        MyRatedPage.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder.create();
  }

  public void onDestroy()
  {
    Logger.d("FlxMain", "MyRatedPage.onDestroy");
    super.onDestroy();
    if (this.timer != null)
    {
      this.timer.cancel();
      this.timer.purge();
    }
    this.timer = null;
  }

  public void onResume()
  {
    Logger.d("FlxMain", "MyRatedPage.onResume");
    super.onResume();
    if (!this.isConnected.booleanValue())
      return;
    if (this.timer == null)
      this.timer = new Timer();
    showDialog(2);
    scheduleUpdatePageTask();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.MyRatedPage
 * JD-Core Version:    0.6.2
 */