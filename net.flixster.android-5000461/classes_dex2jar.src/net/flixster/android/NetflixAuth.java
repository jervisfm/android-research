package net.flixster.android;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.F;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import java.util.Iterator;
import java.util.Set;
import net.flixster.android.data.NetflixDao;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import oauth.signpost.http.HttpParameters;

public class NetflixAuth extends DecoratedSherlockActivity
  implements View.OnClickListener
{
  public static final int LAYOUT_SPLASH = 0;
  public static final String LAYOUT_STATE = "LayoutState";
  public static final int LAYOUT_WEB = 1;
  private final Handler authUrlHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      StringBuilder localStringBuilder = new StringBuilder((String)paramAnonymousMessage.obj);
      localStringBuilder.append("&application_name=").append("Movies");
      localStringBuilder.append("&oauth_consumer_key=").append("rtuhbkms7jmpsn3sft3va7y6");
      Logger.d("FlxMain", "NetflixAuth.onCreate(.) authUrl:" + localStringBuilder.toString());
      NetflixAuth.this.mWebView.loadUrl(localStringBuilder.toString());
    }
  };
  private volatile OAuthConsumer mConsumer;
  private int mLayoutState = 0;
  private Button mLoginButton;
  private NetflixAuth mNetflixAuth;
  private Button mNewAccountButton;
  private volatile OAuthProvider mProvider;
  private RelativeLayout mSelectLayout;
  private WebView mWebView;
  private final Handler netflixPageFinishedHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      HttpParameters localHttpParameters = NetflixAuth.this.mProvider.getResponseParameters();
      Logger.d("FlxMain", "NetflixAuth.FlixsterWebViewClient mConsumer.getTokenSecret():" + NetflixAuth.this.mConsumer.getTokenSecret() + " Map:" + localHttpParameters);
      FlixsterApplication.setNetflixOAuthToken(NetflixAuth.this.mConsumer.getToken());
      FlixsterApplication.setNetflixOAuthTokenSecret(NetflixAuth.this.mConsumer.getTokenSecret());
      FlixsterApplication.setNetflixUserId(localHttpParameters.getFirst("user_id"));
      Trackers.instance().track("/netflix/login/success", "Netflix Login Success");
      NetflixAuth.this.mNetflixAuth.finish();
    }
  };
  private final Runnable retrieveTokenRunnable = new Runnable()
  {
    public void run()
    {
      try
      {
        NetflixAuth.this.mProvider.retrieveAccessToken(NetflixAuth.this.mConsumer, null);
        NetflixAuth.this.netflixPageFinishedHandler.sendEmptyMessage(0);
        return;
      }
      catch (OAuthMessageSignerException localOAuthMessageSignerException)
      {
        Logger.e("FlxMain", "NetflixAuth.onPageFinished OAuthMessageSignerException", localOAuthMessageSignerException);
        return;
      }
      catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
      {
        Logger.e("FlxMain", "NetflixAuth.onPageFinished OAuthNotAuthorizedException responseBody:" + localOAuthNotAuthorizedException.getResponseBody(), localOAuthNotAuthorizedException);
        return;
      }
      catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
      {
        Logger.e("FlxMain", "NetflixAuth.onPageFinished OAuthExpectationFailedException", localOAuthExpectationFailedException);
        return;
      }
      catch (OAuthCommunicationException localOAuthCommunicationException)
      {
        Logger.e("FlxMain", "NetflixAuth.onPageFinished OAuthCommunicationException", localOAuthCommunicationException);
      }
    }
  };
  private final Runnable tokenRequestRunnable = new Runnable()
  {
    public void run()
    {
      try
      {
        String str = NetflixAuth.this.mProvider.retrieveRequestToken(NetflixAuth.this.mConsumer, "flixster://netflix/queue");
        Message localMessage = Message.obtain();
        localMessage.obj = str;
        NetflixAuth.this.authUrlHandler.sendMessage(localMessage);
        return;
      }
      catch (OAuthMessageSignerException localOAuthMessageSignerException)
      {
        Logger.e("FlxMain", "NetflixAuth.onCreate() OAuthMessageSignerException", localOAuthMessageSignerException);
        return;
      }
      catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
      {
        Logger.e("FlxMain", "NetflixAuth.onCreate() OAuthNotAuthorizedException", localOAuthNotAuthorizedException);
        return;
      }
      catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
      {
        Logger.e("FlxMain", "NetflixAuth.onCreate() OAuthExpectationFailedException", localOAuthExpectationFailedException);
        return;
      }
      catch (OAuthCommunicationException localOAuthCommunicationException)
      {
        Logger.e("FlxMain", "NetflixAuth.onCreate() OAuthCommunicationException", localOAuthCommunicationException);
      }
    }
  };

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
      return;
    case 2131165684:
      this.mLayoutState = 1;
      this.mSelectLayout.setVisibility(8);
      Trackers.instance().track("/netflix/login/dialog", "Netflix Login");
      return;
    case 2131165685:
    }
    Trackers.instance().track("/netflix/newaccnt", "Start New Account");
    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817")));
  }

  @SuppressLint({"SetJavaScriptEnabled"})
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mNetflixAuth = this;
    setContentView(2130903142);
    createActionBar();
    setActionBarTitle(2131493069);
    this.mWebView = ((WebView)findViewById(2131165681));
    WebSettings localWebSettings = this.mWebView.getSettings();
    localWebSettings.setSupportZoom(false);
    localWebSettings.setJavaScriptEnabled(true);
    NetflixWebViewClient localNetflixWebViewClient = new NetflixWebViewClient(null);
    this.mWebView.setWebViewClient(localNetflixWebViewClient);
    this.mSelectLayout = ((RelativeLayout)findViewById(2131165682));
    this.mLoginButton = ((Button)findViewById(2131165684));
    this.mLoginButton.setOnClickListener(this);
    this.mNewAccountButton = ((Button)findViewById(2131165685));
    this.mNewAccountButton.setOnClickListener(this);
    NetflixDao.trackNewAccountImpression();
    if (F.API_LEVEL >= 7)
      this.mConsumer = NetflixDao.getNewConsumer();
    for (this.mProvider = NetflixDao.getNewProvider(); ; this.mProvider = NetflixDao.getCommonsHttpProvider())
    {
      WorkerThreads.instance().invokeLater(this.tokenRequestRunnable);
      return;
      this.mConsumer = NetflixDao.getCommonsHttpConsumer();
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    Logger.d("FlxMain", "NetflixAuth.onRestoreInstanceState()");
    this.mLayoutState = paramBundle.getInt("LayoutState");
  }

  protected void onResume()
  {
    super.onResume();
    if (this.mLayoutState == 1)
    {
      this.mSelectLayout.setVisibility(8);
      return;
    }
    Logger.d("FlxMain", "NetflixAuth onResume() NetflixLinkSynergy()");
    NetflixDao.trackNewAccountReferral();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Logger.d("FlxMain", "NetflixAuth.onSaveInstanceState()");
    paramBundle.putInt("LayoutState", this.mLayoutState);
  }

  private class NetflixWebViewClient extends WebViewClient
  {
    private NetflixWebViewClient()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      super.onPageFinished(paramWebView, paramString);
      Logger.d("FlxMain", "NetflixAuth.FlixsterWebViewClient.onPageFinished(..) url:" + paramString);
      NetflixAuth.this.mWebView.loadUrl("javascript:window.document.getElementsByTagName('html')[0].style.backgroundColor='#9A0904';");
      int i = FlixsterApplication.getAndroidBuildInt();
      if ((i >= 5) && (i < 8))
        NetflixAuth.this.mWebView.loadUrl("javascript:window.document.getElementsByTagName('input')[6].style.paddingTop='12px';");
      Iterator localIterator1;
      Iterator localIterator2;
      if (paramString.startsWith("flixster:"))
      {
        NetflixAuth.this.mWebView.loadUrl("javascript:window.document.body.innerHTML='<br><br><br><center><h2>Loading...</h2></center>';");
        NetflixAuth.this.mWebView.loadUrl("javascript:window.document.getElementsByTagName('body')[0].style.margin='-10px';");
        Logger.d("FlxMain", "NetflixAuth.FlixsterWebViewClient pre access token mConsumer.getTokenSecret():" + NetflixAuth.this.mConsumer.getTokenSecret());
        Logger.d("FlxMain", "NetflixAuth.onPageFinished() getAccessTokenEndpointUrl:" + NetflixAuth.this.mProvider.getAccessTokenEndpointUrl());
        Logger.d("FlxMain", "NetflixAuth.onPageFinished() isOAuth10a:" + NetflixAuth.this.mProvider.isOAuth10a());
        localIterator1 = NetflixAuth.this.mProvider.getResponseParameters().keySet().iterator();
        if (localIterator1.hasNext())
          break label273;
        localIterator2 = NetflixAuth.this.mConsumer.getRequestParameters().keySet().iterator();
      }
      while (true)
      {
        if (!localIterator2.hasNext())
        {
          WorkerThreads.instance().invokeLater(NetflixAuth.this.retrieveTokenRunnable);
          return;
          label273: String str1 = (String)localIterator1.next();
          Logger.d("FlxMain", "NetflixAuth.onPageFinished() getResponseParameters:" + str1 + " = " + NetflixAuth.this.mProvider.getResponseParameters().get(str1));
          break;
        }
        String str2 = (String)localIterator2.next();
        Logger.d("FlxMain", "NetflixAuth.onPageFinished() getRequestParameters:" + str2 + " = " + NetflixAuth.this.mConsumer.getRequestParameters().get(str2));
      }
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      super.shouldOverrideUrlLoading(paramWebView, paramString);
      Logger.d("FlxMain", "FacebookAuth.FlixsterWebViewClient.shouldOverrideUrlLoading(..) url:" + paramString);
      return false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.NetflixAuth
 * JD-Core Version:    0.6.2
 */