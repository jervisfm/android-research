package net.flixster.android;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.utils.Logger;
import java.lang.ref.SoftReference;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;
import net.flixster.android.model.NetflixQueueItem;

public class NetflixQueueAdapter extends BaseAdapter
{
  public static final int VIEWTYPE_FOOTER = 4;
  public static final int VIEWTYPE_GETMORE = 1;
  public static final int VIEWTYPE_ITEM = 0;
  public static final int VIEWTYPE_NETFLIXLOGO = 2;
  public static final int VIEWTYPE_SUBHEADER = 3;
  Context context;
  public List<? extends Map<String, ?>> data;
  private LayoutInflater mInflater;
  public NetflixQueueAdapter mNetflixEditAdapter;
  public Handler refreshHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      NetflixQueueAdapter.this.mNetflixEditAdapter.notifyDataSetChanged();
    }
  };

  public NetflixQueueAdapter(Context paramContext, List<? extends Map<String, ?>> paramList, int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    this.data = paramList;
    this.context = paramContext;
    this.mInflater = LayoutInflater.from(paramContext);
    this.mNetflixEditAdapter = this;
  }

  public boolean areAllItemsEnabled()
  {
    return false;
  }

  public int getCount()
  {
    return this.data.size();
  }

  public Object getItem(int paramInt)
  {
    return this.data.get(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public int getItemViewType(int paramInt)
  {
    return ((Integer)((Map)this.data.get(paramInt)).get("type")).intValue();
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    int i = getItemViewType(paramInt);
    int j = FlixsterApplication.getMovieRatingType();
    switch (i)
    {
    case 3:
    default:
    case 0:
    case 1:
    case 2:
    case 4:
    }
    while (true)
    {
      Logger.e("FlxMain", "MovieListAdapter.getView(.) Bad stuff happened! (didn't catch the view type");
      return paramView;
      if (paramView == null)
        paramView = this.mInflater.inflate(2130903144, paramViewGroup, false);
      NetflixViewItemHolder localNetflixViewItemHolder = (NetflixViewItemHolder)paramView.getTag();
      if (localNetflixViewItemHolder == null)
      {
        localNetflixViewItemHolder = new NetflixViewItemHolder();
        ImageView localImageView1 = (ImageView)paramView.findViewById(2131165486);
        localNetflixViewItemHolder.movieThumbnail = localImageView1;
        ImageView localImageView2 = (ImageView)paramView.findViewById(2131165487);
        localNetflixViewItemHolder.moviePlayIcon = localImageView2;
        TextView localTextView2 = (TextView)paramView.findViewById(2131165448);
        localNetflixViewItemHolder.movieTitle = localTextView2;
        TextView localTextView3 = (TextView)paramView.findViewById(2131165452);
        localNetflixViewItemHolder.movieMeta = localTextView3;
        TextView localTextView4 = (TextView)paramView.findViewById(2131165453);
        localNetflixViewItemHolder.movieRelease = localTextView4;
        TextView localTextView5 = (TextView)paramView.findViewById(2131165451);
        localNetflixViewItemHolder.movieActors = localTextView5;
        TextView localTextView6 = (TextView)paramView.findViewById(2131165490);
        localNetflixViewItemHolder.movieScore = localTextView6;
        TextView localTextView7 = (TextView)paramView.findViewById(2131165491);
        localNetflixViewItemHolder.friendScore = localTextView7;
        LinearLayout localLinearLayout = (LinearLayout)paramView.findViewById(2131165488);
        localNetflixViewItemHolder.movieDetailsLayout = localLinearLayout;
        RelativeLayout localRelativeLayout = (RelativeLayout)paramView.findViewById(2131165445);
        localNetflixViewItemHolder.movieItem = localRelativeLayout;
        CheckBox localCheckBox = (CheckBox)paramView.findViewById(2131165691);
        localNetflixViewItemHolder.netflixCheckBox = localCheckBox;
        ImageView localImageView3 = (ImageView)paramView.findViewById(2131165693);
        localNetflixViewItemHolder.movielistGrip = localImageView3;
        paramView.setTag(localNetflixViewItemHolder);
      }
      HashMap localHashMap = (HashMap)this.data.get(paramInt);
      NetflixQueueItem localNetflixQueueItem = (NetflixQueueItem)localHashMap.get("netflixQueueItem");
      String str1 = localNetflixQueueItem.getProperty("id");
      Movie localMovie;
      int m;
      label568: String str4;
      label601: label629: Resources localResources;
      int n;
      switch (((NetflixQueuePage)this.context).mNavSelect)
      {
      default:
        localNetflixViewItemHolder.netflixCheckBox.setChecked(((NetflixQueuePage)this.context).mCheckedMap.containsKey(str1));
        localNetflixViewItemHolder.netflixCheckBox.setTag(localHashMap);
        localNetflixViewItemHolder.netflixCheckBox.setVisibility(0);
        localNetflixViewItemHolder.movielistGrip.setVisibility(0);
        String str2 = localNetflixQueueItem.getProperty("title");
        if (str2 != null)
          localNetflixViewItemHolder.movieTitle.setText(str2);
        if (localNetflixQueueItem.mMovie != null)
        {
          localMovie = localNetflixQueueItem.mMovie;
          localNetflixViewItemHolder.movieThumbnail.setOnClickListener((NetflixQueuePage)this.context);
          localNetflixViewItemHolder.movieThumbnail.setTag(localMovie);
          localNetflixViewItemHolder.movieDetailsLayout.setOnClickListener((NetflixQueuePage)this.context);
          localNetflixViewItemHolder.movieDetailsLayout.setTag(localMovie);
          localNetflixViewItemHolder.netflixCheckBox.setOnClickListener((NetflixQueuePage)this.context);
          if ((localMovie.getTheaterReleaseDate() != null) && (!FlixsterApplication.sToday.after(localMovie.getTheaterReleaseDate())))
          {
            m = 0;
            str4 = localMovie.getProperty("MOVIE_ACTORS_SHORT");
            if ((str4 != null) && (str4.length() != 0))
              break label801;
            localNetflixViewItemHolder.movieActors.setVisibility(8);
            Bitmap localBitmap2 = (Bitmap)localMovie.thumbnailSoftBitmap.get();
            if (localBitmap2 == null)
              break label823;
            localNetflixViewItemHolder.movieThumbnail.setImageBitmap(localBitmap2);
            localResources = this.context.getResources();
            n = 0;
            switch (j)
            {
            default:
              label668: if (n != 0)
                localNetflixViewItemHolder.movieScore.setVisibility(8);
              String str6 = localMovie.getProperty("meta");
              if (str6 != null)
              {
                localNetflixViewItemHolder.movieMeta.setText(str6);
                localNetflixViewItemHolder.movieMeta.setVisibility(0);
              }
              break;
            case 0:
            case 1:
            case 2:
            }
          }
        }
        break;
      case 3:
      case 4:
      }
      while (true)
      {
        return paramView;
        localNetflixViewItemHolder.netflixCheckBox.setChecked(((NetflixQueuePage)this.context).mCheckedMap.containsKey(str1));
        localNetflixViewItemHolder.netflixCheckBox.setTag(localHashMap);
        localNetflixViewItemHolder.netflixCheckBox.setVisibility(0);
        localNetflixViewItemHolder.movielistGrip.setVisibility(4);
        break;
        localNetflixViewItemHolder.netflixCheckBox.setVisibility(8);
        localNetflixViewItemHolder.movielistGrip.setVisibility(4);
        break;
        m = 1;
        break label568;
        label801: localNetflixViewItemHolder.movieActors.setText(str4);
        localNetflixViewItemHolder.movieActors.setVisibility(0);
        break label601;
        label823: String str5 = localMovie.getProperty("thumbnail");
        if ((str5 != null) && (str5.startsWith("http")))
        {
          localNetflixViewItemHolder.movieThumbnail.setImageResource(2130837844);
          long l2 = System.nanoTime();
          if (l2 <= 3000000000.0D + localMovie.getThumbOrderStamp())
            break label629;
          ((FlixsterListActivity)this.context).orderImage(new ImageOrder(0, localMovie, str5, localNetflixViewItemHolder.movieThumbnail, this.refreshHandler));
          localMovie.setThumbOrderStamp(l2);
          break label629;
        }
        localNetflixViewItemHolder.movieThumbnail.setImageResource(2130837839);
        break label629;
        if (localMovie.checkIntProperty("popcornScore"))
        {
          int i4 = localMovie.getIntProperty("popcornScore").intValue();
          int i5;
          label971: int i6;
          if (i4 < 60)
          {
            i5 = 1;
            StringBuilder localStringBuilder = new StringBuilder(i4).append("%");
            if (m == 0)
              localStringBuilder.append(" ").append(localResources.getString(2131493010));
            localNetflixViewItemHolder.movieScore.setText(localStringBuilder.toString());
            if (m != 0)
              break label1096;
            i6 = 2130837750;
          }
          while (true)
          {
            Drawable localDrawable2 = localResources.getDrawable(i6);
            localDrawable2.setBounds(0, 0, localDrawable2.getIntrinsicWidth(), localDrawable2.getIntrinsicHeight());
            localNetflixViewItemHolder.movieScore.setCompoundDrawables(localDrawable2, null, null, null);
            localNetflixViewItemHolder.movieScore.setVisibility(0);
            n = 0;
            break;
            i5 = 0;
            break label971;
            label1096: if (i5 != 0)
              i6 = 2130837743;
            else
              i6 = 2130837738;
          }
        }
        n = 1;
        break label668;
        if (localMovie.checkIntProperty("rottenTomatoes"))
        {
          int i1 = localMovie.getIntProperty("rottenTomatoes").intValue();
          if (i1 < 60);
          for (Drawable localDrawable1 = localResources.getDrawable(2130837741); ; localDrawable1 = localResources.getDrawable(2130837726))
          {
            int i2 = localDrawable1.getIntrinsicWidth();
            int i3 = localDrawable1.getIntrinsicHeight();
            localDrawable1.setBounds(0, 0, i2, i3);
            localNetflixViewItemHolder.movieScore.setCompoundDrawables(localDrawable1, null, null, null);
            localNetflixViewItemHolder.movieScore.setText(i1 + "%");
            localNetflixViewItemHolder.movieScore.setVisibility(0);
            n = 0;
            break;
          }
        }
        n = 1;
        break label668;
        n = 1;
        break label668;
        localNetflixViewItemHolder.movieMeta.setVisibility(8);
        continue;
        localNetflixViewItemHolder.movieScore.setVisibility(8);
        localNetflixViewItemHolder.movieActors.setVisibility(8);
        localNetflixViewItemHolder.movieMeta.setVisibility(8);
        localNetflixViewItemHolder.movieThumbnail.setImageResource(2130837839);
        localNetflixViewItemHolder.movieThumbnail.setOnClickListener(null);
        localNetflixViewItemHolder.movieDetailsLayout.setOnClickListener(null);
        Bitmap localBitmap1 = localNetflixQueueItem.thumbnailBitmap;
        if (localBitmap1 != null)
        {
          localNetflixViewItemHolder.movieThumbnail.setImageBitmap(localBitmap1);
        }
        else
        {
          String str3 = localNetflixQueueItem.getProperty("box_art");
          if ((str3 != null) && (str3.startsWith("http")))
          {
            localNetflixViewItemHolder.movieThumbnail.setImageResource(2130837844);
            long l1 = System.nanoTime();
            if (l1 > 3000000000.0D + localNetflixQueueItem.getThumbOrderStamp())
            {
              ((FlixsterListActivity)this.context).orderImage(new ImageOrder(7, localNetflixQueueItem, str3, localNetflixViewItemHolder.movieThumbnail, this.refreshHandler));
              localNetflixQueueItem.setThumbOrderStamp(l1);
            }
          }
          else
          {
            localNetflixViewItemHolder.movieThumbnail.setImageResource(2130837839);
          }
        }
      }
      if (paramView == null)
        paramView = this.mInflater.inflate(2130903146, paramViewGroup, false);
      paramView.setId(2130903146);
      TextView localTextView1 = (TextView)paramView.findViewById(2131165697);
      int k = ((NetflixQueuePage)this.context).mNavSelect;
      switch (((NetflixQueuePage)this.context).mMoreStateSelect[k])
      {
      default:
      case 1:
      case 2:
      case 3:
      }
      while (true)
      {
        return paramView;
        paramView.setOnClickListener(null);
        localTextView1.setText(2131493080);
        localTextView1.setTextColor(-7829368);
        continue;
        NetflixQueuePage localNetflixQueuePage2 = (NetflixQueuePage)this.context;
        paramView.setOnClickListener(localNetflixQueuePage2);
        localTextView1.setText(2131493081);
        continue;
        paramView.setOnClickListener(null);
        localTextView1.setVisibility(8);
      }
      if (paramView == null)
        paramView = this.mInflater.inflate(2130903145, paramViewGroup, false);
      paramView.setId(2130903145);
      NetflixQueuePage localNetflixQueuePage1 = (NetflixQueuePage)this.context;
      paramView.setOnClickListener(localNetflixQueuePage1);
      return paramView;
      Context localContext = this.context;
      paramView = new TextView(localContext);
      ((TextView)paramView).setText("footer");
    }
  }

  public int getViewTypeCount()
  {
    return 5;
  }

  public boolean isEnabled(int paramInt)
  {
    return ((Integer)((Map)this.data.get(paramInt)).get("type")).intValue() < 3;
  }

  static class NetflixViewItemHolder
  {
    TextView friendScore;
    TextView movieActors;
    LinearLayout movieDetailsLayout;
    TextView movieHeader;
    RelativeLayout movieItem;
    TextView movieMeta;
    ImageView moviePlayIcon;
    TextView movieRelease;
    TextView movieScore;
    ImageView movieThumbnail;
    TextView movieTitle;
    ImageView movielistGrip;
    CheckBox netflixCheckBox;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.NetflixQueueAdapter
 * JD-Core Version:    0.6.2
 */