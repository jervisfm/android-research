package net.flixster.android;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.NewsDao;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviAd;
import net.flixster.android.lvi.LviAdapter;
import net.flixster.android.lvi.LviNewsStory;
import net.flixster.android.lvi.LviPageHeader;
import net.flixster.android.model.NewsStory;

@Deprecated
public class NewsListPage extends LviActivity
{
  private AdapterView.OnItemClickListener mNewsStoryClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      Lvi localLvi = (Lvi)NewsListPage.this.mData.get(paramAnonymousInt);
      if (localLvi.getItemViewType() == Lvi.VIEW_TYPE_NEWSITEM)
      {
        Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(((LviNewsStory)localLvi).mNewsStory.mSource));
        NewsListPage.this.startActivity(localIntent);
      }
    }
  };
  private Handler mUpdateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      NewsListPage.this.mData.clear();
      NewsListPage.this.mData.addAll(NewsListPage.this.mDataHolder);
      NewsListPage.this.mLviListPageAdapter.notifyDataSetChanged();
    }
  };

  private void ScheduleLoadNewsItemsTask(int paramInt)
  {
    this.throbberHandler.sendEmptyMessage(1);
    TimerTask local3 = new TimerTask()
    {
      public void run()
      {
        try
        {
          ArrayList localArrayList = NewsDao.getNewsItemList();
          LviPageHeader localLviPageHeader = new LviPageHeader();
          localLviPageHeader.mTitle = NewsListPage.this.getResourceString(2131493088);
          NewsListPage.this.mData.add(localLviPageHeader);
          NewsListPage.this.destroyExistingLviAd();
          NewsListPage.this.lviAd = new LviAd();
          NewsListPage.this.lviAd.mAdSlot = "TopNews";
          NewsListPage.this.mDataHolder.clear();
          NewsListPage.this.mDataHolder.add(localLviPageHeader);
          NewsListPage.this.mDataHolder.add(NewsListPage.this.lviAd);
          Iterator localIterator = localArrayList.iterator();
          while (true)
          {
            if (!localIterator.hasNext())
            {
              NewsListPage.this.mUpdateHandler.sendEmptyMessage(0);
              return;
            }
            NewsStory localNewsStory = (NewsStory)localIterator.next();
            LviNewsStory localLviNewsStory = new LviNewsStory();
            localLviNewsStory.mNewsStory = localNewsStory;
            NewsListPage.this.mDataHolder.add(localLviNewsStory);
          }
        }
        catch (DaoException localDaoException)
        {
          localDaoException.printStackTrace();
          NewsListPage localNewsListPage = NewsListPage.this;
          localNewsListPage.mRetryCount = (1 + localNewsListPage.mRetryCount);
          if (NewsListPage.this.mRetryCount < 3)
            NewsListPage.this.ScheduleLoadNewsItemsTask(1000);
          while (true)
          {
            return;
            NewsListPage.this.mRetryCount = 0;
            NewsListPage.this.mShowDialogHandler.sendEmptyMessage(2);
          }
        }
        finally
        {
          NewsListPage.this.throbberHandler.sendEmptyMessage(0);
        }
      }
    };
    if (this.mPageTimer != null)
      this.mPageTimer.schedule(local3, 100L);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mListView.setOnItemClickListener(this.mNewsStoryClickListener);
    Trackers.instance().track("/topnews", "top news");
  }

  public void onPause()
  {
    super.onPause();
  }

  public void onResume()
  {
    super.onResume();
    if (this.mPageTimer == null)
      this.mPageTimer = new Timer();
    ScheduleLoadNewsItemsTask(10);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.NewsListPage
 * JD-Core Version:    0.6.2
 */