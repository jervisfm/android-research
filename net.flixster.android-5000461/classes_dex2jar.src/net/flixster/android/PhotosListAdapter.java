package net.flixster.android;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.flixster.android.utils.ImageTask;
import java.util.ArrayList;
import java.util.List;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Photo;

public class PhotosListAdapter extends BaseAdapter
{
  private View.OnClickListener clickListener;
  private Context context;
  private List<Object> data = new ArrayList();
  int mPhotoHeight;
  private Handler photoHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      ImageView localImageView = (ImageView)paramAnonymousMessage.obj;
      if (localImageView != null)
      {
        Photo localPhoto = (Photo)localImageView.getTag();
        if (localPhoto != null)
        {
          localImageView.setImageBitmap(localPhoto.bitmap);
          localImageView.invalidate();
        }
      }
    }
  };

  public PhotosListAdapter(Context paramContext, List<Object> paramList, View.OnClickListener paramOnClickListener)
  {
    this.context = paramContext;
    this.data = paramList;
    this.clickListener = paramOnClickListener;
    this.mPhotoHeight = paramContext.getResources().getDimensionPixelOffset(2131361849);
  }

  public int getCount()
  {
    return this.data.size();
  }

  public Object getItem(int paramInt)
  {
    return this.data.get(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    ImageView localImageView;
    Photo localPhoto;
    if (paramView == null)
    {
      localImageView = new ImageView(this.context);
      localImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
      if ((this.data != null) && (this.data.size() > paramInt))
      {
        localPhoto = (Photo)this.data.get(paramInt);
        if (localPhoto != null)
        {
          localImageView.setTag(localPhoto);
          if (localPhoto.bitmap == null)
            break label132;
          localImageView.setImageBitmap(localPhoto.bitmap);
        }
      }
    }
    while (true)
    {
      localImageView.setMinimumHeight(this.mPhotoHeight);
      localImageView.setFocusable(true);
      localImageView.setClickable(true);
      localImageView.setOnClickListener(this.clickListener);
      return localImageView;
      localImageView = (ImageView)paramView;
      break;
      label132: if (localPhoto.thumbnailUrl != null)
      {
        localImageView.setImageResource(2130837841);
        ImageOrder localImageOrder = new ImageOrder(3, localPhoto, localPhoto.thumbnailUrl, localImageView, this.photoHandler);
        if ((this.context instanceof ImageTask))
          ((ImageTask)this.context).orderImage(localImageOrder);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.PhotosListAdapter
 * JD-Core Version:    0.6.2
 */