package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.utils.Logger;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;
import net.flixster.android.model.User;

public class ProfileReviewPage extends FlixsterActivity
  implements View.OnClickListener
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  public static final String KEY_REVIEW_INDEX = "REVIEW_INDEX";
  private final Handler movieImageHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      ImageView localImageView = (ImageView)paramAnonymousMessage.obj;
      if (localImageView != null)
      {
        Movie localMovie = (Movie)localImageView.getTag();
        if ((localMovie != null) && (localMovie.thumbnailSoftBitmap.get() != null))
        {
          localImageView.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
          localImageView.invalidate();
        }
      }
    }
  };
  private String platformId;
  private int reviewIndex;
  private ArrayList<Review> reviews;
  private View throbber;
  private Timer timer;
  private final Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (ProfileReviewPage.this.isFinishing());
      do
      {
        return;
        Logger.d("FlxMain", "ProfileReviewPage.updateHandler - mReviewsList:" + ProfileReviewPage.this.reviews);
        ProfileReviewPage.this.hideLoading();
        if ((ProfileReviewPage.this.user != null) && (ProfileReviewPage.this.reviews != null) && (!ProfileReviewPage.this.reviews.isEmpty()))
        {
          ProfileReviewPage.this.updatePage();
          return;
        }
      }
      while (ProfileReviewPage.this.isFinishing());
      ProfileReviewPage.this.showDialog(1);
    }
  };
  private User user;
  private ViewFlipper viewFlipper;

  private String getPageTitle()
  {
    return getString(2131492955) + " - " + (1 + this.viewFlipper.getDisplayedChild()) + " of " + this.reviews.size();
  }

  private void hideLoading()
  {
    this.throbber.setVisibility(8);
  }

  private void scheduleUpdatePageTask()
  {
    TimerTask local3 = new TimerTask()
    {
      public void run()
      {
        ProfileReviewPage.this.platformId = ProfileReviewPage.this.getIntent().getExtras().getString("PLATFORM_ID");
        if (ProfileReviewPage.this.platformId == null)
          ProfileReviewPage.this.platformId = FlixsterApplication.getPlatformId();
        if (ProfileReviewPage.this.user == null);
        try
        {
          ProfileReviewPage.this.user = ProfileDao.fetchUser(ProfileReviewPage.this.platformId);
          if ((ProfileReviewPage.this.user != null) && (ProfileReviewPage.this.user.id != null) && ((ProfileReviewPage.this.reviews == null) || (ProfileReviewPage.this.reviews.isEmpty())))
            ProfileReviewPage.this.reviews = new ArrayList();
        }
        catch (DaoException localDaoException2)
        {
          while (true)
          {
            int i;
            try
            {
              ArrayList localArrayList1 = new ArrayList();
              ArrayList localArrayList2 = new ArrayList();
              List localList = ProfileDao.getUserReviews(ProfileReviewPage.this.user.id, 25);
              i = 0;
              if (i >= localList.size())
              {
                ProfileReviewPage.this.user.wantToSeeReviews = localArrayList2;
                ProfileReviewPage.this.user.ratedReviews = localArrayList1;
                ProfileReviewPage.this.reviews.addAll(ProfileReviewPage.this.user.wantToSeeReviews);
                ProfileReviewPage.this.reviews.addAll(ProfileReviewPage.this.user.ratedReviews);
                ProfileReviewPage.this.updateHandler.sendEmptyMessage(0);
                return;
                localDaoException2 = localDaoException2;
                Logger.e("FlxMain", "UserProfilePage.scheduleUpdatePageTask (failed to get user data)", localDaoException2);
                ProfileReviewPage.this.user = null;
                continue;
              }
              Review localReview = (Review)localList.get(i);
              if (localReview.isWantToSee())
                localArrayList2.add(localReview);
              else if (!localReview.isNotInterested())
                localArrayList1.add(localReview);
            }
            catch (DaoException localDaoException1)
            {
              Logger.e("FlxMain", "UserProfilePage.scheduleUpdatePageTask (failed to get review data)", localDaoException1);
              continue;
            }
            i++;
          }
        }
      }
    };
    if (this.timer != null)
      this.timer.schedule(local3, 100L);
  }

  private void showLoading()
  {
    this.throbber.setVisibility(0);
  }

  private void updatePage()
  {
    Logger.d("FlxMain", "ProfileReviewPage.updatePage reviewIndex:" + this.reviewIndex);
    LayoutInflater localLayoutInflater;
    Iterator localIterator;
    if (this.user != null)
    {
      ImageView localImageView1 = (ImageView)findViewById(2131165642);
      Bitmap localBitmap = this.user.getThumbnailBitmap(localImageView1);
      if (localBitmap != null)
        localImageView1.setImageBitmap(localBitmap);
      ((TextView)findViewById(2131165643)).setText(this.user.displayName);
      if ((this.reviews != null) && (!this.reviews.isEmpty()))
      {
        this.viewFlipper.removeAllViews();
        localLayoutInflater = getLayoutInflater();
        localIterator = this.reviews.iterator();
        if (localIterator.hasNext())
          break label146;
        this.viewFlipper.setDisplayedChild(this.reviewIndex);
        setActionBarTitle(getPageTitle());
      }
    }
    return;
    label146: Review localReview = (Review)localIterator.next();
    Movie localMovie = localReview.getMovie();
    ScrollView localScrollView = (ScrollView)localLayoutInflater.inflate(2130903136, this.viewFlipper, false);
    RelativeLayout localRelativeLayout = (RelativeLayout)localScrollView.findViewById(2131165629);
    localRelativeLayout.setTag(localMovie);
    localRelativeLayout.setFocusable(true);
    localRelativeLayout.setOnClickListener(this);
    ImageView localImageView2 = (ImageView)localScrollView.findViewById(2131165630);
    localImageView2.setTag(localMovie);
    if (localMovie.thumbnailSoftBitmap.get() != null)
      localImageView2.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
    String[] arrayOfString;
    int[] arrayOfInt;
    int i;
    while (true)
    {
      arrayOfString = new String[] { "title", "MOVIE_ACTORS_SHORT", "meta" };
      arrayOfInt = new int[] { 2131165631, 2131165632, 2131165633 };
      i = 0;
      if (i < arrayOfString.length)
        break label437;
      ((ImageView)localScrollView.findViewById(2131165636)).setImageResource(Flixster.RATING_LARGE_R[((int)(2.0D * localReview.stars))]);
      ((TextView)localScrollView.findViewById(2131165637)).setText(localReview.comment);
      this.viewFlipper.addView(localScrollView);
      break;
      String str = localMovie.getProperty("thumbnail");
      if ((str != null) && (str.startsWith("http")))
        orderImage(new ImageOrder(0, localMovie, str, localImageView2, this.movieImageHandler));
      else
        localImageView2.setImageResource(2130837839);
    }
    label437: TextView localTextView = (TextView)localScrollView.findViewById(arrayOfInt[i]);
    if (localMovie.checkProperty(arrayOfString[i]))
    {
      localTextView.setText(localMovie.getProperty(arrayOfString[i]));
      localTextView.setVisibility(0);
    }
    while (true)
    {
      i++;
      break;
      localTextView.setVisibility(8);
    }
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
    case 2131165629:
    }
    Movie localMovie;
    do
    {
      return;
      localMovie = (Movie)paramView.getTag();
    }
    while (localMovie == null);
    Starter.launchMovieDetail(localMovie.getId(), this);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.reviewIndex = localBundle.getInt("REVIEW_INDEX");
    setContentView(2130903137);
    findViewById(2131165638).setVisibility(8);
    findViewById(2131165640).setVisibility(8);
    findViewById(2131165639).setVisibility(8);
    findViewById(2131165641).setVisibility(8);
    createActionBar();
    setActionBarTitle(2131492955);
    this.viewFlipper = ((ViewFlipper)findViewById(2131165644));
    this.throbber = findViewById(2131165241);
    updatePage();
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        ProfileReviewPage.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        ProfileReviewPage.this.hideLoading();
      }
    });
    return localBuilder.create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getSupportMenuInflater().inflate(2131689486, paramMenu);
    return true;
  }

  public void onDestroy()
  {
    super.onDestroy();
    if (this.timer != null)
    {
      this.timer.cancel();
      this.timer.purge();
    }
    this.timer = null;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165963:
      this.viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, 2130968584));
      this.viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, 2130968585));
      this.viewFlipper.showPrevious();
      setActionBarTitle(getPageTitle());
      return true;
    case 2131165964:
    }
    this.viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, 2130968582));
    this.viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, 2130968583));
    this.viewFlipper.showNext();
    setActionBarTitle(getPageTitle());
    return true;
  }

  public void onResume()
  {
    super.onResume();
    if (this.timer == null)
      this.timer = new Timer();
    if ((this.reviews == null) || (this.reviews.isEmpty()))
    {
      showLoading();
      scheduleUpdatePageTask();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ProfileReviewPage
 * JD-Core Version:    0.6.2
 */