package net.flixster.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder.DialogListener;
import com.flixster.android.view.QuickRateView;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Property;
import net.flixster.android.model.User;

public class QuickRatePage extends DecoratedSherlockActivity
{
  public static final String KEY_IS_WTS = "KEY_IS_WTS";
  public static final int REWARDS_QUICKRATE_DEFAULT = 25;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      QuickRatePage.this.throbber.setVisibility(8);
      if ((paramAnonymousMessage.obj instanceof DaoException))
        ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, QuickRatePage.this);
    }
  };
  private boolean isWts;
  private final DialogBuilder.DialogListener movieEarnedDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      QuickRatePage.this.finish();
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
    }
  };
  private int progressCompleted;
  private final Handler quickRateActionHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      QuickRatePage localQuickRatePage = QuickRatePage.this;
      localQuickRatePage.progressCompleted += paramAnonymousMessage.what;
      QuickRatePage.this.updateQuickRateProgress();
    }
  };
  private LinearLayout quickRateList;
  private TextView quickRateProgress;
  private int rewardThreshold;
  private final Handler successHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      QuickRatePage.this.quickRateList.removeAllViews();
      QuickRatePage.this.throbber.setVisibility(8);
      Iterator localIterator = ((List)paramAnonymousMessage.obj).iterator();
      while (true)
      {
        if (!localIterator.hasNext())
          return;
        Movie localMovie = (Movie)localIterator.next();
        QuickRateView localQuickRateView = new QuickRateView(QuickRatePage.this, QuickRatePage.this.isWts, QuickRatePage.this.quickRateActionHandler);
        QuickRatePage.this.quickRateList.addView(localQuickRateView);
        localQuickRateView.load(localMovie);
      }
    }
  };
  private ProgressBar throbber;

  private void updateQuickRateProgress()
  {
    User localUser = AccountFacade.getSocialUser();
    if (localUser.isMskEligible)
    {
      this.quickRateProgress.setVisibility(8);
      return;
    }
    TextView localTextView = this.quickRateProgress;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = Integer.valueOf(this.progressCompleted);
    arrayOfObject[1] = Integer.valueOf(Math.max(this.rewardThreshold - this.progressCompleted, 0));
    localTextView.setText(getString(2131493205, arrayOfObject));
    this.quickRateProgress.setVisibility(0);
    if (this.progressCompleted >= this.rewardThreshold)
    {
      String str1;
      Tracker localTracker;
      if (this.isWts)
      {
        str1 = "MKW";
        localUser.isMskWtsEligible = false;
        localTracker = Trackers.instance();
        if (!this.isWts)
          break label169;
      }
      label169: for (String str2 = "QuickWts"; ; str2 = "QuickRate")
      {
        localTracker.trackEvent("/rewards", null, "FlixsterRewards", "CompletedReward", str2, 1);
        ProfileDao.insertLockerRight(null, null, str1);
        showDialog(1000000401, this.movieEarnedDialogListener);
        return;
        str1 = "MKR";
        localUser.isMskRateEligible = false;
        break;
      }
    }
    if (this.isWts)
    {
      localUser.wtsCount = this.progressCompleted;
      return;
    }
    localUser.ratingCount = this.progressCompleted;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903151);
    createActionBar();
    this.isWts = getIntent().getBooleanExtra("KEY_IS_WTS", false);
    Property localProperty = Properties.instance().getProperties();
    int n;
    int i;
    label67: User localUser;
    int j;
    label91: int k;
    label147: int m;
    label174: Tracker localTracker;
    String str1;
    if (localProperty != null)
    {
      if (this.isWts)
      {
        n = localProperty.rewardsQuickWts;
        this.rewardThreshold = n;
        if (n != 0)
          break label259;
      }
    }
    else
    {
      i = 25;
      this.rewardThreshold = i;
      localUser = AccountFacade.getSocialUser();
      if (!this.isWts)
        break label267;
      j = localUser.wtsCount;
      this.progressCompleted = j;
      this.quickRateProgress = ((TextView)findViewById(2131165709));
      this.quickRateList = ((LinearLayout)findViewById(2131165710));
      this.throbber = ((ProgressBar)findViewById(2131165241));
      if (!this.isWts)
        break label277;
      k = 2131493204;
      setActionBarTitle(getString(k));
      TextView localTextView = this.quickRateProgress;
      if (!this.isWts)
        break label284;
      m = 2130837859;
      localTextView.setCompoundDrawablesWithIntrinsicBounds(m, 0, 0, 0);
      updateQuickRateProgress();
      this.throbber.setVisibility(0);
      ProfileDao.getQuickRateMovies(this.successHandler, this.errorHandler, this.isWts);
      localTracker = Trackers.instance();
      if (!this.isWts)
        break label291;
      str1 = "/rewards/quick-wts";
      label227: if (!this.isWts)
        break label298;
    }
    label259: label267: label277: label284: label291: label298: for (String str2 = "Rewards - QuickWts"; ; str2 = "Rewards - QuickRate")
    {
      localTracker.track(str1, str2);
      return;
      n = localProperty.rewardsQuickRate;
      break;
      i = this.rewardThreshold;
      break label67;
      j = localUser.ratingCount;
      break label91;
      k = 2131493203;
      break label147;
      m = 2130837855;
      break label174;
      str1 = "/rewards/quick-rate";
      break label227;
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.QuickRatePage
 * JD-Core Version:    0.6.2
 */