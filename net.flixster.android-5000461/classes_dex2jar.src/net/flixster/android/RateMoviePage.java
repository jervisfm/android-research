package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.utils.Logger;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.Review;
import net.flixster.android.model.User;
import org.json.JSONException;
import org.json.JSONObject;

public class RateMoviePage extends FlixsterActivity
  implements View.OnClickListener, RatingBar.OnRatingBarChangeListener
{
  private static final int DIALOGKEY_NETWORKFAIL = 2;
  private static final int DIALOGKEY_SAVING = 1;
  public static final String KEY_FEED_URL = "net.flixster.FeedUrl";
  static final int REQUESTCODE_REVIEW = 1;
  static final int WIDGET_NI = 2;
  static final int WIDGET_NONE = 0;
  static final int WIDGET_RATINGBAR = 3;
  static final int WIDGET_WTS = 1;
  private Handler handlePostFail = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "RateMoviePage.handlePostFail run...");
      if (!RateMoviePage.this.isFinishing())
        RateMoviePage.this.showDialog(2);
    }
  };
  private Handler handlePostSuccess = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      RateMoviePage.this.mIntent.putExtra("net.flixster.ReviewComment", RateMoviePage.this.mReview.comment);
      RateMoviePage.this.mIntent.putExtra("net.flixster.ReviewStars", RateMoviePage.this.mReview.stars);
      if (RateMoviePage.this.mMovieTitle != null)
        Trackers.instance().track("/movie/addrating", "Add Rating - " + RateMoviePage.this.mMovieTitle);
      while (true)
      {
        JSONObject localJSONObject;
        if ((RateMoviePage.this.postNewsFeed) && (paramAnonymousMessage != null) && (paramAnonymousMessage.obj != null))
        {
          localJSONObject = (JSONObject)paramAnonymousMessage.obj;
          if ((!FlixsterApplication.isWifi()) || (!localJSONObject.has("newsfeedUrl")));
        }
        try
        {
          String str = localJSONObject.getString("newsfeedUrl");
          if (!"".equals(str))
            RateMoviePage.this.mIntent.putExtra("net.flixster.FeedUrl", str);
          RateMoviePage.this.setResult(-1, RateMoviePage.this.mIntent);
          RateMoviePage.this.finish();
          return;
          Trackers.instance().track("/movie/addrating", "Add Rating - <unknown>");
        }
        catch (JSONException localJSONException)
        {
          while (true)
            Logger.w("FlxMain", "problem parsing response " + localJSONObject, localJSONException);
        }
      }
    }
  };
  private EditText mComment;
  private Bundle mExtras;
  private Intent mIntent;
  private Boolean mIsConnected = null;
  private TimerTask mLoadReviewTask;
  private Long mMovieId = Long.valueOf(0L);
  private String mMovieTitle;
  private View mNiButton;
  private Handler mPromoHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!RateMoviePage.this.mIsConnected.booleanValue())
      {
        Intent localIntent = new Intent(RateMoviePage.this.mRateMoviePage, ConnectRatePage.class);
        RateMoviePage.this.startActivityForResult(localIntent, 1);
      }
    }
  };
  private RateMoviePage mRateMoviePage;
  private RatingBar mRatingBar;
  private Review mReview;
  private ProgressDialog mSavingDialog;
  private int mSelectedWidget = 0;
  private Timer mTimer;
  private View mWtsButton;
  private CheckBox postFacebookCheckbox;
  private boolean postNewsFeed;
  private Handler refreshHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "RateMoviePage.refreshHandler run...");
      RateMoviePage.this.mRateMoviePage.populatePage();
    }
  };

  private void ScheduleLoadReviewTask()
  {
    this.mLoadReviewTask = new TimerTask()
    {
      public void run()
      {
        String str = FlixsterApplication.getFlixsterId();
        try
        {
          RateMoviePage.this.mReview = ProfileDao.getUserMovieReview(str, Long.toString(RateMoviePage.this.mMovieId.longValue()));
          RateMoviePage.access$1(RateMoviePage.this).refreshHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          while (true)
            Logger.e("FlxMain", "Error from review", localDaoException);
        }
      }
    };
    if (this.mTimer != null)
      this.mTimer.schedule(this.mLoadReviewTask, 100L);
  }

  private void getExtras()
  {
    Logger.d("FlxMain", "RateMoviePage.getExtras()");
    this.mExtras = getIntent().getExtras();
    if (this.mExtras != null)
    {
      this.mMovieId = Long.valueOf(this.mExtras.getLong("net.flixster.android.EXTRA_MOVIE_ID"));
      if (this.mExtras.containsKey("title"))
        this.mMovieTitle = this.mExtras.getString("title");
      Double localDouble = Double.valueOf(this.mExtras.getDouble("net.flixster.ReviewStars"));
      String str = this.mExtras.getString("net.flixster.ReviewComment");
      this.mIsConnected = Boolean.valueOf(this.mExtras.getBoolean("net.flixster.IsConnected"));
      if ((this.mIsConnected != null) && (this.mIsConnected.booleanValue()) && (this.mReview == null) && ((localDouble != null) || (str != null)))
      {
        Logger.d("FlxMain", "RateMoviePage.getExtras() new Review() comment:" + str + " stars:" + localDouble);
        this.mReview = new Review();
        this.mReview.type = 1;
        this.mReview.comment = str;
        this.mReview.stars = localDouble.doubleValue();
      }
      Trackers.instance().track("/movie/info/myRating", "Add Rating - " + this.mMovieTitle);
      return;
    }
    Logger.e("FlxMain", "Missing the bundle extras... ");
  }

  private void populatePage()
  {
    Logger.d("FlxMain", "RateMoviePage.populatePage()");
    if (this.mReview != null)
    {
      if (this.mReview.stars != 0.0D)
        break label51;
      this.mSelectedWidget = 0;
    }
    while (true)
    {
      selectionSet();
      this.mComment.setText(this.mReview.comment);
      return;
      label51: if (this.mReview.isWantToSee())
        this.mSelectedWidget = 1;
      else if (this.mReview.isNotInterested())
        this.mSelectedWidget = 2;
      else
        this.mSelectedWidget = 3;
    }
  }

  private void saveRating()
  {
    switch (this.mSelectedWidget)
    {
    default:
    case 3:
    case 1:
    case 2:
    }
    while (true)
    {
      this.mReview.comment = this.mComment.getText().toString();
      showDialog(1);
      TimerTask local6 = new TimerTask()
      {
        public void run()
        {
          try
          {
            String str = FlixsterApplication.getFlixsterId();
            RateMoviePage localRateMoviePage = RateMoviePage.this;
            if ((RateMoviePage.this.postFacebookCheckbox != null) && (RateMoviePage.this.postFacebookCheckbox.getVisibility() == 0) && (RateMoviePage.this.postFacebookCheckbox.isChecked()));
            for (boolean bool = true; ; bool = false)
            {
              localRateMoviePage.postNewsFeed = bool;
              JSONObject localJSONObject = ProfileDao.postUserMovieReview(str, Long.toString(RateMoviePage.this.mMovieId.longValue()), RateMoviePage.this.mReview, RateMoviePage.this.postNewsFeed);
              User localUser = AccountFacade.getSocialUser();
              localUser.ratedReviews = null;
              localUser.wantToSeeReviews = null;
              RateMoviePage.this.handlePostSuccess.sendMessage(Message.obtain(RateMoviePage.this.handlePostSuccess, 0, localJSONObject));
              return;
            }
          }
          catch (Exception localException)
          {
            RateMoviePage.this.handlePostFail.sendEmptyMessage(0);
          }
        }
      };
      if (this.mTimer != null)
        this.mTimer.schedule(local6, 10L);
      return;
      this.mReview.stars = this.mRatingBar.getRating();
      continue;
      this.mReview.stars = 5.5D;
      continue;
      this.mReview.stars = 6.0D;
    }
  }

  private void selectionSet()
  {
    int i = 1;
    View localView1 = this.mWtsButton;
    View localView2;
    if (this.mSelectedWidget == i)
    {
      int k = i;
      localView1.setSelected(k);
      localView2 = this.mNiButton;
      if (this.mSelectedWidget != 2)
        break label78;
    }
    while (true)
    {
      localView2.setSelected(i);
      if ((this.mSelectedWidget != 3) || (this.mReview == null))
        break label83;
      this.mRatingBar.setRating((float)this.mReview.stars);
      return;
      int m = 0;
      break;
      label78: int j = 0;
    }
    label83: this.mRatingBar.setRating(0.0F);
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt2 == -1) && (paramInt1 == 1))
    {
      this.mIsConnected = Boolean.valueOf(true);
      getIntent().putExtra("net.flixster.IsConnected", new Boolean(true));
      return;
    }
    setResult(0);
    finish();
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
    case 2131165714:
    case 2131165715:
    case 2131165272:
    case 2131165273:
    }
    while (true)
    {
      selectionSet();
      return;
      if (this.mSelectedWidget == 1)
      {
        this.mSelectedWidget = 0;
        this.mReview.stars = 0.0D;
      }
      else
      {
        this.mSelectedWidget = 1;
        this.mReview.stars = 5.5D;
        continue;
        if (this.mSelectedWidget == 2)
        {
          this.mSelectedWidget = 0;
          this.mReview.stars = 0.0D;
        }
        else
        {
          this.mSelectedWidget = 2;
          this.mReview.stars = 6.0D;
          continue;
          startActivity(new Intent(this, FacebookAuth.class));
          continue;
          startActivity(new Intent(this, FlixsterLoginPage.class));
        }
      }
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mIntent = getIntent();
    setContentView(2130903153);
    createActionBar();
    setActionBarTitle(2131492927);
    this.mRateMoviePage = this;
    this.mWtsButton = findViewById(2131165714);
    this.mWtsButton.setOnClickListener(this);
    this.mNiButton = findViewById(2131165715);
    this.mNiButton.setOnClickListener(this);
    this.mRatingBar = ((RatingBar)findViewById(2131165716));
    this.mRatingBar.setOnRatingBarChangeListener(this);
    this.mComment = ((EditText)findViewById(2131165720));
    this.postFacebookCheckbox = ((CheckBox)findViewById(2131165718));
    this.postFacebookCheckbox.setVisibility(8);
    this.postFacebookCheckbox.setChecked(false);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1:
      this.mSavingDialog = new ProgressDialog(this);
      this.mSavingDialog.setMessage(getResources().getString(2131493173));
      this.mSavingDialog.setIndeterminate(true);
      this.mSavingDialog.setCancelable(true);
      this.mSavingDialog.setCanceledOnTouchOutside(true);
      return this.mSavingDialog;
    case 2:
    }
    return new AlertDialog.Builder(this).setMessage("The network connection failed.").setTitle("Network Error").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        if ((RateMoviePage.this.mSavingDialog != null) && (RateMoviePage.this.mSavingDialog.isShowing()))
          RateMoviePage.this.removeDialog(1);
      }
    }).create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getSupportMenuInflater().inflate(2131689485, paramMenu);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    if (this.mTimer != null)
    {
      this.mTimer.cancel();
      this.mTimer.purge();
    }
    this.mTimer = null;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getAction() == 0) && (paramInt == 4))
    {
      Logger.d("FlxMain", "RateMoviePage.onKeyDown - call finish");
      setResult(0, this.mIntent);
      finish();
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165962:
    }
    saveRating();
    return true;
  }

  protected void onPause()
  {
    super.onPause();
    if (this.mTimer != null)
    {
      this.mTimer.cancel();
      this.mTimer.purge();
    }
    this.mTimer = null;
  }

  public void onRatingChanged(RatingBar paramRatingBar, float paramFloat, boolean paramBoolean)
  {
    Logger.d("FlxMain", "RateMoviePage.onRatingChanged rating:" + paramFloat);
    if (paramFloat != 0.0F)
      this.mSelectedWidget = 3;
    if ((this.mRatingBar != null) && (paramBoolean))
    {
      if (this.mReview == null)
        this.mReview = new Review();
      this.mReview.stars = this.mRatingBar.getRating();
      selectionSet();
    }
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    Logger.d("FlxMain", "RateMoviePage.onRestoreInstanceState()");
    if (this.mReview == null)
      this.mReview = new Review();
    this.mReview.comment = paramBundle.getString("net.flixster.RateMoviePage.mReview.comment");
    this.mComment.setText(this.mReview.comment);
    this.mReview.stars = paramBundle.getDouble("net.flixster.RateMoviePage.mReview.stars");
    Logger.d("FlxMain", "RateMoviePage.onRestoreInstanceState() mComment:" + this.mComment.getText().toString() + " mRatingBar:" + this.mRatingBar.getRating() + " mReview.comment:" + this.mReview.comment + " mReview.stars:" + this.mReview.stars);
  }

  protected void onResume()
  {
    super.onResume();
    getExtras();
    if (this.mTimer == null)
      this.mTimer = new Timer();
    if (this.mReview == null)
      ScheduleLoadReviewTask();
    populatePage();
    this.mPromoHandler.sendEmptyMessage(0);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Logger.d("FlxMain", "RateMoviePage.onSaveInstanceState()");
    paramBundle.putString("net.flixster.RateMoviePage.mComment.text", this.mComment.getText().toString());
    paramBundle.putFloat("net.flixster.RateMoviePage.mRatingBar.rating", this.mRatingBar.getRating());
    Logger.d("FlxMain", "RateMoviePage.onSaveInstanceState() mRatingBar.getRating():" + this.mRatingBar.getRating());
    if (this.mReview != null)
    {
      paramBundle.putDouble("net.flixster.RateMoviePage.mReview.stars", this.mReview.stars);
      paramBundle.putString("net.flixster.RateMoviePage.mReview.comment", this.mComment.getText().toString());
    }
    Logger.d("FlxMain", "RateMoviePage.onSaveInstanceState() outState.getFloat:" + paramBundle.getFloat("net.flixster.RateMoviePage.mRatingBar.rating"));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.RateMoviePage
 * JD-Core Version:    0.6.2
 */