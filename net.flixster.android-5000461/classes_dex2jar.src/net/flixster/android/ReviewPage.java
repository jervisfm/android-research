package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;

public class ReviewPage extends FlixsterActivity
  implements View.OnClickListener
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  Movie mMovie;
  long mMovieId;
  public int mMovieType;
  private ReviewPage mReviewPage;
  private ArrayList<Review> mReviewsList;
  private int mStartReviewIndex;
  private Timer mTimer;
  private ViewFlipper mViewFlipper;
  private final Handler movieImageHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      ImageView localImageView = (ImageView)paramAnonymousMessage.obj;
      if (localImageView != null)
      {
        Movie localMovie = (Movie)localImageView.getTag();
        if ((localMovie != null) && (localMovie.thumbnailSoftBitmap.get() != null))
        {
          localImageView.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
          localImageView.invalidate();
        }
      }
    }
  };
  private final Handler postMovieLoadHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (ReviewPage.this.isFinishing());
      do
      {
        return;
        Logger.d("FlxMain", "ReviewPage.postMovieLoadHandler - mReviewsList:" + ReviewPage.this.mReviewsList);
        ReviewPage.this.hideLoading();
        if ((ReviewPage.this.mMovie != null) && (ReviewPage.this.mReviewsList != null))
        {
          ReviewPage.this.populatePage();
          return;
        }
      }
      while (ReviewPage.this.isFinishing());
      ReviewPage.this.showDialog(1);
    }
  };
  private View throbber;

  private void ScheduleLoadMoviesTask()
  {
    TimerTask local3 = new TimerTask()
    {
      public void run()
      {
        if ((ReviewPage.this.mReviewsList == null) || (ReviewPage.this.mReviewsList.isEmpty()));
        try
        {
          Movie localMovie = MovieDao.getMovieDetail(ReviewPage.this.mMovieId);
          if (localMovie != null)
          {
            localMovie.merge(ReviewPage.this.mMovie);
            ReviewPage.this.mMovie = localMovie;
          }
          ReviewPage.this.mReviewsList = new ArrayList();
          if (ReviewPage.this.mMovie.getFriendWantToSeeList() != null)
            ReviewPage.this.mReviewsList.addAll(ReviewPage.this.mMovie.getFriendWantToSeeList());
          if (ReviewPage.this.mMovie.getFriendRatedReviewsList() != null)
            ReviewPage.this.mReviewsList.addAll(ReviewPage.this.mMovie.getFriendRatedReviewsList());
          if (ReviewPage.this.mMovie.getUserReviewsList() != null)
            ReviewPage.this.mReviewsList.addAll(ReviewPage.this.mMovie.getUserReviewsList());
          if (ReviewPage.this.mMovie.getCriticReviewsList() != null)
            ReviewPage.this.mReviewsList.addAll(ReviewPage.this.mMovie.getCriticReviewsList());
          ReviewPage.this.postMovieLoadHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          ReviewPage localReviewPage = ReviewPage.this;
          localReviewPage.mRetryCount = (1 + localReviewPage.mRetryCount);
          if (ReviewPage.this.mRetryCount > 4)
          {
            ReviewPage.this.mRetryCount = 0;
            ReviewPage.this.mShowDialogHandler.sendEmptyMessage(2);
            return;
          }
          ReviewPage.this.ScheduleLoadMoviesTask();
        }
      }
    };
    if (this.mTimer != null)
      this.mTimer.schedule(local3, 100L);
  }

  private String getPageTitle()
  {
    return getString(2131492955) + " - " + (1 + this.mViewFlipper.getDisplayedChild()) + " of " + this.mReviewsList.size();
  }

  private void hideLoading()
  {
    this.throbber.setVisibility(8);
  }

  private void populatePage()
  {
    String[] arrayOfString = { "title", "MOVIE_ACTORS_SHORT", "meta" };
    int[] arrayOfInt = { 2131165726, 2131165727, 2131165728 };
    int i = arrayOfString.length;
    int j = 0;
    ImageView localImageView1;
    if (j >= i)
    {
      localImageView1 = (ImageView)this.mReviewPage.findViewById(2131165725);
      localImageView1.setTag(this.mMovie);
      if (this.mMovie.thumbnailSoftBitmap.get() == null)
        break label269;
      localImageView1.setImageBitmap((Bitmap)this.mMovie.thumbnailSoftBitmap.get());
    }
    LayoutInflater localLayoutInflater;
    Iterator localIterator;
    while (true)
    {
      if ((this.mReviewsList != null) && (!this.mReviewsList.isEmpty()))
      {
        this.mViewFlipper.removeAllViews();
        localLayoutInflater = this.mReviewPage.getLayoutInflater();
        localIterator = this.mReviewsList.iterator();
        if (localIterator.hasNext())
          break label339;
        Logger.d("FlxMain", "ReviewPage.populatePage() mStartReviewIndex:" + this.mStartReviewIndex);
        this.mViewFlipper.setDisplayedChild(this.mStartReviewIndex);
        setActionBarTitle(getPageTitle());
      }
      return;
      TextView localTextView = (TextView)this.mReviewPage.findViewById(arrayOfInt[j]);
      if (this.mMovie.checkProperty(arrayOfString[j]))
      {
        localTextView.setText(this.mMovie.getProperty(arrayOfString[j]));
        localTextView.setVisibility(0);
      }
      while (true)
      {
        j++;
        break;
        localTextView.setVisibility(8);
      }
      label269: String str = this.mMovie.getProperty("thumbnail");
      if ((str != null) && (str.startsWith("http")))
        orderImage(new ImageOrder(0, this.mMovie, this.mMovie.getProperty("thumbnail"), localImageView1, this.movieImageHandler));
      else
        localImageView1.setImageResource(2130837839);
    }
    label339: Review localReview = (Review)localIterator.next();
    ScrollView localScrollView = (ScrollView)localLayoutInflater.inflate(2130903155, this.mViewFlipper, false);
    ((TextView)localScrollView.findViewById(2131165734)).setText(localReview.name);
    ImageView localImageView2 = (ImageView)localScrollView.findViewById(2131165733);
    Bitmap localBitmap = localReview.getReviewerBitmap(localImageView2);
    if (localBitmap != null)
      localImageView2.setImageBitmap(localBitmap);
    ImageView localImageView3 = (ImageView)localScrollView.findViewById(2131165736);
    ImageView localImageView4 = (ImageView)localScrollView.findViewById(2131165737);
    RelativeLayout localRelativeLayout1 = (RelativeLayout)localScrollView.findViewById(2131165732);
    RelativeLayout localRelativeLayout2 = (RelativeLayout)localScrollView.findViewById(2131165738);
    switch (localReview.type)
    {
    default:
    case 0:
    case 1:
    case 2:
    }
    while (true)
    {
      if (localReview.userId > 0L)
      {
        localRelativeLayout1.setTag(localReview);
        localRelativeLayout1.setOnClickListener(this);
      }
      this.mViewFlipper.addView(localScrollView);
      break;
      ((TextView)localScrollView.findViewById(2131165739)).setText(localReview.comment);
      localImageView4.setVisibility(8);
      ((TextView)localScrollView.findViewById(2131165735)).setText(", " + localReview.source);
      if (localReview.url != null)
      {
        localRelativeLayout2.setTag(localReview);
        localRelativeLayout2.setOnClickListener(this);
      }
      if (localReview.score < 60)
      {
        localImageView3.setImageDrawable(getResources().getDrawable(2130837740));
        continue;
        ((TextView)localScrollView.findViewById(2131165739)).setText(localReview.comment);
        localImageView3.setVisibility(8);
        ((TextView)localScrollView.findViewById(2131165735)).setVisibility(8);
        localImageView4.setImageResource(Flixster.RATING_LARGE_R[((int)(2.0D * localReview.stars))]);
        ((TextView)localScrollView.findViewById(2131165740)).setVisibility(8);
        continue;
        ((TextView)localScrollView.findViewById(2131165739)).setText(localReview.comment);
        localImageView3.setVisibility(8);
        ((TextView)localScrollView.findViewById(2131165735)).setVisibility(8);
        localImageView4.setImageResource(Flixster.RATING_LARGE_R[((int)(2.0D * localReview.stars))]);
        ((TextView)localScrollView.findViewById(2131165740)).setVisibility(8);
      }
    }
  }

  private void showLoading()
  {
    this.throbber.setVisibility(0);
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
    case 2131165738:
      Review localReview;
      do
      {
        return;
        localReview = (Review)paramView.getTag();
        Trackers.instance().track("/movie/criticreview", "Critic Review - " + this.mMovie.getProperty("title"));
      }
      while ((localReview == null) || (localReview.url == null) || (!localReview.url.startsWith("http://")));
      startActivity(new Intent("android.intent.action.VIEW", Uri.parse(localReview.url)));
      return;
    case 2131165732:
    }
    String str = String.valueOf(((Review)paramView.getTag()).userId);
    Intent localIntent = new Intent(this, UserProfilePage.class);
    localIntent.putExtra("PLATFORM_ID", str);
    startActivity(localIntent);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903154);
    findViewById(2131165721).setVisibility(8);
    findViewById(2131165723).setVisibility(8);
    findViewById(2131165724).setVisibility(8);
    findViewById(2131165722).setVisibility(8);
    createActionBar();
    setActionBarTitle(2131492955);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.mMovieId = localBundle.getLong("net.flixster.android.EXTRA_MOVIE_ID");
      this.mMovie = MovieDao.getMovie(this.mMovieId);
      this.mStartReviewIndex = localBundle.getInt("REVIEW_INDEX");
      Bitmap localBitmap = (Bitmap)localBundle.getParcelable("MOVIE_THUMBNAIL");
      if (localBitmap != null)
        this.mMovie.thumbnailSoftBitmap = new SoftReference(localBitmap);
    }
    this.mReviewPage = this;
    this.mViewFlipper = ((ViewFlipper)findViewById(2131165730));
    this.throbber = findViewById(2131165241);
    populatePage();
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        ReviewPage.this.ScheduleLoadMoviesTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        ReviewPage.this.hideLoading();
      }
    });
    return localBuilder.create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getSupportMenuInflater().inflate(2131689486, paramMenu);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Logger.d("FlxMain", "ReviewPage.onDestroy()");
    if (this.mTimer != null)
    {
      this.mTimer.cancel();
      this.mTimer.purge();
    }
    this.mTimer = null;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165963:
      this.mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, 2130968584));
      this.mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, 2130968585));
      this.mViewFlipper.showPrevious();
      setActionBarTitle(getPageTitle());
      return true;
    case 2131165964:
    }
    this.mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, 2130968582));
    this.mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, 2130968583));
    this.mViewFlipper.showNext();
    setActionBarTitle(getPageTitle());
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    if (this.mTimer == null)
      this.mTimer = new Timer();
    if ((this.mReviewsList == null) || (this.mReviewsList.isEmpty()))
    {
      showLoading();
      ScheduleLoadMoviesTask();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ReviewPage
 * JD-Core Version:    0.6.2
 */