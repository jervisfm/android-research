package net.flixster.android;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder.DialogListener;
import com.flixster.android.view.RewardView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Property;
import net.flixster.android.model.User;

public class RewardsPage extends DecoratedSherlockActivity
{
  private TextView availableRewardsHeader;
  private LinearLayout availableRewardsLayout;
  private TextView completedRewardsHeader;
  private LinearLayout completedRewardsLayout;
  private final DialogBuilder.DialogListener movieEarnedDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      RewardsPage.this.finish();
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
    }
  };

  private void initializeRewardViews()
  {
    this.availableRewardsLayout.removeAllViews();
    this.completedRewardsLayout.removeAllViews();
    Object localObject1 = null;
    Object localObject2 = null;
    Object localObject3 = null;
    Object localObject4 = null;
    Object localObject5 = null;
    Object localObject6 = null;
    User localUser = AccountFacade.getSocialUser();
    Iterator localIterator = localUser.getLockerRights().iterator();
    RewardView localRewardView1;
    label104: RewardView localRewardView2;
    label151: RewardView localRewardView3;
    label198: RewardView localRewardView4;
    label245: RewardView localRewardView5;
    label281: RewardView localRewardView6;
    label316: int i;
    label343: TextView localTextView2;
    if (!localIterator.hasNext())
    {
      Property localProperty = Properties.instance().getProperties();
      localRewardView1 = new RewardView(this);
      if (!localUser.isMskRateEligible)
        break label612;
      if ((localProperty != null) && (localProperty.isQuickRateRewardEnabled))
        this.availableRewardsLayout.addView(localRewardView1);
      localRewardView1.load(1, (List)localObject1);
      localRewardView2 = new RewardView(this);
      if (!localUser.isMskWtsEligible)
        break label628;
      if ((localProperty != null) && (localProperty.isQuickWtsRewardEnabled))
        this.availableRewardsLayout.addView(localRewardView2);
      localRewardView2.load(2, (List)localObject2);
      localRewardView3 = new RewardView(this);
      if (!localUser.isMskSmsEligible)
        break label644;
      if ((localProperty != null) && (localProperty.isSmsRewardEnabled))
        this.availableRewardsLayout.addView(localRewardView3);
      localRewardView3.load(3, (List)localObject3);
      localRewardView4 = new RewardView(this);
      if (!localUser.isMskFbInviteEligible)
        break label660;
      if ((localProperty != null) && (localProperty.isFbInviteRewardEnabled))
        this.availableRewardsLayout.addView(localRewardView4);
      localRewardView4.load(6, (List)localObject4);
      localRewardView5 = new RewardView(this);
      if (!localUser.isMskUvCreateEligible)
        break label677;
      this.availableRewardsLayout.addView(localRewardView5);
      localRewardView5.load(4, (List)localObject5);
      localRewardView6 = new RewardView(this);
      if (!localUser.isMskInstallMobileEligible)
        break label694;
      this.availableRewardsLayout.addView(localRewardView6);
      localRewardView6.load(5, (List)localObject6);
      TextView localTextView1 = this.availableRewardsHeader;
      if (this.availableRewardsLayout.getChildCount() <= 0)
        break label711;
      i = 0;
      localTextView1.setVisibility(i);
      localTextView2 = this.completedRewardsHeader;
      if (this.completedRewardsLayout.getChildCount() <= 0)
        break label718;
    }
    label644: label660: label677: label694: label711: label718: for (int j = 0; ; j = 8)
    {
      localTextView2.setVisibility(j);
      if (localUser.isMskInstallMobileEligible)
      {
        localUser.isMskInstallMobileEligible = false;
        insertInstallMobileLockerRight();
      }
      return;
      LockerRight localLockerRight = (LockerRight)localIterator.next();
      switch ($SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType()[localLockerRight.purchaseType.ordinal()])
      {
      default:
        break;
      case 1:
        if (localObject1 == null)
          localObject1 = new ArrayList();
        ((List)localObject1).add(localLockerRight);
        break;
      case 2:
        if (localObject2 == null)
          localObject2 = new ArrayList();
        ((List)localObject2).add(localLockerRight);
        break;
      case 3:
        if (localObject3 == null)
          localObject3 = new ArrayList();
        ((List)localObject3).add(localLockerRight);
        break;
      case 4:
        if (localObject4 == null)
          localObject4 = new ArrayList();
        ((List)localObject4).add(localLockerRight);
        break;
      case 5:
        if (localObject5 == null)
          localObject5 = new ArrayList();
        ((List)localObject5).add(localLockerRight);
        break;
      case 6:
        if (localObject6 == null)
          localObject6 = new ArrayList();
        ((List)localObject6).add(localLockerRight);
        break;
        label612: if (localObject1 == null)
          break label104;
        this.completedRewardsLayout.addView(localRewardView1);
        break label104;
        label628: if (localObject2 == null)
          break label151;
        this.completedRewardsLayout.addView(localRewardView2);
        break label151;
        if (localObject3 == null)
          break label198;
        this.completedRewardsLayout.addView(localRewardView3);
        break label198;
        if (localObject4 == null)
          break label245;
        this.completedRewardsLayout.addView(localRewardView4);
        break label245;
        if (localObject5 == null)
          break label281;
        this.completedRewardsLayout.addView(localRewardView5);
        break label281;
        if (localObject6 == null)
          break label316;
        this.completedRewardsLayout.addView(localRewardView6);
        break label316;
        i = 8;
        break label343;
      }
    }
  }

  private void insertInstallMobileLockerRight()
  {
    Trackers.instance().trackEvent("/rewards", null, "FlixsterRewards", "CompletedReward", "MobileAppInstall", 0);
    ProfileDao.insertLockerRight(null, null, "MPN");
    showDialog(1000000401, this.movieEarnedDialogListener);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903157);
    createActionBar();
    setActionBarTitle(2131493190);
    this.availableRewardsHeader = ((TextView)findViewById(2131165743));
    this.completedRewardsHeader = ((TextView)findViewById(2131165745));
    this.availableRewardsLayout = ((LinearLayout)findViewById(2131165744));
    this.completedRewardsLayout = ((LinearLayout)findViewById(2131165746));
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    initializeRewardViews();
    Trackers.instance().track("/rewards/rewards-center", "Rewards - RewardsCenter");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.RewardsPage
 * JD-Core Version:    0.6.2
 */