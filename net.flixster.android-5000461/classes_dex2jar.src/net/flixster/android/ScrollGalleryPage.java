package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.Gallery.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.PhotoDao;
import net.flixster.android.model.Actor;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Photo;
import net.flixster.android.util.MediaHelper;

public class ScrollGalleryPage extends FlixsterActivity
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  public static final String KEY_GENERIC_GALLERY = "KEY_GENERIC_GALLERY";
  public static final String KEY_GENERIC_PHOTOS = "KEY_GENERIC_PHOTOS";
  public static final String KEY_PHOTO_COUNT = "PHOTO_COUNT";
  public static final String KEY_PHOTO_INDEX = "PHOTO_INDEX";
  public static final String KEY_TITLE = "KEY_TITLE";
  public static final long PHOTO_VIEWTIME = 1000L;
  private static final int SAVE_PHOTO = 3;
  private static final int SAVE_PHOTO_FAILED = 4;
  public static final int TYPE_ACTOR = 2;
  public static final int TYPE_GENERIC = 3;
  public static final int TYPE_MOVIE = 1;
  public static final int TYPE_TOP;
  long mActorId;
  TextView mCaption;
  Context mContext;
  private AdView mDefaultAd;
  String mFilter;
  Gallery mGallery;
  private long mLastPhotoTime = 0L;
  ImageView mLoadingImage;
  long mMovieId;
  private int mPhotoIndex = -1;
  RelativeLayout mPhotoLayout;
  private List<Photo> mPhotos;
  private int mStartPhotoIndex;
  RelativeLayout mTopLayout;
  private int mType = 0;
  private String originalHeaderText;
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "MovieGalleryPage.updateHandler");
      if (ScrollGalleryPage.this.mPhotos.size() > 0)
        ScrollGalleryPage.this.updatePage();
      while (ScrollGalleryPage.this.isFinishing())
        return;
      ScrollGalleryPage.this.showDialog(1);
    }
  };

  private void scheduleUpdatePageTask()
  {
    TimerTask local3 = new TimerTask()
    {
      public void run()
      {
        try
        {
          int i = ScrollGalleryPage.this.mType;
          switch (i)
          {
          default:
          case 0:
            while (true)
            {
              ScrollGalleryPage.this.updateHandler.sendEmptyMessage(0);
              return;
              ScrollGalleryPage.this.mPhotos.clear();
              ScrollGalleryPage.this.mPhotos.addAll(PhotoDao.getTopPhotos(ScrollGalleryPage.this.mFilter));
            }
          case 1:
          case 2:
          }
        }
        catch (DaoException localDaoException)
        {
          while (true)
          {
            Logger.e("FlxMain", "ScrollGalleryPage.scheduleUpdatePageTask:failed to get photos", localDaoException);
            continue;
            ScrollGalleryPage.this.mPhotos.clear();
            ScrollGalleryPage.this.mPhotos.addAll(PhotoDao.getMoviePhotos(ScrollGalleryPage.this.mMovieId));
            continue;
            ScrollGalleryPage.this.mPhotos.clear();
            ScrollGalleryPage.this.mPhotos.addAll(PhotoDao.getActorPhotos(ScrollGalleryPage.this.mActorId));
          }
        }
      }
    };
    if (this.mPageTimer != null)
      this.mPageTimer.schedule(local3, 100L);
  }

  private void updatePage()
  {
    if (this.mPhotos.size() > 0)
    {
      if (this.mPhotoIndex == -1)
        this.mPhotoIndex = this.mStartPhotoIndex;
      this.mGallery.setAdapter(new ImageAdapter(this, this.mPhotos));
      this.mGallery.setOnItemClickListener(new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          if (ScrollGalleryPage.this.mTopLayout.getVisibility() == 0)
          {
            ScrollGalleryPage.this.mTopLayout.setVisibility(8);
            ScrollGalleryPage.this.mCaption.setVisibility(8);
            ScrollGalleryPage.this.hideActionBar();
            ScrollGalleryPage.this.mGallery.invalidate();
            return;
          }
          ScrollGalleryPage.this.mTopLayout.setVisibility(0);
          ScrollGalleryPage.this.mCaption.setVisibility(0);
          ScrollGalleryPage.this.showActionBar();
          ScrollGalleryPage.this.mGallery.postInvalidate();
          ScrollGalleryPage.this.mGallery.invalidate();
        }
      });
      this.mGallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
      {
        public void onItemSelected(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          ScrollGalleryPage.this.mPhotoIndex = paramAnonymousInt;
          if (ScrollGalleryPage.this.mPhotos.size() > paramAnonymousInt)
          {
            Photo localPhoto = (Photo)ScrollGalleryPage.this.mPhotos.get(paramAnonymousInt);
            StringBuilder localStringBuilder = new StringBuilder();
            if ((localPhoto.caption != null) && (localPhoto.caption.length() > 1))
              localStringBuilder.append(localPhoto.caption);
            if ((localPhoto.description != null) && (localPhoto.description.length() > 1))
            {
              if (localStringBuilder.length() > 0)
                localStringBuilder.append(" - ");
              localStringBuilder.append(localPhoto.description);
            }
            if (localStringBuilder.length() <= 0)
              break label262;
            ScrollGalleryPage.this.mCaption.setText(localStringBuilder.toString());
          }
          while (true)
          {
            long l = System.currentTimeMillis();
            if (l - ScrollGalleryPage.this.mLastPhotoTime > 1000L)
              Trackers.instance().track("/photo/view", "Photo View ");
            ScrollGalleryPage.this.mLastPhotoTime = l;
            ScrollGalleryPage.this.setActionBarTitle(ScrollGalleryPage.this.getString(2131492925) + " - " + (1 + ScrollGalleryPage.this.mPhotoIndex) + " of " + ScrollGalleryPage.this.mPhotos.size());
            return;
            label262: ScrollGalleryPage.this.mCaption.setText("");
          }
        }

        public void onNothingSelected(AdapterView<?> paramAnonymousAdapterView)
        {
        }
      });
      this.mGallery.setSelection(this.mPhotoIndex);
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mContext = this;
    setContentView(2130903090);
    findViewById(2131165355).setVisibility(8);
    findViewById(2131165356).setVisibility(8);
    createActionBar();
    setActionBarTitle(2131492925);
    this.mCaption = ((TextView)findViewById(2131165359));
    this.mTopLayout = ((RelativeLayout)findViewById(2131165354));
    this.mPhotoLayout = ((RelativeLayout)findViewById(2131165357));
    this.mGallery = ((Gallery)findViewById(2131165358));
    this.mDefaultAd = ((AdView)findViewById(2131165226));
    this.mTopLayout.setVisibility(8);
    this.mCaption.setVisibility(8);
    hideActionBar();
    Bundle localBundle = getIntent().getExtras();
    Logger.d("FlxMain", "ScrollGalleryPage.onCreate");
    Movie localMovie;
    String[] arrayOfString;
    int j;
    if (localBundle != null)
    {
      this.mStartPhotoIndex = localBundle.getInt("PHOTO_INDEX");
      if (this.mStartPhotoIndex < 0)
        this.mStartPhotoIndex = 0;
      if (!localBundle.containsKey("net.flixster.android.EXTRA_MOVIE_ID"))
        break label338;
      this.mType = 1;
      this.mMovieId = localBundle.getLong("net.flixster.android.EXTRA_MOVIE_ID");
      localMovie = MovieDao.getMovie(this.mMovieId);
      arrayOfString = new String[] { "title" };
      int i = arrayOfString.length;
      j = 0;
      if (j < i)
        break label303;
      this.originalHeaderText = localMovie.getProperty("title");
      if (localMovie.mPhotos == null)
        localMovie.mPhotos = new ArrayList();
      this.mPhotos = localMovie.mPhotos;
    }
    while (true)
    {
      Logger.d("FlxMain", "ScrollGalleryPage.onCreate mType:" + this.mType);
      return;
      label303: String str1 = arrayOfString[j];
      String str2 = localBundle.getString(str1);
      if (str2 != null)
        localMovie.setProperty(str1, str2);
      j++;
      break;
      label338: if (localBundle.containsKey("ACTOR_ID"))
      {
        this.mType = 2;
        Actor localActor = new Actor();
        this.mActorId = localBundle.getLong("ACTOR_ID");
        localActor.id = this.mActorId;
        localActor.name = localBundle.getString("ACTOR_NAME");
        this.originalHeaderText = localActor.name;
        localActor.photos = new ArrayList();
        this.mPhotos = localActor.photos;
      }
      else if (localBundle.containsKey("KEY_GENERIC_GALLERY"))
      {
        this.mType = 3;
        this.mPhotos = ((List)ObjectHolder.instance().get("KEY_PHOTOS"));
        this.originalHeaderText = localBundle.getString("KEY_TITLE");
      }
      else
      {
        this.mPhotos = new ArrayList();
        this.mFilter = localBundle.getString("KEY_PHOTO_FILTER");
        if ((this.mFilter == null) || (this.mFilter.length() <= 0))
          this.mFilter = "random";
        this.originalHeaderText = "Top Photos";
      }
    }
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    case 2:
    default:
      return super.onCreateDialog(paramInt);
    case 1:
      AlertDialog.Builder localBuilder3 = new AlertDialog.Builder(this);
      localBuilder3.setMessage("The network connection failed. Press Retry to make another attempt.");
      localBuilder3.setTitle("Network Error");
      localBuilder3.setCancelable(false);
      localBuilder3.setPositiveButton("Retry", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          ScrollGalleryPage.this.scheduleUpdatePageTask();
        }
      });
      localBuilder3.setNegativeButton(getResources().getString(2131492938), null);
      return localBuilder3.create();
    case 3:
      AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(this);
      localBuilder2.setMessage("The image was saved to your gallery.");
      localBuilder2.setTitle("Image Saved");
      localBuilder2.setCancelable(true);
      localBuilder2.setNegativeButton("OK", null);
      return localBuilder2.create();
    case 4:
    }
    AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(this);
    localBuilder1.setMessage("We're sorry, we were unable to save the image.");
    localBuilder1.setTitle("Save Image Error");
    localBuilder1.setCancelable(true);
    localBuilder1.setNegativeButton("OK", null);
    return localBuilder1.create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getSupportMenuInflater().inflate(2131689487, paramMenu);
    return true;
  }

  public void onDestroy()
  {
    this.mDefaultAd.destroy();
    super.onDestroy();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (this.mPhotos.size() > this.mPhotoIndex)
    {
      Photo localPhoto = (Photo)this.mPhotos.get(this.mPhotoIndex);
      switch (paramMenuItem.getItemId())
      {
      default:
        return super.onOptionsItemSelected(paramMenuItem);
      case 2131165950:
        Intent localIntent2 = new Intent("BOXOFFICE", null, this, Flixster.class);
        localIntent2.setFlags(67108864);
        startActivity(localIntent2);
        return true;
      case 2131165961:
        Intent localIntent1 = new Intent("DETAILS", null, getApplicationContext(), MovieDetails.class);
        localIntent1.putExtra("net.flixster.android.EXTRA_MOVIE_ID", localPhoto.movieId);
        localIntent1.putExtra("title", localPhoto.movieTitle);
        startActivity(localIntent1);
        return true;
      case 2131165962:
      }
      Trackers.instance().track("/photo/save", "galleryUrl:" + localPhoto.galleryUrl);
      if (MediaHelper.savePhoto(localPhoto, this))
      {
        showDialog(3);
        return true;
      }
      showDialog(4);
      return true;
    }
    showDialog(1);
    return true;
  }

  protected void onPause()
  {
    super.onPause();
    if (this.mType != 3)
      this.mPhotos.clear();
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    this.mPhotoIndex = paramBundle.getInt("PHOTO_INDEX");
  }

  protected void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "ScrollGalleryPage.onResume");
    switch (this.mType)
    {
    default:
      Trackers.instance().track("/photo/fullscreen/top_photos", "ScrollGalleryPage");
    case 1:
    case 2:
    }
    while (true)
    {
      scheduleUpdatePageTask();
      this.mDefaultAd.refreshAds();
      return;
      Trackers.instance().track("/photo/fullscreen/movie", "ScrollGalleryPage:" + this.originalHeaderText);
      continue;
      Trackers.instance().track("/photo/fullscreen/actor", "ScrollGalleryPage:" + this.originalHeaderText);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putInt("PHOTO_INDEX", this.mPhotoIndex);
  }

  public class ImageAdapter extends BaseAdapter
  {
    private Context mContext;
    int mDeviceHeight;
    int mDeviceWidth;
    int mGalleryItemBackground;
    List<Photo> mPhotos;
    private Handler photoHandler = new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        ImageView localImageView = (ImageView)paramAnonymousMessage.obj;
        if (localImageView != null)
        {
          Photo localPhoto = (Photo)localImageView.getTag();
          if (localPhoto != null)
          {
            Bitmap localBitmap = (Bitmap)localPhoto.galleryBitmap.get();
            if (localBitmap != null)
            {
              localImageView.setImageBitmap(localBitmap);
              localImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
              localImageView.invalidate();
            }
          }
        }
      }
    };

    public ImageAdapter(List<Photo> arg2)
    {
      Object localObject1;
      this.mContext = localObject1;
      Object localObject2;
      this.mPhotos = localObject2;
      Display localDisplay = ScrollGalleryPage.this.getWindowManager().getDefaultDisplay();
      this.mDeviceWidth = localDisplay.getWidth();
      this.mDeviceHeight = localDisplay.getHeight();
      TypedArray localTypedArray = ScrollGalleryPage.this.obtainStyledAttributes(R.styleable.Gallery1);
      this.mGalleryItemBackground = localTypedArray.getResourceId(0, 0);
      localTypedArray.recycle();
      int i = this.mPhotos.size();
      for (int j = Math.max(ScrollGalleryPage.this.mStartPhotoIndex, i - 1 - ScrollGalleryPage.this.mStartPhotoIndex); ; j--)
      {
        if (j < 0)
          return;
        int k = ScrollGalleryPage.this.mStartPhotoIndex - j;
        int m = j + ScrollGalleryPage.this.mStartPhotoIndex;
        if ((k >= 0) && (j != 0))
        {
          Photo localPhoto2 = (Photo)this.mPhotos.get(k);
          ImageOrder localImageOrder2 = new ImageOrder(3, localPhoto2, localPhoto2.thumbnailUrl, null, null);
          ((FlixsterActivity)this.mContext).orderImage(localImageOrder2);
        }
        if (m < i)
        {
          Photo localPhoto1 = (Photo)this.mPhotos.get(m);
          ImageOrder localImageOrder1 = new ImageOrder(3, localPhoto1, localPhoto1.thumbnailUrl, null, null);
          ((FlixsterActivity)this.mContext).orderImage(localImageOrder1);
        }
      }
    }

    public int getCount()
    {
      return this.mPhotos.size();
    }

    public Object getItem(int paramInt)
    {
      return Integer.valueOf(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return paramInt;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = new ImageView(this.mContext);
      ((ImageView)paramView).setLayoutParams(new Gallery.LayoutParams(-1, -1));
      while (true)
      {
        try
        {
          Photo localPhoto = (Photo)this.mPhotos.get(paramInt);
          Bitmap localBitmap = (Bitmap)localPhoto.galleryBitmap.get();
          if (localBitmap != null)
          {
            ((ImageView)paramView).setImageBitmap(localBitmap);
            ((ImageView)paramView).setScaleType(ImageView.ScaleType.FIT_CENTER);
            return paramView;
          }
          if (localPhoto.bitmap != null)
          {
            ((ImageView)paramView).setImageBitmap(localPhoto.bitmap);
            paramView.setTag(localPhoto);
            String str = localPhoto.galleryUrl;
            Handler localHandler = this.photoHandler;
            ImageOrder localImageOrder = new ImageOrder(8, localPhoto, str, paramView, localHandler);
            Logger.d("FlxMain", "ScrollPhotoGalleryPage url:" + localPhoto.galleryUrl);
            ((FlixsterActivity)this.mContext).orderImage(localImageOrder);
            return paramView;
          }
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
          Logger.w("FlxMain", "ScrollGalleryPage.getView (mPhotos was cleared)");
          return paramView;
        }
        ((ImageView)paramView).setImageResource(2130837812);
        ((ImageView)paramView).setScaleType(ImageView.ScaleType.CENTER);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ScrollGalleryPage
 * JD-Core Version:    0.6.2
 */