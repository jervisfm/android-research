package net.flixster.android;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.ListHelper;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.SubNavBar;
import com.flixster.android.widget.WidgetProvider;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.ActorDao;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.lvi.LviActor;
import net.flixster.android.lvi.LviAd;
import net.flixster.android.lvi.LviFooter;
import net.flixster.android.lvi.LviMessagePanel;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.model.Actor;
import net.flixster.android.model.Movie;

public class SearchPage extends LviActivity
  implements TextView.OnEditorActionListener, View.OnClickListener
{
  private static final int ACTOR_SEARCH = 1;
  private static final int MOVIE_SEARCH;
  private static final ArrayList<Actor> sActorResults = new ArrayList();
  private static final ArrayList<Movie> sMovieResults = new ArrayList();
  private static int sSearchType;
  private View.OnClickListener actorClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Actor localActor = (Actor)paramAnonymousView.getTag();
      if (localActor != null)
      {
        Intent localIntent = new Intent("TOP_ACTOR", null, SearchPage.this, ActorPage.class);
        localIntent.putExtra("ACTOR_ID", localActor.id);
        localIntent.putExtra("ACTOR_NAME", localActor.name);
        SearchPage.this.startActivity(localIntent);
      }
    }
  };
  private String mQueryString;
  private SubNavBar navBar;
  private EditText searchField;
  private View.OnClickListener searchTypeListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      switch (paramAnonymousView.getId())
      {
      default:
      case 2131165784:
      case 2131165785:
      }
      while (true)
      {
        SearchPage.this.updateSearchFieldHint();
        SearchPage.this.ScheduleLoadItemsTask(10L);
        return;
        SearchPage.sSearchType = 0;
        continue;
        SearchPage.sSearchType = 1;
      }
    }
  };

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      TimerTask local4 = new TimerTask()
      {
        public void run()
        {
          label268: Tracker localTracker1;
          StringBuilder localStringBuilder1;
          while (true)
          {
            try
            {
              switch (SearchPage.sSearchType)
              {
              default:
                SearchPage.this.mUpdateHandler.sendEmptyMessage(0);
                return;
              case 0:
                if ((SearchPage.sMovieResults.isEmpty()) && (SearchPage.this.mQueryString != null))
                {
                  SearchPage.this.throbberHandler.sendEmptyMessage(1);
                  MovieDao.searchMovies(SearchPage.this.mQueryString, SearchPage.sMovieResults);
                }
                boolean bool = SearchPage.this.shouldSkipBackgroundTask(this.val$currResumeCtr);
                if (bool)
                  return;
                if (!SearchPage.sMovieResults.isEmpty())
                {
                  Tracker localTracker2 = Trackers.instance();
                  StringBuilder localStringBuilder2 = new StringBuilder("Search Movie - ");
                  if (SearchPage.this.mQueryString != null)
                  {
                    str2 = SearchPage.this.mQueryString;
                    localTracker2.track("/search/movie/results", str2);
                    SearchPage.this.setMovieLviList();
                    continue;
                  }
                }
                break;
              case 1:
              }
            }
            catch (DaoException localDaoException)
            {
              Logger.w("FlxMain", "SearchPage.ScheduleLoadItemsTask.run() mRetryCount:" + SearchPage.this.mRetryCount);
              localDaoException.printStackTrace();
              SearchPage localSearchPage = SearchPage.this;
              localSearchPage.mRetryCount = (1 + localSearchPage.mRetryCount);
              if (SearchPage.this.mRetryCount >= 3)
                break label464;
              SearchPage.this.ScheduleLoadItemsTask(1000L);
              return;
              String str2 = "Results";
              continue;
              Trackers.instance().track("/search", "Search");
              continue;
            }
            finally
            {
              SearchPage.this.throbberHandler.sendEmptyMessage(0);
            }
            if ((SearchPage.sActorResults.isEmpty()) && (SearchPage.this.mQueryString != null))
            {
              SearchPage.this.throbberHandler.sendEmptyMessage(1);
              ActorDao.searchActors(SearchPage.this.mQueryString, SearchPage.sActorResults);
            }
            if (!SearchPage.this.shouldSkipBackgroundTask(this.val$currResumeCtr))
              if (!SearchPage.sActorResults.isEmpty())
              {
                localTracker1 = Trackers.instance();
                localStringBuilder1 = new StringBuilder("Search Actor - ");
                if (SearchPage.this.mQueryString == null)
                  break label487;
              }
          }
          label464: label487: for (String str1 = SearchPage.this.mQueryString; ; str1 = "Results")
          {
            localTracker1.track("/search/actor/results", str1);
            while (true)
            {
              SearchPage.this.setActorResultsLviList();
              break;
              Trackers.instance().track("/search", "Search");
            }
            SearchPage.this.mRetryCount = 0;
            SearchPage.this.mShowDialogHandler.sendEmptyMessage(2);
            break label268;
          }
        }
      };
      Logger.d("FlxMain", "SearchPage.ScheduleLoadItemsTask() loading item");
      if (this.mPageTimer != null)
        this.mPageTimer.schedule(local4, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void addAd()
  {
    destroyExistingLviAd();
    this.lviAd = new LviAd();
    this.lviAd.mAdSlot = "SearchResults";
    this.mDataHolder.add(this.lviAd);
  }

  private void addNoResults()
  {
    LviMessagePanel localLviMessagePanel = new LviMessagePanel();
    localLviMessagePanel.mMessage = getResources().getString(2131493130);
    this.mDataHolder.add(localLviMessagePanel);
  }

  private void hideSoftKeyboard(View paramView)
  {
    ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(paramView.getWindowToken(), 0);
  }

  private void setActorResultsLviList()
  {
    Logger.d("FlxMain", "SearchPage.setActorResultsLviList ");
    this.mDataHolder.clear();
    addAd();
    ArrayList localArrayList = ListHelper.clone(sActorResults);
    Iterator localIterator = localArrayList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        if ((localArrayList.isEmpty()) && (this.mQueryString != null))
          addNoResults();
        this.mQueryString = null;
        return;
      }
      Actor localActor = (Actor)localIterator.next();
      LviActor localLviActor = new LviActor();
      localLviActor.mActor = localActor;
      localLviActor.mActorClick = this.actorClickListener;
      this.mDataHolder.add(localLviActor);
    }
  }

  private void setMovieLviList()
  {
    Logger.d("FlxMain", "SearchPage.setSearchLviList ");
    this.mDataHolder.clear();
    addAd();
    ArrayList localArrayList = ListHelper.clone(sMovieResults);
    Iterator localIterator;
    if (localArrayList.size() > 0)
    {
      localIterator = localArrayList.iterator();
      if (!localIterator.hasNext())
        this.mDataHolder.add(new LviFooter());
    }
    while (true)
    {
      this.mQueryString = null;
      return;
      Movie localMovie = (Movie)localIterator.next();
      LviMovie localLviMovie = new LviMovie();
      localLviMovie.mMovie = localMovie;
      localLviMovie.mTrailerClick = getTrailerOnClickListener();
      this.mDataHolder.add(localLviMovie);
      break;
      if (this.mQueryString != null)
        addNoResults();
    }
  }

  private void shadeNavButtons()
  {
    if (sSearchType == 0)
      this.navBar.setSelectedButton(2131165784);
    while (sSearchType != 1)
      return;
    this.navBar.setSelectedButton(2131165785);
  }

  private void showSoftKeyboard()
  {
    getWindow().setSoftInputMode(4);
  }

  private void updateSearchFieldHint()
  {
    EditText localEditText = this.searchField;
    if (sSearchType == 0);
    for (int i = 2131493126; ; i = 2131493127)
    {
      localEditText.setHint(getResourceString(i));
      return;
    }
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
      return;
    case 2131165441:
    }
    Logger.d("FlxMain", "SearchPage.onClick ok then");
    ((EditText)paramView.getTag()).dispatchKeyEvent(new KeyEvent(0, 66));
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    createActionBar();
    setActionBarTitle(2131492936);
    this.mListView.setOnItemClickListener(getMovieItemClickListener());
    this.mStickyTopAd.setSlot("SearchResultsStickyTop");
    this.mStickyBottomAd.setSlot("SearchResultsStickyBottom");
    LinearLayout localLinearLayout = (LinearLayout)findViewById(2131165427);
    this.navBar = new SubNavBar(this);
    this.navBar.load(this.searchTypeListener, 2131493124, 2131493125);
    this.navBar.setSelectedButton(2131165784);
    localLinearLayout.addView(this.navBar, 0);
    RelativeLayout localRelativeLayout = (RelativeLayout)LayoutInflater.from(this).inflate(2130903115, null);
    this.searchField = ((EditText)localRelativeLayout.findViewById(2131165442));
    this.searchField.setOnEditorActionListener(this);
    this.searchField.setOnFocusChangeListener(new View.OnFocusChangeListener()
    {
      public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean)
          SearchPage.this.showSoftKeyboard();
      }
    });
    this.searchField.requestFocus();
    updateSearchFieldHint();
    ImageButton localImageButton = (ImageButton)localRelativeLayout.findViewById(2131165441);
    localImageButton.setTag(this.searchField);
    localImageButton.setOnClickListener(this);
    localLinearLayout.addView(localRelativeLayout);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent != null) && (paramKeyEvent.getAction() == 0))
    {
      Logger.d("FlxMain", "SearchPage.onEditorAction ok (from action)");
      this.mQueryString = ((EditText)paramTextView).getText().toString();
      hideSoftKeyboard(paramTextView);
      if (("FLX".equalsIgnoreCase(this.mQueryString)) || ("REINDEER FLOTILLA".equalsIgnoreCase(this.mQueryString)) || ("UUDDLRLRBA".equalsIgnoreCase(this.mQueryString)))
      {
        FlixsterApplication.setAdminState(1);
        Toast.makeText(this, 2131493307, 0).show();
      }
      if ("DGNS".equalsIgnoreCase(this.mQueryString))
      {
        FlixsterApplication.enableDiagnosticMode();
        Toast.makeText(this, 2131493308, 0).show();
      }
      switch (sSearchType)
      {
      default:
      case 0:
      case 1:
      }
    }
    while (true)
    {
      ScheduleLoadItemsTask(10L);
      return false;
      sMovieResults.clear();
      continue;
      sActorResults.clear();
    }
  }

  public void onResume()
  {
    super.onResume();
    shadeNavButtons();
    if (WidgetProvider.isOriginatedFromWidget(getIntent()))
      Trackers.instance().trackEvent("/search", "Search", "WidgetEntrance", "Search");
    ScheduleLoadItemsTask(100L);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.SearchPage
 * JD-Core Version:    0.6.2
 */