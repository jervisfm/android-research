package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.facebook.android.AsyncFacebookRunner;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.data.AccountManager;
import com.flixster.android.utils.GoogleApiDetector;
import com.flixster.android.utils.LocationFacade;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.model.User;
import net.flixster.android.util.PostalCodeUtils;

public class SettingsPage extends DecoratedSherlockActivity
  implements View.OnClickListener
{
  private static final int ACTIVITY_RESULT_LOCATION = 0;
  private static final int DIALOG_CACHEPOLICY = 7;
  private static final int DIALOG_CACHE_POLICY_CHANGE = 9;
  private static final int DIALOG_CAPTIONSENABLED = 13;
  private static final int DIALOG_DISTANCE = 2;
  private static final int DIALOG_LOCATION = 1;
  private static final int DIALOG_LOCATION_POLICY_CHANGE = 8;
  private static final int DIALOG_LOGOUT_FACEBOOK = 10;
  private static final int DIALOG_LOGOUT_FLIXSTER = 11;
  private static final int DIALOG_LOGOUT_NETFLIX = 12;
  private static final int DIALOG_MOVIERATINGS = 3;
  private static final int DIALOG_ZIP = 0;
  private static final int DIALOG_ZIP_ERROR = 5;
  public static final boolean IS_CURRENT_LOCATION_DISABLED = false;
  public static final int LISTITEM_FACEBOOK = 4;
  public static final int LISTITEM_FAQ = 5;
  public static final int LISTITEM_FEEDBACK = 6;
  public static final int LISTITEM_FOOTER = 7;
  public static final int LISTITEM_HEADER = 0;
  public static final int LISTITEM_LOCATION = 1;
  public static final int LISTITEM_MOVIERATINGS = 3;
  public static final int LISTITEM_TRAILERZOOM = 2;
  private static final double[] SEARCH_RADIUS_IMPERIAL = { 3.0D, 5.0D, 10.0D, 25.0D, 35.0D, 50.0D, 75.0D, 100.0D, 200.0D };
  private static final double[] SEARCH_RADIUS_METRIC = { 1.24D, 3.11D, 6.22D, 9.32D, 24.859999999999999D, 34.18D, 49.719999999999999D, 62.149999999999999D, 93.230000000000004D, 186.44999999999999D };
  private static SettingsPage mSettingsPage;
  private Handler getCurrentLocationHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      String str = (String)paramAnonymousMessage.obj;
      TextView localTextView = (TextView)SettingsPage.this.findViewById(2131165755);
      localTextView.setText(str);
      if (str.contentEquals(SettingsPage.this.getResources().getString(2131493083)))
      {
        localTextView.setTextColor(-65536);
        return;
      }
      localTextView.setTextColor(-16777216);
    }
  };
  private final View.OnClickListener homepageOnClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      switch (paramAnonymousView.getId())
      {
      default:
        return;
      case 2131165663:
        if (AccountFacade.getNetflixUserId() == null)
        {
          Intent localIntent3 = new Intent(SettingsPage.this, NetflixAuth.class);
          SettingsPage.this.startActivity(localIntent3);
          return;
        }
        SettingsPage.this.showDialog(12);
        return;
      case 2131165653:
        if (AccountFacade.getSocialUserId() == null)
        {
          Intent localIntent2 = new Intent(SettingsPage.this, FacebookAuth.class);
          SettingsPage.this.startActivity(localIntent2);
          return;
        }
        SettingsPage.this.showDialog(10);
        return;
      case 2131165658:
      }
      if (AccountFacade.getSocialUserId() == null)
      {
        Intent localIntent1 = new Intent(SettingsPage.this, FlixsterLoginPage.class);
        SettingsPage.this.startActivity(localIntent1);
        return;
      }
      SettingsPage.this.showDialog(11);
    }
  };
  AsyncFacebookRunner mAsyncRunner;
  private Handler mCacheDialogRemove = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "SettingsPage.mCacheDialogRemove mCachePolicyDialog:" + SettingsPage.this.mCachePolicyDialog);
      if ((SettingsPage.this.mCachePolicyDialog != null) && (SettingsPage.this.mCachePolicyDialog.isShowing()))
      {
        SettingsPage.this.mCachePolicyDialog = null;
        SettingsPage.this.removeDialog(9);
      }
    }
  };
  private ProgressDialog mCachePolicyDialog;
  private Handler mLocationDialogRemove = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "SettingsPage.mLocationDialogRemove mLocationPolicyDialog:" + SettingsPage.this.mLocationPolicyDialog);
      if ((SettingsPage.this.mLocationPolicyDialog != null) && (SettingsPage.this.mLocationPolicyDialog.isShowing()))
      {
        SettingsPage.this.mLocationPolicyDialog = null;
        SettingsPage.this.removeDialog(8);
      }
    }
  };
  private ProgressDialog mLocationPolicyDialog;
  private Handler mRefreshHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      SettingsPage.this.populatePage();
    }
  };
  private Handler mSetupLocationHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      FlixsterApplication.setupLocationServices();
    }
  };
  private Timer mTimer;
  private TextView movieRatingsLabel;
  private TextView theaterDistanceLabel;

  private void ScheduleCacheChange(final int paramInt)
  {
    TimerTask local20 = new TimerTask()
    {
      public void run()
      {
        FlixsterApplication.setCachePolicy(paramInt);
        FlixsterCacheManager.SetCacheLimit();
        FlixsterCacheManager.TrimCache(0L);
        SettingsPage.mSettingsPage.mRefreshHandler.sendEmptyMessage(0);
        SettingsPage.this.mCacheDialogRemove.sendEmptyMessage(0);
      }
    };
    if (this.mTimer != null)
      this.mTimer.schedule(local20, 100L);
  }

  private void ScheduleLocationChange(final int paramInt)
  {
    TimerTask local19 = new TimerTask()
    {
      public void run()
      {
        Logger.d("FlxMain", "SettingsPage.ScheduleLocationChange");
        FlixsterApplication.setLocationPolicy(paramInt);
        Logger.d("FlxMain", "SettingsPage.ScheduleLocationChange post which:" + paramInt);
        switch (paramInt)
        {
        default:
        case 0:
        case 1:
        case 2:
        }
        while (true)
        {
          Logger.d("FlxMain", "SettingsPage.ScheduleLocationChange call SetupLocationServices");
          SettingsPage.this.mSetupLocationHandler.sendEmptyMessage(0);
          Logger.d("FlxMain", "SettingsPage.ScheduleLocationChange ta da...");
          SettingsPage.this.mLocationDialogRemove.sendEmptyMessage(0);
          SettingsPage.this.mRefreshHandler.sendEmptyMessage(0);
          return;
          if (FlixsterApplication.getUseLocationServiceFlag())
          {
            double d1 = FlixsterApplication.getCurrentLatitude();
            double d2 = FlixsterApplication.getCurrentLongitude();
            if ((d1 != 0.0D) && (d2 != 0.0D))
            {
              FlixsterApplication.setUserLatitude(d1);
              FlixsterApplication.setUserLongitude(d2);
              FlixsterApplication.setUserZip(FlixsterApplication.getCurrentZip());
              FlixsterApplication.setUserCity(FlixsterApplication.getCurrentCity());
              FlixsterApplication.setUserLocation(FlixsterApplication.getCurrentLocationDisplay());
            }
            FlixsterApplication.setUseLocationServiceFlag(false);
            continue;
            if (!FlixsterApplication.getUseLocationServiceFlag())
            {
              FlixsterApplication.setUseLocationServiceFlag(true);
              continue;
              if (!FlixsterApplication.getUseLocationServiceFlag())
                FlixsterApplication.setUseLocationServiceFlag(true);
            }
          }
        }
      }
    };
    if (this.mTimer != null)
      this.mTimer.schedule(local19, 100L);
  }

  private void initializeNetflixViews()
  {
    RelativeLayout localRelativeLayout = (RelativeLayout)findViewById(2131165663);
    if ((LocationFacade.isUsLocale()) || (LocationFacade.isCanadaLocale()))
    {
      localRelativeLayout.setOnClickListener(this.homepageOnClickListener);
      localRelativeLayout.setFocusable(true);
      TextView localTextView = (TextView)findViewById(2131165385);
      ImageView localImageView = (ImageView)findViewById(2131165384);
      if (AccountFacade.getNetflixUserId() != null)
      {
        localTextView.setText(getResources().getString(2131493072));
        localImageView.setImageResource(2130837818);
        return;
      }
      localTextView.setText(getResources().getString(2131493071));
      localImageView.setImageResource(2130837815);
      return;
    }
    localRelativeLayout.setVisibility(8);
  }

  private void initializeSocialViews()
  {
    RelativeLayout localRelativeLayout1 = (RelativeLayout)findViewById(2131165653);
    localRelativeLayout1.setOnClickListener(this.homepageOnClickListener);
    localRelativeLayout1.setFocusable(true);
    ImageView localImageView1 = (ImageView)localRelativeLayout1.findViewById(2131165656);
    TextView localTextView1 = (TextView)localRelativeLayout1.findViewById(2131165657);
    RelativeLayout localRelativeLayout2 = (RelativeLayout)findViewById(2131165658);
    localRelativeLayout2.setOnClickListener(this.homepageOnClickListener);
    localRelativeLayout2.setFocusable(true);
    ImageView localImageView2 = (ImageView)localRelativeLayout2.findViewById(2131165661);
    TextView localTextView2 = (TextView)localRelativeLayout2.findViewById(2131165662);
    User localUser = AccountFacade.getSocialUser();
    if (localUser == null)
    {
      String str2 = getResources().getString(2131493071);
      localTextView1.setText(str2);
      localImageView1.setImageResource(2130837642);
      localRelativeLayout1.setVisibility(0);
      localTextView2.setText(str2);
      localImageView2.setImageResource(2130837815);
      localRelativeLayout2.setVisibility(0);
    }
    String str1;
    do
    {
      return;
      str1 = getResources().getString(2131493169);
      if (AccountManager.instance().isPlatformFacebook())
      {
        Object[] arrayOfObject2 = new Object[1];
        arrayOfObject2[0] = localUser.displayName;
        localTextView1.setText(String.format(str1, arrayOfObject2));
        localImageView1.setImageResource(2130837648);
        localRelativeLayout2.setVisibility(8);
        return;
      }
    }
    while (!AccountManager.instance().isPlatformFlixster());
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = localUser.displayName;
    localTextView2.setText(String.format(str1, arrayOfObject1));
    localImageView2.setImageResource(2130837818);
    localRelativeLayout1.setVisibility(8);
  }

  private void populatePage()
  {
    Button localButton = (Button)findViewById(2131165751);
    TextView localTextView1;
    label69: TextView localTextView3;
    RelativeLayout localRelativeLayout5;
    label355: TextView localTextView4;
    RelativeLayout localRelativeLayout6;
    String[] arrayOfString2;
    if (FlixsterApplication.getAdminState() == 0)
    {
      localButton.setVisibility(8);
      initializeNetflixViews();
      initializeSocialViews();
      localTextView1 = (TextView)findViewById(2131165755);
      if (!FlixsterApplication.getUseLocationServiceFlag())
        break label436;
      localTextView1.setText("");
      WorkerThreads.instance().invokeLater(new Runnable()
      {
        public void run()
        {
          String str = FlixsterApplication.getCurrentLocationDisplay();
          SettingsPage.this.getCurrentLocationHandler.sendMessage(Message.obtain(null, 0, str));
        }
      });
      RelativeLayout localRelativeLayout1 = (RelativeLayout)findViewById(2131165754);
      localRelativeLayout1.setOnClickListener(this);
      localRelativeLayout1.setFocusable(true);
      RelativeLayout localRelativeLayout2 = (RelativeLayout)findViewById(2131165756);
      ((TextView)findViewById(2131165758)).setText(getResources().getStringArray(2131623937)[FlixsterApplication.getLocationPolicy()]);
      localRelativeLayout2.setOnClickListener(this);
      localRelativeLayout2.setFocusable(true);
      RelativeLayout localRelativeLayout3 = (RelativeLayout)findViewById(2131165759);
      localRelativeLayout3.setOnClickListener(this);
      localRelativeLayout3.setFocusable(true);
      this.theaterDistanceLabel = ((TextView)findViewById(2131165761));
      TextView localTextView2 = this.theaterDistanceLabel;
      String str = getResources().getString(2131493093);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(FlixsterApplication.getTheaterDistance());
      localTextView2.setText(String.format(str, arrayOfObject));
      this.movieRatingsLabel = ((TextView)findViewById(2131165763));
      String[] arrayOfString1 = getResources().getStringArray(2131623936);
      this.movieRatingsLabel.setText(arrayOfString1[FlixsterApplication.getMovieRatingType()]);
      RelativeLayout localRelativeLayout4 = (RelativeLayout)findViewById(2131165762);
      localRelativeLayout4.setOnClickListener(this);
      localRelativeLayout4.setFocusable(true);
      localTextView3 = (TextView)findViewById(2131165765);
      localRelativeLayout5 = (RelativeLayout)findViewById(2131165764);
      if (!Environment.getExternalStorageState().contentEquals("mounted"))
        break label488;
      localTextView3.setText(getResources().getStringArray(2131623938)[FlixsterApplication.getCachePolicy()]);
      localRelativeLayout5.setOnClickListener(this);
      localRelativeLayout5.setFocusable(true);
      localTextView4 = (TextView)findViewById(2131165767);
      localRelativeLayout6 = (RelativeLayout)findViewById(2131165766);
      arrayOfString2 = getResources().getStringArray(2131623939);
      if (!FlixsterApplication.getCaptionsEnabled())
        break label511;
    }
    label436: label488: label511: for (int i = 1; ; i = 0)
    {
      localTextView4.setText(arrayOfString2[i]);
      localRelativeLayout6.setOnClickListener(this);
      localRelativeLayout6.setFocusable(true);
      return;
      localButton.setVisibility(0);
      localButton.setOnClickListener(this);
      break;
      if ((FlixsterApplication.getUserLocation() != null) && (!"".equals(FlixsterApplication.getUserLocation())))
      {
        localTextView1.setText(FlixsterApplication.getUserLocation());
        localTextView1.setTextColor(-16777216);
        break label69;
      }
      localTextView1.setText(2131493083);
      localTextView1.setTextColor(-65536);
      break label69;
      localTextView3.setText("No external storage detected.");
      localRelativeLayout5.setOnClickListener(null);
      localRelativeLayout5.setFocusable(false);
      break label355;
    }
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    case 2131165752:
    case 2131165753:
    case 2131165755:
    case 2131165757:
    case 2131165758:
    case 2131165760:
    case 2131165761:
    case 2131165763:
    case 2131165765:
    default:
      return;
    case 2131165754:
      if (GoogleApiDetector.instance().isVanillaAndroid());
      for (Object localObject = LocationPage.class; ; localObject = LocationMapPage.class)
      {
        startActivityForResult(new Intent(this, (Class)localObject), 0);
        return;
      }
    case 2131165762:
      showDialog(3);
      return;
    case 2131165756:
      showDialog(1);
      return;
    case 2131165759:
      showDialog(2);
      return;
    case 2131165764:
      showDialog(7);
      return;
    case 2131165766:
      showDialog(13);
      return;
    case 2131165751:
    }
    Starter.launchAdminPage(this);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903162);
    createActionBar();
    setActionBarTitle("Settings - Version " + FlixsterApplication.getVersionName());
    Logger.d("FlxMain", "SettingsPage.onCreate");
    mSettingsPage = this;
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    case 4:
    case 6:
    default:
      return null;
    case 0:
      final View localView = LayoutInflater.from(this).inflate(2130903189, null);
      localView.setMinimumWidth(300);
      AlertDialog.Builder localBuilder10 = new AlertDialog.Builder(this).setView(localView);
      DialogInterface.OnClickListener local8 = new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          String str = PostalCodeUtils.parseZipcodeForShowtimes(((EditText)localView.findViewById(2131165945)).getText().toString());
          if (str != null)
          {
            FlixsterApplication.setUserLocation(str);
            FlixsterApplication.setUseLocationServiceFlag(false);
            SettingsPage.mSettingsPage.mRefreshHandler.sendEmptyMessage(0);
            return;
          }
          SettingsPage.this.showDialog(5);
        }
      };
      return localBuilder10.setPositiveButton(2131492937, local8).setNegativeButton(2131492938, null).setOnKeyListener(new DialogInterface.OnKeyListener()
      {
        public boolean onKey(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
        {
          if (paramAnonymousInt == 66)
          {
            String str = PostalCodeUtils.parseZipcodeForShowtimes(((EditText)localView.findViewById(2131165945)).getText().toString());
            if (str != null)
            {
              FlixsterApplication.setUserLocation(str);
              FlixsterApplication.setUseLocationServiceFlag(false);
              SettingsPage.mSettingsPage.mRefreshHandler.sendEmptyMessage(0);
              SettingsPage.this.removeDialog(0);
            }
          }
          return false;
        }
      }).create();
    case 8:
      this.mLocationPolicyDialog = new ProgressDialog(this);
      this.mLocationPolicyDialog.setMessage("Changing location policy....");
      this.mLocationPolicyDialog.setIndeterminate(true);
      this.mLocationPolicyDialog.setCancelable(true);
      this.mLocationPolicyDialog.setCanceledOnTouchOutside(true);
      return this.mLocationPolicyDialog;
    case 9:
      this.mCachePolicyDialog = new ProgressDialog(this);
      this.mCachePolicyDialog.setMessage("Changing cache policy....");
      this.mCachePolicyDialog.setIndeterminate(true);
      this.mCachePolicyDialog.setCancelable(true);
      this.mCachePolicyDialog.setCanceledOnTouchOutside(true);
      return this.mCachePolicyDialog;
    case 1:
      AlertDialog.Builder localBuilder9 = new AlertDialog.Builder(this);
      localBuilder9.setTitle(2131492950);
      localBuilder9.setItems(2131623937, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          SettingsPage.this.showDialog(8);
          SettingsPage.this.ScheduleLocationChange(paramAnonymousInt);
        }
      });
      return localBuilder9.create();
    case 2:
      AlertDialog.Builder localBuilder8 = new AlertDialog.Builder(this);
      localBuilder8.setTitle(getResources().getString(2131492931));
      double d1 = 1.0D;
      final double[] arrayOfDouble;
      final double d2;
      CharSequence[] arrayOfCharSequence;
      if (FlixsterApplication.getDistanceType() == 0)
      {
        arrayOfDouble = SEARCH_RADIUS_IMPERIAL;
        d2 = d1;
        arrayOfCharSequence = new CharSequence[arrayOfDouble.length];
      }
      for (int i = 0; ; i++)
      {
        if (i >= arrayOfDouble.length)
        {
          localBuilder8.setItems(arrayOfCharSequence, new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              FlixsterApplication.setTheaterDistance((int)Math.round(arrayOfDouble[paramAnonymousInt] * d2));
              SettingsPage.this.theaterDistanceLabel.setText(this.val$radiusChoices[paramAnonymousInt]);
            }
          });
          localBuilder8.setNegativeButton(getResources().getString(2131492938), null);
          return localBuilder8.create();
          d1 = 1.608999967575073D;
          arrayOfDouble = SEARCH_RADIUS_METRIC;
          break;
        }
        String str7 = getResources().getString(2131493093);
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf((int)Math.round(d2 * arrayOfDouble[i]));
        arrayOfCharSequence[i] = String.format(str7, arrayOfObject);
      }
    case 7:
      AlertDialog.Builder localBuilder7 = new AlertDialog.Builder(this).setTitle(2131492954);
      DialogInterface.OnClickListener local12 = new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          SettingsPage.this.showDialog(9);
          SettingsPage.this.ScheduleCacheChange(paramAnonymousInt);
        }
      };
      return localBuilder7.setItems(2131623938, local12).create();
    case 13:
      AlertDialog.Builder localBuilder6 = new AlertDialog.Builder(this).setTitle(2131492920);
      DialogInterface.OnClickListener local13 = new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          if (paramAnonymousInt != 0);
          for (boolean bool = true; ; bool = false)
          {
            FlixsterApplication.setCaptionsEnabled(bool);
            ((TextView)SettingsPage.this.findViewById(2131165767)).setText(SettingsPage.this.getResources().getStringArray(2131623939)[paramAnonymousInt]);
            return;
          }
        }
      };
      return localBuilder6.setItems(2131623939, local13).create();
    case 3:
      AlertDialog.Builder localBuilder5 = new AlertDialog.Builder(this).setTitle(2131492932);
      DialogInterface.OnClickListener local14 = new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          FlixsterApplication.setMovieRatingType(paramAnonymousInt);
          String[] arrayOfString = SettingsPage.this.getResources().getStringArray(2131623936);
          SettingsPage.this.movieRatingsLabel.setText(arrayOfString[paramAnonymousInt]);
        }
      };
      return localBuilder5.setItems(2131623936, local14).create();
    case 5:
      AlertDialog.Builder localBuilder4 = new AlertDialog.Builder(this).setMessage("Invalid Postal Code").setTitle("Invalid Postal Code").setCancelable(false);
      DialogInterface.OnClickListener local15 = new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          SettingsPage.mSettingsPage.mRefreshHandler.sendEmptyMessage(0);
        }
      };
      return localBuilder4.setPositiveButton("OK", local15).create();
    case 10:
      AlertDialog.Builder localBuilder3 = new AlertDialog.Builder(this);
      String str5 = String.format(getResources().getString(2131493102), new Object[] { "Facebook" });
      String str6 = String.format(getResources().getString(2131493103), new Object[] { "Facebook" });
      localBuilder3.setTitle(str5);
      localBuilder3.setMessage(str6);
      localBuilder3.setCancelable(true);
      localBuilder3.setPositiveButton(getResources().getString(2131493171), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          AccountManager.instance().fbLogout(SettingsPage.this);
          SettingsPage.this.initializeSocialViews();
        }
      });
      localBuilder3.setNegativeButton(getResources().getString(2131492938), null);
      return localBuilder3.create();
    case 11:
      AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(this);
      String str3 = String.format(getResources().getString(2131493102), new Object[] { "Flixster" });
      String str4 = String.format(getResources().getString(2131493103), new Object[] { "Flixster" });
      localBuilder2.setTitle(str3);
      localBuilder2.setMessage(str4);
      localBuilder2.setCancelable(true);
      localBuilder2.setPositiveButton(getResources().getString(2131493171), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          AccountManager.instance().onFlxLogout();
          SettingsPage.this.initializeSocialViews();
        }
      });
      localBuilder2.setNegativeButton(getResources().getString(2131492938), null);
      return localBuilder2.create();
    case 12:
    }
    AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(this);
    String str1 = String.format(getResources().getString(2131493102), new Object[] { "Netflix" });
    String str2 = String.format(getResources().getString(2131493103), new Object[] { "Netflix" });
    localBuilder1.setTitle(str1);
    localBuilder1.setMessage(str2);
    localBuilder1.setCancelable(true);
    localBuilder1.setPositiveButton(getResources().getString(2131493171), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        FlixsterApplication.setNetflixOAuthToken(null);
        FlixsterApplication.setNetflixOAuthTokenSecret(null);
        FlixsterApplication.setNetflixUserId(null);
        SettingsPage.this.initializeNetflixViews();
      }
    });
    localBuilder1.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder1.create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  public void onDestroy()
  {
    Logger.d("FlxMain", "SettingsPage.onDestroy");
    super.onDestroy();
    if (this.mTimer != null)
    {
      this.mTimer.cancel();
      this.mTimer.purge();
    }
    this.mTimer = null;
    mSettingsPage = null;
  }

  protected void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "SettingsPage.onResume");
    if (this.mTimer == null)
      this.mTimer = new Timer();
    Trackers.instance().track("/mymovies/settings", "My Movies - Settings");
    this.mRefreshHandler.sendEmptyMessage(0);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.SettingsPage
 * JD-Core Version:    0.6.2
 */