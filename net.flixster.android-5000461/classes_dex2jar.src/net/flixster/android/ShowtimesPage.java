package net.flixster.android;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.DateTimeHelper;
import com.flixster.android.utils.GoogleApiDetector;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.Divider;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.ads.model.DfpAd.DfpCustomTarget;
import net.flixster.android.data.ApiBuilder;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviAd;
import net.flixster.android.lvi.LviDatePanel;
import net.flixster.android.lvi.LviFooter;
import net.flixster.android.lvi.LviIconPanel;
import net.flixster.android.lvi.LviLocationPanel;
import net.flixster.android.lvi.LviMessagePanel;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviShowtimes;
import net.flixster.android.lvi.LviSubHeader;
import net.flixster.android.lvi.LviTheater;
import net.flixster.android.lvi.LviWrapper;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Showtimes;
import net.flixster.android.model.Theater;

public class ShowtimesPage extends LviActivity
{
  private DfpAd.DfpCustomTarget dfpTarget;
  private Movie mMovie;
  private List<Theater> mTheaterList = new ArrayList();
  private final View.OnClickListener mTheaterMapOnClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Starter.launchTheatersMapForMovie(ShowtimesPage.this.mMovie, ShowtimesPage.this);
    }
  };

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      this.throbberHandler.sendEmptyMessage(1);
      TimerTask local2 = new TimerTask()
      {
        public void run()
        {
          Logger.d("FlxMain", "ShowtimesPage.ScheduleLoadItemsTask.run");
          try
          {
            HashMap localHashMap = FlixsterApplication.getFavoriteTheatersList();
            if (FlixsterApplication.getUseLocationServiceFlag());
            for (ShowtimesPage.this.mTheaterList = TheaterDao.findTheatersByMovieLocation(FlixsterApplication.getCurrentLatitude(), FlixsterApplication.getCurrentLongitude(), FlixsterApplication.getShowtimesDate(), ShowtimesPage.this.mMovie.getId(), localHashMap); ; ShowtimesPage.this.mTheaterList = TheaterDao.findTheatersByMovieLocation(FlixsterApplication.getUserLatitude(), FlixsterApplication.getUserLongitude(), FlixsterApplication.getShowtimesDate(), ShowtimesPage.this.mMovie.getId(), localHashMap))
            {
              boolean bool = ShowtimesPage.this.shouldSkipBackgroundTask(this.val$currResumeCtr);
              if (!bool)
                break;
              return;
            }
          }
          catch (DaoException localDaoException)
          {
            Logger.e("FlxMain", "ShowtimesPage.ScheduleLoadItemsTask.run DaoException", localDaoException);
            ShowtimesPage.this.retryLogic(localDaoException);
            return;
            ShowtimesPage.this.setShowtimesLviList();
            ShowtimesPage.this.mUpdateHandler.sendEmptyMessage(0);
            return;
          }
          finally
          {
            ShowtimesPage.this.throbberHandler.sendEmptyMessage(0);
          }
        }
      };
      if (this.mPageTimer != null)
        this.mPageTimer.schedule(local2, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private String createShowtimesSummary(List<Theater> paramList, long paramLong)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (paramList.size() == 0)
    {
      localStringBuilder.append(getString(2131492952)).append("\n");
      localStringBuilder.append("\n");
      return localStringBuilder.toString();
    }
    localStringBuilder.append("Showtimes for ").append(DateTimeHelper.DAY_IN_WEEK_FORMATTER.format(FlixsterApplication.getShowtimesDate())).append("\n\n");
    Iterator localIterator1 = paramList.iterator();
    label82: ArrayList localArrayList;
    Iterator localIterator2;
    while (localIterator1.hasNext())
    {
      Theater localTheater = (Theater)localIterator1.next();
      localArrayList = (ArrayList)localTheater.getShowtimesListByMovieHash().get(Long.valueOf(paramLong));
      if (localArrayList != null)
      {
        localStringBuilder.append(localTheater.getProperty("name")).append("\n");
        localIterator2 = localArrayList.iterator();
      }
    }
    while (true)
    {
      if (!localIterator2.hasNext())
      {
        localStringBuilder.append("\n");
        break label82;
        break;
      }
      Showtimes localShowtimes = (Showtimes)localIterator2.next();
      if (localArrayList.size() > 1)
        localStringBuilder.append(localShowtimes.mShowtimesTitle).append("\n");
      localStringBuilder.append(localShowtimes.getTimesString()).append("\n");
    }
  }

  private void setShowtimesLviList()
  {
    Logger.d("FlxMain", "ShowtimesPage.setShowtimesLviList ");
    this.mDataHolder.clear();
    destroyExistingLviAd();
    this.lviAd = new LviAd();
    this.lviAd.mAdSlot = "MovieShowtimes";
    this.lviAd.dfpTarget = this.dfpTarget;
    this.mDataHolder.add(this.lviAd);
    LviMovie localLviMovie = new LviMovie();
    localLviMovie.mMovie = this.mMovie;
    localLviMovie.mForm = 1;
    this.mDataHolder.add(localLviMovie);
    LviLocationPanel localLviLocationPanel = new LviLocationPanel();
    this.mDataHolder.add(localLviLocationPanel);
    LviDatePanel localLviDatePanel = new LviDatePanel();
    localLviDatePanel.mOnClickListener = getDateSelectOnClickListener();
    this.mDataHolder.add(localLviDatePanel);
    if (!GoogleApiDetector.instance().isVanillaAndroid())
    {
      LviIconPanel localLviIconPanel = new LviIconPanel();
      localLviIconPanel.mLabel = getResources().getString(2131492971);
      localLviIconPanel.mOnClickListener = this.mTheaterMapOnClickListener;
      this.mDataHolder.add(localLviIconPanel);
    }
    LviSubHeader localLviSubHeader = new LviSubHeader();
    localLviSubHeader.mTitle = getResources().getString(2131492934);
    this.mDataHolder.add(localLviSubHeader);
    if (this.mTheaterList.size() > 0)
    {
      Iterator localIterator = this.mTheaterList.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          LviFooter localLviFooter = new LviFooter();
          this.mDataHolder.add(localLviFooter);
          return;
        }
        Theater localTheater = (Theater)localIterator.next();
        ArrayList localArrayList1 = this.mDataHolder;
        Divider localDivider = new Divider(this);
        localArrayList1.add(LviWrapper.convertToLvi(localDivider, Lvi.VIEW_TYPE_DIVIDER));
        LviTheater localLviTheater = new LviTheater();
        localLviTheater.mTheater = localTheater;
        localLviTheater.mStarTheaterClick = getStarTheaterClickListener();
        localLviTheater.mTheaterClick = getTheaterClickListener();
        this.mDataHolder.add(localLviTheater);
        ArrayList localArrayList2 = (ArrayList)localTheater.getShowtimesListByMovieHash().get(Long.valueOf(this.mMovie.getId()));
        if ((localArrayList2 == null) || (localArrayList2.isEmpty()))
        {
          LviShowtimes localLviShowtimes1 = new LviShowtimes();
          localLviShowtimes1.mTheater = localTheater;
          localLviShowtimes1.mShowtimes = null;
          this.mDataHolder.add(localLviShowtimes1);
        }
        else
        {
          int i = localArrayList2.size();
          for (int j = 0; j < i; j++)
          {
            Showtimes localShowtimes = (Showtimes)localArrayList2.get(j);
            LviShowtimes localLviShowtimes2 = new LviShowtimes();
            localLviShowtimes2.mTheater = localTheater;
            localLviShowtimes2.mShowtimesListSize = i;
            localLviShowtimes2.mShowtimePosition = j;
            localLviShowtimes2.mShowtimes = localShowtimes;
            localLviShowtimes2.mBuyClick = getShowtimesClickListener();
            this.mDataHolder.add(localLviShowtimes2);
          }
        }
      }
    }
    LviMessagePanel localLviMessagePanel = new LviMessagePanel();
    localLviMessagePanel.mMessage = getResources().getString(2131492952);
    this.mDataHolder.add(localLviMessagePanel);
  }

  protected Intent createShareIntent()
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("text/plain");
    StringBuilder localStringBuilder = new StringBuilder(String.valueOf(MovieDetails.createMovieSummary(this.mMovie))).append(createShowtimesSummary(this.mTheaterList, this.mMovie.getId()));
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = ApiBuilder.mobileUpsell();
    localIntent.putExtra("android.intent.extra.TEXT", getString(2131493297, arrayOfObject));
    return localIntent;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    createActionBar();
    setActionBarTitle(getString(2131492934));
    this.mStickyTopAd.setSlot("MovieShowtimesStickyTop");
    this.mStickyBottomAd.setSlot("MovieShowtimesStickyBottom");
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.mMovie = MovieDao.getMovie(localBundle.getLong("net.flixster.android.EXTRA_MOVIE_ID"));
    this.dfpTarget = new DfpAd.DfpCustomTarget(this.mMovie);
    this.mStickyTopAd.dfpCustomTarget = this.dfpTarget;
    this.mStickyBottomAd.dfpCustomTarget = this.dfpTarget;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
    case 2131165955:
    }
    while (true)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      Trackers.instance().trackEvent("/showtimes", "Showtimes - " + this.mMovie.getTitle(), "ActionBar", "Share");
    }
  }

  public void onResume()
  {
    super.onResume();
    Trackers.instance().track("/showtimes", "Showtimes - " + this.mMovie.getTitle());
    ScheduleLoadItemsTask(100L);
  }

  protected void retryAction()
  {
    ScheduleLoadItemsTask(1000L);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ShowtimesPage
 * JD-Core Version:    0.6.2
 */