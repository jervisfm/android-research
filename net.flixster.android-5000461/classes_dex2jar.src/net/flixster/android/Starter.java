package net.flixster.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.os.Process;
import android.widget.Toast;
import com.flixster.android.activity.EpisodesPage;
import com.flixster.android.activity.LoginActivity;
import com.flixster.android.activity.PhotoGalleryPage;
import com.flixster.android.activity.WebViewPage;
import com.flixster.android.activity.common.LoadingActivity;
import com.flixster.android.activity.gtv.VideoPlayer;
import com.flixster.android.activity.phone.SearchFragmentActivity;
import com.flixster.android.ads.AdsArbiter;
import com.flixster.android.ads.AdsArbiter.TrailerAdsArbiter;
import com.flixster.android.ads.HtmlAdPage;
import com.flixster.android.msk.MskController;
import com.flixster.android.msk.MskEntryActivity;
import com.flixster.android.utils.FirstLaunchMskHelper;
import com.flixster.android.utils.GoogleApiDetector;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.StringHelper;
import java.lang.ref.SoftReference;
import net.flixster.android.model.Movie;

public class Starter
{
  public static void deepLinkMyMoviesTab(Context paramContext)
  {
    paramContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("flixster://mymovies")));
  }

  public static void forceClose()
  {
    Process.killProcess(Process.myPid());
  }

  public static void launchActor(long paramLong, String paramString, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, ActorPage.class);
    localIntent.putExtra("ACTOR_ID", paramLong);
    localIntent.putExtra("ACTOR_NAME", paramString);
    paramContext.startActivity(localIntent);
  }

  public static void launchActorGallery(long paramLong, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, ActorGalleryPage.class);
    localIntent.putExtra("ACTOR_ID", paramLong);
    paramContext.startActivity(localIntent);
  }

  public static void launchAdFreeTrailer(long paramLong, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, LoadingActivity.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramLong);
    paramContext.startActivity(localIntent);
  }

  public static void launchAdFreeTrailer(String paramString, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, MovieTrailer.class);
    localIntent.addFlags(268435456);
    localIntent.putExtra("low", paramString);
    localIntent.putExtra("title", "");
    AdsArbiter.trailer().originatedFromAds();
    paramContext.startActivity(localIntent);
  }

  public static void launchAdFreeTrailer(Movie paramMovie, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, MovieTrailer.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramMovie.getId());
    localIntent.putExtra("high", paramMovie.getTrailerHigh());
    localIntent.putExtra("med", paramMovie.getTrailerMed());
    localIntent.putExtra("low", paramMovie.getTrailerLow());
    localIntent.putExtra("title", paramMovie.getTitle());
    AdsArbiter.trailer().originatedFromAds();
    paramContext.startActivity(localIntent);
  }

  public static void launchAdHtmlPage(String paramString1, String paramString2, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, HtmlAdPage.class);
    localIntent.setFlags(1073741824);
    localIntent.putExtra("KEY_URL", paramString1);
    localIntent.putExtra("KEY_TITLE", paramString2);
    localIntent.putExtra("KEY_FLAGS", 32);
    paramContext.startActivity(localIntent);
  }

  public static void launchAdminPage(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, AdminPage.class));
  }

  public static void launchBrowser(String paramString, Context paramContext)
  {
    Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
    localIntent.addFlags(268435456);
    paramContext.startActivity(localIntent);
  }

  public static void launchDownloadManager(Context paramContext)
  {
    tryLaunch(paramContext, new Intent("android.intent.action.VIEW_DOWNLOADS"));
  }

  public static void launchEpisodes(long paramLong1, long paramLong2, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, EpisodesPage.class);
    localIntent.putExtra("KEY_SEASON_ID", paramLong1);
    localIntent.putExtra("KEY_RIGHT_ID", paramLong2);
    paramContext.startActivity(localIntent);
  }

  public static void launchEpisodes(long paramLong, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, EpisodesPage.class);
    localIntent.putExtra("KEY_SEASON_ID", paramLong);
    paramContext.startActivity(localIntent);
  }

  public static void launchFbLogin(Context paramContext)
  {
  }

  public static void launchFirstMsk(Context paramContext)
  {
    MskController.instance().setFirstLaunchMode(true);
    if ((FirstLaunchMskHelper.shouldShow()) && (MskController.instance().isReady()))
    {
      FirstLaunchMskHelper.shown();
      paramContext.startActivity(new Intent(paramContext, MskEntryActivity.class));
    }
  }

  public static void launchFlxHtmlPage(String paramString1, String paramString2, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, HtmlAdPage.class);
    localIntent.putExtra("KEY_URL", paramString1);
    localIntent.putExtra("KEY_TITLE", paramString2);
    localIntent.putExtra("KEY_FLAGS", 2);
    paramContext.startActivity(localIntent);
  }

  public static void launchFlxLogin(Context paramContext)
  {
  }

  public static void launchLoginPage(String paramString, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, LoginActivity.class);
    localIntent.putExtra("net.flixster.android.EXTRA_TITLE", paramString);
    paramContext.startActivity(localIntent);
  }

  public static void launchMaps(String paramString, Context paramContext)
  {
    String str1 = StringHelper.replaceSpace(paramString, "+");
    if (GoogleApiDetector.instance().isVanillaAndroid());
    for (String str2 = "http://maps.google.com/maps?q=" + str1; ; str2 = "geo:0,0?q=" + str1)
    {
      paramContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str2)));
      return;
    }
  }

  public static void launchMovieDetail(long paramLong1, long paramLong2, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, MovieDetails.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramLong1);
    localIntent.putExtra("KEY_RIGHT_ID", paramLong2);
    paramContext.startActivity(localIntent);
  }

  public static void launchMovieDetail(long paramLong, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, MovieDetails.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramLong);
    paramContext.startActivity(localIntent);
  }

  public static void launchMovieGallery(long paramLong, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, MovieGalleryPage.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramLong);
    paramContext.startActivity(localIntent);
  }

  public static void launchMovieShowtimes(long paramLong, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, ShowtimesPage.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramLong);
    paramContext.startActivity(localIntent);
  }

  public static void launchMsk(Context paramContext)
  {
    MskController.instance().setFirstLaunchMode(false);
    if (MskController.instance().isReady())
    {
      paramContext.startActivity(new Intent(paramContext, MskEntryActivity.class));
      return;
    }
    Toast.makeText(paramContext, 2131493107, 0).show();
  }

  public static void launchMskHtmlPageForResult(String paramString, boolean paramBoolean, Activity paramActivity, int paramInt)
  {
    int i = 10;
    if (paramBoolean)
      i |= 64;
    String str = paramActivity.getString(2131493173);
    Intent localIntent = new Intent(paramActivity, HtmlAdPage.class);
    localIntent.putExtra("KEY_URL", paramString);
    localIntent.putExtra("KEY_TITLE", str);
    localIntent.putExtra("KEY_FLAGS", i);
    paramActivity.startActivityForResult(localIntent, paramInt);
  }

  public static void launchSearch(String paramString, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, SearchFragmentActivity.class);
    localIntent.putExtra("net.flixster.android.EXTRA_QUERY", paramString);
    paramContext.startActivity(localIntent);
  }

  public static void launchSeasonDetail(long paramLong1, long paramLong2, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, MovieDetails.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramLong1);
    localIntent.putExtra("KEY_IS_SEASON", true);
    localIntent.putExtra("KEY_RIGHT_ID", paramLong2);
    paramContext.startActivity(localIntent);
  }

  public static void launchSeasonDetail(long paramLong, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, MovieDetails.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramLong);
    localIntent.putExtra("KEY_IS_SEASON", true);
    paramContext.startActivity(localIntent);
  }

  public static void launchSettings(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, SettingsPage.class));
  }

  public static void launchSimpleWebViewPage(String paramString, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, WebViewPage.class);
    localIntent.putExtra("KEY_URL", paramString);
    paramContext.startActivity(localIntent);
  }

  public static void launchSystemWirelessSettings(Context paramContext)
  {
    tryLaunch(paramContext, new Intent("android.settings.WIRELESS_SETTINGS"));
  }

  public static void launchTheatersMap(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, TheaterMapPage.class));
  }

  public static void launchTheatersMapForMovie(Movie paramMovie, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, MovieMapPage.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramMovie.getId());
    localIntent.putExtra("MOVIE_THUMBNAIL", (Parcelable)paramMovie.thumbnailSoftBitmap.get());
    localIntent.putExtra("title", paramMovie.getProperty("title"));
    localIntent.putExtra("runningTime", paramMovie.getProperty("runningTime"));
    localIntent.putExtra("MOVIE_ACTORS_SHORT", paramMovie.getProperty("MOVIE_ACTORS_SHORT"));
    localIntent.putExtra("mpaa", paramMovie.getProperty("mpaa"));
    localIntent.putExtra("popcornScore", paramMovie.getIntProperty("popcornScore"));
    localIntent.putExtra("rottenTomatoes", paramMovie.getIntProperty("rottenTomatoes"));
    paramContext.startActivity(localIntent);
  }

  public static void launchTrailer(Movie paramMovie, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, MovieTrailer.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramMovie.getId());
    localIntent.putExtra("high", paramMovie.getTrailerHigh());
    localIntent.putExtra("med", paramMovie.getTrailerMed());
    localIntent.putExtra("low", paramMovie.getTrailerLow());
    localIntent.putExtra("title", paramMovie.getTitle());
    AdsArbiter.trailer().originatedFromNonAds();
    paramContext.startActivity(localIntent);
  }

  public static void launchTrailerForResult(Movie paramMovie, Activity paramActivity, int paramInt)
  {
    Intent localIntent = new Intent(paramActivity, MovieTrailer.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", paramMovie.getId());
    localIntent.putExtra("high", paramMovie.getTrailerHigh());
    localIntent.putExtra("med", paramMovie.getTrailerMed());
    localIntent.putExtra("low", paramMovie.getTrailerLow());
    localIntent.putExtra("title", paramMovie.getTitle());
    AdsArbiter.trailer().originatedFromNonAds();
    paramActivity.startActivityForResult(localIntent, paramInt);
  }

  public static void launchUserGallery(String paramString, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, PhotoGalleryPage.class);
    localIntent.putExtra("KEY_USERNAME", paramString);
    paramContext.startActivity(localIntent);
  }

  public static void launchUserProfile(long paramLong, Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, UserProfilePage.class);
    localIntent.putExtra("PLATFORM_ID", String.valueOf(paramLong));
    paramContext.startActivity(localIntent);
  }

  public static void launchVideo(String paramString, Context paramContext)
  {
    launchVideo(paramString, paramContext, false);
  }

  public static void launchVideo(String paramString, Context paramContext, boolean paramBoolean)
  {
    Intent localIntent = new Intent(paramContext, VideoPlayer.class);
    localIntent.putExtra("KEY_URL", paramString);
    localIntent.putExtra("KEY_LANDSCAPE", paramBoolean);
    paramContext.startActivity(localIntent);
  }

  public static void tryLaunch(Context paramContext, Intent paramIntent)
  {
    try
    {
      paramContext.startActivity(paramIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Logger.w("FlxMain", "Failed to start " + paramIntent, localActivityNotFoundException);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.Starter
 * JD-Core Version:    0.6.2
 */