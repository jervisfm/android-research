package net.flixster.android;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.LocationFacade;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.Divider;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.ads.InterstitialPage;
import net.flixster.android.ads.model.FlixsterAd;
import net.flixster.android.data.ApiBuilder;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviAd;
import net.flixster.android.lvi.LviDatePanel;
import net.flixster.android.lvi.LviFooter;
import net.flixster.android.lvi.LviIconPanel;
import net.flixster.android.lvi.LviMessagePanel;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviShowtimes;
import net.flixster.android.lvi.LviSubHeader;
import net.flixster.android.lvi.LviWrapper;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Property;
import net.flixster.android.model.Showtimes;
import net.flixster.android.model.Theater;

public class TheaterInfoPage extends LviActivity
{
  private String elevenCreativeUrl;
  private final View.OnClickListener m7ElevenListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Trackers.instance().trackEvent("/7eleven", "7-Eleven", "7Eleven", "Button", "Click", 0);
      FlixsterAd localFlixsterAd = new FlixsterAd();
      localFlixsterAd.imageUrl = TheaterInfoPage.this.elevenCreativeUrl;
      localFlixsterAd.button2 = TheaterInfoPage.this.getString(2131492938);
      ObjectHolder.instance().put("CustomInterstitial", localFlixsterAd);
      Intent localIntent = new Intent(TheaterInfoPage.this, InterstitialPage.class);
      localIntent.setFlags(1073741824);
      localIntent.putExtra("SLOT_TYPE", "CustomInterstitial");
      TheaterInfoPage.this.startActivity(localIntent);
    }
  };
  private final View.OnClickListener mCallTheaterListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Intent localIntent = new Intent("android.intent.action.DIAL");
      String str1;
      String str2;
      if (TheaterInfoPage.this.mTheater.checkProperty("phone"))
      {
        str1 = TheaterInfoPage.this.mTheater.getProperty("phone");
        if (!str1.contains("FANDANGO"))
          break label91;
        str2 = str1.replaceAll("FANDANGO", "3263264#");
      }
      while (true)
      {
        localIntent.setData(Uri.parse("tel:" + str2));
        try
        {
          TheaterInfoPage.this.startActivity(localIntent);
          return;
          label91: str2 = PhoneNumberUtils.convertKeypadLettersToDigits(str1);
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
          Logger.w("FlxMain", "Intent not found", localActivityNotFoundException);
        }
      }
    }
  };
  private final View.OnClickListener mMapTheaterListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Intent localIntent = new Intent("android.intent.action.VIEW");
      String str = TheaterInfoPage.this.mTheater.getProperty("street") + "+" + TheaterInfoPage.this.mTheater.getProperty("city") + "+" + TheaterInfoPage.this.mTheater.getProperty("state") + "+" + TheaterInfoPage.this.mTheater.getProperty("zip");
      str.replaceAll(" ", "+");
      localIntent.setData(Uri.parse("geo:0,0?q=" + str));
      Trackers.instance().track("/theaters/info/map", "query:" + str);
      try
      {
        TheaterInfoPage.this.startActivity(localIntent);
        return;
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        Logger.w("FlxMain", "Intent not found", localActivityNotFoundException);
      }
    }
  };
  private ArrayList<Movie> mMovieProtectionReference;
  private HashMap<Long, ArrayList<Showtimes>> mShowtimesByMovieHash;
  private Theater mTheater;
  private String mTheaterAddressString;
  private long mTheaterId;
  private final View.OnClickListener mYelpTheaterListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      try
      {
        String str2 = "http://lite.yelp.com/search?inboundsrc=flixster&cflt=restaurants&rflt=all&sortby=composite&find_loc=" + URLEncoder.encode(TheaterInfoPage.getAddressForYelp(TheaterInfoPage.this.mTheater), "UTF-8");
        str1 = str2;
        Trackers.instance().track("/theaters/info/yelp", "Yelp city:" + TheaterInfoPage.this.mTheater.getProperty("city"));
        localIntent = new Intent("android.intent.action.VIEW", Uri.parse(str1));
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        try
        {
          Intent localIntent;
          TheaterInfoPage.this.startActivity(localIntent);
          return;
          localUnsupportedEncodingException = localUnsupportedEncodingException;
          localUnsupportedEncodingException.printStackTrace();
          String str1 = null;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
          Logger.w("FlxMain", "Intent not found", localActivityNotFoundException);
        }
      }
    }
  };
  private boolean show7Eleven;

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      this.throbberHandler.sendEmptyMessage(1);
      TimerTask local5 = new TimerTask()
      {
        public void run()
        {
          Logger.d("FlxMain", "TheaterInfoPage.ScheduleLoadItemsTask.run mTheaterId:" + TheaterInfoPage.this.mTheaterId);
          try
          {
            if (((TheaterInfoPage.this.mTheater == null) || (TheaterInfoPage.this.mTheater.getId() <= 0L)) && (TheaterInfoPage.this.getIntent().getExtras() != null))
            {
              TheaterInfoPage.this.mTheater = TheaterDao.getTheater(TheaterInfoPage.this.mTheaterId);
              TheaterInfoPage.this.makeTheaterAddressString();
            }
            TheaterInfoPage.this.mShowtimesByMovieHash = TheaterDao.findShowtimesByTheater(FlixsterApplication.getShowtimesDate(), TheaterInfoPage.this.mTheaterId);
            Logger.d("FlxMain", "TheaterInfoPage.ScheduleLoadItemsTask.run() mShowtimesByMovieHash.size:" + TheaterInfoPage.this.mShowtimesByMovieHash.size());
            TheaterInfoPage.this.mMovieProtectionReference = new ArrayList();
            Iterator localIterator = TheaterInfoPage.this.mShowtimesByMovieHash.keySet().iterator();
            while (true)
            {
              if (!localIterator.hasNext())
              {
                boolean bool = TheaterInfoPage.this.shouldSkipBackgroundTask(this.val$currResumeCtr);
                if (!bool)
                  break;
                return;
              }
              Long localLong = (Long)localIterator.next();
              TheaterInfoPage.this.mMovieProtectionReference.add(MovieDao.getMovie(localLong.longValue()));
            }
          }
          catch (DaoException localDaoException)
          {
            Logger.e("FlxMain", "TheaterInfoPage.ScheduleLoadItemsTask DaoException", localDaoException);
            TheaterInfoPage.this.retryLogic(localDaoException);
            return;
            TheaterInfoPage.this.setTheaterInfoLviList();
            TheaterInfoPage.this.mUpdateHandler.sendEmptyMessage(0);
            return;
          }
          finally
          {
            TheaterInfoPage.this.throbberHandler.sendEmptyMessage(0);
          }
        }
      };
      if (this.mPageTimer != null)
        this.mPageTimer.schedule(local5, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private static String createTheaterSummary(Theater paramTheater, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramTheater.getProperty("name")).append("\n");
    localStringBuilder.append(paramTheater.getProperty("phone")).append("\n");
    localStringBuilder.append(paramString).append("\n");
    localStringBuilder.append("\n");
    return localStringBuilder.toString();
  }

  private static String getAddressForYelp(Theater paramTheater)
  {
    if (LocationFacade.isFrenchLocale())
      return paramTheater.getProperty("street") + ", " + paramTheater.getProperty("city") + ", France";
    return paramTheater.getProperty("address");
  }

  private void makeTheaterAddressString()
  {
    this.mTheaterAddressString = (this.mTheater.getProperty("street") + "\n" + this.mTheater.getProperty("city") + ", " + this.mTheater.getProperty("state") + " " + this.mTheater.getProperty("zip"));
  }

  private void setTheaterInfoLviList()
  {
    Logger.d("FlxMain", "TheaterInfoPage.setPopularLviList ");
    this.mDataHolder.clear();
    destroyExistingLviAd();
    this.lviAd = new LviAd();
    this.lviAd.mAdSlot = "TheaterInfo";
    this.mDataHolder.add(this.lviAd);
    LviIconPanel localLviIconPanel1 = new LviIconPanel();
    localLviIconPanel1.mLabel = this.mTheater.getProperty("phone");
    localLviIconPanel1.mDrawableResource = 2130837770;
    localLviIconPanel1.mOnClickListener = this.mCallTheaterListener;
    this.mDataHolder.add(localLviIconPanel1);
    LviIconPanel localLviIconPanel2 = new LviIconPanel();
    localLviIconPanel2.mLabel = this.mTheaterAddressString;
    localLviIconPanel2.mDrawableResource = 2130837767;
    localLviIconPanel2.mOnClickListener = this.mMapTheaterListener;
    this.mDataHolder.add(localLviIconPanel2);
    if ((LocationFacade.isUsLocale()) || (LocationFacade.isCanadaLocale()) || (LocationFacade.isUkLocale()) || (LocationFacade.isFrenchLocale()))
    {
      LviIconPanel localLviIconPanel3 = new LviIconPanel();
      localLviIconPanel3.mLabel = getResources().getString(2131493084);
      localLviIconPanel3.mDrawableResource = 2130837780;
      localLviIconPanel3.mOnClickListener = this.mYelpTheaterListener;
      this.mDataHolder.add(localLviIconPanel3);
    }
    if (this.show7Eleven)
    {
      LviIconPanel localLviIconPanel4 = new LviIconPanel();
      localLviIconPanel4.mLabel = getResources().getString(2131493085);
      localLviIconPanel4.mDrawableResource = 2130837752;
      localLviIconPanel4.mOnClickListener = this.m7ElevenListener;
      this.mDataHolder.add(localLviIconPanel4);
      Trackers.instance().trackEvent("/7eleven", "7-Eleven", "7Eleven", "Button", "Impression", 0);
    }
    LviDatePanel localLviDatePanel = new LviDatePanel();
    localLviDatePanel.mOnClickListener = getDateSelectOnClickListener();
    this.mDataHolder.add(localLviDatePanel);
    LviSubHeader localLviSubHeader = new LviSubHeader();
    localLviSubHeader.mTitle = getResources().getString(2131492934);
    this.mDataHolder.add(localLviSubHeader);
    if (this.mShowtimesByMovieHash != null)
    {
      Logger.v("FlxMain", "TheaterInfoList.LoadMoviesTask mShowtimesHash, size:" + this.mShowtimesByMovieHash.size());
      if (this.mShowtimesByMovieHash.size() == 0)
      {
        LviMessagePanel localLviMessagePanel2 = new LviMessagePanel();
        localLviMessagePanel2.mMessage = getString(2131492952);
        this.mDataHolder.add(localLviMessagePanel2);
        return;
      }
      HashMap localHashMap = this.mShowtimesByMovieHash;
      Iterator localIterator;
      if (localHashMap != null)
        localIterator = localHashMap.keySet().iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          LviFooter localLviFooter = new LviFooter();
          this.mDataHolder.add(localLviFooter);
          return;
        }
        Long localLong = (Long)localIterator.next();
        this.mDataHolder.add(LviWrapper.convertToLvi(new Divider(this), Lvi.VIEW_TYPE_DIVIDER));
        LviMovie localLviMovie = new LviMovie();
        localLviMovie.mMovie = MovieDao.getMovie(localLong.longValue());
        localLviMovie.mTrailerClick = getTrailerOnClickListener();
        this.mDataHolder.add(localLviMovie);
        ArrayList localArrayList = (ArrayList)localHashMap.get(localLong);
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
        {
          Showtimes localShowtimes = (Showtimes)localArrayList.get(j);
          LviShowtimes localLviShowtimes = new LviShowtimes();
          localLviShowtimes.mTheater = this.mTheater;
          localLviShowtimes.mShowtimesListSize = i;
          localLviShowtimes.mShowtimePosition = j;
          localLviShowtimes.mShowtimes = localShowtimes;
          localLviShowtimes.mBuyClick = getShowtimesClickListener();
          this.mDataHolder.add(localLviShowtimes);
        }
      }
    }
    LviMessagePanel localLviMessagePanel1 = new LviMessagePanel();
    localLviMessagePanel1.mMessage = "Network Error Occured";
    this.mDataHolder.add(localLviMessagePanel1);
  }

  protected Intent createShareIntent()
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("text/plain");
    StringBuilder localStringBuilder = new StringBuilder(String.valueOf(createTheaterSummary(this.mTheater, this.mTheaterAddressString)));
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = ApiBuilder.mobileUpsell();
    localIntent.putExtra("android.intent.extra.TEXT", getString(2131493297, arrayOfObject));
    return localIntent;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    createActionBar();
    this.mListView.setOnItemClickListener(getMovieItemClickListener());
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.mTheaterId = localBundle.getLong("net.flixster.android.EXTRA_THEATER_ID");
      this.mTheater = TheaterDao.getTheater(this.mTheaterId);
      makeTheaterAddressString();
    }
    while (true)
    {
      this.mStickyTopAd.setSlot("TheaterInfoStickyTop");
      this.mStickyBottomAd.setSlot("TheaterInfoStickyBottom");
      Property localProperty = Properties.instance().getProperties();
      if (localProperty != null)
      {
        this.show7Eleven = localProperty.is7ElevenEnabled;
        this.elevenCreativeUrl = localProperty.elevenCreativeUrl;
      }
      return;
      Logger.e("FlxMain", "Missing the bundle extras... ");
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getSupportMenuInflater().inflate(2131689490, paramMenu);
    if (FlixsterApplication.favoriteTheaterFlag(Long.toString(this.mTheater.getId())))
      paramMenu.removeItem(2131165965);
    while (true)
    {
      return true;
      paramMenu.removeItem(2131165966);
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
    case 2131165965:
    case 2131165966:
    case 2131165955:
    }
    while (true)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      FlixsterApplication.addFavoriteTheater(String.valueOf(this.mTheater.getId()));
      invalidateOptionsMenu();
      return true;
      FlixsterApplication.removeFavoriteTheater(String.valueOf(this.mTheater.getId()));
      invalidateOptionsMenu();
      return true;
      Trackers.instance().trackEvent("/theater/info", "Theater Info - " + this.mTheater.getProperty("name"), "ActionBar", "Share");
    }
  }

  public void onResume()
  {
    super.onResume();
    Theater localTheater = this.mTheater;
    String str = null;
    if (localTheater != null)
    {
      str = this.mTheater.getProperty("name");
      setActionBarTitle(str);
    }
    Trackers.instance().track("/theater/info", "Theater Info - " + str);
    ScheduleLoadItemsTask(100L);
  }

  protected void retryAction()
  {
    ScheduleLoadItemsTask(1000L);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.TheaterInfoPage
 * JD-Core Version:    0.6.2
 */