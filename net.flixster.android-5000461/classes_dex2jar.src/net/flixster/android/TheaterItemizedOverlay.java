package net.flixster.android;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.widget.Toast;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
import java.util.ArrayList;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.model.Theater;

public class TheaterItemizedOverlay extends ItemizedOverlay
{
  private Context context;
  private ArrayList<OverlayItem> overlayItems = new ArrayList();

  protected TheaterItemizedOverlay(Context paramContext, Drawable paramDrawable)
  {
    super(boundCenterBottom(paramDrawable));
    this.context = paramContext;
  }

  public void addOverlay(OverlayItem paramOverlayItem)
  {
    this.overlayItems.add(paramOverlayItem);
    populate();
  }

  public OverlayItem createItem(int paramInt)
  {
    return (OverlayItem)this.overlayItems.get(paramInt);
  }

  public boolean onTap(int paramInt)
  {
    OverlayItem localOverlayItem = (OverlayItem)this.overlayItems.get(paramInt);
    final String str = localOverlayItem.getSnippet();
    if (str != null)
    {
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.context);
      localBuilder.setMessage(localOverlayItem.getTitle());
      localBuilder.setCancelable(true);
      localBuilder.setPositiveButton("GO", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Intent localIntent = new Intent(TheaterItemizedOverlay.this.context, TheaterInfoPage.class);
          Theater localTheater = TheaterDao.getTheater(Long.parseLong(str));
          localIntent.putExtra("net.flixster.android.EXTRA_THEATER_ID", localTheater.getId());
          localIntent.putExtra("name", localTheater.getProperty("name"));
          localIntent.putExtra("state", localTheater.getProperty("state"));
          localIntent.putExtra("phone", localTheater.getProperty("phone"));
          localIntent.putExtra("street", localTheater.getProperty("street"));
          localIntent.putExtra("zip", localTheater.getProperty("zip"));
          localIntent.putExtra("city", localTheater.getProperty("city"));
          TheaterItemizedOverlay.this.context.startActivity(localIntent);
        }
      });
      localBuilder.setNegativeButton(this.context.getResources().getString(2131492938), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface.cancel();
        }
      });
      localBuilder.create().show();
    }
    while (true)
    {
      return super.onTap(paramInt);
      Toast.makeText(this.context, localOverlayItem.getTitle(), 0).show();
    }
  }

  public int size()
  {
    return this.overlayItems.size();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.TheaterItemizedOverlay
 * JD-Core Version:    0.6.2
 */