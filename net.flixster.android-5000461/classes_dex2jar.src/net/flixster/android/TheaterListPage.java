package net.flixster.android;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.utils.GoogleApiDetector;
import com.flixster.android.utils.ListHelper;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.SubNavBar;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviAd;
import net.flixster.android.lvi.LviMessagePanel;
import net.flixster.android.lvi.LviSubHeader;
import net.flixster.android.lvi.LviTheater;
import net.flixster.android.lvi.LviTheater.TheaterItemHolder;
import net.flixster.android.model.Theater;
import net.flixster.android.model.TheaterDistanceComparator;
import net.flixster.android.model.TheaterNameComparator;

public class TheaterListPage extends LviActivityCopy
{
  private static final double[] DISTANCES_IMPERIAL = { 1.0D, 3.0D, 5.0D, 10.0D, 15.0D, 20.0D };
  private static final double[] DISTANCES_METRIC = { 1.24D, 3.11D, 6.22D, 9.32D, 15.539999999999999D, 21.75D };
  public static final String KEY_THEATERS_FILTER = "KEY_THEATERS_FILTER";
  public static TheaterDistanceComparator mTheaterDistanceComparator = new TheaterDistanceComparator();
  public static TheaterNameComparator mTheaterNameComparator = new TheaterNameComparator();
  private String mDistanceString = "";
  private final ArrayList<Lvi> mFavorites = new ArrayList();
  private TextView mLocationStringTextView;
  private View.OnClickListener mNavListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      switch (paramAnonymousView.getId())
      {
      default:
        return;
      case 2131165784:
      case 2131165785:
        TheaterListPage.this.mNavSelect = paramAnonymousView.getId();
        TheaterListPage.this.mListView.setOnItemClickListener(TheaterListPage.this.getMovieItemClickListener());
        TopLevelDecorator.incrementResumeCtr();
        TheaterListPage.this.scheduleLoadItemsTask(TheaterListPage.this.mNavSelect, 100L);
        return;
      case 2131165786:
      }
      Starter.launchTheatersMap(TheaterListPage.this);
    }
  };
  private int mNavSelect;
  private View.OnClickListener mSettingsClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Logger.d("FlxMain", "TheaterListPage.mSettingsClickListener");
      Intent localIntent = new Intent(TheaterListPage.this, SettingsPage.class);
      TheaterListPage.this.startActivity(localIntent);
    }
  };
  private View.OnClickListener mTheaterClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      LviTheater.TheaterItemHolder localTheaterItemHolder = (LviTheater.TheaterItemHolder)paramAnonymousView.getTag();
      if (localTheaterItemHolder != null)
      {
        Theater localTheater = localTheaterItemHolder.theater;
        Intent localIntent = new Intent(TheaterListPage.this, TheaterInfoPage.class);
        localIntent.putExtra("net.flixster.android.EXTRA_THEATER_ID", localTheater.getId());
        TheaterListPage.this.startActivity(localIntent);
      }
    }
  };
  private final ArrayList<Theater> mTheaters = new ArrayList();
  private SubNavBar navBar;

  private void scheduleLoadItemsTask(final int paramInt, long paramLong)
  {
    try
    {
      this.throbberHandler.sendEmptyMessage(1);
      TimerTask local4 = new TimerTask()
      {
        public void run()
        {
          Logger.d("FlxMain", "TheaterListPage.ScheduleLoadItemsTask.run navSelection:" + paramInt);
          try
          {
            if (TheaterListPage.this.mTheaters.isEmpty())
            {
              localHashMap = FlixsterApplication.getFavoriteTheatersList();
              if (!FlixsterApplication.getUseLocationServiceFlag())
                break label105;
              double d1 = FlixsterApplication.getCurrentLatitude();
              double d2 = FlixsterApplication.getCurrentLongitude();
              TheaterDao.findTheatersLocation(TheaterListPage.this.mTheaters, d1, d2, localHashMap);
            }
            while (true)
            {
              boolean bool = TheaterListPage.this.shouldSkipBackgroundTask(this.val$currResumeCtr);
              if (!bool)
                break label205;
              return;
              label105: if ((FlixsterApplication.getUserLatitude() == 0.0D) || (FlixsterApplication.getUserLongitude() == 0.0D))
                break;
              TheaterDao.findTheatersLocation(TheaterListPage.this.mTheaters, FlixsterApplication.getUserLatitude(), FlixsterApplication.getUserLongitude(), localHashMap);
            }
          }
          catch (DaoException localDaoException)
          {
            while (true)
            {
              HashMap localHashMap;
              Logger.e("FlxMain", "TheaterListPage.ScheduleLoadItemsTask DaoException", localDaoException);
              TheaterListPage.this.retryLogic(localDaoException);
              return;
              TheaterDao.findTheaters(TheaterListPage.this.mTheaters, FlixsterApplication.getUserLocation(), localHashMap);
            }
          }
          finally
          {
            TheaterListPage.this.throbberHandler.sendEmptyMessage(0);
          }
          label205: switch (paramInt)
          {
          default:
          case 2131165784:
          case 2131165785:
          }
          while (true)
          {
            TheaterListPage.this.trackPage();
            TheaterListPage.this.mUpdateHandler.sendEmptyMessage(0);
            TheaterListPage.this.checkAndShowLaunchAd();
            TheaterListPage.this.throbberHandler.sendEmptyMessage(0);
            return;
            TheaterListPage.this.setByDistanceLviList();
            continue;
            TheaterListPage.this.setByNameLviList();
          }
        }
      };
      if (this.mPageTimer != null)
        this.mPageTimer.schedule(local4, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void setByDistanceLviList()
  {
    HashMap localHashMap = FlixsterApplication.getFavoriteTheatersList();
    this.mDataHolder.clear();
    this.mFavorites.clear();
    ArrayList localArrayList = ListHelper.clone(this.mTheaters);
    Collections.sort(localArrayList, mTheaterDistanceComparator);
    double d1 = 1.0D;
    double[] arrayOfDouble;
    if (FlixsterApplication.getDistanceType() == 0)
      arrayOfDouble = DISTANCES_IMPERIAL;
    double d2;
    int i;
    Iterator localIterator;
    while (true)
    {
      d2 = 0.0D;
      i = 0;
      localIterator = localArrayList.iterator();
      if (localIterator.hasNext())
        break;
      if (this.mFavorites.size() > 0)
      {
        Logger.d("FlxMain", "TheaterListPage mFavorites.size:" + this.mFavorites.size() + " mDataHolder.size():" + this.mDataHolder.size());
        LviSubHeader localLviSubHeader2 = new LviSubHeader();
        localLviSubHeader2.mTitle = getResources().getString(2131492894);
        this.mFavorites.add(0, localLviSubHeader2);
        this.mDataHolder.addAll(0, this.mFavorites);
        Logger.d("FlxMain", "TheaterListPage postadd mDataHolder.size():" + this.mDataHolder.size());
      }
      if ((localArrayList.size() == 0) && (this.mFavorites.size() == 0))
      {
        LviMessagePanel localLviMessagePanel = new LviMessagePanel();
        localLviMessagePanel.mMessage = getResources().getString(2131492953);
        this.mDataHolder.add(localLviMessagePanel);
      }
      destroyExistingLviAd();
      this.lviAd = new LviAd();
      this.lviAd.mAdSlot = "TheatersTab";
      this.mDataHolder.add(0, this.lviAd);
      return;
      arrayOfDouble = DISTANCES_METRIC;
      d1 = 1.608999967575073D;
    }
    Theater localTheater = (Theater)localIterator.next();
    double d3 = localTheater.getMiles();
    label325: label336: LviSubHeader localLviSubHeader1;
    String str2;
    Object[] arrayOfObject2;
    if (d3 > d2)
    {
      if (arrayOfDouble[i] < d3)
        break label521;
      localLviSubHeader1 = new LviSubHeader();
      if (i != arrayOfDouble.length)
        break label535;
      d2 = 1000000.0D;
      str2 = getResources().getString(2131493092);
      arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = Double.valueOf(d1 * arrayOfDouble[(-1 + arrayOfDouble.length)]);
    }
    label521: label535: String str1;
    Object[] arrayOfObject1;
    for (localLviSubHeader1.mTitle = String.format(str2, arrayOfObject2); ; localLviSubHeader1.mTitle = String.format(str1, arrayOfObject1))
    {
      this.mDataHolder.add(localLviSubHeader1);
      LviTheater localLviTheater1 = new LviTheater();
      localLviTheater1.mTheater = localTheater;
      localLviTheater1.mTheaterClick = this.mTheaterClickListener;
      localLviTheater1.mStarTheaterClick = getStarTheaterClickListener();
      this.mDataHolder.add(localLviTheater1);
      if (!localHashMap.containsKey(Long.toString(localTheater.getId())))
        break;
      LviTheater localLviTheater2 = new LviTheater();
      localLviTheater2.mTheater = localTheater;
      localLviTheater2.mTheaterClick = this.mTheaterClickListener;
      localLviTheater2.mStarTheaterClick = getStarTheaterClickListener();
      this.mFavorites.add(localLviTheater2);
      break;
      i++;
      if (i != arrayOfDouble.length)
        break label325;
      break label336;
      d2 = arrayOfDouble[i];
      str1 = getResources().getString(2131493091);
      arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = Double.valueOf(d1 * arrayOfDouble[i]);
    }
  }

  private void setByNameLviList()
  {
    HashMap localHashMap = FlixsterApplication.getFavoriteTheatersList();
    this.mDataHolder.clear();
    this.mFavorites.clear();
    ArrayList localArrayList = ListHelper.clone(this.mTheaters);
    Collections.sort(localArrayList, mTheaterNameComparator);
    char c1 = '\000';
    Iterator localIterator = localArrayList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        if (this.mFavorites.size() > 0)
        {
          LviSubHeader localLviSubHeader2 = new LviSubHeader();
          localLviSubHeader2.mTitle = getResources().getString(2131492894);
          this.mFavorites.add(0, localLviSubHeader2);
          this.mDataHolder.addAll(0, this.mFavorites);
        }
        if ((localArrayList.size() == 0) && (this.mFavorites.size() == 0))
        {
          LviMessagePanel localLviMessagePanel = new LviMessagePanel();
          localLviMessagePanel.mMessage = getResources().getString(2131492953);
          this.mDataHolder.add(localLviMessagePanel);
        }
        destroyExistingLviAd();
        this.lviAd = new LviAd();
        this.lviAd.mAdSlot = "TheatersTab";
        this.mDataHolder.add(0, this.lviAd);
        return;
      }
      Theater localTheater = (Theater)localIterator.next();
      char c2 = localTheater.getProperty("name").charAt(0);
      if (!Character.isLetter(c2))
        c2 = '#';
      if (c1 != c2)
      {
        LviSubHeader localLviSubHeader1 = new LviSubHeader();
        localLviSubHeader1.mTitle = String.valueOf(c2);
        this.mDataHolder.add(localLviSubHeader1);
        c1 = c2;
      }
      LviTheater localLviTheater1 = new LviTheater();
      localLviTheater1.mTheater = localTheater;
      localLviTheater1.mTheaterClick = this.mTheaterClickListener;
      localLviTheater1.mStarTheaterClick = getStarTheaterClickListener();
      this.mDataHolder.add(localLviTheater1);
      if (localHashMap.containsKey(Long.toString(localTheater.getId())))
      {
        LviTheater localLviTheater2 = new LviTheater();
        localLviTheater2.mTheater = localTheater;
        localLviTheater2.mTheaterClick = this.mTheaterClickListener;
        localLviTheater2.mStarTheaterClick = getStarTheaterClickListener();
        this.mFavorites.add(localLviTheater2);
      }
    }
  }

  private void setLocationString()
  {
    int i = FlixsterApplication.getTheaterDistance();
    String str1;
    if (FlixsterApplication.getUseLocationServiceFlag())
    {
      str1 = FlixsterApplication.getCurrentZip();
      if ((str1 != null) && (str1.length() != 0))
        break label73;
    }
    label73: String str2;
    Object[] arrayOfObject;
    for (this.mDistanceString = getResources().getString(2131493134); ; this.mDistanceString = String.format(str2, arrayOfObject))
    {
      this.mLocationStringTextView.setText(this.mDistanceString);
      return;
      str1 = FlixsterApplication.getUserZip();
      if ((str1 != null) && (str1.length() != 0))
        break;
      str1 = FlixsterApplication.getUserCity();
      break;
      str2 = getResources().getString(2131493094);
      arrayOfObject = new Object[2];
      arrayOfObject[0] = Integer.valueOf(i);
      arrayOfObject[1] = str1;
    }
  }

  protected String getAnalyticsAction()
  {
    return "Logo";
  }

  protected String getAnalyticsCategory()
  {
    return "WidgetEntrance";
  }

  protected String getAnalyticsTag()
  {
    switch (this.mNavSelect)
    {
    default:
      return null;
    case 2131165784:
      return "/theaters/nearby";
    case 2131165785:
    }
    return "/theaters/name";
  }

  protected String getAnalyticsTitle()
  {
    switch (this.mNavSelect)
    {
    default:
      return null;
    case 2131165784:
      return "Theaters - Nearby";
    case 2131165785:
    }
    return "Theaters - By Name";
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mListView.setOnItemClickListener(getMovieItemClickListener());
    LinearLayout localLinearLayout1 = (LinearLayout)findViewById(2131165427);
    this.navBar = new SubNavBar(this);
    SubNavBar localSubNavBar = this.navBar;
    View.OnClickListener localOnClickListener = this.mNavListener;
    if (GoogleApiDetector.instance().isVanillaAndroid());
    for (int i = 0; ; i = 2131493157)
    {
      localSubNavBar.load(localOnClickListener, 2131493155, 2131493156, i);
      this.navBar.setSelectedButton(2131165784);
      localLinearLayout1.addView(this.navBar, 0);
      LinearLayout localLinearLayout2 = (LinearLayout)findViewById(2131165429);
      View localView = LayoutInflater.from(this).inflate(2130903174, null);
      localLinearLayout2.addView(localView);
      this.mLocationStringTextView = ((TextView)localView.findViewById(2131165792));
      this.mLocationStringTextView.setOnClickListener(this.mSettingsClickListener);
      this.mNavSelect = 2131165784;
      this.mStickyTopAd.setSlot("TheatersTabStickyTop");
      this.mStickyBottomAd.setSlot("TheatersTabStickyBottom");
      return;
    }
  }

  public void onPause()
  {
    super.onPause();
    this.mTheaters.clear();
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    Logger.d("FlxMain", "TheaterListPage.onRestoreInstanceState()");
    this.mNavSelect = paramBundle.getInt("LISTSTATE_NAV");
  }

  public void onResume()
  {
    super.onResume();
    setLocationString();
    this.navBar.setSelectedButton(this.mNavSelect);
    scheduleLoadItemsTask(this.mNavSelect, 100L);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Logger.d("FlxMain", "TheaterListPage.onSaveInstanceState()");
    paramBundle.putInt("LISTSTATE_NAV", this.mNavSelect);
  }

  protected void retryAction()
  {
    scheduleLoadItemsTask(this.mNavSelect, 1000L);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.TheaterListPage
 * JD-Core Version:    0.6.2
 */