package net.flixster.android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.flixster.android.activity.common.DecoratedSherlockMapActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.ListHelper;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.model.Theater;

public class TheaterMapPage extends DecoratedSherlockMapActivity
{
  private static final int DIALOGKEY_LOADING = 1;
  private static final int MAP_ZOOM_DEFAULT = 13;
  private static final HashMap<Integer, Integer> mDistanceZoomMap = new HashMap();
  private String mDistanceString = "";
  private Dialog mLoadingDialog;
  private TextView mLocationStringTextView;
  private MapController mMapController;
  private Handler mMapInvalidateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      TheaterMapPage.this.mMapView.invalidate();
    }
  };
  private MapView mMapView;
  private LinearLayout mMapViewControls;
  protected final Handler mRemoveDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!TheaterMapPage.this.isFinishing())
        TheaterMapPage.this.removeDialog(paramAnonymousMessage.what);
    }
  };
  private View.OnClickListener mSettingsClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Logger.d("FlxMain", "TheaterListPage.mSettingsClickListener");
      Intent localIntent = new Intent(TheaterMapPage.this, SettingsPage.class);
      TheaterMapPage.this.startActivity(localIntent);
    }
  };
  protected final Handler mShowDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!TheaterMapPage.this.isFinishing())
        TheaterMapPage.this.showDialog(paramAnonymousMessage.what);
    }
  };
  private final ArrayList<Theater> mTheaters = new ArrayList();

  static
  {
    mDistanceZoomMap.put(Integer.valueOf(3), Integer.valueOf(15));
    mDistanceZoomMap.put(Integer.valueOf(5), Integer.valueOf(14));
    mDistanceZoomMap.put(Integer.valueOf(10), Integer.valueOf(13));
    mDistanceZoomMap.put(Integer.valueOf(25), Integer.valueOf(12));
    mDistanceZoomMap.put(Integer.valueOf(35), Integer.valueOf(11));
    mDistanceZoomMap.put(Integer.valueOf(50), Integer.valueOf(10));
    mDistanceZoomMap.put(Integer.valueOf(75), Integer.valueOf(9));
    mDistanceZoomMap.put(Integer.valueOf(100), Integer.valueOf(8));
    mDistanceZoomMap.put(Integer.valueOf(200), Integer.valueOf(7));
  }

  private void scheduleLoadItems()
  {
    try
    {
      this.mShowDialogHandler.sendEmptyMessage(1);
      WorkerThreads.instance().invokeLater(new Runnable()
      {
        public void run()
        {
          Logger.d("FlxMain", "TheaterMapPage.scheduleLoadItems.run");
          try
          {
            if (TheaterMapPage.this.mTheaters.isEmpty())
            {
              localHashMap = FlixsterApplication.getFavoriteTheatersList();
              if (!FlixsterApplication.getUseLocationServiceFlag())
                break label112;
              double d1 = FlixsterApplication.getCurrentLatitude();
              double d2 = FlixsterApplication.getCurrentLongitude();
              TheaterDao.findTheatersLocation(TheaterMapPage.this.mTheaters, d1, d2, localHashMap);
            }
            while (true)
            {
              TheaterMapPage.this.updateMap();
              TheaterMapPage.this.mRemoveDialogHandler.sendEmptyMessage(1);
              return;
              label112: if ((FlixsterApplication.getUserLatitude() == 0.0D) || (FlixsterApplication.getUserLongitude() == 0.0D))
                break;
              TheaterDao.findTheatersLocation(TheaterMapPage.this.mTheaters, FlixsterApplication.getUserLatitude(), FlixsterApplication.getUserLongitude(), localHashMap);
            }
          }
          catch (DaoException localDaoException)
          {
            while (true)
            {
              HashMap localHashMap;
              Logger.e("FlxMain", "TheaterMapPage.scheduleLoadItems DaoException", localDaoException);
              ErrorDialog.handleException(localDaoException, TheaterMapPage.this, TheaterMapPage.this.dialogDecorator);
              return;
              TheaterDao.findTheaters(TheaterMapPage.this.mTheaters, FlixsterApplication.getUserLocation(), localHashMap);
            }
          }
          finally
          {
            if ((TheaterMapPage.this.mLoadingDialog != null) && (TheaterMapPage.this.mLoadingDialog.isShowing()))
              TheaterMapPage.this.mRemoveDialogHandler.sendEmptyMessage(1);
          }
        }
      });
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void setLocationString()
  {
    int i = FlixsterApplication.getTheaterDistance();
    String str1;
    if (FlixsterApplication.getUseLocationServiceFlag())
    {
      str1 = FlixsterApplication.getCurrentZip();
      if ((str1 != null) && (str1.length() != 0))
        break label69;
    }
    label69: String str2;
    Object[] arrayOfObject;
    for (this.mDistanceString = getResources().getString(2131493134); ; this.mDistanceString = String.format(str2, arrayOfObject))
    {
      setActionBarTitle(this.mDistanceString);
      return;
      str1 = FlixsterApplication.getUserZip();
      if ((str1 != null) && (str1.length() != 0))
        break;
      str1 = FlixsterApplication.getUserCity();
      break;
      str2 = getResources().getString(2131493094);
      arrayOfObject = new Object[2];
      arrayOfObject[0] = Integer.valueOf(i);
      arrayOfObject[1] = str1;
    }
  }

  private void updateMap()
  {
    double d1;
    if (FlixsterApplication.getUseLocationServiceFlag())
      d1 = FlixsterApplication.getCurrentLatitude();
    for (double d2 = FlixsterApplication.getCurrentLongitude(); (d1 == 0.0D) && (d2 == 0.0D); d2 = FlixsterApplication.getUserLongitude())
    {
      Logger.e("FlxMain", "TheaterMapPage.updateMap lat = long = 0");
      return;
      d1 = FlixsterApplication.getUserLatitude();
    }
    GeoPoint localGeoPoint1 = new GeoPoint((int)(1000000.0D * d1), (int)(1000000.0D * d2));
    this.mMapController.setCenter(localGeoPoint1);
    Resources localResources = getResources();
    TheaterItemizedOverlay localTheaterItemizedOverlay1 = new TheaterItemizedOverlay(this, localResources.getDrawable(2130837677));
    localTheaterItemizedOverlay1.addOverlay(new OverlayItem(localGeoPoint1, "Current Location", null));
    TheaterItemizedOverlay localTheaterItemizedOverlay2 = new TheaterItemizedOverlay(this, localResources.getDrawable(2130837842));
    Iterator localIterator = ListHelper.clone(this.mTheaters).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        List localList = this.mMapView.getOverlays();
        localList.clear();
        localList.add(localTheaterItemizedOverlay1);
        if (localTheaterItemizedOverlay2.size() > 0)
          localList.add(localTheaterItemizedOverlay2);
        this.mMapInvalidateHandler.sendEmptyMessage(0);
        return;
      }
      Theater localTheater = (Theater)localIterator.next();
      int i = (int)(1000000.0D * localTheater.latitude);
      int j = (int)(1000000.0D * localTheater.longitude);
      GeoPoint localGeoPoint2 = new GeoPoint(i, j);
      OverlayItem localOverlayItem = new OverlayItem(localGeoPoint2, localTheater.getProperty("name") + "\n" + localTheater.getProperty("address"), String.valueOf(localTheater.getId()));
      localTheaterItemizedOverlay2.addOverlay(localOverlayItem);
    }
  }

  protected boolean isRouteDisplayed()
  {
    return false;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903172);
    createActionBar();
    this.mLocationStringTextView = ((TextView)findViewById(2131165792));
    this.mLocationStringTextView.setOnClickListener(this.mSettingsClickListener);
    this.mLocationStringTextView.setVisibility(8);
    this.mMapView = ((MapView)findViewById(2131165412));
    this.mMapController = this.mMapView.getController();
    this.mMapViewControls = ((LinearLayout)findViewById(2131165794));
    this.mMapViewControls.addView(this.mMapView.getZoomControls());
    int i = FlixsterApplication.getTheaterDistance();
    int j = -1;
    if (i > 0)
      j = ((Integer)mDistanceZoomMap.get(Integer.valueOf(FlixsterApplication.getTheaterDistance()))).intValue();
    if (j <= 0)
      j = 13;
    this.mMapController.setZoom(j);
    this.mMapView.displayZoomControls(true);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
    }
    this.mLoadingDialog = new ProgressDialog(this);
    ((ProgressDialog)this.mLoadingDialog).setMessage(getResources().getString(2131493173));
    ((ProgressDialog)this.mLoadingDialog).setIndeterminate(true);
    this.mLoadingDialog.setCancelable(true);
    this.mLoadingDialog.setCanceledOnTouchOutside(true);
    return this.mLoadingDialog;
  }

  public void onPause()
  {
    super.onPause();
    this.mTheaters.clear();
  }

  public void onResume()
  {
    super.onResume();
    setLocationString();
    scheduleLoadItems();
    Trackers.instance().track("/theaters/map", "Theaters - By Name");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.TheaterMapPage
 * JD-Core Version:    0.6.2
 */