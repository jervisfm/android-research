package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.model.Listing;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Showtimes;
import net.flixster.android.model.Theater;

public class TicketInfoPage extends DecoratedSherlockActivity
  implements View.OnClickListener
{
  public static final int DIALOGKEY_LOADING_SHOWTIMES = 1;
  public static final int DIALOGKEY_NETWORKFAIL = 2;
  private AdView mAdView;
  private Bundle mExtras;
  private LinearLayout mListingLayout;
  private LinearLayout mListingPastLayout;
  private ProgressDialog mLoadingShowtimesDialog;
  private Movie mMovie;
  private long mMovieId;
  private int mNetworkTrys = 0;
  private ImageView mPosterView;
  private RelativeLayout mShowElapsedShowtimesPanel;
  private String mShowtimesTitle;
  private Handler mStartLoadingShowtimesDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((TicketInfoPage.this.mLoadingShowtimesDialog == null) || (!TicketInfoPage.this.mLoadingShowtimesDialog.isShowing())) && (!TicketInfoPage.this.isFinishing()))
        TicketInfoPage.this.showDialog(1);
    }
  };
  private Handler mStopLoadingShowtimesDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((TicketInfoPage.this.mLoadingShowtimesDialog != null) && (TicketInfoPage.this.mLoadingShowtimesDialog.isShowing()) && (!TicketInfoPage.this.isFinishing()))
        TicketInfoPage.this.removeDialog(1);
    }
  };
  private Theater mTheater;
  private long mTheaterId;
  private TicketInfoPage mTicketInfoPage;
  private Timer mTimer;
  private Handler postShowtimesLoadHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "TicketInfoPage.postShowtimesLoadHandler");
      Object localObject = null;
      Iterator localIterator1 = ((ArrayList)TicketInfoPage.this.mTheater.getShowtimesListByMovieHash().get(Long.valueOf(TicketInfoPage.this.mMovieId))).iterator();
      Drawable localDrawable;
      Iterator localIterator2;
      if (!localIterator1.hasNext())
      {
        Logger.d("FlxMain", "TicketInfoPage.postShowtimesLoadHandler showtimes:" + localObject);
        localDrawable = TicketInfoPage.this.mTicketInfoPage.getResources().getDrawable(2130837637);
        localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
        TicketInfoPage.this.mListingLayout.removeAllViews();
        TicketInfoPage.this.mListingPastLayout.removeAllViews();
        ((TextView)TicketInfoPage.this.findViewById(2131165813)).setText(localObject.mListings.size() + " showtimes total");
        Logger.d("FlxMain", "TicketInfoPage.postShowtimesLoadHandler showtimes.mListings:" + localObject.mListings);
        localIterator2 = localObject.mListings.iterator();
      }
      while (true)
      {
        if (!localIterator2.hasNext())
        {
          if (TicketInfoPage.this.mListingPastLayout.getChildCount() != 0)
            break label473;
          TicketInfoPage.this.mShowElapsedShowtimesPanel.setVisibility(8);
          return;
          Showtimes localShowtimes = (Showtimes)localIterator1.next();
          if (!localShowtimes.mShowtimesTitle.contentEquals(TicketInfoPage.this.mShowtimesTitle))
            break;
          localObject = localShowtimes;
          break;
        }
        Listing localListing = (Listing)localIterator2.next();
        TextView localTextView = new TextView(TicketInfoPage.this.mTicketInfoPage);
        ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-1, -2);
        localTextView.setTag(localListing);
        localTextView.setLayoutParams(localLayoutParams);
        localTextView.setGravity(16);
        localTextView.setText(localListing.getDisplayTimeDate());
        localTextView.setTextAppearance(TicketInfoPage.this, 2131558512);
        if (!localListing.hasElapsed())
        {
          localTextView.setBackgroundResource(2130837621);
          localTextView.setCompoundDrawablePadding(10);
          localTextView.setCompoundDrawables(null, null, localDrawable, null);
          localTextView.setPadding(30, 20, 20, 20);
          localTextView.setOnClickListener(TicketInfoPage.this.mTicketInfoPage);
          TicketInfoPage.this.mListingLayout.addView(localTextView);
        }
        else
        {
          localTextView.setBackgroundResource(2130837811);
          localTextView.setTextColor(TicketInfoPage.this.getResources().getColor(2131296289));
          localTextView.setPadding(30, 30, 30, 20);
          TicketInfoPage.this.mListingPastLayout.addView(localTextView);
        }
      }
      label473: TicketInfoPage.this.mShowElapsedShowtimesPanel.setVisibility(0);
    }
  };

  private void ScheduleLoadShowtimesTask()
  {
    TimerTask local4 = new TimerTask()
    {
      public void run()
      {
        try
        {
          TicketInfoPage.this.mStartLoadingShowtimesDialogHandler.sendEmptyMessage(0);
          Logger.d("FlxMain", "TheaterDao.Schedule mMovie.getId():" + TicketInfoPage.this.mMovieId);
          TicketInfoPage.this.mTheater = TheaterDao.findTheaterByTheaterMoviePair(TicketInfoPage.this.mTheaterId, TicketInfoPage.this.mMovieId, FlixsterApplication.getShowtimesDate());
          Logger.d("FlxMain", "TicketInfoPage.ScheduleLoadShowtimesTask().run() mTheater.getShowtimesHash():" + TicketInfoPage.this.mTheater.getShowtimesListByMovieHash());
          TicketInfoPage.this.mStopLoadingShowtimesDialogHandler.sendEmptyMessage(0);
          TicketInfoPage.this.postShowtimesLoadHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          Logger.e("FlxMain", "TicketInfoPage.ScheduleLoadShowtimesTask Exception mNetworkTrys:" + TicketInfoPage.this.mNetworkTrys, localDaoException);
          if (TicketInfoPage.this.mNetworkTrys < 3)
          {
            TicketInfoPage.this.ScheduleLoadShowtimesTask();
            TicketInfoPage localTicketInfoPage = TicketInfoPage.this;
            localTicketInfoPage.mNetworkTrys = (1 + localTicketInfoPage.mNetworkTrys);
            return;
          }
          Logger.d("FlxMain", "TicketInfoPage.ScheduleLoadShowtimesTask SHOW NETWORK ERROR DIALOG");
          TicketInfoPage.this.mStopLoadingShowtimesDialogHandler.sendEmptyMessage(0);
        }
      }
    };
    this.mTimer.schedule(local4, 100L);
  }

  private void populateDetails()
  {
    TextView localTextView1 = (TextView)findViewById(2131165807);
    TextView localTextView2 = (TextView)findViewById(2131165804);
    TextView localTextView3 = (TextView)findViewById(2131165805);
    TextView localTextView4 = (TextView)findViewById(2131165808);
    localTextView1.setText(this.mTheater.getProperty("name"));
    localTextView4.setText(this.mTheater.getProperty("address"));
    localTextView2.setText(this.mMovie.getProperty("MOVIE_ACTORS_SHORT"));
    localTextView3.setText(this.mMovie.getProperty("meta"));
    if (this.mMovie.getThumbnailPoster() == null)
      this.mPosterView.setImageResource(2130837839);
    Bitmap localBitmap;
    do
    {
      return;
      localBitmap = this.mMovie.getThumbnailBackedProfileBitmap(this.mPosterView);
    }
    while (localBitmap == null);
    this.mPosterView.setImageBitmap(localBitmap);
  }

  public void onClick(View paramView)
  {
    if (paramView.getClass() == TextView.class)
      Starter.launchBrowser(((Listing)paramView.getTag()).ticketUrl, this);
    while (paramView.getId() != 2131165811)
      return;
    this.mListingPastLayout.setVisibility(0);
    this.mShowElapsedShowtimesPanel.setVisibility(8);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mTicketInfoPage = this;
    this.mExtras = getIntent().getExtras();
    if (this.mTimer == null)
      this.mTimer = new Timer();
    setContentView(2130903175);
    createActionBar();
    setActionBarTitle(getString(2131493014));
    TextView localTextView = (TextView)findViewById(2131165803);
    this.mPosterView = ((ImageView)findViewById(2131165801));
    this.mTheaterId = this.mExtras.getLong("id");
    this.mMovieId = this.mExtras.getLong("MOVIE_ID");
    this.mShowtimesTitle = this.mExtras.getString("SHOWTIMES_TITLE");
    this.mTheater = TheaterDao.getTheater(this.mTheaterId);
    this.mMovie = MovieDao.getMovie(this.mMovieId);
    Logger.d("FlxMain", "TicketInfoPage.onCreate mThaterId:" + this.mTheaterId + " mMovieId:" + this.mMovieId + " mShowtimesTitle:" + this.mShowtimesTitle);
    this.mAdView = ((AdView)findViewById(2131165226));
    localTextView.setText(this.mShowtimesTitle);
    populateDetails();
    this.mListingLayout = ((LinearLayout)findViewById(2131165815));
    this.mListingPastLayout = ((LinearLayout)findViewById(2131165814));
    this.mShowElapsedShowtimesPanel = ((RelativeLayout)findViewById(2131165811));
    this.mShowElapsedShowtimesPanel.setFocusable(true);
    this.mShowElapsedShowtimesPanel.setOnClickListener(this);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1:
      this.mLoadingShowtimesDialog = new ProgressDialog(this);
      this.mLoadingShowtimesDialog.setMessage(getResources().getString(2131493173));
      this.mLoadingShowtimesDialog.setIndeterminate(true);
      this.mLoadingShowtimesDialog.setCancelable(true);
      return this.mLoadingShowtimesDialog;
    case 2:
    }
    return new AlertDialog.Builder(this).setMessage("The network connection failed. Press Retry to make another attempt.").setTitle("Network Error").setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        TicketInfoPage.this.mNetworkTrys = 0;
        TicketInfoPage.this.ScheduleLoadShowtimesTask();
      }
    }).setNegativeButton(getResources().getString(2131492938), null).create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  public void onDestroy()
  {
    this.mAdView.destroy();
    super.onDestroy();
  }

  protected void onResume()
  {
    super.onResume();
    if (this.mTimer == null)
      this.mTimer = new Timer();
    Trackers.instance().track("/tickets/info", "TicketInfoPage");
    if (this.mTheater.getShowtimesListByMovieHash() == null)
      ScheduleLoadShowtimesTask();
    while (true)
    {
      this.mAdView.refreshAds();
      return;
      this.postShowtimesLoadHandler.sendEmptyMessage(0);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.TicketInfoPage
 * JD-Core Version:    0.6.2
 */