package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.lang.ref.SoftReference;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.TicketsDao;
import net.flixster.android.model.Movie;
import net.flixster.android.model.MovieTicketException;
import net.flixster.android.model.MovieTicketPurchaseException;
import net.flixster.android.model.Performance;
import net.flixster.android.util.PostalCodeUtils;

@Deprecated
public class TicketSelectPage extends DecoratedSherlockActivity
  implements View.OnKeyListener, View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemSelectedListener
{
  public static final String BUNDLE_DATE = "net.android.flixster.TicketSelectPage.BUNDLE_DATE";
  public static final String BUNDLE_DISPLAY_DATETIME = "net.android.flixster.TicketSelectPage.BUNDLE_DISPLAY_DATETIME";
  public static final String BUNDLE_LISTING = "net.android.flixster.TicketSelectPage.BUNDLE_LISTING";
  public static final String BUNDLE_MIDNIGHT_DISCLAIMER = "net.android.flixster.TicketSelectPage.BUNDLE_MIDNIGHT_DISCLAIMER";
  public static final String BUNDLE_THEATER_ID = "net.android.flixster.TicketSelectPage.BUNDLE_THEATER_ID";
  public static final String BUNDLE_TIME = "net.android.flixster.TicketSelectPage.BUNDLE_TIME";
  public static final int DIALOGKEY_LOADING_PRICING = 4;
  public static final int DIALOGKEY_NETWORKFAIL = 8;
  public static final int DIALOGKEY_PURCHASE_ERROR = 6;
  public static final int DIALOGKEY_PURCHASE_TIMEOUT_ERROR = 7;
  public static final int DIALOGKEY_PURCHASING = 5;
  public static final int DIALOGKEY_SELECT_EXPMONTH = 1;
  public static final int DIALOGKEY_SELECT_EXPYEAR = 2;
  public static final int DIALOGKEY_TICKET_AVAILABILITY = 3;
  public static final String FLOAT2PRICE_FORMAT = "$%.2f";
  public static final int MAXIMUM_TICKETS = 10;
  public static final String MONTH_FORMAT = "%02d";
  public static final int STATE_CONFIRM_RESERVATION = 2;
  public static final int STATE_DISPLAY_RECEIPT = 3;
  public static final int STATE_FIX_FIELDS = 1;
  public static final int STATE_SELECT_TICKETS = 0;
  private static final int YEAROPTIONS = 10;
  public static final String YEAR_FORMAT = "%4d";
  public final int INT = 0;
  public final int PRICE = 1;
  private Handler addTicketBars = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "TicketSelectPage.addTicketBars()");
      LayoutInflater localLayoutInflater = LayoutInflater.from(TicketSelectPage.this.mTicketSelectPage);
      TicketSelectPage.this.mTicketBarLinearLayout.removeAllViews();
      int i = TicketSelectPage.this.mPerformance.getCategoryTally();
      for (int j = 0; ; j++)
      {
        if (j >= i)
        {
          TextView localTextView2 = (TextView)TicketSelectPage.this.mTicketSelectPage.findViewById(2131165836);
          Object[] arrayOfObject2 = new Object[1];
          arrayOfObject2[0] = Float.valueOf(TicketSelectPage.this.mSurcharge);
          String str = String.format("$%.2f", arrayOfObject2);
          localTextView2.setText(String.format(TicketSelectPage.this.getResources().getString(2131493123), new Object[] { str }));
          return;
        }
        RelativeLayout localRelativeLayout = (RelativeLayout)localLayoutInflater.inflate(2130903177, TicketSelectPage.this.mTicketBarLinearLayout, false);
        TicketSelectPage.TicketbarHolder localTicketbarHolder = new TicketSelectPage.TicketbarHolder();
        localTicketbarHolder.index = Integer.valueOf(j);
        localTicketbarHolder.incButton = ((ImageButton)localRelativeLayout.findViewById(2131165890));
        localTicketbarHolder.incButton.setTag(Integer.valueOf(j));
        localTicketbarHolder.incButton.setOnClickListener(TicketSelectPage.this.mTicketSelectPage);
        localTicketbarHolder.decButton = ((ImageButton)localRelativeLayout.findViewById(2131165892));
        localTicketbarHolder.decButton.setTag(Integer.valueOf(j));
        localTicketbarHolder.decButton.setOnClickListener(TicketSelectPage.this.mTicketSelectPage);
        localTicketbarHolder.label = ((TextView)localRelativeLayout.findViewById(2131165895));
        localTicketbarHolder.label.setText(TicketSelectPage.this.mCategoryLabels[j]);
        localTicketbarHolder.price = ((TextView)localRelativeLayout.findViewById(2131165894));
        TextView localTextView1 = localTicketbarHolder.price;
        Object[] arrayOfObject1 = new Object[1];
        arrayOfObject1[0] = Float.valueOf(TicketSelectPage.this.mCategoryPrices[j]);
        localTextView1.setText(String.format("$%.2f", arrayOfObject1));
        localTicketbarHolder.counter = ((TextView)localRelativeLayout.findViewById(2131165891));
        localTicketbarHolder.subTotal = ((TextView)localRelativeLayout.findViewById(2131165893));
        localRelativeLayout.setTag(localTicketbarHolder);
        TicketSelectPage.this.mTicketBarLinearLayout.addView(localRelativeLayout);
      }
    }
  };
  private Drawable mAlertIconCheck;
  private Drawable mAlertIconError;
  private int[] mCardMapType;
  private int mCardMonth = -1;
  private int mCardNumberFieldState = 0;
  private int mCardType = 0;
  private int mCardTypeIndex = 0;
  private int mCardYear = -100;
  private CharSequence[] mCardYearOptions;
  private int mCategories;
  private String[] mCategoryLabels;
  private float[] mCategoryPrices;
  private int mCodeFieldState = 0;
  Button mCompletePurchaseButton;
  private LinearLayout mConfirmReservationLayout;
  Button mContinueButton;
  private float mConvenienceTotal = 0.0F;
  private TextView mConvenienceTotalView;
  private int mEmailFieldState = 0;
  Pattern mEmailPattern;
  private TextView mErrorCardnumberText;
  private TextView mErrorCodenumberText;
  private TextView mErrorEmailText;
  private TextView mErrorExpDateText;
  private TextView mErrorPostalnumberText;
  private TextView mErrorTicketCountText;
  private RelativeLayout mErrorTicketText;
  private int mExpDateFieldState = 0;
  private Bundle mExtras;
  private int mFetchPerformanceTrys = 0;
  private ProgressDialog mLoadingPricingDialog;
  private String[] mMethodIds;
  private String[] mMethodNames;
  private Movie mMovie;
  private long mMovieId;
  private String mMovieTitle;
  private String mParamDate;
  private String mParamDisplayDatetime;
  private String mParamListing;
  private String mParamTheater;
  private String mParamTime;
  private Performance mPerformance;
  private int mPostalFieldState = 0;
  private ImageView mPosterView;
  private ProgressDialog mPurchaseDialog;
  private Dialog mPurchaseError;
  private Handler mPurchaseErrorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!TicketSelectPage.this.isFinishing())
        TicketSelectPage.this.showDialog(6);
    }
  };
  private volatile String mPurchaseErrorMessage;
  private LinearLayout mReceiptInfoLayout;
  private AdView mScrollAd;
  private ScrollView mScrollView;
  private LinearLayout mSelectTicketsLayout;
  private Handler mSelectToReservationFieldHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      ((TextView)TicketSelectPage.this.mTicketSelectPage.findViewById(2131165887)).setText(TicketSelectPage.this.mTicketEmail.getText());
      ((TextView)TicketSelectPage.this.mTicketSelectPage.findViewById(2131165875)).setText(TicketSelectPage.access$0(TicketSelectPage.this).mMovieTitle);
      ((TextView)TicketSelectPage.this.mTicketSelectPage.findViewById(2131165877)).setText(TicketSelectPage.access$0(TicketSelectPage.this).mTheaterName);
      TextView localTextView1 = (TextView)TicketSelectPage.this.mTicketSelectPage.findViewById(2131165880);
      StringBuilder localStringBuilder1 = new StringBuilder();
      if (TicketSelectPage.this.mTicketCounts != null);
      for (int i = 0; ; i++)
      {
        if (i >= TicketSelectPage.this.mCategories)
        {
          localTextView1.setText(localStringBuilder1);
          TextView localTextView2 = (TextView)TicketSelectPage.this.mTicketSelectPage.findViewById(2131165881);
          Object[] arrayOfObject1 = new Object[1];
          arrayOfObject1[0] = Float.valueOf(TicketSelectPage.this.mConvenienceTotal);
          localTextView2.setText(String.format("$%.2f", arrayOfObject1));
          TextView localTextView3 = (TextView)TicketSelectPage.this.mTicketSelectPage.findViewById(2131165882);
          Object[] arrayOfObject2 = new Object[1];
          arrayOfObject2[0] = Float.valueOf(TicketSelectPage.this.mTotal);
          localTextView3.setText(String.format("$%.2f", arrayOfObject2));
          if (TicketSelectPage.this.mMethodNames != null)
            ((TextView)TicketSelectPage.this.mTicketSelectPage.findViewById(2131165883)).setText(TicketSelectPage.this.mMethodNames[TicketSelectPage.this.mCardTypeIndex]);
          TextView localTextView4 = (TextView)TicketSelectPage.this.mTicketSelectPage.findViewById(2131165884);
          String str1 = TicketSelectPage.this.mTicketCardNumber.getText().toString();
          String str2 = str1.substring(Math.max(0, -4 + str1.length()));
          localTextView4.setText("************" + str2);
          TextView localTextView5 = (TextView)TicketSelectPage.this.mTicketSelectPage.findViewById(2131165886);
          Object[] arrayOfObject3 = new Object[1];
          arrayOfObject3[0] = Integer.valueOf(1 + TicketSelectPage.this.mCardMonth);
          StringBuilder localStringBuilder2 = new StringBuilder(String.valueOf(String.format("%02d", arrayOfObject3))).append("/");
          Object[] arrayOfObject4 = new Object[1];
          arrayOfObject4[0] = Integer.valueOf(TicketSelectPage.this.mThisYear + TicketSelectPage.this.mCardYear);
          localTextView5.setText(String.format("%4d", arrayOfObject4));
          return;
        }
        if (TicketSelectPage.this.mTicketCounts[i] > 0.0F)
        {
          if (localStringBuilder1.length() > 0)
            localStringBuilder1.append("\n");
          StringBuilder localStringBuilder3 = localStringBuilder1.append((int)TicketSelectPage.this.mTicketCounts[i]).append("× ").append(TicketSelectPage.this.mCategoryLabels[i]).append(" / ");
          Object[] arrayOfObject5 = new Object[1];
          arrayOfObject5[0] = Float.valueOf(TicketSelectPage.this.mCategoryPrices[i]);
          localStringBuilder3.append(String.format("$%.2f", arrayOfObject5));
        }
      }
    }
  };
  private Handler mShowNetorkErrorDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!TicketSelectPage.this.isFinishing())
        TicketSelectPage.this.showDialog(8);
    }
  };
  private Handler mShowPurchaseErrorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((TicketSelectPage.this.mTicketAvailabilityDialog == null) || (!TicketSelectPage.this.mTicketAvailabilityDialog.isShowing())) && (!TicketSelectPage.this.isFinishing()))
        TicketSelectPage.this.showDialog(6);
    }
  };
  private Handler mShowPurchaseTimeoutErrorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((TicketSelectPage.this.mTicketAvailabilityDialog == null) || (!TicketSelectPage.this.mTicketAvailabilityDialog.isShowing())) && (!TicketSelectPage.this.isFinishing()))
        TicketSelectPage.this.showDialog(7);
    }
  };
  private TextView mShowtimeTextView;
  private Handler mStartGetReservationDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((TicketSelectPage.this.mTicketAvailabilityDialog == null) || (!TicketSelectPage.this.mTicketAvailabilityDialog.isShowing())) && (!TicketSelectPage.this.isFinishing()))
        TicketSelectPage.this.showDialog(3);
    }
  };
  private Handler mStartLoadPricingDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((TicketSelectPage.this.mLoadingPricingDialog == null) || (!TicketSelectPage.this.mLoadingPricingDialog.isShowing())) && (!TicketSelectPage.this.isFinishing()))
      {
        TicketSelectPage.this.showDialog(4);
        TicketSelectPage.this.mLoadingPricingDialog.onStart();
      }
    }
  };
  private Handler mStartPurchaseDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((TicketSelectPage.this.mPurchaseDialog == null) || (!TicketSelectPage.this.mPurchaseDialog.isShowing())) && (!TicketSelectPage.this.isFinishing()))
        TicketSelectPage.this.showDialog(5);
    }
  };
  private int mState = 0;
  private Handler mStateLayout = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (TicketSelectPage.this.mState)
      {
      default:
      case 0:
      case 1:
      case 2:
      case 3:
      }
      while (true)
      {
        TicketSelectPage.this.mScrollView.smoothScrollTo(0, 0);
        return;
        TicketSelectPage.this.mSelectTicketsLayout.setVisibility(0);
        TicketSelectPage.this.mConfirmReservationLayout.setVisibility(8);
        TicketSelectPage.this.mErrorTicketText.setVisibility(8);
        TicketSelectPage.this.mReceiptInfoLayout.setVisibility(8);
        Trackers.instance().track("/tickets/select", "Select");
        continue;
        TicketSelectPage.this.mErrorTicketText.setVisibility(0);
        TicketSelectPage.this.hardValidateAll();
        TicketSelectPage.this.updatePrice.sendEmptyMessage(0);
        TicketSelectPage.this.updateCardStatus.sendEmptyMessage(0);
        TicketSelectPage.this.mConfirmReservationLayout.setVisibility(8);
        TicketSelectPage.this.mReceiptInfoLayout.setVisibility(8);
        Trackers.instance().track("/tickets/fix", "Fix");
        continue;
        TicketSelectPage.this.mSelectToReservationFieldHandler.sendEmptyMessage(0);
        TicketSelectPage.this.mSelectTicketsLayout.setVisibility(8);
        TicketSelectPage.this.mConfirmReservationLayout.setVisibility(0);
        TicketSelectPage.this.mReceiptInfoLayout.setVisibility(8);
        Trackers.instance().track("/tickets/confirm", "Confirm");
        continue;
        TicketSelectPage.this.mCompletePurchaseButton.setText(TicketSelectPage.this.mTicketSelectPage.getResources().getString(2131493062));
        TicketSelectPage.this.mSelectTicketsLayout.setVisibility(8);
        TicketSelectPage.this.mTicketsConfirmMessage.setVisibility(8);
        TicketSelectPage.this.mConfirmReservationLayout.setVisibility(0);
        TicketSelectPage.this.mReceiptInfoLayout.setVisibility(0);
        Trackers.instance().track("/tickets/receipt", "Receipt");
      }
    }
  };
  private Handler mStopGetReservationDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((TicketSelectPage.this.mTicketAvailabilityDialog != null) && (TicketSelectPage.this.mTicketAvailabilityDialog.isShowing()) && (!TicketSelectPage.this.isFinishing()))
        TicketSelectPage.this.removeDialog(3);
    }
  };
  private Handler mStopLoadPricingDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((TicketSelectPage.this.mLoadingPricingDialog != null) && (TicketSelectPage.this.mLoadingPricingDialog.isShowing()) && (!TicketSelectPage.this.isFinishing()));
      try
      {
        TicketSelectPage.this.dismissDialog(4);
        return;
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
    }
  };
  private Handler mStopPurchaseDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((TicketSelectPage.this.mPurchaseDialog != null) && (TicketSelectPage.this.mPurchaseDialog.isShowing()) && (!TicketSelectPage.this.isFinishing()))
        TicketSelectPage.this.removeDialog(5);
    }
  };
  private float mSurcharge = 1.0F;
  private String mTheaterAddress;
  private String mTheaterName;
  private int mThisMonth = -1;
  private int mThisYear = -1;
  private ProgressDialog mTicketAvailabilityDialog;
  private LinearLayout mTicketBarLinearLayout;
  private TextView mTicketCardNumber;
  private TextView mTicketCodeNumber;
  private TextView mTicketConfirmationNumber;
  private float mTicketCount = 0.0F;
  private int mTicketCountState = 0;
  private float[] mTicketCounts;
  private TextView mTicketEmail;
  private TextView mTicketExpDateLabel;
  private TextView mTicketExpMonth;
  private TextView mTicketExpYear;
  private TextView mTicketPostalNumber;
  private TicketSelectPage mTicketSelectPage;
  private TextView mTicketSelectShowtime;
  private TextView mTicketSelectTheaterAddress;
  private TextView mTicketSelectTheaterName;
  private TextView mTicketSelectTitle;
  private float[] mTicketTotals = { 0.0F, 0.0F, 0.0F };
  private TextView mTicketsConfirmMessage;
  Timer mTimer;
  private float mTotal = 0.0F;
  private TextView mTotalView;
  private Handler setupCardSpinner = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Spinner localSpinner = (Spinner)TicketSelectPage.this.findViewById(2131165839);
      ArrayAdapter localArrayAdapter = new ArrayAdapter(TicketSelectPage.this.mTicketSelectPage, 17367048, TicketSelectPage.this.mMethodNames);
      localArrayAdapter.setDropDownViewResource(17367049);
      localSpinner.setAdapter(localArrayAdapter);
      localSpinner.setOnItemSelectedListener(TicketSelectPage.this.mTicketSelectPage);
      localSpinner.setSelection(TicketSelectPage.this.mCardTypeIndex);
    }
  };
  private Handler updateCardStatus = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "TicketSelectPage.updateCardStatus");
      switch (TicketSelectPage.this.mCardNumberFieldState)
      {
      default:
        TicketSelectPage.this.mTicketCardNumber.setCompoundDrawables(null, null, null, null);
        TicketSelectPage.this.mErrorCardnumberText.setVisibility(8);
        label62: switch (TicketSelectPage.this.mCodeFieldState)
        {
        default:
          TicketSelectPage.this.mTicketCodeNumber.setCompoundDrawables(null, null, null, null);
          TicketSelectPage.this.mErrorCodenumberText.setVisibility(8);
          label118: switch (TicketSelectPage.this.mPostalFieldState)
          {
          default:
            TicketSelectPage.this.mTicketPostalNumber.setCompoundDrawables(null, null, null, null);
            TicketSelectPage.this.mErrorPostalnumberText.setVisibility(8);
            if (TicketSelectPage.this.mCardMonth != -1)
            {
              TextView localTextView2 = TicketSelectPage.access$0(TicketSelectPage.this).mTicketExpMonth;
              Object[] arrayOfObject2 = new Object[1];
              arrayOfObject2[0] = Integer.valueOf(1 + TicketSelectPage.this.mCardMonth);
              localTextView2.setText(String.format("%02d", arrayOfObject2));
            }
            if (TicketSelectPage.this.mCardYear != -100)
            {
              TextView localTextView1 = TicketSelectPage.access$0(TicketSelectPage.this).mTicketExpYear;
              Object[] arrayOfObject1 = new Object[1];
              arrayOfObject1[0] = Integer.valueOf(TicketSelectPage.this.mCardYear + TicketSelectPage.this.mThisYear);
              localTextView1.setText(String.format("%4d", arrayOfObject1));
            }
            switch (TicketSelectPage.this.mExpDateFieldState)
            {
            default:
              TicketSelectPage.this.mTicketExpDateLabel.setCompoundDrawables(null, null, null, null);
              TicketSelectPage.this.mErrorExpDateText.setVisibility(8);
            case 3:
            case 2:
            }
            break;
          case 3:
          case 2:
          }
          break;
        case 3:
        case 2:
        }
        label174: break;
      case 3:
      case 2:
      }
      while (true)
        switch (TicketSelectPage.this.mEmailFieldState)
        {
        default:
          TicketSelectPage.this.mTicketEmail.setCompoundDrawables(null, null, null, null);
          TicketSelectPage.this.mErrorEmailText.setVisibility(8);
          return;
          TicketSelectPage.this.mTicketCardNumber.setCompoundDrawables(null, null, TicketSelectPage.this.mAlertIconError, null);
          break label62;
          TicketSelectPage.this.mTicketCardNumber.setCompoundDrawables(null, null, TicketSelectPage.this.mAlertIconCheck, null);
          TicketSelectPage.this.mErrorCardnumberText.setVisibility(8);
          break label62;
          TicketSelectPage.this.mTicketCodeNumber.setCompoundDrawables(null, null, TicketSelectPage.this.mAlertIconError, null);
          break label118;
          TicketSelectPage.this.mTicketCodeNumber.setCompoundDrawables(null, null, TicketSelectPage.this.mAlertIconCheck, null);
          TicketSelectPage.this.mErrorCodenumberText.setVisibility(8);
          break label118;
          TicketSelectPage.this.mTicketPostalNumber.setCompoundDrawables(null, null, TicketSelectPage.this.mAlertIconError, null);
          break label174;
          TicketSelectPage.this.mTicketPostalNumber.setCompoundDrawables(null, null, TicketSelectPage.this.mAlertIconCheck, null);
          TicketSelectPage.this.mErrorPostalnumberText.setVisibility(8);
          break label174;
          TicketSelectPage.this.mTicketExpDateLabel.setCompoundDrawables(null, null, TicketSelectPage.this.mAlertIconError, null);
          continue;
          TicketSelectPage.this.mTicketExpDateLabel.setCompoundDrawables(null, null, TicketSelectPage.this.mAlertIconCheck, null);
          TicketSelectPage.this.mErrorExpDateText.setVisibility(8);
        case 3:
        case 2:
        }
      TicketSelectPage.this.mTicketEmail.setCompoundDrawables(null, null, TicketSelectPage.this.mAlertIconError, null);
      return;
      TicketSelectPage.this.mTicketEmail.setCompoundDrawables(null, null, TicketSelectPage.this.mAlertIconCheck, null);
      TicketSelectPage.this.mErrorEmailText.setVisibility(8);
    }
  };
  private Handler updatePrice = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "TicketSelectPage.updatePrice:");
      try
      {
        TicketSelectPage.this.mTotal = 0.0F;
        TicketSelectPage.this.mTicketCount = 0.0F;
        int i = TicketSelectPage.this.mPerformance.getCategoryTally();
        int j = 0;
        int k;
        if (j >= i)
        {
          TicketSelectPage.this.mConvenienceTotal = (TicketSelectPage.this.mTicketCount * TicketSelectPage.this.mSurcharge);
          TicketSelectPage localTicketSelectPage1 = TicketSelectPage.this;
          localTicketSelectPage1.mTotal += TicketSelectPage.this.mConvenienceTotal;
          k = 0;
          label92: if (k < TicketSelectPage.this.mCategoryPrices.length)
            break label328;
          TextView localTextView2 = TicketSelectPage.this.mConvenienceTotalView;
          Object[] arrayOfObject2 = new Object[1];
          arrayOfObject2[0] = Float.valueOf(TicketSelectPage.this.mConvenienceTotal);
          localTextView2.setText(String.format("$%.2f", arrayOfObject2));
          TextView localTextView3 = TicketSelectPage.this.mTotalView;
          Object[] arrayOfObject3 = new Object[1];
          arrayOfObject3[0] = Float.valueOf(TicketSelectPage.this.mTotal);
          localTextView3.setText(String.format("$%.2f", arrayOfObject3));
          if (TicketSelectPage.this.mTicketCountState != 3)
            break label449;
          if (TicketSelectPage.this.mTicketCount != 0.0F)
            break label421;
          TicketSelectPage.this.mErrorTicketCountText.setText(2131493063, TextView.BufferType.NORMAL);
        }
        while (true)
        {
          TicketSelectPage.this.mErrorTicketCountText.setVisibility(0);
          return;
          TicketSelectPage.this.mTicketTotals[j] = (TicketSelectPage.this.mCategoryPrices[j] * TicketSelectPage.this.mTicketCounts[j]);
          TicketSelectPage localTicketSelectPage2 = TicketSelectPage.this;
          localTicketSelectPage2.mTotal += TicketSelectPage.this.mTicketTotals[j];
          TicketSelectPage localTicketSelectPage3 = TicketSelectPage.this;
          localTicketSelectPage3.mTicketCount += TicketSelectPage.this.mTicketCounts[j];
          j++;
          break;
          label328: TicketSelectPage.TicketbarHolder localTicketbarHolder = (TicketSelectPage.TicketbarHolder)((RelativeLayout)TicketSelectPage.this.mTicketBarLinearLayout.getChildAt(k)).getTag();
          localTicketbarHolder.counter.setText(Integer.toString((int)TicketSelectPage.this.mTicketCounts[k]));
          TextView localTextView1 = localTicketbarHolder.subTotal;
          Object[] arrayOfObject1 = new Object[1];
          arrayOfObject1[0] = Float.valueOf(TicketSelectPage.this.mTicketTotals[k]);
          localTextView1.setText(String.format("$%.2f", arrayOfObject1));
          k++;
          break label92;
          label421: TicketSelectPage.this.mErrorTicketCountText.setText(2131493064, TextView.BufferType.NORMAL);
        }
      }
      catch (Exception localException)
      {
        Logger.e("FlxMain", "TicketSelectPage updatePrice problem", localException);
        return;
      }
      label449: TicketSelectPage.this.mErrorTicketCountText.setVisibility(8);
    }
  };

  private void ScheduleGetReservation()
  {
    Logger.d("FlxMain", "TicketSelectPage.ScheduleTicketAvailabilityCheck() define mTimer:" + this.mTimer);
    TimerTask local18 = new TimerTask()
    {
      public void run()
      {
        Logger.d("FlxMain", "TicketSelectPage.ScheduleTicketAvailabilityCheck() run");
        try
        {
          String str1 = TicketSelectPage.this.mParamTheater;
          String str2 = TicketSelectPage.this.mParamListing;
          String str3 = TicketSelectPage.this.mParamDate;
          String str4 = TicketSelectPage.this.mParamTime;
          String str5 = TicketSelectPage.this.mMethodIds[TicketSelectPage.this.mCardTypeIndex];
          String str6 = TicketSelectPage.this.mTicketCardNumber.getText().toString();
          String str7 = TicketSelectPage.this.mTicketCodeNumber.getText().toString();
          String str8 = PostalCodeUtils.parseZipcodeForShowtimes(TicketSelectPage.this.mTicketPostalNumber.getText().toString());
          Object[] arrayOfObject1 = new Object[1];
          arrayOfObject1[0] = Integer.valueOf(TicketSelectPage.this.mCardYear + TicketSelectPage.this.mThisYear);
          StringBuilder localStringBuilder = new StringBuilder(String.valueOf(String.format("%4d", arrayOfObject1)));
          Object[] arrayOfObject2 = new Object[1];
          arrayOfObject2[0] = Integer.valueOf(1 + TicketSelectPage.this.mCardMonth);
          TicketsDao.getReservation(str1, str2, str3, str4, str5, str6, str7, str8, String.format("%02d", arrayOfObject2), TicketSelectPage.this.mTicketEmail.getText().toString(), TicketSelectPage.this.mPerformance, TicketSelectPage.this.mTicketCounts);
          TicketSelectPage.this.mStopGetReservationDialogHandler.sendEmptyMessage(0);
          if ((TicketSelectPage.this.mPerformance.getError() != null) || (TicketSelectPage.this.mEmailFieldState != 2))
          {
            TicketSelectPage.this.mState = 1;
            TicketSelectPage.this.mStateLayout.sendEmptyMessage(0);
            return;
          }
          Logger.d("FlxMain", "TicketSelectPage.ScheduleGetReservation() run post dao");
          TicketSelectPage.this.mState = 2;
          TicketSelectPage.this.mStateLayout.sendEmptyMessage(0);
          return;
        }
        catch (Exception localException)
        {
          Logger.e("FlxMain", "TicketSelectPage.ScheduleTicketAvailabilityCheck() Exception", localException);
          TicketSelectPage.this.mStopGetReservationDialogHandler.sendEmptyMessage(0);
        }
      }
    };
    if (this.mTimer != null)
      this.mTimer.schedule(local18, 100L);
  }

  private void ScheduleLoadPerformanceTask()
  {
    Logger.d("FlxMain", "TicketSelectPage.ScheduleLoadPerformanceTask() define mTimer:" + this.mTimer);
    TimerTask local17 = new TimerTask()
    {
      public void run()
      {
        Logger.d("FlxMain", "TicketSelectPage.ScheduleLoadPerformanceTask() run");
        try
        {
          TicketSelectPage.this.mPerformance = TicketsDao.getMoviePerformance(TicketSelectPage.this.mParamTheater, TicketSelectPage.this.mParamListing, TicketSelectPage.this.mParamDate, TicketSelectPage.this.mParamTime);
          Logger.d("FlxMain", "TicketSelectPage.ScheduleLoadPerformanceTask() mPerformance:" + TicketSelectPage.this.mPerformance);
          TicketSelectPage.this.mCategories = TicketSelectPage.this.mPerformance.getCategoryTally();
          TicketSelectPage.this.mCategoryLabels = TicketSelectPage.this.mPerformance.getLabels();
          TicketSelectPage.this.mCategoryPrices = TicketSelectPage.this.mPerformance.getPrices();
          TicketSelectPage.this.mTicketTotals = new float[TicketSelectPage.this.mCategories];
          TicketSelectPage.this.mTicketCounts = new float[TicketSelectPage.this.mCategories];
          TicketSelectPage.this.mSurcharge = TicketSelectPage.this.mPerformance.getSurcharge();
          TicketSelectPage.this.mMethodIds = TicketSelectPage.this.mPerformance.getMethodIds();
          TicketSelectPage.this.mMethodNames = TicketSelectPage.this.mPerformance.getMethodNames();
          TicketSelectPage.this.mCardMapType = TicketsDao.makeCardTypeMap(TicketSelectPage.this.mMethodIds);
          TicketSelectPage.this.mCardType = TicketSelectPage.this.mCardMapType[0];
          TicketSelectPage.this.addTicketBars.sendEmptyMessage(0);
          Logger.d("FlxMain", "TicketSelectPage.ScheduleLoadPerformanceTask() time to call updatePrice/updateCardStatus");
          TicketSelectPage.this.setupCardSpinner.sendEmptyMessage(0);
          TicketSelectPage.this.updatePrice.sendEmptyMessage(0);
          TicketSelectPage.this.updateCardStatus.sendEmptyMessage(0);
          TicketSelectPage.this.mStopLoadPricingDialogHandler.sendEmptyMessage(0);
          return;
        }
        catch (MovieTicketException localMovieTicketException)
        {
          TicketSelectPage.this.mPurchaseErrorMessage = localMovieTicketException.getMessage();
          TicketSelectPage.this.mStopLoadPricingDialogHandler.sendEmptyMessage(0);
          TicketSelectPage.this.mShowPurchaseTimeoutErrorHandler.sendEmptyMessage(0);
          return;
        }
        catch (Exception localException)
        {
          Logger.e("FlxMain", "TicketSelectPage.ScheduleLoadPerformanceTask() IOException", localException);
          if (TicketSelectPage.this.mFetchPerformanceTrys < 3)
          {
            TicketSelectPage localTicketSelectPage = TicketSelectPage.this;
            localTicketSelectPage.mFetchPerformanceTrys = (1 + localTicketSelectPage.mFetchPerformanceTrys);
            TicketSelectPage.this.ScheduleLoadPerformanceTask();
            return;
          }
          TicketSelectPage.this.mStopLoadPricingDialogHandler.sendEmptyMessage(0);
          TicketSelectPage.this.mShowNetorkErrorDialogHandler.sendEmptyMessage(0);
        }
      }
    };
    if (this.mTimer != null)
      this.mTimer.schedule(local17, 100L);
  }

  private void SchedulePurchase()
  {
    Logger.d("FlxMain", "TicketSelectPage.SchedulePurchase define mTimer:" + this.mTimer);
    TimerTask local19 = new TimerTask()
    {
      public void run()
      {
        Logger.d("FlxMain", "TicketSelectPage.ScheduleTicketAvailabilityCheck() run");
        try
        {
          String str = TicketsDao.purchaseTickets(TicketSelectPage.this.mPerformance.getReservationId(), TicketSelectPage.this.mTicketEmail.getText().toString());
          TicketSelectPage.this.mTicketConfirmationNumber.setText(str);
          TicketSelectPage.this.mStopPurchaseDialogHandler.sendEmptyMessage(0);
          if (str == null)
          {
            TicketSelectPage.this.mPurchaseErrorHandler.sendEmptyMessage(0);
            return;
          }
          TicketSelectPage.this.mState = 3;
          TicketSelectPage.this.mStateLayout.sendEmptyMessage(0);
          return;
        }
        catch (MovieTicketPurchaseException localMovieTicketPurchaseException)
        {
          Logger.d("FlxMain", "TicketsDao. throw timeout error e:" + localMovieTicketPurchaseException);
          TicketSelectPage.this.mPurchaseErrorMessage = localMovieTicketPurchaseException.getMessage();
          TicketSelectPage.this.mShowPurchaseTimeoutErrorHandler.sendEmptyMessage(0);
          return;
        }
        catch (MovieTicketException localMovieTicketException)
        {
          Logger.d("FlxMain", "TicketsDao. throw standard error e:" + localMovieTicketException);
          TicketSelectPage.this.mPurchaseErrorMessage = localMovieTicketException.getMessage();
          TicketSelectPage.this.mShowPurchaseErrorHandler.sendEmptyMessage(0);
          return;
        }
        catch (Exception localException)
        {
          Logger.e("FlxMain", "TicketSelectPage.ScheduleTicketAvailabilityCheck() Exception", localException);
          TicketSelectPage.this.mStopPurchaseDialogHandler.sendEmptyMessage(0);
        }
      }
    };
    if (this.mTimer != null)
      this.mTimer.schedule(local19, 100L);
  }

  private void hardValidateAll()
  {
    if (this.mCardNumberFieldState != 2)
      this.mCardNumberFieldState = 3;
    if (this.mCodeFieldState != 2)
      this.mCodeFieldState = 3;
    if (this.mPostalFieldState != 2)
      this.mPostalFieldState = 3;
    if (this.mExpDateFieldState != 2)
      this.mExpDateFieldState = 3;
    if (this.mEmailFieldState != 2)
      this.mEmailFieldState = 3;
    if (this.mTicketCount == 0.0F)
      this.mTicketCountState = 3;
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
    case 2131165890:
    case 2131165892:
    case 2131165844:
    case 2131165845:
    case 2131165859:
    case 2131165888:
    }
    do
    {
      return;
      Integer localInteger2 = (Integer)paramView.getTag();
      if (localInteger2 == null)
        throw new NullPointerException("TicketSelect.onClick inc button without tag");
      if (this.mTicketCount < 10.0F)
      {
        float[] arrayOfFloat2 = this.mTicketCounts;
        int j = localInteger2.intValue();
        arrayOfFloat2[j] = (1.0F + arrayOfFloat2[j]);
      }
      for (this.mTicketCountState = 2; ; this.mTicketCountState = 3)
      {
        this.updatePrice.sendEmptyMessage(0);
        return;
      }
      Integer localInteger1 = (Integer)paramView.getTag();
      if (localInteger1 == null)
        throw new NullPointerException("TicketSelect.onClick dec button without tag");
      if (this.mTicketCounts[localInteger1.intValue()] > 0.0F)
      {
        float[] arrayOfFloat1 = this.mTicketCounts;
        int i = localInteger1.intValue();
        arrayOfFloat1[i] -= 1.0F;
        this.mTicketCountState = 2;
      }
      this.updatePrice.sendEmptyMessage(0);
      return;
      Logger.d("FlxMain", "TicketSelectPage.onClick ticket_expmonth");
      showDialog(1);
      return;
      Logger.d("FlxMain", "TicketSelectPage.onClick ticket_expyear");
      showDialog(2);
      return;
      Logger.d("FlxMain", "TicketSelectPage.onClick ticket_continue_button");
      FlixsterApplication.setTicketEmail(this.mTicketEmail.getText().toString());
      if (this.mPerformance != null)
        this.mPerformance.clearError();
      this.mEmailFieldState = TicketsDao.validateEmail(this.mTicketEmail.getText().toString());
      this.mStartGetReservationDialogHandler.sendEmptyMessage(0);
      ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(this.mTicketEmail.getWindowToken(), 0);
      ScheduleGetReservation();
      return;
      if (this.mState == 2)
      {
        Logger.d("FlxMain", "TicketSelectPage.onClick ticket_completepurchase_button");
        this.mStartPurchaseDialogHandler.sendEmptyMessage(0);
        SchedulePurchase();
        return;
      }
    }
    while (this.mState != 3);
    finish();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mTicketSelectPage = this;
    setContentView(2130903176);
    createActionBar();
    setActionBarTitle(getString(2131493016));
    this.mScrollAd = ((AdView)findViewById(2131165226));
    this.mExtras = getIntent().getExtras();
    Logger.d("FlxMain", "TicketSelectPage.onCreate mExtras:" + this.mExtras);
    if (this.mExtras != null)
    {
      this.mParamTheater = this.mExtras.getString("net.android.flixster.TicketSelectPage.BUNDLE_THEATER_ID");
      this.mParamListing = this.mExtras.getString("net.android.flixster.TicketSelectPage.BUNDLE_LISTING");
      this.mParamDate = this.mExtras.getString("net.android.flixster.TicketSelectPage.BUNDLE_DATE");
      this.mParamTime = this.mExtras.getString("net.android.flixster.TicketSelectPage.BUNDLE_TIME");
      this.mParamDisplayDatetime = this.mExtras.getString("net.android.flixster.TicketSelectPage.BUNDLE_DISPLAY_DATETIME");
      this.mMovieTitle = this.mExtras.getString("SHOWTIMES_TITLE");
      this.mTheaterName = this.mExtras.getString("name");
      this.mTheaterAddress = this.mExtras.getString("address");
      this.mMovieId = this.mExtras.getLong("MOVIE_ID");
      Logger.d("FlxMain", "TicketSelectPage.onCreate mParamTime:" + this.mParamTime);
    }
    this.mScrollView = ((ScrollView)findViewById(2131165818));
    this.mSelectTicketsLayout = ((LinearLayout)findViewById(2131165819));
    this.mConfirmReservationLayout = ((LinearLayout)findViewById(2131165860));
    this.mReceiptInfoLayout = ((LinearLayout)findViewById(2131165862));
    this.mTicketsConfirmMessage = ((TextView)findViewById(2131165871));
    this.mStateLayout.sendEmptyMessage(0);
    this.mTicketSelectTitle = ((TextView)findViewById(2131165803));
    this.mTicketSelectTitle.setText(this.mMovieTitle);
    this.mTicketSelectTheaterName = ((TextView)findViewById(2131165807));
    this.mTicketSelectShowtime = ((TextView)findViewById(2131165878));
    this.mTicketSelectShowtime.setText(this.mParamDisplayDatetime);
    this.mTicketSelectTheaterName.setText(this.mTheaterName);
    this.mTicketSelectTheaterAddress = ((TextView)findViewById(2131165808));
    this.mTicketSelectTheaterAddress.setText(this.mTheaterAddress);
    this.mShowtimeTextView = ((TextView)findViewById(2131165826));
    this.mShowtimeTextView.setText(this.mParamDisplayDatetime);
    TextView localTextView1 = (TextView)findViewById(2131165827);
    TextView localTextView2 = (TextView)findViewById(2131165879);
    String str1 = this.mExtras.getString("net.android.flixster.TicketSelectPage.BUNDLE_MIDNIGHT_DISCLAIMER");
    TextView[] arrayOfTextView;
    int j;
    if (str1 == null)
    {
      localTextView1.setVisibility(8);
      localTextView2.setVisibility(8);
      this.mPosterView = ((ImageView)findViewById(2131165824));
      this.mMovie = MovieDao.getMovie(this.mMovieId);
      if (this.mMovie.thumbnailSoftBitmap.get() != null)
        this.mPosterView.setImageBitmap((Bitmap)this.mMovie.thumbnailSoftBitmap.get());
      this.mAlertIconError = getResources().getDrawable(2130837601);
      this.mAlertIconError.setBounds(0, 0, this.mAlertIconError.getIntrinsicWidth(), this.mAlertIconError.getIntrinsicHeight());
      this.mAlertIconCheck = getResources().getDrawable(2130837600);
      this.mAlertIconCheck.setBounds(0, 0, this.mAlertIconCheck.getIntrinsicWidth(), this.mAlertIconCheck.getIntrinsicHeight());
      this.mErrorTicketText = ((RelativeLayout)findViewById(2131165820));
      this.mErrorCardnumberText = ((TextView)findViewById(2131165850));
      this.mErrorCodenumberText = ((TextView)findViewById(2131165852));
      this.mErrorPostalnumberText = ((TextView)findViewById(2131165853));
      this.mErrorExpDateText = ((TextView)findViewById(2131165851));
      this.mErrorEmailText = ((TextView)findViewById(2131165857));
      this.mErrorTicketCountText = ((TextView)findViewById(2131165832));
      this.mErrorTicketText.setVisibility(8);
      this.mErrorCardnumberText.setVisibility(8);
      this.mErrorTicketCountText.setVisibility(8);
      this.mTicketBarLinearLayout = ((LinearLayout)findViewById(2131165828));
      this.mConvenienceTotalView = ((TextView)findViewById(2131165830));
      this.mTotalView = ((TextView)findViewById(2131165834));
      this.mTicketCardNumber = ((TextView)findViewById(2131165842));
      this.mTicketExpDateLabel = ((TextView)findViewById(2131165843));
      this.mTicketExpMonth = ((TextView)findViewById(2131165844));
      this.mTicketExpMonth.setFocusableInTouchMode(false);
      this.mTicketExpYear = ((TextView)findViewById(2131165845));
      this.mTicketExpYear.setFocusableInTouchMode(false);
      this.mTicketCodeNumber = ((TextView)findViewById(2131165847));
      this.mTicketPostalNumber = ((TextView)findViewById(2131165849));
      this.mTicketPostalNumber = ((TextView)findViewById(2131165849));
      this.mTicketEmail = ((TextView)findViewById(2131165856));
      String str2 = FlixsterApplication.getTicketEmail();
      if (str2 != null)
        this.mTicketEmail.setText(str2);
      arrayOfTextView = new TextView[6];
      arrayOfTextView[0] = this.mTicketCardNumber;
      arrayOfTextView[1] = this.mTicketExpMonth;
      arrayOfTextView[2] = this.mTicketExpYear;
      arrayOfTextView[3] = this.mTicketCodeNumber;
      arrayOfTextView[4] = this.mTicketPostalNumber;
      arrayOfTextView[5] = this.mTicketEmail;
      int i = arrayOfTextView.length;
      j = 0;
      label1002: if (j < i)
        break label1149;
      Calendar localCalendar = Calendar.getInstance();
      this.mThisMonth = localCalendar.get(2);
      this.mThisYear = localCalendar.get(1);
      this.mCardYearOptions = new CharSequence[10];
    }
    for (int k = 0; ; k++)
    {
      if (k >= 10)
      {
        this.mEmailPattern = Pattern.compile("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$");
        this.mTimer = new Timer();
        this.mContinueButton = ((Button)findViewById(2131165859));
        this.mContinueButton.setOnClickListener(this);
        this.mCompletePurchaseButton = ((Button)findViewById(2131165888));
        this.mCompletePurchaseButton.setOnClickListener(this);
        this.mTicketConfirmationNumber = ((TextView)findViewById(2131165868));
        return;
        localTextView1.setText(str1);
        localTextView2.setText(str1);
        break;
        label1149: TextView localTextView3 = arrayOfTextView[j];
        localTextView3.setOnFocusChangeListener(this);
        localTextView3.setOnKeyListener(this);
        localTextView3.setOnClickListener(this);
        j++;
        break label1002;
      }
      CharSequence[] arrayOfCharSequence = this.mCardYearOptions;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(k + this.mThisYear);
      arrayOfCharSequence[k] = String.format("%4d", arrayOfObject);
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1:
      return new AlertDialog.Builder(this).setTitle(2131493036).setItems(2131623946, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          TicketSelectPage.this.mCardMonth = paramAnonymousInt;
          TicketSelectPage.this.mExpDateFieldState = TicketsDao.validateDate(TicketSelectPage.this.mCardMonth, TicketSelectPage.this.mCardYear, TicketSelectPage.this.mThisMonth);
          TicketSelectPage.this.updateCardStatus.sendEmptyMessage(0);
        }
      }).create();
    case 2:
      return new AlertDialog.Builder(this).setTitle(2131493037).setItems(this.mCardYearOptions, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          TicketSelectPage.this.mCardYear = paramAnonymousInt;
          TicketSelectPage.this.mExpDateFieldState = TicketsDao.validateDate(TicketSelectPage.this.mCardMonth, TicketSelectPage.this.mCardYear, TicketSelectPage.this.mThisMonth);
          TicketSelectPage.this.updateCardStatus.sendEmptyMessage(0);
        }
      }).create();
    case 3:
      this.mTicketAvailabilityDialog = new ProgressDialog(this);
      this.mTicketAvailabilityDialog.setMessage("Checking Ticket Availability...");
      this.mTicketAvailabilityDialog.setIndeterminate(true);
      this.mTicketAvailabilityDialog.setCancelable(true);
      return this.mTicketAvailabilityDialog;
    case 4:
      this.mLoadingPricingDialog = new ProgressDialog(this);
      this.mLoadingPricingDialog.setMessage(getResources().getString(2131493173));
      this.mLoadingPricingDialog.setIndeterminate(true);
      this.mLoadingPricingDialog.setCancelable(true);
      return this.mLoadingPricingDialog;
    case 5:
      this.mPurchaseDialog = new ProgressDialog(this);
      this.mPurchaseDialog.setMessage("Purchasing tickets...");
      this.mPurchaseDialog.setIndeterminate(true);
      this.mPurchaseDialog.setCancelable(true);
      return this.mPurchaseDialog;
    case 6:
      this.mPurchaseError = new AlertDialog.Builder(this).setMessage(this.mPurchaseErrorMessage).setTitle("Purchase Error").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          TicketSelectPage.this.mStopPurchaseDialogHandler.sendEmptyMessage(0);
          TicketSelectPage.this.mState = 0;
          TicketSelectPage.this.mStateLayout.sendEmptyMessage(0);
        }
      }).create();
      return this.mPurchaseError;
    case 7:
      this.mPurchaseError = new AlertDialog.Builder(this).setMessage(this.mPurchaseErrorMessage).setTitle("Purchase Error").setCancelable(false).setNegativeButton(getResources().getString(2131492938), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          TicketSelectPage.this.mStopPurchaseDialogHandler.sendEmptyMessage(0);
          TicketSelectPage.this.mState = 0;
          TicketSelectPage.this.mStateLayout.sendEmptyMessage(0);
        }
      }).setPositiveButton("Call", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          TicketSelectPage.this.mStopPurchaseDialogHandler.sendEmptyMessage(0);
          TicketSelectPage.this.mState = 0;
          TicketSelectPage.this.mStateLayout.sendEmptyMessage(0);
          Intent localIntent = new Intent("android.intent.action.DIAL");
          localIntent.setData(Uri.parse("tel:1-888-440-8457"));
          TicketSelectPage.this.startActivity(localIntent);
        }
      }).create();
      return this.mPurchaseError;
    case 8:
    }
    return new AlertDialog.Builder(this).setMessage("The network connection failed. Press Retry to make another attempt.").setTitle("Network Error").setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        switch (TicketSelectPage.this.mState)
        {
        default:
          return;
        case 0:
        }
        TicketSelectPage.this.mFetchPerformanceTrys = 0;
        TicketSelectPage.this.mStartLoadPricingDialogHandler.sendEmptyMessage(0);
        TicketSelectPage.this.ScheduleLoadPerformanceTask();
      }
    }).setNegativeButton(getResources().getString(2131492938), null).create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  public void onDestroy()
  {
    this.mScrollAd.destroy();
    super.onDestroy();
  }

  public void onFocusChange(View paramView, boolean paramBoolean)
  {
    Logger.d("FlxMain", "TicketSelectPage.onFocusChange");
    switch (paramView.getId())
    {
    default:
    case 2131165842:
    case 2131165844:
    case 2131165847:
    case 2131165849:
    case 2131165845:
    case 2131165856:
    }
    while (true)
    {
      this.updateCardStatus.sendEmptyMessage(0);
      return;
      if (paramBoolean)
      {
        if (this.mCardNumberFieldState == 3)
          this.mErrorCardnumberText.setVisibility(0);
      }
      else
      {
        this.mErrorCardnumberText.setVisibility(8);
        this.mCardNumberFieldState = TicketsDao.validateCreditCard(this.mTicketCardNumber.getText().toString(), this.mCardType);
        if (this.mCardNumberFieldState == 1)
        {
          this.mCardNumberFieldState = 3;
          continue;
          if (!paramBoolean)
          {
            this.updateCardStatus.sendEmptyMessage(0);
            continue;
            if (paramBoolean)
            {
              if (this.mCodeFieldState == 3)
                this.mErrorCodenumberText.setVisibility(0);
            }
            else
            {
              this.mErrorCodenumberText.setVisibility(8);
              this.mCodeFieldState = TicketsDao.validateCardCode(this.mTicketCodeNumber.getText().toString(), this.mCardType);
              if (this.mCodeFieldState == 1)
                this.mCodeFieldState = 3;
              this.updateCardStatus.sendEmptyMessage(0);
              continue;
              if (paramBoolean)
              {
                if (this.mCodeFieldState == 3)
                  this.mErrorPostalnumberText.setVisibility(0);
              }
              else
              {
                this.mErrorPostalnumberText.setVisibility(8);
                this.mPostalFieldState = TicketsDao.validatePostal(this.mTicketPostalNumber.getText().toString());
                if (this.mPostalFieldState == 1)
                  this.mPostalFieldState = 3;
                this.updateCardStatus.sendEmptyMessage(0);
                continue;
                if (!paramBoolean)
                {
                  this.updateCardStatus.sendEmptyMessage(0);
                  continue;
                  if (!paramBoolean)
                  {
                    this.mEmailFieldState = TicketsDao.validateEmail(this.mTicketEmail.getText().toString());
                    this.updateCardStatus.sendEmptyMessage(0);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  public void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    if (this.mCardMapType != null)
    {
      this.mCardTypeIndex = paramInt;
      this.mCardType = this.mCardMapType[paramInt];
      this.mCardNumberFieldState = TicketsDao.validateCreditCard(this.mTicketCardNumber.getText().toString(), this.mCardType);
      this.mCodeFieldState = TicketsDao.validateCardCode(this.mTicketCodeNumber.getText().toString(), this.mCardType);
      this.mPostalFieldState = TicketsDao.validatePostal(this.mTicketPostalNumber.getText().toString());
      this.updateCardStatus.sendEmptyMessage(0);
    }
  }

  public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    boolean bool;
    if (paramInt == 62)
      bool = true;
    do
    {
      int i;
      do
      {
        int j;
        do
        {
          int k;
          do
          {
            return bool;
            switch (paramView.getId())
            {
            case 2131165843:
            case 2131165845:
            case 2131165846:
            case 2131165848:
            default:
              return false;
            case 2131165842:
              if ((paramInt < 19) || (paramInt > 22))
              {
                this.mCardNumberFieldState = TicketsDao.validateCreditCard(this.mTicketCardNumber.getText().toString(), this.mCardType);
                this.updateCardStatus.sendEmptyMessage(0);
              }
              k = this.mCardNumberFieldState;
              bool = false;
            case 2131165847:
            case 2131165849:
            case 2131165844:
            }
          }
          while (k != 3);
          this.mErrorCardnumberText.setVisibility(0);
          return false;
          if ((paramInt < 19) || (paramInt > 22))
          {
            this.mCodeFieldState = TicketsDao.validateCardCode(this.mTicketCodeNumber.getText().toString(), this.mCardType);
            this.updateCardStatus.sendEmptyMessage(0);
          }
          j = this.mCodeFieldState;
          bool = false;
        }
        while (j != 3);
        this.mErrorCardnumberText.setVisibility(0);
        return false;
        if ((paramInt < 19) || (paramInt > 22))
        {
          this.mPostalFieldState = TicketsDao.validatePostal(this.mTicketPostalNumber.getText().toString());
          this.updateCardStatus.sendEmptyMessage(0);
        }
        i = this.mPostalFieldState;
        bool = false;
      }
      while (i != 3);
      this.mErrorPostalnumberText.setVisibility(0);
      return false;
      if (paramInt < 19)
        break;
      bool = false;
    }
    while (paramInt <= 22);
    String str = this.mTicketExpMonth.getText().toString();
    if (str.length() > 0)
      this.mCardMonth = (-1 + Integer.valueOf(str).intValue());
    this.mExpDateFieldState = TicketsDao.validateDate(this.mCardMonth, this.mCardYear, this.mThisMonth);
    this.updateCardStatus.sendEmptyMessage(0);
    return false;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.mState == 2))
    {
      this.mState = 0;
      this.mStateLayout.sendEmptyMessage(0);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.mState == 2))
      return true;
    return super.onKeyUp(paramInt, paramKeyEvent);
  }

  public void onNothingSelected(AdapterView<?> paramAdapterView)
  {
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    Logger.d("FlxMain", "TicketSelectPage.onRestoreInstanceState(.) pre  mCategories:" + this.mCategories);
    this.mState = paramBundle.getInt("mState");
    this.mTicketCount = paramBundle.getFloat("mTicketCount");
    this.mTicketCounts = paramBundle.getFloatArray("mTicketCounts");
    this.mCategories = paramBundle.getInt("mCategories");
    Logger.d("FlxMain", "TicketSelectPage.onRestoreInstanceState(.) post  mCategories:" + this.mCategories);
    this.mCategoryLabels = paramBundle.getStringArray("mCategoryLabels");
    this.mCategoryPrices = paramBundle.getFloatArray("mCategoryPrices");
    this.mMethodIds = paramBundle.getStringArray("mMethodIds");
    this.mMethodNames = paramBundle.getStringArray("mMethodNames");
    this.mCardMapType = paramBundle.getIntArray("mCardMapType");
    this.mCardTypeIndex = paramBundle.getInt("mCardTypeIndex");
    this.mCardType = paramBundle.getInt("mCardType");
    this.mCardMonth = paramBundle.getInt("mCardMonth");
    this.mCardYear = paramBundle.getInt("mCardYear");
    this.mPerformance = ((Performance)paramBundle.getParcelable("mPerformance"));
  }

  protected void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "TicketSelectPage.onResume() mCategories:" + this.mCategories);
    if (this.mCategories < 1)
    {
      Logger.d("FlxMain", "TicketSelectPage.onCreate() mCategories:" + this.mCategories);
      this.mStartLoadPricingDialogHandler.sendEmptyMessage(0);
      ScheduleLoadPerformanceTask();
    }
    while (true)
    {
      this.mSelectToReservationFieldHandler.sendEmptyMessage(0);
      this.mStateLayout.sendEmptyMessage(0);
      this.mScrollAd.refreshAds();
      return;
      if (this.mTicketBarLinearLayout.getChildCount() == 0)
      {
        this.addTicketBars.sendEmptyMessage(0);
        this.updatePrice.sendEmptyMessage(0);
        this.updateCardStatus.sendEmptyMessage(0);
        this.setupCardSpinner.sendEmptyMessage(0);
      }
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Logger.d("FlxMain", "TicketSelectPage.onSaveInstanceState(.) ");
    paramBundle.putInt("mState", this.mState);
    paramBundle.putFloat("mTicketCount", this.mTicketCount);
    paramBundle.putFloatArray("mTicketCounts", this.mTicketCounts);
    paramBundle.putInt("mCategories", this.mCategories);
    paramBundle.putStringArray("mCategoryLabels", this.mCategoryLabels);
    paramBundle.putFloatArray("mCategoryPrices", this.mCategoryPrices);
    paramBundle.putStringArray("mMethodIds", this.mMethodIds);
    paramBundle.putStringArray("mMethodNames", this.mMethodNames);
    paramBundle.putIntArray("mCardMapType", this.mCardMapType);
    paramBundle.putInt("mCardTypeIndex", this.mCardTypeIndex);
    paramBundle.putInt("mCardType", this.mCardType);
    paramBundle.putInt("mCardMonth", this.mCardMonth);
    paramBundle.putInt("mCardYear", this.mCardYear);
    paramBundle.putParcelable("mPerformance", this.mPerformance);
  }

  static class TicketbarHolder
  {
    TextView counter;
    ImageButton decButton;
    ImageButton incButton;
    Integer index;
    TextView label;
    TextView price;
    TextView subTotal;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.TicketSelectPage
 * JD-Core Version:    0.6.2
 */