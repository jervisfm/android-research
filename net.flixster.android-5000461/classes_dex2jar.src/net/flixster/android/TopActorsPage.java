package net.flixster.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.SubNavBar;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.ActorDao;
import net.flixster.android.data.DaoException;
import net.flixster.android.lvi.LviActor;
import net.flixster.android.lvi.LviAd;
import net.flixster.android.model.Actor;

public class TopActorsPage extends LviActivity
{
  private static final int LIMIT_ACTORS = 25;
  private View.OnClickListener mActorListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Intent localIntent = new Intent("TOP_ACTOR", null, TopActorsPage.this, ActorPage.class);
      if (paramAnonymousView.getTag() != null)
      {
        Actor localActor = (Actor)paramAnonymousView.getTag();
        localIntent.putExtra("ACTOR_ID", localActor.id);
        localIntent.putExtra("ACTOR_NAME", localActor.name);
        TopActorsPage.this.startActivity(localIntent);
      }
    }
  };
  private View.OnClickListener mNavListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      TopActorsPage.this.mNavSelect = paramAnonymousView.getId();
      TopActorsPage.this.trackHelper();
      TopActorsPage.this.mListView.setOnItemClickListener(TopActorsPage.this.getMovieItemClickListener());
      TopActorsPage.this.ScheduleLoadItemsTask(TopActorsPage.this.mNavSelect, 100L);
    }
  };
  private int mNavSelect;
  private ArrayList<Actor> mRandom = new ArrayList();
  private ArrayList<Actor> mTopThisWeek = new ArrayList();
  private SubNavBar navBar;

  private void ScheduleLoadItemsTask(final int paramInt, long paramLong)
  {
    try
    {
      this.throbberHandler.sendEmptyMessage(1);
      TimerTask local3 = new TimerTask()
      {
        public void run()
        {
          while (true)
          {
            try
            {
              switch (paramInt)
              {
              default:
                TopActorsPage.this.mUpdateHandler.sendEmptyMessage(0);
                return;
              case 2131165784:
                if (TopActorsPage.this.mTopThisWeek.isEmpty())
                  TopActorsPage.this.mTopThisWeek = ActorDao.getTopActors("popular", 25);
                TopActorsPage.this.setTopThisWeekLviList();
                continue;
              case 2131165785:
              }
            }
            catch (DaoException localDaoException)
            {
              Logger.w("FlxMain", "TopActorsPage.ScheduleLoadItemsTask.run() mRetryCount:" + TopActorsPage.this.mRetryCount);
              localDaoException.printStackTrace();
              TopActorsPage localTopActorsPage = TopActorsPage.this;
              localTopActorsPage.mRetryCount = (1 + localTopActorsPage.mRetryCount);
              if (TopActorsPage.this.mRetryCount < 3)
              {
                TopActorsPage.this.ScheduleLoadItemsTask(paramInt, 1000L);
                return;
                if (TopActorsPage.this.mRandom.isEmpty())
                  TopActorsPage.this.mRandom = ActorDao.getTopActors("random", 25);
                TopActorsPage.this.setRandomLviList();
                continue;
              }
            }
            finally
            {
              TopActorsPage.this.throbberHandler.sendEmptyMessage(0);
            }
            TopActorsPage.this.mRetryCount = 0;
            TopActorsPage.this.mShowDialogHandler.sendEmptyMessage(2);
          }
        }
      };
      if (this.mPageTimer != null)
        this.mPageTimer.schedule(local3, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void setRandomLviList()
  {
    this.mDataHolder.clear();
    destroyExistingLviAd();
    this.lviAd = new LviAd();
    this.lviAd.mAdSlot = "TopActors";
    this.mDataHolder.add(this.lviAd);
    Iterator localIterator = this.mRandom.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Actor localActor = (Actor)localIterator.next();
      LviActor localLviActor = new LviActor();
      localLviActor.mActor = localActor;
      localLviActor.mActorClick = this.mActorListener;
      this.mDataHolder.add(localLviActor);
    }
  }

  private void setTopThisWeekLviList()
  {
    this.mDataHolder.clear();
    destroyExistingLviAd();
    this.lviAd = new LviAd();
    this.lviAd.mAdSlot = "TopActors";
    this.mDataHolder.add(this.lviAd);
    Iterator localIterator = this.mTopThisWeek.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Actor localActor = (Actor)localIterator.next();
      LviActor localLviActor = new LviActor();
      localLviActor.mActor = localActor;
      localLviActor.mActorClick = this.mActorListener;
      this.mDataHolder.add(localLviActor);
    }
  }

  private void trackHelper()
  {
    switch (this.mNavSelect)
    {
    default:
      return;
    case 2131165784:
      Trackers.instance().track("/actor/top", "Top Actors Page for TopThisWeek");
      return;
    case 2131165785:
    }
    Trackers.instance().track("/actor/random", "Top Actors Page for Random");
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    createActionBar();
    setActionBarTitle(2131492891);
    this.mListView.setOnItemClickListener(getMovieItemClickListener());
    LinearLayout localLinearLayout = (LinearLayout)findViewById(2131165427);
    this.navBar = new SubNavBar(this);
    this.navBar.load(this.mNavListener, 2131492893, 2131492914);
    this.navBar.setSelectedButton(2131165784);
    localLinearLayout.addView(this.navBar, 0);
    this.mNavSelect = 2131165784;
    this.mStickyTopAd.setSlot("TopActorsStickyTop");
    this.mStickyBottomAd.setSlot("TopActorsStickyBottom");
  }

  public void onPause()
  {
    super.onPause();
    this.mTopThisWeek.clear();
    this.mRandom.clear();
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    Logger.d("FlxMain", "TopActorsPage.onRestoreInstanceState()");
    this.mNavSelect = paramBundle.getInt("LISTSTATE_NAV");
  }

  public void onResume()
  {
    super.onResume();
    trackHelper();
    this.navBar.setSelectedButton(this.mNavSelect);
    ScheduleLoadItemsTask(this.mNavSelect, 100L);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Logger.d("FlxMain", "TopActorsPage.onSaveInstanceState()");
    paramBundle.putInt("LISTSTATE_NAV", this.mNavSelect);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.TopActorsPage
 * JD-Core Version:    0.6.2
 */