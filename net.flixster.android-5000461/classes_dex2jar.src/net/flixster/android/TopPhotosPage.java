package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.SubNavBar;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.PhotoDao;
import net.flixster.android.model.Photo;

public class TopPhotosPage extends FlixsterActivity
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  public static final String KEY_PHOTO_FILTER = "KEY_PHOTO_FILTER";
  private static final int MAX_PHOTOS = 100;
  private String filter;
  private View.OnClickListener headerClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      TopPhotosPage.this.mNavSelect = paramAnonymousView.getId();
      TopPhotosPage.this.onResume();
    }
  };
  private AdView mDefaultAd;
  private int mNavSelect;
  private SubNavBar navBar;
  private View.OnClickListener photoClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Photo localPhoto = (Photo)paramAnonymousView.getTag();
      if (localPhoto != null)
      {
        int i = TopPhotosPage.this.topPhotos.indexOf(localPhoto);
        if (i < 0)
          i = 0;
        Trackers.instance().track("/photo/gallery/" + TopPhotosPage.this.filter, "Top Photo Page for photo:" + localPhoto.thumbnailUrl);
        Intent localIntent = new Intent("TOP_PHOTO", null, TopPhotosPage.this.getApplicationContext(), ScrollGalleryPage.class);
        localIntent.putExtra("KEY_PHOTO_FILTER", TopPhotosPage.this.filter);
        localIntent.putExtra("PHOTO_INDEX", i);
        TopPhotosPage.this.startActivity(localIntent);
      }
    }
  };
  private AdapterView.OnItemClickListener photoItemClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      paramAnonymousView.performClick();
    }
  };
  private ArrayList<Photo> photos;
  private ArrayList<Object> topPhotos;
  private final Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "PhotoGalleryPage.updateHandler");
      if (TopPhotosPage.this.photos != null)
        TopPhotosPage.this.updatePage();
      while (TopPhotosPage.this.isFinishing())
        return;
      TopPhotosPage.this.showDialog(1);
    }
  };

  private void scheduleUpdatePageTask()
  {
    TimerTask local6 = new TimerTask()
    {
      public void run()
      {
        if (TopPhotosPage.this.photos == null);
        try
        {
          TopPhotosPage.this.photos = PhotoDao.getTopPhotos(TopPhotosPage.this.filter);
          TopPhotosPage.this.updateHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          while (true)
          {
            Logger.e("FlxMain", "PhotoGalleryPage.scheduleUpdatePageTask:failed to get photos", localDaoException);
            TopPhotosPage.this.photos = null;
          }
        }
      }
    };
    if (this.mPageTimer != null)
      this.mPageTimer.schedule(local6, 100L);
  }

  private void updateFilter()
  {
    switch (this.mNavSelect)
    {
    default:
      return;
    case 2131165784:
      this.filter = "top-actresses";
      return;
    case 2131165785:
      this.filter = "top-actors";
      return;
    case 2131165786:
    }
    this.filter = "top-movies";
  }

  private void updatePage()
  {
    GridView localGridView;
    if (this.photos != null)
    {
      localGridView = (GridView)findViewById(2131165903);
      this.topPhotos = new ArrayList();
    }
    for (int i = 0; ; i++)
    {
      if ((i >= this.photos.size()) || (i >= 100))
      {
        localGridView.setAdapter(new PhotosListAdapter(this, this.topPhotos, this.photoClickListener));
        localGridView.setOnItemClickListener(this.photoItemClickListener);
        localGridView.setClickable(true);
        localGridView.setFocusable(true);
        localGridView.setFocusableInTouchMode(true);
        Trackers.instance().track("/photos/" + this.filter, "Top Photos Page for filter:" + this.filter);
        return;
      }
      Object localObject = this.photos.get(i);
      this.topPhotos.add(localObject);
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxMain", "PhotoGalleryPage.onCreate");
    setContentView(2130903179);
    findViewById(2131165902).setVisibility(8);
    createActionBar();
    setActionBarTitle(2131492892);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.filter = localBundle.getString("KEY_PHOTO_FILTER");
      if ((this.filter == null) || (this.filter.length() <= 0))
        this.filter = "random";
      this.navBar = new SubNavBar(this);
      this.navBar.load(this.headerClickListener, 2131492911, 2131492912, 2131492913);
      this.navBar.setSelectedButton(2131165784);
      ((LinearLayout)findViewById(2131165901)).addView(this.navBar, 1);
    }
    this.mDefaultAd = ((AdView)findViewById(2131165226));
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        TopPhotosPage.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder.create();
  }

  public void onDestroy()
  {
    this.mDefaultAd.destroy();
    super.onDestroy();
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    this.mNavSelect = paramBundle.getInt("LISTSTATE_NAV");
  }

  protected void onResume()
  {
    super.onResume();
    this.navBar.setSelectedButton(this.mNavSelect);
    updateFilter();
    this.photos = null;
    scheduleUpdatePageTask();
    this.mDefaultAd.refreshAds();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putInt("LISTSTATE_NAV", this.mNavSelect);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.TopPhotosPage
 * JD-Core Version:    0.6.2
 */