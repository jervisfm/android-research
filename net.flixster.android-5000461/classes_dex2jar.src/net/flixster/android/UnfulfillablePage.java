package net.flixster.android;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountManager;
import com.flixster.android.utils.ErrorDialog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Unfulfillable;
import net.flixster.android.model.User;

public class UnfulfillablePage extends DecoratedSherlockActivity
{
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((paramAnonymousMessage.obj instanceof DaoException))
        ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, UnfulfillablePage.this);
    }
  };
  private GridView gridView;
  private TextView learnMoreView;
  private final Handler successHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      int i = 0;
      UnfulfillablePage.this.unfulfillableRights.clear();
      Iterator localIterator = AccountManager.instance().getUser().getLockerRights().iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          UnfulfillablePage.this.learnMoreView.setText(Html.fromHtml(UnfulfillablePage.this.getResources().getString(2131492983)));
          UnfulfillablePage.this.learnMoreView.setMovementMethod(LinkMovementMethod.getInstance());
          UnfulfillablePage.this.gridView.setAdapter(new LockerRightGridViewAdapter(UnfulfillablePage.this, UnfulfillablePage.this.unfulfillableRights));
          UnfulfillablePage.this.gridView.setClickable(true);
          UnfulfillablePage.this.gridView.setOnItemClickListener(UnfulfillablePage.this.unfulfillableMovieClickListener);
          return;
        }
        LockerRight localLockerRight = (LockerRight)localIterator.next();
        if (localLockerRight.isUnfulfillable())
        {
          UnfulfillablePage.this.unfulfillableRights.add(i, localLockerRight);
          i++;
        }
      }
    }
  };
  private final AdapterView.OnItemClickListener unfulfillableMovieClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(((Unfulfillable)((LockerRight)UnfulfillablePage.this.unfulfillableRights.get(paramAnonymousInt)).getAsset()).getAlternateUrl()));
      UnfulfillablePage.this.startActivity(localIntent);
    }
  };
  private List<LockerRight> unfulfillableRights;

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903180);
    createActionBar();
    setActionBarTitle(2131492981);
    this.learnMoreView = ((TextView)findViewById(2131165904));
    this.gridView = ((GridView)findViewById(2131165455));
    this.unfulfillableRights = new ArrayList();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    if (AccountManager.instance().hasUserSession())
      ProfileDao.getUserLockerRights(this.successHandler, this.errorHandler);
    Trackers.instance().track("/mymovies/unfulfillables", "My Movies - Unfulfillables");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.UnfulfillablePage
 * JD-Core Version:    0.6.2
 */