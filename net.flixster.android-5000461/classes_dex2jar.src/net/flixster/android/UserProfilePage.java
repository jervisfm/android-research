package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;
import net.flixster.android.model.User;

public class UserProfilePage extends FlixsterActivity
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  protected static final int HIDE_THROBBER = 0;
  private static final int LIMIT_RATING = 3;
  private static final int LIMIT_WANT_TO_SEE = 3;
  protected static final int SHOW_THROBBER = 1;
  private LayoutInflater inflater;
  private RelativeLayout moreRatedLayout;
  private View.OnClickListener moreRatedListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      View localView;
      if ((UserProfilePage.this.ratedLayout != null) && (UserProfilePage.this.user != null) && (UserProfilePage.this.user.ratedReviews != null) && (UserProfilePage.this.user.ratedReviews.size() > 3))
        localView = null;
      for (int i = 3; ; i++)
      {
        if (i >= UserProfilePage.this.user.ratedReviews.size())
        {
          if (UserProfilePage.this.moreRatedLayout != null)
            UserProfilePage.this.moreRatedLayout.setVisibility(8);
          return;
        }
        Review localReview = (Review)UserProfilePage.this.user.ratedReviews.get(i);
        localView = UserProfilePage.this.getReviewView(localReview, localView);
        UserProfilePage.this.ratedLayout.addView(localView);
      }
    }
  };
  private RelativeLayout moreWantToSeeLayout;
  private View.OnClickListener moreWantToSeeListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      View localView;
      if ((UserProfilePage.this.wantToSeeLayout != null) && (UserProfilePage.this.user != null) && (UserProfilePage.this.user.wantToSeeReviews != null) && (UserProfilePage.this.user.wantToSeeReviews.size() > 3))
        localView = null;
      for (int i = 3; ; i++)
      {
        if (i >= UserProfilePage.this.user.wantToSeeReviews.size())
        {
          if (UserProfilePage.this.moreWantToSeeLayout != null)
            UserProfilePage.this.moreWantToSeeLayout.setVisibility(8);
          return;
        }
        Review localReview = (Review)UserProfilePage.this.user.wantToSeeReviews.get(i);
        localView = UserProfilePage.this.getMovieView(localReview, localView);
        UserProfilePage.this.wantToSeeLayout.addView(localView);
      }
    }
  };
  private final Handler movieThumbnailHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      ImageView localImageView = (ImageView)paramAnonymousMessage.obj;
      if (localImageView != null)
      {
        Movie localMovie = (Movie)localImageView.getTag();
        if (localMovie != null)
        {
          localImageView.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
          localImageView.invalidate();
        }
      }
    }
  };
  private String platformId;
  private LinearLayout ratedLayout;
  private Resources resources;
  private View.OnClickListener reviewClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Review localReview = (Review)paramAnonymousView.getTag();
      int j;
      if ((localReview != null) && (UserProfilePage.this.user != null))
      {
        Movie localMovie = localReview.getMovie();
        Trackers.instance().track("/mymovies/profile", "User Review - " + localMovie.getProperty("title"));
        i = UserProfilePage.this.user.wantToSeeReviews.indexOf(localReview);
        if (i < 0)
        {
          j = UserProfilePage.this.user.ratedReviews.indexOf(localReview);
          if (j >= 0)
            break label173;
        }
      }
      label173: for (int i = 0; ; i = j + UserProfilePage.this.user.wantToSeeReviews.size())
      {
        String str = String.valueOf(UserProfilePage.this.user.id);
        Intent localIntent = new Intent("PROFILE_REVIEW_PAGE", null, UserProfilePage.this.getApplicationContext(), ProfileReviewPage.class);
        localIntent.putExtra("REVIEW_INDEX", i);
        localIntent.putExtra("PLATFORM_ID", str);
        UserProfilePage.this.startActivity(localIntent);
        return;
      }
    }
  };
  private View throbber;
  protected final Handler throbberHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
        UserProfilePage.this.hideLoading();
        return;
      case 1:
      }
      UserProfilePage.this.showLoading();
    }
  };
  private Timer timer;
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", "UserProfilePage.updateHandler");
      if ((UserProfilePage.this.user != null) && (UserProfilePage.this.user.reviews != null) && ((!UserProfilePage.this.user.reviews.isEmpty()) || (UserProfilePage.this.user.reviewCount <= 0)))
        UserProfilePage.this.updatePage();
      while (UserProfilePage.this.isFinishing())
        return;
      UserProfilePage.this.showDialog(1);
    }
  };
  private User user;
  private LinearLayout wantToSeeLayout;

  private void addScore(int paramInt, Drawable paramDrawable, TextView paramTextView)
  {
    paramDrawable.setBounds(0, 0, paramDrawable.getIntrinsicWidth(), paramDrawable.getIntrinsicHeight());
    paramTextView.setCompoundDrawables(paramDrawable, null, null, null);
    paramTextView.setText("" + paramInt + "%");
    paramTextView.setVisibility(0);
  }

  private View getMovieView(Review paramReview, View paramView)
  {
    if ((paramView == null) || (!(paramView.getTag() instanceof MovieViewHolder)))
      paramView = this.inflater.inflate(2130903130, null);
    MovieViewHolder localMovieViewHolder = (MovieViewHolder)paramView.getTag();
    if (localMovieViewHolder == null)
    {
      localMovieViewHolder = new MovieViewHolder(null);
      TextView localTextView1 = (TextView)paramView.findViewById(2131165448);
      localMovieViewHolder.titleView = localTextView1;
      ImageView localImageView = (ImageView)paramView.findViewById(2131165486);
      localMovieViewHolder.thumbnailView = localImageView;
      TextView localTextView2 = (TextView)paramView.findViewById(2131165490);
      localMovieViewHolder.scoreView = localTextView2;
      TextView localTextView3 = (TextView)paramView.findViewById(2131165491);
      localMovieViewHolder.friendScore = localTextView3;
      TextView localTextView4 = (TextView)paramView.findViewById(2131165451);
      localMovieViewHolder.actorsView = localTextView4;
      TextView localTextView5 = (TextView)paramView.findViewById(2131165452);
      localMovieViewHolder.metaView = localTextView5;
      TextView localTextView6 = (TextView)paramView.findViewById(2131165453);
      localMovieViewHolder.releaseView = localTextView6;
      paramView.setTag(localMovieViewHolder);
    }
    Movie localMovie = paramReview.getMovie();
    String str1 = localMovie.getProperty("title");
    localMovieViewHolder.titleView.setText(str1);
    int i;
    label248: int j;
    if (localMovie.thumbnailSoftBitmap.get() != null)
    {
      localMovieViewHolder.thumbnailView.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
      if ((localMovie.getTheaterReleaseDate() == null) || (FlixsterApplication.sToday.after(localMovie.getTheaterReleaseDate())))
        break label494;
      i = 0;
      j = FlixsterApplication.getMovieRatingType();
      switch (j)
      {
      default:
        label276: if (FlixsterApplication.getPlatformUsername() == null)
          localMovieViewHolder.friendScore.setVisibility(8);
        break;
      case 0:
      case 1:
      }
    }
    while (true)
    {
      String str3 = localMovie.getProperty("MOVIE_ACTORS_SHORT");
      if ((str3 != null) && (str3.length() > 0))
      {
        localMovieViewHolder.actorsView.setText(str3);
        localMovieViewHolder.actorsView.setVisibility(0);
      }
      String str4 = localMovie.getProperty("meta");
      if (str4 != null)
      {
        localMovieViewHolder.metaView.setText(str4);
        localMovieViewHolder.metaView.setVisibility(0);
      }
      if (localMovie.getProperty("Opening This Week") != null)
      {
        String str5 = localMovie.getProperty("theaterReleaseDate");
        if (str5 != null)
        {
          localMovieViewHolder.releaseView.setText(str5);
          localMovieViewHolder.releaseView.setVisibility(0);
        }
      }
      paramView.setTag(paramReview);
      paramView.setFocusable(true);
      paramView.setClickable(true);
      View.OnClickListener localOnClickListener = this.reviewClickListener;
      paramView.setOnClickListener(localOnClickListener);
      return paramView;
      String str2 = localMovie.getProperty("thumbnail");
      if ((str2 == null) || (str2.length() <= 0))
        break;
      localMovieViewHolder.thumbnailView.setTag(localMovie);
      orderImage(new ImageOrder(0, localMovie, str2, localMovieViewHolder.thumbnailView, this.movieThumbnailHandler));
      break;
      label494: i = 1;
      break label248;
      if (!localMovie.checkIntProperty("popcornScore"))
        break label276;
      int i1 = localMovie.getIntProperty("popcornScore").intValue();
      if (i1 <= 0)
        break label276;
      int i2;
      label539: int i3;
      if (i1 < 60)
      {
        i2 = 1;
        if (i != 0)
          break label577;
        i3 = 2130837750;
      }
      while (true)
      {
        addScore(i1, this.resources.getDrawable(i3), localMovieViewHolder.scoreView);
        break;
        i2 = 0;
        break label539;
        label577: if (i2 != 0)
          i3 = 2130837743;
        else
          i3 = 2130837738;
      }
      if (!localMovie.checkIntProperty("rottenTomatoes"))
        break label276;
      int k = localMovie.getIntProperty("rottenTomatoes").intValue();
      if (k <= 0)
        break label276;
      if (k < 60);
      for (Drawable localDrawable1 = this.resources.getDrawable(2130837741); ; localDrawable1 = this.resources.getDrawable(2130837726))
      {
        TextView localTextView7 = localMovieViewHolder.scoreView;
        addScore(k, localDrawable1, localTextView7);
        break;
      }
      if ((localMovie.checkIntProperty("FRIENDS_RATED_COUNT")) && (localMovie.getIntProperty("FRIENDS_RATED_COUNT").intValue() > 0))
      {
        Drawable localDrawable3 = this.resources.getDrawable(2130837739);
        localDrawable3.setBounds(0, 0, localDrawable3.getIntrinsicWidth(), localDrawable3.getIntrinsicHeight());
        localMovieViewHolder.friendScore.setCompoundDrawables(localDrawable3, null, null, null);
        int n = localMovie.getIntProperty("FRIENDS_RATED_COUNT").intValue();
        StringBuilder localStringBuilder2 = new StringBuilder();
        localStringBuilder2.append(n).append(" ");
        if (n > 1)
          localStringBuilder2.append(this.resources.getString(2131493008));
        while (true)
        {
          localMovieViewHolder.friendScore.setText(localStringBuilder2.toString());
          localMovieViewHolder.friendScore.setVisibility(0);
          break;
          localStringBuilder2.append(this.resources.getString(2131493007));
        }
      }
      if ((localMovie.checkIntProperty("FRIENDS_WTS_COUNT")) && (localMovie.getIntProperty("FRIENDS_WTS_COUNT").intValue() > 0))
      {
        int m = localMovie.getIntProperty("FRIENDS_WTS_COUNT").intValue();
        StringBuilder localStringBuilder1 = new StringBuilder();
        if ((i == 0) && (j == 0))
        {
          localMovieViewHolder.friendScore.setCompoundDrawables(null, null, null, null);
          localStringBuilder1.append("(").append(m).append(" ");
          if (m > 1)
          {
            localStringBuilder1.append(this.resources.getString(2131493006));
            label956: localStringBuilder1.append(")");
          }
        }
        while (true)
        {
          localMovieViewHolder.friendScore.setText(localStringBuilder1.toString());
          localMovieViewHolder.friendScore.setVisibility(0);
          break;
          localStringBuilder1.append(this.resources.getString(2131493005));
          break label956;
          Drawable localDrawable2 = this.resources.getDrawable(2130837751);
          localDrawable2.setBounds(0, 0, localDrawable2.getIntrinsicWidth(), localDrawable2.getIntrinsicHeight());
          localMovieViewHolder.friendScore.setCompoundDrawables(localDrawable2, null, null, null);
          localStringBuilder1.append(m).append(" ");
          if (m > 1)
            localStringBuilder1.append(this.resources.getString(2131493012));
          else
            localStringBuilder1.append(this.resources.getString(2131493011));
        }
      }
      localMovieViewHolder.friendScore.setVisibility(8);
    }
  }

  private View getProfileView(User paramUser)
  {
    View localView = this.inflater.inflate(2130903150, null);
    ProfileViewHolder localProfileViewHolder = (ProfileViewHolder)localView.getTag();
    if (localProfileViewHolder == null)
    {
      localProfileViewHolder = new ProfileViewHolder(null);
      localProfileViewHolder.thumbnailView = ((ImageView)localView.findViewById(2131165706));
      localProfileViewHolder.thumbnailFrameView = ((ImageView)localView.findViewById(2131165707));
      localProfileViewHolder.titleView = ((TextView)localView.findViewById(2131165708));
      localProfileViewHolder.friendView = ((TextView)localView.findViewById(2131165349));
      localProfileViewHolder.ratingView = ((TextView)localView.findViewById(2131165348));
      localProfileViewHolder.wtsView = ((TextView)localView.findViewById(2131165347));
      localProfileViewHolder.collectionsView = ((TextView)localView.findViewById(2131165350));
      localView.setTag(localProfileViewHolder);
    }
    String str1 = FlixsterApplication.getPlatform();
    if ("FLX".equals(str1))
      localProfileViewHolder.thumbnailFrameView.setBackgroundResource(2130837692);
    while (true)
    {
      Bitmap localBitmap = paramUser.getProfileBitmap(localProfileViewHolder.thumbnailView);
      if (localBitmap != null)
        localProfileViewHolder.thumbnailView.setImageBitmap(localBitmap);
      String str2 = paramUser.displayName;
      if (str2 != null)
      {
        localProfileViewHolder.titleView.setText(str2);
        localProfileViewHolder.titleView.setVisibility(0);
      }
      int i = paramUser.friendCount;
      localProfileViewHolder.friendView.setText(new StringBuilder("").append(i).append(" ").append(this.resources.getString(2131492994)));
      int j = paramUser.ratingCount;
      localProfileViewHolder.ratingView.setText(new StringBuilder("").append(j).append(" ").append(this.resources.getString(2131492995)));
      int k = paramUser.wtsCount;
      localProfileViewHolder.wtsView.setText(new StringBuilder("").append(k).append(" ").append(this.resources.getString(2131492996)));
      int m = paramUser.collectionsCount;
      if (m > 0)
      {
        localProfileViewHolder.collectionsView.setText(new StringBuilder("").append(m).append(" ").append(this.resources.getString(2131492997)));
        localProfileViewHolder.collectionsView.setVisibility(0);
      }
      return localView;
      if ("FBK".equals(str1))
        localProfileViewHolder.thumbnailFrameView.setBackgroundResource(2130837833);
    }
  }

  private View getReviewView(Review paramReview, View paramView)
  {
    if ((paramView == null) || (!(paramView.getTag() instanceof ReviewViewHolder)))
      paramView = this.inflater.inflate(2130903129, null);
    ReviewViewHolder localReviewViewHolder = (ReviewViewHolder)paramView.getTag();
    if (localReviewViewHolder == null)
    {
      localReviewViewHolder = new ReviewViewHolder(null);
      localReviewViewHolder.starsView = ((ImageView)paramView.findViewById(2131165501));
      localReviewViewHolder.commentView = ((TextView)paramView.findViewById(2131165503));
      localReviewViewHolder.titleView = ((TextView)paramView.findViewById(2131165499));
      localReviewViewHolder.movieView = ((ImageView)paramView.findViewById(2131165498));
      paramView.setTag(localReviewViewHolder);
    }
    localReviewViewHolder.starsView.setImageResource(Flixster.RATING_SMALL_R[((int)(2.0D * paramReview.stars))]);
    localReviewViewHolder.commentView.setText(paramReview.comment);
    Movie localMovie = paramReview.getMovie();
    if (localMovie != null)
    {
      String str1 = localMovie.getProperty("title");
      localReviewViewHolder.titleView.setText(str1);
      if (localMovie.thumbnailSoftBitmap.get() == null)
        break label229;
      localReviewViewHolder.movieView.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
    }
    while (true)
    {
      paramView.setTag(paramReview);
      paramView.setFocusable(true);
      paramView.setClickable(true);
      paramView.setId(2130903129);
      paramView.setOnClickListener(this.reviewClickListener);
      return paramView;
      label229: String str2 = localMovie.getProperty("thumbnail");
      if (str2 != null)
      {
        localReviewViewHolder.movieView.setTag(localMovie);
        orderImage(new ImageOrder(0, localMovie, str2, localReviewViewHolder.movieView, this.movieThumbnailHandler));
      }
    }
  }

  private View getTextView(String paramString)
  {
    TextView localTextView = new TextView(this);
    localTextView.setText(paramString);
    localTextView.setTextColor(getResources().getColor(2131296268));
    localTextView.setPadding(10, 10, 10, 10);
    return localTextView;
  }

  private void hideLoading()
  {
    this.throbber.setVisibility(8);
  }

  private void scheduleUpdatePageTask()
  {
    TimerTask local8 = new TimerTask()
    {
      public void run()
      {
        UserProfilePage.this.platformId = UserProfilePage.this.getIntent().getExtras().getString("PLATFORM_ID");
        if (UserProfilePage.this.platformId == null)
          UserProfilePage.this.platformId = FlixsterApplication.getPlatformId();
        if ((UserProfilePage.this.user == null) || (UserProfilePage.this.user.reviews == null) || ((UserProfilePage.this.user.reviews.isEmpty()) && (UserProfilePage.this.user.reviewCount > 0)));
        try
        {
          UserProfilePage.this.user = ProfileDao.fetchUser(UserProfilePage.this.platformId);
          if ((UserProfilePage.this.user == null) || (UserProfilePage.this.user.id == null));
        }
        catch (DaoException localDaoException1)
        {
          while (true)
          {
            int i;
            try
            {
              ArrayList localArrayList1 = new ArrayList();
              ArrayList localArrayList2 = new ArrayList();
              List localList = ProfileDao.getUserReviews(UserProfilePage.this.user.id, 25);
              i = 0;
              if (i >= localList.size())
              {
                UserProfilePage.this.user.ratedReviews = localArrayList1;
                UserProfilePage.this.user.wantToSeeReviews = localArrayList2;
                UserProfilePage.this.updateHandler.sendEmptyMessage(0);
                return;
                localDaoException1 = localDaoException1;
                Logger.e("FlxMain", "UserProfilePage.scheduleUpdatePageTask (failed to get user data)", localDaoException1);
                UserProfilePage.this.user = null;
                continue;
              }
              Review localReview = (Review)localList.get(i);
              if (localReview.isWantToSee())
                localArrayList2.add(localReview);
              else if (!localReview.isNotInterested())
                localArrayList1.add(localReview);
            }
            catch (DaoException localDaoException2)
            {
              Logger.e("FlxMain", "UserProfilePage.scheduleUpdatePageTask (failed to get review data)", localDaoException2);
              continue;
            }
            i++;
          }
        }
      }
    };
    if (this.timer != null)
      this.timer.schedule(local8, 100L);
  }

  private void showLoading()
  {
    this.throbber.setVisibility(0);
  }

  private void updatePage()
  {
    LinearLayout localLinearLayout1;
    View localView2;
    int j;
    View localView1;
    if (this.user != null)
    {
      this.inflater = LayoutInflater.from(this);
      this.resources = getResources();
      localLinearLayout1 = (LinearLayout)findViewById(2131165705);
      LinearLayout localLinearLayout2 = (LinearLayout)localLinearLayout1.findViewById(2131165905);
      if (localLinearLayout2.getChildCount() <= 0)
        localLinearLayout2.addView(getProfileView(this.user));
      this.wantToSeeLayout = ((LinearLayout)localLinearLayout1.findViewById(2131165907));
      if ((this.user.wantToSeeReviews != null) && (!this.user.wantToSeeReviews.isEmpty()))
      {
        this.wantToSeeLayout.removeAllViews();
        ((TextView)localLinearLayout1.findViewById(2131165906)).setVisibility(0);
        localView2 = null;
        j = 0;
        if ((j < this.user.wantToSeeReviews.size()) && (j < 3))
          break label614;
        if (this.user.wantToSeeReviews.size() > 3)
        {
          this.moreWantToSeeLayout = ((RelativeLayout)localLinearLayout1.findViewById(2131165908));
          this.moreWantToSeeLayout.setVisibility(0);
          StringBuilder localStringBuilder2 = new StringBuilder();
          localStringBuilder2.append(this.user.wantToSeeReviews.size()).append(" ").append(this.resources.getString(2131492992));
          ((TextView)this.moreWantToSeeLayout.findViewById(2131165910)).setText(localStringBuilder2.toString());
          this.moreWantToSeeLayout.setFocusable(true);
          this.moreWantToSeeLayout.setClickable(true);
          this.moreWantToSeeLayout.setOnClickListener(this.moreWantToSeeListener);
        }
      }
      this.ratedLayout = ((LinearLayout)localLinearLayout1.findViewById(2131165912));
      if ((this.user.ratedReviews != null) && (!this.user.ratedReviews.isEmpty()))
      {
        this.ratedLayout.removeAllViews();
        ((TextView)localLinearLayout1.findViewById(2131165911)).setVisibility(0);
        localView1 = null;
      }
    }
    for (int i = 0; ; i++)
    {
      if ((i >= this.user.ratedReviews.size()) || (i >= 3))
      {
        if (this.user.ratedReviews.size() > 3)
        {
          this.moreRatedLayout = ((RelativeLayout)localLinearLayout1.findViewById(2131165913));
          this.moreRatedLayout.setVisibility(0);
          StringBuilder localStringBuilder1 = new StringBuilder();
          localStringBuilder1.append(this.user.ratedReviews.size()).append(" ").append(this.resources.getString(2131492992));
          ((TextView)this.moreRatedLayout.findViewById(2131165915)).setText(localStringBuilder1.toString());
          this.moreRatedLayout.setFocusable(true);
          this.moreRatedLayout.setClickable(true);
          this.moreRatedLayout.setOnClickListener(this.moreRatedListener);
        }
        if ((this.wantToSeeLayout.getChildCount() <= 0) && (this.ratedLayout.getChildCount() <= 0))
          localLinearLayout1.addView(getTextView(this.user.firstName + " " + this.resources.getString(2131493003)));
        Trackers.instance().track("/mymovies/profile", "User Profile Page for user:" + this.user.id);
        this.throbberHandler.sendEmptyMessage(0);
        return;
        label614: localView2 = getMovieView((Review)this.user.wantToSeeReviews.get(j), localView2);
        this.wantToSeeLayout.addView(localView2);
        j++;
        break;
      }
      localView1 = getReviewView((Review)this.user.ratedReviews.get(i), localView1);
      this.ratedLayout.addView(localView1);
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    Logger.d("FlxMain", "UserProfilePage.onCreate");
    super.onCreate(paramBundle);
    setContentView(2130903181);
    createActionBar();
    setActionBarTitle(2131492988);
    this.throbber = findViewById(2131165241);
    updatePage();
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        UserProfilePage.this.throbberHandler.sendEmptyMessage(1);
        UserProfilePage.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder.create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  public void onDestroy()
  {
    Logger.d("FlxMain", "UserProfilePage.onDestroy");
    super.onDestroy();
    if (this.timer != null)
    {
      this.timer.cancel();
      this.timer.purge();
    }
    this.timer = null;
  }

  public void onResume()
  {
    Logger.d("FlxMain", "UserProfilePage.onResume");
    super.onResume();
    if (this.timer == null)
      this.timer = new Timer();
    this.throbberHandler.sendEmptyMessage(1);
    scheduleUpdatePageTask();
  }

  private static final class MovieViewHolder
  {
    TextView actorsView;
    TextView friendScore;
    TextView metaView;
    TextView releaseView;
    TextView scoreView;
    ImageView thumbnailView;
    TextView titleView;
  }

  private static final class ProfileViewHolder
  {
    TextView collectionsView;
    TextView friendView;
    TextView ratingView;
    ImageView thumbnailFrameView;
    ImageView thumbnailView;
    TextView titleView;
    TextView wtsView;
  }

  private static final class ReviewViewHolder
  {
    TextView commentView;
    ImageView movieView;
    ImageView starsView;
    TextView titleView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.UserProfilePage
 * JD-Core Version:    0.6.2
 */