package net.flixster.android;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.utils.Logger;
import java.lang.ref.SoftReference;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;
import net.flixster.android.model.User;

public class UserReviewPage extends FlixsterActivity
  implements View.OnClickListener
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  public static final String KEY_REVIEW_INDEX = "REVIEW_INDEX";
  public static final String KEY_REVIEW_TYPE = "REVIEW_TYPE";
  public static final int REVIEW_TYPE_MY_RATED = 1;
  public static final int REVIEW_TYPE_MY_REVIEWS = 2;
  public static final int REVIEW_TYPE_MY_WANT_TO_SEE;
  private final Handler movieImageHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      ImageView localImageView = (ImageView)paramAnonymousMessage.obj;
      if (localImageView != null)
      {
        Movie localMovie = (Movie)localImageView.getTag();
        if ((localMovie != null) && (localMovie.thumbnailSoftBitmap.get() != null))
        {
          localImageView.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
          localImageView.invalidate();
        }
      }
    }
  };
  private int reviewIndex;
  private int reviewType;
  private List<Review> reviews;
  private View throbber;
  private Timer timer;
  private final Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (UserReviewPage.this.isFinishing());
      do
      {
        return;
        Logger.d("FlxMain", "UserReviewPage.updateHandler - mReviewsList:" + UserReviewPage.this.reviews);
        UserReviewPage.this.hideLoading();
        if ((UserReviewPage.this.user != null) && (UserReviewPage.this.reviews != null) && (!UserReviewPage.this.reviews.isEmpty()))
        {
          UserReviewPage.this.updatePage();
          return;
        }
      }
      while (UserReviewPage.this.isFinishing());
      UserReviewPage.this.showDialog(1);
    }
  };
  private User user;
  private ViewFlipper viewFlipper;

  private String getPageTitle()
  {
    return getString(2131492955) + " - " + (1 + this.viewFlipper.getDisplayedChild()) + " of " + this.reviews.size();
  }

  private void hideLoading()
  {
    this.throbber.setVisibility(8);
  }

  private void scheduleUpdatePageTask()
  {
    TimerTask local3 = new TimerTask()
    {
      public void run()
      {
        if ((UserReviewPage.this.user == null) || (UserReviewPage.this.reviews == null) || (UserReviewPage.this.reviews.isEmpty()));
        try
        {
          UserReviewPage.this.user = ProfileDao.fetchUser();
          if ((UserReviewPage.this.user == null) || (UserReviewPage.this.user.id == null));
        }
        catch (DaoException localDaoException1)
        {
          try
          {
            switch (UserReviewPage.this.reviewType)
            {
            default:
              UserReviewPage.this.reviews = UserReviewPage.this.user.reviews;
              if ((UserReviewPage.this.reviews != null) && ((!UserReviewPage.this.reviews.isEmpty()) || (UserReviewPage.this.user.reviewCount <= 0)))
                break;
            case 0:
              for (UserReviewPage.this.reviews = ProfileDao.getUserReviews(UserReviewPage.this.user.id, 50); ; UserReviewPage.this.reviews = ProfileDao.getWantToSeeReviews(UserReviewPage.this.user.id, 50))
                do
                {
                  UserReviewPage.this.updateHandler.sendEmptyMessage(0);
                  return;
                  localDaoException1 = localDaoException1;
                  Logger.e("FlxMain", "UserReviewPage.scheduleUpdatePageTask (failed to get user data)", localDaoException1);
                  UserReviewPage.this.user = null;
                  break;
                  UserReviewPage.this.reviews = UserReviewPage.this.user.wantToSeeReviews;
                }
                while ((UserReviewPage.this.reviews != null) && ((!UserReviewPage.this.reviews.isEmpty()) || (UserReviewPage.this.user.wtsCount <= 0)));
            case 1:
            }
          }
          catch (DaoException localDaoException2)
          {
            while (true)
            {
              Logger.e("FlxMain", "UserReviewPage.scheduleUpdatePageTask (failed to get review data)", localDaoException2);
              continue;
              UserReviewPage.this.reviews = UserReviewPage.this.user.ratedReviews;
              if ((UserReviewPage.this.reviews == null) || ((UserReviewPage.this.reviews.isEmpty()) && (UserReviewPage.this.user.ratingCount > 0)))
                UserReviewPage.this.reviews = ProfileDao.getUserRatedReviews(UserReviewPage.this.user.id, 50);
            }
          }
        }
      }
    };
    if (this.timer != null)
      this.timer.schedule(local3, 100L);
  }

  private void showLoading()
  {
    this.throbber.setVisibility(0);
  }

  private void updatePage()
  {
    Logger.d("FlxMain", "UserReviewPage.updatePage reviewIndex:" + this.reviewIndex);
    LayoutInflater localLayoutInflater;
    Iterator localIterator;
    if (this.user != null)
    {
      ImageView localImageView1 = (ImageView)findViewById(2131165642);
      Bitmap localBitmap = this.user.getThumbnailBitmap(localImageView1);
      if (localBitmap != null)
        localImageView1.setImageBitmap(localBitmap);
      ((TextView)findViewById(2131165643)).setText(this.user.displayName);
      if ((this.reviews != null) && (!this.reviews.isEmpty()))
      {
        this.viewFlipper.removeAllViews();
        localLayoutInflater = getLayoutInflater();
        localIterator = this.reviews.iterator();
        if (localIterator.hasNext())
          break label150;
        this.viewFlipper.setDisplayedChild(this.reviewIndex);
        setActionBarTitle(getPageTitle());
      }
    }
    return;
    label150: Review localReview = (Review)localIterator.next();
    Movie localMovie = localReview.getMovie();
    ScrollView localScrollView = (ScrollView)localLayoutInflater.inflate(2130903136, this.viewFlipper, false);
    RelativeLayout localRelativeLayout = (RelativeLayout)localScrollView.findViewById(2131165629);
    localRelativeLayout.setTag(localMovie);
    localRelativeLayout.setFocusable(true);
    localRelativeLayout.setOnClickListener(this);
    ImageView localImageView2 = (ImageView)localScrollView.findViewById(2131165630);
    localImageView2.setTag(localMovie);
    if (localMovie.thumbnailSoftBitmap.get() != null)
      localImageView2.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
    String[] arrayOfString;
    int[] arrayOfInt;
    int i;
    while (true)
    {
      arrayOfString = new String[] { "title", "MOVIE_ACTORS_SHORT", "meta" };
      arrayOfInt = new int[] { 2131165631, 2131165632, 2131165633 };
      i = 0;
      if (i < arrayOfString.length)
        break label441;
      ((ImageView)localScrollView.findViewById(2131165636)).setImageResource(Flixster.RATING_LARGE_R[((int)(2.0D * localReview.stars))]);
      ((TextView)localScrollView.findViewById(2131165637)).setText(localReview.comment);
      this.viewFlipper.addView(localScrollView);
      break;
      String str = localMovie.getProperty("thumbnail");
      if ((str != null) && (str.startsWith("http")))
        orderImage(new ImageOrder(0, localMovie, str, localImageView2, this.movieImageHandler));
      else
        localImageView2.setImageResource(2130837839);
    }
    label441: TextView localTextView = (TextView)localScrollView.findViewById(arrayOfInt[i]);
    if (localMovie.checkProperty(arrayOfString[i]))
    {
      localTextView.setText(localMovie.getProperty(arrayOfString[i]));
      localTextView.setVisibility(0);
    }
    while (true)
    {
      i++;
      break;
      localTextView.setVisibility(8);
    }
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
    case 2131165629:
    }
    Movie localMovie;
    do
    {
      return;
      localMovie = (Movie)paramView.getTag();
    }
    while (localMovie == null);
    Starter.launchMovieDetail(localMovie.getId(), this);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.reviewType = localBundle.getInt("REVIEW_TYPE");
      this.reviewIndex = localBundle.getInt("REVIEW_INDEX");
    }
    setContentView(2130903137);
    findViewById(2131165638).setVisibility(8);
    findViewById(2131165640).setVisibility(8);
    findViewById(2131165639).setVisibility(8);
    findViewById(2131165641).setVisibility(8);
    createActionBar();
    setActionBarTitle(2131492955);
    this.viewFlipper = ((ViewFlipper)findViewById(2131165644));
    this.throbber = findViewById(2131165241);
    updatePage();
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        UserReviewPage.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        UserReviewPage.this.hideLoading();
      }
    });
    return localBuilder.create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getSupportMenuInflater().inflate(2131689486, paramMenu);
    return true;
  }

  public void onDestroy()
  {
    super.onDestroy();
    if (this.timer != null)
    {
      this.timer.cancel();
      this.timer.purge();
    }
    this.timer = null;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165963:
      this.viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, 2130968584));
      this.viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, 2130968585));
      this.viewFlipper.showPrevious();
      setActionBarTitle(getPageTitle());
      return true;
    case 2131165964:
    }
    this.viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, 2130968582));
    this.viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, 2130968583));
    this.viewFlipper.showNext();
    setActionBarTitle(getPageTitle());
    return true;
  }

  public void onResume()
  {
    super.onResume();
    if (this.timer == null)
      this.timer = new Timer();
    if ((this.reviews == null) || (this.reviews.isEmpty()))
    {
      showLoading();
      scheduleUpdatePageTask();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.UserReviewPage
 * JD-Core Version:    0.6.2
 */