package net.flixster.android.ads;

import android.content.Context;
import android.os.Handler;
import com.flixster.android.activity.DeepLink;
import com.flixster.android.utils.ImageGetter;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import java.util.Date;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.Starter;
import net.flixster.android.ads.data.GenericAdsDao;
import net.flixster.android.ads.data.GenericAdsDao.AdsListener;
import net.flixster.android.ads.data.GreedyAdsDao;
import net.flixster.android.ads.model.Ad;
import net.flixster.android.ads.model.FlixsterAd;
import net.flixster.android.ads.model.TaggableAd;
import net.flixster.android.data.ApiBuilder;

public class AdManager
{
  public static final String ADCONTEXT_ID = "ADCONTEXT_ID";
  public static final String CM_ACTOR_DETAIL = "ActorDetails";
  public static final String CM_BOX_OFFICE = "MoviesTab";
  public static final String CM_DVD = "DVDTab";
  public static final String CM_DVD_CATEGORY = "Category";
  public static final String CM_FRIENDRATINGS = "FriendRatings";
  public static final String CM_HOMEPAGE = "Homepage";
  public static final String CM_INTERSTITIALTRAILER = "InterstitialTrailer";
  public static final String CM_LAUNCHAD = "LaunchAd";
  public static final String CM_MOVIESHOWTIMES = "MovieShowtimes";
  public static final String CM_MOVIESIVERATED = "MoviesIveRated";
  public static final String CM_MOVIESIWTS = "MoviesIWantToSee";
  public static final String CM_MOVIE_DETAIL_DVD = "MovieDetailsDvd";
  public static final String CM_MOVIE_DETAIL_PLAYING = "MovieDetailsInTheaters";
  public static final String CM_MOVIE_DETAIL_UPCOMING = "MovieDetailsUpcoming";
  public static final String CM_MYFRIENDS = "MyFriends";
  public static final String CM_PREROLL = "Preroll";
  public static final String CM_SEARCHRESULTS = "SearchResults";
  public static final String CM_THEATERINFO = "TheaterInfo";
  public static final String CM_THEATERS = "TheatersTab";
  public static final String CM_TOPACTORS = "TopActors";
  public static final String CM_TOPNEWS = "TopNews";
  public static final String CM_TOPPHOTOS = "TopPhotos";
  public static final String CM_TRAILERBG = "TrailerBG";
  public static final String CM_UPCOMING = "UpcomingTab";
  public static final String EVENT_CLICK = "Click";
  public static final String EVENT_SKIP = "Skip";
  public static final String EVENT_VIEW = "Impression";
  private static AdManager instance;
  private final GenericAdsDao adsDao;
  private final boolean isDisabled;

  public AdManager()
  {
    if ((Properties.instance().isHoneycombTablet()) || (Properties.instance().isGoogleTv()))
    {
      this.adsDao = null;
      this.isDisabled = true;
      return;
    }
    this.adsDao = new GreedyAdsDao();
    this.isDisabled = false;
  }

  public static AdManager instance()
  {
    if (instance == null)
      instance = new AdManager();
    return instance;
  }

  public static void onFlixsterAdClick(FlixsterAd paramFlixsterAd, Context paramContext)
  {
    if (paramFlixsterAd.targetType != null)
    {
      instance().trackEvent(paramFlixsterAd, "Click");
      switch ($SWITCH_TABLE$net$flixster$android$ads$model$FlixsterAd$AdTarget()[paramFlixsterAd.targetType.ordinal()])
      {
      default:
        return;
      case 3:
        Starter.launchMovieDetail(Long.valueOf(paramFlixsterAd.url).longValue(), paramContext);
        return;
      case 1:
        Starter.launchAdFreeTrailer(paramFlixsterAd.url, paramContext);
        return;
      case 2:
      case 4:
      }
      if (DeepLink.isValid(paramFlixsterAd.url))
      {
        DeepLink.launch(paramFlixsterAd.url, paramContext);
        return;
      }
      Starter.launchBrowser(paramFlixsterAd.url, paramContext);
      return;
    }
    Logger.w("FlxAd", "AdManager.onFlixsterAdClick target null");
  }

  public void addDaoListener(GenericAdsDao.AdsListener paramAdsListener)
  {
    if (this.adsDao != null)
      this.adsDao.addListener(paramAdsListener);
  }

  public Ad getAd(String paramString)
  {
    return getAd(paramString, null, null);
  }

  public Ad getAd(String paramString, Handler paramHandler, Long paramLong)
  {
    Ad localAd;
    if (this.isDisabled)
      localAd = null;
    Date localDate;
    do
    {
      return localAd;
      localAd = null;
      if (paramLong != null)
      {
        boolean bool = paramLong.longValue() < 0L;
        localAd = null;
        if (bool)
          localAd = this.adsDao.find(paramString + paramLong.toString(), paramHandler);
      }
      if (localAd == null)
        localAd = this.adsDao.find(paramString, paramHandler);
      if (localAd == null)
        break;
      localDate = FlixsterApplication.sToday;
    }
    while ((localAd.start == null) || (localAd.end == null) || ((localAd.start.before(localDate)) && (localAd.end.after(localDate))));
    return null;
  }

  public String getPayloadDump()
  {
    if (this.isDisabled)
      return "Ads disabled";
    return this.adsDao.getPayloadDump();
  }

  public boolean isActiveAd(String paramString, Long paramLong)
  {
    if (this.isDisabled);
    while (getAd(paramString, null, paramLong) == null)
      return false;
    return true;
  }

  public boolean isInitialized()
  {
    if (this.adsDao == null)
      return false;
    return this.adsDao.isInitialized();
  }

  protected boolean isTabPlacement(String paramString)
  {
    return ("Homepage".equals(paramString)) || ("MoviesTab".equals(paramString)) || ("TheatersTab".equals(paramString)) || ("UpcomingTab".equals(paramString)) || ("DVDTab".equals(paramString)) || ("Category".equals(paramString));
  }

  public void removeDaoListener(GenericAdsDao.AdsListener paramAdsListener)
  {
    if (this.adsDao != null)
      this.adsDao.removeListener(paramAdsListener);
  }

  public void trackEvent(int paramInt, String paramString1, String paramString2)
  {
    if (this.isDisabled)
      return;
    String str = ApiBuilder.adTrack(paramInt, paramString1, paramString2);
    Logger.i("FlxAd", "AdManager.trackEvent tag:" + paramInt + " event:" + paramString1 + " placement:" + paramString2);
    ImageGetter.instance().get(str, null);
  }

  public void trackEvent(TaggableAd paramTaggableAd, String paramString)
  {
    if (this.isDisabled);
    while ((paramTaggableAd == null) || (paramTaggableAd.tag == null))
      return;
    if (paramTaggableAd.cap != null)
    {
      paramTaggableAd.cap = Integer.valueOf(-1 + paramTaggableAd.cap.intValue());
      if (paramTaggableAd.cap.intValue() == 0)
        this.adsDao.delete(paramTaggableAd);
    }
    trackEvent(paramTaggableAd.tag.intValue(), paramString, paramTaggableAd.placement);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.AdManager
 * JD-Core Version:    0.6.2
 */