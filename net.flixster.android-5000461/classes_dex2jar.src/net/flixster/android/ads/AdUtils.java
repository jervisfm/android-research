package net.flixster.android.ads;

import com.flixster.android.utils.Logger;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.flixster.android.FlixUtils;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.ads.model.Ad;
import net.flixster.android.ads.model.AdMobAd;
import net.flixster.android.ads.model.DfpAd;
import net.flixster.android.ads.model.DfpVideoAd;
import net.flixster.android.ads.model.FlixsterAd;
import net.flixster.android.ads.model.FlixsterAd.AdTarget;
import net.flixster.android.ads.model.GoogleAd;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdUtils
{
  private static final String DEFAULT_KEYWORDS = "movie,watch+movies,movie+releases,free+movies,buy+movies,dvd+releases,films";

  private static Ad parseAd(JSONObject paramJSONObject)
  {
    HashMap localHashMap = new HashMap();
    String[] arrayOfString3;
    int j;
    Object localObject;
    if (paramJSONObject.has("other"))
    {
      arrayOfString3 = FlixUtils.copyString(paramJSONObject.optString("other")).split("&");
      int i = arrayOfString3.length;
      j = 0;
      if (j < i);
    }
    else
    {
      long l = 172800000L;
      if (localHashMap.containsKey("installdelay"))
        l = Long.valueOf((String)localHashMap.get("installdelay")).longValue();
      if (l + FlixsterApplication.sInstallMsPosixTime <= FlixsterApplication.sToday.getTime())
        break label150;
      localObject = null;
    }
    label1422: 
    while (true)
    {
      return localObject;
      String[] arrayOfString4 = arrayOfString3[j].split("=");
      if ((arrayOfString4.length == 2) && (arrayOfString4[0] != null) && (arrayOfString4[1] != null))
        localHashMap.put(arrayOfString4[0], arrayOfString4[1]);
      j++;
      break;
      label150: String str1 = FlixUtils.copyString(paramJSONObject.optString("placement"));
      String str2 = FlixUtils.copyString(paramJSONObject.optString("target"));
      if (str2.contentEquals("AdMob"))
      {
        AdMobAd localAdMobAd = new AdMobAd();
        if (paramJSONObject.has("adSenseChannel"))
          localAdMobAd.channelId = FlixUtils.copyString(paramJSONObject.optString("adSenseChannel"));
        if (paramJSONObject.has("adSenseKeywords"))
          localAdMobAd.keywords = FlixUtils.copyString(paramJSONObject.optString("adSenseKeywords"));
        if ((localAdMobAd.keywords == null) || (localAdMobAd.keywords.length() <= 0))
          localAdMobAd.keywords = "movie,watch+movies,movie+releases,free+movies,buy+movies,dvd+releases,films";
        localObject = localAdMobAd;
        ((Ad)localObject).target = str2;
        ((Ad)localObject).placement = str1;
        String str3 = paramJSONObject.optString("cap", null);
        if ((str3 != null) && (!str1.contentEquals("LaunchAd")))
          ((Ad)localObject).cap = Integer.valueOf(Integer.parseInt(str3));
        String str4 = (String)localHashMap.get("sticky");
        ((Ad)localObject).sticky = str4;
        if ((str4 != null) && (!((Ad)localObject).placement.contentEquals("MoviePhotos")) && (!((Ad)localObject).placement.contentEquals("PhotoGallery")) && (!((Ad)localObject).placement.contentEquals("TicketInfo")) && (!((Ad)localObject).placement.contentEquals("ActorDetails")) && (!((Ad)localObject).placement.contentEquals("ActorBio")) && (!((Ad)localObject).placement.contentEquals("BuyTickets")) && (!((Ad)localObject).placement.contentEquals("TopPhotos")) && (!((Ad)localObject).placement.contentEquals("FriendRatings")) && (!((Ad)localObject).placement.contentEquals("MoviesIveRated")) && (!((Ad)localObject).placement.contentEquals("MyFriends")) && (!((Ad)localObject).placement.contentEquals("FriendReview")) && (!((Ad)localObject).placement.contentEquals("MyReview")) && (!((Ad)localObject).placement.contentEquals("TrailerBG")))
        {
          if (!((Ad)localObject).sticky.contentEquals("top"))
            break label1370;
          String str7 = ((Ad)localObject).placement;
          StringBuilder localStringBuilder3 = new StringBuilder(String.valueOf(str7));
          ((Ad)localObject).placement = "StickyTop";
        }
      }
      while (true)
      {
        while (true)
        {
          if (!paramJSONObject.has("contextId"))
            break label1422;
          ((Ad)localObject).contextId = Long.valueOf(paramJSONObject.optLong("contextId"));
          String str5 = ((Ad)localObject).placement;
          StringBuilder localStringBuilder1 = new StringBuilder(String.valueOf(str5));
          ((Ad)localObject).placement = ((Ad)localObject).contextId.toString();
          return localObject;
          if (str2.contentEquals("DART"))
          {
            if (str1.contentEquals("Preroll"))
            {
              DfpVideoAd localDfpVideoAd = new DfpVideoAd();
              String str9 = paramJSONObject.optString("tag", null);
              if (str9 != null)
                localDfpVideoAd.tag = Integer.decode(str9);
              localObject = localDfpVideoAd;
              break;
            }
            if ((str1.contentEquals("LaunchAd")) || (str1.contentEquals("InterstitialTrailer")))
              return null;
            DfpAd localDfpAd = new DfpAd();
            String str10 = paramJSONObject.optString("tag", null);
            if (str10 != null)
              localDfpAd.tag = Integer.decode(str10);
            localObject = localDfpAd;
            break;
          }
          if (str2.contentEquals("AdSense"))
          {
            GoogleAd localGoogleAd = new GoogleAd();
            if (paramJSONObject.has("adSenseKeywords"))
            {
              localGoogleAd.keywords = FlixUtils.copyString(paramJSONObject.optString("adSenseKeywords"));
              label791: if (!paramJSONObject.has("adSenseChannel"))
                break label928;
              localGoogleAd.channelId = FlixUtils.copyString(paramJSONObject.optString("adSenseChannel"));
            }
            try
            {
              label814: if (localHashMap.containsKey("bcolor"));
              for (localGoogleAd.bcolor = URLDecoder.decode((String)localHashMap.get("bcolor"), "UTF-8"); ; localGoogleAd.bcolor = "#525152")
              {
                if (localHashMap.containsKey("fcolor"))
                  localGoogleAd.fcolor = URLDecoder.decode((String)localHashMap.get("fcolor"), "UTF-8");
                localGoogleAd.webUrl = FlixUtils.copyString(paramJSONObject.optString("adSenseWebURL", null));
                localObject = localGoogleAd;
                break;
                if (paramJSONObject.has("adsenseKeywords"))
                {
                  localGoogleAd.keywords = FlixUtils.copyString(paramJSONObject.optString("adsenseKeywords"));
                  break label791;
                }
                localGoogleAd.keywords = "movie,watch+movies,movie+releases,free+movies,buy+movies,dvd+releases,films";
                break label791;
                label928: localGoogleAd.channelId = "520867217";
                break label814;
              }
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
              while (true)
                localUnsupportedEncodingException.printStackTrace();
            }
          }
        }
        if (str2.contentEquals("MobClix"))
          return null;
        if (str2.contentEquals("web"))
          return null;
        if (str2.contentEquals("Medialets"))
          return null;
        if (str2.contentEquals("transpera"))
          return null;
        FlixsterAd localFlixsterAd = new FlixsterAd();
        String[] arrayOfString1 = str2.split("\\|");
        if ((arrayOfString1 != null) && (arrayOfString1.length == 2))
        {
          localFlixsterAd.url = arrayOfString1[1];
          if (localFlixsterAd.url.startsWith("www"))
            localFlixsterAd.url = ("http://" + localFlixsterAd.url);
          if (!FlixsterAd.AdTarget.MOVIE.literal.equals(arrayOfString1[0]))
            break label1246;
          localFlixsterAd.targetType = FlixsterAd.AdTarget.MOVIE;
        }
        while (true)
        {
          localFlixsterAd.tag = Integer.decode(paramJSONObject.optString("tag", "0"));
          localFlixsterAd.imageUrl = FlixUtils.copyString(paramJSONObject.optString("image", null));
          String str8 = paramJSONObject.optString("size", null);
          if ((str8 != null) && (str8.contains("x")))
          {
            String[] arrayOfString2 = str8.split("x");
            localFlixsterAd.width = Integer.decode(arrayOfString2[0]);
            localFlixsterAd.height = Integer.decode(arrayOfString2[1]);
          }
          localFlixsterAd.button1 = FlixUtils.copyString(paramJSONObject.optString("button1", null));
          localFlixsterAd.button2 = FlixUtils.copyString(paramJSONObject.optString("button2", null));
          localObject = localFlixsterAd;
          break;
          label1246: if (FlixsterAd.AdTarget.URL.literal.equals(arrayOfString1[0]))
          {
            localFlixsterAd.targetType = FlixsterAd.AdTarget.URL;
          }
          else if (FlixsterAd.AdTarget.TRAILER.literal.equals(arrayOfString1[0]))
          {
            localFlixsterAd.targetType = FlixsterAd.AdTarget.TRAILER;
          }
          else if (FlixsterAd.AdTarget.PRODUCT.literal.equals(arrayOfString1[0]))
          {
            localFlixsterAd.targetType = FlixsterAd.AdTarget.PRODUCT;
          }
          else if (FlixsterAd.AdTarget.HTML.literal.equals(arrayOfString1[0]))
          {
            localFlixsterAd.targetType = FlixsterAd.AdTarget.HTML;
            localFlixsterAd.title = FlixUtils.copyString(paramJSONObject.optString("title", null));
          }
        }
        label1370: if (((Ad)localObject).sticky.contentEquals("bottom"))
        {
          String str6 = ((Ad)localObject).placement;
          StringBuilder localStringBuilder2 = new StringBuilder(String.valueOf(str6));
          ((Ad)localObject).placement = "StickyBottom";
        }
      }
    }
  }

  public static List<Ad> parseAds(JSONArray paramJSONArray)
  {
    ArrayList localArrayList = new ArrayList();
    int i = paramJSONArray.length();
    Logger.d("FlxAd", "AdUtils.parseAds size:" + i);
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return localArrayList;
      Ad localAd = parseAd(paramJSONArray.optJSONObject(j));
      if (localAd != null)
        localArrayList.add(localAd);
    }
  }

  public static Set<String> splitKeywords(String paramString)
  {
    HashSet localHashSet = new HashSet();
    String[] arrayOfString = paramString.split(",");
    int i = arrayOfString.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return localHashSet;
      localHashSet.add(arrayOfString[j].trim());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.AdUtils
 * JD-Core Version:    0.6.2
 */