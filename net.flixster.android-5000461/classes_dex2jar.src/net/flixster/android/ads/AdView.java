package net.flixster.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import com.flixster.android.ads.HtmlAdBannerView;
import com.flixster.android.utils.Logger;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdSenseSpec.AdType;
import com.google.ads.AdSenseSpec.ExpandDirection;
import com.google.ads.AdSize;
import com.google.ads.GoogleAdView;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.R.styleable;
import net.flixster.android.ads.model.AdMobAd;
import net.flixster.android.ads.model.DfpAd;
import net.flixster.android.ads.model.DfpAd.DfpCustomTarget;
import net.flixster.android.ads.model.ExtendedAdSenseSpec;
import net.flixster.android.ads.model.FlixsterAd;
import net.flixster.android.ads.model.FlixsterAd.AdTarget;
import net.flixster.android.ads.model.GoogleAd;
import net.flixster.android.model.ImageOrder;

public class AdView extends LinearLayout
{
  private static final LinearLayout.LayoutParams LAYOUT = new LinearLayout.LayoutParams(-1, -2);
  private static final Handler onImageLoad = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      try
      {
        ImageView localImageView = (ImageView)paramAnonymousMessage.obj;
        Logger.d("FlxAd", "AdView.onImageLoad called! adView:" + localImageView);
        if (localImageView != null)
        {
          FlixsterAd localFlixsterAd = (FlixsterAd)localImageView.getTag();
          Logger.d("FlxAd", "AdView.onImageLoad adView.getTag():" + localImageView.getTag());
          if (localFlixsterAd.bitmap != null)
          {
            Logger.d("FlxAd", "AdView.onImageLoad ad.bitmap:" + localFlixsterAd.bitmap);
            localImageView.setImageBitmap(localFlixsterAd.bitmap);
            localImageView.setMinimumHeight((int)(FlixsterApplication.sRelativePixel * localFlixsterAd.bitmap.getHeight()));
            localImageView.setBackgroundColor(localFlixsterAd.bitmap.getPixel(localFlixsterAd.bitmap.getWidth() / 2, -1 + localFlixsterAd.bitmap.getHeight()));
            localImageView.invalidate();
            AdManager.instance().trackEvent(localFlixsterAd, "Impression");
          }
        }
        return;
      }
      catch (Exception localException)
      {
        Logger.e("FlxAd", "Couldn't set image for ad.", localException);
      }
    }
  };
  private volatile net.flixster.android.ads.model.Ad ad;
  public DfpAd.DfpCustomTarget dfpCustomTarget;
  private AdListener googleAdListener;
  private boolean isDfpAdRetry;
  private boolean isTabPlacement;
  public Long mAdContextId = null;
  private final Context mContext;
  private View mView;
  private final Handler onAdLoad = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxAd", "AdView.onAdLoad.handleMessage");
      AdView.this.refreshAds();
    }
  };
  private final View.OnClickListener onClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      AdManager.onFlixsterAdClick((FlixsterAd)paramAnonymousView.getTag(), AdView.this.mContext);
    }
  };
  private String placement;

  public AdView(Context paramContext)
  {
    super(paramContext);
    setOrientation(1);
    this.mContext = paramContext;
    setSlot(null);
  }

  public AdView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setOrientation(1);
    this.mContext = paramContext;
    setSlot(paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AdView).getString(0));
  }

  public AdView(Context paramContext, String paramString)
  {
    super(paramContext);
    setOrientation(1);
    this.mContext = paramContext;
    setSlot(paramString);
  }

  private View getAdView(net.flixster.android.ads.model.Ad paramAd)
  {
    Logger.d("FlxAd", "AdView.getAdView ad:" + paramAd + " mView:" + this.mView);
    if ((paramAd instanceof FlixsterAd))
    {
      FlixsterAd localFlixsterAd = (FlixsterAd)paramAd;
      if (localFlixsterAd.targetType == FlixsterAd.AdTarget.HTML)
      {
        Logger.d("FlxAd", "AdView.getAdView new HtmlAdBannerView");
        HtmlAdBannerView localHtmlAdBannerView = new HtmlAdBannerView(this.mContext, localFlixsterAd);
        localHtmlAdBannerView.loadUrl(localFlixsterAd.url);
        this.mView = localHtmlAdBannerView;
        return this.mView;
      }
      ImageView localImageView = new ImageView(this.mContext);
      Logger.d("FlxAd", "AdView.getAdView new ImageView");
      localImageView.setTag(localFlixsterAd);
      localImageView.setVisibility(0);
      localImageView.setAdjustViewBounds(false);
      localImageView.setFocusable(true);
      if (localFlixsterAd.width != null)
        localImageView.setLayoutParams(new LinearLayout.LayoutParams(localFlixsterAd.width.intValue(), localFlixsterAd.height.intValue()));
      localImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
      localImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
      if (localFlixsterAd.bitmap == null)
        ImageManager.getInstance().enqueImage(new ImageOrder(0, localFlixsterAd, localFlixsterAd.imageUrl, localImageView, onImageLoad));
      while (true)
      {
        localImageView.setOnClickListener(this.onClickListener);
        this.mView = localImageView;
        break;
        localImageView.setImageBitmap(localFlixsterAd.bitmap);
        localImageView.setMinimumHeight((int)(FlixsterApplication.sRelativePixel * localFlixsterAd.bitmap.getHeight()));
        setBackgroundColor(localFlixsterAd.bitmap.getPixel(localFlixsterAd.bitmap.getWidth() / 2, -1 + localFlixsterAd.bitmap.getHeight()));
        AdManager.instance().trackEvent(localFlixsterAd, "Impression");
      }
    }
    if ((paramAd instanceof AdMobAd))
      if (FlixsterApplication.getAndroidBuildInt() >= 4)
      {
        if ((this.mContext instanceof Activity))
        {
          String str5 = ((AdMobAd)paramAd).channelId;
          String str6 = ((AdMobAd)paramAd).keywords;
          Activity localActivity = (Activity)this.mContext;
          AdSize localAdSize2 = AdSize.BANNER;
          if (str5 != null);
          while (true)
          {
            com.google.ads.AdView localAdView2 = new com.google.ads.AdView(localActivity, localAdSize2, str5);
            AdRequest localAdRequest2 = new AdRequest();
            if (str6 != null)
              localAdRequest2.setKeywords(AdUtils.splitKeywords(str6));
            localAdView2.setAdListener(getGoogleAdListener());
            localAdView2.loadAd(localAdRequest2);
            this.mView = localAdView2;
            return this.mView;
            str5 = "a14b625b592ec46";
          }
        }
        Logger.e("FlxAd", "Activity context needed for AdMob: " + this.mContext);
      }
    label672: 
    do
      while (true)
      {
        Logger.w("FlxAd", "AdView.getAdView unsupported ad type: " + paramAd);
        return null;
        if (!(paramAd instanceof DfpAd))
          break;
        if (FlixsterApplication.getAndroidBuildInt() >= 4)
        {
          if ((this.mContext instanceof Activity))
          {
            int i;
            com.google.ads.AdView localAdView1;
            AdRequest localAdRequest1;
            if ((!this.isTabPlacement) || (this.isDfpAdRetry))
            {
              i = 50;
              AdSize localAdSize1 = new AdSize(320, i);
              String str4 = DfpAd.buildAdUnitId(paramAd.placement);
              localAdView1 = new com.google.ads.AdView((Activity)this.mContext, localAdSize1, str4);
              localAdView1.setAdListener(getGoogleAdListener());
              localAdRequest1 = new AdRequest();
              Location localLocation = FlixsterApplication.getCurrentLocation();
              if (localLocation != null)
                localAdRequest1.setLocation(localLocation);
              if (this.dfpCustomTarget == null)
                break label672;
              this.dfpCustomTarget.addCustomKeyValuePairsTo(localAdRequest1);
            }
            while (true)
            {
              localAdView1.loadAd(localAdRequest1);
              this.mView = localAdView1;
              return this.mView;
              i = 150;
              break;
              if (DfpAd.DfpCustomTarget.getTestFlag() != null)
                DfpAd.DfpCustomTarget.addCustomTargetPairTo(localAdRequest1);
            }
          }
          Logger.e("FlxAd", "Activity context needed for DFP: " + this.mContext);
        }
      }
    while (!(paramAd instanceof GoogleAd));
    if (FlixsterApplication.getAndroidBuildInt() < 4)
      return null;
    RelativeLayout localRelativeLayout = new RelativeLayout(this.mContext);
    localRelativeLayout.setHorizontalGravity(1);
    localRelativeLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
    GoogleAd localGoogleAd = (GoogleAd)paramAd;
    GoogleAdView localGoogleAdView = new GoogleAdView(this.mContext);
    Logger.d("FlxAd", "AdView.getAdView new GoogleAdView");
    String str1 = getResources().getConfiguration().locale.getCountry();
    Object localObject = FlixsterApplication.getUserZip();
    try
    {
      if (FlixsterApplication.getUseLocationServiceFlag())
      {
        localObject = FlixsterApplication.getCurrentZip();
        if ((localObject == null) || (((String)localObject).length() == 0))
        {
          localObject = FlixsterApplication.getCurrentCity();
          if (localObject != null)
          {
            String str3 = URLEncoder.encode((String)localObject, "UTF-8");
            localObject = str3;
          }
        }
      }
      while (true)
      {
        Logger.d("FlxAd", "AdView.getAdView country:" + str1 + " city:" + (String)localObject);
        localExtendedAdSenseSpec = new ExtendedAdSenseSpec("ca-mb-app-pub-7518399278719852");
        localExtendedAdSenseSpec.setUserCountry(str1);
        localExtendedAdSenseSpec.setUserCity((String)localObject);
        localExtendedAdSenseSpec.setCompanyName("Flixster Inc.");
        localExtendedAdSenseSpec.setAppName("Movies");
        localExtendedAdSenseSpec.setAdType(AdSenseSpec.AdType.TEXT_IMAGE);
        localExtendedAdSenseSpec.setKeywords(localGoogleAd.keywords);
        localExtendedAdSenseSpec.setChannel(localGoogleAd.channelId);
        localExtendedAdSenseSpec.setAdTestEnabled(false);
        if ((localGoogleAd.sticky == null) || (!localGoogleAd.sticky.contentEquals("bottom")))
          break;
        localExtendedAdSenseSpec.setExpandDirection(AdSenseSpec.ExpandDirection.TOP);
        if (localGoogleAd.bcolor != null)
          localExtendedAdSenseSpec.setColorBackground(localGoogleAd.bcolor);
        if (localGoogleAd.fcolor != null)
          localExtendedAdSenseSpec.setColorText(localGoogleAd.fcolor);
        localGoogleAdView.showAds(localExtendedAdSenseSpec);
        localGoogleAdView.setFocusable(false);
        localGoogleAdView.setFocusableInTouchMode(false);
        localRelativeLayout.addView(localGoogleAdView);
        localRelativeLayout.setBackgroundColor(getResources().getColor(2131296279));
        this.mView = localRelativeLayout;
        return this.mView;
        localObject = FlixsterApplication.getUserZip();
        if ((localObject == null) || (((String)localObject).length() == 0))
        {
          localObject = FlixsterApplication.getUserCity();
          if (localObject != null)
          {
            String str2 = URLEncoder.encode((String)localObject, "UTF-8");
            localObject = str2;
          }
        }
      }
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      while (true)
      {
        ExtendedAdSenseSpec localExtendedAdSenseSpec;
        localUnsupportedEncodingException.printStackTrace();
        continue;
        localExtendedAdSenseSpec.setExpandDirection(AdSenseSpec.ExpandDirection.BOTTOM);
      }
    }
  }

  private AdListener getGoogleAdListener()
  {
    if (this.googleAdListener == null)
      this.googleAdListener = new GoogleAdListener(null);
    return this.googleAdListener;
  }

  public void destroy()
  {
    if ((this.mView instanceof com.google.ads.AdView))
    {
      Logger.v("FlxAd", "AdView.destroy AdMob");
      if (this.mView != null)
        ((com.google.ads.AdView)this.mView).destroy();
    }
    while (true)
    {
      this.mView = null;
      return;
      Logger.v("FlxAd", "AdView.destroy null/na");
    }
  }

  public void refreshAds()
  {
    if (this.mView != null)
      destroy();
    this.ad = AdManager.instance().getAd(this.placement, this.onAdLoad, this.mAdContextId);
    Logger.v("FlxAd", "AdView.refreshAds slot:" + this.placement + " ad:" + this.ad);
    if (this.ad != null)
    {
      View localView = getAdView(this.ad);
      if (localView != null)
      {
        removeAllViews();
        addView(localView, LAYOUT);
        invalidate();
      }
      if (this.isTabPlacement)
        setMinimumHeight(getResources().getDimensionPixelOffset(2131361828));
      while (true)
      {
        setBackgroundColor(getResources().getColor(2131296279));
        setVisibility(0);
        return;
        setMinimumHeight(getResources().getDimensionPixelOffset(2131361828));
      }
    }
    setVisibility(8);
  }

  public void setSlot(String paramString)
  {
    Logger.v("FlxAd", "AdView.setPlacement " + paramString);
    this.placement = paramString;
    this.isTabPlacement = AdManager.instance().isTabPlacement(paramString);
  }

  private class GoogleAdListener
    implements AdListener
  {
    private GoogleAdListener()
    {
    }

    public void onDismissScreen(com.google.ads.Ad paramAd)
    {
      Logger.d("FlxAd", "AdView.GoogleAdListener.onDismissScreen");
    }

    public void onFailedToReceiveAd(com.google.ads.Ad paramAd, AdRequest.ErrorCode paramErrorCode)
    {
      Logger.sw("FlxAd", "AdView.GoogleAdListener.onFailedToReceiveAd errorCode:" + paramErrorCode);
      if ((AdView.this.isTabPlacement) && (!AdView.this.isDfpAdRetry) && ((AdView.this.ad instanceof DfpAd)))
      {
        AdView.this.isDfpAdRetry = true;
        AdView.this.refreshAds();
        return;
      }
      AdView.this.setMinimumHeight(0);
      AdView.this.removeAllViews();
      AdView.this.setVisibility(8);
    }

    public void onLeaveApplication(com.google.ads.Ad paramAd)
    {
      Logger.d("FlxAd", "AdView.GoogleAdListener.onLeaveApplication");
      if ((AdView.this.ad != null) && ((AdView.this.ad instanceof DfpAd)))
        AdManager.instance().trackEvent(((DfpAd)AdView.this.ad).tag.intValue(), "Click", AdView.this.ad.placement);
    }

    public void onPresentScreen(com.google.ads.Ad paramAd)
    {
      Logger.d("FlxAd", "AdView.GoogleAdListener.onPresentScreen");
      if ((AdView.this.ad != null) && ((AdView.this.ad instanceof DfpAd)))
        AdManager.instance().trackEvent(((DfpAd)AdView.this.ad).tag.intValue(), "Click", AdView.this.ad.placement);
    }

    public void onReceiveAd(com.google.ads.Ad paramAd)
    {
      Logger.d("FlxAd", "AdView.GoogleAdListener.onReceiveAd");
      if ((AdView.this.ad != null) && ((AdView.this.ad instanceof DfpAd)))
        AdManager.instance().trackEvent(((DfpAd)AdView.this.ad).tag.intValue(), "Impression", AdView.this.ad.placement);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.AdView
 * JD-Core Version:    0.6.2
 */