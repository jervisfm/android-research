package net.flixster.android.ads;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import com.flixster.android.utils.Logger;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import net.flixster.android.ads.model.BitmapModel;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.util.HttpImageHelper;

public class ImageManager extends Thread
{
  private static final String TAG = ImageManager.class.getName();
  private static ImageManager mInstance;
  private BlockingQueue<ImageOrder> mQueue;

  public ImageManager(String paramString)
  {
    super(paramString);
  }

  public static ImageManager getInstance()
  {
    if (mInstance == null)
      mInstance = new ImageManager(TAG);
    return mInstance;
  }

  public void enqueImage(ImageOrder paramImageOrder)
  {
    if (this.mQueue == null)
    {
      this.mQueue = new LinkedBlockingQueue();
      start();
    }
    try
    {
      this.mQueue.put(paramImageOrder);
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      Logger.e(TAG, "Error queueing image", localInterruptedException);
    }
  }

  public void run()
  {
    while (true)
      try
      {
        ImageOrder localImageOrder = (ImageOrder)this.mQueue.take();
        Bitmap localBitmap = HttpImageHelper.fetchImage(new URL(localImageOrder.urlString));
        Logger.d("FlxMain", "ImageManager.run() imageOrder:" + localImageOrder);
        if (localImageOrder.tag != null)
        {
          Logger.d("FlxMain", "ImageManager.run() imageOrder.tag:" + localImageOrder.tag);
          ((BitmapModel)localImageOrder.tag).bitmap = localBitmap;
        }
        if (localImageOrder.refreshHandler != null)
          localImageOrder.refreshHandler.sendMessage(Message.obtain(localImageOrder.refreshHandler, 0, localImageOrder.movieView));
      }
      catch (InterruptedException localInterruptedException)
      {
        Logger.e(TAG, "Image queue interrupted exception", localInterruptedException);
      }
      catch (IOException localIOException)
      {
        Logger.e(TAG, "Error getting image", localIOException);
      }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.ImageManager
 * JD-Core Version:    0.6.2
 */