package net.flixster.android.ads;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.F;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import net.flixster.android.Starter;
import net.flixster.android.ads.model.FlixsterAd;
import net.flixster.android.model.ImageOrder;

public class InterstitialPage extends Activity
  implements View.OnClickListener
{
  public static final String CUSTOM_INTERSTITIAL = "CustomInterstitial";
  public static final String SLOT_TYPE = "SLOT_TYPE";
  public Handler imageLoaded = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (InterstitialPage.this.isFinishing())
        Logger.w("FlxAd", "InterstitialPage.imageLoaded skipped");
      label185: 
      while (true)
      {
        return;
        Logger.d("FlxAd", "InterstitialPage.imageLoaded");
        InterstitialPage.this.mImageView.setImageBitmap(InterstitialPage.this.mFlixsterAd.bitmap);
        AdManager.instance().trackEvent(InterstitialPage.this.mFlixsterAd, "Impression");
        if (!InterstitialPage.this.mSlotType.contentEquals("CustomInterstitial"))
          Trackers.instance().trackEvent("/launch/interstitial", "launch", "LaunchInterstitial", "Impression");
        while (true)
        {
          if ((InterstitialPage.this.mFlixsterAd == null) || (InterstitialPage.this.mFlixsterAd.bitmap == null))
            break label185;
          int i = InterstitialPage.this.mFlixsterAd.bitmap.getHeight();
          if (i <= 100)
            break;
          int j = InterstitialPage.this.mFlixsterAd.bitmap.getPixel(160, i - 1);
          InterstitialPage.this.mImageView.setBackgroundColor(j);
          return;
          InterstitialPage.this.mImageView.setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramAnonymous2View)
            {
              Trackers.instance().trackEvent("/7eleven", "7-Eleven", "7Eleven", "Interstitial", "Click", 0);
              Starter.launchMaps("7 eleven", InterstitialPage.this);
            }
          });
        }
      }
    }
  };
  private Button mButton1;
  private Button mButton2;
  private FlixsterAd mFlixsterAd;
  private ImageView mImageView;
  private String mSlotType;

  public void onClick(View paramView)
  {
    Logger.d("FlxAd", "InterstitialPage.onClick view id " + paramView.getId());
    if (paramView.getId() == 2131165396)
    {
      AdManager.instance().trackEvent(this.mFlixsterAd, "Skip");
      if (this.mSlotType.contentEquals("CustomInterstitial"))
        Trackers.instance().trackEvent("/7eleven", "7-Eleven", "7Eleven", "Interstitial", "Skip", 0);
      while (true)
      {
        finish();
        return;
        Trackers.instance().trackEvent("/launch/interstitial", "launch", "LaunchInterstitial", "Skip");
      }
    }
    if (this.mSlotType.contentEquals("CustomInterstitial"))
    {
      Trackers.instance().trackEvent("/7eleven", "7-Eleven", "7Eleven", "Interstitial", "Click", 0);
      Starter.launchMaps("7 eleven", this);
      return;
    }
    Trackers.instance().trackEvent("/launch/interstitial", "launch", "LaunchInterstitial", "Click");
    AdManager.onFlixsterAdClick(this.mFlixsterAd, this);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (F.API_LEVEL >= 9);
    for (int i = 7; ; i = 1)
    {
      setRequestedOrientation(i);
      setContentView(2130903095);
      this.mButton1 = ((Button)findViewById(2131165395));
      this.mButton2 = ((Button)findViewById(2131165396));
      this.mImageView = ((ImageView)findViewById(2131165393));
      this.mButton1.setOnClickListener(this);
      this.mButton2.setOnClickListener(this);
      this.mImageView.setFocusable(false);
      this.mImageView.setOnClickListener(null);
      this.mSlotType = getIntent().getExtras().getString("SLOT_TYPE");
      Logger.d("FlxAd", "InterstitialPage.onCreate SLOT_TYPE:" + this.mSlotType);
      if (this.mSlotType != null)
        break;
      finish();
      return;
    }
    if (this.mSlotType.contentEquals("CustomInterstitial"))
    {
      this.mFlixsterAd = ((FlixsterAd)ObjectHolder.instance().remove("CustomInterstitial"));
      Trackers.instance().trackEvent("/7eleven", "7-Eleven", "7Eleven", "Interstitial", "Impression", 0);
    }
    Logger.d("FlxAd", "InterstitialPage.onCreate mFlixsterAd:" + this.mFlixsterAd);
    if (this.mFlixsterAd == null)
    {
      finish();
      return;
    }
    if (this.mFlixsterAd.imageUrl != null)
      ImageManager.getInstance().enqueImage(new ImageOrder(0, this.mFlixsterAd, this.mFlixsterAd.imageUrl, this.mImageView, this.imageLoaded));
    if (this.mFlixsterAd.button1 != null)
      if ((this.mFlixsterAd.url != null) && (this.mFlixsterAd.url.contentEquals("flixster://flixster.com/mobile/android/facebookauth")))
      {
        this.mButton1.setText("Login with Facebook");
        this.mButton1.setTextSize(getResources().getDimensionPixelSize(2131361830));
        this.mButton1.setBackgroundResource(2130837611);
        int j = getResources().getDimensionPixelSize(2131361831);
        int k = getResources().getDimensionPixelSize(2131361832);
        int m = getResources().getDimensionPixelSize(2131361833);
        Drawable localDrawable = getResources().getDrawable(2130837758);
        localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
        this.mButton1.setCompoundDrawables(localDrawable, null, null, null);
        this.mButton1.setPadding(k, j, m, j);
        this.mButton1.setShadowLayer(2.0F, 0.0F, -2.0F, 2131296291);
      }
    while (this.mFlixsterAd.button2 != null)
    {
      this.mButton2.setText(this.mFlixsterAd.button2);
      return;
      this.mButton1.setText(this.mFlixsterAd.button1);
      continue;
      this.mButton1.setVisibility(8);
    }
    this.mButton2.setVisibility(8);
  }

  public void onDestroy()
  {
    super.onDestroy();
    Logger.d("FlxAd", "InterstitialPage.onDestroy");
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getAction() == 0) && (paramInt == 4))
    {
      Logger.d("FlxMain", "InterstitialPage.onKeyDown KEYCODE_BACK");
      AdManager.instance().trackEvent(this.mFlixsterAd, "Skip");
      if (!this.mSlotType.contentEquals("CustomInterstitial"))
        break label71;
      Trackers.instance().trackEvent("/7eleven", "7-Eleven", "7Eleven", "Interstitial", "Skip", 0);
    }
    while (true)
    {
      return super.onKeyDown(paramInt, paramKeyEvent);
      label71: Trackers.instance().trackEvent("/launch/interstitial", "launch", "LaunchInterstitial", "Skip");
    }
  }

  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    float f1 = this.mImageView.getHeight();
    float f2 = this.mImageView.getWidth();
    float f3 = f2 / f1;
    Logger.d("FlxAd", "InterstitialPage.onWindowFocusChanged width:" + f2 + " height:" + f1 + " ratio:" + f3);
    if (f3 < 0.6956D)
    {
      this.mImageView.setScaleType(ImageView.ScaleType.FIT_START);
      return;
    }
    this.mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.InterstitialPage
 * JD-Core Version:    0.6.2
 */