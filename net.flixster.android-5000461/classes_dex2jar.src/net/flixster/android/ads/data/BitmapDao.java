package net.flixster.android.ads.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import com.flixster.android.utils.Logger;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class BitmapDao
{
  private static final String TAG = BitmapDao.class.getName();
  private Context context;

  public BitmapDao(Context paramContext)
  {
    this.context = paramContext;
  }

  public void create(String paramString, Bitmap paramBitmap)
  {
    try
    {
      FileOutputStream localFileOutputStream = this.context.openFileOutput(paramString, 0);
      paramBitmap.compress(Bitmap.CompressFormat.JPEG, 100, localFileOutputStream);
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
    }
  }

  public Bitmap find(String paramString)
  {
    try
    {
      Bitmap localBitmap = BitmapFactory.decodeStream(this.context.openFileInput(paramString));
      return localBitmap;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      Logger.e(TAG, "Couldn't get image.", localFileNotFoundException);
    }
    return null;
  }

  public void remove(String paramString)
  {
    this.context.deleteFile(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.data.BitmapDao
 * JD-Core Version:    0.6.2
 */