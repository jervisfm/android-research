package net.flixster.android.ads.data;

import android.os.Handler;
import net.flixster.android.ads.model.Ad;

public abstract interface GenericAdsDao
{
  public abstract void addListener(AdsListener paramAdsListener);

  public abstract void create(Ad paramAd);

  public abstract void delete(Ad paramAd);

  public abstract Ad find(String paramString);

  public abstract Ad find(String paramString, Handler paramHandler);

  public abstract String getPayloadDump();

  public abstract boolean isInitialized();

  public abstract void removeListener(AdsListener paramAdsListener);

  public static abstract interface AdsListener
  {
    public abstract void onAdsLoaded();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.data.GenericAdsDao
 * JD-Core Version:    0.6.2
 */