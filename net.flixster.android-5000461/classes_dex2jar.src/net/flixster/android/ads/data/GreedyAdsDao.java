package net.flixster.android.ads.data;

import android.os.Handler;
import com.flixster.android.utils.Logger;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.flixster.android.ads.AdUtils;
import net.flixster.android.ads.model.Ad;
import net.flixster.android.ads.model.AdMobAd;
import net.flixster.android.ads.model.TaggableAd;
import net.flixster.android.data.ApiBuilder;
import net.flixster.android.util.HttpHelper;
import org.json.JSONArray;
import org.json.JSONException;

public class GreedyAdsDao
  implements GenericAdsDao
{
  private static final int MAX_RETRYS = 2;
  private HashMap<String, Ad> adMap = new HashMap();
  private volatile boolean isInitialized;
  private final List<GenericAdsDao.AdsListener> listeners = new ArrayList(1);
  private volatile Handler mLastHandler;
  private long mLastUpdate = 0L;
  private int mNetworkRetryCount = 0;

  public GreedyAdsDao()
  {
    refreshAdsIfNeeded();
  }

  private Map<String, Ad> getAds()
    throws IOException, JSONException
  {
    if (this.adMap != null)
      this.adMap.clear();
    Iterator localIterator = AdUtils.parseAds(new JSONArray(HttpHelper.fetchUrl(new URL(ApiBuilder.ads()), false, false))).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return this.adMap;
      create((Ad)localIterator.next());
    }
  }

  private void refreshAdsIfNeeded()
  {
    long l = System.currentTimeMillis();
    if (l - this.mLastUpdate > 1800000L)
    {
      this.mLastUpdate = l;
      new AdFetchThread(null).start();
    }
  }

  public void addListener(GenericAdsDao.AdsListener paramAdsListener)
  {
    synchronized (this.listeners)
    {
      this.listeners.add(paramAdsListener);
      return;
    }
  }

  public void create(Ad paramAd)
  {
    this.adMap.put(paramAd.placement, paramAd);
  }

  public void delete(Ad paramAd)
  {
    this.adMap.remove(paramAd.placement);
  }

  public Ad find(String paramString)
  {
    Ad localAd = (Ad)this.adMap.get(paramString);
    Logger.v("FlxAd", "GreedyAdsDao.find " + paramString + ":" + localAd);
    return localAd;
  }

  public Ad find(String paramString, Handler paramHandler)
  {
    refreshAdsIfNeeded();
    if (paramHandler != null);
    try
    {
      this.mLastHandler = paramHandler;
      return find(paramString);
    }
    finally
    {
    }
  }

  public String getPayloadDump()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = this.adMap.values().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return localStringBuilder.toString();
      Ad localAd = (Ad)localIterator.next();
      localStringBuilder.append(localAd.placement);
      if ((localAd instanceof TaggableAd))
        localStringBuilder.append("  tag:").append(((TaggableAd)localAd).tag);
      localStringBuilder.append("  target:").append(localAd.target);
      if (localAd.contextId != null)
        localStringBuilder.append("  ctxtId:").append(localAd.contextId);
      if ((localAd instanceof AdMobAd))
        localStringBuilder.append("  chId:").append(((AdMobAd)localAd).channelId);
      localStringBuilder.append("\n\n");
    }
  }

  public boolean isInitialized()
  {
    return this.isInitialized;
  }

  public void removeListener(GenericAdsDao.AdsListener paramAdsListener)
  {
    synchronized (this.listeners)
    {
      this.listeners.remove(paramAdsListener);
      return;
    }
  }

  private final class AdFetchThread extends Thread
  {
    private AdFetchThread()
    {
    }

    // ERROR //
    public void run()
    {
      // Byte code:
      //   0: aload_0
      //   1: ldc 2
      //   3: invokevirtual 25	java/lang/Class:getName	()Ljava/lang/String;
      //   6: invokevirtual 29	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:setName	(Ljava/lang/String;)V
      //   9: ldc 31
      //   11: ldc 33
      //   13: invokestatic 39	com/flixster/android/utils/Logger:i	(Ljava/lang/String;Ljava/lang/String;)V
      //   16: aload_0
      //   17: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   20: invokestatic 45	net/flixster/android/ads/data/GreedyAdsDao:access$0	(Lnet/flixster/android/ads/data/GreedyAdsDao;)Ljava/util/Map;
      //   23: pop
      //   24: aload_0
      //   25: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   28: iconst_1
      //   29: invokestatic 49	net/flixster/android/ads/data/GreedyAdsDao:access$1	(Lnet/flixster/android/ads/data/GreedyAdsDao;Z)V
      //   32: ldc 31
      //   34: ldc 51
      //   36: invokestatic 39	com/flixster/android/utils/Logger:i	(Ljava/lang/String;Ljava/lang/String;)V
      //   39: aload_0
      //   40: monitorenter
      //   41: aload_0
      //   42: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   45: invokestatic 55	net/flixster/android/ads/data/GreedyAdsDao:access$2	(Lnet/flixster/android/ads/data/GreedyAdsDao;)Landroid/os/Handler;
      //   48: ifnull +23 -> 71
      //   51: aload_0
      //   52: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   55: invokestatic 55	net/flixster/android/ads/data/GreedyAdsDao:access$2	(Lnet/flixster/android/ads/data/GreedyAdsDao;)Landroid/os/Handler;
      //   58: iconst_5
      //   59: invokevirtual 61	android/os/Handler:sendEmptyMessage	(I)Z
      //   62: pop
      //   63: aload_0
      //   64: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   67: aconst_null
      //   68: invokestatic 65	net/flixster/android/ads/data/GreedyAdsDao:access$3	(Lnet/flixster/android/ads/data/GreedyAdsDao;Landroid/os/Handler;)V
      //   71: aload_0
      //   72: monitorexit
      //   73: aload_0
      //   74: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   77: iconst_0
      //   78: invokestatic 69	net/flixster/android/ads/data/GreedyAdsDao:access$4	(Lnet/flixster/android/ads/data/GreedyAdsDao;I)V
      //   81: aload_0
      //   82: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   85: invokestatic 73	net/flixster/android/ads/data/GreedyAdsDao:access$5	(Lnet/flixster/android/ads/data/GreedyAdsDao;)Ljava/util/List;
      //   88: astore 5
      //   90: aload 5
      //   92: monitorenter
      //   93: aload_0
      //   94: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   97: invokestatic 73	net/flixster/android/ads/data/GreedyAdsDao:access$5	(Lnet/flixster/android/ads/data/GreedyAdsDao;)Ljava/util/List;
      //   100: invokeinterface 79 1 0
      //   105: astore 7
      //   107: aload 7
      //   109: invokeinterface 85 1 0
      //   114: ifne +64 -> 178
      //   117: aload 5
      //   119: monitorexit
      //   120: return
      //   121: astore 4
      //   123: aload_0
      //   124: monitorexit
      //   125: aload 4
      //   127: athrow
      //   128: astore_1
      //   129: ldc 31
      //   131: ldc 87
      //   133: aload_1
      //   134: invokestatic 91	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
      //   137: aload_0
      //   138: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   141: invokestatic 95	net/flixster/android/ads/data/GreedyAdsDao:access$6	(Lnet/flixster/android/ads/data/GreedyAdsDao;)I
      //   144: iconst_2
      //   145: if_icmpge +59 -> 204
      //   148: aload_0
      //   149: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   152: astore_2
      //   153: aload_2
      //   154: iconst_1
      //   155: aload_2
      //   156: invokestatic 95	net/flixster/android/ads/data/GreedyAdsDao:access$6	(Lnet/flixster/android/ads/data/GreedyAdsDao;)I
      //   159: iadd
      //   160: invokestatic 69	net/flixster/android/ads/data/GreedyAdsDao:access$4	(Lnet/flixster/android/ads/data/GreedyAdsDao;I)V
      //   163: new 2	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread
      //   166: dup
      //   167: aload_0
      //   168: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   171: invokespecial 16	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:<init>	(Lnet/flixster/android/ads/data/GreedyAdsDao;)V
      //   174: invokevirtual 98	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:start	()V
      //   177: return
      //   178: aload 7
      //   180: invokeinterface 102 1 0
      //   185: checkcast 104	net/flixster/android/ads/data/GenericAdsDao$AdsListener
      //   188: invokeinterface 107 1 0
      //   193: goto -86 -> 107
      //   196: astore 6
      //   198: aload 5
      //   200: monitorexit
      //   201: aload 6
      //   203: athrow
      //   204: aload_0
      //   205: getfield 10	net/flixster/android/ads/data/GreedyAdsDao$AdFetchThread:this$0	Lnet/flixster/android/ads/data/GreedyAdsDao;
      //   208: iconst_0
      //   209: invokestatic 69	net/flixster/android/ads/data/GreedyAdsDao:access$4	(Lnet/flixster/android/ads/data/GreedyAdsDao;I)V
      //   212: return
      //
      // Exception table:
      //   from	to	target	type
      //   41	71	121	finally
      //   71	73	121	finally
      //   123	125	121	finally
      //   9	41	128	java/lang/Exception
      //   73	93	128	java/lang/Exception
      //   125	128	128	java/lang/Exception
      //   201	204	128	java/lang/Exception
      //   93	107	196	finally
      //   107	120	196	finally
      //   178	193	196	finally
      //   198	201	196	finally
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.data.GreedyAdsDao
 * JD-Core Version:    0.6.2
 */