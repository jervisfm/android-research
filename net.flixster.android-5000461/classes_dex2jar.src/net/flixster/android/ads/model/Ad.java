package net.flixster.android.ads.model;

import java.util.Date;

public abstract class Ad extends BitmapModel
{
  public Integer cap;
  public Long contextId;
  public Date end;
  public String placement;
  public Date start;
  public String sticky;
  public String target;

  public String toString()
  {
    return "[placement=" + this.placement + ", target=" + this.target + "]";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.model.Ad
 * JD-Core Version:    0.6.2
 */