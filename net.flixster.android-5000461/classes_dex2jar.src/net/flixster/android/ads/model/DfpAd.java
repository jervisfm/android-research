package net.flixster.android.ads.model;

import com.flixster.android.utils.Logger;
import com.google.ads.AdRequest;
import com.google.ads.interactivemedia.api.SimpleAdsRequest;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.Movie;

public class DfpAd extends TaggableAd
{
  private static final String DFP_AD_UNIT_ACTOR = "celebrity";
  private static final String DFP_AD_UNIT_BOX_OFFICE = "box_office";
  private static final String DFP_AD_UNIT_DVD = "dvd";
  private static final String DFP_AD_UNIT_HOMEPAGE = "home";
  private static final String DFP_AD_UNIT_MOVIE_DETAIL = "movie";
  private static final String DFP_AD_UNIT_MOVIE_SHOWTIMES = "showtimes";
  private static final String DFP_AD_UNIT_PHOTOS = "photos";
  private static final String DFP_AD_UNIT_THEATERS = "theaters";
  private static final String DFP_AD_UNIT_THEATER_DETAIL = "theater_info";
  private static final String DFP_AD_UNIT_UPCOMING = "upcoming";
  private static final String DFP_NETWORK_CODE = "6327";
  private static final String DFP_PUBLISHER_CODE = "mob.flix.android";

  public static String buildAdUnitId(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("/").append("6327");
    localStringBuilder.append("/").append("mob.flix.android");
    localStringBuilder.append("/");
    if (paramString.startsWith("Homepage"))
      localStringBuilder.append("home");
    while (true)
    {
      return localStringBuilder.toString();
      if (paramString.startsWith("MoviesTab"))
        localStringBuilder.append("box_office");
      else if (paramString.startsWith("TheatersTab"))
        localStringBuilder.append("theaters");
      else if (paramString.startsWith("UpcomingTab"))
        localStringBuilder.append("upcoming");
      else if (paramString.startsWith("DVDTab"))
        localStringBuilder.append("dvd");
      else if ((paramString.startsWith("MovieDetailsInTheaters")) || (paramString.startsWith("MovieDetailsUpcoming")) || (paramString.startsWith("MovieDetailsDvd")))
        localStringBuilder.append("movie");
      else if (paramString.startsWith("MovieShowtimes"))
        localStringBuilder.append("showtimes");
      else if (paramString.startsWith("TheaterInfo"))
        localStringBuilder.append("theater_info");
      else if (paramString.startsWith("ActorDetails"))
        localStringBuilder.append("celebrity");
      else if (paramString.startsWith("TopPhotos"))
        localStringBuilder.append("photos");
      else
        localStringBuilder.append("movie");
    }
  }

  public static class DfpCustomTarget
  {
    public static final String CUST_PARAMS_KEY = "cust_params";
    public String celebrityId;
    public Boolean certifiedFresh;
    public String freshRotten;
    public String[] genres;
    public String movieId;
    public String mpaa;

    public DfpCustomTarget(long paramLong)
    {
      this.celebrityId = String.valueOf(paramLong);
    }

    public DfpCustomTarget(Movie paramMovie)
    {
      this.movieId = String.valueOf(paramMovie.getId());
      this.genres = paramMovie.getGenres();
      this.mpaa = paramMovie.getMpaa();
      if (paramMovie.isFresh());
      for (String str = "fresh"; ; str = "rotten")
      {
        this.freshRotten = str;
        this.certifiedFresh = Boolean.valueOf(paramMovie.isCertifiedFresh());
        return;
      }
    }

    protected static void addCustomKeyValuePairsTo(DfpCustomTarget paramDfpCustomTarget, AdRequest paramAdRequest)
    {
      String str1 = getTestFlag();
      if ((str1 != null) && (str1.length() > 0))
        paramAdRequest.addExtra("target", str1);
      if (paramDfpCustomTarget.movieId != null)
        paramAdRequest.addExtra("movie_id", paramDfpCustomTarget.movieId);
      if (paramDfpCustomTarget.mpaa != null)
        paramAdRequest.addExtra("mpaa", paramDfpCustomTarget.mpaa);
      if (paramDfpCustomTarget.freshRotten != null)
        paramAdRequest.addExtra("fresh_rotten", paramDfpCustomTarget.freshRotten);
      String str3;
      StringBuilder localStringBuilder;
      int i;
      if (paramDfpCustomTarget.certifiedFresh != null)
      {
        if (paramDfpCustomTarget.certifiedFresh.booleanValue())
        {
          str3 = "1";
          paramAdRequest.addExtra("certified_fresh", str3);
        }
      }
      else
      {
        if (paramDfpCustomTarget.celebrityId != null)
          paramAdRequest.addExtra("celebrity_id", paramDfpCustomTarget.celebrityId);
        if (paramDfpCustomTarget.genres != null)
        {
          localStringBuilder = new StringBuilder();
          i = paramDfpCustomTarget.genres.length;
        }
      }
      for (int j = 0; ; j++)
      {
        if (j >= i)
        {
          if (localStringBuilder.length() > 0)
            paramAdRequest.addExtra("genre", formatGenres(localStringBuilder.toString()));
          paramAdRequest.addExtra("app_version", Integer.valueOf(FlixsterApplication.getVersionCode()));
          return;
          str3 = "0";
          break;
        }
        String str2 = paramDfpCustomTarget.genres[j];
        if (str2 != null)
        {
          localStringBuilder.append(str2);
          if (j + 1 < i)
            localStringBuilder.append(",");
        }
      }
    }

    public static String addCustomTargetPairTo(String paramString, Movie paramMovie, SimpleAdsRequest paramSimpleAdsRequest)
    {
      DfpCustomTarget localDfpCustomTarget = new DfpCustomTarget(paramMovie);
      String str1 = getTestFlag();
      StringBuilder localStringBuilder1 = new StringBuilder();
      localStringBuilder1.append("app_version%3D" + FlixsterApplication.getVersionCode());
      if ((str1 != null) && (str1.length() > 0))
        localStringBuilder1.append("%26" + "target%3D" + str1);
      if (localDfpCustomTarget.movieId != null)
        localStringBuilder1.append("%26" + "movie_id%3D" + localDfpCustomTarget.movieId);
      if (localDfpCustomTarget.mpaa != null)
        localStringBuilder1.append("%26" + "mpaa%3D" + localDfpCustomTarget.mpaa);
      if (localDfpCustomTarget.freshRotten != null)
        localStringBuilder1.append("%26" + "fresh_rotten%3D" + localDfpCustomTarget.freshRotten);
      String str4;
      StringBuilder localStringBuilder2;
      int i;
      if (localDfpCustomTarget.certifiedFresh != null)
      {
        StringBuilder localStringBuilder3 = new StringBuilder(String.valueOf("%26")).append("certified_fresh%3D");
        if (localDfpCustomTarget.certifiedFresh.booleanValue())
        {
          str4 = "1";
          localStringBuilder1.append(str4);
        }
      }
      else
      {
        if (localDfpCustomTarget.celebrityId != null)
          localStringBuilder1.append("%26" + "celebrity_id%3D" + localDfpCustomTarget.celebrityId);
        if (localDfpCustomTarget.genres != null)
        {
          localStringBuilder2 = new StringBuilder();
          i = localDfpCustomTarget.genres.length;
        }
      }
      for (int j = 0; ; j++)
      {
        if (j >= i)
        {
          if (localStringBuilder2.length() > 0)
            localStringBuilder1.append("%26" + "genre%3D" + formatGenres(localStringBuilder2.toString()));
          if (localStringBuilder1.length() > 0)
          {
            String str3 = "&t=" + localStringBuilder1.toString();
            Logger.si("FlxAd", "DfpCustomTarget.addCustomTargetPairTo " + str3);
            paramString = paramString + str3;
          }
          return paramString;
          str4 = "0";
          break;
        }
        String str2 = localDfpCustomTarget.genres[j];
        if (str2 != null)
        {
          localStringBuilder2.append(str2);
          if (j + 1 < i)
            localStringBuilder2.append(",");
        }
      }
    }

    public static void addCustomTargetPairTo(AdRequest paramAdRequest)
    {
      String str = getTestFlag();
      if (str != null)
        paramAdRequest.addExtra("target", str);
      paramAdRequest.addExtra("app_version", Integer.valueOf(FlixsterApplication.getVersionCode()));
    }

    private static String formatGenres(String paramString)
    {
      return paramString.toLowerCase().replace(" &", "").replace(" ", "-");
    }

    public static String getTestFlag()
    {
      return FlixsterApplication.getAdAdminDoubleClickTest();
    }

    public void addCustomKeyValuePairsTo(AdRequest paramAdRequest)
    {
      addCustomKeyValuePairsTo(this, paramAdRequest);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.model.DfpAd
 * JD-Core Version:    0.6.2
 */