package net.flixster.android.ads.model;

import android.app.Activity;
import com.flixster.android.ads.AdsArbiter;
import com.flixster.android.ads.AdsArbiter.LaunchAdsArbiter;
import com.flixster.android.ads.DfpLaunchInterstitial;
import com.flixster.android.ads.DfpPostTrailerInterstitial;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.InterstitialAd;
import net.flixster.android.model.Movie;

public class DfpInterstitialAd
{
  private static final String DFP_AD_UNIT_LAUNCH = "launch";
  private static final String DFP_AD_UNIT_POST_TRAILER = "post_trailer";
  private static final String DFP_NETWORK_CODE = "6327";
  private static final String DFP_PUBLISHER_CODE = "mob.flix.android";
  private final boolean isLaunchInterstitial;

  protected DfpInterstitialAd()
  {
    if ((this instanceof DfpLaunchInterstitial))
    {
      this.isLaunchInterstitial = true;
      return;
    }
    if ((this instanceof DfpPostTrailerInterstitial))
    {
      this.isLaunchInterstitial = false;
      return;
    }
    throw new IllegalStateException();
  }

  private String buildAdUnitId()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("/").append("6327");
    localStringBuilder.append("/").append("mob.flix.android");
    localStringBuilder.append("/");
    if (this.isLaunchInterstitial);
    for (String str = "launch"; ; str = "post_trailer")
    {
      localStringBuilder.append(str);
      return localStringBuilder.toString();
    }
  }

  public void loadAd(Activity paramActivity, Movie paramMovie)
  {
    InterstitialAd localInterstitialAd = new InterstitialAd(paramActivity, buildAdUnitId());
    localInterstitialAd.setAdListener(new DfpInterstitialListener(null));
    AdRequest localAdRequest = new AdRequest();
    DfpAd.DfpCustomTarget.addCustomTargetPairTo(localAdRequest);
    if (paramMovie != null)
      DfpAd.DfpCustomTarget.addCustomKeyValuePairsTo(new DfpAd.DfpCustomTarget(paramMovie), localAdRequest);
    localInterstitialAd.loadAd(localAdRequest);
  }

  private class DfpInterstitialListener
    implements AdListener
  {
    private boolean isAdClicked = false;

    private DfpInterstitialListener()
    {
    }

    public void onDismissScreen(Ad paramAd)
    {
      Logger.d("FlxAd", "DfpInterstitialListener.onDismissScreen");
      if ((!this.isAdClicked) && (DfpInterstitialAd.this.isLaunchInterstitial))
        Trackers.instance().trackEvent("/launch/interstitial", "launch", "LaunchInterstitial", "Skip");
    }

    public void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode paramErrorCode)
    {
      Logger.sw("FlxAd", "DfpInterstitialListener.onFailedToReceiveAd errorCode: " + paramErrorCode);
    }

    public void onLeaveApplication(Ad paramAd)
    {
      Logger.d("FlxAd", "DfpInterstitialListener.onLeaveApplication");
      this.isAdClicked = true;
      if (DfpInterstitialAd.this.isLaunchInterstitial)
        Trackers.instance().trackEvent("/launch/interstitial", "launch", "LaunchInterstitial", "Click");
    }

    public void onPresentScreen(Ad paramAd)
    {
      Logger.d("FlxAd", "DfpInterstitialListener.onPresentScreen");
      if (DfpInterstitialAd.this.isLaunchInterstitial)
        Trackers.instance().trackEvent("/launch/interstitial", "launch", "LaunchInterstitial", "Impression");
    }

    public void onReceiveAd(Ad paramAd)
    {
      if (AdsArbiter.isTopLevelActivityOkayToShowInterstitial())
      {
        StringBuilder localStringBuilder1 = new StringBuilder("DfpInterstitialListener.onReceiveAd showing ");
        if (DfpInterstitialAd.this.isLaunchInterstitial);
        for (String str1 = "launch"; ; str1 = "post-trailer")
        {
          Logger.si("FlxAd", str1 + " interstitial");
          ((InterstitialAd)paramAd).show();
          return;
        }
      }
      StringBuilder localStringBuilder2 = new StringBuilder("DfpInterstitialListener.onReceiveAd skipping ");
      if (DfpInterstitialAd.this.isLaunchInterstitial);
      for (String str2 = "launch"; ; str2 = "post-trailer")
      {
        Logger.sw("FlxAd", str2 + " interstitial");
        if (!DfpInterstitialAd.this.isLaunchInterstitial)
          break;
        AdsArbiter.launch().reshowLater();
        return;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.model.DfpInterstitialAd
 * JD-Core Version:    0.6.2
 */