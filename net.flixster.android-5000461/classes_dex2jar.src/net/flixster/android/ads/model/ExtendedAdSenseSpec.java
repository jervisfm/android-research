package net.flixster.android.ads.model;

import android.content.Context;
import com.google.ads.AdSenseSpec;
import com.google.ads.AdSpec.Parameter;
import java.util.ArrayList;
import java.util.List;

public class ExtendedAdSenseSpec extends AdSenseSpec
{
  private String mUserCity;
  private String mUserCountry;
  private String mUserRegion;

  public ExtendedAdSenseSpec(String paramString)
  {
    super(paramString);
  }

  public List<AdSpec.Parameter> generateParameters(Context paramContext)
  {
    ArrayList localArrayList = new ArrayList(super.generateParameters(paramContext));
    if (this.mUserCountry != null)
      localArrayList.add(new AdSpec.Parameter("gl", this.mUserCountry));
    if (this.mUserCity != null)
      localArrayList.add(new AdSpec.Parameter("gcs", this.mUserCity));
    if (this.mUserRegion != null)
      localArrayList.add(new AdSpec.Parameter("gr", this.mUserRegion));
    return localArrayList;
  }

  public String getUserCity()
  {
    return this.mUserCity;
  }

  public String getUserCountry()
  {
    return this.mUserCountry;
  }

  public ExtendedAdSenseSpec setUserCity(String paramString)
  {
    this.mUserCity = paramString;
    return this;
  }

  public ExtendedAdSenseSpec setUserCountry(String paramString)
  {
    this.mUserCountry = paramString;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.model.ExtendedAdSenseSpec
 * JD-Core Version:    0.6.2
 */