package net.flixster.android.ads.model;

public class FlixsterAd extends TaggableAd
{
  public String button1;
  public String button2;
  public Integer height;
  public String imageUrl;
  public AdTarget targetType;
  public String title;
  public String url;
  public Integer width;

  public static enum AdTarget
  {
    public final String literal;

    static
    {
      MOVIE = new AdTarget("MOVIE", 2, "movie");
      PRODUCT = new AdTarget("PRODUCT", 3, "product");
      HTML = new AdTarget("HTML", 4, "html");
      AdTarget[] arrayOfAdTarget = new AdTarget[5];
      arrayOfAdTarget[0] = TRAILER;
      arrayOfAdTarget[1] = URL;
      arrayOfAdTarget[2] = MOVIE;
      arrayOfAdTarget[3] = PRODUCT;
      arrayOfAdTarget[4] = HTML;
    }

    private AdTarget(String arg3)
    {
      Object localObject;
      this.literal = localObject;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.model.FlixsterAd
 * JD-Core Version:    0.6.2
 */