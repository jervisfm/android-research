package net.flixster.android.ads.model;

public class GoogleAd extends TaggableAd
{
  public static final String APP_NAME = "Movies";
  public static final String CLIENT_ID = "ca-mb-app-pub-7518399278719852";
  public static final String COMPANY_NAME = "Flixster Inc.";
  public String bcolor;
  public String channelId;
  public String ed;
  public String fcolor;
  public String keywords;
  public String webUrl;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.ads.model.GoogleAd
 * JD-Core Version:    0.6.2
 */