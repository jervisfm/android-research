package net.flixster.android.data;

import com.flixster.android.utils.DateTimeHelper;
import com.flixster.android.utils.Logger;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import net.flixster.android.FlixUtils;
import net.flixster.android.model.Actor;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Photo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ActorDao
{
  public static final String FILTER_POPULAR = "popular";
  public static final String FILTER_RANDOM = "random";

  // ERROR //
  public static Actor getActor(long paramLong)
    throws DaoException
  {
    // Byte code:
    //   0: new 27	java/net/URL
    //   3: dup
    //   4: lload_0
    //   5: invokestatic 33	net/flixster/android/data/ApiBuilder:actor	(J)Ljava/lang/String;
    //   8: invokespecial 36	java/net/URL:<init>	(Ljava/lang/String;)V
    //   11: iconst_0
    //   12: iconst_0
    //   13: invokestatic 42	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   16: astore 4
    //   18: new 44	org/json/JSONObject
    //   21: dup
    //   22: aload 4
    //   24: invokespecial 45	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   27: invokestatic 49	net/flixster/android/data/ActorDao:parseActor	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Actor;
    //   30: astore 6
    //   32: aload 6
    //   34: areturn
    //   35: astore_3
    //   36: new 51	java/lang/RuntimeException
    //   39: dup
    //   40: aload_3
    //   41: invokespecial 54	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   44: athrow
    //   45: astore_2
    //   46: aload_2
    //   47: invokestatic 58	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   50: athrow
    //   51: astore 5
    //   53: aload 5
    //   55: invokestatic 58	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   58: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   0	18	35	java/net/MalformedURLException
    //   0	18	45	java/io/IOException
    //   18	32	51	org/json/JSONException
  }

  public static ArrayList<Actor> getTopActors(String paramString, int paramInt)
    throws DaoException
  {
    ArrayList localArrayList = new ArrayList();
    getTopActors(paramString, paramInt, localArrayList);
    return localArrayList;
  }

  // ERROR //
  private static void getTopActors(String paramString, int paramInt, List<Actor> paramList)
    throws DaoException
  {
    // Byte code:
    //   0: new 27	java/net/URL
    //   3: dup
    //   4: aload_0
    //   5: iload_1
    //   6: invokestatic 70	net/flixster/android/data/ApiBuilder:topActors	(Ljava/lang/String;I)Ljava/lang/String;
    //   9: invokespecial 36	java/net/URL:<init>	(Ljava/lang/String;)V
    //   12: iconst_0
    //   13: iconst_0
    //   14: invokestatic 42	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   17: astore 5
    //   19: new 72	org/json/JSONArray
    //   22: dup
    //   23: aload 5
    //   25: invokespecial 73	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   28: astore 6
    //   30: iconst_0
    //   31: istore 7
    //   33: aload 6
    //   35: invokevirtual 77	org/json/JSONArray:length	()I
    //   38: istore 9
    //   40: iload 7
    //   42: iload 9
    //   44: if_icmplt +22 -> 66
    //   47: return
    //   48: astore 4
    //   50: new 51	java/lang/RuntimeException
    //   53: dup
    //   54: aload 4
    //   56: invokespecial 54	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   59: athrow
    //   60: astore_3
    //   61: aload_3
    //   62: invokestatic 58	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   65: athrow
    //   66: aload_2
    //   67: aload 6
    //   69: iload 7
    //   71: invokevirtual 81	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   74: invokestatic 49	net/flixster/android/data/ActorDao:parseActor	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Actor;
    //   77: invokeinterface 87 2 0
    //   82: pop
    //   83: iinc 7 1
    //   86: goto -53 -> 33
    //   89: astore 8
    //   91: aload 8
    //   93: invokestatic 58	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   96: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   0	19	48	java/net/MalformedURLException
    //   0	19	60	java/io/IOException
    //   19	30	89	org/json/JSONException
    //   33	40	89	org/json/JSONException
    //   66	83	89	org/json/JSONException
  }

  private static Actor parseActor(JSONObject paramJSONObject)
    throws JSONException
  {
    Actor localActor = new Actor();
    localActor.id = paramJSONObject.getLong("id");
    localActor.name = FlixUtils.copyString(paramJSONObject.getString("name"));
    if (paramJSONObject.has("url"))
      localActor.flixsterUrl = FlixUtils.copyString(paramJSONObject.getString("url"));
    if (paramJSONObject.has("dob"))
    {
      JSONObject localJSONObject3 = paramJSONObject.getJSONObject("dob");
      int k = localJSONObject3.getInt("year");
      int m = localJSONObject3.getInt("month");
      int n = localJSONObject3.getInt("day");
      if (n > 0)
      {
        GregorianCalendar localGregorianCalendar = new GregorianCalendar(k, m - 1, n);
        localActor.birthDate = localGregorianCalendar.getTime();
        localActor.birthDay = DateTimeHelper.formatMonthDayYear(localActor.birthDate);
      }
    }
    if (paramJSONObject.has("pob"))
    {
      Logger.d("FlxMain", "ActorDao.parseActor() has pob");
      localActor.birthplace = FlixUtils.copyString(paramJSONObject.getString("pob"));
      Logger.d("FlxMain", "ActorDao.parseActor() pob:" + localActor.birthplace);
    }
    if (paramJSONObject.has("bio"))
    {
      Logger.d("FlxMain", "ActorDao.parseActor() has bio");
      localActor.biography = FlixUtils.copyString(paramJSONObject.getString("bio")).replaceAll("<br />", "\n");
      Logger.d("FlxMain", "ActorDao.parseActor() has bio:" + localActor.biography);
    }
    if (paramJSONObject.has("photoCount"))
      localActor.photoCount = paramJSONObject.getInt("photoCount");
    if (paramJSONObject.has("photo"))
    {
      JSONObject localJSONObject2 = paramJSONObject.getJSONObject("photo");
      if (localJSONObject2.has("thumbnail"))
        localActor.thumbnailUrl = FlixUtils.copyString(localJSONObject2.getString("thumbnail"));
      if (localJSONObject2.has("lthumbnail"))
        localActor.largeUrl = FlixUtils.copyString(localJSONObject2.getString("lthumbnail"));
    }
    ArrayList localArrayList1;
    JSONArray localJSONArray1;
    int i;
    ArrayList localArrayList2;
    JSONArray localJSONArray2;
    if (paramJSONObject.has("movies"))
    {
      localArrayList1 = new ArrayList();
      localJSONArray1 = paramJSONObject.getJSONArray("movies");
      i = 0;
      if (i >= localJSONArray1.length())
        localActor.movies = localArrayList1;
    }
    else if (paramJSONObject.has("photos"))
    {
      localArrayList2 = new ArrayList();
      localJSONArray2 = paramJSONObject.getJSONArray("photos");
    }
    for (int j = 0; ; j++)
    {
      if (j >= localJSONArray2.length())
      {
        localActor.photos = localArrayList2;
        return localActor;
        JSONObject localJSONObject1 = localJSONArray1.getJSONObject(i);
        Movie localMovie = MovieDao.getMovie(localJSONObject1.getLong("id"));
        localMovie.parseFromJSON(localJSONObject1);
        localArrayList1.add(localMovie);
        i++;
        break;
      }
      Photo localPhoto = new Photo();
      localPhoto.thumbnailUrl = localJSONArray2.getJSONObject(j).getString("url");
      localArrayList2.add(localPhoto);
    }
  }

  public static void searchActors(String paramString, List<Actor> paramList)
    throws DaoException
  {
    try
    {
      getTopActors(URLEncoder.encode(paramString, "UTF-8"), 25, paramList);
      return;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      localUnsupportedEncodingException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.ActorDao
 * JD-Core Version:    0.6.2
 */