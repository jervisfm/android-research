package net.flixster.android.data;

import android.os.Build;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackManager;
import com.flixster.android.utils.CryptHelper;
import com.flixster.android.utils.F;
import com.flixster.android.utils.LocationFacade;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.OsInfo;
import com.flixster.android.utils.Properties;
import com.flixster.android.utils.StringHelper;
import com.flixster.android.utils.UrlHelper;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.Actor;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class ApiBuilder
{
  private static final String[] FLX_API_ENDPOINTS = { "api.flixster.com", "staging.flixster.com", "community.flixsterqa.com", "communitypreview.flixsterqa.com" };
  private static final String[] LOCKER_API_ENDPOINTS = { "ld.flixster.com", "lockerstaging5.flixster.com", "lockerstaging5.flixster.com", "lockerstaging5.flixster.com" };
  private static final int MAX_PHOTO = 50;
  private static final String NETFLIX_USERS_URL = "http://api-public.netflix.com/users/";
  private static final String SECURE_BASE_URL = "https://secure.flixster.com/android/api/v1/";
  private static final SimpleDateFormat URI_DATE_FORMATTER = new SimpleDateFormat("yyyyMMdd");
  private static String platformBasedDeviceType;

  protected static String actor(long paramLong)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("actors/").append(paramLong).append(".json");
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  public static String adTrack(int paramInt, String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("analytics/ads.json");
    localStringBuilder.append("?udt=").append(FlixsterApplication.getHashedUid());
    localStringBuilder.append("&pid=").append(paramInt);
    localStringBuilder.append("&event=").append(paramString1);
    localStringBuilder.append("&placement=").append(paramString2);
    return localStringBuilder.toString();
  }

  public static String ads()
  {
    StringBuilder localStringBuilder1 = new StringBuilder(getBaseUrl());
    localStringBuilder1.append("ads.json?version=");
    localStringBuilder1.append(FlixsterApplication.getVersionCode());
    localStringBuilder1.append("&model=").append(getDeviceModel());
    localStringBuilder1.append("&osVersion=").append(FlixsterApplication.getAndroidBuildInt());
    double d1 = LocationFacade.getAvailableLatitude();
    double d2 = LocationFacade.getAvailableLongitude();
    if ((d1 != 0.0D) && (d2 != 0.0D))
      localStringBuilder1.append("&latitude=").append(d1).append("&longitude=").append(d2);
    String str1 = LocationFacade.getAvailableZip();
    if (str1 != null)
      localStringBuilder1.append("&postal=").append(str1);
    StringBuilder localStringBuilder2 = localStringBuilder1.append("&conn=");
    if (FlixsterApplication.isWifi());
    for (String str2 = "2"; ; str2 = "1")
    {
      localStringBuilder2.append(str2);
      localStringBuilder1.append("&udt=").append(FlixsterApplication.getHashedUid());
      appendAdsParams(localStringBuilder1);
      return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder1)).toString();
    }
  }

  private static void appendAdsParams(StringBuilder paramStringBuilder)
  {
    String str1 = FlixsterApplication.getTicketEmail();
    if (str1 != null)
    {
      String str6 = CryptHelper.encryptEsig(str1);
      if (str6 != null)
        paramStringBuilder.append("&esig=").append(str6);
    }
    String str2 = OsInfo.instance().getAdu();
    if (str2 != null)
      paramStringBuilder.append("&adu=").append(str2);
    String str3 = FlixsterApplication.getReferrer();
    if (str3 != null)
    {
      Map localMap = UrlHelper.getQueries(str3);
      String str4 = UrlHelper.getSingleQueryValue(localMap, "utm_source");
      String str5 = UrlHelper.getSingleQueryValue(localMap, "tracking");
      if (("trialpay".equals(str4)) && (str5 != null))
      {
        Logger.d("FlxAd", "AdUtils.appendParams tp_sid=" + str5);
        paramStringBuilder.append("&tp_sid=" + str5);
      }
    }
  }

  private static StringBuilder appendCountryParam(StringBuilder paramStringBuilder)
  {
    String str1 = FlixsterApplication.getCountryCode();
    if (str1 != null)
      if (paramStringBuilder.indexOf("?") >= 0)
        break label39;
    label39: for (String str2 = "?"; ; str2 = "&")
    {
      paramStringBuilder.append(str2);
      paramStringBuilder.append("country=").append(str1);
      return paramStringBuilder;
    }
  }

  private static void appendCountryParam(ArrayList<NameValuePair> paramArrayList)
  {
    paramArrayList.add(new BasicNameValuePair("country", FlixsterApplication.getCountryCode()));
  }

  private static void appendCpiParams(ArrayList<NameValuePair> paramArrayList)
  {
    String str1 = FlixsterApplication.getReferrer();
    if (str1 != null)
    {
      Map localMap = UrlHelper.getQueries(str1);
      String str2 = UrlHelper.getSingleQueryValue(localMap, "utm_source");
      String str3 = UrlHelper.getSingleQueryValue(localMap, "tracking");
      if (("trialpay".equals(str2)) && (str3 != null))
      {
        Logger.d("FlxAd", "ApiBuilder.appendCpiParams tp_sid=" + str3);
        paramArrayList.add(new BasicNameValuePair("tp_sid", str3));
      }
      if ((str1.contains("utm_medium")) && (str1.contains("mdotm")))
      {
        Logger.d("FlxAd", "ApiBuilder.appendCpiParams mdotm=" + str1);
        paramArrayList.add(new BasicNameValuePair("mdotm", str1));
      }
    }
  }

  private static void appendEmailParam(ArrayList<NameValuePair> paramArrayList)
  {
    String str1 = FlixsterApplication.getTicketEmail();
    if (str1 != null)
    {
      String str2 = CryptHelper.encryptEsig(str1);
      if (str2 != null)
        paramArrayList.add(new BasicNameValuePair("esig", str2));
    }
  }

  private static void appendIdentifierParams(ArrayList<NameValuePair> paramArrayList)
  {
    String str1 = OsInfo.instance().getEncryptedTelephonyDeviceId();
    if (str1 != null)
      paramArrayList.add(new BasicNameValuePair("tdi", str1));
    String str2 = OsInfo.instance().getEncryptedAndroidId();
    if (str2 != null)
      paramArrayList.add(new BasicNameValuePair("sai", str2));
    String str3 = OsInfo.instance().getEncryptedSerialNumber();
    if (str3 != null)
      paramArrayList.add(new BasicNameValuePair("bsn", str3));
    String str4 = OsInfo.instance().getAdu();
    if (str4 != null)
      paramArrayList.add(new BasicNameValuePair("adu", str4));
  }

  private static void appendUserSessionParam(ArrayList<NameValuePair> paramArrayList)
  {
    FlixsterApplication.addCurrentUserPairs(paramArrayList);
  }

  protected static String boxOffice()
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("movies.json?filter=iphone-popular&view=long");
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType());
    localStringBuilder.append("&exp=").append(FlixsterApplication.sDay);
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  public static String device()
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("device.json");
    return localStringBuilder.toString();
  }

  public static ArrayList<NameValuePair> deviceParams()
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new BasicNameValuePair("visitType", Properties.instance().getVisitType()));
    localArrayList.add(new BasicNameValuePair("version", String.valueOf(FlixsterApplication.getVersionCode())));
    localArrayList.add(new BasicNameValuePair("deviceType", getPlatformBasedDeviceType()));
    localArrayList.add(new BasicNameValuePair("deviceModel", getDeviceModel()));
    localArrayList.add(new BasicNameValuePair("osVersion", String.valueOf(F.API_LEVEL)));
    localArrayList.add(new BasicNameValuePair("locale", FlixsterApplication.getLocale().toString()));
    if (FlixsterApplication.isWifi());
    for (String str = "2"; ; str = "1")
    {
      localArrayList.add(new BasicNameValuePair("conn", str));
      localArrayList.add(new BasicNameValuePair("udt", FlixsterApplication.getHashedUid()));
      appendIdentifierParams(localArrayList);
      appendEmailParam(localArrayList);
      appendCpiParams(localArrayList);
      appendUserSessionParam(localArrayList);
      appendCountryParam(localArrayList);
      return localArrayList;
    }
  }

  protected static String dvdCategory(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("movies.json?view=long").append(paramString);
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType());
    localStringBuilder.append("&exp=").append(FlixsterApplication.sDay);
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  protected static String dvdComingSoon()
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("movies.json?filter=coming-soon-dvds&view=long");
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType());
    localStringBuilder.append("&exp=").append(FlixsterApplication.sDay);
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  protected static String dvdNewRelease()
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("movies.json?filter=new-release-dvds&view=long");
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType());
    localStringBuilder.append("&exp=").append(FlixsterApplication.sDay);
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  public static String faq()
  {
    return "http://flixster.desk.com/customer/portal/topics/28130-movies-on-android";
  }

  protected static String fbInvites(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("users/");
    localStringBuilder.append(paramString1).append("/msk/fb-invites");
    localStringBuilder.append("?reqs=").append(paramString2);
    localStringBuilder.append("&requestTextCode=mb");
    return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
  }

  public static String feedback()
  {
    return "http://flixster.desk.com/customer/widget/emails/new";
  }

  protected static String friendReviews(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("users/");
    localStringBuilder.append(paramString).append("/friends/activity.json?limit=50");
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
  }

  protected static String friends(String paramString, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("users/");
    localStringBuilder.append(paramString).append("/friends.json");
    if (paramInt > 0)
      localStringBuilder.append("?limit=").append(paramInt);
    while (true)
    {
      localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
      return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
      localStringBuilder.append("?limit=50");
    }
  }

  protected static String friendsIds(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("users/");
    localStringBuilder.append(paramString).append("/friends/msk/fb.json");
    localStringBuilder.append("?version=").append(FlixsterApplication.getVersionCode());
    return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
  }

  private static String getBaseUrl()
  {
    return "http://" + FLX_API_ENDPOINTS[FlixsterApplication.getAdminApiSource()] + "/android/api/v1/";
  }

  private static String getDeviceModel()
  {
    return UrlHelper.urlEncode(Build.MODEL, "");
  }

  public static String getGlue(Movie paramMovie)
  {
    StringBuilder localStringBuilder = new StringBuilder("http://m.getglue.com");
    String str1 = getGlueMovieName(paramMovie.getTitle());
    if ((str1 != null) && (paramMovie.mDirectors != null) && (paramMovie.mDirectors.size() > 0))
    {
      String str2 = getGlueDirectorName(((Actor)paramMovie.mDirectors.iterator().next()).name);
      if (str2 != null)
        localStringBuilder.append("/checkin/movies/").append(str1).append("/").append(str2);
    }
    localStringBuilder.append("?s=flixster");
    return localStringBuilder.toString();
  }

  private static String getGlueDirectorName(String paramString)
  {
    try
    {
      StringBuilder localStringBuilder = new StringBuilder();
      String[] arrayOfString = StringHelper.replaceNonAlphaNumeric(paramString.toLowerCase()).split("_");
      localStringBuilder.append(arrayOfString[0]).append("_").append(arrayOfString[(-1 + arrayOfString.length)]);
      String str = localStringBuilder.toString();
      return str;
    }
    catch (Exception localException)
    {
      Logger.e("FlxMain", "ApiBuilder.getGlueDirectorName", localException);
    }
    return null;
  }

  private static String getGlueMovieName(String paramString)
  {
    int i = 0;
    String str1;
    try
    {
      StringBuilder localStringBuilder = new StringBuilder();
      String[] arrayOfString = StringHelper.replaceNonAlphaNumeric(paramString.toLowerCase().replace("'", "")).split("_");
      int j = arrayOfString.length;
      while (true)
      {
        if (i >= j)
        {
          str1 = localStringBuilder.toString();
          if (!str1.endsWith("_"))
            break;
          return str1.substring(0, -1 + str1.length());
        }
        String str2 = arrayOfString[i];
        if ((str2.length() > 0) && (!"the".equals(str2)) && (!"a".equals(str2)) && (!"and".equals(str2)))
          localStringBuilder.append(str2).append("_");
        i++;
      }
    }
    catch (Exception localException)
    {
      Logger.e("FlxMain", "ApiBuilder.getGlueMovieName", localException);
      str1 = null;
    }
    return str1;
  }

  private static String getPlatformBasedDeviceType()
  {
    if (platformBasedDeviceType == null)
    {
      if (!Properties.instance().isHoneycombTablet())
        break label25;
      platformBasedDeviceType = "Android_Tablet";
    }
    while (true)
    {
      return platformBasedDeviceType;
      label25: if (Properties.instance().isGoogleTv())
        platformBasedDeviceType = "Android_Gtv";
      else
        platformBasedDeviceType = getDeviceModel();
    }
  }

  private static String getPlatformBasedDeviceType(boolean paramBoolean)
  {
    String str = getPlatformBasedDeviceType();
    if ((paramBoolean) && (Properties.instance().isHoneycombTablet()) && (!F.IS_NATIVE_HC_DRM_ENABLED))
      str = str + "_" + getDeviceModel();
    while ((Properties.instance().isGoogleTv()) || (Properties.instance().isHoneycombTablet()) || (!F.IS_NATIVE_HC_DRM_ENABLED))
      return str;
    return "Android_Tablet";
  }

  private static String getSecureBaseUrl()
  {
    return "https://" + FLX_API_ENDPOINTS[FlixsterApplication.getAdminApiSource()] + "/android/api/v1/";
  }

  private static String getSecureMskBaseUrl()
  {
    return "https://" + LOCKER_API_ENDPOINTS[FlixsterApplication.getAdminApiSource()] + "/msk/";
  }

  private static float getTheaterDistance()
  {
    float f = FlixsterApplication.getTheaterDistance();
    if (FlixsterApplication.getDistanceType() == 1)
      f /= 1.609F;
    return f;
  }

  public static boolean isAdTrack(String paramString)
  {
    return paramString.contains("analytics/ads.json");
  }

  private static boolean isQaApiServer()
  {
    return FlixsterApplication.getAdminApiSource() >= 2;
  }

  public static boolean isSecureFlxUrl(String paramString)
  {
    return (paramString.startsWith("https://secure.flixster.com/android/api/v1/")) || (paramString.startsWith(getSecureBaseUrl()));
  }

  public static final boolean isTrailerUrl(String paramString)
  {
    return paramString.contains("videodetective.net/player.aspx");
  }

  public static boolean isVideoLink(String paramString)
  {
    return (paramString.contains(".mp4")) || (paramString.contains(".m4a")) || (paramString.contains(".3gp"));
  }

  protected static String location(double paramDouble1, double paramDouble2)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("cities.json?latitude=").append(paramDouble1);
    localStringBuilder.append("&longitude=").append(paramDouble2);
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  protected static String location(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    try
    {
      localStringBuilder.append("cities.json?q=").append(URLEncoder.encode(paramString, "UTF-8"));
      return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      while (true)
        localUnsupportedEncodingException.printStackTrace();
    }
  }

  public static String mobileUpsell()
  {
    return "http://tmto.es/go2flix";
  }

  protected static String movie(long paramLong)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("movies/").append(paramLong).append(".json");
    localStringBuilder.append("?version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&exp=").append(FlixsterApplication.sDay);
    localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType());
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  protected static String netflixDeleteQueue(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder("http://api-public.netflix.com/users/");
    localStringBuilder.append(FlixsterApplication.getNetflixUserId());
    localStringBuilder.append(paramString1);
    localStringBuilder.append("/").append(paramString2);
    return localStringBuilder.toString();
  }

  protected static String netflixGetQueue(String paramString, int paramInt1, int paramInt2)
  {
    if ("/queues/disc/available".equals(paramString))
      paramString = "/queues/disc";
    StringBuilder localStringBuilder = new StringBuilder("http://api-public.netflix.com/users/");
    localStringBuilder.append(FlixsterApplication.getNetflixUserId());
    localStringBuilder.append(paramString);
    localStringBuilder.append("?max_results=").append(paramInt2);
    localStringBuilder.append("&start_index=").append(paramInt1);
    localStringBuilder.append("&output=json");
    return localStringBuilder.toString();
  }

  protected static String netflixMovies()
  {
    return "http://api.flixster.com/android/api/v1/netflix/movies.json";
  }

  protected static String netflixPostQueue(String paramString1, String paramString2, int paramInt)
  {
    int i = paramString1.indexOf("/available");
    if (i > -1)
      paramString1 = paramString1.substring(0, i);
    while (true)
    {
      StringBuilder localStringBuilder = new StringBuilder("http://api-public.netflix.com/users/");
      localStringBuilder.append(FlixsterApplication.getNetflixUserId());
      localStringBuilder.append(paramString1);
      localStringBuilder.append("?title_ref=").append(paramString2);
      if (paramInt != 0)
        localStringBuilder.append("&position=").append(paramInt);
      localStringBuilder.append("&output=json");
      return localStringBuilder.toString();
      int j = paramString1.indexOf("/saved");
      if (j > -1)
        paramString1 = paramString1.substring(0, j);
    }
  }

  protected static String netflixTitleState(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder("http://api-public.netflix.com/users/");
    localStringBuilder.append(FlixsterApplication.getNetflixUserId());
    localStringBuilder.append("/title_states?title_refs=").append(paramString);
    localStringBuilder.append("&output=json");
    return localStringBuilder.toString();
  }

  public static String photoInfo(long paramLong)
  {
    return "http://flixster.com/mobile/support/flag-photo?image=" + paramLong;
  }

  protected static String photos(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("photos.json?limit=").append(50);
    localStringBuilder.append(paramString);
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  public static String promos()
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("promos.json");
    localStringBuilder.append("?version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&exp=").append(FlixsterApplication.sDay);
    return appendCountryParam(localStringBuilder).toString();
  }

  public static String properties()
  {
    StringBuilder localStringBuilder1 = new StringBuilder(getBaseUrl());
    localStringBuilder1.append("properties.json");
    localStringBuilder1.append("?version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder1.append("&udt=").append(FlixsterApplication.getHashedUid());
    StringBuilder localStringBuilder2 = localStringBuilder1.append("&telephonySupported=");
    if (Properties.instance().isTelephonySupported());
    for (boolean bool = true; ; bool = false)
    {
      localStringBuilder2.append(bool);
      localStringBuilder1.append("&osVersion=").append(String.valueOf(F.API_LEVEL));
      localStringBuilder1.append("&deviceManufacturer=").append(F.MANUFACTURER);
      return appendCountryParam(localStringBuilder1).toString();
    }
  }

  protected static String quickRate(boolean paramBoolean)
  {
    StringBuilder localStringBuilder1 = new StringBuilder(getBaseUrl());
    localStringBuilder1.append("quickrate/movies.json?");
    StringBuilder localStringBuilder2 = localStringBuilder1.append("&type=");
    if (paramBoolean);
    for (String str = "wts"; ; str = "rate")
    {
      localStringBuilder2.append(str);
      localStringBuilder1.append("&version=").append(FlixsterApplication.getVersionCode());
      return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder1)).toString();
    }
  }

  public static String registerAccount()
  {
    return getSecureMskBaseUrl() + "mobile0/flixsterReg/?standalone=true&native=true";
  }

  protected static String rightDownload(long paramLong, String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("rights/").append(paramLong);
    localStringBuilder.append("/download.json?");
    if (paramString1 != null)
      localStringBuilder.append("&reason=").append(paramString1);
    if (paramString2 != null)
      localStringBuilder.append("&sonicQueueId=").append(paramString2);
    if (paramString3 != null)
      localStringBuilder.append("&playPosition=").append(paramString3);
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType(paramBoolean));
    localStringBuilder.append("&udt=").append(FlixsterApplication.getHashedUid());
    return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
  }

  protected static String rightEpisodes(long paramLong)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("rights/").append(paramLong);
    localStringBuilder.append("/episodes.json?");
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType());
    return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
  }

  protected static String rightLicenseStats(long paramLong, String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    StringBuilder localStringBuilder1 = new StringBuilder(getBaseUrl());
    localStringBuilder1.append("rights/").append(paramLong);
    localStringBuilder1.append("/license/stats.json?");
    StringBuilder localStringBuilder2 = localStringBuilder1.append("&mediaTransfer=");
    String str1;
    StringBuilder localStringBuilder3;
    if (Drm.manager().isStreamingMode())
    {
      str1 = "streaming";
      localStringBuilder2.append(str1);
      if (Drm.manager().isStreamingMode())
        localStringBuilder1.append("&streamId=").append(paramString);
      localStringBuilder3 = localStringBuilder1.append("&status=");
      if (!paramBoolean1)
        break label182;
    }
    label182: for (String str2 = "200"; ; str2 = "500")
    {
      localStringBuilder3.append(str2);
      localStringBuilder1.append("&version=").append(FlixsterApplication.getVersionCode());
      localStringBuilder1.append("&deviceType=").append(getPlatformBasedDeviceType(paramBoolean2));
      localStringBuilder1.append("&udt=").append(FlixsterApplication.getHashedUid());
      return FlixsterApplication.addCurrentUserParameters(localStringBuilder1).toString();
      str1 = "download";
      break;
    }
  }

  protected static String rightPosition(long paramLong, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("rights/").append(paramLong);
    localStringBuilder.append("/position.json?");
    localStringBuilder.append("&playPosition=").append(paramString);
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&udt=").append(FlixsterApplication.getHashedUid());
    return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
  }

  protected static String rightStream(long paramLong, String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("rights/").append(paramLong);
    localStringBuilder.append("/stream.json?");
    if (paramString1 != null)
      localStringBuilder.append("&reason=").append(paramString1);
    if (paramString2 != null)
      localStringBuilder.append("&streamId=").append(paramString2);
    if (paramString3 != null)
      localStringBuilder.append("&playPosition=").append(paramString3);
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType(paramBoolean));
    localStringBuilder.append("&udt=").append(FlixsterApplication.getHashedUid());
    return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
  }

  protected static String searchMovies(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    try
    {
      localStringBuilder.append("movies.json?filter=").append(URLEncoder.encode(paramString, "UTF-8"));
      localStringBuilder.append("&view=long&limit=25");
      localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
      localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType());
      return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      while (true)
        localUnsupportedEncodingException.printStackTrace();
    }
  }

  protected static String season(long paramLong)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("tv/seasons/").append(paramLong).append(".json");
    localStringBuilder.append("?version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&exp=").append(FlixsterApplication.sDay);
    localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType());
    return appendCountryParam(localStringBuilder).toString();
  }

  protected static String sonicAckLicense()
  {
    return "https://www.cinemanow.com/pox/LicenseBusinessLogic.asmx/StreamKeyServerAck";
  }

  protected static ArrayList<NameValuePair> sonicAckLicenseParams(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new BasicNameValuePair("strCustomData", paramString));
    return localArrayList;
  }

  protected static String streamCreate(LockerRight paramLockerRight)
  {
    if (paramLockerRight.isEpisode());
    for (String str = "episodes/"; ; str = "movies/")
    {
      StringBuilder localStringBuilder = new StringBuilder(getSecureBaseUrl());
      localStringBuilder.append("uv/").append(str);
      localStringBuilder.append(paramLockerRight.assetId).append("/stream/create");
      localStringBuilder.append("?deviceType=").append(getPlatformBasedDeviceType());
      return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
    }
  }

  protected static String streamDelete(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("uv/stream/delete?streamHandleId=").append(paramString);
    return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
  }

  public static String supportEmail()
  {
    return "android@flixster-inc.com";
  }

  protected static String theater(long paramLong, Date paramDate)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("theaters/").append(paramLong).append(".json");
    localStringBuilder.append("?date=").append(URI_DATE_FORMATTER.format(paramDate));
    localStringBuilder.append("&showtimes=true");
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  protected static String theaterMoviePair(long paramLong1, long paramLong2, Date paramDate)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("theaters.json?theater=").append(paramLong1);
    localStringBuilder.append("&movie=").append(paramLong2);
    localStringBuilder.append("&showtimes=true");
    localStringBuilder.append("&date=").append(URI_DATE_FORMATTER.format(paramDate));
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    return appendCountryParam(localStringBuilder).toString();
  }

  protected static String theaters(double paramDouble1, double paramDouble2)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("theaters.json?latitude=").append(paramDouble1).append("&longitude=").append(paramDouble2);
    localStringBuilder.append("&radius=").append(getTheaterDistance()).append("&limit=100");
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    return appendCountryParam(localStringBuilder).toString();
  }

  protected static String theaters(String paramString)
  {
    try
    {
      String str2 = URLEncoder.encode(paramString, "ISO-8859-1");
      str1 = str2;
      StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
      localStringBuilder.append("theaters.json?postal=").append(str1);
      localStringBuilder.append("&radius=").append(getTheaterDistance());
      localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
      return appendCountryParam(localStringBuilder).toString();
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      while (true)
        String str1 = paramString;
    }
  }

  protected static String theatersByMovie(double paramDouble1, double paramDouble2, long paramLong, Date paramDate)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("theaters.json?latitude=").append(paramDouble1);
    localStringBuilder.append("&longitude=").append(paramDouble2);
    localStringBuilder.append("&showtimes=true&movie=").append(paramLong);
    localStringBuilder.append("&date=").append(URI_DATE_FORMATTER.format(paramDate));
    localStringBuilder.append("&radius=").append(getTheaterDistance()).append("&limit=100");
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    return appendCountryParam(localStringBuilder).toString();
  }

  protected static String ticketAvailability(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    StringBuilder localStringBuilder = new StringBuilder("https://secure.flixster.com/android/api/v1/");
    localStringBuilder.append("tickets.json");
    localStringBuilder.append("?theater=").append(paramString1);
    localStringBuilder.append("&listing=").append(paramString2);
    localStringBuilder.append("&date=").append(paramString3);
    localStringBuilder.append("&time=").append(paramString4);
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    return appendCountryParam(localStringBuilder).toString();
  }

  protected static String ticketPurchase()
  {
    StringBuilder localStringBuilder = new StringBuilder("https://secure.flixster.com/android/api/v1/");
    localStringBuilder.append("tickets/purchases.json");
    localStringBuilder.append("?version=").append(FlixsterApplication.getVersionCode());
    return appendCountryParam(localStringBuilder).toString();
  }

  protected static String ticketReservation()
  {
    StringBuilder localStringBuilder = new StringBuilder("https://secure.flixster.com/android/api/v1/");
    localStringBuilder.append("tickets/reservations.json");
    localStringBuilder.append("?version=").append(FlixsterApplication.getVersionCode());
    return appendCountryParam(localStringBuilder).toString();
  }

  protected static String tokens()
  {
    if (isQaApiServer());
    for (String str = getBaseUrl(); ; str = getSecureBaseUrl())
    {
      StringBuilder localStringBuilder = new StringBuilder(str);
      localStringBuilder.append("tokens.json");
      return localStringBuilder.toString();
    }
  }

  protected static String topActors(String paramString, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("actors.json?limit=").append(paramInt);
    localStringBuilder.append("&filter=").append(paramString);
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  protected static String upcoming()
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("movies.json?filter=theater-upcoming&view=long");
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType());
    localStringBuilder.append("&exp=").append(FlixsterApplication.sDay);
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  protected static String user(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("users/");
    localStringBuilder.append(paramString).append(".json?locale=en_US&country=us");
    localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
    return FlixsterApplication.addUserParameters(paramString, localStringBuilder).toString();
  }

  protected static String userMovieReview(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("users/");
    localStringBuilder.append(paramString1).append("/ratings/").append(paramString2).append(".json");
    localStringBuilder.append("?version=").append(FlixsterApplication.getVersionCode());
    return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
  }

  protected static String userMovieReviewShort(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("users/");
    localStringBuilder.append(paramString1).append("/ratings/").append(paramString2).append(".json");
    return localStringBuilder.toString();
  }

  protected static String userReviews(String paramString, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("users/");
    localStringBuilder.append(paramString).append("/ratings.json");
    if (paramInt <= 0)
      localStringBuilder.append("?limit=50");
    while (true)
    {
      localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
      return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
      localStringBuilder.append("?limit=").append(paramInt);
    }
  }

  protected static String userRights(String paramString1, String paramString2)
  {
    if (paramString2 == null);
    for (String str = getBaseUrl(); ; str = getSecureBaseUrl())
    {
      StringBuilder localStringBuilder = new StringBuilder(str);
      localStringBuilder.append("users/");
      localStringBuilder.append(paramString1).append("/rights.json?");
      if (paramString2 != null)
        localStringBuilder.append("&moviePurchaseType=").append(paramString2);
      localStringBuilder.append("&version=").append(FlixsterApplication.getVersionCode());
      localStringBuilder.append("&deviceType=").append(getPlatformBasedDeviceType(true));
      localStringBuilder.append("&deviceModel=").append(getDeviceModel());
      localStringBuilder.append("&udt=").append(FlixsterApplication.getHashedUid());
      return FlixsterApplication.addCurrentUserParameters(localStringBuilder).toString();
    }
  }

  protected static String viewer()
  {
    StringBuilder localStringBuilder = new StringBuilder(getBaseUrl());
    localStringBuilder.append("users/viewer.json");
    localStringBuilder.append("?version=").append(FlixsterApplication.getVersionCode());
    return appendCountryParam(FlixsterApplication.addCurrentUserParameters(localStringBuilder)).toString();
  }

  public static String widevineLicense(LockerRight paramLockerRight, String paramString)
  {
    if (paramLockerRight == null)
    {
      Logger.w("FlxMain", "ApiBuilder.widevineLicense asset null");
      return "";
    }
    String str1;
    if (paramLockerRight.isEpisode())
      str1 = "episode_id/";
    label85: String str3;
    while (true)
    {
      StringBuilder localStringBuilder1 = new StringBuilder(getSecureBaseUrl());
      localStringBuilder1.append("uv/widevine/license/");
      localStringBuilder1.append(str1).append(paramLockerRight.assetId);
      StringBuilder localStringBuilder2 = localStringBuilder1.append("/mediaTransfer/");
      String str2;
      if (Drm.manager().isStreamingMode())
      {
        str2 = "streaming";
        localStringBuilder2.append(str2);
        localStringBuilder1.append("/udt/").append(FlixsterApplication.getHashedUid());
        if (Drm.manager().isStreamingMode())
          localStringBuilder1.append("/streamHandleId/").append(paramString);
        str3 = FlixsterApplication.addCurrentUserParameters(localStringBuilder1).toString().replace("?", "/").replace("&", "/").replace("platform=", "platform/").replace("user=", "user/").replace("session=", "session/").replace("token=", "token/");
      }
      try
      {
        int i = str3.indexOf("platform_session/");
        if (i != -1)
        {
          String str4 = str3.substring(i + 17);
          String str5 = str3.replace(str4, URLEncoder.encode(str4, "UTF-8"));
          return str5;
          str1 = "movie_id/";
          continue;
          str2 = "download";
          break label85;
        }
        else
        {
          String str6 = str3.substring(11 + str3.indexOf("auth_token/"));
          String str7 = str3.replace(str6, URLEncoder.encode(str6, "UTF-8"));
          return str7;
        }
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        localUnsupportedEncodingException.printStackTrace();
      }
    }
    return str3;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.ApiBuilder
 * JD-Core Version:    0.6.2
 */