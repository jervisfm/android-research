package net.flixster.android.data;

import com.flixster.android.net.HttpUnauthorizedException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import net.flixster.android.FlixsterApplication;
import org.json.JSONException;
import org.json.JSONObject;

public class DaoException extends Exception
{
  private static final long serialVersionUID = -2143409817196587808L;
  private final Type type;

  private DaoException(Type paramType, Exception paramException)
  {
    super(paramException);
    this.type = paramType;
  }

  private DaoException(Type paramType, String paramString)
  {
    super(paramString);
    this.type = paramType;
  }

  public static DaoException create(Exception paramException)
  {
    if ((paramException instanceof JSONException))
    {
      String str = paramException.toString();
      if (str.contains("End of input at character 0"))
        return new DaoException(Type.NETWORK_UNSTABLE, paramException);
      if (str.contains("Expected"))
        return new DaoException(Type.SERVER_DATA, paramException);
      return new DaoException(Type.SERVER_DATA, paramException);
    }
    if ((paramException instanceof IOException))
    {
      if ((paramException instanceof HttpUnauthorizedException))
        return new DaoException(Type.USER_ACCT, paramException);
      if ((paramException instanceof SocketTimeoutException))
        return new DaoException(Type.NETWORK_UNSTABLE, paramException);
      if (((paramException instanceof UnknownHostException)) || ((paramException instanceof ConnectException)))
        return new DaoException(Type.NETWORK, paramException);
      if (((paramException instanceof FileNotFoundException)) || (FlixsterApplication.isConnected()))
        return new DaoException(Type.SERVER_API, paramException);
      return new DaoException(Type.NETWORK, paramException);
    }
    return new DaoException(Type.UNKNOWN, paramException);
  }

  public static DaoException create(String paramString)
  {
    try
    {
      DaoException localDaoException = create(new JSONObject(paramString));
      return localDaoException;
    }
    catch (JSONException localJSONException)
    {
    }
    return null;
  }

  public static DaoException create(Type paramType, String paramString)
  {
    return new DaoException(paramType, paramString);
  }

  private static DaoException create(JSONObject paramJSONObject)
  {
    return new DaoException(Type.SERVER_ERROR_MSG, paramJSONObject.optString("error"));
  }

  public static boolean hasError(String paramString)
  {
    try
    {
      boolean bool = hasError(new JSONObject(paramString));
      return bool;
    }
    catch (JSONException localJSONException)
    {
    }
    return false;
  }

  private static boolean hasError(JSONObject paramJSONObject)
  {
    return paramJSONObject.has("error");
  }

  public Type getType()
  {
    return this.type;
  }

  public static enum Type
  {
    static
    {
      SERVER_API = new Type("SERVER_API", 3);
      SERVER_ERROR_MSG = new Type("SERVER_ERROR_MSG", 4);
      USER_ACCT = new Type("USER_ACCT", 5);
      STREAM_CREATE = new Type("STREAM_CREATE", 6);
      NOT_LICENSED = new Type("NOT_LICENSED", 7);
      UNKNOWN = new Type("UNKNOWN", 8);
      Type[] arrayOfType = new Type[9];
      arrayOfType[0] = NETWORK;
      arrayOfType[1] = NETWORK_UNSTABLE;
      arrayOfType[2] = SERVER_DATA;
      arrayOfType[3] = SERVER_API;
      arrayOfType[4] = SERVER_ERROR_MSG;
      arrayOfType[5] = USER_ACCT;
      arrayOfType[6] = STREAM_CREATE;
      arrayOfType[7] = NOT_LICENSED;
      arrayOfType[8] = UNKNOWN;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.DaoException
 * JD-Core Version:    0.6.2
 */