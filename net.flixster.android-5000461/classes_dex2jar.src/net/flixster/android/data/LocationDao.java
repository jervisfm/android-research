package net.flixster.android.data;

import java.util.ArrayList;
import net.flixster.android.FlixUtils;
import net.flixster.android.model.Location;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationDao
{
  public static ArrayList<Location> getLocations(double paramDouble1, double paramDouble2)
    throws DaoException
  {
    return getLocationsByUrl(ApiBuilder.location(paramDouble1, paramDouble2));
  }

  public static ArrayList<Location> getLocations(String paramString)
    throws DaoException
  {
    return getLocationsByUrl(ApiBuilder.location(paramString));
  }

  // ERROR //
  private static ArrayList<Location> getLocationsByUrl(String paramString)
    throws DaoException
  {
    // Byte code:
    //   0: new 33	java/net/URL
    //   3: dup
    //   4: aload_0
    //   5: invokespecial 36	java/net/URL:<init>	(Ljava/lang/String;)V
    //   8: iconst_0
    //   9: iconst_0
    //   10: invokestatic 42	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   13: astore_3
    //   14: new 44	org/json/JSONArray
    //   17: dup
    //   18: aload_3
    //   19: invokespecial 45	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   22: astore 4
    //   24: new 47	java/util/ArrayList
    //   27: dup
    //   28: invokespecial 48	java/util/ArrayList:<init>	()V
    //   31: astore 5
    //   33: iconst_0
    //   34: istore 6
    //   36: aload 4
    //   38: invokevirtual 52	org/json/JSONArray:length	()I
    //   41: istore 8
    //   43: iload 6
    //   45: iload 8
    //   47: if_icmplt +22 -> 69
    //   50: aload 5
    //   52: areturn
    //   53: astore_2
    //   54: new 54	java/lang/RuntimeException
    //   57: dup
    //   58: aload_2
    //   59: invokespecial 57	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   62: athrow
    //   63: astore_1
    //   64: aload_1
    //   65: invokestatic 61	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   68: athrow
    //   69: aload 5
    //   71: aload 4
    //   73: iload 6
    //   75: invokevirtual 65	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   78: invokestatic 69	net/flixster/android/data/LocationDao:parseLocation	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Location;
    //   81: invokevirtual 73	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   84: pop
    //   85: iinc 6 1
    //   88: goto -52 -> 36
    //   91: astore 7
    //   93: aload 7
    //   95: invokestatic 61	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   98: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   0	14	53	java/net/MalformedURLException
    //   0	14	63	java/io/IOException
    //   14	33	91	org/json/JSONException
    //   36	43	91	org/json/JSONException
    //   69	85	91	org/json/JSONException
  }

  private static Location parseLocation(JSONObject paramJSONObject)
    throws JSONException
  {
    Location localLocation = new Location();
    if (paramJSONObject.has("city"))
      localLocation.city = FlixUtils.copyString(paramJSONObject.getString("city"));
    if (paramJSONObject.has("state"))
      localLocation.state = FlixUtils.copyString(paramJSONObject.getString("state"));
    if (paramJSONObject.has("stateAbbrev"))
      localLocation.stateCode = FlixUtils.copyString(paramJSONObject.getString("stateAbbrev"));
    if (paramJSONObject.has("country"))
      localLocation.country = FlixUtils.copyString(paramJSONObject.getString("country"));
    if (paramJSONObject.has("zipcode"))
      localLocation.zip = FlixUtils.copyString(paramJSONObject.getString("zipcode"));
    if (paramJSONObject.has("weight"))
      localLocation.weight = paramJSONObject.getInt("weight");
    if (paramJSONObject.has("center"))
    {
      JSONObject localJSONObject = paramJSONObject.getJSONObject("center");
      if (localJSONObject.has("latitude"))
        localLocation.latitude = localJSONObject.getDouble("latitude");
      if (localJSONObject.has("longitude"))
        localLocation.longitude = localJSONObject.getDouble("longitude");
    }
    return localLocation;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.LocationDao
 * JD-Core Version:    0.6.2
 */