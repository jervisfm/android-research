package net.flixster.android.data;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Data;
import com.flixster.android.net.RestClient;
import com.flixster.android.net.RestClient.RequestMethod;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import com.flixster.android.utils.WorkerThreads;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.Contact;
import net.flixster.android.model.Property;
import org.apache.http.NameValuePair;

public class MiscDao
{
  public static void fetchCaptions(final Handler paramHandler1, final Handler paramHandler2, String paramString)
  {
    Logger.d("FlxDrm", "MiscDao.fetchCaptions " + paramString);
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      // ERROR //
      public void run()
      {
        // Byte code:
        //   0: aconst_null
        //   1: astore_1
        //   2: aload_0
        //   3: getfield 20	net/flixster/android/data/MiscDao$4:val$urlString	Ljava/lang/String;
        //   6: ldc 36
        //   8: invokevirtual 42	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //   11: istore 10
        //   13: aconst_null
        //   14: astore_1
        //   15: iload 10
        //   17: ifeq +69 -> 86
        //   20: new 44	java/io/FileInputStream
        //   23: dup
        //   24: new 46	java/io/File
        //   27: dup
        //   28: aload_0
        //   29: getfield 20	net/flixster/android/data/MiscDao$4:val$urlString	Ljava/lang/String;
        //   32: ldc 36
        //   34: ldc 48
        //   36: invokevirtual 52	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
        //   39: invokespecial 55	java/io/File:<init>	(Ljava/lang/String;)V
        //   42: invokespecial 58	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //   45: astore_1
        //   46: new 60	com/flixster/android/net/CaptionsXmlParser
        //   49: dup
        //   50: invokespecial 61	com/flixster/android/net/CaptionsXmlParser:<init>	()V
        //   53: aload_1
        //   54: invokevirtual 65	com/flixster/android/net/CaptionsXmlParser:parse	(Ljava/io/InputStream;)Ljava/util/List;
        //   57: astore 11
        //   59: aload_0
        //   60: getfield 22	net/flixster/android/data/MiscDao$4:val$successHandler	Landroid/os/Handler;
        //   63: aconst_null
        //   64: iconst_0
        //   65: aload 11
        //   67: invokestatic 71	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
        //   70: invokestatic 77	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
        //   73: invokevirtual 83	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //   76: pop
        //   77: aload_1
        //   78: ifnull +7 -> 85
        //   81: aload_1
        //   82: invokevirtual 88	java/io/InputStream:close	()V
        //   85: return
        //   86: new 90	java/net/URL
        //   89: dup
        //   90: aload_0
        //   91: getfield 20	net/flixster/android/data/MiscDao$4:val$urlString	Ljava/lang/String;
        //   94: invokespecial 91	java/net/URL:<init>	(Ljava/lang/String;)V
        //   97: invokestatic 97	net/flixster/android/util/HttpHelper:fetchUrlStream	(Ljava/net/URL;)Ljava/io/InputStream;
        //   100: astore 14
        //   102: aload 14
        //   104: astore_1
        //   105: goto -59 -> 46
        //   108: astore 7
        //   110: aload_0
        //   111: getfield 24	net/flixster/android/data/MiscDao$4:val$errorHandler	Landroid/os/Handler;
        //   114: aconst_null
        //   115: iconst_0
        //   116: aload 7
        //   118: invokestatic 77	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
        //   121: invokevirtual 83	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //   124: pop
        //   125: ldc 99
        //   127: ldc 101
        //   129: aload 7
        //   131: invokestatic 107	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   134: aload_1
        //   135: ifnull -50 -> 85
        //   138: aload_1
        //   139: invokevirtual 88	java/io/InputStream:close	()V
        //   142: return
        //   143: astore 9
        //   145: ldc 99
        //   147: ldc 109
        //   149: aload 9
        //   151: invokestatic 107	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   154: return
        //   155: astore 4
        //   157: aload_0
        //   158: getfield 24	net/flixster/android/data/MiscDao$4:val$errorHandler	Landroid/os/Handler;
        //   161: aconst_null
        //   162: iconst_0
        //   163: aload 4
        //   165: invokestatic 77	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
        //   168: invokevirtual 83	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //   171: pop
        //   172: ldc 99
        //   174: ldc 111
        //   176: aload 4
        //   178: invokestatic 107	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   181: aload_1
        //   182: ifnull -97 -> 85
        //   185: aload_1
        //   186: invokevirtual 88	java/io/InputStream:close	()V
        //   189: return
        //   190: astore 6
        //   192: ldc 99
        //   194: ldc 109
        //   196: aload 6
        //   198: invokestatic 107	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   201: return
        //   202: astore_2
        //   203: aload_1
        //   204: ifnull +7 -> 211
        //   207: aload_1
        //   208: invokevirtual 88	java/io/InputStream:close	()V
        //   211: aload_2
        //   212: athrow
        //   213: astore_3
        //   214: ldc 99
        //   216: ldc 109
        //   218: aload_3
        //   219: invokestatic 107	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   222: goto -11 -> 211
        //   225: astore 13
        //   227: ldc 99
        //   229: ldc 109
        //   231: aload 13
        //   233: invokestatic 107	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   236: return
        //
        // Exception table:
        //   from	to	target	type
        //   2	13	108	java/io/FileNotFoundException
        //   20	46	108	java/io/FileNotFoundException
        //   46	77	108	java/io/FileNotFoundException
        //   86	102	108	java/io/FileNotFoundException
        //   138	142	143	java/io/IOException
        //   2	13	155	java/net/MalformedURLException
        //   20	46	155	java/net/MalformedURLException
        //   46	77	155	java/net/MalformedURLException
        //   86	102	155	java/net/MalformedURLException
        //   185	189	190	java/io/IOException
        //   2	13	202	finally
        //   20	46	202	finally
        //   46	77	202	finally
        //   86	102	202	finally
        //   110	134	202	finally
        //   157	181	202	finally
        //   207	211	213	java/io/IOException
        //   81	85	225	java/io/IOException
      }
    });
  }

  @Deprecated
  public static void fetchFriends(Handler paramHandler1, Handler paramHandler2)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        ArrayList localArrayList = new ArrayList();
        String[] arrayOfString1 = { "contact_id", "data1", "data3", "data2", "data5" };
        Cursor localCursor1 = FlixsterApplication.getContext().getContentResolver().query(ContactsContract.Data.CONTENT_URI, arrayOfString1, "mimetype='vnd.android.cursor.item/name'", null, "data3 ASC,data2 ASC");
        String str1;
        do
        {
          if (!localCursor1.moveToNext())
          {
            localCursor1.close();
            MiscDao.this.sendMessage(Message.obtain(null, 0, Collections.unmodifiableList(localArrayList)));
            return;
          }
          str1 = localCursor1.getString(localCursor1.getColumnIndex("data1"));
        }
        while ((str1 == null) || (str1.equals("")) || (str1.startsWith("#")));
        Iterator localIterator1 = localArrayList.iterator();
        label139: boolean bool1 = localIterator1.hasNext();
        int i = 0;
        label156: String str3;
        String str4;
        String str5;
        Cursor localCursor2;
        String str6;
        Iterator localIterator2;
        label307: int j;
        if (!bool1)
        {
          if (i != 0)
            break label388;
          String str2 = localCursor1.getString(localCursor1.getColumnIndex("contact_id"));
          str3 = localCursor1.getString(localCursor1.getColumnIndex("data2"));
          str4 = localCursor1.getString(localCursor1.getColumnIndex("data5"));
          str5 = localCursor1.getString(localCursor1.getColumnIndex("data3"));
          String[] arrayOfString2 = { "data1" };
          localCursor2 = FlixsterApplication.getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, arrayOfString2, "contact_id=" + str2, null, null);
          if (localCursor2.moveToFirst())
          {
            str6 = localCursor2.getString(localCursor2.getColumnIndex("data1"));
            localIterator2 = localArrayList.iterator();
            boolean bool2 = localIterator2.hasNext();
            j = 0;
            if (bool2)
              break label390;
          }
        }
        while (true)
        {
          if (j == 0)
            localArrayList.add(new Contact(str1, str3, str4, str5, str6));
          localCursor2.close();
          break;
          if (!((Contact)localIterator1.next()).name.equals(str1))
            break label139;
          i = 1;
          break label156;
          label388: break;
          label390: if (!((Contact)localIterator2.next()).number.equals(str6))
            break label307;
          j = 1;
        }
      }
    });
  }

  // ERROR //
  private static Property fetchProperties()
    throws DaoException
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_0
    //   2: new 71	java/net/URL
    //   5: dup
    //   6: invokestatic 76	net/flixster/android/data/ApiBuilder:properties	()Ljava/lang/String;
    //   9: invokespecial 77	java/net/URL:<init>	(Ljava/lang/String;)V
    //   12: astore_1
    //   13: invokestatic 83	net/flixster/android/FlixsterApplication:isConnected	()Z
    //   16: ifeq +5 -> 21
    //   19: iconst_0
    //   20: istore_0
    //   21: aload_1
    //   22: iload_0
    //   23: iconst_1
    //   24: invokestatic 89	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   27: astore 4
    //   29: new 91	net/flixster/android/model/Property
    //   32: dup
    //   33: new 93	org/json/JSONObject
    //   36: dup
    //   37: aload 4
    //   39: invokespecial 94	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   42: invokespecial 97	net/flixster/android/model/Property:<init>	(Lorg/json/JSONObject;)V
    //   45: astore 5
    //   47: aload 5
    //   49: areturn
    //   50: astore_3
    //   51: new 99	java/lang/RuntimeException
    //   54: dup
    //   55: aload_3
    //   56: invokespecial 102	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   59: athrow
    //   60: astore_2
    //   61: aload_2
    //   62: invokestatic 106	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   65: athrow
    //   66: astore 6
    //   68: aload 6
    //   70: invokestatic 106	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   73: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   2	19	50	java/net/MalformedURLException
    //   21	29	50	java/net/MalformedURLException
    //   2	19	60	java/io/IOException
    //   21	29	60	java/io/IOException
    //   29	47	66	org/json/JSONException
  }

  public static void fetchProperties(Handler paramHandler1, Handler paramHandler2)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          Property localProperty = MiscDao.access$0();
          Properties.instance().setProperties(localProperty);
          MiscDao.this.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          Logger.w("FlxMain", "MiscDao.fetchProperties", localDaoException);
        }
      }
    });
  }

  public static void registerDevice()
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        Logger.d("FlxMain", "MiscDao.registerDevice");
        RestClient localRestClient = new RestClient(ApiBuilder.device());
        ArrayList localArrayList = ApiBuilder.deviceParams();
        Iterator localIterator = localArrayList.iterator();
        while (true)
        {
          if (!localIterator.hasNext())
            localRestClient.addParam(localArrayList);
          try
          {
            localRestClient.execute(RestClient.RequestMethod.POST);
            Logger.d("FlxMain", "MiscDao.registerDevice response: " + localRestClient.getResponseCode() + " " + localRestClient.getResponse());
            return;
            NameValuePair localNameValuePair = (NameValuePair)localIterator.next();
            Logger.sd("FlxMain", "MiscDao.registerDevice " + localNameValuePair.getName() + "=" + localNameValuePair.getValue());
          }
          catch (IOException localIOException)
          {
            Logger.e("FlxMain", "MiscDao.registerDevice", localIOException);
          }
        }
      }
    });
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.MiscDao
 * JD-Core Version:    0.6.2
 */