package net.flixster.android.data;

import android.os.Handler;
import android.os.Message;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Season;
import net.flixster.android.model.Unfulfillable;
import net.flixster.android.util.HttpHelper;
import net.flixster.android.util.SoftHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MovieDao
{
  public static final int BOXOFFICE = 0;
  public static final int BROWSE = 3;
  public static final int DVD_COMING_SOON = 2;
  private static final Map<Long, Movie> MOVIE_CACHE = new SoftHashMap();
  public static final int UPCOMING = 1;

  public static void fetchBoxOffice(List<Movie> paramList1, List<Movie> paramList2, List<Movie> paramList3, List<Movie> paramList4, boolean paramBoolean)
    throws DaoException
  {
    String str = fetchJsonList(ApiBuilder.boxOffice(), paramBoolean);
    while (true)
    {
      int i;
      int j;
      Movie localMovie;
      try
      {
        JSONArray localJSONArray = new JSONArray(str);
        i = localJSONArray.length();
        paramList1.clear();
        paramList2.clear();
        paramList3.clear();
        paramList4.clear();
        j = 0;
        break label339;
        JSONObject localJSONObject = localJSONArray.getJSONObject(j);
        localMovie = getMovie(localJSONObject.getLong("id"));
        localMovie.hintTitle(localJSONObject.getString("title"));
        localMovie.isMIT = true;
        localMovie.parseFromJSON(localJSONObject);
        if (localMovie.isFeatured())
          paramList1.add(localMovie);
        if (localMovie.getProperty("Opening This Week") != null)
        {
          if ((!localMovie.isFeatured()) || (paramList2.isEmpty()))
          {
            if ((paramList2.size() == 1) && (((Movie)paramList2.get(0)).isFeatured()))
              paramList2.remove(0);
            paramList2.add(localMovie);
          }
        }
        else if (localMovie.getProperty("Top Box Office") != null)
        {
          if ((localMovie.isFeatured()) && (!paramList3.isEmpty()))
            break label347;
          if ((paramList3.size() == 1) && (((Movie)paramList3.get(0)).isFeatured()))
            paramList3.remove(0);
          paramList3.add(localMovie);
        }
      }
      catch (JSONException localJSONException)
      {
        throw DaoException.create(localJSONException);
      }
      if ((!localMovie.isFeatured()) || (paramList4.isEmpty()))
      {
        if ((paramList4.size() == 1) && (((Movie)paramList4.get(0)).isFeatured()))
          paramList4.remove(0);
        paramList4.add(localMovie);
      }
      label339: 
      while (j >= i)
      {
        return;
        label347: j++;
      }
    }
  }

  public static void fetchBoxOfficeForWidget(List<Movie> paramList, int paramInt, boolean paramBoolean)
    throws DaoException
  {
    String str = fetchJsonList(ApiBuilder.boxOffice(), paramBoolean);
    int i;
    int j;
    do
      try
      {
        JSONArray localJSONArray = new JSONArray(str);
        i = Math.min(paramInt, localJSONArray.length());
        paramList.clear();
        j = 0;
        continue;
        JSONObject localJSONObject = localJSONArray.getJSONObject(j);
        Movie localMovie = getMovie(localJSONObject.getLong("id"));
        localMovie.hintTitle(localJSONObject.getString("title"));
        localMovie.isMIT = true;
        localMovie.parseFromJSON(localJSONObject);
        paramList.add(localMovie);
        j++;
      }
      catch (JSONException localJSONException)
      {
        throw DaoException.create(localJSONException);
      }
    while (j < i);
  }

  public static void fetchDvdCategory(List<Movie> paramList, String paramString, boolean paramBoolean)
    throws DaoException
  {
    fetchMovies(ApiBuilder.dvdCategory(paramString), null, paramList, paramBoolean, false);
  }

  public static void fetchDvdComingSoon(List<Movie> paramList1, List<Movie> paramList2, boolean paramBoolean)
    throws DaoException
  {
    fetchMovies(ApiBuilder.dvdComingSoon(), paramList1, paramList2, paramBoolean, false);
  }

  public static void fetchDvdNewRelease(List<Movie> paramList1, List<Movie> paramList2, boolean paramBoolean)
    throws DaoException
  {
    fetchMovies(ApiBuilder.dvdNewRelease(), paramList1, paramList2, paramBoolean, false);
  }

  private static String fetchJsonList(String paramString, boolean paramBoolean)
    throws DaoException
  {
    try
    {
      String str = HttpHelper.fetchUrl(new URL(paramString), paramBoolean, paramBoolean);
      return str;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      throw new RuntimeException(localMalformedURLException);
    }
    catch (IOException localIOException)
    {
      throw DaoException.create(localIOException);
    }
  }

  private static void fetchMovies(String paramString, List<Movie> paramList1, List<Movie> paramList2, boolean paramBoolean1, boolean paramBoolean2)
    throws DaoException
  {
    String str = fetchJsonList(paramString, paramBoolean1);
    int i;
    int j;
    label175: 
    do
    {
      try
      {
        JSONArray localJSONArray = new JSONArray(str);
        i = localJSONArray.length();
        paramList2.clear();
        if (paramList1 != null)
        {
          paramList1.clear();
          break label175;
          JSONObject localJSONObject = localJSONArray.getJSONObject(j);
          Movie localMovie = getMovie(localJSONObject.getLong("id"));
          localMovie.parseFromJSON(localJSONObject);
          localMovie.isUpcoming = paramBoolean2;
          if ((!localMovie.isFeatured()) || (paramList2.isEmpty()))
          {
            if ((paramList2.size() == 1) && (((Movie)paramList2.get(0)).isFeatured()))
              paramList2.remove(0);
            paramList2.add(localMovie);
          }
          if ((paramList1 != null) && (localMovie.isFeatured()))
            paramList1.add(localMovie);
          j++;
        }
      }
      catch (JSONException localJSONException)
      {
        throw DaoException.create(localJSONException);
      }
      j = 0;
    }
    while (j < i);
  }

  public static void fetchUpcoming(List<Movie> paramList1, List<Movie> paramList2, boolean paramBoolean)
    throws DaoException
  {
    fetchMovies(ApiBuilder.upcoming(), paramList1, paramList2, paramBoolean, true);
  }

  public static Movie getMovie(long paramLong)
  {
    Movie localMovie = (Movie)MOVIE_CACHE.get(Long.valueOf(paramLong));
    if (localMovie == null)
    {
      localMovie = new Movie(paramLong);
      MOVIE_CACHE.put(Long.valueOf(paramLong), localMovie);
    }
    return localMovie;
  }

  // ERROR //
  public static Movie getMovieDetail(long paramLong)
    throws DaoException
  {
    // Byte code:
    //   0: lload_0
    //   1: invokestatic 72	net/flixster/android/data/MovieDao:getMovie	(J)Lnet/flixster/android/model/Movie;
    //   4: astore_2
    //   5: new 155	java/net/URL
    //   8: dup
    //   9: lload_0
    //   10: invokestatic 197	net/flixster/android/data/ApiBuilder:movie	(J)Ljava/lang/String;
    //   13: invokespecial 156	java/net/URL:<init>	(Ljava/lang/String;)V
    //   16: iconst_0
    //   17: iconst_0
    //   18: invokestatic 162	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   21: astore 5
    //   23: aload_2
    //   24: new 64	org/json/JSONObject
    //   27: dup
    //   28: aload 5
    //   30: invokespecial 198	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   33: invokevirtual 91	net/flixster/android/model/Movie:parseFromJSON	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;
    //   36: pop
    //   37: aload_2
    //   38: invokevirtual 201	net/flixster/android/model/Movie:setDetailsApiParsed	()V
    //   41: aload_2
    //   42: areturn
    //   43: astore 4
    //   45: new 164	java/lang/RuntimeException
    //   48: dup
    //   49: aload 4
    //   51: invokespecial 167	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   54: athrow
    //   55: astore_3
    //   56: aload_3
    //   57: invokestatic 123	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   60: athrow
    //   61: astore 6
    //   63: aload 6
    //   65: invokestatic 123	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   68: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   5	23	43	java/net/MalformedURLException
    //   5	23	55	java/io/IOException
    //   23	41	61	org/json/JSONException
  }

  public static Season getSeason(long paramLong)
  {
    long l = Season.generateIdHash(paramLong);
    Object localObject = (Movie)MOVIE_CACHE.get(Long.valueOf(l));
    if (localObject == null)
    {
      localObject = new Season(paramLong);
      MOVIE_CACHE.put(Long.valueOf(l), localObject);
    }
    while ((localObject instanceof Season))
      return (Season)localObject;
    return null;
  }

  public static void getSeasonDetail(long paramLong, Handler paramHandler1, final Handler paramHandler2)
  {
    Season localSeason = getSeason(paramLong);
    if (localSeason.isDetailsApiParsed())
    {
      paramHandler1.handleMessage(Message.obtain(null, 0, localSeason));
      return;
    }
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          Season localSeason = MovieDao.getSeasonDetailLegacy(this.val$seasonId);
          paramHandler2.sendMessage(Message.obtain(null, 0, localSeason));
          return;
        }
        catch (DaoException localDaoException)
        {
          this.val$errorHandler.sendMessage(Message.obtain(null, 0, localDaoException));
        }
      }
    });
  }

  // ERROR //
  public static Season getSeasonDetailLegacy(long paramLong)
    throws DaoException
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: lload_0
    //   3: invokestatic 214	net/flixster/android/data/MovieDao:getSeason	(J)Lnet/flixster/android/model/Season;
    //   6: astore_3
    //   7: new 155	java/net/URL
    //   10: dup
    //   11: lload_0
    //   12: invokestatic 247	net/flixster/android/data/ApiBuilder:season	(J)Ljava/lang/String;
    //   15: invokespecial 156	java/net/URL:<init>	(Ljava/lang/String;)V
    //   18: astore 4
    //   20: invokestatic 252	net/flixster/android/FlixsterApplication:isConnected	()Z
    //   23: ifeq +5 -> 28
    //   26: iconst_0
    //   27: istore_2
    //   28: aload 4
    //   30: iload_2
    //   31: iconst_1
    //   32: invokestatic 162	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   35: astore 7
    //   37: aload_3
    //   38: new 64	org/json/JSONObject
    //   41: dup
    //   42: aload 7
    //   44: invokespecial 198	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   47: invokevirtual 255	net/flixster/android/model/Season:parseFromJSON	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Season;
    //   50: pop
    //   51: aload_3
    //   52: invokevirtual 256	net/flixster/android/model/Season:setDetailsApiParsed	()V
    //   55: aload_3
    //   56: areturn
    //   57: astore 6
    //   59: new 164	java/lang/RuntimeException
    //   62: dup
    //   63: aload 6
    //   65: invokespecial 167	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   68: athrow
    //   69: astore 5
    //   71: aload 5
    //   73: invokestatic 123	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   76: athrow
    //   77: astore 8
    //   79: aload 8
    //   81: invokestatic 123	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   84: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   7	26	57	java/net/MalformedURLException
    //   28	37	57	java/net/MalformedURLException
    //   7	26	69	java/io/IOException
    //   28	37	69	java/io/IOException
    //   37	55	77	org/json/JSONException
  }

  public static Unfulfillable getUnfulfillable(long paramLong)
  {
    long l = Unfulfillable.generateIdHash(paramLong);
    Unfulfillable localUnfulfillable = (Unfulfillable)MOVIE_CACHE.get(Long.valueOf(l));
    if (localUnfulfillable == null)
    {
      localUnfulfillable = new Unfulfillable(paramLong);
      MOVIE_CACHE.put(Long.valueOf(l), localUnfulfillable);
    }
    while ((localUnfulfillable instanceof Unfulfillable))
      return (Unfulfillable)localUnfulfillable;
    return null;
  }

  public static void getUserSeasonDetail(long paramLong, Handler paramHandler1, final Handler paramHandler2)
  {
    Season localSeason = getSeason(paramLong);
    if (localSeason.isUserSeasonParsed())
    {
      paramHandler1.handleMessage(Message.obtain(null, 0, localSeason));
      return;
    }
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        MovieDao.getUserSeasonDetailBg(this.val$seasonId, paramHandler2, this.val$errorHandler);
      }
    });
  }

  // ERROR //
  public static void getUserSeasonDetailBg(long paramLong, Handler paramHandler1, Handler paramHandler2)
  {
    // Byte code:
    //   0: lload_0
    //   1: invokestatic 214	net/flixster/android/data/MovieDao:getSeason	(J)Lnet/flixster/android/model/Season;
    //   4: astore 4
    //   6: aload 4
    //   8: invokevirtual 217	net/flixster/android/model/Season:isDetailsApiParsed	()Z
    //   11: ifne +13 -> 24
    //   14: lload_0
    //   15: invokestatic 272	net/flixster/android/data/MovieDao:getSeasonDetailLegacy	(J)Lnet/flixster/android/model/Season;
    //   18: astore 20
    //   20: aload 20
    //   22: astore 4
    //   24: invokestatic 278	net/flixster/android/data/ProfileDao:fetchUser	()Lnet/flixster/android/model/User;
    //   27: astore 7
    //   29: new 155	java/net/URL
    //   32: dup
    //   33: aload 7
    //   35: lload_0
    //   36: invokevirtual 283	net/flixster/android/model/User:getLockerRightId	(J)J
    //   39: invokestatic 288	net/flixster/android/model/LockerRight:getRawSeasonRightId	(J)J
    //   42: invokestatic 291	net/flixster/android/data/ApiBuilder:rightEpisodes	(J)Ljava/lang/String;
    //   45: invokespecial 156	java/net/URL:<init>	(Ljava/lang/String;)V
    //   48: astore 8
    //   50: invokestatic 252	net/flixster/android/FlixsterApplication:isConnected	()Z
    //   53: ifeq +82 -> 135
    //   56: iconst_0
    //   57: istore 12
    //   59: aload 8
    //   61: iload 12
    //   63: iconst_1
    //   64: invokestatic 162	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   67: astore 13
    //   69: aload 13
    //   71: invokestatic 295	net/flixster/android/data/DaoException:hasError	(Ljava/lang/String;)Z
    //   74: ifeq +101 -> 175
    //   77: aload_3
    //   78: ifnull +18 -> 96
    //   81: aload_3
    //   82: aconst_null
    //   83: iconst_0
    //   84: aload 13
    //   86: invokestatic 298	net/flixster/android/data/DaoException:create	(Ljava/lang/String;)Lnet/flixster/android/data/DaoException;
    //   89: invokestatic 223	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
    //   92: invokevirtual 302	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   95: pop
    //   96: return
    //   97: astore 18
    //   99: aload_3
    //   100: ifnull -4 -> 96
    //   103: aload_3
    //   104: aconst_null
    //   105: iconst_0
    //   106: aload 18
    //   108: invokestatic 223	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
    //   111: invokevirtual 302	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   114: pop
    //   115: return
    //   116: astore 5
    //   118: aload_3
    //   119: ifnull -23 -> 96
    //   122: aload_3
    //   123: aconst_null
    //   124: iconst_0
    //   125: aload 5
    //   127: invokestatic 223	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
    //   130: invokevirtual 302	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   133: pop
    //   134: return
    //   135: iconst_1
    //   136: istore 12
    //   138: goto -79 -> 59
    //   141: astore 11
    //   143: new 164	java/lang/RuntimeException
    //   146: dup
    //   147: aload 11
    //   149: invokespecial 167	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   152: athrow
    //   153: astore 9
    //   155: aload_3
    //   156: ifnull -60 -> 96
    //   159: aload_3
    //   160: aconst_null
    //   161: iconst_0
    //   162: aload 9
    //   164: invokestatic 123	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   167: invokestatic 223	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
    //   170: invokevirtual 302	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   173: pop
    //   174: return
    //   175: aload 7
    //   177: aload 4
    //   179: new 44	org/json/JSONArray
    //   182: dup
    //   183: aload 13
    //   185: invokespecial 47	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   188: invokevirtual 306	net/flixster/android/model/Season:mergeEpisodes	(Lorg/json/JSONArray;)Ljava/util/Collection;
    //   191: invokevirtual 310	net/flixster/android/model/User:addLockerRights	(Ljava/util/Collection;)V
    //   194: aload_2
    //   195: ifnull -99 -> 96
    //   198: aload_2
    //   199: aconst_null
    //   200: iconst_0
    //   201: aload 4
    //   203: invokestatic 223	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
    //   206: invokevirtual 302	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   209: pop
    //   210: return
    //   211: astore 14
    //   213: aload_3
    //   214: ifnull -118 -> 96
    //   217: aload_3
    //   218: aconst_null
    //   219: iconst_0
    //   220: aload 14
    //   222: invokestatic 123	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   225: invokestatic 223	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
    //   228: invokevirtual 302	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   231: pop
    //   232: return
    //
    // Exception table:
    //   from	to	target	type
    //   14	20	97	net/flixster/android/data/DaoException
    //   24	29	116	net/flixster/android/data/DaoException
    //   29	56	141	java/net/MalformedURLException
    //   59	69	141	java/net/MalformedURLException
    //   29	56	153	java/io/IOException
    //   59	69	153	java/io/IOException
    //   175	194	211	org/json/JSONException
    //   198	210	211	org/json/JSONException
  }

  public static void searchMovies(String paramString, List<Movie> paramList)
    throws DaoException
  {
    Logger.d("FlxMain", "MovieDao.searchMovies query:" + paramString);
    String str = fetchJsonList(ApiBuilder.searchMovies(paramString), false);
    int i;
    int j;
    do
      try
      {
        JSONArray localJSONArray = new JSONArray(str);
        i = localJSONArray.length();
        paramList.clear();
        Logger.d("FlxMain", "MovieDao.searchMovies moviesLength:" + i);
        j = 0;
        continue;
        JSONObject localJSONObject = localJSONArray.getJSONObject(j);
        Movie localMovie = getMovie(localJSONObject.getLong("id"));
        localMovie.parseFromJSON(localJSONObject);
        paramList.add(localMovie);
        j++;
      }
      catch (JSONException localJSONException)
      {
        throw DaoException.create(localJSONException);
      }
    while (j < i);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.MovieDao
 * JD-Core Version:    0.6.2
 */