package net.flixster.android.data;

import android.os.Handler;
import android.os.Message;
import com.flixster.android.utils.ImageGetter;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.Movie;
import net.flixster.android.model.NetflixError;
import net.flixster.android.model.NetflixException;
import net.flixster.android.model.NetflixQueue;
import net.flixster.android.model.NetflixQueueItem;
import net.flixster.android.model.NetflixTitleState;
import net.flixster.android.util.HttpHelper;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.basic.DefaultOAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NetflixDao
{
  public static final int MAX_RESULTS = 25;
  public static final String NETFLIX_ACCESS_TOKEN_URL = "http://api-public.netflix.com/oauth/access_token";
  public static final String NETFLIX_APPLICATION_NAME = "Movies";
  public static final String NETFLIX_AUTHORIZE_WEBSITE_URL = "https://api-user.netflix.com/oauth/login";
  public static final String NETFLIX_CALLBACK_URL = "flixster://netflix/queue";
  public static final String NETFLIX_CONSUMER_KEY = "rtuhbkms7jmpsn3sft3va7y6";
  public static final String NETFLIX_CONSUMER_SECRET = "YDPK6rDpPF";
  public static final String NETFLIX_REQUEST_TOKEN_URL = "http://api-public.netflix.com/oauth/request_token";
  public static final String QTYPE_ATHOME = "/at_home";
  public static final String QTYPE_DISC = "/queues/disc/available";
  public static final String QTYPE_INSTANT = "/queues/instant/available";
  public static final String QTYPE_SAVED = "/queues/disc/saved";
  private static final int TIMEOUT_HTTP_GET = 6000;
  private static final int TIMEOUT_HTTP_POST = 8000;
  public static final String URL_NEWACCOUNT = "http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817";
  private static final String URL_NEWACCOUNT_IMPRESSION = "http://gan.doubleclick.net/gan_impression?lid=41000000030512611&pubid=21000000000262817";

  private static String convertFlxDbNetflixIdToNetflixUrl(String paramString)
  {
    return paramString.replace("api.netflix.com", "api-public.netflix.com");
  }

  private static String convertNetflixUrlToFlxDbNetflixId(String paramString)
  {
    return paramString.replace("api-public.netflix.com", "api.netflix.com");
  }

  public static void deleteQueueItem(String paramString1, String paramString2)
    throws OAuthMessageSignerException, OAuthExpectationFailedException, ClientProtocolException, IOException, OAuthNotAuthorizedException, OAuthCommunicationException
  {
    Logger.i("FlxNf", "NetflixDao.deleteQueueItem " + paramString1 + " " + paramString2);
    DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient();
    HttpParams localHttpParams = localDefaultHttpClient.getParams();
    HttpConnectionParams.setConnectionTimeout(localHttpParams, 8000);
    HttpConnectionParams.setSoTimeout(localHttpParams, 8000);
    String str1 = ApiBuilder.netflixDeleteQueue(paramString2, paramString1);
    Logger.d("FlxNf", "NetflixDao.deleteQueueItem url:" + str1);
    HttpDelete localHttpDelete = new HttpDelete(str1);
    localHttpDelete.getParams().setBooleanParameter("http.protocol.expect-continue", false);
    getCommonsHttpConsumer().sign(localHttpDelete);
    HttpResponse localHttpResponse = localDefaultHttpClient.execute(localHttpDelete);
    Logger.d("FlxNf", "NetflixDao.deleteQueueItem post response: " + localHttpResponse.getStatusLine());
    int i = localHttpResponse.getStatusLine().getStatusCode();
    String str2 = localHttpResponse.getStatusLine().getReasonPhrase();
    localHttpResponse.getEntity().consumeContent();
    if (i != 200)
    {
      Logger.e("FlxNf", "NetflixDao.deleteQueueItem  statusCode:" + i + " reason:" + str2);
      throw new OAuthNotAuthorizedException();
    }
    Logger.d("FlxNf", "NetflixDao.deleteQueueItem deleted");
  }

  public static void fetchInsertFlixsterMovies(ArrayList<NetflixQueueItem> paramArrayList)
    throws Exception
  {
    Logger.d("FlxNf", "NetflixDao.fetchInsertFlixsterMovies()");
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new BasicNameValuePair("view", "long"));
    HashMap localHashMap = new HashMap();
    Iterator localIterator = paramArrayList.iterator();
    JSONArray localJSONArray;
    int i;
    if (!localIterator.hasNext())
    {
      localJSONArray = new JSONArray(HttpHelper.postToUrl(ApiBuilder.netflixMovies(), localArrayList));
      i = localJSONArray.length();
      Logger.d("FlxNf", "NetflixDao.fetchInsertFlixsterMovies() length:" + i);
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        return;
        NetflixQueueItem localNetflixQueueItem2 = (NetflixQueueItem)localIterator.next();
        String str2 = localNetflixQueueItem2.getNetflixUrlId();
        if (str2 == null)
          break;
        localHashMap.put(str2, localNetflixQueueItem2);
        localArrayList.add(new BasicNameValuePair("id", convertNetflixUrlToFlxDbNetflixId(str2)));
        break;
      }
      JSONObject localJSONObject = localJSONArray.getJSONObject(j);
      Movie localMovie = MovieDao.getMovie(localJSONObject.getLong("id"));
      localMovie.netflixParseFromJSON(localJSONObject);
      String str1 = convertFlxDbNetflixIdToNetflixUrl(localMovie.getProperty("netflix"));
      localMovie.setProperty("netflix", str1);
      NetflixQueueItem localNetflixQueueItem1 = (NetflixQueueItem)localHashMap.get(str1);
      if (localNetflixQueueItem1 != null)
        localNetflixQueueItem1.mMovie = localMovie;
    }
  }

  public static NetflixQueue fetchQueue(int paramInt, String paramString)
    throws Exception
  {
    Logger.i("FlxNf", "NetflixDao.fetchQueue " + paramString + " " + paramInt);
    URL localURL = new URL(ApiBuilder.netflixGetQueue(paramString, paramInt, 25));
    Logger.d("FlxNf", "NetflixDao.fetchQueue url:" + localURL);
    HttpURLConnection localHttpURLConnection = (HttpURLConnection)localURL.openConnection();
    localHttpURLConnection.setConnectTimeout(6000);
    localHttpURLConnection.setReadTimeout(6000);
    getNewConsumer().sign(localHttpURLConnection);
    try
    {
      byte[] arrayOfByte = HttpHelper.fetchUrlBytes(localHttpURLConnection);
      localHttpURLConnection.disconnect();
      JSONObject localJSONObject = new JSONObject(new String(arrayOfByte, "ISO-8859-1").toString());
      if ((localJSONObject.has("queue")) || (localJSONObject.has("at_home")))
      {
        NetflixQueue localNetflixQueue = new NetflixQueue();
        localNetflixQueue.parseFromJson(localJSONObject, "/queues/disc/available".equals(paramString));
        ArrayList localArrayList = localNetflixQueue.getNetflixQueueItemList();
        Logger.d("FlxNf", "NetflixDao.fetchQueue fetched size:" + localArrayList.size());
        Logger.d("FlxNf", "NetflixDao.fetchQueue total size:" + localNetflixQueue.getNumberOfResults());
        fetchInsertFlixsterMovies(localArrayList);
        return localNetflixQueue;
      }
    }
    finally
    {
      localHttpURLConnection.disconnect();
    }
    throw new Exception("JSON object didn't have a 'queue'");
  }

  public static void fetchQueue(final Handler paramHandler1, final Handler paramHandler2, String paramString)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        Handler localHandler;
        try
        {
          NetflixQueue localNetflixQueue = NetflixDao.fetchQueue(0, NetflixDao.this);
          paramHandler1.sendMessage(Message.obtain(null, 0, localNetflixQueue));
          return;
        }
        catch (Exception localException)
        {
          localHandler = paramHandler2;
          if (NetflixDao.this != "/queues/disc/available");
        }
        for (int i = 3; ; i = 4)
        {
          localHandler.sendMessage(Message.obtain(null, i, localException));
          return;
        }
      }
    });
  }

  public static NetflixTitleState fetchTitleState(String paramString)
    throws IOException, OAuthMessageSignerException, OAuthExpectationFailedException, JSONException, OAuthCommunicationException
  {
    Logger.i("FlxNf", "NetflixDao.fetchTitleState " + paramString);
    URL localURL = new URL(ApiBuilder.netflixTitleState(paramString));
    Logger.d("FlxNf", "NetflixDao.fetchTitleState url:" + localURL);
    HttpURLConnection localHttpURLConnection = (HttpURLConnection)localURL.openConnection();
    getNewConsumer().sign(localHttpURLConnection);
    StringBuilder localStringBuilder = new StringBuilder();
    try
    {
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localHttpURLConnection.getInputStream()));
      while (true)
      {
        String str = localBufferedReader.readLine();
        if (str == null)
        {
          localHttpURLConnection.disconnect();
          JSONObject localJSONObject1 = new JSONObject(localStringBuilder.toString());
          boolean bool1 = localJSONObject1.has("title_states");
          NetflixTitleState localNetflixTitleState = null;
          if (bool1)
          {
            JSONObject localJSONObject2 = localJSONObject1.getJSONObject("title_states");
            boolean bool2 = localJSONObject2.has("title_state");
            localNetflixTitleState = null;
            if (bool2)
            {
              JSONObject localJSONObject3 = localJSONObject2.getJSONObject("title_state");
              localNetflixTitleState = new NetflixTitleState();
              localNetflixTitleState.parseFromJSON(localJSONObject3);
            }
          }
          return localNetflixTitleState;
        }
        localStringBuilder.append(str);
      }
    }
    finally
    {
      localHttpURLConnection.disconnect();
    }
  }

  public static OAuthConsumer getCommonsHttpConsumer()
  {
    CommonsHttpOAuthConsumer localCommonsHttpOAuthConsumer = new CommonsHttpOAuthConsumer("rtuhbkms7jmpsn3sft3va7y6", "YDPK6rDpPF");
    localCommonsHttpOAuthConsumer.setTokenWithSecret(FlixsterApplication.getNetflixOAuthToken(), FlixsterApplication.getNetflixOAuthTokenSecret());
    return localCommonsHttpOAuthConsumer;
  }

  public static OAuthProvider getCommonsHttpProvider()
  {
    CommonsHttpOAuthProvider localCommonsHttpOAuthProvider = new CommonsHttpOAuthProvider("http://api-public.netflix.com/oauth/request_token", "http://api-public.netflix.com/oauth/access_token", "https://api-user.netflix.com/oauth/login");
    localCommonsHttpOAuthProvider.setOAuth10a(false);
    return localCommonsHttpOAuthProvider;
  }

  public static OAuthConsumer getNewConsumer()
  {
    DefaultOAuthConsumer localDefaultOAuthConsumer = new DefaultOAuthConsumer("rtuhbkms7jmpsn3sft3va7y6", "YDPK6rDpPF");
    localDefaultOAuthConsumer.setTokenWithSecret(FlixsterApplication.getNetflixOAuthToken(), FlixsterApplication.getNetflixOAuthTokenSecret());
    return localDefaultOAuthConsumer;
  }

  public static OAuthProvider getNewProvider()
  {
    DefaultOAuthProvider localDefaultOAuthProvider = new DefaultOAuthProvider("http://api-public.netflix.com/oauth/request_token", "http://api-public.netflix.com/oauth/access_token", "https://api-user.netflix.com/oauth/login");
    localDefaultOAuthProvider.setOAuth10a(false);
    return localDefaultOAuthProvider;
  }

  public static void postQueueItem(String paramString1, int paramInt, String paramString2)
    throws OAuthMessageSignerException, JSONException, OAuthExpectationFailedException, ClientProtocolException, IOException, OAuthNotAuthorizedException, OAuthCommunicationException
  {
    Logger.i("FlxNf", "NetflixDao.postQueueItem " + paramString1 + " " + paramInt + " " + paramString2);
    DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient();
    HttpParams localHttpParams = localDefaultHttpClient.getParams();
    HttpConnectionParams.setConnectionTimeout(localHttpParams, 8000);
    HttpConnectionParams.setSoTimeout(localHttpParams, 8000);
    String str1 = ApiBuilder.netflixPostQueue(paramString2, paramString1, paramInt);
    Logger.d("FlxNf", "NetflixDao.postQueueItem post url: " + str1);
    HttpPost localHttpPost = new HttpPost(str1);
    localHttpPost.getParams().setBooleanParameter("http.protocol.expect-continue", false);
    getCommonsHttpConsumer().sign(localHttpPost);
    HttpResponse localHttpResponse = localDefaultHttpClient.execute(localHttpPost);
    Logger.d("FlxNf", "NetflixDao.postQueueItem post response: " + localHttpResponse.getStatusLine());
    int i = localHttpResponse.getStatusLine().getStatusCode();
    StringBuilder localStringBuilder = new StringBuilder();
    localHttpResponse.getEntity().consumeContent();
    if (i != 201)
    {
      HttpEntity localHttpEntity = localHttpResponse.getEntity();
      BufferedReader localBufferedReader;
      if (localHttpEntity != null)
      {
        InputStream localInputStream = localHttpEntity.getContent();
        InputStreamReader localInputStreamReader = new InputStreamReader(localInputStream);
        localBufferedReader = new BufferedReader(localInputStreamReader);
      }
      while (true)
      {
        String str2 = localBufferedReader.readLine();
        if (str2 == null)
        {
          NetflixError localNetflixError = NetflixError.parseFromJSONString(localStringBuilder.toString());
          if ((localNetflixError.mStatusCode != 400) || (localNetflixError.mSubCode != 650))
            break;
          throw new NetflixException(localNetflixError.message);
        }
        localStringBuilder.append(str2).append("\n");
      }
      throw new OAuthNotAuthorizedException();
    }
  }

  public static void trackNewAccountImpression()
  {
    ImageGetter.instance().get("http://gan.doubleclick.net/gan_impression?lid=41000000030512611&pubid=21000000000262817", null);
  }

  public static void trackNewAccountReferral()
  {
    ImageGetter.instance().get("http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817", null);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.NetflixDao
 * JD-Core Version:    0.6.2
 */