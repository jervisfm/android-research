package net.flixster.android.data;

@Deprecated
public class NewsDao
{
  private static final String NEWS_API = "http://api.flixster.com/api/v1/news.json?limit=10&period=10";

  // ERROR //
  public static java.util.ArrayList<net.flixster.android.model.NewsStory> getNewsItemList()
    throws DaoException
  {
    // Byte code:
    //   0: new 25	java/net/URL
    //   3: dup
    //   4: ldc 9
    //   6: invokespecial 28	java/net/URL:<init>	(Ljava/lang/String;)V
    //   9: iconst_0
    //   10: iconst_0
    //   11: invokestatic 34	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   14: astore_2
    //   15: new 36	org/json/JSONArray
    //   18: dup
    //   19: aload_2
    //   20: invokespecial 37	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   23: astore_3
    //   24: aload_3
    //   25: invokevirtual 41	org/json/JSONArray:length	()I
    //   28: istore 5
    //   30: new 43	java/util/ArrayList
    //   33: dup
    //   34: invokespecial 44	java/util/ArrayList:<init>	()V
    //   37: astore 6
    //   39: iconst_0
    //   40: istore 7
    //   42: iload 7
    //   44: iload 5
    //   46: if_icmplt +22 -> 68
    //   49: aload 6
    //   51: areturn
    //   52: astore_1
    //   53: new 46	java/lang/RuntimeException
    //   56: dup
    //   57: aload_1
    //   58: invokespecial 49	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   61: athrow
    //   62: astore_0
    //   63: aload_0
    //   64: invokestatic 53	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   67: athrow
    //   68: aload_3
    //   69: iload 7
    //   71: invokevirtual 57	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   74: astore 8
    //   76: new 59	net/flixster/android/model/NewsStory
    //   79: dup
    //   80: invokespecial 60	net/flixster/android/model/NewsStory:<init>	()V
    //   83: astore 9
    //   85: aload 9
    //   87: aload 8
    //   89: invokevirtual 64	net/flixster/android/model/NewsStory:parseJSONObject	(Lorg/json/JSONObject;)Lnet/flixster/android/model/NewsStory;
    //   92: pop
    //   93: aload 6
    //   95: aload 9
    //   97: invokevirtual 68	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   100: pop
    //   101: iinc 7 1
    //   104: goto -62 -> 42
    //   107: astore 4
    //   109: aload 4
    //   111: invokestatic 53	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   114: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   0	15	52	java/net/MalformedURLException
    //   0	15	62	java/io/IOException
    //   15	39	107	org/json/JSONException
    //   68	101	107	org/json/JSONException
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.NewsDao
 * JD-Core Version:    0.6.2
 */