package net.flixster.android.data;

import android.os.Handler;
import android.os.Message;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import java.util.ArrayList;
import java.util.List;
import net.flixster.android.FlixUtils;
import net.flixster.android.model.Photo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PhotoDao
{
  public static final String FILTER_RANDOM = "random";
  public static final String FILTER_TOP_ACTORS = "top-actors";
  public static final String FILTER_TOP_ACTRESSES = "top-actresses";
  public static final String FILTER_TOP_MOVIES = "top-movies";

  public static ArrayList<Photo> getActorPhotos(long paramLong)
    throws DaoException
  {
    ArrayList localArrayList = new ArrayList();
    getPhotos("&actor=" + paramLong, localArrayList);
    return localArrayList;
  }

  public static ArrayList<Photo> getMoviePhotos(long paramLong)
    throws DaoException
  {
    ArrayList localArrayList = new ArrayList();
    getPhotos("&movie=" + paramLong, localArrayList);
    return localArrayList;
  }

  // ERROR //
  private static void getPhotos(String paramString, List<Photo> paramList)
    throws DaoException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 63	net/flixster/android/data/ApiBuilder:photos	(Ljava/lang/String;)Ljava/lang/String;
    //   4: astore_2
    //   5: new 65	java/net/URL
    //   8: dup
    //   9: aload_2
    //   10: invokespecial 66	java/net/URL:<init>	(Ljava/lang/String;)V
    //   13: iconst_1
    //   14: iconst_1
    //   15: invokestatic 72	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   18: astore 5
    //   20: new 74	org/json/JSONArray
    //   23: dup
    //   24: aload 5
    //   26: invokespecial 75	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   29: aload_1
    //   30: invokestatic 79	net/flixster/android/data/PhotoDao:parsePhotos	(Lorg/json/JSONArray;Ljava/util/List;)V
    //   33: return
    //   34: astore 4
    //   36: new 81	java/lang/RuntimeException
    //   39: dup
    //   40: aload 4
    //   42: invokespecial 84	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   45: athrow
    //   46: astore_3
    //   47: aload_3
    //   48: invokestatic 88	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   51: athrow
    //   52: astore 6
    //   54: aload 6
    //   56: invokestatic 88	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   59: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   5	20	34	java/net/MalformedURLException
    //   5	20	46	java/io/IOException
    //   20	33	52	org/json/JSONException
  }

  public static ArrayList<Photo> getTopPhotos(String paramString)
    throws DaoException
  {
    ArrayList localArrayList = new ArrayList();
    getPhotos("&filter=" + paramString, localArrayList);
    return localArrayList;
  }

  public static void getUserPhotos(String paramString, final List<Photo> paramList, final Handler paramHandler1, final Handler paramHandler2)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          PhotoDao.getPhotos("&username=" + PhotoDao.this, paramList);
          paramHandler1.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          Logger.e("FlxMain", "PhotoDao.getUserPhotos DaoException", localDaoException);
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException));
        }
      }
    });
  }

  private static void parsePhotos(JSONArray paramJSONArray, List<Photo> paramList)
    throws JSONException
  {
    for (int i = 0; ; i++)
    {
      if (i >= paramJSONArray.length())
        return;
      Photo localPhoto = new Photo();
      JSONObject localJSONObject1 = paramJSONArray.getJSONObject(i);
      localPhoto.id = localJSONObject1.getLong("id");
      localPhoto.caption = FlixUtils.copyString(localJSONObject1.getString("caption"));
      localPhoto.description = FlixUtils.copyString(localJSONObject1.getString("description"));
      if (localJSONObject1.has("images"))
      {
        JSONObject localJSONObject4 = localJSONObject1.getJSONObject("images");
        if (localJSONObject4.has("thumbnail"))
          localPhoto.thumbnailUrl = FlixUtils.copyString(localJSONObject4.getJSONObject("thumbnail").getString("url"));
        if (localJSONObject4.has("origional"))
          localPhoto.origionalUrl = FlixUtils.copyString(localJSONObject4.getJSONObject("origional").getString("url"));
        if (localJSONObject4.has("gallery"))
          localPhoto.galleryUrl = FlixUtils.copyString(localJSONObject4.getJSONObject("gallery").getString("url"));
      }
      if (localJSONObject1.has("movie"))
      {
        JSONObject localJSONObject3 = localJSONObject1.getJSONObject("movie");
        localPhoto.movieId = localJSONObject3.getLong("id");
        localPhoto.movieTitle = FlixUtils.copyString(localJSONObject3.getString("title"));
      }
      if (localJSONObject1.has("actor"))
      {
        JSONObject localJSONObject2 = localJSONObject1.getJSONObject("actor");
        localPhoto.actorId = localJSONObject2.getLong("id");
        localPhoto.actorName = FlixUtils.copyString(localJSONObject2.getString("name"));
      }
      paramList.add(localPhoto);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.PhotoDao
 * JD-Core Version:    0.6.2
 */