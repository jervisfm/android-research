package net.flixster.android.data;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.drm.DownloadRight;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.MigrationHelper;
import com.flixster.android.drm.PlaybackLogic;
import com.flixster.android.drm.PlaybackManager;
import com.flixster.android.net.RestClient;
import com.flixster.android.net.RestClient.RequestMethod;
import com.flixster.android.utils.ActivityHolder;
import com.flixster.android.utils.DateTimeHelper;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.FlixUtils;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.LockerRight.RightType;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;
import net.flixster.android.model.Season;
import net.flixster.android.model.User;
import net.flixster.android.model.VideoAsset;
import net.flixster.android.util.HttpHelper;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProfileDao
{
  public static final boolean HTTPS_STREAM_CREATE = true;
  private static User currentUser = null;
  private static HashMap<String, User> usersMap = new HashMap();

  public static void ackLicense(Handler paramHandler1, final Handler paramHandler2, String paramString)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          HttpHelper.postToUrl(ApiBuilder.sonicAckLicense(), ApiBuilder.sonicAckLicenseParams(ProfileDao.this));
          return;
        }
        catch (IOException localIOException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(localIOException)));
        }
      }
    });
  }

  public static void canDownload(final Handler paramHandler1, final Handler paramHandler2, LockerRight paramLockerRight)
  {
    long l = paramLockerRight.rightId;
    DownloadRight localDownloadRight = new DownloadRight(l);
    String str = paramLockerRight.getDownloadUri();
    if (str == null)
    {
      str = localDownloadRight.getDownloadUri();
      if ("".equals(str));
    }
    else
    {
      paramLockerRight.setDownloadUri(str);
      paramLockerRight.setDownloadCaptionsUri(localDownloadRight.getCaptionsUri());
      paramHandler1.sendMessage(Message.obtain(null, 0, paramLockerRight));
      return;
    }
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        String str1 = ApiBuilder.rightDownload(this.val$rightId, null, null, null, paramHandler2.isSonicProxyMode());
        ArrayList localArrayList = new ArrayList();
        String str2;
        try
        {
          str2 = HttpHelper.postToUrl(str1, localArrayList);
          if (DaoException.hasError(str2))
          {
            paramHandler1.sendMessage(Message.obtain(null, 0, DaoException.create(str2)));
            return;
          }
        }
        catch (IOException localIOException)
        {
          paramHandler1.sendMessage(Message.obtain(null, 0, DaoException.create(localIOException)));
          return;
        }
        try
        {
          JSONObject localJSONObject1 = new JSONObject(str2);
          JSONObject localJSONObject2 = localJSONObject1.optJSONObject("assets");
          JSONObject localJSONObject3 = localJSONObject1.optJSONObject("lockerRightParams");
          int i = localJSONObject3.optInt("downloadsRemaining");
          paramHandler2.setDownloadsRemain(i);
          if (i > 0)
          {
            JSONObject localJSONObject4 = localJSONObject2.optJSONObject("downloadLow");
            if (localJSONObject4 != null)
            {
              String str3 = localJSONObject4.optString("fileLocation");
              DownloadRight localDownloadRight = new DownloadRight(this.val$rightId);
              localDownloadRight.setDownloadUri(str3);
              paramHandler2.setDownloadUri(str3);
              String str4 = localJSONObject4.optString("closedCaptionFileLocation");
              localDownloadRight.setCaptionsUri(str4);
              paramHandler2.setDownloadCaptionsUri(str4);
              paramHandler2.setDownloadAssetSize(0);
              if (paramHandler2.isSonicProxyMode())
              {
                String str5 = localJSONObject4.optString("drmServerUrl");
                String str6 = Long.toString(localJSONObject4.optLong("sonicQueueId"));
                String str7 = localJSONObject4.optString("destinationUniqueId") + "|" + localJSONObject4.optInt("deviceTypeId");
                Logger.d("FlxMain", "start download: licenseProxy " + str5 + ", queueId " + str6 + ", deviceId " + str7);
                localDownloadRight.setLicenseProxy(str5);
                localDownloadRight.setQueueId(str6);
                localDownloadRight.setDeviceId(str7);
              }
              localDownloadRight.save();
            }
            int j = 1000 * localJSONObject3.optInt("playPosition");
            SharedPreferences.Editor localEditor = FlixsterApplication.getContext().getSharedPreferences("restartTime", 0).edit();
            localEditor.putInt(String.valueOf(paramHandler2.assetId), j);
            localEditor.commit();
          }
          this.val$successHandler.sendMessage(Message.obtain(null, 0, paramHandler2));
          return;
        }
        catch (JSONException localJSONException)
        {
          paramHandler1.sendMessage(Message.obtain(null, 0, DaoException.create(localJSONException)));
        }
      }
    });
  }

  public static void clearBitmapCache()
  {
    try
    {
      clearUserBitmaps(currentUser);
      if ((usersMap != null) && (!usersMap.isEmpty()))
      {
        Iterator localIterator = usersMap.values().iterator();
        while (true)
        {
          if (!localIterator.hasNext())
            return;
          clearUserBitmaps((User)localIterator.next());
        }
      }
    }
    catch (Exception localException)
    {
      Logger.w("FlxMain", "problem clearing bitmap cache", localException);
    }
  }

  private static void clearUserBitmaps(User paramUser)
  {
    if ((paramUser != null) && (paramUser.friends != null) && (!paramUser.friends.isEmpty()));
    for (int i = 0; ; i++)
    {
      if (i >= paramUser.friends.size())
        return;
      ((User)paramUser.friends.get(i)).bitmap = null;
    }
  }

  public static void createNewUser()
  {
    currentUser = new User();
  }

  public static void endDownload(final Handler paramHandler1, final Handler paramHandler2, long paramLong, String paramString)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        String str = ApiBuilder.rightDownload(this.val$rightId, "stop", paramHandler2, null, false);
        try
        {
          HttpHelper.deleteFromUrl(str);
          this.val$successHandler.sendMessage(Message.obtain(null, 0, paramHandler2));
          return;
        }
        catch (IOException localIOException)
        {
          paramHandler1.sendMessage(Message.obtain(null, 0, DaoException.create(localIOException)));
        }
      }
    });
  }

  // ERROR //
  public static User fetchUser()
    throws DaoException
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_0
    //   2: ldc 2
    //   4: monitorenter
    //   5: ldc 135
    //   7: new 182	java/lang/StringBuilder
    //   10: dup
    //   11: ldc 184
    //   13: invokespecial 186	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   16: getstatic 16	net/flixster/android/data/ProfileDao:currentUser	Lnet/flixster/android/model/User;
    //   19: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   22: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   25: invokestatic 197	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   28: getstatic 16	net/flixster/android/data/ProfileDao:currentUser	Lnet/flixster/android/model/User;
    //   31: ifnull +16 -> 47
    //   34: getstatic 16	net/flixster/android/data/ProfileDao:currentUser	Lnet/flixster/android/model/User;
    //   37: getfield 201	net/flixster/android/model/User:id	Ljava/lang/String;
    //   40: astore 12
    //   42: aload 12
    //   44: ifnonnull +163 -> 207
    //   47: new 203	java/net/URL
    //   50: dup
    //   51: invokestatic 208	net/flixster/android/data/ApiBuilder:viewer	()Ljava/lang/String;
    //   54: invokespecial 209	java/net/URL:<init>	(Ljava/lang/String;)V
    //   57: astore_2
    //   58: invokestatic 214	net/flixster/android/FlixsterApplication:isConnected	()Z
    //   61: ifeq +5 -> 66
    //   64: iconst_0
    //   65: istore_0
    //   66: aload_2
    //   67: iload_0
    //   68: iconst_1
    //   69: invokestatic 220	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   72: astore 5
    //   74: new 222	org/json/JSONObject
    //   77: dup
    //   78: aload 5
    //   80: invokespecial 223	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   83: astore 6
    //   85: ldc 135
    //   87: new 182	java/lang/StringBuilder
    //   90: dup
    //   91: ldc 225
    //   93: invokespecial 186	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   96: aload 6
    //   98: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   101: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   104: invokestatic 197	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   107: aload 6
    //   109: ldc 227
    //   111: aconst_null
    //   112: invokevirtual 231	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   115: astore 8
    //   117: aload 8
    //   119: ifnull +66 -> 185
    //   122: ldc 135
    //   124: new 182	java/lang/StringBuilder
    //   127: dup
    //   128: ldc 233
    //   130: invokespecial 186	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   133: aload 8
    //   135: invokevirtual 236	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   141: invokestatic 239	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   144: getstatic 245	net/flixster/android/data/DaoException$Type:USER_ACCT	Lnet/flixster/android/data/DaoException$Type;
    //   147: aload 8
    //   149: invokestatic 249	net/flixster/android/data/DaoException:create	(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;
    //   152: athrow
    //   153: astore 7
    //   155: aload 7
    //   157: invokestatic 252	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   160: athrow
    //   161: astore_1
    //   162: ldc 2
    //   164: monitorexit
    //   165: aload_1
    //   166: athrow
    //   167: astore 4
    //   169: new 254	java/lang/RuntimeException
    //   172: dup
    //   173: aload 4
    //   175: invokespecial 257	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   178: athrow
    //   179: astore_3
    //   180: aload_3
    //   181: invokestatic 252	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   184: athrow
    //   185: new 133	net/flixster/android/model/User
    //   188: dup
    //   189: invokespecial 164	net/flixster/android/model/User:<init>	()V
    //   192: astore 9
    //   194: aload 9
    //   196: aload 6
    //   198: invokevirtual 261	net/flixster/android/model/User:parseFromJSON	(Lorg/json/JSONObject;)Lnet/flixster/android/model/User;
    //   201: pop
    //   202: aload 9
    //   204: putstatic 16	net/flixster/android/data/ProfileDao:currentUser	Lnet/flixster/android/model/User;
    //   207: getstatic 16	net/flixster/android/data/ProfileDao:currentUser	Lnet/flixster/android/model/User;
    //   210: astore 11
    //   212: ldc 2
    //   214: monitorexit
    //   215: aload 11
    //   217: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   74	117	153	org/json/JSONException
    //   122	153	153	org/json/JSONException
    //   185	207	153	org/json/JSONException
    //   5	42	161	finally
    //   47	64	161	finally
    //   66	74	161	finally
    //   74	117	161	finally
    //   122	153	161	finally
    //   155	161	161	finally
    //   169	179	161	finally
    //   180	185	161	finally
    //   185	207	161	finally
    //   207	212	161	finally
    //   47	64	167	java/net/MalformedURLException
    //   66	74	167	java/net/MalformedURLException
    //   47	64	179	java/io/IOException
    //   66	74	179	java/io/IOException
  }

  public static User fetchUser(String paramString)
    throws DaoException
  {
    Object localObject = (User)usersMap.get(paramString);
    Logger.d("FlxMain", "ProfileDao.fetchUser id:" + paramString + " user:" + localObject);
    if (localObject == null);
    while (true)
    {
      JSONObject localJSONObject;
      try
      {
        String str1 = HttpHelper.fetchUrl(new URL(ApiBuilder.user(paramString)), false, false);
        try
        {
          localJSONObject = new JSONObject(str1);
          String str2 = localJSONObject.optString("error", null);
          if (str2 == null)
            break label147;
          Logger.e("FlxMain", "ProfileDao.getUser error:" + str2);
          throw DaoException.create(DaoException.Type.USER_ACCT, str2);
        }
        catch (JSONException localJSONException1)
        {
        }
        throw DaoException.create(localJSONException1);
      }
      catch (MalformedURLException localMalformedURLException)
      {
        throw new RuntimeException(localMalformedURLException);
      }
      catch (IOException localIOException)
      {
        throw DaoException.create(localIOException);
      }
      label147: User localUser = new User();
      try
      {
        localUser.parseFromJSON(localJSONObject);
        usersMap.put(paramString, localUser);
        localObject = localUser;
        return localObject;
      }
      catch (JSONException localJSONException2)
      {
      }
    }
  }

  public static void fetchUser(Handler paramHandler1, final Handler paramHandler2)
  {
    if ((currentUser != null) && (currentUser.id != null))
    {
      paramHandler1.handleMessage(Message.obtain(null, 0, null));
      return;
    }
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          ProfileDao.fetchUser();
          ProfileDao.this.sendMessage(Message.obtain(null, 0, null));
          return;
        }
        catch (DaoException localDaoException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException));
        }
      }
    });
  }

  public static void getFacebookFriends(final Handler paramHandler1, Handler paramHandler2)
  {
    Logger.d("FlxMain", "ProfileDao.getFacebookFriends");
    if ((currentUser != null) && (currentUser.friendsIds != null))
    {
      paramHandler1.sendEmptyMessage(0);
      return;
    }
    if (currentUser != null)
    {
      WorkerThreads.instance().invokeLater(new Runnable()
      {
        // ERROR //
        public void run()
        {
          // Byte code:
          //   0: new 31	java/net/URL
          //   3: dup
          //   4: invokestatic 35	net/flixster/android/data/ProfileDao:access$0	()Lnet/flixster/android/model/User;
          //   7: getfield 41	net/flixster/android/model/User:id	Ljava/lang/String;
          //   10: invokestatic 47	net/flixster/android/data/ApiBuilder:friendsIds	(Ljava/lang/String;)Ljava/lang/String;
          //   13: invokespecial 50	java/net/URL:<init>	(Ljava/lang/String;)V
          //   16: iconst_0
          //   17: iconst_0
          //   18: invokestatic 56	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
          //   21: astore 4
          //   23: new 58	org/json/JSONArray
          //   26: dup
          //   27: aload 4
          //   29: invokespecial 59	org/json/JSONArray:<init>	(Ljava/lang/String;)V
          //   32: astore 5
          //   34: new 61	java/util/ArrayList
          //   37: dup
          //   38: invokespecial 62	java/util/ArrayList:<init>	()V
          //   41: astore 6
          //   43: iconst_0
          //   44: istore 7
          //   46: iload 7
          //   48: aload 5
          //   50: invokevirtual 66	org/json/JSONArray:length	()I
          //   53: if_icmplt +61 -> 114
          //   56: invokestatic 35	net/flixster/android/data/ProfileDao:access$0	()Lnet/flixster/android/model/User;
          //   59: ifnull +16 -> 75
          //   62: invokestatic 35	net/flixster/android/data/ProfileDao:access$0	()Lnet/flixster/android/model/User;
          //   65: aload 6
          //   67: invokeinterface 72 1 0
          //   72: putfield 75	net/flixster/android/model/User:friendsIds	[Ljava/lang/Object;
          //   75: aload_0
          //   76: getfield 19	net/flixster/android/data/ProfileDao$2:val$successHandler	Landroid/os/Handler;
          //   79: iconst_0
          //   80: invokevirtual 81	android/os/Handler:sendEmptyMessage	(I)Z
          //   83: pop
          //   84: return
          //   85: astore_3
          //   86: new 83	java/lang/RuntimeException
          //   89: dup
          //   90: aload_3
          //   91: invokespecial 86	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
          //   94: athrow
          //   95: astore_1
          //   96: aload_0
          //   97: getfield 17	net/flixster/android/data/ProfileDao$2:val$errorHandler	Landroid/os/Handler;
          //   100: aconst_null
          //   101: iconst_0
          //   102: aload_1
          //   103: invokestatic 92	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
          //   106: invokestatic 98	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
          //   109: invokevirtual 102	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
          //   112: pop
          //   113: return
          //   114: aload 5
          //   116: iload 7
          //   118: invokevirtual 106	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
          //   121: ldc 107
          //   123: aconst_null
          //   124: invokevirtual 113	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
          //   127: astore 10
          //   129: aload 10
          //   131: ifnull +13 -> 144
          //   134: aload 6
          //   136: aload 10
          //   138: invokeinterface 117 2 0
          //   143: pop
          //   144: iinc 7 1
          //   147: goto -101 -> 46
          //   150: astore 8
          //   152: aload_0
          //   153: getfield 17	net/flixster/android/data/ProfileDao$2:val$errorHandler	Landroid/os/Handler;
          //   156: aconst_null
          //   157: iconst_0
          //   158: aload 8
          //   160: invokestatic 92	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
          //   163: invokestatic 98	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
          //   166: invokevirtual 102	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
          //   169: pop
          //   170: return
          //
          // Exception table:
          //   from	to	target	type
          //   0	23	85	java/net/MalformedURLException
          //   0	23	95	java/io/IOException
          //   23	43	150	org/json/JSONException
          //   46	75	150	org/json/JSONException
          //   75	84	150	org/json/JSONException
          //   114	129	150	org/json/JSONException
          //   134	144	150	org/json/JSONException
        }
      });
      return;
    }
    Logger.e("FlxMain", "ProfileDao.getFacebookFriends illegal state user null");
  }

  public static List<Review> getFriendRatedReviews(String paramString)
    throws DaoException
  {
    if ((currentUser != null) && (currentUser.friendRatedReviews != null))
    {
      localObject = currentUser.friendRatedReviews;
      return localObject;
    }
    List localList = getFriendReviews(paramString);
    Object localObject = new ArrayList();
    Iterator localIterator = localList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        if (currentUser == null)
          break;
        currentUser.friendRatedReviews = ((List)localObject);
        return localObject;
      }
      Review localReview = (Review)localIterator.next();
      if ((!localReview.isWantToSee()) && (!localReview.isNotInterested()))
        ((List)localObject).add(localReview);
    }
  }

  public static List<Review> getFriendReviews(String paramString)
    throws DaoException
  {
    Object localObject;
    if ((currentUser != null) && (currentUser.friendReviews != null))
    {
      localObject = currentUser.friendReviews;
      return localObject;
    }
    while (true)
    {
      JSONArray localJSONArray;
      int i;
      try
      {
        while (true)
        {
          String str = HttpHelper.fetchUrl(new URL(ApiBuilder.friendReviews(paramString)), false, false);
          try
          {
            localJSONArray = new JSONArray(str);
            localObject = new ArrayList();
            i = 0;
            if (i >= localJSONArray.length())
            {
              if (currentUser == null)
                break;
              currentUser.friendReviews = ((List)localObject);
              return localObject;
            }
          }
          catch (JSONException localJSONException)
          {
            throw DaoException.create(localJSONException);
          }
        }
      }
      catch (MalformedURLException localMalformedURLException)
      {
        throw new RuntimeException(localMalformedURLException);
      }
      catch (IOException localIOException)
      {
        throw DaoException.create(localIOException);
      }
      JSONObject localJSONObject = localJSONArray.getJSONObject(i);
      if ((localJSONObject.has("type")) && ("rating".equals(localJSONObject.get("type"))))
        ((ArrayList)localObject).add(parseReview(localJSONObject));
      i++;
    }
  }

  public static List<Review> getFriendWantToSeeReviews(String paramString)
    throws DaoException
  {
    if ((currentUser != null) && (currentUser.friendWantToSeeReviews != null))
    {
      localObject = currentUser.friendWantToSeeReviews;
      return localObject;
    }
    List localList = getFriendReviews(paramString);
    Object localObject = new ArrayList();
    Iterator localIterator = localList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        if (currentUser == null)
          break;
        currentUser.friendWantToSeeReviews = ((List)localObject);
        return localObject;
      }
      Review localReview = (Review)localIterator.next();
      if (localReview.isWantToSee())
        ((List)localObject).add(localReview);
    }
  }

  public static List<User> getFriends(String paramString, int paramInt)
    throws DaoException
  {
    Object localObject;
    if ((currentUser != null) && (currentUser.friends != null))
    {
      localObject = currentUser.friends;
      return localObject;
    }
    while (true)
    {
      JSONArray localJSONArray;
      int i;
      try
      {
        while (true)
        {
          String str = HttpHelper.fetchUrl(new URL(ApiBuilder.friends(paramString, paramInt)), false, false);
          try
          {
            localJSONArray = new JSONArray(str);
            localObject = new ArrayList();
            i = 0;
            if (i >= localJSONArray.length())
            {
              if (currentUser == null)
                break;
              currentUser.friends = ((List)localObject);
              return localObject;
            }
          }
          catch (JSONException localJSONException)
          {
            throw DaoException.create(localJSONException);
          }
        }
      }
      catch (MalformedURLException localMalformedURLException)
      {
        throw new RuntimeException(localMalformedURLException);
      }
      catch (IOException localIOException)
      {
        throw DaoException.create(localIOException);
      }
      ((ArrayList)localObject).add(parseUser(localJSONArray.getJSONObject(i)));
      i++;
    }
  }

  public static void getFriends(Handler paramHandler1, final Handler paramHandler2)
  {
    if ((currentUser == null) || (currentUser.id == null))
      return;
    if (currentUser.friends != null)
    {
      paramHandler1.handleMessage(Message.obtain());
      return;
    }
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          List localList = ProfileDao.getFriends(ProfileDao.currentUser.id, -1);
          ProfileDao.currentUser.friends = localList;
          ProfileDao.this.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException));
        }
      }
    });
  }

  public static void getFriendsActivity(Handler paramHandler1, final Handler paramHandler2)
  {
    if ((currentUser == null) || (currentUser.id == null))
      return;
    if (currentUser.friendsActivity != null)
    {
      paramHandler1.handleMessage(Message.obtain());
      return;
    }
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          List localList = ProfileDao.getFriendReviews(ProfileDao.currentUser.id);
          ArrayList localArrayList = new ArrayList();
          Iterator localIterator = localList.iterator();
          while (true)
          {
            if (!localIterator.hasNext())
            {
              if (ProfileDao.currentUser == null)
                break;
              ProfileDao.currentUser.friendsActivity = localArrayList;
              ProfileDao.this.sendEmptyMessage(0);
              return;
            }
            Review localReview = (Review)localIterator.next();
            if (!localReview.isNotInterested())
              localArrayList.add(localReview);
          }
        }
        catch (DaoException localDaoException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException));
        }
      }
    });
  }

  public static void getQuickRateMovies(final Handler paramHandler1, final Handler paramHandler2, boolean paramBoolean)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        ArrayList localArrayList = new ArrayList();
        String str;
        try
        {
          str = HttpHelper.fetchUrl(new URL(ApiBuilder.quickRate(this.val$isWts)), false, false);
          if (DaoException.hasError(str))
          {
            paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(str)));
            return;
          }
        }
        catch (MalformedURLException localMalformedURLException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, new RuntimeException(localMalformedURLException)));
          return;
        }
        catch (IOException localIOException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(localIOException)));
          return;
        }
        try
        {
          JSONArray localJSONArray = new JSONArray(str);
          for (int i = 0; ; i++)
          {
            int j = localJSONArray.length();
            if (i >= j)
            {
              paramHandler1.sendMessage(Message.obtain(null, 0, Collections.unmodifiableList(localArrayList)));
              return;
            }
            JSONObject localJSONObject = localJSONArray.getJSONObject(i);
            Movie localMovie = MovieDao.getMovie(localJSONObject.optLong("id"));
            localMovie.parseFromJSON(localJSONObject);
            localArrayList.add(localMovie);
          }
        }
        catch (JSONException localJSONException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(localJSONException)));
        }
      }
    });
  }

  public static User getUser()
  {
    return currentUser;
  }

  public static void getUserLockerRights(final Handler paramHandler1, Handler paramHandler2)
  {
    if ((currentUser == null) || (currentUser.id == null))
      return;
    if (currentUser.isLockerRightsFetched())
    {
      paramHandler1.handleMessage(Message.obtain());
      return;
    }
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        ArrayList localArrayList = new ArrayList();
        String str;
        do
          try
          {
            URL localURL = new URL(ApiBuilder.userRights(ProfileDao.currentUser.id, null));
            if (FlixsterApplication.isConnected());
            for (boolean bool = false; ; bool = true)
            {
              str = HttpHelper.fetchUrl(localURL, bool, true);
              if (!DaoException.hasError(str))
                break;
              ProfileDao.this.sendMessage(Message.obtain(null, 0, DaoException.create(str)));
              return;
            }
          }
          catch (MalformedURLException localMalformedURLException)
          {
            throw new RuntimeException(localMalformedURLException);
          }
          catch (IOException localIOException)
          {
            ProfileDao.this.sendMessage(Message.obtain(null, 0, DaoException.create(localIOException)));
            return;
          }
        while ((ProfileDao.currentUser == null) || (ProfileDao.currentUser.isLockerRightsFetched()));
        while (true)
        {
          int i;
          Iterator localIterator;
          JSONObject localJSONObject1;
          try
          {
            JSONArray localJSONArray = new JSONArray(str);
            i = 0;
            int j = localJSONArray.length();
            if (i >= j)
            {
              if ((!FlixsterApplication.hasMigratedToRights()) && (FlixsterApplication.isConnected()))
              {
                FlixsterApplication.setMigratedToRights(true);
                Logger.i("FlxMain", "ProfileDao.getUserLockerRights migration for rights api");
                localIterator = localArrayList.iterator();
                if (localIterator.hasNext())
                  break label496;
                MigrationHelper.migrateDownloadedFilesForRightsApi(ProfileDao.currentUser);
                Drm.manager().registerAllLocalAssets(ProfileDao.currentUser.getLockerRights());
              }
              paramHandler1.sendEmptyMessage(0);
              return;
            }
            localJSONObject1 = localJSONArray.getJSONObject(i);
            JSONObject localJSONObject2 = localJSONObject1.optJSONObject("movie");
            if (localJSONObject2 != null)
            {
              long l1 = localJSONObject2.optLong("id");
              localObject = MovieDao.getMovie(l1);
              ((Movie)localObject).parseFromJSON(localJSONObject2);
              LockerRight localLockerRight1 = new LockerRight(localJSONObject1, l1, LockerRight.RightType.MOVIE, (VideoAsset)localObject);
              ProfileDao.currentUser.addLockerRight(localLockerRight1);
              if (localObject == null)
                break label531;
              Drm.logic().setDeviceStreamBlacklisted(localJSONObject1.optBoolean("isStreamingUnsupported"));
              Drm.logic().setDeviceNonWifiStreamBlacklisted(localJSONObject1.optBoolean("isNonWifiStreamingUnsupported"));
              localArrayList.add(localObject);
              break label531;
            }
            JSONObject localJSONObject3 = localJSONObject1.optJSONObject("season");
            if (localJSONObject3 != null)
            {
              long l2 = localJSONObject3.optLong("id");
              localObject = MovieDao.getSeason(l2);
              ((Movie)localObject).parseFromJSON(localJSONObject3);
              LockerRight localLockerRight2 = new LockerRight(localJSONObject1, l2, LockerRight.RightType.SEASON, (VideoAsset)localObject);
              ProfileDao.currentUser.addLockerRight(localLockerRight2);
              continue;
            }
          }
          catch (JSONException localJSONException)
          {
            ProfileDao.this.sendMessage(Message.obtain(null, 0, DaoException.create(localJSONException)));
            return;
          }
          JSONObject localJSONObject4 = localJSONObject1.optJSONObject("unfulfillable");
          Object localObject = null;
          if (localJSONObject4 != null)
          {
            long l3 = localJSONObject4.optLong("id");
            localObject = MovieDao.getUnfulfillable(l3);
            ((Movie)localObject).parseFromJSON(localJSONObject4);
            LockerRight localLockerRight3 = new LockerRight(localJSONObject1, l3, LockerRight.RightType.UNFULFILLABLE, (VideoAsset)localObject);
            ProfileDao.currentUser.addLockerRight(localLockerRight3);
            continue;
            label496: Movie localMovie = (Movie)localIterator.next();
            if (Season.class.isInstance(localMovie))
            {
              MovieDao.getUserSeasonDetailBg(localMovie.getId(), null, null);
              continue;
              label531: i++;
            }
          }
        }
      }
    });
  }

  // ERROR //
  public static Review getUserMovieReview(String paramString1, String paramString2)
    throws DaoException
  {
    // Byte code:
    //   0: new 203	java/net/URL
    //   3: dup
    //   4: aload_0
    //   5: aload_1
    //   6: invokestatic 403	net/flixster/android/data/ApiBuilder:userMovieReview	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   9: invokespecial 209	java/net/URL:<init>	(Ljava/lang/String;)V
    //   12: iconst_0
    //   13: iconst_0
    //   14: invokestatic 220	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   17: astore 4
    //   19: new 222	org/json/JSONObject
    //   22: dup
    //   23: aload 4
    //   25: invokespecial 223	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   28: invokestatic 357	net/flixster/android/data/ProfileDao:parseReview	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Review;
    //   31: astore 6
    //   33: aload 6
    //   35: areturn
    //   36: astore_3
    //   37: new 254	java/lang/RuntimeException
    //   40: dup
    //   41: aload_3
    //   42: invokespecial 257	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   45: athrow
    //   46: astore_2
    //   47: aload_2
    //   48: invokestatic 252	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   51: athrow
    //   52: astore 5
    //   54: aload 5
    //   56: invokestatic 252	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   59: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   0	19	36	java/net/MalformedURLException
    //   0	19	46	java/io/IOException
    //   19	33	52	org/json/JSONException
  }

  public static void getUserMovieReview(final Handler paramHandler1, final Handler paramHandler2, String paramString)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          Review localReview = ProfileDao.getUserMovieReview(FlixsterApplication.getFlixsterId(), ProfileDao.this);
          paramHandler1.sendMessage(Message.obtain(null, 0, localReview));
          return;
        }
        catch (DaoException localDaoException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException));
        }
      }
    });
  }

  public static List<Review> getUserRatedReviews(String paramString, int paramInt)
    throws DaoException
  {
    User localUser = (User)usersMap.get(paramString);
    if (localUser == null)
      if ((currentUser == null) || (!paramString.equals(currentUser.id)))
        break label50;
    label50: for (localUser = currentUser; localUser.ratedReviews != null; localUser = new User())
      return localUser.ratedReviews;
    List localList = getUserReviews(paramString, paramInt);
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = localList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        localUser.ratedReviews = localArrayList;
        return localArrayList;
      }
      Review localReview = (Review)localIterator.next();
      if ((!localReview.isWantToSee()) && (!localReview.isNotInterested()))
        localArrayList.add(localReview);
    }
  }

  public static void getUserRatedReviews(Handler paramHandler1, final Handler paramHandler2)
  {
    if ((currentUser == null) || (currentUser.id == null))
      return;
    if (currentUser.ratedReviews != null)
    {
      paramHandler1.handleMessage(Message.obtain());
      return;
    }
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          List localList = ProfileDao.getUserRatedReviews(ProfileDao.currentUser.id, 50);
          if (ProfileDao.currentUser != null)
            ProfileDao.currentUser.ratedReviews = localList;
          ProfileDao.this.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 2, localDaoException));
        }
      }
    });
  }

  // ERROR //
  public static List<Review> getUserReviews(String paramString, int paramInt)
    throws DaoException
  {
    // Byte code:
    //   0: getstatic 23	net/flixster/android/data/ProfileDao:usersMap	Ljava/util/HashMap;
    //   3: aload_0
    //   4: invokevirtual 265	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   7: checkcast 133	net/flixster/android/model/User
    //   10: astore_2
    //   11: aload_2
    //   12: ifnonnull +26 -> 38
    //   15: getstatic 16	net/flixster/android/data/ProfileDao:currentUser	Lnet/flixster/android/model/User;
    //   18: ifnull +81 -> 99
    //   21: aload_0
    //   22: getstatic 16	net/flixster/android/data/ProfileDao:currentUser	Lnet/flixster/android/model/User;
    //   25: getfield 201	net/flixster/android/model/User:id	Ljava/lang/String;
    //   28: invokevirtual 74	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   31: ifeq +68 -> 99
    //   34: getstatic 16	net/flixster/android/data/ProfileDao:currentUser	Lnet/flixster/android/model/User;
    //   37: astore_2
    //   38: new 203	java/net/URL
    //   41: dup
    //   42: aload_0
    //   43: iload_1
    //   44: invokestatic 421	net/flixster/android/data/ApiBuilder:userReviews	(Ljava/lang/String;I)Ljava/lang/String;
    //   47: invokespecial 209	java/net/URL:<init>	(Ljava/lang/String;)V
    //   50: iconst_0
    //   51: iconst_0
    //   52: invokestatic 220	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   55: astore 5
    //   57: new 334	org/json/JSONArray
    //   60: dup
    //   61: aload 5
    //   63: invokespecial 335	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   66: astore 6
    //   68: new 314	java/util/ArrayList
    //   71: dup
    //   72: invokespecial 315	java/util/ArrayList:<init>	()V
    //   75: astore 7
    //   77: iconst_0
    //   78: istore 8
    //   80: iload 8
    //   82: aload 6
    //   84: invokevirtual 338	org/json/JSONArray:length	()I
    //   87: if_icmplt +41 -> 128
    //   90: aload_2
    //   91: aload 7
    //   93: putfield 424	net/flixster/android/model/User:reviews	Ljava/util/List;
    //   96: aload 7
    //   98: areturn
    //   99: new 133	net/flixster/android/model/User
    //   102: dup
    //   103: invokespecial 164	net/flixster/android/model/User:<init>	()V
    //   106: astore_2
    //   107: goto -69 -> 38
    //   110: astore 4
    //   112: new 254	java/lang/RuntimeException
    //   115: dup
    //   116: aload 4
    //   118: invokespecial 257	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   121: athrow
    //   122: astore_3
    //   123: aload_3
    //   124: invokestatic 252	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   127: athrow
    //   128: aload 7
    //   130: aload 6
    //   132: iload 8
    //   134: invokevirtual 342	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   137: invokestatic 357	net/flixster/android/data/ProfileDao:parseReview	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Review;
    //   140: invokevirtual 358	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   143: pop
    //   144: iinc 8 1
    //   147: goto -67 -> 80
    //   150: astore 9
    //   152: aload 9
    //   154: invokestatic 252	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   157: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   38	57	110	java/net/MalformedURLException
    //   38	57	122	java/io/IOException
    //   57	77	150	org/json/JSONException
    //   80	96	150	org/json/JSONException
    //   128	144	150	org/json/JSONException
  }

  public static List<Review> getWantToSeeReviews(String paramString, int paramInt)
    throws DaoException
  {
    User localUser = (User)usersMap.get(paramString);
    if (localUser == null)
      if ((currentUser == null) || (!paramString.equals(currentUser.id)))
        break label50;
    label50: for (localUser = currentUser; localUser.wantToSeeReviews != null; localUser = new User())
      return localUser.wantToSeeReviews;
    List localList = getUserReviews(paramString, paramInt);
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = localList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        localUser.wantToSeeReviews = localArrayList;
        return localArrayList;
      }
      Review localReview = (Review)localIterator.next();
      if (localReview.isWantToSee())
        localArrayList.add(localReview);
    }
  }

  public static void getWantToSeeReviews(Handler paramHandler1, final Handler paramHandler2)
  {
    if ((currentUser == null) || (currentUser.id == null))
      return;
    if (currentUser.wantToSeeReviews != null)
    {
      paramHandler1.handleMessage(Message.obtain());
      return;
    }
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          List localList = ProfileDao.getWantToSeeReviews(ProfileDao.currentUser.id, 50);
          if (ProfileDao.currentUser != null)
            ProfileDao.currentUser.wantToSeeReviews = localList;
          ProfileDao.this.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 1, localDaoException));
        }
      }
    });
  }

  public static void insertLockerRight(Handler paramHandler1, Handler paramHandler2, String paramString)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      // ERROR //
      public void run()
      {
        // Byte code:
        //   0: invokestatic 29	net/flixster/android/data/ProfileDao:access$0	()Lnet/flixster/android/model/User;
        //   3: getfield 34	net/flixster/android/model/User:id	Ljava/lang/String;
        //   6: aload_0
        //   7: getfield 17	net/flixster/android/data/ProfileDao$7:val$purchaseType	Ljava/lang/String;
        //   10: invokestatic 40	net/flixster/android/data/ApiBuilder:userRights	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   13: astore_1
        //   14: new 42	java/util/ArrayList
        //   17: dup
        //   18: invokespecial 43	java/util/ArrayList:<init>	()V
        //   21: astore_2
        //   22: aload_1
        //   23: aload_2
        //   24: invokestatic 49	net/flixster/android/util/HttpHelper:postToUrl	(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
        //   27: astore 4
        //   29: new 51	org/json/JSONArray
        //   32: dup
        //   33: aload 4
        //   35: invokespecial 53	org/json/JSONArray:<init>	(Ljava/lang/String;)V
        //   38: astore 5
        //   40: iconst_0
        //   41: istore 6
        //   43: aload 5
        //   45: invokevirtual 57	org/json/JSONArray:length	()I
        //   48: istore 8
        //   50: iload 6
        //   52: iload 8
        //   54: if_icmplt +14 -> 68
        //   57: return
        //   58: astore_3
        //   59: aconst_null
        //   60: aload_0
        //   61: getfield 17	net/flixster/android/data/ProfileDao$7:val$purchaseType	Ljava/lang/String;
        //   64: invokestatic 61	net/flixster/android/data/ProfileDao:access$1	(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
        //   67: return
        //   68: aload 5
        //   70: iload 6
        //   72: invokevirtual 65	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
        //   75: astore 9
        //   77: aload 9
        //   79: ldc 67
        //   81: invokevirtual 73	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
        //   84: astore 10
        //   86: aload 10
        //   88: ifnull +69 -> 157
        //   91: aload 10
        //   93: ldc 74
        //   95: invokevirtual 78	org/json/JSONObject:optLong	(Ljava/lang/String;)J
        //   98: lstore 11
        //   100: lload 11
        //   102: invokestatic 84	net/flixster/android/data/MovieDao:getMovie	(J)Lnet/flixster/android/model/Movie;
        //   105: astore 13
        //   107: aload 13
        //   109: aload 10
        //   111: invokevirtual 90	net/flixster/android/model/Movie:parseFromJSON	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;
        //   114: pop
        //   115: new 92	net/flixster/android/model/LockerRight
        //   118: dup
        //   119: aload 9
        //   121: lload 11
        //   123: getstatic 98	net/flixster/android/model/LockerRight$RightType:MOVIE	Lnet/flixster/android/model/LockerRight$RightType;
        //   126: aload 13
        //   128: invokespecial 101	net/flixster/android/model/LockerRight:<init>	(Lorg/json/JSONObject;JLnet/flixster/android/model/LockerRight$RightType;Lnet/flixster/android/model/VideoAsset;)V
        //   131: astore 15
        //   133: invokestatic 29	net/flixster/android/data/ProfileDao:access$0	()Lnet/flixster/android/model/User;
        //   136: aload 15
        //   138: invokevirtual 105	net/flixster/android/model/User:addLockerRight	(Lnet/flixster/android/model/LockerRight;)V
        //   141: invokestatic 29	net/flixster/android/data/ProfileDao:access$0	()Lnet/flixster/android/model/User;
        //   144: iconst_1
        //   145: putfield 109	net/flixster/android/model/User:refreshRequired	Z
        //   148: aload 15
        //   150: aload_0
        //   151: getfield 17	net/flixster/android/data/ProfileDao$7:val$purchaseType	Ljava/lang/String;
        //   154: invokestatic 61	net/flixster/android/data/ProfileDao:access$1	(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
        //   157: iinc 6 1
        //   160: goto -117 -> 43
        //   163: astore 7
        //   165: aconst_null
        //   166: aload_0
        //   167: getfield 17	net/flixster/android/data/ProfileDao$7:val$purchaseType	Ljava/lang/String;
        //   170: invokestatic 61	net/flixster/android/data/ProfileDao:access$1	(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
        //   173: return
        //
        // Exception table:
        //   from	to	target	type
        //   22	29	58	java/io/IOException
        //   29	40	163	org/json/JSONException
        //   43	50	163	org/json/JSONException
        //   68	86	163	org/json/JSONException
        //   91	157	163	org/json/JSONException
      }
    });
  }

  // ERROR //
  public static User loginUser(String paramString1, String paramString2, String paramString3)
    throws DaoException
  {
    // Byte code:
    //   0: ldc 135
    //   2: ldc_w 439
    //   5: invokestatic 197	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   8: ldc_w 441
    //   11: aload_2
    //   12: invokevirtual 74	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   15: istore_3
    //   16: aconst_null
    //   17: astore 4
    //   19: iload_3
    //   20: ifeq +241 -> 261
    //   23: invokestatic 444	net/flixster/android/data/ApiBuilder:tokens	()Ljava/lang/String;
    //   26: astore 5
    //   28: new 314	java/util/ArrayList
    //   31: dup
    //   32: invokespecial 315	java/util/ArrayList:<init>	()V
    //   35: astore 6
    //   37: aload 6
    //   39: new 446	org/apache/http/message/BasicNameValuePair
    //   42: dup
    //   43: ldc_w 448
    //   46: aload_0
    //   47: invokespecial 450	org/apache/http/message/BasicNameValuePair:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   50: invokevirtual 358	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   53: pop
    //   54: aload 6
    //   56: new 446	org/apache/http/message/BasicNameValuePair
    //   59: dup
    //   60: ldc_w 452
    //   63: aload_1
    //   64: invokespecial 450	org/apache/http/message/BasicNameValuePair:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   67: invokevirtual 358	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   70: pop
    //   71: aload 5
    //   73: aload 6
    //   75: invokestatic 456	net/flixster/android/util/HttpHelper:postToUrl	(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    //   78: astore 11
    //   80: new 222	org/json/JSONObject
    //   83: dup
    //   84: aload 11
    //   86: invokevirtual 459	java/lang/String:trim	()Ljava/lang/String;
    //   89: invokespecial 223	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   92: astore 12
    //   94: aload 12
    //   96: ldc 227
    //   98: invokevirtual 348	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   101: ifeq +91 -> 192
    //   104: aload 12
    //   106: ldc 227
    //   108: invokevirtual 461	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   111: invokestatic 466	net/flixster/android/FlixUtils:copyString	(Ljava/lang/String;)Ljava/lang/String;
    //   114: astore 14
    //   116: aconst_null
    //   117: astore 4
    //   119: aload 14
    //   121: ifnull +140 -> 261
    //   124: aload 14
    //   126: invokevirtual 467	java/lang/String:length	()I
    //   129: istore 15
    //   131: aconst_null
    //   132: astore 4
    //   134: iload 15
    //   136: ifle +125 -> 261
    //   139: getstatic 245	net/flixster/android/data/DaoException$Type:USER_ACCT	Lnet/flixster/android/data/DaoException$Type;
    //   142: aload 14
    //   144: invokestatic 249	net/flixster/android/data/DaoException:create	(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;
    //   147: athrow
    //   148: astore 9
    //   150: aload 9
    //   152: invokestatic 252	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   155: astore 10
    //   157: ldc 135
    //   159: ldc_w 469
    //   162: aload 10
    //   164: invokestatic 471	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   167: aload 10
    //   169: athrow
    //   170: astore 16
    //   172: aload 16
    //   174: invokestatic 252	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   177: astore 17
    //   179: ldc 135
    //   181: ldc_w 469
    //   184: aload 17
    //   186: invokestatic 471	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   189: aload 17
    //   191: athrow
    //   192: aload 12
    //   194: ldc_w 473
    //   197: invokevirtual 348	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   200: ifeq +64 -> 264
    //   203: invokestatic 476	net/flixster/android/data/ProfileDao:resetUser	()V
    //   206: new 133	net/flixster/android/model/User
    //   209: dup
    //   210: invokespecial 164	net/flixster/android/model/User:<init>	()V
    //   213: astore 4
    //   215: aload 4
    //   217: aload 12
    //   219: invokevirtual 261	net/flixster/android/model/User:parseFromJSON	(Lorg/json/JSONObject;)Lnet/flixster/android/model/User;
    //   222: pop
    //   223: aload 4
    //   225: ldc_w 441
    //   228: putfield 479	net/flixster/android/model/User:platform	Ljava/lang/String;
    //   231: ldc_w 441
    //   234: invokestatic 482	net/flixster/android/FlixsterApplication:setPlatform	(Ljava/lang/String;)V
    //   237: aload 4
    //   239: getfield 485	net/flixster/android/model/User:userName	Ljava/lang/String;
    //   242: invokestatic 488	net/flixster/android/FlixsterApplication:setFlixsterUsername	(Ljava/lang/String;)V
    //   245: aload 4
    //   247: getfield 490	net/flixster/android/model/User:token	Ljava/lang/String;
    //   250: invokestatic 493	net/flixster/android/FlixsterApplication:setFlixsterSessionKey	(Ljava/lang/String;)V
    //   253: aload 4
    //   255: getfield 201	net/flixster/android/model/User:id	Ljava/lang/String;
    //   258: invokestatic 496	net/flixster/android/FlixsterApplication:setFlixsterId	(Ljava/lang/String;)V
    //   261: aload 4
    //   263: areturn
    //   264: getstatic 245	net/flixster/android/data/DaoException$Type:USER_ACCT	Lnet/flixster/android/data/DaoException$Type;
    //   267: ldc_w 498
    //   270: invokestatic 249	net/flixster/android/data/DaoException:create	(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;
    //   273: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   71	80	148	java/io/IOException
    //   80	94	170	org/json/JSONException
  }

  private static Review parseReview(JSONObject paramJSONObject)
    throws JSONException
  {
    Review localReview = new Review();
    localReview.type = 1;
    if (paramJSONObject.has("user"))
    {
      JSONObject localJSONObject8 = paramJSONObject.getJSONObject("user");
      localReview.userId = localJSONObject8.getLong("id");
      localReview.userName = FlixUtils.copyString(localJSONObject8.getString("userName"));
      localReview.name = FlixUtils.copyString(localJSONObject8.getString("firstName") + " " + localJSONObject8.getString("lastName"));
      if (localJSONObject8.has("images"))
      {
        JSONObject localJSONObject9 = localJSONObject8.getJSONObject("images");
        if (localJSONObject9.has("thumbnail"))
          localReview.mugUrl = FlixUtils.copyString(localJSONObject9.getString("thumbnail"));
      }
    }
    JSONObject localJSONObject1;
    Movie localMovie;
    Date localDate2;
    if (paramJSONObject.has("movie"))
    {
      localJSONObject1 = paramJSONObject.getJSONObject("movie");
      localMovie = MovieDao.getMovie(localJSONObject1.optLong("id"));
      localMovie.hintTitle(FlixUtils.copyString(localJSONObject1.optString("title")));
      localMovie.setProperty("title", FlixUtils.copyString(localJSONObject1.optString("title")));
      if (localJSONObject1.has("mpaa"))
        localMovie.setProperty("mpaa", FlixUtils.copyString(localJSONObject1.optString("mpaa")));
      if (localJSONObject1.has("status"))
        localMovie.setProperty("status", FlixUtils.copyString(localJSONObject1.optString("status")));
      if (localJSONObject1.has("runningTime"))
        localMovie.setProperty("runningTime", FlixUtils.copyString(localJSONObject1.optString("runningTime")));
      if ((localJSONObject1.has("mpaa")) || (localJSONObject1.has("runningTime")))
        localMovie.buildMeta();
      if (localJSONObject1.has("theaterReleaseDate"))
      {
        JSONObject localJSONObject7 = localJSONObject1.getJSONObject("theaterReleaseDate");
        int i1 = localJSONObject7.optInt("day", -1);
        if (i1 > 0)
        {
          int i2 = localJSONObject7.optInt("year", -1);
          int i3 = localJSONObject7.optInt("month", -1);
          GregorianCalendar localGregorianCalendar2 = new GregorianCalendar(i2, i3 - 1, i1);
          localDate2 = localGregorianCalendar2.getTime();
        }
      }
    }
    try
    {
      localMovie.setProperty("theaterReleaseDate", DateTimeHelper.mediumDateFormatter().format(localDate2));
      if (localJSONObject1.has("dvdReleaseDate"))
      {
        JSONObject localJSONObject6 = localJSONObject1.getJSONObject("dvdReleaseDate");
        int k = localJSONObject6.optInt("day", -1);
        if (k > 0)
        {
          int m = localJSONObject6.optInt("year", -1);
          int n = localJSONObject6.optInt("month", -1);
          GregorianCalendar localGregorianCalendar1 = new GregorianCalendar(m, n - 1, k);
          localDate1 = localGregorianCalendar1.getTime();
        }
      }
    }
    catch (Exception localException2)
    {
      try
      {
        localMovie.setProperty("dvdReleaseDate", DateTimeHelper.mediumDateFormatter().format(localDate1));
        if (localJSONObject1.has("poster"))
        {
          JSONObject localJSONObject5 = localJSONObject1.getJSONObject("poster");
          localMovie.setProperty("thumbnail", localJSONObject5.optString("thumbnail"));
          localMovie.setProperty("profile", localJSONObject5.optString("profile"));
          localMovie.setProperty("detailed", localJSONObject5.optString("detailed"));
        }
        if (localJSONObject1.has("actors"))
        {
          localJSONArray = localJSONObject1.getJSONArray("actors");
          i = localJSONArray.length();
          localStringBuilder1 = new StringBuilder();
          localStringBuilder2 = new StringBuilder();
          j = 0;
          if (j >= i)
          {
            localMovie.setProperty("MOVIE_ACTORS", localStringBuilder1.toString());
            localMovie.setProperty("MOVIE_ACTORS_SHORT", localStringBuilder2.toString());
          }
        }
        else
        {
          if (localJSONObject1.has("reviews"))
          {
            JSONObject localJSONObject3 = localJSONObject1.getJSONObject("reviews");
            if (localJSONObject3.has("flixster"))
            {
              localJSONObject4 = localJSONObject3.getJSONObject("flixster");
              if (!localJSONObject4.has("numLiked"))
                break label1046;
              localMovie.setIntProperty("popcornScore", Integer.valueOf(localJSONObject4.optInt("numLiked", -1)));
              localMovie.setIntProperty("numWantToSee", Integer.valueOf(localJSONObject4.optInt("numWantToSee", -1)));
            }
            if (localJSONObject3.has("rottenTomatoes"))
              localMovie.setIntProperty("rottenTomatoes", Integer.valueOf(localJSONObject3.getJSONObject("rottenTomatoes").optInt("rating", -1)));
          }
          if (localJSONObject1.has("trailer"))
          {
            JSONObject localJSONObject2 = localJSONObject1.getJSONObject("trailer");
            if (localJSONObject2.has("high"))
              localMovie.setProperty("high", localJSONObject2.optString("high"));
          }
          localReview.setMovie(localMovie);
          if (paramJSONObject.has("review"))
            localReview.comment = FlixUtils.copyString(paramJSONObject.getString("review"));
          if (paramJSONObject.has("score"))
          {
            str1 = FlixUtils.copyString(paramJSONObject.getString("score"));
            if (!str1.contentEquals("+"))
              break label1069;
            localReview.stars = 5.5D;
          }
          return localReview;
          localException2 = localException2;
          Logger.e("problem parsing movie release date " + localDate2, localException2.getMessage());
        }
      }
      catch (Exception localException1)
      {
        String str1;
        while (true)
        {
          Date localDate1;
          JSONArray localJSONArray;
          int i;
          StringBuilder localStringBuilder1;
          StringBuilder localStringBuilder2;
          int j;
          JSONObject localJSONObject4;
          Logger.e("problem parsing dvd release date " + localDate1, localException1.getMessage());
          continue;
          String str2 = localJSONArray.getJSONObject(j).getString("name");
          localStringBuilder1.append(str2);
          if (j < 2)
          {
            if (j == 1)
              localStringBuilder2.append(", ");
            localStringBuilder2.append(str2);
          }
          if (j < i - 1)
            localStringBuilder1.append(", ");
          j++;
          continue;
          label1046: localMovie.setIntProperty("popcornScore", Integer.valueOf(localJSONObject4.optInt("popcornScore", -1)));
        }
        label1069: if (str1.contentEquals("-"))
        {
          localReview.stars = 6.0D;
          return localReview;
        }
        if (str1.length() > 0)
        {
          localReview.stars = Double.valueOf(str1).doubleValue();
          return localReview;
        }
        localReview.stars = 0.0D;
      }
    }
    return localReview;
  }

  private static User parseUser(JSONObject paramJSONObject)
    throws JSONException
  {
    User localUser = new User();
    localUser.id = String.valueOf(paramJSONObject.getLong("id"));
    localUser.userName = FlixUtils.copyString(paramJSONObject.getString("userName"));
    localUser.displayName = FlixUtils.copyString(paramJSONObject.getString("displayName"));
    localUser.firstName = FlixUtils.copyString(paramJSONObject.getString("firstName"));
    localUser.lastName = FlixUtils.copyString(paramJSONObject.getString("lastName"));
    if (paramJSONObject.has("images"))
    {
      JSONObject localJSONObject2 = paramJSONObject.getJSONObject("images");
      localUser.thumbnailImage = FlixUtils.copyString(localJSONObject2.getString("thumbnail"));
      localUser.smallImage = FlixUtils.copyString(localJSONObject2.getString("small"));
      localUser.profileImage = FlixUtils.copyString(localJSONObject2.getString("profile"));
    }
    if (paramJSONObject.has("stats"))
    {
      JSONObject localJSONObject1 = paramJSONObject.getJSONObject("stats");
      localUser.wtsCount = Integer.valueOf(localJSONObject1.getString("wts")).intValue();
      localUser.ratingCount = Integer.valueOf(localJSONObject1.getString("ratings")).intValue();
      localUser.reviewCount = Integer.valueOf(localJSONObject1.getString("reviews")).intValue();
      localUser.ratedCount = (localUser.ratingCount + localUser.wtsCount);
    }
    return localUser;
  }

  public static JSONObject postUserMovieReview(String paramString1, String paramString2, Review paramReview, boolean paramBoolean)
    throws Exception
  {
    String str1;
    if (paramReview.stars == 5.5D)
      str1 = "+";
    while (true)
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(new BasicNameValuePair("score", str1));
      localArrayList.add(new BasicNameValuePair("review", paramReview.comment));
      if (paramBoolean)
        localArrayList.add(new BasicNameValuePair("newsfeed", "true"));
      FlixsterApplication.addCurrentUserPairs(localArrayList);
      localArrayList.add(new BasicNameValuePair("ver", Integer.toString(FlixsterApplication.getVersionCode())));
      String str2 = HttpHelper.postToUrl(ApiBuilder.userMovieReviewShort(paramString1, paramString2), localArrayList).trim();
      try
      {
        JSONObject localJSONObject = new JSONObject(str2);
        return localJSONObject;
        if (paramReview.stars == 6.0D)
          str1 = "-";
        else
          str1 = Double.toString(paramReview.stars);
      }
      catch (JSONException localJSONException)
      {
        Logger.w("FlxMain", "problem parsing json response " + str2, localJSONException);
      }
    }
    return new JSONObject();
  }

  public static void postUserMovieReview(final Handler paramHandler1, final Handler paramHandler2, String paramString1, final String paramString2, final Review paramReview)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          ProfileDao.postUserMovieReview(ProfileDao.this, paramString2, paramReview, false);
          ProfileDao.currentUser.ratedReviews = null;
          ProfileDao.currentUser.wantToSeeReviews = null;
          paramHandler1.sendEmptyMessage(0);
          return;
        }
        catch (Exception localException)
        {
          while (true)
            paramHandler2.sendMessage(Message.obtain(null, 0, localException));
        }
      }
    });
  }

  public static void resetUser()
  {
    currentUser = null;
  }

  public static void streamStart(final Handler paramHandler1, final Handler paramHandler2, LockerRight paramLockerRight, final String paramString)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        String[] arrayOfString = FlixsterApplication.getStreamStopError();
        long l;
        String str6;
        if ((arrayOfString != null) && (arrayOfString.length == 2))
        {
          l = Long.valueOf(arrayOfString[0]).longValue();
          str6 = arrayOfString[1];
        }
        String str2;
        try
        {
          Logger.d("FlxDrm", "Previous streamStop error detected: rightId=" + l + " streamId=" + str6);
          HttpHelper.deleteFromUrl(ApiBuilder.rightStream(l, "stop", str6, null, false));
          FlixsterApplication.setStreamStopError(null, null);
          Logger.d("FlxDrm", "Previous streamStop error corrected");
          str1 = ApiBuilder.rightStream(ProfileDao.this.rightId, null, null, null, ProfileDao.this.isSonicProxyMode());
          localArrayList = new ArrayList();
        }
        catch (Exception localException)
        {
          try
          {
            String str1;
            ArrayList localArrayList;
            str2 = HttpHelper.postToUrl(str1, localArrayList);
            if (DaoException.hasError(str2))
            {
              paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(str2)));
              return;
              localException = localException;
              Logger.w("FlxDrm", "Previous streamStop error correction failed", localException);
            }
          }
          catch (IOException localIOException)
          {
            paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(localIOException)));
            return;
          }
        }
        while (true)
        {
          String str3;
          try
          {
            JSONObject localJSONObject1 = new JSONObject(str2);
            JSONObject localJSONObject2 = localJSONObject1.optJSONObject("assets");
            if (paramString.equals("adaptive"))
            {
              str3 = "streamingAdaptive";
              JSONObject localJSONObject3 = localJSONObject2.optJSONObject(str3);
              HashMap localHashMap = new HashMap();
              if (localJSONObject3 != null)
              {
                String str4 = localJSONObject3.optString("fileLocation");
                if (str4.contains("?"))
                {
                  int i = str4.indexOf("?");
                  str4 = str4.substring(0, i);
                }
                localHashMap.put("fileLocation", str4);
                localHashMap.put("closedCaptionFileLocation", localJSONObject3.optString("closedCaptionFileLocation"));
                if (ProfileDao.this.isSonicProxyMode())
                {
                  String str5 = localJSONObject3.optString("streamId");
                  FlixsterApplication.setStreamStopError(Long.valueOf(ProfileDao.this.rightId), str5);
                  localHashMap.put("streamId", str5);
                  localHashMap.put("drmServerUrl", localJSONObject3.optString("drmServerUrl"));
                  localHashMap.put("deviceId", localJSONObject3.optString("destinationUniqueId") + "|" + localJSONObject3.optInt("deviceTypeId"));
                  localHashMap.put("customData", localJSONObject3.optString("playbackParams"));
                }
              }
              localHashMap.put("playPosition", Integer.valueOf(localJSONObject1.optJSONObject("lockerRightParams").optInt("playPosition")));
              paramHandler1.sendMessage(Message.obtain(null, 0, localHashMap));
              return;
            }
          }
          catch (JSONException localJSONException)
          {
            paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(localJSONException)));
            return;
          }
          if (paramString.equals("low"))
            str3 = "streamingFixedLow";
          else
            str3 = "streamingFixedHigh";
        }
      }
    });
  }

  public static void streamStop(final Handler paramHandler1, final Handler paramHandler2, LockerRight paramLockerRight, final String paramString1, final String paramString2)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        String str = ApiBuilder.rightStream(ProfileDao.this.rightId, "stop", paramString1, paramString2, ProfileDao.this.isSonicProxyMode());
        try
        {
          HttpHelper.deleteFromUrl(str);
          FlixsterApplication.setStreamStopError(null, null);
          paramHandler1.sendMessage(Message.obtain(null, 0, paramString1));
          return;
        }
        catch (IOException localIOException)
        {
          FlixsterApplication.setStreamStopError(Long.valueOf(ProfileDao.this.rightId), paramString1);
          paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(localIOException)));
        }
      }
    });
  }

  private static void toastLockerRightInsertion(LockerRight paramLockerRight, final String paramString)
  {
    final Activity localActivity = ActivityHolder.instance().getTopLevelActivity();
    if ((localActivity != null) && (!localActivity.isFinishing()))
      localActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          if (ProfileDao.this != null)
          {
            Activity localActivity = localActivity;
            Resources localResources = localActivity.getResources();
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = ProfileDao.this.getTitle();
            Toast.makeText(localActivity, localResources.getString(2131493208, arrayOfObject), 1).show();
          }
          User localUser;
          do
          {
            return;
            Toast.makeText(localActivity, localActivity.getResources().getString(2131493209), 1).show();
            localUser = AccountFacade.getSocialUser();
            if (paramString.equals("MKW"))
            {
              localUser.isMskWtsEligible = true;
              return;
            }
            if (paramString.equals("MKR"))
            {
              localUser.isMskRateEligible = true;
              return;
            }
            if (paramString.startsWith("MT1"))
            {
              localUser.isMskSmsEligible = true;
              return;
            }
          }
          while (!paramString.equals("MPN"));
          localUser.isMskInstallMobileEligible = true;
        }
      });
  }

  public static void trackLicense(Handler paramHandler1, final Handler paramHandler2, LockerRight paramLockerRight, final String paramString, final boolean paramBoolean)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        String str = ApiBuilder.rightLicenseStats(ProfileDao.this.rightId, paramString, paramBoolean, ProfileDao.this.isSonicProxyMode());
        try
        {
          HttpHelper.postToUrl(str, new ArrayList());
          return;
        }
        catch (IOException localIOException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(localIOException)));
        }
      }
    });
  }

  public static void trackMskFbRequest(String paramString)
  {
    final String str = currentUser.id;
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        Logger.d("FlxMain", "ProfileDao.trackMskFbRequest " + ProfileDao.this + " for user " + str);
        RestClient localRestClient = new RestClient(ApiBuilder.fbInvites(str, ProfileDao.this));
        try
        {
          localRestClient.execute(RestClient.RequestMethod.POST);
          Logger.d("FlxMain", "ProfileDao.trackMskFbRequest response: " + localRestClient.getResponseCode() + " " + localRestClient.getResponse());
          return;
        }
        catch (IOException localIOException)
        {
          Logger.e("FlxMain", "ProfileDao.trackMskFbRequest", localIOException);
        }
      }
    });
  }

  public static void updatePlaybackPosition(final Handler paramHandler1, final Handler paramHandler2, LockerRight paramLockerRight, final String paramString)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        String str = ApiBuilder.rightPosition(ProfileDao.this.rightId, paramString);
        ArrayList localArrayList = new ArrayList();
        try
        {
          HttpHelper.postToUrl(str, localArrayList);
          paramHandler1.sendMessage(Message.obtain(null, 0, paramString));
          return;
        }
        catch (IOException localIOException)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(localIOException)));
        }
      }
    });
  }

  public static void uvStreamCreate(final Handler paramHandler1, final Handler paramHandler2, LockerRight paramLockerRight)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      // ERROR //
      public void run()
      {
        // Byte code:
        //   0: invokestatic 42	net/flixster/android/FlixsterApplication:getStreamDeleteError	()[Ljava/lang/String;
        //   3: astore_1
        //   4: aload_1
        //   5: ifnull +81 -> 86
        //   8: aload_1
        //   9: arraylength
        //   10: iconst_2
        //   11: if_icmpne +75 -> 86
        //   14: aload_1
        //   15: iconst_0
        //   16: aaload
        //   17: astore 15
        //   19: aload_1
        //   20: iconst_1
        //   21: aaload
        //   22: astore 16
        //   24: ldc 44
        //   26: new 46	java/lang/StringBuilder
        //   29: dup
        //   30: ldc 48
        //   32: invokespecial 51	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
        //   35: aload 15
        //   37: invokevirtual 55	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   40: ldc 57
        //   42: invokevirtual 55	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   45: aload 16
        //   47: invokevirtual 55	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   50: invokevirtual 61	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //   53: invokestatic 67	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
        //   56: new 69	java/net/URL
        //   59: dup
        //   60: aload 16
        //   62: invokestatic 75	net/flixster/android/data/ApiBuilder:streamDelete	(Ljava/lang/String;)Ljava/lang/String;
        //   65: invokespecial 76	java/net/URL:<init>	(Ljava/lang/String;)V
        //   68: iconst_0
        //   69: iconst_0
        //   70: invokestatic 82	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
        //   73: pop
        //   74: aconst_null
        //   75: aconst_null
        //   76: invokestatic 85	net/flixster/android/FlixsterApplication:setStreamDeleteError	(Ljava/lang/String;Ljava/lang/String;)V
        //   79: ldc 44
        //   81: ldc 87
        //   83: invokestatic 67	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
        //   86: new 89	com/flixster/android/net/RestClient
        //   89: dup
        //   90: aload_0
        //   91: getfield 20	net/flixster/android/data/ProfileDao$16:val$right	Lnet/flixster/android/model/LockerRight;
        //   94: invokestatic 93	net/flixster/android/data/ApiBuilder:streamCreate	(Lnet/flixster/android/model/LockerRight;)Ljava/lang/String;
        //   97: invokespecial 94	com/flixster/android/net/RestClient:<init>	(Ljava/lang/String;)V
        //   100: astore_2
        //   101: aload_2
        //   102: getstatic 100	com/flixster/android/net/RestClient$RequestMethod:GET	Lcom/flixster/android/net/RestClient$RequestMethod;
        //   105: invokevirtual 104	com/flixster/android/net/RestClient:execute	(Lcom/flixster/android/net/RestClient$RequestMethod;)V
        //   108: aload_2
        //   109: invokevirtual 107	com/flixster/android/net/RestClient:getResponse	()Ljava/lang/String;
        //   112: astore 7
        //   114: new 109	org/json/JSONObject
        //   117: dup
        //   118: aload 7
        //   120: invokespecial 110	org/json/JSONObject:<init>	(Ljava/lang/String;)V
        //   123: astore 8
        //   125: aload 8
        //   127: ldc 112
        //   129: aconst_null
        //   130: invokevirtual 116	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   133: astore 11
        //   135: aload 11
        //   137: ifnonnull +93 -> 230
        //   140: aload 8
        //   142: ldc 118
        //   144: invokevirtual 121	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
        //   147: astore 12
        //   149: ldc 123
        //   151: aload 12
        //   153: invokestatic 85	net/flixster/android/FlixsterApplication:setStreamDeleteError	(Ljava/lang/String;Ljava/lang/String;)V
        //   156: aload_0
        //   157: getfield 24	net/flixster/android/data/ProfileDao$16:val$successHandler	Landroid/os/Handler;
        //   160: aconst_null
        //   161: iconst_0
        //   162: aload 12
        //   164: invokestatic 129	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
        //   167: invokevirtual 135	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //   170: pop
        //   171: return
        //   172: astore 17
        //   174: ldc 44
        //   176: ldc 137
        //   178: aload 17
        //   180: invokestatic 141	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   183: goto -97 -> 86
        //   186: astore 5
        //   188: aload_0
        //   189: getfield 22	net/flixster/android/data/ProfileDao$16:val$errorHandler	Landroid/os/Handler;
        //   192: aconst_null
        //   193: iconst_0
        //   194: new 143	java/lang/RuntimeException
        //   197: dup
        //   198: aload 5
        //   200: invokespecial 146	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //   203: invokestatic 129	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
        //   206: invokevirtual 135	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //   209: pop
        //   210: return
        //   211: astore_3
        //   212: aload_0
        //   213: getfield 22	net/flixster/android/data/ProfileDao$16:val$errorHandler	Landroid/os/Handler;
        //   216: aconst_null
        //   217: iconst_0
        //   218: aload_3
        //   219: invokestatic 152	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
        //   222: invokestatic 129	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
        //   225: invokevirtual 135	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //   228: pop
        //   229: return
        //   230: aload_0
        //   231: getfield 22	net/flixster/android/data/ProfileDao$16:val$errorHandler	Landroid/os/Handler;
        //   234: aconst_null
        //   235: iconst_0
        //   236: getstatic 158	net/flixster/android/data/DaoException$Type:STREAM_CREATE	Lnet/flixster/android/data/DaoException$Type;
        //   239: aload 11
        //   241: invokestatic 163	com/flixster/android/utils/StringHelper:appendPeriod	(Ljava/lang/String;)Ljava/lang/String;
        //   244: invokestatic 166	net/flixster/android/data/DaoException:create	(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;
        //   247: invokestatic 129	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
        //   250: invokevirtual 135	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //   253: pop
        //   254: return
        //   255: astore 9
        //   257: aload_0
        //   258: getfield 22	net/flixster/android/data/ProfileDao$16:val$errorHandler	Landroid/os/Handler;
        //   261: aconst_null
        //   262: iconst_0
        //   263: aload 9
        //   265: invokestatic 152	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
        //   268: invokestatic 129	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
        //   271: invokevirtual 135	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //   274: pop
        //   275: return
        //
        // Exception table:
        //   from	to	target	type
        //   24	86	172	java/lang/Exception
        //   86	114	186	java/net/MalformedURLException
        //   86	114	211	java/io/IOException
        //   114	135	255	org/json/JSONException
        //   140	149	255	org/json/JSONException
        //   230	254	255	org/json/JSONException
      }
    });
  }

  public static void uvStreamDelete(final Handler paramHandler1, final Handler paramHandler2, String paramString)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          HttpHelper.fetchUrl(new URL(ApiBuilder.streamDelete(ProfileDao.this)), false, false);
          FlixsterApplication.setStreamDeleteError(null, null);
          paramHandler1.sendMessage(Message.obtain(null, 0, ProfileDao.this));
          return;
        }
        catch (MalformedURLException localMalformedURLException)
        {
          FlixsterApplication.setStreamDeleteError("reserved", ProfileDao.this);
          paramHandler2.sendMessage(Message.obtain(null, 0, new RuntimeException(localMalformedURLException)));
          return;
        }
        catch (IOException localIOException)
        {
          FlixsterApplication.setStreamDeleteError("reserved", ProfileDao.this);
          paramHandler2.sendMessage(Message.obtain(null, 0, DaoException.create(localIOException)));
        }
      }
    });
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.ProfileDao
 * JD-Core Version:    0.6.2
 */