package net.flixster.android.data;

import android.text.TextUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.flixster.android.model.Theater;
import net.flixster.android.model.TheaterDistanceComparator;
import net.flixster.android.model.TheaterNameComparator;
import net.flixster.android.util.SoftHashMap;

public class TheaterDao
{
  public static final Map<Long, Theater> THEATER_CACHE = new SoftHashMap();
  public static TheaterDistanceComparator mTheaterDistanceComparator;
  public static TheaterNameComparator mTheaterNameComparator;

  // ERROR //
  public static HashMap<Long, java.util.ArrayList<net.flixster.android.model.Showtimes>> findShowtimesByTheater(Date paramDate, long paramLong)
    throws DaoException
  {
    // Byte code:
    //   0: lload_1
    //   1: invokestatic 35	net/flixster/android/data/TheaterDao:getTheater	(J)Lnet/flixster/android/model/Theater;
    //   4: astore_3
    //   5: aload_3
    //   6: invokevirtual 41	net/flixster/android/model/Theater:getShowtimesListByMovieHash	()Ljava/util/HashMap;
    //   9: pop
    //   10: new 43	java/net/URL
    //   13: dup
    //   14: lload_1
    //   15: aload_0
    //   16: invokestatic 49	net/flixster/android/data/ApiBuilder:theater	(JLjava/util/Date;)Ljava/lang/String;
    //   19: invokespecial 52	java/net/URL:<init>	(Ljava/lang/String;)V
    //   22: iconst_0
    //   23: iconst_0
    //   24: invokestatic 58	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   27: astore 7
    //   29: aload_3
    //   30: new 60	org/json/JSONObject
    //   33: dup
    //   34: aload 7
    //   36: invokespecial 61	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   39: invokevirtual 65	net/flixster/android/model/Theater:parseFromJSON	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Theater;
    //   42: pop
    //   43: aload_3
    //   44: invokevirtual 41	net/flixster/android/model/Theater:getShowtimesListByMovieHash	()Ljava/util/HashMap;
    //   47: astore 10
    //   49: aload 10
    //   51: areturn
    //   52: astore 6
    //   54: new 67	java/lang/RuntimeException
    //   57: dup
    //   58: aload 6
    //   60: invokespecial 70	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   63: athrow
    //   64: astore 5
    //   66: aload 5
    //   68: invokestatic 74	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   71: athrow
    //   72: astore 8
    //   74: aload 8
    //   76: invokestatic 74	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   79: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   10	29	52	java/net/MalformedURLException
    //   10	29	64	java/io/IOException
    //   29	49	72	org/json/JSONException
  }

  public static Theater findTheaterByTheaterMoviePair(long paramLong1, long paramLong2, Date paramDate)
    throws DaoException
  {
    List localList = findTheatersByMovieUrl(ApiBuilder.theaterMoviePair(paramLong1, paramLong2, paramDate));
    return (Theater)localList.get(-1 + localList.size());
  }

  public static void findTheaters(List<Theater> paramList, String paramString, HashMap<String, Boolean> paramHashMap)
    throws DaoException
  {
    getTheaters(paramList, ApiBuilder.theaters(paramString), paramHashMap);
  }

  public static List<Theater> findTheatersByMovieLocation(double paramDouble1, double paramDouble2, Date paramDate, long paramLong, HashMap<String, Boolean> paramHashMap)
    throws DaoException
  {
    return findTheatersWithFavoritesByMoveUrl(ApiBuilder.theatersByMovie(paramDouble1, paramDouble2, paramLong, paramDate), paramHashMap);
  }

  // ERROR //
  private static List<Theater> findTheatersByMovieUrl(String paramString)
    throws DaoException
  {
    // Byte code:
    //   0: new 115	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 116	java/util/ArrayList:<init>	()V
    //   7: astore_1
    //   8: new 115	java/util/ArrayList
    //   11: dup
    //   12: invokespecial 116	java/util/ArrayList:<init>	()V
    //   15: astore_2
    //   16: new 43	java/net/URL
    //   19: dup
    //   20: aload_0
    //   21: invokespecial 52	java/net/URL:<init>	(Ljava/lang/String;)V
    //   24: iconst_0
    //   25: iconst_0
    //   26: invokestatic 58	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   29: astore 5
    //   31: new 118	org/json/JSONArray
    //   34: dup
    //   35: aload 5
    //   37: invokespecial 119	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   40: astore 6
    //   42: aload 6
    //   44: invokevirtual 122	org/json/JSONArray:length	()I
    //   47: istore 8
    //   49: iconst_0
    //   50: istore 9
    //   52: iload 9
    //   54: iload 8
    //   56: if_icmplt +55 -> 111
    //   59: new 124	net/flixster/android/model/TheaterDistanceComparator
    //   62: dup
    //   63: invokespecial 125	net/flixster/android/model/TheaterDistanceComparator:<init>	()V
    //   66: putstatic 127	net/flixster/android/data/TheaterDao:mTheaterDistanceComparator	Lnet/flixster/android/model/TheaterDistanceComparator;
    //   69: aload_2
    //   70: getstatic 127	net/flixster/android/data/TheaterDao:mTheaterDistanceComparator	Lnet/flixster/android/model/TheaterDistanceComparator;
    //   73: invokestatic 133	java/util/Collections:sort	(Ljava/util/List;Ljava/util/Comparator;)V
    //   76: aload_1
    //   77: getstatic 127	net/flixster/android/data/TheaterDao:mTheaterDistanceComparator	Lnet/flixster/android/model/TheaterDistanceComparator;
    //   80: invokestatic 133	java/util/Collections:sort	(Ljava/util/List;Ljava/util/Comparator;)V
    //   83: aload_2
    //   84: aload_1
    //   85: invokeinterface 137 2 0
    //   90: pop
    //   91: aload_2
    //   92: areturn
    //   93: astore 4
    //   95: new 67	java/lang/RuntimeException
    //   98: dup
    //   99: aload 4
    //   101: invokespecial 70	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   104: athrow
    //   105: astore_3
    //   106: aload_3
    //   107: invokestatic 74	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   110: athrow
    //   111: aload 6
    //   113: iload 9
    //   115: invokevirtual 141	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   118: astore 11
    //   120: aload 11
    //   122: ldc 143
    //   124: invokevirtual 147	org/json/JSONObject:getLong	(Ljava/lang/String;)J
    //   127: invokestatic 35	net/flixster/android/data/TheaterDao:getTheater	(J)Lnet/flixster/android/model/Theater;
    //   130: astore 12
    //   132: aload 12
    //   134: aload 11
    //   136: invokevirtual 65	net/flixster/android/model/Theater:parseFromJSON	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Theater;
    //   139: pop
    //   140: aload_1
    //   141: aload 12
    //   143: invokeinterface 151 2 0
    //   148: pop
    //   149: iinc 9 1
    //   152: goto -100 -> 52
    //   155: astore 7
    //   157: aload 7
    //   159: invokestatic 74	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   162: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   16	31	93	java/net/MalformedURLException
    //   16	31	105	java/io/IOException
    //   31	49	155	org/json/JSONException
    //   59	91	155	org/json/JSONException
    //   111	149	155	org/json/JSONException
  }

  public static void findTheatersLocation(List<Theater> paramList, double paramDouble1, double paramDouble2, HashMap<String, Boolean> paramHashMap)
    throws DaoException
  {
    getTheaters(paramList, ApiBuilder.theaters(paramDouble1, paramDouble2), paramHashMap);
  }

  private static List<Theater> findTheatersWithFavoritesByMoveUrl(String paramString, HashMap<String, Boolean> paramHashMap)
    throws DaoException
  {
    String str = "";
    if (paramHashMap != null)
      str = "&theater=" + TextUtils.join("&theater=", paramHashMap.keySet());
    return findTheatersByMovieUrl(paramString + str);
  }

  public static Theater getTheater(long paramLong)
  {
    Theater localTheater = (Theater)THEATER_CACHE.get(Long.valueOf(paramLong));
    if (localTheater == null)
    {
      localTheater = new Theater(paramLong);
      THEATER_CACHE.put(Long.valueOf(paramLong), localTheater);
    }
    return localTheater;
  }

  // ERROR //
  private static void getTheaters(List<Theater> paramList, String paramString, HashMap<String, Boolean> paramHashMap)
    throws DaoException
  {
    // Byte code:
    //   0: ldc 158
    //   2: astore_3
    //   3: aload_2
    //   4: ifnull +28 -> 32
    //   7: new 160	java/lang/StringBuilder
    //   10: dup
    //   11: ldc 162
    //   13: invokespecial 163	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   16: ldc 162
    //   18: aload_2
    //   19: invokevirtual 169	java/util/HashMap:keySet	()Ljava/util/Set;
    //   22: invokestatic 175	android/text/TextUtils:join	(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    //   25: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   28: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   31: astore_3
    //   32: new 43	java/net/URL
    //   35: dup
    //   36: new 160	java/lang/StringBuilder
    //   39: dup
    //   40: aload_1
    //   41: invokestatic 189	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   44: invokespecial 163	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   47: aload_3
    //   48: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   51: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   54: invokespecial 52	java/net/URL:<init>	(Ljava/lang/String;)V
    //   57: iconst_0
    //   58: iconst_0
    //   59: invokestatic 58	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
    //   62: astore 6
    //   64: new 118	org/json/JSONArray
    //   67: dup
    //   68: aload 6
    //   70: invokespecial 119	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   73: astore 7
    //   75: aload 7
    //   77: invokevirtual 122	org/json/JSONArray:length	()I
    //   80: istore 9
    //   82: iconst_0
    //   83: istore 10
    //   85: iload 10
    //   87: iload 9
    //   89: if_icmplt +24 -> 113
    //   92: return
    //   93: astore 5
    //   95: new 67	java/lang/RuntimeException
    //   98: dup
    //   99: aload 5
    //   101: invokespecial 70	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   104: athrow
    //   105: astore 4
    //   107: aload 4
    //   109: invokestatic 74	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   112: athrow
    //   113: aload 7
    //   115: iload 10
    //   117: invokevirtual 141	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   120: astore 11
    //   122: aload 11
    //   124: ldc 143
    //   126: invokevirtual 147	org/json/JSONObject:getLong	(Ljava/lang/String;)J
    //   129: invokestatic 35	net/flixster/android/data/TheaterDao:getTheater	(J)Lnet/flixster/android/model/Theater;
    //   132: astore 12
    //   134: aload 12
    //   136: aload 11
    //   138: invokevirtual 65	net/flixster/android/model/Theater:parseFromJSON	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Theater;
    //   141: pop
    //   142: aload_0
    //   143: aload 12
    //   145: invokeinterface 151 2 0
    //   150: pop
    //   151: iinc 10 1
    //   154: goto -69 -> 85
    //   157: astore 8
    //   159: aload 8
    //   161: invokestatic 74	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    //   164: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   32	64	93	java/net/MalformedURLException
    //   32	64	105	java/io/IOException
    //   64	82	157	org/json/JSONException
    //   113	151	157	org/json/JSONException
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.TheaterDao
 * JD-Core Version:    0.6.2
 */