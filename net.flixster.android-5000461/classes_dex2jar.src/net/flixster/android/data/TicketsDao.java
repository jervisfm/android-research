package net.flixster.android.data;

import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.flixster.android.FlixUtils;
import net.flixster.android.model.MovieTicketException;
import net.flixster.android.model.MovieTicketPurchaseException;
import net.flixster.android.model.Performance;
import net.flixster.android.util.HttpHelper;
import net.flixster.android.util.PostalCodeUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

@Deprecated
public class TicketsDao
{
  public static final int CC_AMEX = 3;
  public static final int CC_DISCOVER = 4;
  public static final int CC_MASTERCARD = 2;
  public static final int CC_UNKNOWN = 0;
  public static final int CC_VISA = 1;
  public static final int FIELD_STATE_BAD = 3;
  public static final int FIELD_STATE_GOOD = 2;
  public static final int FIELD_STATE_NEW = 0;
  public static final int FIELD_STATE_PARTIAL = 1;
  public static final int UNSELECTED_MONTH = -1;
  public static final int UNSELECTED_YEAR = -100;
  private static final Pattern sAmexPattern = Pattern.compile("^((34)|(37))[0-9]{0,13}");
  private static final Pattern sEmailPattern = Pattern.compile("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$");
  private static final Pattern sMcPattern = Pattern.compile("^5[1-5][0-9]{0,14}");

  private static int cardCheckSum(CharSequence paramCharSequence)
  {
    int i = paramCharSequence.length();
    int j = 0;
    int k = i - 1;
    if (k < 0)
    {
      if (j % 10 == 0)
        return 2;
    }
    else
    {
      int m = Character.digit(paramCharSequence.charAt(k), 10);
      if ((i - k) % 2 == 0)
      {
        int n = m * 2;
        j = j + n % 10 + n / 10;
      }
      while (true)
      {
        k--;
        break;
        j += m;
      }
    }
    return 3;
  }

  // ERROR //
  public static Performance getMoviePerformance(String paramString1, String paramString2, String paramString3, String paramString4)
    throws Exception
  {
    // Byte code:
    //   0: ldc 79
    //   2: new 81	java/lang/StringBuilder
    //   5: dup
    //   6: ldc 83
    //   8: invokespecial 86	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   11: aload_0
    //   12: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   15: ldc 92
    //   17: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   20: aload_1
    //   21: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   24: ldc 94
    //   26: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: aload_2
    //   30: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   33: ldc 96
    //   35: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   38: aload_3
    //   39: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   45: invokestatic 106	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   48: new 108	com/flixster/android/net/RestClient
    //   51: dup
    //   52: aload_0
    //   53: aload_1
    //   54: aload_2
    //   55: aload_3
    //   56: invokestatic 114	net/flixster/android/data/ApiBuilder:ticketAvailability	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   59: invokespecial 115	com/flixster/android/net/RestClient:<init>	(Ljava/lang/String;)V
    //   62: astore 5
    //   64: aload 5
    //   66: getstatic 121	com/flixster/android/net/RestClient$RequestMethod:GET	Lcom/flixster/android/net/RestClient$RequestMethod;
    //   69: invokevirtual 125	com/flixster/android/net/RestClient:execute	(Lcom/flixster/android/net/RestClient$RequestMethod;)V
    //   72: new 127	org/json/JSONObject
    //   75: dup
    //   76: aload 5
    //   78: invokevirtual 130	com/flixster/android/net/RestClient:getResponse	()Ljava/lang/String;
    //   81: invokespecial 131	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   84: astore 7
    //   86: aload 7
    //   88: ldc 133
    //   90: invokevirtual 137	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   93: ifeq +82 -> 175
    //   96: aload 7
    //   98: ldc 133
    //   100: invokevirtual 141	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   103: invokestatic 146	net/flixster/android/FlixUtils:copyString	(Ljava/lang/String;)Ljava/lang/String;
    //   106: astore 10
    //   108: ldc 79
    //   110: new 81	java/lang/StringBuilder
    //   113: dup
    //   114: ldc 148
    //   116: invokespecial 86	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   119: aload 10
    //   121: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   124: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   127: invokestatic 151	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;)V
    //   130: new 153	net/flixster/android/model/MovieTicketException
    //   133: dup
    //   134: aload 10
    //   136: invokespecial 154	net/flixster/android/model/MovieTicketException:<init>	(Ljava/lang/String;)V
    //   139: athrow
    //   140: astore 4
    //   142: ldc 79
    //   144: ldc 156
    //   146: aload 4
    //   148: invokestatic 160	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   151: new 77	java/io/IOException
    //   154: dup
    //   155: ldc 162
    //   157: invokespecial 163	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   160: athrow
    //   161: astore 6
    //   163: ldc 79
    //   165: ldc 165
    //   167: aload 6
    //   169: invokestatic 160	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   172: aload 6
    //   174: athrow
    //   175: new 167	net/flixster/android/model/Performance
    //   178: dup
    //   179: invokespecial 168	net/flixster/android/model/Performance:<init>	()V
    //   182: astore 8
    //   184: aload 8
    //   186: aload 7
    //   188: invokevirtual 172	net/flixster/android/model/Performance:parseFromJSON	(Lorg/json/JSONObject;)Lnet/flixster/android/model/Performance;
    //   191: pop
    //   192: ldc 79
    //   194: new 81	java/lang/StringBuilder
    //   197: dup
    //   198: ldc 174
    //   200: invokespecial 86	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   203: aload 8
    //   205: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   208: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   211: invokestatic 106	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   214: aload 8
    //   216: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   0	64	140	org/json/JSONException
    //   64	72	140	org/json/JSONException
    //   72	140	140	org/json/JSONException
    //   163	175	140	org/json/JSONException
    //   175	214	140	org/json/JSONException
    //   64	72	161	java/io/IOException
  }

  public static Performance getReservation(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, Performance paramPerformance, float[] paramArrayOfFloat)
    throws Exception
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new BasicNameValuePair("theater", paramString1));
    localArrayList.add(new BasicNameValuePair("listing", paramString2));
    localArrayList.add(new BasicNameValuePair("date", paramString3));
    localArrayList.add(new BasicNameValuePair("time", paramString4));
    localArrayList.add(new BasicNameValuePair("ct", paramString5));
    localArrayList.add(new BasicNameValuePair("cn", paramString6));
    localArrayList.add(new BasicNameValuePair("cv", paramString7));
    localArrayList.add(new BasicNameValuePair("cz", paramString8));
    localArrayList.add(new BasicNameValuePair("ce", paramString9));
    localArrayList.add(new BasicNameValuePair("email", paramString10));
    int i = paramPerformance.getCategoryTally();
    int j = 0;
    while (true)
    {
      String str;
      if (j >= i)
      {
        str = HttpHelper.postToUrl(ApiBuilder.ticketReservation(), localArrayList).trim();
        Logger.d("FlxMain", "TicketDao.getReservation(...) response:" + str);
      }
      try
      {
        paramPerformance.parseFromJSON(new JSONObject(str));
        return paramPerformance;
        localArrayList.add(new BasicNameValuePair(paramPerformance.getCategoryIds()[j], Integer.toString((int)paramArrayOfFloat[j])));
        j++;
      }
      catch (Exception localException)
      {
        Logger.e("FlxMain", "TicketsSao.getReservation problem parsing json response " + str, localException);
      }
    }
    return paramPerformance;
  }

  public static int[] makeCardTypeMap(String[] paramArrayOfString)
  {
    int i = paramArrayOfString.length;
    int[] arrayOfInt = new int[i];
    int j = 0;
    if (j >= i)
      return arrayOfInt;
    if (paramArrayOfString[j].contentEquals("VI"))
      arrayOfInt[j] = 1;
    while (true)
    {
      j++;
      break;
      if (paramArrayOfString[j].contentEquals("MC"))
        arrayOfInt[j] = 2;
      else if (paramArrayOfString[j].contentEquals("AX"))
        arrayOfInt[j] = 3;
      else if (paramArrayOfString[j].contentEquals("DS"))
        arrayOfInt[j] = 4;
      else
        arrayOfInt[j] = 0;
    }
  }

  public static String purchaseTickets(String paramString1, String paramString2)
    throws MovieTicketException, Exception
  {
    try
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(new BasicNameValuePair("email", paramString2));
      localArrayList.add(new BasicNameValuePair("reservation", paramString1));
      String str1 = HttpHelper.postToUrl(ApiBuilder.ticketPurchase(), localArrayList).trim();
      Logger.d("FlxMain", "TicketsDao.purchaseTickets response:" + str1);
      localJSONObject = new JSONObject(str1);
      if (localJSONObject.has("error"))
      {
        Logger.d("FlxMain", "TicketsDao. throw standard error");
        throw new MovieTicketException(FlixUtils.copyString(localJSONObject.getString("error")));
      }
    }
    catch (MovieTicketException localMovieTicketException)
    {
      JSONObject localJSONObject;
      throw localMovieTicketException;
      if (localJSONObject.has("confirmation"))
      {
        String str2 = localJSONObject.getString("confirmation");
        return str2;
      }
    }
    catch (Exception localException)
    {
      Logger.d("FlxMain", "Execption on purchse occured", localException);
    }
    throw new MovieTicketPurchaseException("An error has occured. Please call MovieTickets.com (1-888-440-8457) to confirm your purchase.");
  }

  public static int validateCardCode(String paramString, int paramInt)
  {
    int i = 2;
    if (paramString.length() == 0)
    {
      i = 0;
      return i;
    }
    switch (paramInt)
    {
    default:
    case 1:
    case 2:
    case 4:
    case 3:
    }
    do
    {
      do
      {
        return 1;
        if (paramString.length() == 3)
          break;
      }
      while (paramString.length() <= 3);
      return 3;
    }
    while (paramString.length() != 4);
    return i;
  }

  public static int validateCreditCard(String paramString, int paramInt)
  {
    int i = 3;
    int j = paramString.length();
    if (j == 0)
    {
      i = 0;
      return i;
    }
    switch (paramInt)
    {
    default:
    case 1:
    case 2:
    case 4:
    case 3:
    }
    do
    {
      do
      {
        do
        {
          do
          {
            return 1;
            if (!paramString.startsWith("4"))
              break;
            if ((j == 13) && (cardCheckSum(paramString) == 2))
              return 2;
          }
          while (j != 16);
          return cardCheckSum(paramString);
          if ((j > 1) && (!sMcPattern.matcher(paramString).matches()))
            break;
        }
        while (j != 16);
        return cardCheckSum(paramString);
        if ((j > i) && (!paramString.startsWith("6011")))
          break;
      }
      while (j != 16);
      return cardCheckSum(paramString);
      if ((j > 1) && (!sAmexPattern.matcher(paramString).matches()))
        break;
    }
    while (j != 15);
    return cardCheckSum(paramString);
  }

  public static int validateDate(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = 3;
    Logger.d("FlxMain", "MovieTicketDao.validateDate(monthIndex:" + paramInt1 + " yearIndex:" + paramInt2 + " thisMonth:" + paramInt3);
    if ((paramInt1 == -1) && (paramInt2 == -100))
      i = 0;
    do
    {
      do
        return i;
      while ((paramInt1 > 11) || ((paramInt2 < 0) && (paramInt2 != -100)) || ((paramInt1 < 0) && (paramInt1 != -1)));
      if ((paramInt1 == -1) || (paramInt2 == -100))
        return 1;
    }
    while ((paramInt2 <= 0) && (paramInt3 > paramInt1));
    return 2;
  }

  public static int validateEmail(String paramString)
  {
    if (paramString.length() == 0)
      return 0;
    if (sEmailPattern.matcher(paramString).matches())
      return 2;
    return 3;
  }

  public static int validatePostal(String paramString)
  {
    if (paramString.length() == 0)
      return 0;
    if (paramString.length() < 5)
      return 1;
    if (PostalCodeUtils.parseZipcodeForShowtimes(paramString) != null)
      return 2;
    return 3;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.data.TicketsDao
 * JD-Core Version:    0.6.2
 */