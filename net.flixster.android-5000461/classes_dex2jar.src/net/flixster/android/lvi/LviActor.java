package net.flixster.android.lvi;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.flixster.android.activity.ImageViewHandler;
import com.flixster.android.utils.ImageGetter;
import com.flixster.android.utils.ImageTask;
import net.flixster.android.model.Actor;
import net.flixster.android.model.ImageOrder;

public class LviActor extends Lvi
{
  public Actor mActor;
  public View.OnClickListener mActorClick;
  private boolean smallThumbnail;

  public LviActor()
  {
  }

  public LviActor(Actor paramActor, boolean paramBoolean)
  {
    this.mActor = paramActor;
    this.smallThumbnail = paramBoolean;
  }

  public int getItemViewType()
  {
    return VIEW_TYPE_ACTOR;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    if ((paramView == null) || (!(paramView.getTag() instanceof ActorViewHolder)))
      paramView = LayoutInflater.from(paramContext).inflate(2130903063, paramViewGroup, false);
    ActorViewHolder localActorViewHolder = (ActorViewHolder)paramView.getTag();
    if (localActorViewHolder == null)
    {
      localActorViewHolder = new ActorViewHolder(null);
      localActorViewHolder.actorLayout = ((RelativeLayout)paramView.findViewById(2131165224));
      localActorViewHolder.nameView = ((TextView)paramView.findViewById(2131165228));
      localActorViewHolder.imageView = ((ImageView)paramView.findViewById(2131165227));
      paramView.setTag(localActorViewHolder);
    }
    if (this.mActor != null)
    {
      String str = this.mActor.name;
      localActorViewHolder.nameView.setText(str);
      localActorViewHolder.imageView.setTag(this.mActor);
      if (this.smallThumbnail)
      {
        int i = paramContext.getResources().getDimensionPixelSize(2131361843);
        int j = paramContext.getResources().getDimensionPixelSize(2131361835);
        RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(i, i);
        localLayoutParams.setMargins(0, 0, j, 0);
        localActorViewHolder.imageView.setLayoutParams(localLayoutParams);
      }
      if (this.mActor.bitmap == null)
        break label269;
      localActorViewHolder.imageView.setImageBitmap(this.mActor.bitmap);
    }
    while (true)
    {
      localActorViewHolder.actorLayout.setTag(this.mActor);
      localActorViewHolder.actorLayout.setId(2130903063);
      if (this.mActorClick != null)
        localActorViewHolder.actorLayout.setOnClickListener(this.mActorClick);
      return paramView;
      label269: if (this.mActor.thumbnailUrl != null)
      {
        localActorViewHolder.imageView.setImageResource(2130837841);
        if ((paramContext instanceof ImageTask))
        {
          ImageOrder localImageOrder = new ImageOrder(4, this.mActor, this.mActor.thumbnailUrl, localActorViewHolder.imageView, paramHandler);
          ((ImageTask)paramContext).orderImageFifo(localImageOrder);
        }
        else
        {
          ImageGetter.instance().get(this.mActor.thumbnailUrl, new ImageViewHandler(localActorViewHolder.imageView, this.mActor.thumbnailUrl));
        }
      }
    }
  }

  private static final class ActorViewHolder
  {
    RelativeLayout actorLayout;
    ImageView imageView;
    TextView nameView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviActor
 * JD-Core Version:    0.6.2
 */