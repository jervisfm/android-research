package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.flixster.android.utils.Logger;
import net.flixster.android.ads.AdView;
import net.flixster.android.ads.model.DfpAd.DfpCustomTarget;

public class LviAd extends Lvi
{
  public DfpAd.DfpCustomTarget dfpTarget;
  public String mAdSlot;
  private AdView mAdView;

  public void destroy()
  {
    if (this.mAdView != null)
    {
      this.mAdView.destroy();
      this.mAdView = null;
    }
  }

  public int getItemViewType()
  {
    return VIEW_TYPE_ADVIEW;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    Logger.v("FlxAd", "LviAd.getView position:" + paramInt);
    if (this.mAdView == null)
    {
      this.mAdView = new AdView(paramContext, this.mAdSlot);
      this.mAdView.dfpCustomTarget = this.dfpTarget;
      this.mAdView.refreshAds();
    }
    return this.mAdView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviAd
 * JD-Core Version:    0.6.2
 */