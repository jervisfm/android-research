package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.List;

public class LviAdapter extends BaseAdapter
{
  private Context context;
  private List<Lvi> items;
  private final Handler refreshHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      LviAdapter.this.notifyDataSetChanged();
    }
  };

  public LviAdapter(Context paramContext, List<Lvi> paramList)
  {
    this.context = paramContext;
    this.items = paramList;
  }

  public boolean areAllItemsEnabled()
  {
    return false;
  }

  public int getCount()
  {
    return this.items.size();
  }

  public Object getItem(int paramInt)
  {
    return this.items.get(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public int getItemViewType(int paramInt)
  {
    return ((Lvi)this.items.get(paramInt)).getItemViewType();
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    return ((Lvi)this.items.get(paramInt)).getView(paramInt, paramView, paramViewGroup, this.context, this.refreshHandler);
  }

  public int getViewTypeCount()
  {
    return LviType.values().length;
  }

  public static enum LviType
  {
    static
    {
      DETAILSCAP = new LviType("DETAILSCAP", 2);
      SHOWTIMES = new LviType("SHOWTIMES", 3);
      NEWS_ITEM = new LviType("NEWS_ITEM", 4);
      CATEGORY = new LviType("CATEGORY", 5);
      MESSAGE = new LviType("MESSAGE", 6);
      USER_PROFILE = new LviType("USER_PROFILE", 7);
      ACTOR = new LviType("ACTOR", 8);
      ADVIEW = new LviType("ADVIEW", 9);
      LOCATION_PANEL = new LviType("LOCATION_PANEL", 10);
      DATE_PANEL = new LviType("DATE_PANEL", 11);
      ICON_PANEL = new LviType("ICON_PANEL", 12);
      PAGEHEADER = new LviType("PAGEHEADER", 13);
      SUBHEADER = new LviType("SUBHEADER", 14);
      FOOTER = new LviType("FOOTER", 15);
      DIVIDER = new LviType("DIVIDER", 16);
      REFRESHBAR = new LviType("REFRESHBAR", 17);
      LOAD_MORE = new LviType("LOAD_MORE", 18);
      LviType[] arrayOfLviType = new LviType[19];
      arrayOfLviType[0] = MOVIE;
      arrayOfLviType[1] = THEATER;
      arrayOfLviType[2] = DETAILSCAP;
      arrayOfLviType[3] = SHOWTIMES;
      arrayOfLviType[4] = NEWS_ITEM;
      arrayOfLviType[5] = CATEGORY;
      arrayOfLviType[6] = MESSAGE;
      arrayOfLviType[7] = USER_PROFILE;
      arrayOfLviType[8] = ACTOR;
      arrayOfLviType[9] = ADVIEW;
      arrayOfLviType[10] = LOCATION_PANEL;
      arrayOfLviType[11] = DATE_PANEL;
      arrayOfLviType[12] = ICON_PANEL;
      arrayOfLviType[13] = PAGEHEADER;
      arrayOfLviType[14] = SUBHEADER;
      arrayOfLviType[15] = FOOTER;
      arrayOfLviType[16] = DIVIDER;
      arrayOfLviType[17] = REFRESHBAR;
      arrayOfLviType[18] = LOAD_MORE;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviAdapter
 * JD-Core Version:    0.6.2
 */