package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LviCategory extends Lvi
{
  public String mCategory;
  public String mFilter;

  public int getItemViewType()
  {
    return VIEW_TYPE_CATEGORY;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    TextView localTextView;
    if (paramView == null)
    {
      localTextView = new TextView(paramContext);
      localTextView.setGravity(16);
      localTextView.setBackgroundResource(2130837621);
      localTextView.setPadding(20, 20, 20, 20);
      localTextView.setTextAppearance(paramContext, 2131558511);
    }
    while (true)
    {
      localTextView.setText(this.mCategory);
      return localTextView;
      localTextView = (TextView)paramView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviCategory
 * JD-Core Version:    0.6.2
 */