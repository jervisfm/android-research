package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.flixster.android.utils.Logger;
import java.util.Calendar;
import java.util.Date;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.util.DialogUtils;

public class LviDatePanel extends Lvi
{
  public View.OnClickListener mOnClickListener;

  public int getItemViewType()
  {
    return VIEW_TYPE_DATEPANEL;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    Logger.d("FlxMain", "LviDatePanel.getView position:" + paramInt + " convertView:" + paramView);
    if (paramView == null)
      paramView = LayoutInflater.from(paramContext).inflate(2130903108, paramViewGroup, false);
    Date localDate = FlixsterApplication.getShowtimesDate();
    Logger.d("FlxMain", "LviDatePanel.getView showtimes date:" + localDate);
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.setLenient(false);
    localCalendar.set(11, 0);
    localCalendar.set(12, 0);
    localCalendar.set(13, 0);
    localCalendar.set(14, 0);
    long l1 = localCalendar.getTimeInMillis();
    localCalendar.setTime(localDate);
    localCalendar.set(11, 0);
    localCalendar.set(12, 0);
    localCalendar.set(13, 0);
    localCalendar.set(14, 0);
    long l2 = (10800000L + (localCalendar.getTimeInMillis() - l1)) / 86400000L;
    CharSequence[] arrayOfCharSequence = DialogUtils.getShowtimesDateOptions(paramContext);
    TextView localTextView = (TextView)paramView.findViewById(2131165425);
    if ((l2 >= 0L) && (arrayOfCharSequence.length > l2))
      localTextView.setText(arrayOfCharSequence[((int)l2)]);
    View.OnClickListener localOnClickListener = this.mOnClickListener;
    paramView.setOnClickListener(localOnClickListener);
    return paramView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviDatePanel
 * JD-Core Version:    0.6.2
 */