package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.Movie;

public class LviFooter extends Lvi
{
  public Movie mMovie;
  public View.OnClickListener mTrailerClick;

  public int getItemViewType()
  {
    return VIEW_TYPE_FOOTER;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    int i = FlixsterApplication.getMovieRatingType();
    FooterItemHolder localFooterItemHolder;
    if (paramView != null)
      localFooterItemHolder = (FooterItemHolder)paramView.getTag();
    while (i == 1)
    {
      localFooterItemHolder.footerTitle.setVisibility(0);
      localFooterItemHolder.footerFresh.setVisibility(0);
      localFooterItemHolder.footerRotten.setVisibility(0);
      localFooterItemHolder.footerPopcorn.setVisibility(8);
      localFooterItemHolder.footerSpilled.setVisibility(8);
      localFooterItemHolder.footerWts.setVisibility(8);
      localFooterItemHolder.footerTitleFlixster.setVisibility(8);
      return paramView;
      paramView = LayoutInflater.from(paramContext).inflate(2130903072, paramViewGroup, false);
      localFooterItemHolder = new FooterItemHolder();
      localFooterItemHolder.footerTitle = ((TextView)paramView.findViewById(2131165274));
      localFooterItemHolder.footerTitleFlixster = ((TextView)paramView.findViewById(2131165277));
      localFooterItemHolder.footerFresh = ((TextView)paramView.findViewById(2131165275));
      localFooterItemHolder.footerRotten = ((TextView)paramView.findViewById(2131165276));
      localFooterItemHolder.footerPopcorn = ((TextView)paramView.findViewById(2131165278));
      localFooterItemHolder.footerSpilled = ((TextView)paramView.findViewById(2131165279));
      localFooterItemHolder.footerWts = ((TextView)paramView.findViewById(2131165280));
      paramView.setTag(localFooterItemHolder);
    }
    localFooterItemHolder.footerTitleFlixster.setVisibility(0);
    localFooterItemHolder.footerWts.setVisibility(0);
    localFooterItemHolder.footerPopcorn.setVisibility(0);
    localFooterItemHolder.footerSpilled.setVisibility(0);
    localFooterItemHolder.footerTitle.setVisibility(8);
    localFooterItemHolder.footerFresh.setVisibility(8);
    localFooterItemHolder.footerRotten.setVisibility(8);
    return paramView;
  }

  static class FooterItemHolder
  {
    TextView footerFresh;
    TextView footerPopcorn;
    TextView footerRotten;
    TextView footerSpilled;
    TextView footerTitle;
    TextView footerTitleFlixster;
    TextView footerWts;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviFooter
 * JD-Core Version:    0.6.2
 */