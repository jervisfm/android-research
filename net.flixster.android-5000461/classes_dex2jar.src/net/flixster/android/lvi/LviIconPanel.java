package net.flixster.android.lvi;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class LviIconPanel extends Lvi
{
  public int mDrawableResource = 0;
  public String mLabel;
  public View.OnClickListener mOnClickListener;

  public int getItemViewType()
  {
    return VIEW_TYPE_ICONPANEL;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    if (paramView == null);
    for (TextView localTextView = (TextView)LayoutInflater.from(paramContext).inflate(2130903110, paramViewGroup, false); ; localTextView = (TextView)paramView)
    {
      if (this.mLabel != null)
        localTextView.setText(this.mLabel);
      if (this.mDrawableResource != 0)
      {
        Drawable localDrawable = paramContext.getResources().getDrawable(this.mDrawableResource);
        localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
        localTextView.setCompoundDrawables(localDrawable, null, null, null);
      }
      localTextView.setOnClickListener(this.mOnClickListener);
      return localTextView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviIconPanel
 * JD-Core Version:    0.6.2
 */