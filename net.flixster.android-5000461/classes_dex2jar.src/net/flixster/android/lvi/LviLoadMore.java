package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.flixster.android.view.LoadMore;

public class LviLoadMore extends Lvi
{
  private final String subtitleId;
  private final String titleId;
  public final Type type;

  public LviLoadMore(String paramString1, String paramString2, Type paramType)
  {
    this.titleId = paramString1;
    this.subtitleId = paramString2;
    this.type = paramType;
  }

  public int getItemViewType()
  {
    return LviAdapter.LviType.LOAD_MORE.ordinal();
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    if (paramView == null)
      paramView = new LoadMore(paramContext);
    ((LoadMore)paramView).load(this.titleId, this.subtitleId, true);
    return paramView;
  }

  public static enum Type
  {
    static
    {
      ACTOR = new Type("ACTOR", 1);
      Type[] arrayOfType = new Type[2];
      arrayOfType[0] = MOVIE;
      arrayOfType[1] = ACTOR;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviLoadMore
 * JD-Core Version:    0.6.2
 */