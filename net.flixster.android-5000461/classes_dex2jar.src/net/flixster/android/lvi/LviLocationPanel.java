package net.flixster.android.lvi;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import java.lang.reflect.Method;
import net.flixster.android.FlixsterApplication;

public class LviLocationPanel extends Lvi
{
  private View mConvertView;

  public int getItemViewType()
  {
    return VIEW_TYPE_LOCATIONPANEL;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, final Context paramContext, Handler paramHandler)
  {
    if (paramView == null)
      paramView = LayoutInflater.from(paramContext).inflate(2130903111, paramViewGroup, false);
    this.mConvertView = paramView;
    TextView localTextView = (TextView)paramView.findViewById(2131165432);
    if (FlixsterApplication.getUseLocationServiceFlag())
    {
      final Handler local1 = new Handler()
      {
        public void handleMessage(Message paramAnonymousMessage)
        {
          String str = (String)paramAnonymousMessage.obj;
          TextView localTextView = (TextView)LviLocationPanel.this.mConvertView.findViewById(2131165432);
          localTextView.setText(str);
          if (str.contentEquals(paramContext.getResources().getString(2131493083)))
          {
            localTextView.setTextColor(-65536);
            return;
          }
          localTextView.setTextColor(-16777216);
        }
      };
      localTextView.setText("");
      WorkerThreads.instance().invokeLater(new Runnable()
      {
        public void run()
        {
          String str = FlixsterApplication.getCurrentLocationDisplay();
          local1.sendMessage(Message.obtain(null, 0, str));
        }
      });
    }
    try
    {
      while (true)
      {
        paramView.setOnClickListener((View.OnClickListener)paramContext.getClass().getMethod("getStartSettingsClickListener", new Class[0]).invoke(paramContext, new Object[0]));
        return paramView;
        if ((FlixsterApplication.getUserLocation() != null) && (!"".equals(FlixsterApplication.getUserLocation())))
        {
          localTextView.setText(FlixsterApplication.getUserLocation());
          localTextView.setTextColor(-16777216);
        }
        else
        {
          localTextView.setText(2131493083);
          localTextView.setTextColor(-65536);
        }
      }
    }
    catch (Exception localException)
    {
      Logger.e("FlxMain", "getStartSettingsClickListener not found", localException);
    }
    return paramView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviLocationPanel
 * JD-Core Version:    0.6.2
 */