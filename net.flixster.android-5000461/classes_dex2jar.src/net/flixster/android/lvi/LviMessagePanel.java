package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LviMessagePanel extends Lvi
{
  public String mMessage;

  public int getItemViewType()
  {
    return VIEW_TYPE_MESSAGE;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    if (paramView == null)
      paramView = LayoutInflater.from(paramContext).inflate(2130903112, paramViewGroup, false);
    ((TextView)paramView).setText(this.mMessage);
    return paramView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviMessagePanel
 * JD-Core Version:    0.6.2
 */