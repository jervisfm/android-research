package net.flixster.android.lvi;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.activity.ImageViewHandler;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackLogic;
import com.flixster.android.net.DownloadHelper;
import com.flixster.android.utils.ImageGetter;
import com.flixster.android.utils.ImageTask;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.FriendActivity;
import java.lang.ref.SoftReference;
import java.util.Date;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;

public class LviMovie extends Lvi
{
  public static final boolean ENABLE_SPILLED = true;
  public static final int FORM_CAP = 1;
  public static final int FORM_ITEM;
  public View.OnClickListener mDetailsClick;
  private String mFlixScore = null;
  public int mForm = 0;
  private String mGross = null;
  public boolean mIsCollected = false;
  public boolean mIsFeatured = false;
  private Boolean mIsReleased = null;
  public Movie mMovie;
  private Resources mResources = null;
  public Review mReview;
  private String mRtScore = null;
  public View.OnClickListener mTrailerClick;
  public LockerRight right;

  public LviMovie()
  {
  }

  public LviMovie(Movie paramMovie)
  {
    this.mMovie = paramMovie;
  }

  private void setDownloadControls(TextView paramTextView)
  {
    if (this.right == null)
      return;
    String str;
    if (DownloadHelper.isMovieDownloadInProgress(this.right))
      str = this.mResources.getString(2131493263);
    while (true)
    {
      paramTextView.setText(str);
      boolean bool = "".equals(str);
      int i = 0;
      if (bool)
        i = 8;
      paramTextView.setVisibility(i);
      return;
      if (DownloadHelper.isDownloaded(this.right))
      {
        Resources localResources = this.mResources;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = this.right.getDownloadedAssetSize();
        str = localResources.getString(2131493264, arrayOfObject);
      }
      else
      {
        str = "";
      }
    }
  }

  private void setFlixScore(MovieViewItemHolder paramMovieViewItemHolder)
  {
    if (this.mMovie.checkIntProperty("popcornScore"))
    {
      int i = this.mMovie.getIntProperty("popcornScore").intValue();
      int j;
      int k;
      if (i < 60)
      {
        j = 1;
        if (this.mFlixScore == null)
        {
          if (this.mForm != 0)
            break label141;
          this.mFlixScore = (i + "%");
        }
        paramMovieViewItemHolder.movieFlixScore.setText(this.mFlixScore);
        if (this.mIsReleased.booleanValue())
          break label210;
        k = 2130837750;
      }
      while (true)
      {
        Drawable localDrawable = this.mResources.getDrawable(k);
        localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
        paramMovieViewItemHolder.movieFlixScore.setCompoundDrawables(localDrawable, null, null, null);
        return;
        j = 0;
        break;
        label141: Object localObject = this.mResources.getString(2131493219);
        String str = this.mResources.getString(2131493220);
        StringBuilder localStringBuilder = new StringBuilder(String.valueOf(i));
        if (this.mIsReleased.booleanValue());
        while (true)
        {
          this.mFlixScore = ((String)localObject);
          break;
          localObject = str;
        }
        label210: if (j != 0)
          k = 2130837743;
        else
          k = 2130837738;
      }
    }
    paramMovieViewItemHolder.movieFlixScore.setVisibility(8);
  }

  private void setFriendScore(TextView paramTextView, Context paramContext)
  {
    int i = FlixsterApplication.getMovieRatingType();
    if (FlixsterApplication.getPlatformUsername() != null)
    {
      Resources localResources = paramContext.getResources();
      if ((this.mMovie.checkIntProperty("FRIENDS_RATED_COUNT")) && (this.mMovie.getIntProperty("FRIENDS_RATED_COUNT").intValue() > 0))
      {
        Drawable localDrawable2 = localResources.getDrawable(2130837739);
        localDrawable2.setBounds(0, 0, localDrawable2.getIntrinsicWidth(), localDrawable2.getIntrinsicHeight());
        paramTextView.setCompoundDrawables(localDrawable2, null, null, null);
        int k = this.mMovie.getIntProperty("FRIENDS_RATED_COUNT").intValue();
        StringBuilder localStringBuilder2 = new StringBuilder();
        localStringBuilder2.append(k).append(" ");
        if (k > 1)
          localStringBuilder2.append(localResources.getString(2131493008));
        while (true)
        {
          paramTextView.setText(localStringBuilder2.toString());
          paramTextView.setVisibility(0);
          return;
          localStringBuilder2.append(localResources.getString(2131493007));
        }
      }
      if ((this.mMovie.checkIntProperty("FRIENDS_WTS_COUNT")) && (this.mMovie.getIntProperty("FRIENDS_WTS_COUNT").intValue() > 0))
      {
        int j = this.mMovie.getIntProperty("FRIENDS_WTS_COUNT").intValue();
        StringBuilder localStringBuilder1 = new StringBuilder();
        if ((!this.mIsReleased.booleanValue()) && (i == 0))
        {
          paramTextView.setCompoundDrawables(null, null, null, null);
          localStringBuilder1.append("(").append(j).append(" ");
          if (j > 1)
          {
            localStringBuilder1.append(localResources.getString(2131493006));
            localStringBuilder1.append(")");
          }
        }
        while (true)
        {
          paramTextView.setText(localStringBuilder1.toString());
          paramTextView.setVisibility(0);
          return;
          localStringBuilder1.append(localResources.getString(2131493005));
          break;
          Drawable localDrawable1 = localResources.getDrawable(2130837751);
          localDrawable1.setBounds(0, 0, localDrawable1.getIntrinsicWidth(), localDrawable1.getIntrinsicHeight());
          paramTextView.setCompoundDrawables(localDrawable1, null, null, null);
          localStringBuilder1.append(j).append(" ");
          if (j > 1)
            localStringBuilder1.append(localResources.getString(2131493012));
          else
            localStringBuilder1.append(localResources.getString(2131493011));
        }
      }
      paramTextView.setVisibility(8);
      return;
    }
    paramTextView.setVisibility(8);
  }

  private void setRtScore(MovieViewItemHolder paramMovieViewItemHolder)
  {
    if (this.mMovie.checkIntProperty("rottenTomatoes"))
    {
      int i = this.mMovie.getIntProperty("rottenTomatoes").intValue();
      if ((i < 60) && (this.mIsFeatured))
      {
        paramMovieViewItemHolder.movieRtScore.setVisibility(8);
        return;
      }
      Drawable localDrawable;
      if (i < 60)
      {
        localDrawable = this.mResources.getDrawable(2130837741);
        localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
        paramMovieViewItemHolder.movieRtScore.setCompoundDrawables(localDrawable, null, null, null);
        if (this.mRtScore == null)
          if (this.mForm != 0)
            break label151;
      }
      label151: for (this.mRtScore = (i + "%"); ; this.mRtScore = (i + this.mResources.getString(2131493218)))
      {
        paramMovieViewItemHolder.movieRtScore.setText(this.mRtScore);
        return;
        localDrawable = this.mResources.getDrawable(2130837726);
        break;
      }
    }
    paramMovieViewItemHolder.movieRtScore.setVisibility(8);
  }

  public int getItemViewType()
  {
    return VIEW_TYPE_MOVIE;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    int i;
    LayoutInflater localLayoutInflater;
    label65: MovieViewItemHolder localMovieViewItemHolder;
    label421: label470: boolean bool;
    label513: ImageView localImageView2;
    label562: String str2;
    label578: String str3;
    if ((!FriendActivity.class.isInstance(paramViewGroup)) && (paramViewGroup != null))
    {
      i = 0;
      if (this.mResources == null)
        this.mResources = paramContext.getResources();
      int j = FlixsterApplication.getMovieRatingType();
      if (paramView == null)
      {
        localLayoutInflater = LayoutInflater.from(paramContext);
        if (this.mForm != 0)
          break label823;
        paramView = localLayoutInflater.inflate(2130903132, paramViewGroup, false);
      }
      localMovieViewItemHolder = (MovieViewItemHolder)paramView.getTag();
      if (localMovieViewItemHolder == null)
      {
        localMovieViewItemHolder = new MovieViewItemHolder();
        ImageView localImageView1 = (ImageView)paramView.findViewById(2131165446);
        localMovieViewItemHolder.moviePoster = localImageView1;
        TextView localTextView1 = (TextView)paramView.findViewById(2131165448);
        localMovieViewItemHolder.movieTitle = localTextView1;
        TextView localTextView2 = (TextView)paramView.findViewById(2131165452);
        localMovieViewItemHolder.movieMeta = localTextView2;
        TextView localTextView3 = (TextView)paramView.findViewById(2131165453);
        localMovieViewItemHolder.movieRelease = localTextView3;
        TextView localTextView4 = (TextView)paramView.findViewById(2131165451);
        localMovieViewItemHolder.movieActors = localTextView4;
        TextView localTextView5 = (TextView)paramView.findViewById(2131165450);
        localMovieViewItemHolder.movieRtScore = localTextView5;
        TextView localTextView6 = (TextView)paramView.findViewById(2131165449);
        localMovieViewItemHolder.movieFlixScore = localTextView6;
        TextView localTextView7 = (TextView)paramView.findViewById(2131165590);
        localMovieViewItemHolder.movieDownloadStatus = localTextView7;
        if (this.mForm == 0)
        {
          ImageView localImageView3 = (ImageView)paramView.findViewById(2131165487);
          localMovieViewItemHolder.moviePlayIcon = localImageView3;
          TextView localTextView8 = (TextView)paramView.findViewById(2131165589);
          localMovieViewItemHolder.friendRtScore = localTextView8;
          TextView localTextView9 = (TextView)paramView.findViewById(2131165587);
          localMovieViewItemHolder.friendFlixScore = localTextView9;
          LinearLayout localLinearLayout = (LinearLayout)paramView.findViewById(2131165488);
          localMovieViewItemHolder.movieDetailsLayout = localLinearLayout;
          RelativeLayout localRelativeLayout = (RelativeLayout)paramView.findViewById(2131165445);
          localMovieViewItemHolder.movieItem = localRelativeLayout;
          if (this.mDetailsClick != null)
          {
            View.OnClickListener localOnClickListener = this.mDetailsClick;
            paramView.setOnClickListener(localOnClickListener);
          }
        }
        paramView.setTag(localMovieViewItemHolder);
      }
      Review localReview = this.mReview;
      localMovieViewItemHolder.movieReview = localReview;
      if (this.mMovie.getProperty("Opening This Week") == null)
        break label837;
      localMovieViewItemHolder.movieRelease.setText(this.mMovie.getProperty("theaterReleaseDate"));
      localMovieViewItemHolder.movieRelease.setVisibility(0);
      if ((!Properties.instance().isHoneycombTablet()) || (this.right == null) || (!Drm.logic().isAssetDownloadable(this.right)))
        break label948;
      setDownloadControls(localMovieViewItemHolder.movieDownloadStatus);
      localMovieViewItemHolder.movieDownloadStatus.setVisibility(0);
      if (this.mMovie == null)
        break label1518;
      if (this.mIsReleased == null)
      {
        if ((this.mMovie.getTheaterReleaseDate() == null) || (FlixsterApplication.sToday.after(this.mMovie.getTheaterReleaseDate())))
          break label969;
        bool = false;
        this.mIsReleased = Boolean.valueOf(bool);
      }
      localImageView2 = localMovieViewItemHolder.moviePoster;
      if (this.right == null)
        break label1048;
      if (this.mForm != 1)
        break label1001;
      if (this.right.getProfilePoster() != null)
        break label975;
      localImageView2.setImageResource(2130837839);
      if (this.right == null)
        break label1271;
      str2 = this.right.getTitle();
      localMovieViewItemHolder.movieTitle.setText(str2);
      str3 = this.mMovie.getProperty("MOVIE_ACTORS_SHORT");
      if ((str3 != null) && (str3.length() != 0))
        break label1283;
      localMovieViewItemHolder.movieActors.setVisibility(8);
      label623: String str4 = this.mMovie.getProperty("meta");
      if (str4 == null)
        break label1305;
      localMovieViewItemHolder.movieMeta.setText(str4);
      localMovieViewItemHolder.movieMeta.setVisibility(0);
      label659: if (this.mForm != 0)
        break label1503;
      String str5 = this.mMovie.getProperty("high");
      if (((str5 == null) || (!str5.startsWith("http"))) && ((!this.mIsCollected) || (this.mTrailerClick == null)))
        break label1331;
      if (this.mMovie.thumbnailSoftBitmap.get() == null)
        break label1318;
      localMovieViewItemHolder.moviePlayIcon.setVisibility(0);
      localMovieViewItemHolder.moviePlayIcon.setClickable(true);
      localMovieViewItemHolder.moviePlayIcon.setTag(this.mMovie);
      localMovieViewItemHolder.moviePlayIcon.setOnClickListener(this.mTrailerClick);
      label763: switch (j)
      {
      default:
      case 0:
      case 1:
      case 2:
      }
    }
    while (true)
    {
      if (Properties.instance().isHoneycombTablet())
      {
        if (!isSelected())
          break label1530;
        paramView.setBackgroundResource(2130837616);
      }
      return paramView;
      i = 1;
      break;
      label823: paramView = localLayoutInflater.inflate(2130903118, paramViewGroup, false);
      break label65;
      label837: if ((this.mMovie.checkProperty("Top Box Office")) && (this.mMovie.checkIntProperty("boxOffice")))
      {
        Float localFloat = Float.valueOf(this.mMovie.getIntProperty("boxOffice").floatValue() / 1000000.0F);
        if (this.mGross == null)
          this.mGross = String.format("$%.1fM", new Object[] { localFloat });
        localMovieViewItemHolder.movieRelease.setText(this.mGross);
        localMovieViewItemHolder.movieRelease.setVisibility(0);
        break label421;
      }
      localMovieViewItemHolder.movieRelease.setVisibility(8);
      break label421;
      label948: if (localMovieViewItemHolder.movieDownloadStatus == null)
        break label470;
      localMovieViewItemHolder.movieDownloadStatus.setVisibility(8);
      break label470;
      label969: bool = true;
      break label513;
      label975: Bitmap localBitmap4 = this.right.getProfileBitmap(localImageView2);
      if (localBitmap4 == null)
        break label562;
      localImageView2.setImageBitmap(localBitmap4);
      break label562;
      label1001: if (this.right.getThumbnailPoster() == null)
      {
        localImageView2.setImageResource(2130837839);
        break label562;
      }
      Bitmap localBitmap3 = this.right.getThumbnailBitmap(localImageView2);
      if (localBitmap3 == null)
        break label562;
      localImageView2.setImageBitmap(localBitmap3);
      break label562;
      label1048: String str1 = this.mMovie.getThumbnailPoster();
      if (this.mForm == 1)
      {
        if (str1 == null)
        {
          localImageView2.setImageResource(2130837839);
          break label562;
        }
        Bitmap localBitmap2 = this.mMovie.getThumbnailBackedProfileBitmap(localImageView2);
        if (localBitmap2 == null)
          break label562;
        localImageView2.setImageBitmap(localBitmap2);
        break label562;
      }
      Bitmap localBitmap1 = this.mMovie.getSoftThumbnail();
      if (localBitmap1 != null)
      {
        localImageView2.setImageBitmap(localBitmap1);
        break label562;
      }
      if (str1 == null)
      {
        localImageView2.setImageResource(2130837839);
        break label562;
      }
      localImageView2.setImageResource(2130837844);
      if (i != 0)
      {
        ImageGetter.instance().get(str1, paramHandler);
        break label562;
      }
      if ((paramContext instanceof ImageTask))
      {
        long l = System.currentTimeMillis();
        if (l > 3000.0D + this.mMovie.getThumbOrderStamp())
        {
          ImageOrder localImageOrder = new ImageOrder(0, this.mMovie, str1, localImageView2, paramHandler);
          ((ImageTask)paramContext).orderImage(localImageOrder);
        }
        this.mMovie.setThumbOrderStamp(l);
        break label562;
      }
      ImageGetter.instance().get(str1, new ImageViewHandler(localImageView2, str1));
      break label562;
      label1271: str2 = this.mMovie.getTitle();
      break label578;
      label1283: localMovieViewItemHolder.movieActors.setText(str3);
      localMovieViewItemHolder.movieActors.setVisibility(0);
      break label623;
      label1305: localMovieViewItemHolder.movieMeta.setVisibility(8);
      break label659;
      label1318: localMovieViewItemHolder.moviePlayIcon.setVisibility(8);
      break label763;
      label1331: localMovieViewItemHolder.moviePlayIcon.setVisibility(8);
      break label763;
      localMovieViewItemHolder.movieFlixScore.setVisibility(0);
      localMovieViewItemHolder.movieRtScore.setVisibility(8);
      localMovieViewItemHolder.friendFlixScore.setVisibility(0);
      localMovieViewItemHolder.friendRtScore.setVisibility(8);
      setFriendScore(localMovieViewItemHolder.friendFlixScore, paramContext);
      setFlixScore(localMovieViewItemHolder);
      continue;
      localMovieViewItemHolder.movieRtScore.setVisibility(0);
      localMovieViewItemHolder.movieFlixScore.setVisibility(8);
      localMovieViewItemHolder.friendFlixScore.setVisibility(8);
      localMovieViewItemHolder.friendRtScore.setVisibility(0);
      setFriendScore(localMovieViewItemHolder.friendRtScore, paramContext);
      setRtScore(localMovieViewItemHolder);
      continue;
      localMovieViewItemHolder.movieRtScore.setVisibility(8);
      localMovieViewItemHolder.movieFlixScore.setVisibility(8);
      localMovieViewItemHolder.friendFlixScore.setVisibility(8);
      localMovieViewItemHolder.friendRtScore.setVisibility(8);
      continue;
      label1503: setFlixScore(localMovieViewItemHolder);
      setRtScore(localMovieViewItemHolder);
      continue;
      label1518: Logger.d("FlxMain", "Bad stuff is happening here...");
    }
    label1530: paramView.setBackgroundResource(2130837621);
    return paramView;
  }

  public class MovieViewItemHolder
  {
    TextView friendFlixScore;
    TextView friendRtScore;
    TextView movieActors;
    LinearLayout movieDetailsLayout;
    TextView movieDownloadStatus;
    TextView movieFlixScore;
    TextView movieHeader;
    RelativeLayout movieItem;
    TextView movieMeta;
    ImageView moviePlayIcon;
    ImageView moviePoster;
    TextView movieRelease;
    public Review movieReview = null;
    TextView movieRtScore;
    TextView movieTitle;

    public MovieViewItemHolder()
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviMovie
 * JD-Core Version:    0.6.2
 */