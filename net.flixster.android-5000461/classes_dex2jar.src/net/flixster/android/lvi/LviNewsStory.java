package net.flixster.android.lvi;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.utils.ImageTask;
import java.lang.ref.SoftReference;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.NewsStory;

@Deprecated
public class LviNewsStory extends Lvi
{
  public NewsStory mNewsStory;
  private long mThumbOrderStamp = 0L;

  public int getItemViewType()
  {
    return VIEW_TYPE_NEWSITEM;
  }

  public long getThumbOrderStamp()
  {
    return this.mThumbOrderStamp;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    RelativeLayout localRelativeLayout;
    ImageView localImageView;
    if (paramView == null)
    {
      localRelativeLayout = (RelativeLayout)LayoutInflater.from(paramContext).inflate(2130903147, null);
      ((TextView)localRelativeLayout.findViewById(2131165701)).setText(this.mNewsStory.mTitle);
      ((TextView)localRelativeLayout.findViewById(2131165702)).setText(this.mNewsStory.mDescription);
      localImageView = (ImageView)localRelativeLayout.findViewById(2131165699);
      Bitmap localBitmap = (Bitmap)this.mNewsStory.mSoftPhoto.get();
      if (localBitmap == null)
        break label111;
      localImageView.setImageBitmap(localBitmap);
    }
    label111: String str;
    long l;
    do
    {
      do
      {
        return localRelativeLayout;
        localRelativeLayout = (RelativeLayout)paramView;
        break;
        str = this.mNewsStory.mThumbnailUrl;
        localImageView.setImageResource(2130837612);
      }
      while ((str == null) || (!str.startsWith("http")));
      l = System.nanoTime();
    }
    while (l <= 3000000000.0D + this.mNewsStory.getThumbOrderStamp());
    ImageOrder localImageOrder = new ImageOrder(6, this.mNewsStory, str, localImageView, paramHandler);
    if ((paramContext instanceof ImageTask))
      ((ImageTask)paramContext).orderImageFifo(localImageOrder);
    localImageView.setImageResource(2130837627);
    this.mNewsStory.setThumbOrderStamp(l);
    return localRelativeLayout;
  }

  public void setThumbOrderStamp(long paramLong)
  {
    this.mThumbOrderStamp = paramLong;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviNewsStory
 * JD-Core Version:    0.6.2
 */