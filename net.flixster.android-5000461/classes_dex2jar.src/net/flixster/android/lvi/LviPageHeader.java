package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LviPageHeader extends Lvi
{
  public String mTitle;

  public int getItemViewType()
  {
    return VIEW_TYPE_PAGEHEADER;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    if (paramView == null);
    for (TextView localTextView = (TextView)LayoutInflater.from(paramContext).inflate(2130903148, null); ; localTextView = (TextView)paramView)
    {
      localTextView.setText(this.mTitle);
      return localTextView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviPageHeader
 * JD-Core Version:    0.6.2
 */