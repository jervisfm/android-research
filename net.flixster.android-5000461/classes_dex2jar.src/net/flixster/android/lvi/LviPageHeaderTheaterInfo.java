package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.Theater;

public class LviPageHeaderTheaterInfo extends Lvi
{
  public View.OnClickListener mStarTheaterClick;
  public Theater mTheater;
  public String mTitle;

  public int getItemViewType()
  {
    return VIEW_TYPE_PAGEHEADER;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    if (paramView == null)
      paramView = (RelativeLayout)LayoutInflater.from(paramContext).inflate(2130903114, null);
    ((TextView)paramView.findViewById(2131165438)).setText(this.mTitle);
    CheckBox localCheckBox = (CheckBox)paramView.findViewById(2131165439);
    localCheckBox.setChecked(FlixsterApplication.favoriteTheaterFlag(Long.toString(this.mTheater.getId())));
    localCheckBox.setTag(this.mTheater);
    localCheckBox.setOnClickListener(this.mStarTheaterClick);
    return paramView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviPageHeaderTheaterInfo
 * JD-Core Version:    0.6.2
 */