package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView.OnEditorActionListener;

public class LviSearchPanel extends Lvi
{
  public String mHint;
  public View.OnClickListener mOnClickListener;
  public TextView.OnEditorActionListener mOnEditorActionListener;

  public int getItemViewType()
  {
    return VIEW_TYPE_DATEPANEL;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    if (paramView == null)
      paramView = LayoutInflater.from(paramContext).inflate(2130903115, paramViewGroup, false);
    EditText localEditText = (EditText)paramView.findViewById(2131165442);
    ImageButton localImageButton = (ImageButton)paramView.findViewById(2131165441);
    localImageButton.setTag(localEditText);
    localImageButton.setOnClickListener(this.mOnClickListener);
    localEditText.setOnEditorActionListener(this.mOnEditorActionListener);
    if (this.mHint != null)
      localEditText.setHint(this.mHint);
    return paramView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviSearchPanel
 * JD-Core Version:    0.6.2
 */