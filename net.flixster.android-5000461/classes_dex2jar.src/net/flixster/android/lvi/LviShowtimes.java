package net.flixster.android.lvi;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.utils.Properties;
import net.flixster.android.model.Showtimes;
import net.flixster.android.model.Theater;

public class LviShowtimes extends Lvi
{
  public View.OnClickListener mBuyClick;
  public int mShowtimePosition;
  public Showtimes mShowtimes;
  public int mShowtimesListSize;
  public Theater mTheater;

  public int getItemViewType()
  {
    return VIEW_TYPE_SHOWTIMES;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    if (paramView == null)
      paramView = LayoutInflater.from(paramContext).inflate(2130903166, paramViewGroup, false);
    RelativeLayout localRelativeLayout = (RelativeLayout)paramView;
    TextView localTextView1 = (TextView)paramView.findViewById(2131165775);
    TextView localTextView2 = (TextView)paramView.findViewById(2131165777);
    ImageView localImageView = (ImageView)paramView.findViewById(2131165776);
    int i = paramContext.getResources().getDimensionPixelSize(2131361835);
    int j;
    if (this.mShowtimePosition > 0)
    {
      j = 0;
      localRelativeLayout.setPadding(i, j, i, i);
      if ((!this.mTheater.hasTickets()) || (this.mShowtimes == null) || (Properties.instance().isHoneycombTablet()))
        break label181;
      localImageView.setVisibility(0);
      localImageView.setOnClickListener(this.mBuyClick);
      localImageView.setTag(this);
    }
    while (true)
    {
      if (this.mShowtimes != null)
        break label191;
      localTextView1.setText(paramContext.getResources().getString(2131492952));
      localTextView2.setVisibility(8);
      paramView.setTag(this);
      return paramView;
      j = i;
      break;
      label181: localImageView.setVisibility(8);
    }
    label191: if (this.mShowtimesListSize > 1)
    {
      localTextView1.setText(this.mShowtimes.mShowtimesTitle);
      localTextView1.setVisibility(0);
    }
    while (true)
    {
      localTextView2.setVisibility(0);
      localTextView2.setText(this.mShowtimes.getTimesString());
      break;
      localTextView1.setVisibility(8);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviShowtimes
 * JD-Core Version:    0.6.2
 */