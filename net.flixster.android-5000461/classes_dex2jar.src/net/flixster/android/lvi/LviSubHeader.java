package net.flixster.android.lvi;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.flixster.android.view.SubHeader;

public class LviSubHeader extends Lvi
{
  public String mTitle;
  private int titleId;

  public LviSubHeader()
  {
  }

  public LviSubHeader(int paramInt, Activity paramActivity)
  {
    this.titleId = paramInt;
  }

  public int getItemViewType()
  {
    return LviAdapter.LviType.SUBHEADER.ordinal();
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    if (paramView == null)
      paramView = new SubHeader(paramContext);
    if (this.titleId != 0)
    {
      ((SubHeader)paramView).setText(this.titleId);
      return paramView;
    }
    ((SubHeader)paramView).setText(this.mTitle);
    return paramView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviSubHeader
 * JD-Core Version:    0.6.2
 */