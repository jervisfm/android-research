package net.flixster.android.lvi;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.flixster.android.utils.Properties;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.Theater;

public class LviTheater extends Lvi
{
  public View.OnClickListener mStarTheaterClick;
  public Theater mTheater;
  public View.OnClickListener mTheaterClick;

  public int getItemViewType()
  {
    return VIEW_TYPE_THEATER;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    TheaterItemHolder localTheaterItemHolder;
    if (paramView == null)
    {
      paramView = LayoutInflater.from(paramContext).inflate(2130903165, paramViewGroup, false);
      localTheaterItemHolder = new TheaterItemHolder();
      localTheaterItemHolder.showtimesTheater = ((TextView)paramView.findViewById(2131165771));
      localTheaterItemHolder.showtimesTheaterAddress = ((TextView)paramView.findViewById(2131165773));
      localTheaterItemHolder.showtimesTicketIcon = ((ImageView)paramView.findViewById(2131165770));
      localTheaterItemHolder.showtimesStar = ((CheckBox)paramView.findViewById(2131165772));
      if (!this.mTheater.hasTickets())
        break label239;
      localTheaterItemHolder.showtimesTicketIcon.setVisibility(0);
      label101: localTheaterItemHolder.showtimesTheater.setText(this.mTheater.getProperty("name"));
      localTheaterItemHolder.showtimesTheaterAddress.setText(this.mTheater.getProperty("address"));
      if (!Properties.instance().isHoneycombTablet())
        break label261;
      localTheaterItemHolder.showtimesStar.setVisibility(8);
      localTheaterItemHolder.lviTheater = this;
      if (!isSelected())
        break label252;
      paramView.setBackgroundResource(2130837616);
      label173: int i = paramContext.getResources().getDimensionPixelOffset(2131361890);
      paramView.setPadding(i, i, i, i);
    }
    while (true)
    {
      localTheaterItemHolder.theater = this.mTheater;
      paramView.setClickable(true);
      paramView.setTag(localTheaterItemHolder);
      paramView.setOnClickListener(this.mTheaterClick);
      return paramView;
      localTheaterItemHolder = (TheaterItemHolder)paramView.getTag();
      break;
      label239: localTheaterItemHolder.showtimesTicketIcon.setVisibility(8);
      break label101;
      label252: paramView.setBackgroundResource(2130837621);
      break label173;
      label261: localTheaterItemHolder.showtimesStar.setChecked(FlixsterApplication.favoriteTheaterFlag(Long.toString(this.mTheater.getId())));
      localTheaterItemHolder.showtimesStar.setTag(this.mTheater);
      localTheaterItemHolder.showtimesStar.setOnClickListener(this.mStarTheaterClick);
    }
  }

  public boolean isEnabled()
  {
    return true;
  }

  public static class TheaterItemHolder
  {
    public LviTheater lviTheater;
    CheckBox showtimesStar;
    TextView showtimesTheater;
    TextView showtimesTheaterAddress;
    ImageView showtimesTicketIcon;
    public Theater theater;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviTheater
 * JD-Core Version:    0.6.2
 */