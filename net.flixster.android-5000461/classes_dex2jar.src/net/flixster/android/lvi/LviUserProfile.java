package net.flixster.android.lvi;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.User;

public class LviUserProfile extends Lvi
{
  public User mUser;

  public int getItemViewType()
  {
    return VIEW_TYPE_USERPROFILE;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    Resources localResources = paramContext.getResources();
    if (paramView == null)
      paramView = LayoutInflater.from(paramContext).inflate(2130903150, paramViewGroup, false);
    ProfileViewHolder localProfileViewHolder = (ProfileViewHolder)paramView.getTag();
    if (localProfileViewHolder == null)
    {
      localProfileViewHolder = new ProfileViewHolder(null);
      localProfileViewHolder.thumbnailView = ((ImageView)paramView.findViewById(2131165706));
      localProfileViewHolder.thumbnailFrameView = ((ImageView)paramView.findViewById(2131165707));
      localProfileViewHolder.titleView = ((TextView)paramView.findViewById(2131165708));
      localProfileViewHolder.friendView = ((TextView)paramView.findViewById(2131165349));
      localProfileViewHolder.ratingView = ((TextView)paramView.findViewById(2131165348));
      localProfileViewHolder.wtsView = ((TextView)paramView.findViewById(2131165347));
      localProfileViewHolder.collectionsView = ((TextView)paramView.findViewById(2131165350));
      paramView.setTag(localProfileViewHolder);
    }
    String str1 = FlixsterApplication.getPlatform();
    if ("FLX".equals(str1))
      localProfileViewHolder.thumbnailFrameView.setBackgroundResource(2130837692);
    while (true)
    {
      Bitmap localBitmap = this.mUser.getProfileBitmap(localProfileViewHolder.thumbnailView);
      if (localBitmap != null)
        localProfileViewHolder.thumbnailView.setImageBitmap(localBitmap);
      String str2 = this.mUser.displayName;
      if (str2 != null)
      {
        localProfileViewHolder.titleView.setText(str2);
        localProfileViewHolder.titleView.setVisibility(0);
      }
      int i = this.mUser.friendCount;
      localProfileViewHolder.friendView.setText(new StringBuilder("").append(i).append(" ").append(localResources.getString(2131492994)));
      int j = this.mUser.ratingCount;
      localProfileViewHolder.ratingView.setText(new StringBuilder("").append(j).append(" ").append(localResources.getString(2131492995)));
      int k = this.mUser.wtsCount;
      localProfileViewHolder.wtsView.setText(new StringBuilder("").append(k).append(" ").append(localResources.getString(2131492996)));
      int m = this.mUser.collectionsCount;
      if (m > 0)
      {
        localProfileViewHolder.collectionsView.setText(new StringBuilder("").append(m).append(" ").append(localResources.getString(2131492997)));
        localProfileViewHolder.collectionsView.setVisibility(0);
      }
      return paramView;
      if ("FBK".equals(str1))
        localProfileViewHolder.thumbnailFrameView.setBackgroundResource(2130837833);
    }
  }

  private static final class ProfileViewHolder
  {
    TextView collectionsView;
    TextView friendView;
    TextView ratingView;
    ImageView thumbnailFrameView;
    ImageView thumbnailView;
    TextView titleView;
    TextView wtsView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviUserProfile
 * JD-Core Version:    0.6.2
 */