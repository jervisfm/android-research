package net.flixster.android.lvi;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;

public class LviWrapper extends Lvi
{
  private final View view;
  private final int viewType;

  private LviWrapper(View paramView, int paramInt)
  {
    this.view = paramView;
    this.viewType = paramInt;
  }

  public static Lvi convertToLvi(View paramView, int paramInt)
  {
    return new LviWrapper(paramView, paramInt);
  }

  public int getItemViewType()
  {
    return this.viewType;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup, Context paramContext, Handler paramHandler)
  {
    return this.view;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.lvi.LviWrapper
 * JD-Core Version:    0.6.2
 */