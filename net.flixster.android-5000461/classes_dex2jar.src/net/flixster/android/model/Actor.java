package net.flixster.android.model;

import android.graphics.Bitmap;
import java.util.ArrayList;
import java.util.Date;

public class Actor
{
  public static final String KEY_ACTOR_ID = "ACTOR_ID";
  public static final String KEY_ACTOR_NAME = "ACTOR_NAME";
  public String biography;
  public Date birthDate;
  public String birthDay;
  public String birthplace;
  public Bitmap bitmap;
  public String chars;
  public String flixsterUrl;
  public long id;
  public String largeUrl;
  public ArrayList<Movie> movies;
  public String name;
  public int photoCount;
  public ArrayList<Photo> photos;
  public int positionId;
  public String thumbnailUrl;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Actor
 * JD-Core Version:    0.6.2
 */