package net.flixster.android.model;

public class Contact
{
  public String familyName;
  public String givenName;
  public String middleName;
  public String name;
  public String number;

  public Contact(String paramString)
  {
    this.name = paramString;
  }

  public Contact(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    this.name = paramString1;
    this.givenName = paramString2;
    this.middleName = paramString3;
    this.familyName = paramString4;
    this.number = paramString5;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Contact
 * JD-Core Version:    0.6.2
 */