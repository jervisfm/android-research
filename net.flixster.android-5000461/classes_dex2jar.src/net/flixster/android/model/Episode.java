package net.flixster.android.model;

import org.json.JSONObject;

public class Episode extends VideoAsset
{
  public static final int EDITION_ID = 1000;
  private int episodeNum;
  private String synopsis;

  protected Episode()
  {
    this.editionId = 1000L;
  }

  public Episode(long paramLong)
  {
    this();
    this.id = paramLong;
  }

  protected Episode(JSONObject paramJSONObject)
  {
    this();
    merge(paramJSONObject);
  }

  public int getEpisodeNumber()
  {
    return this.episodeNum;
  }

  public String getSynopsis()
  {
    return this.synopsis;
  }

  protected void merge(JSONObject paramJSONObject)
  {
    this.id = paramJSONObject.optLong("id", this.id);
    this.episodeNum = paramJSONObject.optInt("episode", this.episodeNum);
    this.title = paramJSONObject.optString("title", this.title);
    this.synopsis = paramJSONObject.optString("synopsis", this.synopsis);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Episode
 * JD-Core Version:    0.6.2
 */