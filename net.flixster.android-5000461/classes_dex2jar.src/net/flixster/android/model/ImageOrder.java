package net.flixster.android.model;

import android.os.Handler;
import android.view.View;

public class ImageOrder
{
  public static final int TYPE_ACTOR_PHOTO = 4;
  public static final int TYPE_GALLERY_PHOTO = 8;
  public static final int TYPE_GALLERY_THUMB = 9;
  public static final int TYPE_MOVIE_PHOTO = 3;
  public static final int TYPE_MOVIE_THUMB = 0;
  public static final int TYPE_NETFLIX_THUMB = 7;
  public static final int TYPE_REVIEW_MUG = 1;
  public static final int TYPE_STORY_THUMB = 6;
  public static final int TYPE_USER_PHOTO = 5;
  public View movieView;
  public Handler refreshHandler;
  public Object tag;
  public int type;
  public String urlString;

  public ImageOrder(int paramInt, Object paramObject, String paramString, View paramView, Handler paramHandler)
  {
    this.type = paramInt;
    this.tag = paramObject;
    this.urlString = paramString;
    this.movieView = paramView;
    this.refreshHandler = paramHandler;
  }

  public ImageOrder(String paramString)
  {
    this.urlString = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.ImageOrder
 * JD-Core Version:    0.6.2
 */