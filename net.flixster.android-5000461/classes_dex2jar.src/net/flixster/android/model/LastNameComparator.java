package net.flixster.android.model;

import java.util.Comparator;

public class LastNameComparator
  implements Comparator<User>
{
  public int compare(User paramUser1, User paramUser2)
  {
    String str1 = paramUser1.lastName;
    String str2 = paramUser2.lastName;
    if (str1 == null)
      str1 = "";
    if (str2 == null)
      str2 = "";
    return str1.compareTo(str2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.LastNameComparator
 * JD-Core Version:    0.6.2
 */