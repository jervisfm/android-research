package net.flixster.android.model;

public class Location
{
  public String city;
  public String country;
  public String countryCode;
  public double latitude;
  public double longitude;
  public String state;
  public String stateCode;
  public int weight;
  public String zip;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Location
 * JD-Core Version:    0.6.2
 */