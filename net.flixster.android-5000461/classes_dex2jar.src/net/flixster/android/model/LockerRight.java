package net.flixster.android.model;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.widget.ImageView;
import com.flixster.android.activity.ImageViewHandler;
import com.flixster.android.model.Image;
import com.flixster.android.net.DownloadHelper;
import com.flixster.android.utils.Properties;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.MovieDao;
import org.json.JSONObject;

public class LockerRight
{
  private static final String POSTER_NONE_SUBSTRING = "movie.none.";
  private static long SEASON_RIGHT_ID_BASE = 800000000000000000L;
  private VideoAsset asset;
  public final long assetId;
  private HashMap<Long, Long> childAssetToRightIdMap;
  private Image detailedImage;
  protected final String detailedUrl;
  private String downloadCaptionsUri;
  private String downloadSize;
  private int downloadSizeRaw;
  protected String downloadUri;
  private String downloadedSize;
  private int downloadsRemain = 1;
  public final boolean isDownloadSupported;
  public boolean isRental;
  public final boolean isStreamingSupported;
  private Image profileImage;
  protected final String profileUrl;
  public PurchaseType purchaseType;
  public Date rentalExpiration;
  public final long rightId;
  protected final String rightTitle;
  public Source source;
  private Image thumbnailImage;
  protected final String thumbnailUrl;
  protected final RightType type;
  public Date viewingExpiration;

  private LockerRight(long paramLong1, String paramString1, String paramString2, long paramLong2)
  {
    this.rightId = paramLong1;
    this.rightTitle = paramString1;
    this.type = RightType.match(paramString2);
    this.assetId = paramLong2;
    this.source = null;
    this.isStreamingSupported = true;
    this.isDownloadSupported = true;
    this.detailedUrl = null;
    this.profileUrl = null;
    this.thumbnailUrl = null;
  }

  public LockerRight(JSONObject paramJSONObject, long paramLong, RightType paramRightType, VideoAsset paramVideoAsset)
  {
    this.rightId = getRightId(paramJSONObject.optLong("id"), paramRightType);
    this.rightTitle = paramJSONObject.optString("title");
    this.assetId = paramLong;
    this.type = paramRightType;
    boolean bool1;
    boolean bool3;
    label75: String str4;
    String str5;
    label174: String str1;
    label212: String str2;
    label243: String str3;
    if (isSeason())
    {
      bool1 = false;
      this.isStreamingSupported = bool1;
      boolean bool2 = isSeason();
      bool3 = false;
      if (!bool2)
        break label298;
      this.isDownloadSupported = bool3;
      this.source = Source.match(paramJSONObject.optString("source"));
      this.purchaseType = PurchaseType.match(paramJSONObject.optString("purchaseType"));
      this.isRental = "1".equals(paramJSONObject.optString("isRental"));
      if (this.isRental)
      {
        str4 = paramJSONObject.optString("viewingExpiration");
        str5 = paramJSONObject.optString("rentalExpiration");
        if (("".equals(str4)) || (!"".equals(str5)))
          break label309;
        this.viewingExpiration = parse(str4);
      }
      JSONObject localJSONObject = paramJSONObject.optJSONObject("poster");
      if (localJSONObject == null)
        break label413;
      str1 = localJSONObject.optString("thumbnail", null);
      if ((str1 == null) || (str1.contains("movie.none.")))
        break label395;
      this.thumbnailUrl = str1;
      str2 = localJSONObject.optString("profile", null);
      if ((str2 == null) || (str2.contains("movie.none.")))
        break label401;
      this.profileUrl = str2;
      str3 = localJSONObject.optString("detailed", null);
      if ((str3 == null) || (str3.contains("movie.none.")))
        break label407;
      label274: this.detailedUrl = str3;
    }
    while (true)
    {
      this.asset = paramVideoAsset;
      return;
      bool1 = paramJSONObject.optBoolean("streamingSupported");
      break;
      label298: bool3 = paramJSONObject.optBoolean("downloadSupported");
      break label75;
      label309: if (("".equals(str4)) && (!"".equals(str5)))
      {
        this.rentalExpiration = parse(str5);
        break label174;
      }
      if (("".equals(str4)) || ("".equals(str5)))
        break label174;
      if (str4.equals(str5))
      {
        this.rentalExpiration = parse(str5);
        break label174;
      }
      this.viewingExpiration = parse(str4);
      break label174;
      label395: str1 = null;
      break label212;
      label401: str2 = null;
      break label243;
      label407: str3 = null;
      break label274;
      label413: this.detailedUrl = null;
      this.profileUrl = null;
      this.thumbnailUrl = null;
    }
  }

  private Movie getAssetReference()
  {
    if (isMovie())
      return MovieDao.getMovie(this.assetId);
    if (isSeason())
      return MovieDao.getSeason(this.assetId);
    if (isUnfulfillable())
      return MovieDao.getUnfulfillable(this.assetId);
    return null;
  }

  public static LockerRight getMockInstance(long paramLong1, String paramString1, String paramString2, long paramLong2)
  {
    return new LockerRight(paramLong1, paramString1, paramString2, paramLong2);
  }

  public static long getRawSeasonRightId(long paramLong)
  {
    if (paramLong > SEASON_RIGHT_ID_BASE)
      paramLong -= SEASON_RIGHT_ID_BASE;
    return paramLong;
  }

  private static long getRightId(long paramLong, RightType paramRightType)
  {
    if (paramRightType == RightType.SEASON)
      paramLong += SEASON_RIGHT_ID_BASE;
    return paramLong;
  }

  private static Date parse(String paramString)
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
    try
    {
      Date localDate = localSimpleDateFormat.parse(paramString);
      return localDate;
    }
    catch (ParseException localParseException)
    {
      localParseException.printStackTrace();
    }
    return null;
  }

  public void addChildRightId(long paramLong1, long paramLong2)
  {
    if (this.childAssetToRightIdMap == null)
      this.childAssetToRightIdMap = new HashMap(30);
    this.childAssetToRightIdMap.put(Long.valueOf(paramLong2), Long.valueOf(paramLong1));
  }

  public Movie getAsset()
  {
    if (isEpisode())
      return null;
    return (Movie)this.asset;
  }

  public long getChildRightId(long paramLong)
  {
    if (this.childAssetToRightIdMap == null);
    Long localLong;
    do
    {
      return 0L;
      localLong = (Long)this.childAssetToRightIdMap.get(Long.valueOf(paramLong));
    }
    while (localLong == null);
    return localLong.longValue();
  }

  public Collection<Long> getChildRightIds()
  {
    if (this.childAssetToRightIdMap == null)
      return new ArrayList(0);
    return this.childAssetToRightIdMap.values();
  }

  public <T extends ImageView> Bitmap getDetailBitmap(T paramT)
  {
    if (this.detailedImage == null)
      this.detailedImage = new Image(getDetailPoster());
    return this.detailedImage.getBitmap(paramT);
  }

  public Bitmap getDetailBitmap(ImageViewHandler paramImageViewHandler)
  {
    if (this.detailedImage == null)
      this.detailedImage = new Image(getDetailPoster());
    return this.detailedImage.getBitmap(paramImageViewHandler);
  }

  public String getDetailPoster()
  {
    if (this.detailedUrl != null)
      return this.detailedUrl;
    Movie localMovie = getAssetReference();
    if (localMovie != null)
      return localMovie.getDetailPoster();
    return null;
  }

  public long getDownloadAssetBitrate()
  {
    if ((Properties.instance().isHoneycombTablet()) && (!isSonicProxyMode()));
    for (int i = 1000000; ; i = 700000)
      return i;
  }

  public String getDownloadAssetSize()
  {
    return this.downloadSize;
  }

  public long getDownloadAssetSizeRaw()
  {
    return this.downloadSizeRaw;
  }

  public String getDownloadCaptionsUri()
  {
    return this.downloadCaptionsUri;
  }

  public String getDownloadUri()
  {
    return this.downloadUri;
  }

  public String getDownloadedAssetSize()
  {
    long l;
    if (this.downloadedSize == null)
    {
      l = DownloadHelper.findDownloadedMovieSize(this.rightId);
      if (l <= 0L)
        break label58;
    }
    label58: for (String str = " (" + DownloadHelper.readableFileSize(l) + ")"; ; str = "")
    {
      this.downloadedSize = str;
      return this.downloadedSize;
    }
  }

  public int getDownloadsRemain()
  {
    return this.downloadsRemain;
  }

  public int getEstimatedDownloadFileSize()
  {
    if (this.type == RightType.MOVIE)
    {
      String str = getAsset().getProperty("runningTime");
      long l1 = 0L;
      long l2 = 0L;
      if (str != null)
      {
        if (str.contains("hr."))
        {
          l1 = Integer.valueOf(str.substring(0, str.indexOf(" "))).intValue();
          if (str.contains("min."))
            str = str.substring(2 + str.indexOf("."));
        }
        if (str.contains("min."))
          l2 = Integer.valueOf(str.substring(0, str.indexOf(" "))).intValue();
      }
      return (int)()(1.17D * (60L * (l2 + 60L * l1) * getDownloadAssetBitrate()) / 8.0D);
    }
    if (this.type == RightType.EPISODE)
      return (int)()(1.17D * (60L * (45L + 60L * 0L) * getDownloadAssetBitrate()) / 8.0D);
    return 0;
  }

  public <T extends ImageView> Bitmap getProfileBitmap(T paramT)
  {
    if (this.profileImage == null)
      this.profileImage = new Image(getProfilePoster());
    return this.profileImage.getBitmap(paramT);
  }

  public String getProfilePoster()
  {
    if (this.profileUrl != null)
      return this.profileUrl;
    Movie localMovie = getAssetReference();
    if (localMovie != null)
      return localMovie.getProfilePoster();
    return null;
  }

  public String getRentalExpirationString(boolean paramBoolean)
  {
    Date localDate1 = new Date(System.currentTimeMillis());
    Date localDate2 = this.rentalExpiration;
    String str1 = null;
    long l3;
    long l4;
    long l5;
    int i;
    String str2;
    Object[] arrayOfObject;
    String str4;
    if (localDate2 != null)
    {
      long l1 = this.rentalExpiration.getTime() - localDate1.getTime();
      boolean bool = l1 < 0L;
      str1 = null;
      if (bool)
      {
        long l2 = l1 / 1000L;
        l3 = l2 / 60L;
        l4 = l3 / 60L;
        l5 = l4 / 24L;
        Resources localResources = FlixsterApplication.getContext().getResources();
        if (!paramBoolean)
          break label177;
        i = 2131493249;
        str2 = localResources.getString(i);
        arrayOfObject = new Object[1];
        if (l5 != 0L)
          break label237;
        if (l4 != 0L)
          break label211;
        if (l3 != 0L)
          break label185;
        str4 = l2 + " sec";
      }
    }
    while (true)
    {
      arrayOfObject[0] = str4;
      str1 = String.format(str2, arrayOfObject);
      return str1;
      label177: i = 2131493248;
      break;
      label185: str4 = l3 + " min";
      continue;
      label211: str4 = l4 + " hr";
    }
    label237: StringBuilder localStringBuilder = new StringBuilder(String.valueOf(l5));
    if (l5 == 1L);
    for (String str3 = " day"; ; str3 = " days")
    {
      str4 = str3;
      break;
    }
  }

  public RightType getRightType()
  {
    return this.type;
  }

  public <T extends ImageView> Bitmap getThumbnailBitmap(T paramT)
  {
    if (this.thumbnailImage == null)
      this.thumbnailImage = new Image(getThumbnailPoster());
    return this.thumbnailImage.getBitmap(paramT);
  }

  public String getThumbnailPoster()
  {
    if (this.thumbnailUrl != null)
      return this.thumbnailUrl;
    Movie localMovie = getAssetReference();
    if (localMovie != null)
      return localMovie.getThumbnailPoster();
    return null;
  }

  public String getTitle()
  {
    if (!"".equals(this.rightTitle))
      return this.rightTitle;
    return this.asset.getTitle();
  }

  public String getViewingExpirationString()
  {
    Date localDate1 = new Date(System.currentTimeMillis());
    Date localDate2 = this.viewingExpiration;
    String str1 = null;
    long l3;
    long l4;
    String str2;
    Object[] arrayOfObject;
    String str3;
    if (localDate2 != null)
    {
      long l1 = this.viewingExpiration.getTime() - localDate1.getTime();
      boolean bool = l1 < 0L;
      str1 = null;
      if (bool)
      {
        long l2 = l1 / 1000L;
        l3 = l2 / 60L;
        l4 = l3 / 60L;
        str2 = FlixsterApplication.getContext().getResources().getString(2131493247);
        arrayOfObject = new Object[1];
        if (l4 != 0L)
          break label172;
        if (l3 != 0L)
          break label146;
        str3 = l2 + " sec";
      }
    }
    while (true)
    {
      arrayOfObject[0] = str3;
      str1 = String.format(str2, arrayOfObject);
      return str1;
      label146: str3 = l3 + " min";
      continue;
      label172: str3 = l4 + " hr";
    }
  }

  public boolean isEpisode()
  {
    return this.type == RightType.EPISODE;
  }

  public boolean isMovie()
  {
    return this.type == RightType.MOVIE;
  }

  public boolean isSeason()
  {
    return this.type == RightType.SEASON;
  }

  public boolean isSonicProxyMode()
  {
    return this.source == Source.SONIC;
  }

  public boolean isUnfulfillable()
  {
    return this.type == RightType.UNFULFILLABLE;
  }

  public void setDownloadAssetSize(int paramInt)
  {
    int i;
    if (paramInt > 0)
    {
      i = paramInt;
      this.downloadSizeRaw = i;
      if (paramInt <= 0)
        break label54;
    }
    label54: for (String str = "(" + DownloadHelper.readableFileSize(paramInt) + ")"; ; str = "")
    {
      this.downloadSize = str;
      return;
      i = 0;
      break;
    }
  }

  public void setDownloadCaptionsUri(String paramString)
  {
    this.downloadCaptionsUri = paramString;
  }

  public void setDownloadUri(String paramString)
  {
    this.downloadUri = paramString;
  }

  public void setDownloadsRemain(int paramInt)
  {
    this.downloadsRemain = paramInt;
  }

  public void setSonicProxyMode()
  {
    this.source = Source.SONIC;
  }

  public String toString()
  {
    return "{ rightId: " + this.rightId + ", assetId: " + this.assetId + ", source: " + this.source + ", type: " + this.type + ", isStreamingSupported: " + this.isStreamingSupported + ", isDownloadSupported: " + this.isDownloadSupported + " }";
  }

  public static enum PurchaseType
  {
    static
    {
      SMS = new PurchaseType("SMS", 2);
      FB_INVITE = new PurchaseType("FB_INVITE", 3);
      UV_CREATE = new PurchaseType("UV_CREATE", 4);
      INSTALL_MOBILE = new PurchaseType("INSTALL_MOBILE", 5);
      UNKNOWN = new PurchaseType("UNKNOWN", 6);
      PurchaseType[] arrayOfPurchaseType = new PurchaseType[7];
      arrayOfPurchaseType[0] = RATE;
      arrayOfPurchaseType[1] = WTS;
      arrayOfPurchaseType[2] = SMS;
      arrayOfPurchaseType[3] = FB_INVITE;
      arrayOfPurchaseType[4] = UV_CREATE;
      arrayOfPurchaseType[5] = INSTALL_MOBILE;
      arrayOfPurchaseType[6] = UNKNOWN;
    }

    protected static PurchaseType match(String paramString)
    {
      if ("MKR".equals(paramString))
        return RATE;
      if ("MKW".equals(paramString))
        return WTS;
      if (paramString.startsWith("MT"))
        return SMS;
      if (paramString.startsWith("MR"))
        return FB_INVITE;
      if (("MSK".equals(paramString)) || ("MAN".equals(paramString)) || ("MIP".equals(paramString)) || ("MID".equals(paramString)))
        return UV_CREATE;
      if (("MPN".equals(paramString)) || ("MPW".equals(paramString)))
        return INSTALL_MOBILE;
      return UNKNOWN;
    }
  }

  public static enum RightType
  {
    static
    {
      EPISODE = new RightType("EPISODE", 2);
      UNFULFILLABLE = new RightType("UNFULFILLABLE", 3);
      UNKNOWN = new RightType("UNKNOWN", 4);
      RightType[] arrayOfRightType = new RightType[5];
      arrayOfRightType[0] = MOVIE;
      arrayOfRightType[1] = SEASON;
      arrayOfRightType[2] = EPISODE;
      arrayOfRightType[3] = UNFULFILLABLE;
      arrayOfRightType[4] = UNKNOWN;
    }

    protected static RightType match(String paramString)
    {
      if (MOVIE.name().equals(paramString))
        return MOVIE;
      if (EPISODE.name().equals(paramString))
        return EPISODE;
      return UNKNOWN;
    }
  }

  public static enum Source
  {
    static
    {
      SONIC = new Source("SONIC", 1);
      UNKNOWN = new Source("UNKNOWN", 2);
      Source[] arrayOfSource = new Source[3];
      arrayOfSource[0] = WB;
      arrayOfSource[1] = SONIC;
      arrayOfSource[2] = UNKNOWN;
    }

    protected static Source match(String paramString)
    {
      if ("sonic".equals(paramString))
        return SONIC;
      if ("wb".equals(paramString))
        return WB;
      return UNKNOWN;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.LockerRight
 * JD-Core Version:    0.6.2
 */