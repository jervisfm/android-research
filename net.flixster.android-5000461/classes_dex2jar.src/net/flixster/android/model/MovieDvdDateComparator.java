package net.flixster.android.model;

import java.util.Comparator;
import java.util.Date;

public class MovieDvdDateComparator
  implements Comparator<Movie>
{
  public int compare(Movie paramMovie1, Movie paramMovie2)
  {
    if (paramMovie1.getDvdReleaseDate().after(paramMovie2.getDvdReleaseDate()))
      return 1;
    if (paramMovie1.getDvdReleaseDate().before(paramMovie2.getDvdReleaseDate()))
      return -1;
    return 0;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.MovieDvdDateComparator
 * JD-Core Version:    0.6.2
 */