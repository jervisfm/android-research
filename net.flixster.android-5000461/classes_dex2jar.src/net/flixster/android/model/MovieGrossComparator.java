package net.flixster.android.model;

import java.util.Comparator;

public class MovieGrossComparator
  implements Comparator<Movie>
{
  public int compare(Movie paramMovie1, Movie paramMovie2)
  {
    Integer localInteger1 = paramMovie1.getIntProperty("boxOffice");
    Integer localInteger2 = paramMovie2.getIntProperty("boxOffice");
    if (localInteger1 == null)
      localInteger1 = Integer.valueOf(0);
    if (localInteger2 == null)
      localInteger2 = Integer.valueOf(0);
    return localInteger2.intValue() - localInteger1.intValue();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.MovieGrossComparator
 * JD-Core Version:    0.6.2
 */