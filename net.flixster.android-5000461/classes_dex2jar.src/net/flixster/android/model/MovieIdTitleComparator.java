package net.flixster.android.model;

import java.util.Comparator;
import net.flixster.android.data.MovieDao;

public class MovieIdTitleComparator
  implements Comparator<Long>
{
  public int compare(Long paramLong1, Long paramLong2)
  {
    return MovieDao.getMovie(paramLong1.longValue()).getProperty("title").compareTo(MovieDao.getMovie(paramLong2.longValue()).getProperty("title"));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.MovieIdTitleComparator
 * JD-Core Version:    0.6.2
 */