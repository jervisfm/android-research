package net.flixster.android.model;

import java.util.Comparator;

public class MovieRottenScoreComparator
  implements Comparator<Movie>
{
  public int compare(Movie paramMovie1, Movie paramMovie2)
  {
    Integer localInteger1 = paramMovie1.getIntProperty("rottenTomatoes");
    Integer localInteger2 = paramMovie2.getIntProperty("rottenTomatoes");
    if (localInteger1 == null)
      localInteger1 = Integer.valueOf(0);
    if (localInteger2 == null)
      localInteger2 = Integer.valueOf(0);
    int k;
    if (localInteger1.intValue() > localInteger2.intValue())
      k = -1;
    int i;
    int j;
    do
    {
      return k;
      i = localInteger1.intValue();
      j = localInteger2.intValue();
      k = 0;
    }
    while (i >= j);
    return 1;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.MovieRottenScoreComparator
 * JD-Core Version:    0.6.2
 */