package net.flixster.android.model;

import java.util.Comparator;
import java.util.Date;

public class MovieTheaterDateComparator
  implements Comparator<Movie>
{
  public int compare(Movie paramMovie1, Movie paramMovie2)
  {
    if (paramMovie1.getTheaterReleaseDate().after(paramMovie2.getTheaterReleaseDate()))
      return 1;
    if (paramMovie1.getTheaterReleaseDate().before(paramMovie2.getTheaterReleaseDate()))
      return -1;
    return 0;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.MovieTheaterDateComparator
 * JD-Core Version:    0.6.2
 */