package net.flixster.android.model;

import java.io.IOException;

public class MovieTicketException extends IOException
{
  private static final long serialVersionUID = 6291489791057408938L;

  public MovieTicketException()
  {
  }

  public MovieTicketException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.MovieTicketException
 * JD-Core Version:    0.6.2
 */