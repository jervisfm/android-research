package net.flixster.android.model;

public class MovieTicketPurchaseException extends MovieTicketException
{
  private static final long serialVersionUID = -5231666587827392415L;

  public MovieTicketPurchaseException()
  {
  }

  public MovieTicketPurchaseException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.MovieTicketPurchaseException
 * JD-Core Version:    0.6.2
 */