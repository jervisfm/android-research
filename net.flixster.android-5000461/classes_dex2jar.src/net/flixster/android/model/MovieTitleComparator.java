package net.flixster.android.model;

import java.util.Comparator;

public class MovieTitleComparator
  implements Comparator<Movie>
{
  public int compare(Movie paramMovie1, Movie paramMovie2)
  {
    String str1 = paramMovie1.getProperty("title");
    String str2 = paramMovie2.getProperty("title");
    if (str1 == null)
      str1 = "";
    if (str2 == null)
      str2 = "";
    return str1.compareTo(str2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.MovieTitleComparator
 * JD-Core Version:    0.6.2
 */