package net.flixster.android.model;

import org.json.JSONException;
import org.json.JSONObject;

public class NetflixError
{
  public int mStatusCode = 0;
  public int mSubCode = 0;
  public String message;

  public static NetflixError parseFromJSONString(String paramString)
    throws JSONException
  {
    NetflixError localNetflixError = new NetflixError();
    JSONObject localJSONObject1 = new JSONObject(paramString);
    if (localJSONObject1.has("status"))
    {
      JSONObject localJSONObject2 = localJSONObject1.getJSONObject("status");
      if (localJSONObject2.has("sub_code"))
        localNetflixError.mSubCode = Integer.parseInt(localJSONObject2.getString("sub_code"));
      if (localJSONObject2.has("status_code"))
        localNetflixError.mStatusCode = Integer.parseInt(localJSONObject2.getString("status_code"));
      if (localJSONObject2.has("message"))
        localNetflixError.message = localJSONObject2.getString("message");
    }
    return localNetflixError;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.NetflixError
 * JD-Core Version:    0.6.2
 */