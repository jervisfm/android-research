package net.flixster.android.model;

import java.io.IOException;

public class NetflixException extends IOException
{
  private static final long serialVersionUID = 1L;
  public String mMessage;

  public NetflixException()
  {
  }

  public NetflixException(String paramString)
  {
    super(paramString);
    this.mMessage = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.NetflixException
 * JD-Core Version:    0.6.2
 */