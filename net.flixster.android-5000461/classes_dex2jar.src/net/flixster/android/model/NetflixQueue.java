package net.flixster.android.model;

import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import net.flixster.android.FlixUtils;
import net.flixster.android.FlixsterApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NetflixQueue
{
  public static final String NF_ATHOME = "at_home";
  public static final String NF_ATHOMEITEM = "at_home_item";
  public static final String NF_ETAG = "etag";
  public static final String NF_NUMBER_OF_RESULTS = "number_of_results";
  public static final String NF_QUEUE = "queue";
  public static final String NF_QUEUEITEM = "queue_item";
  public static final String NF_RESULTS_PER_PAGE = "results_per_page";
  public static final String NF_START_INDEX = "start_index";
  private ArrayList<NetflixQueueItem> mNetflixQueueItemList;
  private int numberOfResults = 0;
  private int resultsPerPage = 0;
  private int startIndex = 0;

  public ArrayList<NetflixQueueItem> getNetflixQueueItemList()
  {
    return this.mNetflixQueueItemList;
  }

  public int getNumberOfResults()
  {
    return this.numberOfResults;
  }

  public int getResultsPerPage()
  {
    return this.resultsPerPage;
  }

  public int getStartIndex()
  {
    return this.startIndex;
  }

  public NetflixQueue parseFromJson(JSONObject paramJSONObject, boolean paramBoolean)
    throws JSONException
  {
    this.mNetflixQueueItemList = new ArrayList();
    Logger.d("FlxMain", "NetflixQueue.parseFromJson");
    JSONObject localJSONObject4;
    JSONArray localJSONArray2;
    int m;
    if (paramJSONObject.has("queue"))
    {
      localJSONObject4 = paramJSONObject.getJSONObject("queue");
      if (localJSONObject4.has("etag"))
      {
        String str2 = FlixUtils.copyString(localJSONObject4.getString("etag"));
        localJSONObject4.put("etag", str2);
        FlixsterApplication.setNetflixEtag(str2);
      }
      if (localJSONObject4.has("start_index"))
        this.startIndex = localJSONObject4.getInt("start_index");
      if (localJSONObject4.has("results_per_page"))
        this.resultsPerPage = localJSONObject4.getInt("results_per_page");
      if (localJSONObject4.has("number_of_results"))
        this.numberOfResults = localJSONObject4.getInt("number_of_results");
      if (localJSONObject4.has("queue_item"))
      {
        localJSONArray2 = localJSONObject4.optJSONArray("queue_item");
        if (localJSONArray2 == null)
          break label435;
        int k = localJSONArray2.length();
        Logger.d("FlxMain", "NetflixQueue.parseFromJson length:" + k);
        m = 0;
        if (m < k)
          break label372;
      }
    }
    label198: JSONObject localJSONObject1;
    JSONArray localJSONArray1;
    int i;
    if (paramJSONObject.has("at_home"))
    {
      localJSONObject1 = paramJSONObject.getJSONObject("at_home");
      if (localJSONObject1.has("etag"))
      {
        String str1 = localJSONObject1.getString("etag");
        localJSONObject1.put("etag", str1);
        FlixsterApplication.setNetflixEtag(str1);
      }
      if (localJSONObject1.has("start_index"))
        this.startIndex = localJSONObject1.getInt("start_index");
      if (localJSONObject1.has("results_per_page"))
        this.resultsPerPage = localJSONObject1.getInt("results_per_page");
      if (localJSONObject1.has("number_of_results"))
        this.numberOfResults = localJSONObject1.getInt("number_of_results");
      if (localJSONObject1.has("at_home_item"))
      {
        Logger.d("FlxMain", "NetflixQueue.parsing jsonQueue.has(NF_ATHOMEITEM)");
        localJSONArray1 = localJSONObject1.optJSONArray("at_home_item");
        if (localJSONArray1 == null)
          break label557;
        i = localJSONArray1.length();
        Logger.d("FlxMain", "NetflixQueue.parsing jsonQueue.has(NF_ATHOMEITEM) length:" + i);
      }
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        return this;
        label372: JSONObject localJSONObject6 = localJSONArray2.getJSONObject(m);
        NetflixQueueItem localNetflixQueueItem4 = new NetflixQueueItem();
        localNetflixQueueItem4.parseFromJSON(localJSONObject6);
        if ((paramBoolean) && (localNetflixQueueItem4.isSaved()))
        {
          this.numberOfResults = -2147483648;
          break label198;
        }
        this.mNetflixQueueItemList.add(localNetflixQueueItem4);
        m++;
        break;
        label435: JSONObject localJSONObject5 = localJSONObject4.optJSONObject("queue_item");
        NetflixQueueItem localNetflixQueueItem3 = new NetflixQueueItem();
        localNetflixQueueItem3.parseFromJSON(localJSONObject5);
        if ((paramBoolean) && (localNetflixQueueItem3.isSaved()))
        {
          this.numberOfResults = -2147483648;
          break label198;
        }
        this.mNetflixQueueItemList.add(localNetflixQueueItem3);
        break label198;
      }
      JSONObject localJSONObject3 = localJSONArray1.getJSONObject(j);
      NetflixQueueItem localNetflixQueueItem2 = new NetflixQueueItem();
      localNetflixQueueItem2.parseFromJSON(localJSONObject3);
      if ((paramBoolean) && (localNetflixQueueItem2.isSaved()))
      {
        this.numberOfResults = -2147483648;
        return this;
      }
      this.mNetflixQueueItemList.add(localNetflixQueueItem2);
    }
    label557: Logger.d("FlxMain", "NetflixQueue.parsing jsonQueue.has(NF_ATHOMEITEM) single");
    JSONObject localJSONObject2 = localJSONObject1.optJSONObject("at_home_item");
    NetflixQueueItem localNetflixQueueItem1 = new NetflixQueueItem();
    localNetflixQueueItem1.parseFromJSON(localJSONObject2);
    if ((paramBoolean) && (localNetflixQueueItem1.isSaved()))
    {
      this.numberOfResults = -2147483648;
      return this;
    }
    this.mNetflixQueueItemList.add(localNetflixQueueItem1);
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.NetflixQueue
 * JD-Core Version:    0.6.2
 */