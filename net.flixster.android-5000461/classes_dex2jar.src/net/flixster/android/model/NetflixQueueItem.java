package net.flixster.android.model;

import android.graphics.Bitmap;
import java.util.HashMap;
import net.flixster.android.FlixUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NetflixQueueItem
{
  public static final String NF_CATEGORY = "category";
  public static final String NF_CAT_AVAILBLE_NOW = "available now";
  public static final String NF_CAT_SAVED = "saved";
  public static final String NF_ID = "id";
  public static final String NF_POSITION = "position";
  public static final String NF_TITLE = "title";
  public static final String NF_URL_ID_BASE = "http://api-public.netflix.com/catalog/titles/movies/";
  public static final String STRING_THUMBNAIL_URL = "box_art";
  public Movie mMovie = null;
  private HashMap<String, String> mPropertyMap = new HashMap();
  private long mThumbOrderStamp = 0L;
  private int position;
  public Bitmap thumbnailBitmap = null;

  public String getNetflixUrlId()
  {
    return "http://api-public.netflix.com/catalog/titles/movies/" + (String)this.mPropertyMap.get("id");
  }

  public int getPosition()
  {
    return this.position;
  }

  public String getProperty(String paramString)
  {
    if (this.mPropertyMap.containsKey(paramString))
      return (String)this.mPropertyMap.get(paramString);
    return null;
  }

  public long getThumbOrderStamp()
  {
    return this.mThumbOrderStamp;
  }

  public boolean isSaved()
  {
    return this.mPropertyMap.get("saved") != null;
  }

  public NetflixQueueItem parseFromJSON(JSONObject paramJSONObject)
    throws JSONException
  {
    if (paramJSONObject.has("title"))
    {
      JSONObject localJSONObject2 = paramJSONObject.getJSONObject("title");
      if (localJSONObject2.has("regular"))
        this.mPropertyMap.put("title", FlixUtils.copyString(localJSONObject2.getString("regular")));
    }
    if (paramJSONObject.has("id"))
    {
      String str2 = paramJSONObject.getString("id");
      String str3 = str2.substring(1 + str2.lastIndexOf('/'), str2.length());
      this.mPropertyMap.put("id", FlixUtils.copyString(str3));
    }
    if (paramJSONObject.has("box_art"))
    {
      JSONObject localJSONObject1 = paramJSONObject.getJSONObject("box_art");
      if (localJSONObject1.has("medium"))
        this.mPropertyMap.put("box_art", FlixUtils.copyString(localJSONObject1.getString("medium")));
    }
    JSONArray localJSONArray;
    int j;
    if (paramJSONObject.has("category"))
    {
      localJSONArray = paramJSONObject.getJSONArray("category");
      int i = localJSONArray.length();
      j = 0;
      if (j < i);
    }
    else
    {
      this.position = paramJSONObject.optInt("position", 0);
      return this;
    }
    String str1 = localJSONArray.getJSONObject(j).getString("term");
    if (str1.contentEquals("available now"))
      this.mPropertyMap.put("available now", "available now");
    while (true)
    {
      j++;
      break;
      if (str1.contentEquals("saved"))
        this.mPropertyMap.put("saved", "saved");
    }
  }

  public void setThumbOrderStamp(long paramLong)
  {
    this.mThumbOrderStamp = paramLong;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.NetflixQueueItem
 * JD-Core Version:    0.6.2
 */