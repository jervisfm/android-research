package net.flixster.android.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NetflixTitleState
{
  public static final int ACT_DVD_ADD = 2;
  public static final int ACT_DVD_IN_QUEUE = 1;
  public static final int ACT_DVD_SAVE = 4;
  public static final int ACT_INSTANT_ADD = 16;
  public static final int ACT_INSTANT_IN_QUEUE = 8;
  public static final String CAT_ADD = "Add";
  public static final String CAT_BLURAY = "Blu-ray";
  public static final String CAT_DVD = "DVD";
  public static final String CAT_INSTANT = "Instant";
  public static final String CAT_IN_QUEUE = "In Queue";
  public static final String CAT_SAVE = "Save";
  public int mCategoriesMask = 0;

  private void parseTitleStateItem(JSONObject paramJSONObject)
    throws JSONException
  {
    JSONArray localJSONArray1;
    int i;
    if (paramJSONObject.has("format"))
    {
      localJSONArray1 = paramJSONObject.optJSONArray("format");
      if (localJSONArray1 == null)
      {
        localJSONArray1 = new JSONArray();
        localJSONArray1.put(paramJSONObject.optJSONObject("format"));
      }
      i = localJSONArray1.length();
    }
    JSONArray localJSONArray2;
    String str1;
    int m;
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return;
      localJSONArray2 = localJSONArray1.getJSONObject(j).getJSONArray("category");
      int k = localJSONArray2.length();
      str1 = null;
      m = 0;
      if (m < k)
        break;
    }
    String str2 = localJSONArray2.getJSONObject(m).getString("term");
    if ((str2.contentEquals("DVD")) || (str2.contentEquals("Blu-ray")))
      str1 = "DVD";
    while (true)
    {
      m++;
      break;
      if (str2.contentEquals("Instant"))
        str1 = "Instant";
      else if (str1 == "DVD")
      {
        if (str2.contentEquals("In Queue"))
          this.mCategoriesMask = (0x1 | this.mCategoriesMask);
        else if (str2.contentEquals("Add"))
          this.mCategoriesMask = (0x2 | this.mCategoriesMask);
        else if (str2.contentEquals("Save"))
          this.mCategoriesMask = (0x4 | this.mCategoriesMask);
      }
      else if (str2.contentEquals("In Queue"))
        this.mCategoriesMask = (0x8 | this.mCategoriesMask);
      else if (str2.contentEquals("Add"))
        this.mCategoriesMask = (0x10 | this.mCategoriesMask);
    }
  }

  public NetflixTitleState parseFromJSON(JSONObject paramJSONObject)
    throws JSONException
  {
    if (paramJSONObject.has("title_state_item"))
    {
      JSONObject localJSONObject = paramJSONObject.optJSONObject("title_state_item");
      if (localJSONObject == null)
        break label27;
      parseTitleStateItem(localJSONObject);
    }
    while (true)
    {
      return this;
      label27: JSONArray localJSONArray = paramJSONObject.optJSONArray("title_state_item");
      int i = localJSONArray.length();
      for (int j = 0; j < i; j++)
        parseTitleStateItem(localJSONArray.getJSONObject(j));
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.NetflixTitleState
 * JD-Core Version:    0.6.2
 */