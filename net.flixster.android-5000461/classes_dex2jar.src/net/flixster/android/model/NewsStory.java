package net.flixster.android.model;

import android.graphics.Bitmap;
import java.lang.ref.SoftReference;
import net.flixster.android.FlixUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class NewsStory
{
  public final String DESCRIPTION = "description";
  public final String SOURCE = "source";
  public final String THUMBNAIL_URL = "thumbnail";
  public final String TITLE = "title";
  public String mDescription;
  public SoftReference<Bitmap> mSoftPhoto = new SoftReference(null);
  public String mSource;
  private long mThumbOrderStamp = 0L;
  public String mThumbnailUrl;
  public String mTitle;

  public long getThumbOrderStamp()
  {
    return this.mThumbOrderStamp;
  }

  public NewsStory parseJSONObject(JSONObject paramJSONObject)
    throws JSONException
  {
    if (paramJSONObject.has("title"))
      this.mTitle = FlixUtils.copyString(paramJSONObject.getString("title"));
    if (paramJSONObject.has("description"))
      this.mDescription = FlixUtils.copyString(paramJSONObject.getString("description"));
    if (paramJSONObject.has("source"))
      this.mSource = FlixUtils.copyString(paramJSONObject.getString("source"));
    if (paramJSONObject.has("thumbnail"))
      this.mThumbnailUrl = FlixUtils.copyString(paramJSONObject.getString("thumbnail"));
    return this;
  }

  public void setThumbOrderStamp(long paramLong)
  {
    this.mThumbOrderStamp = paramLong;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.NewsStory
 * JD-Core Version:    0.6.2
 */