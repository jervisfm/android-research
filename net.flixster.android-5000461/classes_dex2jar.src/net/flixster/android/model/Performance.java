package net.flixster.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.flixster.android.utils.Logger;
import net.flixster.android.FlixUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Performance
  implements Parcelable
{
  public static final String CATEGORIES = "categories";
  public static final String CATEGORY_ID = "id";
  public static final String CATEGORY_LABEL = "description";
  public static final String CATEGORY_PRICE = "amount";
  public static final String CURRENCY = "currency";
  public static final String ERROR = "error";
  public static final String METHODS = "methods";
  public static final String METHOD_ID = "id";
  public static final String METHOD_NAME = "name";
  public static final String PERFORMANCE = "performance";
  public static final String RESERVATION = "reservation";
  public static final String SURCHARGE = "surcharge";
  public static final String TICKETS = "tickets";
  private String[] mCategoryIds;
  private String[] mCategoryLabels;
  private float[] mCategoryPrices;
  private int mCategoryTally = 0;
  private String mCurrency;
  private String mError;
  private String[] mMethodIds;
  private String[] mMethodNames;
  private int mMethodsTally = 0;
  private String mReservationId;
  private float mSurcharge = 0.0F;

  public Performance()
  {
  }

  public Performance(Parcel paramParcel)
  {
    this.mCurrency = paramParcel.readString();
    this.mCategoryTally = paramParcel.readInt();
    paramParcel.readStringArray(this.mCategoryIds);
    paramParcel.readStringArray(this.mCategoryLabels);
    paramParcel.readFloatArray(this.mCategoryPrices);
    this.mMethodsTally = paramParcel.readInt();
    paramParcel.readStringArray(this.mMethodIds);
    paramParcel.readStringArray(this.mMethodNames);
    this.mReservationId = paramParcel.readString();
    this.mSurcharge = paramParcel.readFloat();
    this.mError = paramParcel.readString();
  }

  public void clearError()
  {
    this.mError = null;
  }

  public int describeContents()
  {
    return 0;
  }

  public String[] getCategoryIds()
  {
    return this.mCategoryIds;
  }

  public int getCategoryTally()
  {
    return this.mCategoryTally;
  }

  public String getError()
  {
    return this.mError;
  }

  public String[] getLabels()
  {
    return this.mCategoryLabels;
  }

  public String[] getMethodIds()
  {
    return this.mMethodIds;
  }

  public String[] getMethodNames()
  {
    return this.mMethodNames;
  }

  public int getMethodsTally()
  {
    return this.mMethodsTally;
  }

  public float[] getPrices()
  {
    return this.mCategoryPrices;
  }

  public String getReservationId()
  {
    return this.mReservationId;
  }

  public float getSurcharge()
  {
    return this.mSurcharge;
  }

  public Performance parseFromJSON(JSONObject paramJSONObject)
    throws JSONException
  {
    Logger.d("FlxMain", "Performance.parseFromJSON");
    if (paramJSONObject.has("error"))
    {
      this.mError = FlixUtils.copyString(paramJSONObject.getString("error"));
      return this;
    }
    JSONArray localJSONArray1;
    int i;
    label135: JSONArray localJSONArray2;
    if (paramJSONObject.has("tickets"))
    {
      JSONObject localJSONObject1 = paramJSONObject.getJSONObject("tickets");
      this.mCurrency = FlixUtils.copyString(localJSONObject1.getString("currency"));
      if (localJSONObject1.has("surcharge"))
        this.mSurcharge = Float.valueOf(localJSONObject1.getString("surcharge")).floatValue();
      localJSONArray1 = localJSONObject1.getJSONArray("categories");
      this.mCategoryTally = localJSONArray1.length();
      this.mCategoryIds = new String[this.mCategoryTally];
      this.mCategoryLabels = new String[this.mCategoryTally];
      this.mCategoryPrices = new float[this.mCategoryTally];
      i = 0;
      if (i < this.mCategoryTally)
        break label231;
      Logger.d("FlxMain", "Performance.parseFromJSON doing methods");
      localJSONArray2 = localJSONObject1.getJSONArray("methods");
      this.mMethodsTally = localJSONArray2.length();
      this.mMethodIds = new String[this.mMethodsTally];
      this.mMethodNames = new String[this.mMethodsTally];
    }
    for (int j = 0; ; j++)
    {
      if (j >= this.mMethodsTally)
      {
        if (!paramJSONObject.has("reservation"))
          break;
        this.mReservationId = FlixUtils.copyString(paramJSONObject.getJSONObject("reservation").getString("id"));
        return this;
        label231: JSONObject localJSONObject2 = localJSONArray1.getJSONObject(i);
        this.mCategoryIds[i] = FlixUtils.copyString(localJSONObject2.getString("id"));
        this.mCategoryLabels[i] = FlixUtils.copyString(localJSONObject2.getString("description"));
        this.mCategoryPrices[i] = Float.valueOf(localJSONObject2.getString("amount")).floatValue();
        i++;
        break label135;
      }
      JSONObject localJSONObject3 = localJSONArray2.getJSONObject(j);
      this.mMethodIds[j] = FlixUtils.copyString(localJSONObject3.getString("id"));
      this.mMethodNames[j] = FlixUtils.copyString(localJSONObject3.getString("name"));
      Logger.d("FlxMain", "Performance.parseFromJSON doing methods mMethodIds[i]:" + this.mMethodIds[j] + " mMethodNames[i]:" + this.mMethodNames[j]);
    }
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.mCurrency);
    paramParcel.writeInt(this.mCategoryTally);
    paramParcel.writeStringArray(this.mCategoryIds);
    paramParcel.writeStringArray(this.mCategoryLabels);
    paramParcel.writeFloatArray(this.mCategoryPrices);
    paramParcel.writeInt(this.mMethodsTally);
    paramParcel.writeStringArray(this.mMethodIds);
    paramParcel.writeStringArray(this.mMethodNames);
    paramParcel.writeString(this.mReservationId);
    paramParcel.writeFloat(this.mSurcharge);
    paramParcel.writeString(this.mError);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Performance
 * JD-Core Version:    0.6.2
 */