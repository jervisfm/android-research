package net.flixster.android.model;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.flixster.android.model.Image;
import java.lang.ref.SoftReference;

public class Photo
{
  public long actorId;
  public String actorName;
  public Bitmap bitmap;
  public String caption;
  public String description;
  public SoftReference<Bitmap> galleryBitmap = new SoftReference(null);
  public String galleryUrl;
  public long id;
  public long movieId;
  public String movieTitle;
  public String origionalUrl;
  private Image thumbnailBitmap;
  public String thumbnailUrl;

  public <T extends ImageView> Bitmap getThumbnailBitmap(T paramT)
  {
    if (this.thumbnailBitmap == null)
      this.thumbnailBitmap = new Image(this.thumbnailUrl);
    return this.thumbnailBitmap.getBitmap(paramT);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Photo
 * JD-Core Version:    0.6.2
 */