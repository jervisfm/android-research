package net.flixster.android.model;

import com.flixster.android.data.AccountFacade;
import com.flixster.android.data.AccountManager;
import org.json.JSONException;
import org.json.JSONObject;

public class Property
{
  private static final String ELEVEN_BUTTON_TEXT = "elevenButtonText";
  private static final String ELEVEN_CREATIVE_URL = "elevenCreativeUrl";
  private static final String IS_CLIENT_CAPPED = "isClientCapped";
  private static final String IS_ELEVEN_ENABLED = "isElevenEnabled";
  private static final String IS_GLUE_ENABLED = "isGlueEnabled";
  private static final String IS_MSK_ENABLED = "isMskEnabled";
  private static final String IS_MSK_FIRST_LAUNCH_ENABLED = "isMskFirstLaunchEnabled";
  private static final String MSK_ANON_ENTRY = "msk.anon.entry";
  private static final String MSK_ANON_PROMO_URL = "mskAnonPromoUrl";
  private static final String MSK_FB_ENTRY = "msk.fb.entry";
  private static final String MSK_FB_LOGIN_EXIT = "msk.fb.login.exit";
  private static final String MSK_FB_REQUEST_BODY = "msk.fb.request.body";
  private static final String MSK_FB_REQUEST_EXIT = "msk.fb.request.exit";
  private static final String MSK_FIRST_LAUNCH_ENTRY = "msk.first.launch.entry";
  private static final String MSK_FLX_ENTRY = "msk.flx.entry";
  private static final String MSK_FLX_LOGIN_EXIT = "msk.flx.login.exit";
  public static final String MSK_SELECTION_PICK_EXIT = "msk.selection.pick.exit";
  public static final String MSK_SELECTION_SEND_EXIT = "msk.selection.send.exit";
  public static final String MSK_SELECTION_SKIP_EXIT = "msk.selection.skip.exit";
  private static final String MSK_TEXT_REQUEST_BODY = "msk.text.request.body";
  private static final String MSK_TEXT_REQUEST_EXIT = "msk.text.request.exit";
  private static final String MSK_TEXT_REQUEST_REWARDS = "msk.text.request.rewards";
  private static final String MSK_TOOLTIP_URL = "mskTooltipUrl";
  private static final String MSK_USER_PROMO_URL = "mskUserPromoUrl";
  private static final String MSK_VARIATION = "msk.variation";
  private static final String REWARDS_FBINVITE = "rewardsFbInvite";
  private static final String REWARDS_FBINVITE_ENABLED = "rewardsFbInviteEnabled";
  private static final String REWARDS_QUICKRATE = "rewardsQuickRate";
  private static final String REWARDS_QUICKRATE_ENABLED = "rewardsQuickRateEnabled";
  private static final String REWARDS_QUICKWTS = "rewardsQuickWts";
  private static final String REWARDS_QUICKWTS_ENABLED = "rewardsQuickWtsEnabled";
  private static final String REWARDS_SMS = "rewardsSms";
  private static final String REWARDS_SMS_BODY = "rewardsSmsBody";
  private static final String REWARDS_SMS_ENABLED = "rewardsSmsEnabled";
  public final String elevenButtonText;
  public final String elevenCreativeUrl;
  public final boolean is7ElevenEnabled;
  public final boolean isClientCapped;
  public final boolean isFbInviteRewardEnabled;
  public final boolean isGetGlueEnabled;
  public final boolean isMskEnabled;
  public final boolean isMskFirstLaunchEnabled;
  public final boolean isQuickRateRewardEnabled;
  public final boolean isQuickWtsRewardEnabled;
  public final boolean isSmsRewardEnabled;
  private final JSONObject json;
  public final String mskAnonEntry;
  public final String mskAnonPromoUrl;
  public final String mskFbEntry;
  public final String mskFbLoginExit;
  public final String mskFbRequestBody;
  public final String mskFbRequestExit;
  public final String mskFirstLaunchEntry;
  public final String mskFlxEntry;
  public final String mskFlxLoginExit;
  public final String mskTextRequestBody;
  public final String mskTextRequestExit;
  public final String mskTextRequestRewards;
  public final int[] mskTextRequestRewardsArray;
  public final String mskTooltipUrl;
  public final String mskUserPromoUrl;
  public final String mskVariation;
  public final String rewardsFbInvite;
  public final int[] rewardsFbInviteArray;
  public final int rewardsQuickRate;
  public final int rewardsQuickWts;
  public final String rewardsSms;
  public final int[] rewardsSmsArray;
  public final String rewardsSmsBody;

  public Property(JSONObject paramJSONObject)
    throws JSONException
  {
    this.json = paramJSONObject;
    this.isMskEnabled = paramJSONObject.optBoolean("isMskEnabled");
    this.isMskFirstLaunchEnabled = paramJSONObject.optBoolean("isMskFirstLaunchEnabled");
    this.mskAnonPromoUrl = paramJSONObject.optString("mskAnonPromoUrl", null);
    this.mskUserPromoUrl = paramJSONObject.optString("mskUserPromoUrl", null);
    this.mskTooltipUrl = paramJSONObject.optString("mskTooltipUrl", null);
    this.isQuickRateRewardEnabled = paramJSONObject.optBoolean("rewardsQuickRateEnabled", true);
    this.rewardsQuickRate = paramJSONObject.optInt("rewardsQuickRate");
    this.isQuickWtsRewardEnabled = paramJSONObject.optBoolean("rewardsQuickWtsEnabled", true);
    this.rewardsQuickWts = paramJSONObject.optInt("rewardsQuickWts");
    this.isSmsRewardEnabled = paramJSONObject.optBoolean("rewardsSmsEnabled", true);
    this.rewardsSmsBody = paramJSONObject.optString("rewardsSmsBody", null);
    this.rewardsSms = paramJSONObject.optString("rewardsSms", null);
    String[] arrayOfString3;
    int k;
    label178: String[] arrayOfString2;
    int j;
    label228: label234: String[] arrayOfString1;
    int i;
    if (this.rewardsSms != null)
    {
      this.rewardsSmsArray = new int[4];
      this.rewardsSmsArray[0] = 0;
      arrayOfString3 = this.rewardsSms.split(",");
      k = 1;
      if (k >= 4)
      {
        this.isFbInviteRewardEnabled = paramJSONObject.optBoolean("rewardsFbInviteEnabled", true);
        this.rewardsFbInvite = paramJSONObject.optString("rewardsFbInvite", null);
        if (this.rewardsFbInvite == null)
          break label509;
        this.rewardsFbInviteArray = new int[3];
        arrayOfString2 = this.rewardsFbInvite.split(",");
        j = 0;
        if (j < 3)
          break label488;
        this.mskAnonEntry = paramJSONObject.optString("msk.anon.entry", null);
        this.mskFbEntry = paramJSONObject.optString("msk.fb.entry", null);
        this.mskFlxEntry = paramJSONObject.optString("msk.flx.entry", null);
        this.mskFbLoginExit = paramJSONObject.optString("msk.fb.login.exit", null);
        this.mskFlxLoginExit = paramJSONObject.optString("msk.flx.login.exit", null);
        this.mskFbRequestExit = paramJSONObject.optString("msk.fb.request.exit", null);
        this.mskTextRequestExit = paramJSONObject.optString("msk.text.request.exit", null);
        this.mskVariation = paramJSONObject.optString("msk.variation", null);
        this.mskFbRequestBody = paramJSONObject.optString("msk.fb.request.body", null);
        this.mskTextRequestBody = paramJSONObject.optString("msk.text.request.body", null);
        this.mskTextRequestRewards = paramJSONObject.optString("msk.text.request.rewards", null);
        if (this.mskTextRequestRewards == null)
          break label537;
        this.mskTextRequestRewardsArray = new int[4];
        this.mskTextRequestRewardsArray[0] = 0;
        arrayOfString1 = this.mskTextRequestRewards.split(",");
        i = 1;
        label388: if (i < 4)
          break label517;
      }
    }
    while (true)
    {
      this.mskFirstLaunchEntry = paramJSONObject.optString("msk.first.launch.entry", null);
      this.isGetGlueEnabled = paramJSONObject.optBoolean("isGlueEnabled");
      this.is7ElevenEnabled = paramJSONObject.optBoolean("isElevenEnabled");
      this.elevenCreativeUrl = paramJSONObject.optString("elevenCreativeUrl", null);
      this.elevenButtonText = paramJSONObject.optString("elevenButtonText", null);
      this.isClientCapped = paramJSONObject.optBoolean("isClientCapped");
      return;
      this.rewardsSmsArray[k] = Integer.parseInt(arrayOfString3[(k - 1)]);
      k++;
      break;
      this.rewardsSmsArray = null;
      break label178;
      label488: this.rewardsFbInviteArray[j] = Integer.parseInt(arrayOfString2[j]);
      j++;
      break label228;
      label509: this.rewardsFbInviteArray = null;
      break label234;
      label517: this.mskTextRequestRewardsArray[i] = Integer.parseInt(arrayOfString1[(i - 1)]);
      i++;
      break label388;
      label537: this.mskTextRequestRewardsArray = null;
    }
  }

  public String getMskEntry()
  {
    if (AccountFacade.getSocialUser() == null)
      return this.mskAnonEntry;
    if (AccountManager.instance().isPlatformFacebook())
      return this.mskFbEntry;
    return this.mskFlxEntry;
  }

  public String optString(String paramString)
  {
    return this.json.optString(paramString, null);
  }

  public String toString()
  {
    return "{ isMskEnabled=" + this.isMskEnabled + ", isMskFirstLaunchEnabled=" + this.isMskFirstLaunchEnabled + ", mskAnonPromoUrl=" + this.mskAnonPromoUrl + ", mskUserPromoUrl=" + this.mskUserPromoUrl + ", mskTooltipUrl=" + this.mskTooltipUrl + ", isQuickRateRewardEnabled=" + this.isQuickRateRewardEnabled + ", rewardsQuickRate=" + this.rewardsQuickRate + ", isQuickWtsRewardEnabled=" + this.isQuickWtsRewardEnabled + ", rewardsQuickWts=" + this.rewardsQuickWts + ", isSmsRewardEnabled=" + this.isSmsRewardEnabled + ", rewardsSmsBody=" + this.rewardsSmsBody + ", rewardsSms=" + this.rewardsSms + ", isFbInviteRewardEnabled=" + this.isFbInviteRewardEnabled + ", rewardsFbInvite=" + this.rewardsFbInvite + ", mskAnonEntry=" + this.mskAnonEntry + ", mskFbEntry=" + this.mskFbEntry + ", mskFlxEntry=" + this.mskFlxEntry + ", mskFbLoginExit=" + this.mskFbLoginExit + ", mskFlxLoginExit=" + this.mskFlxLoginExit + ", mskFbRequestExit=" + this.mskFbRequestExit + ", mskTextRequestExit=" + this.mskTextRequestExit + ", mskVariation=" + this.mskVariation + ", mskFbRequestBody=" + this.mskFbRequestBody + ", mskTextRequestBody=" + this.mskTextRequestBody + ", mskTextRequestRewards=" + this.mskTextRequestRewards + ", mskFirstLaunchEntry=" + this.mskFirstLaunchEntry + ", isGetGlueEnabled=" + this.isGetGlueEnabled + ", isClientCapEnabled=" + this.isClientCapped + ", is7ElevenEnabled=" + this.is7ElevenEnabled + ", elevenCreativeUrl=" + this.elevenCreativeUrl + ", elevenButtonText=" + this.elevenButtonText + "}";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Property
 * JD-Core Version:    0.6.2
 */