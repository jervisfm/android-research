package net.flixster.android.model;

import java.util.Comparator;

public class RatedCountComparator
  implements Comparator<User>
{
  public int compare(User paramUser1, User paramUser2)
  {
    return paramUser2.ratedCount - paramUser1.ratedCount;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.RatedCountComparator
 * JD-Core Version:    0.6.2
 */