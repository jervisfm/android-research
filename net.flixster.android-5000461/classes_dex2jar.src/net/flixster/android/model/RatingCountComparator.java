package net.flixster.android.model;

import java.util.Comparator;

public class RatingCountComparator
  implements Comparator<User>
{
  public int compare(User paramUser1, User paramUser2)
  {
    return paramUser2.ratingCount - paramUser1.ratingCount;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.RatingCountComparator
 * JD-Core Version:    0.6.2
 */