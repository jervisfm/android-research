package net.flixster.android.model;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.flixster.android.model.Image;

public class Review
{
  private static final String CRITIC_NONE_TMB = "critic_default_icon.gif";
  public static final int REVIEWTYPE_CRITIC = 0;
  public static final int REVIEWTYPE_FRIEND = 2;
  public static final int REVIEWTYPE_USER = 1;
  public String comment;
  public String id;
  private Movie movie;
  public String mugUrl;
  public String name;
  private Image profileBitmap;
  public int score;
  public String source;
  public double stars;
  public int type;
  public String url;
  public long userId;
  public String userName;

  public int describeContents()
  {
    return 0;
  }

  public int getActionId()
  {
    if (isWantToSee())
      return 2131493232;
    return 2131493233;
  }

  public Movie getMovie()
  {
    return this.movie;
  }

  public <T extends ImageView> Bitmap getReviewerBitmap(T paramT)
  {
    if ((this.mugUrl != null) && ((this.mugUrl.endsWith("user.none.tmb.jpg")) || (this.mugUrl.endsWith("critic_default_icon.gif"))))
      return null;
    if (this.profileBitmap == null)
      this.profileBitmap = new Image(this.mugUrl);
    return this.profileBitmap.getBitmap(paramT);
  }

  public String getTitle()
  {
    StringBuilder localStringBuilder = new StringBuilder(this.name);
    localStringBuilder.append(" ");
    if (isWantToSee())
      localStringBuilder.append("wants to see...");
    while (true)
    {
      return localStringBuilder.toString();
      if (!isNotInterested())
        localStringBuilder.append("rated a movie...");
    }
  }

  public boolean isNotInterested()
  {
    return 2.0D * this.stars == 12.0D;
  }

  public boolean isWantToSee()
  {
    return 2.0D * this.stars == 11.0D;
  }

  public void setMovie(Movie paramMovie)
  {
    this.movie = paramMovie;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Review
 * JD-Core Version:    0.6.2
 */