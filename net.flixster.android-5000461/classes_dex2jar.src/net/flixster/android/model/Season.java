package net.flixster.android.model;

import com.flixster.android.data.AccountManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Season extends Movie
{
  private static long SEASON_HASH_BASE = 100000000000000000L;
  private final Map<Long, Episode> episodesMap = new LinkedHashMap();
  private String network;
  private int numOfEpisodes;
  private int seasonNum;

  public Season(long paramLong)
  {
    super(paramLong);
  }

  public static long generateIdHash(long paramLong)
  {
    return paramLong + SEASON_HASH_BASE;
  }

  public Collection<Episode> getEpisodes()
  {
    return this.episodesMap.values();
  }

  public String getNetwork()
  {
    if ("".equals(this.network))
      return null;
    return this.network;
  }

  public int getNumOfEpisodes()
  {
    return this.numOfEpisodes;
  }

  public int getSeasonNum()
  {
    return this.seasonNum;
  }

  public boolean isUserSeasonParsed()
  {
    User localUser = AccountManager.instance().getUser();
    return (localUser != null) && (localUser.hasEpisodeRightsForSeason(getId()));
  }

  public Collection<LockerRight> mergeEpisodes(JSONArray paramJSONArray)
  {
    LockerRight localLockerRight1 = AccountManager.instance().getUser().getLockerRightFromAssetId(this.id);
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; ; i++)
    {
      if (i >= paramJSONArray.length())
        return localArrayList;
      JSONObject localJSONObject1 = paramJSONArray.optJSONObject(i);
      if (localJSONObject1 != null)
      {
        JSONObject localJSONObject2 = localJSONObject1.optJSONObject("episode");
        if (localJSONObject2 != null)
        {
          long l = localJSONObject2.optLong("id");
          Episode localEpisode = (Episode)this.episodesMap.get(Long.valueOf(l));
          if (localEpisode != null)
          {
            localEpisode.merge(localJSONObject2);
            LockerRight localLockerRight2 = new LockerRight(localJSONObject1, l, LockerRight.RightType.EPISODE, localEpisode);
            localArrayList.add(localLockerRight2);
            localLockerRight1.addChildRightId(localLockerRight2.rightId, l);
          }
        }
      }
    }
  }

  public Season parseFromJSON(JSONObject paramJSONObject)
    throws JSONException
  {
    super.parseFromJSON(paramJSONObject);
    this.seasonNum = paramJSONObject.optInt("season", this.seasonNum);
    this.numOfEpisodes = paramJSONObject.optInt("numEpisodes", this.numOfEpisodes);
    this.network = paramJSONObject.optString("network", this.network);
    JSONArray localJSONArray = paramJSONObject.optJSONArray("episodes");
    int i;
    if (localJSONArray != null)
    {
      i = 0;
      if (i < localJSONArray.length());
    }
    else
    {
      return this;
    }
    JSONObject localJSONObject = localJSONArray.optJSONObject(i);
    Episode localEpisode;
    if (localJSONObject != null)
    {
      long l = localJSONObject.optLong("id");
      localEpisode = (Episode)this.episodesMap.get(Long.valueOf(l));
      if (localEpisode != null)
        break label149;
      this.episodesMap.put(Long.valueOf(l), new Episode(localJSONObject));
    }
    while (true)
    {
      i++;
      break;
      label149: localEpisode.merge(localJSONObject);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Season
 * JD-Core Version:    0.6.2
 */