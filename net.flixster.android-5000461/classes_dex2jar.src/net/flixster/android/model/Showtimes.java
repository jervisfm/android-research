package net.flixster.android.model;

import com.flixster.android.utils.DateTimeHelper;
import java.util.ArrayList;

public class Showtimes
{
  public static final String NAME = "SHOWTIMES_NAME";
  public static final String TIMES = "SHOWTIMES_TIMES";
  public ArrayList<Listing> mListings = new ArrayList();
  public long mMovieId;
  public String mShowtimesTitle;
  public long mTheaterId;
  private String mTimesString;

  public String getTimesString()
  {
    StringBuilder localStringBuilder;
    int i;
    if (this.mTimesString == null)
    {
      localStringBuilder = new StringBuilder();
      i = this.mListings.size();
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        this.mTimesString = localStringBuilder.toString().replaceAll(" PM", "");
        return this.mTimesString;
      }
      localStringBuilder = localStringBuilder.append(DateTimeHelper.shortTimeFormat(((Listing)this.mListings.get(j)).displayTime));
      if (j < i - 1)
        localStringBuilder = localStringBuilder.append(", ");
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Showtimes
 * JD-Core Version:    0.6.2
 */