package net.flixster.android.model;

import java.util.Comparator;

public class ShowtimesTitleComparator
  implements Comparator<Showtimes>
{
  public int compare(Showtimes paramShowtimes1, Showtimes paramShowtimes2)
  {
    return paramShowtimes1.mShowtimesTitle.compareTo(paramShowtimes2.mShowtimesTitle);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.ShowtimesTitleComparator
 * JD-Core Version:    0.6.2
 */