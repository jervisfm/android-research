package net.flixster.android.model;

import java.util.ArrayList;
import java.util.HashMap;
import net.flixster.android.FlixUtils;
import net.flixster.android.data.MovieDao;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Theater
{
  public static final String THEATER_ADDRESS = "address";
  public static final String THEATER_CITY = "city";
  public static final String THEATER_ID = "id";
  public static final String THEATER_MAP = "map";
  public static final String THEATER_NAME = "name";
  public static final String THEATER_PHONE = "phone";
  public static final String THEATER_STATE = "state";
  public static final String THEATER_STREET = "street";
  public static final String THEATER_TICKET = "ticket";
  public static final String THEATER_TICKETS = "tickets";
  public static final String THEATER_ZIP = "zip";
  private long id;
  public double latitude;
  public double longitude;
  private boolean mHasTickets = false;
  private Double mMiles;
  private HashMap<String, String> mPropertyMap;
  private HashMap<Long, ArrayList<Showtimes>> mShowtimesListByMovieHash;

  public Theater(long paramLong)
  {
    this.id = paramLong;
    this.mPropertyMap = new HashMap();
  }

  public boolean checkProperty(String paramString)
  {
    return this.mPropertyMap.containsKey(paramString);
  }

  public long getId()
  {
    return this.id;
  }

  public double getMiles()
  {
    return this.mMiles.doubleValue();
  }

  public String getProperty(String paramString)
  {
    if (this.mPropertyMap.containsKey(paramString))
      return (String)this.mPropertyMap.get(paramString);
    return null;
  }

  public HashMap<Long, ArrayList<Showtimes>> getShowtimesListByMovieHash()
  {
    return this.mShowtimesListByMovieHash;
  }

  public boolean hasTickets()
  {
    return this.mHasTickets;
  }

  public void invalidateShowtimesHash()
  {
    this.mShowtimesListByMovieHash = null;
  }

  public Theater parseFromJSON(JSONObject paramJSONObject)
    throws JSONException
  {
    if (paramJSONObject.has("id"))
    {
      Long localLong = Long.valueOf(paramJSONObject.getLong("id"));
      if (localLong != null)
        this.id = localLong.longValue();
    }
    String[] arrayOfString1 = { "name", "map", "phone", "id" };
    int i = arrayOfString1.length;
    int j = 0;
    JSONObject localJSONObject3;
    String[] arrayOfString2;
    int i2;
    if (j >= i)
    {
      if (paramJSONObject.has("tickets"))
        this.mHasTickets = paramJSONObject.getBoolean("tickets");
      if (paramJSONObject.has("address"))
      {
        localJSONObject3 = paramJSONObject.getJSONObject("address");
        arrayOfString2 = new String[] { "street", "city", "state", "zip" };
        i2 = arrayOfString2.length;
      }
    }
    JSONArray localJSONArray1;
    int m;
    for (int i3 = 0; ; i3++)
    {
      if (i3 >= i2)
      {
        StringBuilder localStringBuilder = new StringBuilder(String.valueOf((String)this.mPropertyMap.get("street")));
        String str2 = ", " + (String)this.mPropertyMap.get("city") + ", " + (String)this.mPropertyMap.get("zip");
        this.mPropertyMap.put("address", str2);
        if (localJSONObject3.has("distance"))
          this.mMiles = Double.valueOf(localJSONObject3.getDouble("distance"));
        if (localJSONObject3.has("latitude"))
          this.latitude = localJSONObject3.getDouble("latitude");
        if (localJSONObject3.has("longitude"))
          this.longitude = localJSONObject3.getDouble("longitude");
        if (paramJSONObject.has("showtimes"))
        {
          this.mShowtimesListByMovieHash = new HashMap();
          localJSONArray1 = paramJSONObject.getJSONArray("showtimes");
          int k = localJSONArray1.length();
          m = 0;
          if (m < k)
            break label451;
        }
        return this;
        String str1 = arrayOfString1[j];
        if ((!this.mPropertyMap.containsKey(str1)) && (paramJSONObject.has(str1)))
          this.mPropertyMap.put(str1, FlixUtils.copyString(paramJSONObject.getString(str1)));
        j++;
        break;
      }
      String str3 = arrayOfString2[i3];
      if ((!this.mPropertyMap.containsKey(str3)) && (localJSONObject3.has(str3)))
        this.mPropertyMap.put(str3, FlixUtils.copyString(localJSONObject3.getString(str3)));
    }
    label451: JSONObject localJSONObject1 = localJSONArray1.getJSONObject(m);
    JSONObject localJSONObject2 = localJSONObject1.getJSONObject("movie");
    long l = localJSONObject2.getLong("id");
    Showtimes localShowtimes = new Showtimes();
    localShowtimes.mTheaterId = this.id;
    localShowtimes.mMovieId = l;
    MovieDao.getMovie(l).parseFromJSON(localJSONObject2);
    localShowtimes.mShowtimesTitle = localJSONObject2.getString("title");
    JSONArray localJSONArray2 = localJSONObject1.getJSONArray("times");
    int n = localJSONArray2.length();
    int i1 = 0;
    label545: ArrayList localArrayList;
    if (i1 >= n)
    {
      if (!this.mShowtimesListByMovieHash.containsKey(Long.valueOf(l)))
        break label631;
      localArrayList = (ArrayList)this.mShowtimesListByMovieHash.get(Long.valueOf(l));
    }
    while (true)
    {
      localArrayList.add(localShowtimes);
      m++;
      break;
      Listing localListing = new Listing(localJSONArray2.getJSONObject(i1));
      localShowtimes.mListings.add(localListing);
      i1++;
      break label545;
      label631: localArrayList = new ArrayList();
      this.mShowtimesListByMovieHash.put(Long.valueOf(l), localArrayList);
    }
  }

  public void setId(long paramLong)
  {
    this.id = paramLong;
  }

  public void setProperty(String paramString1, String paramString2)
  {
    this.mPropertyMap.put(paramString1, paramString2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Theater
 * JD-Core Version:    0.6.2
 */