package net.flixster.android.model;

import java.util.Comparator;

public class TheaterDistanceComparator
  implements Comparator<Theater>
{
  public int compare(Theater paramTheater1, Theater paramTheater2)
  {
    if (paramTheater2.getMiles() > paramTheater1.getMiles())
      return -1;
    if (paramTheater2.getMiles() < paramTheater1.getMiles())
      return 1;
    return 0;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.TheaterDistanceComparator
 * JD-Core Version:    0.6.2
 */