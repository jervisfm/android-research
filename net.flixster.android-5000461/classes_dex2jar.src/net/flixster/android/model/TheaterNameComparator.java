package net.flixster.android.model;

import java.util.Comparator;

public class TheaterNameComparator
  implements Comparator<Theater>
{
  public int compare(Theater paramTheater1, Theater paramTheater2)
  {
    return paramTheater1.getProperty("name").compareTo(paramTheater2.getProperty("name"));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.TheaterNameComparator
 * JD-Core Version:    0.6.2
 */