package net.flixster.android.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.ListView;
import com.flixster.android.utils.Logger;

public class TouchInterceptor extends ListView
{
  private static final int FLING = 0;
  private static final int SLIDE = 1;
  private Context mContext;
  private int mCoordOffset;
  private Bitmap mDragBitmap;
  private DragListener mDragListener;
  private int mDragPoint;
  private int mDragPos;
  private ImageView mDragView;
  private DropListener mDropListener;
  private int mFirstDragPos;
  private GestureDetector mGestureDetector;
  private int mHeight;
  private int mItemHeightExpanded;
  private int mItemHeightNormal;
  private int mLowerBound;
  private RemoveListener mRemoveListener;
  private int mRemoveMode = -1;
  private Rect mTempRect = new Rect();
  private final int mTouchSlop;
  private int mUpperBound;
  private WindowManager mWindowManager;
  private WindowManager.LayoutParams mWindowParams;

  public TouchInterceptor(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mContext = paramContext;
    this.mRemoveMode = paramContext.getSharedPreferences("Music", 3).getInt("deletemode", -1);
    this.mTouchSlop = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    this.mItemHeightNormal = getResources().getDimensionPixelSize(2131361844);
    this.mItemHeightExpanded = (2 * this.mItemHeightNormal);
  }

  private void adjustScrollBounds(int paramInt)
  {
    if (paramInt >= this.mHeight / 3)
      this.mUpperBound = (this.mHeight / 3);
    if (paramInt <= 2 * this.mHeight / 3)
      this.mLowerBound = (2 * this.mHeight / 3);
  }

  private void doExpansion()
  {
    int i = this.mDragPos - getFirstVisiblePosition();
    if (this.mDragPos > this.mFirstDragPos)
      i++;
    View localView1 = getChildAt(this.mFirstDragPos - getFirstVisiblePosition());
    int j = 0;
    View localView2 = getChildAt(j);
    if (localView2 == null)
      return;
    int k = this.mItemHeightNormal;
    int m;
    if (localView2.equals(localView1))
      if (this.mDragPos == this.mFirstDragPos)
        m = 4;
    while (true)
    {
      ViewGroup.LayoutParams localLayoutParams = localView2.getLayoutParams();
      localLayoutParams.height = k;
      localView2.setLayoutParams(localLayoutParams);
      localView2.setVisibility(m);
      j++;
      break;
      k = 1;
      m = 0;
      continue;
      m = 0;
      if (j == i)
      {
        int n = this.mDragPos;
        int i1 = -1 + getCount();
        m = 0;
        if (n < i1)
        {
          k = this.mItemHeightExpanded;
          m = 0;
        }
      }
    }
  }

  private void dragView(int paramInt1, int paramInt2)
  {
    if (this.mRemoveMode == 1)
    {
      float f = 1.0F;
      int i = this.mDragView.getWidth();
      if (paramInt1 > i / 2)
        f = (i - paramInt1) / (i / 2);
      this.mWindowParams.alpha = f;
    }
    this.mWindowParams.y = (paramInt2 - this.mDragPoint + this.mCoordOffset);
    this.mWindowManager.updateViewLayout(this.mDragView, this.mWindowParams);
  }

  private int getItemForPosition(int paramInt)
  {
    int i = -32 + (paramInt - this.mDragPoint);
    int j = myPointToPosition(0, i);
    if (j >= 0)
      if (j <= this.mFirstDragPos)
        j++;
    while (i >= 0)
      return j;
    return 0;
  }

  private int myPointToPosition(int paramInt1, int paramInt2)
  {
    Rect localRect = this.mTempRect;
    for (int i = -1 + getChildCount(); ; i--)
    {
      if (i < 0)
        return -1;
      getChildAt(i).getHitRect(localRect);
      if (localRect.contains(paramInt1, paramInt2))
        return i + getFirstVisiblePosition();
    }
  }

  private void startDragging(Bitmap paramBitmap, int paramInt)
  {
    Logger.d("FlxMain", "TouchInterceptor.startDragging(.)");
    stopDragging();
    this.mWindowParams = new WindowManager.LayoutParams();
    this.mWindowParams.gravity = 51;
    this.mWindowParams.x = 0;
    this.mWindowParams.y = (paramInt - this.mDragPoint + this.mCoordOffset);
    this.mWindowParams.height = -2;
    this.mWindowParams.width = -2;
    this.mWindowParams.width = -2;
    this.mWindowParams.flags = 408;
    this.mWindowParams.format = -3;
    this.mWindowParams.windowAnimations = 0;
    ImageView localImageView = new ImageView(this.mContext);
    localImageView.setBackgroundColor(this.mContext.getResources().getColor(2131296268));
    localImageView.setImageBitmap(paramBitmap);
    this.mDragBitmap = paramBitmap;
    this.mWindowManager = ((WindowManager)this.mContext.getSystemService("window"));
    this.mWindowManager.addView(localImageView, this.mWindowParams);
    this.mDragView = localImageView;
  }

  private void stopDragging()
  {
    if (this.mDragView != null)
    {
      ((WindowManager)this.mContext.getSystemService("window")).removeView(this.mDragView);
      this.mDragView.setImageDrawable(null);
      this.mDragView = null;
    }
    if (this.mDragBitmap != null)
    {
      this.mDragBitmap.recycle();
      this.mDragBitmap = null;
    }
  }

  private void unExpandViews(boolean paramBoolean)
  {
    for (int i = 0; ; i++)
    {
      View localView = getChildAt(i);
      if (localView == null)
      {
        if (paramBoolean)
        {
          int j = getFirstVisiblePosition();
          int k = getChildAt(0).getTop();
          setAdapter(getAdapter());
          setSelectionFromTop(j, k);
        }
        layoutChildren();
        localView = getChildAt(i);
        if (localView == null)
          return;
      }
      ViewGroup.LayoutParams localLayoutParams = localView.getLayoutParams();
      localLayoutParams.height = this.mItemHeightNormal;
      localView.setLayoutParams(localLayoutParams);
      localView.setVisibility(0);
    }
  }

  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((this.mRemoveListener != null) && (this.mGestureDetector == null) && (this.mRemoveMode == 0))
      this.mGestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener()
      {
        public boolean onFling(MotionEvent paramAnonymousMotionEvent1, MotionEvent paramAnonymousMotionEvent2, float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
          if (TouchInterceptor.this.mDragView != null)
          {
            if (paramAnonymousFloat1 > 1000.0F)
            {
              Rect localRect = TouchInterceptor.this.mTempRect;
              TouchInterceptor.this.mDragView.getDrawingRect(localRect);
              if (paramAnonymousMotionEvent2.getX() > 2 * localRect.right / 3)
              {
                TouchInterceptor.this.stopDragging();
                TouchInterceptor.this.mRemoveListener.remove(TouchInterceptor.this.mFirstDragPos);
                TouchInterceptor.this.unExpandViews(true);
              }
            }
            return true;
          }
          return false;
        }
      });
    if ((this.mDragListener != null) || (this.mDropListener != null))
      switch (paramMotionEvent.getAction())
      {
      default:
      case 0:
      }
    while (true)
    {
      return super.onInterceptTouchEvent(paramMotionEvent);
      int i = (int)paramMotionEvent.getX();
      int j = (int)paramMotionEvent.getY();
      int k = pointToPosition(i, j);
      if (k != -1)
      {
        ViewGroup localViewGroup = (ViewGroup)getChildAt(k - getFirstVisiblePosition());
        View localView = localViewGroup.findViewById(2131165693);
        if (localView != null)
        {
          this.mDragPoint = (j - localViewGroup.getTop());
          this.mCoordOffset = ((int)paramMotionEvent.getRawY() - j);
          Rect localRect = this.mTempRect;
          localView.getDrawingRect(localRect);
          if ((i > localViewGroup.getWidth() - localRect.width()) && (localView.getVisibility() == 0))
          {
            localViewGroup.setDrawingCacheEnabled(true);
            startDragging(Bitmap.createBitmap(localViewGroup.getDrawingCache()), j);
            this.mDragPos = k;
            this.mFirstDragPos = this.mDragPos;
            this.mHeight = getHeight();
            int m = this.mTouchSlop;
            this.mUpperBound = Math.min(j - m, this.mHeight / 3);
            this.mLowerBound = Math.max(j + m, 2 * this.mHeight / 3);
            return false;
          }
        }
        this.mDragView = null;
      }
    }
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (this.mGestureDetector != null)
      this.mGestureDetector.onTouchEvent(paramMotionEvent);
    if (((this.mDragListener != null) || (this.mDropListener != null)) && (this.mDragView != null))
    {
      int i = paramMotionEvent.getAction();
      switch (i)
      {
      default:
      case 1:
      case 3:
      case 0:
      case 2:
      }
      int k;
      int m;
      do
      {
        return true;
        Rect localRect = this.mTempRect;
        this.mDragView.getDrawingRect(localRect);
        stopDragging();
        if ((this.mRemoveMode == 1) && (paramMotionEvent.getX() > 3 * localRect.right / 4))
        {
          if (this.mRemoveListener != null)
            this.mRemoveListener.remove(this.mFirstDragPos);
          unExpandViews(true);
          return true;
        }
        if ((this.mDropListener != null) && (this.mDragPos >= 0) && (this.mDragPos < getCount()))
          this.mDropListener.drop(this.mFirstDragPos, this.mDragPos);
        unExpandViews(false);
        return true;
        int j = (int)paramMotionEvent.getX();
        k = (int)paramMotionEvent.getY();
        dragView(j, k);
        m = getItemForPosition(k);
      }
      while (m < 0);
      if ((i == 0) || (m != this.mDragPos))
      {
        if (this.mDragListener != null)
          this.mDragListener.drag(this.mDragPos, m);
        this.mDragPos = m;
        doExpansion();
      }
      adjustScrollBounds(k);
      if (k > this.mLowerBound)
        if (k > (this.mHeight + this.mLowerBound) / 2)
          i1 = 16;
      while (true)
        label308: if (i1 != 0)
        {
          int i2 = pointToPosition(0, this.mHeight / 2);
          if (i2 == -1)
            i2 = pointToPosition(0, 64 + (this.mHeight / 2 + getDividerHeight()));
          View localView = getChildAt(i2 - getFirstVisiblePosition());
          if (localView == null)
            break;
          setSelectionFromTop(i2, localView.getTop() - i1);
          return true;
          i1 = 4;
          continue;
          int n = this.mUpperBound;
          i1 = 0;
          if (k < n)
            if (k >= this.mUpperBound / 2)
              break label427;
        }
      label427: for (int i1 = -16; ; i1 = -4)
      {
        break label308;
        break;
      }
    }
    return super.onTouchEvent(paramMotionEvent);
  }

  public void setDragListener(DragListener paramDragListener)
  {
    this.mDragListener = paramDragListener;
  }

  public void setDropListener(DropListener paramDropListener)
  {
    this.mDropListener = paramDropListener;
  }

  public void setRemoveListener(RemoveListener paramRemoveListener)
  {
    this.mRemoveListener = paramRemoveListener;
  }

  public static abstract interface DragListener
  {
    public abstract void drag(int paramInt1, int paramInt2);
  }

  public static abstract interface DropListener
  {
    public abstract void drop(int paramInt1, int paramInt2);
  }

  public static abstract interface RemoveListener
  {
    public abstract void remove(int paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.TouchInterceptor
 * JD-Core Version:    0.6.2
 */