package net.flixster.android.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Unfulfillable extends Movie
{
  private static long UNFULFILLABLE_HASH_BASE = 200000000000000000L;
  private String alternateDownloadUrl;
  private String alternateStreamUrl;

  public Unfulfillable(long paramLong)
  {
    super(paramLong);
  }

  public static long generateIdHash(long paramLong)
  {
    return 9223372036854775807L + (paramLong + UNFULFILLABLE_HASH_BASE);
  }

  public String getAlternateUrl()
  {
    if (!"".equals(this.alternateStreamUrl))
      return this.alternateStreamUrl;
    if (!"".equals(this.alternateDownloadUrl))
      return this.alternateDownloadUrl;
    return "http://www.uvvu.com";
  }

  public Unfulfillable parseFromJSON(JSONObject paramJSONObject)
    throws JSONException
  {
    super.parseFromJSON(paramJSONObject);
    this.alternateStreamUrl = paramJSONObject.optString("streamUrl");
    this.alternateDownloadUrl = paramJSONObject.optString("downloadUrl");
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.Unfulfillable
 * JD-Core Version:    0.6.2
 */