package net.flixster.android.model;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.flixster.android.model.Image;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.FlixUtils;
import net.flixster.android.util.HttpImageHelper;
import org.json.JSONException;
import org.json.JSONObject;

public class User
{
  private static final String[] BASE_PROPERTY_ARRAY = { "platform", "userName", "displayName", "firstName", "lastName", "gender" };
  private static final List<LockerRight> EMPTY_RIGHTS = new ArrayList(0);
  public static final String USER_DISPLAYNAME_STRING = "displayName";
  public static final String USER_FIRSTNAME_STRING = "firstName";
  public static final String USER_GENDER_STRING = "gender";
  public static final String USER_LASTNAME_STRING = "lastName";
  protected static final String USER_NONE_PRO = "user.none.pro.jpg";
  protected static final String USER_NONE_TMB = "user.none.tmb.jpg";
  public static final String USER_PLATFORM_STRING = "platform";
  public static final String USER_USERNAME_STRING = "userName";
  public int age;

  @Deprecated
  public Bitmap bitmap;
  public int collectionsCount;
  public String displayName;
  public String firstName;
  public int friendCount;
  public List<Review> friendRatedReviews;
  public List<Review> friendReviews;
  public List<Review> friendWantToSeeReviews;
  public List<User> friends;
  public List<Review> friendsActivity;
  public Object[] friendsIds;
  public String gender;
  public String id;
  public boolean isMskEligible;
  public boolean isMskFbInviteEligible;
  public boolean isMskInstallMobileEligible;
  public boolean isMskRateEligible;
  public boolean isMskSmsEligible;
  public boolean isMskUvCreateEligible;
  public boolean isMskWtsEligible;
  public String lastName;
  private List<LockerRight> lockerRights;
  private HashMap<String, String> mPropertyMap = new HashMap();
  public String platform;
  private Image profileBitmap;
  public String profileImage;
  public int ratedCount;
  public List<Review> ratedReviews;
  public int ratingCount;
  public boolean refreshRequired;
  public int reviewCount;
  public List<Review> reviews;
  public String smallImage;
  private Image thumbnailBitmap;
  public String thumbnailImage;
  public String token;
  public String userName;
  public List<Review> wantToSeeReviews;
  public int wtsCount;

  private void arrayParseJSONObject(JSONObject paramJSONObject, String[] paramArrayOfString)
  {
    int i;
    int j;
    do
      try
      {
        i = paramArrayOfString.length;
        j = 0;
        continue;
        String str = paramArrayOfString[j];
        if ((!this.mPropertyMap.containsKey(str)) && (paramJSONObject.has(str)))
          this.mPropertyMap.put(str, paramJSONObject.getString(str));
        j++;
      }
      catch (JSONException localJSONException)
      {
        Logger.e("FlxMain", "User.arrayParse error ", localJSONException);
        return;
      }
    while (j < i);
  }

  public void addLockerRight(LockerRight paramLockerRight)
  {
    if (this.lockerRights == null)
      this.lockerRights = new ArrayList();
    this.lockerRights.add(paramLockerRight);
  }

  public void addLockerRights(Collection<LockerRight> paramCollection)
  {
    if (this.lockerRights == null)
      this.lockerRights = new ArrayList();
    this.lockerRights.addAll(paramCollection);
  }

  public void fetchProfileBitmap()
    throws IOException
  {
    if (this.profileImage != null)
    {
      this.bitmap = HttpImageHelper.fetchImage(new URL(this.profileImage));
      return;
    }
    Logger.d("FlxMain", "No profileImage url!");
  }

  public long getAssetId(long paramLong)
  {
    Logger.d("FlxMain", "User.getAssetId matching right id: " + paramLong);
    Iterator localIterator = this.lockerRights.iterator();
    LockerRight localLockerRight;
    do
    {
      if (!localIterator.hasNext())
        return 0L;
      localLockerRight = (LockerRight)localIterator.next();
      Logger.d("FlxMain", "User.getAssetId " + localLockerRight);
    }
    while (localLockerRight.rightId != paramLong);
    return localLockerRight.assetId;
  }

  public int getGiftMoviesEarnedCount()
  {
    List localList = this.lockerRights;
    int i = 0;
    ArrayList localArrayList1;
    ArrayList localArrayList2;
    ArrayList localArrayList3;
    ArrayList localArrayList4;
    ArrayList localArrayList5;
    ArrayList localArrayList6;
    Iterator localIterator;
    if (localList != null)
    {
      localArrayList1 = null;
      localArrayList2 = null;
      localArrayList3 = null;
      localArrayList4 = null;
      localArrayList5 = null;
      localArrayList6 = null;
      localIterator = this.lockerRights.iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
        return i;
      LockerRight localLockerRight = (LockerRight)localIterator.next();
      switch ($SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType()[localLockerRight.purchaseType.ordinal()])
      {
      default:
        break;
      case 1:
        if (localArrayList1 == null)
          localArrayList1 = new ArrayList();
        localArrayList1.add(localLockerRight);
        i++;
        break;
      case 2:
        if (localArrayList2 == null)
          localArrayList2 = new ArrayList();
        localArrayList2.add(localLockerRight);
        i++;
        break;
      case 3:
        if (localArrayList3 == null)
          localArrayList3 = new ArrayList();
        localArrayList3.add(localLockerRight);
        i++;
        break;
      case 4:
        if (localArrayList4 == null)
          localArrayList4 = new ArrayList();
        localArrayList4.add(localLockerRight);
        i++;
        break;
      case 5:
        if (localArrayList5 == null)
          localArrayList5 = new ArrayList();
        localArrayList5.add(localLockerRight);
        i++;
        break;
      case 6:
        if (localArrayList6 == null)
          localArrayList6 = new ArrayList();
        localArrayList6.add(localLockerRight);
        i++;
      }
    }
  }

  public String getId()
  {
    return this.id;
  }

  public int getLockerNonEpisodesCount()
  {
    List localList = this.lockerRights;
    int i = 0;
    Iterator localIterator;
    if (localList != null)
      localIterator = this.lockerRights.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return i;
      LockerRight localLockerRight = (LockerRight)localIterator.next();
      if ((!localLockerRight.isEpisode()) && (!localLockerRight.isRental))
        i++;
    }
  }

  public LockerRight getLockerRightFromAssetId(long paramLong)
  {
    if (this.lockerRights == null)
    {
      Logger.w("FlxMain", "User.getLockerRight lockerRights null");
      return null;
    }
    Iterator localIterator = this.lockerRights.iterator();
    LockerRight localLockerRight;
    do
    {
      if (!localIterator.hasNext())
        return null;
      localLockerRight = (LockerRight)localIterator.next();
    }
    while (localLockerRight.assetId != paramLong);
    return localLockerRight;
  }

  public LockerRight getLockerRightFromRightId(long paramLong)
  {
    if (this.lockerRights == null)
      return null;
    Iterator localIterator = this.lockerRights.iterator();
    LockerRight localLockerRight;
    do
    {
      if (!localIterator.hasNext())
        return null;
      localLockerRight = (LockerRight)localIterator.next();
    }
    while (localLockerRight.rightId != paramLong);
    return localLockerRight;
  }

  public long getLockerRightId(long paramLong)
  {
    LockerRight localLockerRight = getLockerRightFromAssetId(paramLong);
    if (localLockerRight == null)
      return 0L;
    return localLockerRight.rightId;
  }

  public List<LockerRight> getLockerRights()
  {
    if (this.lockerRights == null)
      return EMPTY_RIGHTS;
    return this.lockerRights;
  }

  public int getLockerRightsCount()
  {
    if (this.lockerRights == null);
    for (int i = 0; ; i = this.lockerRights.size())
    {
      Logger.d("FlxMain", "User.getLockerRightsCount " + i);
      return i;
    }
  }

  public <T extends ImageView> Bitmap getProfileBitmap(T paramT)
  {
    Bitmap localBitmap;
    if (!isProfileUrlAvailable())
      localBitmap = null;
    do
    {
      return localBitmap;
      if (this.profileBitmap == null)
        this.profileBitmap = new Image(this.profileImage);
      localBitmap = this.profileBitmap.getBitmap(paramT);
    }
    while ((localBitmap != null) || (this.thumbnailBitmap == null));
    this.thumbnailBitmap.getBitmap(null);
    return localBitmap;
  }

  public String getProperty(String paramString)
  {
    return (String)this.mPropertyMap.get(paramString);
  }

  public int getRewardsEligibleCount()
  {
    int i = 3;
    int j = 1;
    Property localProperty = Properties.instance().getProperties();
    int k = 0;
    int m;
    int n;
    label52: int i2;
    label76: label97: int i4;
    if (localProperty != null)
    {
      if ((!this.isMskRateEligible) || (!localProperty.isQuickRateRewardEnabled))
        break label119;
      m = j;
      if ((!this.isMskWtsEligible) || (!localProperty.isQuickWtsRewardEnabled))
        break label125;
      n = j;
      int i1 = n + m;
      if ((!this.isMskSmsEligible) || (!localProperty.isSmsRewardEnabled))
        break label131;
      i2 = i;
      int i3 = i2 + i1;
      if ((!this.isMskFbInviteEligible) || (!localProperty.isFbInviteRewardEnabled))
        break label137;
      i4 = i3 + i;
      if (!this.isMskInstallMobileEligible)
        break label142;
    }
    while (true)
    {
      k = i4 + j;
      return k;
      label119: m = 0;
      break;
      label125: n = 0;
      break label52;
      label131: i2 = 0;
      break label76;
      label137: i = 0;
      break label97;
      label142: j = 0;
    }
  }

  public <T extends ImageView> Bitmap getThumbnailBitmap(T paramT)
  {
    if (!isThumbnailUrlAvailable())
      return null;
    if (this.thumbnailBitmap == null)
      this.thumbnailBitmap = new Image(this.thumbnailImage);
    return this.thumbnailBitmap.getBitmap(paramT);
  }

  public boolean hasEpisodeRightsForSeason(long paramLong)
  {
    if (getLockerRightFromRightId(LockerRight.getRawSeasonRightId(getLockerRightId(paramLong))) != null);
    for (boolean bool = true; ; bool = false)
    {
      Logger.d("FlxMain", "User.hasEpisodeRightsForSeason " + bool);
      return bool;
    }
  }

  public boolean isLockerRightsFetched()
  {
    return this.lockerRights != null;
  }

  public boolean isProfileUrlAvailable()
  {
    return (this.profileImage != null) && (!this.profileImage.endsWith("user.none.pro.jpg"));
  }

  public boolean isRewardsEligible()
  {
    Property localProperty = Properties.instance().getProperties();
    return (localProperty != null) && (((this.isMskRateEligible) && (localProperty.isQuickRateRewardEnabled)) || ((this.isMskWtsEligible) && (localProperty.isQuickWtsRewardEnabled)) || ((this.isMskSmsEligible) && (localProperty.isSmsRewardEnabled)) || ((this.isMskFbInviteEligible) && (localProperty.isFbInviteRewardEnabled)) || (this.isMskInstallMobileEligible));
  }

  public boolean isThumbnailUrlAvailable()
  {
    return (this.thumbnailImage != null) && (!this.thumbnailImage.endsWith("user.none.tmb.jpg"));
  }

  public User parseFromJSON(JSONObject paramJSONObject)
  {
    boolean bool1 = true;
    while (true)
    {
      try
      {
        arrayParseJSONObject(paramJSONObject, BASE_PROPERTY_ARRAY);
        if (paramJSONObject.has("id"))
          this.id = String.valueOf(paramJSONObject.get("id"));
        if (paramJSONObject.has("token"))
          this.token = FlixUtils.copyString(paramJSONObject.optString("token"));
        if (paramJSONObject.has("platform"))
          this.platform = FlixUtils.copyString(paramJSONObject.optString("platform"));
        if (paramJSONObject.has("userName"))
          this.userName = FlixUtils.copyString(paramJSONObject.optString("userName"));
        if (paramJSONObject.has("firstName"))
          this.firstName = FlixUtils.copyString(paramJSONObject.optString("firstName"));
        if (paramJSONObject.has("lastName"))
          this.lastName = FlixUtils.copyString(paramJSONObject.optString("lastName"));
        if ("FLX".equals(this.platform))
        {
          this.displayName = this.firstName;
          if ((this.displayName != null) && (this.lastName != null))
            this.displayName = (this.displayName + " " + this.lastName);
          if (paramJSONObject.has("gender"))
            this.gender = FlixUtils.copyString(paramJSONObject.optString("gender"));
          if (paramJSONObject.has("age"))
            this.age = paramJSONObject.optInt("age", -1);
          if (paramJSONObject.has("images"))
          {
            JSONObject localJSONObject3 = paramJSONObject.getJSONObject("images");
            this.profileImage = FlixUtils.copyString(localJSONObject3.optString("profile"));
            this.thumbnailImage = FlixUtils.copyString(localJSONObject3.optString("thumbnail"));
            this.smallImage = FlixUtils.copyString(localJSONObject3.optString("small"));
          }
          if (paramJSONObject.has("stats"))
          {
            JSONObject localJSONObject2 = paramJSONObject.getJSONObject("stats");
            this.wtsCount = localJSONObject2.optInt("wts");
            this.ratingCount = localJSONObject2.optInt("ratings");
            this.reviewCount = localJSONObject2.optInt("reviews");
            this.friendCount = localJSONObject2.optInt("friends");
            this.collectionsCount = localJSONObject2.optInt("collected");
          }
          if (!paramJSONObject.has("permissions"))
            break;
          JSONObject localJSONObject1 = paramJSONObject.getJSONObject("permissions");
          if (localJSONObject1.optInt("isMskEligible") != 0)
          {
            bool2 = bool1;
            this.isMskEligible = bool2;
            if (localJSONObject1.optInt("isMskRateEligible") == 0)
              break label602;
            bool3 = bool1;
            this.isMskRateEligible = bool3;
            if (localJSONObject1.optInt("isMskWtsEligible") == 0)
              break label608;
            bool4 = bool1;
            this.isMskWtsEligible = bool4;
            if (localJSONObject1.optInt("isMskSmsEligible") == 0)
              break label614;
            bool5 = bool1;
            this.isMskSmsEligible = bool5;
            if (localJSONObject1.optInt("isMskFbInviteEligible") == 0)
              break label620;
            bool6 = bool1;
            this.isMskFbInviteEligible = bool6;
            if (localJSONObject1.optInt("isMskUvCreateEligible") == 0)
              break label626;
            bool7 = bool1;
            this.isMskUvCreateEligible = bool7;
            if (localJSONObject1.optInt("isMskInstallMobileEligible") == 0)
              break label632;
            this.isMskInstallMobileEligible = bool1;
            return this;
          }
        }
        else
        {
          if (!paramJSONObject.has("displayName"))
            continue;
          this.displayName = FlixUtils.copyString(paramJSONObject.optString("displayName"));
          continue;
        }
      }
      catch (JSONException localJSONException)
      {
        Logger.e("FlxMain", "User object parse error ", localJSONException);
        return this;
      }
      boolean bool2 = false;
      continue;
      label602: boolean bool3 = false;
      continue;
      label608: boolean bool4 = false;
      continue;
      label614: boolean bool5 = false;
      continue;
      label620: boolean bool6 = false;
      continue;
      label626: boolean bool7 = false;
      continue;
      label632: bool1 = false;
    }
    return this;
  }

  public void removeLockerRight(LockerRight paramLockerRight)
  {
    if (this.lockerRights != null)
      this.lockerRights.remove(paramLockerRight);
  }

  public void resetLockerRights()
  {
    this.lockerRights = null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.User
 * JD-Core Version:    0.6.2
 */