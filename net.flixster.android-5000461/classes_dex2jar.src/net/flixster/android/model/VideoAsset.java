package net.flixster.android.model;

public abstract class VideoAsset
{
  protected String downloadUri;
  protected long editionId;
  protected long id;
  protected boolean isCollected;
  protected boolean isDownloadSupported;
  protected boolean isStreamingSupported;
  protected String title;

  public VideoAsset()
  {
  }

  public VideoAsset(long paramLong1, long paramLong2)
  {
    this.id = paramLong1;
    this.editionId = paramLong2;
  }

  public long getId()
  {
    return this.id;
  }

  public String getTitle()
  {
    return this.title;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.model.VideoAsset
 * JD-Core Version:    0.6.2
 */