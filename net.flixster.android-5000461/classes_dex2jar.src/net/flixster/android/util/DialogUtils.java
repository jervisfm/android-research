package net.flixster.android.util;

import android.content.Context;
import android.content.res.Resources;
import com.flixster.android.utils.DateTimeHelper;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import net.flixster.android.FlixsterApplication;

public class DialogUtils
{
  private static CharSequence[] sShowtimesDateOptions = null;

  public static CharSequence[] getLocationOptions()
  {
    CharSequence[] arrayOfCharSequence = new CharSequence[3];
    arrayOfCharSequence[0] = "Near me";
    arrayOfCharSequence[1] = "( change postal code )";
    arrayOfCharSequence[2] = FlixsterApplication.getUserLocation();
    return arrayOfCharSequence;
  }

  public static CharSequence[] getShowtimesDateOptions(Context paramContext)
  {
    Calendar localCalendar;
    if (sShowtimesDateOptions == null)
    {
      sShowtimesDateOptions = new CharSequence[7];
      localCalendar = Calendar.getInstance();
      sShowtimesDateOptions[0] = paramContext.getResources().getString(2131493223);
    }
    for (int i = 1; ; i++)
    {
      if (i >= sShowtimesDateOptions.length)
        return sShowtimesDateOptions;
      localCalendar.add(5, 1);
      sShowtimesDateOptions[i] = DateTimeHelper.DAY_IN_WEEK_FORMATTER.format(localCalendar.getTime());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.util.DialogUtils
 * JD-Core Version:    0.6.2
 */