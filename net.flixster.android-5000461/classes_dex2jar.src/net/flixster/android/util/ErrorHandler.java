package net.flixster.android.util;

import android.content.Context;
import android.content.Intent;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.ApiBuilder;

public class ErrorHandler
{
  private static final String DIAGNOSTICS_FILE = "flixster.stacktrace";
  private static final String EMAIL_SUBJECT = "Android Diagnostic Report v";
  private static final ErrorHandler INSTANCE = new ErrorHandler();
  private boolean hasChecked;

  private static String buildDiagnosticMessage(Thread paramThread, Throwable paramThrowable)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Uncaught exception thrown on thread " + paramThread.getName());
    localStringBuilder.append("\n");
    localStringBuilder.append(buildStackTrace(paramThrowable));
    return localStringBuilder.toString();
  }

  private static Intent buildEmailIntent(String paramString1, String paramString2, String paramString3)
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.putExtra("android.intent.extra.EMAIL", new String[] { paramString1 });
    localIntent.putExtra("android.intent.extra.SUBJECT", paramString2);
    localIntent.putExtra("android.intent.extra.TEXT", paramString3);
    localIntent.setType("message/rfc822");
    return localIntent;
  }

  private static String buildStackTrace(Throwable paramThrowable)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Throwable localThrowable = paramThrowable;
    int i = 0;
    if (localThrowable == null)
      return localStringBuilder.toString();
    if (i != 0)
      localStringBuilder.append("Caused by: ");
    localStringBuilder.append(localThrowable.toString());
    localStringBuilder.append("\n");
    StackTraceElement[] arrayOfStackTraceElement = localThrowable.getStackTrace();
    int j = arrayOfStackTraceElement.length;
    for (int k = 0; ; k++)
    {
      if (k >= j)
      {
        localThrowable = localThrowable.getCause();
        i = 1;
        break;
      }
      StackTraceElement localStackTraceElement = arrayOfStackTraceElement[k];
      localStringBuilder.append("\t");
      localStringBuilder.append(localStackTraceElement.toString());
      localStringBuilder.append("\n");
    }
  }

  // ERROR //
  private static String checkForDiagnosticMessage(Context paramContext)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: new 30	java/lang/StringBuilder
    //   5: dup
    //   6: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   9: astore_2
    //   10: new 109	java/io/BufferedReader
    //   13: dup
    //   14: new 111	java/io/InputStreamReader
    //   17: dup
    //   18: aload_0
    //   19: ldc 8
    //   21: invokevirtual 117	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   24: invokespecial 120	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   27: invokespecial 123	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   30: astore_3
    //   31: ldc 125
    //   33: ldc 127
    //   35: invokestatic 133	com/flixster/android/utils/Logger:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   38: aload_3
    //   39: invokevirtual 136	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   42: astore 10
    //   44: aload 10
    //   46: ifnonnull +20 -> 66
    //   49: aload_2
    //   50: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   53: astore 11
    //   55: aload_3
    //   56: ifnull +7 -> 63
    //   59: aload_3
    //   60: invokevirtual 139	java/io/BufferedReader:close	()V
    //   63: aload 11
    //   65: areturn
    //   66: aload_2
    //   67: aload 10
    //   69: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   72: ldc 51
    //   74: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: pop
    //   78: goto -40 -> 38
    //   81: astore 8
    //   83: aload_3
    //   84: astore_1
    //   85: ldc 125
    //   87: ldc 141
    //   89: invokestatic 133	com/flixster/android/utils/Logger:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   92: aload_1
    //   93: ifnull +7 -> 100
    //   96: aload_1
    //   97: invokevirtual 139	java/io/BufferedReader:close	()V
    //   100: aconst_null
    //   101: areturn
    //   102: astore 12
    //   104: ldc 125
    //   106: ldc 143
    //   108: aload 12
    //   110: invokestatic 147	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   113: goto -50 -> 63
    //   116: astore 9
    //   118: ldc 125
    //   120: ldc 143
    //   122: aload 9
    //   124: invokestatic 147	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   127: goto -27 -> 100
    //   130: astore 4
    //   132: ldc 125
    //   134: ldc 143
    //   136: aload 4
    //   138: invokestatic 147	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   141: aload_1
    //   142: ifnull -42 -> 100
    //   145: aload_1
    //   146: invokevirtual 139	java/io/BufferedReader:close	()V
    //   149: goto -49 -> 100
    //   152: astore 7
    //   154: ldc 125
    //   156: ldc 143
    //   158: aload 7
    //   160: invokestatic 147	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   163: goto -63 -> 100
    //   166: astore 5
    //   168: aload_1
    //   169: ifnull +7 -> 176
    //   172: aload_1
    //   173: invokevirtual 139	java/io/BufferedReader:close	()V
    //   176: aload 5
    //   178: athrow
    //   179: astore 6
    //   181: ldc 125
    //   183: ldc 143
    //   185: aload 6
    //   187: invokestatic 147	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   190: goto -14 -> 176
    //   193: astore 5
    //   195: aload_3
    //   196: astore_1
    //   197: goto -29 -> 168
    //   200: astore 4
    //   202: aload_3
    //   203: astore_1
    //   204: goto -72 -> 132
    //   207: astore 14
    //   209: aconst_null
    //   210: astore_1
    //   211: goto -126 -> 85
    //
    // Exception table:
    //   from	to	target	type
    //   31	38	81	java/io/FileNotFoundException
    //   38	44	81	java/io/FileNotFoundException
    //   49	55	81	java/io/FileNotFoundException
    //   66	78	81	java/io/FileNotFoundException
    //   59	63	102	java/io/IOException
    //   96	100	116	java/io/IOException
    //   10	31	130	java/io/IOException
    //   145	149	152	java/io/IOException
    //   10	31	166	finally
    //   85	92	166	finally
    //   132	141	166	finally
    //   172	176	179	java/io/IOException
    //   31	38	193	finally
    //   38	44	193	finally
    //   49	55	193	finally
    //   66	78	193	finally
    //   31	38	200	java/io/IOException
    //   38	44	200	java/io/IOException
    //   49	55	200	java/io/IOException
    //   66	78	200	java/io/IOException
    //   10	31	207	java/io/FileNotFoundException
  }

  public static ErrorHandler instance()
  {
    return INSTANCE;
  }

  public Intent checkForAppCrash(Context paramContext)
  {
    if (this.hasChecked);
    String str1;
    do
    {
      return null;
      this.hasChecked = true;
      str1 = checkForDiagnosticMessage(paramContext);
    }
    while (str1 == null);
    paramContext.deleteFile("flixster.stacktrace");
    String str2 = "Android Diagnostic Report v" + FlixsterApplication.getVersionName();
    String str3 = str1 + FlixsterApplication.getUserAgent();
    return buildEmailIntent(ApiBuilder.supportEmail(), str2, str3);
  }

  public void setDefaultUncaughtExceptionHandler(final Context paramContext)
  {
    Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
    {
      // ERROR //
      public void uncaughtException(Thread paramAnonymousThread, Throwable paramAnonymousThrowable)
      {
        // Byte code:
        //   0: aload_1
        //   1: aload_2
        //   2: invokestatic 38	net/flixster/android/util/ErrorHandler:access$0	(Ljava/lang/Thread;Ljava/lang/Throwable;)Ljava/lang/String;
        //   5: astore_3
        //   6: ldc 40
        //   8: aload_3
        //   9: invokestatic 46	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;)V
        //   12: aconst_null
        //   13: astore 4
        //   15: aload_0
        //   16: getfield 23	net/flixster/android/util/ErrorHandler$1:val$appContext	Landroid/content/Context;
        //   19: ldc 48
        //   21: iconst_0
        //   22: invokevirtual 54	android/content/Context:openFileOutput	(Ljava/lang/String;I)Ljava/io/FileOutputStream;
        //   25: astore 4
        //   27: aload 4
        //   29: aload_3
        //   30: invokevirtual 60	java/lang/String:getBytes	()[B
        //   33: invokevirtual 66	java/io/FileOutputStream:write	([B)V
        //   36: ldc 40
        //   38: new 68	java/lang/StringBuilder
        //   41: dup
        //   42: ldc 70
        //   44: invokespecial 73	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
        //   47: ldc 48
        //   49: invokevirtual 77	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   52: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //   55: invokestatic 84	com/flixster/android/utils/Logger:i	(Ljava/lang/String;Ljava/lang/String;)V
        //   58: aload 4
        //   60: ifnull +8 -> 68
        //   63: aload 4
        //   65: invokevirtual 87	java/io/FileOutputStream:close	()V
        //   68: aload_0
        //   69: getfield 25	net/flixster/android/util/ErrorHandler$1:val$osUeh	Ljava/lang/Thread$UncaughtExceptionHandler;
        //   72: aload_1
        //   73: aload_2
        //   74: invokeinterface 89 3 0
        //   79: return
        //   80: astore 9
        //   82: ldc 40
        //   84: ldc 91
        //   86: aload 9
        //   88: invokestatic 94	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   91: aload 4
        //   93: ifnull -25 -> 68
        //   96: aload 4
        //   98: invokevirtual 87	java/io/FileOutputStream:close	()V
        //   101: goto -33 -> 68
        //   104: astore 10
        //   106: ldc 40
        //   108: ldc 96
        //   110: aload 10
        //   112: invokestatic 94	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   115: goto -47 -> 68
        //   118: astore 7
        //   120: ldc 40
        //   122: ldc 96
        //   124: aload 7
        //   126: invokestatic 94	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   129: aload 4
        //   131: ifnull -63 -> 68
        //   134: aload 4
        //   136: invokevirtual 87	java/io/FileOutputStream:close	()V
        //   139: goto -71 -> 68
        //   142: astore 8
        //   144: ldc 40
        //   146: ldc 96
        //   148: aload 8
        //   150: invokestatic 94	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   153: goto -85 -> 68
        //   156: astore 5
        //   158: aload 4
        //   160: ifnull +8 -> 168
        //   163: aload 4
        //   165: invokevirtual 87	java/io/FileOutputStream:close	()V
        //   168: aload 5
        //   170: athrow
        //   171: astore 6
        //   173: ldc 40
        //   175: ldc 96
        //   177: aload 6
        //   179: invokestatic 94	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   182: goto -14 -> 168
        //   185: astore 11
        //   187: ldc 40
        //   189: ldc 96
        //   191: aload 11
        //   193: invokestatic 94	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   196: goto -128 -> 68
        //
        // Exception table:
        //   from	to	target	type
        //   15	58	80	java/io/FileNotFoundException
        //   96	101	104	java/io/IOException
        //   15	58	118	java/io/IOException
        //   134	139	142	java/io/IOException
        //   15	58	156	finally
        //   82	91	156	finally
        //   120	129	156	finally
        //   163	168	171	java/io/IOException
        //   63	68	185	java/io/IOException
      }
    });
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.util.ErrorHandler
 * JD-Core Version:    0.6.2
 */