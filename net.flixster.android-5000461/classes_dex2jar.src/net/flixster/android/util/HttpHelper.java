package net.flixster.android.util;

import com.flixster.android.net.HttpUnauthorizedException;
import com.flixster.android.utils.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.FlixsterCacheManager;

public class HttpHelper
{
  private static final int BUFFERSIZE = 1024;
  private static final int CHUNKSIZE = 8192;
  private static final int TIMEOUT_CONNECTION = 4000;
  private static final int TIMEOUT_HTTP_POST = 120000;
  private static final int TIMEOUT_READ = 60000;

  // ERROR //
  public static String deleteFromUrl(String paramString)
    throws IOException
  {
    // Byte code:
    //   0: ldc 25
    //   2: new 27	java/lang/StringBuilder
    //   5: dup
    //   6: ldc 29
    //   8: invokespecial 32	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   11: aload_0
    //   12: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   15: invokevirtual 40	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   18: invokestatic 46	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   21: aconst_null
    //   22: astore_1
    //   23: aload_0
    //   24: invokestatic 52	net/flixster/android/data/ApiBuilder:isSecureFlxUrl	(Ljava/lang/String;)Z
    //   27: istore 4
    //   29: aconst_null
    //   30: astore_1
    //   31: iload 4
    //   33: ifeq +167 -> 200
    //   36: new 54	com/flixster/android/net/ssl/SecureFlxHttpClient
    //   39: dup
    //   40: invokespecial 55	com/flixster/android/net/ssl/SecureFlxHttpClient:<init>	()V
    //   43: astore 5
    //   45: aload 5
    //   47: invokeinterface 61 1 0
    //   52: astore 6
    //   54: aload 6
    //   56: ldc 13
    //   58: invokestatic 67	org/apache/http/params/HttpConnectionParams:setConnectionTimeout	(Lorg/apache/http/params/HttpParams;I)V
    //   61: aload 6
    //   63: ldc 13
    //   65: invokestatic 70	org/apache/http/params/HttpConnectionParams:setSoTimeout	(Lorg/apache/http/params/HttpParams;I)V
    //   68: aload 5
    //   70: new 72	org/apache/http/client/methods/HttpDelete
    //   73: dup
    //   74: aload_0
    //   75: invokespecial 73	org/apache/http/client/methods/HttpDelete:<init>	(Ljava/lang/String;)V
    //   78: invokeinterface 77 2 0
    //   83: astore 7
    //   85: new 27	java/lang/StringBuilder
    //   88: dup
    //   89: invokespecial 78	java/lang/StringBuilder:<init>	()V
    //   92: astore 8
    //   94: aload 7
    //   96: invokeinterface 84 1 0
    //   101: astore 9
    //   103: aconst_null
    //   104: astore_1
    //   105: aload 9
    //   107: ifnull +40 -> 147
    //   110: aload 9
    //   112: invokeinterface 90 1 0
    //   117: astore_1
    //   118: new 92	java/io/BufferedReader
    //   121: dup
    //   122: new 94	java/io/InputStreamReader
    //   125: dup
    //   126: aload_1
    //   127: invokespecial 97	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   130: invokespecial 100	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   133: astore 10
    //   135: aload 10
    //   137: invokevirtual 103	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   140: astore 11
    //   142: aload 11
    //   144: ifnonnull +79 -> 223
    //   147: ldc 25
    //   149: new 27	java/lang/StringBuilder
    //   152: dup
    //   153: ldc 105
    //   155: invokespecial 32	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   158: aload 7
    //   160: invokeinterface 109 1 0
    //   165: invokeinterface 115 1 0
    //   170: invokevirtual 118	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   173: invokevirtual 40	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   176: invokestatic 46	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   179: aload 8
    //   181: invokevirtual 40	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   184: invokevirtual 123	java/lang/String:trim	()Ljava/lang/String;
    //   187: astore 13
    //   189: aload_1
    //   190: ifnull +7 -> 197
    //   193: aload_1
    //   194: invokevirtual 128	java/io/InputStream:close	()V
    //   197: aload 13
    //   199: areturn
    //   200: new 130	org/apache/http/impl/client/DefaultHttpClient
    //   203: dup
    //   204: invokespecial 131	org/apache/http/impl/client/DefaultHttpClient:<init>	()V
    //   207: astore 5
    //   209: goto -164 -> 45
    //   212: astore_2
    //   213: aload_1
    //   214: ifnull +7 -> 221
    //   217: aload_1
    //   218: invokevirtual 128	java/io/InputStream:close	()V
    //   221: aload_2
    //   222: athrow
    //   223: aload 8
    //   225: aload 11
    //   227: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   230: ldc 133
    //   232: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   235: pop
    //   236: goto -101 -> 135
    //   239: astore 14
    //   241: aload 13
    //   243: areturn
    //   244: astore_3
    //   245: goto -24 -> 221
    //
    // Exception table:
    //   from	to	target	type
    //   23	29	212	finally
    //   36	45	212	finally
    //   45	103	212	finally
    //   110	135	212	finally
    //   135	142	212	finally
    //   147	189	212	finally
    //   200	209	212	finally
    //   223	236	212	finally
    //   193	197	239	java/io/IOException
    //   217	221	244	java/io/IOException
  }

  public static String fetchUrl(URL paramURL, boolean paramBoolean1, boolean paramBoolean2)
    throws IOException
  {
    return new String(fetchUrlBytes(paramURL, paramBoolean1, paramBoolean2, false), "windows-1252");
  }

  public static byte[] fetchUrlBytes(HttpURLConnection paramHttpURLConnection)
    throws IOException
  {
    return fetchUrlBytes(paramHttpURLConnection, false);
  }

  private static byte[] fetchUrlBytes(HttpURLConnection paramHttpURLConnection, boolean paramBoolean)
    throws IOException
  {
    String str1 = FlixsterApplication.getUserAgent();
    if (paramBoolean)
      str1 = "Flixster" + str1;
    paramHttpURLConnection.setRequestProperty("User-Agent", str1);
    paramHttpURLConnection.setConnectTimeout(4000);
    paramHttpURLConnection.setReadTimeout(60000);
    String str2 = paramHttpURLConnection.getContentEncoding();
    Object localObject;
    try
    {
      InputStream localInputStream = paramHttpURLConnection.getInputStream();
      localObject = localInputStream;
      if (localObject == null)
        throw new IOException("HttpHelper.fetchUrlBytes connection stream null");
    }
    catch (IOException localIOException)
    {
      if (paramHttpURLConnection.getResponseCode() == 401)
        throw new HttpUnauthorizedException(paramHttpURLConnection.getResponseMessage(), localIOException);
      throw localIOException;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      throw new IOException(localIllegalStateException.getMessage());
    }
    if ("gzip".equalsIgnoreCase(str2))
      localObject = new GZIPInputStream((InputStream)localObject);
    return streamToByteArray((InputStream)localObject);
  }

  protected static byte[] fetchUrlBytes(URL paramURL, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    throws IOException
  {
    StringBuilder localStringBuilder1 = new StringBuilder("HttpHelper.fetchUrlBytes ");
    String str1;
    StringBuilder localStringBuilder2;
    if (paramBoolean1)
    {
      str1 = "checkCache ";
      localStringBuilder2 = localStringBuilder1.append(str1);
      if (!paramBoolean2)
        break label99;
    }
    byte[] arrayOfByte2;
    label99: for (String str2 = "shouldCache "; ; str2 = "")
    {
      Logger.d("FlxApi", str2 + paramURL);
      if (!paramBoolean1)
        break label124;
      arrayOfByte2 = FlixsterApplication.sFlixsterCacheManager.get(paramURL.hashCode());
      if ((arrayOfByte2 == null) || (arrayOfByte2.length <= 0))
        break label106;
      Logger.v("FlxApi", "HttpHelper.fetchUrlBytes cache hit");
      return arrayOfByte2;
      str1 = "";
      break;
    }
    label106: if ((arrayOfByte2 != null) && (arrayOfByte2.length == 0))
      Logger.w("FlxApi", "HttpHelper.fetchUrlBytes cache empty");
    while (true)
    {
      label124: HttpURLConnection localHttpURLConnection = (HttpURLConnection)paramURL.openConnection();
      try
      {
        byte[] arrayOfByte1 = fetchUrlBytes(localHttpURLConnection, paramBoolean3);
        localHttpURLConnection.disconnect();
        if (paramBoolean2);
        return arrayOfByte1;
        Logger.v("FlxApi", "HttpHelper.fetchUrlBytes cache miss");
      }
      finally
      {
        localHttpURLConnection.disconnect();
      }
    }
  }

  public static InputStream fetchUrlStream(URL paramURL)
  {
    try
    {
      HttpURLConnection localHttpURLConnection = (HttpURLConnection)paramURL.openConnection();
      localHttpURLConnection.setConnectTimeout(4000);
      localHttpURLConnection.setReadTimeout(60000);
      localHttpURLConnection.connect();
      InputStream localInputStream = localHttpURLConnection.getInputStream();
      return localInputStream;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
    return null;
  }

  public static int getRemoteFileSize(URL paramURL)
    throws IOException
  {
    URLConnection localURLConnection = paramURL.openConnection();
    localURLConnection.connect();
    return localURLConnection.getContentLength();
  }

  // ERROR //
  public static String postToUrl(String paramString, ArrayList<org.apache.http.NameValuePair> paramArrayList)
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 52	net/flixster/android/data/ApiBuilder:isSecureFlxUrl	(Ljava/lang/String;)Z
    //   4: istore_2
    //   5: iload_2
    //   6: ifeq +223 -> 229
    //   9: ldc 25
    //   11: new 27	java/lang/StringBuilder
    //   14: dup
    //   15: ldc_w 275
    //   18: invokespecial 32	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   21: aload_0
    //   22: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   25: ldc_w 277
    //   28: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: aload_1
    //   32: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   35: invokevirtual 40	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   38: invokestatic 280	com/flixster/android/utils/Logger:sd	(Ljava/lang/String;Ljava/lang/String;)V
    //   41: aconst_null
    //   42: astore_3
    //   43: iload_2
    //   44: ifeq +220 -> 264
    //   47: new 54	com/flixster/android/net/ssl/SecureFlxHttpClient
    //   50: dup
    //   51: invokespecial 55	com/flixster/android/net/ssl/SecureFlxHttpClient:<init>	()V
    //   54: astore 4
    //   56: aload 4
    //   58: invokeinterface 61 1 0
    //   63: astore 7
    //   65: aload 7
    //   67: ldc 13
    //   69: invokestatic 67	org/apache/http/params/HttpConnectionParams:setConnectionTimeout	(Lorg/apache/http/params/HttpParams;I)V
    //   72: aload 7
    //   74: ldc 13
    //   76: invokestatic 70	org/apache/http/params/HttpConnectionParams:setSoTimeout	(Lorg/apache/http/params/HttpParams;I)V
    //   79: new 282	org/apache/http/client/methods/HttpPost
    //   82: dup
    //   83: aload_0
    //   84: invokespecial 283	org/apache/http/client/methods/HttpPost:<init>	(Ljava/lang/String;)V
    //   87: astore 8
    //   89: aload 8
    //   91: new 285	org/apache/http/client/entity/UrlEncodedFormEntity
    //   94: dup
    //   95: aload_1
    //   96: invokespecial 288	org/apache/http/client/entity/UrlEncodedFormEntity:<init>	(Ljava/util/List;)V
    //   99: invokevirtual 292	org/apache/http/client/methods/HttpPost:setEntity	(Lorg/apache/http/HttpEntity;)V
    //   102: aload 4
    //   104: aload 8
    //   106: invokeinterface 77 2 0
    //   111: astore 9
    //   113: new 27	java/lang/StringBuilder
    //   116: dup
    //   117: invokespecial 78	java/lang/StringBuilder:<init>	()V
    //   120: astore 10
    //   122: aload 9
    //   124: invokeinterface 84 1 0
    //   129: astore 11
    //   131: aconst_null
    //   132: astore_3
    //   133: aload 11
    //   135: ifnull +40 -> 175
    //   138: aload 11
    //   140: invokeinterface 90 1 0
    //   145: astore_3
    //   146: new 92	java/io/BufferedReader
    //   149: dup
    //   150: new 94	java/io/InputStreamReader
    //   153: dup
    //   154: aload_3
    //   155: invokespecial 97	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   158: invokespecial 100	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   161: astore 12
    //   163: aload 12
    //   165: invokevirtual 103	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   168: astore 13
    //   170: aload 13
    //   172: ifnonnull +117 -> 289
    //   175: ldc 25
    //   177: new 27	java/lang/StringBuilder
    //   180: dup
    //   181: ldc_w 294
    //   184: invokespecial 32	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   187: aload 9
    //   189: invokeinterface 109 1 0
    //   194: invokeinterface 115 1 0
    //   199: invokevirtual 118	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   202: invokevirtual 40	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   205: invokestatic 46	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   208: aload 10
    //   210: invokevirtual 40	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   213: invokevirtual 123	java/lang/String:trim	()Ljava/lang/String;
    //   216: astore 15
    //   218: aload_3
    //   219: ifnull +7 -> 226
    //   222: aload_3
    //   223: invokevirtual 128	java/io/InputStream:close	()V
    //   226: aload 15
    //   228: areturn
    //   229: ldc 25
    //   231: new 27	java/lang/StringBuilder
    //   234: dup
    //   235: ldc_w 275
    //   238: invokespecial 32	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   241: aload_0
    //   242: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   245: ldc_w 277
    //   248: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   251: aload_1
    //   252: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   255: invokevirtual 40	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   258: invokestatic 46	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   261: goto -220 -> 41
    //   264: new 130	org/apache/http/impl/client/DefaultHttpClient
    //   267: dup
    //   268: invokespecial 131	org/apache/http/impl/client/DefaultHttpClient:<init>	()V
    //   271: astore 4
    //   273: goto -217 -> 56
    //   276: astore 5
    //   278: aload_3
    //   279: ifnull +7 -> 286
    //   282: aload_3
    //   283: invokevirtual 128	java/io/InputStream:close	()V
    //   286: aload 5
    //   288: athrow
    //   289: aload 10
    //   291: aload 13
    //   293: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   296: ldc 133
    //   298: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   301: pop
    //   302: goto -139 -> 163
    //   305: astore 16
    //   307: aload 15
    //   309: areturn
    //   310: astore 6
    //   312: goto -26 -> 286
    //
    // Exception table:
    //   from	to	target	type
    //   47	56	276	finally
    //   56	131	276	finally
    //   138	163	276	finally
    //   163	170	276	finally
    //   175	218	276	finally
    //   264	273	276	finally
    //   289	302	276	finally
    //   222	226	305	java/io/IOException
    //   282	286	310	java/io/IOException
  }

  public static byte[] streamToByteArray(InputStream paramInputStream)
    throws IOException
  {
    byte[] arrayOfByte1 = new byte[1024];
    byte[] arrayOfByte2 = new byte[8192];
    ArrayList localArrayList = new ArrayList();
    int i = 8192;
    int j = 0;
    int k = paramInputStream.read(arrayOfByte1);
    byte[] arrayOfByte3;
    int m;
    Iterator localIterator;
    if (k == -1)
    {
      paramInputStream.close();
      arrayOfByte3 = new byte[j + 8192 * localArrayList.size()];
      m = 0;
      localIterator = localArrayList.iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
      {
        System.arraycopy(arrayOfByte2, 0, arrayOfByte3, m, j);
        return arrayOfByte3;
        if (k > i)
        {
          System.arraycopy(arrayOfByte1, 0, arrayOfByte2, j, i);
          localArrayList.add(arrayOfByte2);
          arrayOfByte2 = new byte[8192];
          j = k - i;
          System.arraycopy(arrayOfByte1, i, arrayOfByte2, 0, j);
        }
        while (true)
        {
          i = 8192 - j;
          break;
          System.arraycopy(arrayOfByte1, 0, arrayOfByte2, j, k);
          j += k;
        }
      }
      System.arraycopy((byte[])localIterator.next(), 0, arrayOfByte3, m, 8192);
      m += 8192;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.util.HttpHelper
 * JD-Core Version:    0.6.2
 */