package net.flixster.android.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import java.io.IOException;
import java.net.URL;
import net.flixster.android.data.ApiBuilder;

public class HttpImageHelper
{
  public static Bitmap fetchImage(URL paramURL)
    throws IOException
  {
    byte[] arrayOfByte = HttpHelper.fetchUrlBytes(paramURL, true, true, false);
    Bitmap localBitmap = BitmapFactory.decodeByteArray(arrayOfByte, 0, arrayOfByte.length);
    if (localBitmap == null)
    {
      BitmapFactory.Options localOptions = new BitmapFactory.Options();
      localOptions.inSampleSize = 4;
      localBitmap = BitmapFactory.decodeByteArray(arrayOfByte, 0, arrayOfByte.length, localOptions);
    }
    return localBitmap;
  }

  public static void fetchImageDoNotCache(URL paramURL)
    throws IOException
  {
    HttpHelper.fetchUrlBytes(paramURL, false, false, ApiBuilder.isAdTrack(paramURL.toString()));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.util.HttpImageHelper
 * JD-Core Version:    0.6.2
 */