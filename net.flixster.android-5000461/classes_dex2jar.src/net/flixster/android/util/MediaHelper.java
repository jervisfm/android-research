package net.flixster.android.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.provider.MediaStore.Images.Media;
import com.flixster.android.utils.Logger;
import java.lang.ref.SoftReference;
import net.flixster.android.model.Photo;

public class MediaHelper
{
  public static boolean savePhoto(Photo paramPhoto, Context paramContext)
  {
    String str1 = "photo" + paramPhoto.id + ".png";
    String str2 = "Flixster Movies Photo";
    StringBuilder localStringBuilder = new StringBuilder();
    if ((paramPhoto.caption != null) && (paramPhoto.caption.length() > 1))
    {
      str1 = paramPhoto.caption.replace(" ", "_");
      localStringBuilder.append(paramPhoto.caption);
    }
    if ((paramPhoto.description != null) && (paramPhoto.description.length() > 1))
    {
      if (localStringBuilder.length() > 0)
        localStringBuilder.append(" - ");
      localStringBuilder.append(paramPhoto.description);
    }
    if (localStringBuilder.length() > 0)
      str2 = localStringBuilder.toString();
    try
    {
      Logger.d("FlxMain", "FlixsterActivity savePhoto title:" + str1 + " desc:" + str2);
      String str3 = MediaStore.Images.Media.insertImage(paramContext.getContentResolver(), (Bitmap)paramPhoto.galleryBitmap.get(), str1, str2);
      return str3 != null;
    }
    catch (Exception localException)
    {
      Logger.w("FlxMain", "Problem saving image", localException);
    }
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.util.MediaHelper
 * JD-Core Version:    0.6.2
 */