package net.flixster.android.util;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class SoftHashMap<K, V> extends AbstractMap<K, V>
{
  private final Map<K, SoftValue<K, V>> hashMap = new HashMap();
  private final ReferenceQueue<V> refQueue = new ReferenceQueue();

  private void processQueue()
  {
    while (true)
    {
      SoftValue localSoftValue = (SoftValue)this.refQueue.poll();
      if (localSoftValue == null)
        break;
      this.hashMap.remove(localSoftValue.key);
    }
  }

  public void clear()
  {
    processQueue();
    this.hashMap.clear();
  }

  public Set<Map.Entry<K, V>> entrySet()
  {
    throw new UnsupportedOperationException();
  }

  public V get(Object paramObject)
  {
    SoftValue localSoftValue = (SoftValue)this.hashMap.get(paramObject);
    Object localObject = null;
    if (localSoftValue != null)
    {
      localObject = localSoftValue.get();
      if (localObject == null)
        this.hashMap.remove(paramObject);
    }
    return localObject;
  }

  public V put(K paramK, V paramV)
  {
    processQueue();
    SoftValue localSoftValue = (SoftValue)this.hashMap.put(paramK, new SoftValue(paramK, paramV, this.refQueue, null));
    Object localObject = null;
    if (localSoftValue != null)
      localObject = localSoftValue.get();
    return localObject;
  }

  public V remove(Object paramObject)
  {
    processQueue();
    SoftValue localSoftValue = (SoftValue)this.hashMap.remove(paramObject);
    if (localSoftValue != null)
      return localSoftValue.get();
    return null;
  }

  public int size()
  {
    processQueue();
    return this.hashMap.size();
  }

  private static class SoftValue<K, T> extends SoftReference<T>
  {
    private final K key;

    private SoftValue(K paramK, T paramT, ReferenceQueue<T> paramReferenceQueue)
    {
      super(paramReferenceQueue);
      this.key = paramK;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     net.flixster.android.util.SoftHashMap
 * JD-Core Version:    0.6.2
 */