package oauth.signpost;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import oauth.signpost.http.HttpParameters;
import oauth.signpost.http.HttpRequest;
import oauth.signpost.http.HttpResponse;

public abstract class AbstractOAuthProvider
  implements OAuthProvider
{
  private static final long serialVersionUID = 1L;
  private String accessTokenEndpointUrl;
  private String authorizationWebsiteUrl;
  private Map<String, String> defaultHeaders;
  private boolean isOAuth10a;
  private transient OAuthProviderListener listener;
  private String requestTokenEndpointUrl;
  private HttpParameters responseParameters;

  public AbstractOAuthProvider(String paramString1, String paramString2, String paramString3)
  {
    this.requestTokenEndpointUrl = paramString1;
    this.accessTokenEndpointUrl = paramString2;
    this.authorizationWebsiteUrl = paramString3;
    this.responseParameters = new HttpParameters();
    this.defaultHeaders = new HashMap();
  }

  protected void closeConnection(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse)
    throws Exception
  {
  }

  protected abstract HttpRequest createRequest(String paramString)
    throws Exception;

  public String getAccessTokenEndpointUrl()
  {
    return this.accessTokenEndpointUrl;
  }

  public String getAuthorizationWebsiteUrl()
  {
    return this.authorizationWebsiteUrl;
  }

  public Map<String, String> getRequestHeaders()
  {
    return this.defaultHeaders;
  }

  public String getRequestTokenEndpointUrl()
  {
    return this.requestTokenEndpointUrl;
  }

  protected String getResponseParameter(String paramString)
  {
    return this.responseParameters.getFirst(paramString);
  }

  public HttpParameters getResponseParameters()
  {
    return this.responseParameters;
  }

  protected void handleUnexpectedResponse(int paramInt, HttpResponse paramHttpResponse)
    throws Exception
  {
    if (paramHttpResponse == null)
      return;
    BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(paramHttpResponse.getContent()));
    StringBuilder localStringBuilder = new StringBuilder();
    for (String str = localBufferedReader.readLine(); str != null; str = localBufferedReader.readLine())
      localStringBuilder.append(str);
    switch (paramInt)
    {
    default:
      throw new OAuthCommunicationException("Service provider responded in error: " + paramInt + " (" + paramHttpResponse.getReasonPhrase() + ")", localStringBuilder.toString());
    case 401:
    }
    throw new OAuthNotAuthorizedException(localStringBuilder.toString());
  }

  public boolean isOAuth10a()
  {
    return this.isOAuth10a;
  }

  public void removeListener(OAuthProviderListener paramOAuthProviderListener)
  {
    this.listener = null;
  }

  public void retrieveAccessToken(OAuthConsumer paramOAuthConsumer, String paramString)
    throws OAuthMessageSignerException, OAuthNotAuthorizedException, OAuthExpectationFailedException, OAuthCommunicationException
  {
    if ((paramOAuthConsumer.getToken() == null) || (paramOAuthConsumer.getTokenSecret() == null))
      throw new OAuthExpectationFailedException("Authorized request token or token secret not set. Did you retrieve an authorized request token before?");
    if ((this.isOAuth10a) && (paramString != null))
    {
      retrieveToken(paramOAuthConsumer, this.accessTokenEndpointUrl, new String[] { "oauth_verifier", paramString });
      return;
    }
    retrieveToken(paramOAuthConsumer, this.accessTokenEndpointUrl, new String[0]);
  }

  public String retrieveRequestToken(OAuthConsumer paramOAuthConsumer, String paramString)
    throws OAuthMessageSignerException, OAuthNotAuthorizedException, OAuthExpectationFailedException, OAuthCommunicationException
  {
    paramOAuthConsumer.setTokenWithSecret(null, null);
    retrieveToken(paramOAuthConsumer, this.requestTokenEndpointUrl, new String[0]);
    this.isOAuth10a = false;
    if (this.isOAuth10a)
    {
      String str2 = this.authorizationWebsiteUrl;
      String[] arrayOfString2 = new String[2];
      arrayOfString2[0] = "oauth_token";
      arrayOfString2[1] = paramOAuthConsumer.getToken();
      return OAuth.addQueryParameters(str2, arrayOfString2);
    }
    String str1 = this.authorizationWebsiteUrl;
    String[] arrayOfString1 = new String[4];
    arrayOfString1[0] = "oauth_token";
    arrayOfString1[1] = paramOAuthConsumer.getToken();
    arrayOfString1[2] = "oauth_callback";
    arrayOfString1[3] = paramString;
    return OAuth.addQueryParameters(str1, arrayOfString1);
  }

  // ERROR //
  protected void retrieveToken(OAuthConsumer paramOAuthConsumer, String paramString, String[] paramArrayOfString)
    throws OAuthMessageSignerException, OAuthCommunicationException, OAuthNotAuthorizedException, OAuthExpectationFailedException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 166	oauth/signpost/AbstractOAuthProvider:getRequestHeaders	()Ljava/util/Map;
    //   4: astore 4
    //   6: aload_1
    //   7: invokeinterface 169 1 0
    //   12: ifnull +12 -> 24
    //   15: aload_1
    //   16: invokeinterface 172 1 0
    //   21: ifnonnull +13 -> 34
    //   24: new 130	oauth/signpost/exception/OAuthExpectationFailedException
    //   27: dup
    //   28: ldc 174
    //   30: invokespecial 141	oauth/signpost/exception/OAuthExpectationFailedException:<init>	(Ljava/lang/String;)V
    //   33: athrow
    //   34: aconst_null
    //   35: astore 5
    //   37: aconst_null
    //   38: astore 6
    //   40: aload_0
    //   41: aload_2
    //   42: invokevirtual 176	oauth/signpost/AbstractOAuthProvider:createRequest	(Ljava/lang/String;)Loauth/signpost/http/HttpRequest;
    //   45: astore 5
    //   47: aload 4
    //   49: invokeinterface 182 1 0
    //   54: invokeinterface 188 1 0
    //   59: astore 12
    //   61: aload 12
    //   63: invokeinterface 193 1 0
    //   68: istore 13
    //   70: aconst_null
    //   71: astore 6
    //   73: iload 13
    //   75: ifeq +57 -> 132
    //   78: aload 12
    //   80: invokeinterface 197 1 0
    //   85: checkcast 143	java/lang/String
    //   88: astore 14
    //   90: aload 5
    //   92: aload 14
    //   94: aload 4
    //   96: aload 14
    //   98: invokeinterface 201 2 0
    //   103: checkcast 143	java/lang/String
    //   106: invokeinterface 206 3 0
    //   111: goto -50 -> 61
    //   114: astore 11
    //   116: aload 11
    //   118: athrow
    //   119: astore 8
    //   121: aload_0
    //   122: aload 5
    //   124: aload 6
    //   126: invokevirtual 208	oauth/signpost/AbstractOAuthProvider:closeConnection	(Loauth/signpost/http/HttpRequest;Loauth/signpost/http/HttpResponse;)V
    //   129: aload 8
    //   131: athrow
    //   132: aload_3
    //   133: ifnull +27 -> 160
    //   136: new 36	oauth/signpost/http/HttpParameters
    //   139: dup
    //   140: invokespecial 37	oauth/signpost/http/HttpParameters:<init>	()V
    //   143: astore 29
    //   145: aload 29
    //   147: aload_3
    //   148: iconst_1
    //   149: invokevirtual 212	oauth/signpost/http/HttpParameters:putAll	([Ljava/lang/String;Z)V
    //   152: aload_1
    //   153: aload 29
    //   155: invokeinterface 216 2 0
    //   160: aload_0
    //   161: getfield 124	oauth/signpost/AbstractOAuthProvider:listener	Loauth/signpost/OAuthProviderListener;
    //   164: astore 15
    //   166: aconst_null
    //   167: astore 6
    //   169: aload 15
    //   171: ifnull +14 -> 185
    //   174: aload_0
    //   175: getfield 124	oauth/signpost/AbstractOAuthProvider:listener	Loauth/signpost/OAuthProviderListener;
    //   178: aload 5
    //   180: invokeinterface 222 2 0
    //   185: aload_1
    //   186: aload 5
    //   188: invokeinterface 226 2 0
    //   193: pop
    //   194: aload_0
    //   195: getfield 124	oauth/signpost/AbstractOAuthProvider:listener	Loauth/signpost/OAuthProviderListener;
    //   198: astore 17
    //   200: aconst_null
    //   201: astore 6
    //   203: aload 17
    //   205: ifnull +14 -> 219
    //   208: aload_0
    //   209: getfield 124	oauth/signpost/AbstractOAuthProvider:listener	Loauth/signpost/OAuthProviderListener;
    //   212: aload 5
    //   214: invokeinterface 229 2 0
    //   219: aload_0
    //   220: aload 5
    //   222: invokevirtual 233	oauth/signpost/AbstractOAuthProvider:sendRequest	(Loauth/signpost/http/HttpRequest;)Loauth/signpost/http/HttpResponse;
    //   225: astore 6
    //   227: aload 6
    //   229: invokeinterface 237 1 0
    //   234: istore 18
    //   236: aload_0
    //   237: getfield 124	oauth/signpost/AbstractOAuthProvider:listener	Loauth/signpost/OAuthProviderListener;
    //   240: astore 19
    //   242: iconst_0
    //   243: istore 20
    //   245: aload 19
    //   247: ifnull +22 -> 269
    //   250: aload_0
    //   251: getfield 124	oauth/signpost/AbstractOAuthProvider:listener	Loauth/signpost/OAuthProviderListener;
    //   254: aload 5
    //   256: aload 6
    //   258: invokeinterface 241 3 0
    //   263: istore 21
    //   265: iload 21
    //   267: istore 20
    //   269: iload 20
    //   271: ifeq +24 -> 295
    //   274: aload_0
    //   275: aload 5
    //   277: aload 6
    //   279: invokevirtual 208	oauth/signpost/AbstractOAuthProvider:closeConnection	(Loauth/signpost/http/HttpRequest;Loauth/signpost/http/HttpResponse;)V
    //   282: return
    //   283: astore 28
    //   285: new 94	oauth/signpost/exception/OAuthCommunicationException
    //   288: dup
    //   289: aload 28
    //   291: invokespecial 244	oauth/signpost/exception/OAuthCommunicationException:<init>	(Ljava/lang/Exception;)V
    //   294: athrow
    //   295: iload 18
    //   297: sipush 300
    //   300: if_icmplt +11 -> 311
    //   303: aload_0
    //   304: iload 18
    //   306: aload 6
    //   308: invokevirtual 246	oauth/signpost/AbstractOAuthProvider:handleUnexpectedResponse	(ILoauth/signpost/http/HttpResponse;)V
    //   311: aload 6
    //   313: invokeinterface 76 1 0
    //   318: invokestatic 250	oauth/signpost/OAuth:decodeForm	(Ljava/io/InputStream;)Loauth/signpost/http/HttpParameters;
    //   321: astore 22
    //   323: aload 22
    //   325: ldc 156
    //   327: invokevirtual 62	oauth/signpost/http/HttpParameters:getFirst	(Ljava/lang/Object;)Ljava/lang/String;
    //   330: astore 23
    //   332: aload 22
    //   334: ldc 252
    //   336: invokevirtual 62	oauth/signpost/http/HttpParameters:getFirst	(Ljava/lang/Object;)Ljava/lang/String;
    //   339: astore 24
    //   341: aload 22
    //   343: ldc 156
    //   345: invokevirtual 256	oauth/signpost/http/HttpParameters:remove	(Ljava/lang/Object;)Ljava/util/SortedSet;
    //   348: pop
    //   349: aload 22
    //   351: ldc 252
    //   353: invokevirtual 256	oauth/signpost/http/HttpParameters:remove	(Ljava/lang/Object;)Ljava/util/SortedSet;
    //   356: pop
    //   357: aload_0
    //   358: aload 22
    //   360: invokevirtual 259	oauth/signpost/AbstractOAuthProvider:setResponseParameters	(Loauth/signpost/http/HttpParameters;)V
    //   363: aload 23
    //   365: ifnull +8 -> 373
    //   368: aload 24
    //   370: ifnonnull +19 -> 389
    //   373: new 130	oauth/signpost/exception/OAuthExpectationFailedException
    //   376: dup
    //   377: ldc_w 261
    //   380: invokespecial 141	oauth/signpost/exception/OAuthExpectationFailedException:<init>	(Ljava/lang/String;)V
    //   383: athrow
    //   384: astore 10
    //   386: aload 10
    //   388: athrow
    //   389: aload_1
    //   390: aload 23
    //   392: aload 24
    //   394: invokeinterface 154 3 0
    //   399: aload_0
    //   400: aload 5
    //   402: aload 6
    //   404: invokevirtual 208	oauth/signpost/AbstractOAuthProvider:closeConnection	(Loauth/signpost/http/HttpRequest;Loauth/signpost/http/HttpResponse;)V
    //   407: return
    //   408: astore 27
    //   410: new 94	oauth/signpost/exception/OAuthCommunicationException
    //   413: dup
    //   414: aload 27
    //   416: invokespecial 244	oauth/signpost/exception/OAuthCommunicationException:<init>	(Ljava/lang/Exception;)V
    //   419: athrow
    //   420: astore 7
    //   422: new 94	oauth/signpost/exception/OAuthCommunicationException
    //   425: dup
    //   426: aload 7
    //   428: invokespecial 244	oauth/signpost/exception/OAuthCommunicationException:<init>	(Ljava/lang/Exception;)V
    //   431: athrow
    //   432: astore 9
    //   434: new 94	oauth/signpost/exception/OAuthCommunicationException
    //   437: dup
    //   438: aload 9
    //   440: invokespecial 244	oauth/signpost/exception/OAuthCommunicationException:<init>	(Ljava/lang/Exception;)V
    //   443: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   40	61	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   61	70	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   78	111	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   136	160	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   160	166	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   174	185	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   185	200	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   208	219	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   219	242	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   250	265	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   303	311	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   311	363	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   373	384	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   389	399	114	oauth/signpost/exception/OAuthNotAuthorizedException
    //   40	61	119	finally
    //   61	70	119	finally
    //   78	111	119	finally
    //   116	119	119	finally
    //   136	160	119	finally
    //   160	166	119	finally
    //   174	185	119	finally
    //   185	200	119	finally
    //   208	219	119	finally
    //   219	242	119	finally
    //   250	265	119	finally
    //   303	311	119	finally
    //   311	363	119	finally
    //   373	384	119	finally
    //   386	389	119	finally
    //   389	399	119	finally
    //   422	432	119	finally
    //   274	282	283	java/lang/Exception
    //   40	61	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   61	70	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   78	111	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   136	160	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   160	166	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   174	185	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   185	200	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   208	219	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   219	242	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   250	265	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   303	311	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   311	363	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   373	384	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   389	399	384	oauth/signpost/exception/OAuthExpectationFailedException
    //   399	407	408	java/lang/Exception
    //   40	61	420	java/lang/Exception
    //   61	70	420	java/lang/Exception
    //   78	111	420	java/lang/Exception
    //   136	160	420	java/lang/Exception
    //   160	166	420	java/lang/Exception
    //   174	185	420	java/lang/Exception
    //   185	200	420	java/lang/Exception
    //   208	219	420	java/lang/Exception
    //   219	242	420	java/lang/Exception
    //   250	265	420	java/lang/Exception
    //   303	311	420	java/lang/Exception
    //   311	363	420	java/lang/Exception
    //   373	384	420	java/lang/Exception
    //   389	399	420	java/lang/Exception
    //   121	129	432	java/lang/Exception
  }

  protected abstract HttpResponse sendRequest(HttpRequest paramHttpRequest)
    throws Exception;

  public void setListener(OAuthProviderListener paramOAuthProviderListener)
  {
    this.listener = paramOAuthProviderListener;
  }

  public void setOAuth10a(boolean paramBoolean)
  {
    this.isOAuth10a = paramBoolean;
  }

  public void setRequestHeader(String paramString1, String paramString2)
  {
    this.defaultHeaders.put(paramString1, paramString2);
  }

  public void setResponseParameters(HttpParameters paramHttpParameters)
  {
    this.responseParameters = paramHttpParameters;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     oauth.signpost.AbstractOAuthProvider
 * JD-Core Version:    0.6.2
 */