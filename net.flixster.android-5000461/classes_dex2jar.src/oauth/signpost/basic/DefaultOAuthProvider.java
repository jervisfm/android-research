package oauth.signpost.basic;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import oauth.signpost.AbstractOAuthProvider;
import oauth.signpost.http.HttpRequest;
import oauth.signpost.http.HttpResponse;

public class DefaultOAuthProvider extends AbstractOAuthProvider
{
  private static final long serialVersionUID = 1L;

  public DefaultOAuthProvider(String paramString1, String paramString2, String paramString3)
  {
    super(paramString1, paramString2, paramString3);
  }

  protected void closeConnection(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse)
  {
    HttpURLConnection localHttpURLConnection = (HttpURLConnection)paramHttpRequest.unwrap();
    if (localHttpURLConnection != null)
      localHttpURLConnection.disconnect();
  }

  protected HttpRequest createRequest(String paramString)
    throws MalformedURLException, IOException
  {
    HttpURLConnection localHttpURLConnection = (HttpURLConnection)new URL(paramString).openConnection();
    localHttpURLConnection.setRequestMethod("POST");
    localHttpURLConnection.setAllowUserInteraction(false);
    localHttpURLConnection.setRequestProperty("Content-Length", "0");
    return new HttpURLConnectionRequestAdapter(localHttpURLConnection);
  }

  protected HttpResponse sendRequest(HttpRequest paramHttpRequest)
    throws IOException
  {
    HttpURLConnection localHttpURLConnection = (HttpURLConnection)paramHttpRequest.unwrap();
    localHttpURLConnection.connect();
    return new HttpURLConnectionResponseAdapter(localHttpURLConnection);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     oauth.signpost.basic.DefaultOAuthProvider
 * JD-Core Version:    0.6.2
 */