package oauth.signpost.exception;

public abstract class OAuthException extends Exception
{
  public OAuthException(String paramString)
  {
    super(paramString);
  }

  public OAuthException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public OAuthException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     oauth.signpost.exception.OAuthException
 * JD-Core Version:    0.6.2
 */