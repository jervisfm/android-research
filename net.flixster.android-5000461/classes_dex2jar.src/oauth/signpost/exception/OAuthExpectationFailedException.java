package oauth.signpost.exception;

public class OAuthExpectationFailedException extends OAuthException
{
  public OAuthExpectationFailedException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     oauth.signpost.exception.OAuthExpectationFailedException
 * JD-Core Version:    0.6.2
 */