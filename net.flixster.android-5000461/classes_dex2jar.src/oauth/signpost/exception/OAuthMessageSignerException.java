package oauth.signpost.exception;

public class OAuthMessageSignerException extends OAuthException
{
  public OAuthMessageSignerException(Exception paramException)
  {
    super(paramException);
  }

  public OAuthMessageSignerException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     oauth.signpost.exception.OAuthMessageSignerException
 * JD-Core Version:    0.6.2
 */