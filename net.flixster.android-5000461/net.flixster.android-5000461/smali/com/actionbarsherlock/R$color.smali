.class public final Lcom/actionbarsherlock/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/actionbarsherlock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abs__background_holo_dark:I = 0x7f090000

.field public static final abs__background_holo_light:I = 0x7f090001

.field public static final abs__bright_foreground_disabled_holo_dark:I = 0x7f090004

.field public static final abs__bright_foreground_disabled_holo_light:I = 0x7f090005

.field public static final abs__bright_foreground_holo_dark:I = 0x7f090002

.field public static final abs__bright_foreground_holo_light:I = 0x7f090003

.field public static final abs__bright_foreground_inverse_holo_dark:I = 0x7f090006

.field public static final abs__bright_foreground_inverse_holo_light:I = 0x7f090007

.field public static final abs__holo_blue_light:I = 0x7f090008

.field public static final abs__primary_text_disable_only_holo_dark:I = 0x7f090040

.field public static final abs__primary_text_disable_only_holo_light:I = 0x7f090041

.field public static final abs__primary_text_holo_dark:I = 0x7f090042

.field public static final abs__primary_text_holo_light:I = 0x7f090043

.field public static final actionbar_bg:I = 0x7f090028

.field public static final actionbar_white:I = 0x7f090029

.field public static final ad_background:I = 0x7f090017

.field public static final bg_active:I = 0x7f090019

.field public static final bg_inactive:I = 0x7f090018

.field public static final bg_selected:I = 0x7f09003d

.field public static final bg_subheader:I = 0x7f09003f

.field public static final bgborder:I = 0x7f090039

.field public static final black:I = 0x7f09000c

.field public static final button_subnav_textcolor:I = 0x7f090044

.field public static final darkgreen:I = 0x7f09000a

.field public static final dialog_bg_gray:I = 0x7f090026

.field public static final dialog_transparent_black:I = 0x7f09001c

.field public static final divider:I = 0x7f09001a

.field public static final downloaded_purple:I = 0x7f090027

.field public static final fb_button_base:I = 0x7f090010

.field public static final fb_button_border:I = 0x7f090011

.field public static final fb_button_highlight:I = 0x7f09000f

.field public static final green1:I = 0x7f090012

.field public static final green2:I = 0x7f090013

.field public static final green3:I = 0x7f090014

.field public static final grey05:I = 0x7f090015

.field public static final greyhell:I = 0x7f090016

.field public static final hc_actionbar_bg:I = 0x7f09003e

.field public static final highlight:I = 0x7f090034

.field public static final hyperlink:I = 0x7f09001f

.field public static final hyperlink_real:I = 0x7f090020

.field public static final image_highlight:I = 0x7f09002a

.field public static final leftnav_bar_tab_text_light:I = 0x7f090045

.field public static final light_gray:I = 0x7f09001e

.field public static final lightgray:I = 0x7f09000d

.field public static final list_activated:I = 0x7f09003c

.field public static final list_normal:I = 0x7f09003b

.field public static final netflix_red:I = 0x7f09000e

.field public static final posterborder_semitransparent:I = 0x7f09003a

.field public static final pressed_flxabv44:I = 0x7f09002c

.field public static final red:I = 0x7f090009

.field public static final rental_orange:I = 0x7f09002b

.field public static final stackbar_accent:I = 0x7f09002e

.field public static final stackbar_bg:I = 0x7f09002d

.field public static final stackbar_bg_divider:I = 0x7f090030

.field public static final stackbar_bg_pressed:I = 0x7f09002f

.field public static final subheader:I = 0x7f090033

.field public static final subheader_bg:I = 0x7f090031

.field public static final subheader_shadow:I = 0x7f090023

.field public static final subheader_text:I = 0x7f090032

.field public static final subnav_ics_textcolor_selector:I = 0x7f090046

.field public static final subnav_textcolor:I = 0x7f090025

.field public static final subnavbar_shadow:I = 0x7f090024

.field public static final subtitle_grey:I = 0x7f090021

.field public static final tabtext_default:I = 0x7f090038

.field public static final tabtextcolor_selector:I = 0x7f090047

.field public static final textcolor_selector:I = 0x7f090048

.field public static final topbar_bg:I = 0x7f090022

.field public static final transparent_black:I = 0x7f09001b

.field public static final transparent_gray:I = 0x7f09001d

.field public static final tvblack:I = 0x7f090036

.field public static final tvblack_semitransparent:I = 0x7f090037

.field public static final tvwhite:I = 0x7f090035

.field public static final white:I = 0x7f09000b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
