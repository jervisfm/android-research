.class public final Lcom/actionbarsherlock/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/actionbarsherlock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abs__action_bar_default_height:I = 0x7f0a0001

.field public static final abs__action_bar_icon_vertical_padding:I = 0x7f0a0002

.field public static final abs__action_bar_subtitle_bottom_margin:I = 0x7f0a0006

.field public static final abs__action_bar_subtitle_text_size:I = 0x7f0a0004

.field public static final abs__action_bar_subtitle_top_margin:I = 0x7f0a0005

.field public static final abs__action_bar_title_text_size:I = 0x7f0a0003

.field public static final abs__action_button_min_width:I = 0x7f0a0007

.field public static final abs__alert_dialog_title_height:I = 0x7f0a0008

.field public static final abs__config_prefDialogWidth:I = 0x7f0a0000

.field public static final abs__dialog_min_width_major:I = 0x7f0a0009

.field public static final abs__dialog_min_width_minor:I = 0x7f0a000a

.field public static final action_button_min_width:I = 0x7f0a000b

.field public static final ads_direct_height:I = 0x7f0a0023

.field public static final ads_web_height:I = 0x7f0a0024

.field public static final bgborder_width:I = 0x7f0a0061

.field public static final carousel_caption_height:I = 0x7f0a0041

.field public static final carousel_gradient_height:I = 0x7f0a0040

.field public static final carousel_page_indicator_height:I = 0x7f0a0042

.field public static final critic_photo_size:I = 0x7f0a0038

.field public static final divider_height:I = 0x7f0a0025

.field public static final expdate_dialog_font:I = 0x7f0a0019

.field public static final expdate_dialog_scrollheight:I = 0x7f0a001a

.field public static final facebookbutton_lpad:I = 0x7f0a0028

.field public static final facebookbutton_rpad:I = 0x7f0a0029

.field public static final facebookbutton_textheight:I = 0x7f0a0026

.field public static final facebookbutton_vpad:I = 0x7f0a0027

.field public static final friend_activity_user_photo:I = 0x7f0a0044

.field public static final friends_subtab_photo_size:I = 0x7f0a0045

.field public static final gallery_photo_size:I = 0x7f0a0039

.field public static final header_pad:I = 0x7f0a000d

.field public static final home_carousel_height:I = 0x7f0a002e

.field public static final home_headline_size:I = 0x7f0a003a

.field public static final iconpanel_height:I = 0x7f0a0021

.field public static final iconpanel_padding:I = 0x7f0a001c

.field public static final iconpanel_padding_bottom:I = 0x7f0a001f

.field public static final iconpanel_padding_drawable:I = 0x7f0a0020

.field public static final iconpanel_padding_left:I = 0x7f0a001d

.field public static final iconpanel_padding_top:I = 0x7f0a001e

.field public static final left_nav_collapsed_apparent_width:I = 0x7f0a0064

.field public static final left_nav_collapsed_width:I = 0x7f0a0063

.field public static final left_nav_expanded_apparent_width:I = 0x7f0a0066

.field public static final left_nav_expanded_width:I = 0x7f0a0065

.field public static final left_nav_icon_size:I = 0x7f0a0067

.field public static final load_more_margin_l:I = 0x7f0a004a

.field public static final morepanel_padding_bottom:I = 0x7f0a004c

.field public static final morepanel_padding_left:I = 0x7f0a004d

.field public static final morepanel_padding_top:I = 0x7f0a004b

.field public static final movie_list_height:I = 0x7f0a0034

.field public static final movie_list_width:I = 0x7f0a0033

.field public static final movie_pro_height:I = 0x7f0a0036

.field public static final movie_pro_width:I = 0x7f0a0035

.field public static final movie_tile_poster_height:I = 0x7f0a0048

.field public static final movie_tile_poster_width:I = 0x7f0a0047

.field public static final movie_tile_progress_bar_height:I = 0x7f0a0049

.field public static final movie_tile_width:I = 0x7f0a0046

.field public static final moviedetails_photobox:I = 0x7f0a0037

.field public static final msk_prompt_margin_top:I = 0x7f0a005a

.field public static final navbar_button_pad:I = 0x7f0a0010

.field public static final navbar_pad_top:I = 0x7f0a000f

.field public static final page_indicator_unit:I = 0x7f0a0043

.field public static final relative_pixel:I = 0x7f0a0022

.field public static final search_photo_size:I = 0x7f0a003b

.field public static final showtimespanel_padding:I = 0x7f0a0062

.field public static final spacing_large:I = 0x7f0a002a

.field public static final spacing_lg:I = 0x7f0a0060

.field public static final spacing_line:I = 0x7f0a002d

.field public static final spacing_md:I = 0x7f0a005e

.field public static final spacing_mdp:I = 0x7f0a005f

.field public static final spacing_normal:I = 0x7f0a002b

.field public static final spacing_sm:I = 0x7f0a005d

.field public static final spacing_small:I = 0x7f0a002c

.field public static final spacing_smx:I = 0x7f0a005c

.field public static final spacing_smxx:I = 0x7f0a005b

.field public static final stackbar_divider_height:I = 0x7f0a0050

.field public static final stackbar_divider_width:I = 0x7f0a004f

.field public static final stackbar_indicator_height:I = 0x7f0a004e

.field public static final subheader_padding_b:I = 0x7f0a0057

.field public static final subheader_padding_l:I = 0x7f0a0056

.field public static final subheader_padding_r:I = 0x7f0a0058

.field public static final subheader_padding_t:I = 0x7f0a0059

.field public static final subnavbar_button_margin_h:I = 0x7f0a0055

.field public static final subnavbar_margin_h:I = 0x7f0a0053

.field public static final subnavbar_margin_v:I = 0x7f0a0054

.field public static final subnavbar_padding_h:I = 0x7f0a0051

.field public static final subnavbar_padding_v:I = 0x7f0a0052

.field public static final theater_star_margin:I = 0x7f0a002f

.field public static final ti_indent:I = 0x7f0a000c

.field public static final ticket_bar_leftm_counter:I = 0x7f0a0011

.field public static final ticket_bar_leftm_dec:I = 0x7f0a0012

.field public static final ticket_bar_leftm_label:I = 0x7f0a0013

.field public static final ticket_bar_leftm_price:I = 0x7f0a0014

.field public static final ticket_bar_leftm_total:I = 0x7f0a0015

.field public static final ticket_icon_size:I = 0x7f0a003c

.field public static final ticket_leftm:I = 0x7f0a0016

.field public static final ticket_num_width:I = 0x7f0a003f

.field public static final ticket_rightm:I = 0x7f0a0018

.field public static final ticket_topm:I = 0x7f0a0017

.field public static final ticketconfirm_infomargin:I = 0x7f0a003e

.field public static final ticketconfirm_padding:I = 0x7f0a001b

.field public static final ticketprice_width:I = 0x7f0a003d

.field public static final title_bar_apparent_height:I = 0x7f0a0069

.field public static final title_bar_height:I = 0x7f0a0068

.field public static final topbar_pad:I = 0x7f0a000e

.field public static final user_header_photo_size:I = 0x7f0a0030

.field public static final user_profile_height:I = 0x7f0a0032

.field public static final user_profile_width:I = 0x7f0a0031


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
