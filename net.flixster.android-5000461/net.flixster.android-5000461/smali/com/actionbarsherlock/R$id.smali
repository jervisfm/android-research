.class public final Lcom/actionbarsherlock/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/actionbarsherlock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final AdBundleLayout:I = 0x7f07018f

.field public static final LinearLayout01:I = 0x7f0700d2

.field public static final LinearLayout02:I = 0x7f0700a1

.field public static final RelativeLayout01:I = 0x7f0700fd

.field public static final RelativeLayout02:I = 0x7f0702c1

.field public static final RelativeLayoutMovieDetails:I = 0x7f070143

.field public static final ScrollView01:I = 0x7f07003b

.field public static final ScrollViewBody:I = 0x7f070142

.field public static final SubCapLinearLayout:I = 0x7f07014e

.field public static final TextView01:I = 0x7f070207

.field public static final abs__action_bar:I = 0x7f070022

.field public static final abs__action_bar_container:I = 0x7f070021

.field public static final abs__action_bar_subtitle:I = 0x7f070011

.field public static final abs__action_bar_title:I = 0x7f070010

.field public static final abs__action_context_bar:I = 0x7f070023

.field public static final abs__action_menu_divider:I = 0x7f07000c

.field public static final abs__action_menu_presenter:I = 0x7f07000d

.field public static final abs__action_mode_bar:I = 0x7f070026

.field public static final abs__action_mode_bar_stub:I = 0x7f070025

.field public static final abs__action_mode_close_button:I = 0x7f070014

.field public static final abs__activity_chooser_view_content:I = 0x7f070015

.field public static final abs__checkbox:I = 0x7f07001e

.field public static final abs__content:I = 0x7f07001d

.field public static final abs__default_activity_button:I = 0x7f070018

.field public static final abs__expand_activities_button:I = 0x7f070016

.field public static final abs__home:I = 0x7f07000a

.field public static final abs__icon:I = 0x7f07001a

.field public static final abs__image:I = 0x7f070017

.field public static final abs__imageButton:I = 0x7f070012

.field public static final abs__list_item:I = 0x7f070019

.field public static final abs__progress_circular:I = 0x7f07000e

.field public static final abs__progress_horizontal:I = 0x7f07000f

.field public static final abs__radio:I = 0x7f070020

.field public static final abs__shortcut:I = 0x7f07001f

.field public static final abs__split_action_bar:I = 0x7f070024

.field public static final abs__textButton:I = 0x7f070013

.field public static final abs__title:I = 0x7f07001b

.field public static final abs__titleDivider:I = 0x7f07001c

.field public static final abs__up:I = 0x7f07000b

.field public static final action_bar_home:I = 0x7f0700d6

.field public static final actorBiography:I = 0x7f070030

.field public static final actorBirthday:I = 0x7f07002d

.field public static final actorBirthplace:I = 0x7f07002e

.field public static final actorChars:I = 0x7f070104

.field public static final actorGalleryPhotos:I = 0x7f07003a

.field public static final actorLayout:I = 0x7f070028

.field public static final actorName:I = 0x7f07002c

.field public static final actorPhoto:I = 0x7f07002b

.field public static final actorPhotos:I = 0x7f070032

.field public static final actorPhotosTitle:I = 0x7f070031

.field public static final actorTitle:I = 0x7f070029

.field public static final adView:I = 0x7f07002a

.field public static final ad_label:I = 0x7f0702df

.field public static final ad_skip:I = 0x7f0702e1

.field public static final ad_stickybottom:I = 0x7f07018e

.field public static final ad_stickytop:I = 0x7f070141

.field public static final ad_visit:I = 0x7f0702e0

.field public static final adadmin_dc_testvalue_button:I = 0x7f070040

.field public static final adadmin_dialog_dctest_field:I = 0x7f07006b

.field public static final adadmin_dialog_dctest_label:I = 0x7f07006a

.field public static final adadmin_disable_launch_cap:I = 0x7f070042

.field public static final adadmin_disable_postitial_cap:I = 0x7f07004d

.field public static final adadmin_disable_preroll_cap:I = 0x7f070043

.field public static final adadmin_install_posixtime_add12hours:I = 0x7f07003e

.field public static final adadmin_install_posixtime_sub:I = 0x7f07003c

.field public static final adadmin_install_posixtime_subtract3days:I = 0x7f07003d

.field public static final adadmin_payload:I = 0x7f070044

.field public static final admin_adsmenu_button:I = 0x7f070045

.field public static final admin_apisource_label:I = 0x7f07003f

.field public static final admin_apisource_rb_production:I = 0x7f070048

.field public static final admin_apisource_rb_qapreview:I = 0x7f07004b

.field public static final admin_apisource_rb_qatrunk:I = 0x7f07004a

.field public static final admin_apisource_rb_staging:I = 0x7f070049

.field public static final admin_apisource_rg:I = 0x7f070047

.field public static final admin_doubleClick_target:I = 0x7f07004e

.field public static final admin_fc_button:I = 0x7f070046

.field public static final adsadmin_payload_subheader:I = 0x7f070041

.field public static final adsadmin_payload_subheader2:I = 0x7f07004c

.field public static final audience:I = 0x7f070129

.field public static final audience_icon:I = 0x7f07012a

.field public static final audience_score:I = 0x7f07012b

.field public static final author:I = 0x7f070063

.field public static final available_rewards:I = 0x7f070230

.field public static final back:I = 0x7f07011f

.field public static final bg_line:I = 0x7f07011e

.field public static final biographyTitle:I = 0x7f07002f

.field public static final buttonConnect:I = 0x7f070058

.field public static final buttonbar:I = 0x7f07011c

.field public static final caption1:I = 0x7f0702e2

.field public static final caption2:I = 0x7f0702e3

.field public static final caption3:I = 0x7f0702e4

.field public static final caption4:I = 0x7f0702e5

.field public static final cast:I = 0x7f070124

.field public static final close:I = 0x7f070199

.field public static final collectedCount:I = 0x7f0701e2

.field public static final completed_rewards:I = 0x7f070232

.field public static final content_layout:I = 0x7f070075

.field public static final criticview_item_photo:I = 0x7f070064

.field public static final current_location:I = 0x7f07023c

.field public static final current_location_label:I = 0x7f07023e

.field public static final current_location_message:I = 0x7f070248

.field public static final current_location_text:I = 0x7f07023d

.field public static final custom_view_frame_layout:I = 0x7f0700cd

.field public static final delete_button:I = 0x7f07006d

.field public static final disableHome:I = 0x7f070009

.field public static final divider:I = 0x7f0700b4

.field public static final download_panel:I = 0x7f070072

.field public static final download_text:I = 0x7f07006c

.field public static final edit_text:I = 0x7f070233

.field public static final emailEditText:I = 0x7f0700e8

.field public static final errorText:I = 0x7f0700ec

.field public static final error_image:I = 0x7f070112

.field public static final facebookButton:I = 0x7f0700ed

.field public static final fb_login_upsell:I = 0x7f07007c

.field public static final fb_login_upsell_button:I = 0x7f07007e

.field public static final fb_login_upsell_title:I = 0x7f07007d

.field public static final fbinvite_send:I = 0x7f07007b

.field public static final fbinvite_signup1:I = 0x7f070078

.field public static final fbinvite_signup2:I = 0x7f070079

.field public static final fbinvite_signup3:I = 0x7f07007a

.field public static final fbinvite_text1:I = 0x7f070076

.field public static final fbinvite_text2:I = 0x7f070077

.field public static final feed_permission:I = 0x7f07007f

.field public static final feedbackLayout:I = 0x7f070080

.field public static final feedback_headertext:I = 0x7f070081

.field public static final filmographyMovies:I = 0x7f070035

.field public static final filmographyTitle:I = 0x7f070034

.field public static final firstName:I = 0x7f070095

.field public static final flipper:I = 0x7f070223

.field public static final flixsterLoginError:I = 0x7f070089

.field public static final flixsterLoginLayout:I = 0x7f070085

.field public static final flixsterLoginText:I = 0x7f070086

.field public static final frag_content:I = 0x7f070084

.field public static final frag_list:I = 0x7f070083

.field public static final frags:I = 0x7f070082

.field public static final friendFlixScore:I = 0x7f070193

.field public static final friendImage:I = 0x7f070094

.field public static final friendLayout:I = 0x7f070093

.field public static final friendRtScore:I = 0x7f070195

.field public static final friendScore:I = 0x7f070133

.field public static final friend_activity_image:I = 0x7f07008b

.field public static final friend_activity_movie:I = 0x7f070091

.field public static final friend_activity_rating:I = 0x7f07008f

.field public static final friend_activity_review:I = 0x7f070090

.field public static final friend_activity_title:I = 0x7f07008c

.field public static final friend_activity_title_action:I = 0x7f07008e

.field public static final friend_activity_title_name:I = 0x7f07008d

.field public static final friend_select_add_contact:I = 0x7f070098

.field public static final friend_select_invite:I = 0x7f07009b

.field public static final friend_select_more:I = 0x7f07009a

.field public static final friend_select_selected:I = 0x7f070099

.field public static final friendsCount:I = 0x7f0701e8

.field public static final friendsrated_activity:I = 0x7f0700a7

.field public static final friendsrated_none:I = 0x7f0700a8

.field public static final friendsrated_scroll_layout:I = 0x7f07009d

.field public static final friendsrated_scrollview:I = 0x7f07009c

.field public static final funstuff_news_panel:I = 0x7f0700cc

.field public static final gallery:I = 0x7f0700ae

.field public static final gallery_caption:I = 0x7f0700af

.field public static final gallery_counter:I = 0x7f0700ac

.field public static final gallery_header:I = 0x7f0700ab

.field public static final gallery_layout:I = 0x7f0700a9

.field public static final gallery_photolayout:I = 0x7f0700ad

.field public static final gallery_toplayout:I = 0x7f0700aa

.field public static final gridview:I = 0x7f07010f

.field public static final gtv_frag_content:I = 0x7f0700b1

.field public static final headline_image:I = 0x7f0700b5

.field public static final headline_layout:I = 0x7f07008a

.field public static final headline_title:I = 0x7f0700b6

.field public static final home:I = 0x7f0700d7

.field public static final homeAsUp:I = 0x7f070006

.field public static final homepage_adview:I = 0x7f0700b9

.field public static final homepage_carousel:I = 0x7f0700ba

.field public static final homepage_hot_content:I = 0x7f0700bb

.field public static final homepage_scroll_layout:I = 0x7f0700b8

.field public static final html_ad_webview:I = 0x7f0700cf

.field public static final html_layout:I = 0x7f0700ce

.field public static final icon:I = 0x7f070061

.field public static final interstitial_button1:I = 0x7f0700d3

.field public static final interstitial_button2:I = 0x7f0700d4

.field public static final interstitial_image:I = 0x7f0700d1

.field public static final interstitial_layout:I = 0x7f0700d0

.field public static final isPostFacebook:I = 0x7f070216

.field public static final lastName:I = 0x7f070096

.field public static final left_icon:I = 0x7f0702c8

.field public static final left_nav:I = 0x7f0700b3

.field public static final leftnav_bar_tab:I = 0x7f0700db

.field public static final linearLayout1:I = 0x7f0702e6

.field public static final linkConnect:I = 0x7f070059

.field public static final list:I = 0x7f0701fa

.field public static final listMode:I = 0x7f070002

.field public static final list_divider:I = 0x7f07012c

.field public static final loadMoreMovieActors:I = 0x7f070169

.field public static final loadMoreMovieActorsTitle:I = 0x7f07016a

.field public static final loadMoreMovieActorsTotal:I = 0x7f07016b

.field public static final loadMoreMovieDirectors:I = 0x7f070165

.field public static final loadMoreMovieDirectorsTitle:I = 0x7f070166

.field public static final loadMoreMovieDirectorsTotal:I = 0x7f070167

.field public static final load_more:I = 0x7f0700dc

.field public static final load_more_count:I = 0x7f0700de

.field public static final load_more_title:I = 0x7f0700dd

.field public static final locationPageButtonGo:I = 0x7f0700e1

.field public static final locationView:I = 0x7f0700e2

.field public static final location_label:I = 0x7f0700df

.field public static final location_text:I = 0x7f0700e0

.field public static final loginButton:I = 0x7f0700eb

.field public static final loginFacebook:I = 0x7f0701a3

.field public static final loginFlixster:I = 0x7f0701a4

.field public static final loginNetflix:I = 0x7f0700c4

.field public static final lvi_ad_stickybottom:I = 0x7f0700fc

.field public static final lvi_ad_stickytop:I = 0x7f0700fa

.field public static final lvi_bottombar:I = 0x7f0700f5

.field public static final lvi_datepanel_label:I = 0x7f0700f0

.field public static final lvi_datepanel_layout:I = 0x7f0700ef

.field public static final lvi_datepanel_text:I = 0x7f0700f1

.field public static final lvi_listview:I = 0x7f0700f4

.field public static final lvi_locationpanel_label:I = 0x7f0700f7

.field public static final lvi_locationpanel_layout:I = 0x7f0700f6

.field public static final lvi_locationpanel_text:I = 0x7f0700f8

.field public static final lvi_messagepanel_label:I = 0x7f0700f9

.field public static final lvi_searchpanel_layout:I = 0x7f070100

.field public static final lvi_staticheader:I = 0x7f0700f2

.field public static final lvi_subnav:I = 0x7f0700f3

.field public static final main:I = 0x7f0700d5

.field public static final main_phone_fragment:I = 0x7f070103

.field public static final main_view:I = 0x7f0700b0

.field public static final mapLayout:I = 0x7f0700e3

.field public static final mapView:I = 0x7f0700e4

.field public static final md_actors_short:I = 0x7f07014b

.field public static final md_bottomline:I = 0x7f070191

.field public static final md_critic_byline:I = 0x7f070065

.field public static final md_critic_icon:I = 0x7f070066

.field public static final md_critic_loadmore_rl:I = 0x7f07017e

.field public static final md_critic_loadmore_text1:I = 0x7f07017f

.field public static final md_critic_loadmore_text2:I = 0x7f070180

.field public static final md_critic_reviews_header:I = 0x7f070179

.field public static final md_critic_reviewtext:I = 0x7f070069

.field public static final md_critic_source:I = 0x7f070067

.field public static final md_criticheader_icon:I = 0x7f07017b

.field public static final md_criticreviews_linearlayout:I = 0x7f07017c

.field public static final md_criticreviews_linearlayout_more:I = 0x7f07017d

.field public static final md_download_panel:I = 0x7f070150

.field public static final md_episodes_panel:I = 0x7f070151

.field public static final md_flixster_scoreline:I = 0x7f070148

.field public static final md_footer_fresh:I = 0x7f07005b

.field public static final md_footer_popcorn:I = 0x7f07005e

.field public static final md_footer_rotten:I = 0x7f07005c

.field public static final md_footer_spilled:I = 0x7f07005f

.field public static final md_footer_title:I = 0x7f07005a

.field public static final md_footer_title_flixster:I = 0x7f07005d

.field public static final md_footer_wts:I = 0x7f070060

.field public static final md_friend_ratings:I = 0x7f07014a

.field public static final md_getglue:I = 0x7f070158

.field public static final md_links_flixster:I = 0x7f07018b

.field public static final md_links_header:I = 0x7f07018a

.field public static final md_links_imdb:I = 0x7f07018d

.field public static final md_links_rt:I = 0x7f07018c

.field public static final md_ll_moviedetails:I = 0x7f07015b

.field public static final md_metaline:I = 0x7f07014c

.field public static final md_mw_criticscore:I = 0x7f07017a

.field public static final md_mw_fxscore:I = 0x7f070183

.field public static final md_netflix:I = 0x7f070159

.field public static final md_rating_image:I = 0x7f070157

.field public static final md_rating_layout:I = 0x7f070155

.field public static final md_rating_panel:I = 0x7f070156

.field public static final md_rentalExpiration:I = 0x7f07014d

.field public static final md_review_ratingimage:I = 0x7f070068

.field public static final md_rl_critic_reviews_header:I = 0x7f070178

.field public static final md_rl_user_reviews_header:I = 0x7f070181

.field public static final md_rt_scoreline:I = 0x7f070149

.field public static final md_showtimes_panel:I = 0x7f070153

.field public static final md_trailer_panel:I = 0x7f070152

.field public static final md_user_loadmore_rl:I = 0x7f070187

.field public static final md_user_loadmore_text1:I = 0x7f070188

.field public static final md_user_loadmore_text2:I = 0x7f070189

.field public static final md_user_more_reviews_ll:I = 0x7f070186

.field public static final md_user_reviews_header:I = 0x7f070182

.field public static final md_user_reviews_header_icon:I = 0x7f070184

.field public static final md_user_reviews_linearlayout:I = 0x7f070185

.field public static final md_watchnow_panel:I = 0x7f07014f

.field public static final menu:I = 0x7f0700da

.field public static final menuAddFavorite:I = 0x7f07030d

.field public static final menuFaq:I = 0x7f070305

.field public static final menuFeedback:I = 0x7f070306

.field public static final menuHome:I = 0x7f0702fe

.field public static final menuInvite:I = 0x7f070301

.field public static final menuLogin:I = 0x7f070302

.field public static final menuMovie:I = 0x7f070309

.field public static final menuNext:I = 0x7f07030c

.field public static final menuPrevious:I = 0x7f07030b

.field public static final menuPrivacy:I = 0x7f070308

.field public static final menuRate:I = 0x7f070304

.field public static final menuRefresh:I = 0x7f070307

.field public static final menuRemoveFavorite:I = 0x7f07030e

.field public static final menuSave:I = 0x7f07030a

.field public static final menuSearch:I = 0x7f0702fa

.field public static final menuSettings:I = 0x7f0702fb

.field public static final menuShare:I = 0x7f070303

.field public static final menuSkip:I = 0x7f0702fd

.field public static final menuSort:I = 0x7f0702fc

.field public static final menuSortAlphabetical:I = 0x7f0702ff

.field public static final menuSortRatings:I = 0x7f070300

.field public static final moreFilmography:I = 0x7f070036

.field public static final moreFilmographyLabel:I = 0x7f070038

.field public static final moreFilmographyTitle:I = 0x7f070037

.field public static final moreRated:I = 0x7f0702d9

.field public static final moreRatedLabel:I = 0x7f0702db

.field public static final moreRatedTitle:I = 0x7f0702da

.field public static final moreWantToSee:I = 0x7f0702d4

.field public static final moreWantToSeeLabel:I = 0x7f0702d6

.field public static final moreWantToSeeTitle:I = 0x7f0702d5

.field public static final movieActors:I = 0x7f07010b

.field public static final movieActorsHeader:I = 0x7f070168

.field public static final movieChar:I = 0x7f070131

.field public static final movieDetails:I = 0x7f070144

.field public static final movieDirectors:I = 0x7f070164

.field public static final movieDirectorsHeader:I = 0x7f070163

.field public static final movieDownloadStatus:I = 0x7f070196

.field public static final movieFilmographyLayout:I = 0x7f07012d

.field public static final movieFlixScore:I = 0x7f070109

.field public static final movieMeta:I = 0x7f07010c

.field public static final moviePhotos:I = 0x7f070162

.field public static final moviePoster:I = 0x7f070106

.field public static final movieRelease:I = 0x7f07010d

.field public static final movieReviewAuthor:I = 0x7f07013e

.field public static final movieReviewComment:I = 0x7f07013f

.field public static final movieReviewLayout:I = 0x7f070139

.field public static final movieReviewPhoto:I = 0x7f07013a

.field public static final movieReviewRating:I = 0x7f07013d

.field public static final movieReviewRatingAuthor:I = 0x7f07013c

.field public static final movieReviewTitle:I = 0x7f07013b

.field public static final movieRtScore:I = 0x7f07010a

.field public static final movieScore:I = 0x7f070132

.field public static final movieScoreFlixLayout:I = 0x7f070192

.field public static final movieScoreLayout:I = 0x7f0701fc

.field public static final movieScoreRtLayout:I = 0x7f070194

.field public static final movieThumbnail:I = 0x7f07012e

.field public static final movieTitle:I = 0x7f070108

.field public static final movieWantToSeeLayout:I = 0x7f070140

.field public static final movie_collection_item_downloadStatus:I = 0x7f070118

.field public static final movie_collection_item_poster:I = 0x7f070113

.field public static final movie_collection_item_progressBar:I = 0x7f070114

.field public static final movie_collection_item_ratingImage:I = 0x7f070116

.field public static final movie_collection_item_rentalExpiration:I = 0x7f070117

.field public static final movie_collection_item_title:I = 0x7f070115

.field public static final movie_friends_ratings:I = 0x7f070173

.field public static final movie_friends_ratings_header:I = 0x7f070172

.field public static final movie_friends_wts:I = 0x7f07016d

.field public static final movie_friends_wts_header:I = 0x7f07016c

.field public static final movie_gallery_item_poster:I = 0x7f070135

.field public static final movie_gallery_item_title:I = 0x7f070136

.field public static final movie_load_more_friends_ratings:I = 0x7f070175

.field public static final movie_load_more_friends_ratings_title:I = 0x7f070176

.field public static final movie_load_more_friends_ratings_total:I = 0x7f070177

.field public static final movie_load_more_friends_wts:I = 0x7f07016f

.field public static final movie_load_more_friends_wts_title:I = 0x7f070170

.field public static final movie_load_more_friends_wts_total:I = 0x7f070171

.field public static final movie_more_friends_ratings:I = 0x7f070174

.field public static final movie_more_friends_wts:I = 0x7f07016e

.field public static final movie_photos:I = 0x7f070138

.field public static final movie_title:I = 0x7f070119

.field public static final movie_year:I = 0x7f07011a

.field public static final moviedetails_detailslist:I = 0x7f070146

.field public static final moviedetails_dvdreleasedate:I = 0x7f070161

.field public static final moviedetails_genrevar:I = 0x7f07015e

.field public static final moviedetails_moviename:I = 0x7f070190

.field public static final moviedetails_moviename2:I = 0x7f070147

.field public static final moviedetails_mpaa:I = 0x7f07015d

.field public static final moviedetails_poster:I = 0x7f070145

.field public static final moviedetails_releasedate:I = 0x7f070160

.field public static final moviedetails_runningtimevar:I = 0x7f07015f

.field public static final moviedetails_synopsis:I = 0x7f07015c

.field public static final movielist_details_ll:I = 0x7f070130

.field public static final movielist_grip:I = 0x7f0701fd

.field public static final movielist_playbutton:I = 0x7f07012f

.field public static final movielist_rl:I = 0x7f070105

.field public static final moviepage_nav_layout:I = 0x7f0701ed

.field public static final mpaa_runtime:I = 0x7f070125

.field public static final msk_prompt_layout:I = 0x7f070197

.field public static final myFriends:I = 0x7f0701d4

.field public static final myReviewsTitle:I = 0x7f0701bb

.field public static final my_collection_offlineAlert:I = 0x7f07010e

.field public static final mymovies_collection_layout:I = 0x7f0701aa

.field public static final mymovies_flixster_description:I = 0x7f0701a2

.field public static final mymovies_frame:I = 0x7f0700a0

.field public static final mymovies_friend_activity_none:I = 0x7f0701ba

.field public static final mymovies_friends:I = 0x7f0701b7

.field public static final mymovies_friends_activity:I = 0x7f0701b9

.field public static final mymovies_friends_activity_header:I = 0x7f0701b8

.field public static final mymovies_friends_header:I = 0x7f0701b6

.field public static final mymovies_friends_scrollview:I = 0x7f0701b5

.field public static final mymovies_gallery_netflix_dvd:I = 0x7f0700be

.field public static final mymovies_gallery_netflix_instant:I = 0x7f0700c1

.field public static final mymovies_gridview_rated:I = 0x7f0701b4

.field public static final mymovies_gridview_wts:I = 0x7f0701b0

.field public static final mymovies_header:I = 0x7f07009e

.field public static final mymovies_header_username:I = 0x7f0700a2

.field public static final mymovies_image:I = 0x7f07009f

.field public static final mymovies_image_login:I = 0x7f0701d8

.field public static final mymovies_nav_collected:I = 0x7f0701e1

.field public static final mymovies_nav_friends:I = 0x7f0701e7

.field public static final mymovies_nav_layout:I = 0x7f0701e0

.field public static final mymovies_nav_ratings:I = 0x7f0701e5

.field public static final mymovies_nav_wts:I = 0x7f0701e3

.field public static final mymovies_netflix_description:I = 0x7f0700c3

.field public static final mymovies_offlineAlert:I = 0x7f07019b

.field public static final mymovies_panel_fb:I = 0x7f0701d5

.field public static final mymovies_panel_friendratings:I = 0x7f0701d3

.field public static final mymovies_panel_mycollection:I = 0x7f0701ce

.field public static final mymovies_panel_netflix:I = 0x7f0701d0

.field public static final mymovies_panel_rated:I = 0x7f0701d2

.field public static final mymovies_panel_wts:I = 0x7f0701d1

.field public static final mymovies_rated_layout:I = 0x7f0701b1

.field public static final mymovies_rentals_layout:I = 0x7f0701a8

.field public static final mymovies_rewards:I = 0x7f07019d

.field public static final mymovies_rewards_earned:I = 0x7f07019f

.field public static final mymovies_rewards_more:I = 0x7f0701a0

.field public static final mymovies_scrollview:I = 0x7f0700b7

.field public static final mymovies_sub_accounts:I = 0x7f0701a1

.field public static final mymovies_sub_accounts_netflix:I = 0x7f0700c2

.field public static final mymovies_sub_collection:I = 0x7f0701a9

.field public static final mymovies_sub_movielists:I = 0x7f0701cd

.field public static final mymovies_sub_netflix_dvd:I = 0x7f0700bc

.field public static final mymovies_sub_netflix_instant:I = 0x7f0700bf

.field public static final mymovies_sub_rated:I = 0x7f0701b2

.field public static final mymovies_sub_rentals:I = 0x7f0701a7

.field public static final mymovies_sub_startCollection:I = 0x7f07019c

.field public static final mymovies_sub_wts:I = 0x7f0701ae

.field public static final mymovies_subnav:I = 0x7f07019a

.field public static final mymovies_wts_layout:I = 0x7f0701ad

.field public static final navbar_theaterlist_mapviewcontainer:I = 0x7f070261

.field public static final navbar_theaterlist_mapviewcontrols:I = 0x7f070262

.field public static final netflix_contextmenu:I = 0x7f0701f8

.field public static final netflix_debug:I = 0x7f0701f0

.field public static final netflix_loginaccount_button:I = 0x7f0701f4

.field public static final netflix_logo:I = 0x7f0701ff

.field public static final netflix_logo_layout:I = 0x7f0701fe

.field public static final netflix_morelayout:I = 0x7f070200

.field public static final netflix_moretext:I = 0x7f070201

.field public static final netflix_nav_layout:I = 0x7f0701f7

.field public static final netflix_queue_layout:I = 0x7f0701f6

.field public static final netflix_queueitem_checkbox:I = 0x7f0701fb

.field public static final netflix_removebutton:I = 0x7f0701f9

.field public static final netflix_select_text:I = 0x7f0701f3

.field public static final netflix_selection_layout:I = 0x7f0701f2

.field public static final netflix_startaccount_button:I = 0x7f0701f5

.field public static final newsItemDescription:I = 0x7f070206

.field public static final newsItemTitle:I = 0x7f070205

.field public static final news_item_listview:I = 0x7f070204

.field public static final news_item_rl:I = 0x7f070202

.field public static final news_thumbnail:I = 0x7f070203

.field public static final nextReviewButton:I = 0x7f0701c7

.field public static final normal:I = 0x7f070001

.field public static final offline_alert:I = 0x7f070074

.field public static final passwordEditText:I = 0x7f0700e9

.field public static final passwordField:I = 0x7f070088

.field public static final photo_gallery_gridview:I = 0x7f070208

.field public static final photos_filter_title:I = 0x7f0702ce

.field public static final pick_your_movie:I = 0x7f070198

.field public static final postFacebookLabel:I = 0x7f070217

.field public static final post_feed:I = 0x7f070215

.field public static final poster:I = 0x7f070122

.field public static final poster_reflection:I = 0x7f070123

.field public static final previousReviewButton:I = 0x7f0701c9

.field public static final profileCollections:I = 0x7f0700a6

.field public static final profileFrame:I = 0x7f07020b

.field public static final profileFriends:I = 0x7f0700a5

.field public static final profileHeader:I = 0x7f0702d1

.field public static final profileLayout:I = 0x7f070209

.field public static final profilePhoto:I = 0x7f07020a

.field public static final profileRatings:I = 0x7f0700a4

.field public static final profileTitle:I = 0x7f07020c

.field public static final profileWantToSee:I = 0x7f0700a3

.field public static final progress_circular:I = 0x7f0702ca

.field public static final progress_horizontal:I = 0x7f0702cc

.field public static final promo_header:I = 0x7f07004f

.field public static final promo_image:I = 0x7f070050

.field public static final promo_text:I = 0x7f070051

.field public static final quick_rate:I = 0x7f070211

.field public static final quickrate_list:I = 0x7f07020e

.field public static final quickrate_progress:I = 0x7f07020d

.field public static final quickrate_ratingBar:I = 0x7f07020f

.field public static final quickrate_wtsButton:I = 0x7f070210

.field public static final rate_bar:I = 0x7f070214

.field public static final rate_connect_view:I = 0x7f070052

.field public static final rate_ni:I = 0x7f070213

.field public static final rate_promo_leadtext:I = 0x7f070053

.field public static final rate_promo_screen:I = 0x7f070054

.field public static final rate_promo_text1:I = 0x7f070055

.field public static final rate_promo_text2:I = 0x7f070056

.field public static final rate_promo_text3:I = 0x7f070057

.field public static final rate_review:I = 0x7f070218

.field public static final rate_wts:I = 0x7f070212

.field public static final ratedCount:I = 0x7f070097

.field public static final ratedMovies:I = 0x7f0702d8

.field public static final ratedTitle:I = 0x7f0702d7

.field public static final ratingsCount:I = 0x7f0701e6

.field public static final registerFlixster:I = 0x7f0701a5

.field public static final rev_index:I = 0x7f07021b

.field public static final rev_next_button:I = 0x7f07021a

.field public static final rev_prev_button:I = 0x7f07021c

.field public static final review:I = 0x7f070062

.field public static final reviewComment:I = 0x7f0701c5

.field public static final reviewFlipper:I = 0x7f0701cc

.field public static final reviewHeader:I = 0x7f0701c6

.field public static final reviewIndex:I = 0x7f0701c8

.field public static final reviewItemBody:I = 0x7f0701c3

.field public static final reviewMovie:I = 0x7f0701bd

.field public static final reviewMovieActors:I = 0x7f0701c0

.field public static final reviewMovieGross:I = 0x7f0701c2

.field public static final reviewMovieMeta:I = 0x7f0701c1

.field public static final reviewMovieTitle:I = 0x7f0701bf

.field public static final reviewPoster:I = 0x7f0701be

.field public static final reviewRating:I = 0x7f0701c4

.field public static final reviewUser:I = 0x7f0702dc

.field public static final reviewUserImage:I = 0x7f0701ca

.field public static final reviewUserName:I = 0x7f0701cb

.field public static final review_actors:I = 0x7f07021f

.field public static final review_critic_icon:I = 0x7f070228

.field public static final review_gross:I = 0x7f070221

.field public static final review_header:I = 0x7f070219

.field public static final review_item_body:I = 0x7f07022a

.field public static final review_item_byline:I = 0x7f070226

.field public static final review_item_header:I = 0x7f070224

.field public static final review_item_mug:I = 0x7f070225

.field public static final review_item_source:I = 0x7f070227

.field public static final review_meta:I = 0x7f070220

.field public static final review_more:I = 0x7f07022c

.field public static final review_poster:I = 0x7f07021d

.field public static final review_ratingimage:I = 0x7f070229

.field public static final review_reviewtext:I = 0x7f07022b

.field public static final review_title:I = 0x7f07021e

.field public static final review_vf:I = 0x7f070222

.field public static final reviewflip_layout:I = 0x7f0701bc

.field public static final reward_description:I = 0x7f07022d

.field public static final reward_image:I = 0x7f07019e

.field public static final reward_incentive:I = 0x7f07022e

.field public static final rewardspage_sub_available:I = 0x7f07022f

.field public static final rewardspage_sub_completed:I = 0x7f070231

.field public static final right_icon:I = 0x7f0702cb

.field public static final rootLayout:I = 0x7f070134

.field public static final scroll_layout:I = 0x7f070073

.field public static final scrollview:I = 0x7f07011b

.field public static final scrollview_layout:I = 0x7f07011d

.field public static final search_button:I = 0x7f070101

.field public static final search_edittext:I = 0x7f070027

.field public static final search_fragment:I = 0x7f070234

.field public static final search_query:I = 0x7f070102

.field public static final settingsFlixsterConnect:I = 0x7f0701dd

.field public static final settingsFlixsterIcon:I = 0x7f0701db

.field public static final settingsFlixsterLogin:I = 0x7f0701da

.field public static final settingsFlixsterStatus:I = 0x7f0701de

.field public static final settingsFlixsterText:I = 0x7f0701dc

.field public static final settingsNetflixConnect:I = 0x7f0700c8

.field public static final settingsNetflixIcon:I = 0x7f0700c6

.field public static final settingsNetflixLogin:I = 0x7f0701df

.field public static final settingsNetflixLogout:I = 0x7f0700c5

.field public static final settingsNetflixStatus:I = 0x7f0700c9

.field public static final settingsNetflixText:I = 0x7f0700c7

.field public static final settings_admin:I = 0x7f070237

.field public static final settings_cache_rl:I = 0x7f070244

.field public static final settings_cachepolicy_subtext:I = 0x7f070245

.field public static final settings_captions:I = 0x7f070235

.field public static final settings_captions_rl:I = 0x7f070246

.field public static final settings_captions_subtext:I = 0x7f070247

.field public static final settings_distance:I = 0x7f07023f

.field public static final settings_distance_label:I = 0x7f070241

.field public static final settings_distance_text:I = 0x7f070240

.field public static final settings_email_icon:I = 0x7f0701d6

.field public static final settings_facebook_status:I = 0x7f0701d9

.field public static final settings_footertext:I = 0x7f070249

.field public static final settings_location_rl:I = 0x7f07023a

.field public static final settings_location_subtext:I = 0x7f07023b

.field public static final settings_maintext:I = 0x7f0701d7

.field public static final settings_ratings_rl:I = 0x7f070242

.field public static final settings_ratings_subtext:I = 0x7f070243

.field public static final settings_sub_accounts:I = 0x7f070238

.field public static final settings_sub_preferences:I = 0x7f070239

.field public static final settings_subtext:I = 0x7f070236

.field public static final showCustom:I = 0x7f070008

.field public static final showHome:I = 0x7f070005

.field public static final showTitle:I = 0x7f070007

.field public static final shown_options:I = 0x7f0700d9

.field public static final showtimes_address:I = 0x7f07024d

.field public static final showtimes_buybutton:I = 0x7f070250

.field public static final showtimes_details_layout:I = 0x7f070107

.field public static final showtimes_movietitle:I = 0x7f07024f

.field public static final showtimes_showtimespanel:I = 0x7f07024e

.field public static final showtimes_star:I = 0x7f07024c

.field public static final showtimes_theatername:I = 0x7f07024b

.field public static final showtimes_ticketicon:I = 0x7f07024a

.field public static final showtimes_timelist:I = 0x7f070251

.field public static final splash:I = 0x7f070252

.field public static final subheader:I = 0x7f07006e

.field public static final subheader1:I = 0x7f0700e6

.field public static final subheader2:I = 0x7f0700ee

.field public static final subnavbar_button1:I = 0x7f070258

.field public static final subnavbar_button2:I = 0x7f070259

.field public static final subnavbar_button3:I = 0x7f07025a

.field public static final subnavbar_button4:I = 0x7f07025b

.field public static final subtitle:I = 0x7f0702c9

.field public static final surface_view:I = 0x7f0702dd

.field public static final synopsis:I = 0x7f070070

.field public static final synopsis_header:I = 0x7f07015a

.field public static final tabMode:I = 0x7f070003

.field public static final tab_divider:I = 0x7f07025c

.field public static final tab_image:I = 0x7f07025d

.field public static final tab_indicator:I = 0x7f07025f

.field public static final tab_text:I = 0x7f07025e

.field public static final textView1:I = 0x7f0701cf

.field public static final textView2:I = 0x7f0700e7

.field public static final textView3:I = 0x7f0700ea

.field public static final theaterMapPage:I = 0x7f070137

.field public static final theaterSettings:I = 0x7f070264

.field public static final theaterSettingsButton:I = 0x7f070268

.field public static final theaterSettingsLayout:I = 0x7f070266

.field public static final theaterSettingsPage:I = 0x7f070263

.field public static final theaterSettingsText:I = 0x7f070267

.field public static final theaterSettingsTitle:I = 0x7f070265

.field public static final theaterdetails_favoritestar:I = 0x7f0700ff

.field public static final theaterdetails_name:I = 0x7f0700fe

.field public static final theaterlist_nav_layout:I = 0x7f0701e9

.field public static final theatersByName:I = 0x7f0701eb

.field public static final theatersMap:I = 0x7f0701ec

.field public static final theatersNearby:I = 0x7f0701ea

.field public static final theatersShown:I = 0x7f070260

.field public static final throbber:I = 0x7f070039

.field public static final throbberLayout:I = 0x7f0700fb

.field public static final throbber_collection:I = 0x7f0701a6

.field public static final throbber_netflix_dvd:I = 0x7f0700bd

.field public static final throbber_netflix_instant:I = 0x7f0700c0

.field public static final throbber_rated:I = 0x7f0701b3

.field public static final throbber_wts:I = 0x7f0701af

.field public static final tickeconfirm_confnum_subtext:I = 0x7f0702ab

.field public static final tickeconfirm_instructions_subtext:I = 0x7f0702ad

.field public static final tickeconfirm_ticketinfo_subtext:I = 0x7f0702b0

.field public static final ticket_cardnumber:I = 0x7f070292

.field public static final ticket_cardnumber_error:I = 0x7f07029a

.field public static final ticket_cardnumber_label:I = 0x7f070291

.field public static final ticket_cardnumber_layout:I = 0x7f070290

.field public static final ticket_charge_label:I = 0x7f070285

.field public static final ticket_code:I = 0x7f070297

.field public static final ticket_code_label:I = 0x7f070296

.field public static final ticket_codenumber_error:I = 0x7f07029c

.field public static final ticket_completepurchase_button:I = 0x7f0702c0

.field public static final ticket_continue_button:I = 0x7f0702a3

.field public static final ticket_count_error:I = 0x7f070288

.field public static final ticket_email:I = 0x7f0702a0

.field public static final ticket_email_error:I = 0x7f0702a1

.field public static final ticket_email_header:I = 0x7f07029e

.field public static final ticket_email_label:I = 0x7f07029f

.field public static final ticket_email_message:I = 0x7f0702a2

.field public static final ticket_errormessage:I = 0x7f07027c

.field public static final ticket_expdate_error:I = 0x7f07029b

.field public static final ticket_expdate_label:I = 0x7f070293

.field public static final ticket_expmonth:I = 0x7f070294

.field public static final ticket_expyear:I = 0x7f070295

.field public static final ticket_payment_cardlabel:I = 0x7f07028e

.field public static final ticket_payment_cardspinner:I = 0x7f07028f

.field public static final ticket_payment_header:I = 0x7f07028d

.field public static final ticket_postal:I = 0x7f070299

.field public static final ticket_postal_error:I = 0x7f07029d

.field public static final ticket_postal_label:I = 0x7f070298

.field public static final ticket_surcharge_label:I = 0x7f07028c

.field public static final ticket_total_label:I = 0x7f070289

.field public static final ticketbar_button_dec:I = 0x7f0702c4

.field public static final ticketbar_button_inc:I = 0x7f0702c2

.field public static final ticketbar_counter:I = 0x7f0702c3

.field public static final ticketbar_label:I = 0x7f0702c7

.field public static final ticketbar_layout:I = 0x7f070284

.field public static final ticketbar_price:I = 0x7f0702c6

.field public static final ticketbar_subtotal:I = 0x7f0702c5

.field public static final ticketconfirm_congrats:I = 0x7f0702a9

.field public static final ticketconfirm_congrats_layout:I = 0x7f0702a7

.field public static final ticketconfirm_congrats_message:I = 0x7f0702aa

.field public static final ticketconfirm_email:I = 0x7f0702bf

.field public static final ticketconfirm_error:I = 0x7f07027e

.field public static final ticketconfirm_error_message:I = 0x7f07027f

.field public static final ticketconfirm_headertext:I = 0x7f0702a5

.field public static final ticketconfirm_icon:I = 0x7f0702a8

.field public static final ticketconfirm_icon_error:I = 0x7f07027d

.field public static final ticketconfirm_row:I = 0x7f0702b1

.field public static final ticketconfirm_theatername:I = 0x7f0702b5

.field public static final ticketconfirm_title:I = 0x7f0702b3

.field public static final ticketinfo_actors:I = 0x7f07026c

.field public static final ticketinfo_cardnumber:I = 0x7f0702bc

.field public static final ticketinfo_details_layout:I = 0x7f07026a

.field public static final ticketinfo_fee:I = 0x7f0702b9

.field public static final ticketinfo_label_card:I = 0x7f0702bb

.field public static final ticketinfo_label_movie:I = 0x7f0702b2

.field public static final ticketinfo_label_theater:I = 0x7f0702b4

.field public static final ticketinfo_meta:I = 0x7f07026d

.field public static final ticketinfo_poster:I = 0x7f070269

.field public static final ticketinfo_showelapsed:I = 0x7f070273

.field public static final ticketinfo_showelapsed_main:I = 0x7f070274

.field public static final ticketinfo_showelapsed_sub:I = 0x7f070275

.field public static final ticketinfo_showtime:I = 0x7f0702b6

.field public static final ticketinfo_showtime_footnote:I = 0x7f0702b7

.field public static final ticketinfo_showtimelist:I = 0x7f070277

.field public static final ticketinfo_showtimepast:I = 0x7f070276

.field public static final ticketinfo_showtimeslayout:I = 0x7f070272

.field public static final ticketinfo_theateraddress:I = 0x7f070270

.field public static final ticketinfo_theateritem:I = 0x7f07026e

.field public static final ticketinfo_theaterlist_header:I = 0x7f070271

.field public static final ticketinfo_theatername:I = 0x7f07026f

.field public static final ticketinfo_tickets:I = 0x7f0702b8

.field public static final ticketinfo_titletext:I = 0x7f07026b

.field public static final ticketinfo_total:I = 0x7f0702ba

.field public static final ticketpayment_expdate:I = 0x7f0702be

.field public static final ticketpayment_label_date:I = 0x7f0702bd

.field public static final ticketreceipt_layout:I = 0x7f0702a6

.field public static final tickets_confirm_layout:I = 0x7f0702a4

.field public static final tickets_confirmation_number:I = 0x7f0702ac

.field public static final tickets_convenience_total:I = 0x7f070286

.field public static final tickets_label_adult:I = 0x7f070287

.field public static final tickets_mtcom_logo:I = 0x7f070279

.field public static final tickets_mtcom_message:I = 0x7f070278

.field public static final tickets_price_adult:I = 0x7f07028b

.field public static final tickets_select_layout:I = 0x7f07027b

.field public static final tickets_total:I = 0x7f07028a

.field public static final ticketsconfirm_instructions_text:I = 0x7f0702ae

.field public static final ticketsconfirm_message:I = 0x7f0702af

.field public static final ticketselect_details_layout:I = 0x7f070281

.field public static final ticketselect_poster:I = 0x7f070280

.field public static final ticketselect_scrollview:I = 0x7f07027a

.field public static final ticketselect_showtime:I = 0x7f070282

.field public static final ticketselect_showtime_footnote:I = 0x7f070283

.field public static final title:I = 0x7f07006f

.field public static final title_container:I = 0x7f0700b2

.field public static final tomatometer:I = 0x7f070126

.field public static final tomatometer_icon:I = 0x7f070127

.field public static final tomatometer_score:I = 0x7f070128

.field public static final topActors:I = 0x7f0700cb

.field public static final topPhotos:I = 0x7f0700ca

.field public static final top_list_divider:I = 0x7f070092

.field public static final top_photos:I = 0x7f0702cf

.field public static final top_photos_layout:I = 0x7f0702cd

.field public static final topactors_nav_random:I = 0x7f0701ef

.field public static final topactors_nav_topthisweek:I = 0x7f0701ee

.field public static final trailer:I = 0x7f070120

.field public static final trailer_backgroundad:I = 0x7f0702de

.field public static final unfulfillable_divider:I = 0x7f070111

.field public static final unfulfillable_learn_more:I = 0x7f0702d0

.field public static final unfulfillable_panel:I = 0x7f070110

.field public static final up:I = 0x7f0700d8

.field public static final useLogo:I = 0x7f070004

.field public static final userGreeting:I = 0x7f070255

.field public static final userHeader:I = 0x7f070253

.field public static final userLogout:I = 0x7f070257

.field public static final userPicture:I = 0x7f070254

.field public static final userRefresh:I = 0x7f070256

.field public static final usernameField:I = 0x7f070087

.field public static final uv_footer:I = 0x7f0701ac

.field public static final uv_footer_divider:I = 0x7f0701ab

.field public static final viewActorPhotos:I = 0x7f070033

.field public static final viewPhotos:I = 0x7f070154

.field public static final wantToSeeMovies:I = 0x7f0702d3

.field public static final wantToSeeTitle:I = 0x7f0702d2

.field public static final watch:I = 0x7f070121

.field public static final watchnow_panel:I = 0x7f070071

.field public static final web_view:I = 0x7f0701f1

.field public static final webview:I = 0x7f0702e7

.field public static final widgetArrowsLayout:I = 0x7f0702f5

.field public static final widgetAudienceScore:I = 0x7f0702f3

.field public static final widgetAudienceScoreIcon:I = 0x7f0702f2

.field public static final widgetDownButton:I = 0x7f0702f7

.field public static final widgetFlixsterLogoButton:I = 0x7f0702e9

.field public static final widgetLoading:I = 0x7f0702ee

.field public static final widgetLogoLayout:I = 0x7f0702e8

.field public static final widgetMovieLayout:I = 0x7f0702ed

.field public static final widgetMovieTitle:I = 0x7f0702ef

.field public static final widgetPosterButton:I = 0x7f0702ec

.field public static final widgetPosterLayout:I = 0x7f0702eb

.field public static final widgetSearchButton:I = 0x7f0702ea

.field public static final widgetSecondaryText:I = 0x7f0702f4

.field public static final widgetTomatometer:I = 0x7f0702f1

.field public static final widgetTomatometerIcon:I = 0x7f0702f0

.field public static final widgetUpButton:I = 0x7f0702f6

.field public static final wrap_content:I = 0x7f070000

.field public static final wtsCount:I = 0x7f0701e4

.field public static final zip_edit:I = 0x7f0702f9

.field public static final zip_view:I = 0x7f0702f8

.field public static final zoomView:I = 0x7f0700e5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
