.class public final Lcom/actionbarsherlock/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/actionbarsherlock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abs__action_bar_home:I = 0x7f030000

.field public static final abs__action_bar_tab:I = 0x7f030001

.field public static final abs__action_bar_tab_bar_view:I = 0x7f030002

.field public static final abs__action_bar_title_item:I = 0x7f030003

.field public static final abs__action_menu_item_layout:I = 0x7f030004

.field public static final abs__action_menu_layout:I = 0x7f030005

.field public static final abs__action_mode_bar:I = 0x7f030006

.field public static final abs__action_mode_close_item:I = 0x7f030007

.field public static final abs__activity_chooser_view:I = 0x7f030008

.field public static final abs__activity_chooser_view_list_item:I = 0x7f030009

.field public static final abs__dialog_title_holo:I = 0x7f03000a

.field public static final abs__list_menu_item_checkbox:I = 0x7f03000b

.field public static final abs__list_menu_item_icon:I = 0x7f03000c

.field public static final abs__list_menu_item_layout:I = 0x7f03000d

.field public static final abs__list_menu_item_radio:I = 0x7f03000e

.field public static final abs__popup_menu_item_layout:I = 0x7f03000f

.field public static final abs__screen_action_bar:I = 0x7f030010

.field public static final abs__screen_action_bar_overlay:I = 0x7f030011

.field public static final abs__screen_simple:I = 0x7f030012

.field public static final abs__screen_simple_overlay_action_mode:I = 0x7f030013

.field public static final action_bar:I = 0x7f030014

.field public static final actionbar_search:I = 0x7f030015

.field public static final actor_biography_page:I = 0x7f030016

.field public static final actor_item:I = 0x7f030017

.field public static final actor_page:I = 0x7f030018

.field public static final actor_photos_page:I = 0x7f030019

.field public static final admenu_page:I = 0x7f03001a

.field public static final admin_page:I = 0x7f03001b

.field public static final blank_gray_page:I = 0x7f03001c

.field public static final collection_promo_fragment:I = 0x7f03001d

.field public static final connect_rate_view:I = 0x7f03001e

.field public static final contact_list_item:I = 0x7f03001f

.field public static final critic_footer:I = 0x7f030020

.field public static final critic_review:I = 0x7f030021

.field public static final criticreview_item:I = 0x7f030022

.field public static final dialog_doubleclick_test:I = 0x7f030023

.field public static final download_panel:I = 0x7f030024

.field public static final episode_view:I = 0x7f030025

.field public static final episodes_page:I = 0x7f030026

.field public static final facebook_auth:I = 0x7f030027

.field public static final facebook_invite_page:I = 0x7f030028

.field public static final facebook_login_upsell:I = 0x7f030029

.field public static final feed_permission:I = 0x7f03002a

.field public static final feedback_page:I = 0x7f03002b

.field public static final flixster:I = 0x7f03002c

.field public static final flixster_login:I = 0x7f03002d

.field public static final friend_activity:I = 0x7f03002e

.field public static final friend_item:I = 0x7f03002f

.field public static final friend_select_page:I = 0x7f030030

.field public static final friends_rated_page:I = 0x7f030031

.field public static final gallery:I = 0x7f030032

.field public static final gtv_main_activity:I = 0x7f030033

.field public static final headline:I = 0x7f030034

.field public static final homepage:I = 0x7f030035

.field public static final html_ad_page:I = 0x7f030036

.field public static final interstitial_page:I = 0x7f030037

.field public static final left_nav:I = 0x7f030038

.field public static final leftnav_bar_home:I = 0x7f030039

.field public static final leftnav_bar_option:I = 0x7f03003a

.field public static final leftnav_bar_options:I = 0x7f03003b

.field public static final leftnav_bar_spinner:I = 0x7f03003c

.field public static final leftnav_bar_tab:I = 0x7f03003d

.field public static final leftnav_bar_tabs:I = 0x7f03003e

.field public static final load_more:I = 0x7f03003f

.field public static final loading_layout:I = 0x7f030040

.field public static final location_map_page:I = 0x7f030041

.field public static final location_page:I = 0x7f030042

.field public static final login_dialog_fragment:I = 0x7f030043

.field public static final lvi_datepanel:I = 0x7f030044

.field public static final lvi_fragment:I = 0x7f030045

.field public static final lvi_iconpanel:I = 0x7f030046

.field public static final lvi_locationpanel:I = 0x7f030047

.field public static final lvi_messagepanel:I = 0x7f030048

.field public static final lvi_page:I = 0x7f030049

.field public static final lvi_pageheader_theater:I = 0x7f03004a

.field public static final lvi_searchpanel:I = 0x7f03004b

.field public static final main_phone:I = 0x7f03004c

.field public static final movie_actor_item:I = 0x7f03004d

.field public static final movie_cap:I = 0x7f03004e

.field public static final movie_collection:I = 0x7f03004f

.field public static final movie_collection_item:I = 0x7f030050

.field public static final movie_detail_fragment:I = 0x7f030051

.field public static final movie_detail_header:I = 0x7f030052

.field public static final movie_filmography_item:I = 0x7f030053

.field public static final movie_gallery:I = 0x7f030054

.field public static final movie_gallery_fragment:I = 0x7f030055

.field public static final movie_gallery_item:I = 0x7f030056

.field public static final movie_map_page:I = 0x7f030057

.field public static final movie_photos_page:I = 0x7f030058

.field public static final movie_review_item:I = 0x7f030059

.field public static final movie_wts_item:I = 0x7f03005a

.field public static final moviedetails_page:I = 0x7f03005b

.field public static final movielist_item:I = 0x7f03005c

.field public static final msk_tooltip_dialog:I = 0x7f03005d

.field public static final my_movies_page:I = 0x7f03005e

.field public static final my_rated_page:I = 0x7f03005f

.field public static final my_review_item:I = 0x7f030060

.field public static final my_review_page:I = 0x7f030061

.field public static final mymovies_page:I = 0x7f030062

.field public static final navbar_mymovies:I = 0x7f030063

.field public static final navbar_theaterlist:I = 0x7f030064

.field public static final navbar_topactors:I = 0x7f030065

.field public static final netflix_auth:I = 0x7f030066

.field public static final netflix_edit:I = 0x7f030067

.field public static final netflixlist_item:I = 0x7f030068

.field public static final netflixlist_logo:I = 0x7f030069

.field public static final netflixlist_more:I = 0x7f03006a

.field public static final news_listitem:I = 0x7f03006b

.field public static final page_header_item:I = 0x7f03006c

.field public static final photo_gallery_page:I = 0x7f03006d

.field public static final profile_header:I = 0x7f03006e

.field public static final quickrate_page:I = 0x7f03006f

.field public static final quickrate_view:I = 0x7f030070

.field public static final ratemovie_page:I = 0x7f030071

.field public static final review_page:I = 0x7f030072

.field public static final reviewpage_flipview_item:I = 0x7f030073

.field public static final reward_view:I = 0x7f030074

.field public static final rewards_page:I = 0x7f030075

.field public static final search_fragment:I = 0x7f030076

.field public static final search_fragment_activity:I = 0x7f030077

.field public static final settings_fragment:I = 0x7f030078

.field public static final settings_item:I = 0x7f030079

.field public static final settings_page:I = 0x7f03007a

.field public static final sherlock_spinner_dropdown_item:I = 0x7f03007b

.field public static final sherlock_spinner_item:I = 0x7f03007c

.field public static final showtimes_item:I = 0x7f03007d

.field public static final showtimes_showtimesitem:I = 0x7f03007e

.field public static final splash:I = 0x7f03007f

.field public static final store_fragment:I = 0x7f030080

.field public static final subnavbar_ics:I = 0x7f030081

.field public static final tab_ics_layout:I = 0x7f030082

.field public static final tab_ics_layout_sans_divider:I = 0x7f030083

.field public static final theater_map_page:I = 0x7f030084

.field public static final theater_settings:I = 0x7f030085

.field public static final theaters_distance_bar:I = 0x7f030086

.field public static final ticket_info_page:I = 0x7f030087

.field public static final ticket_select_page:I = 0x7f030088

.field public static final ticketbar:I = 0x7f030089

.field public static final title_bar:I = 0x7f03008a

.field public static final top_photos_page:I = 0x7f03008b

.field public static final unfulfillable_page:I = 0x7f03008c

.field public static final user_profile_page:I = 0x7f03008d

.field public static final user_review:I = 0x7f03008e

.field public static final user_review_page:I = 0x7f03008f

.field public static final video_ad_player:I = 0x7f030090

.field public static final videoplayer:I = 0x7f030091

.field public static final videoview:I = 0x7f030092

.field public static final webview_page:I = 0x7f030093

.field public static final widget:I = 0x7f030094

.field public static final zip_dialog:I = 0x7f030095


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
