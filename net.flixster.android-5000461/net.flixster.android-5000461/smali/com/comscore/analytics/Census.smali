.class public Lcom/comscore/analytics/Census;
.super Ljava/lang/Object;
.source "Census.java"


# static fields
.field private static instance:Lcom/comscore/analytics/Census;


# instance fields
.field private customerID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    sput-object v0, Lcom/comscore/analytics/Census;->instance:Lcom/comscore/analytics/Census;

    .line 7
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public static getInstance()Lcom/comscore/analytics/Census;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/comscore/analytics/Census;->instance:Lcom/comscore/analytics/Census;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Lcom/comscore/analytics/Census;

    invoke-direct {v0}, Lcom/comscore/analytics/Census;-><init>()V

    sput-object v0, Lcom/comscore/analytics/Census;->instance:Lcom/comscore/analytics/Census;

    .line 22
    :cond_0
    sget-object v0, Lcom/comscore/analytics/Census;->instance:Lcom/comscore/analytics/Census;

    return-object v0
.end method


# virtual methods
.method public notifyStart(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "c"
    .parameter "CustomerId"
    .parameter "PublisherSecret"

    .prologue
    .line 26
    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v0

    .line 27
    .local v0, dax:Lcom/comscore/analytics/DAx;
    invoke-virtual {v0, p1}, Lcom/comscore/analytics/DAx;->setAppContext(Landroid/content/Context;)Lcom/comscore/analytics/DAx;

    .line 29
    if-eqz p2, :cond_0

    const-string v2, ""

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 30
    iput-object p2, p0, Lcom/comscore/analytics/Census;->customerID:Ljava/lang/String;

    .line 32
    :cond_0
    if-eqz p3, :cond_1

    const-string v2, ""

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 33
    invoke-virtual {v0, p3}, Lcom/comscore/analytics/DAx;->setSalt(Ljava/lang/String;)Lcom/comscore/analytics/DAx;

    .line 35
    :cond_1
    const-string v2, "http://b.scorecardresearch.com/p?"

    invoke-virtual {v0, v2}, Lcom/comscore/analytics/DAx;->setPixelURL(Ljava/lang/String;)Lcom/comscore/analytics/DAx;

    .line 36
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 37
    .local v1, labels:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "c2"

    iget-object v3, p0, Lcom/comscore/analytics/Census;->customerID:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    const-string v2, "name"

    const-string v3, "start"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v2, Lcom/comscore/applications/EventType;->Start:Lcom/comscore/applications/EventType;

    invoke-virtual {v0, v2, v1}, Lcom/comscore/analytics/DAx;->notify(Lcom/comscore/applications/EventType;Ljava/util/HashMap;)V

    .line 40
    return-void
.end method
