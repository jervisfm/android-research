.class public Lcom/comscore/analytics/DAx;
.super Lcom/comscore/metrics/Analytics;
.source "DAx.java"


# static fields
.field private static instance:Lcom/comscore/analytics/DAx;


# instance fields
.field private anonymous:Ljava/lang/Boolean;

.field private appContext:Landroid/content/Context;

.field private appName:Ljava/lang/String;

.field private dispatcher:Lcom/comscore/analytics/Dispatcher;

.field private genesis:J

.field private pixelURL:Ljava/lang/String;

.field private responders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/comscore/applications/Application;",
            ">;"
        }
    .end annotation
.end field

.field private salt:Ljava/lang/String;

.field private version:Ljava/lang/String;

.field private visitorID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/comscore/analytics/DAx;->instance:Lcom/comscore/analytics/DAx;

    .line 20
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/comscore/metrics/Analytics;-><init>()V

    .line 39
    invoke-static {}, Lcom/comscore/utils/Date;->getInstance()Lcom/comscore/utils/Date;

    move-result-object v0

    invoke-virtual {v0}, Lcom/comscore/utils/Date;->unixTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/comscore/analytics/DAx;->genesis:J

    .line 40
    new-instance v0, Lcom/comscore/analytics/Dispatcher;

    invoke-direct {v0}, Lcom/comscore/analytics/Dispatcher;-><init>()V

    iput-object v0, p0, Lcom/comscore/analytics/DAx;->dispatcher:Lcom/comscore/analytics/Dispatcher;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/comscore/analytics/DAx;->responders:Ljava/util/Map;

    .line 42
    return-void
.end method

.method public static getInstance()Lcom/comscore/analytics/DAx;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/comscore/analytics/DAx;->instance:Lcom/comscore/analytics/DAx;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/comscore/analytics/DAx;

    invoke-direct {v0}, Lcom/comscore/analytics/DAx;-><init>()V

    sput-object v0, Lcom/comscore/analytics/DAx;->instance:Lcom/comscore/analytics/DAx;

    .line 49
    :cond_0
    sget-object v0, Lcom/comscore/analytics/DAx;->instance:Lcom/comscore/analytics/DAx;

    return-object v0
.end method

.method private md5(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .parameter "value"

    .prologue
    .line 198
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 200
    .local v1, defaultBytes:[B
    :try_start_0
    const-string v6, "MD5"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 201
    .local v0, algorithm:Ljava/security/MessageDigest;
    invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V

    .line 202
    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 203
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    .line 205
    .local v5, messageDigest:[B
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 206
    .local v3, hexString:Ljava/lang/StringBuffer;
    const/4 v4, 0x0

    .local v4, i:I
    :goto_0
    array-length v6, v5

    if-lt v4, v6, :cond_0

    .line 212
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v6, p1

    .line 219
    .end local v0           #algorithm:Ljava/security/MessageDigest;
    .end local v3           #hexString:Ljava/lang/StringBuffer;
    .end local v4           #i:I
    .end local v5           #messageDigest:[B
    :goto_1
    return-object v6

    .line 207
    .restart local v0       #algorithm:Ljava/security/MessageDigest;
    .restart local v3       #hexString:Ljava/lang/StringBuffer;
    .restart local v4       #i:I
    .restart local v5       #messageDigest:[B
    :cond_0
    aget-byte v6, v5, v4

    and-int/lit16 v6, v6, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    .line 208
    .local v2, hex:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 209
    const/16 v6, 0x30

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 210
    :cond_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 216
    .end local v0           #algorithm:Ljava/security/MessageDigest;
    .end local v2           #hex:Ljava/lang/String;
    .end local v3           #hexString:Ljava/lang/StringBuffer;
    .end local v4           #i:I
    .end local v5           #messageDigest:[B
    :catch_0
    move-exception v6

    .line 219
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private setVisitorId()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 89
    iget-object v3, p0, Lcom/comscore/analytics/DAx;->visitorID:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 90
    const/4 v1, 0x0

    .line 91
    .local v1, vid:Ljava/lang/String;
    const/4 v0, 0x0

    .line 92
    .local v0, udid:Ljava/lang/String;
    const-string v2, "vid"

    .line 95
    .local v2, vidKey:Ljava/lang/String;
    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x9

    if-lt v3, v4, :cond_0

    .line 96
    invoke-static {}, Lcom/comscore/utils/API9;->getSerial()Ljava/lang/String;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_0

    :try_start_0
    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "unknown"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 99
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v6, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "***"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 100
    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "000"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 101
    new-instance v3, Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/comscore/analytics/DAx;->getSalt()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/comscore/analytics/DAx;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "-s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 102
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/comscore/utils/Storage;->remove(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v3, v6, :cond_3

    .line 107
    iget-object v3, p0, Lcom/comscore/analytics/DAx;->appContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "android_id"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_2

    const-string v3, "9774d56d682e549c"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "unknown"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "android_id"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 109
    :cond_1
    const/4 v0, 0x0

    .line 111
    :cond_2
    if-eqz v0, :cond_3

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 112
    new-instance v3, Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/comscore/analytics/DAx;->getSalt()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/comscore/analytics/DAx;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "-a"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 113
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/comscore/utils/Storage;->remove(Ljava/lang/String;)V

    .line 117
    :cond_3
    if-eqz v0, :cond_4

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    if-nez v1, :cond_6

    .line 118
    :cond_4
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/comscore/utils/Storage;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 119
    if-eqz v1, :cond_5

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 120
    :cond_5
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 121
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/comscore/analytics/DAx;->getSalt()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/comscore/analytics/DAx;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/comscore/utils/Storage;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_6
    iput-object v1, p0, Lcom/comscore/analytics/DAx;->visitorID:Ljava/lang/String;

    .line 127
    .end local v0           #udid:Ljava/lang/String;
    .end local v1           #vid:Ljava/lang/String;
    .end local v2           #vidKey:Ljava/lang/String;
    :cond_7
    return-void

    .line 104
    .restart local v0       #udid:Ljava/lang/String;
    .restart local v1       #vid:Ljava/lang/String;
    .restart local v2       #vidKey:Ljava/lang/String;
    :catch_0
    move-exception v3

    goto/16 :goto_0
.end method


# virtual methods
.method public getAnonymous()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->anonymous:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->appContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 160
    :try_start_0
    new-instance v0, Lcom/comscore/exceptions/NullApplicationContextException;

    invoke-direct {v0}, Lcom/comscore/exceptions/NullApplicationContextException;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/comscore/exceptions/NullApplicationContextException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :catch_0
    move-exception v0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 189
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->appName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/comscore/analytics/DAx;->appName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->appContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 191
    iget-object v1, p0, Lcom/comscore/analytics/DAx;->appContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "app_name"

    const-string v3, "string"

    .line 192
    iget-object v4, p0, Lcom/comscore/analytics/DAx;->appContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 191
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 190
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/DAx;->setAppName(Ljava/lang/String;)V

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getGenesis()J
    .locals 2

    .prologue
    .line 230
    iget-wide v0, p0, Lcom/comscore/analytics/DAx;->genesis:J

    return-wide v0
.end method

.method public getPixelURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->pixelURL:Ljava/lang/String;

    return-object v0
.end method

.method public getSalt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->salt:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/comscore/analytics/DAx;->salt:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    :cond_0
    invoke-virtual {p0}, Lcom/comscore/analytics/DAx;->getAppName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/DAx;->setSalt(Ljava/lang/String;)Lcom/comscore/analytics/DAx;

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->salt:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    const-string v0, "1.1108.12"

    return-object v0
.end method

.method public getVisitorId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->visitorID:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 131
    invoke-direct {p0}, Lcom/comscore/analytics/DAx;->setVisitorId()V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->visitorID:Ljava/lang/String;

    return-object v0
.end method

.method public notify(Lcom/comscore/applications/EventType;Ljava/util/HashMap;)V
    .locals 3
    .parameter "type"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/comscore/applications/EventType;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 271
    .local p2, labels:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/comscore/analytics/DAx;->responders:Ljava/util/Map;

    const-string v2, "Applications"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/comscore/applications/Application;

    .line 272
    .local v0, app:Lcom/comscore/applications/Application;
    iget-object v1, v0, Lcom/comscore/applications/Application;->dispatcher:Lcom/comscore/applications/Dispatcher;

    invoke-virtual {v1, p1, p2}, Lcom/comscore/applications/Dispatcher;->notify(Lcom/comscore/applications/EventType;Ljava/util/HashMap;)V

    .line 273
    return-void
.end method

.method public notify(Lcom/comscore/metrics/EventType;Ljava/util/HashMap;)V
    .locals 1
    .parameter "type"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/comscore/metrics/EventType;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 262
    .local p2, labels:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->dispatcher:Lcom/comscore/analytics/Dispatcher;

    invoke-virtual {v0, p1, p2}, Lcom/comscore/analytics/Dispatcher;->notify(Lcom/comscore/metrics/EventType;Ljava/util/HashMap;)V

    .line 263
    return-void
.end method

.method public notify(Ljava/lang/String;)V
    .locals 4
    .parameter "pixelUrl"

    .prologue
    .line 236
    iget-object v1, p0, Lcom/comscore/analytics/DAx;->pixelURL:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/comscore/analytics/DAx;->setPixelURL(Ljava/lang/String;)Lcom/comscore/analytics/DAx;

    .line 237
    iget-object v1, p0, Lcom/comscore/analytics/DAx;->responders:Ljava/util/Map;

    const-string v2, "Applications"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/comscore/applications/Application;

    .line 238
    .local v0, app:Lcom/comscore/applications/Application;
    iget-object v1, v0, Lcom/comscore/applications/Application;->dispatcher:Lcom/comscore/applications/Dispatcher;

    sget-object v2, Lcom/comscore/applications/EventType;->View:Lcom/comscore/applications/EventType;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/comscore/applications/Dispatcher;->notify(Lcom/comscore/applications/EventType;Ljava/util/HashMap;)V

    .line 241
    return-void
.end method

.method public notify(Ljava/lang/String;Lcom/comscore/applications/EventType;Ljava/util/HashMap;)V
    .locals 0
    .parameter "pixelUrl"
    .parameter "type"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/comscore/applications/EventType;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 276
    .local p3, labels:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/comscore/analytics/DAx;->setPixelURL(Ljava/lang/String;)Lcom/comscore/analytics/DAx;

    .line 277
    invoke-virtual {p0, p2, p3}, Lcom/comscore/analytics/DAx;->notify(Lcom/comscore/applications/EventType;Ljava/util/HashMap;)V

    .line 278
    return-void
.end method

.method public notify(Ljava/lang/String;Lcom/comscore/metrics/EventType;Ljava/util/HashMap;)V
    .locals 1
    .parameter "pixelUrl"
    .parameter "type"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/comscore/metrics/EventType;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 266
    .local p3, labels:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/comscore/analytics/DAx;->setPixelURL(Ljava/lang/String;)Lcom/comscore/analytics/DAx;

    .line 267
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->dispatcher:Lcom/comscore/analytics/Dispatcher;

    invoke-virtual {v0, p2, p3}, Lcom/comscore/analytics/Dispatcher;->notify(Lcom/comscore/metrics/EventType;Ljava/util/HashMap;)V

    .line 268
    return-void
.end method

.method public notify(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 3
    .parameter "pixelUrl"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 254
    .local p2, labels:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/comscore/analytics/DAx;->setPixelURL(Ljava/lang/String;)Lcom/comscore/analytics/DAx;

    .line 255
    iget-object v1, p0, Lcom/comscore/analytics/DAx;->responders:Ljava/util/Map;

    const-string v2, "Applications"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/comscore/applications/Application;

    .line 256
    .local v0, app:Lcom/comscore/applications/Application;
    iget-object v1, v0, Lcom/comscore/applications/Application;->dispatcher:Lcom/comscore/applications/Dispatcher;

    sget-object v2, Lcom/comscore/applications/EventType;->View:Lcom/comscore/applications/EventType;

    invoke-virtual {v1, v2, p2}, Lcom/comscore/applications/Dispatcher;->notify(Lcom/comscore/applications/EventType;Ljava/util/HashMap;)V

    .line 259
    return-void
.end method

.method public notify(Ljava/util/HashMap;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    .local p1, labels:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v2

    const-string v3, "PixelURL"

    invoke-virtual {v2, v3}, Lcom/comscore/utils/Storage;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 245
    .local v1, purl:Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 251
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v2, p0, Lcom/comscore/analytics/DAx;->responders:Ljava/util/Map;

    const-string v3, "Applications"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/comscore/applications/Application;

    .line 248
    .local v0, app:Lcom/comscore/applications/Application;
    iget-object v2, v0, Lcom/comscore/applications/Application;->dispatcher:Lcom/comscore/applications/Dispatcher;

    sget-object v3, Lcom/comscore/applications/EventType;->View:Lcom/comscore/applications/EventType;

    invoke-virtual {v2, v3, p1}, Lcom/comscore/applications/Dispatcher;->notify(Lcom/comscore/applications/EventType;Ljava/util/HashMap;)V

    goto :goto_0
.end method

.method public setAnonymous(Ljava/lang/Boolean;)V
    .locals 2
    .parameter "anonymous"

    .prologue
    .line 142
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lcom/comscore/utils/Storage;->enabled:Ljava/lang/Boolean;

    .line 143
    iput-object p1, p0, Lcom/comscore/analytics/DAx;->anonymous:Ljava/lang/Boolean;

    .line 144
    return-void

    .line 142
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAppContext(Landroid/content/Context;)Lcom/comscore/analytics/DAx;
    .locals 3
    .parameter "c"

    .prologue
    .line 147
    iput-object p1, p0, Lcom/comscore/analytics/DAx;->appContext:Landroid/content/Context;

    .line 149
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v0

    const-string v1, "PixelURL"

    invoke-virtual {v0, v1}, Lcom/comscore/utils/Storage;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/DAx;->setPixelURL(Ljava/lang/String;)Lcom/comscore/analytics/DAx;

    .line 152
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->responders:Ljava/util/Map;

    const-string v1, "Applications"

    new-instance v2, Lcom/comscore/applications/Application;

    invoke-direct {v2}, Lcom/comscore/applications/Application;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    return-object p0
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0
    .parameter "value"

    .prologue
    .line 185
    iput-object p1, p0, Lcom/comscore/analytics/DAx;->appName:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public setPixelURL(Ljava/lang/String;)Lcom/comscore/analytics/DAx;
    .locals 5
    .parameter "value"

    .prologue
    const/4 v4, 0x0

    .line 56
    const-string v0, "&"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "&"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 60
    :cond_0
    const-string v0, "?"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "//"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->pixelURL:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 63
    iget-object v0, p0, Lcom/comscore/analytics/DAx;->pixelURL:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/comscore/analytics/DAx;->pixelURL:Ljava/lang/String;

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/comscore/analytics/DAx;->pixelURL:Ljava/lang/String;

    iget-object v2, p0, Lcom/comscore/analytics/DAx;->pixelURL:Ljava/lang/String;

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 73
    :cond_1
    :goto_0
    const-string v0, "?"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "Application"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 76
    :cond_2
    iput-object p1, p0, Lcom/comscore/analytics/DAx;->pixelURL:Ljava/lang/String;

    .line 77
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v0

    const-string v1, "PixelURL"

    iget-object v2, p0, Lcom/comscore/analytics/DAx;->pixelURL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/comscore/utils/Storage;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-object p0

    .line 66
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 69
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public setSalt(Ljava/lang/String;)Lcom/comscore/analytics/DAx;
    .locals 1
    .parameter "value"

    .prologue
    .line 170
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iput-object p1, p0, Lcom/comscore/analytics/DAx;->salt:Ljava/lang/String;

    .line 174
    :cond_0
    return-object p0
.end method
