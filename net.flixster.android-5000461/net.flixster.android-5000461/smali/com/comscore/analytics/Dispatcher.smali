.class public Lcom/comscore/analytics/Dispatcher;
.super Ljava/lang/Object;
.source "Dispatcher.java"

# interfaces
.implements Lcom/comscore/metrics/DispatcherInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public notify(Lcom/comscore/events/EventArgs;Lcom/comscore/measurement/Measurement;)V
    .locals 1
    .parameter "args"
    .parameter "ms"

    .prologue
    .line 28
    invoke-static {}, Lcom/comscore/utils/Queue;->getInstance()Lcom/comscore/utils/Queue;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/comscore/utils/Queue;->enqueue(Lcom/comscore/measurement/Measurement;)V

    .line 29
    return-void
.end method

.method public notify(Lcom/comscore/metrics/EventType;)V
    .locals 1
    .parameter "type"

    .prologue
    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/comscore/analytics/Dispatcher;->notify(Lcom/comscore/metrics/EventType;Ljava/util/HashMap;)V

    .line 14
    return-void
.end method

.method public notify(Lcom/comscore/metrics/EventType;Ljava/util/HashMap;)V
    .locals 4
    .parameter "type"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/comscore/metrics/EventType;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p2, details:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Lcom/comscore/events/Event;

    invoke-virtual {p1}, Lcom/comscore/metrics/EventType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/comscore/events/Event;-><init>(Ljava/lang/String;)V

    .line 19
    .local v1, e:Lcom/comscore/events/Event;
    new-instance v0, Lcom/comscore/events/EventArgs;

    invoke-direct {v0, v1, p2}, Lcom/comscore/events/EventArgs;-><init>(Lcom/comscore/events/Event;Ljava/util/HashMap;)V

    .line 20
    .local v0, args:Lcom/comscore/events/EventArgs;
    new-instance v2, Lcom/comscore/analytics/Measurement;

    invoke-direct {v2, v0}, Lcom/comscore/analytics/Measurement;-><init>(Lcom/comscore/events/EventArgs;)V

    .line 23
    .local v2, m:Lcom/comscore/analytics/Measurement;
    invoke-virtual {p0, v0, v2}, Lcom/comscore/analytics/Dispatcher;->notify(Lcom/comscore/events/EventArgs;Lcom/comscore/measurement/Measurement;)V

    .line 24
    return-void
.end method
