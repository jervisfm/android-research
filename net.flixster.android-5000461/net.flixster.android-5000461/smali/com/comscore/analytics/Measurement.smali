.class public Lcom/comscore/analytics/Measurement;
.super Lcom/comscore/measurement/Measurement;
.source "Measurement.java"


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 12
    invoke-direct {p0}, Lcom/comscore/measurement/Measurement;-><init>()V

    .line 14
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v1, "c1"

    const-string v2, "19"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 17
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v1, "c4"

    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v2

    invoke-virtual {v2}, Lcom/comscore/analytics/DAx;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 20
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v1, "c10"

    const-string v2, "android"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 23
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v1, "c12"

    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v2

    invoke-virtual {v2}, Lcom/comscore/analytics/DAx;->getVisitorId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 26
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v0

    const-string v1, "vid"

    invoke-virtual {v0, v1}, Lcom/comscore/utils/Storage;->has(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v1, "ns_ap_c12m"

    const-string v2, "1"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 31
    :cond_0
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v1, "ns_ap_device"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 37
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v1, "ns_type"

    sget-object v2, Lcom/comscore/metrics/EventType;->View:Lcom/comscore/metrics/EventType;

    invoke-virtual {v2}, Lcom/comscore/metrics/EventType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 39
    const-string v0, "c3"

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/Measurement;->hasLabel(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 40
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v1, "c3"

    sget-object v2, Lcom/comscore/metrics/EventType;->View:Lcom/comscore/metrics/EventType;

    invoke-virtual {v2}, Lcom/comscore/metrics/EventType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 44
    :cond_1
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v1, "ns_ts"

    invoke-static {}, Lcom/comscore/utils/Date;->getInstance()Lcom/comscore/utils/Date;

    move-result-object v2

    invoke-virtual {v2}, Lcom/comscore/utils/Date;->unixTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/comscore/events/EventArgs;)V
    .locals 7
    .parameter "e"

    .prologue
    const/4 v6, 0x0

    .line 48
    invoke-direct {p0}, Lcom/comscore/measurement/Measurement;-><init>()V

    .line 50
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v3, "c1"

    const-string v4, "19"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 53
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v3, "c4"

    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v4

    invoke-virtual {v4}, Lcom/comscore/analytics/DAx;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 56
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v3, "c10"

    const-string v4, "android"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 59
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v3, "c12"

    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v4

    invoke-virtual {v4}, Lcom/comscore/analytics/DAx;->getVisitorId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 62
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v2

    const-string v3, "vid"

    invoke-virtual {v2, v3}, Lcom/comscore/utils/Storage;->has(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v3, "ns_ap_c12m"

    const-string v4, "1"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 67
    :cond_0
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v3, "ns_ap_device"

    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 69
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v3, "ns_ap_as"

    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v4

    invoke-virtual {v4}, Lcom/comscore/analytics/DAx;->getGenesis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 75
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v3, "ns_type"

    iget-object v4, p1, Lcom/comscore/events/EventArgs;->event:Lcom/comscore/events/Event;

    iget-object v4, v4, Lcom/comscore/events/Event;->type:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 77
    const-string v2, "c3"

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->hasLabel(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 78
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v3, "c3"

    iget-object v4, p1, Lcom/comscore/events/EventArgs;->event:Lcom/comscore/events/Event;

    iget-object v4, v4, Lcom/comscore/events/Event;->type:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 82
    :cond_1
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v3, "ns_ts"

    iget-wide v4, p1, Lcom/comscore/events/EventArgs;->created:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v2}, Lcom/comscore/analytics/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 84
    iget-object v2, p1, Lcom/comscore/events/EventArgs;->details:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 85
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 89
    return-void

    .line 86
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 87
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Lcom/comscore/analytics/Measurement;->setLabel(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
