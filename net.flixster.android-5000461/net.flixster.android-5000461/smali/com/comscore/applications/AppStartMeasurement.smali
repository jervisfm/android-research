.class public Lcom/comscore/applications/AppStartMeasurement;
.super Lcom/comscore/applications/Measurement;
.source "AppStartMeasurement.java"


# direct methods
.method public constructor <init>(Lcom/comscore/applications/EventArgs;)V
    .locals 8
    .parameter "args"

    .prologue
    const/4 v7, 0x0

    .line 8
    invoke-direct {p0, p1}, Lcom/comscore/applications/Measurement;-><init>(Lcom/comscore/applications/EventArgs;)V

    .line 10
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v4, "ns_ap_install"

    const-string v5, "yes"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {v0, v4, v5, v6}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 11
    .local v0, apInstall:Lcom/comscore/measurement/PrivilegedLabel;
    invoke-virtual {p0, v0}, Lcom/comscore/applications/AppStartMeasurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 13
    new-instance v3, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v4, "ns_ap_runs"

    invoke-static {}, Lcom/comscore/utils/Store;->getInstance()Lcom/comscore/utils/Store;

    move-result-object v5

    invoke-virtual {v5}, Lcom/comscore/utils/Store;->getRuns()Lcom/comscore/utils/StoreProp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/comscore/utils/StoreProp;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 14
    .local v3, apRuns:Lcom/comscore/measurement/PrivilegedLabel;
    invoke-virtual {p0, v3}, Lcom/comscore/applications/AppStartMeasurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 16
    new-instance v1, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v4, "ns_ap_pfm"

    const-string v5, "android"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {v1, v4, v5, v6}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 17
    .local v1, apPfm:Lcom/comscore/measurement/PrivilegedLabel;
    invoke-virtual {p0, v1}, Lcom/comscore/applications/AppStartMeasurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 19
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v4, "ns_ap_pfv"

    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {v2, v4, v5, v6}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 20
    .local v2, apPfv:Lcom/comscore/measurement/PrivilegedLabel;
    invoke-virtual {p0, v2}, Lcom/comscore/applications/AppStartMeasurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 21
    return-void
.end method
