.class public Lcom/comscore/applications/Application;
.super Lcom/comscore/metrics/Analytics;
.source "Application.java"


# instance fields
.field public dispatcher:Lcom/comscore/applications/Dispatcher;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/comscore/metrics/Analytics;-><init>()V

    .line 11
    new-instance v1, Lcom/comscore/applications/Dispatcher;

    invoke-direct {v1}, Lcom/comscore/applications/Dispatcher;-><init>()V

    iput-object v1, p0, Lcom/comscore/applications/Application;->dispatcher:Lcom/comscore/applications/Dispatcher;

    .line 14
    invoke-static {}, Lcom/comscore/utils/Store;->getInstance()Lcom/comscore/utils/Store;

    move-result-object v1

    invoke-virtual {v1}, Lcom/comscore/utils/Store;->getRuns()Lcom/comscore/utils/StoreProp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/comscore/utils/StoreProp;->getValue()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15
    invoke-static {}, Lcom/comscore/utils/Store;->getInstance()Lcom/comscore/utils/Store;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/comscore/utils/Store;->setFirstRun(Ljava/lang/Boolean;)V

    .line 16
    invoke-static {}, Lcom/comscore/utils/Store;->getInstance()Lcom/comscore/utils/Store;

    move-result-object v1

    invoke-virtual {v1}, Lcom/comscore/utils/Store;->getRuns()Lcom/comscore/utils/StoreProp;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Lcom/comscore/utils/StoreProp;->setValue(Ljava/lang/String;)V

    .line 19
    :cond_0
    invoke-static {}, Lcom/comscore/utils/Store;->getInstance()Lcom/comscore/utils/Store;

    move-result-object v1

    invoke-virtual {v1}, Lcom/comscore/utils/Store;->getFirstRun()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 20
    new-instance v0, Ljava/lang/Integer;

    invoke-static {}, Lcom/comscore/utils/Store;->getInstance()Lcom/comscore/utils/Store;

    move-result-object v1

    invoke-virtual {v1}, Lcom/comscore/utils/Store;->getRuns()Lcom/comscore/utils/StoreProp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/comscore/utils/StoreProp;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    .line 21
    .local v0, runs:Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 22
    invoke-static {}, Lcom/comscore/utils/Store;->getInstance()Lcom/comscore/utils/Store;

    move-result-object v1

    invoke-virtual {v1}, Lcom/comscore/utils/Store;->getRuns()Lcom/comscore/utils/StoreProp;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/comscore/utils/StoreProp;->setValue(Ljava/lang/String;)V

    .line 24
    .end local v0           #runs:Ljava/lang/Integer;
    :cond_1
    return-void
.end method
