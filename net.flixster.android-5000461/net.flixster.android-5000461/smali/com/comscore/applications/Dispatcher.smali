.class public Lcom/comscore/applications/Dispatcher;
.super Ljava/lang/Object;
.source "Dispatcher.java"

# interfaces
.implements Lcom/comscore/metrics/DispatcherInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method


# virtual methods
.method public notify(Lcom/comscore/applications/EventType;)V
    .locals 1
    .parameter "type"

    .prologue
    .line 18
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/comscore/applications/Dispatcher;->notify(Lcom/comscore/applications/EventType;Ljava/util/HashMap;)V

    .line 19
    return-void
.end method

.method public notify(Lcom/comscore/applications/EventType;Ljava/util/HashMap;)V
    .locals 4
    .parameter "type"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/comscore/applications/EventType;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p2, labels:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Lcom/comscore/applications/Event;

    invoke-direct {v1, p1}, Lcom/comscore/applications/Event;-><init>(Lcom/comscore/applications/EventType;)V

    .line 24
    .local v1, e:Lcom/comscore/applications/Event;
    new-instance v0, Lcom/comscore/applications/EventArgs;

    invoke-direct {v0, p1, v1, p2}, Lcom/comscore/applications/EventArgs;-><init>(Lcom/comscore/applications/EventType;Lcom/comscore/applications/Event;Ljava/util/HashMap;)V

    .line 25
    .local v0, args:Lcom/comscore/applications/EventArgs;
    const/4 v2, 0x0

    .line 27
    .local v2, m:Lcom/comscore/applications/Measurement;
    sget-object v3, Lcom/comscore/applications/EventType;->Start:Lcom/comscore/applications/EventType;

    invoke-virtual {p1, v3}, Lcom/comscore/applications/EventType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 28
    new-instance v2, Lcom/comscore/applications/AppStartMeasurement;

    .end local v2           #m:Lcom/comscore/applications/Measurement;
    invoke-direct {v2, v0}, Lcom/comscore/applications/AppStartMeasurement;-><init>(Lcom/comscore/applications/EventArgs;)V

    .line 35
    .restart local v2       #m:Lcom/comscore/applications/Measurement;
    :cond_0
    :goto_0
    sget-object v3, Lcom/comscore/applications/EventType;->Close:Lcom/comscore/applications/EventType;

    invoke-virtual {p1, v3}, Lcom/comscore/applications/EventType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 36
    invoke-virtual {p0, v0, v2}, Lcom/comscore/applications/Dispatcher;->notify(Lcom/comscore/events/EventArgs;Lcom/comscore/measurement/Measurement;)V

    .line 37
    :cond_1
    return-void

    .line 29
    :cond_2
    sget-object v3, Lcom/comscore/applications/EventType;->Aggregate:Lcom/comscore/applications/EventType;

    invoke-virtual {p1, v3}, Lcom/comscore/applications/EventType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 30
    new-instance v2, Lcom/comscore/applications/AggregateMeasurement;

    .end local v2           #m:Lcom/comscore/applications/Measurement;
    invoke-direct {v2, v0}, Lcom/comscore/applications/AggregateMeasurement;-><init>(Lcom/comscore/applications/EventArgs;)V

    .restart local v2       #m:Lcom/comscore/applications/Measurement;
    goto :goto_0

    .line 31
    :cond_3
    sget-object v3, Lcom/comscore/applications/EventType;->Close:Lcom/comscore/applications/EventType;

    invoke-virtual {p1, v3}, Lcom/comscore/applications/EventType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 32
    new-instance v2, Lcom/comscore/applications/Measurement;

    .end local v2           #m:Lcom/comscore/applications/Measurement;
    invoke-direct {v2, v0}, Lcom/comscore/applications/Measurement;-><init>(Lcom/comscore/applications/EventArgs;)V

    .restart local v2       #m:Lcom/comscore/applications/Measurement;
    goto :goto_0
.end method

.method public notify(Lcom/comscore/events/EventArgs;Lcom/comscore/measurement/Measurement;)V
    .locals 1
    .parameter "args"
    .parameter "ms"

    .prologue
    .line 42
    invoke-static {}, Lcom/comscore/utils/Queue;->getInstance()Lcom/comscore/utils/Queue;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/comscore/utils/Queue;->enqueue(Lcom/comscore/measurement/Measurement;)V

    .line 43
    return-void
.end method
