.class public Lcom/comscore/applications/Event;
.super Lcom/comscore/events/Event;
.source "Event.java"


# instance fields
.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/comscore/applications/EventType;)V
    .locals 1
    .parameter "appType"

    .prologue
    .line 13
    sget-object v0, Lcom/comscore/metrics/EventType;->View:Lcom/comscore/metrics/EventType;

    invoke-direct {p0, p1, v0}, Lcom/comscore/applications/Event;-><init>(Lcom/comscore/applications/EventType;Lcom/comscore/metrics/EventType;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Lcom/comscore/applications/EventType;Lcom/comscore/metrics/EventType;)V
    .locals 1
    .parameter "appType"
    .parameter "anaType"

    .prologue
    .line 8
    invoke-virtual {p2}, Lcom/comscore/metrics/EventType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/comscore/events/Event;-><init>(Ljava/lang/String;)V

    .line 9
    invoke-virtual {p1}, Lcom/comscore/applications/EventType;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/comscore/applications/Event;->name:Ljava/lang/String;

    .line 10
    return-void
.end method
