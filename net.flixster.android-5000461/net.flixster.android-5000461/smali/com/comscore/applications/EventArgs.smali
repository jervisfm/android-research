.class public Lcom/comscore/applications/EventArgs;
.super Lcom/comscore/events/EventArgs;
.source "EventArgs.java"


# instance fields
.field public appEventType:Lcom/comscore/applications/EventType;

.field public pageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/comscore/applications/Event;)V
    .locals 1
    .parameter "e"

    .prologue
    .line 19
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/comscore/applications/EventArgs;-><init>(Lcom/comscore/applications/Event;Ljava/util/HashMap;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/comscore/applications/Event;Ljava/util/HashMap;)V
    .locals 1
    .parameter "e"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/comscore/applications/Event;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p2, details:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lcom/comscore/applications/EventType;->View:Lcom/comscore/applications/EventType;

    invoke-direct {p0, v0, p1, p2}, Lcom/comscore/applications/EventArgs;-><init>(Lcom/comscore/applications/EventType;Lcom/comscore/applications/Event;Ljava/util/HashMap;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Lcom/comscore/applications/EventType;)V
    .locals 2
    .parameter "appType"

    .prologue
    .line 23
    new-instance v0, Lcom/comscore/applications/Event;

    invoke-direct {v0, p1}, Lcom/comscore/applications/Event;-><init>(Lcom/comscore/applications/EventType;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lcom/comscore/applications/EventArgs;-><init>(Lcom/comscore/applications/EventType;Lcom/comscore/applications/Event;Ljava/util/HashMap;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/comscore/applications/EventType;Lcom/comscore/applications/Event;Ljava/util/HashMap;)V
    .locals 1
    .parameter "appType"
    .parameter "e"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/comscore/applications/EventType;",
            "Lcom/comscore/applications/Event;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 10
    .local p3, details:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lcom/comscore/metrics/EventType;->View:Lcom/comscore/metrics/EventType;

    invoke-direct {p0, v0, p2, p3}, Lcom/comscore/events/EventArgs;-><init>(Lcom/comscore/metrics/EventType;Lcom/comscore/events/Event;Ljava/util/HashMap;)V

    .line 11
    iput-object p1, p0, Lcom/comscore/applications/EventArgs;->appEventType:Lcom/comscore/applications/EventType;

    .line 12
    return-void
.end method
