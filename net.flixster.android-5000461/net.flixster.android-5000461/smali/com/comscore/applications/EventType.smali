.class public final enum Lcom/comscore/applications/EventType;
.super Ljava/lang/Enum;
.source "EventType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/comscore/applications/EventType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum Aggregate:Lcom/comscore/applications/EventType;

.field public static final enum Close:Lcom/comscore/applications/EventType;

.field private static final synthetic ENUM$VALUES:[Lcom/comscore/applications/EventType;

.field public static final enum Start:Lcom/comscore/applications/EventType;

.field public static final enum View:Lcom/comscore/applications/EventType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/comscore/applications/EventType;

    const-string v1, "Start"

    invoke-direct {v0, v1, v2}, Lcom/comscore/applications/EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/comscore/applications/EventType;->Start:Lcom/comscore/applications/EventType;

    .line 5
    new-instance v0, Lcom/comscore/applications/EventType;

    const-string v1, "View"

    invoke-direct {v0, v1, v3}, Lcom/comscore/applications/EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/comscore/applications/EventType;->View:Lcom/comscore/applications/EventType;

    .line 6
    new-instance v0, Lcom/comscore/applications/EventType;

    const-string v1, "Close"

    invoke-direct {v0, v1, v4}, Lcom/comscore/applications/EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/comscore/applications/EventType;->Close:Lcom/comscore/applications/EventType;

    .line 7
    new-instance v0, Lcom/comscore/applications/EventType;

    const-string v1, "Aggregate"

    invoke-direct {v0, v1, v5}, Lcom/comscore/applications/EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/comscore/applications/EventType;->Aggregate:Lcom/comscore/applications/EventType;

    .line 3
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/comscore/applications/EventType;

    sget-object v1, Lcom/comscore/applications/EventType;->Start:Lcom/comscore/applications/EventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/comscore/applications/EventType;->View:Lcom/comscore/applications/EventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/comscore/applications/EventType;->Close:Lcom/comscore/applications/EventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/comscore/applications/EventType;->Aggregate:Lcom/comscore/applications/EventType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/comscore/applications/EventType;->ENUM$VALUES:[Lcom/comscore/applications/EventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/comscore/applications/EventType;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/comscore/applications/EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/comscore/applications/EventType;

    return-object v0
.end method

.method public static values()[Lcom/comscore/applications/EventType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/comscore/applications/EventType;->ENUM$VALUES:[Lcom/comscore/applications/EventType;

    array-length v1, v0

    new-array v2, v1, [Lcom/comscore/applications/EventType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
