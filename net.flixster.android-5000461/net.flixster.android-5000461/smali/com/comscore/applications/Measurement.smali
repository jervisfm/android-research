.class public Lcom/comscore/applications/Measurement;
.super Lcom/comscore/analytics/Measurement;
.source "Measurement.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 13
    new-instance v0, Lcom/comscore/events/EventArgs;

    new-instance v1, Lcom/comscore/events/Event;

    sget-object v2, Lcom/comscore/metrics/EventType;->View:Lcom/comscore/metrics/EventType;

    invoke-virtual {v2}, Lcom/comscore/metrics/EventType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/comscore/events/Event;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/comscore/events/EventArgs;-><init>(Lcom/comscore/events/Event;)V

    invoke-direct {p0, v0}, Lcom/comscore/analytics/Measurement;-><init>(Lcom/comscore/events/EventArgs;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Lcom/comscore/applications/EventArgs;)V
    .locals 14
    .parameter "args"

    .prologue
    const/4 v13, 0x0

    .line 17
    new-instance v8, Lcom/comscore/events/EventArgs;

    new-instance v9, Lcom/comscore/events/Event;

    sget-object v10, Lcom/comscore/metrics/EventType;->View:Lcom/comscore/metrics/EventType;

    invoke-virtual {v10}, Lcom/comscore/metrics/EventType;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/comscore/events/Event;-><init>(Ljava/lang/String;)V

    iget-object v10, p1, Lcom/comscore/applications/EventArgs;->details:Ljava/util/HashMap;

    invoke-direct {v8, v9, v10}, Lcom/comscore/events/EventArgs;-><init>(Lcom/comscore/events/Event;Ljava/util/HashMap;)V

    invoke-direct {p0, v8}, Lcom/comscore/analytics/Measurement;-><init>(Lcom/comscore/events/EventArgs;)V

    .line 20
    iget-object v8, p1, Lcom/comscore/applications/EventArgs;->pageName:Ljava/lang/String;

    if-eqz v8, :cond_0

    .line 21
    iget-object v8, p1, Lcom/comscore/applications/EventArgs;->pageName:Ljava/lang/String;

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 22
    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v8

    iget-object v9, p1, Lcom/comscore/applications/EventArgs;->pageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/comscore/analytics/DAx;->setPixelURL(Ljava/lang/String;)Lcom/comscore/analytics/DAx;

    .line 24
    :cond_0
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v8, "ns_ap_event"

    iget-object v9, p1, Lcom/comscore/applications/EventArgs;->appEventType:Lcom/comscore/applications/EventType;

    invoke-virtual {v9}, Lcom/comscore/applications/EventType;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-direct {v0, v8, v9, v10}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 25
    .local v0, apEvent:Lcom/comscore/measurement/PrivilegedLabel;
    invoke-virtual {p0, v0}, Lcom/comscore/applications/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 27
    new-instance v8, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v9, "c3"

    iget-object v10, p1, Lcom/comscore/applications/EventArgs;->appEventType:Lcom/comscore/applications/EventType;

    invoke-virtual {v10}, Lcom/comscore/applications/EventType;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-direct {v8, v9, v10, v11}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v8}, Lcom/comscore/applications/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 29
    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v8

    invoke-virtual {v8}, Lcom/comscore/analytics/DAx;->getAppContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 30
    .local v7, pkg:Ljava/lang/String;
    const/4 v5, 0x0

    .line 32
    .local v5, appVersion:Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v8

    invoke-virtual {v8}, Lcom/comscore/analytics/DAx;->getAppContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v7, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    iget-object v5, v8, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    new-instance v3, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v8, "ns_ap_ver"

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {v3, v8, v5, v9}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 37
    .local v3, apVer:Lcom/comscore/measurement/PrivilegedLabel;
    invoke-virtual {p0, v3}, Lcom/comscore/applications/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 42
    new-instance v2, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v8, "ns_ap_usage"

    iget-wide v9, p1, Lcom/comscore/applications/EventArgs;->created:J

    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v11

    invoke-virtual {v11}, Lcom/comscore/analytics/DAx;->getGenesis()J

    move-result-wide v11

    sub-long/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-direct {v2, v8, v9, v10}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 43
    .local v2, apUsage:Lcom/comscore/measurement/PrivilegedLabel;
    invoke-virtual {p0, v2}, Lcom/comscore/applications/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 45
    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v8

    invoke-virtual {v8}, Lcom/comscore/analytics/DAx;->getAppContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "window"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/WindowManager;

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    .line 46
    .local v6, d:Landroid/view/Display;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Landroid/view/Display;->getWidth()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Landroid/view/Display;->getHeight()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 47
    .local v4, appResolution:Ljava/lang/String;
    new-instance v1, Lcom/comscore/measurement/PrivilegedLabel;

    const-string v8, "ns_ap_res"

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {v1, v8, v4, v9}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 48
    .local v1, apRes:Lcom/comscore/measurement/PrivilegedLabel;
    invoke-virtual {p0, v1}, Lcom/comscore/applications/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    .line 49
    return-void

    .line 33
    .end local v1           #apRes:Lcom/comscore/measurement/PrivilegedLabel;
    .end local v2           #apUsage:Lcom/comscore/measurement/PrivilegedLabel;
    .end local v3           #apVer:Lcom/comscore/measurement/PrivilegedLabel;
    .end local v4           #appResolution:Ljava/lang/String;
    .end local v6           #d:Landroid/view/Display;
    :catch_0
    move-exception v8

    goto :goto_0
.end method
