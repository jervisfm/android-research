.class public Lcom/comscore/events/EventArgs;
.super Ljava/lang/Object;
.source "EventArgs.java"


# instance fields
.field public cancel:Ljava/lang/Boolean;

.field public created:J

.field public details:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public event:Lcom/comscore/events/Event;

.field public eventType:Lcom/comscore/metrics/EventType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(Lcom/comscore/events/Event;)V
    .locals 1
    .parameter "e"

    .prologue
    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/comscore/events/EventArgs;-><init>(Lcom/comscore/events/Event;Ljava/util/HashMap;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/comscore/events/Event;Ljava/util/HashMap;)V
    .locals 2
    .parameter "e"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/comscore/events/Event;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p2, details:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/comscore/events/EventArgs;->event:Lcom/comscore/events/Event;

    .line 30
    invoke-static {}, Lcom/comscore/utils/Date;->getInstance()Lcom/comscore/utils/Date;

    move-result-object v0

    invoke-virtual {v0}, Lcom/comscore/utils/Date;->unixTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/comscore/events/EventArgs;->created:J

    .line 31
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/comscore/events/EventArgs;->cancel:Ljava/lang/Boolean;

    .line 32
    iput-object p2, p0, Lcom/comscore/events/EventArgs;->details:Ljava/util/HashMap;

    .line 33
    return-void
.end method

.method public constructor <init>(Lcom/comscore/metrics/EventType;Lcom/comscore/events/Event;Ljava/util/HashMap;)V
    .locals 0
    .parameter "type"
    .parameter "e"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/comscore/metrics/EventType;",
            "Lcom/comscore/events/Event;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p3, details:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p2, p3}, Lcom/comscore/events/EventArgs;-><init>(Lcom/comscore/events/Event;Ljava/util/HashMap;)V

    .line 22
    iput-object p1, p0, Lcom/comscore/events/EventArgs;->eventType:Lcom/comscore/metrics/EventType;

    .line 23
    return-void
.end method


# virtual methods
.method public cancelEvent()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/comscore/events/EventArgs;->cancel:Ljava/lang/Boolean;

    .line 44
    return-void
.end method
