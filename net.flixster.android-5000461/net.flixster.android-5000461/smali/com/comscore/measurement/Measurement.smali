.class public Lcom/comscore/measurement/Measurement;
.super Ljava/lang/Object;
.source "Measurement.java"

# interfaces
.implements Lcom/comscore/measurement/MeasurementInterface;


# instance fields
.field protected labels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/comscore/measurement/Label;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/comscore/measurement/Measurement;->labels:Ljava/util/ArrayList;

    .line 15
    return-void
.end method

.method public constructor <init>(Lcom/comscore/events/EventArgs;)V
    .locals 0
    .parameter "args"

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, list:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/comscore/measurement/Measurement;-><init>()V

    .line 19
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 20
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 24
    return-void

    .line 21
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 22
    .local v0, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Lcom/comscore/measurement/Measurement;->setLabel(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected appendLabel(Lcom/comscore/measurement/Label;)V
    .locals 2
    .parameter "label"

    .prologue
    .line 53
    iget-object v1, p1, Lcom/comscore/measurement/Label;->name:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/comscore/measurement/Measurement;->retrieveLabel(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 54
    .local v0, labelList:Ljava/util/List;,"Ljava/util/List<Lcom/comscore/measurement/Label;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 55
    iget-object v1, p0, Lcom/comscore/measurement/Measurement;->labels:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_0
    return-void
.end method

.method protected appendLabel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1
    .parameter "name"
    .parameter "value"
    .parameter "aggregate"
    .parameter "comscorePrivilege"

    .prologue
    .line 59
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Lcom/comscore/measurement/PrivilegedLabel;

    invoke-direct {v0, p1, p2, p3}, Lcom/comscore/measurement/PrivilegedLabel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/measurement/Measurement;->appendLabel(Lcom/comscore/measurement/Label;)V

    .line 63
    :goto_0
    return-void

    .line 62
    :cond_0
    new-instance v0, Lcom/comscore/measurement/Label;

    invoke-direct {v0, p1, p2, p3}, Lcom/comscore/measurement/Label;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lcom/comscore/measurement/Measurement;->appendLabel(Lcom/comscore/measurement/Label;)V

    goto :goto_0
.end method

.method protected compareLabel(Ljava/lang/String;J)Ljava/lang/Boolean;
    .locals 1
    .parameter "name"
    .parameter "value"

    .prologue
    .line 49
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/comscore/measurement/Measurement;->compareLabel(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected compareLabel(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 4
    .parameter "name"
    .parameter "value"

    .prologue
    const/4 v3, 0x0

    .line 40
    invoke-virtual {p0, p1}, Lcom/comscore/measurement/Measurement;->retrieveLabel(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 41
    .local v1, labelList:Ljava/util/List;,"Ljava/util/List<Lcom/comscore/measurement/Label;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 42
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/comscore/measurement/Label;

    .line 43
    .local v0, l:Lcom/comscore/measurement/Label;
    iget-object v2, v0, Lcom/comscore/measurement/Label;->value:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 45
    .end local v0           #l:Lcom/comscore/measurement/Label;
    :goto_0
    return-object v2

    :cond_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0
.end method

.method public getLabel(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/comscore/measurement/Label;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0, p1}, Lcom/comscore/measurement/Measurement;->retrieveLabel(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public hasLabel(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2
    .parameter "name"

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lcom/comscore/measurement/Measurement;->retrieveLabel(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 102
    .local v0, labelList:Ljava/util/List;,"Ljava/util/List<Lcom/comscore/measurement/Label;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public pack()Ljava/lang/String;
    .locals 4

    .prologue
    .line 107
    const-string v1, ""

    .line 108
    .local v1, str:Ljava/lang/String;
    iget-object v2, p0, Lcom/comscore/measurement/Measurement;->labels:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 110
    return-object v1

    .line 108
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/comscore/measurement/Label;

    .line 109
    .local v0, l:Lcom/comscore/measurement/Label;
    invoke-virtual {v0}, Lcom/comscore/measurement/Label;->pack()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected removeLabel(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 4
    .parameter "name"
    .parameter "aggregate"
    .parameter "comscorePrivilege"

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/comscore/measurement/Measurement;->retrieveLabel(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 67
    .local v1, labelList:Ljava/util/List;,"Ljava/util/List<Lcom/comscore/measurement/Label;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 70
    return-void

    .line 67
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/comscore/measurement/Label;

    .line 68
    .local v0, l:Lcom/comscore/measurement/Label;
    instance-of v3, v0, Lcom/comscore/measurement/PrivilegedLabel;

    if-nez v3, :cond_2

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 69
    :cond_2
    iget-object v3, p0, Lcom/comscore/measurement/Measurement;->labels:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected retrieveLabel(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/comscore/measurement/Label;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .local v1, labelList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/comscore/measurement/Label;>;"
    iget-object v2, p0, Lcom/comscore/measurement/Measurement;->labels:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 36
    return-object v1

    .line 33
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/comscore/measurement/Label;

    .line 34
    .local v0, l:Lcom/comscore/measurement/Label;
    iget-object v3, v0, Lcom/comscore/measurement/Label;->name:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 35
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setLabel(Lcom/comscore/measurement/Label;)V
    .locals 3
    .parameter "label"

    .prologue
    .line 78
    iget-object v0, p1, Lcom/comscore/measurement/Label;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/comscore/measurement/Label;->aggregate:Ljava/lang/Boolean;

    instance-of v2, p1, Lcom/comscore/measurement/PrivilegedLabel;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/comscore/measurement/Measurement;->removeLabel(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 79
    invoke-virtual {p0, p1}, Lcom/comscore/measurement/Measurement;->appendLabel(Lcom/comscore/measurement/Label;)V

    .line 80
    return-void
.end method

.method public setLabel(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "name"
    .parameter "value"

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/comscore/measurement/Measurement;->setLabel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 92
    return-void
.end method

.method public setLabel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 2
    .parameter "name"
    .parameter "value"
    .parameter "aggregate"

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, p1, p3, v0}, Lcom/comscore/measurement/Measurement;->removeLabel(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 86
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/comscore/measurement/Measurement;->appendLabel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 87
    return-void
.end method

.method protected setLabel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0
    .parameter "name"
    .parameter "value"
    .parameter "aggregate"
    .parameter "comscorePrivilege"

    .prologue
    .line 73
    invoke-virtual {p0, p1, p3, p4}, Lcom/comscore/measurement/Measurement;->removeLabel(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 74
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/comscore/measurement/Measurement;->appendLabel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 75
    return-void
.end method
