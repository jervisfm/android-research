.class public interface abstract Lcom/comscore/measurement/MeasurementInterface;
.super Ljava/lang/Object;
.source "MeasurementInterface.java"


# virtual methods
.method public abstract getLabel(Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/comscore/measurement/Label;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasLabel(Ljava/lang/String;)Ljava/lang/Boolean;
.end method

.method public abstract pack()Ljava/lang/String;
.end method

.method public abstract setLabel(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setLabel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
.end method
