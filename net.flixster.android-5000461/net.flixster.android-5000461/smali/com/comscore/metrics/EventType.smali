.class public final enum Lcom/comscore/metrics/EventType;
.super Ljava/lang/Enum;
.source "EventType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/comscore/metrics/EventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/comscore/metrics/EventType;

.field public static final enum Hidden:Lcom/comscore/metrics/EventType;

.field public static final enum View:Lcom/comscore/metrics/EventType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/comscore/metrics/EventType;

    const-string v1, "View"

    invoke-direct {v0, v1, v2}, Lcom/comscore/metrics/EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/comscore/metrics/EventType;->View:Lcom/comscore/metrics/EventType;

    .line 5
    new-instance v0, Lcom/comscore/metrics/EventType;

    const-string v1, "Hidden"

    invoke-direct {v0, v1, v3}, Lcom/comscore/metrics/EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/comscore/metrics/EventType;->Hidden:Lcom/comscore/metrics/EventType;

    .line 3
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/comscore/metrics/EventType;

    sget-object v1, Lcom/comscore/metrics/EventType;->View:Lcom/comscore/metrics/EventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/comscore/metrics/EventType;->Hidden:Lcom/comscore/metrics/EventType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/comscore/metrics/EventType;->ENUM$VALUES:[Lcom/comscore/metrics/EventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/comscore/metrics/EventType;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/comscore/metrics/EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/comscore/metrics/EventType;

    return-object v0
.end method

.method public static values()[Lcom/comscore/metrics/EventType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/comscore/metrics/EventType;->ENUM$VALUES:[Lcom/comscore/metrics/EventType;

    array-length v1, v0

    new-array v2, v1, [Lcom/comscore/metrics/EventType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
