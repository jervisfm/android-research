.class public Lcom/comscore/metrics/Request;
.super Landroid/os/AsyncTask;
.source "Request.java"

# interfaces
.implements Lcom/comscore/metrics/RequestInterface;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/net/URL;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;",
        "Lcom/comscore/metrics/RequestInterface;"
    }
.end annotation


# instance fields
.field private conn:Ljava/net/HttpURLConnection;

.field private measurement:Lcom/comscore/measurement/Measurement;


# direct methods
.method public constructor <init>(Lcom/comscore/measurement/Measurement;)V
    .locals 0
    .parameter "m"

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/comscore/metrics/Request;->measurement:Lcom/comscore/measurement/Measurement;

    .line 27
    return-void
.end method


# virtual methods
.method public availableConnection()Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 77
    :try_start_0
    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v4

    invoke-virtual {v4}, Lcom/comscore/analytics/DAx;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 78
    .local v0, connManager:Landroid/net/ConnectivityManager;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 79
    .local v2, wlanNetInfo:Landroid/net/NetworkInfo;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 81
    .local v3, wwanNetInfo:Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 82
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 90
    .end local v0           #connManager:Landroid/net/ConnectivityManager;
    .end local v2           #wlanNetInfo:Landroid/net/NetworkInfo;
    .end local v3           #wwanNetInfo:Landroid/net/NetworkInfo;
    :goto_0
    return-object v4

    .line 84
    .restart local v0       #connManager:Landroid/net/ConnectivityManager;
    .restart local v2       #wlanNetInfo:Landroid/net/NetworkInfo;
    .restart local v3       #wwanNetInfo:Landroid/net/NetworkInfo;
    :cond_0
    if-eqz v3, :cond_1

    .line 85
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0

    .line 88
    :cond_1
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 90
    .end local v0           #connManager:Landroid/net/ConnectivityManager;
    .end local v2           #wlanNetInfo:Landroid/net/NetworkInfo;
    .end local v3           #wwanNetInfo:Landroid/net/NetworkInfo;
    :catch_0
    move-exception v1

    .local v1, ex:Ljava/lang/NullPointerException;
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0
.end method

.method protected varargs doInBackground([Ljava/net/URL;)Ljava/lang/Integer;
    .locals 1
    .parameter "arg0"

    .prologue
    .line 98
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Lcom/comscore/metrics/Request;->conn:Ljava/net/HttpURLConnection;

    .line 99
    iget-object v0, p0, Lcom/comscore/metrics/Request;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    .line 101
    :catch_0
    move-exception v0

    .line 103
    const/16 v0, 0x190

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/net/URL;

    invoke-virtual {p0, p1}, Lcom/comscore/metrics/Request;->doInBackground([Ljava/net/URL;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 109
    iget-object v0, p0, Lcom/comscore/metrics/Request;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 112
    invoke-static {}, Lcom/comscore/utils/Queue;->getInstance()Lcom/comscore/utils/Queue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/comscore/utils/Queue;->dequeue()Lcom/comscore/metrics/Request;

    .line 114
    const/16 v0, 0xc8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v0

    const-string v1, "lastTimeout"

    iget-object v2, p0, Lcom/comscore/metrics/Request;->measurement:Lcom/comscore/measurement/Measurement;

    invoke-virtual {v2}, Lcom/comscore/measurement/Measurement;->pack()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/comscore/utils/Storage;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/comscore/metrics/Request;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method public process()Ljava/net/URL;
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/comscore/analytics/DAx;->getPixelURL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/comscore/metrics/Request;->process(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method public process(Ljava/lang/String;)Ljava/net/URL;
    .locals 2
    .parameter "basePixelURL"

    .prologue
    const/16 v1, 0x800

    .line 34
    iget-object v0, p0, Lcom/comscore/metrics/Request;->measurement:Lcom/comscore/measurement/Measurement;

    invoke-virtual {v0}, Lcom/comscore/measurement/Measurement;->pack()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 35
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 40
    :cond_0
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    return-object v0

    .line 42
    :catch_0
    move-exception v0

    .line 44
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public send()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    const-string v0, "sdk"

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    new-array v0, v3, [Ljava/net/URL;

    invoke-virtual {p0}, Lcom/comscore/metrics/Request;->process()Ljava/net/URL;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/comscore/metrics/Request;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 71
    :goto_0
    return-void

    .line 62
    :cond_0
    const-string v0, "android.permission.ACCESS_NETWORK_STATE"

    invoke-static {v0}, Lcom/comscore/utils/Permissions;->check(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/comscore/metrics/Request;->availableConnection()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64
    :cond_1
    new-array v0, v3, [Ljava/net/URL;

    invoke-virtual {p0}, Lcom/comscore/metrics/Request;->process()Ljava/net/URL;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/comscore/metrics/Request;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 68
    :cond_2
    invoke-static {}, Lcom/comscore/utils/Queue;->getInstance()Lcom/comscore/utils/Queue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/comscore/utils/Queue;->dequeue()Lcom/comscore/metrics/Request;

    goto :goto_0
.end method
