.class public Lcom/comscore/product/Product;
.super Ljava/lang/Object;
.source "Product.java"


# instance fields
.field dispatcher:Lcom/comscore/analytics/Dispatcher;

.field parent:Lcom/comscore/analytics/DAx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(Lcom/comscore/analytics/DAx;)V
    .locals 0
    .parameter "p"

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/comscore/product/Product;->parent:Lcom/comscore/analytics/DAx;

    .line 20
    return-void
.end method


# virtual methods
.method public getDispatcher()Lcom/comscore/analytics/Dispatcher;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/comscore/product/Product;->dispatcher:Lcom/comscore/analytics/Dispatcher;

    return-object v0
.end method

.method public getParent()Lcom/comscore/analytics/DAx;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/comscore/product/Product;->parent:Lcom/comscore/analytics/DAx;

    return-object v0
.end method

.method public setDispatcher(Lcom/comscore/analytics/Dispatcher;)V
    .locals 0
    .parameter "dispatcher"

    .prologue
    .line 35
    iput-object p1, p0, Lcom/comscore/product/Product;->dispatcher:Lcom/comscore/analytics/Dispatcher;

    .line 36
    return-void
.end method

.method public setParent(Lcom/comscore/analytics/DAx;)V
    .locals 0
    .parameter "parent"

    .prologue
    .line 27
    iput-object p1, p0, Lcom/comscore/product/Product;->parent:Lcom/comscore/analytics/DAx;

    .line 28
    return-void
.end method
