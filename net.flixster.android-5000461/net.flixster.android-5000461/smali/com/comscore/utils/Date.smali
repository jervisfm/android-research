.class public Lcom/comscore/utils/Date;
.super Ljava/lang/Object;
.source "Date.java"


# static fields
.field private static instance:Lcom/comscore/utils/Date;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    const/4 v0, 0x0

    sput-object v0, Lcom/comscore/utils/Date;->instance:Lcom/comscore/utils/Date;

    .line 3
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/comscore/utils/Date;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/comscore/utils/Date;->instance:Lcom/comscore/utils/Date;

    if-nez v0, :cond_0

    .line 11
    new-instance v0, Lcom/comscore/utils/Date;

    invoke-direct {v0}, Lcom/comscore/utils/Date;-><init>()V

    sput-object v0, Lcom/comscore/utils/Date;->instance:Lcom/comscore/utils/Date;

    .line 12
    :cond_0
    sget-object v0, Lcom/comscore/utils/Date;->instance:Lcom/comscore/utils/Date;

    return-object v0
.end method


# virtual methods
.method public unixTime()J
    .locals 2

    .prologue
    .line 16
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method
