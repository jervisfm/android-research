.class public Lcom/comscore/utils/Permissions;
.super Ljava/lang/Object;
.source "Permissions.java"


# static fields
.field private static granted:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    sput-object v0, Lcom/comscore/utils/Permissions;->granted:[Ljava/lang/String;

    .line 9
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static check(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 7
    .parameter "permission"

    .prologue
    .line 14
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 16
    .local v3, response:Ljava/lang/Boolean;
    sget-object v4, Lcom/comscore/utils/Permissions;->granted:[Ljava/lang/String;

    if-nez v4, :cond_0

    .line 18
    :try_start_0
    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v4

    invoke-virtual {v4}, Lcom/comscore/analytics/DAx;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 19
    .local v0, context:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x1000

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 20
    .local v2, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    sput-object v4, Lcom/comscore/utils/Permissions;->granted:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    .end local v0           #context:Landroid/content/Context;
    .end local v2           #packageInfo:Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    sget-object v4, Lcom/comscore/utils/Permissions;->granted:[Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 27
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    sget-object v4, Lcom/comscore/utils/Permissions;->granted:[Ljava/lang/String;

    array-length v4, v4

    if-lt v1, v4, :cond_2

    .line 38
    .end local v1           #i:I
    :cond_1
    :goto_2
    return-object v3

    .line 29
    .restart local v1       #i:I
    :cond_2
    sget-object v4, Lcom/comscore/utils/Permissions;->granted:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 31
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 32
    goto :goto_2

    .line 27
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 22
    .end local v1           #i:I
    :catch_0
    move-exception v4

    goto :goto_0
.end method
