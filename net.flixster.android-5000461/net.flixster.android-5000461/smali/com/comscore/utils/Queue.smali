.class public Lcom/comscore/utils/Queue;
.super Ljava/util/concurrent/ConcurrentLinkedQueue;
.source "Queue.java"


# static fields
.field private static instance:Lcom/comscore/utils/Queue; = null

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private aggr:Lcom/comscore/applications/AggregateMeasurement;

.field private last:Lcom/comscore/metrics/Request;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-object v0, Lcom/comscore/utils/Queue;->instance:Lcom/comscore/utils/Queue;

    .line 12
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/comscore/utils/Queue;->aggr:Lcom/comscore/applications/AggregateMeasurement;

    .line 26
    return-void
.end method

.method private engage()V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/comscore/utils/Queue;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/comscore/utils/Queue;->process()V

    .line 42
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/comscore/utils/Queue;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/comscore/utils/Queue;->instance:Lcom/comscore/utils/Queue;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/comscore/utils/Queue;

    invoke-direct {v0}, Lcom/comscore/utils/Queue;-><init>()V

    sput-object v0, Lcom/comscore/utils/Queue;->instance:Lcom/comscore/utils/Queue;

    .line 31
    :cond_0
    sget-object v0, Lcom/comscore/utils/Queue;->instance:Lcom/comscore/utils/Queue;

    return-object v0
.end method


# virtual methods
.method public dequeue()Lcom/comscore/metrics/Request;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/comscore/utils/Queue;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 85
    invoke-super {p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/comscore/metrics/Request;

    iput-object v0, p0, Lcom/comscore/utils/Queue;->last:Lcom/comscore/metrics/Request;

    .line 86
    invoke-virtual {p0}, Lcom/comscore/utils/Queue;->process()V

    .line 87
    iget-object v0, p0, Lcom/comscore/utils/Queue;->last:Lcom/comscore/metrics/Request;

    .line 89
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enqueue(Lcom/comscore/measurement/Measurement;)V
    .locals 4
    .parameter "m"

    .prologue
    .line 60
    instance-of v2, p1, Lcom/comscore/applications/AggregateMeasurement;

    if-eqz v2, :cond_1

    .line 61
    iget-object v2, p0, Lcom/comscore/utils/Queue;->aggr:Lcom/comscore/applications/AggregateMeasurement;

    if-nez v2, :cond_0

    .line 62
    check-cast p1, Lcom/comscore/applications/AggregateMeasurement;

    .end local p1
    iput-object p1, p0, Lcom/comscore/utils/Queue;->aggr:Lcom/comscore/applications/AggregateMeasurement;

    .line 63
    iget-object v2, p0, Lcom/comscore/utils/Queue;->aggr:Lcom/comscore/applications/AggregateMeasurement;

    invoke-virtual {v2}, Lcom/comscore/applications/AggregateMeasurement;->formatLists()V

    .line 80
    :goto_0
    return-void

    .line 66
    .restart local p1
    :cond_0
    iget-object v2, p0, Lcom/comscore/utils/Queue;->aggr:Lcom/comscore/applications/AggregateMeasurement;

    check-cast p1, Lcom/comscore/applications/AggregateMeasurement;

    .end local p1
    invoke-virtual {p1}, Lcom/comscore/applications/AggregateMeasurement;->getAggregateLabels()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/comscore/applications/AggregateMeasurement;->aggregateLabels(Ljava/util/List;)V

    goto :goto_0

    .line 70
    .restart local p1
    :cond_1
    iget-object v2, p0, Lcom/comscore/utils/Queue;->aggr:Lcom/comscore/applications/AggregateMeasurement;

    if-eqz v2, :cond_2

    .line 72
    iget-object v2, p0, Lcom/comscore/utils/Queue;->aggr:Lcom/comscore/applications/AggregateMeasurement;

    invoke-virtual {v2}, Lcom/comscore/applications/AggregateMeasurement;->getAggregateLabels()Ljava/util/List;

    move-result-object v0

    .line 73
    .local v0, aggregateLabels:Ljava/util/List;,"Ljava/util/List<Lcom/comscore/measurement/Label;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 76
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/comscore/utils/Queue;->aggr:Lcom/comscore/applications/AggregateMeasurement;

    .line 78
    .end local v0           #aggregateLabels:Ljava/util/List;,"Ljava/util/List<Lcom/comscore/measurement/Label;>;"
    :cond_2
    new-instance v2, Lcom/comscore/metrics/Request;

    invoke-direct {v2, p1}, Lcom/comscore/metrics/Request;-><init>(Lcom/comscore/measurement/Measurement;)V

    invoke-virtual {p0, v2}, Lcom/comscore/utils/Queue;->enqueue(Lcom/comscore/metrics/Request;)V

    goto :goto_0

    .line 73
    .restart local v0       #aggregateLabels:Ljava/util/List;,"Ljava/util/List<Lcom/comscore/measurement/Label;>;"
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/comscore/measurement/Label;

    .line 74
    .local v1, l:Lcom/comscore/measurement/Label;
    invoke-virtual {p1, v1}, Lcom/comscore/measurement/Measurement;->setLabel(Lcom/comscore/measurement/Label;)V

    goto :goto_1
.end method

.method public enqueue(Lcom/comscore/metrics/Request;)V
    .locals 0
    .parameter "r"

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/comscore/utils/Queue;->add(Ljava/lang/Object;)Z

    .line 56
    invoke-direct {p0}, Lcom/comscore/utils/Queue;->engage()V

    .line 57
    return-void
.end method

.method public getLast()Lcom/comscore/metrics/Request;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/comscore/utils/Queue;->last:Lcom/comscore/metrics/Request;

    return-object v0
.end method

.method public peek()Lcom/comscore/metrics/Request;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/comscore/utils/Queue;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 47
    invoke-super {p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/comscore/metrics/Request;

    .line 48
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/comscore/utils/Queue;->peek()Lcom/comscore/metrics/Request;

    move-result-object v0

    return-object v0
.end method

.method public process()V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/comscore/utils/Queue;->peek()Lcom/comscore/metrics/Request;

    move-result-object v0

    .line 94
    .local v0, req:Lcom/comscore/metrics/Request;
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Lcom/comscore/metrics/Request;->send()V

    .line 96
    :cond_0
    return-void
.end method
