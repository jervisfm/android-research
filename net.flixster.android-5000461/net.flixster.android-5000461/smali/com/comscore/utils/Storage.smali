.class public Lcom/comscore/utils/Storage;
.super Ljava/lang/Object;
.source "Storage.java"


# static fields
.field private static final PrefsName:Ljava/lang/String; = "cSPrefs"

.field private static instance:Lcom/comscore/utils/Storage;


# instance fields
.field public enabled:Ljava/lang/Boolean;

.field private prefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    sput-object v0, Lcom/comscore/utils/Storage;->instance:Lcom/comscore/utils/Storage;

    .line 8
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/comscore/utils/Storage;->enabled:Ljava/lang/Boolean;

    .line 17
    invoke-static {}, Lcom/comscore/analytics/DAx;->getInstance()Lcom/comscore/analytics/DAx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/comscore/analytics/DAx;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "cSPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/comscore/utils/Storage;->prefs:Landroid/content/SharedPreferences;

    .line 18
    return-void
.end method

.method public static getInstance()Lcom/comscore/utils/Storage;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/comscore/utils/Storage;->instance:Lcom/comscore/utils/Storage;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcom/comscore/utils/Storage;

    invoke-direct {v0}, Lcom/comscore/utils/Storage;-><init>()V

    sput-object v0, Lcom/comscore/utils/Storage;->instance:Lcom/comscore/utils/Storage;

    .line 23
    :cond_0
    sget-object v0, Lcom/comscore/utils/Storage;->instance:Lcom/comscore/utils/Storage;

    return-object v0
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "key"

    .prologue
    .line 34
    iget-object v0, p0, Lcom/comscore/utils/Storage;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/comscore/utils/Storage;->has(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/comscore/utils/Storage;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "default"

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public has(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1
    .parameter "key"

    .prologue
    .line 27
    iget-object v0, p0, Lcom/comscore/utils/Storage;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/comscore/utils/Storage;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 30
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 2
    .parameter "key"

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/comscore/utils/Storage;->has(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/comscore/utils/Storage;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 51
    .local v0, e:Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 52
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 54
    .end local v0           #e:Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public set(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter "key"
    .parameter "value"

    .prologue
    .line 41
    iget-object v1, p0, Lcom/comscore/utils/Storage;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    iget-object v1, p0, Lcom/comscore/utils/Storage;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 43
    .local v0, e:Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 44
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 46
    .end local v0           #e:Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method
