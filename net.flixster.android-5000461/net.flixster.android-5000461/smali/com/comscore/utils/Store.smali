.class public Lcom/comscore/utils/Store;
.super Ljava/lang/Object;
.source "Store.java"


# static fields
.field private static instance:Lcom/comscore/utils/Store;


# instance fields
.field private firstRun:Ljava/lang/Boolean;

.field private runs:Lcom/comscore/utils/StoreProp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x0

    sput-object v0, Lcom/comscore/utils/Store;->instance:Lcom/comscore/utils/Store;

    .line 3
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/comscore/utils/Store;->firstRun:Ljava/lang/Boolean;

    .line 11
    new-instance v0, Lcom/comscore/utils/StoreProp;

    const-string v1, "runs"

    invoke-direct {v0, v1}, Lcom/comscore/utils/StoreProp;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/comscore/utils/Store;->runs:Lcom/comscore/utils/StoreProp;

    .line 12
    return-void
.end method

.method public static getInstance()Lcom/comscore/utils/Store;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/comscore/utils/Store;->instance:Lcom/comscore/utils/Store;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/comscore/utils/Store;

    invoke-direct {v0}, Lcom/comscore/utils/Store;-><init>()V

    sput-object v0, Lcom/comscore/utils/Store;->instance:Lcom/comscore/utils/Store;

    .line 17
    :cond_0
    sget-object v0, Lcom/comscore/utils/Store;->instance:Lcom/comscore/utils/Store;

    return-object v0
.end method


# virtual methods
.method public getFirstRun()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/comscore/utils/Store;->firstRun:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getRuns()Lcom/comscore/utils/StoreProp;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/comscore/utils/Store;->runs:Lcom/comscore/utils/StoreProp;

    return-object v0
.end method

.method public setFirstRun(Ljava/lang/Boolean;)V
    .locals 0
    .parameter "firstRun"

    .prologue
    .line 25
    iput-object p1, p0, Lcom/comscore/utils/Store;->firstRun:Ljava/lang/Boolean;

    .line 26
    return-void
.end method
