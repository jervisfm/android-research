.class public Lcom/comscore/utils/StoreProp;
.super Ljava/lang/Object;
.source "StoreProp.java"


# instance fields
.field private name:Ljava/lang/String;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter "name"

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lcom/comscore/utils/StoreProp;->name:Ljava/lang/String;

    .line 9
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/comscore/utils/StoreProp;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 12
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v0

    iget-object v1, p0, Lcom/comscore/utils/StoreProp;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/comscore/utils/Storage;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 3
    .parameter "value"

    .prologue
    .line 16
    if-eqz p1, :cond_0

    .line 17
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18
    iput-object p1, p0, Lcom/comscore/utils/StoreProp;->value:Ljava/lang/String;

    .line 19
    invoke-static {}, Lcom/comscore/utils/Storage;->getInstance()Lcom/comscore/utils/Storage;

    move-result-object v0

    iget-object v1, p0, Lcom/comscore/utils/StoreProp;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/comscore/utils/StoreProp;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/comscore/utils/Storage;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    :cond_0
    return-void
.end method
