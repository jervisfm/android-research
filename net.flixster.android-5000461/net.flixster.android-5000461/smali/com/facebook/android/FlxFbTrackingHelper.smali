.class public Lcom/facebook/android/FlxFbTrackingHelper;
.super Ljava/lang/Object;
.source "FlxFbTrackingHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static trackSsoLogin()V
    .locals 3

    .prologue
    .line 15
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/facebook/login/sso"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method protected static trackSsoLoginSuccess()V
    .locals 3

    .prologue
    .line 19
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/facebook/success/sso"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method protected static trackWebLogin()V
    .locals 3

    .prologue
    .line 7
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/facebook/login/web"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    return-void
.end method

.method protected static trackWebLoginSuccess()V
    .locals 3

    .prologue
    .line 11
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/facebook/success/web"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    return-void
.end method
