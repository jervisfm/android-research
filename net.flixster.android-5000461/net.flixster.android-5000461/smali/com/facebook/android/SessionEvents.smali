.class public Lcom/facebook/android/SessionEvents;
.super Ljava/lang/Object;
.source "SessionEvents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/android/SessionEvents$AuthListener;,
        Lcom/facebook/android/SessionEvents$LogoutListener;
    }
.end annotation


# static fields
.field private static mAuthListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/android/SessionEvents$AuthListener;",
            ">;"
        }
    .end annotation
.end field

.field private static mLogoutListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/android/SessionEvents$LogoutListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/facebook/android/SessionEvents;->mAuthListeners:Ljava/util/LinkedList;

    .line 21
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/facebook/android/SessionEvents;->mLogoutListeners:Ljava/util/LinkedList;

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addAuthListener(Lcom/facebook/android/SessionEvents$AuthListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 30
    sget-object v0, Lcom/facebook/android/SessionEvents;->mAuthListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 31
    return-void
.end method

.method public static addLogoutListener(Lcom/facebook/android/SessionEvents$LogoutListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 49
    sget-object v0, Lcom/facebook/android/SessionEvents;->mLogoutListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public static onLoginError(Ljava/lang/String;)V
    .locals 3
    .parameter "error"

    .prologue
    .line 68
    sget-object v1, Lcom/facebook/android/SessionEvents;->mAuthListeners:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 71
    return-void

    .line 68
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/SessionEvents$AuthListener;

    .line 69
    .local v0, listener:Lcom/facebook/android/SessionEvents$AuthListener;
    invoke-interface {v0, p0}, Lcom/facebook/android/SessionEvents$AuthListener;->onAuthFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static onLoginSuccess()V
    .locals 3

    .prologue
    .line 62
    sget-object v1, Lcom/facebook/android/SessionEvents;->mAuthListeners:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, listener:Lcom/facebook/android/SessionEvents$AuthListener;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 65
    return-void

    .line 62
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #listener:Lcom/facebook/android/SessionEvents$AuthListener;
    check-cast v0, Lcom/facebook/android/SessionEvents$AuthListener;

    .line 63
    .restart local v0       #listener:Lcom/facebook/android/SessionEvents$AuthListener;
    invoke-interface {v0}, Lcom/facebook/android/SessionEvents$AuthListener;->onAuthSucceed()V

    goto :goto_0
.end method

.method public static onLogoutBegin()V
    .locals 3

    .prologue
    .line 74
    sget-object v1, Lcom/facebook/android/SessionEvents;->mLogoutListeners:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, l:Lcom/facebook/android/SessionEvents$LogoutListener;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    return-void

    .line 74
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #l:Lcom/facebook/android/SessionEvents$LogoutListener;
    check-cast v0, Lcom/facebook/android/SessionEvents$LogoutListener;

    .line 75
    .restart local v0       #l:Lcom/facebook/android/SessionEvents$LogoutListener;
    invoke-interface {v0}, Lcom/facebook/android/SessionEvents$LogoutListener;->onLogoutBegin()V

    goto :goto_0
.end method

.method public static onLogoutFinish()V
    .locals 3

    .prologue
    .line 80
    sget-object v1, Lcom/facebook/android/SessionEvents;->mLogoutListeners:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, l:Lcom/facebook/android/SessionEvents$LogoutListener;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 83
    return-void

    .line 80
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #l:Lcom/facebook/android/SessionEvents$LogoutListener;
    check-cast v0, Lcom/facebook/android/SessionEvents$LogoutListener;

    .line 81
    .restart local v0       #l:Lcom/facebook/android/SessionEvents$LogoutListener;
    invoke-interface {v0}, Lcom/facebook/android/SessionEvents$LogoutListener;->onLogoutFinish()V

    goto :goto_0
.end method

.method public static removeAuthListener(Lcom/facebook/android/SessionEvents$AuthListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 39
    sget-object v0, Lcom/facebook/android/SessionEvents;->mAuthListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method

.method public static removeLogoutListener(Lcom/facebook/android/SessionEvents$LogoutListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 58
    sget-object v0, Lcom/facebook/android/SessionEvents;->mLogoutListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 59
    return-void
.end method
