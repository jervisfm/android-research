.class Lcom/fiksu/asotracking/PurchaseEventTracker;
.super Lcom/fiksu/asotracking/EventTracker;
.source "PurchaseEventTracker.java"


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/String;)V
    .locals 2
    .parameter "context"
    .parameter "username"
    .parameter "price"
    .parameter "currency"

    .prologue
    .line 15
    const-string v0, "Purchase"

    invoke-direct {p0, p1, v0}, Lcom/fiksu/asotracking/EventTracker;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 16
    const-string v0, "username"

    invoke-virtual {p0, v0, p2}, Lcom/fiksu/asotracking/PurchaseEventTracker;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v0, "fvalue"

    invoke-virtual {p3}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/fiksu/asotracking/PurchaseEventTracker;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    const-string v0, "tvalue"

    invoke-virtual {p0, v0, p4}, Lcom/fiksu/asotracking/PurchaseEventTracker;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    return-void
.end method
