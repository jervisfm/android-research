.class public Lcom/flixster/android/activity/DeepLink;
.super Ljava/lang/Object;
.source "DeepLink.java"


# static fields
.field private static final DEEP_LINK_ACTOR_QUERY:Ljava/lang/String; = "actor"

.field private static final DEEP_LINK_AUTO_LOGIN_PAGE:Ljava/lang/String; = "loginFlixsterUser"

.field private static final DEEP_LINK_EXTRA_MOVIES:Ljava/lang/String; = "extramovies"

.field private static final DEEP_LINK_FLX_AUTH_TOKEN_QUERY:Ljava/lang/String; = "auth_token"

.field private static final DEEP_LINK_FLX_AUTH_USER_QUERY:Ljava/lang/String; = "auth_user"

.field public static final DEEP_LINK_LASP_PAGE:Ljava/lang/String; = "lasp"

.field private static final DEEP_LINK_MOVIE_DETAIL_PAGE:Ljava/lang/String; = "detail"

.field private static final DEEP_LINK_MOVIE_QUERY:Ljava/lang/String; = "movie"

.field private static final DEEP_LINK_MSK_ACTION_QUERY:Ljava/lang/String; = "action"

.field private static final DEEP_LINK_MSK_FB_LOGIN_PAGE:Ljava/lang/String; = "fbLogin"

.field private static final DEEP_LINK_MSK_FB_REQUEST_PAGE:Ljava/lang/String; = "fbRequest"

.field private static final DEEP_LINK_MSK_LOGIN_REQUIRED_QUERY:Ljava/lang/String; = "loginRequired"

.field public static final DEEP_LINK_MSK_PAGE:Ljava/lang/String; = "msk"

.field private static final DEEP_LINK_MSK_PROPERTY_PAGE:Ljava/lang/String; = "property"

.field private static final DEEP_LINK_MSK_TEXT_REQUEST_PAGE:Ljava/lang/String; = "textRequest"

.field private static final DEEP_LINK_MSK_TYPE_QUERY:Ljava/lang/String; = "type"

.field private static final DEEP_LINK_PAGE_QUERY:Ljava/lang/String; = "page"

.field private static final DEEP_LINK_PHOTOS_PAGE:Ljava/lang/String; = "photos"

.field private static final DEEP_LINK_PRODUCT_PAGE:Ljava/lang/String; = "product"

.field private static final DEEP_LINK_TRAILER_PAGE:Ljava/lang/String; = "trailer"

.field private static final DEEP_LINK_URL:Ljava/lang/String; = "http://www.flixster.com/mobile/deep-link"

.field private static final DEEP_LINK_URL_QUERY:Ljava/lang/String; = "url"

.field private static final DEEP_LINK_USERNAME_QUERY:Ljava/lang/String; = "username"

.field private static final DEEP_LINK_VALUE_MSK_LOGIN_FB:Ljava/lang/String; = "fb"

.field private static final DEEP_LINK_VALUE_MSK_LOGIN_FLX:Ljava/lang/String; = "flx"

.field private static final DEEP_LINK_VALUE_MSK_TYPE_PICK:Ljava/lang/String; = "pick"

.field private static final FLIXSTER_SCHEME:Ljava/lang/String; = "flixster"

.field private static final HOST_MOVIE:Ljava/lang/String; = "movie"

.field private static final HOST_MSK:Ljava/lang/String; = "msk"

.field private static final HOST_MYMOVIES:Ljava/lang/String; = "mymovies"

.field private static final HOST_SHOWTIMES:Ljava/lang/String; = "showtimes"

.field private static final HOST_TRAILER:Ljava/lang/String; = "trailer"

.field private static final QUERY_ID:Ljava/lang/String; = "id"

.field private static final QUERY_REASON:Ljava/lang/String; = "reason"

.field private static final QUERY_URL:Ljava/lang/String; = "url"

.field private static final VALUE_MSK:Ljava/lang/String; = "mskFinished"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkMskFinished(Landroid/net/Uri;)V
    .locals 2
    .parameter "uri"

    .prologue
    .line 282
    if-eqz p0, :cond_0

    .line 283
    invoke-static {p0}, Lcom/flixster/android/activity/DeepLink;->convertDeepLinkToAndroid(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p0

    .line 284
    const-string v0, "flixster"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "mymovies"

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    const-string v0, "mskFinished"

    const-string v1, "reason"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->setMskFinishedDeepLinkInitiated()V

    .line 287
    const-string v0, "FlxMain"

    const-string v1, "DeepLink.checkMskFinished true"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_0
    return-void
.end method

.method public static clearDeepLinkParams()V
    .locals 2

    .prologue
    .line 245
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const-string v1, "lasp"

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 246
    return-void
.end method

.method private static convertDeepLinkToAndroid(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 8
    .parameter "iosUri"

    .prologue
    .line 354
    move-object v1, p0

    .line 355
    .local v1, androidUri:Landroid/net/Uri;
    const-string v5, "action"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 356
    .local v0, action:Ljava/lang/String;
    const-string v5, "movieDetails"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 357
    const-string v5, "movieId"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 358
    .local v2, id:Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 359
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "flixster://movie?id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 383
    .end local v2           #id:Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 361
    :cond_1
    const-string v5, "movieShowtimes"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 362
    const-string v5, "movieId"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 363
    .restart local v2       #id:Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 364
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "flixster://showtimes?id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 366
    .end local v2           #id:Ljava/lang/String;
    :cond_2
    const-string v5, "playTrailer"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 367
    const-string v5, "highUrl"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 368
    .local v4, url:Ljava/lang/String;
    if-nez v4, :cond_3

    const-string v5, "lowUrl"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 369
    :cond_3
    if-eqz v4, :cond_4

    .line 370
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "flixster://trailer?url="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 372
    :cond_4
    const-string v5, "movieId"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 373
    .restart local v2       #id:Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 374
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "flixster://trailer?id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 377
    .end local v2           #id:Ljava/lang/String;
    .end local v4           #url:Ljava/lang/String;
    :cond_5
    const-string v5, "myMovies"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 378
    const-string v5, "reason"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 379
    .local v3, reason:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v5, "flixster://mymovies"

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_6

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "?reason="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_0

    :cond_6
    const-string v5, ""

    goto :goto_1

    .line 380
    .end local v3           #reason:Ljava/lang/String;
    :cond_7
    const-string v5, "msk"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 381
    const-string v5, "flixster://msk"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public static convertToMskNativeStep(Ljava/lang/String;)Lcom/flixster/android/msk/MskController$Step;
    .locals 3
    .parameter "url"

    .prologue
    .line 187
    const/4 v1, 0x0

    .line 188
    .local v1, step:Lcom/flixster/android/msk/MskController$Step;
    const-string v2, "http://www.flixster.com/mobile/deep-link"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 189
    const-string v2, "page"

    invoke-static {p0, v2}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, page:Ljava/lang/String;
    const-string v2, "fbLogin"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 191
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    .line 200
    .end local v0           #page:Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 192
    .restart local v0       #page:Ljava/lang/String;
    :cond_1
    const-string v2, "fbRequest"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 193
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    goto :goto_0

    .line 194
    :cond_2
    const-string v2, "textRequest"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 195
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_TEXT_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    goto :goto_0

    .line 196
    :cond_3
    const-string v2, "lasp"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 197
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_LASP:Lcom/flixster/android/msk/MskController$Step;

    goto :goto_0
.end method

.method public static convertToPropertyName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "url"

    .prologue
    .line 175
    const/4 v0, 0x0

    .line 176
    .local v0, name:Ljava/lang/String;
    const-string v2, "http://www.flixster.com/mobile/deep-link"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177
    const-string v2, "page"

    invoke-static {p0, v2}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, page:Ljava/lang/String;
    const-string v2, "property"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    const-string v2, "action"

    invoke-static {p0, v2}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 182
    .end local v1           #page:Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static getExtraMovies(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "url"

    .prologue
    .line 215
    const/4 v0, 0x0

    .line 216
    .local v0, extraMovies:Ljava/lang/String;
    const-string v1, "http://www.flixster.com/mobile/deep-link"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    const-string v1, "extramovies"

    invoke-static {p0, v1}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    :cond_0
    return-object v0
.end method

.method public static getMovieId(Ljava/lang/String;)J
    .locals 4
    .parameter "url"

    .prologue
    .line 204
    const-wide/16 v0, 0x0

    .line 205
    .local v0, id:J
    const-string v3, "http://www.flixster.com/mobile/deep-link"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 206
    const-string v3, "movie"

    invoke-static {p0, v3}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 207
    .local v2, value:Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 208
    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 211
    .end local v2           #value:Ljava/lang/String;
    :cond_0
    return-wide v0
.end method

.method public static isFacebookLoginRequired(Ljava/lang/String;)Z
    .locals 2
    .parameter "url"

    .prologue
    .line 230
    const-string v1, "loginRequired"

    invoke-static {p0, v1}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    .local v0, login:Ljava/lang/String;
    const-string v1, "fb"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isFlixsterDeepLink(Landroid/net/Uri;)Z
    .locals 4
    .parameter "uri"

    .prologue
    .line 276
    if-eqz p0, :cond_0

    const-string v1, "flixster"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 277
    .local v0, result:Z
    :goto_0
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DeepLink.isFlixsterDeepLink uri: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    return v0

    .line 276
    .end local v0           #result:Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFlixsterLoginRequired(Ljava/lang/String;)Z
    .locals 2
    .parameter "url"

    .prologue
    .line 235
    const-string v1, "loginRequired"

    invoke-static {p0, v1}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, login:Ljava/lang/String;
    const-string v1, "flx"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isMskAdUrl(Ljava/lang/String;)Z
    .locals 2
    .parameter "url"

    .prologue
    .line 169
    const-string v1, "page"

    invoke-static {p0, v1}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, page:Ljava/lang/String;
    const-string v1, "msk"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isMskRequestTypePick(Ljava/lang/String;)Z
    .locals 4
    .parameter "url"

    .prologue
    .line 224
    const-string v1, "type"

    invoke-static {p0, v1}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, type:Ljava/lang/String;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DeepLink.isRequestTypePick "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v1, "pick"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isValid(Ljava/lang/String;)Z
    .locals 4
    .parameter "url"

    .prologue
    .line 78
    const-string v1, "http://www.flixster.com/mobile/deep-link"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 79
    .local v0, result:Z
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DeepLink "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return v0
.end method

.method public static launch(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 14
    .parameter "url"
    .parameter "context"

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 85
    const-string v9, "http://www.flixster.com/mobile/deep-link"

    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 86
    const-string v9, "page"

    invoke-static {p0, v9}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, page:Ljava/lang/String;
    if-eqz v4, :cond_11

    .line 88
    const-string v9, "trailer"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 89
    const-string v7, "url"

    invoke-static {p0, v7}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 90
    .local v6, trailerUrl:Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 91
    invoke-static {v6, p1}, Lnet/flixster/android/Starter;->launchAdFreeTrailer(Ljava/lang/String;Landroid/content/Context;)V

    .end local v4           #page:Ljava/lang/String;
    .end local v6           #trailerUrl:Ljava/lang/String;
    :cond_0
    :goto_0
    move v7, v8

    .line 162
    .end local p1
    :cond_1
    :goto_1
    return v7

    .line 93
    .restart local v4       #page:Ljava/lang/String;
    .restart local v6       #trailerUrl:Ljava/lang/String;
    .restart local p1
    :cond_2
    const-string v7, "FlxMain"

    const-string v9, "DeepLink error: null trailer URL"

    invoke-static {v7, v9}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    .end local v6           #trailerUrl:Ljava/lang/String;
    :cond_3
    const-string v9, "detail"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 96
    const-string v7, "movie"

    invoke-static {p0, v7}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 97
    .local v3, movieId:Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 98
    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-static {v9, v10, p1}, Lnet/flixster/android/Starter;->launchMovieDetail(JLandroid/content/Context;)V

    goto :goto_0

    .line 100
    :cond_4
    const-string v7, "FlxMain"

    const-string v9, "DeepLink error: null movie id"

    invoke-static {v7, v9}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 102
    .end local v3           #movieId:Ljava/lang/String;
    :cond_5
    const-string v9, "photos"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 103
    const/4 v2, 0x0

    .line 104
    .local v2, id:Ljava/lang/String;
    const-string v7, "movie"

    invoke-static {p0, v7}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 105
    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-static {v9, v10, p1}, Lnet/flixster/android/Starter;->launchMovieGallery(JLandroid/content/Context;)V

    goto :goto_0

    .line 106
    :cond_6
    const-string v7, "actor"

    invoke-static {p0, v7}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 107
    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-static {v9, v10, p1}, Lnet/flixster/android/Starter;->launchActorGallery(JLandroid/content/Context;)V

    goto :goto_0

    .line 108
    :cond_7
    const-string v7, "username"

    invoke-static {p0, v7}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 109
    invoke-static {v2, p1}, Lnet/flixster/android/Starter;->launchUserGallery(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 111
    :cond_8
    const-string v7, "FlxMain"

    const-string v9, "DeepLink error: null photos id"

    invoke-static {v7, v9}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 113
    .end local v2           #id:Ljava/lang/String;
    :cond_9
    const-string v9, "product"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 114
    const-string v7, "url"

    invoke-static {p0, v7}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 115
    .local v5, productUrl:Ljava/lang/String;
    if-eqz v5, :cond_a

    .line 116
    invoke-static {v5, p1}, Lnet/flixster/android/Starter;->launchBrowser(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 118
    :cond_a
    const-string v7, "FlxMain"

    const-string v9, "DeepLink error: null product URL"

    invoke-static {v7, v9}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    .end local v5           #productUrl:Ljava/lang/String;
    :cond_b
    const-string v9, "property"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 121
    instance-of v8, p1, Landroid/app/Activity;

    if-eqz v8, :cond_1

    .line 122
    check-cast p1, Landroid/app/Activity;

    .end local p1
    const/4 v8, -0x1

    .line 123
    new-instance v9, Landroid/content/Intent;

    const-string v10, "android.intent.action.VIEW"

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 122
    invoke-virtual {p1, v8, v9}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 126
    .restart local p1
    :cond_c
    const-string v9, "lasp"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 127
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v9

    const-string v10, "/MSKRedemption"

    const-string v11, "MSK Redemption"

    const-string v12, "MSKRedemption"

    .line 128
    const-string v13, "SuccessfulRedemption"

    .line 127
    invoke-interface {v9, v10, v11, v12, v13}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v9

    if-nez v9, :cond_d

    .line 131
    const-string v9, "auth_user"

    invoke-static {p0, v9}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 132
    .local v1, authUser:Ljava/lang/String;
    const-string v9, "auth_token"

    invoke-static {p0, v9}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, authToken:Ljava/lang/String;
    if-eqz v1, :cond_d

    if-eqz v0, :cond_d

    .line 134
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v9

    invoke-virtual {v9, v1, v0}, Lcom/flixster/android/data/AccountManager;->createNewUserWithFlxCredential(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v9

    iget v10, v9, Lnet/flixster/android/model/User;->collectionsCount:I

    add-int/lit8 v10, v10, 0x1

    iput v10, v9, Lnet/flixster/android/model/User;->collectionsCount:I

    .line 136
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v9

    iput-boolean v8, v9, Lnet/flixster/android/model/User;->isMskEligible:Z

    .line 140
    .end local v0           #authToken:Ljava/lang/String;
    .end local v1           #authUser:Ljava/lang/String;
    :cond_d
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/flixster/android/msk/MskController;->finishLasp(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 142
    :cond_e
    const-string v9, "loginFlixsterUser"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 143
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v8

    if-nez v8, :cond_1

    .line 144
    const-string v8, "auth_user"

    invoke-static {p0, v8}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 145
    .restart local v1       #authUser:Ljava/lang/String;
    const-string v8, "auth_token"

    invoke-static {p0, v8}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    .restart local v0       #authToken:Ljava/lang/String;
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 147
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v8

    invoke-virtual {v8, v1, v0}, Lcom/flixster/android/data/AccountManager;->createNewUserWithFlxCredential(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 151
    .end local v0           #authToken:Ljava/lang/String;
    .end local v1           #authUser:Ljava/lang/String;
    :cond_f
    const-string v9, "msk"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 153
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v9

    const-string v10, "lasp"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v9, v10, v7}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 154
    invoke-static {p1}, Lnet/flixster/android/Starter;->launchMsk(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 156
    :cond_10
    const-string v7, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "DeepLink error: unknown page type \'"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 159
    :cond_11
    const-string v7, "FlxMain"

    const-string v9, "DeepLink error: null page query"

    invoke-static {v7, v9}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static launchFlixsterDeepLink(Landroid/net/Uri;Landroid/content/Context;)V
    .locals 8
    .parameter "uri"
    .parameter "context"

    .prologue
    .line 295
    invoke-static {p0}, Lcom/flixster/android/activity/DeepLink;->convertDeepLinkToAndroid(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p0

    .line 296
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, host:Ljava/lang/String;
    const-string v5, "movie"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 298
    const-string v5, "id"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 299
    .local v1, id:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 300
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6, p1}, Lnet/flixster/android/Starter;->launchMovieDetail(JLandroid/content/Context;)V

    .line 335
    .end local v1           #id:Ljava/lang/String;
    .end local p1
    :cond_0
    :goto_0
    return-void

    .line 302
    .restart local p1
    :cond_1
    const-string v5, "showtimes"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 303
    const-string v5, "id"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 304
    .restart local v1       #id:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 305
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6, p1}, Lnet/flixster/android/Starter;->launchMovieShowtimes(JLandroid/content/Context;)V

    goto :goto_0

    .line 307
    .end local v1           #id:Ljava/lang/String;
    :cond_2
    const-string v5, "trailer"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 308
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 309
    .local v2, uriString:Ljava/lang/String;
    const-string v5, "url"

    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 310
    .local v4, urlIndex:I
    const/4 v5, -0x1

    if-le v4, v5, :cond_3

    .line 312
    const-string v5, "url"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v4

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 313
    .local v3, url:Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 314
    invoke-static {v3}, Lcom/flixster/android/utils/UrlHelper;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Lnet/flixster/android/Starter;->launchAdFreeTrailer(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 317
    .end local v3           #url:Ljava/lang/String;
    :cond_3
    const-string v5, "id"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 318
    .restart local v1       #id:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 319
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6, p1}, Lnet/flixster/android/Starter;->launchAdFreeTrailer(JLandroid/content/Context;)V

    goto :goto_0

    .line 322
    .end local v1           #id:Ljava/lang/String;
    .end local v2           #uriString:Ljava/lang/String;
    .end local v4           #urlIndex:I
    :cond_4
    const-string v5, "mymovies"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 323
    const-string v5, "mskFinished"

    const-string v6, "reason"

    invoke-virtual {p0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 326
    instance-of v5, p1, Lnet/flixster/android/Flixster;

    if-eqz v5, :cond_0

    .line 327
    check-cast p1, Lnet/flixster/android/Flixster;

    .end local p1
    invoke-virtual {p1}, Lnet/flixster/android/Flixster;->getTabHost()Landroid/widget/TabHost;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0

    .line 329
    .restart local p1
    :cond_5
    const-string v5, "msk"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 332
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v5

    const-string v6, "lasp"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 333
    invoke-static {p1}, Lnet/flixster/android/Starter;->launchMsk(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method public static removeLoginRequired(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "url"

    .prologue
    .line 240
    const-string v0, "loginRequired"

    const-string v1, "ignore"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
