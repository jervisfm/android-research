.class public Lcom/flixster/android/activity/DialogActivity;
.super Lcom/flixster/android/activity/TopLevelActivity;
.source "DialogActivity.java"


# instance fields
.field private lastDialog:Landroid/app/Dialog;

.field private lastDialogId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/flixster/android/activity/TopLevelActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .parameter "id"

    .prologue
    .line 33
    iput p1, p0, Lcom/flixster/android/activity/DialogActivity;->lastDialogId:I

    .line 34
    invoke-static {p0, p1}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/DialogActivity;->lastDialog:Landroid/app/Dialog;

    .line 35
    iget-object v0, p0, Lcom/flixster/android/activity/DialogActivity;->lastDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 36
    invoke-super {p0, p1}, Lcom/flixster/android/activity/TopLevelActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/DialogActivity;->lastDialog:Landroid/app/Dialog;

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/DialogActivity;->lastDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method public showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V
    .locals 1
    .parameter "id"
    .parameter "listener"

    .prologue
    .line 20
    invoke-static {}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->instance()Lcom/flixster/android/view/DialogBuilder$DialogEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->removeListeners()V

    .line 21
    if-eqz p2, :cond_0

    .line 22
    invoke-static {}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->instance()Lcom/flixster/android/view/DialogBuilder$DialogEvents;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->addListener(Lcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/DialogActivity;->lastDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 25
    iget v0, p0, Lcom/flixster/android/activity/DialogActivity;->lastDialogId:I

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/DialogActivity;->removeDialog(I)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/activity/DialogActivity;->lastDialog:Landroid/app/Dialog;

    .line 28
    :cond_1
    invoke-virtual {p0, p1}, Lcom/flixster/android/activity/DialogActivity;->showDialog(I)V

    .line 29
    return-void
.end method
