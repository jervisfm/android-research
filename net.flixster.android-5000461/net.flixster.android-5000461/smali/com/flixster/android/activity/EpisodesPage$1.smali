.class Lcom/flixster/android/activity/EpisodesPage$1;
.super Landroid/os/Handler;
.source "EpisodesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/EpisodesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/EpisodesPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage$1;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    .line 86
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "msg"

    .prologue
    .line 88
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage$1;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/flixster/android/activity/EpisodesPage;->access$0(Lcom/flixster/android/activity/EpisodesPage;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 89
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$1;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/model/Season;

    #setter for: Lcom/flixster/android/activity/EpisodesPage;->season:Lnet/flixster/android/model/Season;
    invoke-static {v1, v0}, Lcom/flixster/android/activity/EpisodesPage;->access$1(Lcom/flixster/android/activity/EpisodesPage;Lnet/flixster/android/model/Season;)V

    .line 90
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage$1;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->seasonRight:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lcom/flixster/android/activity/EpisodesPage;->access$2(Lcom/flixster/android/activity/EpisodesPage;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage$1;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #calls: Lcom/flixster/android/activity/EpisodesPage;->trackPage()V
    invoke-static {v0}, Lcom/flixster/android/activity/EpisodesPage;->access$3(Lcom/flixster/android/activity/EpisodesPage;)V

    .line 92
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage$1;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #calls: Lcom/flixster/android/activity/EpisodesPage;->initializePage()V
    invoke-static {v0}, Lcom/flixster/android/activity/EpisodesPage;->access$4(Lcom/flixster/android/activity/EpisodesPage;)V

    .line 94
    :cond_0
    return-void
.end method
