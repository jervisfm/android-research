.class Lcom/flixster/android/activity/EpisodesPage$11;
.super Landroid/os/Handler;
.source "EpisodesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/EpisodesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/EpisodesPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage$11;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    .line 282
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 284
    iget-object v4, p0, Lcom/flixster/android/activity/EpisodesPage$11;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    invoke-virtual {v4}, Lcom/flixster/android/activity/EpisodesPage;->isFinishing()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 292
    :cond_0
    return-void

    .line 287
    :cond_1
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 288
    .local v0, assetId:J
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    iget-object v4, p0, Lcom/flixster/android/activity/EpisodesPage$11;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->contentLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/flixster/android/activity/EpisodesPage;->access$12(Lcom/flixster/android/activity/EpisodesPage;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 289
    iget-object v4, p0, Lcom/flixster/android/activity/EpisodesPage$11;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->contentLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/flixster/android/activity/EpisodesPage;->access$12(Lcom/flixster/android/activity/EpisodesPage;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/view/EpisodeView;

    .line 290
    .local v3, view:Lcom/flixster/android/view/EpisodeView;
    invoke-virtual {v3, v0, v1}, Lcom/flixster/android/view/EpisodeView;->refresh(J)V

    .line 288
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
