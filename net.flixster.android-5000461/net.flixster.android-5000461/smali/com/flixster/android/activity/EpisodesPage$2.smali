.class Lcom/flixster/android/activity/EpisodesPage$2;
.super Landroid/os/Handler;
.source "EpisodesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/EpisodesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/EpisodesPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage$2;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    .line 97
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "msg"

    .prologue
    .line 99
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage$2;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/flixster/android/activity/EpisodesPage;->access$0(Lcom/flixster/android/activity/EpisodesPage;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 100
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lnet/flixster/android/data/DaoException;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/data/DaoException;

    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$2;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedSherlockActivity;)V

    .line 103
    :cond_0
    return-void
.end method
