.class Lcom/flixster/android/activity/EpisodesPage$3;
.super Ljava/lang/Object;
.source "EpisodesPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/EpisodesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/EpisodesPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage$3;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    const v4, 0x3b9acb2c

    .line 120
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 121
    .local v0, epRight:Lnet/flixster/android/model/LockerRight;
    iget-wide v2, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v2, v3}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    iget-wide v2, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v2, v3}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 123
    .local v1, isLocalPlayback:Z
    :goto_0
    if-nez v1, :cond_1

    invoke-static {}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 124
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 125
    iget-object v2, p0, Lcom/flixster/android/activity/EpisodesPage$3;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    iget-object v3, p0, Lcom/flixster/android/activity/EpisodesPage$3;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v3}, Lcom/flixster/android/activity/EpisodesPage;->access$5(Lcom/flixster/android/activity/EpisodesPage;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/flixster/android/activity/EpisodesPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 129
    :goto_1
    return-void

    .line 122
    .end local v1           #isLocalPlayback:Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 127
    .restart local v1       #isLocalPlayback:Z
    :cond_1
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/flixster/android/drm/PlaybackManager;->playMovie(Lnet/flixster/android/model/LockerRight;)V

    goto :goto_1
.end method
