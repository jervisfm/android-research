.class Lcom/flixster/android/activity/EpisodesPage$5;
.super Ljava/lang/Object;
.source "EpisodesPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/EpisodesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/EpisodesPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage$5;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "view"

    .prologue
    const/4 v6, 0x0

    const v5, 0x3b9acac8

    .line 154
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 155
    .local v0, epRight:Lnet/flixster/android/model/LockerRight;
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 160
    invoke-static {}, Lcom/flixster/android/storage/ExternalStorage;->isWriteable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 161
    invoke-static {}, Lcom/flixster/android/net/DownloadHelper;->findRemainingSpace()J

    move-result-wide v1

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetSizeRaw()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    .line 162
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 163
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$5;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    iget-object v2, p0, Lcom/flixster/android/activity/EpisodesPage$5;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v2}, Lcom/flixster/android/activity/EpisodesPage;->access$6(Lcom/flixster/android/activity/EpisodesPage;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Lcom/flixster/android/activity/EpisodesPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$5;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    const v2, 0x3b9acacd

    invoke-virtual {v1, v2, v6}, Lcom/flixster/android/activity/EpisodesPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 168
    :cond_2
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$5;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    const v2, 0x3b9acacb

    invoke-virtual {v1, v2, v6}, Lcom/flixster/android/activity/EpisodesPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0
.end method
