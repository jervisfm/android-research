.class Lcom/flixster/android/activity/EpisodesPage$6;
.super Landroid/os/Handler;
.source "EpisodesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/EpisodesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/EpisodesPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage$6;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    .line 174
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 176
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 177
    .local v0, epRight:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->getDownloadsRemain()I

    move-result v1

    if-lez v1, :cond_0

    .line 178
    invoke-static {v0}, Lcom/flixster/android/net/DownloadHelper;->downloadMovie(Lnet/flixster/android/model/LockerRight;)V

    .line 179
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$6;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    iget-wide v2, v0, Lnet/flixster/android/model/LockerRight;->assetId:J

    #calls: Lcom/flixster/android/activity/EpisodesPage;->scheduleSingleRefresh(J)V
    invoke-static {v1, v2, v3}, Lcom/flixster/android/activity/EpisodesPage;->access$7(Lcom/flixster/android/activity/EpisodesPage;J)V

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$6;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    const v2, 0x3b9acacc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/flixster/android/activity/EpisodesPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0
.end method
