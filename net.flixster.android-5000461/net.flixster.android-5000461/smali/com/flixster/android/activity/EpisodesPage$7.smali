.class Lcom/flixster/android/activity/EpisodesPage$7;
.super Ljava/lang/Object;
.source "EpisodesPage.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/EpisodesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/EpisodesPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage$7;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 202
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acac8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 203
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 197
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acac8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 198
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 6
    .parameter "which"

    .prologue
    .line 189
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v1

    .line 190
    const v2, 0x3b9acac8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 189
    invoke-virtual {v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 191
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/download"

    const-string v3, "Download"

    const-string v4, "Download"

    const-string v5, "Confirm"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$7;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->canDownloadSuccessHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/activity/EpisodesPage;->access$8(Lcom/flixster/android/activity/EpisodesPage;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/EpisodesPage$7;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->errorHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/EpisodesPage;->access$9(Lcom/flixster/android/activity/EpisodesPage;)Landroid/os/Handler;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lnet/flixster/android/data/ProfileDao;->canDownload(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V

    .line 193
    return-void
.end method
