.class Lcom/flixster/android/activity/EpisodesPage$8;
.super Ljava/lang/Object;
.source "EpisodesPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/EpisodesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/EpisodesPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage$8;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    const v4, 0x3b9acaca

    const v3, 0x3b9acac9

    .line 209
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 210
    .local v0, epRight:Lnet/flixster/android/model/LockerRight;
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 211
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 212
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$8;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    iget-object v2, p0, Lcom/flixster/android/activity/EpisodesPage$8;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v2}, Lcom/flixster/android/activity/EpisodesPage;->access$10(Lcom/flixster/android/activity/EpisodesPage;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/flixster/android/activity/EpisodesPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 215
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$8;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    iget-object v2, p0, Lcom/flixster/android/activity/EpisodesPage$8;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v2}, Lcom/flixster/android/activity/EpisodesPage;->access$11(Lcom/flixster/android/activity/EpisodesPage;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/flixster/android/activity/EpisodesPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0
.end method
