.class Lcom/flixster/android/activity/EpisodesPage$9;
.super Ljava/lang/Object;
.source "EpisodesPage.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/EpisodesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/EpisodesPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage$9;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 239
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acac9

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 240
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 234
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acac9

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 235
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 6
    .parameter "which"

    .prologue
    .line 225
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v1

    .line 226
    const v2, 0x3b9acac9

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 225
    invoke-virtual {v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 227
    .local v0, epRight:Lnet/flixster/android/model/LockerRight;
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->cancelMovieDownload(J)V

    .line 228
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage$9;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    iget-wide v2, v0, Lnet/flixster/android/model/LockerRight;->assetId:J

    #calls: Lcom/flixster/android/activity/EpisodesPage;->scheduleSingleRefresh(J)V
    invoke-static {v1, v2, v3}, Lcom/flixster/android/activity/EpisodesPage;->access$7(Lcom/flixster/android/activity/EpisodesPage;J)V

    .line 229
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/download"

    const-string v3, "Download"

    const-string v4, "Download"

    const-string v5, "Cancel"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    return-void
.end method
