.class Lcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;
.super Ljava/util/TimerTask;
.source "EpisodesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/EpisodesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshTimerTask"
.end annotation


# instance fields
.field private final assetId:J

.field final synthetic this$0:Lcom/flixster/android/activity/EpisodesPage;


# direct methods
.method private constructor <init>(Lcom/flixster/android/activity/EpisodesPage;J)V
    .locals 0
    .parameter
    .parameter "assetId"

    .prologue
    .line 272
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 273
    iput-wide p2, p0, Lcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;->assetId:J

    .line 274
    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/activity/EpisodesPage;JLcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 272
    invoke-direct {p0, p1, p2, p3}, Lcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;-><init>(Lcom/flixster/android/activity/EpisodesPage;J)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 278
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;->this$0:Lcom/flixster/android/activity/EpisodesPage;

    #getter for: Lcom/flixster/android/activity/EpisodesPage;->refreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/flixster/android/activity/EpisodesPage;->access$13(Lcom/flixster/android/activity/EpisodesPage;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;->assetId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 279
    return-void
.end method
