.class public Lcom/flixster/android/activity/EpisodesPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "EpisodesPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;
    }
.end annotation


# static fields
.field public static final KEY_RIGHT_ID:Ljava/lang/String; = "KEY_RIGHT_ID"

.field public static final KEY_SEASON_ID:Ljava/lang/String; = "KEY_SEASON_ID"


# instance fields
.field private final canDownloadSuccessHandler:Landroid/os/Handler;

.field private contentLayout:Landroid/widget/LinearLayout;

.field private final downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private final downloadClickListener:Landroid/view/View$OnClickListener;

.field private final downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private final downloadDeleteClickListener:Landroid/view/View$OnClickListener;

.field private final downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private final errorHandler:Landroid/os/Handler;

.field private offlineAlert:Landroid/widget/TextView;

.field private final refreshHandler:Landroid/os/Handler;

.field private season:Lnet/flixster/android/model/Season;

.field private seasonId:J

.field private seasonRight:Lnet/flixster/android/model/LockerRight;

.field private final successHandler:Landroid/os/Handler;

.field private throbber:Landroid/widget/ProgressBar;

.field private timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

.field private final watchNowClickListener:Landroid/view/View$OnClickListener;

.field private final watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 86
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$1;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->successHandler:Landroid/os/Handler;

    .line 97
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$2;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->errorHandler:Landroid/os/Handler;

    .line 117
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$3;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->watchNowClickListener:Landroid/view/View$OnClickListener;

    .line 132
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$4;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 151
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$5;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$5;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->downloadClickListener:Landroid/view/View$OnClickListener;

    .line 174
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$6;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$6;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->canDownloadSuccessHandler:Landroid/os/Handler;

    .line 186
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$7;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$7;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 206
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$8;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$8;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->downloadDeleteClickListener:Landroid/view/View$OnClickListener;

    .line 222
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$9;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$9;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 243
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$10;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$10;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 282
    new-instance v0, Lcom/flixster/android/activity/EpisodesPage$11;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/EpisodesPage$11;-><init>(Lcom/flixster/android/activity/EpisodesPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->refreshHandler:Landroid/os/Handler;

    .line 37
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/EpisodesPage;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->throbber:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/EpisodesPage;Lnet/flixster/android/model/Season;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lcom/flixster/android/activity/EpisodesPage;->season:Lnet/flixster/android/model/Season;

    return-void
.end method

.method static synthetic access$10(Lcom/flixster/android/activity/EpisodesPage;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 222
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$11(Lcom/flixster/android/activity/EpisodesPage;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 243
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$12(Lcom/flixster/android/activity/EpisodesPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->contentLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$13(Lcom/flixster/android/activity/EpisodesPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 282
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->refreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/EpisodesPage;)Lnet/flixster/android/model/LockerRight;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->seasonRight:Lnet/flixster/android/model/LockerRight;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 295
    invoke-direct {p0}, Lcom/flixster/android/activity/EpisodesPage;->trackPage()V

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/EpisodesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/flixster/android/activity/EpisodesPage;->initializePage()V

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/EpisodesPage;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/EpisodesPage;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 186
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/EpisodesPage;J)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 265
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/EpisodesPage;->scheduleSingleRefresh(J)V

    return-void
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/EpisodesPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->canDownloadSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$9(Lcom/flixster/android/activity/EpisodesPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->errorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private initializePage()V
    .locals 7

    .prologue
    .line 107
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage;->season:Lnet/flixster/android/model/Season;

    invoke-virtual {v1}, Lnet/flixster/android/model/Season;->getEpisodes()Ljava/util/Collection;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage;->season:Lnet/flixster/android/model/Season;

    invoke-virtual {v1}, Lnet/flixster/android/model/Season;->getEpisodes()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 115
    :cond_0
    return-void

    .line 108
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Episode;

    .line 109
    .local v2, episode:Lnet/flixster/android/model/Episode;
    new-instance v0, Lcom/flixster/android/view/EpisodeView;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/EpisodeView;-><init>(Landroid/content/Context;)V

    .line 110
    .local v0, view:Lcom/flixster/android/view/EpisodeView;
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage;->seasonRight:Lnet/flixster/android/model/LockerRight;

    iget-object v3, p0, Lcom/flixster/android/activity/EpisodesPage;->watchNowClickListener:Landroid/view/View$OnClickListener;

    iget-object v4, p0, Lcom/flixster/android/activity/EpisodesPage;->downloadClickListener:Landroid/view/View$OnClickListener;

    .line 111
    iget-object v5, p0, Lcom/flixster/android/activity/EpisodesPage;->downloadDeleteClickListener:Landroid/view/View$OnClickListener;

    .line 110
    invoke-virtual/range {v0 .. v5}, Lcom/flixster/android/view/EpisodeView;->load(Lnet/flixster/android/model/LockerRight;Lnet/flixster/android/model/Episode;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage;->contentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private scheduleSingleRefresh(J)V
    .locals 4
    .parameter "assetId"

    .prologue
    .line 266
    iget-object v0, p0, Lcom/flixster/android/activity/EpisodesPage;->timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

    new-instance v1, Lcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, p2, v2}, Lcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;-><init>(Lcom/flixster/android/activity/EpisodesPage;JLcom/flixster/android/activity/EpisodesPage$RefreshTimerTask;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/flixster/android/activity/decorator/TimerDecorator;->scheduleTask(Ljava/util/TimerTask;J)V

    .line 267
    return-void
.end method

.method private trackPage()V
    .locals 4

    .prologue
    .line 296
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/episodes/info"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Episodes Info - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/EpisodesPage;->season:Lnet/flixster/android/model/Season;

    invoke-virtual {v3}, Lnet/flixster/android/model/Season;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter "savedInstanceState"

    .prologue
    const-wide/16 v6, 0x0

    .line 52
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    new-instance v4, Lcom/flixster/android/activity/decorator/TimerDecorator;

    invoke-direct {v4, p0}, Lcom/flixster/android/activity/decorator/TimerDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v4, p0, Lcom/flixster/android/activity/EpisodesPage;->timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

    invoke-virtual {p0, v4}, Lcom/flixster/android/activity/EpisodesPage;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 54
    iget-object v4, p0, Lcom/flixster/android/activity/EpisodesPage;->timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

    invoke-virtual {v4}, Lcom/flixster/android/activity/decorator/TimerDecorator;->onCreate()V

    .line 56
    invoke-virtual {p0}, Lcom/flixster/android/activity/EpisodesPage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 57
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 58
    const-string v4, "KEY_SEASON_ID"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/flixster/android/activity/EpisodesPage;->seasonId:J

    .line 59
    const-string v4, "KEY_RIGHT_ID"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 60
    .local v1, rightId:J
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v3

    .line 61
    .local v3, user:Lnet/flixster/android/model/User;
    if-eqz v3, :cond_0

    cmp-long v4, v1, v6

    if-lez v4, :cond_0

    .line 62
    invoke-virtual {v3, v1, v2}, Lnet/flixster/android/model/User;->getLockerRightFromRightId(J)Lnet/flixster/android/model/LockerRight;

    move-result-object v4

    iput-object v4, p0, Lcom/flixster/android/activity/EpisodesPage;->seasonRight:Lnet/flixster/android/model/LockerRight;

    .line 66
    .end local v1           #rightId:J
    .end local v3           #user:Lnet/flixster/android/model/User;
    :cond_0
    const v4, 0x7f030026

    invoke-virtual {p0, v4}, Lcom/flixster/android/activity/EpisodesPage;->setContentView(I)V

    .line 67
    invoke-virtual {p0}, Lcom/flixster/android/activity/EpisodesPage;->createActionBar()V

    .line 68
    const v4, 0x7f0c01a6

    invoke-virtual {p0, v4}, Lcom/flixster/android/activity/EpisodesPage;->setActionBarTitle(I)V

    .line 70
    const v4, 0x7f070075

    invoke-virtual {p0, v4}, Lcom/flixster/android/activity/EpisodesPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/flixster/android/activity/EpisodesPage;->contentLayout:Landroid/widget/LinearLayout;

    .line 71
    const v4, 0x7f070074

    invoke-virtual {p0, v4}, Lcom/flixster/android/activity/EpisodesPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/flixster/android/activity/EpisodesPage;->offlineAlert:Landroid/widget/TextView;

    .line 72
    const v4, 0x7f070039

    invoke-virtual {p0, v4}, Lcom/flixster/android/activity/EpisodesPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, p0, Lcom/flixster/android/activity/EpisodesPage;->throbber:Landroid/widget/ProgressBar;

    .line 74
    iget-wide v4, p0, Lcom/flixster/android/activity/EpisodesPage;->seasonId:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    .line 75
    iget-object v4, p0, Lcom/flixster/android/activity/EpisodesPage;->throbber:Landroid/widget/ProgressBar;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 76
    iget-wide v4, p0, Lcom/flixster/android/activity/EpisodesPage;->seasonId:J

    iget-object v6, p0, Lcom/flixster/android/activity/EpisodesPage;->successHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/flixster/android/activity/EpisodesPage;->errorHandler:Landroid/os/Handler;

    invoke-static {v4, v5, v6, v7}, Lnet/flixster/android/data/MovieDao;->getUserSeasonDetail(JLandroid/os/Handler;Landroid/os/Handler;)V

    .line 78
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 301
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 83
    iget-object v1, p0, Lcom/flixster/android/activity/EpisodesPage;->offlineAlert:Landroid/widget/TextView;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    return-void

    .line 83
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
