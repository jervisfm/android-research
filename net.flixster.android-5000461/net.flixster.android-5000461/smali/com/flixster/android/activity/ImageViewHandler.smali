.class public Lcom/flixster/android/activity/ImageViewHandler;
.super Landroid/os/Handler;
.source "ImageViewHandler.java"


# instance fields
.field private final imageUrl:Ljava/lang/String;

.field private final imageView:Landroid/widget/ImageView;

.field private final scaleType:Landroid/widget/ImageView$ScaleType;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 1
    .parameter "imageView"
    .parameter "imageUrl"

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/flixster/android/activity/ImageViewHandler;-><init>(Landroid/widget/ImageView;Ljava/lang/String;Landroid/widget/ImageView$ScaleType;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;Landroid/widget/ImageView$ScaleType;)V
    .locals 0
    .parameter "imageView"
    .parameter "imageUrl"
    .parameter "scaleType"

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/flixster/android/activity/ImageViewHandler;->imageView:Landroid/widget/ImageView;

    .line 30
    iput-object p2, p0, Lcom/flixster/android/activity/ImageViewHandler;->imageUrl:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/flixster/android/activity/ImageViewHandler;->scaleType:Landroid/widget/ImageView$ScaleType;

    .line 32
    invoke-static {p1, p2}, Lcom/flixster/android/view/ViewTag;->set(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 33
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "message"

    .prologue
    .line 38
    iget-object v2, p0, Lcom/flixster/android/activity/ImageViewHandler;->imageView:Landroid/widget/ImageView;

    invoke-static {v2}, Lcom/flixster/android/view/ViewTag;->get(Landroid/widget/ImageView;)Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, currImageUrl:Ljava/lang/String;
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/flixster/android/activity/ImageViewHandler;->imageUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    const/4 v0, 0x0

    .line 45
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v2, v2, Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 46
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .line 50
    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 51
    iget-object v2, p0, Lcom/flixster/android/activity/ImageViewHandler;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 52
    iget-object v2, p0, Lcom/flixster/android/activity/ImageViewHandler;->scaleType:Landroid/widget/ImageView$ScaleType;

    if-eqz v2, :cond_0

    .line 53
    iget-object v2, p0, Lcom/flixster/android/activity/ImageViewHandler;->imageView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/flixster/android/activity/ImageViewHandler;->scaleType:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0

    .line 47
    :cond_3
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v2, v2, Ljava/lang/ref/SoftReference;

    if-eqz v2, :cond_2

    .line 48
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    goto :goto_1
.end method
