.class Lcom/flixster/android/activity/LoginActivity$1;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/LoginActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/LoginActivity;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/LoginActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/LoginActivity$1;->this$0:Lcom/flixster/android/activity/LoginActivity;

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 1
    .parameter "which"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/flixster/android/activity/LoginActivity$1;->this$0:Lcom/flixster/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/flixster/android/activity/LoginActivity;->finish()V

    .line 68
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 1
    .parameter "which"

    .prologue
    .line 62
    iget-object v0, p0, Lcom/flixster/android/activity/LoginActivity$1;->this$0:Lcom/flixster/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/flixster/android/activity/LoginActivity;->finish()V

    .line 63
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 4
    .parameter "which"

    .prologue
    const/16 v3, 0x7b

    .line 44
    packed-switch p1, :pswitch_data_0

    .line 56
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1

    .line 46
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/flixster/android/activity/LoginActivity$1;->this$0:Lcom/flixster/android/activity/LoginActivity;

    const-class v2, Lnet/flixster/android/FacebookAuth;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/flixster/android/activity/LoginActivity$1;->this$0:Lcom/flixster/android/activity/LoginActivity;

    invoke-virtual {v1, v0, v3}, Lcom/flixster/android/activity/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 58
    :goto_0
    return-void

    .line 50
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/flixster/android/activity/LoginActivity$1;->this$0:Lcom/flixster/android/activity/LoginActivity;

    const-class v2, Lnet/flixster/android/FlixsterLoginPage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 51
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/flixster/android/activity/LoginActivity$1;->this$0:Lcom/flixster/android/activity/LoginActivity;

    invoke-virtual {v1, v0, v3}, Lcom/flixster/android/activity/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
