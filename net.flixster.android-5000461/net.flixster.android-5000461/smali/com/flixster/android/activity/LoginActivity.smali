.class public Lcom/flixster/android/activity/LoginActivity;
.super Lcom/flixster/android/activity/common/DecoratedActivity;
.source "LoginActivity.java"


# static fields
.field private static final CHOICE_FACEBOOK:I = 0x0

.field private static final CHOICE_FLX_CREATE:I = 0x2

.field private static final CHOICE_FLX_LOGIN:I = 0x1

.field private static final LOGIN_REQUEST:I = 0x7b


# instance fields
.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 79
    const/16 v0, 0x7b

    if-ne p1, v0, :cond_0

    .line 80
    packed-switch p2, :pswitch_data_0

    .line 91
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LoginActivity.onActivityResult LOGIN_REQUEST "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :goto_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/LoginActivity;->finish()V

    .line 98
    return-void

    .line 82
    :pswitch_0
    const-string v0, "FlxMain"

    const-string v1, "LoginActivity.onActivityResult LOGIN_REQUEST RESULT_OK"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :pswitch_1
    const-string v0, "FlxMain"

    const-string v1, "LoginActivity.onActivityResult LOGIN_REQUEST RESULT_CANCELED"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :cond_0
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LoginActivity.onActivityResult "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    const v4, 0x3b9aca66

    .line 30
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-virtual {p0}, Lcom/flixster/android/activity/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 33
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 34
    const-string v1, "net.flixster.android.EXTRA_TITLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/activity/LoginActivity;->title:Ljava/lang/String;

    .line 36
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/activity/LoginActivity;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 37
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/LoginActivity;->title:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 39
    :cond_1
    new-instance v1, Lcom/flixster/android/activity/LoginActivity$1;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/LoginActivity$1;-><init>(Lcom/flixster/android/activity/LoginActivity;)V

    invoke-virtual {p0, v4, v1}, Lcom/flixster/android/activity/LoginActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 70
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;->onResume()V

    .line 75
    return-void
.end method
