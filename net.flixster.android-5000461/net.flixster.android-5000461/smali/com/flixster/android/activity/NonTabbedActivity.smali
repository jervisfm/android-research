.class public Lcom/flixster/android/activity/NonTabbedActivity;
.super Lcom/flixster/android/activity/DialogActivity;
.source "NonTabbedActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/flixster/android/activity/DialogActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .parameter "keycode"
    .parameter "event"

    .prologue
    .line 20
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x54

    if-ne p1, v1, :cond_0

    .line 21
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/flixster/android/activity/NonTabbedActivity;->className:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".onKeyDown KEYCODE_SEARCH"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/SearchPage;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 23
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/NonTabbedActivity;->startActivity(Landroid/content/Intent;)V

    .line 24
    const/4 v1, 0x1

    .line 26
    .end local v0           #intent:Landroid/content/Intent;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/flixster/android/activity/DialogActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method
