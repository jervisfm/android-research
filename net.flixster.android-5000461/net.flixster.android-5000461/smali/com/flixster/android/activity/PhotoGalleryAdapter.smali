.class public Lcom/flixster/android/activity/PhotoGalleryAdapter;
.super Landroid/widget/BaseAdapter;
.source "PhotoGalleryAdapter.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final mPhotoHeight:I

.field private final photos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p2, photos:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Photo;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/flixster/android/activity/PhotoGalleryAdapter;->context:Landroid/content/Context;

    .line 22
    iput-object p2, p0, Lcom/flixster/android/activity/PhotoGalleryAdapter;->photos:Ljava/util/List;

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/flixster/android/activity/PhotoGalleryAdapter;->mPhotoHeight:I

    .line 24
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryAdapter;->photos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 46
    iget-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryAdapter;->photos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 50
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 27
    if-eqz p2, :cond_0

    instance-of v1, p2, Landroid/widget/ImageView;

    if-nez v1, :cond_1

    .line 28
    :cond_0
    new-instance p2, Landroid/widget/ImageView;

    .end local p2
    iget-object v1, p0, Lcom/flixster/android/activity/PhotoGalleryAdapter;->context:Landroid/content/Context;

    invoke-direct {p2, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 30
    .restart local p2
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/PhotoGalleryAdapter;->photos:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Photo;

    move-object v2, p2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Photo;->getThumbnailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 31
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    move-object v1, p2

    .line 32
    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020151

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    move-object v1, p2

    .line 36
    check-cast v1, Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    move-object v1, p2

    .line 37
    check-cast v1, Landroid/widget/ImageView;

    iget v2, p0, Lcom/flixster/android/activity/PhotoGalleryAdapter;->mPhotoHeight:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 38
    return-object p2

    :cond_2
    move-object v1, p2

    .line 34
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
