.class Lcom/flixster/android/activity/PhotoGalleryPage$1;
.super Landroid/os/Handler;
.source "PhotoGalleryPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/PhotoGalleryPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/PhotoGalleryPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/PhotoGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/PhotoGalleryPage$1;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    .line 69
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage$1;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    #getter for: Lcom/flixster/android/activity/PhotoGalleryPage;->gallery:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/flixster/android/activity/PhotoGalleryPage;->access$0(Lcom/flixster/android/activity/PhotoGalleryPage;)Landroid/widget/GridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage$1;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    #getter for: Lcom/flixster/android/activity/PhotoGalleryPage;->gallery:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/flixster/android/activity/PhotoGalleryPage;->access$0(Lcom/flixster/android/activity/PhotoGalleryPage;)Landroid/widget/GridView;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/activity/PhotoGalleryAdapter;

    iget-object v2, p0, Lcom/flixster/android/activity/PhotoGalleryPage$1;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    iget-object v3, p0, Lcom/flixster/android/activity/PhotoGalleryPage$1;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    #getter for: Lcom/flixster/android/activity/PhotoGalleryPage;->photos:Ljava/util/List;
    invoke-static {v3}, Lcom/flixster/android/activity/PhotoGalleryPage;->access$1(Lcom/flixster/android/activity/PhotoGalleryPage;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/flixster/android/activity/PhotoGalleryAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 73
    iget-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage$1;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    #getter for: Lcom/flixster/android/activity/PhotoGalleryPage;->gallery:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/flixster/android/activity/PhotoGalleryPage;->access$0(Lcom/flixster/android/activity/PhotoGalleryPage;)Landroid/widget/GridView;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/PhotoGalleryPage$1;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    #getter for: Lcom/flixster/android/activity/PhotoGalleryPage;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v1}, Lcom/flixster/android/activity/PhotoGalleryPage;->access$2(Lcom/flixster/android/activity/PhotoGalleryPage;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage$1;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    #getter for: Lcom/flixster/android/activity/PhotoGalleryPage;->gallery:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/flixster/android/activity/PhotoGalleryPage;->access$0(Lcom/flixster/android/activity/PhotoGalleryPage;)Landroid/widget/GridView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setClickable(Z)V

    .line 78
    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const-string v1, "KEY_PHOTOS"

    iget-object v2, p0, Lcom/flixster/android/activity/PhotoGalleryPage$1;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    #getter for: Lcom/flixster/android/activity/PhotoGalleryPage;->photos:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/PhotoGalleryPage;->access$1(Lcom/flixster/android/activity/PhotoGalleryPage;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 79
    return-void
.end method
