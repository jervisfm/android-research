.class Lcom/flixster/android/activity/PhotoGalleryPage$3;
.super Ljava/lang/Object;
.source "PhotoGalleryPage.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/PhotoGalleryPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/PhotoGalleryPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/PhotoGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/PhotoGalleryPage$3;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/flixster/android/activity/PhotoGalleryPage$3;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    #getter for: Lcom/flixster/android/activity/PhotoGalleryPage;->photos:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/PhotoGalleryPage;->access$1(Lcom/flixster/android/activity/PhotoGalleryPage;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Photo;

    .line 94
    .local v1, photo:Lnet/flixster/android/model/Photo;
    if-eqz v1, :cond_0

    .line 96
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/flixster/android/activity/PhotoGalleryPage$3;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    const-class v3, Lnet/flixster/android/ScrollGalleryPage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 97
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "KEY_GENERIC_GALLERY"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 98
    const-string v2, "PHOTO_INDEX"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 99
    const-string v2, "KEY_TITLE"

    iget-object v3, p0, Lcom/flixster/android/activity/PhotoGalleryPage$3;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    #getter for: Lcom/flixster/android/activity/PhotoGalleryPage;->title:Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/activity/PhotoGalleryPage;->access$3(Lcom/flixster/android/activity/PhotoGalleryPage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    iget-object v2, p0, Lcom/flixster/android/activity/PhotoGalleryPage$3;->this$0:Lcom/flixster/android/activity/PhotoGalleryPage;

    invoke-virtual {v2, v0}, Lcom/flixster/android/activity/PhotoGalleryPage;->startActivity(Landroid/content/Intent;)V

    .line 102
    .end local v0           #intent:Landroid/content/Intent;
    :cond_0
    return-void
.end method
