.class public Lcom/flixster/android/activity/PhotoGalleryPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "PhotoGalleryPage.java"


# static fields
.field public static final KEY_PHOTOS:Ljava/lang/String; = "KEY_PHOTOS"

.field public static final KEY_USERNAME:Ljava/lang/String; = "KEY_USERNAME"


# instance fields
.field private final errorHandler:Landroid/os/Handler;

.field private gallery:Landroid/widget/GridView;

.field private final photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private final photos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation
.end field

.field private final successHandler:Landroid/os/Handler;

.field private title:Ljava/lang/String;

.field private username:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->photos:Ljava/util/List;

    .line 69
    new-instance v0, Lcom/flixster/android/activity/PhotoGalleryPage$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/PhotoGalleryPage$1;-><init>(Lcom/flixster/android/activity/PhotoGalleryPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->successHandler:Landroid/os/Handler;

    .line 82
    new-instance v0, Lcom/flixster/android/activity/PhotoGalleryPage$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/PhotoGalleryPage$2;-><init>(Lcom/flixster/android/activity/PhotoGalleryPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->errorHandler:Landroid/os/Handler;

    .line 90
    new-instance v0, Lcom/flixster/android/activity/PhotoGalleryPage$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/PhotoGalleryPage$3;-><init>(Lcom/flixster/android/activity/PhotoGalleryPage;)V

    iput-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 27
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/PhotoGalleryPage;)Landroid/widget/GridView;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->gallery:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/PhotoGalleryPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->photos:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/PhotoGalleryPage;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/PhotoGalleryPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->title:Ljava/lang/String;

    return-object v0
.end method

.method private static createTitle(Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 3
    .parameter "username"
    .parameter "res"

    .prologue
    const/4 v2, 0x1

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 107
    const v1, 0x7f0c003d

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/flixster/android/activity/PhotoGalleryPage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 40
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 41
    const-string v1, "KEY_USERNAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->username:Ljava/lang/String;

    .line 44
    :cond_0
    const v1, 0x7f03006d

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/PhotoGalleryPage;->setContentView(I)V

    .line 45
    invoke-virtual {p0}, Lcom/flixster/android/activity/PhotoGalleryPage;->createActionBar()V

    .line 47
    iget-object v1, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->username:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/flixster/android/activity/PhotoGalleryPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/activity/PhotoGalleryPage;->createTitle(Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->title:Ljava/lang/String;

    .line 48
    iget-object v1, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->title:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/PhotoGalleryPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 49
    const v1, 0x7f070208

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/PhotoGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->gallery:Landroid/widget/GridView;

    .line 51
    iget-object v1, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->photos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 52
    iget-object v1, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->username:Ljava/lang/String;

    iget-object v2, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->photos:Ljava/util/List;

    iget-object v3, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->successHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->errorHandler:Landroid/os/Handler;

    invoke-static {v1, v2, v3, v4}, Lnet/flixster/android/data/PhotoDao;->getUserPhotos(Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 53
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 57
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 58
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/photo/gallery/user"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Phot Gallery - User - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/PhotoGalleryPage;->username:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onStop()V

    .line 66
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const-string v1, "KEY_PHOTOS"

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 67
    return-void
.end method
