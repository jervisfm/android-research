.class public Lcom/flixster/android/activity/TabbedActivity;
.super Lcom/flixster/android/activity/common/DecoratedActivity;
.source "TabbedActivity.java"


# instance fields
.field protected final className:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;-><init>()V

    .line 19
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/TabbedActivity;->className:Ljava/lang/String;

    .line 18
    return-void
.end method

.method private getParentTabActivity()Lnet/flixster/android/Flixster;
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/flixster/android/activity/TabbedActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    .line 50
    .local v0, parent:Landroid/app/Activity;
    instance-of v1, v0, Lnet/flixster/android/Flixster;

    if-eqz v1, :cond_0

    check-cast v0, Lnet/flixster/android/Flixster;

    .end local v0           #parent:Landroid/app/Activity;
    :goto_0
    return-object v0

    .restart local v0       #parent:Landroid/app/Activity;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected checkAndShowLaunchAd()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/flixster/android/activity/TabbedActivity;->getParentTabActivity()Lnet/flixster/android/Flixster;

    move-result-object v0

    .line 34
    .local v0, f:Lnet/flixster/android/Flixster;
    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {v0}, Lnet/flixster/android/Flixster;->checkAndShowLaunchAd()V

    .line 37
    :cond_0
    return-void
.end method

.method protected getAnalyticsAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getAnalyticsCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return-object v0
.end method

.method protected invalidActionBarItems()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/flixster/android/activity/TabbedActivity;->getParentTabActivity()Lnet/flixster/android/Flixster;

    move-result-object v0

    .line 42
    .local v0, f:Lnet/flixster/android/Flixster;
    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {v0}, Lnet/flixster/android/Flixster;->invalidateOptionsMenu()V

    .line 45
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keycode"
    .parameter "event"

    .prologue
    .line 23
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x54

    if-ne p1, v1, :cond_0

    .line 24
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/SearchPage;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 25
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/TabbedActivity;->startActivity(Landroid/content/Intent;)V

    .line 26
    const/4 v1, 0x1

    .line 28
    .end local v0           #intent:Landroid/content/Intent;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/flixster/android/activity/common/DecoratedActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected trackPage()V
    .locals 5

    .prologue
    .line 55
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flixster/android/activity/TabbedActivity;->getAnalyticsTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/activity/TabbedActivity;->getAnalyticsTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lcom/flixster/android/activity/TabbedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/widget/WidgetProvider;->isOriginatedFromWidget(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/activity/TabbedActivity;->getAnalyticsCategory()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flixster/android/activity/TabbedActivity;->getAnalyticsTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/activity/TabbedActivity;->getAnalyticsTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/flixster/android/activity/TabbedActivity;->getAnalyticsCategory()Ljava/lang/String;

    move-result-object v3

    .line 58
    invoke-virtual {p0}, Lcom/flixster/android/activity/TabbedActivity;->getAnalyticsAction()Ljava/lang/String;

    move-result-object v4

    .line 57
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_0
    return-void
.end method
