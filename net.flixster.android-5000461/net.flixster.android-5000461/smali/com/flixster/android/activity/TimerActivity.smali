.class public Lcom/flixster/android/activity/TimerActivity;
.super Lcom/actionbarsherlock/app/SherlockActivity;
.source "TimerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/TimerActivity$HandlerTimerTask;
    }
.end annotation


# instance fields
.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected cancelTimer()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 56
    iget-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    .line 59
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockActivity;->onPause()V

    .line 21
    invoke-virtual {p0}, Lcom/flixster/android/activity/TimerActivity;->cancelTimer()V

    .line 22
    return-void
.end method

.method protected scheduleTask(Landroid/os/Handler$Callback;J)V
    .locals 3
    .parameter "callback"
    .parameter "delay"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/flixster/android/activity/TimerActivity$HandlerTimerTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/flixster/android/activity/TimerActivity$HandlerTimerTask;-><init>(Lcom/flixster/android/activity/TimerActivity;Landroid/os/Handler$Callback;Lcom/flixster/android/activity/TimerActivity$HandlerTimerTask;)V

    invoke-virtual {v0, v1, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 42
    return-void
.end method

.method protected scheduleTask(Landroid/os/Handler$Callback;JJ)V
    .locals 6
    .parameter "callback"
    .parameter "delay"
    .parameter "period"

    .prologue
    .line 46
    iget-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/flixster/android/activity/TimerActivity$HandlerTimerTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/flixster/android/activity/TimerActivity$HandlerTimerTask;-><init>(Lcom/flixster/android/activity/TimerActivity;Landroid/os/Handler$Callback;Lcom/flixster/android/activity/TimerActivity$HandlerTimerTask;)V

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 50
    return-void
.end method

.method protected scheduleTask(Ljava/util/TimerTask;J)V
    .locals 1
    .parameter "task"
    .parameter "delay"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/TimerActivity;->timer:Ljava/util/Timer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 34
    return-void
.end method
