.class public Lcom/flixster/android/activity/TopLevelActivity;
.super Lcom/flixster/android/activity/TimerActivity;
.source "TopLevelActivity.java"


# instance fields
.field protected final className:Ljava/lang/String;

.field private volatile isPausing:Z

.field private level:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/flixster/android/activity/TimerActivity;-><init>()V

    .line 30
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/TopLevelActivity;->className:Ljava/lang/String;

    .line 29
    return-void
.end method

.method protected static getResumeCtr()I
    .locals 1

    .prologue
    .line 76
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    return v0
.end method

.method protected static incrementResumeCtr()V
    .locals 0

    .prologue
    .line 81
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->incrementResumeCtr()V

    .line 82
    return-void
.end method


# virtual methods
.method protected createActionBar()V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/flixster/android/activity/TopLevelActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 88
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 89
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 90
    return-void
.end method

.method protected hideActionBar()V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/flixster/android/activity/TopLevelActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 94
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->hide()V

    .line 95
    return-void
.end method

.method protected isPausing()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/flixster/android/activity/TopLevelActivity;->isPausing:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/flixster/android/activity/TimerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/TopLevelActivity;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/flixster/android/utils/ActivityHolder;->setTopLevelActivity(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcom/flixster/android/activity/TopLevelActivity;->level:I

    .line 41
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 63
    invoke-super {p0}, Lcom/flixster/android/activity/TimerActivity;->onDestroy()V

    .line 64
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/TopLevelActivity;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v0

    iget v1, p0, Lcom/flixster/android/activity/TopLevelActivity;->level:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ActivityHolder;->removeTopLevelActivity(I)V

    .line 67
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 55
    invoke-super {p0}, Lcom/flixster/android/activity/TimerActivity;->onPause()V

    .line 56
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/TopLevelActivity;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/activity/TopLevelActivity;->isPausing:Z

    .line 59
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0}, Lcom/flixster/android/activity/TimerActivity;->onResume()V

    .line 46
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/TopLevelActivity;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onResume #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/flixster/android/activity/TopLevelActivity;->getResumeCtr()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/flixster/android/utils/ActivityHolder;->setTopLevelActivity(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcom/flixster/android/activity/TopLevelActivity;->level:I

    .line 49
    invoke-static {}, Lcom/flixster/android/activity/TopLevelActivity;->incrementResumeCtr()V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flixster/android/activity/TopLevelActivity;->isPausing:Z

    .line 51
    return-void
.end method

.method protected setActionBarTitle(I)V
    .locals 2
    .parameter "resId"

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/flixster/android/activity/TopLevelActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 110
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 111
    invoke-virtual {v0, p1}, Lcom/actionbarsherlock/app/ActionBar;->setTitle(I)V

    .line 112
    return-void
.end method

.method protected setActionBarTitle(Ljava/lang/String;)V
    .locals 2
    .parameter "title"

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/flixster/android/activity/TopLevelActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 104
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 105
    invoke-virtual {v0, p1}, Lcom/actionbarsherlock/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 106
    return-void
.end method

.method protected showActionBar()V
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/flixster/android/activity/TopLevelActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 99
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->show()V

    .line 100
    return-void
.end method
