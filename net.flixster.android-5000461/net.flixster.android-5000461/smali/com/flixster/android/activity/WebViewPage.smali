.class public Lcom/flixster/android/activity/WebViewPage;
.super Landroid/app/Activity;
.source "WebViewPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/WebViewPage$DefaultWebViewClient;
    }
.end annotation


# static fields
.field public static final KEY_URL:Ljava/lang/String; = "KEY_URL"

.field private static final LINEAR_LAYOUT_FILL:Landroid/widget/LinearLayout$LayoutParams;


# instance fields
.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/flixster/android/activity/WebViewPage;->LINEAR_LAYOUT_FILL:Landroid/widget/LinearLayout$LayoutParams;

    .line 12
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private setUpWebView(Ljava/lang/String;)V
    .locals 3
    .parameter "url"

    .prologue
    .line 48
    iget-object v0, p0, Lcom/flixster/android/activity/WebViewPage;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/flixster/android/activity/WebViewPage$DefaultWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/flixster/android/activity/WebViewPage$DefaultWebViewClient;-><init>(Lcom/flixster/android/activity/WebViewPage;Lcom/flixster/android/activity/WebViewPage$DefaultWebViewClient;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 49
    iget-object v0, p0, Lcom/flixster/android/activity/WebViewPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 50
    iget-object v0, p0, Lcom/flixster/android/activity/WebViewPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 51
    iget-object v0, p0, Lcom/flixster/android/activity/WebViewPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/flixster/android/activity/WebViewPage;->mWebView:Landroid/webkit/WebView;

    sget-object v1, Lcom/flixster/android/activity/WebViewPage;->LINEAR_LAYOUT_FILL:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 22
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const/4 v1, 0x0

    .line 25
    .local v1, url:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/flixster/android/activity/WebViewPage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 26
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 27
    const-string v2, "KEY_URL"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 30
    :cond_0
    const v2, 0x7f030093

    invoke-virtual {p0, v2}, Lcom/flixster/android/activity/WebViewPage;->setContentView(I)V

    .line 31
    const v2, 0x7f0702e7

    invoke-virtual {p0, v2}, Lcom/flixster/android/activity/WebViewPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/flixster/android/activity/WebViewPage;->mWebView:Landroid/webkit/WebView;

    .line 32
    invoke-direct {p0, v1}, Lcom/flixster/android/activity/WebViewPage;->setUpWebView(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 37
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/flixster/android/activity/WebViewPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/flixster/android/activity/WebViewPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 39
    const/4 v0, 0x1

    .line 41
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
