.class public Lcom/flixster/android/activity/common/DaoHandler;
.super Landroid/os/Handler;
.source "DaoHandler.java"


# instance fields
.field private exception:Lnet/flixster/android/data/DaoException;

.field private isActive:Z

.field private isMsgSuccess:Z

.field private msgObj:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->isActive:Z

    .line 12
    return-void
.end method


# virtual methods
.method public activate()Lcom/flixster/android/activity/common/DaoHandler;
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->isActive:Z

    .line 67
    return-object p0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->isActive:Z

    .line 73
    return-void
.end method

.method public getException()Lnet/flixster/android/data/DaoException;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->exception:Lnet/flixster/android/data/DaoException;

    return-object v0
.end method

.method public getMsgObj()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->msgObj:Ljava/lang/Object;

    return-object v0
.end method

.method public isMsgSuccess()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->isMsgSuccess:Z

    return v0
.end method

.method public sendExceptionMessage(Lnet/flixster/android/data/DaoException;)V
    .locals 2
    .parameter "de"

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-boolean v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->isActive:Z

    if-eqz v0, :cond_0

    .line 41
    iput-boolean v1, p0, Lcom/flixster/android/activity/common/DaoHandler;->isMsgSuccess:Z

    .line 42
    iput-object p1, p0, Lcom/flixster/android/activity/common/DaoHandler;->exception:Lnet/flixster/android/data/DaoException;

    .line 43
    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DaoHandler;->sendEmptyMessage(I)Z

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    const-string v0, "FlxMain"

    const-string v1, "DaoHandler.sendExceptionMessage cancelled"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->sw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendSuccessMessage()V
    .locals 2

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->isActive:Z

    if-eqz v0, :cond_0

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->isMsgSuccess:Z

    .line 21
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/common/DaoHandler;->sendEmptyMessage(I)Z

    .line 25
    :goto_0
    return-void

    .line 23
    :cond_0
    const-string v0, "FlxMain"

    const-string v1, "DaoHandler.sendSuccessMessage cancelled"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->sw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendSuccessMessage(Ljava/lang/Object;)V
    .locals 2
    .parameter "obj"

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->isActive:Z

    if-eqz v0, :cond_0

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/activity/common/DaoHandler;->isMsgSuccess:Z

    .line 31
    iput-object p1, p0, Lcom/flixster/android/activity/common/DaoHandler;->msgObj:Ljava/lang/Object;

    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/common/DaoHandler;->sendEmptyMessage(I)Z

    .line 36
    :goto_0
    return-void

    .line 34
    :cond_0
    const-string v0, "FlxMain"

    const-string v1, "DaoHandler.sendSuccessMessage cancelled"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->sw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
