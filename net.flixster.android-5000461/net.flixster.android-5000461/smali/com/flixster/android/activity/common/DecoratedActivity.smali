.class public Lcom/flixster/android/activity/common/DecoratedActivity;
.super Landroid/app/Activity;
.source "DecoratedActivity.java"


# instance fields
.field private decorators:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/activity/decorator/ActivityDecorator;",
            ">;"
        }
    .end annotation
.end field

.field protected dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

.field protected topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V
    .locals 1
    .parameter "decorator"

    .prologue
    .line 34
    iget-object v0, p0, Lcom/flixster/android/activity/common/DecoratedActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 35
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedActivity;->decorators:Ljava/util/Collection;

    .line 25
    new-instance v1, Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/DialogDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DecoratedActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 26
    new-instance v1, Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedActivity;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DecoratedActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 28
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 31
    return-void

    .line 28
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 29
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onCreate()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .parameter "id"

    .prologue
    .line 84
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {v1, p1}, Lcom/flixster/android/activity/decorator/DialogDecorator;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 85
    .local v0, dialog:Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .end local v0           #dialog:Landroid/app/Dialog;
    :goto_0
    return-object v0

    .restart local v0       #dialog:Landroid/app/Dialog;
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 69
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    return-void

    .line 69
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 70
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onDestroy()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 53
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 55
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 58
    return-void

    .line 55
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 56
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onPause()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 44
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 46
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 49
    return-void

    .line 46
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 47
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onResume()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 39
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 40
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 63
    return-void
.end method

.method public showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V
    .locals 1
    .parameter "id"
    .parameter "listener"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/flixster/android/activity/common/DecoratedActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {v0, p1, p2}, Lcom/flixster/android/activity/decorator/DialogDecorator;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 79
    return-void
.end method
