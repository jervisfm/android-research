.class public Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.super Lcom/actionbarsherlock/app/SherlockActivity;
.source "DecoratedSherlockActivity.java"


# instance fields
.field protected actionbarDecorator:Lcom/flixster/android/activity/decorator/ActionBarDecorator;

.field private decorators:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/activity/decorator/ActivityDecorator;",
            ">;"
        }
    .end annotation
.end field

.field protected dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

.field protected topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V
    .locals 1
    .parameter "decorator"

    .prologue
    .line 52
    iget-object v0, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method

.method protected createActionBar()V
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 110
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 111
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 112
    return-void
.end method

.method protected createShareIntent()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 180
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    .local v0, shareIntent:Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    const-string v1, "android.intent.extra.TEXT"

    const v2, 0x7f0c01b2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->mobileUpsell()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    return-object v0
.end method

.method protected hideActionBar()V
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 116
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->hide()V

    .line 117
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->decorators:Ljava/util/Collection;

    .line 42
    new-instance v1, Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/DialogDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 43
    new-instance v1, Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 44
    new-instance v1, Lcom/flixster/android/activity/decorator/ActionBarDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/ActionBarDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->actionbarDecorator:Lcom/flixster/android/activity/decorator/ActionBarDecorator;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 46
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 49
    return-void

    .line 46
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 47
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onCreate()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .parameter "id"

    .prologue
    .line 102
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {v1, p1}, Lcom/flixster/android/activity/decorator/DialogDecorator;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 103
    .local v0, dialog:Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .end local v0           #dialog:Landroid/app/Dialog;
    :goto_0
    return-object v0

    .restart local v0       #dialog:Landroid/app/Dialog;
    :cond_0
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->getSupportMenuInflater()Lcom/actionbarsherlock/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0011

    invoke-virtual {v0, v1, p1}, Lcom/actionbarsherlock/view/MenuInflater;->inflate(ILcom/actionbarsherlock/view/Menu;)V

    .line 143
    sget-boolean v0, Lcom/flixster/android/utils/F;->IS_API_LEVEL_HONEYCOMB:Z

    if-eqz v0, :cond_0

    .line 144
    const v0, 0x7f0702fa

    invoke-interface {p1, v0}, Lcom/actionbarsherlock/view/Menu;->removeItem(I)V

    .line 145
    iget-object v0, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->actionbarDecorator:Lcom/flixster/android/activity/decorator/ActionBarDecorator;

    invoke-virtual {v0, p1}, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->createCollapsibleSearch(Lcom/actionbarsherlock/view/Menu;)V

    .line 147
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 85
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockActivity;->onDestroy()V

    .line 87
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 90
    return-void

    .line 87
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 88
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onDestroy()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 4
    .parameter "item"

    .prologue
    const/4 v1, 0x1

    .line 153
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 174
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 157
    :sswitch_0
    invoke-static {p0}, Lcom/flixster/android/bootstrap/BootstrapActivity;->getMainIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {p0, v2}, Landroid/support/v4/app/NavUtils;->navigateUpTo(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 161
    :sswitch_1
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->createShareIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "Share via"

    invoke-static {v2, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 165
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/SearchPage;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 166
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 170
    .end local v0           #intent:Landroid/content/Intent;
    :sswitch_3
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/SettingsPage;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 171
    .restart local v0       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 153
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0702fa -> :sswitch_2
        0x7f0702fb -> :sswitch_3
        0x7f070303 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 71
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockActivity;->onPause()V

    .line 73
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 76
    return-void

    .line 73
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 74
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onPause()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockActivity;->onResume()V

    .line 64
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 67
    return-void

    .line 64
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 65
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onResume()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 57
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockActivity;->onStart()V

    .line 58
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockActivity;->onStop()V

    .line 81
    return-void
.end method

.method protected setActionBarTitle(I)V
    .locals 2
    .parameter "resId"

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 132
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 133
    invoke-virtual {v0, p1}, Lcom/actionbarsherlock/app/ActionBar;->setTitle(I)V

    .line 134
    return-void
.end method

.method protected setActionBarTitle(Ljava/lang/String;)V
    .locals 2
    .parameter "title"

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 126
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 127
    invoke-virtual {v0, p1}, Lcom/actionbarsherlock/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 128
    return-void
.end method

.method protected showActionBar()V
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 121
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->show()V

    .line 122
    return-void
.end method

.method public showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V
    .locals 1
    .parameter "id"
    .parameter "listener"

    .prologue
    .line 96
    iget-object v0, p0, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {v0, p1, p2}, Lcom/flixster/android/activity/decorator/DialogDecorator;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 97
    return-void
.end method
