.class public Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;
.super Lcom/actionbarsherlock/app/SherlockMapActivity;
.source "DecoratedSherlockMapActivity.java"


# instance fields
.field private decorators:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/activity/decorator/ActivityDecorator;",
            ">;"
        }
    .end annotation
.end field

.field protected dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

.field protected topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockMapActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V
    .locals 1
    .parameter "decorator"

    .prologue
    .line 40
    iget-object v0, p0, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 41
    return-void
.end method

.method protected createActionBar()V
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 105
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 106
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 107
    return-void
.end method

.method protected isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->decorators:Ljava/util/Collection;

    .line 31
    new-instance v1, Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/DialogDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 32
    new-instance v1, Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 34
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 37
    return-void

    .line 34
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 35
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onCreate()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .parameter "id"

    .prologue
    .line 90
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {v1, p1}, Lcom/flixster/android/activity/decorator/DialogDecorator;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 91
    .local v0, dialog:Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .end local v0           #dialog:Landroid/app/Dialog;
    :goto_0
    return-object v0

    .restart local v0       #dialog:Landroid/app/Dialog;
    :cond_0
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockMapActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 123
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 73
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockMapActivity;->onDestroy()V

    .line 75
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 78
    return-void

    .line 75
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 76
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onDestroy()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 128
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 134
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockMapActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 131
    :pswitch_0
    invoke-static {p0}, Lcom/flixster/android/bootstrap/BootstrapActivity;->getMainIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/support/v4/app/NavUtils;->navigateUpTo(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 132
    const/4 v0, 0x1

    goto :goto_0

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 59
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockMapActivity;->onPause()V

    .line 61
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 64
    return-void

    .line 61
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 62
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onPause()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 50
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockMapActivity;->onResume()V

    .line 52
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 55
    return-void

    .line 52
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 53
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onResume()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockMapActivity;->onStart()V

    .line 46
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 68
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockMapActivity;->onStop()V

    .line 69
    return-void
.end method

.method protected setActionBarTitle(I)V
    .locals 2
    .parameter "resId"

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 117
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 118
    invoke-virtual {v0, p1}, Lcom/actionbarsherlock/app/ActionBar;->setTitle(I)V

    .line 119
    return-void
.end method

.method protected setActionBarTitle(Ljava/lang/String;)V
    .locals 2
    .parameter "title"

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 111
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 112
    invoke-virtual {v0, p1}, Lcom/actionbarsherlock/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 113
    return-void
.end method

.method public showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V
    .locals 1
    .parameter "id"
    .parameter "listener"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {v0, p1, p2}, Lcom/flixster/android/activity/decorator/DialogDecorator;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 85
    return-void
.end method
