.class Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;
.super Ljava/lang/Object;
.source "DecoratedSherlockTabActivity.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrivacyDialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;


# direct methods
.method private constructor <init>(Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 181
    iput-object p1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;->this$0:Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 181
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;-><init>(Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;)V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 4
    .parameter "which"

    .prologue
    .line 194
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;->this$0:Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;

    invoke-virtual {v2}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0188

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 195
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;->this$0:Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;

    invoke-virtual {v1, v0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->startActivity(Landroid/content/Intent;)V

    .line 196
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 190
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 4
    .parameter "which"

    .prologue
    .line 184
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;->this$0:Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;

    invoke-virtual {v2}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0187

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 185
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;->this$0:Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;

    invoke-virtual {v1, v0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->startActivity(Landroid/content/Intent;)V

    .line 186
    return-void
.end method
