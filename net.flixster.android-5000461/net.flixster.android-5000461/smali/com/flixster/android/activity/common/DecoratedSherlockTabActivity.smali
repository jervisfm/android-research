.class public Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;
.super Lcom/actionbarsherlock/app/SherlockTabActivity;
.source "DecoratedSherlockTabActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;
    }
.end annotation


# instance fields
.field protected actionbarDecorator:Lcom/flixster/android/activity/decorator/ActionBarDecorator;

.field private decorators:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/activity/decorator/ActivityDecorator;",
            ">;"
        }
    .end annotation
.end field

.field protected dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

.field protected timerDecorater:Lcom/flixster/android/activity/decorator/TimerDecorator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockTabActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V
    .locals 1
    .parameter "decorator"

    .prologue
    .line 54
    iget-object v0, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 55
    return-void
.end method

.method protected createActionBar()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 111
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 112
    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 113
    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setLogo(I)V

    .line 114
    return-void
.end method

.method protected createShareIntent()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 175
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 176
    .local v0, shareIntent:Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    const-string v1, "android.intent.extra.TEXT"

    const v2, 0x7f0c01b2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->mobileUpsell()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockTabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->decorators:Ljava/util/Collection;

    .line 44
    new-instance v1, Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/DialogDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 45
    new-instance v1, Lcom/flixster/android/activity/decorator/TimerDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/TimerDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->timerDecorater:Lcom/flixster/android/activity/decorator/TimerDecorator;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 46
    new-instance v1, Lcom/flixster/android/activity/decorator/ActionBarDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/ActionBarDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->actionbarDecorator:Lcom/flixster/android/activity/decorator/ActionBarDecorator;

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 48
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 51
    return-void

    .line 48
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 49
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onCreate()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .parameter "id"

    .prologue
    .line 103
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {v1, p1}, Lcom/flixster/android/activity/decorator/DialogDecorator;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 104
    .local v0, dialog:Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .end local v0           #dialog:Landroid/app/Dialog;
    :goto_0
    return-object v0

    .restart local v0       #dialog:Landroid/app/Dialog;
    :cond_0
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockTabActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->getSupportMenuInflater()Lcom/actionbarsherlock/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f000a

    invoke-virtual {v0, v1, p1}, Lcom/actionbarsherlock/view/MenuInflater;->inflate(ILcom/actionbarsherlock/view/Menu;)V

    .line 119
    sget-boolean v0, Lcom/flixster/android/utils/F;->IS_API_LEVEL_HONEYCOMB:Z

    if-eqz v0, :cond_0

    .line 120
    const v0, 0x7f0702fa

    invoke-interface {p1, v0}, Lcom/actionbarsherlock/view/Menu;->removeItem(I)V

    .line 121
    iget-object v0, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->actionbarDecorator:Lcom/flixster/android/activity/decorator/ActionBarDecorator;

    invoke-virtual {v0, p1}, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->createCollapsibleSearch(Lcom/actionbarsherlock/view/Menu;)V

    .line 123
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 87
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockTabActivity;->onDestroy()V

    .line 89
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    return-void

    .line 89
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 90
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onDestroy()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 7
    .parameter "item"

    .prologue
    const/4 v1, 0x1

    .line 129
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 169
    :pswitch_0
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockTabActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 132
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/main/phone"

    const-string v4, "Main - Phone"

    const-string v5, "MainPhoneActionBar"

    const-string v6, "Search"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/SearchPage;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 134
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 138
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/main/phone"

    const-string v4, "Main - Phone"

    const-string v5, "MainPhoneActionBar"

    const-string v6, "Settings"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/SettingsPage;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    .restart local v0       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 144
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/main/phone"

    const-string v4, "Main - Phone"

    const-string v5, "MainPhoneActionBar"

    const-string v6, "TellAFriend"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->createShareIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "Share via"

    invoke-static {v2, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 149
    :pswitch_4
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/main/phone"

    const-string v4, "Main - Phone"

    const-string v5, "MainPhoneActionBar"

    const-string v6, "RateThisApp"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-static {p0}, Lcom/flixster/android/utils/AppRater;->rateThisApp(Landroid/content/Context;)V

    goto :goto_0

    .line 154
    :pswitch_5
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/main/phone"

    const-string v4, "Main - Phone"

    const-string v5, "MainPhoneActionBar"

    const-string v6, "FAQ"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->faq()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lnet/flixster/android/Starter;->launchBrowser(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 159
    :pswitch_6
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/main/phone"

    const-string v4, "Main - Phone"

    const-string v5, "MainPhoneActionBar"

    const-string v6, "PrivacyPolicy"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const v2, 0x3b9aca0a

    new-instance v3, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;-><init>(Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity$PrivacyDialogListener;)V

    invoke-virtual {p0, v2, v3}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto/16 :goto_0

    .line 164
    :pswitch_7
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/main/phone"

    const-string v4, "Main - Phone"

    const-string v5, "MainPhoneActionBar"

    const-string v6, "Feedback"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->feedback()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0c0135

    invoke-virtual {p0, v3}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, p0}, Lnet/flixster/android/Starter;->launchFlxHtmlPage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 129
    :pswitch_data_0
    .packed-switch 0x7f0702fa
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 73
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockTabActivity;->onPause()V

    .line 75
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 78
    return-void

    .line 75
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 76
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onPause()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 64
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockTabActivity;->onResume()V

    .line 66
    iget-object v1, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 69
    return-void

    .line 66
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 67
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onResume()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockTabActivity;->onStart()V

    .line 60
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockTabActivity;->onStop()V

    .line 83
    return-void
.end method

.method public showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V
    .locals 1
    .parameter "id"
    .parameter "listener"

    .prologue
    .line 98
    iget-object v0, p0, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {v0, p1, p2}, Lcom/flixster/android/activity/decorator/DialogDecorator;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 99
    return-void
.end method
