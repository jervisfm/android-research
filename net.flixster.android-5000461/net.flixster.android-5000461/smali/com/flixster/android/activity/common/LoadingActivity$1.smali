.class Lcom/flixster/android/activity/common/LoadingActivity$1;
.super Lcom/flixster/android/activity/common/DaoHandler;
.source "LoadingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/common/LoadingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/common/LoadingActivity;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/common/LoadingActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/common/LoadingActivity$1;->this$0:Lcom/flixster/android/activity/common/LoadingActivity;

    .line 54
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DaoHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    const/16 v2, 0x8

    .line 56
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/LoadingActivity$1;->isMsgSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "FlxMain"

    const-string v1, "MovieDetailFragment.dataHandler success"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/flixster/android/activity/common/LoadingActivity$1;->this$0:Lcom/flixster/android/activity/common/LoadingActivity;

    #getter for: Lcom/flixster/android/activity/common/LoadingActivity;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/flixster/android/activity/common/LoadingActivity;->access$0(Lcom/flixster/android/activity/common/LoadingActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 59
    iget-object v1, p0, Lcom/flixster/android/activity/common/LoadingActivity$1;->this$0:Lcom/flixster/android/activity/common/LoadingActivity;

    invoke-virtual {p0}, Lcom/flixster/android/activity/common/LoadingActivity$1;->getMsgObj()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    #calls: Lcom/flixster/android/activity/common/LoadingActivity;->launchTrailer(Lnet/flixster/android/model/Movie;)V
    invoke-static {v1, v0}, Lcom/flixster/android/activity/common/LoadingActivity;->access$1(Lcom/flixster/android/activity/common/LoadingActivity;Lnet/flixster/android/model/Movie;)V

    .line 64
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/common/LoadingActivity$1;->this$0:Lcom/flixster/android/activity/common/LoadingActivity;

    #getter for: Lcom/flixster/android/activity/common/LoadingActivity;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/flixster/android/activity/common/LoadingActivity;->access$0(Lcom/flixster/android/activity/common/LoadingActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 62
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/LoadingActivity$1;->getException()Lnet/flixster/android/data/DaoException;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/common/LoadingActivity$1;->this$0:Lcom/flixster/android/activity/common/LoadingActivity;

    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedSherlockActivity;)V

    goto :goto_0
.end method
