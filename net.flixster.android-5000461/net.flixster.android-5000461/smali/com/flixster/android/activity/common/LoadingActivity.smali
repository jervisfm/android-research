.class public Lcom/flixster/android/activity/common/LoadingActivity;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "LoadingActivity.java"


# instance fields
.field private final dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

.field private throbber:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 54
    new-instance v0, Lcom/flixster/android/activity/common/LoadingActivity$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/common/LoadingActivity$1;-><init>(Lcom/flixster/android/activity/common/LoadingActivity;)V

    iput-object v0, p0, Lcom/flixster/android/activity/common/LoadingActivity;->dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

    .line 19
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/common/LoadingActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 20
    iget-object v0, p0, Lcom/flixster/android/activity/common/LoadingActivity;->throbber:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/common/LoadingActivity;Lnet/flixster/android/model/Movie;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/common/LoadingActivity;->launchTrailer(Lnet/flixster/android/model/Movie;)V

    return-void
.end method

.method private launchTrailer(Lnet/flixster/android/model/Movie;)V
    .locals 0
    .parameter "m"

    .prologue
    .line 68
    invoke-static {p1, p0}, Lnet/flixster/android/Starter;->launchAdFreeTrailer(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V

    .line 69
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/LoadingActivity;->finish()V

    .line 70
    return-void
.end method

.method private loadMovie(J)V
    .locals 3
    .parameter "movieId"

    .prologue
    .line 45
    invoke-static {p1, p2}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v0

    .line 46
    .local v0, m:Lnet/flixster/android/model/Movie;
    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->isDetailsApiParsed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    invoke-direct {p0, v0}, Lcom/flixster/android/activity/common/LoadingActivity;->launchTrailer(Lnet/flixster/android/model/Movie;)V

    .line 52
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/activity/common/LoadingActivity;->throbber:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 50
    iget-object v1, p0, Lcom/flixster/android/activity/common/LoadingActivity;->dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

    invoke-static {p1, p2, v1}, Lcom/flixster/android/data/MovieDaoNew;->fetchMovie(JLcom/flixster/android/activity/common/DaoHandler;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    const-wide/16 v4, 0x0

    .line 24
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    const v2, 0x7f030040

    invoke-virtual {p0, v2}, Lcom/flixster/android/activity/common/LoadingActivity;->setContentView(I)V

    .line 26
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/LoadingActivity;->createActionBar()V

    .line 27
    const v2, 0x7f0c0135

    invoke-virtual {p0, v2}, Lcom/flixster/android/activity/common/LoadingActivity;->setActionBarTitle(I)V

    .line 28
    const v2, 0x7f070039

    invoke-virtual {p0, v2}, Lcom/flixster/android/activity/common/LoadingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/flixster/android/activity/common/LoadingActivity;->throbber:Landroid/widget/ProgressBar;

    .line 31
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/LoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .local v0, movieId:J
    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    .line 32
    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/common/LoadingActivity;->loadMovie(J)V

    .line 36
    :goto_0
    return-void

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/LoadingActivity;->finish()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onStop()V

    .line 41
    iget-object v0, p0, Lcom/flixster/android/activity/common/LoadingActivity;->dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

    invoke-virtual {v0}, Lcom/flixster/android/activity/common/DaoHandler;->cancel()V

    .line 42
    return-void
.end method
