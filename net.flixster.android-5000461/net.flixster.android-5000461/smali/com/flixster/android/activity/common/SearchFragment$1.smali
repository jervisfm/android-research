.class Lcom/flixster/android/activity/common/SearchFragment$1;
.super Lcom/flixster/android/activity/common/DaoHandler;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/common/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/common/SearchFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/common/SearchFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/common/SearchFragment$1;->this$0:Lcom/flixster/android/activity/common/SearchFragment;

    .line 155
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DaoHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment$1;->isMsgSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/flixster/android/activity/common/SearchFragment$1;->this$0:Lcom/flixster/android/activity/common/SearchFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/common/SearchFragment;->onDataFetched()V

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment$1;->getException()Lnet/flixster/android/data/DaoException;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/common/SearchFragment$1;->this$0:Lcom/flixster/android/activity/common/SearchFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/common/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;I)V

    goto :goto_0
.end method
