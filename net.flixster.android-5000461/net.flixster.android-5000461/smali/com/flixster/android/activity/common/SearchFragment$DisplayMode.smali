.class final enum Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;
.super Ljava/lang/Enum;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/common/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "DisplayMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

.field public static final enum PREVIEW_ALL:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

.field public static final enum SHOW_ALL_ACTORS:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

.field public static final enum SHOW_ALL_MOVIES:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    const-string v1, "PREVIEW_ALL"

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->PREVIEW_ALL:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    new-instance v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    const-string v1, "SHOW_ALL_MOVIES"

    invoke-direct {v0, v1, v3}, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->SHOW_ALL_MOVIES:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    new-instance v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    const-string v1, "SHOW_ALL_ACTORS"

    invoke-direct {v0, v1, v4}, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->SHOW_ALL_ACTORS:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    .line 39
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    sget-object v1, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->PREVIEW_ALL:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->SHOW_ALL_MOVIES:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->SHOW_ALL_ACTORS:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->ENUM$VALUES:[Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    return-object v0
.end method

.method public static values()[Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->ENUM$VALUES:[Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    array-length v1, v0

    new-array v2, v1, [Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
