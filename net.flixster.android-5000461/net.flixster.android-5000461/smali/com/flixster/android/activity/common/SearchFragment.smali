.class public Lcom/flixster/android/activity/common/SearchFragment;
.super Lcom/actionbarsherlock/app/SherlockListFragment;
.source "SearchFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/actionbarsherlock/app/SherlockListFragment;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$net$flixster$android$lvi$LviLoadMore$Type:[I = null

.field private static final NUM_PREVIEW_RESULTS:I = 0x2


# instance fields
.field private actorResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field

.field private final dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

.field private displayMode:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

.field private isDataFetched:Z

.field private isViewCreated:Z

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/lvi/Lvi;",
            ">;"
        }
    .end annotation
.end field

.field private movieResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private query:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$net$flixster$android$lvi$LviLoadMore$Type()[I
    .locals 3

    .prologue
    .line 38
    sget-object v0, Lcom/flixster/android/activity/common/SearchFragment;->$SWITCH_TABLE$net$flixster$android$lvi$LviLoadMore$Type:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lnet/flixster/android/lvi/LviLoadMore$Type;->values()[Lnet/flixster/android/lvi/LviLoadMore$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lnet/flixster/android/lvi/LviLoadMore$Type;->ACTOR:Lnet/flixster/android/lvi/LviLoadMore$Type;

    invoke-virtual {v1}, Lnet/flixster/android/lvi/LviLoadMore$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lnet/flixster/android/lvi/LviLoadMore$Type;->MOVIE:Lnet/flixster/android/lvi/LviLoadMore$Type;

    invoke-virtual {v1}, Lnet/flixster/android/lvi/LviLoadMore$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lcom/flixster/android/activity/common/SearchFragment;->$SWITCH_TABLE$net$flixster$android$lvi$LviLoadMore$Type:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;-><init>()V

    .line 49
    sget-object v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->PREVIEW_ALL:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    iput-object v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->displayMode:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    .line 155
    new-instance v0, Lcom/flixster/android/activity/common/SearchFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/common/SearchFragment$1;-><init>(Lcom/flixster/android/activity/common/SearchFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/common/SearchFragment;->setRetainInstance(Z)V

    .line 53
    return-void
.end method

.method private createListItems()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/lvi/Lvi;",
            ">;"
        }
    .end annotation

    .prologue
    const v12, 0x7f0c010d

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 174
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    .line 177
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    new-instance v5, Lnet/flixster/android/lvi/LviSubHeader;

    const v6, 0x7f0c0104

    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lnet/flixster/android/lvi/LviSubHeader;-><init>(ILandroid/app/Activity;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->movieResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 179
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    invoke-direct {p0}, Lcom/flixster/android/activity/common/SearchFragment;->createNotFoundMessage()Lnet/flixster/android/lvi/Lvi;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    new-instance v5, Lnet/flixster/android/lvi/LviSubHeader;

    const v6, 0x7f0c0105

    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lnet/flixster/android/lvi/LviSubHeader;-><init>(ILandroid/app/Activity;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->actorResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 198
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    invoke-direct {p0}, Lcom/flixster/android/activity/common/SearchFragment;->createNotFoundMessage()Lnet/flixster/android/lvi/Lvi;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    return-object v4

    .line 180
    :cond_2
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->displayMode:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    sget-object v5, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->PREVIEW_ALL:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    if-eq v4, v5, :cond_3

    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->displayMode:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    sget-object v5, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->SHOW_ALL_ACTORS:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    if-ne v4, v5, :cond_5

    .line 181
    :cond_3
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->movieResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 182
    .local v3, size:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_2
    invoke-static {v3, v10}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-lt v1, v4, :cond_4

    .line 185
    if-le v3, v10, :cond_0

    .line 186
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    new-instance v5, Lnet/flixster/android/lvi/LviLoadMore;

    const v6, 0x7f0c010b

    invoke-virtual {p0, v6}, Lcom/flixster/android/activity/common/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    .line 187
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    .line 186
    invoke-virtual {p0, v12, v7}, Lcom/flixster/android/activity/common/SearchFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 187
    sget-object v8, Lnet/flixster/android/lvi/LviLoadMore$Type;->MOVIE:Lnet/flixster/android/lvi/LviLoadMore$Type;

    invoke-direct {v5, v6, v7, v8}, Lnet/flixster/android/lvi/LviLoadMore;-><init>(Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/lvi/LviLoadMore$Type;)V

    .line 186
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 183
    :cond_4
    iget-object v5, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    new-instance v6, Lnet/flixster/android/lvi/LviMovie;

    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->movieResults:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Movie;

    invoke-direct {v6, v4}, Lnet/flixster/android/lvi/LviMovie;-><init>(Lnet/flixster/android/model/Movie;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 190
    .end local v1           #i:I
    .end local v3           #size:I
    :cond_5
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->movieResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Movie;

    .line 191
    .local v2, movie:Lnet/flixster/android/model/Movie;
    iget-object v5, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    new-instance v6, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v6, v2}, Lnet/flixster/android/lvi/LviMovie;-><init>(Lnet/flixster/android/model/Movie;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 199
    .end local v2           #movie:Lnet/flixster/android/model/Movie;
    :cond_6
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->displayMode:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    sget-object v5, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->PREVIEW_ALL:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    if-eq v4, v5, :cond_7

    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->displayMode:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    sget-object v5, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->SHOW_ALL_MOVIES:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    if-ne v4, v5, :cond_9

    .line 200
    :cond_7
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->actorResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 201
    .restart local v3       #size:I
    const/4 v1, 0x0

    .restart local v1       #i:I
    :goto_4
    invoke-static {v3, v10}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-lt v1, v4, :cond_8

    .line 204
    if-le v3, v10, :cond_1

    .line 205
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    new-instance v5, Lnet/flixster/android/lvi/LviLoadMore;

    const v6, 0x7f0c010c

    invoke-virtual {p0, v6}, Lcom/flixster/android/activity/common/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    .line 206
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    .line 205
    invoke-virtual {p0, v12, v7}, Lcom/flixster/android/activity/common/SearchFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 206
    sget-object v8, Lnet/flixster/android/lvi/LviLoadMore$Type;->ACTOR:Lnet/flixster/android/lvi/LviLoadMore$Type;

    invoke-direct {v5, v6, v7, v8}, Lnet/flixster/android/lvi/LviLoadMore;-><init>(Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/lvi/LviLoadMore$Type;)V

    .line 205
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 202
    :cond_8
    iget-object v5, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    new-instance v6, Lnet/flixster/android/lvi/LviActor;

    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->actorResults:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Actor;

    invoke-direct {v6, v4, v9}, Lnet/flixster/android/lvi/LviActor;-><init>(Lnet/flixster/android/model/Actor;Z)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 209
    .end local v1           #i:I
    .end local v3           #size:I
    :cond_9
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->actorResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 210
    .local v0, actor:Lnet/flixster/android/model/Actor;
    iget-object v5, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    new-instance v6, Lnet/flixster/android/lvi/LviActor;

    invoke-direct {v6, v0, v9}, Lnet/flixster/android/lvi/LviActor;-><init>(Lnet/flixster/android/model/Actor;Z)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5
.end method

.method private createNotFoundMessage()Lnet/flixster/android/lvi/Lvi;
    .locals 3

    .prologue
    .line 218
    new-instance v0, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 219
    .local v0, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c010a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 220
    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/flixster/android/activity/common/SearchFragment;
    .locals 1
    .parameter "query"

    .prologue
    .line 56
    new-instance v0, Lcom/flixster/android/activity/common/SearchFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/common/SearchFragment;-><init>()V

    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/common/SearchFragment;->setQuery(Ljava/lang/String;)Lcom/flixster/android/activity/common/SearchFragment;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected checkState()V
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->isDataFetch()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->isViewCreated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->createView()V

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->fetchData()V

    goto :goto_0
.end method

.method protected createView()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 167
    new-instance v0, Lnet/flixster/android/lvi/LviAdapter;

    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/flixster/android/activity/common/SearchFragment;->createListItems()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/common/SearchFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 168
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 169
    invoke-virtual {p0, v3}, Lcom/flixster/android/activity/common/SearchFragment;->setListShown(Z)V

    .line 170
    iput-boolean v3, p0, Lcom/flixster/android/activity/common/SearchFragment;->isViewCreated:Z

    .line 171
    return-void
.end method

.method protected fetchData()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->movieResults:Ljava/util/List;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->actorResults:Ljava/util/List;

    .line 139
    iget-object v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->query:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->onDataFetched()V

    .line 153
    :goto_0
    return-void

    .line 141
    :cond_0
    const-string v0, "FLX"

    iget-object v1, p0, Lcom/flixster/android/activity/common/SearchFragment;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "REINDEER FLOTILLA"

    iget-object v1, p0, Lcom/flixster/android/activity/common/SearchFragment;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    const-string v0, "UUDDLRLRBA"

    iget-object v1, p0, Lcom/flixster/android/activity/common/SearchFragment;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->enableAdminMode()V

    .line 144
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0c01bb

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 145
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->onDataFetched()V

    goto :goto_0

    .line 146
    :cond_2
    const-string v0, "DGNS"

    iget-object v1, p0, Lcom/flixster/android/activity/common/SearchFragment;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 147
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->enableDiagnosticMode()V

    .line 148
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0c01bc

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 149
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->onDataFetched()V

    goto :goto_0

    .line 151
    :cond_3
    iget-object v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->query:Ljava/lang/String;

    iget-object v1, p0, Lcom/flixster/android/activity/common/SearchFragment;->movieResults:Ljava/util/List;

    iget-object v2, p0, Lcom/flixster/android/activity/common/SearchFragment;->actorResults:Ljava/util/List;

    iget-object v3, p0, Lcom/flixster/android/activity/common/SearchFragment;->dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

    invoke-static {v0, v1, v2, v3}, Lcom/flixster/android/data/MovieDaoNew;->searchMovieAndActor(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/flixster/android/activity/common/DaoHandler;)V

    goto :goto_0
.end method

.method public invalidateData()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    iput-boolean v1, p0, Lcom/flixster/android/activity/common/SearchFragment;->isViewCreated:Z

    iput-boolean v1, p0, Lcom/flixster/android/activity/common/SearchFragment;->isDataFetched:Z

    .line 114
    sget-object v0, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->PREVIEW_ALL:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    iput-object v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->displayMode:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    .line 115
    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/common/SearchFragment;->setListShown(Z)V

    .line 116
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->checkState()V

    .line 117
    return-void
.end method

.method public invalidateView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 124
    iput-boolean v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->isViewCreated:Z

    .line 125
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/common/SearchFragment;->setListShown(Z)V

    .line 126
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->checkState()V

    .line 127
    return-void
.end method

.method protected isDataFetch()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->isDataFetched:Z

    return v0
.end method

.method protected isViewCreated()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->isViewCreated:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/common/ActionBarActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    const v3, 0x7f0c0048

    invoke-virtual {p0, v3}, Lcom/flixster/android/activity/common/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/common/SearchFragment;->query:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/flixster/android/activity/common/ActionBarActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 74
    .local v0, listView:Landroid/widget/ListView;
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 77
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 78
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 66
    invoke-super {p0, p1, p2, p3}, Lcom/actionbarsherlock/app/SherlockListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected onDataFetched()V
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->isDataFetched:Z

    .line 131
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->checkState()V

    .line 132
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .parameter
    .parameter "v"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 225
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->items:Ljava/util/List;

    invoke-interface {v4, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/lvi/Lvi;

    .line 226
    .local v1, item:Lnet/flixster/android/lvi/Lvi;
    invoke-virtual {v1}, Lnet/flixster/android/lvi/Lvi;->getItemViewType()I

    move-result v3

    .line 227
    .local v3, type:I
    sget-object v4, Lnet/flixster/android/lvi/LviAdapter$LviType;->MOVIE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v4}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v4

    if-ne v4, v3, :cond_1

    .line 228
    check-cast v1, Lnet/flixster/android/lvi/LviMovie;

    .end local v1           #item:Lnet/flixster/android/lvi/Lvi;
    iget-object v4, v1, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lnet/flixster/android/Starter;->launchMovieDetail(JLandroid/content/Context;)V

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 229
    .restart local v1       #item:Lnet/flixster/android/lvi/Lvi;
    :cond_1
    sget-object v4, Lnet/flixster/android/lvi/LviAdapter$LviType;->ACTOR:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v4}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v4

    if-ne v4, v3, :cond_2

    .line 230
    check-cast v1, Lnet/flixster/android/lvi/LviActor;

    .end local v1           #item:Lnet/flixster/android/lvi/Lvi;
    iget-object v0, v1, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    .line 231
    .local v0, actor:Lnet/flixster/android/model/Actor;
    iget-wide v4, v0, Lnet/flixster/android/model/Actor;->id:J

    iget-object v6, v0, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lnet/flixster/android/Starter;->launchActor(JLjava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 232
    .end local v0           #actor:Lnet/flixster/android/model/Actor;
    .restart local v1       #item:Lnet/flixster/android/lvi/Lvi;
    :cond_2
    sget-object v4, Lnet/flixster/android/lvi/LviAdapter$LviType;->LOAD_MORE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v4}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v4

    if-ne v4, v3, :cond_0

    .line 233
    check-cast v1, Lnet/flixster/android/lvi/LviLoadMore;

    .end local v1           #item:Lnet/flixster/android/lvi/Lvi;
    iget-object v2, v1, Lnet/flixster/android/lvi/LviLoadMore;->type:Lnet/flixster/android/lvi/LviLoadMore$Type;

    .line 234
    .local v2, loadMoreType:Lnet/flixster/android/lvi/LviLoadMore$Type;
    invoke-static {}, Lcom/flixster/android/activity/common/SearchFragment;->$SWITCH_TABLE$net$flixster$android$lvi$LviLoadMore$Type()[I

    move-result-object v4

    invoke-virtual {v2}, Lnet/flixster/android/lvi/LviLoadMore$Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 242
    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4}, Ljava/lang/IllegalStateException;-><init>()V

    throw v4

    .line 236
    :pswitch_0
    sget-object v4, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->SHOW_ALL_MOVIES:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    iput-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->displayMode:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    .line 246
    :goto_1
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->invalidateView()V

    goto :goto_0

    .line 239
    :pswitch_1
    sget-object v4, Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;->SHOW_ALL_ACTORS:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    iput-object v4, p0, Lcom/flixster/android/activity/common/SearchFragment;->displayMode:Lcom/flixster/android/activity/common/SearchFragment$DisplayMode;

    goto :goto_1

    .line 234
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;->onResume()V

    .line 83
    invoke-virtual {p0}, Lcom/flixster/android/activity/common/SearchFragment;->checkState()V

    .line 84
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;->onStop()V

    .line 89
    iget-object v0, p0, Lcom/flixster/android/activity/common/SearchFragment;->dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

    invoke-virtual {v0}, Lcom/flixster/android/activity/common/DaoHandler;->cancel()V

    .line 90
    return-void
.end method

.method public setQuery(Ljava/lang/String;)Lcom/flixster/android/activity/common/SearchFragment;
    .locals 0
    .parameter "query"

    .prologue
    .line 60
    iput-object p1, p0, Lcom/flixster/android/activity/common/SearchFragment;->query:Ljava/lang/String;

    .line 61
    return-object p0
.end method
