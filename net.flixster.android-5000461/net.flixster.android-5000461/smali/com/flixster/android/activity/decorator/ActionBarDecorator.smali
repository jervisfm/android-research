.class public Lcom/flixster/android/activity/decorator/ActionBarDecorator;
.super Ljava/lang/Object;
.source "ActionBarDecorator.java"

# interfaces
.implements Lcom/flixster/android/activity/decorator/ActivityDecorator;
.implements Lcom/actionbarsherlock/view/MenuItem$OnActionExpandListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private final activity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .parameter "activity"

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->activity:Landroid/app/Activity;

    .line 25
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/decorator/ActionBarDecorator;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method private showSoftKeyboard(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 104
    iget-object v1, p0, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->activity:Landroid/app/Activity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 105
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 106
    return-void
.end method


# virtual methods
.method public createCollapsibleSearch(Lcom/actionbarsherlock/view/Menu;)V
    .locals 4
    .parameter "menu"

    .prologue
    .line 46
    const v2, 0x7f0c0117

    invoke-interface {p1, v2}, Lcom/actionbarsherlock/view/Menu;->add(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    .line 47
    .local v0, searchActionItem:Lcom/actionbarsherlock/view/MenuItem;
    const v2, 0x7f0200cf

    invoke-interface {v0, v2}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    const v3, 0x7f030015

    invoke-interface {v2, v3}, Lcom/actionbarsherlock/view/MenuItem;->setActionView(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    .line 48
    invoke-interface {v2, p0}, Lcom/actionbarsherlock/view/MenuItem;->setOnActionExpandListener(Lcom/actionbarsherlock/view/MenuItem$OnActionExpandListener;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    .line 49
    const/16 v3, 0xa

    invoke-interface {v2, v3}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 51
    invoke-interface {v0}, Lcom/actionbarsherlock/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f070027

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 52
    .local v1, searchEditText:Landroid/widget/TextView;
    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 53
    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 54
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 55
    return-void
.end method

.method protected hideSoftKeyboard(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 109
    iget-object v1, p0, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->activity:Landroid/app/Activity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 110
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 111
    return-void
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 90
    const/4 v2, 0x2

    if-ne p2, v2, :cond_0

    .line 92
    invoke-virtual {p0, p1}, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->hideSoftKeyboard(Landroid/view/View;)V

    .line 93
    invoke-virtual {p1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/actionbarsherlock/view/MenuItem;

    .line 94
    .local v1, searchActionItem:Lcom/actionbarsherlock/view/MenuItem;
    invoke-interface {v1}, Lcom/actionbarsherlock/view/MenuItem;->collapseActionView()Z

    .line 95
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, query:Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v2, p0, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->activity:Landroid/app/Activity;

    invoke-static {v0, v2}, Lnet/flixster/android/Starter;->launchSearch(Ljava/lang/String;Landroid/content/Context;)V

    .line 98
    const/4 v2, 0x1

    .line 100
    .end local v0           #query:Ljava/lang/String;
    .end local v1           #searchActionItem:Lcom/actionbarsherlock/view/MenuItem;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .parameter "v"
    .parameter "hasFocus"

    .prologue
    .line 75
    if-eqz p2, :cond_0

    .line 76
    new-instance v0, Lcom/flixster/android/activity/decorator/ActionBarDecorator$1;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/activity/decorator/ActionBarDecorator$1;-><init>(Lcom/flixster/android/activity/decorator/ActionBarDecorator;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-virtual {p0, p1}, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->hideSoftKeyboard(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onMenuItemActionCollapse(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 68
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->hideSoftKeyboard(Landroid/view/View;)V

    .line 69
    const/4 v0, 0x1

    return v0
.end method

.method public onMenuItemActionExpand(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 3
    .parameter "item"

    .prologue
    .line 60
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f070027

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 61
    .local v0, searchEditText:Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 62
    const/4 v1, 0x1

    return v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method
