.class public Lcom/flixster/android/activity/decorator/DialogDecorator;
.super Ljava/lang/Object;
.source "DialogDecorator.java"

# interfaces
.implements Lcom/flixster/android/activity/decorator/ActivityDecorator;


# instance fields
.field private final activity:Landroid/app/Activity;

.field private lastDialog:Landroid/app/Dialog;

.field private lastDialogId:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .parameter "activity"

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/flixster/android/activity/decorator/DialogDecorator;->activity:Landroid/app/Activity;

    .line 16
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 0

    .prologue
    .line 20
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .parameter "id"

    .prologue
    .line 49
    iput p1, p0, Lcom/flixster/android/activity/decorator/DialogDecorator;->lastDialogId:I

    .line 50
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/DialogDecorator;->activity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/decorator/DialogDecorator;->lastDialog:Landroid/app/Dialog;

    .line 54
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/DialogDecorator;->lastDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V
    .locals 2
    .parameter "id"
    .parameter "listener"

    .prologue
    .line 37
    invoke-static {}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->instance()Lcom/flixster/android/view/DialogBuilder$DialogEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->removeListeners()V

    .line 38
    if-eqz p2, :cond_0

    .line 39
    invoke-static {}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->instance()Lcom/flixster/android/view/DialogBuilder$DialogEvents;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->addListener(Lcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/DialogDecorator;->lastDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/DialogDecorator;->activity:Landroid/app/Activity;

    iget v1, p0, Lcom/flixster/android/activity/decorator/DialogDecorator;->lastDialogId:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/activity/decorator/DialogDecorator;->lastDialog:Landroid/app/Dialog;

    .line 45
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/DialogDecorator;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->showDialog(I)V

    .line 46
    return-void
.end method
