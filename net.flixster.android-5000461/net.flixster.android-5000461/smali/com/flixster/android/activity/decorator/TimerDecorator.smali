.class public Lcom/flixster/android/activity/decorator/TimerDecorator;
.super Ljava/lang/Object;
.source "TimerDecorator.java"

# interfaces
.implements Lcom/flixster/android/activity/decorator/ActivityDecorator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/decorator/TimerDecorator$HandlerTimerTask;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .parameter "activity"

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->activity:Landroid/app/Activity;

    .line 17
    return-void
.end method


# virtual methods
.method public cancelTimer()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 68
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    .line 71
    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/flixster/android/activity/decorator/TimerDecorator;->cancelTimer()V

    .line 30
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method public scheduleTask(Landroid/os/Handler$Callback;J)V
    .locals 3
    .parameter "callback"
    .parameter "delay"

    .prologue
    .line 50
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/flixster/android/activity/decorator/TimerDecorator$HandlerTimerTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/flixster/android/activity/decorator/TimerDecorator$HandlerTimerTask;-><init>(Lcom/flixster/android/activity/decorator/TimerDecorator;Landroid/os/Handler$Callback;Lcom/flixster/android/activity/decorator/TimerDecorator$HandlerTimerTask;)V

    invoke-virtual {v0, v1, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 54
    return-void
.end method

.method public scheduleTask(Landroid/os/Handler$Callback;JJ)V
    .locals 6
    .parameter "callback"
    .parameter "delay"
    .parameter "period"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/flixster/android/activity/decorator/TimerDecorator$HandlerTimerTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/flixster/android/activity/decorator/TimerDecorator$HandlerTimerTask;-><init>(Lcom/flixster/android/activity/decorator/TimerDecorator;Landroid/os/Handler$Callback;Lcom/flixster/android/activity/decorator/TimerDecorator$HandlerTimerTask;)V

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 62
    return-void
.end method

.method public scheduleTask(Ljava/util/TimerTask;J)V
    .locals 1
    .parameter "task"
    .parameter "delay"

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/decorator/TimerDecorator;->timer:Ljava/util/Timer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 46
    return-void
.end method
