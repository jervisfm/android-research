.class public Lcom/flixster/android/activity/decorator/TopLevelDecorator;
.super Ljava/lang/Object;
.source "TopLevelDecorator.java"

# interfaces
.implements Lcom/flixster/android/activity/decorator/ActivityDecorator;


# static fields
.field private static resumeCtr:I


# instance fields
.field private final activity:Landroid/app/Activity;

.field protected className:Ljava/lang/String;

.field private volatile isPausing:Z

.field private level:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, -0x1

    sput v0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->resumeCtr:I

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .parameter "activity"

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->activity:Landroid/app/Activity;

    .line 25
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->className:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public static getResumeCtr()I
    .locals 1

    .prologue
    .line 65
    sget v0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->resumeCtr:I

    return v0
.end method

.method public static incrementResumeCtr()V
    .locals 1

    .prologue
    .line 70
    sget v0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->resumeCtr:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->resumeCtr:I

    .line 71
    return-void
.end method


# virtual methods
.method public isPausing()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->isPausing:Z

    return v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 30
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ActivityHolder;->setTopLevelActivity(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->level:I

    .line 33
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 53
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v0

    iget v1, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->level:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ActivityHolder;->removeTopLevelActivity(I)V

    .line 56
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 46
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->isPausing:Z

    .line 49
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 37
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onResume #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ActivityHolder;->setTopLevelActivity(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->level:I

    .line 40
    sget v0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->resumeCtr:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->resumeCtr:I

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->isPausing:Z

    .line 42
    return-void
.end method
