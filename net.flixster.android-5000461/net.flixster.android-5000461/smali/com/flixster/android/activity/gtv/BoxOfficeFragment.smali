.class public Lcom/flixster/android/activity/gtv/BoxOfficeFragment;
.super Lcom/flixster/android/activity/gtv/MovieGalleryFragment;
.source "BoxOfficeFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    const-string v0, "/boxoffice"

    return-object v0
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    const-string v0, "Box Office"

    return-object v0
.end method

.method protected getType()Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->BOX_OFFICE:Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;

    return-object v0
.end method
