.class public Lcom/flixster/android/activity/gtv/InstanceStateFragment;
.super Lcom/flixster/android/activity/gtv/ResultPassingFragment;
.source "InstanceStateFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final className:Ljava/lang/String;

.field private final hashCode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragment;-><init>()V

    .line 16
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->className:Ljava/lang/String;

    .line 17
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->hashCode:I

    .line 15
    return-void
.end method


# virtual methods
.method protected getSavedInstanceState()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 21
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->hashCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 39
    invoke-super {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragment;->onDestroy()V

    .line 40
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->hashCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 41
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    .line 26
    invoke-super {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragment;->onDestroyView()V

    .line 27
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 28
    .local v0, outState:Landroid/os/Bundle;
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->className:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->hashCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 29
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 30
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter "outState"

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/flixster/android/activity/gtv/ResultPassingFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 35
    return-void
.end method
