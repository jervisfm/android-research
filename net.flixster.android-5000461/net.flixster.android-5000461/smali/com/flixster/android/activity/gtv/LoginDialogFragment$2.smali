.class Lcom/flixster/android/activity/gtv/LoginDialogFragment$2;
.super Ljava/lang/Object;
.source "LoginDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/LoginDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$2;->this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 86
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 98
    :goto_0
    :pswitch_0
    return-void

    .line 88
    :pswitch_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->hasAcceptedTos()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$2;->this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;

    #getter for: Lcom/flixster/android/activity/gtv/LoginDialogFragment;->tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->access$0(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/flixster/android/view/DialogBuilder$DialogListener;->onPositiveButtonClick(I)V

    goto :goto_0

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$2;->this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$2;->this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;

    #getter for: Lcom/flixster/android/activity/gtv/LoginDialogFragment;->tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->access$0(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/view/DialogBuilder;->showTermsOfService(Landroid/app/Activity;Lcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 95
    :pswitch_2
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$2;->this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;

    #calls: Lcom/flixster/android/activity/gtv/LoginDialogFragment;->facebookLogin()V
    invoke-static {v0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->access$1(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)V

    goto :goto_0

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x7f0700eb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
