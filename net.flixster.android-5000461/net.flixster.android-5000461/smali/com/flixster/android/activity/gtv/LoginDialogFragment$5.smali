.class Lcom/flixster/android/activity/gtv/LoginDialogFragment$5;
.super Landroid/os/Handler;
.source "LoginDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/LoginDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$5;->this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;

    .line 145
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 148
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/data/DaoException;

    .line 149
    .local v0, de:Lnet/flixster/android/data/DaoException;
    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v2

    sget-object v3, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, error:Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$5;->this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;

    #getter for: Lcom/flixster/android/activity/gtv/LoginDialogFragment;->errorText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->access$3(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$5;->this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;

    #getter for: Lcom/flixster/android/activity/gtv/LoginDialogFragment;->errorText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->access$3(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)Landroid/widget/TextView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 152
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$5;->this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->throbber:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 153
    return-void

    .line 149
    .end local v1           #error:Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$5;->this$0:Lcom/flixster/android/activity/gtv/LoginDialogFragment;

    const v3, 0x7f0c0161

    invoke-virtual {v2, v3}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
