.class public Lcom/flixster/android/activity/gtv/LoginDialogFragment;
.super Lcom/flixster/android/activity/gtv/ResultPassingFragment;
.source "LoginDialogFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final className:Ljava/lang/String;

.field private emailText:Landroid/widget/EditText;

.field private errorText:Landroid/widget/TextView;

.field private facebookButton:Landroid/widget/Button;

.field private loginButton:Landroid/widget/Button;

.field private final loginErrorHandler:Landroid/os/Handler;

.field private final loginSuccessHandler:Landroid/os/Handler;

.field private final onClickListener:Landroid/view/View$OnClickListener;

.field private final onKeyListener:Landroid/view/View$OnKeyListener;

.field private passwordText:Landroid/widget/EditText;

.field protected throbber:Landroid/widget/ProgressBar;

.field private final tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragment;-><init>()V

    .line 32
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->className:Ljava/lang/String;

    .line 63
    new-instance v0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment$1;-><init>(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->onKeyListener:Landroid/view/View$OnKeyListener;

    .line 83
    new-instance v0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment$2;-><init>(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->onClickListener:Landroid/view/View$OnClickListener;

    .line 101
    new-instance v0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment$3;-><init>(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 130
    new-instance v0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment$4;-><init>(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->loginSuccessHandler:Landroid/os/Handler;

    .line 145
    new-instance v0, Lcom/flixster/android/activity/gtv/LoginDialogFragment$5;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment$5;-><init>(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->loginErrorHandler:Landroid/os/Handler;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->facebookLogin()V

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->flixsterLogin()V

    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/gtv/LoginDialogFragment;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->errorText:Landroid/widget/TextView;

    return-object v0
.end method

.method private facebookLogin()V
    .locals 4

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lnet/flixster/android/FacebookAuth;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 127
    const/16 v2, 0x7b

    .line 126
    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 128
    return-void
.end method

.method private flixsterLogin()V
    .locals 5

    .prologue
    .line 118
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->errorText:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->throbber:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 120
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->emailText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, email:Ljava/lang/String;
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->passwordText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 122
    .local v1, password:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->loginSuccessHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->loginErrorHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/flixster/android/data/AccountManager;->loginToFlixster(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 123
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/flixster/android/activity/gtv/ResultPassingFragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 47
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->className:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".onCreateView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const v1, 0x7f030043

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 49
    .local v0, rootView:Landroid/view/View;
    const v1, 0x7f0700eb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->loginButton:Landroid/widget/Button;

    .line 50
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->loginButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->loginButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 52
    const v1, 0x7f0700ed

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->facebookButton:Landroid/widget/Button;

    .line 53
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->facebookButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    const v1, 0x7f0700e8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->emailText:Landroid/widget/EditText;

    .line 55
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->emailText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 56
    const v1, 0x7f0700e9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->passwordText:Landroid/widget/EditText;

    .line 57
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->passwordText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 58
    const v1, 0x7f0700ec

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->errorText:Landroid/widget/TextView;

    .line 59
    const v1, 0x7f070039

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->throbber:Landroid/widget/ProgressBar;

    .line 60
    return-object v0
.end method

.method protected onFragmentResult(Ljava/lang/String;I)V
    .locals 2
    .parameter "requestClassName"
    .parameter "resultCode"

    .prologue
    .line 158
    const-class v0, Lnet/flixster/android/FacebookAuth;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/LoginDialogFragment;->loginSuccessHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 161
    :cond_0
    return-void
.end method
