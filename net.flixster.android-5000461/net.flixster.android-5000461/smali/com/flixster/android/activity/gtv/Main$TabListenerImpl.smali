.class Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;
.super Ljava/lang/Object;
.source "Main.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TabListenerImpl"
.end annotation


# instance fields
.field private final fragmentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/flixster/android/activity/gtv/Main;


# direct methods
.method private constructor <init>(Lcom/flixster/android/activity/gtv/Main;Ljava/lang/Class;)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p2, fragmentClass:Ljava/lang/Class;,"Ljava/lang/Class<+Landroid/app/Fragment;>;"
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;->this$0:Lcom/flixster/android/activity/gtv/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p2, p0, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;->fragmentClass:Ljava/lang/Class;

    .line 106
    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/activity/gtv/Main;Ljava/lang/Class;Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;-><init>(Lcom/flixster/android/activity/gtv/Main;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 134
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 6
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const v5, 0x10100a1

    aput v5, v3, v4

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 111
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    .line 112
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;->this$0:Lcom/flixster/android/activity/gtv/Main;

    #getter for: Lcom/flixster/android/activity/gtv/Main;->isActionBarReady:Z
    invoke-static {v2}, Lcom/flixster/android/activity/gtv/Main;->access$0(Lcom/flixster/android/activity/gtv/Main;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->setLastTab(Ljava/lang/String;)V

    .line 118
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;->fragmentClass:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 124
    .local v1, fragment:Landroid/app/Fragment;
    const v2, 0x7f0700b1

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v1, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 125
    return-void

    .line 119
    .end local v1           #fragment:Landroid/app/Fragment;
    :catch_0
    move-exception v0

    .line 120
    .local v0, e:Ljava/lang/InstantiationException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 121
    .end local v0           #e:Ljava/lang/InstantiationException;
    :catch_1
    move-exception v0

    .line 122
    .local v0, e:Ljava/lang/IllegalAccessException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 1
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 129
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;->this$0:Lcom/flixster/android/activity/gtv/Main;

    #calls: Lcom/flixster/android/activity/gtv/Main;->resetBackStack()V
    invoke-static {v0}, Lcom/flixster/android/activity/gtv/Main;->access$1(Lcom/flixster/android/activity/gtv/Main;)V

    .line 130
    return-void
.end method
