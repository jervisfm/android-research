.class public Lcom/flixster/android/activity/gtv/Main;
.super Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;
.source "Main.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;
    }
.end annotation


# static fields
.field private static final TAB_BOXOFFICE_RESID:I = 0x7f0c010f

.field private static final TAB_DVD_RESID:I = 0x7f0c0112

.field private static final TAB_SEARCH_RESID:I = 0x7f0c0048

.field private static final TAB_STORE_RESID:I = 0x7f0c0113

.field private static final TAB_UPCOMING_RESID:I = 0x7f0c0111


# instance fields
.field private isActionBarReady:Z

.field private mLeftNavBar:Lcom/google/tv/leftnavbar/LeftNavBar;

.field private skipRatePrompt:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/gtv/Main;)Z
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/flixster/android/activity/gtv/Main;->isActionBarReady:Z

    return v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/gtv/Main;)V
    .locals 0
    .parameter

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/Main;->resetBackStack()V

    return-void
.end method

.method private static getLastTab()I
    .locals 4

    .prologue
    .line 138
    const/4 v0, 0x1

    .line 139
    .local v0, defaultTabPosition:I
    const/4 v2, 0x1

    .line 140
    .local v2, tabPosition:I
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getLastTab()Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, tab:Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 143
    const/4 v3, 0x5

    if-lt v2, v3, :cond_0

    .line 144
    const/4 v2, 0x1

    .line 148
    :cond_0
    :goto_0
    return v2

    .line 146
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private getLeftNavBar()Lcom/google/tv/leftnavbar/LeftNavBar;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/Main;->mLeftNavBar:Lcom/google/tv/leftnavbar/LeftNavBar;

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Lcom/google/tv/leftnavbar/LeftNavBar;

    invoke-direct {v0, p0}, Lcom/google/tv/leftnavbar/LeftNavBar;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/Main;->mLeftNavBar:Lcom/google/tv/leftnavbar/LeftNavBar;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/Main;->mLeftNavBar:Lcom/google/tv/leftnavbar/LeftNavBar;

    return-object v0
.end method

.method private resetBackStack()V
    .locals 3

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "0"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 244
    return-void
.end method

.method private showDiagnosticsOrRateDialog()V
    .locals 4

    .prologue
    .line 158
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->hasUserSessionExpired()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 159
    const v2, 0x3b9acd88

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/flixster/android/activity/gtv/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    invoke-static {}, Lnet/flixster/android/util/ErrorHandler;->instance()Lnet/flixster/android/util/ErrorHandler;

    move-result-object v2

    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lnet/flixster/android/util/ErrorHandler;->checkForAppCrash(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .local v0, intent:Landroid/content/Intent;
    if-eqz v0, :cond_2

    .line 161
    move-object v1, v0

    .line 162
    .local v1, intentz:Landroid/content/Intent;
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/flixster/android/activity/gtv/Main;->skipRatePrompt:Z

    .line 163
    const v2, 0x3b9acde8

    new-instance v3, Lcom/flixster/android/activity/gtv/Main$1;

    invoke-direct {v3, p0, v1}, Lcom/flixster/android/activity/gtv/Main$1;-><init>(Lcom/flixster/android/activity/gtv/Main;Landroid/content/Intent;)V

    invoke-virtual {p0, v2, v3}, Lcom/flixster/android/activity/gtv/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 177
    .end local v1           #intentz:Landroid/content/Intent;
    :cond_2
    iget-boolean v2, p0, Lcom/flixster/android/activity/gtv/Main;->skipRatePrompt:Z

    if-nez v2, :cond_0

    .line 178
    invoke-static {p0}, Lcom/flixster/android/utils/AppRater;->showRateDialog(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method protected createActionBar()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 64
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/Main;->getLeftNavBar()Lcom/google/tv/leftnavbar/LeftNavBar;

    move-result-object v0

    .line 65
    .local v0, bar:Lcom/google/tv/leftnavbar/LeftNavBar;
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/google/tv/leftnavbar/LeftNavBar;->setNavigationMode(I)V

    .line 66
    invoke-virtual {v0, v7}, Lcom/google/tv/leftnavbar/LeftNavBar;->setDisplayShowHomeEnabled(Z)V

    .line 67
    invoke-virtual {v0, v7}, Lcom/google/tv/leftnavbar/LeftNavBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 68
    invoke-virtual {v0, v7}, Lcom/google/tv/leftnavbar/LeftNavBar;->setDisplayShowTitleEnabled(Z)V

    .line 69
    invoke-virtual {v0, v7}, Lcom/google/tv/leftnavbar/LeftNavBar;->showOptionsMenu(Z)V

    .line 71
    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0c0113

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f02014b

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setIcon(I)Landroid/app/ActionBar$Tab;

    move-result-object v4

    .line 72
    .local v4, storeTab:Landroid/app/ActionBar$Tab;
    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0c010f

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f02007f

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setIcon(I)Landroid/app/ActionBar$Tab;

    move-result-object v1

    .line 73
    .local v1, boxOfficeTab:Landroid/app/ActionBar$Tab;
    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0c0111

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0201d8

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setIcon(I)Landroid/app/ActionBar$Tab;

    move-result-object v5

    .line 74
    .local v5, upcomingTab:Landroid/app/ActionBar$Tab;
    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0c0112

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0200b4

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setIcon(I)Landroid/app/ActionBar$Tab;

    move-result-object v2

    .line 75
    .local v2, dvdTab:Landroid/app/ActionBar$Tab;
    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0c0048

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f020181

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setIcon(I)Landroid/app/ActionBar$Tab;

    move-result-object v3

    .line 77
    .local v3, searchTab:Landroid/app/ActionBar$Tab;
    new-instance v6, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;

    const-class v7, Lcom/flixster/android/activity/gtv/StoreFragment;

    invoke-direct {v6, p0, v7, v8}, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;-><init>(Lcom/flixster/android/activity/gtv/Main;Ljava/lang/Class;Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;)V

    invoke-virtual {v4, v6}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 78
    new-instance v6, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;

    const-class v7, Lcom/flixster/android/activity/gtv/BoxOfficeFragment;

    invoke-direct {v6, p0, v7, v8}, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;-><init>(Lcom/flixster/android/activity/gtv/Main;Ljava/lang/Class;Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;)V

    invoke-virtual {v1, v6}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 79
    new-instance v6, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;

    const-class v7, Lcom/flixster/android/activity/gtv/UpcomingFragment;

    invoke-direct {v6, p0, v7, v8}, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;-><init>(Lcom/flixster/android/activity/gtv/Main;Ljava/lang/Class;Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;)V

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 80
    new-instance v6, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;

    const-class v7, Lcom/flixster/android/activity/gtv/DvdFragment;

    invoke-direct {v6, p0, v7, v8}, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;-><init>(Lcom/flixster/android/activity/gtv/Main;Ljava/lang/Class;Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;)V

    invoke-virtual {v2, v6}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 81
    new-instance v6, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;

    const-class v7, Lcom/flixster/android/activity/gtv/SearchFragment;

    invoke-direct {v6, p0, v7, v8}, Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;-><init>(Lcom/flixster/android/activity/gtv/Main;Ljava/lang/Class;Lcom/flixster/android/activity/gtv/Main$TabListenerImpl;)V

    invoke-virtual {v3, v6}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 83
    invoke-virtual {v0, v4}, Lcom/google/tv/leftnavbar/LeftNavBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 84
    invoke-virtual {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 85
    invoke-virtual {v0, v5}, Lcom/google/tv/leftnavbar/LeftNavBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 86
    invoke-virtual {v0, v2}, Lcom/google/tv/leftnavbar/LeftNavBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 87
    invoke-virtual {v0, v3}, Lcom/google/tv/leftnavbar/LeftNavBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 89
    invoke-static {}, Lcom/flixster/android/activity/gtv/Main;->getLastTab()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/google/tv/leftnavbar/LeftNavBar;->setSelectedNavigationItem(I)V

    .line 90
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/flixster/android/activity/gtv/Main;->isActionBarReady:Z

    .line 91
    return-void
.end method

.method protected createShareIntent()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 288
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 289
    .local v0, shareIntent:Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    const-string v1, "android.intent.extra.TEXT"

    const v2, 0x7f0c01b2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->mobileUpsell()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/flixster/android/activity/gtv/Main;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 208
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/16 v0, 0x7b

    if-ne p1, v0, :cond_0

    .line 209
    const-class v0, Lnet/flixster/android/FacebookAuth;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/flixster/android/activity/gtv/Main;->setFragmentResult(Ljava/lang/String;I)V

    .line 211
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/Main;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/flixster/android/activity/gtv/Main;->skipRatePrompt:Z

    .line 52
    const v0, 0x7f030033

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/gtv/Main;->setContentView(I)V

    .line 54
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/Main;->createActionBar()V

    .line 57
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/Main;->showDiagnosticsOrRateDialog()V

    .line 60
    invoke-static {}, Lcom/flixster/android/bootstrap/Startup;->instance()Lcom/flixster/android/bootstrap/Startup;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/flixster/android/bootstrap/Startup;->onStartup(Landroid/app/Activity;)V

    .line 61
    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/Main;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0009

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 249
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 202
    invoke-super {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->onDestroy()V

    .line 203
    const-string v0, "FlxMain"

    const-string v1, "Main.onDestroy"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 254
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 283
    invoke-super {p0, p1}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 256
    :sswitch_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isAdminEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257
    invoke-static {p0}, Lnet/flixster/android/Starter;->launchSettings(Landroid/content/Context;)V

    goto :goto_0

    .line 259
    :cond_0
    new-instance v1, Lcom/flixster/android/activity/gtv/SettingsDialogFragment;

    invoke-direct {v1}, Lcom/flixster/android/activity/gtv/SettingsDialogFragment;-><init>()V

    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "settings_fragment"

    invoke-virtual {v1, v2, v3}, Lcom/flixster/android/activity/gtv/SettingsDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 263
    :sswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/main/phone"

    const-string v3, "Main - Phone"

    const-string v4, "MainPhoneActionBar"

    const-string v5, "TellAFriend"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/Main;->createShareIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "Share via"

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/gtv/Main;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 268
    :sswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/main/phone"

    const-string v3, "Main - Phone"

    const-string v4, "MainPhoneActionBar"

    const-string v5, "RateThisApp"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-static {p0}, Lcom/flixster/android/utils/AppRater;->rateThisApp(Landroid/content/Context;)V

    goto :goto_0

    .line 273
    :sswitch_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/main/phone"

    const-string v3, "Main - Phone"

    const-string v4, "MainPhoneActionBar"

    const-string v5, "FAQ"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->faq()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p0}, Lnet/flixster/android/Starter;->launchBrowser(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 278
    :sswitch_4
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/main/phone"

    const-string v3, "Main - Phone"

    const-string v4, "MainPhoneActionBar"

    const-string v5, "Feedback"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->feedback()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {p0, v2}, Lcom/flixster/android/activity/gtv/Main;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p0}, Lnet/flixster/android/Starter;->launchFlxHtmlPage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 254
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0702fb -> :sswitch_0
        0x7f070303 -> :sswitch_1
        0x7f070304 -> :sswitch_2
        0x7f070305 -> :sswitch_3
        0x7f070306 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 190
    invoke-super {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->onPause()V

    .line 191
    const-string v0, "FlxMain"

    const-string v1, "Main.onPause"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 184
    invoke-super {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->onResume()V

    .line 185
    const-string v0, "FlxMain"

    const-string v1, "Main.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/flixster/android/activity/gtv/Main;->skipRatePrompt:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 196
    invoke-super {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->onStop()V

    .line 197
    const-string v0, "FlxMain"

    const-string v1, "Main.onStop"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    return-void
.end method

.method protected removeTopFragment()V
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 240
    return-void
.end method

.method protected startFragment(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 6
    .parameter
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Fragment;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 222
    .local p1, fragmentClass:Ljava/lang/Class;,"Ljava/lang/Class<+Landroid/app/Fragment;>;"
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Fragment;

    .line 223
    .local v4, newFragment:Landroid/app/Fragment;
    invoke-virtual {v4, p2}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 229
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 230
    .local v2, fm:Landroid/app/FragmentManager;
    invoke-virtual {v2}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    .line 231
    .local v0, backStackCount:I
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 232
    .local v3, ft:Landroid/app/FragmentTransaction;
    const v5, 0x7f0700b1

    invoke-virtual {v3, v5, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 233
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 234
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    .line 235
    return-void

    .line 224
    .end local v0           #backStackCount:I
    .end local v2           #fm:Landroid/app/FragmentManager;
    .end local v3           #ft:Landroid/app/FragmentTransaction;
    .end local v4           #newFragment:Landroid/app/Fragment;
    :catch_0
    move-exception v1

    .line 225
    .local v1, e:Ljava/lang/InstantiationException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 226
    .end local v1           #e:Ljava/lang/InstantiationException;
    :catch_1
    move-exception v1

    .line 227
    .local v1, e:Ljava/lang/IllegalAccessException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method
