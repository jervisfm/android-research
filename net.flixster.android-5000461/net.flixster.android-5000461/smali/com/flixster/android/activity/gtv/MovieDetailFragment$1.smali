.class Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;
.super Lcom/flixster/android/activity/common/DaoHandler;
.source "MovieDetailFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/MovieDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    .line 109
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DaoHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    const/16 v3, 0x8

    .line 111
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->isMsgSuccess()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 112
    const-string v1, "FlxMain"

    const-string v2, "MovieDetailFragment.dataHandler success"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 114
    .local v0, hostContext:Landroid/content/Context;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    .end local v0           #hostContext:Landroid/content/Context;
    :cond_0
    :goto_0
    return-void

    .line 117
    .restart local v0       #hostContext:Landroid/content/Context;
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$0(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 118
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->getMsgObj()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    #setter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->movie:Lnet/flixster/android/model/Movie;
    invoke-static {v3, v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$1(Lcom/flixster/android/activity/gtv/MovieDetailFragment;Lnet/flixster/android/model/Movie;)V

    #calls: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->populateFragment(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V
    invoke-static {v2, v1, v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$2(Lcom/flixster/android/activity/gtv/MovieDetailFragment;Lnet/flixster/android/model/Movie;Landroid/content/Context;)V

    .line 119
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #calls: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->focusSelf()V
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$3(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)V

    goto :goto_0

    .line 121
    .end local v0           #hostContext:Landroid/content/Context;
    :cond_2
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$0(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 122
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->getException()Lnet/flixster/android/data/DaoException;

    move-result-object v2

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/gtv/Main;

    invoke-static {v2, v1}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedActivity;)V

    goto :goto_0
.end method
