.class Lcom/flixster/android/activity/gtv/MovieDetailFragment$2;
.super Ljava/lang/Object;
.source "MovieDetailFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/MovieDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$2;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 136
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 149
    :goto_0
    return-void

    .line 138
    :pswitch_0
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$2;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/Main;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/Main;->removeTopFragment()V

    goto :goto_0

    .line 141
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/trailer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trailer - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$2;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->movieTitle:Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$4(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$2;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->movie:Lnet/flixster/android/model/Movie;
    invoke-static {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$5(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v0

    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->getTrailerHigh()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$2;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lnet/flixster/android/Starter;->launchVideo(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 146
    :pswitch_2
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$2;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$6(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/flixster/android/drm/PlaybackManager;->playMovie(Lnet/flixster/android/model/LockerRight;)V

    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x7f07011f
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
