.class Lcom/flixster/android/activity/gtv/MovieDetailFragment$3;
.super Ljava/lang/Object;
.source "MovieDetailFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/MovieDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$3;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 205
    const/4 v0, 0x0

    .line 206
    .local v0, isHandled:Z
    const/4 v1, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 207
    sparse-switch p2, :sswitch_data_0

    .line 233
    :cond_0
    :goto_0
    return v0

    .line 209
    :sswitch_0
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$3;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$7(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->moveFocusLeft()Z

    move-result v0

    .line 210
    goto :goto_0

    .line 212
    :sswitch_1
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$3;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$7(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->moveFocusRight()Z

    move-result v0

    .line 213
    goto :goto_0

    .line 217
    :sswitch_2
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$3;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$7(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->performClick()V

    .line 218
    const/4 v0, 0x1

    goto :goto_0

    .line 221
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 222
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 224
    :pswitch_0
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$3;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$7(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->canMoveFocusLeft()Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$3;->this$0:Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->access$7(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->loseFocusToLeft()V

    goto :goto_0

    .line 207
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x17 -> :sswitch_2
        0x3e -> :sswitch_2
        0x42 -> :sswitch_2
    .end sparse-switch

    .line 222
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
    .end packed-switch
.end method
