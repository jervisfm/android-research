.class Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;
.super Ljava/lang/Object;
.source "MovieDetailFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/MovieDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FakeFocusButton"
.end annotation


# instance fields
.field private isFakeFocused:Z

.field private final view:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .locals 0
    .parameter "view"

    .prologue
    .line 307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->view:Landroid/widget/TextView;

    .line 309
    return-void
.end method


# virtual methods
.method clearFakeFocus()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->view:Landroid/widget/TextView;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->isFakeFocused:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPressed(Z)V

    .line 321
    return-void
.end method

.method fakeFocus()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->view:Landroid/widget/TextView;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->isFakeFocused:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPressed(Z)V

    .line 317
    return-void
.end method

.method isFakeFocused()Z
    .locals 1

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->isFakeFocused:Z

    return v0
.end method

.method performClick()V
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->view:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->performClick()Z

    .line 325
    return-void
.end method
