.class Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;
.super Ljava/lang/Object;
.source "MovieDetailFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/MovieDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FakeFocusButtonManager"
.end annotation


# instance fields
.field private final buttons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;",
            ">;"
        }
    .end annotation
.end field

.field private currFocusPos:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    .line 239
    const/4 v0, -0x1

    iput v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    .line 237
    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;)V
    .locals 0
    .parameter

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;-><init>()V

    return-void
.end method


# virtual methods
.method addButton(Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;)V
    .locals 1
    .parameter "button"

    .prologue
    .line 243
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    return-void
.end method

.method canMoveFocusLeft()Z
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method canMoveFocusRight()Z
    .locals 2

    .prologue
    .line 264
    iget v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method loseFocusToLeft()V
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->clearFakeFocus()V

    .line 291
    const/4 v0, -0x1

    iput v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    .line 292
    return-void
.end method

.method moveFocusLeft()Z
    .locals 2

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->canMoveFocusLeft()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    iget v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->clearFakeFocus()V

    .line 270
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    iget v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->fakeFocus()V

    .line 271
    const/4 v0, 0x1

    .line 273
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method moveFocusRight()Z
    .locals 2

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->canMoveFocusRight()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279
    iget v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 280
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    iget v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->clearFakeFocus()V

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    iget v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->fakeFocus()V

    .line 283
    const/4 v0, 0x1

    .line 285
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method performClick()V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    iget v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->performClick()V

    .line 300
    return-void
.end method

.method restoreFocus()V
    .locals 3

    .prologue
    .line 252
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 257
    return-void

    .line 252
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    .line 253
    .local v0, button:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;
    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->isFakeFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 254
    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->fakeFocus()V

    goto :goto_0
.end method

.method setFocusRightmost()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    .line 248
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->buttons:Ljava/util/List;

    iget v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->currFocusPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;->fakeFocus()V

    .line 249
    return-void
.end method
