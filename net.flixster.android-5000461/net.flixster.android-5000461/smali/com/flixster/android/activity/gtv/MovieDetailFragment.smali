.class public Lcom/flixster/android/activity/gtv/MovieDetailFragment;
.super Landroid/app/Fragment;
.source "MovieDetailFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;,
        Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;
    }
.end annotation


# static fields
.field private static final MAX_REVIEW_COUNT:I = 0xa


# instance fields
.field private back:Landroid/widget/TextView;

.field private final dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

.field private fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

.field private movie:Lnet/flixster/android/model/Movie;

.field private movieTitle:Ljava/lang/String;

.field private final onClickListener:Landroid/view/View$OnClickListener;

.field private final onKeyListener:Landroid/view/View$OnKeyListener;

.field private right:Lnet/flixster/android/model/LockerRight;

.field private rootLayout:Landroid/view/View;

.field private scrollView:Landroid/view/View;

.field private scrollViewLayout:Landroid/widget/LinearLayout;

.field private throbber:Landroid/widget/ProgressBar;

.field private title:Landroid/widget/TextView;

.field private trailer:Landroid/widget/TextView;

.field private watch:Landroid/widget/TextView;

.field private year:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 109
    new-instance v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$1;-><init>(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

    .line 133
    new-instance v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$2;-><init>(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->onClickListener:Landroid/view/View$OnClickListener;

    .line 202
    new-instance v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$3;-><init>(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->onKeyListener:Landroid/view/View$OnKeyListener;

    .line 40
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->throbber:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/gtv/MovieDetailFragment;Lnet/flixster/android/model/Movie;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->movie:Lnet/flixster/android/model/Movie;

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/gtv/MovieDetailFragment;Lnet/flixster/android/model/Movie;Landroid/content/Context;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 152
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->populateFragment(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->focusSelf()V

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->movieTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->movie:Lnet/flixster/android/model/Movie;

    return-object v0
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Lnet/flixster/android/model/LockerRight;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->right:Lnet/flixster/android/model/LockerRight;

    return-object v0
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/gtv/MovieDetailFragment;)Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    return-object v0
.end method

.method private focusSelf()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->scrollView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 130
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->setFocusRightmost()V

    .line 131
    return-void
.end method

.method private populateFragment(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V
    .locals 12
    .parameter "movie"
    .parameter "context"

    .prologue
    const/4 v11, 0x0

    .line 154
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTrailerHigh()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 155
    iget-object v8, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->trailer:Landroid/widget/TextView;

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    iget-object v8, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    new-instance v9, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    iget-object v10, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->trailer:Landroid/widget/TextView;

    invoke-direct {v9, v10}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v8, v9}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->addButton(Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;)V

    .line 161
    :cond_0
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getReleaseYear()Ljava/lang/String;

    move-result-object v5

    .line 162
    .local v5, releaseYear:Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 163
    iget-object v8, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->year:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    :cond_1
    new-instance v1, Lcom/flixster/android/view/gtv/MovieDetailHeader;

    invoke-direct {v1, p2}, Lcom/flixster/android/view/gtv/MovieDetailHeader;-><init>(Landroid/content/Context;)V

    .line 166
    .local v1, detailHeader:Lcom/flixster/android/view/gtv/MovieDetailHeader;
    iget-object v8, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->scrollViewLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v1, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 167
    iget-object v8, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v1, p1, v8}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->load(Lnet/flixster/android/model/Movie;Lnet/flixster/android/model/LockerRight;)V

    .line 170
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getCriticReviewsList()Ljava/util/ArrayList;

    move-result-object v6

    .line 171
    .local v6, reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    if-eqz v6, :cond_2

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 172
    new-instance v2, Lcom/flixster/android/view/gtv/Subheader;

    invoke-direct {v2, p2}, Lcom/flixster/android/view/gtv/Subheader;-><init>(Landroid/content/Context;)V

    .line 173
    .local v2, header:Landroid/widget/TextView;
    const v8, 0x7f0c0128

    invoke-virtual {p0, v8}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v8, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->scrollViewLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 175
    const/16 v8, 0xa

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 176
    .local v7, size:I
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    if-lt v3, v7, :cond_4

    .line 185
    .end local v2           #header:Landroid/widget/TextView;
    .end local v3           #i:I
    .end local v7           #size:I
    :cond_2
    iget-object v8, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v8, :cond_3

    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v8

    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->right:Lnet/flixster/android/model/LockerRight;

    invoke-interface {v8, v9}, Lcom/flixster/android/drm/PlaybackLogic;->isAssetPlayable(Lnet/flixster/android/model/LockerRight;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 186
    iget-object v8, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->watch:Landroid/widget/TextView;

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    iget-object v8, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    new-instance v9, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    iget-object v10, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->watch:Landroid/widget/TextView;

    invoke-direct {v9, v10}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v8, v9}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->addButton(Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;)V

    .line 189
    :cond_3
    return-void

    .line 177
    .restart local v2       #header:Landroid/widget/TextView;
    .restart local v3       #i:I
    .restart local v7       #size:I
    :cond_4
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Review;

    .line 178
    .local v4, r:Lnet/flixster/android/model/Review;
    new-instance v0, Lcom/flixster/android/view/gtv/CriticReview;

    invoke-direct {v0, p2}, Lcom/flixster/android/view/gtv/CriticReview;-><init>(Landroid/content/Context;)V

    .line 179
    .local v0, cr:Lcom/flixster/android/view/gtv/CriticReview;
    iget-object v8, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->scrollViewLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 180
    invoke-virtual {v0, v4}, Lcom/flixster/android/view/gtv/CriticReview;->load(Lnet/flixster/android/model/Review;)V

    .line 176
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 10
    .parameter "savedInstanceState"

    .prologue
    const-wide/16 v8, 0x0

    .line 85
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 87
    .local v0, args:Landroid/os/Bundle;
    const-wide/16 v1, 0x0

    .line 88
    .local v1, id:J
    if-eqz v0, :cond_1

    .line 89
    const-string v6, "net.flixster.android.EXTRA_TITLE"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->movieTitle:Ljava/lang/String;

    .line 90
    iget-object v6, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->movieTitle:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 91
    iget-object v6, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->title:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->movieTitle:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    :cond_0
    const-string v6, "net.flixster.android.EXTRA_ID"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 94
    const-string v6, "net.flixster.android.EXTRA_RIGHT_ID"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 95
    .local v3, rightId:J
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v5

    .line 96
    .local v5, user:Lnet/flixster/android/model/User;
    if-eqz v5, :cond_1

    cmp-long v6, v3, v8

    if-lez v6, :cond_1

    .line 97
    invoke-virtual {v5, v3, v4}, Lnet/flixster/android/model/User;->getLockerRightFromRightId(J)Lnet/flixster/android/model/LockerRight;

    move-result-object v6

    iput-object v6, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->right:Lnet/flixster/android/model/LockerRight;

    .line 102
    .end local v3           #rightId:J
    .end local v5           #user:Lnet/flixster/android/model/User;
    :cond_1
    cmp-long v6, v1, v8

    if-nez v6, :cond_2

    .line 103
    new-instance v6, Ljava/lang/AssertionError;

    const-string v7, "MovieDetailFragment missing id"

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 105
    :cond_2
    iget-object v6, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->throbber:Landroid/widget/ProgressBar;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 106
    iget-object v6, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->dataHandler:Lcom/flixster/android/activity/common/DaoHandler;

    invoke-static {v1, v2, v6}, Lcom/flixster/android/data/MovieDaoNew;->fetchMovie(JLcom/flixster/android/activity/common/DaoHandler;)V

    .line 107
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 58
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 62
    const v0, 0x7f030051

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->rootLayout:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->rootLayout:Landroid/view/View;

    const v1, 0x7f07011b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->scrollView:Landroid/view/View;

    .line 64
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->scrollView:Landroid/view/View;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 65
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->rootLayout:Landroid/view/View;

    const v1, 0x7f07011d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->scrollViewLayout:Landroid/widget/LinearLayout;

    .line 66
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->rootLayout:Landroid/view/View;

    const v1, 0x7f070119

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->title:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->rootLayout:Landroid/view/View;

    const v1, 0x7f07011a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->year:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->rootLayout:Landroid/view/View;

    const v1, 0x7f070039

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->throbber:Landroid/widget/ProgressBar;

    .line 70
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->rootLayout:Landroid/view/View;

    const v1, 0x7f07011f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->back:Landroid/widget/TextView;

    .line 71
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->rootLayout:Landroid/view/View;

    const v1, 0x7f070120

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->trailer:Landroid/widget/TextView;

    .line 72
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->rootLayout:Landroid/view/View;

    const v1, 0x7f070121

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->watch:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->back:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->trailer:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->watch:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    new-instance v0, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;-><init>(Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    .line 78
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    new-instance v1, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->back:Landroid/widget/TextView;

    invoke-direct {v1, v2}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->addButton(Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButton;)V

    .line 80
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->rootLayout:Landroid/view/View;

    return-object v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 193
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 194
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->fakeButtonManager:Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/MovieDetailFragment$FakeFocusButtonManager;->restoreFocus()V

    .line 195
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/movie/info"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Movie Info - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/MovieDetailFragment;->movieTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    return-void
.end method
