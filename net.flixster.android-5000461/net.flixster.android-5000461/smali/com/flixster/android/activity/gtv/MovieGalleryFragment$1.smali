.class Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;
.super Landroid/os/Handler;
.source "MovieGalleryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/MovieGalleryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    .line 97
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .parameter "msg"

    .prologue
    const/4 v12, 0x0

    .line 100
    const-string v9, "FlxMain"

    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v11, v11, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->className:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, ".successHandler"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    invoke-virtual {v9}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 102
    .local v2, hostContext:Landroid/content/Context;
    if-eqz v2, :cond_0

    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    invoke-virtual {v9}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->isRemoving()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v9, v9, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->throbber:Landroid/widget/ProgressBar;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 106
    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v9, v9, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 108
    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;
    invoke-static {v9, v10}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->access$0(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;Ljava/util/List;)V

    .line 109
    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v9, v9, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->movies:Ljava/util/Collection;

    invoke-interface {v9}, Ljava/util/Collection;->size()I

    move-result v9

    if-lez v9, :cond_6

    .line 110
    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v9, v9, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->movies:Ljava/util/Collection;

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_4

    .line 144
    :cond_3
    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    #calls: Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->focusSelf()V
    invoke-static {v9}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->access$2(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)V

    goto :goto_0

    .line 110
    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/model/NamedList;

    .line 111
    .local v4, section:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/Movie;>;"
    invoke-virtual {v4}, Lcom/flixster/android/model/NamedList;->getName()Ljava/lang/String;

    move-result-object v7

    .line 112
    .local v7, sectionName:Ljava/lang/String;
    invoke-virtual {v4}, Lcom/flixster/android/model/NamedList;->getList()Ljava/util/List;

    move-result-object v6

    .line 113
    .local v6, sectionList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    if-eqz v10, :cond_2

    .line 116
    if-eqz v7, :cond_5

    .line 117
    new-instance v8, Lcom/flixster/android/view/gtv/Subheader;

    invoke-direct {v8, v2}, Lcom/flixster/android/view/gtv/Subheader;-><init>(Landroid/content/Context;)V

    .line 118
    .local v8, title:Landroid/widget/TextView;
    iget-object v10, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    invoke-virtual {v10, v4, v2}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->createSectionTitle(Lcom/flixster/android/model/NamedList;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v10, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v10, v10, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 121
    .end local v8           #title:Landroid/widget/TextView;
    :cond_5
    new-instance v1, Lcom/flixster/android/view/gtv/MovieGallery;

    invoke-direct {v1, v2}, Lcom/flixster/android/view/gtv/MovieGallery;-><init>(Landroid/content/Context;)V

    .line 122
    .local v1, gallery:Lcom/flixster/android/view/gtv/MovieGallery;,"Lcom/flixster/android/view/gtv/MovieGallery<Lnet/flixster/android/model/Movie;>;"
    new-instance v10, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;

    iget-object v11, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    invoke-direct {v10, v11, v12}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;-><init>(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;)V

    invoke-virtual {v1, v6, v10}, Lcom/flixster/android/view/gtv/MovieGallery;->load(Ljava/util/List;Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;)V

    .line 123
    iget-object v10, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v10, v10, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 124
    iget-object v10, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;
    invoke-static {v10}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->access$1(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 126
    .end local v1           #gallery:Lcom/flixster/android/view/gtv/MovieGallery;,"Lcom/flixster/android/view/gtv/MovieGallery<Lnet/flixster/android/model/Movie;>;"
    .end local v4           #section:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/Movie;>;"
    .end local v6           #sectionList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .end local v7           #sectionName:Ljava/lang/String;
    :cond_6
    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v9, v9, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rights:Ljava/util/Collection;

    invoke-interface {v9}, Ljava/util/Collection;->size()I

    move-result v9

    if-lez v9, :cond_3

    .line 127
    iget-object v9, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v9, v9, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rights:Ljava/util/Collection;

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_7
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/model/NamedList;

    .line 128
    .local v3, section:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/LockerRight;>;"
    invoke-virtual {v3}, Lcom/flixster/android/model/NamedList;->getName()Ljava/lang/String;

    move-result-object v7

    .line 129
    .restart local v7       #sectionName:Ljava/lang/String;
    invoke-virtual {v3}, Lcom/flixster/android/model/NamedList;->getList()Ljava/util/List;

    move-result-object v5

    .line 130
    .local v5, sectionList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    if-eqz v10, :cond_7

    .line 133
    if-eqz v7, :cond_8

    .line 134
    new-instance v8, Lcom/flixster/android/view/gtv/Subheader;

    invoke-direct {v8, v2}, Lcom/flixster/android/view/gtv/Subheader;-><init>(Landroid/content/Context;)V

    .line 135
    .restart local v8       #title:Landroid/widget/TextView;
    iget-object v10, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    invoke-virtual {v10, v3, v2}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->createSectionTitle(Lcom/flixster/android/model/NamedList;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v10, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v10, v10, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 138
    .end local v8           #title:Landroid/widget/TextView;
    :cond_8
    new-instance v0, Lcom/flixster/android/view/gtv/MovieGallery;

    invoke-direct {v0, v2}, Lcom/flixster/android/view/gtv/MovieGallery;-><init>(Landroid/content/Context;)V

    .line 139
    .local v0, gallery:Lcom/flixster/android/view/gtv/MovieGallery;,"Lcom/flixster/android/view/gtv/MovieGallery<Lnet/flixster/android/model/LockerRight;>;"
    new-instance v10, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;

    iget-object v11, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    invoke-direct {v10, v11, v12}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;-><init>(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;)V

    invoke-virtual {v0, v5, v10}, Lcom/flixster/android/view/gtv/MovieGallery;->load(Ljava/util/List;Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;)V

    .line 140
    iget-object v10, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v10, v10, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 141
    iget-object v10, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    #getter for: Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;
    invoke-static {v10}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->access$1(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method
