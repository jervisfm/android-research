.class Lcom/flixster/android/activity/gtv/MovieGalleryFragment$2;
.super Landroid/os/Handler;
.source "MovieGalleryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/MovieGalleryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$2;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    .line 148
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "msg"

    .prologue
    .line 151
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$2;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->throbber:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 152
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lnet/flixster/android/data/DaoException;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/data/DaoException;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$2;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/gtv/Main;

    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedActivity;)V

    .line 155
    :cond_0
    return-void
.end method
