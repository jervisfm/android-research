.class Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;
.super Ljava/lang/Object;
.source "MovieGalleryFragment.java"

# interfaces
.implements Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/MovieGalleryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultMovieClickListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;


# direct methods
.method private constructor <init>(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 166
    .local p0, this:Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;,"Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener<TT;>;"
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;-><init>(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)V

    return-void
.end method


# virtual methods
.method public onClick(Ljava/lang/Object;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p0, this:Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;,"Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener<TT;>;"
    .local p1, m:Ljava/lang/Object;,"TT;"
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;->this$0:Lcom/flixster/android/activity/gtv/MovieGalleryFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/gtv/Main;

    .line 170
    .local v1, main:Lcom/flixster/android/activity/gtv/Main;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 171
    .local v0, args:Landroid/os/Bundle;
    instance-of v2, p1, Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_0

    .line 172
    const-string v3, "net.flixster.android.EXTRA_ID"

    move-object v2, p1

    check-cast v2, Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 173
    const-string v2, "net.flixster.android.EXTRA_TITLE"

    check-cast p1, Lnet/flixster/android/model/Movie;

    .end local p1           #m:Ljava/lang/Object;,"TT;"
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :goto_0
    const-class v2, Lcom/flixster/android/activity/gtv/MovieDetailFragment;

    invoke-virtual {v1, v2, v0}, Lcom/flixster/android/activity/gtv/Main;->startFragment(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 182
    return-void

    .line 174
    .restart local p1       #m:Ljava/lang/Object;,"TT;"
    :cond_0
    instance-of v2, p1, Lnet/flixster/android/model/LockerRight;

    if-eqz v2, :cond_1

    .line 175
    const-string v3, "net.flixster.android.EXTRA_ID"

    move-object v2, p1

    check-cast v2, Lnet/flixster/android/model/LockerRight;

    iget-wide v4, v2, Lnet/flixster/android/model/LockerRight;->assetId:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 176
    const-string v3, "net.flixster.android.EXTRA_RIGHT_ID"

    move-object v2, p1

    check-cast v2, Lnet/flixster/android/model/LockerRight;

    iget-wide v4, v2, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 177
    const-string v2, "net.flixster.android.EXTRA_TITLE"

    check-cast p1, Lnet/flixster/android/model/LockerRight;

    .end local p1           #m:Ljava/lang/Object;,"TT;"
    invoke-virtual {p1}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 179
    .restart local p1       #m:Ljava/lang/Object;,"TT;"
    :cond_1
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2
.end method
