.class public abstract Lcom/flixster/android/activity/gtv/MovieGalleryFragment;
.super Lcom/flixster/android/activity/gtv/InstanceStateFragment;
.source "MovieGalleryFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/gtv/MovieGalleryFragment$DefaultMovieClickListener;,
        Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$flixster$android$activity$gtv$MovieGalleryFragment$Type:[I = null

.field private static final KEY_SELECTED_POSITIONS:Ljava/lang/String; = "KEY_SELECTED_POSITIONS"


# instance fields
.field protected final className:Ljava/lang/String;

.field protected final errorHandler:Landroid/os/Handler;

.field private galleries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/view/gtv/MovieGallery",
            "<*>;>;"
        }
    .end annotation
.end field

.field protected final movies:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/model/NamedList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;>;"
        }
    .end annotation
.end field

.field private positions:[I

.field protected final rights:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/model/NamedList",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;>;"
        }
    .end annotation
.end field

.field protected rootLayout:Landroid/widget/LinearLayout;

.field protected final successHandler:Landroid/os/Handler;

.field protected throbber:Landroid/widget/ProgressBar;


# direct methods
.method static synthetic $SWITCH_TABLE$com$flixster$android$activity$gtv$MovieGalleryFragment$Type()[I
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->$SWITCH_TABLE$com$flixster$android$activity$gtv$MovieGalleryFragment$Type:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->values()[Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->BOX_OFFICE:Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->DVD:Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->SEARCH:Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->STORE:Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->UPCOMING:Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->$SWITCH_TABLE$com$flixster$android$activity$gtv$MovieGalleryFragment$Type:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/InstanceStateFragment;-><init>()V

    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->className:Ljava/lang/String;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->movies:Ljava/util/Collection;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rights:Ljava/util/Collection;

    .line 97
    new-instance v0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$1;-><init>(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->successHandler:Landroid/os/Handler;

    .line 148
    new-instance v0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$2;-><init>(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->errorHandler:Landroid/os/Handler;

    .line 34
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/gtv/MovieGalleryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->focusSelf()V

    return-void
.end method

.method private focusSelf()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 160
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->positions:[I

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/view/gtv/MovieGallery;

    invoke-virtual {v0}, Lcom/flixster/android/view/gtv/MovieGallery;->requestFocus()Z

    .line 162
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/view/gtv/MovieGallery;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->positions:[I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/gtv/MovieGallery;->setSelectedItem(I)V

    .line 164
    :cond_0
    return-void
.end method


# virtual methods
.method protected createSectionTitle(Lcom/flixster/android/model/NamedList;Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flixster/android/model/NamedList",
            "<*>;",
            "Landroid/content/Context;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 234
    .local p1, section:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<*>;"
    invoke-virtual {p1}, Lcom/flixster/android/model/NamedList;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getAnalyticsTag()Ljava/lang/String;
.end method

.method protected abstract getAnalyticsTitle()Ljava/lang/String;
.end method

.method protected abstract getType()Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-super {p0, p1}, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 67
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onActivityCreated"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->getSavedInstanceState()Landroid/os/Bundle;

    move-result-object p1

    .line 71
    if-eqz p1, :cond_0

    .line 72
    const-string v0, "KEY_SELECTED_POSITIONS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->positions:[I

    .line 74
    :cond_0
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onActivityCreated saved positions: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->positions:[I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->movies:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->successHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 95
    :goto_0
    return-void

    .line 80
    :cond_1
    invoke-static {}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->$SWITCH_TABLE$com$flixster$android$activity$gtv$MovieGalleryFragment$Type()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->getType()Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 82
    :pswitch_0
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->throbber:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->movies:Ljava/util/Collection;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->successHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->errorHandler:Landroid/os/Handler;

    invoke-static {v0, v1, v2}, Lcom/flixster/android/data/MovieDaoNew;->fetchBoxOffice(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V

    goto :goto_0

    .line 86
    :pswitch_1
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->throbber:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->movies:Ljava/util/Collection;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->successHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->errorHandler:Landroid/os/Handler;

    invoke-static {v0, v1, v2}, Lcom/flixster/android/data/MovieDaoNew;->fetchUpcoming(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V

    goto :goto_0

    .line 90
    :pswitch_2
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->throbber:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->movies:Ljava/util/Collection;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->successHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->errorHandler:Landroid/os/Handler;

    invoke-static {v0, v1, v2}, Lcom/flixster/android/data/MovieDaoNew;->fetchDvd(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 57
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->className:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".onCreateView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const v1, 0x7f030055

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 59
    .local v0, rootView:Landroid/view/View;
    const v1, 0x7f070134

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rootLayout:Landroid/widget/LinearLayout;

    .line 60
    const v1, 0x7f070039

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->throbber:Landroid/widget/ProgressBar;

    .line 61
    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 209
    invoke-super {p0}, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->onDestroy()V

    .line 210
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->resetFragment()V

    .line 212
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 187
    invoke-super {p0}, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->onResume()V

    .line 188
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onResume"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->getAnalyticsTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->getAnalyticsTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .parameter "outState"

    .prologue
    .line 194
    invoke-super {p0, p1}, Lcom/flixster/android/activity/gtv/InstanceStateFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 195
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 196
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->positions:[I

    .line 197
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 200
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->className:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".onSaveInstanceState selected positions: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->positions:[I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v1, "KEY_SELECTED_POSITIONS"

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->positions:[I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 205
    .end local v0           #i:I
    :goto_1
    return-void

    .line 198
    .restart local v0       #i:I
    :cond_0
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->positions:[I

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/view/gtv/MovieGallery;

    invoke-virtual {v1}, Lcom/flixster/android/view/gtv/MovieGallery;->getSelectedItemPosition()I

    move-result v1

    aput v1, v2, v0

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    .end local v0           #i:I
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->positions:[I

    goto :goto_1
.end method

.method protected resetFragment()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->movies:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 216
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rights:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 217
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->galleries:Ljava/util/List;

    .line 218
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rootLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 221
    :cond_0
    return-void
.end method
