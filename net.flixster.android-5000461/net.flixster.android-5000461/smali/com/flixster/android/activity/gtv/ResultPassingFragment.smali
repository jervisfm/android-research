.class public Lcom/flixster/android/activity/gtv/ResultPassingFragment;
.super Landroid/app/Fragment;
.source "ResultPassingFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final RESULT_CANCELED:I = 0x0

.field public static final RESULT_OK:I = -0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected onFragmentResult(Ljava/lang/String;I)V
    .locals 0
    .parameter "requestClassName"
    .parameter "resultCode"

    .prologue
    .line 24
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 28
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 29
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;

    .line 30
    .local v0, activity:Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;
    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->hasFragmentResult()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->getAndClearFragmentResult()Ljava/util/Map$Entry;

    move-result-object v1

    .line 32
    .local v1, result:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/flixster/android/activity/gtv/ResultPassingFragment;->onFragmentResult(Ljava/lang/String;I)V

    .line 34
    .end local v1           #result:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_0
    return-void
.end method

.method protected final setFragmentResult(I)V
    .locals 2
    .parameter "resultCode"

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->setFragmentResult(Ljava/lang/String;I)V

    .line 20
    return-void
.end method
