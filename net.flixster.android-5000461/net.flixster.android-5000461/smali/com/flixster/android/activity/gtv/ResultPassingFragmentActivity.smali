.class public Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;
.super Lcom/flixster/android/activity/common/DecoratedActivity;
.source "ResultPassingFragmentActivity.java"


# instance fields
.field private final fragmentResultMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;-><init>()V

    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->fragmentResultMap:Ljava/util/HashMap;

    .line 11
    return-void
.end method


# virtual methods
.method public getAndClearFragmentResult()Ljava/util/Map$Entry;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    iget-object v3, p0, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->fragmentResultMap:Ljava/util/HashMap;

    monitor-enter v3

    .line 21
    const/4 v1, 0x0

    .line 22
    .local v1, result:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->hasFragmentResult()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 23
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->fragmentResultMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/Map$Entry;

    move-object v1, v0

    .line 24
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->fragmentResultMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 26
    :cond_0
    monitor-exit v3

    return-object v1

    .line 20
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public hasFragmentResult()Z
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->fragmentResultMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFragmentResult(Ljava/lang/String;I)V
    .locals 3
    .parameter "requestClassName"
    .parameter "resultCode"

    .prologue
    .line 31
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->fragmentResultMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 32
    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->fragmentResultMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 33
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/ResultPassingFragmentActivity;->fragmentResultMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    monitor-exit v1

    .line 35
    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
