.class Lcom/flixster/android/activity/gtv/SearchFragment$1;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/SearchFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/SearchFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 82
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_3

    const/16 v2, 0x42

    if-ne p2, v2, :cond_3

    .line 84
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    #getter for: Lcom/flixster/android/activity/gtv/SearchFragment;->editText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/flixster/android/activity/gtv/SearchFragment;->access$0(Lcom/flixster/android/activity/gtv/SearchFragment;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/gtv/SearchFragment;->access$1(Lcom/flixster/android/activity/gtv/SearchFragment;Ljava/lang/String;)V

    .line 87
    const-string v2, "FLX"

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    #getter for: Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/activity/gtv/SearchFragment;->access$2(Lcom/flixster/android/activity/gtv/SearchFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "REINDEER FLOTILLA"

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    #getter for: Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/activity/gtv/SearchFragment;->access$2(Lcom/flixster/android/activity/gtv/SearchFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 88
    const-string v2, "UUDDLRLRBA"

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    #getter for: Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/activity/gtv/SearchFragment;->access$2(Lcom/flixster/android/activity/gtv/SearchFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    :cond_0
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setAdminState(I)V

    .line 90
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/gtv/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0c01bb

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 94
    :cond_1
    const-string v2, "DGNS"

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    #getter for: Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/activity/gtv/SearchFragment;->access$2(Lcom/flixster/android/activity/gtv/SearchFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 95
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->enableDiagnosticMode()V

    .line 96
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/gtv/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0c01bc

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 99
    :cond_2
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/gtv/SearchFragment;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 100
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/gtv/SearchFragment;->throbber:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 101
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    #getter for: Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/SearchFragment;->access$2(Lcom/flixster/android/activity/gtv/SearchFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/gtv/SearchFragment;->movies:Ljava/util/Collection;

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    #getter for: Lcom/flixster/android/activity/gtv/SearchFragment;->searchSuccessHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/flixster/android/activity/gtv/SearchFragment;->access$3(Lcom/flixster/android/activity/gtv/SearchFragment;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    iget-object v4, v4, Lcom/flixster/android/activity/gtv/SearchFragment;->errorHandler:Landroid/os/Handler;

    invoke-static {v1, v2, v3, v4}, Lcom/flixster/android/data/MovieDaoNew;->searchMovie(Ljava/lang/String;Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 102
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/gtv/SearchFragment;->getAnalyticsTag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$1;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/gtv/SearchFragment;->getAnalyticsTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_0
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
