.class Lcom/flixster/android/activity/gtv/SearchFragment$2;
.super Landroid/os/Handler;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/SearchFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/SearchFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/SearchFragment$2;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    .line 109
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 112
    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$2;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/gtv/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 113
    .local v0, hostContext:Landroid/content/Context;
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$2;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/gtv/SearchFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$2;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/gtv/SearchFragment;->successHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 117
    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$2;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/gtv/SearchFragment;->movies:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/model/NamedList;

    .line 118
    .local v1, section:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/Movie;>;"
    invoke-virtual {v1}, Lcom/flixster/android/model/NamedList;->getList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 119
    new-instance v2, Lcom/flixster/android/view/gtv/Subheader;

    invoke-direct {v2, v0}, Lcom/flixster/android/view/gtv/Subheader;-><init>(Landroid/content/Context;)V

    .line 120
    .local v2, title:Landroid/widget/TextView;
    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$2;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    invoke-virtual {v3, v1, v0}, Lcom/flixster/android/activity/gtv/SearchFragment;->createSectionTitle(Lcom/flixster/android/model/NamedList;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment$2;->this$0:Lcom/flixster/android/activity/gtv/SearchFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/gtv/SearchFragment;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
