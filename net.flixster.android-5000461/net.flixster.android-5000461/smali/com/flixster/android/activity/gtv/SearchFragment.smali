.class public Lcom/flixster/android/activity/gtv/SearchFragment;
.super Lcom/flixster/android/activity/gtv/MovieGalleryFragment;
.source "SearchFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private editText:Landroid/widget/EditText;

.field private isEditTextFocusedInitially:Z

.field private final onKeyListener:Landroid/view/View$OnKeyListener;

.field private query:Ljava/lang/String;

.field private final searchSuccessHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;-><init>()V

    .line 79
    new-instance v0, Lcom/flixster/android/activity/gtv/SearchFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/SearchFragment$1;-><init>(Lcom/flixster/android/activity/gtv/SearchFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->onKeyListener:Landroid/view/View$OnKeyListener;

    .line 109
    new-instance v0, Lcom/flixster/android/activity/gtv/SearchFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/SearchFragment$2;-><init>(Lcom/flixster/android/activity/gtv/SearchFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->searchSuccessHandler:Landroid/os/Handler;

    .line 32
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/gtv/SearchFragment;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->editText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/gtv/SearchFragment;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/gtv/SearchFragment;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/gtv/SearchFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->searchSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected createSectionTitle(Lcom/flixster/android/model/NamedList;Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .parameter
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flixster/android/model/NamedList",
            "<*>;",
            "Landroid/content/Context;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, section:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<*>;"
    invoke-virtual {p1}, Lcom/flixster/android/model/NamedList;->getName()Ljava/lang/String;

    move-result-object v1

    .line 55
    .local v1, sectionName:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/flixster/android/model/NamedList;->getList()Ljava/util/List;

    move-result-object v0

    .line 56
    .local v0, sectionList:Ljava/util/List;,"Ljava/util/List<*>;"
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "/search"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "/search/movie/results"

    goto :goto_0
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "Search"

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Search Movie Results - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getType()Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->SEARCH:Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 61
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->className:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".onCreateView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const v1, 0x7f030076

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 63
    .local v0, rootView:Landroid/view/View;
    const v1, 0x7f070134

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->rootLayout:Landroid/widget/LinearLayout;

    .line 64
    const v1, 0x7f070039

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->throbber:Landroid/widget/ProgressBar;

    .line 65
    const v1, 0x7f070233

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->editText:Landroid/widget/EditText;

    .line 66
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->editText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 67
    return-object v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->onResume()V

    .line 73
    iget-boolean v0, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->isEditTextFocusedInitially:Z

    if-nez v0, :cond_0

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->isEditTextFocusedInitially:Z

    .line 75
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/SearchFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 77
    :cond_0
    return-void
.end method
