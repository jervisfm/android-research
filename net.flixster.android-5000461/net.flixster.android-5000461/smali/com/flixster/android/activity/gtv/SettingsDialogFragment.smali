.class public Lcom/flixster/android/activity/gtv/SettingsDialogFragment;
.super Landroid/app/DialogFragment;
.source "SettingsDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 39
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 44
    .end local p1
    :goto_0
    return-void

    .line 41
    .restart local p1
    :pswitch_0
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setCaptionsEnabled(Z)V

    goto :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch 0x7f070235
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 29
    const/4 v1, 0x1

    .line 31
    .local v1, style:I
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/flixster/android/activity/gtv/SettingsDialogFragment;->setStyle(II)V

    .line 32
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 33
    .local v0, dialog:Landroid/app/Dialog;
    const v2, 0x7f0c0018

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setTitle(I)V

    .line 34
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 20
    const v2, 0x7f030078

    invoke-virtual {p1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 21
    .local v1, view:Landroid/view/View;
    const v2, 0x7f070235

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 22
    .local v0, enableCaptions:Landroid/widget/CheckBox;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCaptionsEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 23
    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    return-object v1
.end method
