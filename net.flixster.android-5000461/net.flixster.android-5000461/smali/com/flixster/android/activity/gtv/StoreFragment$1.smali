.class Lcom/flixster/android/activity/gtv/StoreFragment$1;
.super Landroid/os/Handler;
.source "StoreFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/StoreFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/StoreFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/StoreFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/StoreFragment$1;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    .line 103
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 106
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment$1;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/StoreFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 107
    .local v0, hostContext:Landroid/content/Context;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment$1;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/StoreFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment$1;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/gtv/StoreFragment;->throbber:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 112
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 113
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment$1;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    #calls: Lcom/flixster/android/activity/gtv/StoreFragment;->displayUserHeader()V
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/StoreFragment;->access$0(Lcom/flixster/android/activity/gtv/StoreFragment;)V

    .line 114
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment$1;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    #calls: Lcom/flixster/android/activity/gtv/StoreFragment;->fetchCollectedMovies()V
    invoke-static {v1}, Lcom/flixster/android/activity/gtv/StoreFragment;->access$1(Lcom/flixster/android/activity/gtv/StoreFragment;)V

    .line 120
    :goto_1
    if-eqz p1, :cond_0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v1, v1, Lnet/flixster/android/data/DaoException;

    if-eqz v1, :cond_0

    .line 122
    const-string v2, "FlxMain"

    const-string v3, "StoreFragment.fetchUserHandler"

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lnet/flixster/android/data/DaoException;

    invoke-static {v2, v3, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 116
    :cond_2
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment$1;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    const/4 v2, 0x1

    #calls: Lcom/flixster/android/activity/gtv/StoreFragment;->showEmptyCollectionPromo(Z)V
    invoke-static {v1, v2}, Lcom/flixster/android/activity/gtv/StoreFragment;->access$2(Lcom/flixster/android/activity/gtv/StoreFragment;Z)V

    goto :goto_1
.end method
