.class Lcom/flixster/android/activity/gtv/StoreFragment$2;
.super Landroid/os/Handler;
.source "StoreFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/StoreFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/StoreFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/StoreFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/StoreFragment$2;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    .line 160
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .parameter "msg"

    .prologue
    const/4 v7, 0x0

    .line 163
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment$2;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    invoke-virtual {v5}, Lcom/flixster/android/activity/gtv/StoreFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 164
    .local v0, hostContext:Landroid/content/Context;
    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment$2;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    invoke-virtual {v5}, Lcom/flixster/android/activity/gtv/StoreFragment;->isRemoving()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment$2;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    iget-object v5, v5, Lcom/flixster/android/activity/gtv/StoreFragment;->rights:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->clear()V

    .line 169
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 170
    .local v1, lockerRights:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v5

    invoke-virtual {v5}, Lnet/flixster/android/model/User;->getLockerRights()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 180
    new-instance v2, Lcom/flixster/android/model/NamedList;

    const v5, 0x7f0c0071

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5, v1}, Lcom/flixster/android/model/NamedList;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 182
    .local v2, nl:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/LockerRight;>;"
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment$2;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    iget-object v5, v5, Lcom/flixster/android/activity/gtv/StoreFragment;->rights:Ljava/util/Collection;

    invoke-interface {v5, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 183
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment$2;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    iget-object v5, v5, Lcom/flixster/android/activity/gtv/StoreFragment;->successHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 184
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment$2;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    #getter for: Lcom/flixster/android/activity/gtv/StoreFragment;->userRefresh:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/flixster/android/activity/gtv/StoreFragment;->access$3(Lcom/flixster/android/activity/gtv/StoreFragment;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment$2;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    iget-object v5, v5, Lcom/flixster/android/activity/gtv/StoreFragment;->rights:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/model/NamedList;

    .line 187
    .local v4, section:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/LockerRight;>;"
    invoke-virtual {v4}, Lcom/flixster/android/model/NamedList;->getList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_0

    .line 188
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment$2;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    #calls: Lcom/flixster/android/activity/gtv/StoreFragment;->showEmptyCollectionPromo(Z)V
    invoke-static {v5, v7}, Lcom/flixster/android/activity/gtv/StoreFragment;->access$2(Lcom/flixster/android/activity/gtv/StoreFragment;Z)V

    goto :goto_0

    .line 170
    .end local v2           #nl:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/LockerRight;>;"
    .end local v4           #section:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/LockerRight;>;"
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/LockerRight;

    .line 171
    .local v3, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v3}, Lnet/flixster/android/model/LockerRight;->isMovie()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-boolean v6, v3, Lnet/flixster/android/model/LockerRight;->isStreamingSupported:Z

    if-nez v6, :cond_5

    iget-boolean v6, v3, Lnet/flixster/android/model/LockerRight;->isDownloadSupported:Z

    if-nez v6, :cond_5

    :cond_4
    invoke-virtual {v3}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 172
    :cond_5
    iget-boolean v6, v3, Lnet/flixster/android/model/LockerRight;->isRental:Z

    if-nez v6, :cond_2

    .line 175
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
