.class Lcom/flixster/android/activity/gtv/StoreFragment$3;
.super Ljava/lang/Object;
.source "StoreFragment.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/gtv/StoreFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/gtv/StoreFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/gtv/StoreFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/gtv/StoreFragment$3;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 277
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 273
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 265
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment$3;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/gtv/StoreFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/data/AccountManager;->fbLogout(Landroid/app/Activity;)V

    .line 266
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment$3;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/gtv/StoreFragment;->resetFragment()V

    .line 267
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment$3;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    const/4 v1, 0x1

    #calls: Lcom/flixster/android/activity/gtv/StoreFragment;->showEmptyCollectionPromo(Z)V
    invoke-static {v0, v1}, Lcom/flixster/android/activity/gtv/StoreFragment;->access$2(Lcom/flixster/android/activity/gtv/StoreFragment;Z)V

    .line 268
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment$3;->this$0:Lcom/flixster/android/activity/gtv/StoreFragment;

    #getter for: Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;
    invoke-static {v0}, Lcom/flixster/android/activity/gtv/StoreFragment;->access$4(Lcom/flixster/android/activity/gtv/StoreFragment;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0700eb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 269
    return-void
.end method
