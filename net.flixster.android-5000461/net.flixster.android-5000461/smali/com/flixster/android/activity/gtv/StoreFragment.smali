.class public Lcom/flixster/android/activity/gtv/StoreFragment;
.super Lcom/flixster/android/activity/gtv/MovieGalleryFragment;
.source "StoreFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final fetchUserHandler:Landroid/os/Handler;

.field private final logoutDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private rootView:Landroid/view/View;

.field private final storeSuccessHandler:Landroid/os/Handler;

.field private userGreeting:Landroid/widget/TextView;

.field private userHeader:Landroid/widget/RelativeLayout;

.field private userPicture:Landroid/widget/ImageView;

.field private userRefresh:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;-><init>()V

    .line 103
    new-instance v0, Lcom/flixster/android/activity/gtv/StoreFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/StoreFragment$1;-><init>(Lcom/flixster/android/activity/gtv/StoreFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->fetchUserHandler:Landroid/os/Handler;

    .line 160
    new-instance v0, Lcom/flixster/android/activity/gtv/StoreFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/StoreFragment$2;-><init>(Lcom/flixster/android/activity/gtv/StoreFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->storeSuccessHandler:Landroid/os/Handler;

    .line 261
    new-instance v0, Lcom/flixster/android/activity/gtv/StoreFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/gtv/StoreFragment$3;-><init>(Lcom/flixster/android/activity/gtv/StoreFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->logoutDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 38
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/gtv/StoreFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/StoreFragment;->displayUserHeader()V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/gtv/StoreFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/StoreFragment;->fetchCollectedMovies()V

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/gtv/StoreFragment;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 193
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/gtv/StoreFragment;->showEmptyCollectionPromo(Z)V

    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/gtv/StoreFragment;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userRefresh:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/gtv/StoreFragment;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    return-object v0
.end method

.method private displayUserHeader()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 139
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v1

    .line 140
    .local v1, user:Lnet/flixster/android/model/User;
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userGreeting:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/StoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c016d

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v1, Lnet/flixster/android/model/User;->firstName:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userPicture:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/User;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 142
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 143
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userPicture:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 145
    :cond_0
    iget-object v2, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userHeader:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 146
    return-void
.end method

.method private fetchCollectedMovies()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rights:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->storeSuccessHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 158
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->throbber:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userRefresh:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->storeSuccessHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->errorHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Lnet/flixster/android/data/ProfileDao;->getUserLockerRights(Landroid/os/Handler;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method private showEmptyCollectionPromo(Z)V
    .locals 8
    .parameter "isAnonymous"

    .prologue
    const/4 v7, 0x0

    .line 194
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v6, 0x7f07004f

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/view/gtv/Subheader;

    .line 195
    .local v2, promoHeader:Lcom/flixster/android/view/gtv/Subheader;
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v6, 0x7f070050

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 196
    .local v3, promoImage:Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v6, 0x7f070051

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 197
    .local v4, promoText:Landroid/widget/TextView;
    iget-object v5, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v6, 0x7f0700eb

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 199
    .local v0, loginButton:Landroid/widget/Button;
    if-eqz p1, :cond_0

    .line 200
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v1, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 201
    .local v1, lp:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v7, v7, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 202
    invoke-virtual {v2, v1}, Lcom/flixster/android/view/gtv/Subheader;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 203
    const v5, 0x7f0c01a3

    invoke-virtual {v2, v5}, Lcom/flixster/android/view/gtv/Subheader;->setText(I)V

    .line 204
    const v5, 0x7f0c01a5

    invoke-virtual {p0, v5}, Lcom/flixster/android/activity/gtv/StoreFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 206
    new-instance v5, Lcom/flixster/android/activity/gtv/StoreFragment$4;

    invoke-direct {v5, p0}, Lcom/flixster/android/activity/gtv/StoreFragment$4;-><init>(Lcom/flixster/android/activity/gtv/StoreFragment;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    .end local v1           #lp:Landroid/widget/RelativeLayout$LayoutParams;
    :goto_0
    invoke-virtual {v2, v7}, Lcom/flixster/android/view/gtv/Subheader;->setVisibility(I)V

    .line 219
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 220
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 222
    return-void

    .line 214
    :cond_0
    const v5, 0x7f0c01a2

    invoke-virtual {v2, v5}, Lcom/flixster/android/view/gtv/Subheader;->setText(I)V

    .line 215
    const v5, 0x7f0c01a4

    invoke-virtual {p0, v5}, Lcom/flixster/android/activity/gtv/StoreFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected createSectionTitle(Lcom/flixster/android/model/NamedList;Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .parameter
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flixster/android/model/NamedList",
            "<*>;",
            "Landroid/content/Context;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, section:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<*>;"
    invoke-virtual {p1}, Lcom/flixster/android/model/NamedList;->getName()Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, sectionName:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/flixster/android/model/NamedList;->getList()Ljava/util/List;

    move-result-object v0

    .line 63
    .local v0, sectionList:Ljava/util/List;,"Ljava/util/List<*>;"
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string v0, "/collection"

    return-object v0
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "Collection"

    return-object v0
.end method

.method protected getType()Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;->STORE:Lcom/flixster/android/activity/gtv/MovieGalleryFragment$Type;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 93
    invoke-super {p0, p1}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 94
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->fetchUserHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 101
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->throbber:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->fetchUserHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->fetchUserHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Lnet/flixster/android/data/ProfileDao;->fetchUser(Landroid/os/Handler;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 240
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 242
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v0

    invoke-virtual {v0}, Lnet/flixster/android/model/User;->resetLockerRights()V

    .line 244
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rights:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 245
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 246
    invoke-direct {p0}, Lcom/flixster/android/activity/gtv/StoreFragment;->fetchCollectedMovies()V

    goto :goto_0

    .line 250
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->isPlatformFlixster()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/StoreFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/Main;

    const v1, 0x3b9aca64

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->logoutDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/activity/gtv/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 253
    :cond_1
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->isPlatformFacebook()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/StoreFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/gtv/Main;

    const v1, 0x3b9aca65

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->logoutDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/activity/gtv/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x7f070256
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, 0x1

    .line 73
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->className:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".onCreateView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const v1, 0x7f030080

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    .line 75
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v2, 0x7f070134

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootLayout:Landroid/widget/LinearLayout;

    .line 76
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v2, 0x7f070039

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->throbber:Landroid/widget/ProgressBar;

    .line 77
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v2, 0x7f070253

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userHeader:Landroid/widget/RelativeLayout;

    .line 78
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v2, 0x7f070254

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userPicture:Landroid/widget/ImageView;

    .line 79
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v2, 0x7f070255

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userGreeting:Landroid/widget/TextView;

    .line 80
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v2, 0x7f070256

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userRefresh:Landroid/widget/TextView;

    .line 81
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    const v2, 0x7f070257

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 82
    .local v0, userLogout:Landroid/widget/TextView;
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userRefresh:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 83
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userRefresh:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 84
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userRefresh:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 86
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 87
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->rootView:Landroid/view/View;

    return-object v1
.end method

.method protected resetFragment()V
    .locals 2

    .prologue
    .line 226
    invoke-super {p0}, Lcom/flixster/android/activity/gtv/MovieGalleryFragment;->resetFragment()V

    .line 227
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userHeader:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userHeader:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userPicture:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userPicture:Landroid/widget/ImageView;

    const v1, 0x7f0201da

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userGreeting:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 234
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/StoreFragment;->userGreeting:Landroid/widget/TextView;

    const-string v1, "Hello!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    :cond_2
    return-void
.end method
