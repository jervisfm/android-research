.class public Lcom/flixster/android/activity/gtv/VideoPlayer;
.super Landroid/app/Activity;
.source "VideoPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;


# static fields
.field public static final KEY_LANDSCAPE:Ljava/lang/String; = "KEY_LANDSCAPE"

.field public static final KEY_URL:Ljava/lang/String; = "KEY_URL"


# instance fields
.field private currPosition:I

.field private isLandscape:Z

.field private path:Ljava/lang/String;

.field private throbber:Landroid/widget/ProgressBar;

.field private videoView:Landroid/widget/VideoView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0
    .parameter "mp"

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/VideoPlayer;->finish()V

    .line 105
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/flixster/android/activity/gtv/VideoPlayer;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 38
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 39
    const-string v1, "KEY_LANDSCAPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->isLandscape:Z

    .line 40
    const-string v1, "KEY_URL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->path:Ljava/lang/String;

    .line 41
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "VideoPlayer.onCreate path:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->path:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    iget-boolean v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->isLandscape:Z

    if-eqz v1, :cond_1

    .line 45
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v1

    const/16 v2, 0x9

    if-ge v1, v2, :cond_2

    .line 46
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/gtv/VideoPlayer;->setRequestedOrientation(I)V

    .line 51
    :cond_1
    :goto_0
    invoke-static {p0}, Lcom/flixster/android/utils/VersionedViewHelper;->hideSystemNavigation(Landroid/app/Activity;)V

    .line 53
    const v1, 0x7f030091

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/gtv/VideoPlayer;->setContentView(I)V

    .line 54
    const v1, 0x7f0702dd

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/gtv/VideoPlayer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/VideoView;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    .line 55
    const v1, 0x7f070039

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/gtv/VideoPlayer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->throbber:Landroid/widget/ProgressBar;

    .line 58
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    iget-object v2, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    .line 59
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    new-instance v2, Landroid/widget/MediaController;

    invoke-direct {v2, p0}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 60
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1, p0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 61
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1, p0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 62
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1, p0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 63
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->requestFocus()Z

    .line 64
    return-void

    .line 48
    :cond_2
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/gtv/VideoPlayer;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 88
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->currPosition:I

    .line 90
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 92
    :cond_0
    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .parameter "mp"

    .prologue
    .line 98
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->throbber:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 100
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .parameter "hasFocus"

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 68
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    invoke-static {p0}, Lcom/flixster/android/utils/VersionedViewHelper;->hideSystemNavigation(Landroid/app/Activity;)V

    .line 73
    :cond_0
    if-eqz p1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->currPosition:I

    if-lez v1, :cond_1

    .line 74
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->start()V

    .line 75
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v1

    iget v2, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->currPosition:I

    if-ge v1, v2, :cond_1

    .line 76
    iget-object v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->videoView:Landroid/widget/VideoView;

    iget v2, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->currPosition:I

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->seekTo(I)V

    .line 77
    const/4 v1, 0x0

    iput v1, p0, Lcom/flixster/android/activity/gtv/VideoPlayer;->currPosition:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_1
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "FlxMain"

    const-string v2, "VideoPlayer.onWindowFocusChanged exception"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
