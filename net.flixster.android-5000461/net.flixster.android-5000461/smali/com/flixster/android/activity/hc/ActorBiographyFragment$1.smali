.class Lcom/flixster/android/activity/hc/ActorBiographyFragment$1;
.super Landroid/os/Handler;
.source "ActorBiographyFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/ActorBiographyFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$1;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    .line 128
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 131
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$1;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 132
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$1;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    const-string v1, "FlxMain"

    const-string v2, "ActorBiographyPage.updateHandler"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$1;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->access$0(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$1;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    #calls: Lcom/flixster/android/activity/hc/ActorBiographyFragment;->updatePage()V
    invoke-static {v1}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->access$1(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)V

    goto :goto_0
.end method
