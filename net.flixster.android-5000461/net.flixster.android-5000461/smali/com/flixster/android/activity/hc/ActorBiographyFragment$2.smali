.class Lcom/flixster/android/activity/hc/ActorBiographyFragment$2;
.super Landroid/os/Handler;
.source "ActorBiographyFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/ActorBiographyFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$2;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    .line 202
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "message"

    .prologue
    .line 205
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$2;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/widget/ImageView;

    .line 210
    .local v2, imageView:Landroid/widget/ImageView;
    if-eqz v2, :cond_0

    .line 211
    invoke-virtual {v2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 212
    .local v0, actor:Lnet/flixster/android/model/Actor;
    if-eqz v0, :cond_0

    .line 213
    iget-object v1, v0, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    .line 214
    .local v1, bitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 215
    invoke-virtual {v2}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0
.end method
