.class Lcom/flixster/android/activity/hc/ActorBiographyFragment$4;
.super Ljava/util/TimerTask;
.source "ActorBiographyFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/ActorBiographyFragment;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    .line 106
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 108
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->access$0(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v1

    if-nez v1, :cond_1

    .line 114
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actorId:J
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->access$3(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lnet/flixster/android/data/ActorDao;->getActor(J)Lnet/flixster/android/model/Actor;

    move-result-object v2

    #setter for: Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v1, v2}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->access$4(Lcom/flixster/android/activity/hc/ActorBiographyFragment;Lnet/flixster/android/model/Actor;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorBiographyFragment;->updateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->access$5(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "ActorBiographyPage.scheduleUpdatePageTask (failed to get actor data)"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 117
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    const/4 v2, 0x0

    #setter for: Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v1, v2}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->access$4(Lcom/flixster/android/activity/hc/ActorBiographyFragment;Lnet/flixster/android/model/Actor;)V

    goto :goto_1
.end method
