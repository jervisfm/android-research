.class public Lcom/flixster/android/activity/hc/ActorBiographyFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "ActorBiographyFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final DIALOG_ACTOR:I = 0x2

.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field public static final KEY_ACTOR_ID:Ljava/lang/String; = "ACTOR_ID"

.field public static final KEY_ACTOR_NAME:Ljava/lang/String; = "ACTOR_NAME"


# instance fields
.field private actor:Lnet/flixster/android/model/Actor;

.field private actorId:J

.field private final actorThumbnailHandler:Landroid/os/Handler;

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 128
    new-instance v0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment$1;-><init>(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->updateHandler:Landroid/os/Handler;

    .line 202
    new-instance v0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment$2;-><init>(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actorThumbnailHandler:Landroid/os/Handler;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)Lnet/flixster/android/model/Actor;
    .locals 1
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->updatePage()V

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)J
    .locals 2
    .parameter

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actorId:J

    return-wide v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/ActorBiographyFragment;Lnet/flixster/android/model/Actor;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 222
    const/16 v0, 0x68

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/ActorBiographyFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 42
    new-instance v0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;-><init>()V

    .line 43
    .local v0, af:Lcom/flixster/android/activity/hc/ActorBiographyFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->setArguments(Landroid/os/Bundle;)V

    .line 44
    return-object v0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 106
    new-instance v0, Lcom/flixster/android/activity/hc/ActorBiographyFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment$4;-><init>(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)V

    .line 123
    .local v0, updatePageTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 126
    :cond_0
    return-void
.end method

.method private updatePage()V
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x0

    .line 149
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    if-eqz v1, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f070028

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 151
    .local v9, actorLayout:Landroid/widget/LinearLayout;
    const v1, 0x7f07002b

    invoke-virtual {v9, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 152
    .local v4, thumbnailView:Landroid/widget/ImageView;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v1, v1, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 153
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v1, v1, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 167
    :goto_0
    const v1, 0x7f07002c

    invoke-virtual {v9, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 168
    .local v10, actorName:Landroid/widget/TextView;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v1, v1, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v10, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    const v1, 0x7f07002d

    invoke-virtual {v9, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 170
    .local v7, actorBirthday:Landroid/widget/TextView;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v1, v1, Lnet/flixster/android/model/Actor;->birthDay:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Birthday: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v2, v2, Lnet/flixster/android/model/Actor;->birthDay:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    :goto_1
    const v1, 0x7f07002e

    invoke-virtual {v9, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 177
    .local v8, actorBirthplace:Landroid/widget/TextView;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v1, v1, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ""

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v2, v2, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Birthplace: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v2, v2, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 183
    :goto_2
    const v1, 0x7f07002f

    invoke-virtual {v9, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 184
    .local v11, biographyTitle:Landroid/widget/TextView;
    const v1, 0x7f070030

    invoke-virtual {v9, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 185
    .local v6, actorBiography:Landroid/widget/TextView;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "ActorBiography.updatePage() actor.biography:"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v5, v5, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v1, v1, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v1, v1, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 187
    const-string v1, "FlxMain"

    const-string v2, "ActorBiography.updatePage() non null"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v1, v1, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    const-string v1, "FlxMain"

    const-string v2, "ActorBiography.updatePage() text set"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :goto_3
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->trackPage()V

    .line 200
    .end local v4           #thumbnailView:Landroid/widget/ImageView;
    .end local v6           #actorBiography:Landroid/widget/TextView;
    .end local v7           #actorBirthday:Landroid/widget/TextView;
    .end local v8           #actorBirthplace:Landroid/widget/TextView;
    .end local v9           #actorLayout:Landroid/widget/LinearLayout;
    .end local v10           #actorName:Landroid/widget/TextView;
    .end local v11           #biographyTitle:Landroid/widget/TextView;
    :cond_0
    return-void

    .line 155
    .restart local v4       #thumbnailView:Landroid/widget/ImageView;
    .restart local v9       #actorLayout:Landroid/widget/LinearLayout;
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v3, v1, Lnet/flixster/android/model/Actor;->largeUrl:Ljava/lang/String;

    .line 156
    .local v3, thumbnailUrl:Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 157
    const v1, 0x7f020151

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 158
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 159
    new-instance v0, Lnet/flixster/android/model/ImageOrder;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    .line 160
    iget-object v5, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actorThumbnailHandler:Landroid/os/Handler;

    .line 159
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 161
    .local v0, imageOrder:Lnet/flixster/android/model/ImageOrder;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v1, v0}, Lcom/flixster/android/activity/hc/Main;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    goto/16 :goto_0

    .line 163
    .end local v0           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_2
    const v1, 0x7f0201da

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 174
    .end local v3           #thumbnailUrl:Ljava/lang/String;
    .restart local v7       #actorBirthday:Landroid/widget/TextView;
    .restart local v10       #actorName:Landroid/widget/TextView;
    :cond_3
    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 181
    .restart local v8       #actorBirthplace:Landroid/widget/TextView;
    :cond_4
    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 194
    .restart local v6       #actorBiography:Landroid/widget/TextView;
    .restart local v11       #biographyTitle:Landroid/widget/TextView;
    :cond_5
    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 195
    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method


# virtual methods
.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "dialogId"

    .prologue
    const v5, 0x7f0c0135

    const/4 v4, 0x1

    .line 73
    packed-switch p1, :pswitch_data_0

    .line 96
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    invoke-direct {v1, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 97
    .local v1, defaultDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 98
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 99
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 100
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    move-object v2, v1

    .line 101
    .end local v1           #defaultDialog:Landroid/app/ProgressDialog;
    :goto_0
    return-object v2

    .line 76
    :pswitch_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 77
    .local v2, ratingsDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 79
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 80
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 83
    .end local v2           #ratingsDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 84
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v3, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 85
    const-string v3, "Network Error"

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 86
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 87
    const-string v3, "Retry"

    new-instance v4, Lcom/flixster/android/activity/hc/ActorBiographyFragment$3;

    invoke-direct {v4, p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment$3;-><init>(Lcom/flixster/android/activity/hc/ActorBiographyFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 93
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 94
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 49
    const v4, 0x7f030016

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 50
    .local v3, view:Landroid/view/View;
    iget-wide v4, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actorId:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 52
    .local v1, extras:Landroid/os/Bundle;
    const-string v4, "ACTOR_ID"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actorId:J

    .line 53
    const-string v4, "ACTOR_NAME"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 54
    const-string v4, "ACTOR_NAME"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, name:Ljava/lang/String;
    const v4, 0x7f07002c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 56
    .local v0, actorTitle:Landroid/widget/TextView;
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    .end local v0           #actorTitle:Landroid/widget/TextView;
    .end local v1           #extras:Landroid/os/Bundle;
    .end local v2           #name:Ljava/lang/String;
    :cond_0
    return-object v3
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 64
    const-string v0, "FlxMain"

    const-string v1, "ActorBiographyPage.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 67
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->scheduleUpdatePageTask()V

    .line 69
    return-void
.end method

.method public trackPage()V
    .locals 5

    .prologue
    .line 145
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/actor/bio"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Actor Actor Page for actor:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorBiographyFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-wide v3, v3, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    return-void
.end method
