.class Lcom/flixster/android/activity/hc/ActorFragment$1;
.super Landroid/os/Handler;
.source "ActorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/ActorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorFragment$1;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    .line 164
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 168
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorFragment$1;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 169
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorFragment$1;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/ActorFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    const-string v1, "FlxMain"

    const-string v2, "ActorPage.updateHandler"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorFragment$1;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/ActorFragment;->access$0(Lcom/flixster/android/activity/hc/ActorFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 175
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorFragment$1;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #calls: Lcom/flixster/android/activity/hc/ActorFragment;->updatePage()V
    invoke-static {v1}, Lcom/flixster/android/activity/hc/ActorFragment;->access$1(Lcom/flixster/android/activity/hc/ActorFragment;)V

    goto :goto_0

    .line 176
    :cond_2
    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 177
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/flixster/android/activity/hc/Main;->showDialog(I)V

    goto :goto_0
.end method
