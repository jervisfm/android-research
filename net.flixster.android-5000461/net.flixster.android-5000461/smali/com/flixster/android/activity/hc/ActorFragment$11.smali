.class Lcom/flixster/android/activity/hc/ActorFragment$11;
.super Ljava/util/TimerTask;
.source "ActorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/ActorFragment;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorFragment$11;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    .line 139
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 141
    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment$11;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 142
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment$11;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/ActorFragment;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment$11;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ActorFragment;->access$0(Lcom/flixster/android/activity/hc/ActorFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v2

    if-nez v2, :cond_2

    .line 147
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ActorPage.scheduleUpdatePageTask.run actorId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/ActorFragment$11;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->mActorId:J
    invoke-static {v4}, Lcom/flixster/android/activity/hc/ActorFragment;->access$6(Lcom/flixster/android/activity/hc/ActorFragment;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :try_start_0
    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment$11;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$11;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->mActorId:J
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$6(Lcom/flixster/android/activity/hc/ActorFragment;)J

    move-result-wide v3

    invoke-static {v3, v4}, Lnet/flixster/android/data/ActorDao;->getActor(J)Lnet/flixster/android/model/Actor;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$7(Lcom/flixster/android/activity/hc/ActorFragment;Lnet/flixster/android/model/Actor;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment$11;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->updateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ActorFragment;->access$8(Lcom/flixster/android/activity/hc/ActorFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    const-string v3, "ActorPage.scheduleUpdatePageTask DaoException"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 152
    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment$11;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    const/4 v3, 0x0

    #setter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$7(Lcom/flixster/android/activity/hc/ActorFragment;Lnet/flixster/android/model/Actor;)V

    goto :goto_1
.end method
