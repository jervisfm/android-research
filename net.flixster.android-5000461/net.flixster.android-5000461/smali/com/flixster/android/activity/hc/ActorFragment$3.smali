.class Lcom/flixster/android/activity/hc/ActorFragment$3;
.super Landroid/os/Handler;
.source "ActorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/ActorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorFragment$3;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    .line 333
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 336
    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment$3;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/ActorFragment;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/widget/ImageView;

    .line 341
    .local v1, photoView:Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 342
    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Photo;

    .line 343
    .local v0, photo:Lnet/flixster/android/model/Photo;
    if-eqz v0, :cond_0

    .line 344
    iget-object v2, v0, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 345
    iget-object v2, v0, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 346
    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0
.end method
