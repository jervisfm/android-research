.class Lcom/flixster/android/activity/hc/ActorFragment$4;
.super Ljava/lang/Object;
.source "ActorFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/ActorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    .line 353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 355
    new-instance v0, Landroid/content/Intent;

    const-string v1, "TOP_ACTOR_BIOGRPHY"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 356
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "ACTOR_ID"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ActorFragment;->access$0(Lcom/flixster/android/activity/hc/ActorFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v2

    iget-wide v2, v2, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 357
    const-string v1, "ACTOR_NAME"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ActorFragment;->access$0(Lcom/flixster/android/activity/hc/ActorFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 358
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorFragment$4;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    const-class v2, Lcom/flixster/android/activity/hc/ActorBiographyFragment;

    const/4 v3, 0x0

    .line 359
    const-class v4, Lcom/flixster/android/activity/hc/ActorFragment;

    .line 358
    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    .line 360
    return-void
.end method
