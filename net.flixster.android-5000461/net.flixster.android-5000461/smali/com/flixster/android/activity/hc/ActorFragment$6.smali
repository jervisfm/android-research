.class Lcom/flixster/android/activity/hc/ActorFragment$6;
.super Ljava/lang/Object;
.source "ActorFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/ActorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorFragment$6;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .parameter "view"

    .prologue
    const/4 v5, 0x0

    .line 384
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    .line 385
    .local v1, movie:Lnet/flixster/android/model/Movie;
    if-eqz v1, :cond_2

    .line 386
    new-instance v0, Landroid/content/Intent;

    const-string v4, "DETAILS"

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/flixster/android/activity/hc/ActorFragment$6;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v7}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-class v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v0, v4, v6, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 387
    .local v0, intent:Landroid/content/Intent;
    const-string v4, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v1}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v6

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 388
    const-string v4, "MOVIE_IN_THEATER_FLAG"

    iget-boolean v6, v1, Lnet/flixster/android/model/Movie;->isMIT:Z

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 389
    const-string v6, "MOVIE_THUMBNAIL"

    iget-object v4, v1, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Parcelable;

    invoke-virtual {v0, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 390
    const/16 v4, 0xc

    new-array v2, v4, [Ljava/lang/String;

    const-string v4, "title"

    aput-object v4, v2, v5

    const/4 v4, 0x1

    const-string v6, "high"

    aput-object v6, v2, v4

    const/4 v4, 0x2

    const-string v6, "mpaa"

    aput-object v6, v2, v4

    const/4 v4, 0x3

    .line 391
    const-string v6, "MOVIE_ACTORS"

    aput-object v6, v2, v4

    const/4 v4, 0x4

    const-string v6, "thumbnail"

    aput-object v6, v2, v4

    const/4 v4, 0x5

    const-string v6, "runningTime"

    aput-object v6, v2, v4

    const/4 v4, 0x6

    const-string v6, "directors"

    aput-object v6, v2, v4

    const/4 v4, 0x7

    .line 392
    const-string v6, "theaterReleaseDate"

    aput-object v6, v2, v4

    const/16 v4, 0x8

    const-string v6, "genre"

    aput-object v6, v2, v4

    const/16 v4, 0x9

    const-string v6, "status"

    aput-object v6, v2, v4

    const/16 v4, 0xa

    .line 393
    const-string v6, "MOVIE_ACTORS_SHORT"

    aput-object v6, v2, v4

    const/16 v4, 0xb

    const-string v6, "meta"

    aput-object v6, v2, v4

    .line 394
    .local v2, movieProperties:[Ljava/lang/String;
    array-length v6, v2

    move v4, v5

    :goto_0
    if-lt v4, v6, :cond_3

    .line 397
    const-string v4, "boxOffice"

    const-string v6, "boxOffice"

    invoke-virtual {v1, v6}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 398
    const-string v4, "popcornScore"

    invoke-virtual {v1, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 399
    const-string v4, "popcornScore"

    const-string v6, "popcornScore"

    invoke-virtual {v1, v6}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 401
    :cond_0
    const-string v4, "rottenTomatoes"

    invoke-virtual {v1, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 402
    const-string v4, "rottenTomatoes"

    const-string v6, "rottenTomatoes"

    invoke-virtual {v1, v6}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 404
    :cond_1
    iget-object v4, p0, Lcom/flixster/android/activity/hc/ActorFragment$6;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/activity/hc/Main;

    const-class v6, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 405
    const-class v7, Lcom/flixster/android/activity/hc/ActorFragment;

    .line 404
    invoke-virtual {v4, v0, v6, v5, v7}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    .line 408
    .end local v0           #intent:Landroid/content/Intent;
    .end local v2           #movieProperties:[Ljava/lang/String;
    :cond_2
    return-void

    .line 394
    .restart local v0       #intent:Landroid/content/Intent;
    .restart local v2       #movieProperties:[Ljava/lang/String;
    :cond_3
    aget-object v3, v2, v4

    .line 395
    .local v3, propertyKey:Ljava/lang/String;
    invoke-virtual {v1, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 394
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
