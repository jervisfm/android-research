.class Lcom/flixster/android/activity/hc/ActorFragment$7;
.super Ljava/lang/Object;
.source "ActorFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/ActorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorFragment$7;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "view"

    .prologue
    .line 413
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .line 414
    .local v0, movie:Lnet/flixster/android/model/Movie;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->hasTrailer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 415
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorFragment$7;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lnet/flixster/android/Starter;->launchTrailer(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V

    .line 417
    :cond_0
    return-void
.end method
