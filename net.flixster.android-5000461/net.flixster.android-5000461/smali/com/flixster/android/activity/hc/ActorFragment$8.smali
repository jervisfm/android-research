.class Lcom/flixster/android/activity/hc/ActorFragment$8;
.super Ljava/lang/Object;
.source "ActorFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/ActorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    .line 420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 422
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->filmographyLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$2(Lcom/flixster/android/activity/hc/ActorFragment;)Landroid/widget/LinearLayout;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$0(Lcom/flixster/android/activity/hc/ActorFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$0(Lcom/flixster/android/activity/hc/ActorFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$0(Lcom/flixster/android/activity/hc/ActorFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x5

    if-le v3, v4, :cond_0

    .line 424
    const/4 v2, 0x0

    .line 425
    .local v2, movieView:Landroid/view/View;
    const/4 v0, 0x5

    .local v0, i:I
    :goto_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$0(Lcom/flixster/android/activity/hc/ActorFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_2

    .line 431
    .end local v0           #i:I
    .end local v2           #movieView:Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$4(Lcom/flixster/android/activity/hc/ActorFragment;)Landroid/widget/RelativeLayout;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 432
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$4(Lcom/flixster/android/activity/hc/ActorFragment;)Landroid/widget/RelativeLayout;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 434
    :cond_1
    return-void

    .line 426
    .restart local v0       #i:I
    .restart local v2       #movieView:Landroid/view/View;
    :cond_2
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$0(Lcom/flixster/android/activity/hc/ActorFragment;)Lnet/flixster/android/model/Actor;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    .line 427
    .local v1, movie:Lnet/flixster/android/model/Movie;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #calls: Lcom/flixster/android/activity/hc/ActorFragment;->getMovieView(Lnet/flixster/android/model/Movie;Landroid/view/View;)Landroid/view/View;
    invoke-static {v3, v1, v2}, Lcom/flixster/android/activity/hc/ActorFragment;->access$3(Lcom/flixster/android/activity/hc/ActorFragment;Lnet/flixster/android/model/Movie;Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    .line 428
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$8;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    #getter for: Lcom/flixster/android/activity/hc/ActorFragment;->filmographyLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->access$2(Lcom/flixster/android/activity/hc/ActorFragment;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 425
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
