.class Lcom/flixster/android/activity/hc/ActorFragment$9;
.super Landroid/os/Handler;
.source "ActorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/ActorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ActorFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ActorFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorFragment$9;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    .line 609
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "message"

    .prologue
    .line 612
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment$9;->this$0:Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/ActorFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 625
    :cond_0
    :goto_0
    return-void

    .line 616
    :cond_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/widget/ImageView;

    .line 617
    .local v1, imageView:Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 618
    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Movie;

    .line 619
    .local v2, movie:Lnet/flixster/android/model/Movie;
    if-eqz v2, :cond_0

    .line 620
    iget-object v3, v2, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 621
    .local v0, bitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 622
    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0
.end method
