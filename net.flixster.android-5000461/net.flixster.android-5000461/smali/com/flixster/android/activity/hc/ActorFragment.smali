.class public Lcom/flixster/android/activity/hc/ActorFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "ActorFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;
    }
.end annotation


# static fields
.field private static final DIALOG_ACTOR:I = 0x2

.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field public static final KEY_ACTOR_ID:Ljava/lang/String; = "ACTOR_ID"

.field public static final KEY_ACTOR_NAME:Ljava/lang/String; = "ACTOR_NAME"

.field private static final MAX_MOVIES:I = 0x5

.field private static final MAX_PHOTOS:I = 0xf

.field private static final MAX_PHOTO_COUNT:I = 0x32


# instance fields
.field private actor:Lnet/flixster/android/model/Actor;

.field private actorBiographyClickListener:Landroid/view/View$OnClickListener;

.field private final actorThumbnailHandler:Landroid/os/Handler;

.field private filmographyLayout:Landroid/widget/LinearLayout;

.field private mActorId:J

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private moreFilmographyLayout:Landroid/widget/RelativeLayout;

.field private moreFilmographyListener:Landroid/view/View$OnClickListener;

.field private movieClickListener:Landroid/view/View$OnClickListener;

.field private final movieThumbnailHandler:Landroid/os/Handler;

.field private photoClickListener:Landroid/view/View$OnClickListener;

.field private photoHandler:Landroid/os/Handler;

.field private resources:Landroid/content/res/Resources;

.field private trailerClickListener:Landroid/view/View$OnClickListener;

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 164
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment$1;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->updateHandler:Landroid/os/Handler;

    .line 314
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment$2;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->actorThumbnailHandler:Landroid/os/Handler;

    .line 333
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment$3;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->photoHandler:Landroid/os/Handler;

    .line 353
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment$4;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->actorBiographyClickListener:Landroid/view/View$OnClickListener;

    .line 363
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment$5;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment$5;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->photoClickListener:Landroid/view/View$OnClickListener;

    .line 382
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment$6;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment$6;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->movieClickListener:Landroid/view/View$OnClickListener;

    .line 411
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment$7;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment$7;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->trailerClickListener:Landroid/view/View$OnClickListener;

    .line 420
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment$8;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment$8;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyListener:Landroid/view/View$OnClickListener;

    .line 609
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment$9;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment$9;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->movieThumbnailHandler:Landroid/os/Handler;

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/ActorFragment;)Lnet/flixster/android/model/Actor;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/ActorFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->updatePage()V

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/ActorFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->filmographyLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/ActorFragment;Lnet/flixster/android/model/Movie;Landroid/view/View;)Landroid/view/View;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 438
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/hc/ActorFragment;->getMovieView(Lnet/flixster/android/model/Movie;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/ActorFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/ActorFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/ActorFragment;)J
    .locals 2
    .parameter

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->mActorId:J

    return-wide v0
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/ActorFragment;Lnet/flixster/android/model/Actor;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    return-void
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/ActorFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 164
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ActorFragment;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addScore(ILandroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V
    .locals 4
    .parameter "score"
    .parameter "scoreIcon"
    .parameter "scoreView"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 603
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 604
    invoke-virtual {p3, p2, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 605
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 606
    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 607
    return-void
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 655
    const/16 v0, 0x67

    return v0
.end method

.method private getMovieView(Lnet/flixster/android/model/Movie;Landroid/view/View;)Landroid/view/View;
    .locals 30
    .parameter "movie"
    .parameter "movieView"

    .prologue
    .line 440
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030053

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 441
    new-instance v26, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;

    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-direct {v0, v4}, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;-><init>(Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;)V

    .line 442
    .local v26, viewHolder:Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;
    const v4, 0x7f07012d

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v26

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->movieLayout:Landroid/widget/RelativeLayout;

    .line 443
    const v4, 0x7f070108

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v26

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->titleView:Landroid/widget/TextView;

    .line 444
    const v4, 0x7f07012e

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v26

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    .line 445
    const v4, 0x7f07012f

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v26

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->trailerView:Landroid/widget/ImageView;

    .line 446
    const v4, 0x7f070132

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v26

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->scoreView:Landroid/widget/TextView;

    .line 447
    const v4, 0x7f070133

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v26

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    .line 448
    const v4, 0x7f07010c

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v26

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->metaView:Landroid/widget/TextView;

    .line 449
    const v4, 0x7f070131

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v26

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->charView:Landroid/widget/TextView;

    .line 450
    const v4, 0x7f07010d

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v26

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->releaseView:Landroid/widget/TextView;

    .line 454
    const-string v4, "title"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 455
    .local v22, title:Ljava/lang/String;
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->titleView:Landroid/widget/TextView;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 457
    move-object/from16 v0, p1

    iget-object v4, v0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 458
    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    iget-object v4, v0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 469
    :cond_0
    :goto_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lnet/flixster/android/model/Movie;->mActorPageChars:Ljava/lang/String;

    if-eqz v4, :cond_7

    move-object/from16 v0, p1

    iget-object v4, v0, Lnet/flixster/android/model/Movie;->mActorPageChars:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_7

    .line 470
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->charView:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v5, v0, Lnet/flixster/android/model/Movie;->mActorPageChars:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 471
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->charView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 478
    :goto_1
    const-string v4, "high"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 479
    .local v25, trailerUrl:Ljava/lang/String;
    if-eqz v25, :cond_1

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 480
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->trailerView:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 481
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->trailerView:Landroid/widget/ImageView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 482
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->trailerView:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flixster/android/activity/hc/ActorFragment;->trailerClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 483
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->trailerView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 485
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 486
    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-nez v4, :cond_8

    const/4 v13, 0x0

    .line 487
    .local v13, isReleased:Z
    :goto_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v17

    .line 488
    .local v17, ratingType:I
    packed-switch v17, :pswitch_data_0

    .line 521
    :cond_2
    :goto_3
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_d

    .line 522
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 574
    :goto_4
    const-string v4, "meta"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 575
    .local v15, meta:Ljava/lang/String;
    if-eqz v15, :cond_3

    .line 576
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->metaView:Landroid/widget/TextView;

    invoke-virtual {v4, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 577
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->metaView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 580
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v18

    .line 581
    .local v18, releaseDate:Ljava/util/Date;
    if-nez v18, :cond_4

    .line 582
    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Movie;->getDvdReleaseDate()Ljava/util/Date;

    move-result-object v18

    .line 585
    :cond_4
    const/16 v19, 0x0

    .line 586
    .local v19, releaseYear:Ljava/lang/String;
    if-eqz v18, :cond_5

    .line 588
    invoke-virtual/range {v18 .. v18}, Ljava/util/Date;->getYear()I

    move-result v4

    add-int/lit16 v4, v4, 0x76c

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    .line 589
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "title"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 590
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->titleView:Landroid/widget/TextView;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 595
    :cond_5
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->releaseView:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 597
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->movieLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 598
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->movieLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flixster/android/activity/hc/ActorFragment;->movieClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599
    return-object p2

    .line 460
    .end local v13           #isReleased:Z
    .end local v15           #meta:Ljava/lang/String;
    .end local v17           #ratingType:I
    .end local v18           #releaseDate:Ljava/util/Date;
    .end local v19           #releaseYear:Ljava/lang/String;
    .end local v25           #trailerUrl:Ljava/lang/String;
    :cond_6
    const-string v4, "thumbnail"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 461
    .local v6, thumbnailUrl:Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 462
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 463
    new-instance v3, Lnet/flixster/android/model/ImageOrder;

    const/4 v4, 0x0

    .line 464
    move-object/from16 v0, v26

    iget-object v7, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/flixster/android/activity/hc/ActorFragment;->movieThumbnailHandler:Landroid/os/Handler;

    move-object/from16 v5, p1

    .line 463
    invoke-direct/range {v3 .. v8}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 465
    .local v3, imageOrder:Lnet/flixster/android/model/ImageOrder;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v4, v3}, Lcom/flixster/android/activity/hc/Main;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    goto/16 :goto_0

    .line 474
    .end local v3           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .end local v6           #thumbnailUrl:Ljava/lang/String;
    :cond_7
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->charView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->charView:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 486
    .restart local v25       #trailerUrl:Ljava/lang/String;
    :cond_8
    const/4 v13, 0x1

    goto/16 :goto_2

    .line 492
    .restart local v13       #isReleased:Z
    .restart local v17       #ratingType:I
    :pswitch_0
    const-string v4, "popcornScore"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 493
    const-string v4, "popcornScore"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 494
    .local v9, flixsterScore:I
    if-lez v9, :cond_2

    .line 495
    const/16 v4, 0x3c

    if-ge v9, v4, :cond_9

    const/4 v14, 0x1

    .line 496
    .local v14, isSpilled:Z
    :goto_5
    if-nez v13, :cond_a

    const v12, 0x7f0200f6

    .line 498
    .local v12, iconId:I
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    invoke-virtual {v4, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v21

    .line 499
    .local v21, scoreIcon:Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->scoreView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v9, v1, v4}, Lcom/flixster/android/activity/hc/ActorFragment;->addScore(ILandroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 495
    .end local v12           #iconId:I
    .end local v14           #isSpilled:Z
    .end local v21           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_9
    const/4 v14, 0x0

    goto :goto_5

    .line 496
    .restart local v14       #isSpilled:Z
    :cond_a
    if-eqz v14, :cond_b

    const v12, 0x7f0200ef

    goto :goto_6

    .line 497
    :cond_b
    const v12, 0x7f0200ea

    goto :goto_6

    .line 506
    .end local v9           #flixsterScore:I
    .end local v14           #isSpilled:Z
    :pswitch_1
    const-string v4, "rottenTomatoes"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 507
    const-string v4, "rottenTomatoes"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v24

    .line 508
    .local v24, tomatoScore:I
    if-lez v24, :cond_2

    .line 510
    const/16 v4, 0x3c

    move/from16 v0, v24

    if-ge v0, v4, :cond_c

    .line 511
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0200ed

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .line 515
    .local v23, tomatoIcon:Landroid/graphics/drawable/Drawable;
    :goto_7
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->scoreView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move/from16 v1, v24

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2, v4}, Lcom/flixster/android/activity/hc/ActorFragment;->addScore(ILandroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 513
    .end local v23           #tomatoIcon:Landroid/graphics/drawable/Drawable;
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0200de

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .restart local v23       #tomatoIcon:Landroid/graphics/drawable/Drawable;
    goto :goto_7

    .line 523
    .end local v23           #tomatoIcon:Landroid/graphics/drawable/Drawable;
    .end local v24           #tomatoScore:I
    :cond_d
    const-string v4, "FRIENDS_RATED_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 524
    const-string v4, "FRIENDS_RATED_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_f

    .line 525
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0200eb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    .line 526
    .local v20, reviewIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 527
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v4, v0, v5, v7, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 528
    const-string v4, "FRIENDS_RATED_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 529
    .local v10, friendsRatedCount:I
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 530
    .local v16, ratingCountBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    const/4 v4, 0x1

    if-le v10, v4, :cond_e

    .line 532
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c0090

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 536
    :goto_8
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 537
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 534
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c008f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 538
    .end local v10           #friendsRatedCount:I
    .end local v16           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v20           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_f
    const-string v4, "FRIENDS_WTS_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 539
    const-string v4, "FRIENDS_WTS_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_13

    .line 540
    const-string v4, "FRIENDS_WTS_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 541
    .local v11, friendsWantToSeeCount:I
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    .line 542
    .local v27, wantToSeeCountBuilder:Ljava/lang/StringBuilder;
    if-nez v13, :cond_11

    if-nez v17, :cond_11

    .line 543
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v29, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v4, v5, v7, v8, v0}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 544
    const-string v4, "("

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 545
    const/4 v4, 0x1

    if-le v11, v4, :cond_10

    .line 546
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c008e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    :goto_9
    const-string v4, ")"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    :goto_a
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 563
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 548
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c008d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 552
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0200f7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v28

    .line 553
    .local v28, wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 554
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v4, v0, v5, v7, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 555
    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    const/4 v4, 0x1

    if-le v11, v4, :cond_12

    .line 557
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c0094

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_a

    .line 559
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c0093

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_a

    .line 565
    .end local v11           #friendsWantToSeeCount:I
    .end local v27           #wantToSeeCountBuilder:Ljava/lang/StringBuilder;
    .end local v28           #wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    :cond_13
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/ActorFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 76
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/ActorFragment;-><init>()V

    .line 77
    .local v0, af:Lcom/flixster/android/activity/hc/ActorFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment;->setArguments(Landroid/os/Bundle;)V

    .line 78
    return-object v0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 139
    new-instance v0, Lcom/flixster/android/activity/hc/ActorFragment$11;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ActorFragment$11;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    .line 159
    .local v0, updatePageTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 162
    :cond_0
    return-void
.end method

.method private updatePage()V
    .locals 32

    .prologue
    .line 183
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    if-eqz v4, :cond_4

    .line 186
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    .line 187
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getView()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f070028

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    .line 188
    .local v17, actorLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f07002b

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 189
    .local v7, thumbnailView:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_5

    .line 190
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 204
    :goto_0
    const v4, 0x7f07002c

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 205
    .local v18, actorName:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    const v4, 0x7f07002d

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 207
    .local v15, actorBirthday:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->birthDay:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 208
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Birthday: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v5, v5, Lnet/flixster/android/model/Actor;->birthDay:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    const/4 v4, 0x0

    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 213
    :goto_1
    const v4, 0x7f07002e

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 214
    .local v16, actorBirthplace:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    if-eqz v4, :cond_8

    const-string v4, ""

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v5, v5, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 215
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Birthplace: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v5, v5, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 220
    :goto_2
    const v4, 0x7f07002f

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 221
    .local v19, biographyTitle:Landroid/widget/TextView;
    const v4, 0x7f070030

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 222
    .local v14, actorBiography:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    if-eqz v4, :cond_9

    const-string v4, ""

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v5, v5, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 223
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 224
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    const/4 v4, 0x1

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 227
    const/4 v4, 0x1

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 228
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actorBiographyClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    :goto_3
    const v4, 0x7f070031

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/TextView;

    .line 235
    .local v28, photosTitle:Landroid/widget/TextView;
    const v4, 0x7f070033

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/TextView;

    .line 236
    .local v30, viewPhotos:Landroid/widget/TextView;
    const v4, 0x7f070032

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/LinearLayout;

    .line 237
    .local v25, photoLayout:Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v0, v4, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    .line 238
    .local v27, photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    if-eqz v27, :cond_d

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_d

    .line 239
    invoke-virtual/range {v25 .. v25}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 240
    const/16 v20, 0x0

    .local v20, i:I
    :goto_4
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v20

    if-ge v0, v4, :cond_0

    const/16 v4, 0xf

    move/from16 v0, v20

    if-lt v0, v4, :cond_a

    .line 267
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 269
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0040

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 270
    .local v26, photoLinkBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget v0, v4, Lnet/flixster/android/model/Actor;->photoCount:I

    move/from16 v24, v0

    .line 271
    .local v24, photoCount:I
    const/16 v4, 0x32

    move/from16 v0, v24

    if-le v0, v4, :cond_1

    .line 272
    const/16 v24, 0x32

    .line 274
    :cond_1
    const-string v4, " ("

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    const/16 v4, 0x8

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 278
    const/4 v4, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 279
    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    .end local v20           #i:I
    .end local v24           #photoCount:I
    .end local v26           #photoLinkBuilder:Ljava/lang/StringBuilder;
    :goto_5
    const v4, 0x7f070035

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->filmographyLayout:Landroid/widget/LinearLayout;

    .line 286
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 287
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->filmographyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 288
    const v4, 0x7f070034

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    .line 289
    .local v31, wantToSeeText:Landroid/widget/TextView;
    const/4 v4, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 290
    const/16 v23, 0x0

    .line 291
    .local v23, movieView:Landroid/view/View;
    const/16 v20, 0x0

    .restart local v20       #i:I
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v20

    if-ge v0, v4, :cond_2

    const/4 v4, 0x5

    move/from16 v0, v20

    if-lt v0, v4, :cond_e

    .line 296
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x5

    if-le v4, v5, :cond_3

    .line 297
    const v4, 0x7f070036

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    .line 298
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 299
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    .line 300
    .local v29, totalBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 301
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flixster/android/activity/hc/ActorFragment;->resources:Landroid/content/res/Resources;

    const v8, 0x7f0c0080

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    .line 303
    const v5, 0x7f070038

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v21

    .line 302
    check-cast v21, Landroid/widget/TextView;

    .line 304
    .local v21, moreFilmographyLabel:Landroid/widget/TextView;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 306
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 307
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flixster/android/activity/hc/ActorFragment;->moreFilmographyListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    .end local v20           #i:I
    .end local v21           #moreFilmographyLabel:Landroid/widget/TextView;
    .end local v23           #movieView:Landroid/view/View;
    .end local v29           #totalBuilder:Ljava/lang/StringBuilder;
    .end local v31           #wantToSeeText:Landroid/widget/TextView;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->trackPage()V

    .line 312
    .end local v7           #thumbnailView:Landroid/widget/ImageView;
    .end local v14           #actorBiography:Landroid/widget/TextView;
    .end local v15           #actorBirthday:Landroid/widget/TextView;
    .end local v16           #actorBirthplace:Landroid/widget/TextView;
    .end local v17           #actorLayout:Landroid/widget/LinearLayout;
    .end local v18           #actorName:Landroid/widget/TextView;
    .end local v19           #biographyTitle:Landroid/widget/TextView;
    .end local v25           #photoLayout:Landroid/widget/LinearLayout;
    .end local v27           #photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    .end local v28           #photosTitle:Landroid/widget/TextView;
    .end local v30           #viewPhotos:Landroid/widget/TextView;
    :cond_4
    return-void

    .line 192
    .restart local v7       #thumbnailView:Landroid/widget/ImageView;
    .restart local v17       #actorLayout:Landroid/widget/LinearLayout;
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v6, v4, Lnet/flixster/android/model/Actor;->largeUrl:Ljava/lang/String;

    .line 193
    .local v6, thumbnailUrl:Ljava/lang/String;
    if-eqz v6, :cond_6

    .line 194
    const v4, 0x7f020151

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 195
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 196
    new-instance v3, Lnet/flixster/android/model/ImageOrder;

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    .line 197
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actorThumbnailHandler:Landroid/os/Handler;

    .line 196
    invoke-direct/range {v3 .. v8}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 198
    .local v3, imageOrder:Lnet/flixster/android/model/ImageOrder;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v4, v3}, Lcom/flixster/android/activity/hc/Main;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    goto/16 :goto_0

    .line 200
    .end local v3           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_6
    const v4, 0x7f0201da

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 211
    .end local v6           #thumbnailUrl:Ljava/lang/String;
    .restart local v15       #actorBirthday:Landroid/widget/TextView;
    .restart local v18       #actorName:Landroid/widget/TextView;
    :cond_7
    const/16 v4, 0x8

    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 218
    .restart local v16       #actorBirthplace:Landroid/widget/TextView;
    :cond_8
    const/16 v4, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 231
    .restart local v14       #actorBiography:Landroid/widget/TextView;
    .restart local v19       #biographyTitle:Landroid/widget/TextView;
    :cond_9
    const/16 v4, 0x8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    const/16 v4, 0x8

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 241
    .restart local v20       #i:I
    .restart local v25       #photoLayout:Landroid/widget/LinearLayout;
    .restart local v27       #photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    .restart local v28       #photosTitle:Landroid/widget/TextView;
    .restart local v30       #viewPhotos:Landroid/widget/TextView;
    :cond_a
    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lnet/flixster/android/model/Photo;

    .line 242
    .local v10, photo:Lnet/flixster/android/model/Photo;
    new-instance v12, Landroid/widget/ImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v12, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 244
    .local v12, photoView:Landroid/widget/ImageView;
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 245
    const v8, 0x7f0a0037

    .line 244
    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 245
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 246
    const v9, 0x7f0a0037

    .line 245
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    invoke-direct {v4, v5, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 244
    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 247
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a002b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v12, v4, v5, v8, v9}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 252
    invoke-virtual {v12, v10}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 253
    iget-object v4, v10, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_c

    .line 254
    iget-object v4, v10, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 263
    :cond_b
    :goto_7
    const/4 v4, 0x1

    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 264
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->photoClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 240
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_4

    .line 256
    :cond_c
    const v4, 0x7f020151

    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 257
    iget-object v4, v10, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    if-eqz v4, :cond_b

    iget-object v4, v10, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    const-string v5, "http://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 258
    new-instance v3, Lnet/flixster/android/model/ImageOrder;

    const/4 v9, 0x3

    .line 259
    iget-object v11, v10, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/ActorFragment;->photoHandler:Landroid/os/Handler;

    move-object v8, v3

    .line 258
    invoke-direct/range {v8 .. v13}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 260
    .restart local v3       #imageOrder:Lnet/flixster/android/model/ImageOrder;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v4, v3}, Lcom/flixster/android/activity/hc/Main;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_7

    .line 281
    .end local v3           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .end local v10           #photo:Lnet/flixster/android/model/Photo;
    .end local v12           #photoView:Landroid/widget/ImageView;
    .end local v20           #i:I
    :cond_d
    const/16 v4, 0x8

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 282
    const/16 v4, 0x8

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 283
    const/16 v4, 0x8

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 292
    .restart local v20       #i:I
    .restart local v23       #movieView:Landroid/view/View;
    .restart local v31       #wantToSeeText:Landroid/widget/TextView;
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lnet/flixster/android/model/Movie;

    .line 293
    .local v22, movie:Lnet/flixster/android/model/Movie;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/activity/hc/ActorFragment;->getMovieView(Lnet/flixster/android/model/Movie;Landroid/view/View;)Landroid/view/View;

    move-result-object v23

    .line 294
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/ActorFragment;->filmographyLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 291
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_6
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 642
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 652
    :cond_0
    :goto_0
    return-void

    .line 644
    :pswitch_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    if-eqz v1, :cond_0

    .line 645
    new-instance v0, Landroid/content/Intent;

    const-string v1, "PHOTOS"

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lnet/flixster/android/ActorGalleryPage;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 646
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "ACTOR_ID"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-wide v2, v2, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 647
    const-string v1, "ACTOR_NAME"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v2, v2, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 648
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/hc/ActorFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 642
    :pswitch_data_0
    .packed-switch 0x7f070033
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "dialogId"

    .prologue
    const v5, 0x7f0c0135

    const/4 v4, 0x1

    .line 106
    packed-switch p1, :pswitch_data_0

    .line 128
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 129
    .local v1, defaultDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 130
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 131
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 132
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    move-object v2, v1

    .line 133
    .end local v1           #defaultDialog:Landroid/app/ProgressDialog;
    :goto_0
    return-object v2

    .line 109
    :pswitch_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 110
    .local v2, ratingsDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 111
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 112
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 113
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 116
    .end local v2           #ratingsDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 117
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v3, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 118
    const-string v3, "Network Error"

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 119
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 120
    const-string v3, "Retry"

    new-instance v4, Lcom/flixster/android/activity/hc/ActorFragment$10;

    invoke-direct {v4, p0}, Lcom/flixster/android/activity/hc/ActorFragment$10;-><init>(Lcom/flixster/android/activity/hc/ActorFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 125
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 126
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 83
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ActorFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 84
    iget-object v4, p0, Lcom/flixster/android/activity/hc/ActorFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030018

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 85
    .local v3, view:Landroid/view/View;
    iget-wide v4, p0, Lcom/flixster/android/activity/hc/ActorFragment;->mActorId:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 87
    .local v1, bundle:Landroid/os/Bundle;
    const-string v4, "ACTOR_ID"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/flixster/android/activity/hc/ActorFragment;->mActorId:J

    .line 88
    const-string v4, "ACTOR_NAME"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 89
    const-string v4, "ACTOR_NAME"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 90
    .local v2, name:Ljava/lang/String;
    const v4, 0x7f07002c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 91
    .local v0, actorTitle:Landroid/widget/TextView;
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    .end local v0           #actorTitle:Landroid/widget/TextView;
    .end local v1           #bundle:Landroid/os/Bundle;
    .end local v2           #name:Ljava/lang/String;
    :cond_0
    return-object v3
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 99
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 100
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/ActorFragment;->scheduleUpdatePageTask()V

    .line 102
    return-void
.end method

.method public trackPage()V
    .locals 4

    .prologue
    .line 660
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/actor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Actor Actor Page for actor:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/ActorFragment;->actor:Lnet/flixster/android/model/Actor;

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    return-void
.end method
