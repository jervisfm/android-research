.class Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;
.super Landroid/os/Handler;
.source "BoxOfficeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/BoxOfficeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    .line 118
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    const/4 v5, 0x0

    .line 121
    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    .line 122
    .local v3, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->isRemoving()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    const-wide/16 v1, 0x0

    .line 127
    .local v1, id:J
    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeFeatured:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$0(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 128
    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeFeatured:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$0(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v1

    .line 135
    :cond_2
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_0

    .line 136
    new-instance v0, Landroid/content/Intent;

    const-string v4, "DETAILS"

    const/4 v5, 0x0

    const-class v6, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v0, v4, v5, v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    .local v0, i:Landroid/content/Intent;
    const-string v4, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 138
    const-class v4, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v3, v0, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 129
    .end local v0           #i:Landroid/content/Intent;
    :cond_3
    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOtw:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$1(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 130
    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOtw:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$1(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v1

    goto :goto_1

    .line 131
    :cond_4
    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeTbo:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$2(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 132
    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeTbo:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$2(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v1

    goto :goto_1
.end method
