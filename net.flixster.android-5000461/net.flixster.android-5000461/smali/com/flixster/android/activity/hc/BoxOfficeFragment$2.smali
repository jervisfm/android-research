.class Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;
.super Ljava/util/TimerTask;
.source "BoxOfficeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/BoxOfficeFragment;->ScheduleLoadItemsTask(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

.field private final synthetic val$sortOption:I


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/BoxOfficeFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    iput p2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->val$sortOption:I

    .line 56
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 60
    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 61
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->isRemoving()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "BoxOfficePage.ScheduleLoadItemsTask.run sortOption:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->val$sortOption:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :try_start_0
    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOtw:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$1(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 70
    iget-object v4, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeFeatured:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$0(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOtw:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$1(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeTbo:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$2(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v6

    iget-object v7, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOthers:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$3(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;

    move-result-object v7

    .line 71
    iget-object v8, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    iget v8, v8, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mRetryCount:I

    if-nez v8, :cond_2

    move v2, v3

    .line 70
    :cond_2
    invoke-static {v4, v5, v6, v7, v2}, Lnet/flixster/android/data/MovieDao;->fetchBoxOffice(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V

    .line 75
    :cond_3
    iget v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->val$sortOption:I

    packed-switch v2, :pswitch_data_0

    .line 90
    :goto_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->trackPage()V

    .line 91
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mUpdateHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 93
    if-eqz v1, :cond_4

    .line 94
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 96
    :cond_4
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    iget-boolean v2, v2, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->isInitialContentLoaded:Z

    if-nez v2, :cond_5

    .line 97
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->isInitialContentLoaded:Z

    .line 98
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #getter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->initialContentLoadingHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$8(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :cond_5
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 77
    :pswitch_0
    :try_start_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    const/4 v4, 0x1

    #setter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mSortOption:I
    invoke-static {v2, v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$4(Lcom/flixster/android/activity/hc/BoxOfficeFragment;I)V

    .line 78
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #calls: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->setPopularLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$5(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v2, "FlxMain"

    const-string v4, "BoxOfficePage.ScheduleLoadItemsTask.run DaoException"

    invoke-static {v2, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 102
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    invoke-virtual {v2, v0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 81
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_1
    :try_start_3
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    const/4 v4, 0x2

    #setter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mSortOption:I
    invoke-static {v2, v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$4(Lcom/flixster/android/activity/hc/BoxOfficeFragment;I)V

    .line 82
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #calls: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->setRatingLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$6(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 103
    :catchall_0
    move-exception v2

    .line 104
    if-eqz v1, :cond_6

    iget-object v4, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v4, :cond_6

    iget-object v4, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 105
    iget-object v4, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 107
    :cond_6
    throw v2

    .line 85
    :pswitch_2
    :try_start_4
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    const/4 v4, 0x3

    #setter for: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mSortOption:I
    invoke-static {v2, v4}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$4(Lcom/flixster/android/activity/hc/BoxOfficeFragment;I)V

    .line 86
    iget-object v2, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;->this$0:Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    #calls: Lcom/flixster/android/activity/hc/BoxOfficeFragment;->setAlphaLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->access$7(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_1

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
