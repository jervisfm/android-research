.class public Lcom/flixster/android/activity/hc/BoxOfficeFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "BoxOfficeFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final LISTSTATE_SORT:Ljava/lang/String; = "LISTSTATE_SORT"


# instance fields
.field public final SORT_ALPHA:I

.field public final SORT_POPULAR:I

.field public final SORT_RATING:I

.field public final SORT_START:I

.field private initialContentLoadingHandler:Landroid/os/Handler;

.field private final mBoxOfficeFeatured:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mBoxOfficeOthers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mBoxOfficeOtw:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mBoxOfficeTbo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private mSortOption:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 29
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->SORT_START:I

    .line 32
    iput v1, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->SORT_POPULAR:I

    .line 33
    const/4 v0, 0x2

    iput v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->SORT_RATING:I

    .line 34
    const/4 v0, 0x3

    iput v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->SORT_ALPHA:I

    .line 35
    iput v1, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mSortOption:I

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOtw:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeTbo:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOthers:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment$1;-><init>(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    .line 29
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(IJ)V
    .locals 4
    .parameter "sortOption"
    .parameter "delay"

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 56
    new-instance v0, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/activity/hc/BoxOfficeFragment$2;-><init>(Lcom/flixster/android/activity/hc/BoxOfficeFragment;I)V

    .line 112
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 113
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 114
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v0, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :cond_0
    monitor-exit p0

    return-void

    .line 54
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    .end local v1           #main:Lcom/flixster/android/activity/hc/Main;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOtw:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeTbo:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOthers:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/BoxOfficeFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35
    iput p1, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mSortOption:I

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->setPopularLviList()V

    return-void
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->setRatingLviList()V

    return-void
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 258
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->setAlphaLviList()V

    return-void
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/BoxOfficeFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 118
    iget-object v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x1

    return v0
.end method

.method private setAlphaLviList()V
    .locals 12

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Lcom/flixster/android/activity/hc/Main;

    .line 260
    .local v6, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->isRemoving()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 268
    .local v0, completeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 269
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOtw:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 270
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeTbo:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 271
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOthers:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 273
    new-instance v8, Lnet/flixster/android/model/MovieTitleComparator;

    invoke-direct {v8}, Lnet/flixster/android/model/MovieTitleComparator;-><init>()V

    .line 274
    .local v8, movieTitleComparator:Lnet/flixster/android/model/MovieTitleComparator;
    invoke-static {v0, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 276
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 283
    const/4 v3, 0x0

    .line 284
    .local v3, indexChar:C
    const/4 v1, 0x0

    .line 286
    .local v1, firstLetter:C
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 304
    new-instance v2, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 305
    .local v2, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 286
    .end local v2           #footer:Lnet/flixster/android/lvi/LviFooter;
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/model/Movie;

    .line 288
    .local v7, movie:Lnet/flixster/android/model/Movie;
    const-string v10, "title"

    invoke-virtual {v7, v10}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 289
    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v10

    if-nez v10, :cond_3

    .line 290
    const/16 v1, 0x23

    .line 292
    :cond_3
    if-eq v3, v1, :cond_4

    .line 293
    new-instance v5, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 294
    .local v5, lviSubHeader:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 295
    iget-object v10, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    move v3, v1

    .line 298
    .end local v5           #lviSubHeader:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_4
    new-instance v4, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 299
    .local v4, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v7, v4, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 300
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v4, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 301
    iget-object v10, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private setPopularLviList()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 164
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/activity/hc/Main;

    .line 165
    .local v4, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->isRemoving()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 178
    .local v0, boxOfficeLists:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;>;"
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOtw:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeTbo:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOthers:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    new-array v8, v14, [Ljava/lang/String;

    .line 184
    .local v8, subheaderList:[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v12, 0x7f0c00be

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    .line 185
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v12, 0x7f0c00bf

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    .line 186
    const/4 v9, 0x2

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0c00c0

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v8, v9

    .line 187
    const/4 v9, 0x3

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0c00c1

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v8, v9

    .line 191
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-lt v2, v14, :cond_2

    .line 208
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 209
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 192
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    :cond_2
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 193
    .local v6, movieList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 194
    new-instance v7, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v7}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 195
    .local v7, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    aget-object v9, v8, v2

    iput-object v9, v7, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 196
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    .line 191
    .end local v7           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 198
    .restart local v7       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_4
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Movie;

    .line 199
    .local v5, movie:Lnet/flixster/android/model/Movie;
    new-instance v3, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 200
    .local v3, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v5, v3, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 201
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v9

    iput-object v9, v3, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 202
    if-nez v2, :cond_5

    move v9, v10

    :goto_3
    iput-boolean v9, v3, Lnet/flixster/android/lvi/LviMovie;->mIsFeatured:Z

    .line 203
    iget-object v9, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    move v9, v11

    .line 202
    goto :goto_3
.end method

.method private setRatingLviList()V
    .locals 13

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/flixster/android/activity/hc/Main;

    .line 214
    .local v5, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->isRemoving()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 221
    .local v0, completeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    iget-object v10, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 222
    iget-object v10, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOtw:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 223
    iget-object v10, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeTbo:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 224
    iget-object v10, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mBoxOfficeOthers:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 226
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v9

    .line 227
    .local v9, rt:I
    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 228
    new-instance v8, Lnet/flixster/android/model/MovieRottenScoreComparator;

    invoke-direct {v8}, Lnet/flixster/android/model/MovieRottenScoreComparator;-><init>()V

    .line 229
    .local v8, movieRottenScoreComparator:Lnet/flixster/android/model/MovieRottenScoreComparator;
    invoke-static {v0, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 235
    .end local v8           #movieRottenScoreComparator:Lnet/flixster/android/model/MovieRottenScoreComparator;
    :goto_1
    iget-object v10, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 242
    const-wide/16 v2, 0x0

    .line 243
    .local v2, lastMovieId:J
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_4

    .line 253
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 254
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v10, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 231
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    .end local v2           #lastMovieId:J
    :cond_3
    new-instance v7, Lnet/flixster/android/model/MoviePopcornComparator;

    invoke-direct {v7}, Lnet/flixster/android/model/MoviePopcornComparator;-><init>()V

    .line 232
    .local v7, moviePopcornComparator:Lnet/flixster/android/model/MoviePopcornComparator;
    invoke-static {v0, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1

    .line 243
    .end local v7           #moviePopcornComparator:Lnet/flixster/android/model/MoviePopcornComparator;
    .restart local v2       #lastMovieId:J
    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lnet/flixster/android/model/Movie;

    .line 244
    .local v6, movie:Lnet/flixster/android/model/Movie;
    invoke-virtual {v6}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v11

    cmp-long v11, v2, v11

    if-eqz v11, :cond_2

    .line 245
    invoke-virtual {v6}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v2

    .line 246
    new-instance v4, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 247
    .local v4, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v6, v4, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 248
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v11

    iput-object v11, v4, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 249
    iget-object v11, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method


# virtual methods
.method public onResume()V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 50
    iget v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mSortOption:I

    const-wide/16 v1, 0x64

    invoke-direct {p0, v0, v1, v2}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->ScheduleLoadItemsTask(IJ)V

    .line 51
    return-void
.end method

.method protected retryAction()V
    .locals 3

    .prologue
    .line 145
    iget v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mSortOption:I

    const-wide/16 v1, 0x3e8

    invoke-direct {p0, v0, v1, v2}, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->ScheduleLoadItemsTask(IJ)V

    .line 146
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 150
    iget v0, p0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;->mSortOption:I

    packed-switch v0, :pswitch_data_0

    .line 161
    :goto_0
    return-void

    .line 152
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/boxoffice/popular"

    const-string v2, "Box Office - Popular"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/boxoffice/rating"

    const-string v2, "Box Office - Rating"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :pswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/boxoffice/title"

    const-string v2, "Box Office - Title"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
