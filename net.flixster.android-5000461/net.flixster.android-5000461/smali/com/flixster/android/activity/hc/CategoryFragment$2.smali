.class Lcom/flixster/android/activity/hc/CategoryFragment$2;
.super Ljava/util/TimerTask;
.source "CategoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/CategoryFragment;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/CategoryFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/CategoryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    .line 70
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 73
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/CategoryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 74
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/CategoryFragment;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    #getter for: Lcom/flixster/android/activity/hc/CategoryFragment;->mCategory:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/CategoryFragment;->access$0(Lcom/flixster/android/activity/hc/CategoryFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 80
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    #getter for: Lcom/flixster/android/activity/hc/CategoryFragment;->mCategory:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/CategoryFragment;->access$0(Lcom/flixster/android/activity/hc/CategoryFragment;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    #getter for: Lcom/flixster/android/activity/hc/CategoryFragment;->mCategoryFilter:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/CategoryFragment;->access$1(Lcom/flixster/android/activity/hc/CategoryFragment;)Ljava/lang/String;

    move-result-object v6

    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    iget v2, v2, Lcom/flixster/android/activity/hc/CategoryFragment;->mRetryCount:I

    if-nez v2, :cond_4

    move v2, v3

    :goto_1
    invoke-static {v5, v6, v2}, Lnet/flixster/android/data/MovieDao;->fetchDvdCategory(Ljava/util/List;Ljava/lang/String;Z)V

    .line 83
    :cond_2
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    #calls: Lcom/flixster/android/activity/hc/CategoryFragment;->setMovieLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/CategoryFragment;->access$2(Lcom/flixster/android/activity/hc/CategoryFragment;)V

    .line 85
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/CategoryFragment;->mUpdateHandler:Landroid/os/Handler;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 87
    if-eqz v1, :cond_3

    .line 88
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 90
    :cond_3
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    iget-boolean v2, v2, Lcom/flixster/android/activity/hc/CategoryFragment;->isInitialContentLoaded:Z

    if-nez v2, :cond_0

    .line 91
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    const/4 v5, 0x1

    iput-boolean v5, v2, Lcom/flixster/android/activity/hc/CategoryFragment;->isInitialContentLoaded:Z

    .line 92
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    #getter for: Lcom/flixster/android/activity/hc/CategoryFragment;->initialContentLoadingHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/CategoryFragment;->access$3(Lcom/flixster/android/activity/hc/CategoryFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 96
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CategoryPage.ScheduleLoadItemsTask.run() mRetryCount:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    iget v6, v6, Lcom/flixster/android/activity/hc/CategoryFragment;->mRetryCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->printStackTrace()V

    .line 98
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    iget v5, v2, Lcom/flixster/android/activity/hc/CategoryFragment;->mRetryCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, Lcom/flixster/android/activity/hc/CategoryFragment;->mRetryCount:I

    .line 99
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    iget v2, v2, Lcom/flixster/android/activity/hc/CategoryFragment;->mRetryCount:I

    const/4 v5, 0x3

    if-ge v2, v5, :cond_5

    .line 100
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    const-wide/16 v3, 0x3e8

    #calls: Lcom/flixster/android/activity/hc/CategoryFragment;->ScheduleLoadItemsTask(J)V
    invoke-static {v2, v3, v4}, Lcom/flixster/android/activity/hc/CategoryFragment;->access$4(Lcom/flixster/android/activity/hc/CategoryFragment;J)V

    goto :goto_0

    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_4
    move v2, v4

    .line 80
    goto :goto_1

    .line 102
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    :cond_5
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment$2;->this$0:Lcom/flixster/android/activity/hc/CategoryFragment;

    iput v4, v2, Lcom/flixster/android/activity/hc/CategoryFragment;->mRetryCount:I

    .line 103
    if-eqz v1, :cond_0

    .line 104
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 105
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method
