.class public Lcom/flixster/android/activity/hc/CategoryFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "CategoryFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final CATEGORY_FILTER:Ljava/lang/String; = "CATEGORY_FILTER"

.field public static final CATEGORY_TITLE:Ljava/lang/String; = "CATEGORY_TITLE"


# instance fields
.field private initialContentLoadingHandler:Landroid/os/Handler;

.field private final mCategory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryFilter:Ljava/lang/String;

.field private mCategoryTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mCategory:Ljava/util/ArrayList;

    .line 32
    const-string v0, "Flixster Top 100"

    iput-object v0, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mCategoryTitle:Ljava/lang/String;

    .line 33
    const-string v0, "&filter=flixster-top-100"

    iput-object v0, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mCategoryFilter:Ljava/lang/String;

    .line 121
    new-instance v0, Lcom/flixster/android/activity/hc/CategoryFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/CategoryFragment$1;-><init>(Lcom/flixster/android/activity/hc/CategoryFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    .line 26
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 3
    .parameter "delay"

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/CategoryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 70
    new-instance v0, Lcom/flixster/android/activity/hc/CategoryFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/CategoryFragment$2;-><init>(Lcom/flixster/android/activity/hc/CategoryFragment;)V

    .line 114
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    const-string v1, "FlxMain"

    const-string v2, "CategoryPage.ScheduleLoadItemsTask() loading item"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/CategoryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/CategoryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v1, v0, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :cond_0
    monitor-exit p0

    return-void

    .line 69
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/CategoryFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mCategory:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/CategoryFragment;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mCategoryFilter:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/CategoryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/CategoryFragment;->setMovieLviList()V

    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/CategoryFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 121
    iget-object v0, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/CategoryFragment;J)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/hc/CategoryFragment;->ScheduleLoadItemsTask(J)V

    return-void
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 166
    const/16 v0, 0x8

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/CategoryFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 37
    new-instance v0, Lcom/flixster/android/activity/hc/CategoryFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/CategoryFragment;-><init>()V

    .line 38
    .local v0, f:Lcom/flixster/android/activity/hc/CategoryFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/CategoryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 39
    return-object v0
.end method

.method private setMovieLviList()V
    .locals 6

    .prologue
    .line 140
    const-string v4, "FlxMain"

    const-string v5, "CategoryPage.setPopularLviList "

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v4, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 145
    new-instance v2, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 146
    .local v2, lviSubHeader:Lnet/flixster/android/lvi/LviSubHeader;
    iget-object v4, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mCategoryTitle:Ljava/lang/String;

    iput-object v4, v2, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 147
    iget-object v4, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v4, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mCategory:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 161
    new-instance v0, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 162
    .local v0, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v4, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    return-void

    .line 154
    .end local v0           #footer:Lnet/flixster/android/lvi/LviFooter;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Movie;

    .line 155
    .local v3, movie:Lnet/flixster/android/model/Movie;
    new-instance v1, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 156
    .local v1, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v3, v1, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 157
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/CategoryFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v5

    iput-object v5, v1, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 158
    iget-object v5, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/LviFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 45
    .local v1, view:Landroid/view/View;
    iget-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/CategoryFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 50
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/CategoryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 51
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 52
    const-string v2, "CATEGORY_FILTER"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mCategoryFilter:Ljava/lang/String;

    .line 53
    const-string v2, "CATEGORY_TITLE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mCategoryTitle:Ljava/lang/String;

    .line 57
    :goto_0
    return-object v1

    .line 55
    :cond_0
    const-string v2, "FlxMain"

    const-string v3, "CategoryPage.onCreate Missing the bundle extras... "

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 64
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/CategoryFragment;->trackPage()V

    .line 65
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/CategoryFragment;->ScheduleLoadItemsTask(J)V

    .line 66
    return-void
.end method

.method public trackPage()V
    .locals 4

    .prologue
    .line 171
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/dvds/browse/genre"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Category "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/CategoryFragment;->mCategoryTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    return-void
.end method
