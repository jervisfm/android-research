.class public Lcom/flixster/android/activity/hc/CollectionPromoFragment;
.super Lcom/flixster/android/activity/hc/FlixsterFragment;
.source "CollectionPromoFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field protected final className:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;-><init>()V

    .line 19
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/hc/CollectionPromoFragment;->className:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 46
    const/16 v0, 0x82

    return v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 23
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/CollectionPromoFragment;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 29
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/flixster/android/activity/hc/CollectionPromoFragment;->className:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ".onCreateView"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    const v4, 0x7f03001d

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 31
    .local v2, rootView:Landroid/view/View;
    const v4, 0x7f07004f

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 32
    .local v0, promoHeader:Landroid/widget/TextView;
    const v4, 0x7f070051

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 34
    .local v1, promoText:Landroid/widget/TextView;
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v3

    .line 35
    .local v3, user:Lnet/flixster/android/model/User;
    if-nez v3, :cond_0

    .line 36
    const v4, 0x7f0c01a3

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 40
    :goto_0
    const v4, 0x7f0c01a5

    invoke-virtual {p0, v4}, Lcom/flixster/android/activity/hc/CollectionPromoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    return-object v2

    .line 38
    :cond_0
    const v4, 0x7f0c01a2

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public trackPage()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method
