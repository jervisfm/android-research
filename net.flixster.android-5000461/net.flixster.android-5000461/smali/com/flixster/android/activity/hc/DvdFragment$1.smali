.class Lcom/flixster/android/activity/hc/DvdFragment$1;
.super Landroid/os/Handler;
.source "DvdFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/DvdFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/DvdFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/DvdFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/DvdFragment$1;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    .line 194
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    const/4 v5, 0x0

    .line 197
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$1;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    .line 198
    .local v3, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$1;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->isRemoving()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    const-wide/16 v1, 0x0

    .line 203
    .local v1, id:J
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$1;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I
    invoke-static {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->access$0(Lcom/flixster/android/activity/hc/DvdFragment;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 218
    :cond_2
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_0

    .line 219
    new-instance v0, Landroid/content/Intent;

    const-string v4, "DETAILS"

    const/4 v5, 0x0

    const-class v6, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v0, v4, v5, v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 220
    .local v0, i:Landroid/content/Intent;
    const-string v4, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 221
    const-class v4, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v3, v0, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 205
    .end local v0           #i:Landroid/content/Intent;
    :pswitch_0
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$1;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleases:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->access$1(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 206
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$1;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleases:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->access$1(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v1

    .line 208
    goto :goto_1

    .line 210
    :pswitch_1
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$1;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoon:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->access$2(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 211
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$1;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoon:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->access$2(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v1

    .line 213
    goto :goto_1

    .line 203
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
