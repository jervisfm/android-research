.class Lcom/flixster/android/activity/hc/DvdFragment$2;
.super Ljava/lang/Object;
.source "DvdFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/DvdFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/DvdFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/DvdFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/DvdFragment$2;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    .line 394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .parameter
    .parameter "arg1"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 397
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment$2;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/DvdFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/lvi/Lvi;

    .line 398
    .local v1, lvi:Lnet/flixster/android/lvi/Lvi;
    invoke-virtual {v1}, Lnet/flixster/android/lvi/Lvi;->getItemViewType()I

    move-result v2

    sget v3, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_CATEGORY:I

    if-ne v2, v3, :cond_0

    .line 399
    new-instance v0, Landroid/content/Intent;

    const-string v2, "DETAILS"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$2;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/flixster/android/activity/hc/CategoryFragment;

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 400
    .local v0, i:Landroid/content/Intent;
    const-string v2, "TYPE"

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 401
    const-string v3, "CATEGORY_FILTER"

    move-object v2, v1

    check-cast v2, Lnet/flixster/android/lvi/LviCategory;

    iget-object v2, v2, Lnet/flixster/android/lvi/LviCategory;->mFilter:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 402
    const-string v2, "CATEGORY_TITLE"

    check-cast v1, Lnet/flixster/android/lvi/LviCategory;

    .end local v1           #lvi:Lnet/flixster/android/lvi/Lvi;
    iget-object v3, v1, Lnet/flixster/android/lvi/LviCategory;->mCategory:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 403
    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment$2;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/DvdFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    const-class v3, Lcom/flixster/android/activity/hc/CategoryFragment;

    invoke-virtual {v2, v0, v3}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    .line 405
    .end local v0           #i:Landroid/content/Intent;
    :cond_0
    return-void
.end method
