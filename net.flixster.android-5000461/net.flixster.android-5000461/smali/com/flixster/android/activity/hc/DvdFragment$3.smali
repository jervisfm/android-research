.class Lcom/flixster/android/activity/hc/DvdFragment$3;
.super Ljava/lang/Object;
.source "DvdFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/DvdFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/DvdFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/DvdFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 410
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 426
    :goto_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/DvdFragment;->access$0(Lcom/flixster/android/activity/hc/DvdFragment;)I

    move-result v1

    const-wide/16 v2, 0x64

    #calls: Lcom/flixster/android/activity/hc/DvdFragment;->ScheduleLoadItemsTask(IJ)V
    invoke-static {v0, v1, v2, v3}, Lcom/flixster/android/activity/hc/DvdFragment;->access$5(Lcom/flixster/android/activity/hc/DvdFragment;IJ)V

    .line 427
    return-void

    .line 412
    :pswitch_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    const v1, 0x7f070258

    #setter for: Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/DvdFragment;->access$3(Lcom/flixster/android/activity/hc/DvdFragment;I)V

    .line 413
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/DvdFragment;->trackPage()V

    .line 414
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/DvdFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/DvdFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    .line 417
    :pswitch_1
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    const v1, 0x7f070259

    #setter for: Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/DvdFragment;->access$3(Lcom/flixster/android/activity/hc/DvdFragment;I)V

    .line 418
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/DvdFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/DvdFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    .line 421
    :pswitch_2
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    const v1, 0x7f07025a

    #setter for: Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/DvdFragment;->access$3(Lcom/flixster/android/activity/hc/DvdFragment;I)V

    .line 422
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/DvdFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment$3;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mCategoryListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/DvdFragment;->access$4(Lcom/flixster/android/activity/hc/DvdFragment;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    .line 410
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
