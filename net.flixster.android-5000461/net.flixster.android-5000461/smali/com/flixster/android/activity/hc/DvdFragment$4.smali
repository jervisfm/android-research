.class Lcom/flixster/android/activity/hc/DvdFragment$4;
.super Ljava/util/TimerTask;
.source "DvdFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/DvdFragment;->ScheduleLoadItemsTask(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/DvdFragment;

.field private final synthetic val$navSelection:I


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/DvdFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    iput p2, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->val$navSelection:I

    .line 140
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 143
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 144
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->isRemoving()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DvdPage.ScheduleLoadItemsTask.run navSelection:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->val$navSelection:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :try_start_0
    iget v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->val$navSelection:I

    packed-switch v4, :pswitch_data_0

    .line 168
    :goto_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/DvdFragment;->mUpdateHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 170
    if-eqz v1, :cond_2

    .line 171
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 173
    :cond_2
    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    iget-boolean v2, v2, Lcom/flixster/android/activity/hc/DvdFragment;->isInitialContentLoaded:Z

    if-nez v2, :cond_3

    .line 174
    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/flixster/android/activity/hc/DvdFragment;->isInitialContentLoaded:Z

    .line 175
    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->initialContentLoadingHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/DvdFragment;->access$11(Lcom/flixster/android/activity/hc/DvdFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :cond_3
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 182
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 152
    :pswitch_0
    :try_start_1
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleases:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->access$1(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 153
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleasesFeatured:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->access$6(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleases:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/DvdFragment;->access$1(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    iget v6, v6, Lcom/flixster/android/activity/hc/DvdFragment;->mRetryCount:I

    if-nez v6, :cond_4

    move v2, v3

    :cond_4
    invoke-static {v4, v5, v2}, Lnet/flixster/android/data/MovieDao;->fetchDvdNewRelease(Ljava/util/List;Ljava/util/List;Z)V

    .line 155
    :cond_5
    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #calls: Lcom/flixster/android/activity/hc/DvdFragment;->setNewReleasesLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/DvdFragment;->access$7(Lcom/flixster/android/activity/hc/DvdFragment;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v2, "FlxMain"

    const-string v4, "DvdPage.ScheduleLoadItemsTask.run DaoException"

    invoke-static {v2, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 179
    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    invoke-virtual {v2, v0}, Lcom/flixster/android/activity/hc/DvdFragment;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 181
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 182
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 158
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_1
    :try_start_3
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoon:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->access$2(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 159
    iget-object v4, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoonFeatured:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/DvdFragment;->access$8(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #getter for: Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoon:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/DvdFragment;->access$2(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    iget v6, v6, Lcom/flixster/android/activity/hc/DvdFragment;->mRetryCount:I

    if-nez v6, :cond_6

    move v2, v3

    :cond_6
    invoke-static {v4, v5, v2}, Lnet/flixster/android/data/MovieDao;->fetchDvdComingSoon(Ljava/util/List;Ljava/util/List;Z)V

    .line 161
    :cond_7
    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #calls: Lcom/flixster/android/activity/hc/DvdFragment;->setComingSoonLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/DvdFragment;->access$9(Lcom/flixster/android/activity/hc/DvdFragment;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 180
    :catchall_0
    move-exception v2

    .line 181
    if-eqz v1, :cond_8

    iget-object v4, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v4, :cond_8

    iget-object v4, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 182
    iget-object v4, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 184
    :cond_8
    throw v2

    .line 164
    :pswitch_2
    :try_start_4
    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment$4;->this$0:Lcom/flixster/android/activity/hc/DvdFragment;

    #calls: Lcom/flixster/android/activity/hc/DvdFragment;->setBrowseLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/DvdFragment;->access$10(Lcom/flixster/android/activity/hc/DvdFragment;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_1

    .line 150
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
