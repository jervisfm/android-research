.class public Lcom/flixster/android/activity/hc/DvdFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "DvdFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private initialContentLoadingHandler:Landroid/os/Handler;

.field private mCategoryListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private final mComingSoon:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mComingSoonFeatured:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private mNavListener:Landroid/view/View$OnClickListener;

.field private mNavSelect:I

.field private mNavbar:Lcom/flixster/android/view/SubNavBar;

.field private mNavbarHolder:Landroid/widget/LinearLayout;

.field private final mNewReleases:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mNewReleasesFeatured:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleases:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoon:Ljava/util/ArrayList;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleasesFeatured:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoonFeatured:Ljava/util/ArrayList;

    .line 194
    new-instance v0, Lcom/flixster/android/activity/hc/DvdFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/DvdFragment$1;-><init>(Lcom/flixster/android/activity/hc/DvdFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    .line 394
    new-instance v0, Lcom/flixster/android/activity/hc/DvdFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/DvdFragment$2;-><init>(Lcom/flixster/android/activity/hc/DvdFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mCategoryListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 408
    new-instance v0, Lcom/flixster/android/activity/hc/DvdFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/DvdFragment$3;-><init>(Lcom/flixster/android/activity/hc/DvdFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavListener:Landroid/view/View$OnClickListener;

    .line 39
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(IJ)V
    .locals 4
    .parameter "navSelection"
    .parameter "delay"

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 140
    new-instance v0, Lcom/flixster/android/activity/hc/DvdFragment$4;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/activity/hc/DvdFragment$4;-><init>(Lcom/flixster/android/activity/hc/DvdFragment;I)V

    .line 188
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 189
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 190
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v0, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    :cond_0
    monitor-exit p0

    return-void

    .line 138
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    .end local v1           #main:Lcom/flixster/android/activity/hc/Main;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/DvdFragment;)I
    .locals 1
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I

    return v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleases:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$10(Lcom/flixster/android/activity/hc/DvdFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 343
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->setBrowseLviList()V

    return-void
.end method

.method static synthetic access$11(Lcom/flixster/android/activity/hc/DvdFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 194
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoon:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/DvdFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/DvdFragment;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .parameter

    .prologue
    .line 394
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mCategoryListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/DvdFragment;IJ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 137
    invoke-direct {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/DvdFragment;->ScheduleLoadItemsTask(IJ)V

    return-void
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleasesFeatured:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/DvdFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->setNewReleasesLviList()V

    return-void
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/DvdFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoonFeatured:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$9(Lcom/flixster/android/activity/hc/DvdFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 289
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->setComingSoonLviList()V

    return-void
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 431
    const/4 v0, 0x4

    return v0
.end method

.method private setBrowseLviList()V
    .locals 7

    .prologue
    .line 347
    iget-object v5, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 359
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0008

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 360
    .local v0, categories:[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0009

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 361
    .local v4, tags:[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_0

    .line 368
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 369
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v5, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    return-void

    .line 362
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    :cond_0
    new-instance v3, Lnet/flixster/android/lvi/LviCategory;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviCategory;-><init>()V

    .line 363
    .local v3, lviCategory:Lnet/flixster/android/lvi/LviCategory;
    aget-object v5, v0, v2

    iput-object v5, v3, Lnet/flixster/android/lvi/LviCategory;->mCategory:Ljava/lang/String;

    .line 364
    aget-object v5, v4, v2

    iput-object v5, v3, Lnet/flixster/android/lvi/LviCategory;->mFilter:Ljava/lang/String;

    .line 365
    iget-object v5, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private setComingSoonLviList()V
    .locals 11

    .prologue
    .line 292
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 294
    .local v0, date:Ljava/util/Date;
    iget-object v9, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 307
    iget-object v9, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoonFeatured:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    .line 308
    .local v4, mComingSoonFeaturedCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 309
    new-instance v8, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 310
    .local v8, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c00be

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 311
    iget-object v9, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 322
    .end local v8           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_0
    const-string v9, "FlxMain"

    const-string v10, "DvdPage.setUpcomingLviLlist"

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v9, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoon:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 324
    .local v3, mComingSoonCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 339
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 340
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v9, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    return-void

    .line 312
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    .end local v3           #mComingSoonCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .restart local v8       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Movie;

    .line 313
    .local v5, movie:Lnet/flixster/android/model/Movie;
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 314
    .local v2, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v5, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 315
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v2, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 316
    iget-object v10, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 324
    .end local v2           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v5           #movie:Lnet/flixster/android/model/Movie;
    .end local v8           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    .restart local v3       #mComingSoonCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Movie;

    .line 325
    .restart local v5       #movie:Lnet/flixster/android/model/Movie;
    const-string v10, "dvdReleaseDate"

    invoke-virtual {v5, v10}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 326
    .local v7, releaseDateString:Ljava/lang/String;
    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getDvdReleaseDate()Ljava/util/Date;

    move-result-object v6

    .line 327
    .local v6, releaseDate:Ljava/util/Date;
    if-eqz v6, :cond_3

    invoke-virtual {v0, v6}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 328
    new-instance v8, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 329
    .restart local v8       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    iput-object v7, v8, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 330
    iget-object v10, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    move-object v0, v6

    .line 333
    .end local v8           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_3
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 334
    .restart local v2       #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v5, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 335
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v2, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 336
    iget-object v10, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private setNewReleasesLviList()V
    .locals 13

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    check-cast v8, Lcom/flixster/android/activity/hc/Main;

    .line 233
    .local v8, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->isRemoving()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    iget-object v11, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 253
    iget-object v11, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleasesFeatured:Ljava/util/ArrayList;

    invoke-static {v11}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v7

    .line 254
    .local v7, mNewReleasesFeaturedCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 255
    new-instance v10, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v10}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 256
    .local v10, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c00be

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 257
    iget-object v11, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_3

    .line 266
    .end local v10           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_2
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 267
    .local v0, date:Ljava/util/Date;
    const/4 v4, 0x1

    .line 268
    .local v4, isFirst:Z
    iget-object v11, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleases:Ljava/util/ArrayList;

    invoke-static {v11}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v6

    .line 269
    .local v6, mNewReleasesCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_4

    .line 285
    new-instance v3, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 286
    .local v3, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v11, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    .end local v0           #date:Ljava/util/Date;
    .end local v3           #footer:Lnet/flixster/android/lvi/LviFooter;
    .end local v4           #isFirst:Z
    .end local v6           #mNewReleasesCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .restart local v10       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_3
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lnet/flixster/android/model/Movie;

    .line 259
    .local v9, movie:Lnet/flixster/android/model/Movie;
    new-instance v5, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 260
    .local v5, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v9, v5, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 261
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v12

    iput-object v12, v5, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 262
    iget-object v12, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 269
    .end local v5           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v9           #movie:Lnet/flixster/android/model/Movie;
    .end local v10           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    .restart local v0       #date:Ljava/util/Date;
    .restart local v4       #isFirst:Z
    .restart local v6       #mNewReleasesCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    :cond_4
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lnet/flixster/android/model/Movie;

    .line 270
    .restart local v9       #movie:Lnet/flixster/android/model/Movie;
    const-string v12, "dvdReleaseDate"

    invoke-virtual {v9, v12}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 271
    .local v2, dvdReleaseString:Ljava/lang/String;
    invoke-virtual {v9}, Lnet/flixster/android/model/Movie;->getDvdReleaseDate()Ljava/util/Date;

    move-result-object v1

    .line 272
    .local v1, dvdRelease:Ljava/util/Date;
    if-nez v4, :cond_5

    if-eqz v1, :cond_6

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 273
    :cond_5
    const/4 v4, 0x0

    .line 274
    new-instance v10, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v10}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 275
    .restart local v10       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    iput-object v2, v10, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 276
    iget-object v12, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    move-object v0, v1

    .line 279
    .end local v10           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_6
    new-instance v5, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 280
    .restart local v5       #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v9, v5, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 281
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v12

    iput-object v12, v5, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 282
    iget-object v12, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const v6, 0x7f070258

    .line 59
    invoke-super {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/LviFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 61
    .local v0, view:Landroid/view/View;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 62
    const v1, 0x7f0700f3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    .line 64
    new-instance v1, Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    .line 65
    iget-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavListener:Landroid/view/View$OnClickListener;

    const v3, 0x7f0c002e

    const v4, 0x7f0c002d

    const v5, 0x7f0c0029

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;III)V

    .line 66
    iget-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {v1, v6}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 67
    iget-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 72
    iput v6, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I

    .line 75
    return-object v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onPause()V

    .line 102
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleases:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 103
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoon:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 104
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNewReleasesFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 105
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mComingSoonFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 106
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 80
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 87
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->trackPage()V

    .line 90
    iget v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I

    const v1, 0x7f07025a

    if-ne v0, v1, :cond_0

    .line 91
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mCategoryListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 95
    :goto_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    iget v1, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 96
    iget v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I

    const-wide/16 v1, 0x64

    invoke-direct {p0, v0, v1, v2}, Lcom/flixster/android/activity/hc/DvdFragment;->ScheduleLoadItemsTask(IJ)V

    .line 97
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/DvdFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/LviFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 111
    const-string v0, "FlxMain"

    const-string v1, "DvdPage.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v0, "LISTSTATE_NAV"

    iget v1, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 113
    return-void
.end method

.method protected retryAction()V
    .locals 3

    .prologue
    .line 228
    iget v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I

    const-wide/16 v1, 0x3e8

    invoke-direct {p0, v0, v1, v2}, Lcom/flixster/android/activity/hc/DvdFragment;->ScheduleLoadItemsTask(IJ)V

    .line 229
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 124
    iget v0, p0, Lcom/flixster/android/activity/hc/DvdFragment;->mNavSelect:I

    packed-switch v0, :pswitch_data_0

    .line 135
    :goto_0
    return-void

    .line 126
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/dvds/new-releases"

    const-string v2, "Dvds - New Releases"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/dvds/coming-soon"

    const-string v2, "Dvds - Coming Soon"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :pswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/dvds/browse"

    const-string v2, "Dvds - Browse"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
