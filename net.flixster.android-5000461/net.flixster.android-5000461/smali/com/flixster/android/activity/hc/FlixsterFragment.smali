.class public abstract Lcom/flixster/android/activity/hc/FlixsterFragment;
.super Landroid/app/Fragment;
.source "FlixsterFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field protected static final ACTIVITY_RESULT_EMAIL:I = 0x0

.field protected static final ACTIVITY_RESULT_INFO:I = 0x1

.field public static final BUNDLE:Ljava/lang/String; = "BUNDLE"

.field public static final DIALOGKEY_DATESELECT:I = 0x6

.field public static final DIALOGKEY_LOADING:I = 0x1

.field public static final DIALOGKEY_NETWORKERROR:I = 0x2

.field public static final FRAGMENTID_ACTOR:I = 0x67

.field public static final FRAGMENTID_ACTORBIO:I = 0x68

.field public static final FRAGMENTID_BOXOFFICELIST:I = 0x1

.field public static final FRAGMENTID_COLLECTION_PROMO:I = 0x82

.field public static final FRAGMENTID_DVDLIST:I = 0x4

.field public static final FRAGMENTID_DVD_CATEGORY_LIST:I = 0x8

.field public static final FRAGMENTID_FLIXSTER:I = 0x0

.field public static final FRAGMENTID_MOVIEDETAILS:I = 0x6c

.field public static final FRAGMENTID_MYMOVIECOLLECTION:I = 0xa

.field public static final FRAGMENTID_MYMOVIESLIST:I = 0x5

.field public static final FRAGMENTID_NETFLIXAUTH:I = 0x6d

.field public static final FRAGMENTID_NETFLIXQUEUE:I = 0x6

.field public static final FRAGMENTID_REVIEW:I = 0x6a

.field public static final FRAGMENTID_SEARCH:I = 0x7

.field public static final FRAGMENTID_SHOWTIMES:I = 0x65

.field public static final FRAGMENTID_THEATERINFO:I = 0x66

.field public static final FRAGMENTID_THEATERLIST:I = 0x2

.field public static final FRAGMENTID_TICKETINFO:I = 0x69

.field public static final FRAGMENTID_TOP_ACTORS:I = 0x9

.field public static final FRAGMENTID_TOP_NEWS:I = 0x7a

.field public static final FRAGMENTID_TOP_PHOTOS:I = 0x78

.field public static final FRAGMENTID_UPCOMINGLIST:I = 0x3

.field public static final FRAGMENTID_USERREVIEW:I = 0x6b

.field public static final INITIAL_CONTENT_LOADED:Ljava/lang/String; = "INITIAL_CONTENT_LOADED"

.field protected static final KEY_PLATFORM_ID:Ljava/lang/String; = "PLATFORM_ID"

.field public static final LISTSTATE_POSITION:Ljava/lang/String; = "LISTSTATE_POSITION"

.field protected static final SWIPE_MAX_OFF_PATH:I = 0xfa

.field protected static final SWIPE_MIN_DISTANCE:I = 0x78

.field protected static final SWIPE_THRESHOLD_VELOCITY:I = 0xc8


# instance fields
.field protected isInitialContentLoaded:Z

.field protected mListAd:Lnet/flixster/android/ads/AdView;

.field protected mRetryCount:I

.field protected mStickyBottomAd:Lnet/flixster/android/ads/AdView;

.field protected mStickyTopAd:Lnet/flixster/android/ads/AdView;

.field private stackTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->mRetryCount:I

    .line 27
    return-void
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected NetworkAttemptsCancelled()V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method protected getStackTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->stackTag:Ljava/lang/String;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 107
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onCreate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-boolean v0, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->isInitialContentLoaded:Z

    if-nez v0, :cond_1

    .line 109
    if-eqz p1, :cond_0

    const-string v0, "INITIAL_CONTENT_LOADED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 108
    :goto_0
    iput-boolean v0, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->isInitialContentLoaded:Z

    .line 110
    return-void

    .line 109
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 157
    packed-switch p1, :pswitch_data_0

    .line 203
    :goto_0
    :pswitch_0
    return-object v0

    .line 160
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->shouldShowSplash()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->splashShown()V

    .line 162
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 163
    const v2, 0x3b9ac9ff

    .line 162
    invoke-static {v1, v2}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v1

    iput-object v1, v0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    .line 164
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 167
    :cond_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    .line 168
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 169
    const v2, 0x7f0c0135

    .line 168
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 170
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 171
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 172
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 173
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    .line 176
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 177
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c012f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 178
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/util/DialogUtils;->getShowtimesDateOptions(Landroid/content/Context;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 179
    new-instance v2, Lcom/flixster/android/activity/hc/FlixsterFragment$1;

    invoke-direct {v2, p0}, Lcom/flixster/android/activity/hc/FlixsterFragment$1;-><init>(Lcom/flixster/android/activity/hc/FlixsterFragment;)V

    .line 178
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 192
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 194
    :pswitch_3
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 195
    const-string v2, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 196
    const-string v2, "Network Error"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 197
    const-string v2, "Retry"

    new-instance v3, Lcom/flixster/android/activity/hc/FlixsterFragment$2;

    invoke-direct {v3, p0}, Lcom/flixster/android/activity/hc/FlixsterFragment$2;-><init>(Lcom/flixster/android/activity/hc/FlixsterFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 201
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onHiddenChanged(Z)V
    .locals 0
    .parameter "hidden"

    .prologue
    .line 97
    if-nez p1, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->trackPage()V

    .line 100
    :cond_0
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0}, Landroid/app/Fragment;->onLowMemory()V

    .line 91
    const-string v0, "FlxMain"

    const-string v1, "Low memory!"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->clearBitmapCache()V

    .line 93
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .parameter "outState"

    .prologue
    .line 114
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 115
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onSaveInstanceState initialContentLoaded="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 116
    iget-boolean v2, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->isInitialContentLoaded:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v0, "INITIAL_CONTENT_LOADED"

    iget-boolean v1, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->isInitialContentLoaded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 118
    return-void
.end method

.method protected retryAction()V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method protected retryLogic(Lnet/flixster/android/data/DaoException;)V
    .locals 3
    .parameter "de"

    .prologue
    const/4 v2, 0x0

    .line 127
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    iput v2, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->mRetryCount:I

    .line 129
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    invoke-static {p1, v0}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedActivity;)V

    .line 137
    :goto_0
    return-void

    .line 130
    :cond_0
    iget v0, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->mRetryCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 131
    iget v0, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->mRetryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->mRetryCount:I

    .line 132
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->retryAction()V

    goto :goto_0

    .line 134
    :cond_1
    iput v2, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->mRetryCount:I

    .line 135
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    invoke-static {p1, v0}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedActivity;)V

    goto :goto_0
.end method

.method protected setStackTag(Ljava/lang/String;)V
    .locals 0
    .parameter "tag"

    .prologue
    .line 251
    iput-object p1, p0, Lcom/flixster/android/activity/hc/FlixsterFragment;->stackTag:Ljava/lang/String;

    .line 252
    return-void
.end method

.method public abstract trackPage()V
.end method
