.class public Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
.super Ljava/lang/Object;
.source "FlixsterFragmentStack.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final TAG_PREFIX:Ljava/lang/String; = "FFS"


# instance fields
.field private final fm:Landroid/app/FragmentManager;

.field private final stack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final uid:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/app/FragmentManager;[Ljava/lang/String;)V
    .locals 4
    .parameter "fm"
    .parameter "persistedTags"

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->fm:Landroid/app/FragmentManager;

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->uid:Ljava/lang/String;

    .line 26
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    .line 27
    if-eqz p2, :cond_0

    .line 28
    invoke-static {p2}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->logTags([Ljava/lang/String;)V

    .line 29
    array-length v2, p2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_1

    .line 33
    :cond_0
    return-void

    .line 29
    :cond_1
    aget-object v0, p2, v1

    .line 30
    .local v0, tag:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v3, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private generateStackTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FFS"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static logTags([Ljava/lang/String;)V
    .locals 6
    .parameter "tags"

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    .local v0, sb:Ljava/lang/StringBuilder;
    array-length v3, p0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 105
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FFS "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    return-void

    .line 103
    :cond_0
    aget-object v1, p0, v2

    .line 104
    .local v1, t:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized elementAt(I)Lcom/flixster/android/activity/hc/FlixsterFragment;
    .locals 5
    .parameter "i"

    .prologue
    .line 67
    monitor-enter p0

    const/4 v1, 0x0

    .line 68
    .local v1, f:Lcom/flixster/android/activity/hc/FlixsterFragment;
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->empty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 69
    iget-object v3, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v3, p1}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 70
    .local v2, tag:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->fm:Landroid/app/FragmentManager;

    invoke-virtual {v3, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-object v1, v0

    .line 71
    if-nez v1, :cond_0

    .line 72
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "FlixsterFragmentStack out of sync with FragmentManager"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    .end local v2           #tag:Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 75
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized peek()Lcom/flixster/android/activity/hc/FlixsterFragment;
    .locals 5

    .prologue
    .line 43
    monitor-enter p0

    const/4 v1, 0x0

    .line 44
    .local v1, f:Lcom/flixster/android/activity/hc/FlixsterFragment;
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->empty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 45
    iget-object v3, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 46
    .local v2, tag:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->fm:Landroid/app/FragmentManager;

    invoke-virtual {v3, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-object v1, v0

    .line 47
    if-nez v1, :cond_0

    .line 48
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "FlixsterFragmentStack out of sync with FragmentManager"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    .end local v2           #tag:Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 51
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method protected declared-synchronized persist()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    .line 37
    .local v0, fragmentTags:[Ljava/lang/String;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 38
    invoke-static {v0}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->logTags([Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    monitor-exit p0

    return-object v0

    .line 36
    .end local v0           #fragmentTags:[Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized pop()Lcom/flixster/android/activity/hc/FlixsterFragment;
    .locals 5

    .prologue
    .line 55
    monitor-enter p0

    const/4 v1, 0x0

    .line 56
    .local v1, f:Lcom/flixster/android/activity/hc/FlixsterFragment;
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->empty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 57
    iget-object v3, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 58
    .local v2, tag:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->fm:Landroid/app/FragmentManager;

    invoke-virtual {v3, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-object v1, v0

    .line 59
    if-nez v1, :cond_0

    .line 60
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "FlixsterFragmentStack out of sync with FragmentManager"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    .end local v2           #tag:Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 63
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized push(Lcom/flixster/android/activity/hc/FlixsterFragment;)Ljava/lang/String;
    .locals 2
    .parameter "f"

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->generateStackTag()Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, tag:Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->setStackTag(Ljava/lang/String;)V

    .line 85
    iget-object v1, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    monitor-exit p0

    return-object v0

    .line 83
    .end local v0           #tag:Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    return v0
.end method
