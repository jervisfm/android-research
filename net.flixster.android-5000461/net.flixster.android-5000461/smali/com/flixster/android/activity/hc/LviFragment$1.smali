.class Lcom/flixster/android/activity/hc/LviFragment$1;
.super Landroid/os/Handler;
.source "LviFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/LviFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/LviFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/LviFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/LviFragment$1;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    .line 92
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 95
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment$1;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 96
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment$1;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/LviFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FlixsterLviActivity.mUpdateHandler mDataHolder.size():"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/LviFragment$1;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/LviFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment$1;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/LviFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 102
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment$1;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/LviFragment;->mData:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/LviFragment$1;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/LviFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 103
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment$1;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/LviFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 105
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment$1;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/LviFragment;->mLviListFragmentAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
