.class Lcom/flixster/android/activity/hc/LviFragment$2;
.super Ljava/lang/Object;
.source "LviFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/LviFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/LviFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/LviFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .parameter
    .parameter "v"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v6, v6, Lcom/flixster/android/activity/hc/LviFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/lvi/Lvi;

    .line 123
    .local v2, lviObject:Lnet/flixster/android/lvi/Lvi;
    invoke-virtual {v2}, Lnet/flixster/android/lvi/Lvi;->getItemViewType()I

    move-result v6

    sget v7, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_MOVIE:I

    if-ne v6, v7, :cond_4

    .line 124
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v6, v6, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedLvi:Lnet/flixster/android/lvi/Lvi;

    if-eqz v6, :cond_0

    .line 125
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v6, v6, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedLvi:Lnet/flixster/android/lvi/Lvi;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lnet/flixster/android/lvi/Lvi;->setSelected(Z)V

    .line 127
    :cond_0
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iput-object v2, v6, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedLvi:Lnet/flixster/android/lvi/Lvi;

    .line 128
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v6, v6, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedLvi:Lnet/flixster/android/lvi/Lvi;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lnet/flixster/android/lvi/Lvi;->setSelected(Z)V

    .line 129
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget v4, v6, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedMovieIndex:I

    .line 130
    .local v4, oldPosition:I
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iput p3, v6, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedMovieIndex:I

    move-object v6, v2

    .line 132
    check-cast v6, Lnet/flixster/android/lvi/LviMovie;

    iget-object v3, v6, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 133
    .local v3, movie:Lnet/flixster/android/model/Movie;
    check-cast v2, Lnet/flixster/android/lvi/LviMovie;

    .end local v2           #lviObject:Lnet/flixster/android/lvi/Lvi;
    iget-object v5, v2, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    .line 134
    .local v5, right:Lnet/flixster/android/model/LockerRight;
    if-eqz v3, :cond_5

    .line 135
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v7, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v7, v7, Lcom/flixster/android/activity/hc/LviFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v7

    iput v7, v6, Lcom/flixster/android/activity/hc/LviFragment;->mPositionLast:I

    .line 136
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    const/4 v7, 0x1

    iput-boolean v7, v6, Lcom/flixster/android/activity/hc/LviFragment;->mPositionRecover:Z

    .line 138
    new-instance v1, Landroid/content/Intent;

    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-class v7, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v1, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    .local v1, i:Landroid/content/Intent;
    const-string v6, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v7

    invoke-virtual {v1, v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 140
    instance-of v6, v3, Lnet/flixster/android/model/Season;

    if-eqz v6, :cond_1

    .line 141
    const-string v6, "KEY_IS_SEASON"

    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 143
    :cond_1
    if-eqz v5, :cond_2

    .line 144
    const-string v6, "KEY_RIGHT_ID"

    iget-wide v7, v5, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-virtual {v1, v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 146
    :cond_2
    if-ge p3, v4, :cond_3

    .line 147
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Lcom/flixster/android/activity/hc/Main;

    const-class v7, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 148
    const/4 v8, 0x3

    iget-object v9, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    .line 147
    invoke-virtual {v6, v1, v7, v8, v9}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    .line 150
    :cond_3
    if-le p3, v4, :cond_4

    .line 151
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Lcom/flixster/android/activity/hc/Main;

    const-class v7, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const/4 v8, 0x1

    .line 152
    iget-object v9, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    .line 151
    invoke-virtual {v6, v1, v7, v8, v9}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    .line 163
    .end local v1           #i:Landroid/content/Intent;
    .end local v3           #movie:Lnet/flixster/android/model/Movie;
    .end local v4           #oldPosition:I
    .end local v5           #right:Lnet/flixster/android/model/LockerRight;
    :cond_4
    :goto_0
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v6

    check-cast v6, Landroid/widget/BaseAdapter;

    invoke-virtual {v6}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 164
    return-void

    .line 155
    .restart local v3       #movie:Lnet/flixster/android/model/Movie;
    .restart local v4       #oldPosition:I
    .restart local v5       #right:Lnet/flixster/android/model/LockerRight;
    :cond_5
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_4

    .line 156
    new-instance v0, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    iget-object v7, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v7}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 157
    const v8, 0x7f0c0054

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 156
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v0, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 158
    .local v0, critic:Landroid/content/Intent;
    iget-object v6, p0, Lcom/flixster/android/activity/hc/LviFragment$2;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v6, v0}, Lcom/flixster/android/activity/hc/LviFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
