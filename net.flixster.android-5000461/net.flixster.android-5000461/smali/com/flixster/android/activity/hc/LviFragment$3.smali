.class Lcom/flixster/android/activity/hc/LviFragment$3;
.super Ljava/lang/Object;
.source "LviFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/LviFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/LviFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/LviFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/LviFragment$3;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 178
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment$3;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/LviFragment$3;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/LviFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    iput v2, v1, Lcom/flixster/android/activity/hc/LviFragment;->mPositionLast:I

    .line 179
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment$3;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/flixster/android/activity/hc/LviFragment;->mPositionRecover:Z

    .line 181
    const-string v1, "FlxMain"

    const-string v2, "BoxOfficePage - mTrailerClick.onClick"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .line 183
    .local v0, movie:Lnet/flixster/android/model/Movie;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->hasTrailer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment$3;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lnet/flixster/android/Starter;->launchTrailer(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V

    .line 186
    :cond_0
    return-void
.end method
