.class Lcom/flixster/android/activity/hc/LviFragment$4;
.super Ljava/lang/Object;
.source "LviFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/LviFragment;->getTheaterClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/LviFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/LviFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .parameter "view"

    .prologue
    .line 203
    const-string v8, "FlxMain"

    const-string v9, "LviFragment.getTheaterClickListener().onClick()"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;

    .line 205
    .local v5, theaterHolder:Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;
    if-eqz v5, :cond_2

    .line 207
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v8, v8, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedLvi:Lnet/flixster/android/lvi/Lvi;

    if-eqz v8, :cond_0

    .line 208
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v8, v8, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedLvi:Lnet/flixster/android/lvi/Lvi;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lnet/flixster/android/lvi/Lvi;->setSelected(Z)V

    .line 210
    :cond_0
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v9, v5, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->lviTheater:Lnet/flixster/android/lvi/LviTheater;

    iput-object v9, v8, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedLvi:Lnet/flixster/android/lvi/Lvi;

    .line 212
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v8, v8, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedLvi:Lnet/flixster/android/lvi/Lvi;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lnet/flixster/android/lvi/Lvi;->setSelected(Z)V

    .line 214
    iget-object v8, v5, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->theater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v8}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v6

    .line 215
    .local v6, theaterId:J
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-wide v3, v8, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedTheaterDistance:D

    .line 216
    .local v3, oldDistance:D
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v9, v5, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->theater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v9}, Lnet/flixster/android/model/Theater;->getMiles()D

    move-result-wide v9

    iput-wide v9, v8, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedTheaterDistance:D

    .line 217
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-boolean v2, v8, Lcom/flixster/android/activity/hc/LviFragment;->mIsSelectedTheaterFavorite:Z

    .line 218
    .local v2, isOldFavorite:Z
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v9

    .line 219
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    .line 218
    invoke-virtual {v9, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    iput-boolean v9, v8, Lcom/flixster/android/activity/hc/LviFragment;->mIsSelectedTheaterFavorite:Z

    .line 221
    new-instance v1, Landroid/content/Intent;

    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v8}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const-class v9, Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-direct {v1, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 222
    .local v1, i:Landroid/content/Intent;
    const-string v8, "net.flixster.android.EXTRA_THEATER_ID"

    invoke-virtual {v1, v8, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 224
    const/4 v0, 0x0

    .line 225
    .local v0, hint:I
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-boolean v8, v8, Lcom/flixster/android/activity/hc/LviFragment;->mIsSelectedTheaterFavorite:Z

    if-eqz v8, :cond_3

    if-nez v2, :cond_3

    .line 226
    const/4 v0, 0x3

    .line 235
    :cond_1
    :goto_0
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v8}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    check-cast v8, Lcom/flixster/android/activity/hc/Main;

    const-class v9, Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    .line 236
    iget-object v10, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    .line 235
    invoke-virtual {v8, v1, v9, v0, v10}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    .line 239
    .end local v0           #hint:I
    .end local v1           #i:Landroid/content/Intent;
    .end local v2           #isOldFavorite:Z
    .end local v3           #oldDistance:D
    .end local v6           #theaterId:J
    :cond_2
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-object v8, v8, Lcom/flixster/android/activity/hc/LviFragment;->mLviListFragmentAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v8}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 240
    return-void

    .line 227
    .restart local v0       #hint:I
    .restart local v1       #i:Landroid/content/Intent;
    .restart local v2       #isOldFavorite:Z
    .restart local v3       #oldDistance:D
    .restart local v6       #theaterId:J
    :cond_3
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-boolean v8, v8, Lcom/flixster/android/activity/hc/LviFragment;->mIsSelectedTheaterFavorite:Z

    if-nez v8, :cond_4

    if-eqz v2, :cond_4

    .line 228
    const/4 v0, 0x1

    goto :goto_0

    .line 229
    :cond_4
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-wide v8, v8, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedTheaterDistance:D

    cmpl-double v8, v8, v3

    if-lez v8, :cond_5

    .line 230
    const/4 v0, 0x1

    goto :goto_0

    .line 231
    :cond_5
    iget-object v8, p0, Lcom/flixster/android/activity/hc/LviFragment$4;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget-wide v8, v8, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedTheaterDistance:D

    cmpg-double v8, v8, v3

    if-gez v8, :cond_1

    .line 232
    const/4 v0, 0x3

    goto :goto_0
.end method
