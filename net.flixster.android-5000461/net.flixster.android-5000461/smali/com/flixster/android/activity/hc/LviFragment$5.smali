.class Lcom/flixster/android/activity/hc/LviFragment$5;
.super Ljava/lang/Object;
.source "LviFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/LviFragment;->getShowtimesClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/LviFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/LviFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/LviFragment$5;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 254
    const-string v2, "FlxMain"

    const-string v3, "FlixsterLviActivity.getShowtimesClickListener() pow!"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/lvi/LviShowtimes;

    .line 256
    .local v1, lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    if-eqz v1, :cond_0

    .line 258
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/LviFragment$5;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lnet/flixster/android/TicketInfoPage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 259
    .local v0, i:Landroid/content/Intent;
    const-string v2, "MOVIE_ID"

    iget-object v3, v1, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    iget-wide v3, v3, Lnet/flixster/android/model/Showtimes;->mMovieId:J

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 260
    const-string v2, "id"

    iget-object v3, v1, Lnet/flixster/android/lvi/LviShowtimes;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v3}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 261
    const-string v2, "SHOWTIMES_TITLE"

    iget-object v3, v1, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    iget-object v3, v3, Lnet/flixster/android/model/Showtimes;->mShowtimesTitle:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    iget-object v2, p0, Lcom/flixster/android/activity/hc/LviFragment$5;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    const-class v3, Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-virtual {v2, v0, v3}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    .line 265
    .end local v0           #i:Landroid/content/Intent;
    :cond_0
    return-void
.end method
