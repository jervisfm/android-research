.class Lcom/flixster/android/activity/hc/LviFragment$6;
.super Ljava/lang/Object;
.source "LviFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/LviFragment;->getStarTheaterClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/LviFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/LviFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/LviFragment$6;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    .line 278
    move-object v0, p1

    check-cast v0, Landroid/widget/CheckBox;

    .line 279
    .local v0, cb:Landroid/widget/CheckBox;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Theater;

    .line 280
    .local v1, t:Lnet/flixster/android/model/Theater;
    if-eqz v1, :cond_0

    .line 281
    invoke-virtual {v1}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 282
    .local v2, theaterIdString:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 283
    const-string v3, "FlxMain"

    const-string v4, "TheaterInfoList.StarTheaterListener() selected"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->addFavoriteTheater(Ljava/lang/String;)V

    .line 289
    :goto_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/LviFragment$6;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/Main;->peekListBackStack()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onResume()V

    .line 291
    .end local v2           #theaterIdString:Ljava/lang/String;
    :cond_0
    return-void

    .line 286
    .restart local v2       #theaterIdString:Ljava/lang/String;
    :cond_1
    const-string v3, "FlxMain"

    const-string v4, "TheaterInfoList.StarTheaterListener() UNselected"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->removeFavoriteTheater(Ljava/lang/String;)V

    goto :goto_0
.end method
