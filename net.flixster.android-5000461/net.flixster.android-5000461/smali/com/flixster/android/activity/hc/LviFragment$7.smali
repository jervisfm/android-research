.class Lcom/flixster/android/activity/hc/LviFragment$7;
.super Ljava/lang/Object;
.source "LviFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/LviFragment;->getActorClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/LviFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/LviFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/LviFragment$7;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "view"

    .prologue
    .line 305
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 306
    .local v0, actor:Lnet/flixster/android/model/Actor;
    if-eqz v0, :cond_1

    .line 307
    new-instance v1, Landroid/content/Intent;

    const-string v2, "TOP_ACTOR"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/LviFragment$7;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lnet/flixster/android/ActorPage;

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 308
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "ACTOR_ID"

    iget-wide v3, v0, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 309
    const-string v2, "ACTOR_NAME"

    iget-object v3, v0, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 310
    iget-object v2, p0, Lcom/flixster/android/activity/hc/LviFragment$7;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget v2, v2, Lcom/flixster/android/activity/hc/LviFragment;->mLastListPosition:I

    iget v3, v0, Lnet/flixster/android/model/Actor;->positionId:I

    if-ge v2, v3, :cond_2

    .line 311
    iget-object v2, p0, Lcom/flixster/android/activity/hc/LviFragment$7;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    const-class v3, Lcom/flixster/android/activity/hc/ActorFragment;

    const/4 v4, 0x1

    .line 312
    iget-object v5, p0, Lcom/flixster/android/activity/hc/LviFragment$7;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 311
    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    .line 317
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/flixster/android/activity/hc/LviFragment$7;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget v3, v0, Lnet/flixster/android/model/Actor;->positionId:I

    iput v3, v2, Lcom/flixster/android/activity/hc/LviFragment;->mLastListPosition:I

    .line 319
    .end local v1           #intent:Landroid/content/Intent;
    :cond_1
    return-void

    .line 313
    .restart local v1       #intent:Landroid/content/Intent;
    :cond_2
    iget-object v2, p0, Lcom/flixster/android/activity/hc/LviFragment$7;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    iget v2, v2, Lcom/flixster/android/activity/hc/LviFragment;->mLastListPosition:I

    iget v3, v0, Lnet/flixster/android/model/Actor;->positionId:I

    if-le v2, v3, :cond_0

    .line 314
    iget-object v2, p0, Lcom/flixster/android/activity/hc/LviFragment$7;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    const-class v3, Lcom/flixster/android/activity/hc/ActorFragment;

    const/4 v4, 0x3

    .line 315
    iget-object v5, p0, Lcom/flixster/android/activity/hc/LviFragment$7;->this$0:Lcom/flixster/android/activity/hc/LviFragment;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 314
    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    goto :goto_0
.end method
