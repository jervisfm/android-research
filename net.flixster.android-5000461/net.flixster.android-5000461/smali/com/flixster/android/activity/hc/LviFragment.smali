.class public abstract Lcom/flixster/android/activity/hc/LviFragment;
.super Lcom/flixster/android/activity/hc/FlixsterFragment;
.source "LviFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final LISTSTATE_NAV:Ljava/lang/String; = "LISTSTATE_NAV"


# instance fields
.field private actorClickListener:Landroid/view/View$OnClickListener;

.field protected mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/lvi/Lvi;",
            ">;"
        }
    .end annotation
.end field

.field protected mDataHolder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/lvi/Lvi;",
            ">;"
        }
    .end annotation
.end field

.field protected mIsSelectedTheaterFavorite:Z

.field protected mLastListPosition:I

.field protected mListView:Landroid/widget/ListView;

.field protected mLviListFragmentAdapter:Landroid/widget/BaseAdapter;

.field private mMovieItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field protected mPositionLast:I

.field protected mPositionRecover:Z

.field protected mSelectedLvi:Lnet/flixster/android/lvi/Lvi;

.field protected mSelectedMovieIndex:I

.field protected mSelectedTheaterDistance:D

.field private mShowtimesClickListener:Landroid/view/View$OnClickListener;

.field private mStarTheaterClickListener:Landroid/view/View$OnClickListener;

.field private mTheaterClickListener:Landroid/view/View$OnClickListener;

.field protected mTrailerClick:Landroid/view/View$OnClickListener;

.field protected mUpdateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;-><init>()V

    .line 45
    iput-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedLvi:Lnet/flixster/android/lvi/Lvi;

    .line 46
    iput v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mSelectedMovieIndex:I

    .line 52
    iput v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mPositionLast:I

    .line 53
    iput-boolean v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mPositionRecover:Z

    .line 92
    new-instance v0, Lcom/flixster/android/activity/hc/LviFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/LviFragment$1;-><init>(Lcom/flixster/android/activity/hc/LviFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mUpdateHandler:Landroid/os/Handler;

    .line 192
    iput-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    .line 247
    iput-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mShowtimesClickListener:Landroid/view/View$OnClickListener;

    .line 271
    iput-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mStarTheaterClickListener:Landroid/view/View$OnClickListener;

    .line 40
    return-void
.end method


# virtual methods
.method protected declared-synchronized getActorClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 301
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->actorClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 302
    new-instance v0, Lcom/flixster/android/activity/hc/LviFragment$7;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/LviFragment$7;-><init>(Lcom/flixster/android/activity/hc/LviFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->actorClickListener:Landroid/view/View$OnClickListener;

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->actorClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 301
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mMovieItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Lcom/flixster/android/activity/hc/LviFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/LviFragment$2;-><init>(Lcom/flixster/android/activity/hc/LviFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mMovieItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mMovieItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getShowtimesClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 250
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mShowtimesClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 251
    new-instance v0, Lcom/flixster/android/activity/hc/LviFragment$5;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/LviFragment$5;-><init>(Lcom/flixster/android/activity/hc/LviFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mShowtimesClickListener:Landroid/view/View$OnClickListener;

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mShowtimesClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getStarTheaterClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 274
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mStarTheaterClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 275
    new-instance v0, Lcom/flixster/android/activity/hc/LviFragment$6;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/LviFragment$6;-><init>(Lcom/flixster/android/activity/hc/LviFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mStarTheaterClickListener:Landroid/view/View$OnClickListener;

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mStarTheaterClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getTheaterClickListener()Landroid/view/View$OnClickListener;
    .locals 2

    .prologue
    .line 197
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 198
    const-string v0, "FlxMain"

    const-string v1, "LviFragment.getTheaterClickListener() fresh"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    new-instance v0, Lcom/flixster/android/activity/hc/LviFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/LviFragment$4;-><init>(Lcom/flixster/android/activity/hc/LviFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mTheaterClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getTrailerOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mTrailerClick:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 175
    new-instance v0, Lcom/flixster/android/activity/hc/LviFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/LviFragment$3;-><init>(Lcom/flixster/android/activity/hc/LviFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/LviFragment;->mTrailerClick:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 74
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 78
    const v1, 0x7f030045

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, view:Landroid/view/View;
    const v1, 0x7f0700f4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mListView:Landroid/widget/ListView;

    .line 80
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 81
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/LviFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 83
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mData:Ljava/util/ArrayList;

    .line 84
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mDataHolder:Ljava/util/ArrayList;

    .line 86
    new-instance v1, Lnet/flixster/android/lvi/LviAdapter;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/LviFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/LviFragment;->mData:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3}, Lnet/flixster/android/lvi/LviAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mLviListFragmentAdapter:Landroid/widget/BaseAdapter;

    .line 87
    iget-object v1, p0, Lcom/flixster/android/activity/hc/LviFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/LviFragment;->mLviListFragmentAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 89
    return-object v0
.end method
