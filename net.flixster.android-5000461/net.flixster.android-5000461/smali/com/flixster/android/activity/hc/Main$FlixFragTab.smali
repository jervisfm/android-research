.class final enum Lcom/flixster/android/activity/hc/Main$FlixFragTab;
.super Ljava/lang/Enum;
.source "Main.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FlixFragTab"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flixster/android/activity/hc/Main$FlixFragTab;",
        ">;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$flixster$android$activity$hc$Main$FlixFragTab:[I

.field public static final enum BOX_OFFICE:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

.field public static final enum DVD:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

.field private static final synthetic ENUM$VALUES:[Lcom/flixster/android/activity/hc/Main$FlixFragTab;

.field public static final enum MY_MOVIES:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

.field public static final enum SEARCH:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

.field public static final enum THEATERS:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

.field public static final enum UPCOMING:Lcom/flixster/android/activity/hc/Main$FlixFragTab;


# instance fields
.field private final iconId:I

.field private final text:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$flixster$android$activity$hc$Main$FlixFragTab()[I
    .locals 3

    .prologue
    .line 584
    sget-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->$SWITCH_TABLE$com$flixster$android$activity$hc$Main$FlixFragTab:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->values()[Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->BOX_OFFICE:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->DVD:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->MY_MOVIES:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->SEARCH:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->THEATERS:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->UPCOMING:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->$SWITCH_TABLE$com$flixster$android$activity$hc$Main$FlixFragTab:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 585
    new-instance v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    const-string v1, "BOX_OFFICE"

    const v2, 0x7f0200d8

    const v3, 0x7f0c010f

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->BOX_OFFICE:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    new-instance v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    const-string v1, "THEATERS"

    .line 586
    const v2, 0x7f0200f0

    const v3, 0x7f0c0110

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->THEATERS:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    new-instance v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    const-string v1, "UPCOMING"

    .line 587
    const v2, 0x7f0200f3

    const v3, 0x7f0c0111

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->UPCOMING:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    new-instance v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    const-string v1, "DVD"

    const v2, 0x7f0200db

    .line 588
    const v3, 0x7f0c0112

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->DVD:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    new-instance v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    const-string v1, "MY_MOVIES"

    const v2, 0x7f0200e5

    const v3, 0x7f0c0113

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->MY_MOVIES:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    new-instance v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    const-string v1, "SEARCH"

    const/4 v2, 0x5

    .line 589
    invoke-direct {v0, v1, v2, v4, v4}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->SEARCH:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    .line 584
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->BOX_OFFICE:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    aput-object v1, v0, v4

    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->THEATERS:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    aput-object v1, v0, v5

    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->UPCOMING:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    aput-object v1, v0, v6

    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->DVD:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    aput-object v1, v0, v7

    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->MY_MOVIES:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->SEARCH:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    aput-object v2, v0, v1

    sput-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ENUM$VALUES:[Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter "iconId"
    .parameter "textResrouceId"

    .prologue
    .line 594
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 595
    iput p3, p0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->iconId:I

    .line 599
    iput p4, p0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->text:I

    .line 600
    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/Main$FlixFragTab;)I
    .locals 1
    .parameter

    .prologue
    .line 592
    iget v0, p0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->text:I

    return v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/Main$FlixFragTab;)I
    .locals 1
    .parameter

    .prologue
    .line 591
    iget v0, p0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->iconId:I

    return v0
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/Main$FlixFragTab;)Ljava/lang/Class;
    .locals 1
    .parameter

    .prologue
    .line 603
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->getFragClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method private getFragClass()Ljava/lang/Class;
    .locals 3

    .prologue
    .line 604
    const/4 v0, 0x0

    .line 605
    .local v0, classRefernce:Ljava/lang/Class;
    invoke-static {}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->$SWITCH_TABLE$com$flixster$android$activity$hc$Main$FlixFragTab()[I

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 622
    :goto_0
    return-object v0

    .line 607
    :pswitch_0
    const-class v0, Lcom/flixster/android/activity/hc/BoxOfficeFragment;

    .line 608
    goto :goto_0

    .line 610
    :pswitch_1
    const-class v0, Lcom/flixster/android/activity/hc/TheaterListFragment;

    .line 611
    goto :goto_0

    .line 613
    :pswitch_2
    const-class v0, Lcom/flixster/android/activity/hc/UpcomingFragment;

    .line 614
    goto :goto_0

    .line 616
    :pswitch_3
    const-class v0, Lcom/flixster/android/activity/hc/DvdFragment;

    .line 617
    goto :goto_0

    .line 619
    :pswitch_4
    const-class v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;

    goto :goto_0

    .line 605
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flixster/android/activity/hc/Main$FlixFragTab;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    return-object v0
.end method

.method public static values()[Lcom/flixster/android/activity/hc/Main$FlixFragTab;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ENUM$VALUES:[Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    array-length v1, v0

    new-array v2, v1, [Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
