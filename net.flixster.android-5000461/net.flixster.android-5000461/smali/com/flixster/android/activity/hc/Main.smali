.class public Lcom/flixster/android/activity/hc/Main;
.super Lcom/flixster/android/activity/common/DecoratedActivity;
.source "Main.java"

# interfaces
.implements Lcom/flixster/android/utils/ImageTask;
.implements Landroid/app/ActionBar$TabListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/hc/Main$FlixFragTab;,
        Lcom/flixster/android/activity/hc/Main$PrivacyDialogListener;
    }
.end annotation


# static fields
.field public static final DIALOGKEY_ADSENSEHACK:I = 0x7

.field public static final DIALOGKEY_APIDOWN:I = 0x3

.field public static final DIALOGKEY_DATESELECT:I = 0x6

.field public static final DIALOGKEY_LOADING:I = 0x1

.field public static final DIALOGKEY_NETWORKERROR:I = 0x2

.field public static final DIALOGKEY_SORTSELECT:I = 0x5

.field public static final DIALOGKEY_UNKOWNERROR:I = 0x4

.field public static final FRAGMENT_TYPE_CONTENT:I = 0x2

.field public static final FRAGMENT_TYPE_LIST:I = 0x1

.field private static final KEY_CONTENT_FRAGMENT_STACK:Ljava/lang/String; = "KEY_CONTENT_FRAGMENT_STACK"

.field private static final KEY_LAST_TAB_INDEX:Ljava/lang/String; = "KEY_LAST_TAB_INDEX"

.field private static final KEY_LIST_FRAGMENT_STACK:Ljava/lang/String; = "KEY_LIST_FRAGMENT_STACK"

.field public static final KEY_MOVIE_ID:Ljava/lang/String; = "net.flixster.android.EXTRA_MOVIE_ID"

.field public static final KEY_THEATER_ID:Ljava/lang/String; = "net.flixster.android.EXTRA_THEATER_ID"

.field public static final PANEL_CHILD:I = 0x0

.field public static final PANEL_PARENT:I = 0x2

.field public static final PANEL_SIBLING:I = 0x1

.field public static final RATING_LARGE_R:[I = null

.field public static final RATING_SMALL_R:[I = null

.field public static final SLIDE_HINT_DOWN:I = 0x3

.field public static final SLIDE_HINT_LEFT:I = 0x4

.field public static final SLIDE_HINT_NONE:I = 0x0

.field public static final SLIDE_HINT_RIGHT:I = 0x2

.field public static final SLIDE_HINT_UP:I = 0x1


# instance fields
.field private mActionBarView:Landroid/view/View;

.field private mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

.field private mCurrTab:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

.field private mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

.field private mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

.field public mLoadingDialog:Landroid/app/Dialog;

.field protected mPageTimer:Ljava/util/Timer;

.field protected mRemoveDialogHandler:Landroid/os/Handler;

.field protected mShowDialogHandler:Landroid/os/Handler;

.field private mStartSettingsClickListener:Landroid/view/View$OnClickListener;

.field private skipRatePrompt:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xd

    .line 65
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/flixster/android/activity/hc/Main;->RATING_LARGE_R:[I

    .line 71
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/flixster/android/activity/hc/Main;->RATING_SMALL_R:[I

    .line 61
    return-void

    .line 65
    nop

    :array_0
    .array-data 0x4
        0x9dt 0x1t 0x2t 0x7ft
        0x9et 0x1t 0x2t 0x7ft
        0x9ft 0x1t 0x2t 0x7ft
        0xa0t 0x1t 0x2t 0x7ft
        0xa1t 0x1t 0x2t 0x7ft
        0xa2t 0x1t 0x2t 0x7ft
        0xa3t 0x1t 0x2t 0x7ft
        0xa4t 0x1t 0x2t 0x7ft
        0xa5t 0x1t 0x2t 0x7ft
        0xa6t 0x1t 0x2t 0x7ft
        0xa7t 0x1t 0x2t 0x7ft
        0x6dt 0x1t 0x2t 0x7ft
        0x69t 0x1t 0x2t 0x7ft
    .end array-data

    .line 71
    :array_1
    .array-data 0x4
        0xa8t 0x1t 0x2t 0x7ft
        0xa9t 0x1t 0x2t 0x7ft
        0xaat 0x1t 0x2t 0x7ft
        0xabt 0x1t 0x2t 0x7ft
        0xact 0x1t 0x2t 0x7ft
        0xadt 0x1t 0x2t 0x7ft
        0xaet 0x1t 0x2t 0x7ft
        0xaft 0x1t 0x2t 0x7ft
        0xb0t 0x1t 0x2t 0x7ft
        0xb1t 0x1t 0x2t 0x7ft
        0xb2t 0x1t 0x2t 0x7ft
        0x6at 0x1t 0x2t 0x7ft
        0x66t 0x1t 0x2t 0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;-><init>()V

    .line 340
    new-instance v0, Lcom/flixster/android/activity/hc/Main$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/Main$1;-><init>(Lcom/flixster/android/activity/hc/Main;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    .line 351
    new-instance v0, Lcom/flixster/android/activity/hc/Main$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/Main$2;-><init>(Lcom/flixster/android/activity/hc/Main;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    .line 489
    iput-object v1, p0, Lcom/flixster/android/activity/hc/Main;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

    .line 510
    iput-object v1, p0, Lcom/flixster/android/activity/hc/Main;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;

    .line 61
    return-void
.end method

.method private createActionBar(I)V
    .locals 10
    .parameter "selectedIndex"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 627
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 628
    .local v0, bar:Landroid/app/ActionBar;
    invoke-static {}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->values()[Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    move-result-object v7

    array-length v8, v7

    move v6, v5

    :goto_0
    if-lt v6, v8, :cond_0

    .line 636
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f030014

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/flixster/android/activity/hc/Main;->mActionBarView:Landroid/view/View;

    .line 637
    iget-object v3, p0, Lcom/flixster/android/activity/hc/Main;->mActionBarView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 638
    const/16 v3, 0x11

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 639
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 640
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 641
    return-void

    .line 628
    :cond_0
    aget-object v1, v7, v6

    .line 629
    .local v1, fragTab:Lcom/flixster/android/activity/hc/Main$FlixFragTab;
    #getter for: Lcom/flixster/android/activity/hc/Main$FlixFragTab;->text:I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->access$3(Lcom/flixster/android/activity/hc/Main$FlixFragTab;)I

    move-result v3

    if-eqz v3, :cond_1

    #getter for: Lcom/flixster/android/activity/hc/Main$FlixFragTab;->iconId:I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->access$4(Lcom/flixster/android/activity/hc/Main$FlixFragTab;)I

    move-result v3

    if-eqz v3, :cond_1

    .line 630
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v3

    #getter for: Lcom/flixster/android/activity/hc/Main$FlixFragTab;->text:I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->access$3(Lcom/flixster/android/activity/hc/Main$FlixFragTab;)I

    move-result v9

    invoke-virtual {v3, v9}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    .line 631
    .local v2, tab:Landroid/app/ActionBar$Tab;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    #getter for: Lcom/flixster/android/activity/hc/Main$FlixFragTab;->iconId:I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->access$4(Lcom/flixster/android/activity/hc/Main$FlixFragTab;)I

    move-result v9

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/ActionBar$Tab;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    .line 632
    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ordinal()I

    move-result v3

    if-ne v3, p1, :cond_2

    move v3, v4

    :goto_1
    invoke-virtual {v0, v2, v3}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 628
    .end local v2           #tab:Landroid/app/ActionBar$Tab;
    :cond_1
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_0

    .restart local v2       #tab:Landroid/app/ActionBar$Tab;
    :cond_2
    move v3, v5

    .line 632
    goto :goto_1
.end method

.method private static isContentFragment(Ljava/lang/Class;)Z
    .locals 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flixster/android/activity/hc/FlixsterFragment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p0, fc:Ljava/lang/Class;,"Ljava/lang/Class<+Lcom/flixster/android/activity/hc/FlixsterFragment;>;"
    const/4 v4, 0x0

    .line 858
    :try_start_0
    const-string v5, "getFlixFragId"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 859
    .local v3, staticGetId:Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v3, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 860
    .local v2, o:Ljava/lang/Object;
    check-cast v2, Ljava/lang/Integer;

    .end local v2           #o:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v1

    .line 861
    .local v1, fragType:I
    const/16 v5, 0x64

    if-le v1, v5, :cond_0

    const/4 v4, 0x1

    .line 871
    .end local v1           #fragType:I
    .end local v3           #staticGetId:Ljava/lang/reflect/Method;
    :cond_0
    :goto_0
    return v4

    .line 862
    :catch_0
    move-exception v0

    .line 863
    .local v0, e:Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 864
    .end local v0           #e:Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 865
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 866
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 867
    .local v0, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 868
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 869
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private removeFragment(Lcom/flixster/android/activity/hc/FlixsterFragmentStack;II)V
    .locals 5
    .parameter "stack"
    .parameter "enterAnim"
    .parameter "exitAnim"

    .prologue
    .line 875
    invoke-virtual {p1}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 876
    invoke-virtual {p1}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->pop()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v0

    .line 877
    .local v0, exitFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    invoke-virtual {p1}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->peek()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v2

    .line 878
    .local v2, restoreFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 879
    .local v1, ft:Landroid/app/FragmentTransaction;
    invoke-virtual {v1, p2, p3}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    .line 880
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 881
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 882
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 884
    .end local v0           #exitFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .end local v1           #ft:Landroid/app/FragmentTransaction;
    .end local v2           #restoreFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    :cond_0
    return-void
.end method

.method private resetContentBackStack(Landroid/app/FragmentTransaction;)V
    .locals 3
    .parameter "ft"

    .prologue
    .line 911
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Main.resetContentBackStack count="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    :goto_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 916
    return-void

    .line 913
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->pop()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/flixster/android/activity/hc/Main;->tryRemove(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    goto :goto_0
.end method

.method private resetListBackStack(Landroid/app/FragmentTransaction;)V
    .locals 3
    .parameter "ft"

    .prologue
    .line 919
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Main.resetListBackStack count="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 920
    :goto_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 924
    return-void

    .line 921
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->pop()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/flixster/android/activity/hc/Main;->tryRemove(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    goto :goto_0
.end method

.method private showDiagnosticsOrRateDialog()V
    .locals 4

    .prologue
    .line 314
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->hasUserSessionExpired()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 315
    const v2, 0x3b9acd88

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/flixster/android/activity/hc/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    invoke-static {}, Lnet/flixster/android/util/ErrorHandler;->instance()Lnet/flixster/android/util/ErrorHandler;

    move-result-object v2

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lnet/flixster/android/util/ErrorHandler;->checkForAppCrash(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .local v0, intent:Landroid/content/Intent;
    if-eqz v0, :cond_2

    .line 317
    move-object v1, v0

    .line 318
    .local v1, intentz:Landroid/content/Intent;
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/flixster/android/activity/hc/Main;->skipRatePrompt:Z

    .line 319
    const v2, 0x3b9acde8

    new-instance v3, Lcom/flixster/android/activity/hc/Main$3;

    invoke-direct {v3, p0, v1}, Lcom/flixster/android/activity/hc/Main$3;-><init>(Lcom/flixster/android/activity/hc/Main;Landroid/content/Intent;)V

    invoke-virtual {p0, v2, v3}, Lcom/flixster/android/activity/hc/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 333
    .end local v1           #intentz:Landroid/content/Intent;
    :cond_2
    iget-boolean v2, p0, Lcom/flixster/android/activity/hc/Main;->skipRatePrompt:Z

    if-nez v2, :cond_0

    .line 334
    invoke-static {p0}, Lcom/flixster/android/utils/AppRater;->showRateDialog(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private declared-synchronized startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;Landroid/app/FragmentTransaction;)V
    .locals 9
    .parameter "i"
    .parameter
    .parameter "slideHint"
    .parameter
    .parameter "ft"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flixster/android/activity/hc/FlixsterFragment;",
            ">;I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flixster/android/activity/hc/FlixsterFragment;",
            ">;",
            "Landroid/app/FragmentTransaction;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, fc:Ljava/lang/Class;,"Ljava/lang/Class<+Lcom/flixster/android/activity/hc/FlixsterFragment;>;"
    .local p4, source:Ljava/lang/Class;,"Ljava/lang/Class<+Lcom/flixster/android/activity/hc/FlixsterFragment;>;"
    const/4 v6, 0x1

    .line 752
    monitor-enter p0

    :try_start_0
    const-string v5, "FlxMain"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Main.startFragment "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    if-eqz p4, :cond_2

    invoke-static {p4}, Lcom/flixster/android/activity/hc/Main;->isContentFragment(Ljava/lang/Class;)Z

    move-result v5

    if-nez v5, :cond_2

    move v4, v6

    .line 755
    .local v4, relationship:I
    :goto_0
    invoke-static {p2}, Lcom/flixster/android/activity/hc/Main;->isContentFragment(Ljava/lang/Class;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_6

    .line 756
    const/4 v2, 0x0

    .local v2, newFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    const/4 v3, 0x0

    .line 758
    .local v3, oldFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-object v2, v0

    .line 759
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/flixster/android/activity/hc/FlixsterFragment;->setArguments(Landroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    .line 766
    :goto_1
    if-eqz v2, :cond_1

    .line 767
    :try_start_2
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v5}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 768
    if-ne v4, v6, :cond_3

    .line 769
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v5}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->pop()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v3

    .line 770
    invoke-direct {p0, p5}, Lcom/flixster/android/activity/hc/Main;->resetContentBackStack(Landroid/app/FragmentTransaction;)V

    .line 775
    :cond_0
    :goto_2
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v5, v2}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->push(Lcom/flixster/android/activity/hc/FlixsterFragment;)Ljava/lang/String;

    .line 776
    if-ne v4, v6, :cond_4

    .line 777
    packed-switch p3, :pswitch_data_0

    .line 785
    :pswitch_0
    const v5, 0x7f040002

    const v7, 0x7f040004

    invoke-virtual {p5, v5, v7}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    .line 792
    :goto_3
    const v5, 0x7f070084

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getStackTag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p5, v5, v2, v7}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 793
    invoke-virtual {p5, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 794
    if-eqz v3, :cond_1

    .line 795
    if-ne v4, v6, :cond_5

    .line 796
    invoke-static {p5, v3}, Lcom/flixster/android/activity/hc/Main;->tryRemove(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    .line 802
    :cond_1
    :goto_4
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Main.startFragment finished mContentFragmentStack.size="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v7}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 854
    :goto_5
    monitor-exit p0

    return-void

    .line 754
    .end local v2           #newFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .end local v3           #oldFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .end local v4           #relationship:I
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 760
    .restart local v2       #newFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .restart local v3       #oldFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .restart local v4       #relationship:I
    :catch_0
    move-exception v1

    .line 761
    .local v1, e:Ljava/lang/InstantiationException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 752
    .end local v1           #e:Ljava/lang/InstantiationException;
    .end local v2           #newFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .end local v3           #oldFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .end local v4           #relationship:I
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 762
    .restart local v2       #newFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .restart local v3       #oldFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .restart local v4       #relationship:I
    :catch_1
    move-exception v1

    .line 763
    .local v1, e:Ljava/lang/IllegalAccessException;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 772
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :cond_3
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v5}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->peek()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v3

    goto :goto_2

    .line 779
    :pswitch_1
    const v5, 0x7f04000f

    const v7, 0x7f040010

    invoke-virtual {p5, v5, v7}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    goto :goto_3

    .line 782
    :pswitch_2
    const/high16 v5, 0x7f04

    const v7, 0x7f040001

    invoke-virtual {p5, v5, v7}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    goto :goto_3

    .line 789
    :cond_4
    const v5, 0x7f040002

    const v7, 0x7f040004

    invoke-virtual {p5, v5, v7}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    goto :goto_3

    .line 798
    :cond_5
    invoke-static {p5, v3}, Lcom/flixster/android/activity/hc/Main;->tryHide(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    .line 805
    .end local v2           #newFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .end local v3           #oldFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    :cond_6
    const/4 v2, 0x0

    .restart local v2       #newFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    const/4 v3, 0x0

    .line 807
    .restart local v3       #oldFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    :try_start_5
    invoke-virtual {p2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-object v2, v0

    .line 808
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/flixster/android/activity/hc/FlixsterFragment;->setArguments(Landroid/os/Bundle;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
    .catch Ljava/lang/InstantiationException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_3

    .line 815
    :goto_6
    if-eqz v2, :cond_8

    .line 816
    :try_start_6
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v5}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    .line 817
    if-ne v4, v6, :cond_9

    .line 818
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v5}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->pop()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v3

    .line 819
    invoke-direct {p0, p5}, Lcom/flixster/android/activity/hc/Main;->resetListBackStack(Landroid/app/FragmentTransaction;)V

    .line 824
    :cond_7
    :goto_7
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v5, v2}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->push(Lcom/flixster/android/activity/hc/FlixsterFragment;)Ljava/lang/String;

    .line 825
    if-ne v4, v6, :cond_a

    .line 826
    packed-switch p3, :pswitch_data_1

    .line 834
    :pswitch_3
    const v5, 0x7f040003

    .line 835
    const v7, 0x7f040005

    .line 834
    invoke-virtual {p5, v5, v7}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    .line 842
    :goto_8
    const v5, 0x7f070083

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/FlixsterFragment;->getStackTag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p5, v5, v2, v7}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 843
    invoke-virtual {p5, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 844
    if-eqz v3, :cond_8

    .line 845
    if-ne v4, v6, :cond_b

    .line 846
    invoke-static {p5, v3}, Lcom/flixster/android/activity/hc/Main;->tryRemove(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    .line 852
    :cond_8
    :goto_9
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Main.startFragment finished mListFragmentStack.size="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v7}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 809
    :catch_2
    move-exception v1

    .line 810
    .local v1, e:Ljava/lang/InstantiationException;
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_6

    .line 811
    .end local v1           #e:Ljava/lang/InstantiationException;
    :catch_3
    move-exception v1

    .line 812
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_6

    .line 821
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :cond_9
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v5}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->peek()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v3

    goto :goto_7

    .line 828
    :pswitch_4
    const v5, 0x7f04000f

    const v7, 0x7f040010

    invoke-virtual {p5, v5, v7}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    goto :goto_8

    .line 831
    :pswitch_5
    const/high16 v5, 0x7f04

    const v7, 0x7f040001

    invoke-virtual {p5, v5, v7}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    goto :goto_8

    .line 839
    :cond_a
    const v5, 0x7f040003

    const v7, 0x7f040005

    invoke-virtual {p5, v5, v7}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    goto :goto_8

    .line 848
    :cond_b
    invoke-static {p5, v3}, Lcom/flixster/android/activity/hc/Main;->tryHide(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_9

    .line 777
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 826
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method private static tryHide(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V
    .locals 4
    .parameter "ft"
    .parameter "frag"

    .prologue
    .line 947
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 951
    :goto_0
    return-void

    .line 948
    :catch_0
    move-exception v0

    .line 949
    .local v0, ise:Ljava/lang/IllegalStateException;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Main.tryHide "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static tryRemove(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V
    .locals 4
    .parameter "ft"
    .parameter "frag"

    .prologue
    .line 939
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 943
    :goto_0
    return-void

    .line 940
    :catch_0
    move-exception v0

    .line 941
    .local v0, ise:Ljava/lang/IllegalStateException;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Main.tryRemove "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected declared-synchronized getDateSelectOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 493
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 494
    new-instance v0, Lcom/flixster/android/activity/hc/Main$6;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/Main$6;-><init>(Lcom/flixster/android/activity/hc/Main;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 493
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getPhotoInfoUri(Lnet/flixster/android/model/Photo;)Landroid/net/Uri;
    .locals 3
    .parameter "photo"

    .prologue
    .line 561
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://flixster.com/mobile/support/flag-photo?image="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p1, Lnet/flixster/android/model/Photo;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected getResourceString(I)Ljava/lang/String;
    .locals 1
    .parameter "resourceId"

    .prologue
    .line 565
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getStartSettingsClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 513
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 514
    new-instance v0, Lcom/flixster/android/activity/hc/Main$7;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/Main$7;-><init>(Lcom/flixster/android/activity/hc/Main;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 513
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, 0x0

    .line 120
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 121
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    :goto_0
    iput-boolean v5, p0, Lcom/flixster/android/activity/hc/Main;->skipRatePrompt:Z

    .line 122
    const-string v5, "FlxMain"

    const-string v6, "Main.onCreate"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-static {}, Lcom/flixster/android/utils/ImageTaskImpl;->instance()Lcom/flixster/android/utils/ImageTaskImpl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/flixster/android/utils/ImageTaskImpl;->startTask()V

    .line 125
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-nez v5, :cond_0

    .line 126
    new-instance v5, Ljava/util/Timer;

    invoke-direct {v5}, Ljava/util/Timer;-><init>()V

    iput-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    .line 130
    :cond_0
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    if-nez v5, :cond_2

    move-object v3, v4

    .line 131
    check-cast v3, [Ljava/lang/String;

    .line 132
    .local v3, persistedTags:[Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 133
    const-string v5, "KEY_CONTENT_FRAGMENT_STACK"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 135
    :cond_1
    new-instance v5, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-direct {v5, v6, v3}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;-><init>(Landroid/app/FragmentManager;[Ljava/lang/String;)V

    iput-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    .line 136
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mContentFragmentStack.size="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v7}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    .end local v3           #persistedTags:[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    if-nez v5, :cond_4

    move-object v3, v4

    .line 141
    check-cast v3, [Ljava/lang/String;

    .line 142
    .restart local v3       #persistedTags:[Ljava/lang/String;
    if-eqz p1, :cond_3

    .line 143
    const-string v4, "KEY_LIST_FRAGMENT_STACK"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 145
    :cond_3
    new-instance v4, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;-><init>(Landroid/app/FragmentManager;[Ljava/lang/String;)V

    iput-object v4, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    .line 146
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mListFragmentStack.size="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    .end local v3           #persistedTags:[Ljava/lang/String;
    :cond_4
    const/4 v2, 0x0

    .line 151
    .local v2, lastTabIndex:I
    if-eqz p1, :cond_5

    .line 152
    const-string v4, "KEY_LAST_TAB_INDEX"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 153
    invoke-static {}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->values()[Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    move-result-object v4

    aget-object v4, v4, v2

    iput-object v4, p0, Lcom/flixster/android/activity/hc/Main;->mCurrTab:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    .line 156
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 157
    .local v0, ft:Landroid/app/FragmentTransaction;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    iget-object v4, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-lt v1, v4, :cond_7

    .line 160
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-lt v1, v4, :cond_8

    .line 163
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 166
    .end local v0           #ft:Landroid/app/FragmentTransaction;
    .end local v1           #i:I
    :cond_5
    const v4, 0x7f03002c

    invoke-virtual {p0, v4}, Lcom/flixster/android/activity/hc/Main;->setContentView(I)V

    .line 167
    invoke-direct {p0, v2}, Lcom/flixster/android/activity/hc/Main;->createActionBar(I)V

    .line 170
    invoke-static {p0}, Lnet/flixster/android/FlixsterApplication;->setCurrentDimensions(Landroid/app/Activity;)V

    .line 173
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/Main;->showDiagnosticsOrRateDialog()V

    .line 176
    invoke-static {}, Lcom/flixster/android/bootstrap/Startup;->instance()Lcom/flixster/android/bootstrap/Startup;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/flixster/android/bootstrap/Startup;->onStartup(Landroid/app/Activity;)V

    .line 250
    return-void

    .line 121
    .end local v2           #lastTabIndex:I
    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 158
    .restart local v0       #ft:Landroid/app/FragmentTransaction;
    .restart local v1       #i:I
    .restart local v2       #lastTabIndex:I
    :cond_7
    iget-object v4, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v4, v1}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->elementAt(I)Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 161
    :cond_8
    iget-object v4, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v4, v1}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->elementAt(I)Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 160
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v3, 0x1

    .line 364
    packed-switch p1, :pswitch_data_0

    .line 408
    :pswitch_0
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 368
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->shouldShowSplash()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->splashShown()V

    .line 370
    const v0, 0x3b9ac9ff

    invoke-static {p0, v0}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    .line 371
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 373
    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    .line 374
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 376
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 377
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 378
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 381
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c012f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 382
    invoke-static {p0}, Lnet/flixster/android/util/DialogUtils;->getShowtimesDateOptions(Landroid/content/Context;)[Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/flixster/android/activity/hc/Main$4;

    invoke-direct {v2, p0}, Lcom/flixster/android/activity/hc/Main$4;-><init>(Lcom/flixster/android/activity/hc/Main;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 395
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 397
    :pswitch_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 398
    const-string v1, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 399
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 400
    const-string v1, "Retry"

    new-instance v2, Lcom/flixster/android/activity/hc/Main$5;

    invoke-direct {v2, p0}, Lcom/flixster/android/activity/hc/Main$5;-><init>(Lcom/flixster/android/activity/hc/Main;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 404
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 364
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 645
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 646
    .local v0, inflater:Landroid/view/MenuInflater;
    const v1, 0x7f0f000b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 647
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 290
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;->onDestroy()V

    .line 291
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->stopLocationRequest()V

    .line 292
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .parameter "keycode"
    .parameter "event"

    .prologue
    const/4 v2, 0x1

    .line 431
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x4

    if-ne p1, v3, :cond_1

    .line 432
    iget-object v3, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v0

    .line 433
    .local v0, contentSize:I
    iget-object v3, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v1

    .line 434
    .local v1, listSize:I
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Main.onKeyDown mContentFragmentStack.size="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mListFragmentStack.size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 435
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 434
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    if-le v0, v2, :cond_0

    .line 437
    iget-object v3, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    const v4, 0x7f04000a

    const v5, 0x7f04000c

    invoke-direct {p0, v3, v4, v5}, Lcom/flixster/android/activity/hc/Main;->removeFragment(Lcom/flixster/android/activity/hc/FlixsterFragmentStack;II)V

    .line 446
    .end local v0           #contentSize:I
    .end local v1           #listSize:I
    :goto_0
    return v2

    .line 439
    .restart local v0       #contentSize:I
    .restart local v1       #listSize:I
    :cond_0
    if-le v1, v2, :cond_1

    .line 440
    iget-object v3, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    const v4, 0x7f04000b

    .line 441
    const v5, 0x7f04000d

    .line 440
    invoke-direct {p0, v3, v4, v5}, Lcom/flixster/android/activity/hc/Main;->removeFragment(Lcom/flixster/android/activity/hc/FlixsterFragmentStack;II)V

    goto :goto_0

    .line 446
    .end local v0           #contentSize:I
    .end local v1           #listSize:I
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/flixster/android/activity/common/DecoratedActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 572
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;->onLowMemory()V

    .line 573
    const-string v0, "FlxMain"

    const-string v1, "Low memory!"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->clearBitmapCache()V

    .line 575
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 11
    .parameter "item"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 653
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 700
    :pswitch_0
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 656
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/main/tablet"

    const-string v2, "Main - Tablet"

    const-string v9, "MainTabletActionBar"

    const-string v10, "Search"

    invoke-interface {v0, v1, v2, v9, v10}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    sget-object v0, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->SEARCH:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    iput-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mCurrTab:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    .line 658
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 659
    .local v5, ft:Landroid/app/FragmentTransaction;
    invoke-direct {p0, v5}, Lcom/flixster/android/activity/hc/Main;->resetContentBackStack(Landroid/app/FragmentTransaction;)V

    .line 660
    invoke-direct {p0, v5}, Lcom/flixster/android/activity/hc/Main;->resetListBackStack(Landroid/app/FragmentTransaction;)V

    .line 661
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/flixster/android/activity/hc/SearchFragment;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;Landroid/app/FragmentTransaction;)V

    .line 662
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commit()I

    move v0, v8

    .line 663
    goto :goto_0

    .line 666
    .end local v5           #ft:Landroid/app/FragmentTransaction;
    :pswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/main/tablet"

    const-string v2, "Main - Tablet"

    const-string v3, "MainTabletActionBar"

    const-string v4, "Settings"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    new-instance v6, Landroid/content/Intent;

    const-class v0, Lnet/flixster/android/SettingsPage;

    invoke-direct {v6, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 668
    .local v6, intent:Landroid/content/Intent;
    invoke-virtual {p0, v6}, Lcom/flixster/android/activity/hc/Main;->startActivity(Landroid/content/Intent;)V

    move v0, v8

    .line 669
    goto :goto_0

    .line 672
    .end local v6           #intent:Landroid/content/Intent;
    :pswitch_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/main/tablet"

    const-string v2, "Main - Tablet"

    const-string v4, "MainTabletActionBar"

    const-string v9, "TellAFriend"

    invoke-interface {v0, v1, v2, v4, v9}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    new-instance v7, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 674
    .local v7, shareIntent:Landroid/content/Intent;
    const-string v0, "text/plain"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 675
    const-string v0, "android.intent.extra.TEXT"

    .line 676
    const v1, 0x7f0c01b2

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->mobileUpsell()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/flixster/android/activity/hc/Main;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 675
    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 677
    const-string v0, "Share via"

    invoke-static {v7, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/hc/Main;->startActivity(Landroid/content/Intent;)V

    move v0, v8

    .line 678
    goto/16 :goto_0

    .line 680
    .end local v7           #shareIntent:Landroid/content/Intent;
    :pswitch_4
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/main/tablet"

    const-string v2, "Main - Tablet"

    const-string v3, "MainTabletActionBar"

    const-string v4, "RateThisApp"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    invoke-static {p0}, Lcom/flixster/android/utils/AppRater;->rateThisApp(Landroid/content/Context;)V

    move v0, v8

    .line 682
    goto/16 :goto_0

    .line 685
    :pswitch_5
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/main/tablet"

    const-string v2, "Main - Tablet"

    const-string v3, "MainTabletActionBar"

    const-string v4, "FAQ"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->faq()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lnet/flixster/android/Starter;->launchBrowser(Ljava/lang/String;Landroid/content/Context;)V

    move v0, v8

    .line 687
    goto/16 :goto_0

    .line 690
    :pswitch_6
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/main/tablet"

    const-string v2, "Main - Tablet"

    const-string v3, "MainTabletActionBar"

    const-string v9, "PrivacyPolicy"

    invoke-interface {v0, v1, v2, v3, v9}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    const v0, 0x3b9aca0a

    new-instance v1, Lcom/flixster/android/activity/hc/Main$PrivacyDialogListener;

    invoke-direct {v1, p0, v4}, Lcom/flixster/android/activity/hc/Main$PrivacyDialogListener;-><init>(Lcom/flixster/android/activity/hc/Main;Lcom/flixster/android/activity/hc/Main$PrivacyDialogListener;)V

    invoke-virtual {p0, v0, v1}, Lcom/flixster/android/activity/hc/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    move v0, v8

    .line 692
    goto/16 :goto_0

    .line 695
    :pswitch_7
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/main/tablet"

    const-string v2, "Main - Tablet"

    const-string v3, "MainTabletActionBar"

    const-string v4, "Feedback"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->feedback()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0c0135

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/hc/Main;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lnet/flixster/android/Starter;->launchFlxHtmlPage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    move v0, v8

    .line 697
    goto/16 :goto_0

    .line 653
    nop

    :pswitch_data_0
    .packed-switch 0x7f0702fa
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 279
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;->onPause()V

    .line 280
    invoke-static {}, Lcom/flixster/android/utils/ImageTaskImpl;->instance()Lcom/flixster/android/utils/ImageTaskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/ImageTaskImpl;->stopTask()V

    .line 281
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 283
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    .line 286
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 254
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;->onResume()V

    .line 256
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 257
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    .line 259
    :cond_0
    const-string v0, "FlxMain"

    const-string v1, "Main.onResume start ImageTask"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-static {}, Lcom/flixster/android/utils/ImageTaskImpl;->instance()Lcom/flixster/android/utils/ImageTaskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/ImageTaskImpl;->startTask()V

    .line 263
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sNetflixStartupValidate:Z

    if-nez v0, :cond_1

    .line 264
    const/4 v0, 0x1

    sput-boolean v0, Lnet/flixster/android/FlixsterApplication;->sNetflixStartupValidate:Z

    .line 267
    :try_start_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 268
    const-string v0, "12"

    invoke-static {v0}, Lnet/flixster/android/data/NetflixDao;->fetchTitleState(Ljava/lang/String;)Lnet/flixster/android/model/NetflixTitleState;

    .line 269
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/startupvalidate"

    const-string v2, "User token valid"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    :cond_1
    :goto_0
    return-void

    .line 271
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 304
    iget-boolean v0, p0, Lcom/flixster/android/activity/hc/Main;->skipRatePrompt:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 296
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 297
    const-string v0, "KEY_CONTENT_FRAGMENT_STACK"

    iget-object v1, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->persist()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 298
    const-string v0, "KEY_LIST_FRAGMENT_STACK"

    iget-object v1, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->persist()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 299
    const-string v0, "KEY_LAST_TAB_INDEX"

    iget-object v1, p0, Lcom/flixster/android/activity/hc/Main;->mCurrTab:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 300
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;->onStart()V

    .line 114
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 115
    return-void
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 727
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mCurrTab:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    sget-object v1, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->SEARCH:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    if-ne v0, v1, :cond_0

    .line 728
    invoke-virtual {p0, p1, p2}, Lcom/flixster/android/activity/hc/Main;->onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V

    .line 730
    :cond_0
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 7
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 707
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    .line 708
    .local v6, newTab:Lcom/flixster/android/activity/hc/Main$FlixFragTab;
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mCurrTab:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    if-ne v0, v6, :cond_0

    .line 710
    const-string v0, "FlxMain"

    const-string v1, "Main.onTabSelected ignored"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    :goto_0
    return-void

    .line 712
    :cond_0
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Main.onTabSelected currTab="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/activity/hc/Main;->mCurrTab:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " newTab="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    iput-object v6, p0, Lcom/flixster/android/activity/hc/Main;->mCurrTab:Lcom/flixster/android/activity/hc/Main$FlixFragTab;

    .line 714
    invoke-direct {p0, p2}, Lcom/flixster/android/activity/hc/Main;->resetContentBackStack(Landroid/app/FragmentTransaction;)V

    .line 715
    invoke-direct {p0, p2}, Lcom/flixster/android/activity/hc/Main;->resetListBackStack(Landroid/app/FragmentTransaction;)V

    .line 716
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #calls: Lcom/flixster/android/activity/hc/Main$FlixFragTab;->getFragClass()Ljava/lang/Class;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/Main$FlixFragTab;->access$5(Lcom/flixster/android/activity/hc/Main$FlixFragTab;)Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;Landroid/app/FragmentTransaction;)V

    goto :goto_0
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 722
    return-void
.end method

.method public orderImage(Lnet/flixster/android/model/ImageOrder;)V
    .locals 1
    .parameter "io"

    .prologue
    .line 415
    invoke-static {}, Lcom/flixster/android/utils/ImageTaskImpl;->instance()Lcom/flixster/android/utils/ImageTaskImpl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/flixster/android/utils/ImageTaskImpl;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 416
    return-void
.end method

.method public orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V
    .locals 1
    .parameter "io"

    .prologue
    .line 425
    invoke-static {}, Lcom/flixster/android/utils/ImageTaskImpl;->instance()Lcom/flixster/android/utils/ImageTaskImpl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/flixster/android/utils/ImageTaskImpl;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 426
    return-void
.end method

.method public orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V
    .locals 1
    .parameter "io"

    .prologue
    .line 420
    invoke-static {}, Lcom/flixster/android/utils/ImageTaskImpl;->instance()Lcom/flixster/android/utils/ImageTaskImpl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/flixster/android/utils/ImageTaskImpl;->orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 421
    return-void
.end method

.method protected peekContentBackStack()Lcom/flixster/android/activity/hc/FlixsterFragment;
    .locals 1

    .prologue
    .line 907
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->peek()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v0

    return-object v0
.end method

.method protected peekListBackStack()Lcom/flixster/android/activity/hc/FlixsterFragment;
    .locals 1

    .prologue
    .line 902
    iget-object v0, p0, Lcom/flixster/android/activity/hc/Main;->mListFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->peek()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v0

    return-object v0
.end method

.method protected popContentBackStack()V
    .locals 6

    .prologue
    .line 888
    iget-object v3, p0, Lcom/flixster/android/activity/hc/Main;->mContentFragmentStack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;

    .line 889
    .local v3, stack:Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    .line 890
    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->pop()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v0

    .line 891
    .local v0, exitFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/FlixsterFragmentStack;->peek()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v2

    .line 892
    .local v2, restoreFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 893
    .local v1, ft:Landroid/app/FragmentTransaction;
    const v4, 0x7f04000a

    const v5, 0x7f04000c

    invoke-virtual {v1, v4, v5}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    .line 894
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 895
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 896
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 898
    .end local v0           #exitFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    .end local v1           #ft:Landroid/app/FragmentTransaction;
    .end local v2           #restoreFrag:Lcom/flixster/android/activity/hc/FlixsterFragment;
    :cond_0
    return-void
.end method

.method protected savePhoto(Lnet/flixster/android/model/Photo;)Z
    .locals 10
    .parameter "photo"

    .prologue
    const/4 v7, 0x1

    .line 528
    const-string v6, "FlxMain"

    const-string v8, "FlixsterActivity savePhoto title:"

    invoke-static {v6, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    const/4 v5, 0x0

    .line 531
    .local v5, wasSuccessfull:Z
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "photo"

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, p1, Lnet/flixster/android/model/Photo;->id:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ".png"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 532
    .local v4, title:Ljava/lang/String;
    const-string v0, "Flixster Movies Photo"

    .line 533
    .local v0, description:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 535
    .local v1, descriptionBuilder:Ljava/lang/StringBuilder;
    iget-object v6, p1, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p1, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v7, :cond_0

    .line 536
    iget-object v4, p1, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    .line 537
    const-string v6, " "

    const-string v8, "_"

    invoke-virtual {v4, v6, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 538
    iget-object v6, p1, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    :cond_0
    iget-object v6, p1, Lnet/flixster/android/model/Photo;->description:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p1, Lnet/flixster/android/model/Photo;->description:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v7, :cond_2

    .line 541
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 542
    const-string v6, " - "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544
    :cond_1
    iget-object v6, p1, Lnet/flixster/android/model/Photo;->description:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_3

    .line 547
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 550
    :cond_3
    :try_start_0
    const-string v6, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "FlixsterActivity savePhoto title:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " desc:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    iget-object v6, p1, Lnet/flixster/android/model/Photo;->galleryBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v6}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    invoke-static {v8, v6, v4, v0}, Landroid/provider/MediaStore$Images$Media;->insertImage(Landroid/content/ContentResolver;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 553
    .local v3, status:Ljava/lang/String;
    if-eqz v3, :cond_4

    move v5, v7

    .line 557
    .end local v3           #status:Ljava/lang/String;
    :goto_0
    return v5

    .line 553
    .restart local v3       #status:Ljava/lang/String;
    :cond_4
    const/4 v5, 0x0

    goto :goto_0

    .line 554
    .end local v3           #status:Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 555
    .local v2, e:Ljava/lang/Exception;
    const-string v6, "FlxMain"

    const-string v7, "Problem saving image"

    invoke-static {v6, v7, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected startFragment(Landroid/content/Intent;Ljava/lang/Class;)V
    .locals 2
    .parameter "i"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flixster/android/activity/hc/FlixsterFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 739
    .local p2, fc:Ljava/lang/Class;,"Ljava/lang/Class<+Lcom/flixster/android/activity/hc/FlixsterFragment;>;"
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    .line 740
    return-void
.end method

.method protected startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V
    .locals 6
    .parameter "i"
    .parameter
    .parameter "slideHint"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flixster/android/activity/hc/FlixsterFragment;",
            ">;I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flixster/android/activity/hc/FlixsterFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 745
    .local p2, fc:Ljava/lang/Class;,"Ljava/lang/Class<+Lcom/flixster/android/activity/hc/FlixsterFragment;>;"
    .local p4, source:Ljava/lang/Class;,"Ljava/lang/Class<+Lcom/flixster/android/activity/hc/FlixsterFragment;>;"
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .local v5, ft:Landroid/app/FragmentTransaction;
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    .line 746
    invoke-direct/range {v0 .. v5}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;Landroid/app/FragmentTransaction;)V

    .line 747
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commit()I

    .line 748
    return-void
.end method
