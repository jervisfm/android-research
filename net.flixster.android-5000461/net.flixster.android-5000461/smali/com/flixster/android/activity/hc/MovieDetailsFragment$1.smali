.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$1;
.super Ljava/lang/Object;
.source "MovieDetailsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$1;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 752
    sget-boolean v1, Lcom/flixster/android/utils/F;->IS_NATIVE_HC_DRM_ENABLED:Z

    if-eqz v1, :cond_0

    .line 753
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$1;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/flixster/android/drm/PlaybackManager;->playMovie(Lnet/flixster/android/model/LockerRight;)V

    .line 763
    :goto_0
    return-void

    .line 755
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$1;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    iget-wide v1, v1, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v1

    if-nez v1, :cond_1

    .line 756
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$1;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    iget-wide v1, v1, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 757
    .local v0, isLocalPlayback:Z
    :goto_1
    if-nez v0, :cond_2

    invoke-static {}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 758
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$1;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    const v2, 0x3b9acb2c

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$1;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$1(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/flixster/android/activity/hc/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 756
    .end local v0           #isLocalPlayback:Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 760
    .restart local v0       #isLocalPlayback:Z
    :cond_2
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$1;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/flixster/android/drm/PlaybackManager;->playMovie(Lnet/flixster/android/model/LockerRight;)V

    goto :goto_0
.end method
