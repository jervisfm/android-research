.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$11;
.super Landroid/os/Handler;
.source "MovieDetailsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$11;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 1297
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 1300
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$11;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 1301
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$11;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1308
    :cond_0
    :goto_0
    return-void

    .line 1305
    :cond_1
    const-string v1, "FlxMain"

    const-string v2, "MovieDetailsFragment.refreshHandler run..."

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1307
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$11;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->populatePage()V
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$11(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    goto :goto_0
.end method
