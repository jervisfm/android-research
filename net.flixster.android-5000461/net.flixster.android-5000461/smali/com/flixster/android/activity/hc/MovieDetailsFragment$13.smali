.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;
.super Ljava/lang/Object;
.source "MovieDetailsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 1532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "view"

    .prologue
    .line 1534
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Photo;

    .line 1535
    .local v2, photo:Lnet/flixster/android/model/Photo;
    if-eqz v2, :cond_1

    .line 1536
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1537
    .local v0, index:I
    if-gez v0, :cond_0

    .line 1538
    const/4 v0, 0x0

    .line 1540
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTagPrefix()Ljava/lang/String;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$13(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/photo"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1541
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTitlePrefix()Ljava/lang/String;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$14(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "Photo Page for photo:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1540
    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1542
    new-instance v1, Landroid/content/Intent;

    const-string v3, "PHOTO"

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v5}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-class v6, Lnet/flixster/android/ScrollGalleryPage;

    invoke-direct {v1, v3, v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 1543
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1544
    const-string v3, "PHOTO_INDEX"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1546
    const-string v3, "title"

    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    const-string v5, "title"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1547
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v3, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->startActivity(Landroid/content/Intent;)V

    .line 1549
    .end local v0           #index:I
    .end local v1           #intent:Landroid/content/Intent;
    :cond_1
    return-void
.end method
