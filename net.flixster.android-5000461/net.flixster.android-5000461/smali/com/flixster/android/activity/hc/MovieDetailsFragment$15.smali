.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;
.super Ljava/lang/Object;
.source "MovieDetailsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 1578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "view"

    .prologue
    .line 1580
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorsLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$18(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/widget/LinearLayout;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x3

    if-le v4, v5, :cond_0

    .line 1583
    const/4 v1, 0x0

    .line 1587
    .local v1, actorView:Landroid/view/View;
    const/4 v3, 0x3

    .local v3, i:I
    :goto_0
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_2

    .line 1600
    .end local v1           #actorView:Landroid/view/View;
    .end local v3           #i:I
    :cond_0
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$19(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/widget/RelativeLayout;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1601
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$19(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/widget/RelativeLayout;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1603
    :cond_1
    return-void

    .line 1588
    .restart local v1       #actorView:Landroid/view/View;
    .restart local v3       #i:I
    :cond_2
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 1589
    .local v0, actor:Lnet/flixster/android/model/Actor;
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActorView(Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;
    invoke-static {v4, v0, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$16(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 1591
    new-instance v2, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1592
    .local v2, dividerView:Landroid/widget/ImageView;
    const v4, 0x7f09001a

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1593
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 v5, -0x1

    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 1594
    const v7, 0x7f0a0025

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    invoke-direct {v4, v5, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1593
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1596
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorsLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$18(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1597
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorsLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$18(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1587
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
