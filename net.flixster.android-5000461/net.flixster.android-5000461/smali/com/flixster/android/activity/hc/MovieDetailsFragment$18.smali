.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;
.super Landroid/os/Handler;
.source "MovieDetailsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 1637
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .parameter "message"

    .prologue
    const v12, 0x7f0c00d1

    const/4 v11, 0x1

    const/4 v10, 0x0

    const v9, 0x7f0c00d3

    .line 1640
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 1641
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isRemoving()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1705
    :cond_0
    return-void

    .line 1645
    :cond_1
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getView()Landroid/view/View;

    move-result-object v5

    .line 1646
    .local v5, view:Landroid/view/View;
    if-eqz v5, :cond_0

    .line 1649
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1650
    const v6, 0x7f070159

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1651
    .local v3, netflixText:Landroid/widget/TextView;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1652
    .local v2, menuActions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1653
    const-string v6, "FlxMain"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "MovieDetailsFragment.mNetflixTitleStateHandler mNetflixTitleState:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1654
    iget-object v8, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v8}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1653
    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655
    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(I)V

    .line 1656
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v6

    iget v6, v6, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v6, v6, 0x8

    if-eqz v6, :cond_5

    .line 1657
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(I)V

    .line 1658
    const/4 v6, 0x6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1659
    const/16 v6, 0xb

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1660
    const/16 v6, 0xc

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1668
    :cond_2
    :goto_0
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v6

    iget v6, v6, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_6

    .line 1669
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(I)V

    .line 1670
    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1671
    const/16 v6, 0x9

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1672
    const/16 v6, 0xa

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1684
    :cond_3
    :goto_1
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v6

    iget v6, v6, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v6, v6, 0x9

    if-eqz v6, :cond_8

    .line 1685
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(I)V

    .line 1692
    :cond_4
    :goto_2
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1694
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1695
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1696
    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 1698
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1699
    .local v4, totalActions:I
    const-string v6, "FlxMain"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "menuActions:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1700
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    new-array v7, v4, [I

    #setter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I
    invoke-static {v6, v7}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$21(Lcom/flixster/android/activity/hc/MovieDetailsFragment;[I)V

    .line 1701
    const/4 v0, 0x0

    .local v0, i:I
    :goto_3
    if-ge v0, v4, :cond_0

    .line 1702
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$22(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)[I

    move-result-object v7

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput v6, v7, v0

    .line 1701
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1663
    .end local v0           #i:I
    .end local v4           #totalActions:I
    :cond_5
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v6

    iget v6, v6, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v6, v6, 0x10

    if-eqz v6, :cond_2

    .line 1664
    const/4 v6, 0x7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1665
    const/16 v6, 0x8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1676
    :cond_6
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v6

    iget v6, v6, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_7

    .line 1677
    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1678
    const/4 v6, 0x4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1679
    :cond_7
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v6

    iget v6, v6, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v6, v6, 0x4

    if-eqz v6, :cond_3

    .line 1680
    const/4 v6, 0x5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1686
    :cond_8
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v6

    iget v6, v6, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v6, v6, 0x4

    if-eqz v6, :cond_9

    .line 1687
    const v6, 0x7f0c00d2

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 1688
    :cond_9
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v6

    iget v6, v6, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v6, v6, 0x12

    if-eqz v6, :cond_4

    .line 1689
    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2
.end method
