.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;
.super Ljava/lang/Object;
.source "MovieDetailsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "view"

    .prologue
    const/4 v5, 0x0

    const v4, 0x3b9acac8

    .line 769
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    iget-wide v0, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v0, v1}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 771
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    iget-wide v0, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v0, v1}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 774
    invoke-static {}, Lcom/flixster/android/storage/ExternalStorage;->isWriteable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 775
    invoke-static {}, Lcom/flixster/android/net/DownloadHelper;->findRemainingSpace()J

    move-result-wide v0

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetSizeRaw()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 776
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 777
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 778
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$2(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/flixster/android/activity/hc/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 786
    :cond_0
    :goto_0
    return-void

    .line 780
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    const v1, 0x3b9acacd

    invoke-virtual {v0, v1, v5}, Lcom/flixster/android/activity/hc/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 783
    :cond_2
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    const v1, 0x3b9acacb

    invoke-virtual {v0, v1, v5}, Lcom/flixster/android/activity/hc/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0
.end method
