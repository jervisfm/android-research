.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$20;
.super Ljava/util/TimerTask;
.source "MovieDetailsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleNetflixTitleState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$20;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 1091
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1094
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$20;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$20;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v2

    const-string v3, "netflix"

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/data/NetflixDao;->fetchTitleState(Ljava/lang/String;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v2

    #setter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v1, v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$24(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Lnet/flixster/android/model/NetflixTitleState;)V

    .line 1095
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$20;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleStateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$25(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_4

    .line 1109
    :goto_0
    return-void

    .line 1097
    :catch_0
    move-exception v0

    .line 1098
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetailsFragment.ScheduleNetflixTitleState OAuthMessageSignerException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1099
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v0

    .line 1100
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetailsFragment.ScheduleNetflixTitleState OAuthExpectationFailedException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1101
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_2
    move-exception v0

    .line 1102
    .local v0, e:Ljava/io/IOException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetailsFragment.ScheduleNetflixTitleState IOException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1103
    .end local v0           #e:Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 1104
    .local v0, e:Lorg/json/JSONException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetailsFragment.ScheduleNetflixTitleState JSONException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1105
    .end local v0           #e:Lorg/json/JSONException;
    :catch_4
    move-exception v0

    .line 1107
    .local v0, e:Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_0
.end method
