.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$23;
.super Ljava/util/TimerTask;
.source "MovieDetailsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleRemoveFromQueue(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

.field private final synthetic val$qtype:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$23;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    iput-object p2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$23;->val$qtype:Ljava/lang/String;

    .line 1197
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1201
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$23;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v3

    const-string v4, "netflix"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1202
    .local v1, movieUrl:Ljava/lang/String;
    const/16 v3, 0x25

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1203
    .local v2, movieUrlId:Ljava/lang/String;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieDetailsFragment.ScheduleRemoveFromQueue movieUrl:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1204
    const-string v5, " movieUrlId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1203
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$23;->val$qtype:Ljava/lang/String;

    invoke-static {v2, v3}, Lnet/flixster/android/data/NetflixDao;->deleteQueueItem(Ljava/lang/String;Ljava/lang/String;)V

    .line 1209
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/netflix/removefromqueue"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$23;->val$qtype:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Delete"

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$23;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleNetflixTitleState()V
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$26(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1226
    .end local v1           #movieUrl:Ljava/lang/String;
    .end local v2           #movieUrlId:Ljava/lang/String;
    :goto_0
    return-void

    .line 1211
    :catch_0
    move-exception v0

    .line 1212
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetailsFragment.ScheduleRemoveFromQueue OAuthMessageSignerException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1213
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v0

    .line 1214
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetailsFragment.ScheduleRemoveFromQueue OAuthExpectationFailedException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1215
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_2
    move-exception v0

    .line 1216
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetailsFragment.ScheduleRemoveFromQueue ClientProtocolException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1217
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_3
    move-exception v0

    .line 1218
    .local v0, e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetailsFragment.ScheduleRemoveFromQueue OAuthNotAuthorizedException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1219
    .end local v0           #e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_4
    move-exception v0

    .line 1220
    .local v0, e:Ljava/io/IOException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetailsFragment.ScheduleRemoveFromQueue IOException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1221
    .end local v0           #e:Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 1223
    .local v0, e:Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_0
.end method
