.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;
.super Ljava/util/TimerTask;
.source "MovieDetailsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleLoadMovieTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 1234
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 1237
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    .line 1238
    .local v2, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1269
    :cond_0
    :goto_0
    return-void

    .line 1242
    :cond_1
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieDetailsFragment.ScheduleLoadMovieTask.run mMovieId:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovieId:J
    invoke-static {v5}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$28(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    :try_start_0
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isSeason:Z
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$29(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovieId:J
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$28(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)J

    move-result-wide v5

    invoke-static {v5, v6}, Lnet/flixster/android/data/MovieDao;->getSeasonDetailLegacy(J)Lnet/flixster/android/model/Season;

    move-result-object v3

    :goto_1
    #setter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4, v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$30(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Lnet/flixster/android/model/Movie;)V

    .line 1246
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1247
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleNetflixTitleState()V
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$26(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    .line 1250
    :cond_2
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const/4 v4, 0x0

    iput v4, v3, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mRetryCount:I

    .line 1251
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mFromRating:Z
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$31(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1252
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1253
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterId()Ljava/lang/String;
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1255
    .local v1, flixsterId:Ljava/lang/String;
    :try_start_1
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovieId:J
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$28(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lnet/flixster/android/data/ProfileDao;->getUserMovieReview(Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/Review;

    move-result-object v4

    #setter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;
    invoke-static {v3, v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$32(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Lnet/flixster/android/model/Review;)V
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1261
    .end local v1           #flixsterId:Ljava/lang/String;
    :cond_3
    :goto_2
    :try_start_2
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->refreshHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$33(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catch Lnet/flixster/android/data/DaoException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1265
    :catch_0
    move-exception v0

    .line 1266
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetailsFragment.ScheduleLoadMovieTask.run DaoException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1267
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v3, v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->retryLogic(Lnet/flixster/android/data/DaoException;)V

    goto :goto_0

    .line 1244
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_4
    :try_start_3
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovieId:J
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$28(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)J

    move-result-wide v5

    invoke-static {v5, v6}, Lnet/flixster/android/data/MovieDao;->getMovieDetail(J)Lnet/flixster/android/model/Movie;

    move-result-object v3

    goto :goto_1

    .line 1256
    .restart local v1       #flixsterId:Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 1257
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    const-string v3, "FlxMain"

    const-string v4, "Error from review"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2
.end method
