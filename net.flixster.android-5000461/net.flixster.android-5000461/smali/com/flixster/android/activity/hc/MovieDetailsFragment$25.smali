.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;
.super Ljava/lang/Object;
.source "MovieDetailsFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 1767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1770
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MovieDetailsFragment.onCreateDialog mNetflixMenuToAction[which]:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1771
    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I
    invoke-static {v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$22(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)[I

    move-result-object v2

    aget v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1770
    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1772
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$22(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)[I

    move-result-object v0

    aget v0, v0, p2

    packed-switch v0, :pswitch_data_0

    .line 1824
    const-string v0, "FlxMain"

    const-string v1, "MovieDetailsFragment.onCreateDialog -default-"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1828
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->NetflixRemoveMenuDialog()V
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$37(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    .line 1832
    return-void

    .line 1776
    :pswitch_1
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 1780
    :pswitch_2
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const-string v1, "/queues/disc/available"

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleAddToQueue(ILjava/lang/String;)V
    invoke-static {v0, v3, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$34(Lcom/flixster/android/activity/hc/MovieDetailsFragment;ILjava/lang/String;)V

    .line 1781
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v3, v0, v3

    goto :goto_0

    .line 1785
    :pswitch_3
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const-string v1, "/queues/disc/available"

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleAddToQueue(ILjava/lang/String;)V
    invoke-static {v0, v5, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$34(Lcom/flixster/android/activity/hc/MovieDetailsFragment;ILjava/lang/String;)V

    .line 1786
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v3, v0, v3

    goto :goto_0

    .line 1791
    :pswitch_4
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const-string v1, "/queues/disc/available"

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleMoveToBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$35(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Ljava/lang/String;)V

    .line 1792
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v3, v0, v3

    goto :goto_0

    .line 1796
    :pswitch_5
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const-string v1, "/queues/instant/available"

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleAddToQueue(ILjava/lang/String;)V
    invoke-static {v0, v3, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$34(Lcom/flixster/android/activity/hc/MovieDetailsFragment;ILjava/lang/String;)V

    .line 1797
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v3, v0, v4

    goto :goto_0

    .line 1800
    :pswitch_6
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const-string v1, "/queues/instant/available"

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleAddToQueue(ILjava/lang/String;)V
    invoke-static {v0, v5, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$34(Lcom/flixster/android/activity/hc/MovieDetailsFragment;ILjava/lang/String;)V

    .line 1801
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v3, v0, v4

    goto :goto_0

    .line 1806
    :pswitch_7
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const-string v1, "/queues/instant/available"

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleMoveToBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$35(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Ljava/lang/String;)V

    .line 1807
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v3, v0, v4

    goto :goto_0

    .line 1810
    :pswitch_8
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const-string v1, "/queues/disc/saved"

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleAddToQueue(ILjava/lang/String;)V
    invoke-static {v0, v5, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$34(Lcom/flixster/android/activity/hc/MovieDetailsFragment;ILjava/lang/String;)V

    .line 1811
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v3, v0, v6

    goto :goto_0

    .line 1815
    :pswitch_9
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const-string v1, "/queues/disc/available"

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleRemoveFromQueue(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$36(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Ljava/lang/String;)V

    .line 1816
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v3, v0, v3

    .line 1817
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v3, v0, v6

    goto :goto_0

    .line 1820
    :pswitch_a
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const-string v1, "/queues/instant/available"

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleRemoveFromQueue(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$36(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Ljava/lang/String;)V

    .line 1821
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v3, v0, v4

    goto/16 :goto_0

    .line 1772
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_9
        :pswitch_2
        :pswitch_3
        :pswitch_8
        :pswitch_a
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method
