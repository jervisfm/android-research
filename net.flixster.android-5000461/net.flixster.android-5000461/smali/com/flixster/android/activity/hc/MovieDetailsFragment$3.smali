.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;
.super Ljava/lang/Object;
.source "MovieDetailsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    const v3, 0x3b9acaca

    .line 792
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    iget-wide v0, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v0, v1}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    const v1, 0x3b9acac9

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$3(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/activity/hc/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 800
    :cond_0
    :goto_0
    return-void

    .line 794
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    iget-wide v0, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v0, v1}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 796
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$4(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/flixster/android/activity/hc/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0
.end method
