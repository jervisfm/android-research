.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$4;
.super Landroid/os/Handler;
.source "MovieDetailsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$4;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 803
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 806
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$4;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$5(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 808
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$4;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$5(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 814
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$4;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight;->getDownloadsRemain()I

    move-result v1

    if-lez v1, :cond_1

    .line 815
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$4;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    invoke-static {v1}, Lcom/flixster/android/net/DownloadHelper;->downloadMovie(Lnet/flixster/android/model/LockerRight;)V

    .line 816
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$4;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->delayedStreamingUiUpdate()V
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$6(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    .line 820
    :goto_1
    return-void

    .line 809
    :catch_0
    move-exception v0

    .line 811
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 818
    .end local v0           #e:Ljava/lang/Exception;
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$4;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    const v2, 0x3b9acacc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/flixster/android/activity/hc/Main;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_1
.end method
