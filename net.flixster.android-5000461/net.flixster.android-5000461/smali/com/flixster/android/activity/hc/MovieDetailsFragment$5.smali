.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$5;
.super Landroid/os/Handler;
.source "MovieDetailsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$5;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 823
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 826
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$5;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$5(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 828
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$5;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$5(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 834
    :cond_0
    :goto_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v1, v1, Lnet/flixster/android/data/DaoException;

    if-eqz v1, :cond_1

    .line 835
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lnet/flixster/android/data/DaoException;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$5;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    invoke-static {v1, v2}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedActivity;)V

    .line 837
    :cond_1
    return-void

    .line 829
    :catch_0
    move-exception v0

    .line 831
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
