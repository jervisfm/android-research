.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;
.super Ljava/lang/Object;
.source "MovieDetailsFragment.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 879
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acac8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 880
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 874
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acac8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 875
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 5
    .parameter "which"

    .prologue
    .line 861
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acac8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 862
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/download"

    const-string v2, "Download"

    const-string v3, "Download"

    const-string v4, "Confirm"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$5(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 865
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    invoke-direct {v2, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #setter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$7(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Landroid/app/ProgressDialog;)V

    .line 866
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$5(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0134

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 868
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$5(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 869
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->canDownloadSuccessHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$8(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->errorHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$9(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lnet/flixster/android/data/ProfileDao;->canDownload(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V

    .line 870
    return-void
.end method
