.class Lcom/flixster/android/activity/hc/MovieDetailsFragment$8;
.super Ljava/lang/Object;
.source "MovieDetailsFragment.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$8;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    .line 883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 897
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 893
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 5
    .parameter "which"

    .prologue
    .line 886
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$8;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #getter for: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    iget-wide v0, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v0, v1}, Lcom/flixster/android/net/DownloadHelper;->cancelMovieDownload(J)V

    .line 887
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$8;->this$0:Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    #calls: Lcom/flixster/android/activity/hc/MovieDetailsFragment;->delayedStreamingUiUpdate()V
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->access$6(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    .line 888
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/download"

    const-string v2, "Download"

    const-string v3, "Download"

    const-string v4, "Cancel"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    return-void
.end method
