.class public Lcom/flixster/android/activity/hc/MovieDetailsFragment;
.super Lcom/flixster/android/activity/hc/FlixsterFragment;
.source "MovieDetailsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;
    }
.end annotation


# static fields
.field private static final DIALOG_NETFLIX_ACTION:I = 0x0

.field private static final DIALOG_NETFLIX_EXCEPTION:I = 0x1

.field public static final KEY_IS_SEASON:Ljava/lang/String; = "KEY_IS_SEASON"

.field public static final KEY_RIGHT_ID:Ljava/lang/String; = "KEY_RIGHT_ID"

.field private static final MAX_ACTORS:I = 0x3

.field private static final MAX_DIRECTORS:I = 0x3

.field private static final MAX_PHOTOS:I = 0xf

.field private static final NETFLIX_ACTION_CANCEL:I = 0x0

.field private static final NETFLIX_ACTION_DVD_ADD_BOTTOM:I = 0x4

.field private static final NETFLIX_ACTION_DVD_ADD_TOP:I = 0x3

.field private static final NETFLIX_ACTION_DVD_MOVE_BOTTOM:I = 0xa

.field private static final NETFLIX_ACTION_DVD_MOVE_TOP:I = 0x9

.field private static final NETFLIX_ACTION_DVD_REMOVE:I = 0x2

.field private static final NETFLIX_ACTION_DVD_SAVE:I = 0x5

.field private static final NETFLIX_ACTION_INSTANT_ADD_BOTTOM:I = 0x8

.field private static final NETFLIX_ACTION_INSTANT_ADD_TOP:I = 0x7

.field private static final NETFLIX_ACTION_INSTANT_MOVE_BOTTOM:I = 0xc

.field private static final NETFLIX_ACTION_INSTANT_MOVE_TOP:I = 0xb

.field private static final NETFLIX_ACTION_INSTANT_REMOVE:I = 0x6

.field private static final NETFLIX_ACTION_VIEW_QUEUE:I = 0x1

.field static final RESULTCODE_REVIEW:I = 0x1

.field static final RESULT_CODE_FEED_PERMISSION:I = 0x2


# instance fields
.field private final REVIEW_SUBSET_COUNT:I

.field private actorClickListener:Landroid/view/View$OnClickListener;

.field private actorHandler:Landroid/os/Handler;

.field private actorsLayout:Landroid/widget/LinearLayout;

.field private final canDownloadSuccessHandler:Landroid/os/Handler;

.field private directorsLayout:Landroid/widget/LinearLayout;

.field private final downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private final downloadClickListener:Landroid/view/View$OnClickListener;

.field private final downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private final downloadDeleteClickListener:Landroid/view/View$OnClickListener;

.field private final downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private downloadInitDialog:Landroid/app/ProgressDialog;

.field private downloadPanel:Lcom/flixster/android/view/DownloadPanel;

.field private final errorHandler:Landroid/os/Handler;

.field private isSeason:Z

.field private mFromRating:Z

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mMovie:Lnet/flixster/android/model/Movie;

.field private mMovieId:J

.field private mNetflixMenuToAction:[I

.field private mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;

.field private mNetflixTitleStateHandler:Landroid/os/Handler;

.field private mPlayTrailer:Ljava/lang/Boolean;

.field private mReview:Lnet/flixster/android/model/Review;

.field private moreActorsLayout:Landroid/widget/RelativeLayout;

.field private moreActorsListener:Landroid/view/View$OnClickListener;

.field private moreDirectorsLayout:Landroid/widget/RelativeLayout;

.field private moreDirectorsListener:Landroid/view/View$OnClickListener;

.field private photoClickListener:Landroid/view/View$OnClickListener;

.field private photoHandler:Landroid/os/Handler;

.field private final populateStreamingUiHandler:Landroid/os/Handler;

.field private refreshHandler:Landroid/os/Handler;

.field private right:Lnet/flixster/android/model/LockerRight;

.field private showGetGlue:Z

.field private final watchNowClickListener:Landroid/view/View$OnClickListener;

.field private final watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private watchNowPanel:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;-><init>()V

    .line 117
    const/4 v0, 0x3

    iput v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->REVIEW_SUBSET_COUNT:I

    .line 122
    iput-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mPlayTrailer:Ljava/lang/Boolean;

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mFromRating:Z

    .line 124
    iput-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I

    .line 749
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$1;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->watchNowClickListener:Landroid/view/View$OnClickListener;

    .line 766
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$2;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadClickListener:Landroid/view/View$OnClickListener;

    .line 789
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$3;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadDeleteClickListener:Landroid/view/View$OnClickListener;

    .line 803
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$4;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->canDownloadSuccessHandler:Landroid/os/Handler;

    .line 823
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$5;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$5;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->errorHandler:Landroid/os/Handler;

    .line 843
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$6;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$6;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 858
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$7;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 883
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$8;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$8;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 900
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$9;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$9;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 932
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$10;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$10;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->populateStreamingUiHandler:Landroid/os/Handler;

    .line 1297
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$11;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$11;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->refreshHandler:Landroid/os/Handler;

    .line 1311
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$12;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$12;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->photoHandler:Landroid/os/Handler;

    .line 1532
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$13;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->photoClickListener:Landroid/view/View$OnClickListener;

    .line 1552
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$14;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$14;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreDirectorsListener:Landroid/view/View$OnClickListener;

    .line 1578
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$15;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsListener:Landroid/view/View$OnClickListener;

    .line 1606
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$16;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$16;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorClickListener:Landroid/view/View$OnClickListener;

    .line 1619
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$17;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$17;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorHandler:Landroid/os/Handler;

    .line 1637
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$18;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleStateHandler:Landroid/os/Handler;

    .line 79
    return-void
.end method

.method private NetflixRemoveMenuDialog()V
    .locals 2

    .prologue
    .line 1291
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1292
    .local v0, msg:Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1293
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1294
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1295
    return-void
.end method

.method private NetflixShowExceptionDialog()V
    .locals 2

    .prologue
    .line 1284
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1285
    .local v0, msg:Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1286
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1287
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1288
    return-void
.end method

.method private ScheduleAddToQueue(ILjava/lang/String;)V
    .locals 4
    .parameter "nPosition"
    .parameter "queueType"

    .prologue
    .line 1119
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$21;

    invoke-direct {v0, p0, p1, p2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$21;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;ILjava/lang/String;)V

    .line 1150
    .local v0, netflixAddQueueTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 1151
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1153
    :cond_0
    return-void
.end method

.method private ScheduleLoadMovieTask(J)V
    .locals 3
    .parameter "delay"

    .prologue
    .line 1234
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$24;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    .line 1272
    .local v0, loadMovieTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 1273
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 1274
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v0, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1276
    :cond_0
    return-void
.end method

.method private ScheduleMoveToBottom(Ljava/lang/String;)V
    .locals 4
    .parameter "queueType"

    .prologue
    .line 1156
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$22;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$22;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Ljava/lang/String;)V

    .line 1191
    .local v0, netflixMoveToBottom:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 1192
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1194
    :cond_0
    return-void
.end method

.method private ScheduleNetflixTitleState()V
    .locals 5

    .prologue
    .line 1091
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$20;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$20;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    .line 1112
    .local v0, checkNetflixQueueTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 1113
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 1114
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v3, 0x64

    invoke-virtual {v2, v0, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1116
    :cond_0
    return-void
.end method

.method private ScheduleRemoveFromQueue(Ljava/lang/String;)V
    .locals 4
    .parameter "qtype"

    .prologue
    .line 1197
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment$23;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$23;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Ljava/lang/String;)V

    .line 1228
    .local v0, netflixDeleteQueueTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 1229
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1231
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/LockerRight;
    .locals 1
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 843
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$10(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 729
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->populateStreamingUi()V

    return-void
.end method

.method static synthetic access$11(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->populatePage()V

    return-void
.end method

.method static synthetic access$12(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    return-object v0
.end method

.method static synthetic access$13(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 1877
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$14(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 1881
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$15(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 108
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->directorsLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$16(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1708
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActorView(Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$17(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$18(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 110
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorsLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$19(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 858
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$20(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lnet/flixster/android/model/NetflixTitleState;
    .locals 1
    .parameter

    .prologue
    .line 121
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;

    return-object v0
.end method

.method static synthetic access$21(Lcom/flixster/android/activity/hc/MovieDetailsFragment;[I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 124
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I

    return-void
.end method

.method static synthetic access$22(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)[I
    .locals 1
    .parameter

    .prologue
    .line 124
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I

    return-object v0
.end method

.method static synthetic access$23(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 932
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->populateStreamingUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$24(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Lnet/flixster/android/model/NetflixTitleState;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 121
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;

    return-void
.end method

.method static synthetic access$25(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1637
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixTitleStateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$26(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1090
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleNetflixTitleState()V

    return-void
.end method

.method static synthetic access$27(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1283
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->NetflixShowExceptionDialog()V

    return-void
.end method

.method static synthetic access$28(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)J
    .locals 2
    .parameter

    .prologue
    .line 120
    iget-wide v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovieId:J

    return-wide v0
.end method

.method static synthetic access$29(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Z
    .locals 1
    .parameter

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isSeason:Z

    return v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 883
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$30(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Lnet/flixster/android/model/Movie;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 119
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    return-void
.end method

.method static synthetic access$31(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Z
    .locals 1
    .parameter

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mFromRating:Z

    return v0
.end method

.method static synthetic access$32(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Lnet/flixster/android/model/Review;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 118
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;

    return-void
.end method

.method static synthetic access$33(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1297
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->refreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$34(Lcom/flixster/android/activity/hc/MovieDetailsFragment;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1118
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleAddToQueue(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$35(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1155
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleMoveToBottom(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$36(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1196
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleRemoveFromQueue(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$37(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1290
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->NetflixRemoveMenuDialog()V

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 900
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 115
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadInitDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 920
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->delayedStreamingUiUpdate()V

    return-void
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Landroid/app/ProgressDialog;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 115
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadInitDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 803
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->canDownloadSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$9(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 823
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->errorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private delayedStreamingUiUpdate()V
    .locals 5

    .prologue
    .line 921
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 922
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 923
    iget-object v1, v0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    new-instance v2, Lcom/flixster/android/activity/hc/MovieDetailsFragment$19;

    invoke-direct {v2, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$19;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    .line 928
    const-wide/16 v3, 0x7d0

    .line 923
    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 930
    :cond_0
    return-void
.end method

.method private getActorView(Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;
    .locals 9
    .parameter "actor"
    .parameter "actorView"

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f0201da

    .line 1710
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03004d

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1711
    new-instance v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;

    invoke-direct {v8, v4}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;)V

    .line 1712
    .local v8, viewHolder:Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;
    const v1, 0x7f070028

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    .line 1713
    const v1, 0x7f07002c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->nameView:Landroid/widget/TextView;

    .line 1714
    const v1, 0x7f070104

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->charsView:Landroid/widget/TextView;

    .line 1715
    const v1, 0x7f07002b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    .line 1717
    if-eqz p1, :cond_1

    .line 1718
    iget-object v7, p1, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    .line 1719
    .local v7, name:Ljava/lang/String;
    iget-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->nameView:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1721
    iget-object v6, p1, Lnet/flixster/android/model/Actor;->chars:Ljava/lang/String;

    .line 1722
    .local v6, chars:Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 1723
    iget-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->charsView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1724
    iget-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->charsView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1727
    :cond_0
    iget-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1728
    iget-object v1, p1, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 1729
    iget-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    iget-object v2, p1, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1738
    :goto_0
    iget-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 1739
    iget-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f030017

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 1740
    iget-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1742
    .end local v6           #chars:Ljava/lang/String;
    .end local v7           #name:Ljava/lang/String;
    :cond_1
    return-object p2

    .line 1730
    .restart local v6       #chars:Ljava/lang/String;
    .restart local v7       #name:Ljava/lang/String;
    :cond_2
    iget-object v1, p1, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1731
    iget-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1732
    new-instance v0, Lnet/flixster/android/model/ImageOrder;

    const/4 v1, 0x4

    iget-object v3, p1, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    .line 1733
    iget-object v4, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorHandler:Landroid/os/Handler;

    move-object v2, p1

    .line 1732
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 1734
    .local v0, imageOrder:Lnet/flixster/android/model/ImageOrder;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v1, v0}, Lcom/flixster/android/activity/hc/Main;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_0

    .line 1736
    .end local v0           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_3
    iget-object v1, v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 1858
    const/16 v0, 0x6c

    return v0
.end method

.method private getGaTagPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1878
    iget-boolean v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isSeason:Z

    if-eqz v0, :cond_0

    const-string v0, "/tv-season"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "/movie"

    goto :goto_0
.end method

.method private getGaTitlePrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1882
    iget-boolean v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isSeason:Z

    if-eqz v0, :cond_0

    const-string v0, "TV Season "

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Movie "

    goto :goto_0
.end method

.method private loadCriticReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V
    .locals 15
    .parameter "startIndex"
    .parameter "endIndex"
    .parameter "startTagIndex"
    .parameter "ll"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 985
    .local p5, reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    move/from16 v4, p3

    .line 996
    .local v4, iTagIndex:I
    const/4 v3, 0x0

    .line 998
    .local v3, iReviewCount:I
    move/from16 v3, p1

    :goto_0
    move/from16 v0, p2

    if-lt v3, v0, :cond_0

    .line 1032
    return-void

    .line 999
    :cond_0
    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/model/Review;

    .line 1001
    .local v7, r:Lnet/flixster/android/model/Review;
    iget-object v12, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v13, 0x7f030022

    const/4 v14, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v12, v13, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 1002
    .local v5, itemView:Landroid/view/View;
    const v12, 0x7f030022

    invoke-virtual {v5, v12}, Landroid/view/View;->setId(I)V

    .line 1004
    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Landroid/view/View;->setFocusable(Z)V

    .line 1005
    const v12, 0x7f070068

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 1006
    .local v10, ri:Landroid/widget/ImageView;
    const/16 v12, 0x8

    invoke-virtual {v10, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1007
    const v12, 0x7f070065

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1008
    .local v2, bylineView:Landroid/widget/TextView;
    iget-object v12, v7, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    invoke-virtual {v2, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1009
    const v12, 0x7f070067

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 1010
    .local v11, sourceView:Landroid/widget/TextView;
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, ", "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, v7, Lnet/flixster/android/model/Review;->source:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1011
    const v12, 0x7f070069

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 1012
    .local v9, reviewView:Landroid/widget/TextView;
    const/16 v12, 0xa

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1013
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "     "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, v7, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1014
    iget v12, v7, Lnet/flixster/android/model/Review;->score:I

    const/16 v13, 0x3c

    if-ge v12, v13, :cond_1

    .line 1015
    const v12, 0x7f070066

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 1016
    .local v8, reviewIconView:Landroid/widget/ImageView;
    const v12, 0x7f0200ed

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1019
    .end local v8           #reviewIconView:Landroid/widget/ImageView;
    :cond_1
    new-instance v12, Ljava/lang/Integer;

    invoke-direct {v12, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v5, v12}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1020
    const-string v12, "FlxMain"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "MovieDetailsFragment.populatePage()  iTagIndex:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    add-int/lit8 v4, v4, 0x1

    .line 1022
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1025
    const v12, 0x7f070064

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 1026
    .local v6, mugImageView:Landroid/widget/ImageView;
    invoke-virtual {v7, v6}, Lnet/flixster/android/model/Review;->getReviewerBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1027
    .local v1, bitmap:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_2

    .line 1028
    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1030
    :cond_2
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 998
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0
.end method

.method private loadFriendReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V
    .locals 17
    .parameter "begin"
    .parameter "end"
    .parameter "start"
    .parameter "reviewLayout"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 952
    .local p5, reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    move/from16 v11, p3

    .line 954
    .local v11, tag:I
    move/from16 v5, p1

    .local v5, i:I
    :goto_0
    move/from16 v0, p2

    if-lt v5, v0, :cond_0

    .line 979
    return-void

    .line 955
    :cond_0
    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/model/Review;

    .line 956
    .local v7, review:Lnet/flixster/android/model/Review;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v13, 0x7f030022

    const/4 v14, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v12, v13, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 957
    .local v9, reviewView:Landroid/view/View;
    const v12, 0x7f030022

    invoke-virtual {v9, v12}, Landroid/view/View;->setId(I)V

    .line 958
    const/4 v12, 0x1

    invoke-virtual {v9, v12}, Landroid/view/View;->setFocusable(Z)V

    .line 959
    const v12, 0x7f070068

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 960
    .local v6, imageView:Landroid/widget/ImageView;
    sget-object v12, Lcom/flixster/android/activity/hc/Main;->RATING_SMALL_R:[I

    iget-wide v13, v7, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v15, 0x4000

    mul-double/2addr v13, v15

    double-to-int v13, v13

    aget v12, v12, v13

    invoke-virtual {v6, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 961
    const v12, 0x7f070065

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 962
    .local v1, authorView:Landroid/widget/TextView;
    iget-object v12, v7, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 963
    const v12, 0x7f070067

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 964
    .local v10, sourceView:Landroid/widget/TextView;
    const/16 v12, 0x8

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 965
    const v12, 0x7f070069

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 966
    .local v3, commentView:Landroid/widget/TextView;
    iget-object v12, v7, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 967
    const v12, 0x7f070066

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 968
    .local v8, reviewIconView:Landroid/widget/ImageView;
    const/16 v12, 0x8

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 969
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 970
    add-int/lit8 v11, v11, 0x1

    .line 971
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 972
    const v12, 0x7f070064

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 973
    .local v4, friendImage:Landroid/widget/ImageView;
    invoke-virtual {v7, v4}, Lnet/flixster/android/model/Review;->getReviewerBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 974
    .local v2, bitmap:Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1

    .line 975
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 977
    :cond_1
    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 954
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0
.end method

.method private loadUserReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V
    .locals 17
    .parameter "startIndex"
    .parameter "endIndex"
    .parameter "startTagIndex"
    .parameter "ll"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1039
    .local p5, reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    move/from16 v4, p3

    .line 1049
    .local v4, iTagIndex:I
    const/4 v3, 0x0

    .line 1051
    .local v3, iReviewCount:I
    move/from16 v3, p1

    :goto_0
    move/from16 v0, p2

    if-lt v3, v0, :cond_0

    .line 1088
    return-void

    .line 1054
    :cond_0
    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/model/Review;

    .line 1056
    .local v7, r:Lnet/flixster/android/model/Review;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v13, 0x7f030022

    const/4 v14, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v12, v13, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 1057
    .local v5, itemView:Landroid/view/View;
    const v12, 0x7f030022

    invoke-virtual {v5, v12}, Landroid/view/View;->setId(I)V

    .line 1058
    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Landroid/view/View;->setFocusable(Z)V

    .line 1060
    const v12, 0x7f070068

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 1061
    .local v10, ri:Landroid/widget/ImageView;
    sget-object v12, Lcom/flixster/android/activity/hc/Main;->RATING_SMALL_R:[I

    iget-wide v13, v7, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v15, 0x4000

    mul-double/2addr v13, v15

    double-to-int v13, v13

    aget v12, v12, v13

    invoke-virtual {v10, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1063
    const v12, 0x7f070065

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1064
    .local v2, bylineView:Landroid/widget/TextView;
    iget-object v12, v7, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    invoke-virtual {v2, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1065
    const v12, 0x7f070067

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 1067
    .local v11, sourceView:Landroid/widget/TextView;
    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1068
    const v12, 0x7f070069

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 1069
    .local v9, reviewView:Landroid/widget/TextView;
    iget-object v12, v7, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1072
    const v12, 0x7f070066

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 1073
    .local v8, reviewIconView:Landroid/widget/ImageView;
    const/16 v12, 0x8

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1075
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1077
    add-int/lit8 v4, v4, 0x1

    .line 1078
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1081
    const v12, 0x7f070064

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 1082
    .local v6, mugImageView:Landroid/widget/ImageView;
    invoke-virtual {v7, v6}, Lnet/flixster/android/model/Review;->getReviewerBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1083
    .local v1, bitmap:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_1

    .line 1084
    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1086
    :cond_1
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1051
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/MovieDetailsFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 134
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;-><init>()V

    .line 135
    .local v0, df:Lcom/flixster/android/activity/hc/MovieDetailsFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 136
    return-object v0
.end method

.method private populatePage()V
    .locals 105

    .prologue
    .line 185
    const-string v4, "FlxMain"

    const-string v6, "MovieDetailsFragment.populatePage()"

    invoke-static {v4, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    if-nez v4, :cond_1

    .line 187
    const-string v4, "FlxMain"

    const-string v6, "MovieDetailsFragment.populatePage() mMovie == null, skip return early from populatePage()"

    invoke-static {v4, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getView()Landroid/view/View;

    move-result-object v100

    .line 191
    .local v100, view:Landroid/view/View;
    if-eqz v100, :cond_0

    .line 195
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v93

    .line 196
    .local v93, title:Ljava/lang/String;
    :goto_1
    if-eqz v93, :cond_2

    .line 197
    const v4, 0x7f070190

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v71

    check-cast v71, Landroid/widget/TextView;

    .line 198
    .local v71, movieTitleView:Landroid/widget/TextView;
    move-object/from16 v0, v71

    move-object/from16 v1, v93

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    const v4, 0x7f070147

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v71

    .end local v71           #movieTitleView:Landroid/widget/TextView;
    check-cast v71, Landroid/widget/TextView;

    .line 200
    .restart local v71       #movieTitleView:Landroid/widget/TextView;
    move-object/from16 v0, v71

    move-object/from16 v1, v93

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    .end local v71           #movieTitleView:Landroid/widget/TextView;
    :cond_2
    const v4, 0x7f070155

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v77

    check-cast v77, Landroid/widget/RelativeLayout;

    .line 204
    .local v77, ratingLayout:Landroid/widget/RelativeLayout;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isSeason:Z

    if-eqz v4, :cond_10

    .line 205
    const/16 v4, 0x8

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 221
    :goto_2
    const v4, 0x7f070153

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v89

    check-cast v89, Landroid/widget/TextView;

    .line 223
    .local v89, showtimesTextView:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-boolean v4, v4, Lnet/flixster/android/model/Movie;->isMIT:Z

    if-eqz v4, :cond_12

    .line 224
    const/4 v4, 0x0

    move-object/from16 v0, v89

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 229
    :goto_3
    const v4, 0x7f070158

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v50

    check-cast v50, Landroid/widget/TextView;

    .line 230
    .local v50, getGlue:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->showGetGlue:Z

    if-eqz v4, :cond_13

    .line 231
    const/4 v4, 0x0

    move-object/from16 v0, v50

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    const/4 v4, 0x1

    move-object/from16 v0, v50

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 233
    move-object/from16 v0, v50

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    :goto_4
    const v4, 0x7f070162

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v73

    check-cast v73, Landroid/widget/LinearLayout;

    .line 239
    .local v73, photoLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f070154

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v101

    check-cast v101, Landroid/widget/TextView;

    .line 240
    .local v101, viewPhotos:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v0, v4, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    move-object/from16 v74, v0

    .line 241
    .local v74, photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    const/16 v4, 0x8

    move-object/from16 v0, v101

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 242
    if-eqz v74, :cond_19

    .line 243
    invoke-virtual/range {v74 .. v74}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 244
    const/16 v4, 0x8

    move-object/from16 v0, v101

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 290
    :goto_5
    const v4, 0x7f070145

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v59

    check-cast v59, Landroid/widget/ImageView;

    .line 291
    .local v59, iv:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v4, :cond_1b

    .line 292
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->getProfilePoster()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1a

    .line 293
    const v4, 0x7f02014f

    move-object/from16 v0, v59

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 311
    :cond_3
    :goto_6
    const v4, 0x7f070164

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->directorsLayout:Landroid/widget/LinearLayout;

    .line 312
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 313
    const v4, 0x7f070163

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/RelativeLayout;

    .line 314
    .local v43, directorsHeader:Landroid/widget/RelativeLayout;
    const/4 v4, 0x0

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 315
    const/16 v42, 0x0

    .line 316
    .local v42, directorView:Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->directorsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 317
    const/16 v52, 0x0

    .local v52, i:I
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v52

    if-ge v0, v4, :cond_4

    const/4 v4, 0x3

    move/from16 v0, v52

    if-lt v0, v4, :cond_1d

    .line 322
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_5

    .line 323
    const v4, 0x7f070165

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    .line 324
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 325
    new-instance v94, Ljava/lang/StringBuilder;

    invoke-direct/range {v94 .. v94}, Ljava/lang/StringBuilder;-><init>()V

    .line 326
    .local v94, totalBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, v94

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " total"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    .line 328
    const v6, 0x7f070167

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v67

    .line 327
    check-cast v67, Landroid/widget/TextView;

    .line 329
    .local v67, moreDirectorsLabel:Landroid/widget/TextView;
    invoke-virtual/range {v94 .. v94}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v67

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 331
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 332
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreDirectorsListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 336
    .end local v42           #directorView:Landroid/view/View;
    .end local v43           #directorsHeader:Landroid/widget/RelativeLayout;
    .end local v52           #i:I
    .end local v67           #moreDirectorsLabel:Landroid/widget/TextView;
    .end local v94           #totalBuilder:Ljava/lang/StringBuilder;
    :cond_5
    const v4, 0x7f07010b

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorsLayout:Landroid/widget/LinearLayout;

    .line 337
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    .line 338
    const v4, 0x7f070168

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/RelativeLayout;

    .line 339
    .local v33, actorsHeader:Landroid/widget/RelativeLayout;
    const/4 v4, 0x0

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 340
    const/16 v32, 0x0

    .line 341
    .local v32, actorView:Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 342
    const/16 v52, 0x0

    .restart local v52       #i:I
    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v52

    if-ge v0, v4, :cond_6

    const/4 v4, 0x3

    move/from16 v0, v52

    if-lt v0, v4, :cond_1e

    .line 347
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_7

    .line 348
    const v4, 0x7f070169

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsLayout:Landroid/widget/RelativeLayout;

    .line 349
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 350
    new-instance v94, Ljava/lang/StringBuilder;

    invoke-direct/range {v94 .. v94}, Ljava/lang/StringBuilder;-><init>()V

    .line 351
    .restart local v94       #totalBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, v94

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " total"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsLayout:Landroid/widget/RelativeLayout;

    const v6, 0x7f07016b

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v66

    check-cast v66, Landroid/widget/TextView;

    .line 353
    .local v66, moreActorsLabel:Landroid/widget/TextView;
    invoke-virtual/range {v94 .. v94}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 355
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 356
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->moreActorsListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 360
    .end local v32           #actorView:Landroid/view/View;
    .end local v33           #actorsHeader:Landroid/widget/RelativeLayout;
    .end local v52           #i:I
    .end local v66           #moreActorsLabel:Landroid/widget/TextView;
    .end local v94           #totalBuilder:Ljava/lang/StringBuilder;
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->isDetailsApiParsed()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 361
    const/4 v4, 0x6

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v75, v0

    const/4 v4, 0x0

    const-string v6, "genre"

    aput-object v6, v75, v4

    const/4 v4, 0x1

    const-string v6, "runningTime"

    aput-object v6, v75, v4

    const/4 v4, 0x2

    const-string v6, "theaterReleaseDate"

    aput-object v6, v75, v4

    const/4 v4, 0x3

    .line 362
    const-string v6, "dvdReleaseDate"

    aput-object v6, v75, v4

    const/4 v4, 0x4

    const-string v6, "mpaa"

    aput-object v6, v75, v4

    const/4 v4, 0x5

    const-string v6, "synopsis"

    aput-object v6, v75, v4

    .line 363
    .local v75, propArray:[Ljava/lang/String;
    const/4 v4, 0x6

    new-array v0, v4, [I

    move-object/from16 v92, v0

    fill-array-data v92, :array_0

    .line 366
    .local v92, textViewIdArray:[I
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v79

    .line 367
    .local v79, res:Landroid/content/res/Resources;
    const/4 v4, 0x6

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v60, v0

    const/4 v4, 0x0

    const v6, 0x7f0c0041

    move-object/from16 v0, v79

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v60, v4

    const/4 v4, 0x1

    const v6, 0x7f0c0023

    move-object/from16 v0, v79

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v60, v4

    const/4 v4, 0x2

    .line 368
    const v6, 0x7f0c0024

    move-object/from16 v0, v79

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v60, v4

    const/4 v4, 0x3

    const v6, 0x7f0c0028

    move-object/from16 v0, v79

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v60, v4

    const/4 v4, 0x4

    .line 369
    const v6, 0x7f0c0022

    move-object/from16 v0, v79

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v60, v4

    const/4 v4, 0x5

    const v6, 0x7f0c0021

    move-object/from16 v0, v79

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v60, v4

    .line 371
    .local v60, labelArray:[Ljava/lang/String;
    move-object/from16 v0, v75

    array-length v0, v0

    move/from16 v61, v0

    .line 374
    .local v61, length:I
    const/16 v52, 0x0

    .restart local v52       #i:I
    :goto_9
    move/from16 v0, v52

    move/from16 v1, v61

    if-lt v0, v1, :cond_1f

    .line 395
    .end local v52           #i:I
    .end local v60           #labelArray:[Ljava/lang/String;
    .end local v61           #length:I
    .end local v75           #propArray:[Ljava/lang/String;
    .end local v79           #res:Landroid/content/res/Resources;
    .end local v92           #textViewIdArray:[I
    :cond_8
    const v4, 0x7f070149

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v84

    check-cast v84, Landroid/widget/TextView;

    .line 396
    .local v84, rtScoreline:Landroid/widget/TextView;
    const v4, 0x7f07017b

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v40

    check-cast v40, Landroid/widget/ImageView;

    .line 397
    .local v40, criticsImage:Landroid/widget/ImageView;
    const v4, 0x7f07017a

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v82

    check-cast v82, Landroid/widget/TextView;

    .line 398
    .local v82, rottenScore:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "rottenTomatoes"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 399
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "rottenTomatoes"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v54

    .line 401
    .local v54, iRS:I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v54 .. v54}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f0c0162

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v84

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 404
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v54 .. v54}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "%"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v82

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    const/16 v4, 0x3c

    move/from16 v0, v54

    if-ge v0, v4, :cond_9

    .line 408
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0200ed

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v81

    .line 409
    .local v81, rotten:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v81 .. v81}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual/range {v81 .. v81}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    move-object/from16 v0, v81

    invoke-virtual {v0, v4, v6, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 410
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v84

    move-object/from16 v1, v81

    invoke-virtual {v0, v1, v4, v6, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 413
    const v4, 0x7f0200ec

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 421
    .end local v54           #iRS:I
    .end local v81           #rotten:Landroid/graphics/drawable/Drawable;
    :cond_9
    :goto_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v4

    if-eqz v4, :cond_22

    .line 422
    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v6}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-nez v4, :cond_22

    const/16 v57, 0x0

    .line 424
    .local v57, isReleased:Z
    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "popcornScore"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 425
    const v4, 0x7f070148

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v86

    check-cast v86, Landroid/widget/TextView;

    .line 426
    .local v86, scoreTextView:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "popcornScore"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v53

    .line 427
    .local v53, iFS:I
    const/16 v4, 0x3c

    move/from16 v0, v53

    if-ge v0, v4, :cond_23

    const/16 v58, 0x1

    .line 430
    .local v58, isSpilled:Z
    :goto_c
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0163

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v62

    .line 431
    .local v62, liked:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0164

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v104

    .line 432
    .local v104, wts:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v57, :cond_24

    .end local v62           #liked:Ljava/lang/String;
    :goto_d
    move-object/from16 v0, v62

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v86

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 433
    if-nez v57, :cond_25

    const v55, 0x7f0200f6

    .line 435
    .local v55, iconId:I
    :goto_e
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move/from16 v0, v55

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v85

    .line 436
    .local v85, scoreIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v85 .. v85}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual/range {v85 .. v85}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    move-object/from16 v0, v85

    invoke-virtual {v0, v4, v6, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 437
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v86

    move-object/from16 v1, v85

    invoke-virtual {v0, v1, v4, v6, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 440
    const v4, 0x7f070183

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v86

    .end local v86           #scoreTextView:Landroid/widget/TextView;
    check-cast v86, Landroid/widget/TextView;

    .line 441
    .restart local v86       #scoreTextView:Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "%"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v86

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 442
    const v4, 0x7f070184

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v51

    check-cast v51, Landroid/widget/ImageView;

    .line 443
    .local v51, headerIcon:Landroid/widget/ImageView;
    if-nez v57, :cond_27

    const v4, 0x7f0200f5

    :goto_f
    move-object/from16 v0, v51

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 452
    .end local v51           #headerIcon:Landroid/widget/ImageView;
    .end local v53           #iFS:I
    .end local v55           #iconId:I
    .end local v58           #isSpilled:Z
    .end local v85           #scoreIcon:Landroid/graphics/drawable/Drawable;
    .end local v104           #wts:Ljava/lang/String;
    :goto_10
    const v4, 0x7f07014a

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v45

    check-cast v45, Landroid/widget/TextView;

    .line 453
    .local v45, friendScore:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_2b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2b

    .line 454
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v46

    .line 455
    .local v46, friendsRatedCount:I
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0200eb

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v80

    .line 456
    .local v80, reviewIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v80 .. v80}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual/range {v80 .. v80}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    move-object/from16 v0, v80

    invoke-virtual {v0, v4, v6, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 457
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v45

    move-object/from16 v1, v80

    invoke-virtual {v0, v1, v4, v6, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 458
    new-instance v76, Ljava/lang/StringBuilder;

    invoke-direct/range {v76 .. v76}, Ljava/lang/StringBuilder;-><init>()V

    .line 459
    .local v76, ratingCountBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, v76

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 460
    const/4 v4, 0x1

    move/from16 v0, v46

    if-le v0, v4, :cond_2a

    .line 461
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0090

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v76

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    :goto_11
    invoke-virtual/range {v76 .. v76}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v45

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 466
    const/4 v4, 0x0

    move-object/from16 v0, v45

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 484
    .end local v46           #friendsRatedCount:I
    .end local v76           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v80           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :goto_12
    const v4, 0x7f07014b

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/TextView;

    .line 485
    .local v34, actorsShort:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2e

    .line 486
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 487
    const/4 v4, 0x0

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 492
    :goto_13
    const v4, 0x7f07014c

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v65

    check-cast v65, Landroid/widget/TextView;

    .line 493
    .local v65, metaLine:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "meta"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 494
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "meta"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v65

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 495
    const/4 v4, 0x0

    move-object/from16 v0, v65

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 506
    :goto_14
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v4

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v4, :cond_a

    .line 507
    invoke-direct/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->populateStreamingUi()V

    .line 510
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->populateSeasonUi()V

    .line 512
    const/16 v87, 0x0

    .line 514
    .local v87, showMovieWebsitesHeaderFlag:Z
    const v4, 0x7f07018b

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v44

    check-cast v44, Landroid/widget/TextView;

    .line 515
    .local v44, flixsterPanel:Landroid/widget/TextView;
    if-eqz v87, :cond_30

    .line 516
    const/16 v87, 0x1

    .line 517
    const/4 v4, 0x1

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 518
    const/4 v4, 0x1

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 519
    move-object/from16 v0, v44

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 520
    const/4 v4, 0x0

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 527
    :goto_15
    const v4, 0x7f07018c

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v83

    check-cast v83, Landroid/widget/TextView;

    .line 528
    .local v83, rottentomatoesPanel:Landroid/widget/TextView;
    if-eqz v87, :cond_31

    .line 529
    const/16 v87, 0x1

    .line 530
    const/4 v4, 0x1

    move-object/from16 v0, v83

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 531
    const/4 v4, 0x1

    move-object/from16 v0, v83

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 532
    move-object/from16 v0, v83

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 533
    const/4 v4, 0x0

    move-object/from16 v0, v83

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 539
    :goto_16
    const v4, 0x7f07018d

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v56

    check-cast v56, Landroid/widget/TextView;

    .line 540
    .local v56, imdbTextView:Landroid/widget/TextView;
    if-eqz v87, :cond_32

    .line 541
    const/16 v87, 0x1

    .line 542
    const/4 v4, 0x1

    move-object/from16 v0, v56

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 543
    const/4 v4, 0x1

    move-object/from16 v0, v56

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 544
    move-object/from16 v0, v56

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 545
    const/4 v4, 0x0

    move-object/from16 v0, v56

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 552
    :goto_17
    const v4, 0x7f07018a

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v63

    check-cast v63, Landroid/widget/TextView;

    .line 553
    .local v63, linksHeader:Landroid/widget/TextView;
    if-eqz v87, :cond_33

    .line 554
    const/4 v4, 0x0

    move-object/from16 v0, v63

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 560
    :goto_18
    const v4, 0x7f070178

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/RelativeLayout;

    .line 561
    .local v37, criticHeader:Landroid/widget/RelativeLayout;
    const v4, 0x7f07016c

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v49

    check-cast v49, Landroid/widget/RelativeLayout;

    .line 562
    .local v49, friendsWantToSeeHeader:Landroid/widget/RelativeLayout;
    const v4, 0x7f07016d

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 563
    .local v12, friendsWantToSeeLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f07016e

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v69

    check-cast v69, Landroid/widget/LinearLayout;

    .line 564
    .local v69, moreFriendsWantToSeeLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f070172

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v47

    check-cast v47, Landroid/widget/RelativeLayout;

    .line 565
    .local v47, friendsRatingsHeader:Landroid/widget/RelativeLayout;
    const v4, 0x7f070173

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    .line 566
    .local v18, friendsRatingsLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f070174

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v68

    check-cast v68, Landroid/widget/LinearLayout;

    .line 567
    .local v68, moreFriendsRatingsLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f070181

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v98

    check-cast v98, Landroid/widget/RelativeLayout;

    .line 568
    .local v98, userReviewHeader:Landroid/widget/RelativeLayout;
    const v4, 0x7f070185

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/LinearLayout;

    .line 569
    .local v23, userReviewLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f070186

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v99

    check-cast v99, Landroid/widget/LinearLayout;

    .line 570
    .local v99, userReviewMoreLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f07017c

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/LinearLayout;

    .line 571
    .local v29, criticLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f07017d

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/LinearLayout;

    .line 572
    .local v38, criticLayoutMore:Landroid/widget/LinearLayout;
    const/4 v11, 0x0

    .line 574
    .local v11, iTagIndex:I
    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-nez v4, :cond_b

    .line 575
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v13

    .line 576
    .local v13, reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    if-eqz v13, :cond_b

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_b

    .line 577
    const/4 v4, 0x0

    invoke-virtual {v12, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 578
    const/16 v4, 0x8

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 579
    const/4 v4, 0x0

    move-object/from16 v0, v49

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 580
    const/4 v9, 0x0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v10

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v13}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->loadFriendReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 582
    const v4, 0x7f07016f

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v70

    check-cast v70, Landroid/widget/RelativeLayout;

    .line 583
    .local v70, moreReviewsLayout:Landroid/widget/RelativeLayout;
    const/16 v4, 0x8

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 584
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_34

    .line 585
    const v4, 0x7f070171

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v95

    check-cast v95, Landroid/widget/TextView;

    .line 586
    .local v95, totalView:Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " friends total"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v95

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    move-object/from16 v0, v70

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588
    const/4 v4, 0x1

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 589
    const/4 v4, 0x0

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 593
    .end local v95           #totalView:Landroid/widget/TextView;
    :goto_19
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/2addr v11, v4

    .line 597
    .end local v13           #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .end local v70           #moreReviewsLayout:Landroid/widget/RelativeLayout;
    :cond_b
    invoke-virtual/range {v18 .. v18}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-nez v4, :cond_c

    .line 598
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v13

    .line 599
    .restart local v13       #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    if-eqz v13, :cond_c

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_c

    .line 600
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 601
    const/16 v4, 0x8

    move-object/from16 v0, v68

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 602
    const/4 v4, 0x0

    move-object/from16 v0, v47

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 603
    const/4 v15, 0x0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v16

    move-object/from16 v14, p0

    move/from16 v17, v11

    move-object/from16 v19, v13

    invoke-direct/range {v14 .. v19}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->loadFriendReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 606
    const v4, 0x7f070175

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v70

    .line 605
    check-cast v70, Landroid/widget/RelativeLayout;

    .line 607
    .restart local v70       #moreReviewsLayout:Landroid/widget/RelativeLayout;
    const/16 v4, 0x8

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 608
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_35

    .line 609
    const v4, 0x7f070177

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v95

    check-cast v95, Landroid/widget/TextView;

    .line 610
    .restart local v95       #totalView:Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " reviews total"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v95

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 611
    move-object/from16 v0, v70

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 612
    const/4 v4, 0x1

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 613
    const/4 v4, 0x0

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 617
    .end local v95           #totalView:Landroid/widget/TextView;
    :goto_1a
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/2addr v11, v4

    .line 621
    .end local v13           #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .end local v70           #moreReviewsLayout:Landroid/widget/RelativeLayout;
    :cond_c
    invoke-virtual/range {v23 .. v23}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-nez v4, :cond_d

    .line 622
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getUserReviewsList()Ljava/util/ArrayList;

    move-result-object v24

    .line 623
    .local v24, reviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    if-eqz v24, :cond_37

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_37

    .line 624
    const/4 v4, 0x0

    move-object/from16 v0, v98

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 625
    const/4 v4, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 626
    const/16 v4, 0x8

    move-object/from16 v0, v99

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 628
    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 629
    const/16 v20, 0x0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v21

    move-object/from16 v19, p0

    move/from16 v22, v11

    invoke-direct/range {v19 .. v24}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->loadUserReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 632
    const v4, 0x7f070187

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v97

    check-cast v97, Landroid/widget/RelativeLayout;

    .line 633
    .local v97, userLoadMorePanel:Landroid/widget/RelativeLayout;
    const/16 v4, 0x8

    move-object/from16 v0, v97

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 635
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_36

    .line 636
    const v4, 0x7f070189

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v64

    check-cast v64, Landroid/widget/TextView;

    .line 637
    .local v64, loadCount:Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " reviews total"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 638
    move-object/from16 v0, v97

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 639
    const/4 v4, 0x1

    move-object/from16 v0, v97

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 640
    const/4 v4, 0x0

    move-object/from16 v0, v97

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 644
    .end local v64           #loadCount:Landroid/widget/TextView;
    :goto_1b
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/2addr v11, v4

    .line 652
    .end local v24           #reviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .end local v97           #userLoadMorePanel:Landroid/widget/RelativeLayout;
    :cond_d
    :goto_1c
    const v4, 0x7f07017e

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v39

    check-cast v39, Landroid/widget/RelativeLayout;

    .line 653
    .local v39, criticLoadMoreLayout:Landroid/widget/RelativeLayout;
    invoke-virtual/range {v29 .. v29}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-nez v4, :cond_e

    .line 654
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getCriticReviewsList()Ljava/util/ArrayList;

    move-result-object v30

    .line 655
    .local v30, criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    if-eqz v30, :cond_39

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_39

    .line 656
    const/4 v4, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 657
    const/16 v4, 0x8

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 658
    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 659
    const/16 v26, 0x0

    const/4 v4, 0x3

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v27

    move-object/from16 v25, p0

    move/from16 v28, v11

    invoke-direct/range {v25 .. v30}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->loadCriticReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 662
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_38

    .line 663
    const v4, 0x7f070180

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v64

    check-cast v64, Landroid/widget/TextView;

    .line 664
    .restart local v64       #loadCount:Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " reviews total"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 665
    move-object/from16 v0, v39

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 666
    const/4 v4, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 667
    const/4 v4, 0x0

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 680
    .end local v30           #criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .end local v64           #loadCount:Landroid/widget/TextView;
    :cond_e
    :goto_1d
    const v4, 0x7f070152

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v96

    check-cast v96, Landroid/widget/TextView;

    .line 681
    .local v96, trailerPanel:Landroid/widget/TextView;
    const v4, 0x7f07014f

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v103

    check-cast v103, Landroid/widget/TextView;

    .line 682
    .local v103, watchNow:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->hasTrailer()Z

    move-result v4

    if-eqz v4, :cond_3a

    invoke-virtual/range {v103 .. v103}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_3a

    .line 683
    const/4 v4, 0x0

    move-object/from16 v0, v96

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 684
    const/4 v4, 0x1

    move-object/from16 v0, v96

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 685
    move-object/from16 v0, v96

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 690
    :goto_1e
    const v4, 0x7f070153

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v88

    check-cast v88, Landroid/widget/TextView;

    .line 691
    .local v88, showtimesPanel:Landroid/widget/TextView;
    const/4 v4, 0x1

    move-object/from16 v0, v88

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 692
    move-object/from16 v0, v88

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 695
    const v4, 0x7f070159

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v72

    check-cast v72, Landroid/widget/TextView;

    .line 696
    .local v72, netflixText:Landroid/widget/TextView;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3b

    .line 698
    move-object/from16 v0, v72

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 699
    const/4 v4, 0x1

    move-object/from16 v0, v72

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    goto/16 :goto_0

    .line 195
    .end local v11           #iTagIndex:I
    .end local v12           #friendsWantToSeeLayout:Landroid/widget/LinearLayout;
    .end local v18           #friendsRatingsLayout:Landroid/widget/LinearLayout;
    .end local v23           #userReviewLayout:Landroid/widget/LinearLayout;
    .end local v29           #criticLayout:Landroid/widget/LinearLayout;
    .end local v34           #actorsShort:Landroid/widget/TextView;
    .end local v37           #criticHeader:Landroid/widget/RelativeLayout;
    .end local v38           #criticLayoutMore:Landroid/widget/LinearLayout;
    .end local v39           #criticLoadMoreLayout:Landroid/widget/RelativeLayout;
    .end local v40           #criticsImage:Landroid/widget/ImageView;
    .end local v44           #flixsterPanel:Landroid/widget/TextView;
    .end local v45           #friendScore:Landroid/widget/TextView;
    .end local v47           #friendsRatingsHeader:Landroid/widget/RelativeLayout;
    .end local v49           #friendsWantToSeeHeader:Landroid/widget/RelativeLayout;
    .end local v50           #getGlue:Landroid/widget/TextView;
    .end local v56           #imdbTextView:Landroid/widget/TextView;
    .end local v57           #isReleased:Z
    .end local v59           #iv:Landroid/widget/ImageView;
    .end local v63           #linksHeader:Landroid/widget/TextView;
    .end local v65           #metaLine:Landroid/widget/TextView;
    .end local v68           #moreFriendsRatingsLayout:Landroid/widget/LinearLayout;
    .end local v69           #moreFriendsWantToSeeLayout:Landroid/widget/LinearLayout;
    .end local v72           #netflixText:Landroid/widget/TextView;
    .end local v73           #photoLayout:Landroid/widget/LinearLayout;
    .end local v74           #photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    .end local v77           #ratingLayout:Landroid/widget/RelativeLayout;
    .end local v82           #rottenScore:Landroid/widget/TextView;
    .end local v83           #rottentomatoesPanel:Landroid/widget/TextView;
    .end local v84           #rtScoreline:Landroid/widget/TextView;
    .end local v86           #scoreTextView:Landroid/widget/TextView;
    .end local v87           #showMovieWebsitesHeaderFlag:Z
    .end local v88           #showtimesPanel:Landroid/widget/TextView;
    .end local v89           #showtimesTextView:Landroid/widget/TextView;
    .end local v93           #title:Ljava/lang/String;
    .end local v96           #trailerPanel:Landroid/widget/TextView;
    .end local v98           #userReviewHeader:Landroid/widget/RelativeLayout;
    .end local v99           #userReviewMoreLayout:Landroid/widget/LinearLayout;
    .end local v101           #viewPhotos:Landroid/widget/TextView;
    .end local v103           #watchNow:Landroid/widget/TextView;
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v93

    goto/16 :goto_1

    .line 207
    .restart local v77       #ratingLayout:Landroid/widget/RelativeLayout;
    .restart local v93       #title:Ljava/lang/String;
    :cond_10
    const v4, 0x7f070156

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v78

    check-cast v78, Landroid/widget/TextView;

    .line 208
    .local v78, ratingView:Landroid/widget/TextView;
    const/4 v4, 0x1

    move-object/from16 v0, v78

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 209
    move-object/from16 v0, v78

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    const v4, 0x7f070157

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v59

    check-cast v59, Landroid/widget/ImageView;

    .line 211
    .restart local v59       #iv:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;

    if-eqz v4, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;

    iget-wide v8, v4, Lnet/flixster/android/model/Review;->stars:D

    const-wide/16 v14, 0x0

    cmpl-double v4, v8, v14

    if-eqz v4, :cond_11

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_11

    .line 212
    const/4 v4, 0x0

    move-object/from16 v0, v59

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 213
    sget-object v4, Lcom/flixster/android/activity/hc/Main;->RATING_SMALL_R:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;

    iget-wide v8, v6, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v14, 0x4000

    mul-double/2addr v8, v14

    double-to-int v6, v8

    aget v4, v4, v6

    move-object/from16 v0, v59

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 214
    const v4, 0x7f0c003f

    move-object/from16 v0, v78

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 216
    :cond_11
    const/16 v4, 0x8

    move-object/from16 v0, v59

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 217
    const v4, 0x7f0c003e

    move-object/from16 v0, v78

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 226
    .end local v59           #iv:Landroid/widget/ImageView;
    .end local v78           #ratingView:Landroid/widget/TextView;
    .restart local v89       #showtimesTextView:Landroid/widget/TextView;
    :cond_12
    const/16 v4, 0x8

    move-object/from16 v0, v89

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 235
    .restart local v50       #getGlue:Landroid/widget/TextView;
    :cond_13
    const/16 v4, 0x8

    move-object/from16 v0, v50

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 247
    .restart local v73       #photoLayout:Landroid/widget/LinearLayout;
    .restart local v74       #photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    .restart local v101       #viewPhotos:Landroid/widget/TextView;
    :cond_14
    invoke-virtual/range {v73 .. v73}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 248
    const/16 v52, 0x0

    .restart local v52       #i:I
    :goto_1f
    invoke-virtual/range {v74 .. v74}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v52

    if-ge v0, v4, :cond_15

    const/16 v4, 0xf

    move/from16 v0, v52

    if-lt v0, v4, :cond_16

    .line 272
    :cond_15
    const/4 v4, 0x0

    move-object/from16 v0, v73

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_5

    .line 249
    :cond_16
    move-object/from16 v0, v74

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Photo;

    .line 250
    .local v5, photo:Lnet/flixster/android/model/Photo;
    new-instance v7, Landroid/widget/ImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v7, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 251
    .local v7, photoView:Landroid/widget/ImageView;
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 252
    const v8, 0x7f0a0037

    .line 251
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    .line 252
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 253
    const v9, 0x7f0a0037

    .line 252
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    invoke-direct {v4, v6, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 251
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 254
    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f0a002c

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    .line 255
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a002b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    const/4 v9, 0x0

    .line 254
    invoke-virtual {v7, v4, v6, v8, v9}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 256
    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 257
    iget-object v4, v5, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_18

    .line 258
    iget-object v4, v5, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 267
    :cond_17
    :goto_20
    const/4 v4, 0x1

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 268
    const/4 v4, 0x1

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 269
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->photoClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    move-object/from16 v0, v73

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 248
    add-int/lit8 v52, v52, 0x1

    goto/16 :goto_1f

    .line 260
    :cond_18
    const v4, 0x7f020151

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 261
    iget-object v4, v5, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    if-eqz v4, :cond_17

    iget-object v4, v5, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    const-string v6, "http://"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 262
    new-instance v3, Lnet/flixster/android/model/ImageOrder;

    const/4 v4, 0x3

    .line 263
    iget-object v6, v5, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->photoHandler:Landroid/os/Handler;

    .line 262
    invoke-direct/range {v3 .. v8}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 264
    .local v3, imageOrder:Lnet/flixster/android/model/ImageOrder;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v4, v3}, Lcom/flixster/android/activity/hc/Main;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_20

    .line 286
    .end local v3           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .end local v5           #photo:Lnet/flixster/android/model/Photo;
    .end local v7           #photoView:Landroid/widget/ImageView;
    .end local v52           #i:I
    :cond_19
    const/16 v4, 0x8

    move-object/from16 v0, v73

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 287
    const/16 v4, 0x8

    move-object/from16 v0, v101

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 295
    .restart local v59       #iv:Landroid/widget/ImageView;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    move-object/from16 v0, v59

    invoke-virtual {v4, v0}, Lnet/flixster/android/model/LockerRight;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v35

    .line 296
    .local v35, bitmap:Landroid/graphics/Bitmap;
    if-eqz v35, :cond_3

    .line 297
    move-object/from16 v0, v59

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_6

    .line 301
    .end local v35           #bitmap:Landroid/graphics/Bitmap;
    :cond_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getThumbnailPoster()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1c

    .line 302
    const v4, 0x7f02014f

    move-object/from16 v0, v59

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_6

    .line 304
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    move-object/from16 v0, v59

    invoke-virtual {v4, v0}, Lnet/flixster/android/model/Movie;->getThumbnailBackedProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v35

    .line 305
    .restart local v35       #bitmap:Landroid/graphics/Bitmap;
    if-eqz v35, :cond_3

    .line 306
    move-object/from16 v0, v59

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_6

    .line 318
    .end local v35           #bitmap:Landroid/graphics/Bitmap;
    .restart local v42       #directorView:Landroid/view/View;
    .restart local v43       #directorsHeader:Landroid/widget/RelativeLayout;
    .restart local v52       #i:I
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    move/from16 v0, v52

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lnet/flixster/android/model/Actor;

    .line 319
    .local v41, director:Lnet/flixster/android/model/Actor;
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    move-object/from16 v2, v42

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActorView(Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;

    move-result-object v42

    .line 320
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->directorsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, v42

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 317
    add-int/lit8 v52, v52, 0x1

    goto/16 :goto_7

    .line 343
    .end local v41           #director:Lnet/flixster/android/model/Actor;
    .end local v42           #directorView:Landroid/view/View;
    .end local v43           #directorsHeader:Landroid/widget/RelativeLayout;
    .restart local v32       #actorView:Landroid/view/View;
    .restart local v33       #actorsHeader:Landroid/widget/RelativeLayout;
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    move/from16 v0, v52

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lnet/flixster/android/model/Actor;

    .line 344
    .local v31, actor:Lnet/flixster/android/model/Actor;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActorView(Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;

    move-result-object v32

    .line 345
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->actorsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 342
    add-int/lit8 v52, v52, 0x1

    goto/16 :goto_8

    .line 375
    .end local v31           #actor:Lnet/flixster/android/model/Actor;
    .end local v32           #actorView:Landroid/view/View;
    .end local v33           #actorsHeader:Landroid/widget/RelativeLayout;
    .restart local v60       #labelArray:[Ljava/lang/String;
    .restart local v61       #length:I
    .restart local v75       #propArray:[Ljava/lang/String;
    .restart local v79       #res:Landroid/content/res/Resources;
    .restart local v92       #textViewIdArray:[I
    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    aget-object v6, v75, v52

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 377
    .local v36, content:Ljava/lang/String;
    aget v4, v92, v52

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v91

    check-cast v91, Landroid/widget/TextView;

    .line 378
    .local v91, textView:Landroid/widget/TextView;
    if-eqz v36, :cond_20

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_20

    .line 381
    new-instance v4, Ljava/lang/StringBuilder;

    aget-object v6, v60, v52

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    move-object/from16 v0, v91

    invoke-virtual {v0, v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 383
    invoke-virtual/range {v91 .. v91}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v90

    check-cast v90, Landroid/text/Spannable;

    .line 384
    .local v90, spannableText:Landroid/text/Spannable;
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v6, 0x1

    invoke-direct {v4, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v6, 0x0

    aget-object v8, v60, v52

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    .line 385
    const/16 v9, 0x21

    .line 384
    move-object/from16 v0, v90

    invoke-interface {v0, v4, v6, v8, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 387
    const/4 v4, 0x0

    move-object/from16 v0, v91

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 374
    .end local v90           #spannableText:Landroid/text/Spannable;
    :goto_21
    add-int/lit8 v52, v52, 0x1

    goto/16 :goto_9

    .line 389
    :cond_20
    const/16 v4, 0x8

    move-object/from16 v0, v91

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_21

    .line 416
    .end local v36           #content:Ljava/lang/String;
    .end local v52           #i:I
    .end local v60           #labelArray:[Ljava/lang/String;
    .end local v61           #length:I
    .end local v75           #propArray:[Ljava/lang/String;
    .end local v79           #res:Landroid/content/res/Resources;
    .end local v91           #textView:Landroid/widget/TextView;
    .end local v92           #textViewIdArray:[I
    .restart local v40       #criticsImage:Landroid/widget/ImageView;
    .restart local v82       #rottenScore:Landroid/widget/TextView;
    .restart local v84       #rtScoreline:Landroid/widget/TextView;
    :cond_21
    const/16 v4, 0x8

    move-object/from16 v0, v84

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 417
    const/16 v4, 0x8

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 418
    const/16 v4, 0x8

    move-object/from16 v0, v82

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_a

    .line 422
    :cond_22
    const/16 v57, 0x1

    goto/16 :goto_b

    .line 427
    .restart local v53       #iFS:I
    .restart local v57       #isReleased:Z
    .restart local v86       #scoreTextView:Landroid/widget/TextView;
    :cond_23
    const/16 v58, 0x0

    goto/16 :goto_c

    .restart local v58       #isSpilled:Z
    .restart local v62       #liked:Ljava/lang/String;
    .restart local v104       #wts:Ljava/lang/String;
    :cond_24
    move-object/from16 v62, v104

    .line 432
    goto/16 :goto_d

    .line 433
    .end local v62           #liked:Ljava/lang/String;
    :cond_25
    if-eqz v58, :cond_26

    const v55, 0x7f0200ef

    goto/16 :goto_e

    .line 434
    :cond_26
    const v55, 0x7f0200ea

    goto/16 :goto_e

    .line 443
    .restart local v51       #headerIcon:Landroid/widget/ImageView;
    .restart local v55       #iconId:I
    .restart local v85       #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_27
    if-eqz v58, :cond_28

    const v4, 0x7f0200ee

    goto/16 :goto_f

    .line 444
    :cond_28
    const v4, 0x7f0200e9

    goto/16 :goto_f

    .line 446
    .end local v51           #headerIcon:Landroid/widget/ImageView;
    .end local v53           #iFS:I
    .end local v55           #iconId:I
    .end local v58           #isSpilled:Z
    .end local v85           #scoreIcon:Landroid/graphics/drawable/Drawable;
    .end local v86           #scoreTextView:Landroid/widget/TextView;
    .end local v104           #wts:Ljava/lang/String;
    :cond_29
    const v4, 0x7f070148

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v86

    check-cast v86, Landroid/widget/TextView;

    .line 447
    .restart local v86       #scoreTextView:Landroid/widget/TextView;
    const/16 v4, 0x8

    move-object/from16 v0, v86

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 448
    const v4, 0x7f070183

    move-object/from16 v0, v100

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v86

    .end local v86           #scoreTextView:Landroid/widget/TextView;
    check-cast v86, Landroid/widget/TextView;

    .line 449
    .restart local v86       #scoreTextView:Landroid/widget/TextView;
    const/4 v4, 0x4

    move-object/from16 v0, v86

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_10

    .line 463
    .restart local v45       #friendScore:Landroid/widget/TextView;
    .restart local v46       #friendsRatedCount:I
    .restart local v76       #ratingCountBuilder:Ljava/lang/StringBuilder;
    .restart local v80       #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_2a
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c008f

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v76

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_11

    .line 467
    .end local v46           #friendsRatedCount:I
    .end local v76           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v80           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_2b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_2d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2d

    .line 468
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v48

    .line 469
    .local v48, friendsWantToSeeCount:I
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0200f7

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v102

    .line 470
    .local v102, wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v102 .. v102}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual/range {v102 .. v102}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    move-object/from16 v0, v102

    invoke-virtual {v0, v4, v6, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 471
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v45

    move-object/from16 v1, v102

    invoke-virtual {v0, v1, v4, v6, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 472
    const/16 v104, 0x0

    .line 473
    .restart local v104       #wts:Ljava/lang/String;
    const/4 v4, 0x1

    move/from16 v0, v48

    if-le v0, v4, :cond_2c

    .line 474
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0166

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v104

    .line 478
    :goto_22
    move-object/from16 v0, v45

    move-object/from16 v1, v104

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 479
    const/4 v4, 0x0

    move-object/from16 v0, v45

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_12

    .line 476
    :cond_2c
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0165

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v104

    goto :goto_22

    .line 481
    .end local v48           #friendsWantToSeeCount:I
    .end local v102           #wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    .end local v104           #wts:Ljava/lang/String;
    :cond_2d
    const/16 v4, 0x8

    move-object/from16 v0, v45

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_12

    .line 489
    .restart local v34       #actorsShort:Landroid/widget/TextView;
    :cond_2e
    const/16 v4, 0x8

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_13

    .line 497
    .restart local v65       #metaLine:Landroid/widget/TextView;
    :cond_2f
    const/16 v4, 0x8

    move-object/from16 v0, v65

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_14

    .line 524
    .restart local v44       #flixsterPanel:Landroid/widget/TextView;
    .restart local v87       #showMovieWebsitesHeaderFlag:Z
    :cond_30
    const/16 v4, 0x8

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_15

    .line 535
    .restart local v83       #rottentomatoesPanel:Landroid/widget/TextView;
    :cond_31
    const/16 v4, 0x8

    move-object/from16 v0, v83

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_16

    .line 547
    .restart local v56       #imdbTextView:Landroid/widget/TextView;
    :cond_32
    const/16 v4, 0x8

    move-object/from16 v0, v56

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_17

    .line 556
    .restart local v63       #linksHeader:Landroid/widget/TextView;
    :cond_33
    const/16 v4, 0x8

    move-object/from16 v0, v63

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_18

    .line 591
    .restart local v11       #iTagIndex:I
    .restart local v12       #friendsWantToSeeLayout:Landroid/widget/LinearLayout;
    .restart local v13       #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .restart local v18       #friendsRatingsLayout:Landroid/widget/LinearLayout;
    .restart local v23       #userReviewLayout:Landroid/widget/LinearLayout;
    .restart local v29       #criticLayout:Landroid/widget/LinearLayout;
    .restart local v37       #criticHeader:Landroid/widget/RelativeLayout;
    .restart local v38       #criticLayoutMore:Landroid/widget/LinearLayout;
    .restart local v47       #friendsRatingsHeader:Landroid/widget/RelativeLayout;
    .restart local v49       #friendsWantToSeeHeader:Landroid/widget/RelativeLayout;
    .restart local v68       #moreFriendsRatingsLayout:Landroid/widget/LinearLayout;
    .restart local v69       #moreFriendsWantToSeeLayout:Landroid/widget/LinearLayout;
    .restart local v70       #moreReviewsLayout:Landroid/widget/RelativeLayout;
    .restart local v98       #userReviewHeader:Landroid/widget/RelativeLayout;
    .restart local v99       #userReviewMoreLayout:Landroid/widget/LinearLayout;
    :cond_34
    const/16 v4, 0x8

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_19

    .line 615
    :cond_35
    const/16 v4, 0x8

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1a

    .line 642
    .end local v13           #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .end local v70           #moreReviewsLayout:Landroid/widget/RelativeLayout;
    .restart local v24       #reviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .restart local v97       #userLoadMorePanel:Landroid/widget/RelativeLayout;
    :cond_36
    const/16 v4, 0x8

    move-object/from16 v0, v97

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1b

    .line 646
    .end local v97           #userLoadMorePanel:Landroid/widget/RelativeLayout;
    :cond_37
    const/16 v4, 0x8

    move-object/from16 v0, v98

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 647
    const/16 v4, 0x8

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1c

    .line 669
    .end local v24           #reviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .restart local v30       #criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .restart local v39       #criticLoadMoreLayout:Landroid/widget/RelativeLayout;
    :cond_38
    const/16 v4, 0x8

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1d

    .line 672
    :cond_39
    const/16 v4, 0x8

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 673
    const/16 v4, 0x8

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 674
    const/16 v4, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 675
    const/16 v4, 0x8

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1d

    .line 687
    .end local v30           #criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .restart local v96       #trailerPanel:Landroid/widget/TextView;
    .restart local v103       #watchNow:Landroid/widget/TextView;
    :cond_3a
    const/16 v4, 0x8

    move-object/from16 v0, v96

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1e

    .line 701
    .restart local v72       #netflixText:Landroid/widget/TextView;
    .restart local v88       #showtimesPanel:Landroid/widget/TextView;
    :cond_3b
    const/16 v4, 0x8

    move-object/from16 v0, v72

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 363
    nop

    :array_0
    .array-data 0x4
        0x5et 0x1t 0x7t 0x7ft
        0x5ft 0x1t 0x7t 0x7ft
        0x60t 0x1t 0x7t 0x7ft
        0x61t 0x1t 0x7t 0x7ft
        0x5dt 0x1t 0x7t 0x7ft
        0x5ct 0x1t 0x7t 0x7ft
    .end array-data
.end method

.method private populateSeasonUi()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 708
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getView()Landroid/view/View;

    move-result-object v3

    .line 709
    .local v3, view:Landroid/view/View;
    const v5, 0x7f070151

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 710
    .local v4, viewEpisodes:Landroid/widget/TextView;
    iget-boolean v5, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isSeason:Z

    if-eqz v5, :cond_1

    .line 711
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 712
    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 713
    const v5, 0x7f07015a

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 714
    .local v2, synopsisHeader:Landroid/widget/TextView;
    const v5, 0x7f0c01a9

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 715
    const v5, 0x7f07014c

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 716
    .local v0, metaLine:Landroid/widget/TextView;
    iget-object v5, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    check-cast v5, Lnet/flixster/android/model/Season;

    invoke-virtual {v5}, Lnet/flixster/android/model/Season;->getNetwork()Ljava/lang/String;

    move-result-object v1

    .line 717
    .local v1, network:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 718
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 719
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 726
    .end local v0           #metaLine:Landroid/widget/TextView;
    .end local v1           #network:Ljava/lang/String;
    .end local v2           #synopsisHeader:Landroid/widget/TextView;
    :goto_0
    return-void

    .line 721
    .restart local v0       #metaLine:Landroid/widget/TextView;
    .restart local v1       #network:Ljava/lang/String;
    .restart local v2       #synopsisHeader:Landroid/widget/TextView;
    :cond_0
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 724
    .end local v0           #metaLine:Landroid/widget/TextView;
    .end local v1           #network:Ljava/lang/String;
    .end local v2           #synopsisHeader:Landroid/widget/TextView;
    :cond_1
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private populateStreamingUi()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 730
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 731
    .local v0, view:Landroid/view/View;
    const v1, 0x7f07014f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->watchNowPanel:Landroid/widget/TextView;

    .line 732
    const v1, 0x7f070150

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/view/DownloadPanel;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    .line 734
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    invoke-interface {v1, v2}, Lcom/flixster/android/drm/PlaybackLogic;->isAssetPlayable(Lnet/flixster/android/model/LockerRight;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 735
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->watchNowPanel:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 736
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->watchNowPanel:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->watchNowClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 741
    :goto_0
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    invoke-interface {v1, v2}, Lcom/flixster/android/drm/PlaybackLogic;->isAssetDownloadable(Lnet/flixster/android/model/LockerRight;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 742
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    invoke-virtual {v1, v3}, Lcom/flixster/android/view/DownloadPanel;->setVisibility(I)V

    .line 743
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadClickListener:Landroid/view/View$OnClickListener;

    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadDeleteClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2, v3, v4}, Lcom/flixster/android/view/DownloadPanel;->load(Lnet/flixster/android/model/LockerRight;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 747
    :goto_1
    return-void

    .line 738
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->watchNowPanel:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 745
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    invoke-virtual {v1, v4}, Lcom/flixster/android/view/DownloadPanel;->setVisibility(I)V

    goto :goto_1
.end method

.method private readExtras()V
    .locals 6

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovieId:J

    .line 153
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "KEY_IS_SEASON"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isSeason:Z

    .line 154
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "KEY_RIGHT_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 155
    .local v1, rightId:J
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mPlayTrailer:Ljava/lang/Boolean;

    if-nez v4, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "net.flixster.PLAY_TRAILER"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mPlayTrailer:Ljava/lang/Boolean;

    .line 159
    :cond_0
    iget-boolean v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->isSeason:Z

    if-eqz v4, :cond_2

    iget-wide v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovieId:J

    invoke-static {v4, v5}, Lnet/flixster/android/data/MovieDao;->getSeason(J)Lnet/flixster/android/model/Season;

    move-result-object v4

    :goto_0
    iput-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    .line 160
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v3

    .line 161
    .local v3, user:Lnet/flixster/android/model/User;
    if-eqz v3, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-lez v4, :cond_1

    .line 162
    invoke-virtual {v3, v1, v2}, Lnet/flixster/android/model/User;->getLockerRightFromRightId(J)Lnet/flixster/android/model/LockerRight;

    move-result-object v4

    iput-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    .line 165
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 166
    .local v0, p:Lnet/flixster/android/model/Property;
    iget-object v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->right:Lnet/flixster/android/model/LockerRight;

    if-nez v4, :cond_3

    if-eqz v0, :cond_3

    iget-boolean v4, v0, Lnet/flixster/android/model/Property;->isGetGlueEnabled:Z

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    :goto_1
    iput-boolean v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->showGetGlue:Z

    .line 168
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->populatePage()V

    .line 169
    return-void

    .line 159
    .end local v0           #p:Lnet/flixster/android/model/Property;
    .end local v3           #user:Lnet/flixster/android/model/User;
    :cond_2
    iget-wide v4, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovieId:J

    invoke-static {v4, v5}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v4

    goto :goto_0

    .line 166
    .restart local v0       #p:Lnet/flixster/android/model/Property;
    .restart local v3       #user:Lnet/flixster/android/model/User;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected NetworkAttemptsCancelled()V
    .locals 2

    .prologue
    .line 1847
    const-string v0, "FlxMain"

    const-string v1, "MovieDetailsFragment.NetworkAttemptsCancelled()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1848
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 141
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 142
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v2, 0x1

    .line 1510
    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    if-ne p1, v2, :cond_1

    .line 1511
    iput-boolean v2, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mFromRating:Z

    .line 1512
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1513
    .local v0, extras:Landroid/os/Bundle;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;

    if-nez v1, :cond_0

    .line 1514
    new-instance v1, Lnet/flixster/android/model/Review;

    invoke-direct {v1}, Lnet/flixster/android/model/Review;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;

    .line 1515
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;

    iput v2, v1, Lnet/flixster/android/model/Review;->type:I

    .line 1517
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;

    const-string v2, "net.flixster.ReviewComment"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    .line 1518
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MovieDetails onActivityResult comment:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;

    iget-object v3, v3, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1519
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mReview:Lnet/flixster/android/model/Review;

    const-string v2, "net.flixster.ReviewStars"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, v1, Lnet/flixster/android/model/Review;->stars:D

    .line 1530
    .end local v0           #extras:Landroid/os/Bundle;
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 29
    .parameter "v"

    .prologue
    .line 1350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    if-nez v2, :cond_1

    .line 1506
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 1353
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getView()Landroid/view/View;

    move-result-object v28

    .line 1355
    .local v28, view:Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 1358
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    .line 1359
    .local v27, reviewIndex:Ljava/lang/Integer;
    new-instance v26, Landroid/content/Intent;

    const-string v2, "REVIEW_PAGE"

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v8, Lcom/flixster/android/activity/hc/ReviewFragment;

    move-object/from16 v0, v26

    invoke-direct {v0, v2, v3, v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 1361
    .local v26, i:Landroid/content/Intent;
    if-nez v27, :cond_2

    .line 1362
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    .line 1364
    :cond_2
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieDetailsFragment reviewIndex:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    const-string v2, "REVIEW_INDEX"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1366
    const-string v2, "net.flixster.android.EXTRA_MOVIE_ID"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1368
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    const-class v3, Lcom/flixster/android/activity/hc/ReviewFragment;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 1373
    .end local v26           #i:Landroid/content/Intent;
    .end local v27           #reviewIndex:Ljava/lang/Integer;
    :sswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/website/flixster"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1374
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "Info (Flixster) - "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "title"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1373
    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1376
    new-instance v26, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "flixster"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1377
    .restart local v26       #i:Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1381
    .end local v26           #i:Landroid/content/Intent;
    :sswitch_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/website/rottentomatoes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1382
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "Website (RT) - "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "title"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1381
    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    new-instance v26, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "rottentomatoes"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1385
    .restart local v26       #i:Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1388
    .end local v26           #i:Landroid/content/Intent;
    :sswitch_4
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/website/imdb"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1389
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "Info (IMDB) - "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "title"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1388
    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1391
    new-instance v26, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "imdb"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1392
    .restart local v26       #i:Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1395
    .end local v26           #i:Landroid/content/Intent;
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_0

    .line 1396
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v2, v3}, Lnet/flixster/android/Starter;->launchTrailer(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1400
    :sswitch_6
    const-string v2, "FlxMain"

    const-string v3, "MovieDetailsFragment.onClick md_showtimes_panel"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_0

    .line 1402
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieDetailsFragment.onClick md_showtimes_panel mMovie:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1403
    new-instance v26, Landroid/content/Intent;

    const-string v2, "DETAILS"

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v8, Lcom/flixster/android/activity/hc/ShowtimesFragment;

    move-object/from16 v0, v26

    invoke-direct {v0, v2, v3, v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 1404
    .restart local v26       #i:Landroid/content/Intent;
    const-string v2, "net.flixster.android.EXTRA_MOVIE_ID"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1405
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    const-class v3, Lcom/flixster/android/activity/hc/ShowtimesFragment;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 1409
    .end local v26           #i:Landroid/content/Intent;
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_0

    .line 1410
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/getglue"

    const/4 v4, 0x0

    const-string v8, "GetGlue"

    const-string v9, "Click"

    invoke-interface {v2, v3, v4, v8, v9}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1411
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-static {v2}, Lnet/flixster/android/data/ApiBuilder;->getGlue(Lnet/flixster/android/model/Movie;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v2, v3}, Lnet/flixster/android/Starter;->launchBrowser(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1452
    :sswitch_8
    const v2, 0x7f07017d

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 1453
    .local v6, criticPanel:Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getCriticReviewsList()Ljava/util/ArrayList;

    move-result-object v7

    .line 1454
    .local v7, criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeListSize()I

    move-result v2

    .line 1455
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsListSize()I

    move-result v3

    .line 1454
    add-int/2addr v2, v3

    .line 1455
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getUserReviewsList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1454
    add-int/2addr v2, v3

    add-int/lit8 v5, v2, 0x3

    .line 1457
    .local v5, moreCriticReviewStart:I
    const/4 v3, 0x3

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->loadCriticReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 1459
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1460
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1463
    .end local v5           #moreCriticReviewStart:I
    .end local v6           #criticPanel:Landroid/widget/LinearLayout;
    .end local v7           #criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    :sswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeListSize()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsListSize()I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v11, v2, 0x3

    .line 1465
    .local v11, moreUserReviewStart:I
    const v2, 0x7f070186

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 1466
    .local v12, userPanel:Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getUserReviewsList()Ljava/util/ArrayList;

    move-result-object v13

    .line 1467
    .local v13, userReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    const/4 v9, 0x3

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v10

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v13}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->loadUserReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 1469
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1470
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1473
    .end local v11           #moreUserReviewStart:I
    .end local v12           #userPanel:Landroid/widget/LinearLayout;
    .end local v13           #userReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    :sswitch_a
    const/16 v17, 0x3

    .line 1474
    .local v17, moreFriendWantToSeeStart:I
    const v2, 0x7f07016e

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    .line 1475
    .local v18, moreWantToSeeLayout:Landroid/widget/LinearLayout;
    const/4 v15, 0x3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeListSize()I

    move-result v16

    .line 1476
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v14, p0

    .line 1475
    invoke-direct/range {v14 .. v19}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->loadFriendReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 1477
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1478
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1481
    .end local v17           #moreFriendWantToSeeStart:I
    .end local v18           #moreWantToSeeLayout:Landroid/widget/LinearLayout;
    :sswitch_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeListSize()I

    move-result v2

    add-int/lit8 v22, v2, 0x3

    .line 1482
    .local v22, moreFriendRatedStart:I
    const v2, 0x7f070174

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/LinearLayout;

    .line 1483
    .local v23, moreRatingsLayout:Landroid/widget/LinearLayout;
    const/16 v20, 0x3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsListSize()I

    move-result v21

    .line 1484
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v19, p0

    .line 1483
    invoke-direct/range {v19 .. v24}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->loadFriendReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 1485
    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1486
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1490
    .end local v22           #moreFriendRatedStart:I
    .end local v23           #moreRatingsLayout:Landroid/widget/LinearLayout;
    :sswitch_c
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieDetailsFragment.onClick : md_netflix mNetflixMenuToAction:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1491
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1490
    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I

    if-eqz v2, :cond_0

    .line 1493
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v25

    .line 1494
    .local v25, dialog:Landroid/app/Dialog;
    invoke-virtual/range {v25 .. v25}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 1499
    .end local v25           #dialog:Landroid/app/Dialog;
    :sswitch_d
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 1500
    const-string v3, "Feature coming soon! In the meantime, please use www.flixster.com to watch episodes."

    .line 1501
    const/4 v4, 0x1

    .line 1499
    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 1501
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1355
    :sswitch_data_0
    .sparse-switch
        0x7f030022 -> :sswitch_1
        0x7f070151 -> :sswitch_d
        0x7f070152 -> :sswitch_5
        0x7f070153 -> :sswitch_6
        0x7f070154 -> :sswitch_0
        0x7f070156 -> :sswitch_0
        0x7f070158 -> :sswitch_7
        0x7f070159 -> :sswitch_c
        0x7f070162 -> :sswitch_0
        0x7f07016f -> :sswitch_a
        0x7f070175 -> :sswitch_b
        0x7f07017e -> :sswitch_8
        0x7f070187 -> :sswitch_9
        0x7f07018b -> :sswitch_2
        0x7f07018c -> :sswitch_3
        0x7f07018d -> :sswitch_4
    .end sparse-switch
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 8
    .parameter "dialog"

    .prologue
    const/4 v5, 0x0

    .line 1752
    packed-switch p1, :pswitch_data_0

    .line 1843
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v5

    :cond_0
    :goto_0
    return-object v5

    .line 1754
    :pswitch_0
    const-string v6, "FlxMain"

    const-string v7, "MovieDetailsFragment.onCreateDialog DIALOG_NETFLIX_ACTION"

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1755
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e000b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 1756
    .local v4, netflixMenuReference:[Ljava/lang/String;
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I

    if-eqz v6, :cond_0

    .line 1759
    iget-object v5, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I

    array-length v1, v5

    .line 1760
    .local v1, menuLength:I
    new-array v3, v1, [Ljava/lang/String;

    .line 1761
    .local v3, netflixMenuArray:[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 1765
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1766
    const v6, 0x7f0c00ce

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 1767
    new-instance v6, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;

    invoke-direct {v6, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment$25;-><init>(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V

    invoke-virtual {v5, v3, v6}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 1833
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto :goto_0

    .line 1762
    :cond_1
    iget-object v5, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mNetflixMenuToAction:[I

    aget v5, v5, v0

    aget-object v5, v4, v5

    aput-object v5, v3, v0

    .line 1761
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1835
    .end local v0           #i:I
    .end local v1           #menuLength:I
    .end local v3           #netflixMenuArray:[Ljava/lang/String;
    .end local v4           #netflixMenuReference:[Ljava/lang/String;
    :pswitch_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v2, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1836
    .local v2, netflixAlertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v6, "Netflix Error"

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1837
    const-string v6, "You can\'t add movies to your Netflix Queue because it is full."

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1838
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1839
    const-string v6, "OK"

    invoke-virtual {v2, v6, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1841
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto :goto_0

    .line 1752
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 146
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 147
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03005b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 148
    .local v0, view:Landroid/view/View;
    return-object v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 173
    invoke-super {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onResume()V

    .line 174
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    if-nez v0, :cond_0

    .line 175
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->readExtras()V

    .line 178
    :cond_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->trackPage()V

    .line 180
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleLoadMovieTask(J)V

    .line 181
    return-void
.end method

.method protected retryAction()V
    .locals 2

    .prologue
    .line 1280
    const-wide/16 v0, 0x3e8

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->ScheduleLoadMovieTask(J)V

    .line 1281
    return-void
.end method

.method public trackPage()V
    .locals 6

    .prologue
    .line 1863
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v1, :cond_1

    .line 1865
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v2, "title"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1866
    .local v0, title:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "Info - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1868
    iget-boolean v1, p0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->showGetGlue:Z

    if-eqz v1, :cond_0

    .line 1869
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/getglue"

    const/4 v3, 0x0

    const-string v4, "GetGlue"

    const-string v5, "Impression"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1875
    .end local v0           #title:Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1873
    :cond_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "Info - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
