.class Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;
.super Landroid/os/Handler;
.source "MyMovieCollectionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    .line 100
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 102
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 103
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #calls: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->setCollectedLviList()V
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$0(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V

    .line 108
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 110
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    iget-boolean v1, v1, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->isInitialContentLoaded:Z

    if-nez v1, :cond_2

    .line 111
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    iput-boolean v3, v1, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->isInitialContentLoaded:Z

    .line 112
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->initialContentLoadingHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$1(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 116
    :cond_2
    invoke-static {}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$2()Z

    move-result v1

    if-nez v1, :cond_0

    .line 117
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$3(Z)V

    .line 118
    sget-boolean v1, Lcom/flixster/android/utils/F;->IS_NATIVE_HC_DRM_ENABLED:Z

    if-nez v1, :cond_3

    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/flixster/android/drm/PlaybackManager;->isRooted()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 119
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/flixster/android/view/DialogBuilder;->showStreamUnsupportedOnRootedDevices(Landroid/app/Activity;)V

    goto :goto_0

    .line 120
    :cond_3
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v1

    invoke-interface {v1}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceNonWifiStreamBlacklisted()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isWifi()Z

    move-result v1

    if-nez v1, :cond_4

    .line 121
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/flixster/android/view/DialogBuilder;->showNonWifiStreamUnsupported(Landroid/app/Activity;)V

    goto :goto_0

    .line 122
    :cond_4
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v1

    invoke-interface {v1}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceStreamBlacklisted()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v1

    invoke-interface {v1}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceStreamingCompatible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    :cond_5
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/flixster/android/view/DialogBuilder;->showStreamUnsupported(Landroid/app/Activity;)V

    goto :goto_0
.end method
