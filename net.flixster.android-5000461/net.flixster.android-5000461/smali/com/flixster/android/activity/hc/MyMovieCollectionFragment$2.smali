.class Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$2;
.super Landroid/os/Handler;
.source "MyMovieCollectionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$2;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    .line 129
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 131
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$2;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 132
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$2;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MyMovieCollection.errorHandler: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v1, v1, Lnet/flixster/android/data/DaoException;

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lnet/flixster/android/data/DaoException;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$2;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    invoke-static {v1, v2}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedActivity;)V

    goto :goto_0
.end method
