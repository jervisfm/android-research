.class Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$3;
.super Landroid/os/Handler;
.source "MyMovieCollectionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$3;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    .line 143
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .parameter "msg"

    .prologue
    .line 146
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$3;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 147
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$3;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$3;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$4(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/model/User;->getLockerRightsCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 152
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$3;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$4(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/model/User;->getLockerRights()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/LockerRight;

    .line 153
    .local v2, right:Lnet/flixster/android/model/LockerRight;
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 154
    .local v0, i:Landroid/content/Intent;
    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    iget-wide v4, v2, Lnet/flixster/android/model/LockerRight;->assetId:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 155
    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 156
    const-string v3, "KEY_IS_SEASON"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 158
    :cond_2
    const-string v3, "KEY_RIGHT_ID"

    iget-wide v4, v2, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 159
    const-class v3, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v1, v0, v3}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0
.end method
