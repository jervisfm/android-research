.class Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$4;
.super Ljava/lang/Object;
.source "MyMovieCollectionFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$4;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 221
    const-string v0, "FlxMain"

    const-string v1, "RefreshBar -  refreshBarClickListener.onClick"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$4;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$4(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Lnet/flixster/android/model/User;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$4;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$4(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Lnet/flixster/android/model/User;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lnet/flixster/android/model/User;->refreshRequired:Z

    .line 224
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$4;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$4(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Lnet/flixster/android/model/User;

    move-result-object v0

    invoke-virtual {v0}, Lnet/flixster/android/model/User;->resetLockerRights()V

    .line 225
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$4;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->onResume()V

    .line 227
    :cond_0
    return-void
.end method
