.class Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;
.super Ljava/util/TimerTask;
.source "MyMovieCollectionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    .line 50
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 54
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    .line 55
    .local v2, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$4(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$4(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-boolean v3, v3, Lnet/flixster/android/model/User;->refreshRequired:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v3, :cond_3

    .line 62
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;

    move-result-object v4

    #setter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3, v4}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$5(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;Lnet/flixster/android/model/User;)V

    .line 63
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$4(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    const/4 v4, 0x0

    iput-boolean v4, v3, Lnet/flixster/android/model/User;->refreshRequired:Z

    .line 64
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->collectedMoviesSuccessHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$6(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->errorHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$7(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Landroid/os/Handler;

    move-result-object v4

    invoke-static {v3, v4}, Lnet/flixster/android/data/ProfileDao;->getUserLockerRights(Landroid/os/Handler;Landroid/os/Handler;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 86
    :cond_3
    :goto_1
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_0

    .line 87
    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v3, "FlxMain"

    .line 67
    const-string v4, "MyMovieCollectionFragment.scheduleUpdatePageTask (failed to get user data)"

    .line 66
    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 68
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    const/4 v4, 0x0

    #setter for: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3, v4}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$5(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;Lnet/flixster/android/model/User;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 71
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catch_1
    move-exception v1

    .line 73
    .local v1, e:Ljava/lang/Exception;
    :try_start_3
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MyMovieCollectionFragment.ScheduleLoadItemsTask.run() mRetryCount:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    iget v5, v5, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mRetryCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 75
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    iget v4, v3, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mRetryCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mRetryCount:I

    .line 76
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    iget v3, v3, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mRetryCount:I

    const/4 v4, 0x3

    if-ge v3, v4, :cond_5

    .line 77
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    const-wide/16 v4, 0x3e8

    #calls: Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->ScheduleLoadItemsTask(J)V
    invoke-static {v3, v4, v5}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->access$8(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 86
    :cond_4
    :goto_2
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_0

    .line 87
    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 79
    :cond_5
    :try_start_4
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    const/4 v4, 0x0

    iput v4, v3, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mRetryCount:I

    .line 80
    if-eqz v2, :cond_4

    .line 81
    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 85
    .end local v1           #e:Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 86
    if-eqz v2, :cond_6

    iget-object v4, v2, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v4, :cond_6

    .line 87
    iget-object v4, v2, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 89
    :cond_6
    throw v3
.end method
