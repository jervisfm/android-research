.class public Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "MyMovieCollectionFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static isNoticeShown:Z


# instance fields
.field private final collectedMoviesSuccessHandler:Landroid/os/Handler;

.field private final errorHandler:Landroid/os/Handler;

.field private initialContentLoadingHandler:Landroid/os/Handler;

.field private mUser:Lnet/flixster/android/model/User;

.field private refreshBarClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 100
    new-instance v0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$1;-><init>(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->collectedMoviesSuccessHandler:Landroid/os/Handler;

    .line 129
    new-instance v0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$2;-><init>(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->errorHandler:Landroid/os/Handler;

    .line 143
    new-instance v0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$3;-><init>(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    .line 219
    new-instance v0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$4;-><init>(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->refreshBarClickListener:Landroid/view/View$OnClickListener;

    .line 34
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 3
    .parameter "delay"

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 50
    new-instance v0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment$5;-><init>(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V

    .line 94
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    const-string v1, "FlxMain"

    const-string v2, "MyMovieCollectionFragment.ScheduleLoadItemsTask() loading item"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v1, v0, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    :cond_0
    monitor-exit p0

    return-void

    .line 48
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->setCollectedLviList()V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 143
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2()Z
    .locals 1

    .prologue
    .line 35
    sget-boolean v0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->isNoticeShown:Z

    return v0
.end method

.method static synthetic access$3(Z)V
    .locals 0
    .parameter

    .prologue
    .line 35
    sput-boolean p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->isNoticeShown:Z

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Lnet/flixster/android/model/User;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;Lnet/flixster/android/model/User;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;

    return-void
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->collectedMoviesSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->errorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;J)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->ScheduleLoadItemsTask(J)V

    return-void
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 211
    const/16 v0, 0xa

    return v0
.end method

.method private setCollectedLviList()V
    .locals 10

    .prologue
    .line 165
    const-string v6, "FlxMain"

    const-string v7, "MyMovieCollection.setCollectedLviList "

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 169
    const/4 v1, 0x0

    .local v1, insertionIndex:I
    const/4 v0, 0x0

    .line 170
    .local v0, downloadInsertionIndex:I
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mUser:Lnet/flixster/android/model/User;

    invoke-virtual {v6}, Lnet/flixster/android/model/User;->getLockerRights()Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/flixster/android/utils/ListHelper;->copy(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 171
    .local v5, rights:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 195
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mDataHolder:Ljava/util/ArrayList;

    const/4 v7, 0x0

    new-instance v8, Lcom/flixster/android/view/RefreshBar;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/flixster/android/view/RefreshBar;-><init>(Landroid/content/Context;)V

    iget-object v9, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->refreshBarClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v9}, Lcom/flixster/android/view/RefreshBar;->setListener(Landroid/view/View$OnClickListener;)Lcom/flixster/android/view/RefreshBar;

    move-result-object v8

    .line 196
    sget v9, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_REFRESHBAR:I

    .line 195
    invoke-static {v8, v9}, Lnet/flixster/android/lvi/LviWrapper;->convertToLvi(Landroid/view/View;I)Lnet/flixster/android/lvi/Lvi;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 197
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mDataHolder:Ljava/util/ArrayList;

    new-instance v7, Lcom/flixster/android/view/UnfulfillableFooter;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/flixster/android/view/UnfulfillableFooter;-><init>(Landroid/content/Context;)V

    sget v8, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_FOOTER:I

    invoke-static {v7, v8}, Lnet/flixster/android/lvi/LviWrapper;->convertToLvi(Landroid/view/View;I)Lnet/flixster/android/lvi/Lvi;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v6, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mDataHolder:Ljava/util/ArrayList;

    new-instance v7, Lcom/flixster/android/view/UvFooter;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/flixster/android/view/UvFooter;-><init>(Landroid/content/Context;)V

    sget v8, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_FOOTER:I

    invoke-static {v7, v8}, Lnet/flixster/android/lvi/LviWrapper;->convertToLvi(Landroid/view/View;I)Lnet/flixster/android/lvi/Lvi;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    return-void

    .line 171
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/LockerRight;

    .line 172
    .local v4, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->isMovie()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-boolean v7, v4, Lnet/flixster/android/model/LockerRight;->isStreamingSupported:Z

    if-nez v7, :cond_3

    iget-boolean v7, v4, Lnet/flixster/android/model/LockerRight;->isDownloadSupported:Z

    if-nez v7, :cond_3

    :cond_2
    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 173
    :cond_3
    iget-boolean v7, v4, Lnet/flixster/android/model/LockerRight;->isRental:Z

    if-nez v7, :cond_0

    .line 176
    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->getAsset()Lnet/flixster/android/model/Movie;

    move-result-object v3

    .line 177
    .local v3, movie:Lnet/flixster/android/model/Movie;
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 178
    .local v2, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v3, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 179
    iput-object v4, v2, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    .line 181
    const/4 v7, 0x1

    iput-boolean v7, v2, Lnet/flixster/android/lvi/LviMovie;->mIsCollected:Z

    .line 183
    invoke-static {v4}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(Lnet/flixster/android/model/LockerRight;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 184
    iget-object v7, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v7, v0, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 185
    add-int/lit8 v0, v0, 0x1

    .line 186
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 188
    :cond_4
    iget-object v7, p0, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v7, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 189
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public onResume()V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 43
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/mycollection"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;->ScheduleLoadItemsTask(J)V

    .line 45
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 216
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/mycollection"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    return-void
.end method
