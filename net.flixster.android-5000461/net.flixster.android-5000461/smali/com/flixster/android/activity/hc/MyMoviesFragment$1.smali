.class Lcom/flixster/android/activity/hc/MyMoviesFragment$1;
.super Landroid/os/Handler;
.source "MyMoviesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MyMoviesFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    .line 189
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 192
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 193
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$1;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getInitialFragment()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0
.end method
