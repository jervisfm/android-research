.class Lcom/flixster/android/activity/hc/MyMoviesFragment$2;
.super Landroid/os/Handler;
.source "MyMoviesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/MyMoviesFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$2;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    .line 349
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    const/4 v2, 0x0

    .line 351
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$2;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 352
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$2;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v1

    invoke-virtual {v1}, Lnet/flixster/android/model/User;->getLockerNonEpisodesCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 357
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$2;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mMovieListsText:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 358
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$2;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mCollectionText:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
