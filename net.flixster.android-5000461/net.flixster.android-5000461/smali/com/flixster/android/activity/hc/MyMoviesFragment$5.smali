.class Lcom/flixster/android/activity/hc/MyMoviesFragment$5;
.super Ljava/util/TimerTask;
.source "MyMoviesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/MyMoviesFragment;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    .line 214
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 217
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    .line 218
    .local v2, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    :try_start_0
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MyMoviesPage.scheduleUpdatePageTask get mUser:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$1(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Lnet/flixster/android/model/User;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$1(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    if-nez v3, :cond_2

    .line 225
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;

    move-result-object v4

    #setter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3, v4}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$2(Lcom/flixster/android/activity/hc/MyMoviesFragment;Lnet/flixster/android/model/User;)V

    .line 227
    :cond_2
    const-string v3, "FlxMain"

    const-string v4, "MyMoviesPage.scheduleUpdatePageTask fetch mUser.bitmap"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$1(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$1(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->bitmap:Landroid/graphics/Bitmap;

    if-nez v3, :cond_3

    .line 229
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$1(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/model/User;->fetchProfileBitmap()V

    .line 231
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MyMoviesPage.scheduleUpdatePageTask mUser.bitmap:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$1(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Lnet/flixster/android/model/User;

    move-result-object v5

    iget-object v5, v5, Lnet/flixster/android/model/User;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 238
    :cond_3
    :goto_1
    const-string v3, "FlxMain"

    const-string v4, "MyMoviesPage.scheduleUpdatePageTask - calling refresh"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$3(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v3, "FlxMain"

    const-string v4, "Failed to get mUser"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 235
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catch_1
    move-exception v1

    .line 236
    .local v1, ie:Ljava/io/IOException;
    const-string v3, "FlxMain"

    const-string v4, "Failed to get profile bitmap"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
