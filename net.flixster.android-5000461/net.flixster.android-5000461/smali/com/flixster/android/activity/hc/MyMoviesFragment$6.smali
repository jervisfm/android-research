.class Lcom/flixster/android/activity/hc/MyMoviesFragment$6;
.super Ljava/util/TimerTask;
.source "MyMoviesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/MyMoviesFragment;->populatePage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$6;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    .line 303
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 306
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$6;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$1(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Lnet/flixster/android/model/User;

    move-result-object v1

    invoke-virtual {v1}, Lnet/flixster/android/model/User;->fetchProfileBitmap()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    :goto_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$6;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    #getter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$3(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 311
    return-void

    .line 307
    :catch_0
    move-exception v0

    .line 308
    .local v0, ie:Ljava/io/IOException;
    const-string v1, "FlxMain"

    const-string v2, "Failed to get profile bitmap"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
