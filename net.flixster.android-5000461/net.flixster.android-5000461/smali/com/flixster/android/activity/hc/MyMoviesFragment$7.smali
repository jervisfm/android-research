.class Lcom/flixster/android/activity/hc/MyMoviesFragment$7;
.super Ljava/lang/Object;
.source "MyMoviesFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/MyMoviesFragment;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$7;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    .line 389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter "dialog"
    .parameter "whichButton"

    .prologue
    .line 391
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$7;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/data/AccountManager;->fbLogout(Landroid/app/Activity;)V

    .line 394
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$7;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    const/4 v1, 0x0

    #setter for: Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$2(Lcom/flixster/android/activity/hc/MyMoviesFragment;Lnet/flixster/android/model/User;)V

    .line 395
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$7;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$7;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getInitialFragment()Ljava/lang/Class;

    move-result-object v2

    .line 396
    const/4 v3, 0x0

    const-class v4, Lcom/flixster/android/activity/hc/MyMoviesFragment;

    .line 395
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    .line 397
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment$7;->this$0:Lcom/flixster/android/activity/hc/MyMoviesFragment;

    #calls: Lcom/flixster/android/activity/hc/MyMoviesFragment;->populatePage()V
    invoke-static {v0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->access$0(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    .line 398
    return-void
.end method
