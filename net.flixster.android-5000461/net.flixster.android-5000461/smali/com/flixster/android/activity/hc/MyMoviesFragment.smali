.class public Lcom/flixster/android/activity/hc/MyMoviesFragment;
.super Lcom/flixster/android/activity/hc/FlixsterFragment;
.source "MyMoviesFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final DIALOG_LOGOUT_FACEBOOK:I = 0x0

.field private static final DIALOG_LOGOUT_FLIXSTER:I = 0x1

.field private static final DIALOG_LOGOUT_NETFLIX:I = 0x2

.field private static final DIALOG_PRIVACY:I = 0x4

.field private static final DIALOG_WINDOWREFRESH:I = 0x3

.field private static final TEMP_ENABLE_LOGIN:Z = true


# instance fields
.field private final collectedMoviesSuccessHandler:Landroid/os/Handler;

.field private final errorHandler:Landroid/os/Handler;

.field private initialContentLoadingHandler:Landroid/os/Handler;

.field mActorsText:Landroid/widget/TextView;

.field mAsyncRunner:Lcom/facebook/android/AsyncFacebookRunner;

.field mCollectionText:Landroid/widget/TextView;

.field mFacebookIcon:Landroid/widget/ImageView;

.field mFacebookLayout:Landroid/widget/RelativeLayout;

.field mFacebookStatus:Landroid/widget/TextView;

.field mFlixsterIcon:Landroid/widget/ImageView;

.field mFlixsterLayout:Landroid/widget/RelativeLayout;

.field mFlixsterNewsText:Landroid/widget/TextView;

.field mFlixsterStatus:Landroid/widget/TextView;

.field mFriendRatedText:Landroid/widget/TextView;

.field mFriendText:Landroid/widget/TextView;

.field mHeaderLayout:Landroid/widget/RelativeLayout;

.field mMovieListsText:Landroid/widget/TextView;

.field mNetflixLayout:Landroid/widget/RelativeLayout;

.field mNetflixLoginButton:Landroid/widget/ImageView;

.field mNetflixLoginMessage:Landroid/widget/TextView;

.field mNetflixText:Landroid/widget/TextView;

.field mPhotosText:Landroid/widget/TextView;

.field mRatedText:Landroid/widget/TextView;

.field private mRefreshHandler:Landroid/os/Handler;

.field private mUser:Lnet/flixster/android/model/User;

.field mWantToSeeText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    .line 189
    new-instance v0, Lcom/flixster/android/activity/hc/MyMoviesFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$1;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    .line 349
    new-instance v0, Lcom/flixster/android/activity/hc/MyMoviesFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$2;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->collectedMoviesSuccessHandler:Landroid/os/Handler;

    .line 363
    new-instance v0, Lcom/flixster/android/activity/hc/MyMoviesFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$3;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->errorHandler:Landroid/os/Handler;

    .line 474
    new-instance v0, Lcom/flixster/android/activity/hc/MyMoviesFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$4;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mRefreshHandler:Landroid/os/Handler;

    .line 46
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 250
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->populatePage()V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Lnet/flixster/android/model/User;
    .locals 1
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/MyMoviesFragment;Lnet/flixster/android/model/User;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/MyMoviesFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 474
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mRefreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 606
    const/4 v0, 0x5

    return v0
.end method

.method private populatePage()V
    .locals 17

    .prologue
    .line 251
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    .line 252
    .local v3, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v3, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->isRemoving()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    const-string v13, "FlxMain"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "MyMoviesPage.populatePage() netflix id:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_5

    .line 259
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixText:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 260
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 261
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixText:Landroid/widget/TextView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 263
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixLoginMessage:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c00d0

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixLoginButton:Landroid/widget/ImageView;

    const v14, 0x7f02013a

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 271
    :goto_1
    const-string v13, "FlxMain"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "MyMovies.populatePage mUser:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    if-eqz v13, :cond_a

    .line 274
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->collectedMoviesSuccessHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->errorHandler:Landroid/os/Handler;

    invoke-static {v13, v14}, Lnet/flixster/android/data/ProfileDao;->getUserLockerRights(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 276
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v4

    .line 277
    .local v4, platform:Ljava/lang/String;
    const-string v13, "FlxMain"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "MyMovies.populatePage platform:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0131

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 279
    .local v10, text:Ljava/lang/String;
    const-string v13, "FBK"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 280
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookStatus:Landroid/widget/TextView;

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v10, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookIcon:Landroid/widget/ImageView;

    const v14, 0x7f020090

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 282
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterLayout:Landroid/widget/RelativeLayout;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 288
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mHeaderLayout:Landroid/widget/RelativeLayout;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 289
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mHeaderLayout:Landroid/widget/RelativeLayout;

    const v14, 0x7f0700a2

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 290
    .local v2, headerStatus:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    iget-object v13, v13, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mHeaderLayout:Landroid/widget/RelativeLayout;

    const v14, 0x7f0700a0

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 293
    .local v11, thumbnailFrame:Landroid/widget/ImageView;
    const-string v13, "FLX"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 294
    const v13, 0x7f0200bc

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 298
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    iget-object v13, v13, Lnet/flixster/android/model/User;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v13, :cond_8

    .line 299
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mHeaderLayout:Landroid/widget/RelativeLayout;

    const v14, 0x7f07009f

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 300
    .local v12, userIcon:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    iget-object v13, v13, Lnet/flixster/android/model/User;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 318
    .end local v12           #userIcon:Landroid/widget/ImageView;
    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mHeaderLayout:Landroid/widget/RelativeLayout;

    const v14, 0x7f0700a5

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 319
    .local v6, profileFriendsText:Landroid/widget/TextView;
    new-instance v13, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    iget v14, v14, Lnet/flixster/android/model/User;->friendCount:I

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c0082

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mHeaderLayout:Landroid/widget/RelativeLayout;

    const v14, 0x7f0700a4

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 321
    .local v8, profileRatedText:Landroid/widget/TextView;
    new-instance v13, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    iget v14, v14, Lnet/flixster/android/model/User;->ratingCount:I

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c0083

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mHeaderLayout:Landroid/widget/RelativeLayout;

    const v14, 0x7f0700a3

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 323
    .local v9, profileWantToSeeText:Landroid/widget/TextView;
    new-instance v13, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    iget v14, v14, Lnet/flixster/android/model/User;->wtsCount:I

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c0084

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mHeaderLayout:Landroid/widget/RelativeLayout;

    const v14, 0x7f0700a6

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 326
    .local v5, profileCollectionsText:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    iget v1, v13, Lnet/flixster/android/model/User;->collectionsCount:I

    .line 327
    .local v1, collectionCount:I
    if-lez v1, :cond_9

    .line 328
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 329
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c0085

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 328
    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 266
    .end local v1           #collectionCount:I
    .end local v2           #headerStatus:Landroid/widget/TextView;
    .end local v4           #platform:Ljava/lang/String;
    .end local v5           #profileCollectionsText:Landroid/widget/TextView;
    .end local v6           #profileFriendsText:Landroid/widget/TextView;
    .end local v8           #profileRatedText:Landroid/widget/TextView;
    .end local v9           #profileWantToSeeText:Landroid/widget/TextView;
    .end local v10           #text:Ljava/lang/String;
    .end local v11           #thumbnailFrame:Landroid/widget/ImageView;
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixLoginMessage:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c00cf

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixLoginButton:Landroid/widget/ImageView;

    const v14, 0x7f020137

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 268
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixText:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 283
    .restart local v4       #platform:Ljava/lang/String;
    .restart local v10       #text:Ljava/lang/String;
    :cond_6
    const-string v13, "FLX"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 284
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterStatus:Landroid/widget/TextView;

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v10, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterIcon:Landroid/widget/ImageView;

    const v14, 0x7f02013a

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 286
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookLayout:Landroid/widget/RelativeLayout;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 295
    .restart local v2       #headerStatus:Landroid/widget/TextView;
    .restart local v11       #thumbnailFrame:Landroid/widget/ImageView;
    :cond_7
    const-string v13, "FBK"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 296
    const v13, 0x7f020149

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 302
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    iget-object v13, v13, Lnet/flixster/android/model/User;->profileImage:Ljava/lang/String;

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    iget-object v13, v13, Lnet/flixster/android/model/User;->profileImage:Ljava/lang/String;

    const-string v14, "http://"

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 303
    new-instance v7, Lcom/flixster/android/activity/hc/MyMoviesFragment$6;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$6;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    .line 313
    .local v7, profileImageTask:Ljava/util/TimerTask;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    check-cast v13, Lcom/flixster/android/activity/hc/Main;

    iget-object v13, v13, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v13, :cond_4

    .line 314
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    check-cast v13, Lcom/flixster/android/activity/hc/Main;

    iget-object v13, v13, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v14, 0x64

    invoke-virtual {v13, v7, v14, v15}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_4

    .line 332
    .end local v7           #profileImageTask:Ljava/util/TimerTask;
    .restart local v1       #collectionCount:I
    .restart local v5       #profileCollectionsText:Landroid/widget/TextView;
    .restart local v6       #profileFriendsText:Landroid/widget/TextView;
    .restart local v8       #profileRatedText:Landroid/widget/TextView;
    .restart local v9       #profileWantToSeeText:Landroid/widget/TextView;
    :cond_9
    const/16 v13, 0x8

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 335
    .end local v1           #collectionCount:I
    .end local v2           #headerStatus:Landroid/widget/TextView;
    .end local v4           #platform:Ljava/lang/String;
    .end local v5           #profileCollectionsText:Landroid/widget/TextView;
    .end local v6           #profileFriendsText:Landroid/widget/TextView;
    .end local v8           #profileRatedText:Landroid/widget/TextView;
    .end local v9           #profileWantToSeeText:Landroid/widget/TextView;
    .end local v10           #text:Ljava/lang/String;
    .end local v11           #thumbnailFrame:Landroid/widget/ImageView;
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c00cf

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 336
    .restart local v10       #text:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mHeaderLayout:Landroid/widget/RelativeLayout;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 337
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookStatus:Landroid/widget/TextView;

    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookIcon:Landroid/widget/ImageView;

    const v14, 0x7f02008a

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 339
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookLayout:Landroid/widget/RelativeLayout;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 340
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterStatus:Landroid/widget/TextView;

    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterIcon:Landroid/widget/ImageView;

    const v14, 0x7f020137

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 342
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterLayout:Landroid/widget/RelativeLayout;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 343
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFriendText:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 344
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mMovieListsText:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 345
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mCollectionText:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private scheduleUpdatePageTask()V
    .locals 5

    .prologue
    .line 214
    new-instance v1, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$5;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    .line 243
    .local v1, updatePageTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 244
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 245
    iget-object v2, v0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v3, 0x64

    invoke-virtual {v2, v1, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 247
    :cond_0
    return-void
.end method


# virtual methods
.method protected getInitialFragment()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flixster/android/activity/hc/FlixsterFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    const-class v0, Lcom/flixster/android/activity/hc/CollectionPromoFragment;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 88
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "view"

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 487
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MyMovies.onClick view.getId:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v2

    .line 492
    .local v2, username:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 601
    const-string v3, "FlxMain"

    const-string v4, "MyMovies.onClick"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :goto_0
    return-void

    .line 494
    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 495
    .local v1, intent:Landroid/content/Intent;
    const/high16 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 497
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    const-class v4, Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v3, v1, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 501
    .end local v1           #intent:Landroid/content/Intent;
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 502
    .restart local v1       #intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    const-class v4, Lcom/flixster/android/activity/hc/MyMovieCollectionFragment;

    invoke-virtual {v3, v1, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 506
    .end local v1           #intent:Landroid/content/Intent;
    :sswitch_2
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lnet/flixster/android/MoviesIWantToSeePage;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 507
    .restart local v1       #intent:Landroid/content/Intent;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MyMoviesPage.onClick username:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    if-eqz v2, :cond_0

    .line 509
    const-string v3, "net.flixster.IsConnected"

    invoke-virtual {v1, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 513
    :goto_1
    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 511
    :cond_0
    const-string v3, "net.flixster.IsConnected"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1

    .line 517
    .end local v1           #intent:Landroid/content/Intent;
    :sswitch_3
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lnet/flixster/android/MyRatedPage;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 518
    .restart local v1       #intent:Landroid/content/Intent;
    if-eqz v2, :cond_1

    .line 519
    const-string v3, "net.flixster.IsConnected"

    invoke-virtual {v1, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 523
    :goto_2
    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 521
    :cond_1
    const-string v3, "net.flixster.IsConnected"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    .line 527
    .end local v1           #intent:Landroid/content/Intent;
    :sswitch_4
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lnet/flixster/android/FriendsRatedPage;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 528
    .restart local v1       #intent:Landroid/content/Intent;
    if-eqz v2, :cond_2

    .line 529
    const-string v3, "net.flixster.IsConnected"

    invoke-virtual {v1, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 533
    :goto_3
    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 531
    :cond_2
    const-string v3, "net.flixster.IsConnected"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_3

    .line 537
    .end local v1           #intent:Landroid/content/Intent;
    :sswitch_5
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lnet/flixster/android/FriendsPage;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 538
    .restart local v1       #intent:Landroid/content/Intent;
    if-eqz v2, :cond_3

    .line 539
    const-string v3, "net.flixster.IsConnected"

    invoke-virtual {v1, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 543
    :goto_4
    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 541
    :cond_3
    const-string v3, "net.flixster.IsConnected"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_4

    .line 547
    .end local v1           #intent:Landroid/content/Intent;
    :sswitch_6
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 548
    .restart local v1       #intent:Landroid/content/Intent;
    const-string v3, "KEY_PHOTO_FILTER"

    const-string v4, "top-actresses"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 549
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    const-class v4, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    .line 550
    const-class v5, Lcom/flixster/android/activity/hc/MyMoviesFragment;

    .line 549
    invoke-virtual {v3, v1, v4, v6, v5}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    goto/16 :goto_0

    .line 554
    .end local v1           #intent:Landroid/content/Intent;
    :sswitch_7
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-class v5, Lcom/flixster/android/activity/hc/TopActorsFragment;

    invoke-virtual {v3, v4, v5}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 558
    :sswitch_8
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mUser:Lnet/flixster/android/model/User;

    if-eqz v3, :cond_4

    .line 560
    const-string v3, "FLX"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 561
    :cond_4
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lnet/flixster/android/FacebookAuth;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 562
    .restart local v1       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 565
    .end local v1           #intent:Landroid/content/Intent;
    :cond_5
    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v3}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 566
    invoke-static {}, Lcom/facebook/android/SessionEvents;->onLogoutBegin()V

    .line 567
    iget-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mAsyncRunner:Lcom/facebook/android/AsyncFacebookRunner;

    if-nez v3, :cond_6

    .line 568
    new-instance v3, Lcom/facebook/android/AsyncFacebookRunner;

    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-direct {v3, v4}, Lcom/facebook/android/AsyncFacebookRunner;-><init>(Lcom/facebook/android/Facebook;)V

    iput-object v3, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mAsyncRunner:Lcom/facebook/android/AsyncFacebookRunner;

    .line 574
    :cond_6
    invoke-virtual {p0, v6}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 575
    .local v0, dialog:Landroid/app/Dialog;
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 581
    .end local v0           #dialog:Landroid/app/Dialog;
    :sswitch_9
    if-eqz v2, :cond_7

    const-string v3, "FBK"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 582
    :cond_7
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lnet/flixster/android/FlixsterLoginPage;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 583
    .restart local v1       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 585
    .end local v1           #intent:Landroid/content/Intent;
    :cond_8
    invoke-virtual {p0, v7}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 586
    .restart local v0       #dialog:Landroid/app/Dialog;
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 591
    .end local v0           #dialog:Landroid/app/Dialog;
    :sswitch_a
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_9

    .line 592
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 593
    .restart local v1       #intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    const-class v4, Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    invoke-virtual {v3, v1, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 595
    .end local v1           #intent:Landroid/content/Intent;
    :cond_9
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 596
    .restart local v0       #dialog:Landroid/app/Dialog;
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 492
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0700ca -> :sswitch_6
        0x7f0700cb -> :sswitch_7
        0x7f0701ce -> :sswitch_1
        0x7f0701d0 -> :sswitch_0
        0x7f0701d1 -> :sswitch_2
        0x7f0701d2 -> :sswitch_3
        0x7f0701d3 -> :sswitch_4
        0x7f0701d4 -> :sswitch_5
        0x7f0701d5 -> :sswitch_8
        0x7f0701da -> :sswitch_9
        0x7f0701df -> :sswitch_a
    .end sparse-switch
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 12
    .parameter "dialog"

    .prologue
    const v7, 0x7f0c00ee

    const v11, 0x7f0c004a

    const/4 v5, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 380
    packed-switch p1, :pswitch_data_0

    .line 466
    :goto_0
    return-object v5

    .line 382
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 383
    .local v0, facebookAlertBuilder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    const-string v8, "Facebook"

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 384
    .local v4, title:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00ef

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    const-string v8, "Facebook"

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 385
    .local v2, msg:Ljava/lang/String;
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 386
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 387
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 388
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0133

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 389
    new-instance v7, Lcom/flixster/android/activity/hc/MyMoviesFragment$7;

    invoke-direct {v7, p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$7;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    .line 388
    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 400
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 401
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto :goto_0

    .line 403
    .end local v0           #facebookAlertBuilder:Landroid/app/AlertDialog$Builder;
    .end local v2           #msg:Ljava/lang/String;
    .end local v4           #title:Ljava/lang/String;
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 404
    .local v1, flixsterAlertBuilder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    const-string v8, "Flixster"

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 405
    .restart local v4       #title:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00ef

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    const-string v8, "Flixster"

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 406
    .restart local v2       #msg:Ljava/lang/String;
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 407
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 408
    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 409
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0133

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 410
    new-instance v7, Lcom/flixster/android/activity/hc/MyMoviesFragment$8;

    invoke-direct {v7, p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$8;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    .line 409
    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 422
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 423
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto/16 :goto_0

    .line 425
    .end local v1           #flixsterAlertBuilder:Landroid/app/AlertDialog$Builder;
    .end local v2           #msg:Ljava/lang/String;
    .end local v4           #title:Ljava/lang/String;
    :pswitch_2
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 426
    .local v3, netflixAlertBuilder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    const-string v8, "Netflix"

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 427
    .restart local v4       #title:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00ef

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    const-string v8, "Netflix"

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 428
    .restart local v2       #msg:Ljava/lang/String;
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 429
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 430
    invoke-virtual {v3, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 431
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0133

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 432
    new-instance v7, Lcom/flixster/android/activity/hc/MyMoviesFragment$9;

    invoke-direct {v7, p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$9;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    .line 431
    invoke-virtual {v3, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 440
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 441
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto/16 :goto_0

    .line 443
    .end local v2           #msg:Ljava/lang/String;
    .end local v3           #netflixAlertBuilder:Landroid/app/AlertDialog$Builder;
    .end local v4           #title:Ljava/lang/String;
    :pswitch_3
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v7, "Refresh the window"

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 444
    const-string v7, "Window Refresh"

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 445
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto/16 :goto_0

    .line 447
    :pswitch_4
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 448
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c018b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 449
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c018a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 450
    invoke-virtual {v5, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 451
    const-string v6, "OK"

    new-instance v7, Lcom/flixster/android/activity/hc/MyMoviesFragment$10;

    invoke-direct {v7, p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$10;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 455
    invoke-virtual {v5, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 456
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c018a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 457
    new-instance v7, Lcom/flixster/android/activity/hc/MyMoviesFragment$11;

    invoke-direct {v7, p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment$11;-><init>(Lcom/flixster/android/activity/hc/MyMoviesFragment;)V

    .line 456
    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 464
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto/16 :goto_0

    .line 380
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 93
    const v1, 0x7f030062

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 95
    .local v0, view:Landroid/view/View;
    const v1, 0x7f0701df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixLayout:Landroid/widget/RelativeLayout;

    .line 96
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isUsLocale()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isCanadaLocale()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 103
    :goto_0
    const v1, 0x7f0700c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixLoginMessage:Landroid/widget/TextView;

    .line 104
    const v1, 0x7f0700c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixLoginButton:Landroid/widget/ImageView;

    .line 105
    const v1, 0x7f0701d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixText:Landroid/widget/TextView;

    .line 107
    const v1, 0x7f0701cd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mMovieListsText:Landroid/widget/TextView;

    .line 109
    const v1, 0x7f0701ce

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mCollectionText:Landroid/widget/TextView;

    .line 110
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mCollectionText:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mCollectionText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 113
    const v1, 0x7f0701d1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mWantToSeeText:Landroid/widget/TextView;

    .line 114
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mWantToSeeText:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mWantToSeeText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 117
    const v1, 0x7f0701d2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mRatedText:Landroid/widget/TextView;

    .line 118
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mRatedText:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mRatedText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 121
    const v1, 0x7f0701d3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFriendRatedText:Landroid/widget/TextView;

    .line 122
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFriendRatedText:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFriendRatedText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 125
    const v1, 0x7f0701d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFriendText:Landroid/widget/TextView;

    .line 126
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFriendText:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFriendText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 129
    const v1, 0x7f0700ca

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mPhotosText:Landroid/widget/TextView;

    .line 130
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mPhotosText:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mPhotosText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 133
    const v1, 0x7f0700cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mActorsText:Landroid/widget/TextView;

    .line 134
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mActorsText:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mActorsText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 137
    const v1, 0x7f0700cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterNewsText:Landroid/widget/TextView;

    .line 140
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterNewsText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    const v1, 0x7f0701d5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookLayout:Landroid/widget/RelativeLayout;

    .line 143
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f0701d8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookIcon:Landroid/widget/ImageView;

    .line 144
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f0701d9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookStatus:Landroid/widget/TextView;

    .line 145
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFacebookLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 148
    const v1, 0x7f0701da

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterLayout:Landroid/widget/RelativeLayout;

    .line 149
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 152
    const v1, 0x7f0701dd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterIcon:Landroid/widget/ImageView;

    .line 153
    const v1, 0x7f0701de

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mFlixsterStatus:Landroid/widget/TextView;

    .line 155
    const v1, 0x7f07009e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mHeaderLayout:Landroid/widget/RelativeLayout;

    .line 163
    return-object v0

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mNetflixLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 168
    invoke-super {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onResume()V

    .line 169
    const-string v0, "FlxMain"

    const-string v1, "MyMoviesPage.onResume()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->trackPage()V

    .line 172
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->scheduleUpdatePageTask()V

    .line 173
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->mRefreshHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 178
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/MyMoviesFragment;->populatePage()V

    .line 183
    iget-boolean v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->isInitialContentLoaded:Z

    if-nez v0, :cond_0

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->isInitialContentLoaded:Z

    .line 185
    iget-object v0, p0, Lcom/flixster/android/activity/hc/MyMoviesFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 187
    :cond_0
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 647
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/home"

    const-string v2, "My Movies - Settings"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    return-void
.end method
