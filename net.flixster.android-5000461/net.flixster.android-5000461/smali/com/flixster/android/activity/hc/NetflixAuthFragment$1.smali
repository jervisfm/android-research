.class Lcom/flixster/android/activity/hc/NetflixAuthFragment$1;
.super Ljava/lang/Object;
.source "NetflixAuthFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NetflixAuthFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 114
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthConsumer;

    move-result-object v4

    const-string v5, "flixster://netflix/queue"

    invoke-interface {v3, v4, v5}, Loauth/signpost/OAuthProvider;->retrieveRequestToken(Loauth/signpost/OAuthConsumer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    .local v0, baseAuth:Ljava/lang/String;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 116
    .local v2, msg:Landroid/os/Message;
    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 117
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->authUrlHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$2(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_3

    .line 127
    .end local v0           #baseAuth:Ljava/lang/String;
    .end local v2           #msg:Landroid/os/Message;
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v1

    .line 119
    .local v1, e:Loauth/signpost/exception/OAuthMessageSignerException;
    const-string v3, "FlxMain"

    const-string v4, "NetflixAuth.onCreate() OAuthMessageSignerException"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 120
    .end local v1           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v1

    .line 121
    .local v1, e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    const-string v3, "FlxMain"

    const-string v4, "NetflixAuth.onCreate() OAuthNotAuthorizedException"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 122
    .end local v1           #e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_2
    move-exception v1

    .line 123
    .local v1, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    const-string v3, "FlxMain"

    const-string v4, "NetflixAuth.onCreate() OAuthExpectationFailedException"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 124
    .end local v1           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_3
    move-exception v1

    .line 125
    .local v1, e:Loauth/signpost/exception/OAuthCommunicationException;
    const-string v3, "FlxMain"

    const-string v4, "NetflixAuth.onCreate() OAuthCommunicationException"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
