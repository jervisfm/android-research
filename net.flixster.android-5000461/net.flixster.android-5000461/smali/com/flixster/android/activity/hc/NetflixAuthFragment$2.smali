.class Lcom/flixster/android/activity/hc/NetflixAuthFragment$2;
.super Landroid/os/Handler;
.source "NetflixAuthFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NetflixAuthFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$2;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    .line 130
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .parameter "msg"

    .prologue
    .line 133
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$2;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    .line 134
    .local v2, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$2;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 139
    .local v1, baseAuth:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 140
    .local v0, authUrl:Ljava/lang/StringBuilder;
    const-string v3, "&application_name="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Movies"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string v3, "&oauth_consumer_key="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "rtuhbkms7jmpsn3sft3va7y6"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixAuth.onCreate(.) authUrl:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$2;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mWebView:Landroid/webkit/WebView;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Landroid/webkit/WebView;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method
