.class Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;
.super Landroid/os/Handler;
.source "NetflixAuthFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NetflixAuthFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    .line 256
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 258
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 275
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthProvider;

    move-result-object v1

    invoke-interface {v1}, Loauth/signpost/OAuthProvider;->getResponseParameters()Loauth/signpost/http/HttpParameters;

    move-result-object v0

    .line 264
    .local v0, params:Loauth/signpost/http/HttpParameters;
    const-string v1, "FlxMain"

    .line 265
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixAuth.FlixsterWebViewClient mConsumer.getTokenSecret():"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthConsumer;

    move-result-object v3

    invoke-interface {v3}, Loauth/signpost/OAuthConsumer;->getTokenSecret()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 266
    const-string v3, " Map:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 265
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 264
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthConsumer;

    move-result-object v1

    invoke-interface {v1}, Loauth/signpost/OAuthConsumer;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setNetflixOAuthToken(Ljava/lang/String;)V

    .line 268
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthConsumer;

    move-result-object v1

    invoke-interface {v1}, Loauth/signpost/OAuthConsumer;->getTokenSecret()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setNetflixOAuthTokenSecret(Ljava/lang/String;)V

    .line 269
    const-string v1, "user_id"

    invoke-virtual {v0, v1}, Loauth/signpost/http/HttpParameters;->getFirst(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setNetflixUserId(Ljava/lang/String;)V

    .line 271
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/netflix/login/success"

    const-string v3, "Netflix Login Success"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main;->peekListBackStack()Lcom/flixster/android/activity/hc/FlixsterFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onResume()V

    .line 274
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main;->popContentBackStack()V

    goto :goto_0
.end method
