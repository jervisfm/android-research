.class Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "NetflixAuthFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NetflixAuthFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetflixWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;


# direct methods
.method private constructor <init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 175
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 175
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;-><init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 6
    .parameter "view"
    .parameter "url"

    .prologue
    .line 191
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 193
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NetflixAuth.FlixsterWebViewClient.onPageFinished(..) url:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mWebView:Landroid/webkit/WebView;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Landroid/webkit/WebView;

    move-result-object v2

    const-string v3, "javascript:window.document.getElementsByTagName(\'html\')[0].style.backgroundColor=\'#9A0904\';"

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 199
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v0

    .line 200
    .local v0, build:I
    const/4 v2, 0x5

    if-lt v0, v2, :cond_0

    const/16 v2, 0x8

    if-ge v0, v2, :cond_0

    .line 201
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mWebView:Landroid/webkit/WebView;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Landroid/webkit/WebView;

    move-result-object v2

    const-string v3, "javascript:window.document.getElementsByTagName(\'input\')[6].style.paddingTop=\'12px\';"

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 204
    :cond_0
    const-string v2, "flixster:"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 208
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mWebView:Landroid/webkit/WebView;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Landroid/webkit/WebView;

    move-result-object v2

    const-string v3, "javascript:window.document.body.innerHTML=\'<br><br><br><center><h2>Loading...</h2></center>\';"

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 209
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mWebView:Landroid/webkit/WebView;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Landroid/webkit/WebView;

    move-result-object v2

    const-string v3, "javascript:window.document.getElementsByTagName(\'body\')[0].style.margin=\'-10px\';"

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 212
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NetflixAuth.FlixsterWebViewClient pre access token mConsumer.getTokenSecret():"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 213
    iget-object v4, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthConsumer;

    move-result-object v4

    invoke-interface {v4}, Loauth/signpost/OAuthConsumer;->getTokenSecret()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 212
    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string v2, "FlxMain"

    .line 217
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NetflixAuth.onPageFinished() getAccessTokenEndpointUrl:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 218
    iget-object v4, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthProvider;

    move-result-object v4

    invoke-interface {v4}, Loauth/signpost/OAuthProvider;->getAccessTokenEndpointUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 217
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 215
    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NetflixAuth.onPageFinished() isOAuth10a:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthProvider;

    move-result-object v4

    invoke-interface {v4}, Loauth/signpost/OAuthProvider;->isOAuth10a()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthProvider;

    move-result-object v2

    invoke-interface {v2}, Loauth/signpost/OAuthProvider;->getResponseParameters()Loauth/signpost/http/HttpParameters;

    move-result-object v2

    invoke-virtual {v2}, Loauth/signpost/http/HttpParameters;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 224
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthConsumer;

    move-result-object v2

    invoke-interface {v2}, Loauth/signpost/OAuthConsumer;->getRequestParameters()Loauth/signpost/http/HttpParameters;

    move-result-object v2

    invoke-virtual {v2}, Loauth/signpost/http/HttpParameters;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 229
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->retrieveTokenRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$5(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 231
    :cond_1
    return-void

    .line 220
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 221
    .local v1, key:Ljava/lang/String;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixAuth.onPageFinished() getResponseParameters:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 222
    iget-object v5, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthProvider;

    move-result-object v5

    invoke-interface {v5}, Loauth/signpost/OAuthProvider;->getResponseParameters()Loauth/signpost/http/HttpParameters;

    move-result-object v5

    invoke-virtual {v5, v1}, Loauth/signpost/http/HttpParameters;->get(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 221
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 224
    .end local v1           #key:Ljava/lang/String;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 225
    .restart local v1       #key:Ljava/lang/String;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixAuth.onPageFinished() getRequestParameters:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 226
    iget-object v5, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;->this$0:Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthConsumer;

    move-result-object v5

    invoke-interface {v5}, Loauth/signpost/OAuthConsumer;->getRequestParameters()Loauth/signpost/http/HttpParameters;

    move-result-object v5

    invoke-virtual {v5, v1}, Loauth/signpost/http/HttpParameters;->get(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 225
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3
    .parameter "view"
    .parameter "url"

    .prologue
    .line 179
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    .line 180
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FacebookAuth.FlixsterWebViewClient.shouldOverrideUrlLoading(..) url:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const/4 v0, 0x0

    return v0
.end method
