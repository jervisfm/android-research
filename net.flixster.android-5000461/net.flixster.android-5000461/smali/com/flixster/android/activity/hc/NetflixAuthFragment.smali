.class public Lcom/flixster/android/activity/hc/NetflixAuthFragment;
.super Lcom/flixster/android/activity/hc/FlixsterFragment;
.source "NetflixAuthFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;
    }
.end annotation


# static fields
.field public static final LAYOUT_SPLASH:I = 0x0

.field public static final LAYOUT_STATE:Ljava/lang/String; = "LayoutState"

.field public static final LAYOUT_WEB:I = 0x1


# instance fields
.field private final authUrlHandler:Landroid/os/Handler;

.field private volatile mConsumer:Loauth/signpost/OAuthConsumer;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mLayoutState:I

.field private mLoginButton:Landroid/widget/Button;

.field private mNewAccountButton:Landroid/widget/Button;

.field private volatile mProvider:Loauth/signpost/OAuthProvider;

.field private mSelectLayout:Landroid/widget/RelativeLayout;

.field private mWebView:Landroid/webkit/WebView;

.field private final netflixPageFinishedHandler:Landroid/os/Handler;

.field private final retrieveTokenRunnable:Ljava/lang/Runnable;

.field private final tokenRequestRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mLayoutState:I

    .line 109
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixAuthFragment$1;-><init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->tokenRequestRunnable:Ljava/lang/Runnable;

    .line 130
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixAuthFragment$2;-><init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->authUrlHandler:Landroid/os/Handler;

    .line 234
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixAuthFragment$3;-><init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->retrieveTokenRunnable:Ljava/lang/Runnable;

    .line 256
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixAuthFragment$4;-><init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->netflixPageFinishedHandler:Landroid/os/Handler;

    .line 37
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthProvider;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mProvider:Loauth/signpost/OAuthProvider;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Loauth/signpost/OAuthConsumer;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mConsumer:Loauth/signpost/OAuthConsumer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 130
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->authUrlHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Landroid/webkit/WebView;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 256
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->netflixPageFinishedHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/NetflixAuthFragment;)Ljava/lang/Runnable;
    .locals 1
    .parameter

    .prologue
    .line 234
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->retrieveTokenRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 298
    const/16 v0, 0x6d

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/NetflixAuthFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 57
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;-><init>()V

    .line 58
    .local v0, naf:Lcom/flixster/android/activity/hc/NetflixAuthFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->setArguments(Landroid/os/Bundle;)V

    .line 59
    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 279
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 295
    :goto_0
    return-void

    .line 281
    :pswitch_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mLayoutState:I

    .line 282
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mSelectLayout:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 283
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/netflix/login/dialog"

    const-string v3, "Netflix Login"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 288
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/netflix/newaccnt"

    const-string v3, "Start New Account"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 290
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 279
    :pswitch_data_0
    .packed-switch 0x7f0701f4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onCreate(Landroid/os/Bundle;)V

    .line 100
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->trackNewAccountImpression()V

    .line 103
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->getNewConsumer()Loauth/signpost/OAuthConsumer;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mConsumer:Loauth/signpost/OAuthConsumer;

    .line 104
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->getNewProvider()Loauth/signpost/OAuthProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mProvider:Loauth/signpost/OAuthProvider;

    .line 106
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->tokenRequestRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 107
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x0

    .line 64
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 65
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030066

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 66
    .local v0, view:Landroid/view/View;
    const v3, 0x7f0701f1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/webkit/WebView;

    iput-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mWebView:Landroid/webkit/WebView;

    .line 68
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 69
    .local v1, webSettings:Landroid/webkit/WebSettings;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 75
    sget-object v3, Landroid/webkit/WebSettings$ZoomDensity;->CLOSE:Landroid/webkit/WebSettings$ZoomDensity;

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setDefaultZoom(Landroid/webkit/WebSettings$ZoomDensity;)V

    .line 77
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 79
    new-instance v2, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;

    invoke-direct {v2, p0, v5}, Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;-><init>(Lcom/flixster/android/activity/hc/NetflixAuthFragment;Lcom/flixster/android/activity/hc/NetflixAuthFragment$NetflixWebViewClient;)V

    .line 80
    .local v2, wv:Landroid/webkit/WebViewClient;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 84
    const v3, 0x7f0701f2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mSelectLayout:Landroid/widget/RelativeLayout;

    .line 86
    const v3, 0x7f0701f4

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mLoginButton:Landroid/widget/Button;

    .line 87
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mLoginButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    const v3, 0x7f0701f5

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mNewAccountButton:Landroid/widget/Button;

    .line 90
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mNewAccountButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-object v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 153
    invoke-super {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onResume()V

    .line 154
    iget v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mLayoutState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mSelectLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 160
    :goto_0
    return-void

    .line 157
    :cond_0
    const-string v0, "FlxMain"

    const-string v1, "NetflixAuth onResume() NetflixLinkSynergy()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->trackNewAccountReferral()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 165
    const-string v0, "FlxMain"

    const-string v1, "NetflixAuth.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v0, "LayoutState"

    iget v1, p0, Lcom/flixster/android/activity/hc/NetflixAuthFragment;->mLayoutState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 167
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 303
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/invalid"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    return-void
.end method
