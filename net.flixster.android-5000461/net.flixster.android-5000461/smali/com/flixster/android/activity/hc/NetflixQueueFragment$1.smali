.class Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;
.super Ljava/lang/Object;
.source "NetflixQueueFragment.java"

# interfaces
.implements Lnet/flixster/android/model/TouchInterceptor$DropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NetflixQueueFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public drop(II)V
    .locals 7
    .parameter "from"
    .parameter "to"

    .prologue
    .line 270
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixQueuePage.mDropListener.drop(from:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", to:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 271
    const-string v5, ") mOffsetSelect[mNavSelect]:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v5}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v5

    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget v6, v6, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 270
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x3

    .line 286
    .local v0, lastMovieIndex:I
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixQueuePage.mDropListener.drop(from:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", to:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") lastMovieIndex:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 287
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 286
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    if-le p2, v0, :cond_0

    .line 289
    move p2, v0

    .line 292
    :cond_0
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixQueuePage.mDropListener.drop(from:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", to:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    if-eq p2, p1, :cond_1

    .line 297
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 299
    .local v2, swapMapItem:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 300
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p2, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 302
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->notifyDataSetChanged()V

    .line 304
    const-string v3, "netflixQueueItem"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/NetflixQueueItem;

    .line 307
    .local v1, netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    const-string v3, "FlxMain"

    const-string v4, "NetflixQueuePage.mDropListener.drop calling ScheduleDiscMove"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    const-string v4, "id"

    invoke-virtual {v1, v4}, Lnet/flixster/android/model/NetflixQueueItem;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v5, p2, 0x1

    #calls: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleDiscMove(Ljava/lang/String;I)V
    invoke-static {v3, v4, v5}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$2(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/lang/String;I)V

    .line 311
    .end local v1           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    .end local v2           #swapMapItem:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_1
    return-void
.end method
