.class Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;
.super Ljava/util/TimerTask;
.source "NetflixQueueFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleDiscMove(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

.field private final synthetic val$netflixMovieId:Ljava/lang/String;

.field private final synthetic val$to:I


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iput-object p2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->val$netflixMovieId:Ljava/lang/String;

    iput p3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->val$to:I

    .line 340
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 346
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mDiscAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    if-ne v1, v2, :cond_1

    .line 347
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://api-public.netflix.com/catalog/titles/movies/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 348
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->val$netflixMovieId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 347
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 348
    iget v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->val$to:I

    const-string v3, "/queues/disc/available"

    .line 347
    invoke-static {v1, v2, v3}, Lnet/flixster/android/data/NetflixDao;->postQueueItem(Ljava/lang/String;ILjava/lang/String;)V

    .line 376
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mInstantAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    if-ne v1, v2, :cond_0

    .line 350
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://api-public.netflix.com/catalog/titles/movies/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 351
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->val$netflixMovieId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 350
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 351
    iget v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;->val$to:I

    const-string v3, "/queues/instant/available"

    .line 350
    invoke-static {v1, v2, v3}, Lnet/flixster/android/data/NetflixDao;->postQueueItem(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_6

    goto :goto_0

    .line 353
    :catch_0
    move-exception v0

    .line 355
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthMessageSignerException;->printStackTrace()V

    goto :goto_0

    .line 356
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v0

    .line 358
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthExpectationFailedException;->printStackTrace()V

    goto :goto_0

    .line 359
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_2
    move-exception v0

    .line 361
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 362
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_3
    move-exception v0

    .line 364
    .local v0, e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthNotAuthorizedException;->printStackTrace()V

    goto :goto_0

    .line 365
    .end local v0           #e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_4
    move-exception v0

    .line 367
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 368
    .end local v0           #e:Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 370
    .local v0, e:Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_0

    .line 371
    .end local v0           #e:Loauth/signpost/exception/OAuthCommunicationException;
    :catch_6
    move-exception v0

    .line 373
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
