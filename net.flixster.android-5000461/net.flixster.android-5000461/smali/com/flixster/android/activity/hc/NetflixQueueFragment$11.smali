.class Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;
.super Ljava/util/TimerTask;
.source "NetflixQueueFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleDiscDelete(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

.field private final synthetic val$netflixDeleteId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iput-object p2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->val$netflixDeleteId:Ljava/lang/String;

    .line 384
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 392
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mDiscAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    if-ne v1, v2, :cond_1

    .line 393
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->val$netflixDeleteId:Ljava/lang/String;

    const-string v2, "/queues/disc/available"

    invoke-static {v1, v2}, Lnet/flixster/android/data/NetflixDao;->deleteQueueItem(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->val$netflixDeleteId:Ljava/lang/String;

    #calls: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->deleteItem(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$14(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/lang/String;)V

    .line 400
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    aget v3, v1, v2

    add-int/lit8 v3, v3, -0x1

    aput v3, v1, v2

    .line 401
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    aget v3, v1, v2

    add-int/lit8 v3, v3, -0x1

    aput v3, v1, v2

    .line 422
    :goto_1
    return-void

    .line 394
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mInstantAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    if-ne v1, v2, :cond_2

    .line 395
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->val$netflixDeleteId:Ljava/lang/String;

    const-string v2, "/queues/instant/available"

    invoke-static {v1, v2}, Lnet/flixster/android/data/NetflixDao;->deleteQueueItem(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 402
    :catch_0
    move-exception v0

    .line 404
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthMessageSignerException;->printStackTrace()V

    goto :goto_1

    .line 396
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mSavedAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    if-ne v1, v2, :cond_0

    .line 397
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;->val$netflixDeleteId:Ljava/lang/String;

    const-string v2, "/queues/disc/saved"

    invoke-static {v1, v2}, Lnet/flixster/android/data/NetflixDao;->deleteQueueItem(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_0

    .line 405
    :catch_1
    move-exception v0

    .line 407
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthExpectationFailedException;->printStackTrace()V

    goto :goto_1

    .line 408
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_2
    move-exception v0

    .line 410
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_1

    .line 411
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_3
    move-exception v0

    .line 413
    .local v0, e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthNotAuthorizedException;->printStackTrace()V

    goto :goto_1

    .line 414
    .end local v0           #e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_4
    move-exception v0

    .line 416
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 417
    .end local v0           #e:Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 419
    .local v0, e:Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_1
.end method
