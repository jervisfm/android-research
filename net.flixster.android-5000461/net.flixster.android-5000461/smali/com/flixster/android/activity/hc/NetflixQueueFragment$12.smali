.class Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;
.super Ljava/util/TimerTask;
.source "NetflixQueueFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleLoadMoviesTask(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

.field private final synthetic val$currNavSelect:I


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iput p2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->val$currNavSelect:I

    .line 452
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 454
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 455
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 459
    :cond_1
    const-string v2, "FlxMain"

    const-string v3, "NetflixQueuePage.ScheduleLoadMoviesTask()"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreStateSelect:[I

    iget v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->val$currNavSelect:I

    aget v2, v2, v3

    if-ne v2, v7, :cond_2

    .line 464
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v2

    iget v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->val$currNavSelect:I

    aput v5, v2, v3

    .line 466
    :cond_2
    iget v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->val$currNavSelect:I

    packed-switch v2, :pswitch_data_0

    .line 534
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 535
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mNetflixQueueItemList.size():"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :cond_4
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->postMovieChangeHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$24(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 540
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-boolean v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->isInitialContentLoaded:Z

    if-nez v2, :cond_0

    .line 541
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iput-boolean v6, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->isInitialContentLoaded:Z

    .line 542
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->initialContentLoadingHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$25(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;

    move-result-object v2

    iget v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->val$currNavSelect:I

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 468
    :pswitch_0
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v3

    aget v3, v3, v6

    const-string v4, "/queues/disc/available"

    #calls: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getDvdQueue(ILjava/lang/String;I)Ljava/util/List;
    invoke-static {v2, v3, v4, v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$15(Lcom/flixster/android/activity/hc/NetflixQueueFragment;ILjava/lang/String;I)Ljava/util/List;

    move-result-object v1

    .line 469
    .local v1, tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    if-eqz v1, :cond_3

    .line 470
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 471
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 472
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v3

    aget v3, v3, v6

    invoke-interface {v2, v3, v1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 476
    :goto_2
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v3

    .line 477
    iget-object v4, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$17(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 476
    aput v3, v2, v6

    .line 479
    :cond_5
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v5, v2, v6

    goto/16 :goto_1

    .line 474
    :cond_6
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #setter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v2, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$16(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V

    goto :goto_2

    .line 483
    .end local v1           #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :pswitch_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v3

    aget v3, v3, v8

    const-string v4, "/queues/instant/available"

    #calls: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getDvdQueue(ILjava/lang/String;I)Ljava/util/List;
    invoke-static {v2, v3, v4, v8}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$15(Lcom/flixster/android/activity/hc/NetflixQueueFragment;ILjava/lang/String;I)Ljava/util/List;

    move-result-object v1

    .line 484
    .restart local v1       #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    if-eqz v1, :cond_3

    .line 485
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 486
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$4(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 487
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$4(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v2

    .line 488
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v3

    aget v3, v3, v8

    invoke-interface {v2, v3, v1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 492
    :goto_3
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$4(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v3

    .line 493
    iget-object v4, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$19(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 492
    aput v3, v2, v8

    .line 495
    :cond_7
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v5, v2, v8

    goto/16 :goto_1

    .line 490
    :cond_8
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #setter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v2, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$18(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V

    goto :goto_3

    .line 499
    .end local v1           #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :pswitch_2
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v3

    aget v3, v3, v7

    const-string v4, "/queues/disc/saved"

    #calls: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getDvdQueue(ILjava/lang/String;I)Ljava/util/List;
    invoke-static {v2, v3, v4, v7}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$15(Lcom/flixster/android/activity/hc/NetflixQueueFragment;ILjava/lang/String;I)Ljava/util/List;

    move-result-object v1

    .line 500
    .restart local v1       #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    if-eqz v1, :cond_3

    .line 501
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_9

    .line 502
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$5(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 503
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$5(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v3

    aget v3, v3, v7

    invoke-interface {v2, v3, v1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 507
    :goto_4
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$5(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v3

    .line 508
    iget-object v4, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$21(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 507
    aput v3, v2, v7

    .line 510
    :cond_9
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v5, v2, v7

    goto/16 :goto_1

    .line 505
    :cond_a
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #setter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v2, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$20(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V

    goto :goto_4

    .line 514
    .end local v1           #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :pswitch_3
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v3

    aget v3, v3, v9

    const-string v4, "/at_home"

    #calls: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getDvdQueue(ILjava/lang/String;I)Ljava/util/List;
    invoke-static {v2, v3, v4, v9}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$15(Lcom/flixster/android/activity/hc/NetflixQueueFragment;ILjava/lang/String;I)Ljava/util/List;

    move-result-object v1

    .line 515
    .restart local v1       #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    if-eqz v1, :cond_3

    .line 516
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_b

    .line 517
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$6(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 518
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$6(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v3

    aget v3, v3, v9

    invoke-interface {v2, v3, v1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 522
    :goto_5
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$6(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v3

    .line 523
    iget-object v4, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$23(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 522
    aput v3, v2, v9

    .line 525
    :cond_b
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v5, v2, v9

    goto/16 :goto_1

    .line 520
    :cond_c
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #setter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v2, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$22(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V

    goto :goto_5

    .line 466
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
