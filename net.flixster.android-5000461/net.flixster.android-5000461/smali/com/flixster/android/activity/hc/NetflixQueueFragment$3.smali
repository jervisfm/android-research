.class Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;
.super Landroid/os/Handler;
.source "NetflixQueueFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NetflixQueueFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    .line 550
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .parameter "msg"

    .prologue
    const/4 v7, 0x0

    .line 553
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    .line 554
    .local v2, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v2, :cond_0

    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->isRemoving()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 558
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    .line 559
    .local v1, intendedNavSelect:I
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget v6, v6, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    if-ne v6, v1, :cond_0

    .line 563
    const/4 v3, 0x0

    .line 564
    .local v3, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget v6, v6, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    packed-switch v6, :pswitch_data_0

    .line 588
    :cond_2
    :goto_1
    const/4 v4, 0x0

    .line 589
    .local v4, movie:Lnet/flixster/android/model/Movie;
    if-eqz v3, :cond_3

    .line 590
    const-string v6, "netflixQueueItem"

    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/NetflixQueueItem;

    .line 591
    .local v5, nqi:Lnet/flixster/android/model/NetflixQueueItem;
    if-eqz v5, :cond_3

    .line 592
    iget-object v4, v5, Lnet/flixster/android/model/NetflixQueueItem;->mMovie:Lnet/flixster/android/model/Movie;

    .line 595
    .end local v5           #nqi:Lnet/flixster/android/model/NetflixQueueItem;
    :cond_3
    if-eqz v4, :cond_0

    .line 596
    new-instance v0, Landroid/content/Intent;

    const-string v6, "DETAILS"

    const/4 v7, 0x0

    const-class v8, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v0, v6, v7, v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 597
    .local v0, i:Landroid/content/Intent;
    const-string v6, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v7

    invoke-virtual {v0, v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 598
    const-class v6, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v2, v0, v6}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 566
    .end local v0           #i:Landroid/content/Intent;
    .end local v4           #movie:Lnet/flixster/android/model/Movie;
    :pswitch_0
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 567
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3           #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    check-cast v3, Ljava/util/Map;

    .line 569
    .restart local v3       #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    goto :goto_1

    .line 571
    :pswitch_1
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$4(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 572
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$4(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3           #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    check-cast v3, Ljava/util/Map;

    .line 574
    .restart local v3       #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    goto :goto_1

    .line 576
    :pswitch_2
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$5(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 577
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$5(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3           #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    check-cast v3, Ljava/util/Map;

    .line 579
    .restart local v3       #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    goto :goto_1

    .line 581
    :pswitch_3
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$6(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 582
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$6(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3           #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    check-cast v3, Ljava/util/Map;

    .line 584
    .restart local v3       #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    goto/16 :goto_1

    .line 564
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
