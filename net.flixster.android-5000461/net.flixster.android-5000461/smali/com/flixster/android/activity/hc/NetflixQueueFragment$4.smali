.class Lcom/flixster/android/activity/hc/NetflixQueueFragment$4;
.super Landroid/os/Handler;
.source "NetflixQueueFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NetflixQueueFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    .line 711
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    const/4 v2, 0x0

    .line 714
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    :goto_0
    return-void

    .line 718
    :cond_0
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.postMovieChangeHandler"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->removeLoadingDialog:Landroid/os/Handler;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$7(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 720
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$8(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 721
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 722
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixContextMenu:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$9(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 724
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$4;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixContextMenu:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$9(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method
