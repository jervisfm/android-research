.class Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;
.super Ljava/lang/Object;
.source "NetflixQueueFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NetflixQueueFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    .line 956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "view"

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 958
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1040
    :cond_0
    :goto_0
    return-void

    .line 960
    :pswitch_0
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.navListener - disc"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/dvd"

    const-string v2, "Netflix DVDs"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iput v4, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    .line 963
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 964
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$3(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v1

    #setter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$10(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V

    .line 965
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mDiscAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iput-object v1, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    .line 966
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$8(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 968
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aget-boolean v0, v0, v4

    if-eqz v0, :cond_1

    .line 969
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 970
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v0

    aput v3, v0, v4

    .line 971
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v0

    aput v3, v0, v4

    .line 973
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 974
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->showLoadingDialog:Landroid/os/Handler;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$12(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 975
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v1

    aget v1, v1, v4

    #calls: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleLoadMoviesTask(I)V
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$13(Lcom/flixster/android/activity/hc/NetflixQueueFragment;I)V

    goto :goto_0

    .line 979
    :pswitch_1
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.navListener - instant"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/instant"

    const-string v2, "Netflix Instant"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iput v5, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    .line 982
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 983
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$4(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v1

    #setter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$10(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V

    .line 985
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mInstantAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iput-object v1, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    .line 986
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$8(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 988
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aget-boolean v0, v0, v5

    if-eqz v0, :cond_2

    .line 989
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 990
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v0

    aput v3, v0, v5

    .line 991
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v0

    aput v3, v0, v5

    .line 993
    :cond_2
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 994
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->showLoadingDialog:Landroid/os/Handler;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$12(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 995
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v1

    aget v1, v1, v5

    #calls: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleLoadMoviesTask(I)V
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$13(Lcom/flixster/android/activity/hc/NetflixQueueFragment;I)V

    goto/16 :goto_0

    .line 999
    :pswitch_2
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.navListener - saved"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/saved"

    const-string v2, "Netflix Saved"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iput v6, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    .line 1002
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1003
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$5(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v1

    #setter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$10(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V

    .line 1005
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mSavedAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iput-object v1, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    .line 1006
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$8(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1008
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aget-boolean v0, v0, v6

    if-eqz v0, :cond_3

    .line 1009
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1010
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v0

    aput v3, v0, v6

    .line 1011
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v0

    aput v3, v0, v6

    .line 1013
    :cond_3
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1014
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->showLoadingDialog:Landroid/os/Handler;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$12(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1015
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v1

    aget v1, v1, v6

    #calls: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleLoadMoviesTask(I)V
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$13(Lcom/flixster/android/activity/hc/NetflixQueueFragment;I)V

    goto/16 :goto_0

    .line 1020
    :pswitch_3
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.navListener - saved"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/athome"

    const-string v2, "Netflix AtHome"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iput v7, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    .line 1023
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1024
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$6(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v1

    #setter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$10(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V

    .line 1026
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAtHomeAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iput-object v1, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    .line 1027
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$8(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1028
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aget-boolean v0, v0, v7

    if-eqz v0, :cond_4

    .line 1029
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1030
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v0

    aput v3, v0, v7

    .line 1031
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v0

    aput v3, v0, v7

    .line 1033
    :cond_4
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1034
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->showLoadingDialog:Landroid/os/Handler;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$12(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1035
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;->this$0:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    #getter for: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I

    move-result-object v1

    aget v1, v1, v7

    #calls: Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleLoadMoviesTask(I)V
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->access$13(Lcom/flixster/android/activity/hc/NetflixQueueFragment;I)V

    goto/16 :goto_0

    .line 958
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
