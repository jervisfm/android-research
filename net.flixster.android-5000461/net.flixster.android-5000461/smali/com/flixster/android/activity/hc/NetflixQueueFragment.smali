.class public Lcom/flixster/android/activity/hc/NetflixQueueFragment;
.super Lcom/flixster/android/activity/hc/FlixsterFragment;
.source "NetflixQueueFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final DIALOG_LOADING_MOVIES:I = 0x1

.field private static final DIALOG_NETWORKFAIL:I = 0x2

.field public static final MORE_CLICKFORMORE:I = 0x2

.field public static final MORE_EMPTYQUEUE:I = 0x1

.field public static final MORE_ENDOFQUEUE:I = 0x3

.field public static final MORE_UNKNOWN:I = 0x0

.field public static final NAV_ATHOME:I = 0x4

.field public static final NAV_DVD:I = 0x1

.field public static final NAV_INSTANT:I = 0x2

.field public static final NAV_NONE:I = 0x0

.field public static final NAV_SAVED:I = 0x3


# instance fields
.field private initialContentLoadingHandler:Landroid/os/Handler;

.field mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

.field mAtHomeAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

.field protected mCheckedMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field mDiscAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

.field private mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

.field private mEditMode:Z

.field mInstantAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

.field public mIsMoreVisible:Z

.field public mLastPosition:I

.field public mLayoutInflater:Landroid/view/LayoutInflater;

.field private mLogoNetflixItemHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mMoreIndexSelect:[I

.field private mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mMoreNetflixQueueDiscItemHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mMoreNetflixQueueInstantItemHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mMoreNetflixQueueSavedItemHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public mMoreStateSelect:[I

.field private mNavListener:Landroid/view/View$OnClickListener;

.field public mNavSelect:I

.field private mNavbar:Lcom/flixster/android/view/SubNavBar;

.field private mNavbarHolder:Landroid/widget/LinearLayout;

.field private mNetflixContextMenu:Landroid/widget/RelativeLayout;

.field private mNetflixList:Landroid/widget/ListView;

.field private mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNetflixQueueDiscItemHashList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNetflixQueueInstantItemHashList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNetflixQueueSavedItemHashList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNetflixQueueSelectedItemHashList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOffsetSelect:[I

.field private mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

.field private mRemoveQueueButton:Landroid/widget/Button;

.field mSavedAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

.field private mTimer:Ljava/util/Timer;

.field private postMovieChangeHandler:Landroid/os/Handler;

.field private removeLoadingDialog:Landroid/os/Handler;

.field private removeNetworkFailDialog:Landroid/os/Handler;

.field private showLoadingDialog:Landroid/os/Handler;

.field private showNetworkFailDialog:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;-><init>()V

    .line 64
    iput v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLastPosition:I

    .line 76
    iput-boolean v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mEditMode:Z

    .line 77
    iput-boolean v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mIsMoreVisible:Z

    .line 89
    iput-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    .line 90
    iput-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    .line 91
    iput-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    .line 92
    iput-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    .line 93
    iput-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;

    .line 96
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreStateSelect:[I

    .line 97
    iput-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    .line 98
    iput-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    .line 265
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$1;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

    .line 441
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$2;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

    .line 550
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$3;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    .line 711
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$4;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->postMovieChangeHandler:Landroid/os/Handler;

    .line 729
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$5;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$5;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->showLoadingDialog:Landroid/os/Handler;

    .line 744
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$6;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$6;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->removeLoadingDialog:Landroid/os/Handler;

    .line 759
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$7;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$7;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->showNetworkFailDialog:Landroid/os/Handler;

    .line 774
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$8;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$8;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->removeNetworkFailDialog:Landroid/os/Handler;

    .line 956
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$9;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavListener:Landroid/view/View$OnClickListener;

    .line 54
    return-void
.end method

.method private ScheduleDiscDelete(Ljava/lang/String;)V
    .locals 4
    .parameter "netflixDeleteId"

    .prologue
    .line 384
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$11;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/lang/String;)V

    .line 425
    .local v0, discDelete:Ljava/util/TimerTask;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixQueuePage mTimer:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 427
    return-void
.end method

.method private ScheduleDiscMove(Ljava/lang/String;I)V
    .locals 4
    .parameter "netflixMovieId"
    .parameter "to"

    .prologue
    .line 340
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;

    invoke-direct {v0, p0, p1, p2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$10;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/lang/String;I)V

    .line 379
    .local v0, discMove:Ljava/util/TimerTask;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixQueuePage mTimer:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 381
    return-void
.end method

.method private ScheduleLoadMoviesTask(I)V
    .locals 5
    .parameter "offset"

    .prologue
    .line 451
    iget v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    .line 452
    .local v0, currNavSelect:I
    new-instance v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;

    invoke-direct {v1, p0, v0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$12;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;I)V

    .line 547
    .local v1, loadNetflixMovies:Ljava/util/TimerTask;
    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mTimer:Ljava/util/Timer;

    const-wide/16 v3, 0x64

    invoke-virtual {v2, v1, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 548
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I
    .locals 1
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$10(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 89
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$11(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)[I
    .locals 1
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    return-object v0
.end method

.method static synthetic access$12(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 729
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->showLoadingDialog:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$13(Lcom/flixster/android/activity/hc/NetflixQueueFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 450
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleLoadMoviesTask(I)V

    return-void
.end method

.method static synthetic access$14(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 429
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->deleteItem(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$15(Lcom/flixster/android/activity/hc/NetflixQueueFragment;ILjava/lang/String;I)Ljava/util/List;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 603
    invoke-direct {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getDvdQueue(ILjava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$16(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$17(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/Map;
    .locals 1
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$18(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$19(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/Map;
    .locals 1
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 339
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleDiscMove(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$20(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 92
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$21(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/Map;
    .locals 1
    .parameter

    .prologue
    .line 108
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$22(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 93
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$23(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/Map;
    .locals 1
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$24(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 711
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->postMovieChangeHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$25(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 550
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$26(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 774
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->removeNetworkFailDialog:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 744
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->removeLoadingDialog:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixContextMenu:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private declared-synchronized deleteItem(Ljava/lang/String;)V
    .locals 4
    .parameter "netflixDeleteId"

    .prologue
    .line 430
    monitor-enter p0

    :try_start_0
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixQueuePage.deleteItem() pre netflixDeleteId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 431
    const-string v3, " mNetflixQueueItemHashList.size():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 430
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 433
    .local v0, netflixDeleteMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 436
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->postMovieChangeHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 437
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixQueuePage.deleteItem() post mNetflixQueueItemHashList.size():"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 438
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 437
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    monitor-exit p0

    return-void

    .line 430
    .end local v0           #netflixDeleteMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized getDvdQueue(ILjava/lang/String;I)Ljava/util/List;
    .locals 9
    .parameter "offset"
    .parameter "qtype"
    .parameter "currNavSelect"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 604
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 607
    .local v1, list:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 612
    .local v3, netflixQueueItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NetflixQueueItem;>;"
    :try_start_1
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() about to fetch "

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    invoke-static {p1, p2}, Lnet/flixster/android/data/NetflixDao;->fetchQueue(ILjava/lang/String;)Lnet/flixster/android/model/NetflixQueue;

    move-result-object v5

    .line 614
    .local v5, queue:Lnet/flixster/android/model/NetflixQueue;
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() about to post fetch "

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    invoke-virtual {v5}, Lnet/flixster/android/model/NetflixQueue;->getNetflixQueueItemList()Ljava/util/ArrayList;

    move-result-object v3

    .line 616
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 626
    if-nez p1, :cond_0

    .line 628
    packed-switch p3, :pswitch_data_0

    .line 678
    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    iget-object v7, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    aget v7, v7, p3

    add-int/lit8 v7, v7, 0x19

    aput v7, v6, p3

    .line 682
    invoke-virtual {v5}, Lnet/flixster/android/model/NetflixQueue;->getNumberOfResults()I

    move-result v6

    if-nez v6, :cond_2

    .line 683
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreStateSelect:[I

    const/4 v7, 0x1

    aput v7, v6, p3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 708
    .end local v1           #list:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v5           #queue:Lnet/flixster/android/model/NetflixQueue;
    :goto_2
    monitor-exit p0

    return-object v1

    .line 616
    .restart local v1       #list:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .restart local v5       #queue:Lnet/flixster/android/model/NetflixQueue;
    :cond_1
    :try_start_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/NetflixQueueItem;

    .line 618
    .local v4, nqi:Lnet/flixster/android/model/NetflixQueueItem;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 619
    .local v2, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v7, "type"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    const-string v7, "netflixQueueItem"

    invoke-interface {v2, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 621
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 693
    .end local v2           #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4           #nqi:Lnet/flixster/android/model/NetflixQueueItem;
    .end local v5           #queue:Lnet/flixster/android/model/NetflixQueue;
    :catch_0
    move-exception v0

    .line 694
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    :try_start_3
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() OAuthMessageSignerException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 706
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :goto_3
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue - ioex"

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 708
    const/4 v1, 0x0

    goto :goto_2

    .line 630
    .restart local v5       #queue:Lnet/flixster/android/model/NetflixQueue;
    :pswitch_0
    :try_start_4
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;

    .line 631
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;

    const-string v7, "offset"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 632
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 633
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 634
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    .line 635
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_1

    .line 695
    .end local v5           #queue:Lnet/flixster/android/model/NetflixQueue;
    :catch_1
    move-exception v0

    .line 696
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :try_start_5
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() OAuthExpectationFailedException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 604
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    .end local v1           #list:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v3           #netflixQueueItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NetflixQueueItem;>;"
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 639
    .restart local v1       #list:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .restart local v3       #netflixQueueItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NetflixQueueItem;>;"
    .restart local v5       #queue:Lnet/flixster/android/model/NetflixQueue;
    :pswitch_1
    :try_start_6
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;

    .line 640
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;

    const-string v7, "offset"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 642
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 643
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    .line 644
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 645
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_1

    .line 697
    .end local v5           #queue:Lnet/flixster/android/model/NetflixQueue;
    :catch_2
    move-exception v0

    .line 698
    .local v0, e:Ljava/io/IOException;
    :try_start_7
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() IOException network error"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 699
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->removeLoadingDialog:Landroid/os/Handler;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 700
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->showNetworkFailDialog:Landroid/os/Handler;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    .line 648
    .end local v0           #e:Ljava/io/IOException;
    .restart local v5       #queue:Lnet/flixster/android/model/NetflixQueue;
    :pswitch_2
    :try_start_8
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;

    .line 649
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;

    const-string v7, "offset"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 650
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 651
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 652
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    .line 653
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 654
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_1

    .line 702
    .end local v5           #queue:Lnet/flixster/android/model/NetflixQueue;
    :catch_3
    move-exception v0

    .line 703
    .local v0, e:Ljava/lang/Exception;
    :try_start_9
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() Exception"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_3

    .line 658
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v5       #queue:Lnet/flixster/android/model/NetflixQueue;
    :pswitch_3
    :try_start_a
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;

    .line 659
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;

    const-string v7, "offset"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 660
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 661
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 662
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    .line 663
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 664
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLogoNetflixItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 684
    :cond_2
    invoke-virtual {v5}, Lnet/flixster/android/model/NetflixQueue;->getNumberOfResults()I

    move-result v6

    iget-object v7, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    aget v7, v7, p3

    if-gt v6, v7, :cond_3

    .line 685
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreStateSelect:[I

    const/4 v7, 0x3

    aput v7, v6, p3

    goto/16 :goto_2

    .line 689
    :cond_3
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreStateSelect:[I

    const/4 v7, 0x2

    aput v7, v6, p3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_a .. :try_end_a} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    goto/16 :goto_2

    .line 628
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x6

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/MovieDetailsFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 123
    new-instance v0, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;-><init>()V

    .line 124
    .local v0, df:Lcom/flixster/android/activity/hc/MovieDetailsFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/MovieDetailsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 125
    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12
    .parameter "view"

    .prologue
    .line 848
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 950
    const-string v8, "FlxMain"

    const-string v9, "NetflixQueuePage.onClick (default)"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    :cond_0
    :goto_0
    return-void

    .line 851
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 852
    .local v0, hashMapItem:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v8, "netflixQueueItem"

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lnet/flixster/android/model/NetflixQueueItem;

    .line 853
    .local v6, netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    const-string v8, "id"

    invoke-virtual {v6, v8}, Lnet/flixster/android/model/NetflixQueueItem;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 855
    .local v7, urlId:Ljava/lang/String;
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v8, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    .line 857
    .local v2, isChecked:Z
    if-eqz v7, :cond_1

    .line 858
    if-eqz v2, :cond_2

    .line 859
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v8, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 865
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v8

    if-nez v8, :cond_3

    .line 866
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixContextMenu:Landroid/widget/RelativeLayout;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 861
    :cond_2
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v8, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 868
    :cond_3
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixContextMenu:Landroid/widget/RelativeLayout;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 873
    .end local v0           #hashMapItem:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2           #isChecked:Z
    .end local v6           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    .end local v7           #urlId:Ljava/lang/String;
    :sswitch_1
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 875
    .local v5, netflixId:Ljava/lang/String;
    invoke-direct {p0, v5}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleDiscDelete(Ljava/lang/String;)V

    goto :goto_2

    .line 881
    .end local v5           #netflixId:Ljava/lang/String;
    :sswitch_2
    const-string v8, "FlxMain"

    const-string v9, "Fire up movie details intent"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 883
    check-cast v3, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;

    .line 887
    .local v3, itemHolder:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;
    iget-object v4, v3, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movie:Lnet/flixster/android/model/Movie;

    .line 894
    .local v4, movie:Lnet/flixster/android/model/Movie;
    new-instance v1, Landroid/content/Intent;

    const-string v8, "DETAILS"

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const-class v11, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v1, v8, v9, v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 895
    .local v1, i:Landroid/content/Intent;
    const-string v8, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v9

    invoke-virtual {v1, v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 897
    const-string v8, "title"

    const-string v9, "title"

    invoke-virtual {v4, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 900
    iget v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    .line 901
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    const/4 v9, 0x2

    const/4 v10, 0x1

    aput-boolean v10, v8, v9

    .line 902
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 903
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    const/4 v9, 0x2

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 904
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    const/4 v9, 0x2

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 905
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    const/4 v9, 0x3

    const/4 v10, 0x1

    aput-boolean v10, v8, v9

    .line 906
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 907
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    const/4 v9, 0x3

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 908
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    const/4 v9, 0x3

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 929
    :cond_4
    :goto_3
    iget v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLastPosition:I

    iget v9, v3, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->position:I

    if-le v8, v9, :cond_8

    .line 930
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    check-cast v8, Lcom/flixster/android/activity/hc/Main;

    const-class v9, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const/4 v10, 0x3

    .line 931
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    .line 930
    invoke-virtual {v8, v1, v9, v10, v11}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    .line 936
    :cond_5
    :goto_4
    iget v8, v3, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->position:I

    iput v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLastPosition:I

    goto/16 :goto_0

    .line 909
    :cond_6
    iget v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_7

    .line 910
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    const/4 v9, 0x1

    const/4 v10, 0x1

    aput-boolean v10, v8, v9

    .line 911
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 912
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 913
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 914
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    const/4 v9, 0x3

    const/4 v10, 0x1

    aput-boolean v10, v8, v9

    .line 915
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 916
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    const/4 v9, 0x3

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 917
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    const/4 v9, 0x3

    const/4 v10, 0x0

    aput v10, v8, v9

    goto :goto_3

    .line 918
    :cond_7
    iget v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_4

    .line 919
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    const/4 v9, 0x2

    const/4 v10, 0x1

    aput-boolean v10, v8, v9

    .line 920
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 921
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    const/4 v9, 0x2

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 922
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    const/4 v9, 0x2

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 923
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    const/4 v9, 0x1

    const/4 v10, 0x1

    aput-boolean v10, v8, v9

    .line 924
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 925
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 926
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    goto/16 :goto_3

    .line 932
    :cond_8
    iget v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLastPosition:I

    iget v9, v3, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->position:I

    if-ge v8, v9, :cond_5

    .line 933
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    check-cast v8, Lcom/flixster/android/activity/hc/Main;

    const-class v9, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    const/4 v10, 0x1

    .line 934
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    .line 933
    invoke-virtual {v8, v1, v9, v10, v11}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;ILjava/lang/Class;)V

    goto/16 :goto_4

    .line 939
    .end local v1           #i:Landroid/content/Intent;
    .end local v3           #itemHolder:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;
    .end local v4           #movie:Lnet/flixster/android/model/Movie;
    :sswitch_3
    const-string v8, "FlxMain"

    const-string v9, "NetflixQueuePage.onClick (MORE)"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->showLoadingDialog:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 941
    iget-object v8, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    iget v9, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    aget v8, v8, v9

    invoke-direct {p0, v8}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleLoadMoviesTask(I)V

    goto/16 :goto_0

    .line 944
    :sswitch_4
    const-string v8, "FlxMain"

    const-string v9, "NetflixQueuePage.onClick (LOGO)"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    new-instance v1, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    const-string v9, "http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817"

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v1, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 946
    .restart local v1       #i:Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 848
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f030069 -> :sswitch_4
        0x7f03006a -> :sswitch_3
        0x7f07012e -> :sswitch_2
        0x7f070130 -> :sswitch_2
        0x7f0701f9 -> :sswitch_1
        0x7f0701fb -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "dialog"

    .prologue
    const/4 v3, 0x1

    .line 798
    packed-switch p1, :pswitch_data_0

    .line 841
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 800
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 801
    .local v0, progressDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 802
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 803
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 804
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 805
    new-instance v1, Lcom/flixster/android/activity/hc/NetflixQueueFragment$13;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$13;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 814
    .end local v0           #progressDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 815
    const-string v2, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 816
    const-string v2, "Network Error"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 817
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 818
    const-string v2, "Retry"

    new-instance v3, Lcom/flixster/android/activity/hc/NetflixQueueFragment$14;

    invoke-direct {v3, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$14;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 828
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 829
    new-instance v3, Lcom/flixster/android/activity/hc/NetflixQueueFragment$15;

    invoke-direct {v3, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment$15;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;)V

    .line 828
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 839
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 798
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 130
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 131
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030067

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 134
    .local v6, view:Landroid/view/View;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    .line 136
    const v0, 0x7f0701fa

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    .line 138
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 139
    iget-boolean v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mEditMode:Z

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    check-cast v0, Lnet/flixster/android/model/TouchInterceptor;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

    invoke-virtual {v0, v1}, Lnet/flixster/android/model/TouchInterceptor;->setDropListener(Lnet/flixster/android/model/TouchInterceptor$DropListener;)V

    .line 141
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    check-cast v0, Lnet/flixster/android/model/TouchInterceptor;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

    invoke-virtual {v0, v1}, Lnet/flixster/android/model/TouchInterceptor;->setRemoveListener(Lnet/flixster/android/model/TouchInterceptor$RemoveListener;)V

    .line 142
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 148
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 149
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    const v1, 0x7f09000b

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 150
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 153
    const v0, 0x7f0701f9

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mRemoveQueueButton:Landroid/widget/Button;

    .line 154
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mRemoveQueueButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    const v0, 0x7f0701f8

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixContextMenu:Landroid/widget/RelativeLayout;

    .line 156
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 157
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mTimer:Ljava/util/Timer;

    .line 160
    :cond_1
    new-array v0, v9, [I

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    .line 161
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    aput v7, v0, v8

    .line 162
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    aput v7, v0, v10

    .line 163
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    aput v7, v0, v11

    .line 164
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    const/4 v1, 0x4

    aput v7, v0, v1

    .line 168
    new-array v0, v9, [I

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    .line 169
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    aput v7, v0, v8

    .line 170
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    aput v7, v0, v10

    .line 171
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    aput v7, v0, v11

    .line 172
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    const/4 v1, 0x4

    aput v7, v0, v1

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    .line 178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;

    .line 179
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    .line 183
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    const v3, 0x7f03005c

    .line 184
    new-array v4, v9, [Ljava/lang/String;

    const-string v1, "movieTitle"

    aput-object v1, v4, v7

    const-string v1, "movieActors"

    aput-object v1, v4, v8

    const-string v1, "movieMeta"

    aput-object v1, v4, v10

    const-string v1, "movieRelease"

    aput-object v1, v4, v11

    const/4 v1, 0x4

    const-string v5, "movieThumbnail"

    aput-object v5, v4, v1

    new-array v5, v9, [I

    fill-array-data v5, :array_0

    move-object v1, p0

    .line 185
    invoke-direct/range {v0 .. v5}, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 183
    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mDiscAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    .line 186
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    .line 187
    const v3, 0x7f03005c

    new-array v4, v9, [Ljava/lang/String;

    const-string v1, "movieTitle"

    aput-object v1, v4, v7

    const-string v1, "movieActors"

    aput-object v1, v4, v8

    const-string v1, "movieMeta"

    aput-object v1, v4, v10

    const-string v1, "movieRelease"

    aput-object v1, v4, v11

    const/4 v1, 0x4

    .line 188
    const-string v5, "movieThumbnail"

    aput-object v5, v4, v1

    new-array v5, v9, [I

    fill-array-data v5, :array_1

    move-object v1, p0

    .line 189
    invoke-direct/range {v0 .. v5}, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 186
    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mInstantAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    .line 190
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    const v3, 0x7f03005c

    .line 191
    new-array v4, v9, [Ljava/lang/String;

    const-string v1, "movieTitle"

    aput-object v1, v4, v7

    const-string v1, "movieActors"

    aput-object v1, v4, v8

    const-string v1, "movieMeta"

    aput-object v1, v4, v10

    const-string v1, "movieRelease"

    aput-object v1, v4, v11

    const/4 v1, 0x4

    const-string v5, "movieThumbnail"

    aput-object v5, v4, v1

    new-array v5, v9, [I

    fill-array-data v5, :array_2

    move-object v1, p0

    .line 192
    invoke-direct/range {v0 .. v5}, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 190
    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mSavedAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    .line 193
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;

    .line 194
    const v3, 0x7f03005c

    new-array v4, v9, [Ljava/lang/String;

    const-string v1, "movieTitle"

    aput-object v1, v4, v7

    const-string v1, "movieActors"

    aput-object v1, v4, v8

    const-string v1, "movieMeta"

    aput-object v1, v4, v10

    const-string v1, "movieRelease"

    aput-object v1, v4, v11

    const/4 v1, 0x4

    .line 195
    const-string v5, "movieThumbnail"

    aput-object v5, v4, v1

    new-array v5, v9, [I

    fill-array-data v5, :array_3

    move-object v1, p0

    .line 196
    invoke-direct/range {v0 .. v5}, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 193
    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAtHomeAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    .line 198
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mDiscAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mDiscAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 201
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mDiscAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mAdapterSelected:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    .line 205
    :cond_2
    const v0, 0x7f0701f7

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    .line 206
    new-instance v0, Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    .line 207
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavListener:Landroid/view/View$OnClickListener;

    const v2, 0x7f0c00e1

    const v3, 0x7f0c00d6

    const v4, 0x7f0c00d5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;III)V

    .line 208
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    const v1, 0x7f070258

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 209
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {v0, v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 211
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v8, v0, v8

    .line 212
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v8, v0, v10

    .line 213
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v8, v0, v11

    .line 214
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    const/4 v1, 0x4

    aput-boolean v8, v0, v1

    .line 216
    return-object v6

    .line 184
    nop

    :array_0
    .array-data 0x4
        0x8t 0x1t 0x7t 0x7ft
        0xbt 0x1t 0x7t 0x7ft
        0xct 0x1t 0x7t 0x7ft
        0xdt 0x1t 0x7t 0x7ft
        0x2et 0x1t 0x7t 0x7ft
    .end array-data

    .line 188
    :array_1
    .array-data 0x4
        0x8t 0x1t 0x7t 0x7ft
        0xbt 0x1t 0x7t 0x7ft
        0xct 0x1t 0x7t 0x7ft
        0xdt 0x1t 0x7t 0x7ft
        0x2et 0x1t 0x7t 0x7ft
    .end array-data

    .line 191
    :array_2
    .array-data 0x4
        0x8t 0x1t 0x7t 0x7ft
        0xbt 0x1t 0x7t 0x7ft
        0xct 0x1t 0x7t 0x7ft
        0xdt 0x1t 0x7t 0x7ft
        0x2et 0x1t 0x7t 0x7ft
    .end array-data

    .line 195
    :array_3
    .array-data 0x4
        0x8t 0x1t 0x7t 0x7ft
        0xbt 0x1t 0x7t 0x7ft
        0xct 0x1t 0x7t 0x7ft
        0xdt 0x1t 0x7t 0x7ft
        0x2et 0x1t 0x7t 0x7ft
    .end array-data
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 221
    invoke-super {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onResume()V

    .line 222
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mTimer:Ljava/util/Timer;

    if-nez v3, :cond_0

    .line 223
    new-instance v3, Ljava/util/Timer;

    invoke-direct {v3}, Ljava/util/Timer;-><init>()V

    iput-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mTimer:Ljava/util/Timer;

    .line 225
    :cond_0
    const-string v3, "FlxMain"

    const-string v4, "NetflixQueue.onResume()"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 227
    .local v2, uri:Landroid/net/Uri;
    if-eqz v2, :cond_1

    .line 228
    const-string v3, "oauth_token"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 229
    .local v1, oauth_token:Ljava/lang/String;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixQueue.onResume() oauth_token:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " uri:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setNetflixOAuthToken(Ljava/lang/String;)V

    .line 233
    .end local v1           #oauth_token:Ljava/lang/String;
    :cond_1
    iget v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    .line 234
    .local v0, currNavSelect:I
    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_2

    .line 235
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->showLoadingDialog:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 236
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 237
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreIndexSelect:[I

    aput v6, v3, v0

    .line 238
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    aput v6, v3, v0

    .line 239
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->invalidateViews()V

    .line 241
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mOffsetSelect:[I

    aget v3, v3, v0

    invoke-direct {p0, v3}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->ScheduleLoadMoviesTask(I)V

    .line 244
    :cond_2
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->trackPage()V

    .line 245
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 316
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 317
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 249
    iget v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    packed-switch v0, :pswitch_data_0

    .line 263
    :goto_0
    return-void

    .line 251
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/dvd"

    const-string v2, "Netflix DVDs"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 254
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/instant"

    const-string v2, "Netflix Instant"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 257
    :pswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/saved"

    const-string v2, "Netflix Saved"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 260
    :pswitch_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/athome"

    const-string v2, "Netflix AtHome"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
