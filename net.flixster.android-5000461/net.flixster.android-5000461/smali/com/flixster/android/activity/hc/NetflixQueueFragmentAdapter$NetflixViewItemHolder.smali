.class public Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;
.super Ljava/lang/Object;
.source "NetflixQueueFragmentAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NetflixViewItemHolder"
.end annotation


# instance fields
.field friendScore:Landroid/widget/TextView;

.field movie:Lnet/flixster/android/model/Movie;

.field movieActors:Landroid/widget/TextView;

.field movieDetailsLayout:Landroid/widget/LinearLayout;

.field movieHeader:Landroid/widget/TextView;

.field movieItem:Landroid/widget/RelativeLayout;

.field movieMeta:Landroid/widget/TextView;

.field moviePlayIcon:Landroid/widget/ImageView;

.field movieRelease:Landroid/widget/TextView;

.field movieScore:Landroid/widget/TextView;

.field movieThumbnail:Landroid/widget/ImageView;

.field movieTitle:Landroid/widget/TextView;

.field movielistGrip:Landroid/widget/ImageView;

.field netflixCheckBox:Landroid/widget/CheckBox;

.field position:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
