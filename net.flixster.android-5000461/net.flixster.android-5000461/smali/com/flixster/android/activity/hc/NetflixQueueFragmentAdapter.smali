.class public Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;
.super Landroid/widget/BaseAdapter;
.source "NetflixQueueFragmentAdapter.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;
    }
.end annotation


# static fields
.field public static final VIEWTYPE_FOOTER:I = 0x4

.field public static final VIEWTYPE_GETMORE:I = 0x1

.field public static final VIEWTYPE_ITEM:I = 0x0

.field public static final VIEWTYPE_NETFLIXLOGO:I = 0x2

.field public static final VIEWTYPE_SUBHEADER:I = 0x3


# instance fields
.field public data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;"
        }
    .end annotation
.end field

.field mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

.field private mInflater:Landroid/view/LayoutInflater;

.field public mNetflixEditAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

.field public refreshHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/flixster/android/activity/hc/NetflixQueueFragment;Ljava/util/List;I[Ljava/lang/String;[I)V
    .locals 1
    .parameter "fragment"
    .parameter
    .parameter "resource"
    .parameter "from"
    .parameter "to"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flixster/android/activity/hc/NetflixQueueFragment;",
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;I[",
            "Ljava/lang/String;",
            "[I)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p2, data:Ljava/util/List;,"Ljava/util/List<+Ljava/util/Map<Ljava/lang/String;*>;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 391
    new-instance v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$1;-><init>(Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->refreshHandler:Landroid/os/Handler;

    .line 57
    iput-object p2, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->data:Ljava/util/List;

    .line 58
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    .line 60
    iget-object v0, p1, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 61
    iput-object p0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mNetflixEditAdapter:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;

    .line 62
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 407
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 412
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .parameter "position"

    .prologue
    .line 80
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v1, "type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 34
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 85
    invoke-virtual/range {p0 .. p1}, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->getItemViewType(I)I

    move-result v32

    .line 86
    .local v32, type:I
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v22

    .line 87
    .local v22, movieRatingType:I
    packed-switch v32, :pswitch_data_0

    .line 352
    :goto_0
    :pswitch_0
    const-string v2, "FlxMain"

    const-string v3, "MovieListAdapter.getView(.) Bad stuff happened! (didn\'t catch the view type"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v14, p2

    .line 353
    .end local p2
    .local v14, convertView:Landroid/view/View;
    :goto_1
    return-object v14

    .line 89
    .end local v14           #convertView:Landroid/view/View;
    .restart local p2
    :pswitch_1
    if-nez p2, :cond_0

    .line 90
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030068

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 92
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;

    .line 93
    .local v33, viewHolder:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;
    if-nez v33, :cond_1

    .line 94
    new-instance v33, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;

    .end local v33           #viewHolder:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;
    invoke-direct/range {v33 .. v33}, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;-><init>()V

    .line 95
    .restart local v33       #viewHolder:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;
    const v2, 0x7f07012e

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    .line 96
    const v2, 0x7f07012f

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->moviePlayIcon:Landroid/widget/ImageView;

    .line 97
    const v2, 0x7f070108

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieTitle:Landroid/widget/TextView;

    .line 98
    const v2, 0x7f07010c

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieMeta:Landroid/widget/TextView;

    .line 99
    const v2, 0x7f07010d

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieRelease:Landroid/widget/TextView;

    .line 100
    const v2, 0x7f07010b

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieActors:Landroid/widget/TextView;

    .line 101
    const v2, 0x7f070132

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    .line 102
    const v2, 0x7f070133

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->friendScore:Landroid/widget/TextView;

    .line 103
    const v2, 0x7f070130

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieDetailsLayout:Landroid/widget/LinearLayout;

    .line 104
    const v2, 0x7f070105

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieItem:Landroid/widget/RelativeLayout;

    .line 105
    const v2, 0x7f0701fb

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    .line 106
    const v2, 0x7f0701fd

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movielistGrip:Landroid/widget/ImageView;

    .line 107
    move-object/from16 v0, p2

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 112
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->data:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/HashMap;

    .line 113
    .local v16, hashMapItem:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "netflixQueueItem"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/model/NetflixQueueItem;

    .line 116
    .local v7, netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    const-string v2, "id"

    invoke-virtual {v7, v2}, Lnet/flixster/android/model/NetflixQueueItem;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 118
    .local v24, netflixId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    packed-switch v2, :pswitch_data_1

    .line 130
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 131
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 132
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 133
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movielistGrip:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 142
    :goto_2
    const-string v2, "title"

    invoke-virtual {v7, v2}, Lnet/flixster/android/model/NetflixQueueItem;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 143
    .local v31, title:Ljava/lang/String;
    if-eqz v31, :cond_2

    .line 144
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieTitle:Landroid/widget/TextView;

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :cond_2
    iget-object v2, v7, Lnet/flixster/android/model/NetflixQueueItem;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_13

    .line 148
    iget-object v4, v7, Lnet/flixster/android/model/NetflixQueueItem;->mMovie:Lnet/flixster/android/model/Movie;

    .line 150
    .local v4, movie:Lnet/flixster/android/model/Movie;
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 153
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieDetailsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    move-object/from16 v0, v33

    iput-object v4, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movie:Lnet/flixster/android/model/Movie;

    .line 155
    move/from16 v0, p1

    move-object/from16 v1, v33

    iput v0, v1, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->position:I

    .line 156
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieDetailsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 160
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 163
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-nez v2, :cond_7

    const/16 v18, 0x0

    .line 166
    .local v18, isReleased:Z
    :goto_3
    const-string v2, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 167
    .local v11, actors:Ljava/lang/String;
    if-eqz v11, :cond_3

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_8

    .line 168
    :cond_3
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieActors:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 175
    :goto_4
    iget-object v2, v4, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/graphics/Bitmap;

    .line 176
    .local v12, b:Landroid/graphics/Bitmap;
    if-eqz v12, :cond_9

    .line 177
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 206
    .end local v7           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    :cond_4
    :goto_5
    const/4 v13, 0x0

    .line 208
    .local v13, bNoScore:Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 209
    .local v25, resources:Landroid/content/res/Resources;
    packed-switch v22, :pswitch_data_2

    .line 259
    :goto_6
    if-eqz v13, :cond_5

    .line 260
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    :cond_5
    const-string v2, "meta"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 265
    .local v20, meta:Ljava/lang/String;
    if-eqz v20, :cond_12

    .line 266
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieMeta:Landroid/widget/TextView;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieMeta:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .end local v4           #movie:Lnet/flixster/android/model/Movie;
    .end local v11           #actors:Ljava/lang/String;
    .end local v13           #bNoScore:Z
    .end local v18           #isReleased:Z
    .end local v20           #meta:Ljava/lang/String;
    .end local v25           #resources:Landroid/content/res/Resources;
    :cond_6
    :goto_7
    move-object/from16 v14, p2

    .line 310
    .end local p2
    .restart local v14       #convertView:Landroid/view/View;
    goto/16 :goto_1

    .line 120
    .end local v12           #b:Landroid/graphics/Bitmap;
    .end local v14           #convertView:Landroid/view/View;
    .end local v31           #title:Ljava/lang/String;
    .restart local v7       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    .restart local p2
    :pswitch_2
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mCheckedMap:Ljava/util/HashMap;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 121
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 122
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 123
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movielistGrip:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 126
    :pswitch_3
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 127
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movielistGrip:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 163
    .restart local v4       #movie:Lnet/flixster/android/model/Movie;
    .restart local v31       #title:Ljava/lang/String;
    :cond_7
    const/16 v18, 0x1

    goto/16 :goto_3

    .line 170
    .restart local v11       #actors:Ljava/lang/String;
    .restart local v18       #isReleased:Z
    :cond_8
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieActors:Landroid/widget/TextView;

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieActors:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 179
    .restart local v12       #b:Landroid/graphics/Bitmap;
    :cond_9
    const-string v2, "thumbnail"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 180
    .local v8, thumbnailUrl:Ljava/lang/String;
    if-eqz v8, :cond_a

    const-string v2, "http"

    invoke-virtual {v8, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 182
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f020154

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 183
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v29

    .line 184
    .local v29, timeStamp:J
    move-wide/from16 v0, v29

    long-to-double v2, v0

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getThumbOrderStamp()J

    move-result-wide v5

    long-to-double v5, v5

    const-wide v9, 0x41e65a0bc0000000L

    add-double/2addr v5, v9

    cmpl-double v2, v2, v5

    if-lez v2, :cond_4

    .line 185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/flixster/android/activity/hc/Main;

    new-instance v2, Lnet/flixster/android/model/ImageOrder;

    const/4 v3, 0x0

    .line 186
    const-string v5, "thumbnail"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v33

    iget-object v6, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    .line 187
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->refreshHandler:Landroid/os/Handler;

    .end local v7           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 185
    invoke-virtual {v9, v2}, Lcom/flixster/android/activity/hc/Main;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 188
    move-wide/from16 v0, v29

    invoke-virtual {v4, v0, v1}, Lnet/flixster/android/model/Movie;->setThumbOrderStamp(J)V

    goto/16 :goto_5

    .line 191
    .end local v29           #timeStamp:J
    .restart local v7       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    :cond_a
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f02014f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    .line 213
    .end local v7           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    .end local v8           #thumbnailUrl:Ljava/lang/String;
    .restart local v13       #bNoScore:Z
    .restart local v25       #resources:Landroid/content/res/Resources;
    :pswitch_4
    const-string v2, "popcornScore"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 214
    const-string v2, "popcornScore"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 215
    .local v15, flixsterScore:I
    const/16 v2, 0x3c

    if-ge v15, v2, :cond_c

    const/16 v19, 0x1

    .line 217
    .local v19, isSpilled:Z
    :goto_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v15}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    .line 218
    .local v27, scoreBuilder:Ljava/lang/StringBuilder;
    if-nez v18, :cond_b

    .line 219
    const-string v2, " "

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0c0092

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    :cond_b
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    if-nez v18, :cond_d

    const v17, 0x7f0200f6

    .line 225
    .local v17, iconId:I
    :goto_9
    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v28

    .line 227
    .local v28, scoreIcon:Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v3, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 228
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v2, v0, v3, v5, v6}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 229
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 215
    .end local v17           #iconId:I
    .end local v19           #isSpilled:Z
    .end local v27           #scoreBuilder:Ljava/lang/StringBuilder;
    .end local v28           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_c
    const/16 v19, 0x0

    goto :goto_8

    .line 224
    .restart local v19       #isSpilled:Z
    .restart local v27       #scoreBuilder:Ljava/lang/StringBuilder;
    :cond_d
    if-eqz v19, :cond_e

    const v17, 0x7f0200ef

    goto :goto_9

    :cond_e
    const v17, 0x7f0200ea

    goto :goto_9

    .line 231
    .end local v15           #flixsterScore:I
    .end local v19           #isSpilled:Z
    .end local v27           #scoreBuilder:Ljava/lang/StringBuilder;
    :cond_f
    const/4 v13, 0x1

    .line 233
    goto/16 :goto_6

    .line 237
    :pswitch_5
    const-string v2, "rottenTomatoes"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 238
    const-string v2, "rottenTomatoes"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v26

    .line 239
    .local v26, rottenScore:I
    const/16 v2, 0x3c

    move/from16 v0, v26

    if-ge v0, v2, :cond_10

    .line 240
    const v2, 0x7f0200ed

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v28

    .line 245
    .restart local v28       #scoreIcon:Landroid/graphics/drawable/Drawable;
    :goto_a
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v3, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 246
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v2, v0, v3, v5, v6}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 247
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 242
    .end local v28           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_10
    const v2, 0x7f0200de

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v28

    .restart local v28       #scoreIcon:Landroid/graphics/drawable/Drawable;
    goto :goto_a

    .line 250
    .end local v26           #rottenScore:I
    .end local v28           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_11
    const/4 v13, 0x1

    .line 252
    goto/16 :goto_6

    .line 256
    :pswitch_6
    const/4 v13, 0x1

    goto/16 :goto_6

    .line 269
    .restart local v20       #meta:Ljava/lang/String;
    :cond_12
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieMeta:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 275
    .end local v4           #movie:Lnet/flixster/android/model/Movie;
    .end local v11           #actors:Ljava/lang/String;
    .end local v12           #b:Landroid/graphics/Bitmap;
    .end local v13           #bNoScore:Z
    .end local v18           #isReleased:Z
    .end local v20           #meta:Ljava/lang/String;
    .end local v25           #resources:Landroid/content/res/Resources;
    .restart local v7       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    :cond_13
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 276
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieActors:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 277
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieMeta:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 278
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f02014f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 280
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieDetailsLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    iget-object v12, v7, Lnet/flixster/android/model/NetflixQueueItem;->thumbnailBitmap:Landroid/graphics/Bitmap;

    .line 286
    .restart local v12       #b:Landroid/graphics/Bitmap;
    if-eqz v12, :cond_14

    .line 287
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_7

    .line 289
    :cond_14
    const-string v2, "box_art"

    invoke-virtual {v7, v2}, Lnet/flixster/android/model/NetflixQueueItem;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 293
    .restart local v8       #thumbnailUrl:Ljava/lang/String;
    if-eqz v8, :cond_15

    const-string v2, "http"

    invoke-virtual {v8, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 295
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f020154

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 296
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v29

    .line 297
    .restart local v29       #timeStamp:J
    move-wide/from16 v0, v29

    long-to-double v2, v0

    invoke-virtual {v7}, Lnet/flixster/android/model/NetflixQueueItem;->getThumbOrderStamp()J

    move-result-wide v5

    long-to-double v5, v5

    const-wide v9, 0x41e65a0bc0000000L

    add-double/2addr v5, v9

    cmpl-double v2, v2, v5

    if-lez v2, :cond_6

    .line 298
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    new-instance v5, Lnet/flixster/android/model/ImageOrder;

    .line 299
    const/4 v6, 0x7

    .line 300
    move-object/from16 v0, v33

    iget-object v9, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->refreshHandler:Landroid/os/Handler;

    invoke-direct/range {v5 .. v10}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 298
    invoke-virtual {v2, v5}, Lcom/flixster/android/activity/hc/Main;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 301
    move-wide/from16 v0, v29

    invoke-virtual {v7, v0, v1}, Lnet/flixster/android/model/NetflixQueueItem;->setThumbOrderStamp(J)V

    goto/16 :goto_7

    .line 304
    .end local v29           #timeStamp:J
    :cond_15
    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f02014f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    .line 312
    .end local v7           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    .end local v8           #thumbnailUrl:Ljava/lang/String;
    .end local v12           #b:Landroid/graphics/Bitmap;
    .end local v16           #hashMapItem:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v24           #netflixId:Ljava/lang/String;
    .end local v31           #title:Ljava/lang/String;
    .end local v33           #viewHolder:Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter$NetflixViewItemHolder;
    :pswitch_7
    if-nez p2, :cond_16

    .line 313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03006a

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 315
    :cond_16
    const v2, 0x7f03006a

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    .line 316
    const v2, 0x7f070201

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 317
    .local v21, moreMessage:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget v0, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mNavSelect:I

    move/from16 v23, v0

    .line 318
    .local v23, navSelect:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->mMoreStateSelect:[I

    aget v2, v2, v23

    packed-switch v2, :pswitch_data_3

    :goto_b
    move-object/from16 v14, p2

    .line 338
    .end local p2
    .restart local v14       #convertView:Landroid/view/View;
    goto/16 :goto_1

    .line 320
    .end local v14           #convertView:Landroid/view/View;
    .restart local p2
    :pswitch_8
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    const v2, 0x7f0c00d8

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 322
    const v2, -0x777778

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_b

    .line 325
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    const v2, 0x7f0c00d9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_b

    .line 329
    :pswitch_a
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    const/16 v2, 0x8

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_b

    .line 340
    .end local v21           #moreMessage:Landroid/widget/TextView;
    .end local v23           #navSelect:I
    :pswitch_b
    if-nez p2, :cond_17

    .line 341
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030069

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 343
    :cond_17
    const v2, 0x7f030069

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    .line 344
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v14, p2

    .line 345
    .end local p2
    .restart local v14       #convertView:Landroid/view/View;
    goto/16 :goto_1

    .line 347
    .end local v14           #convertView:Landroid/view/View;
    .restart local p2
    :pswitch_c
    new-instance p2, Landroid/widget/TextView;

    .end local p2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->mFragment:Lcom/flixster/android/activity/hc/NetflixQueueFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/NetflixQueueFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .restart local p2
    move-object/from16 v2, p2

    .line 348
    check-cast v2, Landroid/widget/TextView;

    const-string v3, "footer"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_b
        :pswitch_0
        :pswitch_c
    .end packed-switch

    .line 118
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 209
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 318
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x5

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .parameter "position"

    .prologue
    .line 70
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NetflixQueueFragmentAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v1, "type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
