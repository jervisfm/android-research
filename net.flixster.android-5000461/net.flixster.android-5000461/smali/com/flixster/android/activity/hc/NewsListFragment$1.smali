.class Lcom/flixster/android/activity/hc/NewsListFragment$1;
.super Landroid/os/Handler;
.source "NewsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NewsListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NewsListFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NewsListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NewsListFragment$1;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    .line 135
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 138
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NewsListFragment$1;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/NewsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 139
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/NewsListFragment$1;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/NewsListFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NewsListFragment$1;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NewsListFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 144
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NewsListFragment$1;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NewsListFragment;->mData:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NewsListFragment$1;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/NewsListFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 145
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NewsListFragment$1;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/NewsListFragment;->mLviListFragmentAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
