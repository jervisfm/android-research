.class Lcom/flixster/android/activity/hc/NewsListFragment$2;
.super Ljava/lang/Object;
.source "NewsListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/NewsListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NewsListFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NewsListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NewsListFragment$2;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .parameter
    .parameter "v"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NewsListFragment$2;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/NewsListFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/lvi/Lvi;

    .line 155
    .local v1, lvi:Lnet/flixster/android/lvi/Lvi;
    invoke-virtual {v1}, Lnet/flixster/android/lvi/Lvi;->getItemViewType()I

    move-result v3

    sget v4, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_NEWSITEM:I

    if-ne v3, v4, :cond_0

    .line 156
    check-cast v1, Lnet/flixster/android/lvi/LviNewsStory;

    .end local v1           #lvi:Lnet/flixster/android/lvi/Lvi;
    iget-object v2, v1, Lnet/flixster/android/lvi/LviNewsStory;->mNewsStory:Lnet/flixster/android/model/NewsStory;

    .line 157
    .local v2, ns:Lnet/flixster/android/model/NewsStory;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    iget-object v4, v2, Lnet/flixster/android/model/NewsStory;->mSource:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 158
    .local v0, critic:Landroid/content/Intent;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/NewsListFragment$2;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    invoke-virtual {v3, v0}, Lcom/flixster/android/activity/hc/NewsListFragment;->startActivity(Landroid/content/Intent;)V

    .line 161
    .end local v0           #critic:Landroid/content/Intent;
    .end local v2           #ns:Lnet/flixster/android/model/NewsStory;
    :cond_0
    return-void
.end method
