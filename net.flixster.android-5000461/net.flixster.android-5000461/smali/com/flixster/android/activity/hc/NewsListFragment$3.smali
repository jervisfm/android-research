.class Lcom/flixster/android/activity/hc/NewsListFragment$3;
.super Ljava/util/TimerTask;
.source "NewsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/NewsListFragment;->ScheduleLoadNewsItemsTask(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/NewsListFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/NewsListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    .line 77
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 80
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/NewsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    .line 81
    .local v3, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v3, :cond_0

    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/NewsListFragment;->isRemoving()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    :try_start_0
    invoke-static {}, Lnet/flixster/android/data/NewsDao;->getNewsItemList()Ljava/util/ArrayList;

    move-result-object v4

    .line 88
    .local v4, newsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    new-instance v1, Lnet/flixster/android/lvi/LviPageHeader;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviPageHeader;-><init>()V

    .line 89
    .local v1, lviPageHeader:Lnet/flixster/android/lvi/LviPageHeader;
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    invoke-virtual {v6}, Lcom/flixster/android/activity/hc/NewsListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00e0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lnet/flixster/android/lvi/LviPageHeader;->mTitle:Ljava/lang/String;

    .line 90
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget-object v6, v6, Lcom/flixster/android/activity/hc/NewsListFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget-object v6, v6, Lcom/flixster/android/activity/hc/NewsListFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 93
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget-object v6, v6, Lcom/flixster/android/activity/hc/NewsListFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    .line 108
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    #getter for: Lcom/flixster/android/activity/hc/NewsListFragment;->mUpdateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/flixster/android/activity/hc/NewsListFragment;->access$0(Lcom/flixster/android/activity/hc/NewsListFragment;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    .end local v1           #lviPageHeader:Lnet/flixster/android/lvi/LviPageHeader;
    .end local v4           #newsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    :cond_2
    :goto_2
    if-eqz v3, :cond_0

    iget-object v6, v3, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v6, :cond_0

    iget-object v6, v3, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 125
    iget-object v6, v3, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 102
    .restart local v1       #lviPageHeader:Lnet/flixster/android/lvi/LviPageHeader;
    .restart local v4       #newsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    :cond_3
    :try_start_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/NewsStory;

    .line 103
    .local v5, story:Lnet/flixster/android/model/NewsStory;
    new-instance v2, Lnet/flixster/android/lvi/LviNewsStory;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviNewsStory;-><init>()V

    .line 104
    .local v2, lviStory:Lnet/flixster/android/lvi/LviNewsStory;
    iput-object v5, v2, Lnet/flixster/android/lvi/LviNewsStory;->mNewsStory:Lnet/flixster/android/model/NewsStory;

    .line 105
    iget-object v7, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget-object v7, v7, Lcom/flixster/android/activity/hc/NewsListFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 110
    .end local v1           #lviPageHeader:Lnet/flixster/android/lvi/LviPageHeader;
    .end local v2           #lviStory:Lnet/flixster/android/lvi/LviNewsStory;
    .end local v4           #newsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    .end local v5           #story:Lnet/flixster/android/model/NewsStory;
    :catch_0
    move-exception v0

    .line 111
    .local v0, de:Lnet/flixster/android/data/DaoException;
    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->printStackTrace()V

    .line 113
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget v7, v6, Lcom/flixster/android/activity/hc/NewsListFragment;->mRetryCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v6, Lcom/flixster/android/activity/hc/NewsListFragment;->mRetryCount:I

    .line 114
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iget v6, v6, Lcom/flixster/android/activity/hc/NewsListFragment;->mRetryCount:I

    const/4 v7, 0x3

    if-ge v6, v7, :cond_4

    .line 115
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    const/16 v7, 0x3e8

    #calls: Lcom/flixster/android/activity/hc/NewsListFragment;->ScheduleLoadNewsItemsTask(I)V
    invoke-static {v6, v7}, Lcom/flixster/android/activity/hc/NewsListFragment;->access$1(Lcom/flixster/android/activity/hc/NewsListFragment;I)V

    goto :goto_2

    .line 117
    :cond_4
    iget-object v6, p0, Lcom/flixster/android/activity/hc/NewsListFragment$3;->this$0:Lcom/flixster/android/activity/hc/NewsListFragment;

    iput v8, v6, Lcom/flixster/android/activity/hc/NewsListFragment;->mRetryCount:I

    .line 118
    if-eqz v3, :cond_2

    .line 119
    iget-object v6, v3, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2
.end method
