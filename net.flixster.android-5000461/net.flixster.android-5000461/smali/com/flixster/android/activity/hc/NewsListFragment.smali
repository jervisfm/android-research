.class public Lcom/flixster/android/activity/hc/NewsListFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "NewsListFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private mNewsStoryClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mUpdateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 135
    new-instance v0, Lcom/flixster/android/activity/hc/NewsListFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NewsListFragment$1;-><init>(Lcom/flixster/android/activity/hc/NewsListFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NewsListFragment;->mUpdateHandler:Landroid/os/Handler;

    .line 150
    new-instance v0, Lcom/flixster/android/activity/hc/NewsListFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NewsListFragment$2;-><init>(Lcom/flixster/android/activity/hc/NewsListFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/NewsListFragment;->mNewsStoryClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 32
    return-void
.end method

.method private ScheduleLoadNewsItemsTask(I)V
    .locals 4
    .parameter "delay"

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NewsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 77
    new-instance v0, Lcom/flixster/android/activity/hc/NewsListFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/NewsListFragment$3;-><init>(Lcom/flixster/android/activity/hc/NewsListFragment;)V

    .line 130
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NewsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NewsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 133
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/NewsListFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 135
    iget-object v0, p0, Lcom/flixster/android/activity/hc/NewsListFragment;->mUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/NewsListFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/hc/NewsListFragment;->ScheduleLoadNewsItemsTask(I)V

    return-void
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 165
    const/16 v0, 0x7a

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/NewsListFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 36
    new-instance v0, Lcom/flixster/android/activity/hc/NewsListFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/NewsListFragment;-><init>()V

    .line 37
    .local v0, f:Lcom/flixster/android/activity/hc/NewsListFragment;
    return-object v0
.end method


# virtual methods
.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v3, 0x1

    .line 57
    packed-switch p1, :pswitch_data_0

    .line 66
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/LviFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 59
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NewsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 60
    .local v0, loadingDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NewsListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 61
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 62
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 63
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/LviFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 44
    .local v0, view:Landroid/view/View;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/NewsListFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/NewsListFragment;->mNewsStoryClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 45
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/NewsListFragment;->trackPage()V

    .line 47
    return-object v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 72
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/flixster/android/activity/hc/NewsListFragment;->ScheduleLoadNewsItemsTask(I)V

    .line 73
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 52
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/topnews"

    const-string v2, "top news"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method
