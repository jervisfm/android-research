.class Lcom/flixster/android/activity/hc/PhotoGalleryFragment$1;
.super Landroid/os/Handler;
.source "PhotoGalleryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/PhotoGalleryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$1;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    .line 157
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 161
    iget-object v1, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$1;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 162
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$1;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    const-string v1, "FlxMain"

    const-string v2, "PhotoGalleryPage.updateHandler"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v1, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$1;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    #getter for: Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photos:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->access$0(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 168
    iget-object v1, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$1;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    #calls: Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->updatePage()V
    invoke-static {v1}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->access$1(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V

    goto :goto_0

    .line 170
    :cond_2
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/flixster/android/activity/hc/Main;->showDialog(I)V

    goto :goto_0
.end method
