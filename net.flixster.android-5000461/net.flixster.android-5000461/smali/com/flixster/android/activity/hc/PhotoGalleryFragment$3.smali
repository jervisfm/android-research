.class Lcom/flixster/android/activity/hc/PhotoGalleryFragment$3;
.super Ljava/lang/Object;
.source "PhotoGalleryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/PhotoGalleryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$3;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "view"

    .prologue
    .line 203
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Photo;

    .line 204
    .local v2, photo:Lnet/flixster/android/model/Photo;
    if-eqz v2, :cond_1

    .line 205
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$3;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    #getter for: Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->topPhotos:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->access$2(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 206
    .local v0, index:I
    if-gez v0, :cond_0

    .line 207
    const/4 v0, 0x0

    .line 209
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/photo/gallery/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$3;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    #getter for: Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->access$3(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Top Photo Page for photo:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v2, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    new-instance v1, Landroid/content/Intent;

    const-string v3, "TOP_PHOTO"

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$3;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    invoke-virtual {v5}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-class v6, Lnet/flixster/android/ScrollGalleryPage;

    invoke-direct {v1, v3, v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 212
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "KEY_PHOTO_FILTER"

    iget-object v4, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$3;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    #getter for: Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->access$3(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    const-string v3, "PHOTO_INDEX"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 215
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$3;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    invoke-virtual {v3, v1}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->startActivity(Landroid/content/Intent;)V

    .line 217
    .end local v0           #index:I
    .end local v1           #intent:Landroid/content/Intent;
    :cond_1
    return-void
.end method
