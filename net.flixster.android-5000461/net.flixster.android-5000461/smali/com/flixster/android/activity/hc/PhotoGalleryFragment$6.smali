.class Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;
.super Ljava/util/TimerTask;
.source "PhotoGalleryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    .line 134
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 136
    iget-object v2, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 137
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    #getter for: Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photos:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->access$0(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_2

    .line 143
    :try_start_0
    iget-object v2, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    #getter for: Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->access$3(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/data/PhotoDao;->getTopPhotos(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photos:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->access$6(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    #getter for: Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->updateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->access$7(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    const-string v3, "PhotoGalleryPage.scheduleUpdatePageTask:failed to get photos"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 146
    iget-object v2, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;->this$0:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    const/4 v3, 0x0

    #setter for: Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photos:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->access$6(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;Ljava/util/ArrayList;)V

    goto :goto_1
.end method
