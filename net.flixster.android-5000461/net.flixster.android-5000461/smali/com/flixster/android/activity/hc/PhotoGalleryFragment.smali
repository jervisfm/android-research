.class public Lcom/flixster/android/activity/hc/PhotoGalleryFragment;
.super Lcom/flixster/android/activity/hc/FlixsterFragment;
.source "PhotoGalleryFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field private static final DIALOG_PHOTOS:I = 0x2

.field public static final KEY_PHOTO_FILTER:Ljava/lang/String; = "KEY_PHOTO_FILTER"

.field private static final MAX_PHOTOS:I = 0x64


# instance fields
.field private filter:Ljava/lang/String;

.field private headerClickListener:Landroid/view/View$OnClickListener;

.field private mNavSelect:I

.field private mNavbar:Lcom/flixster/android/view/SubNavBar;

.field private photoClickListener:Landroid/view/View$OnClickListener;

.field private photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private photos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation
.end field

.field private topPhotos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;-><init>()V

    .line 157
    new-instance v0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$1;-><init>(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->updateHandler:Landroid/os/Handler;

    .line 195
    new-instance v0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$2;-><init>(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 201
    new-instance v0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$3;-><init>(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photoClickListener:Landroid/view/View$OnClickListener;

    .line 220
    new-instance v0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$4;-><init>(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->headerClickListener:Landroid/view/View$OnClickListener;

    .line 35
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photos:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->updatePage()V

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->topPhotos:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 46
    iput p1, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->mNavSelect:I

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photos:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 244
    const/16 v0, 0x78

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/PhotoGalleryFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 52
    new-instance v0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;-><init>()V

    .line 53
    .local v0, f:Lcom/flixster/android/activity/hc/PhotoGalleryFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 134
    new-instance v0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$6;-><init>(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V

    .line 152
    .local v0, updatePageTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 155
    :cond_0
    return-void
.end method

.method private updateHeader()V
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->mNavSelect:I

    packed-switch v0, :pswitch_data_0

    .line 241
    :goto_0
    return-void

    .line 232
    :pswitch_0
    const-string v0, "top-actresses"

    iput-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;

    goto :goto_0

    .line 235
    :pswitch_1
    const-string v0, "top-actors"

    iput-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;

    goto :goto_0

    .line 238
    :pswitch_2
    const-string v0, "top-movies"

    iput-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;

    goto :goto_0

    .line 230
    nop

    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updatePage()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 179
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photos:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 180
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0702cf

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 181
    .local v0, galleryView:Landroid/widget/GridView;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->topPhotos:Ljava/util/ArrayList;

    .line 182
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    const/16 v3, 0x64

    if-lt v1, v3, :cond_2

    .line 186
    :cond_0
    new-instance v3, Lnet/flixster/android/PhotosListAdapter;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->topPhotos:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photoClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v3, v4, v5, v6}, Lnet/flixster/android/PhotosListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 187
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 188
    invoke-virtual {v0, v7}, Landroid/widget/GridView;->setClickable(Z)V

    .line 189
    invoke-virtual {v0, v7}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 190
    invoke-virtual {v0, v7}, Landroid/widget/GridView;->setFocusableInTouchMode(Z)V

    .line 191
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->trackPage()V

    .line 193
    .end local v0           #galleryView:Landroid/widget/GridView;
    .end local v1           #i:I
    :cond_1
    return-void

    .line 183
    .restart local v0       #galleryView:Landroid/widget/GridView;
    .restart local v1       #i:I
    :cond_2
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 184
    .local v2, photo:Ljava/lang/Object;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->topPhotos:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "dialogId"

    .prologue
    const v5, 0x7f0c0135

    const/4 v4, 0x1

    .line 98
    packed-switch p1, :pswitch_data_0

    .line 124
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 125
    .local v1, defaultDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 126
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 127
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 128
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    move-object v2, v1

    .line 129
    .end local v1           #defaultDialog:Landroid/app/ProgressDialog;
    :goto_0
    return-object v2

    .line 101
    :pswitch_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 102
    .local v2, photosDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 103
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 104
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 105
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 109
    .end local v2           #photosDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 110
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v3, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 111
    const-string v3, "Network Error"

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 112
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 113
    const-string v3, "Retry"

    new-instance v4, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$5;

    invoke-direct {v4, p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment$5;-><init>(Lcom/flixster/android/activity/hc/PhotoGalleryFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 120
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 121
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 59
    const-string v3, "FlxMain"

    const-string v4, "PhotoGalleryFragment.onCreateView"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const v3, 0x7f03008b

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 61
    .local v2, view:Landroid/view/View;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 62
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_2

    .line 63
    const-string v3, "KEY_PHOTO_FILTER"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;

    .line 64
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-gtz v3, :cond_1

    .line 65
    :cond_0
    const-string v3, "random"

    iput-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;

    .line 67
    :cond_1
    new-instance v3, Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    .line 68
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    iget-object v4, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->headerClickListener:Landroid/view/View$OnClickListener;

    const v5, 0x7f0c002f

    const v6, 0x7f0c0030

    .line 69
    const v7, 0x7f0c0031

    .line 68
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;III)V

    .line 70
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    const v4, 0x7f070258

    invoke-virtual {v3, v4}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 71
    const v3, 0x7f0702cd

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 73
    .local v1, header:Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 74
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->updateHeader()V

    .line 80
    .end local v1           #header:Landroid/widget/LinearLayout;
    :cond_2
    return-object v2
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onResume()V

    .line 86
    iget-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    iget v1, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->mNavSelect:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 87
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->updateHeader()V

    .line 88
    const-string v0, "FlxMain"

    const-string v1, "PhotoGalleryPage.onCreate"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->photos:Ljava/util/ArrayList;

    .line 90
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->scheduleUpdatePageTask()V

    .line 94
    return-void
.end method

.method public trackPage()V
    .locals 4

    .prologue
    .line 249
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/photos/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Top Photos Page for filter:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/PhotoGalleryFragment;->filter:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    return-void
.end method
