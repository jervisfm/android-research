.class Lcom/flixster/android/activity/hc/ReviewFragment$3;
.super Ljava/util/TimerTask;
.source "ReviewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/ReviewFragment;->ScheduleLoadMoviesTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ReviewFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ReviewFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    .line 179
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 181
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 182
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/ReviewFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ReviewFragment;->access$0(Lcom/flixster/android/activity/hc/ReviewFragment;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ReviewFragment;->access$0(Lcom/flixster/android/activity/hc/ReviewFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 190
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-wide v3, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovieId:J

    invoke-static {v3, v4}, Lnet/flixster/android/data/MovieDao;->getMovieDetail(J)Lnet/flixster/android/model/Movie;

    move-result-object v2

    .line 192
    .local v2, tempMovie:Lnet/flixster/android/model/Movie;
    if-eqz v2, :cond_3

    .line 193
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->merge(Lnet/flixster/android/model/Movie;)V

    .line 194
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iput-object v2, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    .line 197
    :cond_3
    const-string v3, "FlxMain"

    .line 198
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ReviewFragment.ScheduleLoadMovieTask() findMovieDetails done.."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-object v5, v5, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 197
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v3, v4}, Lcom/flixster/android/activity/hc/ReviewFragment;->access$3(Lcom/flixster/android/activity/hc/ReviewFragment;Ljava/util/ArrayList;)V

    .line 205
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 206
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ReviewFragment;->access$0(Lcom/flixster/android/activity/hc/ReviewFragment;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-object v4, v4, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 208
    :cond_4
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 209
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ReviewFragment;->access$0(Lcom/flixster/android/activity/hc/ReviewFragment;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-object v4, v4, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 211
    :cond_5
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getUserReviewsList()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 212
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ReviewFragment;->access$0(Lcom/flixster/android/activity/hc/ReviewFragment;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-object v4, v4, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getUserReviewsList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 214
    :cond_6
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getCriticReviewsList()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 215
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ReviewFragment;->access$0(Lcom/flixster/android/activity/hc/ReviewFragment;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget-object v4, v4, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getCriticReviewsList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 217
    :cond_7
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/ReviewFragment;->postMovieLoadHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ReviewFragment;->access$4(Lcom/flixster/android/activity/hc/ReviewFragment;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 218
    .end local v2           #tempMovie:Lnet/flixster/android/model/Movie;
    :catch_0
    move-exception v0

    .line 219
    .local v0, de:Lnet/flixster/android/data/DaoException;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget v4, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mRetryCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mRetryCount:I

    .line 220
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iget v3, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mRetryCount:I

    const/4 v4, 0x4

    if-le v3, v4, :cond_8

    .line 221
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    iput v7, v3, Lcom/flixster/android/activity/hc/ReviewFragment;->mRetryCount:I

    goto/16 :goto_0

    .line 225
    :cond_8
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment$3;->this$0:Lcom/flixster/android/activity/hc/ReviewFragment;

    #calls: Lcom/flixster/android/activity/hc/ReviewFragment;->ScheduleLoadMoviesTask()V
    invoke-static {v3}, Lcom/flixster/android/activity/hc/ReviewFragment;->access$2(Lcom/flixster/android/activity/hc/ReviewFragment;)V

    goto/16 :goto_0
.end method
