.class public Lcom/flixster/android/activity/hc/ReviewFragment;
.super Lcom/flixster/android/activity/hc/FlixsterFragment;
.source "ReviewFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field private static final DIALOG_REVIEW:I = 0x2


# instance fields
.field private mIndexTextView:Landroid/widget/TextView;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field public mListType:I

.field protected mLoadDetailsTask:Ljava/util/TimerTask;

.field mMovie:Lnet/flixster/android/model/Movie;

.field mMovieId:J

.field public mMovieType:I

.field mNextButton:Landroid/widget/Button;

.field mPrevButton:Landroid/widget/Button;

.field private mReviewsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field private mStartReviewIndex:I

.field private mViewFlipper:Landroid/widget/ViewFlipper;

.field private postMovieLoadHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;-><init>()V

    .line 337
    new-instance v0, Lcom/flixster/android/activity/hc/ReviewFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ReviewFragment$1;-><init>(Lcom/flixster/android/activity/hc/ReviewFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->postMovieLoadHandler:Landroid/os/Handler;

    .line 45
    return-void
.end method

.method private ScheduleLoadMoviesTask()V
    .locals 4

    .prologue
    .line 179
    new-instance v0, Lcom/flixster/android/activity/hc/ReviewFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ReviewFragment$3;-><init>(Lcom/flixster/android/activity/hc/ReviewFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mLoadDetailsTask:Ljava/util/TimerTask;

    .line 235
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mLoadDetailsTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 238
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/ReviewFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/ReviewFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->populatePage()V

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/ReviewFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->ScheduleLoadMoviesTask()V

    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/ReviewFragment;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/ReviewFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 337
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->postMovieLoadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 382
    const/16 v0, 0x6a

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/ReviewFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 72
    new-instance v0, Lcom/flixster/android/activity/hc/ReviewFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/ReviewFragment;-><init>()V

    .line 73
    .local v0, rf:Lcom/flixster/android/activity/hc/ReviewFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->setArguments(Landroid/os/Bundle;)V

    .line 74
    return-object v0
.end method

.method private populatePage()V
    .locals 18

    .prologue
    .line 247
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;

    if-eqz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_0

    .line 249
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v12}, Landroid/widget/ViewFlipper;->removeAllViews()V

    .line 251
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_1

    .line 324
    const-string v12, "FlxMain"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "ReviewPage.populatePage() mStartReviewIndex:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v14, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mStartReviewIndex:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mStartReviewIndex:I

    invoke-virtual {v12, v13}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 326
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mIndexTextView:Landroid/widget/TextView;

    new-instance v13, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v14}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, " of "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 328
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mPrevButton:Landroid/widget/Button;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/Button;->setVisibility(I)V

    .line 329
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mNextButton:Landroid/widget/Button;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/Button;->setVisibility(I)V

    .line 330
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 331
    .local v4, main:Landroid/app/Activity;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v12

    if-nez v12, :cond_0

    .line 332
    const/4 v12, 0x2

    invoke-virtual {v4, v12}, Landroid/app/Activity;->removeDialog(I)V

    .line 335
    .end local v4           #main:Landroid/app/Activity;
    :cond_0
    return-void

    .line 251
    :cond_1
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lnet/flixster/android/model/Review;

    .line 253
    .local v6, r:Lnet/flixster/android/model/Review;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v14, 0x7f030073

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    .line 254
    const/16 v16, 0x0

    .line 253
    invoke-virtual/range {v13 .. v16}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    .line 255
    .local v3, item:Landroid/widget/ScrollView;
    const v13, 0x7f070226

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 256
    .local v11, tv:Landroid/widget/TextView;
    iget-object v13, v6, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    const v13, 0x7f070225

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 259
    .local v5, mugImageView:Landroid/widget/ImageView;
    invoke-virtual {v6, v5}, Lnet/flixster/android/model/Review;->getReviewerBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 260
    .local v1, bitmap:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_2

    .line 261
    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 264
    :cond_2
    const v13, 0x7f070228

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 265
    .local v2, critic_icon:Landroid/widget/ImageView;
    const v13, 0x7f070229

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 266
    .local v9, ri:Landroid/widget/ImageView;
    const v13, 0x7f070224

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 267
    .local v7, reviewHeader:Landroid/widget/RelativeLayout;
    const v13, 0x7f07022a

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    .line 269
    .local v8, reviewItemBody:Landroid/widget/RelativeLayout;
    iget v13, v6, Lnet/flixster/android/model/Review;->type:I

    packed-switch v13, :pswitch_data_0

    .line 312
    :cond_3
    :goto_1
    iget-wide v13, v6, Lnet/flixster/android/model/Review;->userId:J

    const-wide/16 v15, 0x0

    cmp-long v13, v13, v15

    if-lez v13, :cond_5

    .line 313
    invoke-virtual {v7, v6}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 314
    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 315
    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v13, v3}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 271
    :pswitch_0
    const/16 v13, 0x8

    invoke-virtual {v9, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 272
    const v13, 0x7f07022b

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 273
    .restart local v11       #tv:Landroid/widget/TextView;
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "     "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v14, v6, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    const v13, 0x7f070227

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 275
    .restart local v11       #tv:Landroid/widget/TextView;
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, ", "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v14, v6, Lnet/flixster/android/model/Review;->source:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    iget-object v13, v6, Lnet/flixster/android/model/Review;->url:Ljava/lang/String;

    if-eqz v13, :cond_4

    .line 279
    invoke-virtual {v8, v6}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 280
    const/4 v13, 0x1

    invoke-virtual {v8, v13}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 281
    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    const v13, 0x7f0200c2

    invoke-virtual {v8, v13}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 283
    const/16 v13, 0x9

    const/16 v14, 0x9

    const/16 v15, 0x9

    const/16 v16, 0x9

    move/from16 v0, v16

    invoke-virtual {v8, v13, v14, v15, v0}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 285
    :cond_4
    iget v13, v6, Lnet/flixster/android/model/Review;->score:I

    const/16 v14, 0x3c

    if-ge v13, v14, :cond_3

    .line 286
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0200ec

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 287
    .local v10, scoreIcon:Landroid/graphics/drawable/Drawable;
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v15

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v10, v13, v14, v15, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 288
    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 292
    .end local v10           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :pswitch_1
    const v13, 0x7f07022b

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 293
    .restart local v11       #tv:Landroid/widget/TextView;
    iget-object v13, v6, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    const/16 v13, 0x8

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 295
    const v13, 0x7f070227

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 296
    .restart local v11       #tv:Landroid/widget/TextView;
    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 297
    sget-object v13, Lnet/flixster/android/Flixster;->RATING_LARGE_R:[I

    iget-wide v14, v6, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v16, 0x4000

    mul-double v14, v14, v16

    double-to-int v14, v14

    aget v13, v13, v14

    invoke-virtual {v9, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 298
    const v13, 0x7f07022c

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 299
    .restart local v11       #tv:Landroid/widget/TextView;
    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 302
    :pswitch_2
    const v13, 0x7f07022b

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 303
    .restart local v11       #tv:Landroid/widget/TextView;
    iget-object v13, v6, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    const/16 v13, 0x8

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 305
    const v13, 0x7f070227

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 306
    .restart local v11       #tv:Landroid/widget/TextView;
    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 307
    sget-object v13, Lnet/flixster/android/Flixster;->RATING_LARGE_R:[I

    iget-wide v14, v6, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v16, 0x4000

    mul-double v14, v14, v16

    double-to-int v14, v14

    aget v13, v13, v14

    invoke-virtual {v9, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 308
    const v13, 0x7f07022c

    invoke-virtual {v3, v13}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 309
    .restart local v11       #tv:Landroid/widget/TextView;
    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 317
    :cond_5
    const v13, 0x7f09000b

    invoke-virtual {v7, v13}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 318
    const/16 v13, 0xa

    const/16 v14, 0xa

    const/16 v15, 0xa

    const/16 v16, 0xa

    move/from16 v0, v16

    invoke-virtual {v7, v13, v14, v15, v0}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    goto/16 :goto_2

    .line 269
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "v"

    .prologue
    .line 359
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 361
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Review;

    .line 363
    .local v1, r:Lnet/flixster/android/model/Review;
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/movie/criticreview"

    .line 364
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Critic Review - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "title"

    invoke-virtual {v5, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 363
    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    if-eqz v1, :cond_0

    iget-object v2, v1, Lnet/flixster/android/model/Review;->url:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lnet/flixster/android/model/Review;->url:Ljava/lang/String;

    const-string v3, "http://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 367
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, v1, Lnet/flixster/android/model/Review;->url:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 368
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/hc/ReviewFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 359
    nop

    :pswitch_data_0
    .packed-switch 0x7f07022a
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "dialogId"

    .prologue
    const v5, 0x7f0c0135

    const/4 v4, 0x1

    .line 146
    packed-switch p1, :pswitch_data_0

    .line 169
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 170
    .local v1, defaultDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 171
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 172
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 173
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    move-object v2, v1

    .line 174
    .end local v1           #defaultDialog:Landroid/app/ProgressDialog;
    :goto_0
    return-object v2

    .line 149
    :pswitch_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 150
    .local v2, ratingsDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 151
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 152
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 153
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 156
    .end local v2           #ratingsDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 157
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v3, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 158
    const-string v3, "Network Error"

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 159
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 160
    const-string v3, "Retry"

    new-instance v4, Lcom/flixster/android/activity/hc/ReviewFragment$2;

    invoke-direct {v4, p0}, Lcom/flixster/android/activity/hc/ReviewFragment$2;-><init>(Lcom/flixster/android/activity/hc/ReviewFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 166
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 167
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 103
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 104
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030072

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 106
    .local v2, view:Landroid/view/View;
    const v3, 0x7f07021b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mIndexTextView:Landroid/widget/TextView;

    .line 108
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 110
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 111
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ReviewFragment.onCreateView getInt:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "REVIEW_INDEX"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ReviewFragment.onCreateView getLong:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovieId:J

    .line 115
    iget-wide v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovieId:J

    invoke-static {v3, v4}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v3

    iput-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    .line 116
    const-string v3, "REVIEW_INDEX"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mStartReviewIndex:I

    .line 118
    const-string v3, "MOVIE_THUMBNAIL"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 119
    .local v0, b:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 120
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mMovie:Lnet/flixster/android/model/Movie;

    new-instance v4, Ljava/lang/ref/SoftReference;

    invoke-direct {v4, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, v3, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    .line 125
    .end local v0           #b:Landroid/graphics/Bitmap;
    :cond_0
    const v3, 0x7f070222

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ViewFlipper;

    iput-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    .line 126
    const v3, 0x7f07021c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mPrevButton:Landroid/widget/Button;

    .line 127
    const v3, 0x7f07021a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mNextButton:Landroid/widget/Button;

    .line 129
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mPrevButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 130
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 132
    return-object v2
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onResume()V

    .line 139
    const-string v0, "FlxMain"

    const-string v1, "MoviePage.onResume()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->ScheduleLoadMoviesTask()V

    .line 142
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .parameter "view"
    .parameter "event"

    .prologue
    const/4 v2, 0x1

    .line 78
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 98
    :cond_0
    :pswitch_0
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 81
    :pswitch_1
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f040006

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 82
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f040007

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 83
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->showNext()V

    .line 84
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v4}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 85
    const-string v4, " of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, nextReviewText:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mIndexTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 89
    .end local v0           #nextReviewText:Ljava/lang/String;
    :pswitch_2
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f040008

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 90
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f040009

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 91
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->showPrevious()V

    .line 92
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v4}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 93
    const-string v4, " of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mReviewsList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 94
    .local v1, previousReviewText:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/ReviewFragment;->mIndexTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x7f07021a
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public trackPage()V
    .locals 0

    .prologue
    .line 388
    return-void
.end method
