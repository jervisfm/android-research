.class Lcom/flixster/android/activity/hc/SearchFragment$1;
.super Landroid/os/Handler;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/SearchFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/SearchFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/SearchFragment$1;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    .line 158
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 161
    iget-object v4, p0, Lcom/flixster/android/activity/hc/SearchFragment$1;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    .line 162
    .local v2, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/SearchFragment$1;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/SearchFragment;->isRemoving()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$0()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 168
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$1()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 169
    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$1()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Movie;

    .line 170
    .local v3, movie:Lnet/flixster/android/model/Movie;
    new-instance v1, Landroid/content/Intent;

    const-string v4, "DETAILS"

    const-class v5, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v1, v4, v6, v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 171
    .local v1, intent:Landroid/content/Intent;
    const-string v4, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v5

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 172
    const-class v4, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v2, v1, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 176
    .end local v1           #intent:Landroid/content/Intent;
    .end local v3           #movie:Lnet/flixster/android/model/Movie;
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 177
    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 178
    .local v0, actor:Lnet/flixster/android/model/Actor;
    new-instance v1, Landroid/content/Intent;

    const-string v4, "TOP_ACTOR"

    const-class v5, Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-direct {v1, v4, v6, v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 179
    .restart local v1       #intent:Landroid/content/Intent;
    const-string v4, "ACTOR_ID"

    iget-wide v5, v0, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 180
    const-string v4, "ACTOR_NAME"

    iget-object v5, v0, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const-class v4, Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v2, v1, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    .line 182
    iget-object v4, p0, Lcom/flixster/android/activity/hc/SearchFragment$1;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    iget v5, v0, Lnet/flixster/android/model/Actor;->positionId:I

    iput v5, v4, Lcom/flixster/android/activity/hc/SearchFragment;->mLastListPosition:I

    goto :goto_0

    .line 166
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
