.class Lcom/flixster/android/activity/hc/SearchFragment$3;
.super Ljava/util/TimerTask;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/SearchFragment;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/SearchFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/SearchFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    .line 88
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 91
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 92
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$0()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 125
    :goto_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/SearchFragment;->mUpdateHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 126
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    iget-boolean v2, v2, Lcom/flixster/android/activity/hc/SearchFragment;->isInitialContentLoaded:Z

    if-nez v2, :cond_2

    .line 127
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/flixster/android/activity/hc/SearchFragment;->isInitialContentLoaded:Z

    .line 128
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #getter for: Lcom/flixster/android/activity/hc/SearchFragment;->initialContentLoadingHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$8(Lcom/flixster/android/activity/hc/SearchFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :cond_2
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 99
    :pswitch_0
    :try_start_1
    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$1()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #getter for: Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$5(Lcom/flixster/android/activity/hc/SearchFragment;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 100
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 101
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #getter for: Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$5(Lcom/flixster/android/activity/hc/SearchFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$1()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v2, v3}, Lnet/flixster/android/data/MovieDao;->searchMovies(Ljava/lang/String;Ljava/util/List;)V

    .line 103
    :cond_3
    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$1()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 104
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/search/movie/results"

    .line 105
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v2, "Search Movie - "

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #getter for: Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$5(Lcom/flixster/android/activity/hc/SearchFragment;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #getter for: Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$5(Lcom/flixster/android/activity/hc/SearchFragment;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 104
    invoke-interface {v3, v4, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :goto_3
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #calls: Lcom/flixster/android/activity/hc/SearchFragment;->setMovieLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$6(Lcom/flixster/android/activity/hc/SearchFragment;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 130
    :catch_0
    move-exception v0

    .line 132
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SearchPage.ScheduleLoadItemsTask.run() mRetryCount:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    iget v4, v4, Lcom/flixster/android/activity/hc/SearchFragment;->mRetryCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->printStackTrace()V

    .line 134
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    iget v3, v2, Lcom/flixster/android/activity/hc/SearchFragment;->mRetryCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/flixster/android/activity/hc/SearchFragment;->mRetryCount:I

    .line 135
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    iget v2, v2, Lcom/flixster/android/activity/hc/SearchFragment;->mRetryCount:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_b

    .line 136
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    const-wide/16 v3, 0x3e8

    #calls: Lcom/flixster/android/activity/hc/SearchFragment;->ScheduleLoadItemsTask(J)V
    invoke-static {v2, v3, v4}, Lcom/flixster/android/activity/hc/SearchFragment;->access$4(Lcom/flixster/android/activity/hc/SearchFragment;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 144
    :cond_4
    :goto_4
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 105
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_5
    :try_start_3
    const-string v2, "Results"

    goto :goto_2

    .line 107
    :cond_6
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/search"

    const-string v4, "Search"

    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    .line 143
    :catchall_0
    move-exception v2

    .line 144
    if-eqz v1, :cond_7

    iget-object v3, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_7

    iget-object v3, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 145
    iget-object v3, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 147
    :cond_7
    throw v2

    .line 112
    :pswitch_1
    :try_start_4
    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #getter for: Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$5(Lcom/flixster/android/activity/hc/SearchFragment;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 113
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 114
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #getter for: Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$5(Lcom/flixster/android/activity/hc/SearchFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v2, v3}, Lnet/flixster/android/data/ActorDao;->searchActors(Ljava/lang/String;Ljava/util/List;)V

    .line 116
    :cond_8
    invoke-static {}, Lcom/flixster/android/activity/hc/SearchFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 117
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/search/actor/results"

    .line 118
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v2, "Search Actor - "

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #getter for: Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$5(Lcom/flixster/android/activity/hc/SearchFragment;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #getter for: Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$5(Lcom/flixster/android/activity/hc/SearchFragment;)Ljava/lang/String;

    move-result-object v2

    :goto_5
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 117
    invoke-interface {v3, v4, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :goto_6
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    #calls: Lcom/flixster/android/activity/hc/SearchFragment;->setActorResultsLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/SearchFragment;->access$7(Lcom/flixster/android/activity/hc/SearchFragment;)V

    goto/16 :goto_1

    .line 118
    :cond_9
    const-string v2, "Results"

    goto :goto_5

    .line 120
    :cond_a
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/search"

    const-string v4, "Search"

    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_6

    .line 138
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    :cond_b
    :try_start_5
    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment$3;->this$0:Lcom/flixster/android/activity/hc/SearchFragment;

    const/4 v3, 0x0

    iput v3, v2, Lcom/flixster/android/activity/hc/SearchFragment;->mRetryCount:I

    .line 139
    if-eqz v1, :cond_4

    .line 140
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_4

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
