.class public Lcom/flixster/android/activity/hc/SearchFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "SearchFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final ACTOR_SEARCH:I = 0x1

.field private static final MOVIE_SEARCH:I

.field private static final sActorResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field

.field private static final sMovieResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private static sSearchType:I


# instance fields
.field private initialContentLoadingHandler:Landroid/os/Handler;

.field private mNavbar:Lcom/flixster/android/view/SubNavBar;

.field private mNavbarHolder:Landroid/widget/LinearLayout;

.field private mQueryString:Ljava/lang/String;

.field private searchTypeListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/flixster/android/activity/hc/SearchFragment;->sMovieResults:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/flixster/android/activity/hc/SearchFragment;->sActorResults:Ljava/util/ArrayList;

    .line 43
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 158
    new-instance v0, Lcom/flixster/android/activity/hc/SearchFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/SearchFragment$1;-><init>(Lcom/flixster/android/activity/hc/SearchFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/SearchFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    .line 302
    new-instance v0, Lcom/flixster/android/activity/hc/SearchFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/SearchFragment$2;-><init>(Lcom/flixster/android/activity/hc/SearchFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/SearchFragment;->searchTypeListener:Landroid/view/View$OnClickListener;

    .line 43
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 3
    .parameter "delay"

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/flixster/android/activity/hc/SearchFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/SearchFragment$3;-><init>(Lcom/flixster/android/activity/hc/SearchFragment;)V

    .line 151
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    const-string v1, "FlxMain"

    const-string v2, "SearchPage.ScheduleLoadItemsTask() loading item"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 154
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v1, v0, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :cond_0
    monitor-exit p0

    return-void

    .line 88
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method static synthetic access$0()I
    .locals 1

    .prologue
    .line 50
    sget v0, Lcom/flixster/android/activity/hc/SearchFragment;->sSearchType:I

    return v0
.end method

.method static synthetic access$1()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/flixster/android/activity/hc/SearchFragment;->sMovieResults:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/flixster/android/activity/hc/SearchFragment;->sActorResults:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(I)V
    .locals 0
    .parameter

    .prologue
    .line 50
    sput p0, Lcom/flixster/android/activity/hc/SearchFragment;->sSearchType:I

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/SearchFragment;J)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/hc/SearchFragment;->ScheduleLoadItemsTask(J)V

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/SearchFragment;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/SearchFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->setMovieLviList()V

    return-void
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/SearchFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->setActorResultsLviList()V

    return-void
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/SearchFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lcom/flixster/android/activity/hc/SearchFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addAd()V
    .locals 0

    .prologue
    .line 238
    return-void
.end method

.method private addNoResults()V
    .locals 3

    .prologue
    .line 251
    new-instance v0, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 252
    .local v0, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    const v2, 0x7f0c010a

    invoke-virtual {v1, v2}, Lcom/flixster/android/activity/hc/Main;->getResourceString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 253
    iget-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    return-void
.end method

.method private addSearchPanel()V
    .locals 3

    .prologue
    .line 241
    new-instance v0, Lnet/flixster/android/lvi/LviSearchPanel;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviSearchPanel;-><init>()V

    .line 242
    .local v0, lviSearchPanel:Lnet/flixster/android/lvi/LviSearchPanel;
    iput-object p0, v0, Lnet/flixster/android/lvi/LviSearchPanel;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 243
    iput-object p0, v0, Lnet/flixster/android/lvi/LviSearchPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 244
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 245
    sget v2, Lcom/flixster/android/activity/hc/SearchFragment;->sSearchType:I

    if-nez v2, :cond_0

    const v2, 0x7f0c0106

    :goto_0
    invoke-virtual {v1, v2}, Lcom/flixster/android/activity/hc/Main;->getResourceString(I)Ljava/lang/String;

    move-result-object v1

    .line 244
    iput-object v1, v0, Lnet/flixster/android/lvi/LviSearchPanel;->mHint:Ljava/lang/String;

    .line 247
    iget-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 248
    return-void

    .line 246
    :cond_0
    const v2, 0x7f0c0107

    goto :goto_0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x7

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/SearchFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 58
    new-instance v0, Lcom/flixster/android/activity/hc/SearchFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/SearchFragment;-><init>()V

    .line 59
    .local v0, af:Lcom/flixster/android/activity/hc/SearchFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/SearchFragment;->setArguments(Landroid/os/Bundle;)V

    .line 60
    return-object v0
.end method

.method private setActorResultsLviList()V
    .locals 7

    .prologue
    .line 212
    const-string v5, "FlxMain"

    const-string v6, "SearchPage.setActorResultsLviList "

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v5, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 214
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->addAd()V

    .line 215
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->addSearchPanel()V

    .line 217
    sget-object v5, Lcom/flixster/android/activity/hc/SearchFragment;->sActorResults:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    .line 219
    .local v4, sActorResultsCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Actor;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    long-to-int v1, v5

    .line 220
    .local v1, id:I
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 227
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 228
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->addNoResults()V

    .line 230
    :cond_0
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;

    .line 231
    return-void

    .line 220
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 221
    .local v0, actor:Lnet/flixster/android/model/Actor;
    add-int/lit8 v2, v1, 0x1

    .end local v1           #id:I
    .local v2, id:I
    iput v1, v0, Lnet/flixster/android/model/Actor;->positionId:I

    .line 222
    new-instance v3, Lnet/flixster/android/lvi/LviActor;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviActor;-><init>()V

    .line 223
    .local v3, lviActor:Lnet/flixster/android/lvi/LviActor;
    iput-object v0, v3, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    .line 224
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getActorClickListener()Landroid/view/View$OnClickListener;

    move-result-object v6

    iput-object v6, v3, Lnet/flixster/android/lvi/LviActor;->mActorClick:Landroid/view/View$OnClickListener;

    .line 225
    iget-object v6, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .end local v2           #id:I
    .restart local v1       #id:I
    goto :goto_0
.end method

.method private setMovieLviList()V
    .locals 5

    .prologue
    .line 190
    const-string v3, "FlxMain"

    const-string v4, "SearchPage.setSearchLviList "

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v3, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 192
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->addAd()V

    .line 193
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->addSearchPanel()V

    .line 195
    sget-object v3, Lcom/flixster/android/activity/hc/SearchFragment;->sMovieResults:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 196
    .local v2, sMovieResultsCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 198
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 204
    iget-object v3, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mDataHolder:Ljava/util/ArrayList;

    new-instance v4, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    :cond_0
    :goto_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;

    .line 209
    return-void

    .line 198
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    .line 199
    .local v1, movie:Lnet/flixster/android/model/Movie;
    new-instance v0, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 200
    .local v0, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v1, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 201
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    iput-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 202
    iget-object v4, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 205
    .end local v0           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v1           #movie:Lnet/flixster/android/model/Movie;
    :cond_2
    iget-object v3, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 206
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->addNoResults()V

    goto :goto_1
.end method

.method private shadeNavButtons()V
    .locals 2

    .prologue
    .line 318
    sget v0, Lcom/flixster/android/activity/hc/SearchFragment;->sSearchType:I

    if-nez v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    const v1, 0x7f070258

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    sget v0, Lcom/flixster/android/activity/hc/SearchFragment;->sSearchType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 321
    iget-object v0, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    const v1, 0x7f070259

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    const/4 v4, 0x0

    .line 257
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 267
    :goto_0
    return-void

    .line 259
    :pswitch_0
    const-string v2, "FlxMain"

    const-string v3, "SearchPage.onClick ok then"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 261
    .local v0, editText:Landroid/widget/EditText;
    new-instance v2, Landroid/view/KeyEvent;

    const/16 v3, 0x42

    invoke-direct {v2, v4, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    .line 262
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 263
    const-string v3, "input_method"

    .line 262
    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 264
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    .line 257
    :pswitch_data_0
    .packed-switch 0x7f070101
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 65
    invoke-super {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/LviFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 66
    .local v0, view:Landroid/view/View;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 71
    const v1, 0x7f0700f3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    .line 72
    new-instance v1, Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    .line 73
    iget-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment;->searchTypeListener:Landroid/view/View$OnClickListener;

    const v3, 0x7f0c0104

    const v4, 0x7f0c0105

    invoke-virtual {v1, v2, v3, v4}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;II)V

    .line 74
    iget-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    const v2, 0x7f070258

    invoke-virtual {v1, v2}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 75
    iget-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 77
    return-object v0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    const/4 v2, 0x0

    .line 270
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 271
    const-string v0, "FlxMain"

    const-string v1, "SearchPage.onEditorAction ok (from action)"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    check-cast p1, Landroid/widget/EditText;

    .end local p1
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;

    .line 275
    const-string v0, "FLX"

    iget-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 276
    const-string v0, "REINDEER FLOTILLA"

    iget-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    const-string v0, "UUDDLRLRBA"

    iget-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setAdminState(I)V

    .line 279
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0c01bb

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 283
    :cond_1
    const-string v0, "DGNS"

    iget-object v1, p0, Lcom/flixster/android/activity/hc/SearchFragment;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->enableDiagnosticMode()V

    .line 285
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0c01bc

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 288
    :cond_2
    sget v0, Lcom/flixster/android/activity/hc/SearchFragment;->sSearchType:I

    packed-switch v0, :pswitch_data_0

    .line 296
    :goto_0
    iput-boolean v2, p0, Lcom/flixster/android/activity/hc/SearchFragment;->isInitialContentLoaded:Z

    .line 297
    const-wide/16 v0, 0xa

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/SearchFragment;->ScheduleLoadItemsTask(J)V

    .line 299
    :cond_3
    return v2

    .line 290
    :pswitch_0
    sget-object v0, Lcom/flixster/android/activity/hc/SearchFragment;->sMovieResults:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 293
    :pswitch_1
    sget-object v0, Lcom/flixster/android/activity/hc/SearchFragment;->sActorResults:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 83
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/SearchFragment;->shadeNavButtons()V

    .line 84
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/SearchFragment;->ScheduleLoadItemsTask(J)V

    .line 85
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 331
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/search"

    const-string v2, "Search"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    return-void
.end method
