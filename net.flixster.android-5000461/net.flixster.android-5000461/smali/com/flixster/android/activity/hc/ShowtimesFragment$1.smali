.class Lcom/flixster/android/activity/hc/ShowtimesFragment$1;
.super Ljava/lang/Object;
.source "ShowtimesFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/ShowtimesFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ShowtimesFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 240
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lnet/flixster/android/MovieMapPage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 241
    .local v0, mapIntent:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #getter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v2

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 242
    const-string v2, "MOVIE_THUMBNAIL"

    iget-object v1, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #getter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 243
    const-string v1, "title"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #getter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v2

    const-string v3, "title"

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    const-string v1, "runningTime"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #getter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v2

    const-string v3, "runningTime"

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    const-string v1, "MOVIE_ACTORS_SHORT"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #getter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v2

    const-string v3, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    const-string v1, "mpaa"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #getter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v2

    const-string v3, "mpaa"

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    const-string v1, "popcornScore"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #getter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v2

    const-string v3, "popcornScore"

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 248
    const-string v1, "rottenTomatoes"

    iget-object v2, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #getter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v2

    const-string v3, "rottenTomatoes"

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 249
    iget-object v1, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    invoke-virtual {v1, v0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->startActivity(Landroid/content/Intent;)V

    .line 250
    return-void
.end method
