.class Lcom/flixster/android/activity/hc/ShowtimesFragment$2;
.super Ljava/util/TimerTask;
.source "ShowtimesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/ShowtimesFragment;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/ShowtimesFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    .line 107
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 110
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    check-cast v9, Lcom/flixster/android/activity/hc/Main;

    .line 111
    .local v9, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v9, :cond_0

    iget-object v0, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    const-string v0, "FlxMain"

    const-string v1, "ShowtimesPage.ScheduleLoadItemsTask.run"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :try_start_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v7

    .line 118
    .local v7, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 120
    iget-object v10, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v0

    .line 121
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v2

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v4

    .line 122
    iget-object v5, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #getter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v5

    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v5

    .line 120
    invoke-static/range {v0 .. v7}, Lnet/flixster/android/data/TheaterDao;->findTheatersByMovieLocation(DDLjava/util/Date;JLjava/util/HashMap;)Ljava/util/List;

    move-result-object v0

    #setter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mTheaterList:Ljava/util/List;
    invoke-static {v10, v0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$1(Lcom/flixster/android/activity/hc/ShowtimesFragment;Ljava/util/List;)V

    .line 129
    :goto_1
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #calls: Lcom/flixster/android/activity/hc/ShowtimesFragment;->setShowtimesLviList()V
    invoke-static {v0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$2(Lcom/flixster/android/activity/hc/ShowtimesFragment;)V

    .line 130
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mUpdateHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 131
    .end local v7           #favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catch_0
    move-exception v8

    .line 132
    .local v8, de:Lnet/flixster/android/data/DaoException;
    const-string v0, "FlxMain"

    const-string v1, "ShowtimesPage.ScheduleLoadItemsTask.run DaoException"

    invoke-static {v0, v1, v8}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 133
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    invoke-virtual {v0, v8}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->retryLogic(Lnet/flixster/android/data/DaoException;)V

    goto :goto_0

    .line 125
    .end local v8           #de:Lnet/flixster/android/data/DaoException;
    .restart local v7       #favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_2
    :try_start_1
    iget-object v10, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v0

    .line 126
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v2

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v4

    .line 127
    iget-object v5, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;->this$0:Lcom/flixster/android/activity/hc/ShowtimesFragment;

    #getter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;

    move-result-object v5

    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v5

    .line 125
    invoke-static/range {v0 .. v7}, Lnet/flixster/android/data/TheaterDao;->findTheatersByMovieLocation(DDLjava/util/Date;JLjava/util/HashMap;)Ljava/util/List;

    move-result-object v0

    #setter for: Lcom/flixster/android/activity/hc/ShowtimesFragment;->mTheaterList:Ljava/util/List;
    invoke-static {v10, v0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->access$1(Lcom/flixster/android/activity/hc/ShowtimesFragment;Ljava/util/List;)V
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
