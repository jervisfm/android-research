.class public Lcom/flixster/android/activity/hc/ShowtimesFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "ShowtimesFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private mMovie:Lnet/flixster/android/model/Movie;

.field private mTheaterList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation
.end field

.field private mTheaterMapOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mTheaterList:Ljava/util/List;

    .line 236
    new-instance v0, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment$1;-><init>(Lcom/flixster/android/activity/hc/ShowtimesFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mTheaterMapOnClickListener:Landroid/view/View$OnClickListener;

    .line 43
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 3
    .parameter "delay"

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment$2;-><init>(Lcom/flixster/android/activity/hc/ShowtimesFragment;)V

    .line 139
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 140
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 141
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v0, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :cond_0
    monitor-exit p0

    return-void

    .line 107
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    .end local v1           #main:Lcom/flixster/android/activity/hc/Main;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/ShowtimesFragment;)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/ShowtimesFragment;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mTheaterList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/ShowtimesFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->setShowtimesLviList()V

    return-void
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 255
    const/16 v0, 0x65

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/ShowtimesFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 50
    new-instance v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;-><init>()V

    .line 51
    .local v0, sf:Lcom/flixster/android/activity/hc/ShowtimesFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v0
.end method

.method private setShowtimesLviList()V
    .locals 19

    .prologue
    .line 151
    const-string v15, "FlxMain"

    const-string v16, "ShowtimesPage.setShowtimesLviList "

    invoke-static/range {v15 .. v16}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 171
    new-instance v6, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 172
    .local v6, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iput-object v15, v6, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 173
    const/4 v15, 0x1

    iput v15, v6, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    .line 174
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    new-instance v4, Lnet/flixster/android/lvi/LviLocationPanel;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviLocationPanel;-><init>()V

    .line 177
    .local v4, lviLocationPanel:Lnet/flixster/android/lvi/LviLocationPanel;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    new-instance v2, Lnet/flixster/android/lvi/LviDatePanel;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviDatePanel;-><init>()V

    .line 180
    .local v2, lviDatePanel:Lnet/flixster/android/lvi/LviDatePanel;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    check-cast v15, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v15}, Lcom/flixster/android/activity/hc/Main;->getDateSelectOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v15

    iput-object v15, v2, Lnet/flixster/android/lvi/LviDatePanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 181
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    invoke-static {}, Lcom/flixster/android/utils/GoogleApiDetector;->instance()Lcom/flixster/android/utils/GoogleApiDetector;

    move-result-object v15

    invoke-virtual {v15}, Lcom/flixster/android/utils/GoogleApiDetector;->isVanillaAndroid()Z

    move-result v15

    if-nez v15, :cond_0

    .line 184
    new-instance v3, Lnet/flixster/android/lvi/LviIconPanel;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviIconPanel;-><init>()V

    .line 185
    .local v3, lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0c006b

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v3, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    .line 186
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mTheaterMapOnClickListener:Landroid/view/View$OnClickListener;

    iput-object v15, v3, Lnet/flixster/android/lvi/LviIconPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 187
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    .end local v3           #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    :cond_0
    new-instance v13, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v13}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 191
    .local v13, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0c0046

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v13, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 192
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mTheaterList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-lez v15, :cond_5

    .line 195
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mTheaterList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_1
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_2

    .line 225
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 226
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    :goto_1
    return-void

    .line 195
    :cond_2
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lnet/flixster/android/model/Theater;

    .line 196
    .local v14, theater:Lnet/flixster/android/model/Theater;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    new-instance v17, Lcom/flixster/android/view/Divider;

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lcom/flixster/android/view/Divider;-><init>(Landroid/content/Context;)V

    sget v18, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_DIVIDER:I

    invoke-static/range {v17 .. v18}, Lnet/flixster/android/lvi/LviWrapper;->convertToLvi(Landroid/view/View;I)Lnet/flixster/android/lvi/Lvi;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    new-instance v8, Lnet/flixster/android/lvi/LviTheater;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviTheater;-><init>()V

    .line 199
    .local v8, lviTheater:Lnet/flixster/android/lvi/LviTheater;
    iput-object v14, v8, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    .line 200
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getStarTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v8, Lnet/flixster/android/lvi/LviTheater;->mStarTheaterClick:Landroid/view/View$OnClickListener;

    .line 201
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v8, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    invoke-virtual {v14}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/ArrayList;

    .line 205
    .local v11, showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    if-eqz v11, :cond_3

    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v16

    if-eqz v16, :cond_4

    .line 206
    :cond_3
    new-instance v7, Lnet/flixster/android/lvi/LviShowtimes;

    invoke-direct {v7}, Lnet/flixster/android/lvi/LviShowtimes;-><init>()V

    .line 207
    .local v7, lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    iput-object v14, v7, Lnet/flixster/android/lvi/LviShowtimes;->mTheater:Lnet/flixster/android/model/Theater;

    .line 208
    const/16 v16, 0x0

    move-object/from16 v0, v16

    iput-object v0, v7, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 211
    .end local v7           #lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    :cond_4
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 212
    .local v12, size:I
    const/4 v9, 0x0

    .local v9, n:I
    :goto_2
    if-ge v9, v12, :cond_1

    .line 213
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lnet/flixster/android/model/Showtimes;

    .line 214
    .local v10, showtimes:Lnet/flixster/android/model/Showtimes;
    new-instance v7, Lnet/flixster/android/lvi/LviShowtimes;

    invoke-direct {v7}, Lnet/flixster/android/lvi/LviShowtimes;-><init>()V

    .line 215
    .restart local v7       #lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    iput-object v14, v7, Lnet/flixster/android/lvi/LviShowtimes;->mTheater:Lnet/flixster/android/model/Theater;

    .line 216
    iput v12, v7, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimesListSize:I

    .line 217
    iput v9, v7, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimePosition:I

    .line 218
    iput-object v10, v7, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    .line 219
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getShowtimesClickListener()Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lnet/flixster/android/lvi/LviShowtimes;->mBuyClick:Landroid/view/View$OnClickListener;

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 229
    .end local v7           #lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    .end local v8           #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    .end local v9           #n:I
    .end local v10           #showtimes:Lnet/flixster/android/model/Showtimes;
    .end local v11           #showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    .end local v12           #size:I
    .end local v14           #theater:Lnet/flixster/android/model/Theater;
    :cond_5
    new-instance v5, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 230
    .local v5, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0c0058

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v5, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 231
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v15, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/LviFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 58
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedState"

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/LviFragment;->onCreate(Landroid/os/Bundle;)V

    .line 77
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 78
    .local v0, movieId:J
    invoke-static {v0, v1}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;

    .line 92
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 62
    invoke-super {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/LviFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 64
    .local v0, view:Landroid/view/View;
    return-object v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 102
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->trackPage()V

    .line 103
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->ScheduleLoadItemsTask(J)V

    .line 104
    return-void
.end method

.method protected retryAction()V
    .locals 2

    .prologue
    .line 147
    const-wide/16 v0, 0x3e8

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/ShowtimesFragment;->ScheduleLoadItemsTask(J)V

    .line 148
    return-void
.end method

.method public trackPage()V
    .locals 4

    .prologue
    .line 265
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/showtimes"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Showtimes - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/ShowtimesFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    return-void
.end method
