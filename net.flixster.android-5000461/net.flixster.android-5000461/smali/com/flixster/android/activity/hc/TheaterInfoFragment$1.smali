.class Lcom/flixster/android/activity/hc/TheaterInfoFragment$1;
.super Ljava/lang/Object;
.source "TheaterInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/TheaterInfoFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "v"

    .prologue
    .line 259
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 261
    .local v0, callIntent:Landroid/content/Intent;
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v4

    const-string v5, "street"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 262
    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v4

    const-string v5, "city"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v4

    const-string v5, "state"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 263
    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v4

    const-string v5, "zip"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 261
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 264
    .local v2, query:Ljava/lang/String;
    const-string v3, " "

    const-string v4, "+"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 266
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "geo:0,0?q="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 268
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/theaters/info/map"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "query:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v3, v0}, Lcom/flixster/android/activity/hc/Main;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    :goto_0
    return-void

    .line 272
    :catch_0
    move-exception v1

    .line 273
    .local v1, e:Landroid/content/ActivityNotFoundException;
    const-string v3, "FlxMain"

    const-string v4, "Intent not found"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
