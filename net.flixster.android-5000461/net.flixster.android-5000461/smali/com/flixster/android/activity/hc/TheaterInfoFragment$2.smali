.class Lcom/flixster/android/activity/hc/TheaterInfoFragment$2;
.super Ljava/lang/Object;
.source "TheaterInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/TheaterInfoFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$2;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    .line 281
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.DIAL"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 284
    .local v0, callIntent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$2;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v3

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Theater;->checkProperty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 285
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$2;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v3

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 286
    .local v2, number:Ljava/lang/String;
    const-string v3, "FANDANGO"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 287
    const-string v3, "FANDANGO"

    const-string v4, "3263264#"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 291
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tel:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 293
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$2;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v3, v0}, Lcom/flixster/android/activity/hc/Main;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    .end local v2           #number:Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 289
    .restart local v2       #number:Ljava/lang/String;
    :cond_1
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->convertKeypadLettersToDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 294
    :catch_0
    move-exception v1

    .line 295
    .local v1, e:Landroid/content/ActivityNotFoundException;
    const-string v3, "FlxMain"

    const-string v4, "Intent not found"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
