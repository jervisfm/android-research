.class Lcom/flixster/android/activity/hc/TheaterInfoFragment$3;
.super Ljava/lang/Object;
.source "TheaterInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/TheaterInfoFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .parameter "v"

    .prologue
    .line 303
    const/4 v2, 0x0

    .line 305
    .local v2, uriString:Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "http://lite.yelp.com/search?inboundsrc=flixster&cflt=restaurants&rflt=all&sortby=composite&find_loc="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 306
    iget-object v5, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v5

    #calls: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getAddressForYelp(Lnet/flixster/android/model/Theater;)Ljava/lang/String;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$1(Lnet/flixster/android/model/Theater;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-static {v5, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 305
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 311
    :goto_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v4

    const-string v5, "/theaters/info/yelp"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Yelp city:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v7}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v7

    const-string v8, "city"

    invoke-virtual {v7, v8}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 315
    .local v3, yelpIntent:Landroid/content/Intent;
    :try_start_1
    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v4, v3}, Lcom/flixster/android/activity/hc/Main;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 319
    :goto_1
    return-void

    .line 307
    .end local v3           #yelpIntent:Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 309
    .local v1, e1:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 316
    .end local v1           #e1:Ljava/io/UnsupportedEncodingException;
    .restart local v3       #yelpIntent:Landroid/content/Intent;
    :catch_1
    move-exception v0

    .line 317
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v4, "FlxMain"

    const-string v5, "Intent not found"

    invoke-static {v4, v5, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
