.class Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;
.super Ljava/util/TimerTask;
.source "TheaterInfoFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/TheaterInfoFragment;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    .line 93
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 96
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    .line 97
    .local v2, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TheaterInfoPage.ScheduleLoadItemsTask.run mTheaterId:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheaterId:J
    invoke-static {v5}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$2(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gtz v3, :cond_3

    .line 104
    :cond_2
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "net.flixster.android.EXTRA_THEATER_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    #setter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheaterId:J
    invoke-static {v3, v4, v5}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$3(Lcom/flixster/android/activity/hc/TheaterInfoFragment;J)V

    .line 105
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheaterId:J
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$2(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lnet/flixster/android/data/TheaterDao;->getTheater(J)Lnet/flixster/android/model/Theater;

    move-result-object v4

    #setter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v3, v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$4(Lcom/flixster/android/activity/hc/TheaterInfoFragment;Lnet/flixster/android/model/Theater;)V

    .line 106
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #calls: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->makeTheaterAddressString()V
    invoke-static {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$5(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V

    .line 108
    :cond_3
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v4

    .line 109
    iget-object v5, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheaterId:J
    invoke-static {v5}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$2(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)J

    move-result-wide v5

    .line 108
    invoke-static {v4, v5, v6}, Lnet/flixster/android/data/TheaterDao;->findShowtimesByTheater(Ljava/util/Date;J)Ljava/util/HashMap;

    move-result-object v4

    #setter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mShowtimesByMovieHash:Ljava/util/HashMap;
    invoke-static {v3, v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$6(Lcom/flixster/android/activity/hc/TheaterInfoFragment;Ljava/util/HashMap;)V

    .line 111
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TheaterInfoPage.ScheduleLoadItemsTask.run() mShowtimesByMovieHash.size:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 112
    iget-object v5, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mShowtimesByMovieHash:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$7(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 111
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mMovieProtectionReference:Ljava/util/ArrayList;
    invoke-static {v3, v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$8(Lcom/flixster/android/activity/hc/TheaterInfoFragment;Ljava/util/ArrayList;)V

    .line 115
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mShowtimesByMovieHash:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$7(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 119
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #calls: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->setTheaterInfoLviList()V
    invoke-static {v3}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$10(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V

    .line 121
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    iget-object v3, v3, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mUpdateHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 128
    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 115
    :cond_4
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 116
    .local v1, l:Ljava/lang/Long;
    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mMovieProtectionReference:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->access$9(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 123
    .end local v1           #l:Ljava/lang/Long;
    :catch_0
    move-exception v0

    .line 124
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v3, "FlxMain"

    const-string v4, "TheaterInfoPage.ScheduleLoadItemsTask DaoException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 125
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-virtual {v3, v0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 127
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 128
    iget-object v3, v2, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 126
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catchall_0
    move-exception v3

    .line 127
    if-eqz v2, :cond_5

    iget-object v4, v2, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v4, :cond_5

    iget-object v4, v2, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 128
    iget-object v4, v2, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 130
    :cond_5
    throw v3
.end method
