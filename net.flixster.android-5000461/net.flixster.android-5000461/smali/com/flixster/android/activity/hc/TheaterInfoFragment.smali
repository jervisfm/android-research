.class public Lcom/flixster/android/activity/hc/TheaterInfoFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "TheaterInfoFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field public mCallTheaterListener:Landroid/view/View$OnClickListener;

.field public mMapTheaterListener:Landroid/view/View$OnClickListener;

.field private mMovieProtectionReference:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private mShowtimesByMovieHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Showtimes;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTheater:Lnet/flixster/android/model/Theater;

.field private mTheaterAddressString:Ljava/lang/String;

.field private mTheaterId:J

.field public mYelpTheaterListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 255
    new-instance v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment$1;-><init>(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mMapTheaterListener:Landroid/view/View$OnClickListener;

    .line 277
    new-instance v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment$2;-><init>(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mCallTheaterListener:Landroid/view/View$OnClickListener;

    .line 301
    new-instance v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment$3;-><init>(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mYelpTheaterListener:Landroid/view/View$OnClickListener;

    .line 46
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 3
    .parameter "delay"

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment$4;-><init>(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V

    .line 134
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 135
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 136
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v0, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :cond_0
    monitor-exit p0

    return-void

    .line 93
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    .end local v1           #main:Lcom/flixster/android/activity/hc/Main;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Lnet/flixster/android/model/Theater;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/model/Theater;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 322
    invoke-static {p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getAddressForYelp(Lnet/flixster/android/model/Theater;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$10(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->setTheaterInfoLviList()V

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)J
    .locals 2
    .parameter

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheaterId:J

    return-wide v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/TheaterInfoFragment;J)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 48
    iput-wide p1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheaterId:J

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/TheaterInfoFragment;Lnet/flixster/android/model/Theater;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->makeTheaterAddressString()V

    return-void
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/TheaterInfoFragment;Ljava/util/HashMap;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mShowtimesByMovieHash:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Ljava/util/HashMap;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mShowtimesByMovieHash:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/TheaterInfoFragment;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mMovieProtectionReference:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$9(Lcom/flixster/android/activity/hc/TheaterInfoFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mMovieProtectionReference:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static getAddressForYelp(Lnet/flixster/android/model/Theater;)Ljava/lang/String;
    .locals 2
    .parameter "t"

    .prologue
    .line 323
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isFrenchLocale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "street"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "city"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", France"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 327
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "address"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 338
    const/16 v0, 0x69

    return v0
.end method

.method private makeTheaterAddressString()V
    .locals 3

    .prologue
    .line 332
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "street"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 333
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "city"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "state"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 334
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "zip"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 332
    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheaterAddressString:Ljava/lang/String;

    .line 335
    return-void
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/TheaterInfoFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 56
    new-instance v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;-><init>()V

    .line 57
    .local v0, tif:Lcom/flixster/android/activity/hc/TheaterInfoFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->setArguments(Landroid/os/Bundle;)V

    .line 58
    return-object v0
.end method

.method private setTheaterInfoLviList()V
    .locals 22

    .prologue
    .line 146
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    check-cast v9, Lcom/flixster/android/activity/hc/Main;

    .line 147
    .local v9, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v9, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->isRemoving()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    const-string v18, "FlxMain"

    const-string v19, "TheaterInfoPage.setPopularLviList "

    invoke-static/range {v18 .. v19}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 159
    new-instance v7, Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;

    invoke-direct {v7}, Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;-><init>()V

    .line 160
    .local v7, lviPageHeaderTheaterInfo:Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v0, v7, Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;->mTheater:Lnet/flixster/android/model/Theater;

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    move-object/from16 v18, v0

    const-string v19, "name"

    invoke-virtual/range {v18 .. v19}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v7, Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;->mTitle:Ljava/lang/String;

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getStarTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v7, Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;->mStarTheaterClick:Landroid/view/View$OnClickListener;

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    new-instance v4, Lnet/flixster/android/lvi/LviIconPanel;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviIconPanel;-><init>()V

    .line 171
    .local v4, lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    move-object/from16 v18, v0

    const-string v19, "phone"

    invoke-virtual/range {v18 .. v19}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    .line 172
    const v18, 0x7f02010a

    move/from16 v0, v18

    iput v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mDrawableResource:I

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mCallTheaterListener:Landroid/view/View$OnClickListener;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    new-instance v4, Lnet/flixster/android/lvi/LviIconPanel;

    .end local v4           #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    invoke-direct {v4}, Lnet/flixster/android/lvi/LviIconPanel;-><init>()V

    .line 177
    .restart local v4       #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheaterAddressString:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    .line 178
    const v18, 0x7f020107

    move/from16 v0, v18

    iput v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mDrawableResource:I

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mMapTheaterListener:Landroid/view/View$OnClickListener;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isUsLocale()Z

    move-result v18

    if-nez v18, :cond_2

    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isCanadaLocale()Z

    move-result v18

    if-nez v18, :cond_2

    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isUkLocale()Z

    move-result v18

    if-nez v18, :cond_2

    .line 183
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isFrenchLocale()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 184
    :cond_2
    new-instance v4, Lnet/flixster/android/lvi/LviIconPanel;

    .end local v4           #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    invoke-direct {v4}, Lnet/flixster/android/lvi/LviIconPanel;-><init>()V

    .line 185
    .restart local v4       #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0c00dc

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    .line 186
    const v18, 0x7f020114

    move/from16 v0, v18

    iput v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mDrawableResource:I

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mYelpTheaterListener:Landroid/view/View$OnClickListener;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_3
    new-instance v3, Lnet/flixster/android/lvi/LviDatePanel;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviDatePanel;-><init>()V

    .line 192
    .local v3, lviDatePanel:Lnet/flixster/android/lvi/LviDatePanel;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v18

    check-cast v18, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual/range {v18 .. v18}, Lcom/flixster/android/activity/hc/Main;->getDateSelectOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v3, Lnet/flixster/android/lvi/LviDatePanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    new-instance v16, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct/range {v16 .. v16}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 196
    .local v16, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0c0046

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mShowtimesByMovieHash:Ljava/util/HashMap;

    move-object/from16 v18, v0

    if-eqz v18, :cond_8

    .line 206
    const-string v18, "FlxMain"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TheaterInfoList.LoadMoviesTask mShowtimesHash, size:"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mShowtimesByMovieHash:Ljava/util/HashMap;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->size()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mShowtimesByMovieHash:Ljava/util/HashMap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/HashMap;->size()I

    move-result v18

    if-nez v18, :cond_4

    .line 209
    new-instance v5, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 210
    .local v5, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    const v18, 0x7f0c0058

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v5, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 215
    .end local v5           #lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mShowtimesByMovieHash:Ljava/util/HashMap;

    move-object/from16 v17, v0

    .line 216
    .local v17, tempShowtimesByMovieHash:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;>;"
    if-eqz v17, :cond_6

    .line 218
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v14

    .line 219
    .local v14, showtimesMovieIdList:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_7

    .line 243
    .end local v14           #showtimesMovieIdList:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_6
    new-instance v2, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 244
    .local v2, footer:Lnet/flixster/android/lvi/LviFooter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 219
    .end local v2           #footer:Lnet/flixster/android/lvi/LviFooter;
    .restart local v14       #showtimesMovieIdList:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_7
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    .line 220
    .local v10, movieId:Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    new-instance v20, Lcom/flixster/android/view/Divider;

    move-object/from16 v0, v20

    invoke-direct {v0, v9}, Lcom/flixster/android/view/Divider;-><init>(Landroid/content/Context;)V

    sget v21, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_DIVIDER:I

    invoke-static/range {v20 .. v21}, Lnet/flixster/android/lvi/LviWrapper;->convertToLvi(Landroid/view/View;I)Lnet/flixster/android/lvi/Lvi;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    new-instance v6, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 223
    .local v6, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v6, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 224
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v6, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/ArrayList;

    .line 229
    .local v13, showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v15

    .line 230
    .local v15, size:I
    const/4 v11, 0x0

    .local v11, n:I
    :goto_1
    if-ge v11, v15, :cond_5

    .line 231
    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lnet/flixster/android/model/Showtimes;

    .line 232
    .local v12, showtimes:Lnet/flixster/android/model/Showtimes;
    new-instance v8, Lnet/flixster/android/lvi/LviShowtimes;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviShowtimes;-><init>()V

    .line 233
    .local v8, lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iput-object v0, v8, Lnet/flixster/android/lvi/LviShowtimes;->mTheater:Lnet/flixster/android/model/Theater;

    .line 234
    iput v15, v8, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimesListSize:I

    .line 235
    iput v11, v8, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimePosition:I

    .line 236
    iput-object v12, v8, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    .line 237
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getShowtimesClickListener()Landroid/view/View$OnClickListener;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v8, Lnet/flixster/android/lvi/LviShowtimes;->mBuyClick:Landroid/view/View$OnClickListener;

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 248
    .end local v6           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v8           #lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    .end local v10           #movieId:Ljava/lang/Long;
    .end local v11           #n:I
    .end local v12           #showtimes:Lnet/flixster/android/model/Showtimes;
    .end local v13           #showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    .end local v14           #showtimesMovieIdList:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v15           #size:I
    .end local v17           #tempShowtimesByMovieHash:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;>;"
    :cond_8
    new-instance v5, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 249
    .restart local v5       #lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    const-string v18, "Network Error Occured"

    move-object/from16 v0, v18

    iput-object v0, v5, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 63
    invoke-super {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/LviFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 66
    .local v0, view:Landroid/view/View;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "net.flixster.android.EXTRA_THEATER_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheaterId:J

    .line 67
    iget-wide v1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheaterId:J

    invoke-static {v1, v2}, Lnet/flixster/android/data/TheaterDao;->getTheater(J)Lnet/flixster/android/model/Theater;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    .line 68
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->makeTheaterAddressString()V

    .line 72
    return-object v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 85
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->trackPage()V

    .line 86
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->ScheduleLoadItemsTask(J)V

    .line 88
    return-void
.end method

.method protected retryAction()V
    .locals 2

    .prologue
    .line 142
    const-wide/16 v0, 0x3e8

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->ScheduleLoadItemsTask(J)V

    .line 143
    return-void
.end method

.method public trackPage()V
    .locals 5

    .prologue
    .line 348
    const/4 v0, 0x0

    .line 349
    .local v0, theaterName:Ljava/lang/String;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    if-eqz v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TheaterInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "name"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 352
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/theater/info"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Theater Info - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    return-void
.end method
