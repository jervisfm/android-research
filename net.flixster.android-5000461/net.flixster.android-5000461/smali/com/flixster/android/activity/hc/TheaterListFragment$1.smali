.class Lcom/flixster/android/activity/hc/TheaterListFragment$1;
.super Landroid/os/Handler;
.source "TheaterListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/TheaterListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TheaterListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    .line 233
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    .line 236
    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    .line 237
    .local v3, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/TheaterListFragment;->isRemoving()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    const-wide/16 v1, 0x0

    .line 242
    .local v1, id:J
    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 243
    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$0(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/lvi/LviTheater;

    iget-object v4, v4, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v4}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v1

    .line 248
    :cond_2
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_0

    .line 249
    new-instance v0, Landroid/content/Intent;

    const-string v4, "DETAILS"

    const/4 v5, 0x0

    const-class v6, Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-direct {v0, v4, v5, v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 250
    .local v0, i:Landroid/content/Intent;
    const-string v4, "net.flixster.android.EXTRA_THEATER_ID"

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 251
    const-class v4, Lcom/flixster/android/activity/hc/TheaterInfoFragment;

    invoke-virtual {v3, v0, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 244
    .end local v0           #i:Landroid/content/Intent;
    :cond_3
    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$1(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 245
    iget-object v4, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$1;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$1(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Theater;

    invoke-virtual {v4}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v1

    goto :goto_1
.end method
