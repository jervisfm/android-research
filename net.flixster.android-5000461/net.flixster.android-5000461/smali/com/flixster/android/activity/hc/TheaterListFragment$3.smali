.class Lcom/flixster/android/activity/hc/TheaterListFragment$3;
.super Ljava/lang/Object;
.source "TheaterListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/TheaterListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TheaterListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$3;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    .line 486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 488
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 498
    :goto_0
    return-void

    .line 491
    :pswitch_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$3;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    #setter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavSelect:I
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$4(Lcom/flixster/android/activity/hc/TheaterListFragment;I)V

    .line 492
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$3;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$3;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 493
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$3;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$3;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavSelect:I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$5(Lcom/flixster/android/activity/hc/TheaterListFragment;)I

    move-result v1

    const-wide/16 v2, 0x64

    #calls: Lcom/flixster/android/activity/hc/TheaterListFragment;->ScheduleLoadItemsTask(IJ)V
    invoke-static {v0, v1, v2, v3}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$6(Lcom/flixster/android/activity/hc/TheaterListFragment;IJ)V

    goto :goto_0

    .line 488
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
