.class Lcom/flixster/android/activity/hc/TheaterListFragment$4;
.super Ljava/lang/Object;
.source "TheaterListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/TheaterListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TheaterListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    .line 501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 503
    const-string v1, "FlxMain"

    const-string v2, "TheaterListPage.mSettingsClickListener"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lnet/flixster/android/SettingsPage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 505
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$4;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    invoke-virtual {v1, v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->startActivity(Landroid/content/Intent;)V

    .line 506
    return-void
.end method
