.class Lcom/flixster/android/activity/hc/TheaterListFragment$5;
.super Ljava/util/TimerTask;
.source "TheaterListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/TheaterListFragment;->ScheduleLoadItemsTask(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

.field private final synthetic val$navSelection:I


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TheaterListFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    iput p2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->val$navSelection:I

    .line 170
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const-wide/16 v8, 0x0

    const/4 v14, 0x1

    .line 173
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    check-cast v13, Lcom/flixster/android/activity/hc/Main;

    .line 174
    .local v13, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v13, :cond_0

    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    const-string v0, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "TheaterListPage.ScheduleLoadItemsTask.run navSelection:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->val$navSelection:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$1(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 181
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v5

    .line 182
    .local v5, favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 183
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v1

    .line 184
    .local v1, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v3

    .line 185
    .local v3, longitude:D
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$1(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static/range {v0 .. v5}, Lnet/flixster/android/data/TheaterDao;->findTheatersLocation(Ljava/util/List;DDLjava/util/HashMap;)V

    .line 195
    .end local v1           #latitude:D
    .end local v3           #longitude:D
    .end local v5           #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_2
    :goto_1
    iget v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->val$navSelection:I

    packed-switch v0, :pswitch_data_0

    .line 207
    :goto_2
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mUpdateHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 209
    if-eqz v13, :cond_3

    .line 210
    iget-object v0, v13, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 212
    :cond_3
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    iget-boolean v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->isInitialContentLoaded:Z

    if-nez v0, :cond_4

    .line 213
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    const/4 v6, 0x1

    iput-boolean v6, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->isInitialContentLoaded:Z

    .line 214
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->initialContentLoadingHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$10(Lcom/flixster/android/activity/hc/TheaterListFragment;)Landroid/os/Handler;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :cond_4
    if-eqz v13, :cond_0

    iget-object v0, v13, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, v13, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, v13, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 186
    .restart local v5       #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_5
    :try_start_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v6

    cmpl-double v0, v6, v8

    if-eqz v0, :cond_6

    .line 187
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v6

    cmpl-double v0, v6, v8

    if-eqz v0, :cond_6

    .line 188
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$1(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v7

    .line 189
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v9

    move-object v11, v5

    .line 188
    invoke-static/range {v6 .. v11}, Lnet/flixster/android/data/TheaterDao;->findTheatersLocation(Ljava/util/List;DDLjava/util/HashMap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 216
    .end local v5           #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catch_0
    move-exception v12

    .line 217
    .local v12, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v0, "FlxMain"

    const-string v6, "TheaterListPage.ScheduleLoadItemsTask DaoException"

    invoke-static {v0, v6, v12}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 218
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    invoke-virtual {v0, v12}, Lcom/flixster/android/activity/hc/TheaterListFragment;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 220
    if-eqz v13, :cond_0

    iget-object v0, v13, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, v13, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, v13, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 191
    .end local v12           #de:Lnet/flixster/android/data/DaoException;
    .restart local v5       #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_6
    :try_start_3
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #getter for: Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$1(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6, v5}, Lnet/flixster/android/data/TheaterDao;->findTheaters(Ljava/util/List;Ljava/lang/String;Ljava/util/HashMap;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 219
    .end local v5           #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catchall_0
    move-exception v0

    .line 220
    if-eqz v13, :cond_7

    iget-object v6, v13, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v6, :cond_7

    iget-object v6, v13, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 221
    iget-object v6, v13, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v6, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 223
    :cond_7
    throw v0

    .line 197
    :pswitch_0
    :try_start_4
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #calls: Lcom/flixster/android/activity/hc/TheaterListFragment;->setByDistanceLviList()V
    invoke-static {v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$7(Lcom/flixster/android/activity/hc/TheaterListFragment;)V

    goto/16 :goto_2

    .line 200
    :pswitch_1
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #calls: Lcom/flixster/android/activity/hc/TheaterListFragment;->setByNameLviList()V
    invoke-static {v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$8(Lcom/flixster/android/activity/hc/TheaterListFragment;)V

    goto/16 :goto_2

    .line 203
    :pswitch_2
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;->this$0:Lcom/flixster/android/activity/hc/TheaterListFragment;

    #calls: Lcom/flixster/android/activity/hc/TheaterListFragment;->updateMap()V
    invoke-static {v0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->access$9(Lcom/flixster/android/activity/hc/TheaterListFragment;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_2

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
