.class public Lcom/flixster/android/activity/hc/TheaterListFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "TheaterListFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final KEY_THEATERS_FILTER:Ljava/lang/String; = "KEY_THEATERS_FILTER"

.field public static mTheaterDistanceComparator:Lnet/flixster/android/model/TheaterDistanceComparator;

.field public static mTheaterNameComparator:Lnet/flixster/android/model/TheaterNameComparator;


# instance fields
.field private final DISTANCES_IMPERIAL:[D

.field private final DISTANCES_METRIC:[D

.field private initialContentLoadingHandler:Landroid/os/Handler;

.field private mDistanceString:Ljava/lang/String;

.field private final mFavorites:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/lvi/Lvi;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationStringHandler:Landroid/os/Handler;

.field private mLocationStringTextView:Landroid/widget/TextView;

.field private mNavBar:Lcom/flixster/android/view/SubNavBar;

.field private mNavListener:Landroid/view/View$OnClickListener;

.field private mNavSelect:I

.field private mNavbarHolder:Landroid/widget/LinearLayout;

.field private mSettingsClickListener:Landroid/view/View$OnClickListener;

.field private final mTheaters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x6

    .line 46
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDistanceString:Ljava/lang/String;

    .line 66
    new-array v0, v1, [D

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->DISTANCES_IMPERIAL:[D

    .line 67
    new-array v0, v1, [D

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->DISTANCES_METRIC:[D

    .line 233
    new-instance v0, Lcom/flixster/android/activity/hc/TheaterListFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TheaterListFragment$1;-><init>(Lcom/flixster/android/activity/hc/TheaterListFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    .line 283
    new-instance v0, Lcom/flixster/android/activity/hc/TheaterListFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TheaterListFragment$2;-><init>(Lcom/flixster/android/activity/hc/TheaterListFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mLocationStringHandler:Landroid/os/Handler;

    .line 486
    new-instance v0, Lcom/flixster/android/activity/hc/TheaterListFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TheaterListFragment$3;-><init>(Lcom/flixster/android/activity/hc/TheaterListFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavListener:Landroid/view/View$OnClickListener;

    .line 501
    new-instance v0, Lcom/flixster/android/activity/hc/TheaterListFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TheaterListFragment$4;-><init>(Lcom/flixster/android/activity/hc/TheaterListFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mSettingsClickListener:Landroid/view/View$OnClickListener;

    .line 46
    return-void

    .line 66
    nop

    :array_0
    .array-data 0x8
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0xf0t 0x3ft
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x8t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x14t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x24t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x2et 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x34t 0x40t
    .end array-data

    .line 67
    :array_1
    .array-data 0x8
        0xd7t 0xa3t 0x70t 0x3dt 0xat 0xd7t 0xf3t 0x3ft
        0xe1t 0x7at 0x14t 0xaet 0x47t 0xe1t 0x8t 0x40t
        0xe1t 0x7at 0x14t 0xaet 0x47t 0xe1t 0x18t 0x40t
        0xa4t 0x70t 0x3dt 0xat 0xd7t 0xa3t 0x22t 0x40t
        0x14t 0xaet 0x47t 0xe1t 0x7at 0x14t 0x2ft 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0xc0t 0x35t 0x40t
    .end array-data
.end method

.method private declared-synchronized ScheduleLoadItemsTask(IJ)V
    .locals 4
    .parameter "navSelection"
    .parameter "delay"

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 170
    new-instance v0, Lcom/flixster/android/activity/hc/TheaterListFragment$5;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/activity/hc/TheaterListFragment$5;-><init>(Lcom/flixster/android/activity/hc/TheaterListFragment;I)V

    .line 227
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 228
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 229
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v0, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    :cond_0
    monitor-exit p0

    return-void

    .line 168
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    .end local v1           #main:Lcom/flixster/android/activity/hc/Main;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$10(Lcom/flixster/android/activity/hc/TheaterListFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 233
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/TheaterListFragment;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mLocationStringTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/TheaterListFragment;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDistanceString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/TheaterListFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58
    iput p1, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavSelect:I

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/TheaterListFragment;)I
    .locals 1
    .parameter

    .prologue
    .line 58
    iget v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavSelect:I

    return v0
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/TheaterListFragment;IJ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 167
    invoke-direct {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/TheaterListFragment;->ScheduleLoadItemsTask(IJ)V

    return-void
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/TheaterListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->setByDistanceLviList()V

    return-void
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/TheaterListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 384
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->setByNameLviList()V

    return-void
.end method

.method static synthetic access$9(Lcom/flixster/android/activity/hc/TheaterListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 449
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->updateMap()V

    return-void
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 510
    const/4 v0, 0x2

    return v0
.end method

.method private setByDistanceLviList()V
    .locals 23

    .prologue
    .line 295
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v12

    check-cast v12, Lcom/flixster/android/activity/hc/Main;

    .line 296
    .local v12, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v12, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->isRemoving()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v8

    .line 303
    .local v8, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v11

    .line 307
    .local v11, mTheatersCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    sget-object v17, Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaterDistanceComparator:Lnet/flixster/android/model/TheaterDistanceComparator;

    move-object/from16 v0, v17

    invoke-static {v11, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 310
    const-wide/high16 v2, 0x3ff0

    .line 311
    .local v2, conversion:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getDistanceType()I

    move-result v17

    if-nez v17, :cond_4

    .line 312
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->DISTANCES_IMPERIAL:[D

    .line 317
    .local v7, distances:[D
    :goto_1
    const-wide/16 v4, 0x0

    .line 318
    .local v4, currentDistance:D
    const/4 v6, 0x0

    .line 319
    .local v6, distanceIndex:I
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_2
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_5

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    if-lez v17, :cond_3

    .line 362
    const-string v17, "FlxMain"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "TheaterListPage mFavorites.size:"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " mDataHolder.size():"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 362
    invoke-static/range {v17 .. v18}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    new-instance v15, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v15}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 366
    .local v15, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0c001e

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v15, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v15}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v17 .. v19}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 369
    const-string v17, "FlxMain"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "TheaterListPage postadd mDataHolder.size():"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    .end local v15           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_3
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v17

    if-nez v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    if-nez v17, :cond_0

    .line 373
    new-instance v9, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v9}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 374
    .local v9, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0c0059

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v9, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 314
    .end local v4           #currentDistance:D
    .end local v6           #distanceIndex:I
    .end local v7           #distances:[D
    .end local v9           #lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    :cond_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->DISTANCES_METRIC:[D

    .line 315
    .restart local v7       #distances:[D
    const-wide v2, 0x3ff9be76c0000000L

    goto/16 :goto_1

    .line 319
    .restart local v4       #currentDistance:D
    .restart local v6       #distanceIndex:I
    :cond_5
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lnet/flixster/android/model/Theater;

    .line 320
    .local v16, theater:Lnet/flixster/android/model/Theater;
    invoke-virtual/range {v16 .. v16}, Lnet/flixster/android/model/Theater;->getMiles()D

    move-result-wide v13

    .line 322
    .local v13, miles:D
    cmpl-double v18, v13, v4

    if-lez v18, :cond_7

    .line 323
    :cond_6
    aget-wide v18, v7, v6

    cmpg-double v18, v18, v13

    if-ltz v18, :cond_8

    .line 329
    :goto_3
    new-instance v15, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v15}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 331
    .restart local v15       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    array-length v0, v7

    move/from16 v18, v0

    move/from16 v0, v18

    if-ne v6, v0, :cond_9

    .line 332
    const-wide v4, 0x412e848000000000L

    .line 334
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0c00e4

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    .line 335
    array-length v0, v7

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    aget-wide v21, v7, v21

    mul-double v21, v21, v2

    invoke-static/range {v21 .. v22}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v21

    aput-object v21, v19, v20

    .line 333
    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v15, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 342
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    .end local v15           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_7
    new-instance v10, Lnet/flixster/android/lvi/LviTheater;

    invoke-direct {v10}, Lnet/flixster/android/lvi/LviTheater;-><init>()V

    .line 346
    .local v10, lviTheater:Lnet/flixster/android/lvi/LviTheater;
    move-object/from16 v0, v16

    iput-object v0, v10, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    .line 347
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v10, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    invoke-virtual/range {v16 .. v16}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 353
    new-instance v10, Lnet/flixster/android/lvi/LviTheater;

    .end local v10           #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    invoke-direct {v10}, Lnet/flixster/android/lvi/LviTheater;-><init>()V

    .line 354
    .restart local v10       #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    move-object/from16 v0, v16

    iput-object v0, v10, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    .line 355
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v10, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 324
    .end local v10           #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    :cond_8
    add-int/lit8 v6, v6, 0x1

    .line 325
    array-length v0, v7

    move/from16 v18, v0

    move/from16 v0, v18

    if-ne v6, v0, :cond_6

    goto/16 :goto_3

    .line 337
    .restart local v15       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_9
    aget-wide v4, v7, v6

    .line 339
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0c00e3

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    .line 340
    aget-wide v21, v7, v6

    mul-double v21, v21, v2

    invoke-static/range {v21 .. v22}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v21

    aput-object v21, v19, v20

    .line 338
    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v15, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    goto/16 :goto_4
.end method

.method private setByNameLviList()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 385
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Lcom/flixster/android/activity/hc/Main;

    .line 386
    .local v6, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->isRemoving()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 447
    :cond_0
    :goto_0
    return-void

    .line 392
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v1

    .line 394
    .local v1, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v9, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 395
    iget-object v9, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 397
    iget-object v9, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    .line 398
    .local v5, mTheatersCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    sget-object v9, Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaterNameComparator:Lnet/flixster/android/model/TheaterNameComparator;

    invoke-static {v5, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 401
    const/4 v0, 0x0

    .line 403
    .local v0, currentLetter:C
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_4

    .line 430
    iget-object v9, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_3

    .line 431
    new-instance v7, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v7}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 432
    .local v7, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c001e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 433
    iget-object v9, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v9, v12, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 434
    iget-object v9, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v9, v12, v10}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 437
    .end local v7           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-nez v9, :cond_0

    .line 438
    new-instance v3, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 439
    .local v3, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c0059

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v3, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 440
    iget-object v9, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 403
    .end local v3           #lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lnet/flixster/android/model/Theater;

    .line 404
    .local v8, theater:Lnet/flixster/android/model/Theater;
    const-string v10, "name"

    invoke-virtual {v8, v10}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v12}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 405
    .local v2, firstLetter:C
    invoke-static {v2}, Ljava/lang/Character;->isLetter(C)Z

    move-result v10

    if-nez v10, :cond_5

    .line 406
    const/16 v2, 0x23

    .line 408
    :cond_5
    if-eq v0, v2, :cond_6

    .line 409
    new-instance v7, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v7}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 410
    .restart local v7       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 411
    iget-object v10, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    move v0, v2

    .line 414
    .end local v7           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_6
    new-instance v4, Lnet/flixster/android/lvi/LviTheater;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviTheater;-><init>()V

    .line 415
    .local v4, lviTheater:Lnet/flixster/android/lvi/LviTheater;
    iput-object v8, v4, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    .line 416
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v4, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    .line 418
    iget-object v10, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    invoke-virtual {v8}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 422
    new-instance v4, Lnet/flixster/android/lvi/LviTheater;

    .end local v4           #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    invoke-direct {v4}, Lnet/flixster/android/lvi/LviTheater;-><init>()V

    .line 423
    .restart local v4       #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    iput-object v8, v4, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    .line 424
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v4, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    .line 426
    iget-object v10, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method private setLocationString()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 263
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getTheaterDistance()I

    move-result v0

    .line 264
    .local v0, distance:I
    const/4 v1, 0x0

    .line 265
    .local v1, place:Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 266
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentZip()Ljava/lang/String;

    move-result-object v1

    .line 274
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    .line 275
    :cond_1
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c010e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDistanceString:Ljava/lang/String;

    .line 280
    :goto_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mLocationStringHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 281
    return-void

    .line 268
    :cond_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserZip()Ljava/lang/String;

    move-result-object v1

    .line 269
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 270
    :cond_3
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserCity()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 277
    :cond_4
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 278
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object v1, v3, v4

    .line 277
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mDistanceString:Ljava/lang/String;

    goto :goto_1
.end method

.method private updateMap()V
    .locals 0

    .prologue
    .line 471
    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const v6, 0x7f070258

    .line 90
    invoke-super {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/LviFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 92
    .local v1, view:Landroid/view/View;
    new-instance v2, Lnet/flixster/android/model/TheaterDistanceComparator;

    invoke-direct {v2}, Lnet/flixster/android/model/TheaterDistanceComparator;-><init>()V

    sput-object v2, Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaterDistanceComparator:Lnet/flixster/android/model/TheaterDistanceComparator;

    .line 93
    new-instance v2, Lnet/flixster/android/model/TheaterNameComparator;

    invoke-direct {v2}, Lnet/flixster/android/model/TheaterNameComparator;-><init>()V

    sput-object v2, Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaterNameComparator:Lnet/flixster/android/model/TheaterNameComparator;

    .line 95
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 96
    const v2, 0x7f0700f3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    .line 98
    new-instance v2, Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavBar:Lcom/flixster/android/view/SubNavBar;

    .line 99
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavBar:Lcom/flixster/android/view/SubNavBar;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavListener:Landroid/view/View$OnClickListener;

    const v4, 0x7f0c0123

    const v5, 0x7f0c0124

    invoke-virtual {v2, v3, v4, v5}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;II)V

    .line 100
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavBar:Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {v2, v6}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 101
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavBar:Lcom/flixster/android/view/SubNavBar;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 103
    const v2, 0x7f0700f5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    .line 104
    const v2, 0x7f030086

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 105
    .local v0, distanceBar:Landroid/view/View;
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 107
    const v2, 0x7f070260

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mLocationStringTextView:Landroid/widget/TextView;

    .line 108
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mLocationStringTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mSettingsClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iput v6, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavSelect:I

    .line 115
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onPause()V

    .line 135
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mTheaters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 136
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 121
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 126
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->setLocationString()V

    .line 128
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TheaterListFragment;->trackPage()V

    .line 129
    iget v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavSelect:I

    const-wide/16 v1, 0x64

    invoke-direct {p0, v0, v1, v2}, Lcom/flixster/android/activity/hc/TheaterListFragment;->ScheduleLoadItemsTask(IJ)V

    .line 130
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 140
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/LviFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 141
    const-string v0, "FlxMain"

    const-string v1, "TheaterListPage.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v0, "LISTSTATE_NAV"

    iget v1, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavSelect:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    return-void
.end method

.method protected retryAction()V
    .locals 3

    .prologue
    .line 258
    iget v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavSelect:I

    const-wide/16 v1, 0x3e8

    invoke-direct {p0, v0, v1, v2}, Lcom/flixster/android/activity/hc/TheaterListFragment;->ScheduleLoadItemsTask(IJ)V

    .line 259
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 154
    iget v0, p0, Lcom/flixster/android/activity/hc/TheaterListFragment;->mNavSelect:I

    packed-switch v0, :pswitch_data_0

    .line 165
    :goto_0
    return-void

    .line 156
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/theaters/nearby"

    const-string v2, "Theaters - Nearby"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 159
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/theaters/name"

    const-string v2, "Theaters - By Name"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 162
    :pswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/theaters/map"

    const-string v2, "Theaters - By Name"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
