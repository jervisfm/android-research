.class Lcom/flixster/android/activity/hc/TicketInfoFragment$1;
.super Landroid/os/Handler;
.source "TicketInfoFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/TicketInfoFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TicketInfoFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    .line 210
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "msg"

    .prologue
    .line 214
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 215
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$1;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$0(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 220
    :cond_2
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 221
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/flixster/android/activity/hc/Main;->showDialog(I)V

    goto :goto_0
.end method
