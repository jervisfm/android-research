.class Lcom/flixster/android/activity/hc/TicketInfoFragment$3;
.super Landroid/os/Handler;
.source "TicketInfoFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/TicketInfoFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TicketInfoFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    .line 244
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 17
    .parameter "msg"

    .prologue
    .line 248
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-virtual {v12}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/flixster/android/activity/hc/Main;

    .line 249
    .local v5, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v5, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-virtual {v12}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->isRemoving()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    const-string v12, "FlxMain"

    const-string v13, "TicketInfoPage.postShowtimesLoadHandler"

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-virtual {v12}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getView()Landroid/view/View;

    move-result-object v11

    .line 255
    .local v11, view:Landroid/view/View;
    const/4 v6, 0x0

    .line 256
    .local v6, showtimes:Lnet/flixster/android/model/Showtimes;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v12}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$1(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v12

    invoke-virtual {v12}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;

    move-result-object v8

    .line 257
    .local v8, showtimesListByMovieHash:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovieId:J
    invoke-static {v12}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$2(Lcom/flixster/android/activity/hc/TicketInfoFragment;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 258
    .local v7, showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_3

    .line 263
    const-string v12, "FlxMain"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "TicketInfoPage.postShowtimesLoadHandler showtimes:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    invoke-virtual {v5}, Lcom/flixster/android/activity/hc/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020085

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 267
    .local v1, buyIcon:Landroid/graphics/drawable/Drawable;
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v14

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v15

    invoke-virtual {v1, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 269
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mListingLayout:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$4(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 270
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mListingPastLayout:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$5(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 272
    const v12, 0x7f070275

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 273
    .local v10, totalShowtimes:Landroid/widget/TextView;
    new-instance v12, Ljava/lang/StringBuilder;

    iget-object v13, v6, Lnet/flixster/android/model/Showtimes;->mListings:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, " showtimes total"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    const-string v12, "FlxMain"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "TicketInfoPage.postShowtimesLoadHandler showtimes.mListings:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v14, v6, Lnet/flixster/android/model/Showtimes;->mListings:Ljava/util/ArrayList;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v12, v6, Lnet/flixster/android/model/Showtimes;->mListings:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_4

    .line 304
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mListingPastLayout:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$5(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v12

    if-nez v12, :cond_5

    .line 305
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;
    invoke-static {v12}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$6(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/widget/RelativeLayout;

    move-result-object v12

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 258
    .end local v1           #buyIcon:Landroid/graphics/drawable/Drawable;
    .end local v10           #totalShowtimes:Landroid/widget/TextView;
    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lnet/flixster/android/model/Showtimes;

    .line 259
    .local v9, tempShowtimes:Lnet/flixster/android/model/Showtimes;
    iget-object v13, v9, Lnet/flixster/android/model/Showtimes;->mShowtimesTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowtimesTitle:Ljava/lang/String;
    invoke-static {v14}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$3(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 260
    move-object v6, v9

    goto/16 :goto_1

    .line 276
    .end local v9           #tempShowtimes:Lnet/flixster/android/model/Showtimes;
    .restart local v1       #buyIcon:Landroid/graphics/drawable/Drawable;
    .restart local v10       #totalShowtimes:Landroid/widget/TextView;
    :cond_4
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Listing;

    .line 277
    .local v2, listing:Lnet/flixster/android/model/Listing;
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 278
    .local v3, listingView:Landroid/widget/TextView;
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 v13, -0x1

    .line 279
    const/4 v14, -0x2

    .line 278
    invoke-direct {v4, v13, v14}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 281
    .local v4, lp:Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 282
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 283
    const/16 v13, 0x10

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 284
    invoke-virtual {v2}, Lnet/flixster/android/model/Listing;->getDisplayTimeDate()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-virtual {v13}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    const v14, 0x7f0d0070

    invoke-virtual {v3, v13, v14}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 289
    const v13, 0x7f020075

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 290
    const/16 v13, 0xa

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 291
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v3, v13, v14, v1, v15}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 292
    const/16 v13, 0x1e

    const/16 v14, 0x14

    const/16 v15, 0x14

    const/16 v16, 0x14

    move/from16 v0, v16

    invoke-virtual {v3, v13, v14, v15, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 293
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mListingLayout:Landroid/widget/LinearLayout;
    invoke-static {v13}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$4(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/widget/LinearLayout;

    move-result-object v13

    invoke-virtual {v13, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 307
    .end local v2           #listing:Lnet/flixster/android/model/Listing;
    .end local v3           #listingView:Landroid/widget/TextView;
    .end local v4           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;
    invoke-static {v12}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$6(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/widget/RelativeLayout;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method
