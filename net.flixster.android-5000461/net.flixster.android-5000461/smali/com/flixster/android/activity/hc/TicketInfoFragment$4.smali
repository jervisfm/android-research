.class Lcom/flixster/android/activity/hc/TicketInfoFragment$4;
.super Ljava/util/TimerTask;
.source "TicketInfoFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/TicketInfoFragment;->ScheduleLoadShowtimesTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TicketInfoFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    .line 146
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 150
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 151
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mStartLoadingShowtimesDialogHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$7(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 158
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TheaterDao.Schedule mMovie.getId():"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovieId:J
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$2(Lcom/flixster/android/activity/hc/TicketInfoFragment;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheaterId:J
    invoke-static {v3}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$8(Lcom/flixster/android/activity/hc/TicketInfoFragment;)J

    move-result-wide v3

    iget-object v5, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovieId:J
    invoke-static {v5}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$2(Lcom/flixster/android/activity/hc/TicketInfoFragment;)J

    move-result-wide v5

    .line 160
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v7

    .line 159
    invoke-static {v3, v4, v5, v6, v7}, Lnet/flixster/android/data/TheaterDao;->findTheaterByTheaterMoviePair(JJLjava/util/Date;)Lnet/flixster/android/model/Theater;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$9(Lcom/flixster/android/activity/hc/TicketInfoFragment;Lnet/flixster/android/model/Theater;)V

    .line 161
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TicketInfoPage.ScheduleLoadShowtimesTask().run() mTheater.getShowtimesHash():"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 162
    iget-object v4, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$1(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Lnet/flixster/android/model/Theater;

    move-result-object v4

    invoke-virtual {v4}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 161
    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mStopLoadingShowtimesDialogHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$10(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 164
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->postShowtimesLoadHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$11(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TicketInfoPage.ScheduleLoadShowtimesTask Exception mNetworkTrys:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mNetworkTrys:I
    invoke-static {v4}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$12(Lcom/flixster/android/activity/hc/TicketInfoFragment;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 168
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mNetworkTrys:I
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$12(Lcom/flixster/android/activity/hc/TicketInfoFragment;)I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_2

    .line 169
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #calls: Lcom/flixster/android/activity/hc/TicketInfoFragment;->ScheduleLoadShowtimesTask()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$13(Lcom/flixster/android/activity/hc/TicketInfoFragment;)V

    .line 170
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mNetworkTrys:I
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$12(Lcom/flixster/android/activity/hc/TicketInfoFragment;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    #setter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mNetworkTrys:I
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$14(Lcom/flixster/android/activity/hc/TicketInfoFragment;I)V

    goto/16 :goto_0

    .line 172
    :cond_2
    const-string v2, "FlxMain"

    const-string v3, "TicketInfoPage.ScheduleLoadShowtimesTask SHOW NETWORK ERROR DIALOG"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;->this$0:Lcom/flixster/android/activity/hc/TicketInfoFragment;

    #getter for: Lcom/flixster/android/activity/hc/TicketInfoFragment;->mStopLoadingShowtimesDialogHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->access$10(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method
