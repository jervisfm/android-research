.class public Lcom/flixster/android/activity/hc/TicketInfoFragment;
.super Lcom/flixster/android/activity/hc/FlixsterFragment;
.source "TicketInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final DIALOGKEY_LOADING_SHOWTIMES:I = 0x1

.field public static final DIALOGKEY_NETWORKFAIL:I = 0x2


# instance fields
.field private mExtras:Landroid/os/Bundle;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mListingLayout:Landroid/widget/LinearLayout;

.field private mListingPastLayout:Landroid/widget/LinearLayout;

.field private mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

.field private mMovie:Lnet/flixster/android/model/Movie;

.field private mMovieId:J

.field private mNetworkTrys:I

.field private mPosterView:Landroid/widget/ImageView;

.field private mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

.field private mShowtimesTitle:Ljava/lang/String;

.field private mStartLoadingShowtimesDialogHandler:Landroid/os/Handler;

.field private mStopLoadingShowtimesDialogHandler:Landroid/os/Handler;

.field private mTheater:Lnet/flixster/android/model/Theater;

.field private mTheaterId:J

.field private postShowtimesLoadHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mNetworkTrys:I

    .line 210
    new-instance v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment$1;-><init>(Lcom/flixster/android/activity/hc/TicketInfoFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mStartLoadingShowtimesDialogHandler:Landroid/os/Handler;

    .line 227
    new-instance v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment$2;-><init>(Lcom/flixster/android/activity/hc/TicketInfoFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mStopLoadingShowtimesDialogHandler:Landroid/os/Handler;

    .line 244
    new-instance v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment$3;-><init>(Lcom/flixster/android/activity/hc/TicketInfoFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->postShowtimesLoadHandler:Landroid/os/Handler;

    .line 42
    return-void
.end method

.method private ScheduleLoadShowtimesTask()V
    .locals 4

    .prologue
    .line 146
    new-instance v0, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment$4;-><init>(Lcom/flixster/android/activity/hc/TicketInfoFragment;)V

    .line 180
    .local v0, loadShowtimesTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 181
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 183
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Lnet/flixster/android/model/Theater;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    return-object v0
.end method

.method static synthetic access$10(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 227
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mStopLoadingShowtimesDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$11(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 244
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->postShowtimesLoadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$12(Lcom/flixster/android/activity/hc/TicketInfoFragment;)I
    .locals 1
    .parameter

    .prologue
    .line 59
    iget v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mNetworkTrys:I

    return v0
.end method

.method static synthetic access$13(Lcom/flixster/android/activity/hc/TicketInfoFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->ScheduleLoadShowtimesTask()V

    return-void
.end method

.method static synthetic access$14(Lcom/flixster/android/activity/hc/TicketInfoFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59
    iput p1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mNetworkTrys:I

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/TicketInfoFragment;)J
    .locals 2
    .parameter

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovieId:J

    return-wide v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowtimesTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mListingLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mListingPastLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/TicketInfoFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 210
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mStartLoadingShowtimesDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/TicketInfoFragment;)J
    .locals 2
    .parameter

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheaterId:J

    return-wide v0
.end method

.method static synthetic access$9(Lcom/flixster/android/activity/hc/TicketInfoFragment;Lnet/flixster/android/model/Theater;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    return-void
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 337
    const/16 v0, 0x69

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/flixster/android/activity/hc/TicketInfoFragment;
    .locals 1
    .parameter "bundle"

    .prologue
    .line 64
    new-instance v0, Lcom/flixster/android/activity/hc/TicketInfoFragment;

    invoke-direct {v0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;-><init>()V

    .line 65
    .local v0, tf:Lcom/flixster/android/activity/hc/TicketInfoFragment;
    invoke-virtual {v0, p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->setArguments(Landroid/os/Bundle;)V

    .line 66
    return-object v0
.end method

.method private populateDetails()V
    .locals 8

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getView()Landroid/view/View;

    move-result-object v5

    .line 125
    .local v5, view:Landroid/view/View;
    const v6, 0x7f07026f

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 126
    .local v4, theaterName:Landroid/widget/TextView;
    const v6, 0x7f07026c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 127
    .local v1, movieActors:Landroid/widget/TextView;
    const v6, 0x7f07026d

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 128
    .local v2, movieMeta:Landroid/widget/TextView;
    const v6, 0x7f070270

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 130
    .local v3, theaterAddress:Landroid/widget/TextView;
    iget-object v6, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v7, "name"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v6, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v7, "address"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v6, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v7, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v6, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v7, "meta"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v6, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v6}, Lnet/flixster/android/model/Movie;->getThumbnailPoster()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    .line 136
    iget-object v6, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mPosterView:Landroid/widget/ImageView;

    const v7, 0x7f02014f

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v6, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v7, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mPosterView:Landroid/widget/ImageView;

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Movie;->getThumbnailBackedProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 139
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 140
    iget-object v6, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mPosterView:Landroid/widget/ImageView;

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 313
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Landroid/widget/TextView;

    if-ne v1, v2, :cond_1

    .line 314
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Listing;

    .line 315
    .local v0, l:Lnet/flixster/android/model/Listing;
    iget-object v1, v0, Lnet/flixster/android/model/Listing;->ticketUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v2}, Lnet/flixster/android/Starter;->launchBrowser(Ljava/lang/String;Landroid/content/Context;)V

    .line 334
    .end local v0           #l:Lnet/flixster/android/model/Listing;
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f070273

    if-ne v1, v2, :cond_0

    .line 330
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mListingPastLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 331
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 187
    packed-switch p1, :pswitch_data_0

    .line 207
    :goto_0
    return-object v0

    .line 189
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    .line 190
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 192
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 193
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 195
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 196
    const-string v2, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 197
    const-string v2, "Network Error"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 198
    const-string v2, "Retry"

    new-instance v3, Lcom/flixster/android/activity/hc/TicketInfoFragment$5;

    invoke-direct {v3, p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment$5;-><init>(Lcom/flixster/android/activity/hc/TicketInfoFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 205
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 187
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 71
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 72
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030087

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 74
    .local v1, view:Landroid/view/View;
    const v2, 0x7f07026b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 78
    .local v0, movieTitle:Landroid/widget/TextView;
    const v2, 0x7f070269

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mPosterView:Landroid/widget/ImageView;

    .line 80
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mExtras:Landroid/os/Bundle;

    .line 81
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mExtras:Landroid/os/Bundle;

    const-string v3, "id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheaterId:J

    .line 82
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mExtras:Landroid/os/Bundle;

    const-string v3, "MOVIE_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovieId:J

    .line 83
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mExtras:Landroid/os/Bundle;

    const-string v3, "SHOWTIMES_TITLE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowtimesTitle:Ljava/lang/String;

    .line 84
    iget-wide v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheaterId:J

    invoke-static {v2, v3}, Lnet/flixster/android/data/TheaterDao;->getTheater(J)Lnet/flixster/android/model/Theater;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    .line 85
    iget-wide v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovieId:J

    invoke-static {v2, v3}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovie:Lnet/flixster/android/model/Movie;

    .line 86
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TicketInfoPage.onCreate mThaterId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheaterId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mMovieId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mMovieId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 87
    const-string v4, " mShowtimesTitle:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowtimesTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowtimesTitle:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    const v2, 0x7f070277

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mListingLayout:Landroid/widget/LinearLayout;

    .line 97
    const v2, 0x7f070276

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mListingPastLayout:Landroid/widget/LinearLayout;

    .line 98
    const v2, 0x7f070273

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

    .line 99
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 100
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    return-object v1
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onResume()V

    .line 110
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->populateDetails()V

    .line 112
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->trackPage()V

    .line 114
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v0}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 115
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TicketInfoFragment;->ScheduleLoadShowtimesTask()V

    .line 121
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TicketInfoFragment;->postShowtimesLoadHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 342
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/tickets/info"

    const-string v2, "TicketInfoPage"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    return-void
.end method
