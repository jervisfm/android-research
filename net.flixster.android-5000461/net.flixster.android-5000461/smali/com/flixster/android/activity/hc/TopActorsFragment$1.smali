.class Lcom/flixster/android/activity/hc/TopActorsFragment$1;
.super Landroid/os/Handler;
.source "TopActorsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/TopActorsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TopActorsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$1;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    .line 163
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .parameter "msg"

    .prologue
    .line 166
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$1;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    .line 167
    .local v2, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$1;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    invoke-virtual {v3}, Lcom/flixster/android/activity/hc/TopActorsFragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$1;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    #getter for: Lcom/flixster/android/activity/hc/TopActorsFragment;->mTopThisWeek:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$0(Lcom/flixster/android/activity/hc/TopActorsFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 172
    new-instance v1, Landroid/content/Intent;

    const-string v3, "TOP_ACTOR"

    const/4 v4, 0x0

    const-class v5, Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-direct {v1, v3, v4, v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 173
    .local v1, intent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$1;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    #getter for: Lcom/flixster/android/activity/hc/TopActorsFragment;->mTopThisWeek:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$0(Lcom/flixster/android/activity/hc/TopActorsFragment;)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 174
    .local v0, actor:Lnet/flixster/android/model/Actor;
    const-string v3, "ACTOR_ID"

    iget-wide v4, v0, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 175
    const-string v3, "ACTOR_NAME"

    iget-object v4, v0, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    const-class v3, Lcom/flixster/android/activity/hc/ActorFragment;

    invoke-virtual {v2, v1, v3}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0
.end method
