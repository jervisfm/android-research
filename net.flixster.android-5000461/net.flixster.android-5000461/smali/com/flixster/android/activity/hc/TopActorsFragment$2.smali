.class Lcom/flixster/android/activity/hc/TopActorsFragment$2;
.super Ljava/lang/Object;
.source "TopActorsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/TopActorsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TopActorsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$2;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 232
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 241
    :goto_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$2;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->trackPage()V

    .line 242
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$2;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    iget-object v0, v0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$2;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 243
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$2;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$2;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    #getter for: Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavSelect:I
    invoke-static {v1}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$2(Lcom/flixster/android/activity/hc/TopActorsFragment;)I

    move-result v1

    const-wide/16 v2, 0x64

    #calls: Lcom/flixster/android/activity/hc/TopActorsFragment;->ScheduleLoadItemsTask(IJ)V
    invoke-static {v0, v1, v2, v3}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$3(Lcom/flixster/android/activity/hc/TopActorsFragment;IJ)V

    .line 244
    return-void

    .line 234
    :pswitch_0
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$2;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    const v1, 0x7f070258

    #setter for: Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavSelect:I
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$1(Lcom/flixster/android/activity/hc/TopActorsFragment;I)V

    goto :goto_0

    .line 237
    :pswitch_1
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$2;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    const v1, 0x7f070259

    #setter for: Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavSelect:I
    invoke-static {v0, v1}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$1(Lcom/flixster/android/activity/hc/TopActorsFragment;I)V

    goto :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
