.class Lcom/flixster/android/activity/hc/TopActorsFragment$3;
.super Ljava/util/TimerTask;
.source "TopActorsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/TopActorsFragment;->ScheduleLoadItemsTask(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

.field private final synthetic val$navSelection:I


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/TopActorsFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    iput p2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->val$navSelection:I

    .line 102
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 106
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 107
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/TopActorsFragment;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    :try_start_0
    iget v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->val$navSelection:I

    packed-switch v2, :pswitch_data_0

    .line 127
    :goto_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/TopActorsFragment;->mUpdateHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 129
    if-eqz v1, :cond_2

    .line 130
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 133
    :cond_2
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    iget-boolean v2, v2, Lcom/flixster/android/activity/hc/TopActorsFragment;->isInitialContentLoaded:Z

    if-nez v2, :cond_0

    .line 134
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/flixster/android/activity/hc/TopActorsFragment;->isInitialContentLoaded:Z

    .line 135
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    #getter for: Lcom/flixster/android/activity/hc/TopActorsFragment;->initialContentLoadingHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$9(Lcom/flixster/android/activity/hc/TopActorsFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 140
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TopActorsPage.ScheduleLoadItemsTask.run() mRetryCount:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    iget v4, v4, Lcom/flixster/android/activity/hc/TopActorsFragment;->mRetryCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->printStackTrace()V

    .line 142
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    iget v3, v2, Lcom/flixster/android/activity/hc/TopActorsFragment;->mRetryCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/flixster/android/activity/hc/TopActorsFragment;->mRetryCount:I

    .line 143
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    iget v2, v2, Lcom/flixster/android/activity/hc/TopActorsFragment;->mRetryCount:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_5

    .line 144
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    iget v3, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->val$navSelection:I

    const-wide/16 v4, 0x3e8

    #calls: Lcom/flixster/android/activity/hc/TopActorsFragment;->ScheduleLoadItemsTask(IJ)V
    invoke-static {v2, v3, v4, v5}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$3(Lcom/flixster/android/activity/hc/TopActorsFragment;IJ)V

    goto :goto_0

    .line 114
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_0
    :try_start_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    #getter for: Lcom/flixster/android/activity/hc/TopActorsFragment;->mTopThisWeek:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$0(Lcom/flixster/android/activity/hc/TopActorsFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 115
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    const-string v3, "popular"

    const/16 v4, 0x19

    invoke-static {v3, v4}, Lnet/flixster/android/data/ActorDao;->getTopActors(Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/hc/TopActorsFragment;->mTopThisWeek:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$4(Lcom/flixster/android/activity/hc/TopActorsFragment;Ljava/util/ArrayList;)V

    .line 117
    :cond_3
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    #calls: Lcom/flixster/android/activity/hc/TopActorsFragment;->setTopThisWeekLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$5(Lcom/flixster/android/activity/hc/TopActorsFragment;)V

    goto :goto_1

    .line 120
    :pswitch_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    #getter for: Lcom/flixster/android/activity/hc/TopActorsFragment;->mRandom:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$6(Lcom/flixster/android/activity/hc/TopActorsFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 121
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    const-string v3, "random"

    const/16 v4, 0x19

    invoke-static {v3, v4}, Lnet/flixster/android/data/ActorDao;->getTopActors(Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/hc/TopActorsFragment;->mRandom:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$7(Lcom/flixster/android/activity/hc/TopActorsFragment;Ljava/util/ArrayList;)V

    .line 123
    :cond_4
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    #calls: Lcom/flixster/android/activity/hc/TopActorsFragment;->setRandomLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/TopActorsFragment;->access$8(Lcom/flixster/android/activity/hc/TopActorsFragment;)V
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 146
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    :cond_5
    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;->this$0:Lcom/flixster/android/activity/hc/TopActorsFragment;

    iput v5, v2, Lcom/flixster/android/activity/hc/TopActorsFragment;->mRetryCount:I

    .line 147
    if-eqz v1, :cond_0

    .line 148
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 149
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
