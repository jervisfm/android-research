.class public Lcom/flixster/android/activity/hc/TopActorsFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "TopActorsFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final LIMIT_ACTORS:I = 0x19


# instance fields
.field private initialContentLoadingHandler:Landroid/os/Handler;

.field private mNavListener:Landroid/view/View$OnClickListener;

.field private mNavSelect:I

.field private mNavbar:Lcom/flixster/android/view/SubNavBar;

.field private mNavbarHolder:Landroid/widget/LinearLayout;

.field private mRandom:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field

.field private mTopThisWeek:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mTopThisWeek:Ljava/util/ArrayList;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mRandom:Ljava/util/ArrayList;

    .line 163
    new-instance v0, Lcom/flixster/android/activity/hc/TopActorsFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TopActorsFragment$1;-><init>(Lcom/flixster/android/activity/hc/TopActorsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    .line 230
    new-instance v0, Lcom/flixster/android/activity/hc/TopActorsFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/TopActorsFragment$2;-><init>(Lcom/flixster/android/activity/hc/TopActorsFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavListener:Landroid/view/View$OnClickListener;

    .line 28
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(IJ)V
    .locals 3
    .parameter "navSelection"
    .parameter "delay"

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    const-string v1, "FlxMain"

    const-string v2, "TopActors.PageScheduleLoadItemsTask(...)"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 102
    new-instance v0, Lcom/flixster/android/activity/hc/TopActorsFragment$3;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/activity/hc/TopActorsFragment$3;-><init>(Lcom/flixster/android/activity/hc/TopActorsFragment;I)V

    .line 158
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 159
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v1, v0, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :cond_0
    monitor-exit p0

    return-void

    .line 99
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/TopActorsFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mTopThisWeek:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/TopActorsFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37
    iput p1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavSelect:I

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/TopActorsFragment;)I
    .locals 1
    .parameter

    .prologue
    .line 37
    iget v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavSelect:I

    return v0
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/TopActorsFragment;IJ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 98
    invoke-direct {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/TopActorsFragment;->ScheduleLoadItemsTask(IJ)V

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/TopActorsFragment;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mTopThisWeek:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/TopActorsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->setTopThisWeekLviList()V

    return-void
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/TopActorsFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mRandom:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/TopActorsFragment;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 31
    iput-object p1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mRandom:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$8(Lcom/flixster/android/activity/hc/TopActorsFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->setRandomLviList()V

    return-void
.end method

.method static synthetic access$9(Lcom/flixster/android/activity/hc/TopActorsFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 163
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 248
    const/16 v0, 0x9

    return v0
.end method

.method private setRandomLviList()V
    .locals 7

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/activity/hc/Main;

    .line 207
    .local v4, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->isRemoving()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 228
    :cond_0
    return-void

    .line 212
    :cond_1
    iget-object v5, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 220
    const/4 v1, 0x0

    .line 221
    .local v1, id:I
    iget-object v5, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mRandom:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 222
    .local v0, actor:Lnet/flixster/android/model/Actor;
    add-int/lit8 v2, v1, 0x1

    .end local v1           #id:I
    .local v2, id:I
    iput v1, v0, Lnet/flixster/android/model/Actor;->positionId:I

    .line 223
    new-instance v3, Lnet/flixster/android/lvi/LviActor;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviActor;-><init>()V

    .line 224
    .local v3, lviActor:Lnet/flixster/android/lvi/LviActor;
    iput-object v0, v3, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    .line 225
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getActorClickListener()Landroid/view/View$OnClickListener;

    move-result-object v6

    iput-object v6, v3, Lnet/flixster/android/lvi/LviActor;->mActorClick:Landroid/view/View$OnClickListener;

    .line 226
    iget-object v6, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .end local v2           #id:I
    .restart local v1       #id:I
    goto :goto_0
.end method

.method private setTopThisWeekLviList()V
    .locals 7

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/flixster/android/activity/hc/Main;

    .line 183
    .local v4, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->isRemoving()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 203
    :cond_0
    return-void

    .line 188
    :cond_1
    iget-object v5, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 195
    const/4 v1, 0x0

    .line 196
    .local v1, id:I
    iget-object v5, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mTopThisWeek:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 197
    .local v0, actor:Lnet/flixster/android/model/Actor;
    add-int/lit8 v2, v1, 0x1

    .end local v1           #id:I
    .local v2, id:I
    iput v1, v0, Lnet/flixster/android/model/Actor;->positionId:I

    .line 198
    new-instance v3, Lnet/flixster/android/lvi/LviActor;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviActor;-><init>()V

    .line 199
    .local v3, lviActor:Lnet/flixster/android/lvi/LviActor;
    iput-object v0, v3, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    .line 200
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getActorClickListener()Landroid/view/View$OnClickListener;

    move-result-object v6

    iput-object v6, v3, Lnet/flixster/android/lvi/LviActor;->mActorClick:Landroid/view/View$OnClickListener;

    .line 201
    iget-object v6, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .end local v2           #id:I
    .restart local v1       #id:I
    goto :goto_0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const v5, 0x7f070258

    .line 42
    invoke-super {p0, p1, p2, p3}, Lcom/flixster/android/activity/hc/LviFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 43
    .local v0, view:Landroid/view/View;
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 44
    const v1, 0x7f0700f3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    .line 45
    new-instance v1, Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    .line 46
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavListener:Landroid/view/View$OnClickListener;

    const v3, 0x7f0c001d

    const v4, 0x7f0c0032

    invoke-virtual {v1, v2, v3, v4}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;II)V

    .line 47
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {v1, v5}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 49
    iget-object v1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavbarHolder:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 54
    iput v5, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavSelect:I

    .line 56
    return-object v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onPause()V

    .line 75
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mTopThisWeek:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 76
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mRandom:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 77
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 66
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/TopActorsFragment;->trackPage()V

    .line 68
    iget-object v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavbar:Lcom/flixster/android/view/SubNavBar;

    iget v1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavSelect:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 69
    iget v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavSelect:I

    const-wide/16 v1, 0x64

    invoke-direct {p0, v0, v1, v2}, Lcom/flixster/android/activity/hc/TopActorsFragment;->ScheduleLoadItemsTask(IJ)V

    .line 70
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/flixster/android/activity/hc/LviFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 82
    const-string v0, "FlxMain"

    const-string v1, "TopActorsPage.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v0, "LISTSTATE_NAV"

    iget v1, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavSelect:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 88
    iget v0, p0, Lcom/flixster/android/activity/hc/TopActorsFragment;->mNavSelect:I

    packed-switch v0, :pswitch_data_0

    .line 96
    :goto_0
    return-void

    .line 90
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/actor/top"

    const-string v2, "Top Actors Page for TopThisWeek"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/actor/random"

    const-string v2, "Top Actors Page for Random"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
