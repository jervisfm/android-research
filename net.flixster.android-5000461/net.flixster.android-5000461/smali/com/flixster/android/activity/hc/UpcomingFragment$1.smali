.class Lcom/flixster/android/activity/hc/UpcomingFragment$1;
.super Landroid/os/Handler;
.source "UpcomingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/UpcomingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/UpcomingFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$1;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    .line 98
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    .line 101
    iget-object v4, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$1;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/UpcomingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    .line 102
    .local v3, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$1;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/UpcomingFragment;->isRemoving()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    iget-object v4, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$1;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    #getter for: Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcoming:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/UpcomingFragment;->access$0(Lcom/flixster/android/activity/hc/UpcomingFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 107
    iget-object v4, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$1;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    #getter for: Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcoming:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/UpcomingFragment;->access$0(Lcom/flixster/android/activity/hc/UpcomingFragment;)Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v1

    .line 108
    .local v1, id:J
    new-instance v0, Landroid/content/Intent;

    const-string v4, "DETAILS"

    const/4 v5, 0x0

    const-class v6, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v0, v4, v5, v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 109
    .local v0, i:Landroid/content/Intent;
    const-string v4, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 110
    const-class v4, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v3, v0, v4}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0
.end method
