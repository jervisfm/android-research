.class Lcom/flixster/android/activity/hc/UpcomingFragment$2;
.super Ljava/util/TimerTask;
.source "UpcomingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/UpcomingFragment;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/UpcomingFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    .line 56
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 59
    iget-object v4, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/UpcomingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 60
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    invoke-virtual {v4}, Lcom/flixster/android/activity/hc/UpcomingFragment;->isRemoving()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    const-string v4, "FlxMain"

    const-string v5, "Upcoming.ScheduleLoadItemsTask.run"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :try_start_0
    iget-object v4, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    #getter for: Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcoming:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/UpcomingFragment;->access$0(Lcom/flixster/android/activity/hc/UpcomingFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 67
    iget-object v4, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    #getter for: Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcomingFeatured:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/flixster/android/activity/hc/UpcomingFragment;->access$1(Lcom/flixster/android/activity/hc/UpcomingFragment;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    #getter for: Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcoming:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/flixster/android/activity/hc/UpcomingFragment;->access$0(Lcom/flixster/android/activity/hc/UpcomingFragment;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    iget v6, v6, Lcom/flixster/android/activity/hc/UpcomingFragment;->mRetryCount:I

    if-nez v6, :cond_2

    move v2, v3

    :cond_2
    invoke-static {v4, v5, v2}, Lnet/flixster/android/data/MovieDao;->fetchUpcoming(Ljava/util/List;Ljava/util/List;Z)V

    .line 70
    :cond_3
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    #calls: Lcom/flixster/android/activity/hc/UpcomingFragment;->setUpcomingLviList()V
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UpcomingFragment;->access$2(Lcom/flixster/android/activity/hc/UpcomingFragment;)V

    .line 72
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpdateHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 74
    if-eqz v1, :cond_4

    .line 75
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 77
    :cond_4
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    iget-boolean v2, v2, Lcom/flixster/android/activity/hc/UpcomingFragment;->isInitialContentLoaded:Z

    if-nez v2, :cond_5

    .line 78
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/flixster/android/activity/hc/UpcomingFragment;->isInitialContentLoaded:Z

    .line 79
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    #getter for: Lcom/flixster/android/activity/hc/UpcomingFragment;->initialContentLoadingHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UpcomingFragment;->access$3(Lcom/flixster/android/activity/hc/UpcomingFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :cond_5
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_1
    const-string v2, "FlxMain"

    const-string v4, "UpcomingFragment.ScheduleLoadItemsTask.run DaoException"

    invoke-static {v2, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 83
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;->this$0:Lcom/flixster/android/activity/hc/UpcomingFragment;

    invoke-virtual {v2, v0}, Lcom/flixster/android/activity/hc/UpcomingFragment;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 84
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catchall_0
    move-exception v2

    .line 85
    if-eqz v1, :cond_6

    iget-object v4, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v4, :cond_6

    iget-object v4, v1, Lcom/flixster/android/activity/hc/Main;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 86
    iget-object v4, v1, Lcom/flixster/android/activity/hc/Main;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 88
    :cond_6
    throw v2
.end method
