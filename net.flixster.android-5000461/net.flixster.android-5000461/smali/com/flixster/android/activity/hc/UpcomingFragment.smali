.class public Lcom/flixster/android/activity/hc/UpcomingFragment;
.super Lcom/flixster/android/activity/hc/LviFragment;
.source "UpcomingFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private initialContentLoadingHandler:Landroid/os/Handler;

.field private final mUpcoming:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mUpcomingFeatured:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/LviFragment;-><init>()V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcoming:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcomingFeatured:Ljava/util/ArrayList;

    .line 98
    new-instance v0, Lcom/flixster/android/activity/hc/UpcomingFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/UpcomingFragment$1;-><init>(Lcom/flixster/android/activity/hc/UpcomingFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    .line 27
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 4
    .parameter "delay"

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UpcomingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    iget-object v2, v2, Lcom/flixster/android/activity/hc/Main;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 56
    new-instance v0, Lcom/flixster/android/activity/hc/UpcomingFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/UpcomingFragment$2;-><init>(Lcom/flixster/android/activity/hc/UpcomingFragment;)V

    .line 92
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UpcomingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 93
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 94
    iget-object v2, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v0, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :cond_0
    monitor-exit p0

    return-void

    .line 54
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    .end local v1           #main:Lcom/flixster/android/activity/hc/Main;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/UpcomingFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcoming:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/UpcomingFragment;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcomingFeatured:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/UpcomingFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/UpcomingFragment;->setUpcomingLviList()V

    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/UpcomingFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->initialContentLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x3

    return v0
.end method

.method private setUpcomingLviList()V
    .locals 12

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UpcomingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/flixster/android/activity/hc/Main;

    .line 122
    .local v5, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UpcomingFragment;->isRemoving()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    const-string v10, "FlxMain"

    const-string v11, "UpcomingFragment.setPopularLviList "

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v10, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 139
    iget-object v10, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcomingFeatured:Ljava/util/ArrayList;

    invoke-static {v10}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    .line 140
    .local v4, mUpcomingFeaturedCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_2

    .line 141
    new-instance v7, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v7}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 142
    .local v7, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UpcomingFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c00be

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 143
    iget-object v10, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_3

    .line 154
    .end local v7           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_2
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 155
    .local v0, date:Ljava/util/Date;
    iget-object v10, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcoming:Ljava/util/ArrayList;

    invoke-static {v10}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 156
    .local v3, mUpcomingCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_4

    .line 171
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 172
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v10, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    .end local v0           #date:Ljava/util/Date;
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    .end local v3           #mUpcomingCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .restart local v7       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lnet/flixster/android/model/Movie;

    .line 145
    .local v6, movie:Lnet/flixster/android/model/Movie;
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 146
    .local v2, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v6, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 147
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UpcomingFragment;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v11

    iput-object v11, v2, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 148
    iget-object v11, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 156
    .end local v2           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v6           #movie:Lnet/flixster/android/model/Movie;
    .end local v7           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    .restart local v0       #date:Ljava/util/Date;
    .restart local v3       #mUpcomingCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lnet/flixster/android/model/Movie;

    .line 157
    .restart local v6       #movie:Lnet/flixster/android/model/Movie;
    const-string v11, "theaterReleaseDate"

    invoke-virtual {v6, v11}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 158
    .local v9, theaterReleaseString:Ljava/lang/String;
    invoke-virtual {v6}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v8

    .line 159
    .local v8, theaterRelease:Ljava/util/Date;
    invoke-virtual {v0, v8}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 160
    new-instance v7, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v7}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 161
    .restart local v7       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    iput-object v9, v7, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 162
    iget-object v11, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    move-object v0, v8

    .line 165
    .end local v7           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_5
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 166
    .restart local v2       #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v6, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 168
    iget-object v11, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method


# virtual methods
.method public onPause()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onPause()V

    .line 49
    iget-object v0, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcoming:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 50
    iget-object v0, p0, Lcom/flixster/android/activity/hc/UpcomingFragment;->mUpcomingFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 51
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/flixster/android/activity/hc/LviFragment;->onResume()V

    .line 41
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UpcomingFragment;->trackPage()V

    .line 43
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/UpcomingFragment;->ScheduleLoadItemsTask(J)V

    .line 44
    return-void
.end method

.method protected retryAction()V
    .locals 2

    .prologue
    .line 117
    const-wide/16 v0, 0x3e8

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/activity/hc/UpcomingFragment;->ScheduleLoadItemsTask(J)V

    .line 118
    return-void
.end method

.method public trackPage()V
    .locals 3

    .prologue
    .line 181
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/upcoming"

    const-string v2, "Upcoming Movies"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    return-void
.end method
