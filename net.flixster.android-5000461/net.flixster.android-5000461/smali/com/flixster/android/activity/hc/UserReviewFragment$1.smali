.class Lcom/flixster/android/activity/hc/UserReviewFragment$1;
.super Landroid/os/Handler;
.source "UserReviewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/hc/UserReviewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/UserReviewFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$1;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    .line 167
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "message"

    .prologue
    .line 171
    iget-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$1;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/hc/Main;

    .line 172
    .local v0, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$1;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    invoke-virtual {v1}, Lcom/flixster/android/activity/hc/UserReviewFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UserReviewPage.updateHandler - mReviewsList:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$1;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$1;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$1;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$1;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v1}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 178
    iget-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$1;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #calls: Lcom/flixster/android/activity/hc/UserReviewFragment;->updatePage()V
    invoke-static {v1}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$2(Lcom/flixster/android/activity/hc/UserReviewFragment;)V

    goto :goto_0

    .line 180
    :cond_2
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/flixster/android/activity/hc/Main;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 181
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/flixster/android/activity/hc/Main;->showDialog(I)V

    goto :goto_0
.end method
