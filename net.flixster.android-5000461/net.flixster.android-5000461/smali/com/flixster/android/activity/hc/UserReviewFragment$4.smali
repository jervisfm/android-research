.class Lcom/flixster/android/activity/hc/UserReviewFragment$4;
.super Ljava/util/TimerTask;
.source "UserReviewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/activity/hc/UserReviewFragment;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;


# direct methods
.method constructor <init>(Lcom/flixster/android/activity/hc/UserReviewFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    .line 208
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 210
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    .line 211
    .local v1, main:Lcom/flixster/android/activity/hc/Main;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    invoke-virtual {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 217
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$4(Lcom/flixster/android/activity/hc/UserReviewFragment;Lnet/flixster/android/model/User;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 226
    :try_start_1
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviewType:I
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$5(Lcom/flixster/android/activity/hc/UserReviewFragment;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 240
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->reviews:Ljava/util/List;

    #setter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$6(Lcom/flixster/android/activity/hc/UserReviewFragment;Ljava/util/List;)V

    .line 241
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget v2, v2, Lnet/flixster/android/model/User;->reviewCount:I

    if-lez v2, :cond_5

    .line 242
    :cond_4
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v4, 0x32

    invoke-static {v3, v4}, Lnet/flixster/android/data/ProfileDao;->getUserReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$6(Lcom/flixster/android/activity/hc/UserReviewFragment;Ljava/util/List;)V
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_1

    .line 250
    :cond_5
    :goto_2
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->updateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$7(Lcom/flixster/android/activity/hc/UserReviewFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    const-string v3, "UserReviewPage.scheduleUpdatePageTask (failed to get user data)"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 220
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    const/4 v3, 0x0

    #setter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$4(Lcom/flixster/android/activity/hc/UserReviewFragment;Lnet/flixster/android/model/User;)V

    goto :goto_1

    .line 228
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_0
    :try_start_2
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    #setter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$6(Lcom/flixster/android/activity/hc/UserReviewFragment;Ljava/util/List;)V

    .line 229
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget v2, v2, Lnet/flixster/android/model/User;->wtsCount:I

    if-lez v2, :cond_5

    .line 230
    :cond_6
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v4, 0x32

    invoke-static {v3, v4}, Lnet/flixster/android/data/ProfileDao;->getWantToSeeReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$6(Lcom/flixster/android/activity/hc/UserReviewFragment;Ljava/util/List;)V
    :try_end_2
    .catch Lnet/flixster/android/data/DaoException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 246
    :catch_1
    move-exception v0

    .line 247
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    const-string v3, "UserReviewPage.scheduleUpdatePageTask (failed to get review data)"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 234
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_1
    :try_start_3
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    #setter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$6(Lcom/flixster/android/activity/hc/UserReviewFragment;Ljava/util/List;)V

    .line 235
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget v2, v2, Lnet/flixster/android/model/User;->ratingCount:I

    if-lez v2, :cond_5

    .line 236
    :cond_7
    iget-object v2, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;->this$0:Lcom/flixster/android/activity/hc/UserReviewFragment;

    #getter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v4, 0x32

    invoke-static {v3, v4}, Lnet/flixster/android/data/ProfileDao;->getUserRatedReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/hc/UserReviewFragment;->access$6(Lcom/flixster/android/activity/hc/UserReviewFragment;Ljava/util/List;)V
    :try_end_3
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2

    .line 226
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
