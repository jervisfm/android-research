.class public Lcom/flixster/android/activity/hc/UserReviewFragment;
.super Lcom/flixster/android/activity/hc/FlixsterFragment;
.source "UserReviewFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field private static final DIALOG_REVIEW:I = 0x2

.field public static final KEY_REVIEW_INDEX:Ljava/lang/String; = "REVIEW_INDEX"

.field public static final KEY_REVIEW_TYPE:Ljava/lang/String; = "REVIEW_TYPE"

.field public static final REVIEW_TYPE_MY_RATED:I = 0x1

.field public static final REVIEW_TYPE_MY_REVIEWS:I = 0x2

.field public static final REVIEW_TYPE_MY_WANT_TO_SEE:I


# instance fields
.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mNextButton:Landroid/widget/Button;

.field private mPreviousButton:Landroid/widget/Button;

.field private mReviewIndex:I

.field private mReviewIndexView:Landroid/widget/TextView;

.field private mReviewType:I

.field private mReviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field private mUser:Lnet/flixster/android/model/User;

.field private mViewFlipper:Landroid/widget/ViewFlipper;

.field private movieImageHandler:Landroid/os/Handler;

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;-><init>()V

    .line 167
    new-instance v0, Lcom/flixster/android/activity/hc/UserReviewFragment$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/UserReviewFragment$1;-><init>(Lcom/flixster/android/activity/hc/UserReviewFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->updateHandler:Landroid/os/Handler;

    .line 187
    new-instance v0, Lcom/flixster/android/activity/hc/UserReviewFragment$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/UserReviewFragment$2;-><init>(Lcom/flixster/android/activity/hc/UserReviewFragment;)V

    iput-object v0, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->movieImageHandler:Landroid/os/Handler;

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/hc/UserReviewFragment;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/hc/UserReviewFragment;)Lnet/flixster/android/model/User;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/hc/UserReviewFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 258
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->updatePage()V

    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/activity/hc/UserReviewFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/activity/hc/UserReviewFragment;Lnet/flixster/android/model/User;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/activity/hc/UserReviewFragment;)I
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviewType:I

    return v0
.end method

.method static synthetic access$6(Lcom/flixster/android/activity/hc/UserReviewFragment;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;

    return-void
.end method

.method static synthetic access$7(Lcom/flixster/android/activity/hc/UserReviewFragment;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getFlixFragId()I
    .locals 1

    .prologue
    .line 327
    const/16 v0, 0x6b

    return v0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 208
    new-instance v0, Lcom/flixster/android/activity/hc/UserReviewFragment$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/activity/hc/UserReviewFragment$4;-><init>(Lcom/flixster/android/activity/hc/UserReviewFragment;)V

    .line 253
    .local v0, updatePageTask:Ljava/util/TimerTask;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 254
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/activity/hc/Main;

    iget-object v1, v1, Lcom/flixster/android/activity/hc/Main;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 256
    :cond_0
    return-void
.end method

.method private updatePage()V
    .locals 27

    .prologue
    .line 259
    const-string v3, "FlxMain"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v22, "UserReviewPage.updatePage reviewIndex:"

    move-object/from16 v0, v22

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviewIndex:I

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;

    if-eqz v3, :cond_1

    .line 261
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getView()Landroid/view/View;

    move-result-object v21

    .line 262
    .local v21, view:Landroid/view/View;
    const v3, 0x7f0701ca

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageView;

    .line 263
    .local v20, userImageView:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lnet/flixster/android/model/User;->getThumbnailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 264
    .local v8, bitmap:Landroid/graphics/Bitmap;
    if-eqz v8, :cond_0

    .line 265
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 268
    :cond_0
    const v3, 0x7f0701cb

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 269
    .local v17, nameView:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mUser:Lnet/flixster/android/model/User;

    iget-object v3, v3, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 273
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->removeAllViews()V

    .line 275
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 315
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviewIndex:I

    invoke-virtual {v3, v7}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 316
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, ""

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v7}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " of "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 317
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 318
    .local v11, indexText:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviewIndexView:Landroid/widget/TextView;

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mPreviousButton:Landroid/widget/Button;

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 320
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mNextButton:Landroid/widget/Button;

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 324
    .end local v8           #bitmap:Landroid/graphics/Bitmap;
    .end local v11           #indexText:Ljava/lang/String;
    .end local v17           #nameView:Landroid/widget/TextView;
    .end local v20           #userImageView:Landroid/widget/ImageView;
    .end local v21           #view:Landroid/view/View;
    :cond_1
    return-void

    .line 275
    .restart local v8       #bitmap:Landroid/graphics/Bitmap;
    .restart local v17       #nameView:Landroid/widget/TextView;
    .restart local v20       #userImageView:Landroid/widget/ImageView;
    .restart local v21       #view:Landroid/view/View;
    :cond_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lnet/flixster/android/model/Review;

    .line 276
    .local v19, review:Lnet/flixster/android/model/Review;
    invoke-virtual/range {v19 .. v19}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v4

    .line 277
    .local v4, movie:Lnet/flixster/android/model/Movie;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 278
    const v7, 0x7f030060

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v3, v7, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 277
    check-cast v12, Landroid/widget/ScrollView;

    .line 279
    .local v12, item:Landroid/widget/ScrollView;
    const v3, 0x7f0701bd

    invoke-virtual {v12, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    .line 280
    .local v13, movieLayout:Landroid/widget/RelativeLayout;
    invoke-virtual {v13, v4}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 281
    const/4 v3, 0x1

    invoke-virtual {v13, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 282
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    const v3, 0x7f0701be

    invoke-virtual {v12, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 284
    .local v6, posterView:Landroid/widget/ImageView;
    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 285
    iget-object v3, v4, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 286
    iget-object v3, v4, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 297
    :goto_1
    const/4 v3, 0x3

    new-array v14, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v7, "title"

    aput-object v7, v14, v3

    const/4 v3, 0x1

    const-string v7, "MOVIE_ACTORS_SHORT"

    aput-object v7, v14, v3

    const/4 v3, 0x2

    const-string v7, "meta"

    aput-object v7, v14, v3

    .line 298
    .local v14, movieProperties:[Ljava/lang/String;
    const/4 v3, 0x3

    new-array v0, v3, [I

    move-object/from16 v16, v0

    fill-array-data v16, :array_0

    .line 300
    .local v16, movieTextViewIds:[I
    const/4 v10, 0x0

    .local v10, i:I
    :goto_2
    array-length v3, v14

    if-lt v10, v3, :cond_5

    .line 309
    const v3, 0x7f0701c4

    invoke-virtual {v12, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageView;

    .line 310
    .local v18, ratingView:Landroid/widget/ImageView;
    sget-object v3, Lnet/flixster/android/Flixster;->RATING_LARGE_R:[I

    move-object/from16 v0, v19

    iget-wide v0, v0, Lnet/flixster/android/model/Review;->stars:D

    move-wide/from16 v23, v0

    const-wide/high16 v25, 0x4000

    mul-double v23, v23, v25

    move-wide/from16 v0, v23

    double-to-int v7, v0

    aget v3, v3, v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 311
    const v3, 0x7f0701c5

    invoke-virtual {v12, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 312
    .local v9, commentView:Landroid/widget/TextView;
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3, v12}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 288
    .end local v9           #commentView:Landroid/widget/TextView;
    .end local v10           #i:I
    .end local v14           #movieProperties:[Ljava/lang/String;
    .end local v16           #movieTextViewIds:[I
    .end local v18           #ratingView:Landroid/widget/ImageView;
    :cond_3
    const-string v3, "thumbnail"

    invoke-virtual {v4, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 289
    .local v5, thumbnailUrl:Ljava/lang/String;
    if-eqz v5, :cond_4

    const-string v3, "http"

    invoke-virtual {v5, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 290
    new-instance v2, Lnet/flixster/android/model/ImageOrder;

    const/4 v3, 0x0

    .line 291
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/flixster/android/activity/hc/UserReviewFragment;->movieImageHandler:Landroid/os/Handler;

    .line 290
    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 292
    .local v2, imageOrder:Lnet/flixster/android/model/ImageOrder;
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/activity/hc/Main;

    invoke-virtual {v3, v2}, Lcom/flixster/android/activity/hc/Main;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_1

    .line 294
    .end local v2           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_4
    const v3, 0x7f02014f

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 301
    .end local v5           #thumbnailUrl:Ljava/lang/String;
    .restart local v10       #i:I
    .restart local v14       #movieProperties:[Ljava/lang/String;
    .restart local v16       #movieTextViewIds:[I
    :cond_5
    aget v3, v16, v10

    invoke-virtual {v12, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 302
    .local v15, movieTextView:Landroid/widget/TextView;
    aget-object v3, v14, v10

    invoke-virtual {v4, v3}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 303
    aget-object v3, v14, v10

    invoke-virtual {v4, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2

    .line 306
    :cond_6
    const/16 v3, 0x8

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 298
    :array_0
    .array-data 0x4
        0xbft 0x1t 0x7t 0x7ft
        0xc0t 0x1t 0x7t 0x7ft
        0xc1t 0x1t 0x7t 0x7ft
    .end array-data
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "view"

    .prologue
    .line 90
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 92
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    .line 93
    .local v1, movie:Lnet/flixster/android/model/Movie;
    if-eqz v1, :cond_0

    .line 94
    new-instance v0, Landroid/content/Intent;

    const-string v2, "DETAILS"

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 95
    .local v0, i:Landroid/content/Intent;
    const-string v2, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v1}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 96
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/flixster/android/activity/hc/Main;

    const-class v3, Lcom/flixster/android/activity/hc/MovieDetailsFragment;

    invoke-virtual {v2, v0, v3}, Lcom/flixster/android/activity/hc/Main;->startFragment(Landroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x7f0701bd
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "dialogId"

    .prologue
    const v5, 0x7f0c0135

    const/4 v4, 0x1

    .line 125
    packed-switch p1, :pswitch_data_0

    .line 148
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 149
    .local v1, defaultDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 150
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 151
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 152
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    move-object v2, v1

    .line 153
    .end local v1           #defaultDialog:Landroid/app/ProgressDialog;
    :goto_0
    return-object v2

    .line 127
    :pswitch_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 128
    .local v2, ratingsDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 129
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 130
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 131
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 134
    .end local v2           #ratingsDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 135
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v3, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 136
    const-string v3, "Network Error"

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 137
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 138
    const-string v3, "Retry"

    new-instance v4, Lcom/flixster/android/activity/hc/UserReviewFragment$3;

    invoke-direct {v4, p0}, Lcom/flixster/android/activity/hc/UserReviewFragment$3;-><init>(Lcom/flixster/android/activity/hc/UserReviewFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 145
    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 146
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 109
    iput-object p1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 110
    iget-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030061

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 112
    .local v0, view:Landroid/view/View;
    const v1, 0x7f0701c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviewIndexView:Landroid/widget/TextView;

    .line 113
    const v1, 0x7f0701cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ViewFlipper;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    .line 114
    const v1, 0x7f0701c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mPreviousButton:Landroid/widget/Button;

    .line 115
    iget-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mPreviousButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 116
    const v1, 0x7f0701c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mNextButton:Landroid/widget/Button;

    .line 117
    iget-object v1, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 118
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->updatePage()V

    .line 119
    return-object v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 160
    const-string v0, "FlxMain"

    const-string v1, "UserReviewPage.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-super {p0}, Lcom/flixster/android/activity/hc/FlixsterFragment;->onResume()V

    .line 164
    invoke-direct {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->scheduleUpdatePageTask()V

    .line 165
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .parameter "view"
    .parameter "event"

    .prologue
    const/4 v2, 0x1

    .line 66
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    .line 67
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 86
    :cond_0
    :pswitch_0
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 69
    :pswitch_1
    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f040006

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 70
    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f040007

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 71
    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->showNext()V

    .line 72
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v4}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 73
    const-string v4, " of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, nextReviewText:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviewIndexView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 77
    .end local v0           #nextReviewText:Ljava/lang/String;
    :pswitch_2
    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f040008

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 78
    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/flixster/android/activity/hc/UserReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f040009

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 79
    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->showPrevious()V

    .line 80
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v4}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 81
    const-string v4, " of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviews:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, previousReviewText:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/activity/hc/UserReviewFragment;->mReviewIndexView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x7f0701c7
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public trackPage()V
    .locals 0

    .prologue
    .line 333
    return-void
.end method
