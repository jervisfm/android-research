.class public Lcom/flixster/android/activity/phone/Main;
.super Lcom/actionbarsherlock/app/SherlockActivity;
.source "Main.java"

# interfaces
.implements Lcom/actionbarsherlock/app/ActionBar$TabListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private isActionBarReady:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockActivity;-><init>()V

    return-void
.end method

.method private static getLastTab()I
    .locals 4

    .prologue
    .line 117
    const/4 v0, 0x1

    .line 118
    .local v0, defaultTabPosition:I
    const/4 v2, 0x1

    .line 119
    .local v2, tabPosition:I
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getLastTab()Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, tab:Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 122
    const/4 v3, 0x5

    if-lt v2, v3, :cond_0

    .line 123
    const/4 v2, 0x1

    .line 127
    :cond_0
    :goto_0
    return v2

    .line 125
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private resetBackStack()V
    .locals 3

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/flixster/android/activity/phone/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "0"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 114
    return-void
.end method

.method private static setLastTab(I)V
    .locals 1
    .parameter "pos"

    .prologue
    .line 131
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setLastTab(Ljava/lang/String;)V

    .line 132
    return-void
.end method


# virtual methods
.method protected createActionBar()V
    .locals 8

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/flixster/android/activity/phone/Main;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 47
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/actionbarsherlock/app/ActionBar;->setNavigationMode(I)V

    .line 48
    const v6, 0x7f020001

    invoke-virtual {v0, v6}, Lcom/actionbarsherlock/app/ActionBar;->setLogo(I)V

    .line 49
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 51
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->newTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0201bd

    invoke-virtual {v6, v7}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setIcon(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v3

    .line 52
    .local v3, homeTab:Lcom/actionbarsherlock/app/ActionBar$Tab;
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->newTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0201bb

    invoke-virtual {v6, v7}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setIcon(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    .line 53
    .local v1, boxOfficeTab:Lcom/actionbarsherlock/app/ActionBar$Tab;
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->newTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0201be

    invoke-virtual {v6, v7}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setIcon(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v4

    .line 54
    .local v4, myMoviesTab:Lcom/actionbarsherlock/app/ActionBar$Tab;
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->newTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0201bf

    invoke-virtual {v6, v7}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setIcon(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v5

    .line 55
    .local v5, theatersTab:Lcom/actionbarsherlock/app/ActionBar$Tab;
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->newTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v6

    const v7, 0x7f0201bc

    invoke-virtual {v6, v7}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setIcon(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v2

    .line 57
    .local v2, dvdTab:Lcom/actionbarsherlock/app/ActionBar$Tab;
    invoke-virtual {v3, p0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTabListener(Lcom/actionbarsherlock/app/ActionBar$TabListener;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    .line 58
    invoke-virtual {v1, p0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTabListener(Lcom/actionbarsherlock/app/ActionBar$TabListener;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    .line 59
    invoke-virtual {v4, p0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTabListener(Lcom/actionbarsherlock/app/ActionBar$TabListener;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    .line 60
    invoke-virtual {v5, p0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTabListener(Lcom/actionbarsherlock/app/ActionBar$TabListener;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    .line 61
    invoke-virtual {v2, p0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTabListener(Lcom/actionbarsherlock/app/ActionBar$TabListener;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    .line 63
    invoke-virtual {v0, v3}, Lcom/actionbarsherlock/app/ActionBar;->addTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 64
    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->addTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 65
    invoke-virtual {v0, v4}, Lcom/actionbarsherlock/app/ActionBar;->addTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 66
    invoke-virtual {v0, v5}, Lcom/actionbarsherlock/app/ActionBar;->addTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 67
    invoke-virtual {v0, v2}, Lcom/actionbarsherlock/app/ActionBar;->addTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 69
    invoke-static {}, Lcom/flixster/android/activity/phone/Main;->getLastTab()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/actionbarsherlock/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 70
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/flixster/android/activity/phone/Main;->isActionBarReady:Z

    .line 71
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v0, 0x7f03004c

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/phone/Main;->setContentView(I)V

    .line 32
    invoke-virtual {p0}, Lcom/flixster/android/activity/phone/Main;->createActionBar()V

    .line 33
    return-void
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/flixster/android/activity/phone/Main;->getSupportMenuInflater()Lcom/actionbarsherlock/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f000a

    invoke-virtual {v0, v1, p1}, Lcom/actionbarsherlock/view/MenuInflater;->inflate(ILcom/actionbarsherlock/view/Menu;)V

    .line 139
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 37
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockActivity;->onDestroy()V

    .line 38
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 3
    .parameter "item"

    .prologue
    const/4 v1, 0x1

    .line 145
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 161
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v1

    :goto_0
    :sswitch_0
    return v1

    .line 151
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/SearchPage;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/phone/Main;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 156
    .end local v0           #intent:Landroid/content/Intent;
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/SettingsPage;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 157
    .restart local v0       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/phone/Main;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 145
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0702fa -> :sswitch_1
        0x7f0702fb -> :sswitch_2
        0x7f070303 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockActivity;->onResume()V

    .line 43
    return-void
.end method

.method public onTabReselected(Lcom/actionbarsherlock/app/ActionBar$Tab;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 0
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 110
    return-void
.end method

.method public onTabSelected(Lcom/actionbarsherlock/app/ActionBar$Tab;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 6
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 78
    iget-boolean v4, p0, Lcom/flixster/android/activity/phone/Main;->isActionBarReady:Z

    if-eqz v4, :cond_0

    .line 79
    invoke-virtual {p1}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getPosition()I

    move-result v4

    invoke-static {v4}, Lcom/flixster/android/activity/phone/Main;->setLastTab(I)V

    .line 82
    :cond_0
    const/4 v2, 0x0

    .line 83
    .local v2, fragmentClass:Ljava/lang/Class;,"Ljava/lang/Class<+Landroid/support/v4/app/Fragment;>;"
    invoke-virtual {p1}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 84
    .local v3, tag:Ljava/lang/Object;
    if-eqz v3, :cond_1

    move-object v2, v3

    .line 85
    check-cast v2, Ljava/lang/Class;

    .line 87
    :cond_1
    if-nez v2, :cond_2

    .line 100
    :goto_0
    return-void

    .line 93
    :cond_2
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 99
    .local v1, fragment:Landroid/support/v4/app/Fragment;
    const v4, 0x7f070103

    const/4 v5, 0x0

    invoke-virtual {p2, v4, v1, v5}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_0

    .line 94
    .end local v1           #fragment:Landroid/support/v4/app/Fragment;
    :catch_0
    move-exception v0

    .line 95
    .local v0, e:Ljava/lang/InstantiationException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 96
    .end local v0           #e:Ljava/lang/InstantiationException;
    :catch_1
    move-exception v0

    .line 97
    .local v0, e:Ljava/lang/IllegalAccessException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public onTabUnselected(Lcom/actionbarsherlock/app/ActionBar$Tab;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 0
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/flixster/android/activity/phone/Main;->resetBackStack()V

    .line 105
    return-void
.end method
