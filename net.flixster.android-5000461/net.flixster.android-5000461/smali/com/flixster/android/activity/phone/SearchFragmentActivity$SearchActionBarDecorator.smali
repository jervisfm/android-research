.class Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;
.super Lcom/flixster/android/activity/decorator/ActionBarDecorator;
.source "SearchFragmentActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/activity/phone/SearchFragmentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchActionBarDecorator"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/activity/phone/SearchFragmentActivity;


# direct methods
.method private constructor <init>(Lcom/flixster/android/activity/phone/SearchFragmentActivity;Landroid/app/Activity;)V
    .locals 0
    .parameter
    .parameter "activity"

    .prologue
    .line 69
    iput-object p1, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;->this$0:Lcom/flixster/android/activity/phone/SearchFragmentActivity;

    .line 70
    invoke-direct {p0, p2}, Lcom/flixster/android/activity/decorator/ActionBarDecorator;-><init>(Landroid/app/Activity;)V

    .line 71
    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/activity/phone/SearchFragmentActivity;Landroid/app/Activity;Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;-><init>(Lcom/flixster/android/activity/phone/SearchFragmentActivity;Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 76
    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    .line 78
    invoke-virtual {p0, p1}, Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;->hideSoftKeyboard(Landroid/view/View;)V

    .line 79
    invoke-virtual {p1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/actionbarsherlock/view/MenuItem;

    .line 80
    .local v0, searchActionItem:Lcom/actionbarsherlock/view/MenuItem;
    invoke-interface {v0}, Lcom/actionbarsherlock/view/MenuItem;->collapseActionView()Z

    .line 81
    iget-object v1, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;->this$0:Lcom/flixster/android/activity/phone/SearchFragmentActivity;

    iget-object v2, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;->this$0:Lcom/flixster/android/activity/phone/SearchFragmentActivity;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    #setter for: Lcom/flixster/android/activity/phone/SearchFragmentActivity;->query:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->access$0(Lcom/flixster/android/activity/phone/SearchFragmentActivity;Ljava/lang/String;)V

    #calls: Lcom/flixster/android/activity/phone/SearchFragmentActivity;->performNewSearch(Ljava/lang/String;)V
    invoke-static {v1, v3}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->access$1(Lcom/flixster/android/activity/phone/SearchFragmentActivity;Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;->this$0:Lcom/flixster/android/activity/phone/SearchFragmentActivity;

    #calls: Lcom/flixster/android/activity/phone/SearchFragmentActivity;->updateTitle()V
    invoke-static {v1}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->access$2(Lcom/flixster/android/activity/phone/SearchFragmentActivity;)V

    .line 83
    const/4 v1, 0x1

    .line 85
    .end local v0           #searchActionItem:Lcom/actionbarsherlock/view/MenuItem;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
