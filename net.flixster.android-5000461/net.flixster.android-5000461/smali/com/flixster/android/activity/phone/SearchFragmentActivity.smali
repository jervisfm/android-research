.class public Lcom/flixster/android/activity/phone/SearchFragmentActivity;
.super Lcom/flixster/android/activity/common/DecoratedSherlockFragmentActivity;
.source "SearchFragmentActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;
    }
.end annotation


# instance fields
.field private query:Ljava/lang/String;

.field private searchEditText:Landroid/widget/TextView;

.field private searchFragment:Lcom/flixster/android/activity/common/SearchFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/activity/phone/SearchFragmentActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->query:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/activity/phone/SearchFragmentActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->performNewSearch(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/activity/phone/SearchFragmentActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->updateTitle()V

    return-void
.end method

.method private performNewSearch(Ljava/lang/String;)V
    .locals 1
    .parameter "query"

    .prologue
    .line 63
    iget-object v0, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->searchEditText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->searchFragment:Lcom/flixster/android/activity/common/SearchFragment;

    invoke-virtual {v0, p1}, Lcom/flixster/android/activity/common/SearchFragment;->setQuery(Ljava/lang/String;)Lcom/flixster/android/activity/common/SearchFragment;

    .line 65
    iget-object v0, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->searchFragment:Lcom/flixster/android/activity/common/SearchFragment;

    invoke-virtual {v0}, Lcom/flixster/android/activity/common/SearchFragment;->invalidateData()V

    .line 66
    return-void
.end method

.method private updateTitle()V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f0c0048

    invoke-virtual {p0, v1}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 60
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    iget-object v0, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->actionbarDecorator:Lcom/flixster/android/activity/decorator/ActionBarDecorator;

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->removeDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 30
    new-instance v0, Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;-><init>(Lcom/flixster/android/activity/phone/SearchFragmentActivity;Landroid/app/Activity;Lcom/flixster/android/activity/phone/SearchFragmentActivity$SearchActionBarDecorator;)V

    iput-object v0, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->actionbarDecorator:Lcom/flixster/android/activity/decorator/ActionBarDecorator;

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 31
    iget-object v0, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->actionbarDecorator:Lcom/flixster/android/activity/decorator/ActionBarDecorator;

    invoke-virtual {v0}, Lcom/flixster/android/activity/decorator/ActionBarDecorator;->onCreate()V

    .line 33
    const v0, 0x7f030077

    invoke-virtual {p0, v0}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->setContentView(I)V

    .line 34
    invoke-virtual {p0}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->createActionBar()V

    .line 35
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "net.flixster.android.EXTRA_QUERY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->query:Ljava/lang/String;

    .line 37
    invoke-virtual {p0}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f070234

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/common/SearchFragment;

    iput-object v0, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->searchFragment:Lcom/flixster/android/activity/common/SearchFragment;

    .line 38
    iget-object v0, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->searchFragment:Lcom/flixster/android/activity/common/SearchFragment;

    iget-object v1, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/flixster/android/activity/common/SearchFragment;->setQuery(Ljava/lang/String;)Lcom/flixster/android/activity/common/SearchFragment;

    .line 39
    invoke-direct {p0}, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->updateTitle()V

    .line 40
    return-void

    .line 36
    :cond_0
    const-string v0, "net.flixster.android.EXTRA_QUERY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 3
    .parameter "menu"

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockFragmentActivity;->onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z

    .line 45
    const v1, 0x7f070303

    invoke-interface {p1, v1}, Lcom/actionbarsherlock/view/Menu;->removeItem(I)V

    .line 46
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Lcom/actionbarsherlock/view/Menu;->getItem(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    .line 47
    .local v0, searchActionItem:Lcom/actionbarsherlock/view/MenuItem;
    invoke-interface {v0}, Lcom/actionbarsherlock/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f070027

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->searchEditText:Landroid/widget/TextView;

    .line 48
    iget-object v1, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->searchEditText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->query:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    const/4 v1, 0x1

    return v1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 55
    const-string v0, "net.flixster.android.EXTRA_QUERY"

    iget-object v1, p0, Lcom/flixster/android/activity/phone/SearchFragmentActivity;->query:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method
