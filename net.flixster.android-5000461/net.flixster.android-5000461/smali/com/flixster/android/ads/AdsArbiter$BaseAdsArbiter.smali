.class public Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;
.super Ljava/lang/Object;
.source "AdsArbiter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/ads/AdsArbiter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "BaseAdsArbiter"
.end annotation


# instance fields
.field private isEnabled:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    #calls: Lcom/flixster/android/ads/AdsArbiter;->isAdsEnabled()Z
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->access$3()Z

    move-result v0

    iput-boolean v0, p0, Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;->isEnabled:Z

    .line 178
    return-void
.end method


# virtual methods
.method protected isEnabled()Z
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;->isEnabled:Z

    return v0
.end method

.method public onAppStart()V
    .locals 1

    .prologue
    .line 185
    #calls: Lcom/flixster/android/ads/AdsArbiter;->isAdsEnabled()Z
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->access$3()Z

    move-result v0

    iput-boolean v0, p0, Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;->isEnabled:Z

    .line 186
    return-void
.end method
