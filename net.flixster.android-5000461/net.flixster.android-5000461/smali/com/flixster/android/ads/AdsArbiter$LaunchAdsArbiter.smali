.class public Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;
.super Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;
.source "AdsArbiter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/ads/AdsArbiter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LaunchAdsArbiter"
.end annotation


# instance fields
.field private final launchRule:Lcom/flixster/android/ads/rule/AdRule;

.field private final timeCapRule:Lcom/flixster/android/ads/rule/AdRule;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;-><init>()V

    .line 139
    new-instance v0, Lcom/flixster/android/ads/rule/TimeCapRule;

    const-wide/32 v1, 0x1b7740

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/ads/rule/TimeCapRule;-><init>(J)V

    iput-object v0, p0, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->timeCapRule:Lcom/flixster/android/ads/rule/AdRule;

    .line 140
    new-instance v0, Lcom/flixster/android/ads/rule/LaunchRule;

    invoke-direct {v0}, Lcom/flixster/android/ads/rule/LaunchRule;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->launchRule:Lcom/flixster/android/ads/rule/AdRule;

    .line 141
    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;)V
    .locals 0
    .parameter

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAppStart()V
    .locals 1

    .prologue
    .line 167
    invoke-super {p0}, Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;->onAppStart()V

    .line 168
    iget-object v0, p0, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->launchRule:Lcom/flixster/android/ads/rule/AdRule;

    invoke-interface {v0}, Lcom/flixster/android/ads/rule/AdRule;->reset()V

    .line 169
    return-void
.end method

.method public reshowLater()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->timeCapRule:Lcom/flixster/android/ads/rule/AdRule;

    invoke-interface {v0}, Lcom/flixster/android/ads/rule/AdRule;->reset()V

    .line 162
    iget-object v0, p0, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->launchRule:Lcom/flixster/android/ads/rule/AdRule;

    invoke-interface {v0}, Lcom/flixster/android/ads/rule/AdRule;->reset()V

    .line 163
    return-void
.end method

.method public shouldShow()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 144
    invoke-virtual {p0}, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 145
    #calls: Lcom/flixster/android/ads/AdsArbiter;->isClientLaunchCapped()Z
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->access$2()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 146
    iget-object v2, p0, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->timeCapRule:Lcom/flixster/android/ads/rule/AdRule;

    invoke-interface {v2}, Lcom/flixster/android/ads/rule/AdRule;->shouldShowAd()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->isTopLevelActivityOkayToShowInterstitial()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 151
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 146
    goto :goto_0

    .line 148
    :cond_2
    iget-object v2, p0, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->launchRule:Lcom/flixster/android/ads/rule/AdRule;

    invoke-interface {v2}, Lcom/flixster/android/ads/rule/AdRule;->shouldShowAd()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->isTopLevelActivityOkayToShowInterstitial()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 151
    goto :goto_0
.end method

.method public shown()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->timeCapRule:Lcom/flixster/android/ads/rule/AdRule;

    invoke-interface {v0}, Lcom/flixster/android/ads/rule/AdRule;->adShown()V

    .line 157
    iget-object v0, p0, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->launchRule:Lcom/flixster/android/ads/rule/AdRule;

    invoke-interface {v0}, Lcom/flixster/android/ads/rule/AdRule;->adShown()V

    .line 158
    return-void
.end method
