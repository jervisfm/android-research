.class public Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;
.super Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;
.source "AdsArbiter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/ads/AdsArbiter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TrailerAdsArbiter"
.end annotation


# static fields
.field private static final PREROLL_INTERVAL:I = 0x3


# instance fields
.field private originatedFromAds:Z

.field private final postitialTimeCapRule:Lcom/flixster/android/ads/rule/AdRule;

.field private final prerollIntervalCapRule:Lcom/flixster/android/ads/rule/AdRule;

.field private prerollShown:Z


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;-><init>()V

    .line 68
    new-instance v0, Lcom/flixster/android/ads/rule/IntervalCapRule;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/flixster/android/ads/rule/IntervalCapRule;-><init>(I)V

    iput-object v0, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->prerollIntervalCapRule:Lcom/flixster/android/ads/rule/AdRule;

    .line 69
    new-instance v0, Lcom/flixster/android/ads/rule/TimeCapRule;

    const-wide/32 v1, 0x1b7740

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/ads/rule/TimeCapRule;-><init>(J)V

    iput-object v0, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->postitialTimeCapRule:Lcom/flixster/android/ads/rule/AdRule;

    .line 70
    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;)V
    .locals 0
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;-><init>()V

    return-void
.end method

.method private onNewTrailerPlayback()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 121
    iput-boolean v0, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromAds:Z

    .line 122
    iput-boolean v0, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->prerollShown:Z

    .line 123
    return-void
.end method


# virtual methods
.method public bridge synthetic onAppStart()V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;->onAppStart()V

    return-void
.end method

.method public originatedFromAds()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->onNewTrailerPlayback()V

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromAds:Z

    .line 76
    return-void
.end method

.method public originatedFromNonAds()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->onNewTrailerPlayback()V

    .line 81
    return-void
.end method

.method public postitialShown()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->postitialTimeCapRule:Lcom/flixster/android/ads/rule/AdRule;

    invoke-interface {v0}, Lcom/flixster/android/ads/rule/AdRule;->adShown()V

    .line 117
    invoke-direct {p0}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->onNewTrailerPlayback()V

    .line 118
    return-void
.end method

.method public prerollShown()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->prerollShown:Z

    .line 101
    return-void
.end method

.method public shouldShowPostitial()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 104
    invoke-virtual {p0}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 105
    #calls: Lcom/flixster/android/ads/AdsArbiter;->isClientPostitialCapped()Z
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->access$1()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 106
    iget-boolean v2, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromAds:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->prerollShown:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->postitialTimeCapRule:Lcom/flixster/android/ads/rule/AdRule;

    invoke-interface {v2}, Lcom/flixster/android/ads/rule/AdRule;->shouldShowAd()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 106
    goto :goto_0

    .line 108
    :cond_2
    iget-boolean v2, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromAds:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->prerollShown:Z

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 111
    goto :goto_0
.end method

.method public shouldShowPreroll()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 88
    invoke-virtual {p0}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    #calls: Lcom/flixster/android/ads/AdsArbiter;->isClientPrerollCapped()Z
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->access$0()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    iget-object v2, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->prerollIntervalCapRule:Lcom/flixster/android/ads/rule/AdRule;

    invoke-interface {v2}, Lcom/flixster/android/ads/rule/AdRule;->shouldShowAd()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromAds:Z

    if-nez v2, :cond_0

    move v0, v1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 92
    :cond_1
    iget-boolean v2, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromAds:Z

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public shouldShowSponsor()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromAds:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
