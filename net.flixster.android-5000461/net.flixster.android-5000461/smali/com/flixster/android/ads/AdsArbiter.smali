.class public Lcom/flixster/android/ads/AdsArbiter;
.super Ljava/lang/Object;
.source "AdsArbiter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/ads/AdsArbiter$BaseAdsArbiter;,
        Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;,
        Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;
    }
.end annotation


# static fields
.field private static final LAUNCH:Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;

.field private static final TRAILER:Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    new-instance v0, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    invoke-direct {v0, v1}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;-><init>(Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;)V

    sput-object v0, Lcom/flixster/android/ads/AdsArbiter;->TRAILER:Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    .line 23
    new-instance v0, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;

    invoke-direct {v0, v1}, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;-><init>(Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;)V

    sput-object v0, Lcom/flixster/android/ads/AdsArbiter;->LAUNCH:Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;

    .line 21
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method static synthetic access$0()Z
    .locals 1

    .prologue
    .line 226
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->isClientPrerollCapped()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1()Z
    .locals 1

    .prologue
    .line 235
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->isClientPostitialCapped()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2()Z
    .locals 1

    .prologue
    .line 217
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->isClientLaunchCapped()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3()Z
    .locals 1

    .prologue
    .line 192
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->isAdsEnabled()Z

    move-result v0

    return v0
.end method

.method private static isAdsEnabled()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 195
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 202
    :cond_0
    :goto_0
    return v0

    .line 197
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v1

    if-nez v1, :cond_0

    .line 199
    sget-wide v1, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    const-wide/32 v3, 0xa4cb800

    add-long/2addr v1, v3

    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    .line 202
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isClientLaunchCapped()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 218
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isAdAdminLaunchCapDisabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 222
    .local v0, p:Lnet/flixster/android/model/Property;
    :cond_0
    :goto_0
    return v1

    .line 221
    .end local v0           #p:Lnet/flixster/android/model/Property;
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 222
    .restart local v0       #p:Lnet/flixster/android/model/Property;
    if-eqz v0, :cond_2

    iget-boolean v2, v0, Lnet/flixster/android/model/Property;->isClientCapped:Z

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static isClientPostitialCapped()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 236
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isAdAdminPostitialCapDisabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 240
    .local v0, p:Lnet/flixster/android/model/Property;
    :cond_0
    :goto_0
    return v1

    .line 239
    .end local v0           #p:Lnet/flixster/android/model/Property;
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 240
    .restart local v0       #p:Lnet/flixster/android/model/Property;
    if-eqz v0, :cond_2

    iget-boolean v2, v0, Lnet/flixster/android/model/Property;->isClientCapped:Z

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static isClientPrerollCapped()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 227
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isAdAdminPrerollCapDisabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 231
    .local v0, p:Lnet/flixster/android/model/Property;
    :cond_0
    :goto_0
    return v1

    .line 230
    .end local v0           #p:Lnet/flixster/android/model/Property;
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 231
    .restart local v0       #p:Lnet/flixster/android/model/Property;
    if-eqz v0, :cond_2

    iget-boolean v2, v0, Lnet/flixster/android/model/Property;->isClientCapped:Z

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isTopLevelActivityOkayToShowInterstitial()Z
    .locals 4

    .prologue
    .line 208
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/ActivityHolder;->getTopLevelActivity()Landroid/app/Activity;

    move-result-object v0

    .line 209
    .local v0, a:Landroid/app/Activity;
    instance-of v2, v0, Lcom/flixster/android/activity/TabbedActivity;

    if-nez v2, :cond_1

    instance-of v2, v0, Lnet/flixster/android/MovieDetails;

    if-nez v2, :cond_1

    instance-of v2, v0, Lnet/flixster/android/TheaterInfoPage;

    if-nez v2, :cond_1

    .line 210
    instance-of v2, v0, Lnet/flixster/android/ShowtimesPage;

    if-nez v2, :cond_1

    instance-of v2, v0, Lnet/flixster/android/ActorPage;

    if-nez v2, :cond_1

    const/4 v1, 0x0

    .line 211
    .local v1, b:Z
    :goto_0
    if-nez v1, :cond_0

    .line 212
    const-string v2, "FlxAd"

    const-string v3, "AdsArbiter.isTopLevelActivityOkayToShowInterstitial false"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->sw(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_0
    return v1

    .line 210
    .end local v1           #b:Z
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static launch()Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/flixster/android/ads/AdsArbiter;->LAUNCH:Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;

    return-object v0
.end method

.method public static trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/flixster/android/ads/AdsArbiter;->TRAILER:Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    return-object v0
.end method
