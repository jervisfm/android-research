.class public Lcom/flixster/android/ads/DfpVideoAdRunner;
.super Ljava/lang/Object;
.source "DfpVideoAdRunner.java"

# interfaces
.implements Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedListener;
.implements Lcom/google/ads/interactivemedia/api/AdErrorListener;


# static fields
.field private static final PROD_URL:Ljava/lang/String; = "http://pubads.g.doubleclick.net/gampad/ads?sz=480x320&iu=%2F6327%2Fmob.flix.android%2Ftrailer_preroll&ciu_szs=&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&m_ast=vast"


# instance fields
.field private final activity:Landroid/app/Activity;

.field private adsLoader:Lcom/google/ads/interactivemedia/api/AdsLoader;

.field private adsManager:Lcom/google/ads/interactivemedia/api/VideoAdsManager;

.field private final callback:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;

.field private final movie:Lnet/flixster/android/model/Movie;

.field private final player:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;Lnet/flixster/android/model/Movie;)V
    .locals 0
    .parameter "activity"
    .parameter "player"
    .parameter "callback"
    .parameter "movie"

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->activity:Landroid/app/Activity;

    .line 42
    iput-object p2, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->player:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

    .line 43
    iput-object p3, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->callback:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;

    .line 44
    iput-object p4, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->movie:Lnet/flixster/android/model/Movie;

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/ads/DfpVideoAdRunner;)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->movie:Lnet/flixster/android/model/Movie;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/ads/DfpVideoAdRunner;)Lcom/google/ads/interactivemedia/api/AdsLoader;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->adsLoader:Lcom/google/ads/interactivemedia/api/AdsLoader;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/ads/DfpVideoAdRunner;)Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->callback:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;

    return-object v0
.end method


# virtual methods
.method public onAdError(Lcom/google/ads/interactivemedia/api/AdErrorEvent;)V
    .locals 7
    .parameter "event"

    .prologue
    .line 87
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/api/AdErrorEvent;->getError()Lcom/google/ads/interactivemedia/api/AdError;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/api/AdError;->getMessage()Ljava/lang/String;

    move-result-object v5

    .line 88
    .local v5, error:Ljava/lang/String;
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DfpVideoAdRunner.onAdError "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/preroll/dfp"

    const-string v2, "Preroll - DFP"

    const-string v3, "Preroll"

    const-string v4, "AdError"

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 90
    iget-object v0, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->activity:Landroid/app/Activity;

    new-instance v1, Lcom/flixster/android/ads/DfpVideoAdRunner$2;

    invoke-direct {v1, p0}, Lcom/flixster/android/ads/DfpVideoAdRunner$2;-><init>(Lcom/flixster/android/ads/DfpVideoAdRunner;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 97
    return-void
.end method

.method public onAdsLoaded(Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;)V
    .locals 6
    .parameter "event"

    .prologue
    .line 67
    const-string v1, "FlxAd"

    const-string v2, "DfpVideoAdRunner.onAdsLoaded playing"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/preroll/dfp"

    const-string v3, "Preroll - DFP"

    const-string v4, "Preroll"

    const-string v5, "AdLoadSuccess"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;->getManager()Lcom/google/ads/interactivemedia/api/AdsManager;

    move-result-object v1

    check-cast v1, Lcom/google/ads/interactivemedia/api/VideoAdsManager;

    iput-object v1, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->adsManager:Lcom/google/ads/interactivemedia/api/VideoAdsManager;

    .line 70
    iget-object v1, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->adsManager:Lcom/google/ads/interactivemedia/api/VideoAdsManager;

    iget-object v2, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->callback:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;

    invoke-virtual {v1, v2}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->addAdEventListener(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;)V

    .line 71
    iget-object v1, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->adsManager:Lcom/google/ads/interactivemedia/api/VideoAdsManager;

    invoke-virtual {v1, p0}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->addAdErrorListener(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V

    .line 73
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->adsManager:Lcom/google/ads/interactivemedia/api/VideoAdsManager;

    iget-object v2, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->player:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

    invoke-virtual {v1, v2}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->play(Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, ise:Ljava/lang/IllegalStateException;
    const-string v1, "FlxAd"

    const-string v2, "DfpVideoAdRunner.onAdsLoaded"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public showAd()V
    .locals 5

    .prologue
    .line 48
    const-string v0, "FlxAd"

    const-string v1, "DfpVideoAdRunner.showAd requesting"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/preroll/dfp"

    const-string v2, "Preroll - DFP"

    const-string v3, "Preroll"

    const-string v4, "AdRequest"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdsLoader;

    iget-object v1, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/ads/interactivemedia/api/AdsLoader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->adsLoader:Lcom/google/ads/interactivemedia/api/AdsLoader;

    .line 51
    iget-object v0, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->adsLoader:Lcom/google/ads/interactivemedia/api/AdsLoader;

    invoke-virtual {v0, p0}, Lcom/google/ads/interactivemedia/api/AdsLoader;->addAdsLoadedListener(Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedListener;)V

    .line 52
    iget-object v0, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->adsLoader:Lcom/google/ads/interactivemedia/api/AdsLoader;

    invoke-virtual {v0, p0}, Lcom/google/ads/interactivemedia/api/AdsLoader;->addAdErrorListener(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V

    .line 53
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/ads/DfpVideoAdRunner$1;

    invoke-direct {v1, p0}, Lcom/flixster/android/ads/DfpVideoAdRunner$1;-><init>(Lcom/flixster/android/ads/DfpVideoAdRunner;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 62
    return-void
.end method

.method public unloadAd()V
    .locals 2

    .prologue
    .line 80
    const-string v0, "FlxAd"

    const-string v1, "DfpVideoAdRunner.unloadAd"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/flixster/android/ads/DfpVideoAdRunner;->adsManager:Lcom/google/ads/interactivemedia/api/VideoAdsManager;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->unload()V

    .line 82
    return-void
.end method
