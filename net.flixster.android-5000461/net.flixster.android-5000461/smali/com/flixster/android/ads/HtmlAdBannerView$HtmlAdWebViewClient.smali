.class final Lcom/flixster/android/ads/HtmlAdBannerView$HtmlAdWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "HtmlAdBannerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/ads/HtmlAdBannerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HtmlAdWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/ads/HtmlAdBannerView;


# direct methods
.method private constructor <init>(Lcom/flixster/android/ads/HtmlAdBannerView;)V
    .locals 0
    .parameter

    .prologue
    .line 41
    iput-object p1, p0, Lcom/flixster/android/ads/HtmlAdBannerView$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdBannerView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/ads/HtmlAdBannerView;Lcom/flixster/android/ads/HtmlAdBannerView$HtmlAdWebViewClient;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/flixster/android/ads/HtmlAdBannerView$HtmlAdWebViewClient;-><init>(Lcom/flixster/android/ads/HtmlAdBannerView;)V

    return-void
.end method


# virtual methods
.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3
    .parameter "view"
    .parameter "url"

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    .line 45
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HtmlAdBannerView.shouldOverrideUrlLoading true "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdBannerView$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdBannerView;

    #calls: Lcom/flixster/android/ads/HtmlAdBannerView;->launchHtmlAd(Ljava/lang/String;)V
    invoke-static {v0, p2}, Lcom/flixster/android/ads/HtmlAdBannerView;->access$0(Lcom/flixster/android/ads/HtmlAdBannerView;Ljava/lang/String;)V

    .line 47
    const/4 v0, 0x1

    return v0
.end method
