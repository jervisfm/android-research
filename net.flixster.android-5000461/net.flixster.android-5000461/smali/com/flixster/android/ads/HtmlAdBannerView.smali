.class public Lcom/flixster/android/ads/HtmlAdBannerView;
.super Landroid/webkit/WebView;
.source "HtmlAdBannerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/ads/HtmlAdBannerView$HtmlAdWebViewClient;
    }
.end annotation


# static fields
.field private static final FRAME_LAYOUT_FILL:Landroid/widget/FrameLayout$LayoutParams;


# instance fields
.field private final htmlAd:Lnet/flixster/android/ads/model/FlixsterAd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 20
    const/4 v1, -0x1

    const/4 v2, -0x2

    .line 19
    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/flixster/android/ads/HtmlAdBannerView;->FRAME_LAYOUT_FILL:Landroid/widget/FrameLayout$LayoutParams;

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lnet/flixster/android/ads/model/FlixsterAd;)V
    .locals 3
    .parameter "context"
    .parameter "htmlAd"

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 26
    iput-object p2, p0, Lcom/flixster/android/ads/HtmlAdBannerView;->htmlAd:Lnet/flixster/android/ads/model/FlixsterAd;

    .line 27
    invoke-virtual {p0, v2}, Lcom/flixster/android/ads/HtmlAdBannerView;->setVerticalScrollBarEnabled(Z)V

    .line 28
    invoke-virtual {p0, v2}, Lcom/flixster/android/ads/HtmlAdBannerView;->setHorizontalScrollBarEnabled(Z)V

    .line 29
    new-instance v0, Lcom/flixster/android/ads/HtmlAdBannerView$HtmlAdWebViewClient;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/flixster/android/ads/HtmlAdBannerView$HtmlAdWebViewClient;-><init>(Lcom/flixster/android/ads/HtmlAdBannerView;Lcom/flixster/android/ads/HtmlAdBannerView$HtmlAdWebViewClient;)V

    invoke-virtual {p0, v0}, Lcom/flixster/android/ads/HtmlAdBannerView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 30
    invoke-virtual {p0}, Lcom/flixster/android/ads/HtmlAdBannerView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 31
    invoke-virtual {p0}, Lcom/flixster/android/ads/HtmlAdBannerView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 32
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/ads/HtmlAdBannerView;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/flixster/android/ads/HtmlAdBannerView;->launchHtmlAd(Ljava/lang/String;)V

    return-void
.end method

.method private launchHtmlAd(Ljava/lang/String;)V
    .locals 3
    .parameter "url"

    .prologue
    .line 52
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdBannerView;->htmlAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd;->title:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/flixster/android/ads/HtmlAdBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lnet/flixster/android/Starter;->launchAdHtmlPage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 53
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdBannerView;->htmlAd:Lnet/flixster/android/ads/model/FlixsterAd;

    const-string v2, "Click"

    invoke-virtual {v0, v1, v2}, Lnet/flixster/android/ads/AdManager;->trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V

    .line 54
    return-void
.end method


# virtual methods
.method public loadUrl(Ljava/lang/String;)V
    .locals 3
    .parameter "url"

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/flixster/android/ads/HtmlAdBannerView;->FRAME_LAYOUT_FILL:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/flixster/android/ads/HtmlAdBannerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 38
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdBannerView;->htmlAd:Lnet/flixster/android/ads/model/FlixsterAd;

    const-string v2, "Impression"

    invoke-virtual {v0, v1, v2}, Lnet/flixster/android/ads/AdManager;->trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V

    .line 39
    return-void
.end method
