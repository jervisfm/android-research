.class final Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "HtmlAdPage.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x7
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/ads/HtmlAdPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HtmlAdWebChromeClient"
.end annotation


# instance fields
.field private customCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field private customView:Landroid/view/View;

.field final synthetic this$0:Lcom/flixster/android/ads/HtmlAdPage;


# direct methods
.method private constructor <init>(Lcom/flixster/android/ads/HtmlAdPage;)V
    .locals 0
    .parameter

    .prologue
    .line 254
    iput-object p1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/ads/HtmlAdPage;Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;-><init>(Lcom/flixster/android/ads/HtmlAdPage;)V

    return-void
.end method


# virtual methods
.method public getVideoLoadingProgressView()Landroid/view/View;
    .locals 2

    .prologue
    .line 302
    new-instance v0, Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public onHideCustomView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 284
    const-string v0, "FlxAd"

    const-string v1, "HtmlAdPage.onHideCustomView"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->customView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 297
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->customView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 289
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    #getter for: Lcom/flixster/android/ads/HtmlAdPage;->customViewLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/flixster/android/ads/HtmlAdPage;->access$4(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->customView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 290
    iput-object v3, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->customView:Landroid/view/View;

    .line 291
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    #getter for: Lcom/flixster/android/ads/HtmlAdPage;->customViewLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/flixster/android/ads/HtmlAdPage;->access$4(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 292
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->customCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 293
    iput-object v3, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->customCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    .line 294
    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v2, 0x9

    if-lt v0, v2, :cond_1

    const/4 v0, 0x7

    :goto_1
    invoke-virtual {v1, v0}, Lcom/flixster/android/ads/HtmlAdPage;->setRequestedOrientation(I)V

    .line 296
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    #getter for: Lcom/flixster/android/ads/HtmlAdPage;->htmlLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/flixster/android/ads/HtmlAdPage;->access$5(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 295
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 5
    .parameter "view"
    .parameter "callback"

    .prologue
    const/4 v1, 0x0

    .line 260
    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    .line 261
    instance-of v2, p1, Landroid/widget/FrameLayout;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 262
    check-cast v0, Landroid/widget/FrameLayout;

    .line 263
    .local v0, frame:Landroid/widget/FrameLayout;
    const-string v2, "FlxAd"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "HtmlAdPage.onShowCustomView "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getFocusedChild()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    .end local v0           #frame:Landroid/widget/FrameLayout;
    :goto_0
    iget-object v2, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->customView:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 269
    invoke-interface {p2}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 270
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->customView:Landroid/view/View;

    .line 280
    :goto_1
    return-void

    .line 265
    :cond_0
    const-string v2, "FlxAd"

    const-string v3, "HtmlAdPage.onShowCustomView"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 273
    :cond_1
    iput-object p1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->customView:Landroid/view/View;

    .line 274
    iput-object p2, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->customCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    .line 275
    iget-object v2, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    #getter for: Lcom/flixster/android/ads/HtmlAdPage;->customViewLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/flixster/android/ads/HtmlAdPage;->access$4(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 276
    iget-object v2, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    #getter for: Lcom/flixster/android/ads/HtmlAdPage;->customViewLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/flixster/android/ads/HtmlAdPage;->access$4(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 277
    iget-object v2, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    #getter for: Lcom/flixster/android/ads/HtmlAdPage;->htmlLayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/flixster/android/ads/HtmlAdPage;->access$5(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/widget/LinearLayout;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 278
    iget-object v2, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    sget v3, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v4, 0x9

    if-lt v3, v4, :cond_2

    const/4 v1, 0x6

    :cond_2
    invoke-virtual {v2, v1}, Lcom/flixster/android/ads/HtmlAdPage;->setRequestedOrientation(I)V

    goto :goto_1
.end method
