.class final Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "HtmlAdPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/ads/HtmlAdPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HtmlAdWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/ads/HtmlAdPage;


# direct methods
.method private constructor <init>(Lcom/flixster/android/ads/HtmlAdPage;)V
    .locals 0
    .parameter

    .prologue
    .line 199
    iput-object p1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/ads/HtmlAdPage;Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;-><init>(Lcom/flixster/android/ads/HtmlAdPage;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .parameter "view"
    .parameter "url"

    .prologue
    .line 213
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    #getter for: Lcom/flixster/android/ads/HtmlAdPage;->mDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/flixster/android/ads/HtmlAdPage;->access$1(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    .line 215
    const-string v0, "/msk/mobile"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "/selection"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/msk/MskController;->trackSelectionPage()V

    .line 222
    :cond_0
    :goto_0
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HtmlAdPage.onPageFinished "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    #getter for: Lcom/flixster/android/ads/HtmlAdPage;->useHtmlTitle:Z
    invoke-static {v0}, Lcom/flixster/android/ads/HtmlAdPage;->access$2(Lcom/flixster/android/ads/HtmlAdPage;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    const-string v0, "javascript:window.flixster.setPageTitle(document.title);"

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 226
    :cond_1
    return-void

    .line 218
    :cond_2
    const-string v0, "/invite"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/msk/MskController;->trackRewardConfirmationPage()V

    goto :goto_0
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4
    .parameter "view"
    .parameter "url"
    .parameter "favicon"

    .prologue
    .line 202
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 204
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    #getter for: Lcom/flixster/android/ads/HtmlAdPage;->mDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/flixster/android/ads/HtmlAdPage;->access$1(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :goto_0
    const-string v1, "FlxAd"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HtmlAdPage.onPageStarted "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    return-void

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4
    .parameter "view"
    .parameter "url"

    .prologue
    .line 230
    const/4 v0, 0x0

    .line 231
    .local v0, result:Z
    invoke-static {p2}, Lcom/flixster/android/activity/DeepLink;->isValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 232
    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    invoke-static {p2, v1}, Lcom/flixster/android/activity/DeepLink;->launch(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    invoke-virtual {v1}, Lcom/flixster/android/ads/HtmlAdPage;->finish()V

    .line 235
    :cond_0
    const/4 v0, 0x1

    .line 243
    :cond_1
    :goto_0
    const-string v1, "FlxAd"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HtmlAdPage.shouldOverrideUrlLoading "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    return v0

    .line 236
    :cond_2
    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    #getter for: Lcom/flixster/android/ads/HtmlAdPage;->launchBrowserForLinks:Z
    invoke-static {v1}, Lcom/flixster/android/ads/HtmlAdPage;->access$3(Lcom/flixster/android/ads/HtmlAdPage;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 237
    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    invoke-static {p2, v1}, Lnet/flixster/android/Starter;->launchBrowser(Ljava/lang/String;Landroid/content/Context;)V

    .line 238
    const/4 v0, 0x1

    goto :goto_0

    .line 239
    :cond_3
    invoke-static {p2}, Lnet/flixster/android/data/ApiBuilder;->isTrailerUrl(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {p2}, Lnet/flixster/android/data/ApiBuilder;->isVideoLink(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 240
    :cond_4
    iget-object v1, p0, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;->this$0:Lcom/flixster/android/ads/HtmlAdPage;

    const/4 v2, 0x1

    invoke-static {p2, v1, v2}, Lnet/flixster/android/Starter;->launchVideo(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 241
    const/4 v0, 0x1

    goto :goto_0
.end method
