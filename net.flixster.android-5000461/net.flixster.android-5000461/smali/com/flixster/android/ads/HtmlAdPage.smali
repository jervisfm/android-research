.class public Lcom/flixster/android/ads/HtmlAdPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "HtmlAdPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/ads/HtmlAdPage$FlixsterJavacriptInterface;,
        Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;,
        Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;
    }
.end annotation


# static fields
.field public static final FLAG_BACK_NAVIGATION:I = 0x4

.field public static final FLAG_CLEAR_COOKIE:I = 0x1

.field public static final FLAG_HTML_TITLE:I = 0x2

.field public static final FLAG_IS_AD:I = 0x20

.field public static final FLAG_LAUNCH_BROWSER:I = 0x10

.field public static final FLAG_SHOW_SKIP:I = 0x40

.field public static final FLAG_SUPPORT_ZOOM:I = 0x8

.field public static final KEY_FLAGS:Ljava/lang/String; = "KEY_FLAGS"

.field public static final KEY_TITLE:Ljava/lang/String; = "KEY_TITLE"

.field public static final KEY_URL:Ljava/lang/String; = "KEY_URL"

.field private static final LINEAR_LAYOUT_FILL:Landroid/widget/LinearLayout$LayoutParams;

.field private static instanceCount:I


# instance fields
.field private clearCookie:Z

.field private customViewLayout:Landroid/widget/FrameLayout;

.field private enableBackNavigation:Z

.field private htmlLayout:Landroid/widget/LinearLayout;

.field private isAd:Z

.field private launchBrowserForLinks:Z

.field private mDialog:Landroid/app/ProgressDialog;

.field private mWebView:Landroid/webkit/WebView;

.field private showSkip:Z

.field private supportZoom:Z

.field private final titleHandler:Landroid/os/Handler;

.field private useHtmlTitle:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 63
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/flixster/android/ads/HtmlAdPage;->LINEAR_LAYOUT_FILL:Landroid/widget/LinearLayout$LayoutParams;

    .line 66
    const/4 v0, 0x0

    sput v0, Lcom/flixster/android/ads/HtmlAdPage;->instanceCount:I

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 179
    new-instance v0, Lcom/flixster/android/ads/HtmlAdPage$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/ads/HtmlAdPage$1;-><init>(Lcom/flixster/android/ads/HtmlAdPage;)V

    iput-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->titleHandler:Landroid/os/Handler;

    .line 49
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/ads/HtmlAdPage;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/flixster/android/ads/HtmlAdPage;->setTitleText(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/ads/HtmlAdPage;)Z
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->useHtmlTitle:Z

    return v0
.end method

.method static synthetic access$3(Lcom/flixster/android/ads/HtmlAdPage;)Z
    .locals 1
    .parameter

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->launchBrowserForLinks:Z

    return v0
.end method

.method static synthetic access$4(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/widget/FrameLayout;
    .locals 1
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->customViewLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->htmlLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$6(Lcom/flixster/android/ads/HtmlAdPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 179
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->titleHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static addTokenToUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "url"

    .prologue
    .line 189
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    const-string v2, "flixster.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 195
    .local v0, e:Ljava/net/MalformedURLException;
    :cond_0
    :goto_0
    return-object p0

    .line 194
    .end local v0           #e:Ljava/net/MalformedURLException;
    :catch_0
    move-exception v0

    .line 195
    .restart local v0       #e:Ljava/net/MalformedURLException;
    goto :goto_0
.end method

.method private setTitleText(Ljava/lang/String;)V
    .locals 1
    .parameter "title"

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->isAd:Z

    if-nez v0, :cond_0

    .line 143
    invoke-virtual {p0, p1}, Lcom/flixster/android/ads/HtmlAdPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 145
    :cond_0
    return-void
.end method

.method private setUpWebView(Ljava/lang/String;)V
    .locals 4
    .parameter "url"
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 124
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 125
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 126
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v0

    const/4 v1, 0x7

    if-lt v0, v1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;

    invoke-direct {v1, p0, v2}, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;-><init>(Lcom/flixster/android/ads/HtmlAdPage;Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebChromeClient;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;

    invoke-direct {v1, p0, v2}, Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;-><init>(Lcom/flixster/android/ads/HtmlAdPage;Lcom/flixster/android/ads/HtmlAdPage$HtmlAdWebViewClient;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 130
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 131
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    iget-boolean v1, p0, Lcom/flixster/android/ads/HtmlAdPage;->supportZoom:Z

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 132
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/flixster/android/ads/HtmlAdPage$FlixsterJavacriptInterface;

    invoke-direct {v1, p0, v2}, Lcom/flixster/android/ads/HtmlAdPage$FlixsterJavacriptInterface;-><init>(Lcom/flixster/android/ads/HtmlAdPage;Lcom/flixster/android/ads/HtmlAdPage$FlixsterJavacriptInterface;)V

    const-string v2, "flixster"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    sget-object v1, Lcom/flixster/android/ads/HtmlAdPage;->LINEAR_LAYOUT_FILL:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mDialog:Landroid/app/ProgressDialog;

    .line 137
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/ads/HtmlAdPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 139
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 79
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    sget v4, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v7, 0x9

    if-lt v4, v7, :cond_2

    const/4 v4, 0x7

    :goto_0
    invoke-virtual {p0, v4}, Lcom/flixster/android/ads/HtmlAdPage;->setRequestedOrientation(I)V

    .line 82
    sget v4, Lcom/flixster/android/ads/HtmlAdPage;->instanceCount:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/flixster/android/ads/HtmlAdPage;->instanceCount:I

    .line 84
    const/4 v3, 0x0

    .line 85
    .local v3, url:Ljava/lang/String;
    const/4 v2, 0x0

    .line 86
    .local v2, title:Ljava/lang/String;
    const/4 v0, 0x0

    .line 87
    .local v0, configBits:I
    invoke-virtual {p0}, Lcom/flixster/android/ads/HtmlAdPage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 88
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 89
    const-string v4, "KEY_URL"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 90
    const-string v4, "KEY_TITLE"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 91
    const-string v4, "KEY_FLAGS"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 93
    if-eqz v0, :cond_0

    .line 94
    and-int/lit8 v4, v0, 0x2

    const/4 v7, 0x2

    if-ne v4, v7, :cond_3

    move v4, v5

    :goto_1
    iput-boolean v4, p0, Lcom/flixster/android/ads/HtmlAdPage;->useHtmlTitle:Z

    .line 95
    and-int/lit8 v4, v0, 0x4

    const/4 v7, 0x4

    if-ne v4, v7, :cond_4

    move v4, v5

    :goto_2
    iput-boolean v4, p0, Lcom/flixster/android/ads/HtmlAdPage;->enableBackNavigation:Z

    .line 96
    and-int/lit8 v4, v0, 0x8

    const/16 v7, 0x8

    if-ne v4, v7, :cond_5

    move v4, v5

    :goto_3
    iput-boolean v4, p0, Lcom/flixster/android/ads/HtmlAdPage;->supportZoom:Z

    .line 97
    and-int/lit8 v4, v0, 0x10

    const/16 v7, 0x10

    if-ne v4, v7, :cond_6

    move v4, v5

    :goto_4
    iput-boolean v4, p0, Lcom/flixster/android/ads/HtmlAdPage;->launchBrowserForLinks:Z

    .line 98
    and-int/lit8 v4, v0, 0x1

    if-ne v4, v5, :cond_7

    move v4, v5

    :goto_5
    iput-boolean v4, p0, Lcom/flixster/android/ads/HtmlAdPage;->clearCookie:Z

    .line 99
    and-int/lit8 v4, v0, 0x20

    const/16 v7, 0x20

    if-ne v4, v7, :cond_8

    move v4, v5

    :goto_6
    iput-boolean v4, p0, Lcom/flixster/android/ads/HtmlAdPage;->isAd:Z

    .line 100
    and-int/lit8 v4, v0, 0x40

    const/16 v7, 0x40

    if-ne v4, v7, :cond_9

    :goto_7
    iput-boolean v5, p0, Lcom/flixster/android/ads/HtmlAdPage;->showSkip:Z

    .line 104
    :cond_0
    const-string v4, "FlxAd"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "HtmlAdPage.onCreate "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const v4, 0x7f030036

    invoke-virtual {p0, v4}, Lcom/flixster/android/ads/HtmlAdPage;->setContentView(I)V

    .line 107
    iget-boolean v4, p0, Lcom/flixster/android/ads/HtmlAdPage;->isAd:Z

    if-eqz v4, :cond_a

    .line 108
    invoke-virtual {p0}, Lcom/flixster/android/ads/HtmlAdPage;->hideActionBar()V

    .line 116
    :cond_1
    :goto_8
    const v4, 0x7f0700cd

    invoke-virtual {p0, v4}, Lcom/flixster/android/ads/HtmlAdPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout;

    iput-object v4, p0, Lcom/flixster/android/ads/HtmlAdPage;->customViewLayout:Landroid/widget/FrameLayout;

    .line 117
    const v4, 0x7f0700ce

    invoke-virtual {p0, v4}, Lcom/flixster/android/ads/HtmlAdPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/flixster/android/ads/HtmlAdPage;->htmlLayout:Landroid/widget/LinearLayout;

    .line 118
    const v4, 0x7f0700cf

    invoke-virtual {p0, v4}, Lcom/flixster/android/ads/HtmlAdPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/webkit/WebView;

    iput-object v4, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    .line 119
    const-string v4, "/msk/mobile"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    .end local v3           #url:Ljava/lang/String;
    :goto_9
    invoke-direct {p0, v3}, Lcom/flixster/android/ads/HtmlAdPage;->setUpWebView(Ljava/lang/String;)V

    .line 120
    return-void

    .end local v0           #configBits:I
    .end local v1           #extras:Landroid/os/Bundle;
    .end local v2           #title:Ljava/lang/String;
    :cond_2
    move v4, v5

    .line 81
    goto/16 :goto_0

    .restart local v0       #configBits:I
    .restart local v1       #extras:Landroid/os/Bundle;
    .restart local v2       #title:Ljava/lang/String;
    .restart local v3       #url:Ljava/lang/String;
    :cond_3
    move v4, v6

    .line 94
    goto/16 :goto_1

    :cond_4
    move v4, v6

    .line 95
    goto/16 :goto_2

    :cond_5
    move v4, v6

    .line 96
    goto :goto_3

    :cond_6
    move v4, v6

    .line 97
    goto :goto_4

    :cond_7
    move v4, v6

    .line 98
    goto :goto_5

    :cond_8
    move v4, v6

    .line 99
    goto :goto_6

    :cond_9
    move v5, v6

    .line 100
    goto :goto_7

    .line 110
    :cond_a
    invoke-virtual {p0}, Lcom/flixster/android/ads/HtmlAdPage;->createActionBar()V

    .line 111
    if-eqz v2, :cond_1

    .line 112
    invoke-direct {p0, v2}, Lcom/flixster/android/ads/HtmlAdPage;->setTitleText(Ljava/lang/String;)V

    goto :goto_8

    .line 119
    :cond_b
    invoke-static {v3}, Lcom/flixster/android/ads/HtmlAdPage;->addTokenToUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_9
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->showSkip:Z

    if-eqz v0, :cond_0

    .line 318
    invoke-virtual {p0}, Lcom/flixster/android/ads/HtmlAdPage;->getSupportMenuInflater()Lcom/actionbarsherlock/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1, p1}, Lcom/actionbarsherlock/view/MenuInflater;->inflate(ILcom/actionbarsherlock/view/Menu;)V

    .line 320
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 159
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onDestroy()V

    .line 160
    sget v2, Lcom/flixster/android/ads/HtmlAdPage;->instanceCount:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Lcom/flixster/android/ads/HtmlAdPage;->instanceCount:I

    .line 162
    iget-object v2, p0, Lcom/flixster/android/ads/HtmlAdPage;->mDialog:Landroid/app/ProgressDialog;

    if-eqz v2, :cond_0

    .line 164
    :try_start_0
    iget-object v2, p0, Lcom/flixster/android/ads/HtmlAdPage;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/flixster/android/ads/HtmlAdPage;->clearCookie:Z

    if-eqz v2, :cond_1

    sget v2, Lcom/flixster/android/ads/HtmlAdPage;->instanceCount:I

    if-nez v2, :cond_1

    .line 172
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 173
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 174
    .local v0, cookieManager:Landroid/webkit/CookieManager;
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 175
    const-string v2, "FlxAd"

    const-string v3, "HtmlAdPage.onDestroy cookies cleared"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    .end local v0           #cookieManager:Landroid/webkit/CookieManager;
    :cond_1
    return-void

    .line 165
    :catch_0
    move-exception v1

    .line 166
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->enableBackNavigation:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/flixster/android/ads/HtmlAdPage;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 151
    const/4 v0, 0x1

    .line 153
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 325
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 332
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 327
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/msk/MskController;->trackSkipFirstLaunchMsk()V

    .line 328
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->setMskPromptShown()V

    .line 329
    invoke-virtual {p0}, Lcom/flixster/android/ads/HtmlAdPage;->finish()V

    .line 330
    const/4 v0, 0x1

    goto :goto_0

    .line 325
    nop

    :pswitch_data_0
    .packed-switch 0x7f0702fd
        :pswitch_0
    .end packed-switch
.end method
