.class public Lcom/flixster/android/ads/rule/IntervalCapRule;
.super Ljava/lang/Object;
.source "IntervalCapRule.java"

# interfaces
.implements Lcom/flixster/android/ads/rule/AdRule;


# instance fields
.field private counter:I

.field private final interval:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .parameter "interval"

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/flixster/android/ads/rule/IntervalCapRule;->interval:I

    .line 12
    return-void
.end method


# virtual methods
.method public adShown()V
    .locals 0

    .prologue
    .line 23
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/flixster/android/ads/rule/IntervalCapRule;->counter:I

    .line 28
    return-void
.end method

.method public shouldShowAd()Z
    .locals 2

    .prologue
    .line 17
    iget v0, p0, Lcom/flixster/android/ads/rule/IntervalCapRule;->counter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/flixster/android/ads/rule/IntervalCapRule;->counter:I

    iget v1, p0, Lcom/flixster/android/ads/rule/IntervalCapRule;->interval:I

    rem-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
