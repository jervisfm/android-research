.class public Lcom/flixster/android/ads/rule/LaunchRule;
.super Ljava/lang/Object;
.source "LaunchRule.java"

# interfaces
.implements Lcom/flixster/android/ads/rule/AdRule;


# instance fields
.field private shown:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public adShown()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/ads/rule/LaunchRule;->shown:Z

    .line 17
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flixster/android/ads/rule/LaunchRule;->shown:Z

    .line 22
    return-void
.end method

.method public shouldShowAd()Z
    .locals 1

    .prologue
    .line 11
    iget-boolean v0, p0, Lcom/flixster/android/ads/rule/LaunchRule;->shown:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
