.class public Lcom/flixster/android/ads/rule/TimeCapRule;
.super Ljava/lang/Object;
.source "TimeCapRule.java"

# interfaces
.implements Lcom/flixster/android/ads/rule/AdRule;


# instance fields
.field private final intervalMs:J

.field private lastShownTimeMs:J


# direct methods
.method public constructor <init>(J)V
    .locals 0
    .parameter "intervalMs"

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-wide p1, p0, Lcom/flixster/android/ads/rule/TimeCapRule;->intervalMs:J

    .line 14
    return-void
.end method


# virtual methods
.method public adShown()V
    .locals 2

    .prologue
    .line 23
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/flixster/android/ads/rule/TimeCapRule;->lastShownTimeMs:J

    .line 24
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 28
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/flixster/android/ads/rule/TimeCapRule;->lastShownTimeMs:J

    .line 29
    return-void
.end method

.method public shouldShowAd()Z
    .locals 4

    .prologue
    .line 18
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/flixster/android/ads/rule/TimeCapRule;->lastShownTimeMs:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/flixster/android/ads/rule/TimeCapRule;->intervalMs:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
