.class public Lcom/flixster/android/analytics/InstallReferrerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "InstallReferrerReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static onReceive(Landroid/content/Intent;)V
    .locals 10
    .parameter "intent"

    .prologue
    .line 23
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "referrer"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 24
    .local v4, referrer:Ljava/lang/String;
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "InstallReferrerReceiver.onReceive referrer: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->fd(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const/4 v1, 0x0

    .line 28
    .local v1, existingReferrer:Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isSharedPrefInitialized()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 29
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getReferrer()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 32
    invoke-static {v4}, Lcom/flixster/android/utils/UrlHelper;->getQueries(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v6

    .line 33
    .local v6, vars:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const-string v7, "utm_source"

    invoke-static {v6, v7}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 34
    .local v5, source:Ljava/lang/String;
    const-string v7, "utm_medium"

    invoke-static {v6, v7}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 35
    .local v3, medium:Ljava/lang/String;
    const-string v7, "utm_campaign"

    invoke-static {v6, v7}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, campaign:Ljava/lang/String;
    const-string v7, "term"

    invoke-static {v6, v7}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, keywords:Ljava/lang/String;
    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setReferrer(Ljava/lang/String;)V

    .line 42
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v7

    invoke-interface {v7, v5, v3, v0, v2}, Lcom/flixster/android/analytics/Tracker;->trackInstall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .end local v0           #campaign:Ljava/lang/String;
    .end local v2           #keywords:Ljava/lang/String;
    .end local v3           #medium:Ljava/lang/String;
    .end local v5           #source:Ljava/lang/String;
    .end local v6           #vars:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_0
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "InstallReferrerReceiver.onReceive existingReferrer: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 45
    if-eqz v1, :cond_1

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v1, "the same"

    .end local v1           #existingReferrer:Ljava/lang/String;
    :cond_1
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 44
    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->fd(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 19
    invoke-static {p2}, Lcom/flixster/android/analytics/InstallReferrerReceiver;->onReceive(Landroid/content/Intent;)V

    .line 20
    return-void
.end method
