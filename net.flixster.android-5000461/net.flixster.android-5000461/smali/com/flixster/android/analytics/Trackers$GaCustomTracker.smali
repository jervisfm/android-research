.class Lcom/flixster/android/analytics/Trackers$GaCustomTracker;
.super Ljava/lang/Object;
.source "Trackers.java"

# interfaces
.implements Lcom/flixster/android/analytics/Tracker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/analytics/Trackers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GaCustomTracker"
.end annotation


# static fields
.field private static final GA_DEV_ACCOUNT:Ljava/lang/String; = "UA-23007375-1"

.field private static final GA_GTV_ACCOUNT:Ljava/lang/String; = "UA-19205027-1"

.field private static final GA_HONEYCOMB_ACCOUNT:Ljava/lang/String; = "UA-22157832-1"

.field private static final GA_PHONE_ACCOUNT:Ljava/lang/String; = "UA-8284864-1"

.field private static final GA_REFERRAL_ACCOUNT:Ljava/lang/String; = "UA-21839535-1"


# instance fields
.field private final account:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 5

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 125
    .local v3, model:Ljava/lang/String;
    const-string v4, "kindle"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "nook"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "nexus 7"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v2, 0x0

    .line 126
    .local v2, isReader:Z
    :goto_0
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v1

    .line 127
    .local v1, isHcTablet:Z
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v0

    .line 128
    .local v0, isGtv:Z
    if-eqz v0, :cond_1

    const-string v4, "UA-19205027-1"

    :goto_1
    iput-object v4, p0, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->account:Ljava/lang/String;

    .line 130
    return-void

    .line 125
    .end local v0           #isGtv:Z
    .end local v1           #isHcTablet:Z
    .end local v2           #isReader:Z
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 129
    .restart local v0       #isGtv:Z
    .restart local v1       #isHcTablet:Z
    .restart local v2       #isReader:Z
    :cond_1
    if-nez v1, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    const-string v4, "UA-22157832-1"

    goto :goto_1

    :cond_3
    const-string v4, "UA-8284864-1"

    goto :goto_1
.end method

.method synthetic constructor <init>(Lcom/flixster/android/analytics/Trackers$GaCustomTracker;)V
    .locals 0
    .parameter

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;-><init>()V

    return-void
.end method

.method private static createEventTrackingParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .parameter "category"
    .parameter "action"
    .parameter "label"
    .parameter "value"

    .prologue
    .line 261
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 262
    :cond_0
    const-string v1, ""

    .line 273
    :goto_0
    return-object v1

    .line 264
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "&utmt=event&utme=5"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 265
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    if-nez p2, :cond_2

    .line 268
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 270
    :cond_2
    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/flixster/android/utils/StringHelper;->replaceSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/flixster/android/utils/StringHelper;->removeParentheses(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private static createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .parameter "tag"
    .parameter "title"
    .parameter "account"

    .prologue
    const/4 v2, 0x0

    .line 176
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, p2

    invoke-static/range {v0 .. v10}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .parameter "tag"
    .parameter "title"
    .parameter "category"
    .parameter "action"
    .parameter "account"

    .prologue
    const/4 v4, 0x0

    .line 180
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, v4

    move-object v10, p4

    invoke-static/range {v0 .. v10}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 11
    .parameter "tag"
    .parameter "title"
    .parameter "category"
    .parameter "action"
    .parameter "label"
    .parameter "value"
    .parameter "account"

    .prologue
    .line 185
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move-object/from16 v10, p6

    invoke-static/range {v0 .. v10}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 15
    .parameter "tag"
    .parameter "title"
    .parameter "category"
    .parameter "action"
    .parameter "label"
    .parameter "value"
    .parameter "source"
    .parameter "medium"
    .parameter "campaign"
    .parameter "keywords"
    .parameter "account"

    .prologue
    .line 191
    if-nez p0, :cond_0

    const-string p0, "null"

    .line 192
    :cond_0
    if-nez p1, :cond_1

    const-string p1, "null"

    .line 193
    :cond_1
    const/4 v2, 0x0

    .line 195
    .local v2, url:Ljava/lang/String;
    :try_start_0
    sget-object v11, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v12, "UTF-8"

    invoke-static {v11, v12}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 196
    .local v8, utmfl:Ljava/lang/String;
    const-string v11, "UTF-8"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 197
    .local v7, utmdt:Ljava/lang/String;
    const-string v11, "UTF-8"

    invoke-static {p0, v11}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 198
    .local v9, utmp:Ljava/lang/String;
    const-string v5, "239913861"

    .line 199
    .local v5, utmcc7:Ljava/lang/String;
    invoke-static/range {p6 .. p9}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->createReferrerParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 200
    .local v6, utmccpost:Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "__utma%3D239913861."

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 201
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getGaFirstSession()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getGaLastSession()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 202
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getGaThisSession()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getGaSessionCount()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 203
    const-string v12, "%3B%2B__utmz%3D"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getGaThisSession()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 204
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getGaSessionCount()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 200
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 205
    .local v4, utmcc:Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetworkType()Ljava/lang/String;

    move-result-object v11

    const-string v12, "UTF-8"

    invoke-static {v11, v12}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 206
    .local v10, utmsc:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v11, "http://www.google-analytics.com/__utm.gif?utmwv=4.3"

    invoke-direct {v3, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 207
    .local v3, urlBuilder:Ljava/lang/StringBuilder;
    const-string v11, "&utmn="

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/flixster/android/utils/MathHelper;->randomId()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 208
    const-string v11, "&utmhn=www.flixster.com"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    invoke-static/range {p2 .. p5}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->createEventTrackingParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    const-string v11, "&utmcs=ISO-8859-1&utmsr="

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentDimensions()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    const-string v11, "&utmsc="

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    const-string v11, "&utmul=en-us&utmje=0&utmfl="

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const-string v11, "&utmdt="

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    const-string v11, "&utmhid="

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/flixster/android/utils/MathHelper;->randomId()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 215
    const-string v11, "&utmr=0&utmp="

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    const-string v11, "&utmac="

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p10

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    const-string v11, "&utmcc="

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 223
    .end local v3           #urlBuilder:Ljava/lang/StringBuilder;
    .end local v4           #utmcc:Ljava/lang/String;
    .end local v5           #utmcc7:Ljava/lang/String;
    .end local v6           #utmccpost:Ljava/lang/String;
    .end local v7           #utmdt:Ljava/lang/String;
    .end local v8           #utmfl:Ljava/lang/String;
    .end local v9           #utmp:Ljava/lang/String;
    .end local v10           #utmsc:Ljava/lang/String;
    :goto_0
    const-string v12, "FlxGa"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v13, "tag:\""

    invoke-direct {v11, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 224
    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 225
    const-string v13, "\" title:\""

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 226
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 227
    const-string v13, "\""

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 228
    if-eqz p2, :cond_2

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v14, " category:\""

    invoke-direct {v11, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "\" action:\""

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "\""

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :goto_1
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 229
    if-eqz p4, :cond_3

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v14, " label:\""

    invoke-direct {v11, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "\""

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :goto_2
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 230
    if-eqz p4, :cond_4

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v14, " value:\""

    invoke-direct {v11, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "\""

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :goto_3
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 231
    if-eqz p6, :cond_5

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v14, " source:\""

    invoke-direct {v11, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "\" medium:\""

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p7

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "\" campaign:\""

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p8

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 232
    const-string v14, "\" keywords:\""

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p9

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "\""

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 231
    :goto_4
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 223
    invoke-static {v12, v11}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v11, "FlxGa"

    invoke-static {v11, v2}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    return-object v2

    .line 220
    :catch_0
    move-exception v1

    .line 221
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    const-string v11, "FlxGa"

    const-string v12, "GaCustomTracker.createGaUrl"

    invoke-static {v11, v12, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 228
    .end local v1           #e:Ljava/io/UnsupportedEncodingException;
    :cond_2
    const-string v11, ""

    goto/16 :goto_1

    .line 229
    :cond_3
    const-string v11, ""

    goto :goto_2

    .line 230
    :cond_4
    const-string v11, ""

    goto :goto_3

    .line 232
    :cond_5
    const-string v11, ""

    goto :goto_4
.end method

.method private static createReferrerParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "source"
    .parameter "medium"
    .parameter "campaign"
    .parameter "keywords"

    .prologue
    .line 239
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 240
    const-string v1, ".1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)"

    .line 255
    :goto_0
    return-object v1

    .line 242
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ".1."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 243
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "utmcsr="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p0, :cond_3

    const-string v1, "null"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 244
    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const-string v1, "utmccn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p2, :cond_4

    const-string v1, "null"

    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 246
    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    const-string v1, "utmcmd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_5

    const-string v1, "null"

    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 248
    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    if-eqz p3, :cond_1

    .line 250
    const-string v1, "utmctr="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3}, Lcom/flixster/android/utils/StringHelper;->replaceNonAlphaNumeric(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x7c

    if-ne v1, v2, :cond_2

    .line 253
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 255
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/flixster/android/utils/UrlHelper;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 243
    :cond_3
    invoke-static {p0}, Lcom/flixster/android/utils/StringHelper;->replaceNonAlphaNumeric(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 245
    :cond_4
    invoke-static {p2}, Lcom/flixster/android/utils/StringHelper;->replaceNonAlphaNumeric(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 247
    :cond_5
    invoke-static {p1}, Lcom/flixster/android/utils/StringHelper;->replaceNonAlphaNumeric(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method


# virtual methods
.method public start(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 168
    return-void
.end method

.method public stop()V
    .locals 0

    .prologue
    .line 173
    return-void
.end method

.method public track(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "tag"
    .parameter "title"

    .prologue
    .line 134
    iget-object v1, p0, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->account:Ljava/lang/String;

    invoke-static {p1, p2, v1}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, url:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 136
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    .line 138
    :cond_0
    return-void
.end method

.method public trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "tag"
    .parameter "title"
    .parameter "category"
    .parameter "action"

    .prologue
    .line 142
    iget-object v1, p0, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->account:Ljava/lang/String;

    invoke-static {p1, p2, p3, p4, v1}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, url:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 144
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    .line 146
    :cond_0
    return-void
.end method

.method public trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .parameter "tag"
    .parameter "title"
    .parameter "category"
    .parameter "action"
    .parameter "label"
    .parameter "value"

    .prologue
    .line 150
    iget-object v6, p0, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->account:Ljava/lang/String;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-static/range {v0 .. v6}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 151
    .local v7, url:Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 152
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v7, v1}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    .line 154
    :cond_0
    return-void
.end method

.method public trackInstall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .parameter "source"
    .parameter "medium"
    .parameter "campaign"
    .parameter "keywords"

    .prologue
    .line 158
    const-string v0, "/install"

    const-string v1, "Install"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 159
    const-string v10, "UA-21839535-1"

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    move-object/from16 v9, p4

    .line 158
    invoke-static/range {v0 .. v10}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;->createGaUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 160
    .local v11, url:Ljava/lang/String;
    if-eqz v11, :cond_0

    .line 161
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v11, v1}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    .line 163
    :cond_0
    return-void
.end method
