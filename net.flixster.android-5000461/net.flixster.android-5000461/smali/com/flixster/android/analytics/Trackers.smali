.class public Lcom/flixster/android/analytics/Trackers;
.super Ljava/lang/Object;
.source "Trackers.java"

# interfaces
.implements Lcom/flixster/android/analytics/Tracker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/analytics/Trackers$GaCustomTracker;,
        Lcom/flixster/android/analytics/Trackers$GraphiteCustomTracker;
    }
.end annotation


# static fields
.field private static final INSTANCE:Lcom/flixster/android/analytics/Trackers;


# instance fields
.field private final gaCustomTracker:Lcom/flixster/android/analytics/Tracker;

.field private final graphiteCustomTracker:Lcom/flixster/android/analytics/Tracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/flixster/android/analytics/Trackers;

    invoke-direct {v0}, Lcom/flixster/android/analytics/Trackers;-><init>()V

    sput-object v0, Lcom/flixster/android/analytics/Trackers;->INSTANCE:Lcom/flixster/android/analytics/Trackers;

    .line 21
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;

    invoke-direct {v0, v1}, Lcom/flixster/android/analytics/Trackers$GaCustomTracker;-><init>(Lcom/flixster/android/analytics/Trackers$GaCustomTracker;)V

    iput-object v0, p0, Lcom/flixster/android/analytics/Trackers;->gaCustomTracker:Lcom/flixster/android/analytics/Tracker;

    .line 31
    new-instance v0, Lcom/flixster/android/analytics/Trackers$GraphiteCustomTracker;

    invoke-direct {v0, v1}, Lcom/flixster/android/analytics/Trackers$GraphiteCustomTracker;-><init>(Lcom/flixster/android/analytics/Trackers$GraphiteCustomTracker;)V

    iput-object v0, p0, Lcom/flixster/android/analytics/Trackers;->graphiteCustomTracker:Lcom/flixster/android/analytics/Tracker;

    .line 32
    return-void
.end method

.method public static instance()Lcom/flixster/android/analytics/Tracker;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/flixster/android/analytics/Trackers;->INSTANCE:Lcom/flixster/android/analytics/Trackers;

    return-object v0
.end method


# virtual methods
.method public start(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 68
    return-void
.end method

.method public stop()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public track(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "tag"
    .parameter "title"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/flixster/android/analytics/Trackers;->gaCustomTracker:Lcom/flixster/android/analytics/Tracker;

    invoke-interface {v0, p1, p2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-string v0, "/msk"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/flixster/android/analytics/Trackers;->graphiteCustomTracker:Lcom/flixster/android/analytics/Tracker;

    invoke-interface {v0, p1, p2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_0
    return-void
.end method

.method public trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "tag"
    .parameter "title"
    .parameter "category"
    .parameter "action"

    .prologue
    .line 49
    iget-object v0, p0, Lcom/flixster/android/analytics/Trackers;->gaCustomTracker:Lcom/flixster/android/analytics/Tracker;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .parameter "tag"
    .parameter "title"
    .parameter "category"
    .parameter "action"
    .parameter "label"
    .parameter "value"

    .prologue
    .line 54
    iget-object v0, p0, Lcom/flixster/android/analytics/Trackers;->gaCustomTracker:Lcom/flixster/android/analytics/Tracker;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 55
    const-string v0, "/msk"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/flixster/android/analytics/Trackers;->graphiteCustomTracker:Lcom/flixster/android/analytics/Tracker;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 58
    :cond_0
    return-void
.end method

.method public trackInstall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "source"
    .parameter "medium"
    .parameter "campaign"
    .parameter "keywords"

    .prologue
    .line 62
    iget-object v0, p0, Lcom/flixster/android/analytics/Trackers;->gaCustomTracker:Lcom/flixster/android/analytics/Tracker;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/flixster/android/analytics/Tracker;->trackInstall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method
