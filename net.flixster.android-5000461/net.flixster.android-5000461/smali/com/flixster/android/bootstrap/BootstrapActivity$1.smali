.class Lcom/flixster/android/bootstrap/BootstrapActivity$1;
.super Landroid/os/Handler;
.source "BootstrapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/bootstrap/BootstrapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/bootstrap/BootstrapActivity;


# direct methods
.method constructor <init>(Lcom/flixster/android/bootstrap/BootstrapActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/bootstrap/BootstrapActivity$1;->this$0:Lcom/flixster/android/bootstrap/BootstrapActivity;

    .line 124
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 126
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/ActivityHolder;->getTopLevelActivity()Landroid/app/Activity;

    move-result-object v0

    .line 127
    .local v0, activity:Landroid/app/Activity;
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v2

    if-nez v2, :cond_0

    .line 129
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v1

    .line 130
    .local v1, p:Lnet/flixster/android/model/Property;
    iget-boolean v2, v1, Lnet/flixster/android/model/Property;->isMskFirstLaunchEnabled:Z

    if-eqz v2, :cond_0

    .line 131
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v2

    const-string v3, "lasp"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 132
    invoke-static {v0}, Lnet/flixster/android/Starter;->launchFirstMsk(Landroid/content/Context;)V

    .line 135
    .end local v1           #p:Lnet/flixster/android/model/Property;
    :cond_0
    return-void
.end method
