.class public Lcom/flixster/android/bootstrap/BootstrapActivity;
.super Landroid/app/Activity;
.source "BootstrapActivity.java"


# instance fields
.field private final fetchPropertiesSuccessHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 124
    new-instance v0, Lcom/flixster/android/bootstrap/BootstrapActivity$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/bootstrap/BootstrapActivity$1;-><init>(Lcom/flixster/android/bootstrap/BootstrapActivity;)V

    iput-object v0, p0, Lcom/flixster/android/bootstrap/BootstrapActivity;->fetchPropertiesSuccessHandler:Landroid/os/Handler;

    .line 30
    return-void
.end method

.method private static getGoogleTvTopLevelActivity()Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 110
    const/4 v0, 0x0

    .line 112
    .local v0, c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :try_start_0
    const-string v2, "com.flixster.android.activity.gtv.Main"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 116
    return-object v0

    .line 113
    :catch_0
    move-exception v1

    .line 114
    .local v1, e:Ljava/lang/ClassNotFoundException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private static getHoneycombTopLevelActivity()Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 102
    .local v0, c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :try_start_0
    const-string v2, "com.flixster.android.activity.hc.Main"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 106
    return-object v0

    .line 103
    :catch_0
    move-exception v1

    .line 104
    .local v1, e:Ljava/lang/ClassNotFoundException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static getMainIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4
    .parameter "context"

    .prologue
    .line 93
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v1

    .line 94
    .local v1, isHcTablet:Z
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v0

    .line 95
    .local v0, isGtv:Z
    new-instance v3, Landroid/content/Intent;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/flixster/android/bootstrap/BootstrapActivity;->getGoogleTvTopLevelActivity()Ljava/lang/Class;

    move-result-object v2

    :goto_0
    invoke-direct {v3, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v3

    :cond_0
    if-eqz v1, :cond_1

    invoke-static {}, Lcom/flixster/android/bootstrap/BootstrapActivity;->getHoneycombTopLevelActivity()Ljava/lang/Class;

    move-result-object v2

    goto :goto_0

    .line 96
    :cond_1
    invoke-static {}, Lcom/flixster/android/bootstrap/BootstrapActivity;->getPhoneTopLevelActivity()Ljava/lang/Class;

    move-result-object v2

    goto :goto_0
.end method

.method private static getPhoneTopLevelActivity()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 121
    const-class v0, Lnet/flixster/android/Flixster;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 34
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const-string v1, "FlxMain"

    const-string v2, "BootstrapActivity.onCreate"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    invoke-static {}, Lnet/flixster/android/Flixster;->getInstanceCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 39
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->setupLocationServices()V

    .line 42
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->launch()Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->onAppStart()V

    .line 43
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->onAppStart()V

    .line 44
    invoke-static {}, Lcom/flixster/android/bootstrap/Startup;->instance()Lcom/flixster/android/bootstrap/Startup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/bootstrap/Startup;->reset()V

    .line 45
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->reset()V

    .line 48
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    .line 51
    invoke-static {p0}, Lcom/flixster/android/utils/AppRater;->appLaunched(Landroid/content/Context;)V

    .line 52
    invoke-static {}, Lcom/flixster/android/utils/FirstLaunchMskHelper;->appLaunched()V

    .line 56
    :try_start_0
    invoke-static {}, Lcom/comscore/analytics/Census;->getInstance()Lcom/comscore/analytics/Census;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/bootstrap/BootstrapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "3002201"

    .line 57
    const-string v4, "cf451e1a3ba37e25b3bc99931721b0ea"

    .line 56
    invoke-virtual {v1, v2, v3, v4}, Lcom/comscore/analytics/Census;->notifyStart(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    iget-object v1, p0, Lcom/flixster/android/bootstrap/BootstrapActivity;->fetchPropertiesSuccessHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lnet/flixster/android/data/MiscDao;->fetchProperties(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 67
    :goto_1
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "FlxMain"

    const-string v2, "BootstrapActivity.onCreate"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 65
    .end local v0           #e:Ljava/lang/Exception;
    :cond_0
    const-string v1, "FlxMain"

    const-string v2, "BootstrapActivity.onCreate existing Flixster instance detected"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 88
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 89
    const-string v0, "FlxMain"

    const-string v1, "BootstrapActivity.onDestroy"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    .line 71
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 72
    const-string v4, "FlxMain"

    const-string v5, "BootstrapActivity.onResume"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v2

    .line 74
    .local v2, isHcTablet:Z
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v1

    .line 75
    .local v1, isGtv:Z
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v4, "Bootstrapping "

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 76
    if-eqz v2, :cond_0

    const-string v4, "Honeycomb tablet version"

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 75
    invoke-static {v5, v4}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-static {p0}, Lcom/flixster/android/bootstrap/BootstrapActivity;->getMainIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 78
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/flixster/android/bootstrap/BootstrapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 79
    .local v3, originalIntent:Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 80
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 81
    invoke-static {v3, v0}, Lcom/flixster/android/widget/WidgetProvider;->propagateIntentExtras(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 82
    invoke-virtual {p0, v0}, Lcom/flixster/android/bootstrap/BootstrapActivity;->startActivity(Landroid/content/Intent;)V

    .line 83
    invoke-virtual {p0}, Lcom/flixster/android/bootstrap/BootstrapActivity;->finish()V

    .line 84
    return-void

    .line 76
    .end local v0           #intent:Landroid/content/Intent;
    .end local v3           #originalIntent:Landroid/content/Intent;
    :cond_0
    if-eqz v1, :cond_1

    const-string v4, "Google TV version"

    goto :goto_0

    :cond_1
    const-string v4, "phone version"

    goto :goto_0
.end method
