.class public Lcom/flixster/android/bootstrap/Startup;
.super Ljava/lang/Object;
.source "Startup.java"


# static fields
.field private static final INSTANCE:Lcom/flixster/android/bootstrap/Startup;


# instance fields
.field private isFinished:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/flixster/android/bootstrap/Startup;

    invoke-direct {v0}, Lcom/flixster/android/bootstrap/Startup;-><init>()V

    sput-object v0, Lcom/flixster/android/bootstrap/Startup;->INSTANCE:Lcom/flixster/android/bootstrap/Startup;

    .line 17
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method private static extendFbAccessToken(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 80
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->isPlatformFacebook()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "FlxMain"

    const-string v1, "Startup.extendFbAccessToken"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/facebook/android/Facebook;->extendAccessTokenIfNeeded(Landroid/content/Context;Lcom/facebook/android/Facebook$ServiceListener;)Z

    .line 84
    :cond_0
    return-void
.end method

.method public static instance()Lcom/flixster/android/bootstrap/Startup;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/flixster/android/bootstrap/Startup;->INSTANCE:Lcom/flixster/android/bootstrap/Startup;

    return-object v0
.end method

.method private static onGtvStartup(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 66
    const-string v0, "FlxMain"

    const-string v1, "Startup.onGtvStartup"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lcom/flixster/android/bootstrap/Startup;->refreshUserSession()V

    .line 68
    invoke-static {}, Lcom/flixster/android/bootstrap/Startup;->registerDevice()V

    .line 69
    invoke-static {p0}, Lcom/flixster/android/bootstrap/Startup;->extendFbAccessToken(Landroid/app/Activity;)V

    .line 70
    return-void
.end method

.method private static onHcTabletStartup(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 59
    const-string v0, "FlxMain"

    const-string v1, "Startup.onHcTabletStartup"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-static {}, Lcom/flixster/android/bootstrap/Startup;->refreshUserSession()V

    .line 61
    invoke-static {}, Lcom/flixster/android/bootstrap/Startup;->registerDevice()V

    .line 62
    invoke-static {p0}, Lcom/flixster/android/bootstrap/Startup;->extendFbAccessToken(Landroid/app/Activity;)V

    .line 63
    return-void
.end method

.method private static onPhoneStartup(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 52
    const-string v0, "FlxMain"

    const-string v1, "Startup.onPhoneStartup"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-static {}, Lcom/flixster/android/bootstrap/Startup;->refreshUserSession()V

    .line 54
    invoke-static {}, Lcom/flixster/android/bootstrap/Startup;->registerDevice()V

    .line 55
    invoke-static {p0}, Lcom/flixster/android/bootstrap/Startup;->extendFbAccessToken(Landroid/app/Activity;)V

    .line 56
    return-void
.end method

.method private static refreshUserSession()V
    .locals 2

    .prologue
    .line 75
    const-string v0, "FlxMain"

    const-string v1, "Startup.refreshUserSession"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->refreshUserSession()V

    .line 77
    return-void
.end method

.method private static registerDevice()V
    .locals 0

    .prologue
    .line 88
    invoke-static {}, Lnet/flixster/android/data/MiscDao;->registerDevice()V

    .line 89
    return-void
.end method


# virtual methods
.method public onStartup(Landroid/app/Activity;)V
    .locals 3
    .parameter "activity"

    .prologue
    .line 30
    iget-boolean v2, p0, Lcom/flixster/android/bootstrap/Startup;->isFinished:Z

    if-eqz v2, :cond_0

    .line 43
    :goto_0
    return-void

    .line 33
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/flixster/android/bootstrap/Startup;->isFinished:Z

    .line 34
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v0

    .line 35
    .local v0, isGtv:Z
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v1

    .line 36
    .local v1, isHcTablet:Z
    if-eqz v0, :cond_1

    .line 37
    invoke-static {p1}, Lcom/flixster/android/bootstrap/Startup;->onGtvStartup(Landroid/app/Activity;)V

    goto :goto_0

    .line 38
    :cond_1
    if-eqz v1, :cond_2

    .line 39
    invoke-static {p1}, Lcom/flixster/android/bootstrap/Startup;->onHcTabletStartup(Landroid/app/Activity;)V

    goto :goto_0

    .line 41
    :cond_2
    invoke-static {p1}, Lcom/flixster/android/bootstrap/Startup;->onPhoneStartup(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flixster/android/bootstrap/Startup;->isFinished:Z

    .line 47
    return-void
.end method
