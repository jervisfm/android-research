.class public Lcom/flixster/android/data/AccountFacade;
.super Ljava/lang/Object;
.source "AccountFacade.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fetchSocialUserId(Landroid/os/Handler;Landroid/os/Handler;)Ljava/lang/String;
    .locals 3
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 32
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUserId()Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, id:Ljava/lang/String;
    if-nez v0, :cond_0

    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/data/AccountManager;->hasUserCredential()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    const-string v1, "FlxMain"

    const-string v2, "AccountFacade.fetchSocialUserId fetch required"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-static {p0, p1}, Lnet/flixster/android/data/ProfileDao;->fetchUser(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 37
    :cond_0
    return-object v0
.end method

.method public static getNetflixUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSocialUser()Lnet/flixster/android/model/User;
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v0

    return-object v0
.end method

.method public static getSocialUserId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v0

    .line 27
    .local v0, user:Lnet/flixster/android/model/User;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    goto :goto_0
.end method
