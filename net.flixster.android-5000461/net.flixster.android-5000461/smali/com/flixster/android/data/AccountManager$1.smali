.class Lcom/flixster/android/data/AccountManager$1;
.super Ljava/lang/Object;
.source "AccountManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/data/AccountManager;->restoreUserSession()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/data/AccountManager;


# direct methods
.method constructor <init>(Lcom/flixster/android/data/AccountManager;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/data/AccountManager$1;->this$0:Lcom/flixster/android/data/AccountManager;

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 124
    const/4 v1, 0x0

    .line 126
    .local v1, user:Lnet/flixster/android/model/User;
    :try_start_0
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 136
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    .line 137
    const-string v2, "FBK"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 138
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/facebook/login/restore"

    const-string v4, "Facebook Session Restore"

    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_1
    :goto_1
    const-string v2, "FlxMain"

    const-string v3, "AccountManager.restoreUserSession successful"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_2
    return-void

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    const-string v3, "AccountManager.restoreUserSession failed"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 131
    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 132
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->setUserSessionExpired()V

    .line 133
    iget-object v2, p0, Lcom/flixster/android/data/AccountManager$1;->this$0:Lcom/flixster/android/data/AccountManager;

    invoke-virtual {v2}, Lcom/flixster/android/data/AccountManager;->clearUserCredential()V

    goto :goto_0

    .line 139
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_3
    const-string v2, "FLX"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 140
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/flixster/login/restore"

    const-string v4, "Flixster Session Restore"

    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
