.class Lcom/flixster/android/data/AccountManager$2;
.super Ljava/lang/Object;
.source "AccountManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/data/AccountManager;->loginToFlixster(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/data/AccountManager;

.field private final synthetic val$email:Ljava/lang/String;

.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$password:Ljava/lang/String;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/flixster/android/data/AccountManager;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/data/AccountManager$2;->this$0:Lcom/flixster/android/data/AccountManager;

    iput-object p2, p0, Lcom/flixster/android/data/AccountManager$2;->val$email:Ljava/lang/String;

    iput-object p3, p0, Lcom/flixster/android/data/AccountManager$2;->val$password:Ljava/lang/String;

    iput-object p4, p0, Lcom/flixster/android/data/AccountManager$2;->val$successHandler:Landroid/os/Handler;

    iput-object p5, p0, Lcom/flixster/android/data/AccountManager$2;->val$errorHandler:Landroid/os/Handler;

    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 223
    :try_start_0
    iget-object v2, p0, Lcom/flixster/android/data/AccountManager$2;->val$email:Ljava/lang/String;

    iget-object v3, p0, Lcom/flixster/android/data/AccountManager$2;->val$password:Ljava/lang/String;

    const-string v4, "FLX"

    invoke-static {v2, v3, v4}, Lnet/flixster/android/data/ProfileDao;->loginUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/User;

    move-result-object v1

    .line 224
    .local v1, user:Lnet/flixster/android/model/User;
    if-eqz v1, :cond_0

    .line 225
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;

    .line 226
    iget-object v2, p0, Lcom/flixster/android/data/AccountManager$2;->val$successHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 235
    .end local v1           #user:Lnet/flixster/android/model/User;
    :goto_0
    return-void

    .line 228
    .restart local v1       #user:Lnet/flixster/android/model/User;
    :cond_0
    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    .line 229
    const-string v3, "The email or password you entered is invalid."

    .line 228
    invoke-static {v2, v3}, Lnet/flixster/android/data/DaoException;->create(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v0

    .line 230
    .local v0, de:Lnet/flixster/android/data/DaoException;
    iget-object v2, p0, Lcom/flixster/android/data/AccountManager$2;->val$errorHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v3, v4, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 232
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    .end local v1           #user:Lnet/flixster/android/model/User;
    :catch_0
    move-exception v0

    .line 233
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    iget-object v2, p0, Lcom/flixster/android/data/AccountManager$2;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v6, v5, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
