.class public Lcom/flixster/android/data/AccountManager$FbLogoutListener;
.super Ljava/lang/Object;
.source "AccountManager.java"

# interfaces
.implements Lcom/facebook/android/SessionEvents$LogoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/data/AccountManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FbLogoutListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLogoutBegin()V
    .locals 2

    .prologue
    .line 344
    const-string v0, "FlxMain"

    const-string v1, "AccountManager.FbLogoutListener.onLogoutBegin"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    return-void
.end method

.method public onLogoutFinish()V
    .locals 2

    .prologue
    .line 348
    const-string v0, "FlxMain"

    const-string v1, "AccountManager.FbLogoutListener.onLogoutFinish"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    return-void
.end method
