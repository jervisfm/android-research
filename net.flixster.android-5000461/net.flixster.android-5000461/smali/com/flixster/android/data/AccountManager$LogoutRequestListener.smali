.class Lcom/flixster/android/data/AccountManager$LogoutRequestListener;
.super Lcom/facebook/android/BaseRequestListener;
.source "AccountManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/data/AccountManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LogoutRequestListener"
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .parameter "activity"

    .prologue
    .line 290
    invoke-direct {p0}, Lcom/facebook/android/BaseRequestListener;-><init>()V

    .line 291
    iput-object p1, p0, Lcom/flixster/android/data/AccountManager$LogoutRequestListener;->activity:Landroid/app/Activity;

    .line 292
    return-void
.end method

.method synthetic constructor <init>(Landroid/app/Activity;Lcom/flixster/android/data/AccountManager$LogoutRequestListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 290
    invoke-direct {p0, p1}, Lcom/flixster/android/data/AccountManager$LogoutRequestListener;-><init>(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .parameter "response"
    .parameter "state"

    .prologue
    .line 295
    iget-object v0, p0, Lcom/flixster/android/data/AccountManager$LogoutRequestListener;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flixster/android/data/AccountManager$LogoutRequestListener;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 298
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/data/AccountManager$LogoutRequestListener;->activity:Landroid/app/Activity;

    new-instance v1, Lcom/flixster/android/data/AccountManager$LogoutRequestListener$1;

    invoke-direct {v1, p0}, Lcom/flixster/android/data/AccountManager$LogoutRequestListener$1;-><init>(Lcom/flixster/android/data/AccountManager$LogoutRequestListener;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
