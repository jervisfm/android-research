.class public Lcom/flixster/android/data/AccountManager;
.super Ljava/lang/Object;
.source "AccountManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/data/AccountManager$FbAuthListener;,
        Lcom/flixster/android/data/AccountManager$FbLogoutListener;,
        Lcom/flixster/android/data/AccountManager$LogoutRequestListener;
    }
.end annotation


# static fields
.field private static final INSTANCE:Lcom/flixster/android/data/AccountManager; = null

.field public static final PLATFORM_FBK:Ljava/lang/String; = "FBK"

.field public static final PLATFORM_FLX:Ljava/lang/String; = "FLX"


# instance fields
.field private skipRefreshUserSession:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/flixster/android/data/AccountManager;

    invoke-direct {v0}, Lcom/flixster/android/data/AccountManager;-><init>()V

    sput-object v0, Lcom/flixster/android/data/AccountManager;->INSTANCE:Lcom/flixster/android/data/AccountManager;

    .line 33
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method

.method private static clearCookies()V
    .locals 3

    .prologue
    .line 279
    const-string v1, "FlxMain"

    const-string v2, "AccountManager.clearCookies clearing cookies"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :try_start_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/android/Util;->clearCookies(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    .local v0, e:Ljava/lang/Exception;
    :goto_0
    return-void

    .line 282
    .end local v0           #e:Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 283
    .restart local v0       #e:Ljava/lang/Exception;
    const-string v1, "FlxMain"

    const-string v2, "AccountManager.clearCookies"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static instance()Lcom/flixster/android/data/AccountManager;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/flixster/android/data/AccountManager;->INSTANCE:Lcom/flixster/android/data/AccountManager;

    return-object v0
.end method

.method private static logFbCredential(Lcom/facebook/android/Facebook;)V
    .locals 8
    .parameter "fb"

    .prologue
    const-wide/16 v6, 0x0

    .line 75
    invoke-virtual {p0}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v2

    .line 76
    .local v2, isValid:Z
    invoke-virtual {p0}, Lcom/facebook/android/Facebook;->getAccessExpires()J

    move-result-wide v0

    .line 77
    .local v0, expiration:J
    if-eqz v2, :cond_1

    .line 78
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v3, "AccountManager.logFbCredential valid expires "

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 79
    cmp-long v3, v0, v6

    if-nez v3, :cond_0

    const-string v3, "0"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 78
    invoke-static {v4, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :goto_1
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AccountManager.logFbCredential fb username: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFacebookUsername()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->si(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AccountManager.logFbCredential flx id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->si(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AccountManager.logFbCredential fb id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFacebookId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->si(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AccountManager.logFbCredential fb token: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->si(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    return-void

    .line 79
    :cond_0
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    goto :goto_0

    .line 81
    :cond_1
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v3, "AccountManager.logFbSession invalid expires "

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 82
    cmp-long v3, v0, v6

    if-nez v3, :cond_2

    const-string v3, "0"

    :goto_2
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 81
    invoke-static {v4, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 82
    :cond_2
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    goto :goto_2
.end method

.method private static logFlxCredential()V
    .locals 3

    .prologue
    .line 91
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountManager.logFlxCredential flx username: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterUsername()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->si(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountManager.logFlxCredential flx id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->si(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountManager.logFlxCredential flx token: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterSessionKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->si(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method private onFbLogout()V
    .locals 1

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/flixster/android/data/AccountManager;->clearUserCredential()V

    .line 274
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->resetUser()V

    .line 275
    const/4 v0, 0x0

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setMigratedToRights(Z)V

    .line 276
    return-void
.end method


# virtual methods
.method public clearUserCredential()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setFlixsterId(Ljava/lang/String;)V

    .line 103
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/android/SessionStore;->clear(Landroid/content/Context;)V

    .line 104
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setFacebookUsername(Ljava/lang/String;)V

    .line 105
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setFacebookId(Ljava/lang/String;)V

    .line 108
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setFlixsterSessionKey(Ljava/lang/String;)V

    .line 109
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setFlixsterUsername(Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method public createNewUserWithFlxCredential(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "authUser"
    .parameter "authToken"

    .prologue
    .line 323
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->resetUser()V

    .line 324
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->createNewUser()V

    .line 325
    const-string v0, "FLX"

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setPlatform(Ljava/lang/String;)V

    .line 326
    invoke-static {p2}, Lnet/flixster/android/FlixsterApplication;->setFlixsterSessionKey(Ljava/lang/String;)V

    .line 327
    invoke-static {p1}, Lnet/flixster/android/FlixsterApplication;->setFlixsterId(Ljava/lang/String;)V

    .line 328
    const-string v0, "Flixster user"

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setFlixsterUsername(Ljava/lang/String;)V

    .line 330
    return-void
.end method

.method public fbLogout(Landroid/app/Activity;)V
    .locals 5
    .parameter "activity"

    .prologue
    .line 261
    const-string v2, "FlxMain"

    const-string v3, "AccountManager.fbLogout"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-direct {p0}, Lcom/flixster/android/data/AccountManager;->onFbLogout()V

    .line 263
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    .line 264
    .local v1, fb:Lcom/facebook/android/Facebook;
    invoke-virtual {v1}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 265
    invoke-static {}, Lcom/facebook/android/SessionEvents;->onLogoutBegin()V

    .line 266
    new-instance v0, Lcom/facebook/android/AsyncFacebookRunner;

    invoke-direct {v0, v1}, Lcom/facebook/android/AsyncFacebookRunner;-><init>(Lcom/facebook/android/Facebook;)V

    .line 268
    .local v0, asyncRunner:Lcom/facebook/android/AsyncFacebookRunner;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/flixster/android/data/AccountManager$LogoutRequestListener;

    const/4 v4, 0x0

    invoke-direct {v3, p1, v4}, Lcom/flixster/android/data/AccountManager$LogoutRequestListener;-><init>(Landroid/app/Activity;Lcom/flixster/android/data/AccountManager$LogoutRequestListener;)V

    invoke-virtual {v0, v2, v3}, Lcom/facebook/android/AsyncFacebookRunner;->logout(Landroid/content/Context;Lcom/facebook/android/AsyncFacebookRunner$RequestListener;)V

    .line 270
    .end local v0           #asyncRunner:Lcom/facebook/android/AsyncFacebookRunner;
    :cond_0
    return-void
.end method

.method public getUser()Lnet/flixster/android/model/User;
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->getUser()Lnet/flixster/android/model/User;

    move-result-object v0

    return-object v0
.end method

.method public hasFacebookUserSession()Z
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/data/AccountManager;->isPlatformFacebook()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUserCredential()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, platform:Ljava/lang/String;
    const-string v3, "FBK"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 56
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFacebookId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 57
    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v3}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 56
    goto :goto_0

    .line 58
    :cond_2
    const-string v3, "FLX"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 59
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterSessionKey()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    move v1, v2

    .line 61
    goto :goto_0
.end method

.method public hasUserSession()Z
    .locals 1

    .prologue
    .line 184
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->getUser()Lnet/flixster/android/model/User;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlatformFacebook()Z
    .locals 2

    .prologue
    .line 202
    const-string v0, "FBK"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isPlatformFlixster()Z
    .locals 2

    .prologue
    .line 207
    const-string v0, "FLX"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public logUserCredential()V
    .locals 2

    .prologue
    .line 66
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, platform:Ljava/lang/String;
    const-string v1, "FBK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-static {v1}, Lcom/flixster/android/data/AccountManager;->logFbCredential(Lcom/facebook/android/Facebook;)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    const-string v1, "FLX"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->logFlxCredential()V

    goto :goto_0
.end method

.method public loginToFacebook(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter "username"
    .parameter "password"
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 242
    return-void
.end method

.method public loginToFlixster(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 8
    .parameter "email"
    .parameter "password"
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 213
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->NETWORK:Lnet/flixster/android/data/DaoException$Type;

    .line 215
    const-string v1, "The device has no connection to the Internet. Please verify the settings."

    .line 214
    invoke-static {v0, v1}, Lnet/flixster/android/data/DaoException;->create(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    .line 216
    .local v6, de:Lnet/flixster/android/data/DaoException;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 237
    .end local v6           #de:Lnet/flixster/android/data/DaoException;
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v7

    new-instance v0, Lcom/flixster/android/data/AccountManager$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/flixster/android/data/AccountManager$2;-><init>(Lcom/flixster/android/data/AccountManager;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v7, v0}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onFbLogin(Lcom/facebook/android/Facebook;Ljava/lang/String;)V
    .locals 1
    .parameter "fb"
    .parameter "id"

    .prologue
    .line 247
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/android/SessionStore;->save(Lcom/facebook/android/Facebook;Landroid/content/Context;)Z

    .line 248
    const-string v0, "FBK"

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setPlatform(Ljava/lang/String;)V

    .line 249
    invoke-static {p2}, Lnet/flixster/android/FlixsterApplication;->setFacebookId(Ljava/lang/String;)V

    .line 250
    return-void
.end method

.method public onFbLogin(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "name"
    .parameter "flixsterId"

    .prologue
    .line 253
    invoke-static {p1}, Lnet/flixster/android/FlixsterApplication;->setFacebookUsername(Ljava/lang/String;)V

    .line 254
    invoke-static {p2}, Lnet/flixster/android/FlixsterApplication;->setFlixsterId(Ljava/lang/String;)V

    .line 255
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-static {v0}, Lcom/flixster/android/data/AccountManager;->logFbCredential(Lcom/facebook/android/Facebook;)V

    .line 256
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 257
    return-void
.end method

.method public onFlxLogin()V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method public onFlxLogout()V
    .locals 1

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/flixster/android/data/AccountManager;->clearUserCredential()V

    .line 316
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->resetUser()V

    .line 317
    const/4 v0, 0x0

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setMigratedToRights(Z)V

    .line 318
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->clearCookies()V

    .line 319
    return-void
.end method

.method public refreshUserSession()V
    .locals 2

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/flixster/android/data/AccountManager;->skipRefreshUserSession:Z

    if-nez v0, :cond_0

    .line 161
    const-string v0, "FlxMain"

    const-string v1, "AccountManager.refreshUserSession necessary"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->resetUser()V

    .line 163
    invoke-virtual {p0}, Lcom/flixster/android/data/AccountManager;->restoreUserSession()V

    .line 168
    :goto_0
    return-void

    .line 165
    :cond_0
    const-string v0, "FlxMain"

    const-string v1, "AccountManager.refreshUserSession not necessary"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flixster/android/data/AccountManager;->skipRefreshUserSession:Z

    goto :goto_0
.end method

.method public restoreFbSession()V
    .locals 0

    .prologue
    .line 308
    return-void
.end method

.method public restoreUserSession()V
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/flixster/android/data/AccountManager;->hasUserCredential()Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    const-string v0, "FlxMain"

    const-string v1, "FlixsterApplication.restoreUserSession credential unavailable"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :goto_0
    return-void

    .line 120
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 121
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/data/AccountManager$1;

    invoke-direct {v1, p0}, Lcom/flixster/android/data/AccountManager$1;-><init>(Lcom/flixster/android/data/AccountManager;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setSkipRefreshUserSession()V
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/data/AccountManager;->skipRefreshUserSession:Z

    .line 172
    return-void
.end method
