.class public final enum Lcom/flixster/android/data/AccountSessionManager$Platform;
.super Ljava/lang/Enum;
.source "AccountSessionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/data/AccountSessionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Platform"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flixster/android/data/AccountSessionManager$Platform;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/flixster/android/data/AccountSessionManager$Platform;

.field public static final enum FBK:Lcom/flixster/android/data/AccountSessionManager$Platform;

.field public static final enum FLX:Lcom/flixster/android/data/AccountSessionManager$Platform;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/flixster/android/data/AccountSessionManager$Platform;

    const-string v1, "FBK"

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/data/AccountSessionManager$Platform;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/data/AccountSessionManager$Platform;->FBK:Lcom/flixster/android/data/AccountSessionManager$Platform;

    new-instance v0, Lcom/flixster/android/data/AccountSessionManager$Platform;

    const-string v1, "FLX"

    invoke-direct {v0, v1, v3}, Lcom/flixster/android/data/AccountSessionManager$Platform;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/data/AccountSessionManager$Platform;->FLX:Lcom/flixster/android/data/AccountSessionManager$Platform;

    .line 17
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/flixster/android/data/AccountSessionManager$Platform;

    sget-object v1, Lcom/flixster/android/data/AccountSessionManager$Platform;->FBK:Lcom/flixster/android/data/AccountSessionManager$Platform;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flixster/android/data/AccountSessionManager$Platform;->FLX:Lcom/flixster/android/data/AccountSessionManager$Platform;

    aput-object v1, v0, v3

    sput-object v0, Lcom/flixster/android/data/AccountSessionManager$Platform;->ENUM$VALUES:[Lcom/flixster/android/data/AccountSessionManager$Platform;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flixster/android/data/AccountSessionManager$Platform;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/flixster/android/data/AccountSessionManager$Platform;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/data/AccountSessionManager$Platform;

    return-object v0
.end method

.method public static values()[Lcom/flixster/android/data/AccountSessionManager$Platform;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/flixster/android/data/AccountSessionManager$Platform;->ENUM$VALUES:[Lcom/flixster/android/data/AccountSessionManager$Platform;

    array-length v1, v0

    new-array v2, v1, [Lcom/flixster/android/data/AccountSessionManager$Platform;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
