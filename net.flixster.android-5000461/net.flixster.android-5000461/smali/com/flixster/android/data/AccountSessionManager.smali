.class public Lcom/flixster/android/data/AccountSessionManager;
.super Ljava/lang/Object;
.source "AccountSessionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/data/AccountSessionManager$Platform;
    }
.end annotation


# static fields
.field private static final FLX_PREFS_FILE:Ljava/lang/String; = "FlixsterPrefs"

.field private static final INSTANCE:Lcom/flixster/android/data/AccountSessionManager; = null

.field private static final PREFS_FB_EXPIRES:Ljava/lang/String; = "PREFS_FB_EXPIRES"

.field private static final PREFS_FB_SESSIONKEY:Ljava/lang/String; = "PREFS_FB_SESSIONKEY"

.field private static final PREFS_FB_USERID:Ljava/lang/String; = "PREFS_FB_USERID"

.field private static final PREFS_FB_USERNAME:Ljava/lang/String; = "PREFS_FB_USERNAME"

.field private static final PREFS_FLIXSTER_ID:Ljava/lang/String; = "PREFS_FLIXSTER_ID"

.field private static final PREFS_FLIXSTER_SESSION_KEY:Ljava/lang/String; = "PREFS_FLIXSTER_SESSION_KEY"

.field private static final PREFS_FLIXSTER_USERNAME:Ljava/lang/String; = "PREFS_FLIXSTER_USERNAME"

.field private static final PREFS_PLATFORM:Ljava/lang/String; = "PREFS_PLATFORM"


# instance fields
.field private fb:Lcom/facebook/android/Facebook;

.field private fbAccessToken:Ljava/lang/String;

.field private fbExpires:J

.field private fbId:Ljava/lang/String;

.field private fbUsername:Ljava/lang/String;

.field private flxId:Ljava/lang/String;

.field private flxSessionKey:Ljava/lang/String;

.field private flxUsername:Ljava/lang/String;

.field private platform:Lcom/flixster/android/data/AccountSessionManager$Platform;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/flixster/android/data/AccountSessionManager;

    invoke-direct {v0}, Lcom/flixster/android/data/AccountSessionManager;-><init>()V

    sput-object v0, Lcom/flixster/android/data/AccountSessionManager;->INSTANCE:Lcom/flixster/android/data/AccountSessionManager;

    .line 14
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method private clearFbSession(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    const/4 v2, 0x0

    .line 120
    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbAccessToken:Ljava/lang/String;

    .line 121
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/flixster/android/data/AccountSessionManager;->fbExpires:J

    .line 122
    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbUsername:Ljava/lang/String;

    .line 123
    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbId:Ljava/lang/String;

    .line 124
    invoke-direct {p0, p1}, Lcom/flixster/android/data/AccountSessionManager;->persistFbSession(Landroid/content/Context;)V

    .line 126
    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fb:Lcom/facebook/android/Facebook;

    .line 127
    return-void
.end method

.method private clearFlxSession(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    const/4 v0, 0x0

    .line 139
    iput-object v0, p0, Lcom/flixster/android/data/AccountSessionManager;->flxUsername:Ljava/lang/String;

    .line 140
    iput-object v0, p0, Lcom/flixster/android/data/AccountSessionManager;->flxSessionKey:Ljava/lang/String;

    .line 141
    iput-object v0, p0, Lcom/flixster/android/data/AccountSessionManager;->flxId:Ljava/lang/String;

    .line 142
    invoke-direct {p0, p1}, Lcom/flixster/android/data/AccountSessionManager;->persistFlxSession(Landroid/content/Context;)V

    .line 143
    return-void
.end method

.method private clearSession(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/flixster/android/data/AccountSessionManager;->clearFbSession(Landroid/content/Context;)V

    .line 114
    invoke-direct {p0, p1}, Lcom/flixster/android/data/AccountSessionManager;->clearFlxSession(Landroid/content/Context;)V

    .line 116
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->resetUser()V

    .line 117
    return-void
.end method

.method public static instance()Lcom/flixster/android/data/AccountSessionManager;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/flixster/android/data/AccountSessionManager;->INSTANCE:Lcom/flixster/android/data/AccountSessionManager;

    return-object v0
.end method

.method private persistFbSession(Landroid/content/Context;)V
    .locals 4
    .parameter "context"

    .prologue
    .line 130
    const-string v1, "FlixsterPrefs"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 131
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v1, "PREFS_FB_USERNAME"

    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbUsername:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 132
    const-string v1, "PREFS_FB_SESSIONKEY"

    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbAccessToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 133
    const-string v1, "PREFS_FB_USERID"

    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 134
    const-string v1, "PREFS_FB_EXPIRES"

    iget-wide v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbExpires:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 135
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 136
    return-void
.end method

.method private persistFlxSession(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    .line 146
    const-string v1, "FlixsterPrefs"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 147
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v1, "PREFS_FLIXSTER_USERNAME"

    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->flxUsername:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 148
    const-string v1, "PREFS_FLIXSTER_SESSION_KEY"

    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->flxSessionKey:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 149
    const-string v1, "PREFS_FLIXSTER_ID"

    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->flxId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 150
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 151
    return-void
.end method


# virtual methods
.method public isSessionValid()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 103
    sget-object v2, Lcom/flixster/android/data/AccountSessionManager$Platform;->FBK:Lcom/flixster/android/data/AccountSessionManager$Platform;

    iget-object v3, p0, Lcom/flixster/android/data/AccountSessionManager;->platform:Lcom/flixster/android/data/AccountSessionManager$Platform;

    if-ne v2, v3, :cond_2

    .line 104
    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fb:Lcom/facebook/android/Facebook;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fb:Lcom/facebook/android/Facebook;

    invoke-virtual {v2}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 104
    goto :goto_0

    .line 105
    :cond_2
    sget-object v2, Lcom/flixster/android/data/AccountSessionManager$Platform;->FLX:Lcom/flixster/android/data/AccountSessionManager$Platform;

    iget-object v3, p0, Lcom/flixster/android/data/AccountSessionManager;->platform:Lcom/flixster/android/data/AccountSessionManager$Platform;

    if-ne v2, v3, :cond_3

    .line 106
    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->flxSessionKey:Ljava/lang/String;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 108
    goto :goto_0
.end method

.method public persistSession(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 100
    return-void
.end method

.method public restoreSession(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    const/4 v5, 0x0

    .line 56
    const-string v2, "FlixsterPrefs"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 59
    .local v1, prefs:Landroid/content/SharedPreferences;
    :try_start_0
    const-string v2, "PREFS_PLATFORM"

    sget-object v3, Lcom/flixster/android/data/AccountSessionManager$Platform;->FBK:Lcom/flixster/android/data/AccountSessionManager$Platform;

    invoke-virtual {v3}, Lcom/flixster/android/data/AccountSessionManager$Platform;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/flixster/android/data/AccountSessionManager$Platform;->valueOf(Ljava/lang/String;)Lcom/flixster/android/data/AccountSessionManager$Platform;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->platform:Lcom/flixster/android/data/AccountSessionManager$Platform;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    const-string v2, "PREFS_FB_USERNAME"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbUsername:Ljava/lang/String;

    .line 65
    const-string v2, "PREFS_FB_USERID"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbId:Ljava/lang/String;

    .line 66
    const-string v2, "PREFS_FB_SESSIONKEY"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbAccessToken:Ljava/lang/String;

    .line 67
    const-string v2, "PREFS_FB_EXPIRES"

    const-wide/16 v3, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fbExpires:J

    .line 69
    const-string v2, "PREFS_FLIXSTER_USERNAME"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->flxUsername:Ljava/lang/String;

    .line 70
    const-string v2, "PREFS_FLIXSTER_ID"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->flxId:Ljava/lang/String;

    .line 71
    const-string v2, "PREFS_FLIXSTER_SESSION_KEY"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->flxSessionKey:Ljava/lang/String;

    .line 73
    new-instance v2, Lcom/facebook/android/Facebook;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFbAppId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/android/Facebook;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fb:Lcom/facebook/android/Facebook;

    .line 74
    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fb:Lcom/facebook/android/Facebook;

    iget-object v3, p0, Lcom/flixster/android/data/AccountSessionManager;->fbAccessToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/android/Facebook;->setAccessToken(Ljava/lang/String;)V

    .line 75
    iget-object v2, p0, Lcom/flixster/android/data/AccountSessionManager;->fb:Lcom/facebook/android/Facebook;

    iget-wide v3, p0, Lcom/flixster/android/data/AccountSessionManager;->fbExpires:J

    invoke-virtual {v2, v3, v4}, Lcom/facebook/android/Facebook;->setAccessExpires(J)V

    .line 78
    invoke-virtual {p0}, Lcom/flixster/android/data/AccountSessionManager;->isSessionValid()Z

    move-result v2

    if-nez v2, :cond_0

    .line 79
    invoke-direct {p0, p1}, Lcom/flixster/android/data/AccountSessionManager;->clearSession(Landroid/content/Context;)V

    .line 81
    :cond_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, iae:Ljava/lang/IllegalArgumentException;
    iput-object v5, p0, Lcom/flixster/android/data/AccountSessionManager;->platform:Lcom/flixster/android/data/AccountSessionManager$Platform;

    goto :goto_0
.end method

.method public saveSession(Lcom/flixster/android/data/AccountSessionManager$Platform;)V
    .locals 2
    .parameter "platform"

    .prologue
    const/4 v1, 0x0

    .line 84
    iput-object p1, p0, Lcom/flixster/android/data/AccountSessionManager;->platform:Lcom/flixster/android/data/AccountSessionManager$Platform;

    .line 85
    sget-object v0, Lcom/flixster/android/data/AccountSessionManager$Platform;->FBK:Lcom/flixster/android/data/AccountSessionManager$Platform;

    if-ne v0, p1, :cond_0

    .line 86
    iput-object v1, p0, Lcom/flixster/android/data/AccountSessionManager;->fbAccessToken:Ljava/lang/String;

    .line 87
    iput-object v1, p0, Lcom/flixster/android/data/AccountSessionManager;->fbId:Ljava/lang/String;

    .line 96
    :goto_0
    return-void

    .line 93
    :cond_0
    sget-object v0, Lcom/flixster/android/data/AccountSessionManager$Platform;->FLX:Lcom/flixster/android/data/AccountSessionManager$Platform;

    goto :goto_0
.end method
