.class public Lcom/flixster/android/data/FacebookFacade;
.super Ljava/lang/Object;
.source "FacebookFacade.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static sendRequest(Lcom/facebook/android/Facebook;Ljava/lang/String;Lcom/facebook/android/Facebook$DialogListener;Landroid/content/Context;)V
    .locals 3
    .parameter "fb"
    .parameter "recipients"
    .parameter "listener"
    .parameter "context"

    .prologue
    .line 24
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 25
    .local v1, params:Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 26
    const-string v2, "to"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v2

    iget-object v0, v2, Lnet/flixster/android/model/Property;->mskFbRequestBody:Ljava/lang/String;

    .line 29
    .local v0, body:Ljava/lang/String;
    if-nez v0, :cond_1

    .line 30
    const v2, 0x7f0c00f2

    invoke-virtual {p3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 32
    :cond_1
    const-string v2, "message"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v2, "apprequests"

    invoke-virtual {p0, p3, v2, v1, p2}, Lcom/facebook/android/Facebook;->dialog(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/android/Facebook$DialogListener;)V

    .line 34
    return-void
.end method
