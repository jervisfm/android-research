.class Lcom/flixster/android/data/MovieDaoNew$1;
.super Ljava/lang/Object;
.source "MovieDaoNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/data/MovieDaoNew;->fetchBoxOffice(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$movies:Ljava/util/Collection;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/data/MovieDaoNew$1;->val$movies:Ljava/util/Collection;

    iput-object p2, p0, Lcom/flixster/android/data/MovieDaoNew$1;->val$successHandler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/flixster/android/data/MovieDaoNew$1;->val$errorHandler:Landroid/os/Handler;

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 33
    const-string v5, "FlxMain"

    const-string v6, "MovieDaoNew.fetchBoxOffice"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v2, featuredList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .local v3, otwList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 37
    .local v4, tboList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v0, aitList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    const/4 v5, 0x1

    :try_start_0
    invoke-static {v2, v3, v4, v0, v5}, Lnet/flixster/android/data/MovieDao;->fetchBoxOffice(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V

    .line 40
    iget-object v5, p0, Lcom/flixster/android/data/MovieDaoNew$1;->val$movies:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->clear()V

    .line 45
    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 46
    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 47
    iget-object v5, p0, Lcom/flixster/android/data/MovieDaoNew$1;->val$movies:Ljava/util/Collection;

    new-instance v6, Lcom/flixster/android/model/NamedList;

    const v7, 0x7f0c01b3

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v3}, Lcom/flixster/android/model/NamedList;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 48
    iget-object v5, p0, Lcom/flixster/android/data/MovieDaoNew$1;->val$successHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v1

    .line 50
    .local v1, de:Lnet/flixster/android/data/DaoException;
    const-string v5, "FlxMain"

    const-string v6, "MovieDaoNew.fetchBoxOffice"

    invoke-static {v5, v6, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    iget-object v5, p0, Lcom/flixster/android/data/MovieDaoNew$1;->val$errorHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-static {v6, v8, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
