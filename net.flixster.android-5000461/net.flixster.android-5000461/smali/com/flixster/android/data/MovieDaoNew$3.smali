.class Lcom/flixster/android/data/MovieDaoNew$3;
.super Ljava/lang/Object;
.source "MovieDaoNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/data/MovieDaoNew;->fetchDvd(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$movies:Ljava/util/Collection;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/data/MovieDaoNew$3;->val$movies:Ljava/util/Collection;

    iput-object p2, p0, Lcom/flixster/android/data/MovieDaoNew$3;->val$successHandler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/flixster/android/data/MovieDaoNew$3;->val$errorHandler:Landroid/os/Handler;

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 86
    const-string v3, "FlxMain"

    const-string v4, "MovieDaoNew.fetchDvd"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v1, featuredList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v2, regularList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    const/4 v3, 0x1

    :try_start_0
    invoke-static {v1, v2, v3}, Lnet/flixster/android/data/MovieDao;->fetchDvdNewRelease(Ljava/util/List;Ljava/util/List;Z)V

    .line 91
    iget-object v3, p0, Lcom/flixster/android/data/MovieDaoNew$3;->val$movies:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->clear()V

    .line 93
    iget-object v3, p0, Lcom/flixster/android/data/MovieDaoNew$3;->val$movies:Ljava/util/Collection;

    new-instance v4, Lcom/flixster/android/model/NamedList;

    const v5, 0x7f0c002e

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lcom/flixster/android/model/NamedList;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 94
    iget-object v3, p0, Lcom/flixster/android/data/MovieDaoNew$3;->val$successHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_0
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDaoNew.fetchDvd"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 97
    iget-object v3, p0, Lcom/flixster/android/data/MovieDaoNew$3;->val$errorHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-static {v4, v6, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
