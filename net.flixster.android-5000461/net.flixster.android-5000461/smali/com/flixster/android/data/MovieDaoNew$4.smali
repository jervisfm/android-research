.class Lcom/flixster/android/data/MovieDaoNew$4;
.super Ljava/lang/Object;
.source "MovieDaoNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/data/MovieDaoNew;->searchMovie(Ljava/lang/String;Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$movies:Ljava/util/Collection;

.field private final synthetic val$query:Ljava/lang/String;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/data/MovieDaoNew$4;->val$query:Ljava/lang/String;

    iput-object p2, p0, Lcom/flixster/android/data/MovieDaoNew$4;->val$movies:Ljava/util/Collection;

    iput-object p3, p0, Lcom/flixster/android/data/MovieDaoNew$4;->val$successHandler:Landroid/os/Handler;

    iput-object p4, p0, Lcom/flixster/android/data/MovieDaoNew$4;->val$errorHandler:Landroid/os/Handler;

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 109
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieDaoNew.searchMovie: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flixster/android/data/MovieDaoNew$4;->val$query:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 112
    .local v1, regularList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    :try_start_0
    iget-object v2, p0, Lcom/flixster/android/data/MovieDaoNew$4;->val$query:Ljava/lang/String;

    invoke-static {v2, v1}, Lnet/flixster/android/data/MovieDao;->searchMovies(Ljava/lang/String;Ljava/util/List;)V

    .line 113
    iget-object v2, p0, Lcom/flixster/android/data/MovieDaoNew$4;->val$movies:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->clear()V

    .line 114
    iget-object v2, p0, Lcom/flixster/android/data/MovieDaoNew$4;->val$movies:Ljava/util/Collection;

    new-instance v3, Lcom/flixster/android/model/NamedList;

    const v4, 0x7f0c0109

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/flixster/android/model/NamedList;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v2, p0, Lcom/flixster/android/data/MovieDaoNew$4;->val$successHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    const-string v3, "MovieDaoNew.searchMovie"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 118
    iget-object v2, p0, Lcom/flixster/android/data/MovieDaoNew$4;->val$errorHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-static {v3, v5, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
