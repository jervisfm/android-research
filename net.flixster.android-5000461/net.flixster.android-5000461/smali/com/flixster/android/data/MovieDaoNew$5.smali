.class Lcom/flixster/android/data/MovieDaoNew$5;
.super Ljava/lang/Object;
.source "MovieDaoNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/data/MovieDaoNew;->fetchMovie(JLcom/flixster/android/activity/common/DaoHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$handler:Lcom/flixster/android/activity/common/DaoHandler;

.field private final synthetic val$id:J


# direct methods
.method constructor <init>(JLcom/flixster/android/activity/common/DaoHandler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-wide p1, p0, Lcom/flixster/android/data/MovieDaoNew$5;->val$id:J

    iput-object p3, p0, Lcom/flixster/android/data/MovieDaoNew$5;->val$handler:Lcom/flixster/android/activity/common/DaoHandler;

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 135
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieDaoNew.fetchMovie "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/flixster/android/data/MovieDaoNew$5;->val$id:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :try_start_0
    iget-wide v2, p0, Lcom/flixster/android/data/MovieDaoNew$5;->val$id:J

    invoke-static {v2, v3}, Lnet/flixster/android/data/MovieDao;->getMovieDetail(J)Lnet/flixster/android/model/Movie;

    move-result-object v1

    .line 138
    .local v1, m:Lnet/flixster/android/model/Movie;
    iget-object v2, p0, Lcom/flixster/android/data/MovieDaoNew$5;->val$handler:Lcom/flixster/android/activity/common/DaoHandler;

    invoke-virtual {v2, v1}, Lcom/flixster/android/activity/common/DaoHandler;->sendSuccessMessage(Ljava/lang/Object;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    .end local v1           #m:Lnet/flixster/android/model/Movie;
    :goto_0
    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieDaoNew.fetchMovie "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/flixster/android/data/MovieDaoNew$5;->val$id:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 141
    iget-object v2, p0, Lcom/flixster/android/data/MovieDaoNew$5;->val$handler:Lcom/flixster/android/activity/common/DaoHandler;

    invoke-virtual {v2, v0}, Lcom/flixster/android/activity/common/DaoHandler;->sendExceptionMessage(Lnet/flixster/android/data/DaoException;)V

    goto :goto_0
.end method
