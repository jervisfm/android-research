.class Lcom/flixster/android/data/MovieDaoNew$6;
.super Ljava/lang/Object;
.source "MovieDaoNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/data/MovieDaoNew;->searchMovieAndActor(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/flixster/android/activity/common/DaoHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$actorResults:Ljava/util/List;

.field private final synthetic val$handler:Lcom/flixster/android/activity/common/DaoHandler;

.field private final synthetic val$movieResults:Ljava/util/List;

.field private final synthetic val$query:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/flixster/android/activity/common/DaoHandler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$query:Ljava/lang/String;

    iput-object p2, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$movieResults:Ljava/util/List;

    iput-object p3, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$actorResults:Ljava/util/List;

    iput-object p4, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$handler:Lcom/flixster/android/activity/common/DaoHandler;

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 154
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MovieDaoNew.searchMovieAndActor: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$query:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$query:Ljava/lang/String;

    iget-object v2, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$movieResults:Ljava/util/List;

    invoke-static {v1, v2}, Lnet/flixster/android/data/MovieDao;->searchMovies(Ljava/lang/String;Ljava/util/List;)V

    .line 157
    iget-object v1, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$query:Ljava/lang/String;

    iget-object v2, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$actorResults:Ljava/util/List;

    invoke-static {v1, v2}, Lnet/flixster/android/data/ActorDao;->searchActors(Ljava/lang/String;Ljava/util/List;)V

    .line 158
    iget-object v1, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$handler:Lcom/flixster/android/activity/common/DaoHandler;

    invoke-virtual {v1}, Lcom/flixster/android/activity/common/DaoHandler;->sendSuccessMessage()V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    return-void

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDaoNew.searchMovieAndActor"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 161
    iget-object v1, p0, Lcom/flixster/android/data/MovieDaoNew$6;->val$handler:Lcom/flixster/android/activity/common/DaoHandler;

    invoke-virtual {v1, v0}, Lcom/flixster/android/activity/common/DaoHandler;->sendExceptionMessage(Lnet/flixster/android/data/DaoException;)V

    goto :goto_0
.end method
