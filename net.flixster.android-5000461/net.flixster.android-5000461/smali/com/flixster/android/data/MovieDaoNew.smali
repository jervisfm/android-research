.class public Lcom/flixster/android/data/MovieDaoNew;
.super Ljava/lang/Object;
.source "MovieDaoNew.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fetchBoxOffice(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter
    .parameter "successHandler"
    .parameter "errorHandler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/model/NamedList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;>;",
            "Landroid/os/Handler;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, movies:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/Movie;>;>;"
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/data/MovieDaoNew$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/flixster/android/data/MovieDaoNew$1;-><init>(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 55
    return-void
.end method

.method public static fetchDvd(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter
    .parameter "successHandler"
    .parameter "errorHandler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/model/NamedList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;>;",
            "Landroid/os/Handler;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 83
    .local p0, movies:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/Movie;>;>;"
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/data/MovieDaoNew$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/flixster/android/data/MovieDaoNew$3;-><init>(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 101
    return-void
.end method

.method public static fetchMovie(JLcom/flixster/android/activity/common/DaoHandler;)V
    .locals 2
    .parameter "id"
    .parameter "handler"

    .prologue
    .line 131
    invoke-virtual {p2}, Lcom/flixster/android/activity/common/DaoHandler;->activate()Lcom/flixster/android/activity/common/DaoHandler;

    .line 132
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/data/MovieDaoNew$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/flixster/android/data/MovieDaoNew$5;-><init>(JLcom/flixster/android/activity/common/DaoHandler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 145
    return-void
.end method

.method public static fetchUpcoming(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter
    .parameter "successHandler"
    .parameter "errorHandler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/model/NamedList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;>;",
            "Landroid/os/Handler;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    .local p0, movies:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/Movie;>;>;"
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/data/MovieDaoNew$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/flixster/android/data/MovieDaoNew$2;-><init>(Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 78
    return-void
.end method

.method public static getMovie(J)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter "id"

    .prologue
    .line 126
    invoke-static {p0, p1}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v0

    return-object v0
.end method

.method public static searchMovie(Ljava/lang/String;Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "query"
    .parameter
    .parameter "successHandler"
    .parameter "errorHandler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/model/NamedList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;>;",
            "Landroid/os/Handler;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, movies:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/flixster/android/model/NamedList<Lnet/flixster/android/model/Movie;>;>;"
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/data/MovieDaoNew$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/flixster/android/data/MovieDaoNew$4;-><init>(Ljava/lang/String;Ljava/util/Collection;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 122
    return-void
.end method

.method public static searchMovieAndActor(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/flixster/android/activity/common/DaoHandler;)V
    .locals 2
    .parameter "query"
    .parameter
    .parameter
    .parameter "handler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;",
            "Lcom/flixster/android/activity/common/DaoHandler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 150
    .local p1, movieResults:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .local p2, actorResults:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Actor;>;"
    invoke-virtual {p3}, Lcom/flixster/android/activity/common/DaoHandler;->activate()Lcom/flixster/android/activity/common/DaoHandler;

    .line 151
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/data/MovieDaoNew$6;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/flixster/android/data/MovieDaoNew$6;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/flixster/android/activity/common/DaoHandler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 165
    return-void
.end method
