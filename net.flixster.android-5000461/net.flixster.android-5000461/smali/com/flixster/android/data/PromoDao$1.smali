.class Lcom/flixster/android/data/PromoDao$1;
.super Ljava/lang/Object;
.source "PromoDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/data/PromoDao;->get(Ljava/util/List;Ljava/util/List;Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$featuredItems:Ljava/util/List;

.field private final synthetic val$hotTodayItems:Ljava/util/List;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;Ljava/util/List;Ljava/util/List;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/data/PromoDao$1;->val$errorHandler:Landroid/os/Handler;

    iput-object p2, p0, Lcom/flixster/android/data/PromoDao$1;->val$featuredItems:Ljava/util/List;

    iput-object p3, p0, Lcom/flixster/android/data/PromoDao$1;->val$hotTodayItems:Ljava/util/List;

    iput-object p4, p0, Lcom/flixster/android/data/PromoDao$1;->val$successHandler:Landroid/os/Handler;

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 31
    const/4 v5, 0x0

    .line 34
    .local v5, response:Ljava/lang/String;
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->promos()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 45
    :try_start_1
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v5}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 46
    .local v4, promos:Lorg/json/JSONArray;
    iget-object v6, p0, Lcom/flixster/android/data/PromoDao$1;->val$featuredItems:Ljava/util/List;

    iget-object v7, p0, Lcom/flixster/android/data/PromoDao$1;->val$hotTodayItems:Ljava/util/List;

    invoke-static {v4, v6, v7}, Lcom/flixster/android/model/PromoItem;->parseArray(Lorg/json/JSONArray;Ljava/util/List;Ljava/util/List;)V

    .line 47
    iget-object v6, p0, Lcom/flixster/android/data/PromoDao$1;->val$successHandler:Landroid/os/Handler;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 53
    .end local v4           #promos:Lorg/json/JSONArray;
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v3

    .line 36
    .local v3, mue:Ljava/net/MalformedURLException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 37
    .end local v3           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v1

    .line 38
    .local v1, ie:Ljava/io/IOException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v0

    .line 39
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v6, "FlxMain"

    const-string v7, "PromoDao.get DaoException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 40
    iget-object v6, p0, Lcom/flixster/android/data/PromoDao$1;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v10, v9, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 48
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    .end local v1           #ie:Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 49
    .local v2, je:Lorg/json/JSONException;
    invoke-static {v2}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v0

    .line 50
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    const-string v6, "FlxMain"

    const-string v7, "PromoDao.get DaoException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    iget-object v6, p0, Lcom/flixster/android/data/PromoDao$1;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v10, v9, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
