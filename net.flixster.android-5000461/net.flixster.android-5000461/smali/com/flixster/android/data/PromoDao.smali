.class public Lcom/flixster/android/data/PromoDao;
.super Ljava/lang/Object;
.source "PromoDao.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Ljava/util/List;Ljava/util/List;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter
    .parameter
    .parameter "successHandler"
    .parameter "errorHandler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/model/CarouselItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/model/PromoItem;",
            ">;",
            "Landroid/os/Handler;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, featuredItems:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/model/CarouselItem;>;"
    .local p1, hotTodayItems:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/model/PromoItem;>;"
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/data/PromoDao$1;

    invoke-direct {v1, p3, p0, p1, p2}, Lcom/flixster/android/data/PromoDao$1;-><init>(Landroid/os/Handler;Ljava/util/List;Ljava/util/List;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 55
    return-void
.end method
