.class public Lcom/flixster/android/drm/DownloadLock;
.super Lcom/flixster/android/storage/ExternalPreferenceFile;
.source "DownloadLock.java"


# static fields
.field public static final EXTENSION:Ljava/lang/String; = ".dlck"


# instance fields
.field private assetId:J

.field private downloadId:J

.field private exists:Z

.field private rightId:J

.field private rightType:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(J)V
    .locals 3
    .parameter "rightId"

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/flixster/android/storage/ExternalPreferenceFile;-><init>()V

    .line 22
    iput-wide p1, p0, Lcom/flixster/android/drm/DownloadLock;->rightId:J

    .line 23
    invoke-virtual {p0}, Lcom/flixster/android/drm/DownloadLock;->read()Z

    move-result v0

    iput-boolean v0, p0, Lcom/flixster/android/drm/DownloadLock;->exists:Z

    .line 24
    iget-boolean v0, p0, Lcom/flixster/android/drm/DownloadLock;->exists:Z

    if-eqz v0, :cond_0

    .line 25
    const-string v0, "FlxDrm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DownloadLock for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    :goto_0
    return-void

    .line 27
    :cond_0
    const-string v0, "FlxDrm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DownloadLock for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static deNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "s"

    .prologue
    .line 111
    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string p0, "."

    .end local p0
    :cond_1
    return-object p0
.end method


# virtual methods
.method protected createFileContent()Ljava/lang/String;
    .locals 3

    .prologue
    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/flixster/android/drm/DownloadLock;->rightId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/flixster/android/drm/DownloadLock;->downloadId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/drm/DownloadLock;->title:Ljava/lang/String;

    invoke-static {v1}, Lcom/flixster/android/drm/DownloadLock;->deNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/drm/DownloadLock;->rightType:Ljava/lang/String;

    invoke-static {v1}, Lcom/flixster/android/drm/DownloadLock;->deNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 92
    iget-wide v1, p0, Lcom/flixster/android/drm/DownloadLock;->assetId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected createFileName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/flixster/android/drm/DownloadLock;->rightId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".dlck"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected createFileSubDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const-string v0, "wv/"

    return-object v0
.end method

.method public exists()Z
    .locals 4

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/flixster/android/drm/DownloadLock;->exists:Z

    if-nez v0, :cond_0

    .line 34
    const-string v0, "FlxDrm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DownloadLock for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/flixster/android/drm/DownloadLock;->rightId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    :cond_0
    iget-boolean v0, p0, Lcom/flixster/android/drm/DownloadLock;->exists:Z

    return v0
.end method

.method public getAssetId()J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/flixster/android/drm/DownloadLock;->assetId:J

    return-wide v0
.end method

.method public getDownloadId()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/flixster/android/drm/DownloadLock;->downloadId:J

    return-wide v0
.end method

.method public getRightId()J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/flixster/android/drm/DownloadLock;->rightId:J

    return-wide v0
.end method

.method public getRightType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadLock;->rightType:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadLock;->rightType:Ljava/lang/String;

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadLock;->title:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadLock;->title:Ljava/lang/String;

    goto :goto_0
.end method

.method protected parseFileContent(Ljava/lang/String;)Z
    .locals 8
    .parameter "content"

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 97
    invoke-virtual {p0, p1}, Lcom/flixster/android/drm/DownloadLock;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 98
    .local v0, params:[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v4, v0

    const/4 v5, 0x5

    if-ne v4, v5, :cond_0

    .line 99
    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/flixster/android/drm/DownloadLock;->rightId:J

    .line 100
    aget-object v1, v0, v3

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/flixster/android/drm/DownloadLock;->downloadId:J

    .line 101
    const-string v1, "."

    aget-object v4, v0, v6

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v2

    :goto_0
    iput-object v1, p0, Lcom/flixster/android/drm/DownloadLock;->title:Ljava/lang/String;

    .line 102
    const-string v1, "."

    aget-object v4, v0, v7

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_1
    iput-object v2, p0, Lcom/flixster/android/drm/DownloadLock;->rightType:Ljava/lang/String;

    .line 103
    const/4 v1, 0x4

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/flixster/android/drm/DownloadLock;->assetId:J

    move v1, v3

    .line 106
    :cond_0
    return v1

    .line 101
    :cond_1
    aget-object v1, v0, v6

    goto :goto_0

    .line 102
    :cond_2
    aget-object v2, v0, v7

    goto :goto_1
.end method

.method public setAssetId(J)V
    .locals 0
    .parameter "l"

    .prologue
    .line 76
    iput-wide p1, p0, Lcom/flixster/android/drm/DownloadLock;->assetId:J

    .line 77
    return-void
.end method

.method public setDownloadId(J)V
    .locals 0
    .parameter "l"

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/flixster/android/drm/DownloadLock;->downloadId:J

    .line 65
    return-void
.end method

.method public setRightType(Ljava/lang/String;)V
    .locals 0
    .parameter "s"

    .prologue
    .line 72
    iput-object p1, p0, Lcom/flixster/android/drm/DownloadLock;->rightType:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .parameter "s"

    .prologue
    .line 68
    iput-object p1, p0, Lcom/flixster/android/drm/DownloadLock;->title:Ljava/lang/String;

    .line 69
    return-void
.end method
