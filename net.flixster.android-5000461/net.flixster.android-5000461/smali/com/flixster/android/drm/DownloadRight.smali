.class public Lcom/flixster/android/drm/DownloadRight;
.super Lcom/flixster/android/storage/ExternalPreferenceFile;
.source "DownloadRight.java"


# static fields
.field public static final EXTENSION:Ljava/lang/String; = ".rght"


# instance fields
.field private captionsUri:Ljava/lang/String;

.field private deviceId:Ljava/lang/String;

.field private downloadUri:Ljava/lang/String;

.field private exists:Z

.field private licenseProxy:Ljava/lang/String;

.field private queueId:Ljava/lang/String;

.field private final rightId:J


# direct methods
.method public constructor <init>(J)V
    .locals 3
    .parameter "rightId"

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/flixster/android/storage/ExternalPreferenceFile;-><init>()V

    .line 19
    iput-wide p1, p0, Lcom/flixster/android/drm/DownloadRight;->rightId:J

    .line 20
    invoke-virtual {p0}, Lcom/flixster/android/drm/DownloadRight;->read()Z

    move-result v0

    iput-boolean v0, p0, Lcom/flixster/android/drm/DownloadRight;->exists:Z

    .line 21
    iget-boolean v0, p0, Lcom/flixster/android/drm/DownloadRight;->exists:Z

    if-eqz v0, :cond_0

    .line 22
    const-string v0, "FlxDrm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DownloadRight for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    :goto_0
    return-void

    .line 24
    :cond_0
    const-string v0, "FlxDrm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DownloadRight for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static deNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "s"

    .prologue
    .line 117
    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string p0, "."

    .end local p0
    :cond_1
    return-object p0
.end method


# virtual methods
.method protected createFileContent()Ljava/lang/String;
    .locals 3

    .prologue
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/flixster/android/drm/DownloadRight;->rightId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/drm/DownloadRight;->downloadUri:Ljava/lang/String;

    invoke-static {v1}, Lcom/flixster/android/drm/DownloadRight;->deNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/drm/DownloadRight;->licenseProxy:Ljava/lang/String;

    invoke-static {v1}, Lcom/flixster/android/drm/DownloadRight;->deNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/flixster/android/drm/DownloadRight;->queueId:Ljava/lang/String;

    invoke-static {v1}, Lcom/flixster/android/drm/DownloadRight;->deNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/drm/DownloadRight;->deviceId:Ljava/lang/String;

    invoke-static {v1}, Lcom/flixster/android/drm/DownloadRight;->deNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/drm/DownloadRight;->captionsUri:Ljava/lang/String;

    invoke-static {v1}, Lcom/flixster/android/drm/DownloadRight;->deNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected createFileName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/flixster/android/drm/DownloadRight;->rightId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".rght"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected createFileSubDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    const-string v0, "wv/"

    return-object v0
.end method

.method public exists()Z
    .locals 4

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/flixster/android/drm/DownloadRight;->exists:Z

    if-nez v0, :cond_0

    .line 30
    const-string v0, "FlxDrm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DownloadRight for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/flixster/android/drm/DownloadRight;->rightId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    :cond_0
    iget-boolean v0, p0, Lcom/flixster/android/drm/DownloadRight;->exists:Z

    return v0
.end method

.method public getCaptionsUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadRight;->captionsUri:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadRight;->captionsUri:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadRight;->deviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadRight;->deviceId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDownloadUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadRight;->downloadUri:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadRight;->downloadUri:Ljava/lang/String;

    goto :goto_0
.end method

.method public getLicenseProxy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadRight;->licenseProxy:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadRight;->licenseProxy:Ljava/lang/String;

    goto :goto_0
.end method

.method public getQueueId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadRight;->queueId:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/flixster/android/drm/DownloadRight;->queueId:Ljava/lang/String;

    goto :goto_0
.end method

.method protected parseFileContent(Ljava/lang/String;)Z
    .locals 8
    .parameter "content"

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 103
    invoke-virtual {p0, p1}, Lcom/flixster/android/drm/DownloadRight;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, params:[Ljava/lang/String;
    if-eqz v0, :cond_5

    array-length v1, v0

    const/4 v4, 0x6

    if-ne v1, v4, :cond_5

    .line 105
    const-string v1, "."

    aget-object v4, v0, v3

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v2

    :goto_0
    iput-object v1, p0, Lcom/flixster/android/drm/DownloadRight;->downloadUri:Ljava/lang/String;

    .line 106
    const-string v1, "."

    aget-object v4, v0, v5

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v2

    :goto_1
    iput-object v1, p0, Lcom/flixster/android/drm/DownloadRight;->licenseProxy:Ljava/lang/String;

    .line 107
    const-string v1, "."

    aget-object v4, v0, v6

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    :goto_2
    iput-object v1, p0, Lcom/flixster/android/drm/DownloadRight;->queueId:Ljava/lang/String;

    .line 108
    const-string v1, "."

    aget-object v4, v0, v7

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v2

    :goto_3
    iput-object v1, p0, Lcom/flixster/android/drm/DownloadRight;->deviceId:Ljava/lang/String;

    .line 109
    const-string v1, "."

    const/4 v4, 0x5

    aget-object v4, v0, v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_4
    iput-object v2, p0, Lcom/flixster/android/drm/DownloadRight;->captionsUri:Ljava/lang/String;

    move v1, v3

    .line 112
    :goto_5
    return v1

    .line 105
    :cond_0
    aget-object v1, v0, v3

    goto :goto_0

    .line 106
    :cond_1
    aget-object v1, v0, v5

    goto :goto_1

    .line 107
    :cond_2
    aget-object v1, v0, v6

    goto :goto_2

    .line 108
    :cond_3
    aget-object v1, v0, v7

    goto :goto_3

    .line 109
    :cond_4
    const/4 v1, 0x5

    aget-object v2, v0, v1

    goto :goto_4

    .line 112
    :cond_5
    const/4 v1, 0x0

    goto :goto_5
.end method

.method public setCaptionsUri(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;
    .locals 0
    .parameter "s"

    .prologue
    .line 81
    iput-object p1, p0, Lcom/flixster/android/drm/DownloadRight;->captionsUri:Ljava/lang/String;

    .line 82
    return-object p0
.end method

.method public setDeviceId(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;
    .locals 0
    .parameter "s"

    .prologue
    .line 76
    iput-object p1, p0, Lcom/flixster/android/drm/DownloadRight;->deviceId:Ljava/lang/String;

    .line 77
    return-object p0
.end method

.method public setDownloadUri(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;
    .locals 0
    .parameter "s"

    .prologue
    .line 61
    iput-object p1, p0, Lcom/flixster/android/drm/DownloadRight;->downloadUri:Ljava/lang/String;

    .line 62
    return-object p0
.end method

.method public setLicenseProxy(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;
    .locals 0
    .parameter "s"

    .prologue
    .line 66
    iput-object p1, p0, Lcom/flixster/android/drm/DownloadRight;->licenseProxy:Ljava/lang/String;

    .line 67
    return-object p0
.end method

.method public setQueueId(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;
    .locals 0
    .parameter "s"

    .prologue
    .line 71
    iput-object p1, p0, Lcom/flixster/android/drm/DownloadRight;->queueId:Ljava/lang/String;

    .line 72
    return-object p0
.end method
