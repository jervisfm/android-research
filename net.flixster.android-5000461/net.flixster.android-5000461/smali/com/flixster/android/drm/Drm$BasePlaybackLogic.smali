.class abstract Lcom/flixster/android/drm/Drm$BasePlaybackLogic;
.super Ljava/lang/Object;
.source "Drm.java"

# interfaces
.implements Lcom/flixster/android/drm/PlaybackLogic;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/Drm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "BasePlaybackLogic"
.end annotation


# instance fields
.field private isNonWifiStreamUnsupported:Z

.field private isStreamUnsupported:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/drm/Drm$BasePlaybackLogic;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;-><init>()V

    return-void
.end method


# virtual methods
.method public isAssetDownloadable(Lnet/flixster/android/model/LockerRight;)Z
    .locals 1
    .parameter "r"

    .prologue
    .line 51
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lnet/flixster/android/model/LockerRight;->isDownloadSupported:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;->isDeviceDownloadCompatible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAssetPlayable(Lnet/flixster/android/model/LockerRight;)Z
    .locals 2
    .parameter "r"

    .prologue
    .line 44
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45
    iget-boolean v0, p1, Lnet/flixster/android/model/LockerRight;->isStreamingSupported:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;->isDeviceStreamingCompatible()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;->isDeviceDownloadCompatible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 46
    invoke-virtual {p1}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v0

    if-nez v0, :cond_2

    iget-wide v0, p1, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v0, v1}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    .line 44
    goto :goto_0
.end method

.method public isDeviceNonWifiStreamBlacklisted()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;->isNonWifiStreamUnsupported:Z

    return v0
.end method

.method public isDeviceStreamBlacklisted()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;->isStreamUnsupported:Z

    return v0
.end method

.method public setDeviceNonWifiStreamBlacklisted(Z)V
    .locals 1
    .parameter "b"

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;->isNonWifiStreamUnsupported:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;->isNonWifiStreamUnsupported:Z

    .line 72
    return-void
.end method

.method public setDeviceStreamBlacklisted(Z)V
    .locals 1
    .parameter "b"

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;->isStreamUnsupported:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;->isStreamUnsupported:Z

    .line 67
    return-void
.end method
