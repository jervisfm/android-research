.class Lcom/flixster/android/drm/Drm$WvLibPlaybackLogic;
.super Lcom/flixster/android/drm/Drm$BasePlaybackLogic;
.source "Drm.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/Drm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WvLibPlaybackLogic"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;-><init>(Lcom/flixster/android/drm/Drm$BasePlaybackLogic;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/drm/Drm$WvLibPlaybackLogic;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/flixster/android/drm/Drm$WvLibPlaybackLogic;-><init>()V

    return-void
.end method


# virtual methods
.method public isDeviceDownloadCompatible()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 90
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v1

    const/16 v2, 0x9

    if-ge v1, v2, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 92
    :cond_1
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/flixster/android/drm/PlaybackManager;->isRooted()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/flixster/android/drm/PlaybackManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDeviceStreamingCompatible()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 80
    :cond_1
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/flixster/android/drm/PlaybackManager;->isRooted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isNativeWv()Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method
