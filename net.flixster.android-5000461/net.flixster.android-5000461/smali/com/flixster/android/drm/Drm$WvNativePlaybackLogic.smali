.class Lcom/flixster/android/drm/Drm$WvNativePlaybackLogic;
.super Lcom/flixster/android/drm/Drm$BasePlaybackLogic;
.source "Drm.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/Drm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WvNativePlaybackLogic"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/Drm$BasePlaybackLogic;-><init>(Lcom/flixster/android/drm/Drm$BasePlaybackLogic;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/drm/Drm$WvNativePlaybackLogic;)V
    .locals 0
    .parameter

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/flixster/android/drm/Drm$WvNativePlaybackLogic;-><init>()V

    return-void
.end method


# virtual methods
.method public isDeviceDownloadCompatible()Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public isDeviceStreamingCompatible()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 109
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v1

    const/16 v2, 0xc

    if-ge v1, v2, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 111
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 112
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v1

    const/16 v2, 0xd

    if-lt v1, v2, :cond_0

    .line 115
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isNativeWv()Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method
