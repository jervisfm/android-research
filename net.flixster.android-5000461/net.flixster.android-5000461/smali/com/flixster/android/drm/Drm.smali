.class public Lcom/flixster/android/drm/Drm;
.super Ljava/lang/Object;
.source "Drm.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/drm/Drm$BasePlaybackLogic;,
        Lcom/flixster/android/drm/Drm$WvLibPlaybackLogic;,
        Lcom/flixster/android/drm/Drm$WvNativePlaybackLogic;
    }
.end annotation


# static fields
.field private static logicInstance:Lcom/flixster/android/drm/PlaybackLogic;

.field private static managerInstance:Lcom/flixster/android/drm/PlaybackManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static logic()Lcom/flixster/android/drm/PlaybackLogic;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    sget-object v0, Lcom/flixster/android/drm/Drm;->logicInstance:Lcom/flixster/android/drm/PlaybackLogic;

    if-nez v0, :cond_0

    .line 29
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    new-instance v0, Lcom/flixster/android/drm/Drm$WvNativePlaybackLogic;

    invoke-direct {v0, v1}, Lcom/flixster/android/drm/Drm$WvNativePlaybackLogic;-><init>(Lcom/flixster/android/drm/Drm$WvNativePlaybackLogic;)V

    sput-object v0, Lcom/flixster/android/drm/Drm;->logicInstance:Lcom/flixster/android/drm/PlaybackLogic;

    .line 35
    :cond_0
    :goto_0
    sget-object v0, Lcom/flixster/android/drm/Drm;->logicInstance:Lcom/flixster/android/drm/PlaybackLogic;

    return-object v0

    .line 32
    :cond_1
    sget-boolean v0, Lcom/flixster/android/utils/F;->IS_NATIVE_HC_DRM_ENABLED:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/flixster/android/drm/Drm$WvNativePlaybackLogic;

    invoke-direct {v0, v1}, Lcom/flixster/android/drm/Drm$WvNativePlaybackLogic;-><init>(Lcom/flixster/android/drm/Drm$WvNativePlaybackLogic;)V

    :goto_1
    sput-object v0, Lcom/flixster/android/drm/Drm;->logicInstance:Lcom/flixster/android/drm/PlaybackLogic;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/flixster/android/drm/Drm$WvLibPlaybackLogic;

    invoke-direct {v0, v1}, Lcom/flixster/android/drm/Drm$WvLibPlaybackLogic;-><init>(Lcom/flixster/android/drm/Drm$WvLibPlaybackLogic;)V

    goto :goto_1
.end method

.method public static manager()Lcom/flixster/android/drm/PlaybackManager;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/flixster/android/drm/Drm;->managerInstance:Lcom/flixster/android/drm/PlaybackManager;

    if-nez v0, :cond_0

    .line 18
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc;

    invoke-direct {v0}, Lcom/flixster/android/drm/DrmManagerHc;-><init>()V

    sput-object v0, Lcom/flixster/android/drm/Drm;->managerInstance:Lcom/flixster/android/drm/PlaybackManager;

    .line 24
    :cond_0
    :goto_0
    sget-object v0, Lcom/flixster/android/drm/Drm;->managerInstance:Lcom/flixster/android/drm/PlaybackManager;

    return-object v0

    .line 21
    :cond_1
    sget-boolean v0, Lcom/flixster/android/utils/F;->IS_NATIVE_HC_DRM_ENABLED:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc;

    invoke-direct {v0}, Lcom/flixster/android/drm/DrmManagerHc;-><init>()V

    :goto_1
    sput-object v0, Lcom/flixster/android/drm/Drm;->managerInstance:Lcom/flixster/android/drm/PlaybackManager;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/flixster/android/drm/DrmManager;

    invoke-direct {v0}, Lcom/flixster/android/drm/DrmManager;-><init>()V

    goto :goto_1
.end method
