.class Lcom/flixster/android/drm/DrmManager$2;
.super Landroid/os/Handler;
.source "DrmManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/DrmManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManager;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/DrmManager;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    .line 277
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .parameter "msg"

    .prologue
    .line 280
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/util/HashMap;

    .line 281
    .local v2, params:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    const-string v3, "fileLocation"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManager;->access$0(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V

    .line 282
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    const-string v3, "playPosition"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    #setter for: Lcom/flixster/android/drm/DrmManager;->playbackPosition:I
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManager;->access$1(Lcom/flixster/android/drm/DrmManager;I)V

    .line 283
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    const-string v3, "closedCaptionFileLocation"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManager;->captionsUri:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManager;->access$2(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V

    .line 285
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v3}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 286
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    const-string v3, "streamId"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManager;->access$4(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V

    .line 287
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    const-string v3, "customData"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManager;->customData:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManager;->access$5(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V

    .line 289
    const-string v3, "drmServerUrl"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 290
    .local v1, drmServerUrl:Ljava/lang/String;
    const-string v3, "deviceId"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 291
    .local v0, deviceId:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;
    invoke-static {v4}, Lcom/flixster/android/drm/DrmManager;->access$6(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v4

    #calls: Lcom/flixster/android/drm/DrmManager;->updateDrmCredentials(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v1, v0, v4}, Lcom/flixster/android/drm/DrmManager;->access$7(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->playbackPosition:I
    invoke-static {v4}, Lcom/flixster/android/drm/DrmManager;->access$8(Lcom/flixster/android/drm/DrmManager;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/flixster/android/drm/DrmManager;->wvPlay(I)V

    .line 296
    .end local v0           #deviceId:Ljava/lang/String;
    .end local v1           #drmServerUrl:Ljava/lang/String;
    :goto_0
    return-void

    .line 294
    :cond_0
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->uvStreamCreateSuccessHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/flixster/android/drm/DrmManager;->access$9(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->errorHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/flixster/android/drm/DrmManager;->access$10(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/drm/DrmManager$2;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v5}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lnet/flixster/android/data/ProfileDao;->uvStreamCreate(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V

    goto :goto_0
.end method
