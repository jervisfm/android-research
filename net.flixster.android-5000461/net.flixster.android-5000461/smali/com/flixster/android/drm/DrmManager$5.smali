.class Lcom/flixster/android/drm/DrmManager$5;
.super Landroid/os/Handler;
.source "DrmManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/DrmManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManager;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/DrmManager;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager$5;->this$0:Lcom/flixster/android/drm/DrmManager;

    .line 313
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 316
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/util/HashMap;

    .line 317
    .local v2, params:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$5;->this$0:Lcom/flixster/android/drm/DrmManager;

    const-string v3, "fileLocation"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManager;->access$0(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V

    .line 318
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$5;->this$0:Lcom/flixster/android/drm/DrmManager;

    const-string v3, "streamId"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManager;->access$4(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V

    .line 319
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$5;->this$0:Lcom/flixster/android/drm/DrmManager;

    const-string v3, "customData"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManager;->customData:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManager;->access$5(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V

    .line 321
    const-string v3, "drmServerUrl"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 322
    .local v1, drmServerUrl:Ljava/lang/String;
    const-string v3, "deviceId"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 323
    .local v0, deviceId:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager$5;->this$0:Lcom/flixster/android/drm/DrmManager;

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager$5;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;
    invoke-static {v4}, Lcom/flixster/android/drm/DrmManager;->access$6(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v4

    #calls: Lcom/flixster/android/drm/DrmManager;->updateDrmCredentials(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v1, v0, v4}, Lcom/flixster/android/drm/DrmManager;->access$7(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    return-void
.end method
