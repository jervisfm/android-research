.class Lcom/flixster/android/drm/DrmManager$6;
.super Landroid/os/Handler;
.source "DrmManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/DrmManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManager;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/DrmManager;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager$6;->this$0:Lcom/flixster/android/drm/DrmManager;

    .line 327
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 329
    const-string v0, "FlxDrm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DrmManager.uvStreamDeleteSuccessHandler "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager$6;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " streamId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager$6;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/drm/DrmManager;->access$6(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    return-void
.end method
