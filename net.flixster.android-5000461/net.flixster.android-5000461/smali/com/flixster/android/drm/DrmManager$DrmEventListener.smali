.class Lcom/flixster/android/drm/DrmManager$DrmEventListener;
.super Ljava/lang/Object;
.source "DrmManager.java"

# interfaces
.implements Lcom/widevine/drmapi/android/WVEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/DrmManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrmEventListener"
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$widevine$drmapi$android$WVEvent:[I


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManager;


# direct methods
.method static synthetic $SWITCH_TABLE$com$widevine$drmapi$android$WVEvent()[I
    .locals 3

    .prologue
    .line 490
    sget-object v0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->$SWITCH_TABLE$com$widevine$drmapi$android$WVEvent:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/widevine/drmapi/android/WVEvent;->values()[Lcom/widevine/drmapi/android/WVEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->EndOfList:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_e

    :goto_1
    :try_start_1
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_d

    :goto_2
    :try_start_2
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Initialized:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_c

    :goto_3
    :try_start_3
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseReceived:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_b

    :goto_4
    :try_start_4
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRemoved:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_a

    :goto_5
    :try_start_5
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_9

    :goto_6
    :try_start_6
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->NullEvent:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_8

    :goto_7
    :try_start_7
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->PlayFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :goto_8
    :try_start_8
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Playing:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_6

    :goto_9
    :try_start_9
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->QueryStatus:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_5

    :goto_a
    :try_start_a
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Registered:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_4

    :goto_b
    :try_start_b
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->SecureStore:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_3

    :goto_c
    :try_start_c
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Stopped:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_2

    :goto_d
    :try_start_d
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Terminated:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1

    :goto_e
    :try_start_e
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Unregistered:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_0

    :goto_f
    sput-object v0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->$SWITCH_TABLE$com$widevine$drmapi$android$WVEvent:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_f

    :catch_1
    move-exception v1

    goto :goto_e

    :catch_2
    move-exception v1

    goto :goto_d

    :catch_3
    move-exception v1

    goto :goto_c

    :catch_4
    move-exception v1

    goto :goto_b

    :catch_5
    move-exception v1

    goto :goto_a

    :catch_6
    move-exception v1

    goto :goto_9

    :catch_7
    move-exception v1

    goto :goto_8

    :catch_8
    move-exception v1

    goto :goto_7

    :catch_9
    move-exception v1

    goto :goto_6

    :catch_a
    move-exception v1

    goto :goto_5

    :catch_b
    move-exception v1

    goto/16 :goto_4

    :catch_c
    move-exception v1

    goto/16 :goto_3

    :catch_d
    move-exception v1

    goto/16 :goto_2

    :catch_e
    move-exception v1

    goto/16 :goto_1
.end method

.method private constructor <init>(Lcom/flixster/android/drm/DrmManager;)V
    .locals 0
    .parameter

    .prologue
    .line 490
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/drm/DrmManager;Lcom/flixster/android/drm/DrmManager$DrmEventListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 490
    invoke-direct {p0, p1}, Lcom/flixster/android/drm/DrmManager$DrmEventListener;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 13
    .parameter "eventType"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/widevine/drmapi/android/WVEvent;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/widevine/drmapi/android/WVStatus;"
        }
    .end annotation

    .prologue
    .local p2, attributes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v12, -0x1

    const/4 v9, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 495
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #calls: Lcom/flixster/android/drm/DrmManager;->logEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V
    invoke-static {v6, p1, p2}, Lcom/flixster/android/drm/DrmManager;->access$15(Lcom/flixster/android/drm/DrmManager;Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V

    .line 496
    const/4 v3, 0x0

    .line 497
    .local v3, status:Lcom/widevine/drmapi/android/WVStatus;
    const-string v6, "WVStatusKey"

    invoke-virtual {p2, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 498
    const-string v6, "WVStatusKey"

    invoke-virtual {p2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3           #status:Lcom/widevine/drmapi/android/WVStatus;
    check-cast v3, Lcom/widevine/drmapi/android/WVStatus;

    .line 499
    .restart local v3       #status:Lcom/widevine/drmapi/android/WVStatus;
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v3, v6, :cond_3

    .line 500
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->player:Lcom/flixster/android/drm/WidevinePlayer;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$16(Lcom/flixster/android/drm/DrmManager;)Lcom/flixster/android/drm/WidevinePlayer;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 502
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v3, v6, :cond_5

    .line 503
    sget-object v2, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->NETWORK:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    .line 511
    .local v2, error:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 512
    .local v4, wvError:Ljava/lang/StringBuilder;
    invoke-virtual {v3}, Lcom/widevine/drmapi/android/WVStatus;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    const-string v6, "WVErrorKey"

    invoke-virtual {p2, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 514
    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v6, "WVErrorKey"

    invoke-virtual {p2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    :cond_0
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->player:Lcom/flixster/android/drm/WidevinePlayer;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$16(Lcom/flixster/android/drm/DrmManager;)Lcom/flixster/android/drm/WidevinePlayer;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Lcom/flixster/android/drm/WidevinePlayer;->notifyPlaybackFailure(Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;Ljava/lang/String;)V

    .line 518
    .end local v2           #error:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    .end local v4           #wvError:Ljava/lang/StringBuilder;
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 519
    .local v5, wvLabel:Ljava/lang/StringBuilder;
    const-string v6, "WVStatus: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/widevine/drmapi/android/WVStatus;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    const-string v6, "WVErrorKey"

    invoke-virtual {p2, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 521
    const-string v6, ", WVError: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v6, "WVErrorKey"

    invoke-virtual {p2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 523
    :cond_2
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    #calls: Lcom/flixster/android/drm/DrmManager;->trackPlaybackEvent(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/flixster/android/drm/DrmManager;->access$13(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V

    .line 527
    .end local v5           #wvLabel:Ljava/lang/StringBuilder;
    :cond_3
    invoke-static {}, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->$SWITCH_TABLE$com$widevine$drmapi$android$WVEvent()[I

    move-result-object v6

    invoke-virtual {p1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 627
    :cond_4
    :goto_1
    :pswitch_0
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    :goto_2
    return-object v6

    .line 504
    :cond_5
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->NotLicensed:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v3, v6, :cond_6

    .line 505
    sget-object v2, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->LICENSE:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    .restart local v2       #error:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    goto :goto_0

    .line 506
    .end local v2           #error:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    :cond_6
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v3, v6, :cond_7

    .line 507
    sget-object v2, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->SECURITY:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    .restart local v2       #error:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    goto/16 :goto_0

    .line 509
    .end local v2           #error:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    :cond_7
    sget-object v2, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->UNKNOWN:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    .restart local v2       #error:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    goto/16 :goto_0

    .line 530
    .end local v2           #error:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    :pswitch_1
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #setter for: Lcom/flixster/android/drm/DrmManager;->isInitialized:Z
    invoke-static {v6, v11}, Lcom/flixster/android/drm/DrmManager;->access$17(Lcom/flixster/android/drm/DrmManager;Z)V

    .line 531
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->isInitializedOnDemand:Z
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$18(Lcom/flixster/android/drm/DrmManager;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 532
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v7

    #calls: Lcom/flixster/android/drm/DrmManager;->registerLocalAsset(Lnet/flixster/android/model/LockerRight;)V
    invoke-static {v6, v7}, Lcom/flixster/android/drm/DrmManager;->access$19(Lcom/flixster/android/drm/DrmManager;Lnet/flixster/android/model/LockerRight;)V

    .line 534
    :cond_8
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_2

    .line 536
    :pswitch_2
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$14(Lcom/flixster/android/drm/DrmManager;)Landroid/app/ProgressDialog;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 538
    :try_start_0
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$14(Lcom/flixster/android/drm/DrmManager;)Landroid/app/ProgressDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 543
    :cond_9
    :goto_3
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_2

    .line 539
    :catch_0
    move-exception v1

    .line 540
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 545
    .end local v1           #e:Ljava/lang/Exception;
    :pswitch_3
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v6

    invoke-virtual {v6}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v6

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->isStreamingMode:Z
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$20(Lcom/flixster/android/drm/DrmManager;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 546
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->trackLicenseSuccessHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$21(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$22(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v7

    iget-object v8, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v8}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v8

    iget-object v9, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;
    invoke-static {v9}, Lcom/flixster/android/drm/DrmManager;->access$6(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v7, v8, v9, v11}, Lnet/flixster/android/data/ProfileDao;->trackLicense(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Z)V

    .line 547
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->ackLicenseSuccessHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$23(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$22(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v7

    iget-object v8, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->customData:Ljava/lang/String;
    invoke-static {v8}, Lcom/flixster/android/drm/DrmManager;->access$12(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lnet/flixster/android/data/ProfileDao;->ackLicense(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    .line 549
    :cond_a
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto/16 :goto_2

    .line 555
    :pswitch_4
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto/16 :goto_2

    .line 559
    :pswitch_5
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v6

    invoke-virtual {v6}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 560
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->trackLicenseSuccessHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$21(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$22(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v7

    iget-object v8, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v8}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v8

    invoke-static {v6, v7, v8, v9, v11}, Lnet/flixster/android/data/ProfileDao;->trackLicense(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Z)V

    .line 562
    :cond_b
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$24(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$24(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->downloadAssetUri:Ljava/lang/String;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$25(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 563
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v7

    iget-wide v7, v7, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v7, v8}, Lcom/flixster/android/net/DownloadHelper;->findDownloadCaptionsFile(J)Ljava/lang/String;

    move-result-object v7

    #setter for: Lcom/flixster/android/drm/DrmManager;->captionsUri:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/flixster/android/drm/DrmManager;->access$2(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V

    .line 564
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    invoke-virtual {v6, v12}, Lcom/flixster/android/drm/DrmManager;->wvPlay(I)V

    .line 566
    :cond_c
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #calls: Lcom/flixster/android/drm/DrmManager;->registerLocalAssetLooper()V
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$26(Lcom/flixster/android/drm/DrmManager;)V

    goto/16 :goto_1

    .line 569
    :pswitch_6
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v6

    invoke-virtual {v6}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v6

    if-eqz v6, :cond_d

    .line 570
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->trackLicenseSuccessHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$21(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$22(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v7

    iget-object v8, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v8}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v8

    invoke-static {v6, v7, v8, v9, v10}, Lnet/flixster/android/data/ProfileDao;->trackLicense(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Z)V

    .line 572
    :cond_d
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$14(Lcom/flixster/android/drm/DrmManager;)Landroid/app/ProgressDialog;

    move-result-object v6

    if-eqz v6, :cond_e

    .line 574
    :try_start_1
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$14(Lcom/flixster/android/drm/DrmManager;)Landroid/app/ProgressDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 579
    :cond_e
    :goto_4
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #calls: Lcom/flixster/android/drm/DrmManager;->registerLocalAssetLooper()V
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$26(Lcom/flixster/android/drm/DrmManager;)V

    goto/16 :goto_1

    .line 575
    :catch_1
    move-exception v1

    .line 576
    .restart local v1       #e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 584
    .end local v1           #e:Ljava/lang/Exception;
    :pswitch_7
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->downloadAssetUri:Ljava/lang/String;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$25(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/flixster/android/drm/DrmManager;->wvQuery(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 587
    :pswitch_8
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v3, v6, :cond_11

    .line 588
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 589
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->downloadAssetUri:Ljava/lang/String;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$25(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/flixster/android/drm/DrmManager;->wvRequestLicense(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 591
    :cond_f
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$14(Lcom/flixster/android/drm/DrmManager;)Landroid/app/ProgressDialog;

    move-result-object v6

    if-eqz v6, :cond_10

    .line 593
    :try_start_2
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$14(Lcom/flixster/android/drm/DrmManager;)Landroid/app/ProgressDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 599
    :cond_10
    :goto_5
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->context:Landroid/content/Context;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$27(Lcom/flixster/android/drm/DrmManager;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c017e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 600
    .local v2, error:Ljava/lang/String;
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->errorHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$10(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v6

    .line 601
    sget-object v7, Lnet/flixster/android/data/DaoException$Type;->NOT_LICENSED:Lnet/flixster/android/data/DaoException$Type;

    invoke-static {v7, v2}, Lnet/flixster/android/data/DaoException;->create(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v7

    .line 600
    invoke-static {v9, v10, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 603
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #calls: Lcom/flixster/android/drm/DrmManager;->registerLocalAssetLooper()V
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$26(Lcom/flixster/android/drm/DrmManager;)V

    goto/16 :goto_1

    .line 594
    .end local v2           #error:Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 595
    .restart local v1       #e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 606
    .end local v1           #e:Ljava/lang/Exception;
    :cond_11
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$24(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_12

    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$24(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->downloadAssetUri:Ljava/lang/String;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$25(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 607
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v7

    iget-wide v7, v7, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v7, v8}, Lcom/flixster/android/net/DownloadHelper;->findDownloadCaptionsFile(J)Ljava/lang/String;

    move-result-object v7

    #setter for: Lcom/flixster/android/drm/DrmManager;->captionsUri:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/flixster/android/drm/DrmManager;->access$2(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V

    .line 608
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    invoke-virtual {v6, v12}, Lcom/flixster/android/drm/DrmManager;->wvPlay(I)V

    .line 610
    :cond_12
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #calls: Lcom/flixster/android/drm/DrmManager;->registerLocalAssetLooper()V
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$26(Lcom/flixster/android/drm/DrmManager;)V

    goto/16 :goto_1

    .line 614
    :pswitch_9
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v6

    invoke-virtual {v6}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v6

    if-eqz v6, :cond_13

    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->NotLicensed:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v3, v6, :cond_13

    .line 615
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->trackLicenseSuccessHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/flixster/android/drm/DrmManager;->access$21(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/flixster/android/drm/DrmManager;->access$22(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;

    move-result-object v7

    iget-object v8, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v8}, Lcom/flixster/android/drm/DrmManager;->access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;

    move-result-object v8

    iget-object v9, p0, Lcom/flixster/android/drm/DrmManager$DrmEventListener;->this$0:Lcom/flixster/android/drm/DrmManager;

    #getter for: Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;
    invoke-static {v9}, Lcom/flixster/android/drm/DrmManager;->access$6(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v7, v8, v9, v10}, Lnet/flixster/android/data/ProfileDao;->trackLicense(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 616
    :cond_13
    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v3, v6, :cond_4

    .line 617
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/flixster/android/utils/ActivityHolder;->getTopLevelActivity()Landroid/app/Activity;

    move-result-object v0

    .line 618
    .local v0, activity:Landroid/app/Activity;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v6

    if-nez v6, :cond_4

    .line 619
    invoke-static {v0}, Lcom/flixster/android/view/DialogBuilder;->showStreamUnsupportedOnRootedDevices(Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 527
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_9
        :pswitch_4
        :pswitch_8
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
