.class public Lcom/flixster/android/drm/DrmManager;
.super Ljava/lang/Object;
.source "DrmManager.java"

# interfaces
.implements Lcom/flixster/android/drm/PlaybackManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/drm/DrmManager$DrmEventListener;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$widevine$drmapi$android$WVEvent:[I

.field private static synthetic $SWITCH_TABLE$com$widevine$drmapi$android$WVStatus:[I

.field private static final PORTAL_KEYS:[Ljava/lang/String;


# instance fields
.field private final ackLicenseSuccessHandler:Landroid/os/Handler;

.field private captionsUri:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private customData:Ljava/lang/String;

.field private dialog:Landroid/app/ProgressDialog;

.field private downloadAssetUri:Ljava/lang/String;

.field private downloadedAssetRights:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;"
        }
    .end annotation
.end field

.field private final endDownloadSuccessHandler:Landroid/os/Handler;

.field private final errorHandler:Landroid/os/Handler;

.field private isInitialized:Z

.field private isInitializedOnDemand:Z

.field private isRooted:Ljava/lang/Boolean;

.field private isStreamingMode:Z

.field private playbackAssetUri:Ljava/lang/String;

.field private playbackPosition:I

.field private player:Lcom/flixster/android/drm/WidevinePlayer;

.field private r:Lnet/flixster/android/model/LockerRight;

.field private final silentErrorHandler:Landroid/os/Handler;

.field private streamId:Ljava/lang/String;

.field private final streamResumeSuccessHandler:Landroid/os/Handler;

.field private final streamStartSuccessHandler:Landroid/os/Handler;

.field private final streamStopSuccessHandler:Landroid/os/Handler;

.field private final trackLicenseSuccessHandler:Landroid/os/Handler;

.field private final updatePlaybackPositionSuccessHandler:Landroid/os/Handler;

.field private final uvStreamCreateSuccessHandler:Landroid/os/Handler;

.field private final uvStreamDeleteSuccessHandler:Landroid/os/Handler;

.field private final uvStreamResumeSuccessHandler:Landroid/os/Handler;

.field private wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;


# direct methods
.method static synthetic $SWITCH_TABLE$com$widevine$drmapi$android$WVEvent()[I
    .locals 3

    .prologue
    .line 37
    sget-object v0, Lcom/flixster/android/drm/DrmManager;->$SWITCH_TABLE$com$widevine$drmapi$android$WVEvent:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/widevine/drmapi/android/WVEvent;->values()[Lcom/widevine/drmapi/android/WVEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->EndOfList:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_e

    :goto_1
    :try_start_1
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_d

    :goto_2
    :try_start_2
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Initialized:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_c

    :goto_3
    :try_start_3
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseReceived:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_b

    :goto_4
    :try_start_4
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRemoved:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_a

    :goto_5
    :try_start_5
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_9

    :goto_6
    :try_start_6
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->NullEvent:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_8

    :goto_7
    :try_start_7
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->PlayFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :goto_8
    :try_start_8
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Playing:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_6

    :goto_9
    :try_start_9
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->QueryStatus:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_5

    :goto_a
    :try_start_a
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Registered:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_4

    :goto_b
    :try_start_b
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->SecureStore:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_3

    :goto_c
    :try_start_c
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Stopped:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_2

    :goto_d
    :try_start_d
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Terminated:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1

    :goto_e
    :try_start_e
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Unregistered:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_0

    :goto_f
    sput-object v0, Lcom/flixster/android/drm/DrmManager;->$SWITCH_TABLE$com$widevine$drmapi$android$WVEvent:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_f

    :catch_1
    move-exception v1

    goto :goto_e

    :catch_2
    move-exception v1

    goto :goto_d

    :catch_3
    move-exception v1

    goto :goto_c

    :catch_4
    move-exception v1

    goto :goto_b

    :catch_5
    move-exception v1

    goto :goto_a

    :catch_6
    move-exception v1

    goto :goto_9

    :catch_7
    move-exception v1

    goto :goto_8

    :catch_8
    move-exception v1

    goto :goto_7

    :catch_9
    move-exception v1

    goto :goto_6

    :catch_a
    move-exception v1

    goto :goto_5

    :catch_b
    move-exception v1

    goto/16 :goto_4

    :catch_c
    move-exception v1

    goto/16 :goto_3

    :catch_d
    move-exception v1

    goto/16 :goto_2

    :catch_e
    move-exception v1

    goto/16 :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$widevine$drmapi$android$WVStatus()[I
    .locals 3

    .prologue
    .line 37
    sget-object v0, Lcom/flixster/android/drm/DrmManager;->$SWITCH_TABLE$com$widevine$drmapi$android$WVStatus:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/widevine/drmapi/android/WVStatus;->values()[Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->AlreadyInitialized:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1d

    :goto_1
    :try_start_1
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->AlreadyPlaying:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1c

    :goto_2
    :try_start_2
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->AlreadyRegistered:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1b

    :goto_3
    :try_start_3
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->AssetDBWasCorrupted:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1a

    :goto_4
    :try_start_4
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->AssetExpired:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_19

    :goto_5
    :try_start_5
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_18

    :goto_6
    :try_start_6
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->BadURL:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_17

    :goto_7
    :try_start_7
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToDrmServer:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_16

    :goto_8
    :try_start_8
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToMediaServer:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_15

    :goto_9
    :try_start_9
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->ClockTamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_14

    :goto_a
    :try_start_a
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->FileNotPresent:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_13

    :goto_b
    :try_start_b
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->FileSystemError:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_12

    :goto_c
    :try_start_c
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->HardwareIDAbsent:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_11

    :goto_d
    :try_start_d
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->HeartbeatError:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_10

    :goto_e
    :try_start_e
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->LicenseDenied:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_f

    :goto_f
    :try_start_f
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->LicenseExpired:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_e

    :goto_10
    :try_start_10
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->LicenseRequestLimitReached:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_d

    :goto_11
    :try_start_11
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_c

    :goto_12
    :try_start_12
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->MandatorySettingAbsent:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_b

    :goto_13
    :try_start_13
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_a

    :goto_14
    :try_start_14
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->NotLicensed:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_9

    :goto_15
    :try_start_15
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->NotLicensedByRegion:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_8

    :goto_16
    :try_start_16
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->NotPlaying:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_7

    :goto_17
    :try_start_17
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->NotRegistered:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_6

    :goto_18
    :try_start_18
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_5

    :goto_19
    :try_start_19
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OutOfMemoryError:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_4

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OutOfRange:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_3

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->PendingServerNotification:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_2

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->SystemCallError:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_1

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_0

    :goto_1e
    sput-object v0, Lcom/flixster/android/drm/DrmManager;->$SWITCH_TABLE$com$widevine$drmapi$android$WVStatus:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_1e

    :catch_1
    move-exception v1

    goto :goto_1d

    :catch_2
    move-exception v1

    goto :goto_1c

    :catch_3
    move-exception v1

    goto :goto_1b

    :catch_4
    move-exception v1

    goto :goto_1a

    :catch_5
    move-exception v1

    goto :goto_19

    :catch_6
    move-exception v1

    goto :goto_18

    :catch_7
    move-exception v1

    goto :goto_17

    :catch_8
    move-exception v1

    goto :goto_16

    :catch_9
    move-exception v1

    goto :goto_15

    :catch_a
    move-exception v1

    goto :goto_14

    :catch_b
    move-exception v1

    goto/16 :goto_13

    :catch_c
    move-exception v1

    goto/16 :goto_12

    :catch_d
    move-exception v1

    goto/16 :goto_11

    :catch_e
    move-exception v1

    goto/16 :goto_10

    :catch_f
    move-exception v1

    goto/16 :goto_f

    :catch_10
    move-exception v1

    goto/16 :goto_e

    :catch_11
    move-exception v1

    goto/16 :goto_d

    :catch_12
    move-exception v1

    goto/16 :goto_c

    :catch_13
    move-exception v1

    goto/16 :goto_b

    :catch_14
    move-exception v1

    goto/16 :goto_a

    :catch_15
    move-exception v1

    goto/16 :goto_9

    :catch_16
    move-exception v1

    goto/16 :goto_8

    :catch_17
    move-exception v1

    goto/16 :goto_7

    :catch_18
    move-exception v1

    goto/16 :goto_6

    :catch_19
    move-exception v1

    goto/16 :goto_5

    :catch_1a
    move-exception v1

    goto/16 :goto_4

    :catch_1b
    move-exception v1

    goto/16 :goto_3

    :catch_1c
    move-exception v1

    goto/16 :goto_2

    :catch_1d
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "warnerbros"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CN"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "IDM"

    aput-object v2, v0, v1

    sput-object v0, Lcom/flixster/android/drm/DrmManager;->PORTAL_KEYS:[Ljava/lang/String;

    .line 37
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    new-instance v0, Lcom/flixster/android/drm/DrmManager$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$1;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->endDownloadSuccessHandler:Landroid/os/Handler;

    .line 277
    new-instance v0, Lcom/flixster/android/drm/DrmManager$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$2;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->streamStartSuccessHandler:Landroid/os/Handler;

    .line 299
    new-instance v0, Lcom/flixster/android/drm/DrmManager$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$3;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->uvStreamCreateSuccessHandler:Landroid/os/Handler;

    .line 307
    new-instance v0, Lcom/flixster/android/drm/DrmManager$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$4;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->streamStopSuccessHandler:Landroid/os/Handler;

    .line 313
    new-instance v0, Lcom/flixster/android/drm/DrmManager$5;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$5;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->streamResumeSuccessHandler:Landroid/os/Handler;

    .line 327
    new-instance v0, Lcom/flixster/android/drm/DrmManager$6;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$6;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->uvStreamDeleteSuccessHandler:Landroid/os/Handler;

    .line 333
    new-instance v0, Lcom/flixster/android/drm/DrmManager$7;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$7;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->uvStreamResumeSuccessHandler:Landroid/os/Handler;

    .line 340
    new-instance v0, Lcom/flixster/android/drm/DrmManager$8;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$8;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->updatePlaybackPositionSuccessHandler:Landroid/os/Handler;

    .line 346
    new-instance v0, Lcom/flixster/android/drm/DrmManager$9;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$9;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->trackLicenseSuccessHandler:Landroid/os/Handler;

    .line 352
    new-instance v0, Lcom/flixster/android/drm/DrmManager$10;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$10;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->ackLicenseSuccessHandler:Landroid/os/Handler;

    .line 358
    new-instance v0, Lcom/flixster/android/drm/DrmManager$11;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$11;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;

    .line 364
    new-instance v0, Lcom/flixster/android/drm/DrmManager$12;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManager$12;-><init>(Lcom/flixster/android/drm/DrmManager;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->errorHandler:Landroid/os/Handler;

    .line 37
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/drm/DrmManager;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55
    iput p1, p0, Lcom/flixster/android/drm/DrmManager;->playbackPosition:I

    return-void
.end method

.method static synthetic access$10(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 364
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->errorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$11(Lcom/flixster/android/drm/DrmManager;)V
    .locals 0
    .parameter

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/flixster/android/drm/DrmManager;->updateDrmCredentials()V

    return-void
.end method

.method static synthetic access$12(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->customData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$13(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 893
    invoke-direct {p0, p1}, Lcom/flixster/android/drm/DrmManager;->trackPlaybackEvent(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$14(Lcom/flixster/android/drm/DrmManager;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$15(Lcom/flixster/android/drm/DrmManager;Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 631
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/drm/DrmManager;->logEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V

    return-void
.end method

.method static synthetic access$16(Lcom/flixster/android/drm/DrmManager;)Lcom/flixster/android/drm/WidevinePlayer;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->player:Lcom/flixster/android/drm/WidevinePlayer;

    return-object v0
.end method

.method static synthetic access$17(Lcom/flixster/android/drm/DrmManager;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/flixster/android/drm/DrmManager;->isInitialized:Z

    return-void
.end method

.method static synthetic access$18(Lcom/flixster/android/drm/DrmManager;)Z
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/flixster/android/drm/DrmManager;->isInitializedOnDemand:Z

    return v0
.end method

.method static synthetic access$19(Lcom/flixster/android/drm/DrmManager;Lnet/flixster/android/model/LockerRight;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lcom/flixster/android/drm/DrmManager;->registerLocalAsset(Lnet/flixster/android/model/LockerRight;)V

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager;->captionsUri:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$20(Lcom/flixster/android/drm/DrmManager;)Z
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/flixster/android/drm/DrmManager;->isStreamingMode:Z

    return v0
.end method

.method static synthetic access$21(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 346
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->trackLicenseSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$22(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 358
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$23(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 352
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->ackLicenseSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$24(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$25(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->downloadAssetUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$26(Lcom/flixster/android/drm/DrmManager;)V
    .locals 0
    .parameter

    .prologue
    .line 949
    invoke-direct {p0}, Lcom/flixster/android/drm/DrmManager;->registerLocalAssetLooper()V

    return-void
.end method

.method static synthetic access$27(Lcom/flixster/android/drm/DrmManager;)Landroid/content/Context;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/drm/DrmManager;)Lnet/flixster/android/model/LockerRight;
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager;->customData:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/flixster/android/drm/DrmManager;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7(Lcom/flixster/android/drm/DrmManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 124
    invoke-direct {p0, p1, p2, p3}, Lcom/flixster/android/drm/DrmManager;->updateDrmCredentials(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$8(Lcom/flixster/android/drm/DrmManager;)I
    .locals 1
    .parameter

    .prologue
    .line 55
    iget v0, p0, Lcom/flixster/android/drm/DrmManager;->playbackPosition:I

    return v0
.end method

.method static synthetic access$9(Lcom/flixster/android/drm/DrmManager;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 299
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->uvStreamCreateSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private eventToString(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 3
    .parameter "eventType"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/widevine/drmapi/android/WVEvent;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 638
    .local p2, attributes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, ""

    .line 640
    .local v0, result:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/drm/DrmManager;->$SWITCH_TABLE$com$widevine$drmapi$android$WVEvent()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 688
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: Error UNKNOWN\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 692
    :goto_0
    const-string v1, "WVLicenseDurationRemainingKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 693
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVLicenseDurationRemaining: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVLicenseDurationRemainingKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 694
    :cond_0
    const-string v1, "WVPurchaseDurationRemainingKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 695
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVPurchaseDurationRemaining: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVPurchaseDurationRemainingKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 696
    :cond_1
    const-string v1, "WVPlaybackElapsedTimeKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 697
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVPlaybackElapsedTimeKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVPlaybackElapsedTimeKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 698
    :cond_2
    const-string v1, "WVAssetPathKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 699
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVAssetPathKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVAssetPathKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 700
    :cond_3
    const-string v1, "WVLicenseTypeKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 701
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVLicenseTypeKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVLicenseTypeKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 703
    :cond_4
    const-string v1, "WVIsEncryptedKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 704
    const-string v1, "WVIsEncryptedKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 705
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVIsEncryptedKey: True\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 710
    :cond_5
    :goto_1
    const-string v1, "WVStatusKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 712
    invoke-static {}, Lcom/flixster/android/drm/DrmManager;->$SWITCH_TABLE$com$widevine$drmapi$android$WVStatus()[I

    move-result-object v2

    const-string v1, "WVStatusKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1

    .line 801
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: Unknown Error\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 806
    :cond_6
    :goto_2
    const-string v1, "WVVersionKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 807
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVVersionKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVVersionKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 808
    :cond_7
    const-string v1, "WVAssetTypeKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 809
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVAssetTypeKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVAssetTypeKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 810
    :cond_8
    const-string v1, "WVSystemIDKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 811
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVSystemIDKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVSystemIDKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 812
    :cond_9
    const-string v1, "WVAssetIDKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 813
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVAssetIDKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVAssetIDKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 814
    :cond_a
    const-string v1, "WVKeyIDKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 815
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVKeyID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVKeyIDKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 817
    :cond_b
    const-string v1, "WVErrorKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 818
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVErrorKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "WVErrorKey"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 820
    :cond_c
    return-object v0

    .line 643
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: NullEvent\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 644
    goto/16 :goto_0

    .line 646
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: LicenseReceived\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 647
    goto/16 :goto_0

    .line 649
    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: LicenseRequestFailed\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 650
    goto/16 :goto_0

    .line 652
    :pswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: Playing\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 653
    goto/16 :goto_0

    .line 655
    :pswitch_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: PlayFailed\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 656
    goto/16 :goto_0

    .line 658
    :pswitch_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: Stopped\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 659
    goto/16 :goto_0

    .line 661
    :pswitch_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: QueryStatus\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 662
    goto/16 :goto_0

    .line 664
    :pswitch_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: EndOfList\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 665
    goto/16 :goto_0

    .line 667
    :pswitch_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: Initialized\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 668
    goto/16 :goto_0

    .line 670
    :pswitch_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: InitializeFailed\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 671
    goto/16 :goto_0

    .line 673
    :pswitch_b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: Terminated\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 674
    goto/16 :goto_0

    .line 676
    :pswitch_c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: LicenseRemoved\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 677
    goto/16 :goto_0

    .line 679
    :pswitch_d
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: Registered\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 680
    goto/16 :goto_0

    .line 682
    :pswitch_e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: Unregistered\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 683
    goto/16 :goto_0

    .line 685
    :pswitch_f
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onEvent: SecureStore\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 686
    goto/16 :goto_0

    .line 707
    :cond_d
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVIsEncryptedKey: False\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 714
    :pswitch_10
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: OK\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 715
    goto/16 :goto_2

    .line 717
    :pswitch_11
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: NotInitialized\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 718
    goto/16 :goto_2

    .line 720
    :pswitch_12
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: AlreadyInitialized\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 721
    goto/16 :goto_2

    .line 723
    :pswitch_13
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: CantConnectToMediaServer\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 724
    goto/16 :goto_2

    .line 726
    :pswitch_14
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: BadMedia\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 727
    goto/16 :goto_2

    .line 729
    :pswitch_15
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: CantConnectToDrmServer\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 730
    goto/16 :goto_2

    .line 732
    :pswitch_16
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: NotLicensed\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 733
    goto/16 :goto_2

    .line 735
    :pswitch_17
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: LicenseDenied\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 736
    goto/16 :goto_2

    .line 738
    :pswitch_18
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: LostConnection\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 739
    goto/16 :goto_2

    .line 741
    :pswitch_19
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: LicenseExpired\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 742
    goto/16 :goto_2

    .line 744
    :pswitch_1a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: AssetExpired\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 745
    goto/16 :goto_2

    .line 747
    :pswitch_1b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: NotLicensedByRegion\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 748
    goto/16 :goto_2

    .line 750
    :pswitch_1c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: EntitlenentRequestLimitReached\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 751
    goto/16 :goto_2

    .line 753
    :pswitch_1d
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: BadURL\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 754
    goto/16 :goto_2

    .line 756
    :pswitch_1e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: FileNotPresent\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 757
    goto/16 :goto_2

    .line 759
    :pswitch_1f
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: NotRegistered\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 760
    goto/16 :goto_2

    .line 762
    :pswitch_20
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: AlreadyRegistered\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 763
    goto/16 :goto_2

    .line 765
    :pswitch_21
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: NotPlaying\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 766
    goto/16 :goto_2

    .line 768
    :pswitch_22
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: AlreadyPlaying\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 769
    goto/16 :goto_2

    .line 771
    :pswitch_23
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: FileSystemError\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 772
    goto/16 :goto_2

    .line 774
    :pswitch_24
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: AssetDBWasCorrupted\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 775
    goto/16 :goto_2

    .line 777
    :pswitch_25
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: MandatorySettingAbsent\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 778
    goto/16 :goto_2

    .line 780
    :pswitch_26
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: SystemCallError\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 781
    goto/16 :goto_2

    .line 783
    :pswitch_27
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: OutOfMemoryError\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 784
    goto/16 :goto_2

    .line 786
    :pswitch_28
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatusKey: TamperDetected\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 787
    goto/16 :goto_2

    .line 789
    :pswitch_29
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatus: PendingServerNotification\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 790
    goto/16 :goto_2

    .line 792
    :pswitch_2a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatus: HardwareIDAbsent\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 793
    goto/16 :goto_2

    .line 795
    :pswitch_2b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatus: OutOfRange\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 796
    goto/16 :goto_2

    .line 798
    :pswitch_2c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "WVStatus: HeartbeatError\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 799
    goto/16 :goto_2

    .line 640
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 712
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_0
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
    .end packed-switch
.end method

.method private getSettings()Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 104
    .local v0, settings:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "WVDRMServer"

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    if-nez v1, :cond_0

    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string v1, "WVPortalKey"

    sget-object v2, Lcom/flixster/android/drm/DrmManager;->PORTAL_KEYS:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    const-string v1, "WVAssetRootKey"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    return-object v0

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;

    invoke-static {v1, v3}, Lnet/flixster/android/data/ApiBuilder;->widevineLicense(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static isMovieOrEpisodeDownloaded(Lnet/flixster/android/model/LockerRight;)Z
    .locals 3
    .parameter "right"

    .prologue
    const/4 v0, 0x0

    .line 940
    iget-wide v1, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 945
    :cond_0
    :goto_0
    return v0

    .line 942
    :cond_1
    iget-wide v1, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 943
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private logEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V
    .locals 2
    .parameter "eventType"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/widevine/drmapi/android/WVEvent;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 632
    .local p2, attributes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "FlxDrm"

    const-string v1, "===================================="

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    const-string v0, "FlxDrm"

    invoke-direct {p0, p1, p2}, Lcom/flixster/android/drm/DrmManager;->eventToString(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const-string v0, "FlxDrm"

    const-string v1, "===================================="

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    return-void
.end method

.method private registerLocalAsset(Lnet/flixster/android/model/LockerRight;)V
    .locals 8
    .parameter "right"

    .prologue
    .line 164
    iget-wide v5, p1, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v5, v6}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedMovie(J)Ljava/lang/String;

    move-result-object v4

    .line 165
    .local v4, localUri:Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 166
    const-string v5, "file://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 167
    :cond_0
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    .line 168
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/flixster/android/drm/DrmManager;->isStreamingMode:Z

    .line 169
    new-instance v2, Lcom/flixster/android/drm/DownloadRight;

    iget-wide v5, p1, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-direct {v2, v5, v6}, Lcom/flixster/android/drm/DownloadRight;-><init>(J)V

    .line 170
    .local v2, downloadRight:Lcom/flixster/android/drm/DownloadRight;
    invoke-virtual {v2}, Lcom/flixster/android/drm/DownloadRight;->getLicenseProxy()Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, downloadLicenseProxy:Ljava/lang/String;
    const-string v5, ""

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 172
    invoke-direct {p0}, Lcom/flixster/android/drm/DrmManager;->updateDrmCredentials()V

    .line 178
    :goto_0
    const-string v5, "FlxDrm"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "DrmManager.registerLocalAsset "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p0, v4}, Lcom/flixster/android/drm/DrmManager;->wvRegister(Ljava/lang/String;)V

    .line 191
    .end local v1           #downloadLicenseProxy:Ljava/lang/String;
    .end local v2           #downloadRight:Lcom/flixster/android/drm/DownloadRight;
    :goto_1
    return-void

    .line 174
    .restart local v1       #downloadLicenseProxy:Ljava/lang/String;
    .restart local v2       #downloadRight:Lcom/flixster/android/drm/DownloadRight;
    :cond_1
    invoke-virtual {p1}, Lnet/flixster/android/model/LockerRight;->setSonicProxyMode()V

    .line 175
    invoke-virtual {v2}, Lcom/flixster/android/drm/DownloadRight;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, downloadDeviceId:Ljava/lang/String;
    const/4 v5, 0x0

    invoke-direct {p0, v1, v0, v5}, Lcom/flixster/android/drm/DrmManager;->updateDrmCredentials(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 181
    .end local v0           #downloadDeviceId:Ljava/lang/String;
    .end local v1           #downloadLicenseProxy:Ljava/lang/String;
    .end local v2           #downloadRight:Lcom/flixster/android/drm/DownloadRight;
    :cond_2
    const-string v5, "FlxDrm"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "DrmManager.registerLocalAsset cannot find "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    if-eqz v5, :cond_3

    .line 184
    :try_start_0
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :cond_3
    :goto_2
    invoke-direct {p0}, Lcom/flixster/android/drm/DrmManager;->registerLocalAssetLooper()V

    goto :goto_1

    .line 185
    :catch_0
    move-exception v3

    .line 186
    .local v3, e:Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method private registerLocalAssetLooper()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 950
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->downloadedAssetRights:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->downloadedAssetRights:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 951
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->downloadedAssetRights:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 952
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DrmManager.registerLocalAssetLooper registering "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    iput-object v4, p0, Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;

    .line 954
    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManager;->registerLocalAsset(Lnet/flixster/android/model/LockerRight;)V

    .line 961
    .end local v0           #right:Lnet/flixster/android/model/LockerRight;
    :goto_0
    return-void

    .line 955
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->downloadedAssetRights:Ljava/util/List;

    if-nez v1, :cond_1

    .line 956
    const-string v1, "FlxDrm"

    const-string v2, "DrmManager.registerLocalAssetLooper uninitialized"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 958
    :cond_1
    const-string v1, "FlxDrm"

    const-string v2, "DrmManager.registerLocalAssetLooper finished"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    iput-object v4, p0, Lcom/flixster/android/drm/DrmManager;->downloadedAssetRights:Ljava/util/List;

    goto :goto_0
.end method

.method private statusToString(Lcom/widevine/drmapi/android/WVStatus;)Ljava/lang/String;
    .locals 2
    .parameter "status"

    .prologue
    .line 824
    invoke-static {}, Lcom/flixster/android/drm/DrmManager;->$SWITCH_TABLE$com$widevine$drmapi$android$WVStatus()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 886
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 826
    :pswitch_0
    const-string v0, "WVStatus.OK"

    goto :goto_0

    .line 828
    :pswitch_1
    const-string v0, "WVStatus.NotInitialized"

    goto :goto_0

    .line 830
    :pswitch_2
    const-string v0, "WVStatus.AlreadyInitialized"

    goto :goto_0

    .line 832
    :pswitch_3
    const-string v0, "WVStatus.CantConnectToMediaServer"

    goto :goto_0

    .line 834
    :pswitch_4
    const-string v0, "WVStatus.BadMedia"

    goto :goto_0

    .line 836
    :pswitch_5
    const-string v0, "WVStatus.CantConnectToDrmServer"

    goto :goto_0

    .line 838
    :pswitch_6
    const-string v0, "WVStatus.NotLicensed"

    goto :goto_0

    .line 840
    :pswitch_7
    const-string v0, "WVStatus.LicenseDenied"

    goto :goto_0

    .line 842
    :pswitch_8
    const-string v0, "WVStatus.LostConnection"

    goto :goto_0

    .line 844
    :pswitch_9
    const-string v0, "WVStatus.LicenseExpired"

    goto :goto_0

    .line 846
    :pswitch_a
    const-string v0, "WVStatus.AssetExpired"

    goto :goto_0

    .line 848
    :pswitch_b
    const-string v0, "WVStatus.NotLicensedByRegion"

    goto :goto_0

    .line 850
    :pswitch_c
    const-string v0, "WVStatus.LicenseRequestLimitReached"

    goto :goto_0

    .line 852
    :pswitch_d
    const-string v0, "WVStatus.BadURL"

    goto :goto_0

    .line 854
    :pswitch_e
    const-string v0, "WVStatus.FileNotPresent"

    goto :goto_0

    .line 856
    :pswitch_f
    const-string v0, "WVStatus.NotRegistered"

    goto :goto_0

    .line 858
    :pswitch_10
    const-string v0, "WVStatus.AlreadyRegistered"

    goto :goto_0

    .line 860
    :pswitch_11
    const-string v0, "WVStatus.NotPlaying"

    goto :goto_0

    .line 862
    :pswitch_12
    const-string v0, "WVStatus.AlreadyPlaying"

    goto :goto_0

    .line 864
    :pswitch_13
    const-string v0, "WVStatus.FileSystemError"

    goto :goto_0

    .line 866
    :pswitch_14
    const-string v0, "WVStatus.AssetDBWasCorrupted"

    goto :goto_0

    .line 868
    :pswitch_15
    const-string v0, "WVStatus.ClockTamperDetected"

    goto :goto_0

    .line 870
    :pswitch_16
    const-string v0, "WVStatus.MandatorySettingAbsent"

    goto :goto_0

    .line 872
    :pswitch_17
    const-string v0, "WVStatus.SystemCallError"

    goto :goto_0

    .line 874
    :pswitch_18
    const-string v0, "WVStatus.OutOfMemoryError"

    goto :goto_0

    .line 876
    :pswitch_19
    const-string v0, "WVStatus.TamperDetected"

    goto :goto_0

    .line 878
    :pswitch_1a
    const-string v0, "WVStatus.PendingServerNotification"

    goto :goto_0

    .line 880
    :pswitch_1b
    const-string v0, "WVStatus.HardwareIDAbsent"

    goto :goto_0

    .line 882
    :pswitch_1c
    const-string v0, "WVStatus.OutOfRange"

    goto :goto_0

    .line 884
    :pswitch_1d
    const-string v0, "WVStatus.HeartbeatError"

    goto :goto_0

    .line 824
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method private trackPlayback()V
    .locals 3

    .prologue
    .line 890
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flixster/android/drm/DrmManager;->getPlaybackTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/drm/DrmManager;->getPlaybackTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    return-void
.end method

.method private trackPlaybackEvent(Ljava/lang/String;)V
    .locals 7
    .parameter "eventLabel"

    .prologue
    .line 894
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flixster/android/drm/DrmManager;->getPlaybackTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/drm/DrmManager;->getPlaybackTitle()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Streaming"

    .line 895
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v4}, Lcom/flixster/android/utils/UrlHelper;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    move-object v5, p1

    .line 894
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 896
    return-void
.end method

.method private updateDrmCredentials()V
    .locals 3

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/flixster/android/drm/DrmManager;->getSettings()Ljava/util/HashMap;

    move-result-object v0

    .line 115
    .local v0, settings:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v1, v0}, Lcom/widevine/drmapi/android/WVPlayback;->setCredentials(Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    .line 117
    const-string v1, "FlxDrm"

    const-string v2, "DrmManager.updateDrmCredentials: proxy URL https://..."

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method private updateDrmCredentials(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "drmServerUrl"
    .parameter "deviceId"
    .parameter "streamId"

    .prologue
    .line 125
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DrmManager.updateDrmCredentials: proxy URL "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 127
    .local v0, settings:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "WVDRMServer"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    const-string v1, "WVPortalKey"

    sget-object v2, Lcom/flixster/android/drm/DrmManager;->PORTAL_KEYS:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    const-string v1, "WVAssetRootKey"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    const-string v1, "WVDeviceIDKey"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    if-eqz p3, :cond_0

    .line 132
    const-string v1, "WVStreamIDKey"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v1, v0}, Lcom/widevine/drmapi/android/WVPlayback;->setCredentials(Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    .line 135
    return-void
.end method


# virtual methods
.method public getPlaybackTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 900
    iget-boolean v0, p0, Lcom/flixster/android/drm/DrmManager;->isStreamingMode:Z

    if-eqz v0, :cond_0

    const-string v0, "/movie/play/streaming"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "/movie/play/offline"

    goto :goto_0
.end method

.method public getPlaybackTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 905
    new-instance v1, Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/flixster/android/drm/DrmManager;->isStreamingMode:Z

    if-eqz v0, :cond_0

    const-string v0, "Movie Play Streaming - "

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 906
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 905
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "Movie Play Offline - "

    goto :goto_0

    .line 906
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getPlayer()Lcom/flixster/android/drm/WidevinePlayer;
    .locals 1

    .prologue
    .line 916
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->player:Lcom/flixster/android/drm/WidevinePlayer;

    return-object v0
.end method

.method public initializeDRM(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 59
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager;->context:Landroid/content/Context;

    .line 61
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    if-nez v2, :cond_0

    .line 62
    new-instance v2, Lcom/widevine/drmapi/android/WVPlayback;

    invoke-direct {v2}, Lcom/widevine/drmapi/android/WVPlayback;-><init>()V

    iput-object v2, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    .line 64
    invoke-virtual {p0}, Lcom/flixster/android/drm/DrmManager;->isRooted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 65
    const-string v2, "FlxDrm"

    const-string v3, "WVPlayback.initialize failed on rooted device"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    invoke-direct {p0}, Lcom/flixster/android/drm/DrmManager;->getSettings()Ljava/util/HashMap;

    move-result-object v0

    .line 68
    .local v0, settings:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->context:Landroid/content/Context;

    new-instance v4, Lcom/flixster/android/drm/DrmManager$DrmEventListener;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/flixster/android/drm/DrmManager$DrmEventListener;-><init>(Lcom/flixster/android/drm/DrmManager;Lcom/flixster/android/drm/DrmManager$DrmEventListener;)V

    invoke-virtual {v2, v3, v0, v4}, Lcom/widevine/drmapi/android/WVPlayback;->initialize(Landroid/content/Context;Ljava/util/HashMap;Lcom/widevine/drmapi/android/WVEventListener;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v1

    .line 69
    .local v1, status:Lcom/widevine/drmapi/android/WVStatus;
    const-string v2, "FlxDrm"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "WVPlayback.initialize: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/flixster/android/drm/DrmManager;->statusToString(Lcom/widevine/drmapi/android/WVStatus;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/flixster/android/drm/DrmManager;->isInitialized:Z

    return v0
.end method

.method public isRooted()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->isRooted:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->isRooted:Ljava/lang/Boolean;

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->isRooted:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v0}, Lcom/widevine/drmapi/android/WVPlayback;->isRooted()Z

    move-result v0

    goto :goto_0
.end method

.method public isStreamingMode()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/flixster/android/drm/DrmManager;->isStreamingMode:Z

    return v0
.end method

.method public playMovie(Lnet/flixster/android/model/LockerRight;)V
    .locals 9
    .parameter "right"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 207
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    .line 208
    const-string v6, "FlxDrm"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DrmManager.playMovie "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    iget-wide v6, v6, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v6, v7}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedMovie(J)Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, downloadUri:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 212
    const-string v6, "file://"

    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x7

    invoke-virtual {v1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 214
    :cond_0
    if-eqz v1, :cond_2

    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    iget-wide v6, v6, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v6, v7}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v6

    if-nez v6, :cond_2

    move v2, v5

    .line 216
    .local v2, isLocalPlayback:Z
    :goto_0
    if-eqz v2, :cond_1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 217
    :cond_1
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager;->context:Landroid/content/Context;

    if-nez v6, :cond_3

    .line 243
    :goto_1
    return-void

    .end local v2           #isLocalPlayback:Z
    :cond_2
    move v2, v4

    .line 214
    goto :goto_0

    .line 221
    .restart local v2       #isLocalPlayback:Z
    :cond_3
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0134

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 222
    .local v3, loading:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/flixster/android/utils/ActivityHolder;->getTopLevelActivity()Landroid/app/Activity;

    move-result-object v0

    .line 223
    .local v0, activity:Landroid/app/Activity;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v6

    if-nez v6, :cond_4

    .line 224
    new-instance v6, Landroid/app/ProgressDialog;

    invoke-direct {v6, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    .line 225
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 226
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 227
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 228
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 229
    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->show()V

    .line 233
    .end local v0           #activity:Landroid/app/Activity;
    .end local v3           #loading:Ljava/lang/String;
    :cond_4
    if-eqz v2, :cond_5

    :goto_2
    iput-boolean v4, p0, Lcom/flixster/android/drm/DrmManager;->isStreamingMode:Z

    .line 234
    invoke-direct {p0}, Lcom/flixster/android/drm/DrmManager;->trackPlayback()V

    .line 235
    if-eqz v2, :cond_6

    .line 236
    iput-object v1, p0, Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;

    .line 237
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    invoke-direct {p0, v4}, Lcom/flixster/android/drm/DrmManager;->registerLocalAsset(Lnet/flixster/android/model/LockerRight;)V

    goto :goto_1

    :cond_5
    move v4, v5

    .line 233
    goto :goto_2

    .line 239
    :cond_6
    const-string v4, "stream_create"

    invoke-direct {p0, v4}, Lcom/flixster/android/drm/DrmManager;->trackPlaybackEvent(Ljava/lang/String;)V

    .line 240
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManager;->streamStartSuccessHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/flixster/android/drm/DrmManager;->errorHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isWifi()Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "high"

    :goto_3
    invoke-static {v5, v6, v7, v4}, Lnet/flixster/android/data/ProfileDao;->streamStart(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V

    goto :goto_1

    .line 241
    :cond_7
    const-string v4, "low"

    goto :goto_3
.end method

.method public registerAllLocalAssets(Ljava/util/List;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 926
    .local p1, rights:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const-string v1, "FlxDrm"

    const-string v2, "DrmManager.registerAllLocalAssets"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/drm/DrmManager;->downloadedAssetRights:Ljava/util/List;

    .line 928
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 936
    invoke-direct {p0}, Lcom/flixster/android/drm/DrmManager;->registerLocalAssetLooper()V

    .line 937
    return-void

    .line 928
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 929
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isMovie()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isEpisode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 930
    :cond_2
    invoke-static {v0}, Lcom/flixster/android/drm/DrmManager;->isMovieOrEpisodeDownloaded(Lnet/flixster/android/model/LockerRight;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 931
    const-string v2, "FlxDrm"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DrmManager.registerAllLocalAssets found downloaded asset "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->downloadedAssetRights:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public registerAssetOnDownloadComplete(Lnet/flixster/android/model/LockerRight;)V
    .locals 6
    .parameter "mockRight"

    .prologue
    .line 139
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    .line 140
    iget-boolean v2, p0, Lcom/flixster/android/drm/DrmManager;->isInitialized:Z

    if-nez v2, :cond_1

    .line 141
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/flixster/android/drm/DrmManager;->isInitializedOnDemand:Z

    .line 142
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/flixster/android/drm/DrmManager;->initializeDRM(Landroid/content/Context;)V

    .line 147
    :goto_0
    new-instance v1, Lcom/flixster/android/drm/DownloadRight;

    iget-wide v2, p1, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-direct {v1, v2, v3}, Lcom/flixster/android/drm/DownloadRight;-><init>(J)V

    .line 148
    .local v1, right:Lcom/flixster/android/drm/DownloadRight;
    invoke-virtual {v1}, Lcom/flixster/android/drm/DownloadRight;->getQueueId()Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, downloadQueueId:Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 150
    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/flixster/android/drm/DownloadRight;->setQueueId(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;

    .line 151
    invoke-virtual {v1}, Lcom/flixster/android/drm/DownloadRight;->save()V

    .line 152
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->endDownloadSuccessHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;

    iget-wide v4, p1, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v2, v3, v4, v5, v0}, Lnet/flixster/android/data/ProfileDao;->endDownload(Landroid/os/Handler;Landroid/os/Handler;JLjava/lang/String;)V

    .line 154
    :cond_0
    return-void

    .line 144
    .end local v0           #downloadQueueId:Ljava/lang/String;
    .end local v1           #right:Lcom/flixster/android/drm/DownloadRight;
    :cond_1
    invoke-direct {p0, p1}, Lcom/flixster/android/drm/DrmManager;->registerLocalAsset(Lnet/flixster/android/model/LockerRight;)V

    goto :goto_0
.end method

.method public setPlayer(Lcom/flixster/android/drm/WidevinePlayer;)V
    .locals 0
    .parameter "player"

    .prologue
    .line 911
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager;->player:Lcom/flixster/android/drm/WidevinePlayer;

    .line 912
    return-void
.end method

.method public streamResume()V
    .locals 4

    .prologue
    .line 259
    const-string v0, "FlxDrm"

    const-string v1, "DrmManager.streamResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v0, "stream_resume"

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManager;->trackPlaybackEvent(Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->streamResumeSuccessHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    .line 264
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isWifi()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "high"

    .line 263
    :goto_0
    invoke-static {v1, v2, v3, v0}, Lnet/flixster/android/data/ProfileDao;->streamStart(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V

    .line 268
    :goto_1
    return-void

    .line 264
    :cond_0
    const-string v0, "low"

    goto :goto_0

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->uvStreamResumeSuccessHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    invoke-static {v0, v1, v2}, Lnet/flixster/android/data/ProfileDao;->uvStreamCreate(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V

    goto :goto_1
.end method

.method public streamStop(I)V
    .locals 5
    .parameter "playPosition"

    .prologue
    .line 247
    const-string v0, "FlxDrm"

    const-string v1, "DrmManager.streamStop"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v0, "stream_delete"

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManager;->trackPlaybackEvent(Ljava/lang/String;)V

    .line 250
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->streamStopSuccessHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;

    .line 251
    if-ltz p1, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 250
    :goto_0
    invoke-static {v1, v2, v3, v4, v0}, Lnet/flixster/android/data/ProfileDao;->streamStop(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->uvStreamDeleteSuccessHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->streamId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lnet/flixster/android/data/ProfileDao;->uvStreamDelete(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    .line 255
    :cond_0
    return-void

    .line 251
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public terminateDRM()V
    .locals 4

    .prologue
    .line 76
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVPlayback;->terminate()Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    .line 78
    .local v0, status:Lcom/widevine/drmapi/android/WVStatus;
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WVPlayback.terminate: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManager;->statusToString(Lcom/widevine/drmapi/android/WVStatus;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    .line 81
    .end local v0           #status:Lcom/widevine/drmapi/android/WVStatus;
    :cond_0
    return-void
.end method

.method public unregisterLocalAsset(Lnet/flixster/android/model/LockerRight;)V
    .locals 1
    .parameter "right"

    .prologue
    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;

    .line 203
    return-void
.end method

.method public updatePlaybackPosition(I)V
    .locals 4
    .parameter "playPosition"

    .prologue
    .line 272
    const-string v0, "FlxDrm"

    const-string v1, "DrmManager.updatePlaybackPosition"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManager;->updatePlaybackPositionSuccessHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->silentErrorHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    .line 274
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 273
    invoke-static {v0, v1, v2, v3}, Lnet/flixster/android/data/ProfileDao;->updatePlaybackPosition(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V

    .line 275
    return-void
.end method

.method protected wvPlay(I)V
    .locals 6
    .parameter "playPosition"

    .prologue
    const/4 v5, -0x1

    .line 408
    const/4 v2, 0x0

    .line 409
    .local v2, localUri:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 410
    :cond_0
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_1

    .line 412
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    :cond_1
    :goto_0
    iget-boolean v3, p0, Lcom/flixster/android/drm/DrmManager;->isStreamingMode:Z

    if-eqz v3, :cond_2

    .line 418
    invoke-virtual {p0, v5}, Lcom/flixster/android/drm/DrmManager;->streamStop(I)V

    .line 462
    :cond_2
    :goto_1
    return-void

    .line 413
    :catch_0
    move-exception v0

    .line 414
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 422
    .end local v0           #e:Ljava/lang/Exception;
    :cond_3
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/widevine/drmapi/android/WVPlayback;->play(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 423
    if-nez v2, :cond_5

    .line 424
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_4

    .line 426
    :try_start_1
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 431
    :cond_4
    :goto_2
    iget-boolean v3, p0, Lcom/flixster/android/drm/DrmManager;->isStreamingMode:Z

    if-eqz v3, :cond_2

    .line 432
    invoke-virtual {p0, v5}, Lcom/flixster/android/drm/DrmManager;->streamStop(I)V

    goto :goto_1

    .line 427
    :catch_1
    move-exception v0

    .line 428
    .restart local v0       #e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 437
    .end local v0           #e:Ljava/lang/Exception;
    :cond_5
    const-string v3, "FlxDrm"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "WVPlayback.play: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_6

    .line 441
    :try_start_2
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 447
    :cond_6
    :goto_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 448
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 449
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "video/*"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 450
    const/high16 v3, 0x1000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 451
    const-string v3, "KEY_RIGHT_ID"

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager;->r:Lnet/flixster/android/model/LockerRight;

    iget-wide v4, v4, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 452
    const-string v3, "KEY_PLAY_POSITION"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 453
    const-string v3, "KEY_CAPTIONS_URI"

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManager;->captionsUri:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 460
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->context:Landroid/content/Context;

    const-class v4, Lcom/flixster/android/drm/WidevinePlayer;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 461
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->context:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 442
    .end local v1           #intent:Landroid/content/Intent;
    :catch_2
    move-exception v0

    .line 443
    .restart local v0       #e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method protected wvQuery(Ljava/lang/String;)V
    .locals 4
    .parameter "assetUri"

    .prologue
    .line 398
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v1, p1}, Lcom/widevine/drmapi/android/WVPlayback;->queryAssetStatus(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    .line 399
    .local v0, status:Lcom/widevine/drmapi/android/WVStatus;
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WVPlayback.queryAssetStatus: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManager;->statusToString(Lcom/widevine/drmapi/android/WVStatus;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    return-void
.end method

.method protected wvRegister(Ljava/lang/String;)V
    .locals 4
    .parameter "assetUri"

    .prologue
    .line 387
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v1, p1}, Lcom/widevine/drmapi/android/WVPlayback;->registerAsset(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    .line 388
    .local v0, status:Lcom/widevine/drmapi/android/WVStatus;
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WVPlayback.registerAsset: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManager;->statusToString(Lcom/widevine/drmapi/android/WVStatus;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManager;->downloadAssetUri:Ljava/lang/String;

    .line 390
    return-void
.end method

.method protected wvRequestLicense(Ljava/lang/String;)V
    .locals 4
    .parameter "assetUri"

    .prologue
    .line 403
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v1, p1}, Lcom/widevine/drmapi/android/WVPlayback;->requestLicense(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    .line 404
    .local v0, status:Lcom/widevine/drmapi/android/WVStatus;
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WVPlayback.requestLicense: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManager;->statusToString(Lcom/widevine/drmapi/android/WVStatus;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    return-void
.end method

.method public wvResume()Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 466
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 474
    :cond_0
    :goto_0
    return-object v1

    .line 469
    :cond_1
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManager;->playbackAssetUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/widevine/drmapi/android/WVPlayback;->play(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 470
    .local v0, localUri:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 473
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WVPlayback.play: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public wvStop()V
    .locals 4

    .prologue
    .line 479
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    if-nez v1, :cond_0

    .line 484
    :goto_0
    return-void

    .line 482
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVPlayback;->stop()Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    .line 483
    .local v0, status:Lcom/widevine/drmapi/android/WVStatus;
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WVPlayback.stop: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManager;->statusToString(Lcom/widevine/drmapi/android/WVStatus;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected wvUnregister(Ljava/lang/String;)V
    .locals 4
    .parameter "assetUri"

    .prologue
    .line 393
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManager;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v1, p1}, Lcom/widevine/drmapi/android/WVPlayback;->unregisterAsset(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    .line 394
    .local v0, status:Lcom/widevine/drmapi/android/WVStatus;
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WVPlayback.unregisterAsset: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManager;->statusToString(Lcom/widevine/drmapi/android/WVStatus;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    return-void
.end method
