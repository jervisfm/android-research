.class Lcom/flixster/android/drm/DrmManagerHc$1;
.super Landroid/os/Handler;
.source "DrmManagerHc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/DrmManagerHc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManagerHc;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/DrmManagerHc;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    .line 264
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .parameter "msg"

    .prologue
    .line 267
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/util/HashMap;

    .line 268
    .local v2, params:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    const-string v3, "fileLocation"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #calls: Lcom/flixster/android/drm/DrmManagerHc;->convertWidevineProtocol(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$0(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    #setter for: Lcom/flixster/android/drm/DrmManagerHc;->assetUri:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$1(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V

    .line 269
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    const-string v3, "playPosition"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    #setter for: Lcom/flixster/android/drm/DrmManagerHc;->playbackPosition:I
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$2(Lcom/flixster/android/drm/DrmManagerHc;I)V

    .line 270
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    const-string v3, "closedCaptionFileLocation"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManagerHc;->captionsUri:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$3(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V

    .line 272
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$4(Lcom/flixster/android/drm/DrmManagerHc;)Lnet/flixster/android/model/LockerRight;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 273
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    const-string v3, "streamId"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$5(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V

    .line 274
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    const-string v3, "customData"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManagerHc;->customData:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$6(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V

    .line 276
    const-string v3, "drmServerUrl"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 277
    .local v1, drmServerUrl:Ljava/lang/String;
    const-string v3, "deviceId"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 278
    .local v0, deviceId:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;
    invoke-static {v5}, Lcom/flixster/android/drm/DrmManagerHc;->access$7(Lcom/flixster/android/drm/DrmManagerHc;)Ljava/lang/String;

    move-result-object v5

    #calls: Lcom/flixster/android/drm/DrmManagerHc;->getDrmInfoRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/drm/DrmInfoRequest;
    invoke-static {v4, v1, v5, v0}, Lcom/flixster/android/drm/DrmManagerHc;->access$8(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/drm/DrmInfoRequest;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->playbackPosition:I
    invoke-static {v5}, Lcom/flixster/android/drm/DrmManagerHc;->access$9(Lcom/flixster/android/drm/DrmManagerHc;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/flixster/android/drm/DrmManagerHc;->wvPlay(Landroid/drm/DrmInfoRequest;I)V

    .line 282
    .end local v0           #deviceId:Ljava/lang/String;
    .end local v1           #drmServerUrl:Ljava/lang/String;
    :goto_0
    return-void

    .line 280
    :cond_0
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->uvStreamCreateSuccessHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$10(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->errorHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/flixster/android/drm/DrmManagerHc;->access$11(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc$1;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v5}, Lcom/flixster/android/drm/DrmManagerHc;->access$4(Lcom/flixster/android/drm/DrmManagerHc;)Lnet/flixster/android/model/LockerRight;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lnet/flixster/android/data/ProfileDao;->uvStreamCreate(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V

    goto :goto_0
.end method
