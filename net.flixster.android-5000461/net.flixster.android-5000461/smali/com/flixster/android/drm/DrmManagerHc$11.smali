.class Lcom/flixster/android/drm/DrmManagerHc$11;
.super Landroid/os/Handler;
.source "DrmManagerHc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/DrmManagerHc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManagerHc;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/DrmManagerHc;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc$11;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    .line 354
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 356
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/ActivityHolder;->getTopLevelActivity()Landroid/app/Activity;

    move-result-object v0

    .line 357
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 373
    :cond_0
    :goto_0
    return-void

    .line 360
    :cond_1
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc$11;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    const-string v3, "api_error"

    #calls: Lcom/flixster/android/drm/DrmManagerHc;->trackPlaybackEvent(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$14(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V

    .line 361
    const-string v2, "FlxDrm"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DrmManagerHc.errorHandler: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc$11;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/flixster/android/drm/DrmManagerHc;->access$15(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 364
    :try_start_0
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc$11;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/flixster/android/drm/DrmManagerHc;->access$15(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    :cond_2
    :goto_1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v2, v2, Lnet/flixster/android/data/DaoException;

    if-eqz v2, :cond_0

    .line 371
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lnet/flixster/android/data/DaoException;

    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/utils/ActivityHolder;->getTopLevelActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;I)V

    goto :goto_0

    .line 365
    :catch_0
    move-exception v1

    .line 366
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
