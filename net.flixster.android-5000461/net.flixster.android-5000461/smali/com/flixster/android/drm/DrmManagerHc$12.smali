.class Lcom/flixster/android/drm/DrmManagerHc$12;
.super Ljava/lang/Object;
.source "DrmManagerHc.java"

# interfaces
.implements Landroid/drm/DrmManagerClient$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/drm/DrmManagerHc;->initializeDRM(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManagerHc;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/DrmManagerHc;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc$12;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/drm/DrmManagerClient;Landroid/drm/DrmInfoEvent;)V
    .locals 2
    .parameter "client"
    .parameter "event"

    .prologue
    .line 64
    invoke-virtual {p2}, Landroid/drm/DrmInfoEvent;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 65
    const-string v0, "FlxDrm"

    const-string v1, "Rights installed"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_0
    return-void
.end method
