.class Lcom/flixster/android/drm/DrmManagerHc$13;
.super Ljava/lang/Object;
.source "DrmManagerHc.java"

# interfaces
.implements Landroid/drm/DrmManagerClient$OnEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/drm/DrmManagerHc;->initializeDRM(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManagerHc;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/DrmManagerHc;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc$13;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/drm/DrmManagerClient;Landroid/drm/DrmEvent;)V
    .locals 5
    .parameter "client"
    .parameter "event"

    .prologue
    .line 72
    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 74
    :pswitch_0
    const-string v0, "FlxDrm"

    const-string v1, "Info Processed\n"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc$13;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lcom/flixster/android/drm/DrmManagerHc;->access$4(Lcom/flixster/android/drm/DrmManagerHc;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc$13;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->trackLicenseSuccessHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/flixster/android/drm/DrmManagerHc;->access$16(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc$13;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->silentErrorHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/drm/DrmManagerHc;->access$17(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc$13;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lcom/flixster/android/drm/DrmManagerHc;->access$4(Lcom/flixster/android/drm/DrmManagerHc;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManagerHc$13;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$7(Lcom/flixster/android/drm/DrmManagerHc;)Ljava/lang/String;

    move-result-object v3

    .line 77
    const/4 v4, 0x1

    .line 76
    invoke-static {v0, v1, v2, v3, v4}, Lnet/flixster/android/data/ProfileDao;->trackLicense(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Z)V

    .line 78
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc$13;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->ackLicenseSuccessHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/flixster/android/drm/DrmManagerHc;->access$18(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc$13;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->silentErrorHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/drm/DrmManagerHc;->access$17(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc$13;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->customData:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/drm/DrmManagerHc;->access$13(Lcom/flixster/android/drm/DrmManagerHc;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lnet/flixster/android/data/ProfileDao;->ackLicense(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x3ea
        :pswitch_0
    .end packed-switch
.end method
