.class Lcom/flixster/android/drm/DrmManagerHc$14;
.super Ljava/lang/Object;
.source "DrmManagerHc.java"

# interfaces
.implements Landroid/drm/DrmManagerClient$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/drm/DrmManagerHc;->initializeDRM(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManagerHc;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/DrmManagerHc;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc$14;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/drm/DrmManagerClient;Landroid/drm/DrmErrorEvent;)V
    .locals 6
    .parameter "client"
    .parameter "event"

    .prologue
    .line 87
    invoke-virtual {p2}, Landroid/drm/DrmErrorEvent;->getType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 124
    :goto_0
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DrmErrorEvent message: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/drm/DrmErrorEvent;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-void

    .line 89
    :sswitch_0
    const-string v1, "FlxDrm"

    const-string v2, "Remove All Rights failed\n"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :sswitch_1
    const-string v1, "FlxDrm"

    const-string v2, "Info Processed failed\n"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :sswitch_2
    const-string v1, "FlxDrm"

    const-string v2, "No Internet Connection\n"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :sswitch_3
    const-string v1, "FlxDrm"

    const-string v2, "Not Supported\n"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :sswitch_4
    const-string v1, "FlxDrm"

    const-string v2, "Out of Memory\n"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :sswitch_5
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc$14;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->player:Lcom/flixster/android/drm/WidevinePlayer;
    invoke-static {v1}, Lcom/flixster/android/drm/DrmManagerHc;->access$19(Lcom/flixster/android/drm/DrmManagerHc;)Lcom/flixster/android/drm/WidevinePlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 105
    sget-object v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->LICENSE:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    .line 106
    .local v0, error:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc$14;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->player:Lcom/flixster/android/drm/WidevinePlayer;
    invoke-static {v1}, Lcom/flixster/android/drm/DrmManagerHc;->access$19(Lcom/flixster/android/drm/DrmManagerHc;)Lcom/flixster/android/drm/WidevinePlayer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/flixster/android/drm/WidevinePlayer;->notifyPlaybackFailure(Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;Ljava/lang/String;)V

    .line 108
    .end local v0           #error:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc$14;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lcom/flixster/android/drm/DrmManagerHc;->access$4(Lcom/flixster/android/drm/DrmManagerHc;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 109
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc$14;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->trackLicenseSuccessHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/drm/DrmManagerHc;->access$16(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc$14;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->silentErrorHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/flixster/android/drm/DrmManagerHc;->access$17(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManagerHc$14;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;
    invoke-static {v3}, Lcom/flixster/android/drm/DrmManagerHc;->access$4(Lcom/flixster/android/drm/DrmManagerHc;)Lnet/flixster/android/model/LockerRight;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc$14;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;
    invoke-static {v4}, Lcom/flixster/android/drm/DrmManagerHc;->access$7(Lcom/flixster/android/drm/DrmManagerHc;)Ljava/lang/String;

    move-result-object v4

    .line 110
    const/4 v5, 0x0

    .line 109
    invoke-static {v1, v2, v3, v4, v5}, Lnet/flixster/android/data/ProfileDao;->trackLicense(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Z)V

    .line 112
    :cond_1
    const-string v1, "FlxDrm"

    const-string v2, "Process DRM Info failed\n"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 115
    :sswitch_6
    const-string v1, "FlxDrm"

    const-string v2, "Remove all rights\n"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 118
    :sswitch_7
    const-string v1, "FlxDrm"

    const-string v2, "Rights not installed\n"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 121
    :sswitch_8
    const-string v1, "FlxDrm"

    const-string v2, "Rights renewal not allowed\n"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 87
    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_1
        0x7d1 -> :sswitch_7
        0x7d2 -> :sswitch_8
        0x7d3 -> :sswitch_3
        0x7d4 -> :sswitch_4
        0x7d5 -> :sswitch_2
        0x7d6 -> :sswitch_5
        0x7d7 -> :sswitch_6
    .end sparse-switch
.end method
