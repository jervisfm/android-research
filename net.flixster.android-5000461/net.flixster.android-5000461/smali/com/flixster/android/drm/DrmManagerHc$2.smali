.class Lcom/flixster/android/drm/DrmManagerHc$2;
.super Landroid/os/Handler;
.source "DrmManagerHc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/DrmManagerHc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManagerHc;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/DrmManagerHc;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc$2;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    .line 285
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 288
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc$2;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/flixster/android/drm/DrmManagerHc;->access$5(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V

    .line 291
    const-string v0, "FlxDrm"

    const-string v1, "DrmManagerHc.streamCreateSuccessHandler: proxy URL https://..."

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc$2;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc$2;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #calls: Lcom/flixster/android/drm/DrmManagerHc;->getDrmInfoRequest()Landroid/drm/DrmInfoRequest;
    invoke-static {v1}, Lcom/flixster/android/drm/DrmManagerHc;->access$12(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/drm/DrmInfoRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc$2;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    #getter for: Lcom/flixster/android/drm/DrmManagerHc;->playbackPosition:I
    invoke-static {v2}, Lcom/flixster/android/drm/DrmManagerHc;->access$9(Lcom/flixster/android/drm/DrmManagerHc;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/drm/DrmManagerHc;->wvPlay(Landroid/drm/DrmInfoRequest;I)V

    .line 298
    return-void
.end method
