.class Lcom/flixster/android/drm/DrmManagerHc$4;
.super Landroid/os/Handler;
.source "DrmManagerHc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/DrmManagerHc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/DrmManagerHc;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/DrmManagerHc;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc$4;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    .line 307
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 310
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/HashMap;

    .line 311
    .local v0, params:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc$4;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    const-string v1, "fileLocation"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManagerHc;->assetUri:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/flixster/android/drm/DrmManagerHc;->access$1(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V

    .line 312
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc$4;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    const-string v1, "streamId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/flixster/android/drm/DrmManagerHc;->access$5(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V

    .line 313
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc$4;->this$0:Lcom/flixster/android/drm/DrmManagerHc;

    const-string v1, "customData"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    #setter for: Lcom/flixster/android/drm/DrmManagerHc;->customData:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/flixster/android/drm/DrmManagerHc;->access$6(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V

    .line 314
    return-void
.end method
