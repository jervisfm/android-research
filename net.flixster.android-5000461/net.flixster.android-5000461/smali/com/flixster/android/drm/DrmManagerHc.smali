.class public Lcom/flixster/android/drm/DrmManagerHc;
.super Ljava/lang/Object;
.source "DrmManagerHc.java"

# interfaces
.implements Lcom/flixster/android/drm/PlaybackManager;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final PORTAL_KEYS:[Ljava/lang/String;


# instance fields
.field private final ackLicenseSuccessHandler:Landroid/os/Handler;

.field private assetUri:Ljava/lang/String;

.field private captionsUri:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private customData:Ljava/lang/String;

.field private dialog:Landroid/app/ProgressDialog;

.field private drmManagerClient:Landroid/drm/DrmManagerClient;

.field private final errorHandler:Landroid/os/Handler;

.field private playbackPosition:I

.field private player:Lcom/flixster/android/drm/WidevinePlayer;

.field private r:Lnet/flixster/android/model/LockerRight;

.field private final silentErrorHandler:Landroid/os/Handler;

.field private streamId:Ljava/lang/String;

.field private final streamResumeSuccessHandler:Landroid/os/Handler;

.field private final streamStartSuccessHandler:Landroid/os/Handler;

.field private final streamStopSuccessHandler:Landroid/os/Handler;

.field private final trackLicenseSuccessHandler:Landroid/os/Handler;

.field private final updatePlaybackPositionSuccessHandler:Landroid/os/Handler;

.field private final uvStreamCreateSuccessHandler:Landroid/os/Handler;

.field private final uvStreamDeleteSuccessHandler:Landroid/os/Handler;

.field private final uvStreamResumeSuccessHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "warnerbros"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CN"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "IDM"

    aput-object v2, v0, v1

    sput-object v0, Lcom/flixster/android/drm/DrmManagerHc;->PORTAL_KEYS:[Ljava/lang/String;

    .line 39
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 264
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$1;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamStartSuccessHandler:Landroid/os/Handler;

    .line 285
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$2;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->uvStreamCreateSuccessHandler:Landroid/os/Handler;

    .line 301
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$3;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamStopSuccessHandler:Landroid/os/Handler;

    .line 307
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$4;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamResumeSuccessHandler:Landroid/os/Handler;

    .line 317
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$5;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$5;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->uvStreamDeleteSuccessHandler:Landroid/os/Handler;

    .line 323
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$6;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$6;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->uvStreamResumeSuccessHandler:Landroid/os/Handler;

    .line 330
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$7;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$7;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->updatePlaybackPositionSuccessHandler:Landroid/os/Handler;

    .line 336
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$8;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$8;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->trackLicenseSuccessHandler:Landroid/os/Handler;

    .line 342
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$9;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$9;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->ackLicenseSuccessHandler:Landroid/os/Handler;

    .line 348
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$10;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$10;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->silentErrorHandler:Landroid/os/Handler;

    .line 354
    new-instance v0, Lcom/flixster/android/drm/DrmManagerHc$11;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/DrmManagerHc$11;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->errorHandler:Landroid/os/Handler;

    .line 39
    return-void
.end method

.method static synthetic access$0(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 420
    invoke-static {p0}, Lcom/flixster/android/drm/DrmManagerHc;->convertWidevineProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc;->assetUri:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$10(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 285
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->uvStreamCreateSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$11(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 354
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->errorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$12(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/drm/DrmInfoRequest;
    .locals 1
    .parameter

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/flixster/android/drm/DrmManagerHc;->getDrmInfoRequest()Landroid/drm/DrmInfoRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$13(Lcom/flixster/android/drm/DrmManagerHc;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->customData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$14(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 444
    invoke-direct {p0, p1}, Lcom/flixster/android/drm/DrmManagerHc;->trackPlaybackEvent(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$15(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$16(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 336
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->trackLicenseSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$17(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 348
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->silentErrorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$18(Lcom/flixster/android/drm/DrmManagerHc;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 342
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->ackLicenseSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$19(Lcom/flixster/android/drm/DrmManagerHc;)Lcom/flixster/android/drm/WidevinePlayer;
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->player:Lcom/flixster/android/drm/WidevinePlayer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/drm/DrmManagerHc;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52
    iput p1, p0, Lcom/flixster/android/drm/DrmManagerHc;->playbackPosition:I

    return-void
.end method

.method static synthetic access$3(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc;->captionsUri:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/flixster/android/drm/DrmManagerHc;)Lnet/flixster/android/model/LockerRight;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc;->customData:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$7(Lcom/flixster/android/drm/DrmManagerHc;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8(Lcom/flixster/android/drm/DrmManagerHc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/drm/DrmInfoRequest;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 196
    invoke-direct {p0, p1, p2, p3}, Lcom/flixster/android/drm/DrmManagerHc;->getDrmInfoRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/drm/DrmInfoRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9(Lcom/flixster/android/drm/DrmManagerHc;)I
    .locals 1
    .parameter

    .prologue
    .line 52
    iget v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->playbackPosition:I

    return v0
.end method

.method private static convertWidevineProtocol(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "url"

    .prologue
    .line 421
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v0

    const/16 v1, 0xd

    if-lt v0, v1, :cond_0

    .line 422
    const-string v0, "http:"

    const-string v1, "widevine:"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 424
    .end local p0
    :cond_0
    return-object p0
.end method

.method private getDrmInfoRequest()Landroid/drm/DrmInfoRequest;
    .locals 5

    .prologue
    .line 169
    new-instance v0, Landroid/drm/DrmInfoRequest;

    const/4 v2, 0x3

    .line 170
    const-string v3, "video/wvm"

    .line 169
    invoke-direct {v0, v2, v3}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 172
    .local v0, rightsAcquisitionInfo:Landroid/drm/DrmInfoRequest;
    const-string v2, "WVDRMServerKey"

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;

    invoke-static {v3, v4}, Lnet/flixster/android/data/ApiBuilder;->widevineLicense(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 173
    const-string v2, "WVAssetURIKey"

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManagerHc;->assetUri:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 174
    const-string v2, "WVPortalKey"

    sget-object v3, Lcom/flixster/android/drm/DrmManagerHc;->PORTAL_KEYS:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 175
    const-string v2, "WVLicenseTypeKey"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 177
    invoke-static {}, Lcom/flixster/android/drm/DrmManagerHc;->getWvDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, wvDeviceId:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 179
    const-string v2, "FlxDrm"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DrmManagerHc.getDrmInfoRequest WVDeviceIDKey = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v2, "WVDeviceIDKey"

    invoke-virtual {v0, v2, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 183
    :cond_0
    return-object v0
.end method

.method private getDrmInfoRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/drm/DrmInfoRequest;
    .locals 4
    .parameter "drmServerUrl"
    .parameter "streamId"
    .parameter "deviceId"

    .prologue
    .line 197
    new-instance v0, Landroid/drm/DrmInfoRequest;

    const/4 v1, 0x3

    .line 198
    const-string v2, "video/wvm"

    .line 197
    invoke-direct {v0, v1, v2}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 199
    .local v0, rightsAcquisitionInfo:Landroid/drm/DrmInfoRequest;
    const-string v1, "WVDRMServerKey"

    invoke-virtual {v0, v1, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 200
    const-string v1, "WVAssetURIKey"

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->assetUri:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 201
    const-string v1, "WVPortalKey"

    sget-object v2, Lcom/flixster/android/drm/DrmManagerHc;->PORTAL_KEYS:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 202
    const-string v1, "WVLicenseTypeKey"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 203
    const-string v1, "WVStreamIDKey"

    invoke-virtual {v0, v1, p2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 204
    const-string v1, "WVDeviceIDKey"

    invoke-virtual {v0, v1, p3}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 205
    return-object v0
.end method

.method private static getWvDeviceId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 187
    const/4 v0, 0x0

    .line 188
    .local v0, id:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v1

    .line 189
    .local v1, user:Lnet/flixster/android/model/User;
    if-eqz v1, :cond_0

    .line 190
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lnet/flixster/android/model/User;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "@WVM-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 192
    :cond_0
    return-object v0
.end method

.method private trackPlayback()V
    .locals 3

    .prologue
    .line 441
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flixster/android/drm/DrmManagerHc;->getPlaybackTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/drm/DrmManagerHc;->getPlaybackTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    return-void
.end method

.method private trackPlaybackEvent(Ljava/lang/String;)V
    .locals 7
    .parameter "eventLabel"

    .prologue
    .line 445
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flixster/android/drm/DrmManagerHc;->getPlaybackTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/drm/DrmManagerHc;->getPlaybackTitle()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Streaming"

    .line 446
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v4}, Lcom/flixster/android/utils/UrlHelper;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    move-object v5, p1

    .line 445
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 447
    return-void
.end method


# virtual methods
.method public getPlaybackTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 451
    const-string v0, "/movie/play/streaming"

    return-object v0
.end method

.method public getPlaybackTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 456
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Movie Play Streaming - "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPlayer()Lcom/flixster/android/drm/WidevinePlayer;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->player:Lcom/flixster/android/drm/WidevinePlayer;

    return-object v0
.end method

.method public initializeDRM(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 56
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc;->context:Landroid/content/Context;

    .line 58
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->drmManagerClient:Landroid/drm/DrmManagerClient;

    if-nez v0, :cond_0

    .line 59
    const-string v0, "FlxDrm"

    const-string v1, "DrmManagerHc.initializeDRM: new DrmManagerClient"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    new-instance v0, Landroid/drm/DrmManagerClient;

    invoke-direct {v0, p1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->drmManagerClient:Landroid/drm/DrmManagerClient;

    .line 62
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->drmManagerClient:Landroid/drm/DrmManagerClient;

    new-instance v1, Lcom/flixster/android/drm/DrmManagerHc$12;

    invoke-direct {v1, p0}, Lcom/flixster/android/drm/DrmManagerHc$12;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    invoke-virtual {v0, v1}, Landroid/drm/DrmManagerClient;->setOnInfoListener(Landroid/drm/DrmManagerClient$OnInfoListener;)V

    .line 70
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->drmManagerClient:Landroid/drm/DrmManagerClient;

    new-instance v1, Lcom/flixster/android/drm/DrmManagerHc$13;

    invoke-direct {v1, p0}, Lcom/flixster/android/drm/DrmManagerHc$13;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    invoke-virtual {v0, v1}, Landroid/drm/DrmManagerClient;->setOnEventListener(Landroid/drm/DrmManagerClient$OnEventListener;)V

    .line 85
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->drmManagerClient:Landroid/drm/DrmManagerClient;

    new-instance v1, Lcom/flixster/android/drm/DrmManagerHc$14;

    invoke-direct {v1, p0}, Lcom/flixster/android/drm/DrmManagerHc$14;-><init>(Lcom/flixster/android/drm/DrmManagerHc;)V

    invoke-virtual {v0, v1}, Landroid/drm/DrmManagerClient;->setOnErrorListener(Landroid/drm/DrmManagerClient$OnErrorListener;)V

    .line 128
    :cond_0
    return-void
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    return v0
.end method

.method public isRooted()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public isStreamingMode()Z
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x1

    return v0
.end method

.method public playMovie(Lnet/flixster/android/model/LockerRight;)V
    .locals 6
    .parameter "right"

    .prologue
    const/4 v5, 0x0

    .line 210
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    .line 211
    const-string v2, "FlxDrm"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DrmManagerHc.playMovie "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->context:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 231
    :goto_0
    return-void

    .line 217
    :cond_0
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0134

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 218
    .local v1, loading:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/ActivityHolder;->getTopLevelActivity()Landroid/app/Activity;

    move-result-object v0

    .line 219
    .local v0, activity:Landroid/app/Activity;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 220
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    .line 221
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 223
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 224
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 225
    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 228
    :cond_1
    invoke-direct {p0}, Lcom/flixster/android/drm/DrmManagerHc;->trackPlayback()V

    .line 229
    const-string v2, "stream_create"

    invoke-direct {p0, v2}, Lcom/flixster/android/drm/DrmManagerHc;->trackPlaybackEvent(Ljava/lang/String;)V

    .line 230
    iget-object v3, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamStartSuccessHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc;->errorHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "low"

    :goto_1
    invoke-static {v3, v4, v5, v2}, Lnet/flixster/android/data/ProfileDao;->streamStart(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "adaptive"

    goto :goto_1
.end method

.method public registerAllLocalAssets(Ljava/util/List;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, rights:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public registerAssetOnDownloadComplete(Lnet/flixster/android/model/LockerRight;)V
    .locals 1
    .parameter "right"

    .prologue
    .line 152
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setPlayer(Lcom/flixster/android/drm/WidevinePlayer;)V
    .locals 0
    .parameter "player"

    .prologue
    .line 461
    iput-object p1, p0, Lcom/flixster/android/drm/DrmManagerHc;->player:Lcom/flixster/android/drm/WidevinePlayer;

    .line 462
    return-void
.end method

.method public streamResume()V
    .locals 4

    .prologue
    .line 247
    const-string v0, "FlxDrm"

    const-string v1, "DrmManagerHc.streamResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v0, "stream_resume"

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManagerHc;->trackPlaybackEvent(Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamResumeSuccessHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc;->silentErrorHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    const-string v3, "adaptive"

    invoke-static {v0, v1, v2, v3}, Lnet/flixster/android/data/ProfileDao;->streamStart(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V

    .line 255
    :goto_0
    return-void

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->uvStreamResumeSuccessHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc;->silentErrorHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    invoke-static {v0, v1, v2}, Lnet/flixster/android/data/ProfileDao;->uvStreamCreate(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V

    goto :goto_0
.end method

.method public streamStop(I)V
    .locals 5
    .parameter "playPosition"

    .prologue
    .line 235
    const-string v0, "FlxDrm"

    const-string v1, "DrmManagerHc.streamStop"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v0, "stream_delete"

    invoke-direct {p0, v0}, Lcom/flixster/android/drm/DrmManagerHc;->trackPlaybackEvent(Ljava/lang/String;)V

    .line 238
    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamStopSuccessHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->silentErrorHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;

    .line 239
    if-ltz p1, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 238
    :goto_0
    invoke-static {v1, v2, v3, v4, v0}, Lnet/flixster/android/data/ProfileDao;->streamStop(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->uvStreamDeleteSuccessHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc;->silentErrorHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->streamId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lnet/flixster/android/data/ProfileDao;->uvStreamDelete(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    .line 243
    :cond_0
    return-void

    .line 239
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public terminateDRM()V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public unregisterLocalAsset(Lnet/flixster/android/model/LockerRight;)V
    .locals 1
    .parameter "right"

    .prologue
    .line 158
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public updatePlaybackPosition(I)V
    .locals 4
    .parameter "playPosition"

    .prologue
    .line 259
    const-string v0, "FlxDrm"

    const-string v1, "DrmManagerHc.updatePlaybackPosition"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/flixster/android/drm/DrmManagerHc;->updatePlaybackPositionSuccessHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/drm/DrmManagerHc;->silentErrorHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    .line 261
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 260
    invoke-static {v0, v1, v2, v3}, Lnet/flixster/android/data/ProfileDao;->updatePlaybackPosition(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V

    .line 262
    return-void
.end method

.method protected wvPlay(Landroid/drm/DrmInfoRequest;I)V
    .locals 8
    .parameter "drmInfoRequest"
    .parameter "playPosition"

    .prologue
    .line 377
    const-string v5, "FlxDrm"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "DrmManagerHc.wvPlay: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/flixster/android/drm/DrmManagerHc;->assetUri:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    iget-object v4, p0, Lcom/flixster/android/drm/DrmManagerHc;->assetUri:Ljava/lang/String;

    .line 379
    .local v4, localUri:Ljava/lang/String;
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc;->drmManagerClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, p1}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    .line 381
    .local v0, drmInfo:Landroid/drm/DrmInfo;
    :try_start_0
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc;->drmManagerClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, v0}, Landroid/drm/DrmManagerClient;->processDrmInfo(Landroid/drm/DrmInfo;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    if-eqz v5, :cond_0

    .line 396
    :try_start_1
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 402
    :cond_0
    :goto_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 403
    .local v3, intent:Landroid/content/Intent;
    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 404
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const-string v6, "video/*"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    const/high16 v5, 0x1000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 406
    const-string v5, "KEY_RIGHT_ID"

    iget-object v6, p0, Lcom/flixster/android/drm/DrmManagerHc;->r:Lnet/flixster/android/model/LockerRight;

    iget-wide v6, v6, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-virtual {v3, v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 407
    const-string v5, "KEY_PLAY_POSITION"

    invoke-virtual {v3, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 408
    const-string v5, "KEY_CAPTIONS_URI"

    iget-object v6, p0, Lcom/flixster/android/drm/DrmManagerHc;->captionsUri:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 415
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc;->context:Landroid/content/Context;

    const-class v6, Lcom/flixster/android/drm/WidevinePlayer;

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 416
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc;->context:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 417
    .end local v3           #intent:Landroid/content/Intent;
    :cond_1
    :goto_1
    return-void

    .line 382
    :catch_0
    move-exception v2

    .line 383
    .local v2, iae:Ljava/lang/IllegalArgumentException;
    const-string v5, "FlxDrm"

    const-string v6, "DrmManagerClient.processDrmInfo received invalid drmInfo, exiting"

    invoke-static {v5, v6, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 384
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    if-eqz v5, :cond_1

    .line 386
    :try_start_2
    iget-object v5, p0, Lcom/flixster/android/drm/DrmManagerHc;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 387
    :catch_1
    move-exception v1

    .line 388
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 397
    .end local v1           #e:Ljava/lang/Exception;
    .end local v2           #iae:Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 398
    .restart local v1       #e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public wvResume()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 431
    const/4 v0, 0x0

    return-object v0
.end method

.method public wvStop()V
    .locals 0

    .prologue
    .line 438
    return-void
.end method
