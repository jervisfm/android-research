.class public Lcom/flixster/android/drm/MigrationHelper;
.super Ljava/lang/Object;
.source "MigrationHelper.java"


# static fields
.field public static final WV_DIR:Ljava/lang/String; = "wv/"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static extractAssetId(Ljava/lang/String;)J
    .locals 9
    .parameter "localUri"

    .prologue
    const-wide/16 v5, 0x0

    const/4 v8, -0x1

    .line 143
    const-string v7, "/"

    invoke-virtual {p0, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 144
    .local v3, lastSlashIndex:I
    const-string v7, "."

    invoke-virtual {p0, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 145
    .local v2, lastDotIndex:I
    if-le v3, v8, :cond_0

    if-le v2, v8, :cond_0

    .line 146
    add-int/lit8 v7, v3, 0x1

    invoke-virtual {p0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, filename:Ljava/lang/String;
    const-string v7, "_"

    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v0, v7, v8

    .line 149
    .local v0, assetId:Ljava/lang/String;
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    .line 154
    .end local v0           #assetId:Ljava/lang/String;
    .end local v1           #filename:Ljava/lang/String;
    :cond_0
    :goto_0
    return-wide v5

    .line 150
    .restart local v0       #assetId:Ljava/lang/String;
    .restart local v1       #filename:Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 151
    .local v4, nfe:Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method private static migrateAssets(Lnet/flixster/android/model/User;)V
    .locals 12
    .parameter "user"

    .prologue
    const-wide/16 v10, 0x0

    .line 34
    const-string v6, "wv/"

    const-string v7, ".mp4"

    invoke-static {v6, v7}, Lcom/flixster/android/storage/ExternalStorage;->findFiles(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v3

    .line 35
    .local v3, localUris:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez v3, :cond_1

    .line 59
    :cond_0
    return-void

    .line 38
    :cond_1
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 39
    .local v2, localUri:Ljava/lang/String;
    const-string v7, "_0.mp4"

    invoke-virtual {v2, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "_1000.mp4"

    invoke-virtual {v2, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 40
    :cond_2
    const-string v7, "FlxDrm"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "MigrationHelper.migrateAssets migrating "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    invoke-static {v2}, Lcom/flixster/android/drm/MigrationHelper;->extractAssetId(Ljava/lang/String;)J

    move-result-wide v0

    .line 42
    .local v0, assetId:J
    cmp-long v7, v0, v10

    if-lez v7, :cond_4

    .line 43
    invoke-virtual {p0, v0, v1}, Lnet/flixster/android/model/User;->getLockerRightId(J)J

    move-result-wide v4

    .line 44
    .local v4, rightId:J
    cmp-long v7, v4, v10

    if-eqz v7, :cond_3

    .line 47
    const-string v7, "wv/"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ".mp4"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v7, v8}, Lcom/flixster/android/storage/ExternalStorage;->renameFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 49
    :cond_3
    const-string v7, "FlxDrm"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "MigrationHelper.migrateAssets no locker right for asset "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 52
    .end local v4           #rightId:J
    :cond_4
    const-string v7, "FlxDrm"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "MigrationHelper.migrateAssets failed to extract asset id "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 56
    .end local v0           #assetId:J
    :cond_5
    const-string v7, "FlxDrm"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "MigrationHelper.migrateAssets unknown .mp4 file "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static migrateDownloadedFilesForRightsApi(Lnet/flixster/android/model/User;)V
    .locals 0
    .parameter "user"

    .prologue
    .line 27
    invoke-static {p0}, Lcom/flixster/android/drm/MigrationHelper;->migrateAssets(Lnet/flixster/android/model/User;)V

    .line 28
    invoke-static {p0}, Lcom/flixster/android/drm/MigrationHelper;->migrateLocks(Lnet/flixster/android/model/User;)V

    .line 29
    invoke-static {p0}, Lcom/flixster/android/drm/MigrationHelper;->migrateRights(Lnet/flixster/android/model/User;)V

    .line 30
    return-void
.end method

.method private static migrateLocks(Lnet/flixster/android/model/User;)V
    .locals 14
    .parameter "user"

    .prologue
    .line 64
    const-string v10, "wv/"

    const-string v11, ".lck"

    invoke-static {v10, v11}, Lcom/flixster/android/storage/ExternalStorage;->findFiles(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v5

    .line 65
    .local v5, localUris:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez v5, :cond_1

    .line 98
    :cond_0
    return-void

    .line 68
    :cond_1
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 69
    .local v4, localUri:Ljava/lang/String;
    invoke-static {v4}, Lcom/flixster/android/storage/ExternalStorage;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 70
    .local v2, content:Ljava/lang/String;
    const/4 v7, 0x0

    check-cast v7, [Ljava/lang/String;

    .line 71
    .local v7, params:[Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 72
    const-string v10, "ZZZZ"

    invoke-virtual {v2, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 74
    :cond_2
    if-eqz v7, :cond_5

    array-length v10, v7

    const/4 v12, 0x4

    if-ne v10, v12, :cond_5

    .line 75
    const-string v10, "FlxDrm"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "MigrationHelper.migrateLocks migrating "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const/4 v10, 0x1

    aget-object v10, v7, v10

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 79
    .local v0, assetId:J
    const/4 v10, 0x2

    aget-object v3, v7, v10

    .line 80
    .local v3, editionId:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lnet/flixster/android/model/User;->getLockerRightId(J)J

    move-result-wide v8

    .line 81
    .local v8, rightId:J
    const-wide/16 v12, 0x0

    cmp-long v10, v8, v12

    if-eqz v10, :cond_4

    .line 82
    new-instance v6, Lcom/flixster/android/drm/DownloadLock;

    invoke-direct {v6, v8, v9}, Lcom/flixster/android/drm/DownloadLock;-><init>(J)V

    .line 83
    .local v6, lock:Lcom/flixster/android/drm/DownloadLock;
    const/4 v10, 0x0

    aget-object v10, v7, v10

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v6, v12, v13}, Lcom/flixster/android/drm/DownloadLock;->setDownloadId(J)V

    .line 84
    const/4 v10, 0x3

    aget-object v10, v7, v10

    invoke-virtual {v6, v10}, Lcom/flixster/android/drm/DownloadLock;->setTitle(Ljava/lang/String;)V

    .line 85
    const-string v10, "1000"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    sget-object v10, Lnet/flixster/android/model/LockerRight$RightType;->EPISODE:Lnet/flixster/android/model/LockerRight$RightType;

    invoke-virtual {v10}, Lnet/flixster/android/model/LockerRight$RightType;->name()Ljava/lang/String;

    move-result-object v10

    :goto_1
    invoke-virtual {v6, v10}, Lcom/flixster/android/drm/DownloadLock;->setRightType(Ljava/lang/String;)V

    .line 86
    invoke-virtual {v6, v0, v1}, Lcom/flixster/android/drm/DownloadLock;->setAssetId(J)V

    .line 87
    invoke-virtual {v6}, Lcom/flixster/android/drm/DownloadLock;->save()V

    .line 90
    invoke-static {v4}, Lcom/flixster/android/storage/ExternalStorage;->deleteFile(Ljava/lang/String;)Z

    goto :goto_0

    .line 85
    :cond_3
    sget-object v10, Lnet/flixster/android/model/LockerRight$RightType;->MOVIE:Lnet/flixster/android/model/LockerRight$RightType;

    invoke-virtual {v10}, Lnet/flixster/android/model/LockerRight$RightType;->name()Ljava/lang/String;

    move-result-object v10

    goto :goto_1

    .line 92
    .end local v6           #lock:Lcom/flixster/android/drm/DownloadLock;
    :cond_4
    const-string v10, "FlxDrm"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "MigrationHelper.migrateLocks no locker right for asset "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 95
    .end local v0           #assetId:J
    .end local v3           #editionId:Ljava/lang/String;
    .end local v8           #rightId:J
    :cond_5
    const-string v10, "FlxDrm"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "MigrationHelper.migrateLocks invalid .lck file "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static migrateRights(Lnet/flixster/android/model/User;)V
    .locals 15
    .parameter "user"

    .prologue
    const-wide/16 v13, 0x0

    .line 102
    const-string v9, "wv/"

    const-string v10, ".params"

    invoke-static {v9, v10}, Lcom/flixster/android/storage/ExternalStorage;->findFiles(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v4

    .line 103
    .local v4, localUris:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez v4, :cond_1

    .line 139
    :cond_0
    return-void

    .line 106
    :cond_1
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 107
    .local v3, localUri:Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/storage/ExternalStorage;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, content:Ljava/lang/String;
    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/String;

    .line 109
    .local v5, params:[Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 110
    const-string v10, "ZZZZ"

    invoke-virtual {v2, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 112
    :cond_2
    if-eqz v5, :cond_5

    array-length v10, v5

    const/4 v11, 0x4

    if-ne v10, v11, :cond_5

    .line 113
    invoke-static {v3}, Lcom/flixster/android/drm/MigrationHelper;->extractAssetId(Ljava/lang/String;)J

    move-result-wide v0

    .line 114
    .local v0, assetId:J
    cmp-long v10, v0, v13

    if-lez v10, :cond_4

    .line 115
    const-string v10, "FlxDrm"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "MigrationHelper.migrateRights migrating "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p0, v0, v1}, Lnet/flixster/android/model/User;->getLockerRightId(J)J

    move-result-wide v7

    .line 119
    .local v7, rightId:J
    cmp-long v10, v7, v13

    if-eqz v10, :cond_3

    .line 120
    new-instance v6, Lcom/flixster/android/drm/DownloadRight;

    invoke-direct {v6, v7, v8}, Lcom/flixster/android/drm/DownloadRight;-><init>(J)V

    .line 121
    .local v6, right:Lcom/flixster/android/drm/DownloadRight;
    const/4 v10, 0x0

    aget-object v10, v5, v10

    invoke-virtual {v6, v10}, Lcom/flixster/android/drm/DownloadRight;->setDownloadUri(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;

    .line 122
    const/4 v10, 0x1

    aget-object v10, v5, v10

    invoke-virtual {v6, v10}, Lcom/flixster/android/drm/DownloadRight;->setLicenseProxy(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;

    .line 123
    const/4 v10, 0x2

    aget-object v10, v5, v10

    invoke-virtual {v6, v10}, Lcom/flixster/android/drm/DownloadRight;->setQueueId(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;

    .line 124
    const/4 v10, 0x3

    aget-object v10, v5, v10

    invoke-virtual {v6, v10}, Lcom/flixster/android/drm/DownloadRight;->setDeviceId(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;

    .line 125
    invoke-virtual {v6}, Lcom/flixster/android/drm/DownloadRight;->save()V

    .line 128
    invoke-static {v3}, Lcom/flixster/android/storage/ExternalStorage;->deleteFile(Ljava/lang/String;)Z

    goto :goto_0

    .line 130
    .end local v6           #right:Lcom/flixster/android/drm/DownloadRight;
    :cond_3
    const-string v10, "FlxDrm"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "MigrationHelper.migrateRights no locker right for asset "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 133
    .end local v7           #rightId:J
    :cond_4
    const-string v10, "FlxDrm"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "MigrationHelper.migrateRights failed to extract asset id "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 136
    .end local v0           #assetId:J
    :cond_5
    const-string v10, "FlxDrm"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "MigrationHelper.migrateRights invalid .params file "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
