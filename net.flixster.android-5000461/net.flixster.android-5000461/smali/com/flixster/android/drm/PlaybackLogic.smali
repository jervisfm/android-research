.class public interface abstract Lcom/flixster/android/drm/PlaybackLogic;
.super Ljava/lang/Object;
.source "PlaybackLogic.java"


# virtual methods
.method public abstract isAssetDownloadable(Lnet/flixster/android/model/LockerRight;)Z
.end method

.method public abstract isAssetPlayable(Lnet/flixster/android/model/LockerRight;)Z
.end method

.method public abstract isDeviceDownloadCompatible()Z
.end method

.method public abstract isDeviceNonWifiStreamBlacklisted()Z
.end method

.method public abstract isDeviceStreamBlacklisted()Z
.end method

.method public abstract isDeviceStreamingCompatible()Z
.end method

.method public abstract isNativeWv()Z
.end method

.method public abstract setDeviceNonWifiStreamBlacklisted(Z)V
.end method

.method public abstract setDeviceStreamBlacklisted(Z)V
.end method
