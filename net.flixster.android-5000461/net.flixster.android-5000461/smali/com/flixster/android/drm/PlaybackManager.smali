.class public interface abstract Lcom/flixster/android/drm/PlaybackManager;
.super Ljava/lang/Object;
.source "PlaybackManager.java"


# virtual methods
.method public abstract getPlaybackTag()Ljava/lang/String;
.end method

.method public abstract getPlaybackTitle()Ljava/lang/String;
.end method

.method public abstract getPlayer()Lcom/flixster/android/drm/WidevinePlayer;
.end method

.method public abstract initializeDRM(Landroid/content/Context;)V
.end method

.method public abstract isInitialized()Z
.end method

.method public abstract isRooted()Z
.end method

.method public abstract isStreamingMode()Z
.end method

.method public abstract playMovie(Lnet/flixster/android/model/LockerRight;)V
.end method

.method public abstract registerAllLocalAssets(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract registerAssetOnDownloadComplete(Lnet/flixster/android/model/LockerRight;)V
.end method

.method public abstract setPlayer(Lcom/flixster/android/drm/WidevinePlayer;)V
.end method

.method public abstract streamResume()V
.end method

.method public abstract streamStop(I)V
.end method

.method public abstract terminateDRM()V
.end method

.method public abstract unregisterLocalAsset(Lnet/flixster/android/model/LockerRight;)V
.end method

.method public abstract updatePlaybackPosition(I)V
.end method

.method public abstract wvResume()Landroid/net/Uri;
.end method

.method public abstract wvStop()V
.end method
