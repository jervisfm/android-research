.class Lcom/flixster/android/drm/WidevinePlayer$1;
.super Landroid/os/Handler;
.source "WidevinePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/WidevinePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$flixster$android$drm$WidevinePlayer$PlaybackError:[I


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/WidevinePlayer;


# direct methods
.method static synthetic $SWITCH_TABLE$com$flixster$android$drm$WidevinePlayer$PlaybackError()[I
    .locals 3

    .prologue
    .line 103
    sget-object v0, Lcom/flixster/android/drm/WidevinePlayer$1;->$SWITCH_TABLE$com$flixster$android$drm$WidevinePlayer$PlaybackError:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->values()[Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->LICENSE:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    invoke-virtual {v1}, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->NETWORK:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    invoke-virtual {v1}, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->SECURITY:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    invoke-virtual {v1}, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->UNKNOWN:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    invoke-virtual {v1}, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/flixster/android/drm/WidevinePlayer$1;->$SWITCH_TABLE$com$flixster$android$drm$WidevinePlayer$PlaybackError:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer$1;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    .line 103
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/drm/WidevinePlayer$1;)Lcom/flixster/android/drm/WidevinePlayer;
    .locals 1
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer$1;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    .line 105
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer$1;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-virtual {v4}, Lcom/flixster/android/drm/WidevinePlayer;->isFinishing()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 106
    const-string v4, "FlxDrm"

    const-string v5, "WidevinePlayer.showErrorDialogHandler activity finished"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer$1;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/flixster/android/drm/WidevinePlayer;->access$0(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->hide()V

    .line 112
    iget v3, p1, Landroid/os/Message;->what:I

    .line 113
    .local v3, what:I
    sget-object v1, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->UNKNOWN:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    .line 114
    .local v1, errorType:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    if-nez v3, :cond_1

    .line 115
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v1           #errorType:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    check-cast v1, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    .line 118
    .restart local v1       #errorType:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    :cond_1
    const/4 v2, 0x0

    .line 119
    .local v2, message:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/drm/WidevinePlayer$1;->$SWITCH_TABLE$com$flixster$android$drm$WidevinePlayer$PlaybackError()[I

    move-result-object v4

    invoke-virtual {v1}, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 129
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer$1;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    const v5, 0x7f0c017b

    invoke-virtual {v4, v5}, Lcom/flixster/android/drm/WidevinePlayer;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 133
    :goto_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer$1;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 134
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const v4, 0x7f0c017a

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 135
    const-string v5, "OK"

    new-instance v6, Lcom/flixster/android/drm/WidevinePlayer$1$1;

    invoke-direct {v6, p0}, Lcom/flixster/android/drm/WidevinePlayer$1$1;-><init>(Lcom/flixster/android/drm/WidevinePlayer$1;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 140
    new-instance v5, Lcom/flixster/android/drm/WidevinePlayer$1$2;

    invoke-direct {v5, p0}, Lcom/flixster/android/drm/WidevinePlayer$1$2;-><init>(Lcom/flixster/android/drm/WidevinePlayer$1;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 146
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer$1;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    #setter for: Lcom/flixster/android/drm/WidevinePlayer;->errorDialog:Landroid/app/AlertDialog;
    invoke-static {v4, v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$1(Lcom/flixster/android/drm/WidevinePlayer;Landroid/app/AlertDialog;)V

    .line 147
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer$1;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->errorDialog:Landroid/app/AlertDialog;
    invoke-static {v4}, Lcom/flixster/android/drm/WidevinePlayer;->access$2(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/app/AlertDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 121
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :pswitch_0
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer$1;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    const v5, 0x7f0c017c

    invoke-virtual {v4, v5}, Lcom/flixster/android/drm/WidevinePlayer;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 122
    goto :goto_1

    .line 124
    :pswitch_1
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer$1;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    const v5, 0x7f0c017d

    invoke-virtual {v4, v5}, Lcom/flixster/android/drm/WidevinePlayer;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 125
    goto :goto_1

    .line 119
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
