.class Lcom/flixster/android/drm/WidevinePlayer$2;
.super Landroid/os/Handler;
.source "WidevinePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/WidevinePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field currentPosition:I

.field final synthetic this$0:Lcom/flixster/android/drm/WidevinePlayer;

.field ttIndex:I


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    .line 218
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .parameter "msg"

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 223
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captions:Ljava/util/List;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$3(Lcom/flixster/android/drm/WidevinePlayer;)Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 224
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->outMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$4(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v4, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 225
    .local v4, width:I
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->outMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$4(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v1, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 226
    .local v1, height:I
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$5(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/widget/VideoView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v5

    iget v6, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->currentPosition:I

    if-ge v5, v6, :cond_0

    .line 227
    iput v8, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->ttIndex:I

    .line 228
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    const/4 v5, 0x4

    if-lt v2, v5, :cond_2

    .line 232
    .end local v2           #i:I
    :cond_0
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$5(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/widget/VideoView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v5

    iput v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->currentPosition:I

    .line 233
    iget v2, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->ttIndex:I

    .restart local v2       #i:I
    :goto_1
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captions:Ljava/util/List;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$3(Lcom/flixster/android/drm/WidevinePlayer;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v2, v5, :cond_3

    .line 255
    .end local v1           #height:I
    .end local v2           #i:I
    .end local v4           #width:I
    :cond_1
    return-void

    .line 229
    .restart local v1       #height:I
    .restart local v2       #i:I
    .restart local v4       #width:I
    :cond_2
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$6(Lcom/flixster/android/drm/WidevinePlayer;)[Landroid/widget/TextView;

    move-result-object v5

    aget-object v5, v5, v2

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 228
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 234
    :cond_3
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captions:Ljava/util/List;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$3(Lcom/flixster/android/drm/WidevinePlayer;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/flixster/android/net/TimedTextElement;

    .line 235
    .local v3, ttElement:Lcom/flixster/android/net/TimedTextElement;
    iget v5, v3, Lcom/flixster/android/net/TimedTextElement;->end:I

    iget v6, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->currentPosition:I

    if-gt v5, v6, :cond_6

    .line 236
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$6(Lcom/flixster/android/drm/WidevinePlayer;)[Landroid/widget/TextView;

    move-result-object v5

    iget v6, v3, Lcom/flixster/android/net/TimedTextElement;->region:I

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/widget/TextView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_4

    .line 237
    const-string v5, "FlxDrm"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "hiding index "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", text "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/flixster/android/net/TimedTextElement;->text:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$6(Lcom/flixster/android/drm/WidevinePlayer;)[Landroid/widget/TextView;

    move-result-object v5

    iget v6, v3, Lcom/flixster/android/net/TimedTextElement;->region:I

    aget-object v5, v5, v6

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 240
    :cond_4
    iget v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->ttIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->ttIndex:I

    .line 233
    :cond_5
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 241
    :cond_6
    iget v5, v3, Lcom/flixster/android/net/TimedTextElement;->begin:I

    iget v6, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->currentPosition:I

    if-gt v5, v6, :cond_1

    .line 242
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$6(Lcom/flixster/android/drm/WidevinePlayer;)[Landroid/widget/TextView;

    move-result-object v5

    iget v6, v3, Lcom/flixster/android/net/TimedTextElement;->region:I

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/widget/TextView;->getVisibility()I

    move-result v5

    if-ne v5, v9, :cond_5

    .line 243
    const-string v5, "FlxDrm"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "showing index "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", text "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/flixster/android/net/TimedTextElement;->text:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$6(Lcom/flixster/android/drm/WidevinePlayer;)[Landroid/widget/TextView;

    move-result-object v5

    iget v6, v3, Lcom/flixster/android/net/TimedTextElement;->region:I

    aget-object v5, v5, v6

    .line 245
    invoke-virtual {v5}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 244
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 246
    .local v0, cvParams:Landroid/view/ViewGroup$MarginLayoutParams;
    iget v5, v3, Lcom/flixster/android/net/TimedTextElement;->originX:I

    mul-int/2addr v5, v4

    div-int/lit8 v5, v5, 0x64

    iget v6, v3, Lcom/flixster/android/net/TimedTextElement;->originY:I

    mul-int/2addr v6, v1

    div-int/lit8 v6, v6, 0x64

    invoke-virtual {v0, v5, v6, v8, v8}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 247
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$6(Lcom/flixster/android/drm/WidevinePlayer;)[Landroid/widget/TextView;

    move-result-object v5

    iget v6, v3, Lcom/flixster/android/net/TimedTextElement;->region:I

    aget-object v5, v5, v6

    iget-object v6, v3, Lcom/flixster/android/net/TimedTextElement;->text:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$2;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$6(Lcom/flixster/android/drm/WidevinePlayer;)[Landroid/widget/TextView;

    move-result-object v5

    iget v6, v3, Lcom/flixster/android/net/TimedTextElement;->region:I

    aget-object v5, v5, v6

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method
