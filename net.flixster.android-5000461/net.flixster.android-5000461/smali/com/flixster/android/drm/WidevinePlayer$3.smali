.class Lcom/flixster/android/drm/WidevinePlayer$3;
.super Landroid/os/Handler;
.source "WidevinePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/WidevinePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field estimatedLoadingTime:I

.field final synthetic this$0:Lcom/flixster/android/drm/WidevinePlayer;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;)V
    .locals 1
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer$3;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    .line 368
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 369
    #calls: Lcom/flixster/android/drm/WidevinePlayer;->getEstimatedLoadingTime()I
    invoke-static {p1}, Lcom/flixster/android/drm/WidevinePlayer;->access$7(Lcom/flixster/android/drm/WidevinePlayer;)I

    move-result v0

    iput v0, p0, Lcom/flixster/android/drm/WidevinePlayer$3;->estimatedLoadingTime:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    .line 372
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$3;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->startLoadingTime:J
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$8(Lcom/flixster/android/drm/WidevinePlayer;)J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    long-to-double v0, v3

    .line 373
    .local v0, elapsedLoadingTime:D
    iget v3, p0, Lcom/flixster/android/drm/WidevinePlayer$3;->estimatedLoadingTime:I

    int-to-double v3, v3

    div-double v3, v0, v3

    const-wide/high16 v5, 0x4034

    mul-double/2addr v3, v5

    double-to-int v3, v3

    mul-int/lit8 v2, v3, 0x5

    .line 374
    .local v2, progressPercentage:I
    const/16 v3, 0x64

    if-ge v2, v3, :cond_0

    .line 375
    iget-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer$3;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/flixster/android/drm/WidevinePlayer;->access$0(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/app/ProgressDialog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$3;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-virtual {v5}, Lcom/flixster/android/drm/WidevinePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0135

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 376
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 375
    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 378
    :cond_0
    return-void
.end method
