.class Lcom/flixster/android/drm/WidevinePlayer$6;
.super Ljava/lang/Object;
.source "WidevinePlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/drm/WidevinePlayer;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/WidevinePlayer;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer$6;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    .line 352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 354
    :goto_0
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$6;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackPrepared:Z
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$22(Lcom/flixster/android/drm/WidevinePlayer;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$6;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackActive:Z
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$19(Lcom/flixster/android/drm/WidevinePlayer;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$6;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->hasPlaybackFailed:Z
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$10(Lcom/flixster/android/drm/WidevinePlayer;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 362
    :cond_0
    return-void

    .line 355
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$6;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->progressHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$23(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 357
    const-wide/16 v1, 0x3e8

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 358
    :catch_0
    move-exception v0

    .line 359
    .local v0, e:Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
