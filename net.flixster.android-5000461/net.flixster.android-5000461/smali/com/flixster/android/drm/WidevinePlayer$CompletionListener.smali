.class Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;
.super Ljava/lang/Object;
.source "WidevinePlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/WidevinePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CompletionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/WidevinePlayer;


# direct methods
.method private constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 258
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 258
    invoke-direct {p0, p1}, Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;-><init>(Lcom/flixster/android/drm/WidevinePlayer;)V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4
    .parameter "mp"

    .prologue
    .line 261
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    const-string v1, "playback_complete"

    const/4 v2, 0x0

    #calls: Lcom/flixster/android/drm/WidevinePlayer;->trackPlaybackEvent(Ljava/lang/String;I)V
    invoke-static {v0, v1, v2}, Lcom/flixster/android/drm/WidevinePlayer;->access$12(Lcom/flixster/android/drm/WidevinePlayer;Ljava/lang/String;I)V

    .line 262
    const-string v0, "FlxDrm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WidevinePlayer.onCompletion rightId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->rightId:J
    invoke-static {v2}, Lcom/flixster/android/drm/WidevinePlayer;->access$17(Lcom/flixster/android/drm/WidevinePlayer;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resetting seek time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    const/4 v1, 0x1

    #setter for: Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackCompleted:Z
    invoke-static {v0, v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$21(Lcom/flixster/android/drm/WidevinePlayer;Z)V

    .line 264
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-virtual {v0}, Lcom/flixster/android/drm/WidevinePlayer;->finish()V

    .line 265
    return-void
.end method
