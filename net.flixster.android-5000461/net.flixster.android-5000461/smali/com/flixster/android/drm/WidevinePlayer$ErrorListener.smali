.class Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;
.super Ljava/lang/Object;
.source "WidevinePlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/WidevinePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ErrorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/WidevinePlayer;


# direct methods
.method private constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;-><init>(Lcom/flixster/android/drm/WidevinePlayer;)V

    return-void
.end method


# virtual methods
.method public onError(Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;Ljava/lang/String;)V
    .locals 6
    .parameter "errorType"
    .parameter "errorDescription"

    .prologue
    const/4 v5, 0x0

    .line 92
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->hasPlaybackFailed:Z
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$10(Lcom/flixster/android/drm/WidevinePlayer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    const/4 v2, 0x1

    #setter for: Lcom/flixster/android/drm/WidevinePlayer;->hasPlaybackFailed:Z
    invoke-static {v1, v2}, Lcom/flixster/android/drm/WidevinePlayer;->access$11(Lcom/flixster/android/drm/WidevinePlayer;Z)V

    .line 94
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WidevinePlayer.onError "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getConnectionType()Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, connectionType:Ljava/lang/String;
    iget-object v2, p0, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "playback_error_"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 97
    if-eqz p2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "_"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 96
    #calls: Lcom/flixster/android/drm/WidevinePlayer;->trackPlaybackEvent(Ljava/lang/String;I)V
    invoke-static {v2, v1, v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$12(Lcom/flixster/android/drm/WidevinePlayer;Ljava/lang/String;I)V

    .line 98
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->showErrorDialogHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$13(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2, v5, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 100
    .end local v0           #connectionType:Ljava/lang/String;
    :cond_0
    return-void

    .line 97
    .restart local v0       #connectionType:Ljava/lang/String;
    :cond_1
    const-string v1, ""

    goto :goto_0
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    .prologue
    const/4 v4, 0x1

    .line 79
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->hasPlaybackFailed:Z
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$10(Lcom/flixster/android/drm/WidevinePlayer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #setter for: Lcom/flixster/android/drm/WidevinePlayer;->hasPlaybackFailed:Z
    invoke-static {v1, v4}, Lcom/flixster/android/drm/WidevinePlayer;->access$11(Lcom/flixster/android/drm/WidevinePlayer;Z)V

    .line 81
    const-string v1, "FlxDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WidevinePlayer.onError: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getConnectionType()Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, connectionType:Ljava/lang/String;
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    .line 84
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "playback_error_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 85
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 84
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 85
    const/4 v3, 0x0

    .line 83
    #calls: Lcom/flixster/android/drm/WidevinePlayer;->trackPlaybackEvent(Ljava/lang/String;I)V
    invoke-static {v1, v2, v3}, Lcom/flixster/android/drm/WidevinePlayer;->access$12(Lcom/flixster/android/drm/WidevinePlayer;Ljava/lang/String;I)V

    .line 86
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->showErrorDialogHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$13(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 88
    .end local v0           #connectionType:Ljava/lang/String;
    :cond_0
    return v4
.end method
