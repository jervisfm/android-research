.class Lcom/flixster/android/drm/WidevinePlayer$InfoListener;
.super Ljava/lang/Object;
.source "WidevinePlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/WidevinePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InfoListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/WidevinePlayer;


# direct methods
.method private constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 151
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer$InfoListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;Lcom/flixster/android/drm/WidevinePlayer$InfoListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 151
    invoke-direct {p0, p1}, Lcom/flixster/android/drm/WidevinePlayer$InfoListener;-><init>(Lcom/flixster/android/drm/WidevinePlayer;)V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    .prologue
    const/4 v3, 0x1

    .line 154
    const-string v0, "FlxDrm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WidevinePlayer.onInfo: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    packed-switch p2, :pswitch_data_0

    .line 166
    :goto_0
    return v3

    .line 158
    :pswitch_0
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer$InfoListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/flixster/android/drm/WidevinePlayer;->access$0(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 159
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer$InfoListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/flixster/android/drm/WidevinePlayer;->access$0(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$InfoListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-virtual {v1}, Lcom/flixster/android/drm/WidevinePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0136

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer$InfoListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/flixster/android/drm/WidevinePlayer;->access$0(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 163
    :pswitch_1
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer$InfoListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/flixster/android/drm/WidevinePlayer;->access$0(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    goto :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x2bd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
