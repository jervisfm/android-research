.class public final enum Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
.super Ljava/lang/Enum;
.source "WidevinePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/WidevinePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlaybackError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

.field public static final enum LICENSE:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

.field public static final enum NETWORK:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

.field public static final enum SECURITY:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

.field public static final enum UNKNOWN:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    const-string v1, "LICENSE"

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->LICENSE:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    new-instance v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v3}, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->NETWORK:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    new-instance v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    const-string v1, "SECURITY"

    invoke-direct {v0, v1, v4}, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->SECURITY:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    new-instance v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->UNKNOWN:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    .line 56
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    sget-object v1, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->LICENSE:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->NETWORK:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->SECURITY:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->UNKNOWN:Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    aput-object v1, v0, v5

    sput-object v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->ENUM$VALUES:[Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    return-object v0
.end method

.method public static values()[Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;->ENUM$VALUES:[Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    array-length v1, v0

    new-array v2, v1, [Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
