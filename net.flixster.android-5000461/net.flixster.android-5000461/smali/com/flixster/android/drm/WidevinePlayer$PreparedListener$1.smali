.class Lcom/flixster/android/drm/WidevinePlayer$PreparedListener$1;
.super Ljava/lang/Object;
.source "WidevinePlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;


# direct methods
.method constructor <init>(Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener$1;->this$1:Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 202
    :goto_0
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener$1;->this$1:Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->access$1(Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;)Lcom/flixster/android/drm/WidevinePlayer;

    move-result-object v1

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackActive:Z
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$19(Lcom/flixster/android/drm/WidevinePlayer;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener$1;->this$1:Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->access$1(Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;)Lcom/flixster/android/drm/WidevinePlayer;

    move-result-object v1

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->hasPlaybackFailed:Z
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$10(Lcom/flixster/android/drm/WidevinePlayer;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 210
    :cond_0
    return-void

    .line 203
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener$1;->this$1:Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->access$1(Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;)Lcom/flixster/android/drm/WidevinePlayer;

    move-result-object v1

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captionHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/drm/WidevinePlayer;->access$20(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 205
    const-wide/16 v1, 0x12c

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, e:Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
