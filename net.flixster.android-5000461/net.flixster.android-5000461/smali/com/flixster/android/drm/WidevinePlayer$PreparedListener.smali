.class Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;
.super Ljava/lang/Object;
.source "WidevinePlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/drm/WidevinePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreparedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/drm/WidevinePlayer;


# direct methods
.method private constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 170
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/drm/WidevinePlayer;Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;-><init>(Lcom/flixster/android/drm/WidevinePlayer;)V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;)Lcom/flixster/android/drm/WidevinePlayer;
    .locals 1
    .parameter

    .prologue
    .line 170
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    return-object v0
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 10
    .parameter "mp"

    .prologue
    const/4 v9, 0x0

    .line 173
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    invoke-virtual {v5}, Lcom/flixster/android/drm/WidevinePlayer;->isFinishing()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 174
    const-string v5, "FlxDrm"

    const-string v6, "WidevinePlayer.onPrepared activity finished"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->hasPlaybackFailed:Z
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$10(Lcom/flixster/android/drm/WidevinePlayer;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 179
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getConnectionType()Ljava/lang/String;

    move-result-object v1

    .line 180
    .local v1, connectionType:Ljava/lang/String;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    iget-object v7, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->startLoadingTime:J
    invoke-static {v7}, Lcom/flixster/android/drm/WidevinePlayer;->access$8(Lcom/flixster/android/drm/WidevinePlayer;)J

    move-result-wide v7

    sub-long/2addr v5, v7

    long-to-int v5, v5

    div-int/lit16 v3, v5, 0x3e8

    .line 181
    .local v3, elapsedLoadingTime:I
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "loading_time_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    #calls: Lcom/flixster/android/drm/WidevinePlayer;->trackPlaybackEvent(Ljava/lang/String;I)V
    invoke-static {v5, v6, v3}, Lcom/flixster/android/drm/WidevinePlayer;->access$12(Lcom/flixster/android/drm/WidevinePlayer;Ljava/lang/String;I)V

    .line 182
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$0(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/app/ProgressDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->hide()V

    .line 184
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    const-string v6, "estimatedLoadingTime"

    invoke-virtual {v5, v6, v9}, Lcom/flixster/android/drm/WidevinePlayer;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 185
    .local v4, prefs:Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 186
    .local v2, editor:Landroid/content/SharedPreferences$Editor;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "loadingTime_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 187
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 189
    new-instance v5, Lcom/flixster/android/drm/WidevinePlayer$InfoListener;

    iget-object v6, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Lcom/flixster/android/drm/WidevinePlayer$InfoListener;-><init>(Lcom/flixster/android/drm/WidevinePlayer;Lcom/flixster/android/drm/WidevinePlayer$InfoListener;)V

    invoke-virtual {p1, v5}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 191
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v5

    invoke-virtual {v5}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->startTime:I
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$14(Lcom/flixster/android/drm/WidevinePlayer;)I

    move-result v5

    iget-object v6, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;
    invoke-static {v6}, Lcom/flixster/android/drm/WidevinePlayer;->access$5(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/widget/VideoView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/VideoView;->getDuration()I

    move-result v6

    if-lt v5, v6, :cond_2

    .line 192
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #setter for: Lcom/flixster/android/drm/WidevinePlayer;->startTime:I
    invoke-static {v5, v9}, Lcom/flixster/android/drm/WidevinePlayer;->access$15(Lcom/flixster/android/drm/WidevinePlayer;I)V

    .line 194
    :cond_2
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$5(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/widget/VideoView;

    move-result-object v5

    iget-object v6, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->startTime:I
    invoke-static {v6}, Lcom/flixster/android/drm/WidevinePlayer;->access$14(Lcom/flixster/android/drm/WidevinePlayer;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/VideoView;->seekTo(I)V

    .line 195
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$5(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/widget/VideoView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/VideoView;->start()V

    .line 196
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    const/4 v6, 0x1

    #setter for: Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackPrepared:Z
    invoke-static {v5, v6}, Lcom/flixster/android/drm/WidevinePlayer;->access$16(Lcom/flixster/android/drm/WidevinePlayer;Z)V

    .line 197
    const-string v5, "FlxDrm"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "WidevinePlayer.onPrepared rightId "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->rightId:J
    invoke-static {v7}, Lcom/flixster/android/drm/WidevinePlayer;->access$17(Lcom/flixster/android/drm/WidevinePlayer;)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", seeking to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->startTime:I
    invoke-static {v7}, Lcom/flixster/android/drm/WidevinePlayer;->access$14(Lcom/flixster/android/drm/WidevinePlayer;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCaptionsEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;->this$0:Lcom/flixster/android/drm/WidevinePlayer;

    #getter for: Lcom/flixster/android/drm/WidevinePlayer;->captionsUri:Ljava/lang/String;
    invoke-static {v5}, Lcom/flixster/android/drm/WidevinePlayer;->access$18(Lcom/flixster/android/drm/WidevinePlayer;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 200
    new-instance v0, Ljava/lang/Thread;

    new-instance v5, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener$1;

    invoke-direct {v5, p0}, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener$1;-><init>(Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;)V

    invoke-direct {v0, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 212
    .local v0, captionMonitorThread:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0
.end method
