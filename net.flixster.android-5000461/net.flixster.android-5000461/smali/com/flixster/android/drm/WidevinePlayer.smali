.class public Lcom/flixster/android/drm/WidevinePlayer;
.super Landroid/app/Activity;
.source "WidevinePlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;,
        Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;,
        Lcom/flixster/android/drm/WidevinePlayer$InfoListener;,
        Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;,
        Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;
    }
.end annotation


# static fields
.field private static final CAPTION_MONITOR_INTERVAL_MS:I = 0x12c

.field private static final CAPTION_REGIONS:I = 0x4

.field public static final KEY_CAPTIONS_URI:Ljava/lang/String; = "KEY_CAPTIONS_URI"

.field public static final KEY_PLAY_POSITION:Ljava/lang/String; = "KEY_PLAY_POSITION"

.field public static final KEY_RIGHT_ID:Ljava/lang/String; = "KEY_RIGHT_ID"

.field private static final LOADING_TIME_ETHERNET:I = 0x5

.field private static final LOADING_TIME_MOBILE:I = 0x3c

.field private static final LOADING_TIME_WIFI:I = 0x14

.field private static final LOADING_TIME_WIMAX:I = 0x1e

.field private static final PROGRESS_MONITOR_INTERVAL_MS:I = 0x3e8


# instance fields
.field private final captionHandler:Landroid/os/Handler;

.field private captionViews:[Landroid/widget/TextView;

.field private captions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/net/TimedTextElement;",
            ">;"
        }
    .end annotation
.end field

.field private captionsUri:Ljava/lang/String;

.field private dialog:Landroid/app/ProgressDialog;

.field private errorDialog:Landroid/app/AlertDialog;

.field private final errorHandler:Landroid/os/Handler;

.field private errorListener:Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;

.field private hasPlaybackFailed:Z

.field private isPlaybackActive:Z

.field private isPlaybackCompleted:Z

.field private isPlaybackPrepared:Z

.field private outMetrics:Landroid/util/DisplayMetrics;

.field private final progressHandler:Landroid/os/Handler;

.field private rightId:J

.field private final showErrorDialogHandler:Landroid/os/Handler;

.field private startLoadingTime:J

.field private startTime:I

.field private final successHandler:Landroid/os/Handler;

.field private videoView:Landroid/widget/VideoView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 103
    new-instance v0, Lcom/flixster/android/drm/WidevinePlayer$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/WidevinePlayer$1;-><init>(Lcom/flixster/android/drm/WidevinePlayer;)V

    iput-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->showErrorDialogHandler:Landroid/os/Handler;

    .line 218
    new-instance v0, Lcom/flixster/android/drm/WidevinePlayer$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/WidevinePlayer$2;-><init>(Lcom/flixster/android/drm/WidevinePlayer;)V

    iput-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionHandler:Landroid/os/Handler;

    .line 368
    new-instance v0, Lcom/flixster/android/drm/WidevinePlayer$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/WidevinePlayer$3;-><init>(Lcom/flixster/android/drm/WidevinePlayer;)V

    iput-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->progressHandler:Landroid/os/Handler;

    .line 470
    new-instance v0, Lcom/flixster/android/drm/WidevinePlayer$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/WidevinePlayer$4;-><init>(Lcom/flixster/android/drm/WidevinePlayer;)V

    iput-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->successHandler:Landroid/os/Handler;

    .line 478
    new-instance v0, Lcom/flixster/android/drm/WidevinePlayer$5;

    invoke-direct {v0, p0}, Lcom/flixster/android/drm/WidevinePlayer$5;-><init>(Lcom/flixster/android/drm/WidevinePlayer;)V

    iput-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->errorHandler:Landroid/os/Handler;

    .line 42
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/drm/WidevinePlayer;Landroid/app/AlertDialog;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer;->errorDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic access$10(Lcom/flixster/android/drm/WidevinePlayer;)Z
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->hasPlaybackFailed:Z

    return v0
.end method

.method static synthetic access$11(Lcom/flixster/android/drm/WidevinePlayer;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/flixster/android/drm/WidevinePlayer;->hasPlaybackFailed:Z

    return-void
.end method

.method static synthetic access$12(Lcom/flixster/android/drm/WidevinePlayer;Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 457
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/drm/WidevinePlayer;->trackPlaybackEvent(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$13(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->showErrorDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$14(Lcom/flixster/android/drm/WidevinePlayer;)I
    .locals 1
    .parameter

    .prologue
    .line 67
    iget v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    return v0
.end method

.method static synthetic access$15(Lcom/flixster/android/drm/WidevinePlayer;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 67
    iput p1, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    return-void
.end method

.method static synthetic access$16(Lcom/flixster/android/drm/WidevinePlayer;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackPrepared:Z

    return-void
.end method

.method static synthetic access$17(Lcom/flixster/android/drm/WidevinePlayer;)J
    .locals 2
    .parameter

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->rightId:J

    return-wide v0
.end method

.method static synthetic access$18(Lcom/flixster/android/drm/WidevinePlayer;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionsUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$19(Lcom/flixster/android/drm/WidevinePlayer;)Z
    .locals 1
    .parameter

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackActive:Z

    return v0
.end method

.method static synthetic access$2(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/app/AlertDialog;
    .locals 1
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->errorDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$20(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 218
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$21(Lcom/flixster/android/drm/WidevinePlayer;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackCompleted:Z

    return-void
.end method

.method static synthetic access$22(Lcom/flixster/android/drm/WidevinePlayer;)Z
    .locals 1
    .parameter

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackPrepared:Z

    return v0
.end method

.method static synthetic access$23(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 368
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->progressHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/drm/WidevinePlayer;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->captions:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/util/DisplayMetrics;
    .locals 1
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->outMetrics:Landroid/util/DisplayMetrics;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/drm/WidevinePlayer;)Landroid/widget/VideoView;
    .locals 1
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/flixster/android/drm/WidevinePlayer;)[Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/flixster/android/drm/WidevinePlayer;)I
    .locals 1
    .parameter

    .prologue
    .line 381
    invoke-direct {p0}, Lcom/flixster/android/drm/WidevinePlayer;->getEstimatedLoadingTime()I

    move-result v0

    return v0
.end method

.method static synthetic access$8(Lcom/flixster/android/drm/WidevinePlayer;)J
    .locals 2
    .parameter

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->startLoadingTime:J

    return-wide v0
.end method

.method static synthetic access$9(Lcom/flixster/android/drm/WidevinePlayer;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/flixster/android/drm/WidevinePlayer;->captions:Ljava/util/List;

    return-void
.end method

.method private getEstimatedLoadingTime()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 382
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getConnectionType()Ljava/lang/String;

    move-result-object v0

    .line 383
    .local v0, connectionType:Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "estimatedLoadingTime"

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 384
    .local v3, prefs:Landroid/content/SharedPreferences;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "loadingTime_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 385
    .local v2, loadingTime:I
    if-nez v2, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ETHERNET"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x5

    .line 389
    .local v1, estimatedLoadingTime:I
    :goto_0
    const-string v4, "FlxDrm"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WidevinePlayer.getEstimatedLoadingTime: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for network type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 390
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 389
    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    return v1

    .line 386
    .end local v1           #estimatedLoadingTime:I
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "WIFI"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v1, 0x14

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 387
    const-string v5, "WIMAX"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v1, 0x1e

    goto :goto_0

    :cond_2
    const/16 v1, 0x3c

    goto :goto_0

    :cond_3
    move v1, v2

    .line 388
    goto :goto_0
.end method

.method private trackPlaybackEvent(Ljava/lang/String;I)V
    .locals 7
    .parameter "eventLabel"
    .parameter "eventValue"

    .prologue
    .line 458
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/flixster/android/drm/PlaybackManager;->getPlaybackTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/flixster/android/drm/PlaybackManager;->getPlaybackTitle()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Streaming"

    .line 459
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v4}, Lcom/flixster/android/utils/UrlHelper;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v5, p1

    move v6, p2

    .line 458
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 460
    return-void
.end method


# virtual methods
.method public getPlaybackElapsedDuration()I
    .locals 2

    .prologue
    .line 467
    iget-boolean v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackPrepared:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    iget v1, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    sub-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x3e8

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyPlaybackFailure(Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;Ljava/lang/String;)V
    .locals 1
    .parameter "errorType"
    .parameter "errorDescription"

    .prologue
    .line 463
    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->errorListener:Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;

    invoke-virtual {v0, p1, p2}, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;->onError(Lcom/flixster/android/drm/WidevinePlayer$PlaybackError;Ljava/lang/String;)V

    .line 464
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 273
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 274
    const-string v3, "FlxDrm"

    const-string v4, "WidevinePlayer.onCreate"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v3

    const/16 v4, 0x9

    if-ge v3, v4, :cond_1

    .line 277
    invoke-virtual {p0, v6}, Lcom/flixster/android/drm/WidevinePlayer;->setRequestedOrientation(I)V

    .line 281
    :goto_0
    invoke-static {p0}, Lcom/flixster/android/utils/VersionedViewHelper;->hideSystemNavigation(Landroid/app/Activity;)V

    .line 282
    invoke-virtual {p0}, Lcom/flixster/android/drm/WidevinePlayer;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 284
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->outMetrics:Landroid/util/DisplayMetrics;

    .line 285
    invoke-virtual {p0}, Lcom/flixster/android/drm/WidevinePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->outMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v3, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 287
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v3

    invoke-interface {v3, p0}, Lcom/flixster/android/drm/PlaybackManager;->setPlayer(Lcom/flixster/android/drm/WidevinePlayer;)V

    .line 289
    const v3, 0x7f030092

    invoke-virtual {p0, v3}, Lcom/flixster/android/drm/WidevinePlayer;->setContentView(I)V

    .line 290
    const v3, 0x7f0702dd

    invoke-virtual {p0, v3}, Lcom/flixster/android/drm/WidevinePlayer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/VideoView;

    iput-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    .line 291
    iget-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    new-instance v4, Landroid/widget/MediaController;

    invoke-direct {v4, p0}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 292
    iget-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    new-instance v4, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;

    invoke-direct {v4, p0, v5}, Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;-><init>(Lcom/flixster/android/drm/WidevinePlayer;Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;)V

    iput-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->errorListener:Lcom/flixster/android/drm/WidevinePlayer$ErrorListener;

    invoke-virtual {v3, v4}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 293
    iget-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    new-instance v4, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;

    invoke-direct {v4, p0, v5}, Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;-><init>(Lcom/flixster/android/drm/WidevinePlayer;Lcom/flixster/android/drm/WidevinePlayer$PreparedListener;)V

    invoke-virtual {v3, v4}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 294
    iget-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    new-instance v4, Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;

    invoke-direct {v4, p0, v5}, Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;-><init>(Lcom/flixster/android/drm/WidevinePlayer;Lcom/flixster/android/drm/WidevinePlayer$CompletionListener;)V

    invoke-virtual {v3, v4}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 295
    iget-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v3}, Landroid/widget/VideoView;->requestFocus()Z

    .line 297
    new-instance v3, Landroid/app/ProgressDialog;

    invoke-direct {v3, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;

    .line 298
    iget-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v7}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 300
    invoke-virtual {p0}, Lcom/flixster/android/drm/WidevinePlayer;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 301
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "KEY_RIGHT_ID"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->rightId:J

    .line 302
    const-string v3, "KEY_PLAY_POSITION"

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 303
    .local v1, playPosition:I
    const-string v3, "KEY_CAPTIONS_URI"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionsUri:Ljava/lang/String;

    .line 304
    const-string v3, "restartTime"

    invoke-virtual {p0, v3, v6}, Lcom/flixster/android/drm/WidevinePlayer;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 305
    .local v2, prefs:Landroid/content/SharedPreferences;
    if-ltz v1, :cond_2

    mul-int/lit16 v3, v1, 0x3e8

    :goto_1
    iput v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    .line 306
    iput-boolean v7, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackActive:Z

    .line 308
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCaptionsEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionsUri:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 309
    const/4 v3, 0x4

    new-array v3, v3, [Landroid/widget/TextView;

    iput-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;

    .line 310
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;

    const v3, 0x7f0702e2

    invoke-virtual {p0, v3}, Lcom/flixster/android/drm/WidevinePlayer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v4, v6

    .line 311
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;

    const v3, 0x7f0702e3

    invoke-virtual {p0, v3}, Lcom/flixster/android/drm/WidevinePlayer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v4, v7

    .line 312
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;

    const/4 v5, 0x2

    const v3, 0x7f0702e4

    invoke-virtual {p0, v3}, Lcom/flixster/android/drm/WidevinePlayer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v4, v5

    .line 313
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionViews:[Landroid/widget/TextView;

    const/4 v5, 0x3

    const v3, 0x7f0702e5

    invoke-virtual {p0, v3}, Lcom/flixster/android/drm/WidevinePlayer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v4, v5

    .line 314
    iget-object v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->successHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->errorHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer;->captionsUri:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lnet/flixster/android/data/MiscDao;->fetchCaptions(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    .line 316
    :cond_0
    return-void

    .line 279
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #playPosition:I
    .end local v2           #prefs:Landroid/content/SharedPreferences;
    :cond_1
    const/4 v3, 0x6

    invoke-virtual {p0, v3}, Lcom/flixster/android/drm/WidevinePlayer;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 305
    .restart local v0       #intent:Landroid/content/Intent;
    .restart local v1       #playPosition:I
    .restart local v2       #prefs:Landroid/content/SharedPreferences;
    :cond_2
    iget-wide v3, p0, Lcom/flixster/android/drm/WidevinePlayer;->rightId:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 443
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 444
    const-string v1, "FlxDrm"

    const-string v2, "WidevinePlayer.onDestroy"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/flixster/android/drm/PlaybackManager;->setPlayer(Lcom/flixster/android/drm/WidevinePlayer;)V

    .line 450
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    :goto_0
    return-void

    .line 451
    :catch_0
    move-exception v0

    .line 453
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 403
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 404
    const-string v6, "FlxDrm"

    const-string v7, "WidevinePlayer.onPause"

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    iget-boolean v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackPrepared:Z

    if-eqz v6, :cond_2

    .line 407
    const-string v6, "restartTime"

    invoke-virtual {p0, v6, v5}, Lcom/flixster/android/drm/WidevinePlayer;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 408
    .local v4, prefs:Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 409
    .local v3, editor:Landroid/content/SharedPreferences$Editor;
    iget-object v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v6}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v1

    .line 410
    .local v1, currentPosition:I
    iget v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    sub-int v6, v1, v6

    div-int/lit16 v6, v6, 0x3e8

    const/4 v7, 0x1

    if-le v6, v7, :cond_0

    .line 411
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getConnectionType()Ljava/lang/String;

    move-result-object v0

    .line 412
    .local v0, connectionType:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "playback_duration_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    sub-int v7, v1, v7

    div-int/lit16 v7, v7, 0x3e8

    invoke-direct {p0, v6, v7}, Lcom/flixster/android/drm/WidevinePlayer;->trackPlaybackEvent(Ljava/lang/String;I)V

    .line 414
    .end local v0           #connectionType:Ljava/lang/String;
    :cond_0
    iget-boolean v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackCompleted:Z

    if-eqz v6, :cond_1

    move v1, v5

    .end local v1           #currentPosition:I
    :cond_1
    iput v1, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    .line 415
    iget-wide v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->rightId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 416
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 417
    const-string v6, "FlxDrm"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "WidevinePlayer.onPause rightId "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, p0, Lcom/flixster/android/drm/WidevinePlayer;->rightId:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", saving seek time "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    .end local v3           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v4           #prefs:Landroid/content/SharedPreferences;
    :cond_2
    iget-object v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v6}, Landroid/widget/VideoView;->stopPlayback()V

    .line 420
    iget-object v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 421
    iget-object v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 423
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v6

    invoke-interface {v6}, Lcom/flixster/android/drm/PlaybackManager;->wvStop()V

    .line 424
    iput-boolean v5, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackActive:Z

    .line 426
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v5

    invoke-interface {v5}, Lcom/flixster/android/drm/PlaybackManager;->isStreamingMode()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 427
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v6

    iget-boolean v5, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackPrepared:Z

    if-eqz v5, :cond_5

    iget v5, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    div-int/lit16 v5, v5, 0x3e8

    :goto_0
    invoke-interface {v6, v5}, Lcom/flixster/android/drm/PlaybackManager;->streamStop(I)V

    .line 433
    :cond_3
    :goto_1
    :try_start_0
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer;->errorDialog:Landroid/app/AlertDialog;

    if-eqz v5, :cond_4

    .line 434
    iget-object v5, p0, Lcom/flixster/android/drm/WidevinePlayer;->errorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 439
    :cond_4
    :goto_2
    return-void

    .line 427
    :cond_5
    const/4 v5, -0x1

    goto :goto_0

    .line 428
    :cond_6
    iget-boolean v5, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackPrepared:Z

    if-eqz v5, :cond_3

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 429
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v5

    iget v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->startTime:I

    div-int/lit16 v6, v6, 0x3e8

    invoke-interface {v5, v6}, Lcom/flixster/android/drm/PlaybackManager;->updatePlaybackPosition(I)V

    goto :goto_1

    .line 436
    :catch_0
    move-exception v2

    .line 437
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 320
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 321
    const-string v4, "FlxDrm"

    const-string v5, "WidevinePlayer.onResume"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getConnectionType()Ljava/lang/String;

    move-result-object v0

    .line 324
    .local v0, connectionType:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "playback_init_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v6}, Lcom/flixster/android/drm/WidevinePlayer;->trackPlaybackEvent(Ljava/lang/String;I)V

    .line 326
    iget-boolean v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackActive:Z

    if-nez v4, :cond_0

    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/flixster/android/drm/PlaybackManager;->isStreamingMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 327
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/flixster/android/drm/PlaybackManager;->streamResume()V

    .line 330
    :cond_0
    const/4 v3, 0x0

    .line 331
    .local v3, uri:Landroid/net/Uri;
    iget-boolean v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackActive:Z

    if-nez v4, :cond_2

    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v4

    invoke-interface {v4}, Lcom/flixster/android/drm/PlaybackLogic;->isNativeWv()Z

    move-result v4

    if-nez v4, :cond_2

    .line 332
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/flixster/android/drm/PlaybackManager;->wvResume()Landroid/net/Uri;

    move-result-object v3

    .line 333
    if-nez v3, :cond_1

    .line 334
    invoke-virtual {p0}, Lcom/flixster/android/drm/WidevinePlayer;->finish()V

    .line 340
    :cond_1
    :goto_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackActive:Z

    .line 341
    iput-boolean v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->isPlaybackPrepared:Z

    .line 342
    iput-boolean v6, p0, Lcom/flixster/android/drm/WidevinePlayer;->hasPlaybackFailed:Z

    .line 344
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v4, v6}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 345
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v4, v3}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 347
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v4, v6}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 348
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/flixster/android/drm/WidevinePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0135

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 349
    iget-object v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->show()V

    .line 350
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/flixster/android/drm/WidevinePlayer;->startLoadingTime:J

    .line 352
    new-instance v2, Ljava/lang/Thread;

    new-instance v4, Lcom/flixster/android/drm/WidevinePlayer$6;

    invoke-direct {v4, p0}, Lcom/flixster/android/drm/WidevinePlayer$6;-><init>(Lcom/flixster/android/drm/WidevinePlayer;)V

    invoke-direct {v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 365
    .local v2, progressMonitorThread:Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 366
    return-void

    .line 337
    .end local v2           #progressMonitorThread:Ljava/lang/Thread;
    :cond_2
    invoke-virtual {p0}, Lcom/flixster/android/drm/WidevinePlayer;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 338
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .parameter "hasFocus"

    .prologue
    .line 395
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 396
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/flixster/android/drm/WidevinePlayer;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    invoke-static {p0}, Lcom/flixster/android/utils/VersionedViewHelper;->hideSystemNavigation(Landroid/app/Activity;)V

    .line 399
    :cond_0
    return-void
.end method
