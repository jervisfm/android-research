.class public interface abstract Lcom/flixster/android/model/HeadlineItem;
.super Ljava/lang/Object;
.source "HeadlineItem.java"


# virtual methods
.method public abstract getBitmap(Landroid/widget/ImageView;Landroid/widget/ImageView$ScaleType;)Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;",
            "Landroid/widget/ImageView$ScaleType;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation
.end method

.method public abstract getImageUrl()Ljava/lang/String;
.end method

.method public abstract getLinkUrl()Ljava/lang/String;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method
