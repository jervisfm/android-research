.class Lcom/flixster/android/model/Image$ImageHandler;
.super Landroid/os/Handler;
.source "Image.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/model/Image;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/model/Image;


# direct methods
.method private constructor <init>(Lcom/flixster/android/model/Image;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/flixster/android/model/Image$ImageHandler;->this$0:Lcom/flixster/android/model/Image;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/model/Image;Lcom/flixster/android/model/Image$ImageHandler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/flixster/android/model/Image$ImageHandler;-><init>(Lcom/flixster/android/model/Image;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 67
    iget-object v1, p0, Lcom/flixster/android/model/Image$ImageHandler;->this$0:Lcom/flixster/android/model/Image;

    const/4 v2, 0x0

    #setter for: Lcom/flixster/android/model/Image;->isLoading:Z
    invoke-static {v1, v2}, Lcom/flixster/android/model/Image;->access$0(Lcom/flixster/android/model/Image;Z)V

    .line 68
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 69
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/flixster/android/model/Image$ImageHandler;->this$0:Lcom/flixster/android/model/Image;

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    #setter for: Lcom/flixster/android/model/Image;->softBitmap:Ljava/lang/ref/SoftReference;
    invoke-static {v1, v2}, Lcom/flixster/android/model/Image;->access$1(Lcom/flixster/android/model/Image;Ljava/lang/ref/SoftReference;)V

    .line 70
    iget-object v1, p0, Lcom/flixster/android/model/Image$ImageHandler;->this$0:Lcom/flixster/android/model/Image;

    #getter for: Lcom/flixster/android/model/Image;->callback:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/model/Image;->access$2(Lcom/flixster/android/model/Image;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/flixster/android/model/Image$ImageHandler;->this$0:Lcom/flixster/android/model/Image;

    #getter for: Lcom/flixster/android/model/Image;->callback:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/model/Image;->access$2(Lcom/flixster/android/model/Image;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 72
    iget-object v1, p0, Lcom/flixster/android/model/Image$ImageHandler;->this$0:Lcom/flixster/android/model/Image;

    const/4 v2, 0x0

    #setter for: Lcom/flixster/android/model/Image;->callback:Landroid/os/Handler;
    invoke-static {v1, v2}, Lcom/flixster/android/model/Image;->access$3(Lcom/flixster/android/model/Image;Landroid/os/Handler;)V

    .line 74
    :cond_0
    return-void
.end method
