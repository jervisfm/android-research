.class public Lcom/flixster/android/model/Image;
.super Ljava/lang/Object;
.source "Image.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/model/Image$ImageHandler;
    }
.end annotation


# instance fields
.field private callback:Landroid/os/Handler;

.field protected imageUrl:Ljava/lang/String;

.field private isLoading:Z

.field private softBitmap:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter "imageUrl"

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/flixster/android/model/Image;->imageUrl:Ljava/lang/String;

    .line 29
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/model/Image;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/flixster/android/model/Image;->isLoading:Z

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/model/Image;Ljava/lang/ref/SoftReference;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20
    iput-object p1, p0, Lcom/flixster/android/model/Image;->softBitmap:Ljava/lang/ref/SoftReference;

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/model/Image;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/flixster/android/model/Image;->callback:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/model/Image;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21
    iput-object p1, p0, Lcom/flixster/android/model/Image;->callback:Landroid/os/Handler;

    return-void
.end method

.method private declared-synchronized load(Landroid/os/Handler;)V
    .locals 4
    .parameter "callback"

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/model/Image;->imageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/flixster/android/model/Image;->isLoading:Z

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/model/Image;->isLoading:Z

    .line 60
    iput-object p1, p0, Lcom/flixster/android/model/Image;->callback:Landroid/os/Handler;

    .line 61
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/model/Image;->imageUrl:Ljava/lang/String;

    new-instance v2, Lcom/flixster/android/model/Image$ImageHandler;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/flixster/android/model/Image$ImageHandler;-><init>(Lcom/flixster/android/model/Image;Lcom/flixster/android/model/Image$ImageHandler;)V

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    :cond_0
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;Landroid/widget/ImageView$ScaleType;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap(Landroid/widget/ImageView;Landroid/widget/ImageView$ScaleType;)Landroid/graphics/Bitmap;
    .locals 3
    .parameter
    .parameter "scaleType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;",
            "Landroid/widget/ImageView$ScaleType;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    const/4 v0, 0x0

    .line 38
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/flixster/android/model/Image;->softBitmap:Ljava/lang/ref/SoftReference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/flixster/android/model/Image;->softBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    .line 39
    new-instance v1, Lcom/flixster/android/activity/ImageViewHandler;

    iget-object v2, p0, Lcom/flixster/android/model/Image;->imageUrl:Ljava/lang/String;

    invoke-direct {v1, p1, v2, p2}, Lcom/flixster/android/activity/ImageViewHandler;-><init>(Landroid/widget/ImageView;Ljava/lang/String;Landroid/widget/ImageView$ScaleType;)V

    invoke-direct {p0, v1}, Lcom/flixster/android/model/Image;->load(Landroid/os/Handler;)V

    .line 41
    :cond_1
    return-object v0
.end method

.method public getBitmap(Lcom/flixster/android/activity/ImageViewHandler;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter "callback"

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/flixster/android/model/Image;->softBitmap:Ljava/lang/ref/SoftReference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/flixster/android/model/Image;->softBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    .line 48
    invoke-direct {p0, p1}, Lcom/flixster/android/model/Image;->load(Landroid/os/Handler;)V

    .line 50
    :cond_1
    return-object v0
.end method
