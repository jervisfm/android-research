.class public Lcom/flixster/android/model/NamedList;
.super Ljava/lang/Object;
.source "NamedList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .parameter "name"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 9
    .local p0, this:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<TT;>;"
    .local p2, list:Ljava/util/List;,"Ljava/util/List<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/flixster/android/model/NamedList;->name:Ljava/lang/String;

    .line 11
    iput-object p2, p0, Lcom/flixster/android/model/NamedList;->list:Ljava/util/List;

    .line 12
    return-void
.end method


# virtual methods
.method public getList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, this:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<TT;>;"
    iget-object v0, p0, Lcom/flixster/android/model/NamedList;->list:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    .local p0, this:Lcom/flixster/android/model/NamedList;,"Lcom/flixster/android/model/NamedList<TT;>;"
    iget-object v0, p0, Lcom/flixster/android/model/NamedList;->name:Ljava/lang/String;

    return-object v0
.end method
