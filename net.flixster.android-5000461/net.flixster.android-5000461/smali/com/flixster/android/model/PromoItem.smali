.class public Lcom/flixster/android/model/PromoItem;
.super Lcom/flixster/android/model/Image;
.source "PromoItem.java"

# interfaces
.implements Lcom/flixster/android/model/HeadlineItem;


# instance fields
.field protected header:Ljava/lang/String;

.field protected linkUrl:Ljava/lang/String;

.field protected sequence:I


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/flixster/android/model/Image;-><init>()V

    .line 15
    return-void
.end method

.method protected static parse(Lorg/json/JSONObject;)Lcom/flixster/android/model/PromoItem;
    .locals 4
    .parameter "jsonObject"

    .prologue
    const/4 v3, 0x0

    .line 32
    const-string v1, "featured"

    const-string v2, "section"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/flixster/android/model/CarouselItem;

    invoke-direct {v0}, Lcom/flixster/android/model/CarouselItem;-><init>()V

    .line 33
    .local v0, promo:Lcom/flixster/android/model/PromoItem;
    :goto_0
    const-string v1, "header"

    invoke-virtual {p0, v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/flixster/android/model/PromoItem;->header:Ljava/lang/String;

    .line 34
    const-string v1, "imageUrl"

    invoke-virtual {p0, v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/flixster/android/model/PromoItem;->imageUrl:Ljava/lang/String;

    .line 35
    const-string v1, "linkUrl"

    invoke-virtual {p0, v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/flixster/android/model/PromoItem;->linkUrl:Ljava/lang/String;

    .line 36
    const-string v1, "sequence"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/flixster/android/model/PromoItem;->sequence:I

    .line 37
    return-object v0

    .line 32
    .end local v0           #promo:Lcom/flixster/android/model/PromoItem;
    :cond_0
    new-instance v0, Lcom/flixster/android/model/PromoItem;

    invoke-direct {v0}, Lcom/flixster/android/model/PromoItem;-><init>()V

    goto :goto_0
.end method

.method public static parseArray(Lorg/json/JSONArray;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .parameter "jsonArray"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/model/CarouselItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/model/PromoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, featuredItems:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/model/CarouselItem;>;"
    .local p2, hotTodayItems:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/model/PromoItem;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 29
    return-void

    .line 19
    :cond_0
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 20
    .local v2, jsonObject:Lorg/json/JSONObject;
    if-eqz v2, :cond_1

    .line 21
    invoke-static {v2}, Lcom/flixster/android/model/PromoItem;->parse(Lorg/json/JSONObject;)Lcom/flixster/android/model/PromoItem;

    move-result-object v1

    .line 22
    .local v1, item:Lcom/flixster/android/model/PromoItem;
    instance-of v3, v1, Lcom/flixster/android/model/CarouselItem;

    if-eqz v3, :cond_2

    .line 23
    check-cast v1, Lcom/flixster/android/model/CarouselItem;

    .end local v1           #item:Lcom/flixster/android/model/PromoItem;
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25
    .restart local v1       #item:Lcom/flixster/android/model/PromoItem;
    :cond_2
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/flixster/android/model/PromoItem;->imageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/flixster/android/model/PromoItem;->linkUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSequence()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/flixster/android/model/PromoItem;->sequence:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/flixster/android/model/PromoItem;->header:Ljava/lang/String;

    return-object v0
.end method
