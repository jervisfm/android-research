.class public Lcom/flixster/android/msk/FacebookInvitePage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "FacebookInvitePage.java"


# static fields
.field private static final FBINVITE_REWARDS_DEFAULT:[I


# instance fields
.field private final friendsIdsHandler:Landroid/os/Handler;

.field private rewardLevels:[I

.field private sendInvites:Landroid/widget/ImageView;

.field private final sendInvitesClickListener:Landroid/view/View$OnClickListener;

.field private signupLevel1:Landroid/widget/TextView;

.field private signupLevel2:Landroid/widget/TextView;

.field private signupLevel3:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/flixster/android/msk/FacebookInvitePage;->FBINVITE_REWARDS_DEFAULT:[I

    .line 20
    return-void

    .line 21
    nop

    :array_0
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0xat 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 57
    new-instance v0, Lcom/flixster/android/msk/FacebookInvitePage$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/msk/FacebookInvitePage$1;-><init>(Lcom/flixster/android/msk/FacebookInvitePage;)V

    iput-object v0, p0, Lcom/flixster/android/msk/FacebookInvitePage;->sendInvitesClickListener:Landroid/view/View$OnClickListener;

    .line 64
    new-instance v0, Lcom/flixster/android/msk/FacebookInvitePage$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/msk/FacebookInvitePage$2;-><init>(Lcom/flixster/android/msk/FacebookInvitePage;)V

    iput-object v0, p0, Lcom/flixster/android/msk/FacebookInvitePage;->friendsIdsHandler:Landroid/os/Handler;

    .line 20
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/msk/FacebookInvitePage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookInvitePage;->friendsIdsHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private initializeInviteViews()V
    .locals 7

    .prologue
    const v6, 0x7f0c015d

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 52
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookInvitePage;->signupLevel1:Landroid/widget/TextView;

    const v1, 0x7f0c015c

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FacebookInvitePage;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/flixster/android/msk/FacebookInvitePage;->rewardLevels:[I

    aget v3, v3, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookInvitePage;->signupLevel2:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/flixster/android/msk/FacebookInvitePage;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/flixster/android/msk/FacebookInvitePage;->rewardLevels:[I

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookInvitePage;->signupLevel3:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/flixster/android/msk/FacebookInvitePage;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/flixster/android/msk/FacebookInvitePage;->rewardLevels:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const v1, 0x7f030028

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FacebookInvitePage;->setContentView(I)V

    .line 31
    invoke-virtual {p0}, Lcom/flixster/android/msk/FacebookInvitePage;->createActionBar()V

    .line 32
    const v1, 0x7f0c0150

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FacebookInvitePage;->setActionBarTitle(I)V

    .line 34
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 35
    .local v0, p:Lnet/flixster/android/model/Property;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lnet/flixster/android/model/Property;->rewardsFbInviteArray:[I

    if-nez v1, :cond_1

    :cond_0
    sget-object v1, Lcom/flixster/android/msk/FacebookInvitePage;->FBINVITE_REWARDS_DEFAULT:[I

    :goto_0
    iput-object v1, p0, Lcom/flixster/android/msk/FacebookInvitePage;->rewardLevels:[I

    .line 36
    const v1, 0x7f070078

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FacebookInvitePage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/msk/FacebookInvitePage;->signupLevel1:Landroid/widget/TextView;

    .line 37
    const v1, 0x7f070079

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FacebookInvitePage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/msk/FacebookInvitePage;->signupLevel2:Landroid/widget/TextView;

    .line 38
    const v1, 0x7f07007a

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FacebookInvitePage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/msk/FacebookInvitePage;->signupLevel3:Landroid/widget/TextView;

    .line 39
    const v1, 0x7f07007b

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FacebookInvitePage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/flixster/android/msk/FacebookInvitePage;->sendInvites:Landroid/widget/ImageView;

    .line 40
    iget-object v1, p0, Lcom/flixster/android/msk/FacebookInvitePage;->sendInvites:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/flixster/android/msk/FacebookInvitePage;->sendInvitesClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/rewards/fb-invites"

    const-string v3, "Rewards - FBInvites"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    return-void

    .line 35
    :cond_1
    iget-object v1, v0, Lnet/flixster/android/model/Property;->rewardsFbInviteArray:[I

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 48
    invoke-direct {p0}, Lcom/flixster/android/msk/FacebookInvitePage;->initializeInviteViews()V

    .line 49
    return-void
.end method
