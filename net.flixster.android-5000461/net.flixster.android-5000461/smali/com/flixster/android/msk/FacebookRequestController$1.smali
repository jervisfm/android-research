.class Lcom/flixster/android/msk/FacebookRequestController$1;
.super Ljava/lang/Object;
.source "FacebookRequestController.java"

# interfaces
.implements Lcom/facebook/android/Facebook$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/msk/FacebookRequestController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/msk/FacebookRequestController;


# direct methods
.method constructor <init>(Lcom/flixster/android/msk/FacebookRequestController;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #calls: Lcom/flixster/android/msk/FacebookRequestController;->proceed()V
    invoke-static {v0}, Lcom/flixster/android/msk/FacebookRequestController;->access$7(Lcom/flixster/android/msk/FacebookRequestController;)V

    .line 107
    return-void
.end method

.method public onComplete(Landroid/os/Bundle;)V
    .locals 8
    .parameter "values"

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/os/Bundle;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 57
    iget-object v4, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->numOfRecipientsSent:I
    invoke-static {v4}, Lcom/flixster/android/msk/FacebookRequestController;->access$0(Lcom/flixster/android/msk/FacebookRequestController;)I

    move-result v5

    invoke-virtual {p1}, Landroid/os/Bundle;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    add-int/2addr v5, v6

    #setter for: Lcom/flixster/android/msk/FacebookRequestController;->numOfRecipientsSent:I
    invoke-static {v4, v5}, Lcom/flixster/android/msk/FacebookRequestController;->access$1(Lcom/flixster/android/msk/FacebookRequestController;I)V

    .line 59
    :cond_0
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 63
    #calls: Lcom/flixster/android/msk/FacebookRequestController;->wasRequestSent(Landroid/os/Bundle;)Z
    invoke-static {p1}, Lcom/flixster/android/msk/FacebookRequestController;->access$2(Landroid/os/Bundle;)Z

    move-result v3

    .line 64
    .local v3, wasRequestSent:Z
    if-eqz v3, :cond_1

    .line 65
    const-string v4, "request"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 66
    .local v2, requestId:Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 67
    invoke-static {v2}, Lnet/flixster/android/data/ProfileDao;->trackMskFbRequest(Ljava/lang/String;)V

    .line 71
    .end local v2           #requestId:Ljava/lang/String;
    :cond_1
    if-eqz v3, :cond_3

    iget-object v4, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->recipientGroups:Ljava/util/List;
    invoke-static {v4}, Lcom/flixster/android/msk/FacebookRequestController;->access$3(Lcom/flixster/android/msk/FacebookRequestController;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 72
    iget-object v4, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->recipientGroups:Ljava/util/List;
    invoke-static {v4}, Lcom/flixster/android/msk/FacebookRequestController;->access$3(Lcom/flixster/android/msk/FacebookRequestController;)Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 73
    .local v1, recipients:Ljava/lang/String;
    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    iget-object v5, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->requestListener:Lcom/facebook/android/Facebook$DialogListener;
    invoke-static {v5}, Lcom/flixster/android/msk/FacebookRequestController;->access$4(Lcom/flixster/android/msk/FacebookRequestController;)Lcom/facebook/android/Facebook$DialogListener;

    move-result-object v5

    iget-object v6, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/flixster/android/msk/FacebookRequestController;->access$5(Lcom/flixster/android/msk/FacebookRequestController;)Landroid/app/Activity;

    move-result-object v6

    invoke-static {v4, v1, v5, v6}, Lcom/flixster/android/data/FacebookFacade;->sendRequest(Lcom/facebook/android/Facebook;Ljava/lang/String;Lcom/facebook/android/Facebook$DialogListener;Landroid/content/Context;)V

    .line 88
    .end local v1           #recipients:Ljava/lang/String;
    :goto_1
    return-void

    .line 59
    .end local v3           #wasRequestSent:Z
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 60
    .local v0, key:Ljava/lang/String;
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "FacebookRequestController.onComplete key="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " value="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->si(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 75
    .end local v0           #key:Ljava/lang/String;
    .restart local v3       #wasRequestSent:Z
    :cond_3
    iget-object v4, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/flixster/android/msk/FacebookRequestController;->access$5(Lcom/flixster/android/msk/FacebookRequestController;)Landroid/app/Activity;

    move-result-object v4

    instance-of v4, v4, Lcom/flixster/android/msk/MskEntryActivity;

    if-eqz v4, :cond_5

    .line 76
    iget-object v4, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->isTypePick:Z
    invoke-static {v4}, Lcom/flixster/android/msk/FacebookRequestController;->access$6(Lcom/flixster/android/msk/FacebookRequestController;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 77
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->numOfRecipientsSent:I
    invoke-static {v5}, Lcom/flixster/android/msk/FacebookRequestController;->access$0(Lcom/flixster/android/msk/FacebookRequestController;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/flixster/android/msk/MskController;->trackFbRequestPickEventSent(I)V

    .line 78
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->numOfRecipientsSent:I
    invoke-static {v5}, Lcom/flixster/android/msk/FacebookRequestController;->access$0(Lcom/flixster/android/msk/FacebookRequestController;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/flixster/android/msk/MskController;->trackFbRequestPickEventSentHisto(I)V

    .line 86
    :goto_2
    iget-object v4, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #calls: Lcom/flixster/android/msk/FacebookRequestController;->proceed()V
    invoke-static {v4}, Lcom/flixster/android/msk/FacebookRequestController;->access$7(Lcom/flixster/android/msk/FacebookRequestController;)V

    goto :goto_1

    .line 80
    :cond_4
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->numOfRecipientsSent:I
    invoke-static {v5}, Lcom/flixster/android/msk/FacebookRequestController;->access$0(Lcom/flixster/android/msk/FacebookRequestController;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/flixster/android/msk/MskController;->trackFbRequestAllEventSent(I)V

    .line 81
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->numOfRecipientsSent:I
    invoke-static {v5}, Lcom/flixster/android/msk/FacebookRequestController;->access$0(Lcom/flixster/android/msk/FacebookRequestController;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/flixster/android/msk/MskController;->trackFbRequestAllEventSentHisto(I)V

    goto :goto_2

    .line 84
    :cond_5
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v4

    iget-object v5, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->numOfRecipientsSent:I
    invoke-static {v5}, Lcom/flixster/android/msk/FacebookRequestController;->access$0(Lcom/flixster/android/msk/FacebookRequestController;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/flixster/android/msk/MskController;->trackFbRequestInvitesRewardEvent(I)V

    goto :goto_2
.end method

.method public onError(Lcom/facebook/android/DialogError;)V
    .locals 3
    .parameter "e"

    .prologue
    .line 99
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FacebookRequestController.onError "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/android/DialogError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/flixster/android/msk/FacebookRequestController;->access$5(Lcom/flixster/android/msk/FacebookRequestController;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/android/DialogError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 101
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #calls: Lcom/flixster/android/msk/FacebookRequestController;->proceed()V
    invoke-static {v0}, Lcom/flixster/android/msk/FacebookRequestController;->access$7(Lcom/flixster/android/msk/FacebookRequestController;)V

    .line 102
    return-void
.end method

.method public onFacebookError(Lcom/facebook/android/FacebookError;)V
    .locals 3
    .parameter "e"

    .prologue
    .line 92
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FacebookRequestController.onFacebookError "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #getter for: Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/flixster/android/msk/FacebookRequestController;->access$5(Lcom/flixster/android/msk/FacebookRequestController;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Facebook Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 94
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController$1;->this$0:Lcom/flixster/android/msk/FacebookRequestController;

    #calls: Lcom/flixster/android/msk/FacebookRequestController;->proceed()V
    invoke-static {v0}, Lcom/flixster/android/msk/FacebookRequestController;->access$7(Lcom/flixster/android/msk/FacebookRequestController;)V

    .line 95
    return-void
.end method
