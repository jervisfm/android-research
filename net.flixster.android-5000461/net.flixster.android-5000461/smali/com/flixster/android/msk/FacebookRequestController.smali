.class public Lcom/flixster/android/msk/FacebookRequestController;
.super Ljava/lang/Object;
.source "FacebookRequestController.java"


# static fields
.field private static final MAX_RECIPIENTS_PER_REQUEST:I


# instance fields
.field private final activity:Landroid/app/Activity;

.field private isTypePick:Z

.field private numOfRecipientsSent:I

.field private final recipientGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final requestListener:Lcom/facebook/android/Facebook$DialogListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/16 v0, 0x32

    .line 23
    sput v0, Lcom/flixster/android/msk/FacebookRequestController;->MAX_RECIPIENTS_PER_REQUEST:I

    .line 22
    return-void
.end method

.method protected constructor <init>(Landroid/app/Activity;[Ljava/lang/Object;)V
    .locals 1
    .parameter "activity"
    .parameter "friendsIds"

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lcom/flixster/android/msk/FacebookRequestController$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/msk/FacebookRequestController$1;-><init>(Lcom/flixster/android/msk/FacebookRequestController;)V

    iput-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController;->requestListener:Lcom/facebook/android/Facebook$DialogListener;

    .line 32
    iput-object p1, p0, Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;

    .line 33
    if-nez p2, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController;->recipientGroups:Ljava/util/List;

    .line 34
    return-void

    .line 33
    :cond_0
    invoke-static {p2}, Lcom/flixster/android/msk/FacebookRequestController;->convertToRecipientStrings([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/flixster/android/msk/FacebookRequestController;)I
    .locals 1
    .parameter

    .prologue
    .line 29
    iget v0, p0, Lcom/flixster/android/msk/FacebookRequestController;->numOfRecipientsSent:I

    return v0
.end method

.method static synthetic access$1(Lcom/flixster/android/msk/FacebookRequestController;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 29
    iput p1, p0, Lcom/flixster/android/msk/FacebookRequestController;->numOfRecipientsSent:I

    return-void
.end method

.method static synthetic access$2(Landroid/os/Bundle;)Z
    .locals 1
    .parameter

    .prologue
    .line 117
    invoke-static {p0}, Lcom/flixster/android/msk/FacebookRequestController;->wasRequestSent(Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/flixster/android/msk/FacebookRequestController;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController;->recipientGroups:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/msk/FacebookRequestController;)Lcom/facebook/android/Facebook$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController;->requestListener:Lcom/facebook/android/Facebook$DialogListener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/msk/FacebookRequestController;)Landroid/app/Activity;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$6(Lcom/flixster/android/msk/FacebookRequestController;)Z
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/flixster/android/msk/FacebookRequestController;->isTypePick:Z

    return v0
.end method

.method static synthetic access$7(Lcom/flixster/android/msk/FacebookRequestController;)V
    .locals 0
    .parameter

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/flixster/android/msk/FacebookRequestController;->proceed()V

    return-void
.end method

.method private static convertToRecipientStrings([Ljava/lang/Object;)Ljava/util/List;
    .locals 5
    .parameter "ids"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 124
    .local v1, recipients:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .local v2, sb:Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    .local v0, i:I
    :goto_0
    array-length v3, p0

    add-int/lit8 v3, v3, 0x1

    if-lt v0, v3, :cond_0

    .line 132
    return-object v1

    .line 126
    :cond_0
    add-int/lit8 v3, v0, -0x1

    aget-object v3, p0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    sget v3, Lcom/flixster/android/msk/FacebookRequestController;->MAX_RECIPIENTS_PER_REQUEST:I

    rem-int v3, v0, v3

    if-eqz v3, :cond_1

    array-length v3, p0

    if-ne v0, v3, :cond_2

    .line 128
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    .end local v2           #sb:Ljava/lang/StringBuilder;
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .restart local v2       #sb:Ljava/lang/StringBuilder;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private proceed()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;

    instance-of v0, v0, Lcom/flixster/android/msk/MskEntryActivity;

    if-eqz v0, :cond_0

    .line 112
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v1

    iget-object v0, p0, Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;

    check-cast v0, Lcom/flixster/android/msk/MskEntryActivity;

    invoke-virtual {v1, v0}, Lcom/flixster/android/msk/MskController;->handleNativeFbRequestStepCallback(Lcom/flixster/android/msk/MskEntryActivity;)V

    .line 114
    :cond_0
    return-void
.end method

.method private static wasRequestSent(Landroid/os/Bundle;)Z
    .locals 2
    .parameter "values"

    .prologue
    const/4 v0, 0x1

    .line 118
    invoke-virtual {p0}, Landroid/os/Bundle;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    const-string v1, "request"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected start()V
    .locals 5

    .prologue
    .line 37
    iget-object v1, p0, Lcom/flixster/android/msk/FacebookRequestController;->recipientGroups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 38
    iget-object v1, p0, Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;

    instance-of v1, v1, Lcom/flixster/android/msk/MskEntryActivity;

    if-eqz v1, :cond_0

    .line 39
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController;->trackFbRequestAll()V

    .line 41
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/msk/FacebookRequestController;->recipientGroups:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 42
    .local v0, recipients:Ljava/lang/String;
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    iget-object v2, p0, Lcom/flixster/android/msk/FacebookRequestController;->requestListener:Lcom/facebook/android/Facebook$DialogListener;

    iget-object v3, p0, Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;

    invoke-static {v1, v0, v2, v3}, Lcom/flixster/android/data/FacebookFacade;->sendRequest(Lcom/facebook/android/Facebook;Ljava/lang/String;Lcom/facebook/android/Facebook$DialogListener;Landroid/content/Context;)V

    .line 50
    .end local v0           #recipients:Ljava/lang/String;
    :goto_0
    return-void

    .line 44
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/flixster/android/msk/FacebookRequestController;->isTypePick:Z

    .line 45
    iget-object v1, p0, Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;

    instance-of v1, v1, Lcom/flixster/android/msk/MskEntryActivity;

    if-eqz v1, :cond_2

    .line 46
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController;->trackFbRequestPick()V

    .line 48
    :cond_2
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/flixster/android/msk/FacebookRequestController;->requestListener:Lcom/facebook/android/Facebook$DialogListener;

    iget-object v4, p0, Lcom/flixster/android/msk/FacebookRequestController;->activity:Landroid/app/Activity;

    invoke-static {v1, v2, v3, v4}, Lcom/flixster/android/data/FacebookFacade;->sendRequest(Lcom/facebook/android/Facebook;Ljava/lang/String;Lcom/facebook/android/Facebook$DialogListener;Landroid/content/Context;)V

    goto :goto_0
.end method
