.class Lcom/flixster/android/msk/FriendSelectPage$3;
.super Ljava/lang/Object;
.source "FriendSelectPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/msk/FriendSelectPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/msk/FriendSelectPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/msk/FriendSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .parameter "view"

    .prologue
    .line 260
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    #getter for: Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I
    invoke-static {v0}, Lcom/flixster/android/msk/FriendSelectPage;->access$0(Lcom/flixster/android/msk/FriendSelectPage;)I

    move-result v0

    iget-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    #getter for: Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I
    invoke-static {v1}, Lcom/flixster/android/msk/FriendSelectPage;->access$1(Lcom/flixster/android/msk/FriendSelectPage;)[I

    move-result-object v1

    const/4 v2, 0x1

    aget v1, v1, v2

    if-lt v0, v1, :cond_5

    .line 261
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    iget-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    #getter for: Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I
    invoke-static {v1}, Lcom/flixster/android/msk/FriendSelectPage;->access$0(Lcom/flixster/android/msk/FriendSelectPage;)I

    move-result v1

    #calls: Lcom/flixster/android/msk/FriendSelectPage;->getGiftCount(I)I
    invoke-static {v0, v1}, Lcom/flixster/android/msk/FriendSelectPage;->access$2(Lcom/flixster/android/msk/FriendSelectPage;I)I

    move-result v7

    .line 262
    .local v7, giftCount:I
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    #getter for: Lcom/flixster/android/msk/FriendSelectPage;->isMsk:Z
    invoke-static {v0}, Lcom/flixster/android/msk/FriendSelectPage;->access$3(Lcom/flixster/android/msk/FriendSelectPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    #getter for: Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I
    invoke-static {v1}, Lcom/flixster/android/msk/FriendSelectPage;->access$0(Lcom/flixster/android/msk/FriendSelectPage;)I

    move-result v1

    invoke-virtual {v0, v7, v1}, Lcom/flixster/android/msk/MskController;->trackTextRequestAttemptEvent(II)V

    .line 269
    :goto_0
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v0, "sms:"

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 270
    .local v9, smsRecipients:Ljava/lang/StringBuilder;
    const/4 v8, 0x0

    .local v8, i:I
    :goto_1
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    #getter for: Lcom/flixster/android/msk/FriendSelectPage;->friends:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/msk/FriendSelectPage;->access$4(Lcom/flixster/android/msk/FriendSelectPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v8, v0, :cond_1

    .line 280
    new-instance v8, Landroid/content/Intent;

    .end local v8           #i:I
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 281
    .local v8, i:Landroid/content/Intent;
    const-string v0, "sms_body"

    iget-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    #getter for: Lcom/flixster/android/msk/FriendSelectPage;->body:Ljava/lang/String;
    invoke-static {v1}, Lcom/flixster/android/msk/FriendSelectPage;->access$5(Lcom/flixster/android/msk/FriendSelectPage;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    const/16 v1, 0x3ea

    invoke-virtual {v0, v8, v1}, Lcom/flixster/android/msk/FriendSelectPage;->startActivityForResult(Landroid/content/Intent;I)V

    .line 287
    .end local v7           #giftCount:I
    .end local v8           #i:Landroid/content/Intent;
    .end local v9           #smsRecipients:Ljava/lang/StringBuilder;
    :goto_2
    return-void

    .line 265
    .restart local v7       #giftCount:I
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/rewards"

    const/4 v2, 0x0

    const-string v3, "FlixsterRewards"

    const-string v4, "SendTextInvites"

    .line 266
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    #getter for: Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I
    invoke-static {v6}, Lcom/flixster/android/msk/FriendSelectPage;->access$0(Lcom/flixster/android/msk/FriendSelectPage;)I

    move-result v6

    .line 265
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 271
    .local v8, i:I
    .restart local v9       #smsRecipients:Ljava/lang/StringBuilder;
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    invoke-virtual {v0}, Lcom/flixster/android/msk/FriendSelectPage;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 272
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const-string v1, "sms:"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 273
    sget-object v0, Lcom/flixster/android/utils/F;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 274
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_4

    const-string v0, ","

    .line 273
    :goto_3
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_2
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    #getter for: Lcom/flixster/android/msk/FriendSelectPage;->friends:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/msk/FriendSelectPage;->access$4(Lcom/flixster/android/msk/FriendSelectPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Contact;

    iget-object v0, v0, Lnet/flixster/android/model/Contact;->number:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 274
    :cond_4
    const-string v0, ";"

    goto :goto_3

    .line 284
    .end local v7           #giftCount:I
    .end local v8           #i:I
    .end local v9           #smsRecipients:Ljava/lang/StringBuilder;
    :cond_5
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    iget-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage$3;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    const v2, 0x7f0c00fe

    invoke-virtual {v1, v2}, Lcom/flixster/android/msk/FriendSelectPage;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 285
    const/4 v2, 0x0

    .line 284
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 285
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method
