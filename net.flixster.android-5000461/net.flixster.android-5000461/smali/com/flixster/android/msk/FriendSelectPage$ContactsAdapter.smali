.class Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FriendSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/msk/FriendSelectPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lnet/flixster/android/model/Contact;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/msk/FriendSelectPage;


# direct methods
.method constructor <init>(Lcom/flixster/android/msk/FriendSelectPage;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216
    .local p3, contacts:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Contact;>;"
    iput-object p1, p0, Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    .line 217
    const v0, 0x1090010

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 218
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const v9, 0x7f0d0070

    const/16 v8, 0x11

    const/4 v5, 0x0

    .line 222
    if-nez p2, :cond_0

    .line 223
    iget-object v4, p0, Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    invoke-virtual {v4}, Lcom/flixster/android/msk/FriendSelectPage;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v6, 0x1090010

    invoke-virtual {v4, v6, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 227
    :cond_0
    const v4, 0x1020014

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 228
    .local v2, nameView:Landroid/widget/TextView;
    iget-object v4, p0, Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    #getter for: Lcom/flixster/android/msk/FriendSelectPage;->friends:Ljava/util/List;
    invoke-static {v4}, Lcom/flixster/android/msk/FriendSelectPage;->access$4(Lcom/flixster/android/msk/FriendSelectPage;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Contact;

    .line 229
    .local v0, c:Lnet/flixster/android/model/Contact;
    new-instance v1, Landroid/text/SpannableString;

    iget-object v4, v0, Lnet/flixster/android/model/Contact;->name:Ljava/lang/String;

    invoke-direct {v1, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 230
    .local v1, nameText:Landroid/text/SpannableString;
    iget-object v4, v0, Lnet/flixster/android/model/Contact;->givenName:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lnet/flixster/android/model/Contact;->givenName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, v0, Lnet/flixster/android/model/Contact;->givenName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    .line 231
    :goto_0
    iget-object v6, v0, Lnet/flixster/android/model/Contact;->middleName:Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v6, v0, Lnet/flixster/android/model/Contact;->middleName:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, v0, Lnet/flixster/android/model/Contact;->middleName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    .line 230
    :goto_1
    add-int v3, v4, v6

    .line 232
    .local v3, spanPivotPosition:I
    iget-object v4, v0, Lnet/flixster/android/model/Contact;->name:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v3, v4, :cond_4

    .line 233
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    iget-object v6, p0, Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    invoke-direct {v4, v6, v9}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v4, v5, v3, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 235
    iget-object v4, v0, Lnet/flixster/android/model/Contact;->familyName:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lnet/flixster/android/model/Contact;->familyName:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 236
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    iget-object v5, p0, Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    const v6, 0x7f0d0071

    invoke-direct {v4, v5, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 237
    iget-object v5, v0, Lnet/flixster/android/model/Contact;->name:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    .line 236
    invoke-virtual {v1, v4, v3, v5, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 243
    :cond_1
    :goto_2
    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 245
    return-object p2

    .end local v3           #spanPivotPosition:I
    :cond_2
    move v4, v5

    .line 230
    goto :goto_0

    :cond_3
    move v6, v5

    .line 231
    goto :goto_1

    .line 240
    .restart local v3       #spanPivotPosition:I
    :cond_4
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    iget-object v6, p0, Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;->this$0:Lcom/flixster/android/msk/FriendSelectPage;

    invoke-direct {v4, v6, v9}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iget-object v6, v0, Lnet/flixster/android/model/Contact;->name:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v1, v4, v5, v6, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2
.end method
