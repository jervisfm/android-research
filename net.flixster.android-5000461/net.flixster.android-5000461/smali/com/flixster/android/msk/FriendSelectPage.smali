.class public Lcom/flixster/android/msk/FriendSelectPage;
.super Lnet/flixster/android/FlixsterListActivity;
.source "FriendSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;
    }
.end annotation


# static fields
.field private static final CONTACT_PICKER_RESULT:I = 0x3e9

.field public static final KEY_IS_MSK:Ljava/lang/String; = "KEY_IS_MSK"

.field private static final PHONE_MAX_LENGTH:I = 0xb

.field private static final PHONE_MIN_LENGTH:I = 0x7

.field private static final SMS_BODY_DEFAULT:Ljava/lang/String; = "I gave you a full-length movie in Flixster! http://tmto.es/NSsIfY"

.field private static final SMS_COMPOSER_RESULT:I = 0x3ea

.field private static final SMS_REWARDS_DEFAULT:[I


# instance fields
.field private adapter:Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;

.field private final addContactClickListener:Landroid/view/View$OnClickListener;

.field private addContactView:Landroid/widget/ImageView;

.field private body:Ljava/lang/String;

.field private friends:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final inviteClickListener:Landroid/view/View$OnClickListener;

.field private inviteView:Landroid/widget/ImageView;

.field private isMsk:Z

.field private moreView:Landroid/widget/TextView;

.field private final movieEarnedDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private rewardLevels:[I

.field private selectedCount:I

.field private selectedView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x1

    const/16 v2, 0xa

    aput v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0xf

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x19

    aput v2, v0, v1

    sput-object v0, Lcom/flixster/android/msk/FriendSelectPage;->SMS_REWARDS_DEFAULT:[I

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lnet/flixster/android/FlixsterListActivity;-><init>()V

    .line 162
    new-instance v0, Lcom/flixster/android/msk/FriendSelectPage$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/msk/FriendSelectPage$1;-><init>(Lcom/flixster/android/msk/FriendSelectPage;)V

    iput-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->movieEarnedDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 249
    new-instance v0, Lcom/flixster/android/msk/FriendSelectPage$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/msk/FriendSelectPage$2;-><init>(Lcom/flixster/android/msk/FriendSelectPage;)V

    iput-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->addContactClickListener:Landroid/view/View$OnClickListener;

    .line 257
    new-instance v0, Lcom/flixster/android/msk/FriendSelectPage$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/msk/FriendSelectPage$3;-><init>(Lcom/flixster/android/msk/FriendSelectPage;)V

    iput-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->inviteClickListener:Landroid/view/View$OnClickListener;

    .line 38
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/msk/FriendSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    return v0
.end method

.method static synthetic access$1(Lcom/flixster/android/msk/FriendSelectPage;)[I
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/msk/FriendSelectPage;I)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 210
    invoke-direct {p0, p1}, Lcom/flixster/android/msk/FriendSelectPage;->getGiftCount(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/flixster/android/msk/FriendSelectPage;)Z
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->isMsk:Z

    return v0
.end method

.method static synthetic access$4(Lcom/flixster/android/msk/FriendSelectPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->friends:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5(Lcom/flixster/android/msk/FriendSelectPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->body:Ljava/lang/String;

    return-object v0
.end method

.method private getGiftCount(I)I
    .locals 4
    .parameter "friends"

    .prologue
    const/4 v0, 0x3

    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 211
    iget-object v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    aget v3, v3, v0

    if-lt p1, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    aget v0, v0, v1

    if-lt p1, v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    aget v0, v0, v2

    if-lt p1, v0, :cond_2

    move v0, v2

    goto :goto_0

    .line 212
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateFriendCountViews()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 188
    iget v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    invoke-direct {p0, v3}, Lcom/flixster/android/msk/FriendSelectPage;->getGiftCount(I)I

    move-result v0

    .line 189
    .local v0, giftCount:I
    iget-object v4, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 190
    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 191
    iget v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    if-ne v3, v6, :cond_0

    const v3, 0x7f0c00f9

    invoke-virtual {p0, v3}, Lcom/flixster/android/msk/FriendSelectPage;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 193
    const-string v5, " = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 194
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 195
    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 196
    if-ne v0, v6, :cond_1

    const v3, 0x7f0c00fb

    invoke-virtual {p0, v3}, Lcom/flixster/android/msk/FriendSelectPage;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 189
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    iget v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    iget-object v4, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    aget v4, v4, v8

    if-lt v3, v4, :cond_2

    iget-object v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    const/4 v4, 0x3

    aget v2, v3, v4

    .line 201
    .local v2, nextCount:I
    :goto_2
    iget v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    sub-int v1, v2, v3

    .line 202
    .local v1, moreCount:I
    if-lez v1, :cond_4

    .line 203
    iget-object v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->moreView:Landroid/widget/TextView;

    const v4, 0x7f0c00fc

    invoke-virtual {p0, v4}, Lcom/flixster/android/msk/FriendSelectPage;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->moreView:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 208
    :goto_3
    return-void

    .line 192
    .end local v1           #moreCount:I
    .end local v2           #nextCount:I
    :cond_0
    const v3, 0x7f0c00f8

    invoke-virtual {p0, v3}, Lcom/flixster/android/msk/FriendSelectPage;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 197
    :cond_1
    const v3, 0x7f0c00fa

    invoke-virtual {p0, v3}, Lcom/flixster/android/msk/FriendSelectPage;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 200
    :cond_2
    iget v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    iget-object v4, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    aget v4, v4, v6

    if-lt v3, v4, :cond_3

    iget-object v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    aget v2, v3, v8

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    aget v2, v3, v6

    goto :goto_2

    .line 206
    .restart local v1       #moreCount:I
    .restart local v2       #nextCount:I
    :cond_4
    iget-object v3, p0, Lcom/flixster/android/msk/FriendSelectPage;->moreView:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 24
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 87
    packed-switch p1, :pswitch_data_0

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 89
    :pswitch_0
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    .line 90
    if-eqz p3, :cond_0

    .line 91
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 92
    .local v3, result:Landroid/net/Uri;
    if-eqz v3, :cond_0

    .line 93
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "display_name"

    aput-object v7, v4, v2

    const/4 v2, 0x1

    const-string v7, "data1"

    aput-object v7, v4, v2

    .line 94
    .local v4, dataProjection:[Ljava/lang/String;
    const/16 v18, 0x0

    .line 96
    .local v18, dataCursor:Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/msk/FriendSelectPage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v18

    .line 103
    :goto_1
    if-eqz v18, :cond_0

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    const-string v2, "display_name"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 105
    .local v6, name:Ljava/lang/String;
    const-string v2, "data1"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 106
    const-string v7, " "

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "("

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, ")"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "-"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 107
    const-string v7, "+"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 108
    .local v10, number:Ljava/lang/String;
    const/16 v21, 0x0

    .line 109
    .local v21, isDuplicate:Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/msk/FriendSelectPage;->friends:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    .line 115
    :goto_2
    if-nez v21, :cond_5

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v7, 0x7

    if-lt v2, v7, :cond_5

    .line 116
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v7, 0xb

    if-gt v2, v7, :cond_5

    .line 117
    new-instance v5, Lnet/flixster/android/model/Contact;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v5 .. v10}, Lnet/flixster/android/model/Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .local v5, c:Lnet/flixster/android/model/Contact;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/msk/FriendSelectPage;->friends:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/msk/FriendSelectPage;->getListView()Landroid/widget/ListView;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/flixster/android/msk/FriendSelectPage;->friends:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 120
    move-object/from16 v0, p0

    iget v2, v0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    .line 121
    invoke-direct/range {p0 .. p0}, Lcom/flixster/android/msk/FriendSelectPage;->updateFriendCountViews()V

    goto/16 :goto_0

    .line 97
    .end local v5           #c:Lnet/flixster/android/model/Contact;
    .end local v6           #name:Ljava/lang/String;
    .end local v10           #number:Ljava/lang/String;
    .end local v21           #isDuplicate:Z
    :catch_0
    move-exception v19

    .line 98
    .local v19, e:Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/flixster/android/msk/FriendSelectPage;->isMsk:Z

    if-eqz v2, :cond_2

    .line 99
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/msk/MskController;->trackTextRequestExceptionEvent()V

    .line 101
    :cond_2
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 109
    .end local v19           #e:Ljava/lang/Exception;
    .restart local v6       #name:Ljava/lang/String;
    .restart local v10       #number:Ljava/lang/String;
    .restart local v21       #isDuplicate:Z
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Contact;

    .line 110
    .restart local v5       #c:Lnet/flixster/android/model/Contact;
    iget-object v7, v5, Lnet/flixster/android/model/Contact;->name:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, v5, Lnet/flixster/android/model/Contact;->number:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 111
    :cond_4
    const/16 v21, 0x1

    .line 112
    goto :goto_2

    .line 124
    .end local v5           #c:Lnet/flixster/android/model/Contact;
    :cond_5
    const v2, 0x7f0c00fd

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/flixster/android/msk/FriendSelectPage;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 125
    const/4 v7, 0x0

    .line 123
    move-object/from16 v0, p0

    invoke-static {v0, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 125
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 133
    .end local v3           #result:Landroid/net/Uri;
    .end local v4           #dataProjection:[Ljava/lang/String;
    .end local v6           #name:Ljava/lang/String;
    .end local v10           #number:Ljava/lang/String;
    .end local v18           #dataCursor:Landroid/database/Cursor;
    .end local v21           #isDuplicate:Z
    :pswitch_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/flixster/android/msk/FriendSelectPage;->getGiftCount(I)I

    move-result v17

    .line 134
    .local v17, giftCount:I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/flixster/android/msk/FriendSelectPage;->isMsk:Z

    if-eqz v2, :cond_6

    .line 135
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/flixster/android/msk/MskController;->trackTextRequestSuccessEvent(I)V

    .line 136
    new-instance v20, Landroid/content/Intent;

    invoke-direct/range {v20 .. v20}, Landroid/content/Intent;-><init>()V

    .line 137
    .local v20, i:Landroid/content/Intent;
    const-string v2, "moviesEarned"

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 138
    const/4 v2, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Lcom/flixster/android/msk/FriendSelectPage;->setResult(ILandroid/content/Intent;)V

    .line 139
    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/msk/FriendSelectPage;->finish()V

    goto/16 :goto_0

    .line 141
    .end local v20           #i:Landroid/content/Intent;
    :cond_6
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v23

    .line 142
    .local v23, user:Lnet/flixster/android/model/User;
    const/4 v2, 0x0

    move-object/from16 v0, v23

    iput-boolean v2, v0, Lnet/flixster/android/model/User;->isMskSmsEligible:Z

    .line 145
    const/4 v2, 0x3

    move/from16 v0, v17

    if-ne v0, v2, :cond_7

    .line 146
    const-string v22, "MT1,MT2,MT3"

    .line 152
    .local v22, purchaseType:Ljava/lang/String;
    :goto_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v11

    const-string v12, "/rewards"

    const/4 v13, 0x0

    const-string v14, "FlixsterRewards"

    const-string v15, "CompletedReward"

    .line 153
    const-string v16, "SendTextInvites"

    .line 152
    invoke-interface/range {v11 .. v17}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 155
    const/4 v2, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v22

    invoke-static {v2, v7, v0}, Lnet/flixster/android/data/ProfileDao;->insertLockerRight(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    .line 156
    const v2, 0x3b9acb91

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/flixster/android/msk/FriendSelectPage;->movieEarnedDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v7}, Lcom/flixster/android/msk/FriendSelectPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto/16 :goto_0

    .line 147
    .end local v22           #purchaseType:Ljava/lang/String;
    :cond_7
    const/4 v2, 0x2

    move/from16 v0, v17

    if-ne v0, v2, :cond_8

    .line 148
    const-string v22, "MT1,MT2"

    .restart local v22       #purchaseType:Ljava/lang/String;
    goto :goto_3

    .line 150
    .end local v22           #purchaseType:Ljava/lang/String;
    :cond_8
    const-string v22, "MT1"

    .restart local v22       #purchaseType:Ljava/lang/String;
    goto :goto_3

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 57
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v1, 0x7f030030

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FriendSelectPage;->setContentView(I)V

    .line 59
    invoke-virtual {p0}, Lcom/flixster/android/msk/FriendSelectPage;->createActionBar()V

    .line 60
    const v1, 0x7f0c00f6

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FriendSelectPage;->setActionBarTitle(I)V

    .line 62
    invoke-virtual {p0}, Lcom/flixster/android/msk/FriendSelectPage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_IS_MSK"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->isMsk:Z

    .line 63
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 64
    .local v0, p:Lnet/flixster/android/model/Property;
    if-eqz v0, :cond_0

    .line 65
    iget-boolean v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->isMsk:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lnet/flixster/android/model/Property;->mskTextRequestRewardsArray:[I

    :goto_0
    iput-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    if-nez v1, :cond_4

    :cond_0
    sget-object v1, Lcom/flixster/android/msk/FriendSelectPage;->SMS_REWARDS_DEFAULT:[I

    .line 64
    :goto_1
    iput-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    .line 67
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->isMsk:Z

    if-eqz v1, :cond_5

    iget-object v1, v0, Lnet/flixster/android/model/Property;->mskTextRequestBody:Ljava/lang/String;

    :goto_2
    iput-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->body:Ljava/lang/String;

    if-nez v1, :cond_6

    :cond_1
    const-string v1, "I gave you a full-length movie in Flixster! http://tmto.es/NSsIfY"

    :goto_3
    iput-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->body:Ljava/lang/String;

    .line 70
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->friends:Ljava/util/List;

    .line 71
    new-instance v1, Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;

    iget-object v2, p0, Lcom/flixster/android/msk/FriendSelectPage;->friends:Ljava/util/List;

    invoke-direct {v1, p0, p0, v2}, Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;-><init>(Lcom/flixster/android/msk/FriendSelectPage;Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->adapter:Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;

    .line 72
    iget-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->adapter:Lcom/flixster/android/msk/FriendSelectPage$ContactsAdapter;

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FriendSelectPage;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 74
    const v1, 0x7f070099

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FriendSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedView:Landroid/widget/TextView;

    .line 75
    const v1, 0x7f07009a

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FriendSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->moreView:Landroid/widget/TextView;

    .line 76
    const v1, 0x7f070098

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FriendSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->addContactView:Landroid/widget/ImageView;

    .line 77
    iget-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->addContactView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/flixster/android/msk/FriendSelectPage;->addContactClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    const v1, 0x7f07009b

    invoke-virtual {p0, v1}, Lcom/flixster/android/msk/FriendSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->inviteView:Landroid/widget/ImageView;

    .line 79
    iget-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->inviteView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/flixster/android/msk/FriendSelectPage;->inviteClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    invoke-direct {p0}, Lcom/flixster/android/msk/FriendSelectPage;->updateFriendCountViews()V

    .line 81
    iget-boolean v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->isMsk:Z

    if-nez v1, :cond_2

    .line 82
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/rewards/select-contacts"

    const-string v3, "Rewards - SelectContacts"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :cond_2
    return-void

    .line 65
    :cond_3
    iget-object v1, v0, Lnet/flixster/android/model/Property;->rewardsSmsArray:[I

    goto :goto_0

    .line 66
    :cond_4
    iget-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->rewardLevels:[I

    goto :goto_1

    .line 67
    :cond_5
    iget-object v1, v0, Lnet/flixster/android/model/Property;->rewardsSmsBody:Ljava/lang/String;

    goto :goto_2

    .line 68
    :cond_6
    iget-object v1, p0, Lcom/flixster/android/msk/FriendSelectPage;->body:Ljava/lang/String;

    goto :goto_3
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 1
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    .prologue
    .line 179
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    .line 184
    :goto_0
    invoke-direct {p0}, Lcom/flixster/android/msk/FriendSelectPage;->updateFriendCountViews()V

    .line 185
    return-void

    .line 182
    :cond_0
    iget v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/flixster/android/msk/FriendSelectPage;->selectedCount:I

    goto :goto_0
.end method
