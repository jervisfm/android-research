.class Lcom/flixster/android/msk/MskController$FriendsIdsCallBack;
.super Ljava/lang/Object;
.source "MskController.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/msk/MskController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FriendsIdsCallBack"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/msk/MskController;


# direct methods
.method private constructor <init>(Lcom/flixster/android/msk/MskController;)V
    .locals 0
    .parameter

    .prologue
    .line 291
    iput-object p1, p0, Lcom/flixster/android/msk/MskController$FriendsIdsCallBack;->this$0:Lcom/flixster/android/msk/MskController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/msk/MskController;Lcom/flixster/android/msk/MskController$FriendsIdsCallBack;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/flixster/android/msk/MskController$FriendsIdsCallBack;-><init>(Lcom/flixster/android/msk/MskController;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .parameter "msg"

    .prologue
    .line 294
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v2

    sget-object v3, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v3}, Lcom/flixster/android/msk/MskController$Step;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 295
    .local v0, activity:Ljava/lang/Object;
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v1, v2, Lnet/flixster/android/model/User;->friendsIds:[Ljava/lang/Object;

    .line 296
    .local v1, friendsIds:[Ljava/lang/Object;
    iget-object v2, p0, Lcom/flixster/android/msk/MskController$FriendsIdsCallBack;->this$0:Lcom/flixster/android/msk/MskController;

    check-cast v0, Lcom/flixster/android/msk/MskEntryActivity;

    .end local v0           #activity:Ljava/lang/Object;
    #calls: Lcom/flixster/android/msk/MskController;->handleNativeFbRequestStep(Lcom/flixster/android/msk/MskEntryActivity;[Ljava/lang/Object;)V
    invoke-static {v2, v0, v1}, Lcom/flixster/android/msk/MskController;->access$1(Lcom/flixster/android/msk/MskController;Lcom/flixster/android/msk/MskEntryActivity;[Ljava/lang/Object;)V

    .line 297
    const/4 v2, 0x1

    return v2
.end method
