.class public final enum Lcom/flixster/android/msk/MskController$Step;
.super Ljava/lang/Enum;
.source "MskController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/msk/MskController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Step"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flixster/android/msk/MskController$Step;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/flixster/android/msk/MskController$Step;

.field public static final enum MOBILE_WEB:Lcom/flixster/android/msk/MskController$Step;

.field public static final enum NATIVE_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

.field public static final enum NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

.field public static final enum NATIVE_FLX_LOGIN:Lcom/flixster/android/msk/MskController$Step;

.field public static final enum NATIVE_IMPLICIT_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

.field public static final enum NATIVE_IMPLICIT_FLX_LOGIN:Lcom/flixster/android/msk/MskController$Step;

.field public static final enum NATIVE_LASP:Lcom/flixster/android/msk/MskController$Step;

.field public static final enum NATIVE_TEXT_REQUEST:Lcom/flixster/android/msk/MskController$Step;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Lcom/flixster/android/msk/MskController$Step;

    const-string v1, "NATIVE_FB_LOGIN"

    invoke-direct {v0, v1, v3}, Lcom/flixster/android/msk/MskController$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    new-instance v0, Lcom/flixster/android/msk/MskController$Step;

    const-string v1, "NATIVE_FLX_LOGIN"

    invoke-direct {v0, v1, v4}, Lcom/flixster/android/msk/MskController$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FLX_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    new-instance v0, Lcom/flixster/android/msk/MskController$Step;

    const-string v1, "NATIVE_FB_REQUEST"

    invoke-direct {v0, v1, v5}, Lcom/flixster/android/msk/MskController$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    new-instance v0, Lcom/flixster/android/msk/MskController$Step;

    const-string v1, "NATIVE_TEXT_REQUEST"

    invoke-direct {v0, v1, v6}, Lcom/flixster/android/msk/MskController$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/msk/MskController$Step;->NATIVE_TEXT_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    new-instance v0, Lcom/flixster/android/msk/MskController$Step;

    const-string v1, "NATIVE_IMPLICIT_FB_LOGIN"

    invoke-direct {v0, v1, v7}, Lcom/flixster/android/msk/MskController$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    new-instance v0, Lcom/flixster/android/msk/MskController$Step;

    const-string v1, "NATIVE_IMPLICIT_FLX_LOGIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/msk/MskController$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FLX_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    new-instance v0, Lcom/flixster/android/msk/MskController$Step;

    const-string v1, "MOBILE_WEB"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/msk/MskController$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/msk/MskController$Step;->MOBILE_WEB:Lcom/flixster/android/msk/MskController$Step;

    new-instance v0, Lcom/flixster/android/msk/MskController$Step;

    const-string v1, "NATIVE_LASP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/msk/MskController$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/msk/MskController$Step;->NATIVE_LASP:Lcom/flixster/android/msk/MskController$Step;

    .line 41
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/flixster/android/msk/MskController$Step;

    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FLX_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    aput-object v1, v0, v4

    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    aput-object v1, v0, v5

    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_TEXT_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    aput-object v1, v0, v6

    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FLX_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/flixster/android/msk/MskController$Step;->MOBILE_WEB:Lcom/flixster/android/msk/MskController$Step;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/flixster/android/msk/MskController$Step;->NATIVE_LASP:Lcom/flixster/android/msk/MskController$Step;

    aput-object v2, v0, v1

    sput-object v0, Lcom/flixster/android/msk/MskController$Step;->ENUM$VALUES:[Lcom/flixster/android/msk/MskController$Step;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flixster/android/msk/MskController$Step;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/flixster/android/msk/MskController$Step;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/msk/MskController$Step;

    return-object v0
.end method

.method public static values()[Lcom/flixster/android/msk/MskController$Step;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/flixster/android/msk/MskController$Step;->ENUM$VALUES:[Lcom/flixster/android/msk/MskController$Step;

    array-length v1, v0

    new-array v2, v1, [Lcom/flixster/android/msk/MskController$Step;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
