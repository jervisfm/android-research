.class public Lcom/flixster/android/msk/MskController;
.super Ljava/lang/Object;
.source "MskController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/msk/MskController$FriendsIdsCallBack;,
        Lcom/flixster/android/msk/MskController$MskControllerSingletonHolder;,
        Lcom/flixster/android/msk/MskController$Step;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$flixster$android$msk$MskController$Step:[I = null

.field private static final REQUEST_CODE_BASE:I = 0x3b9aca91

.field private static firstLaunch:Z


# instance fields
.field private extraMovies:Ljava/lang/String;

.field private movieId:J

.field private moviesEarned:I

.field private p:Lnet/flixster/android/model/Property;

.field private rewards:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$flixster$android$msk$MskController$Step()[I
    .locals 3

    .prologue
    .line 38
    sget-object v0, Lcom/flixster/android/msk/MskController;->$SWITCH_TABLE$com$flixster$android$msk$MskController$Step:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/flixster/android/msk/MskController$Step;->values()[Lcom/flixster/android/msk/MskController$Step;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->MOBILE_WEB:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_1
    :try_start_1
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_3
    :try_start_3
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FLX_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_4
    :try_start_4
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_5
    :try_start_5
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FLX_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_6
    :try_start_6
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_LASP:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_7
    :try_start_7
    sget-object v1, Lcom/flixster/android/msk/MskController$Step;->NATIVE_TEXT_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_8
    sput-object v0, Lcom/flixster/android/msk/MskController;->$SWITCH_TABLE$com$flixster$android$msk$MskController$Step:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    .line 54
    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/msk/MskController;)V
    .locals 0
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/flixster/android/msk/MskController;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/msk/MskController;Lcom/flixster/android/msk/MskEntryActivity;[Ljava/lang/Object;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 172
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/msk/MskController;->handleNativeFbRequestStep(Lcom/flixster/android/msk/MskEntryActivity;[Ljava/lang/Object;)V

    return-void
.end method

.method private static convertToRequestCode(Lcom/flixster/android/msk/MskController$Step;)I
    .locals 2
    .parameter "step"

    .prologue
    .line 308
    const v0, 0x3b9aca91

    invoke-virtual {p0}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private static convertToStep(I)Lcom/flixster/android/msk/MskController$Step;
    .locals 2
    .parameter "requestCode"

    .prologue
    .line 312
    invoke-static {}, Lcom/flixster/android/msk/MskController$Step;->values()[Lcom/flixster/android/msk/MskController$Step;

    move-result-object v0

    const v1, 0x3b9aca91

    sub-int v1, p0, v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private static getHistoLabel(I)Ljava/lang/String;
    .locals 2
    .parameter "i"

    .prologue
    .line 522
    const-string v0, ""

    .line 523
    .local v0, l:Ljava/lang/String;
    const/4 v1, 0x4

    if-ge p0, v1, :cond_0

    .line 524
    const-string v0, "1-3"

    .line 548
    :goto_0
    return-object v0

    .line 525
    :cond_0
    const/4 v1, 0x7

    if-ge p0, v1, :cond_1

    .line 526
    const-string v0, "4-6"

    goto :goto_0

    .line 527
    :cond_1
    const/16 v1, 0xb

    if-ge p0, v1, :cond_2

    .line 528
    const-string v0, "7-10"

    goto :goto_0

    .line 529
    :cond_2
    const/16 v1, 0x10

    if-ge p0, v1, :cond_3

    .line 530
    const-string v0, "11-15"

    goto :goto_0

    .line 531
    :cond_3
    const/16 v1, 0x15

    if-ge p0, v1, :cond_4

    .line 532
    const-string v0, "16-20"

    goto :goto_0

    .line 533
    :cond_4
    const/16 v1, 0x33

    if-ge p0, v1, :cond_5

    .line 534
    const-string v0, "21-50"

    goto :goto_0

    .line 535
    :cond_5
    const/16 v1, 0x65

    if-ge p0, v1, :cond_6

    .line 536
    const-string v0, "51-100"

    goto :goto_0

    .line 537
    :cond_6
    const/16 v1, 0x97

    if-ge p0, v1, :cond_7

    .line 538
    const-string v0, "101-150"

    goto :goto_0

    .line 539
    :cond_7
    const/16 v1, 0xc9

    if-ge p0, v1, :cond_8

    .line 540
    const-string v0, "151-200"

    goto :goto_0

    .line 541
    :cond_8
    const/16 v1, 0xfb

    if-ge p0, v1, :cond_9

    .line 542
    const-string v0, "201-250"

    goto :goto_0

    .line 543
    :cond_9
    const/16 v1, 0x12c

    if-ge p0, v1, :cond_a

    .line 544
    const-string v0, "251-300"

    goto :goto_0

    .line 546
    :cond_a
    const-string v0, "300+"

    goto :goto_0
.end method

.method private handleLaspStep(Lcom/flixster/android/msk/MskEntryActivity;)V
    .locals 0
    .parameter "activity"

    .prologue
    .line 205
    invoke-virtual {p0, p1}, Lcom/flixster/android/msk/MskController;->finishLasp(Landroid/content/Context;)V

    .line 206
    return-void
.end method

.method private handleMobileWebStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;Ljava/lang/String;)V
    .locals 6
    .parameter "activity"
    .parameter "step"
    .parameter "url"

    .prologue
    const/4 v5, 0x0

    .line 181
    invoke-static {}, Lcom/flixster/android/msk/MskController;->$SWITCH_TABLE$com$flixster$android$msk$MskController$Step()[I

    move-result-object v1

    invoke-virtual {p2}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 202
    :goto_0
    return-void

    .line 183
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 184
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    iget-wide v1, p0, Lcom/flixster/android/msk/MskController;->movieId:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 185
    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_3

    const-string v1, "?"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "movie="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/flixster/android/msk/MskController;->movieId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/msk/MskController;->extraMovies:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 188
    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_4

    const-string v1, "?"

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->extraMovies:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    iput-object v5, p0, Lcom/flixster/android/msk/MskController;->extraMovies:Ljava/lang/String;

    .line 191
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/msk/MskController;->rewards:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 192
    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_5

    const-string v1, "?"

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "rewards="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->rewards:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    iput-object v5, p0, Lcom/flixster/android/msk/MskController;->rewards:Ljava/lang/String;

    .line 196
    :cond_2
    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_6

    const-string v1, "?"

    :goto_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "purchaseType=MAN"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 198
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    .line 199
    sget-object v3, Lcom/flixster/android/msk/MskController$Step;->MOBILE_WEB:Lcom/flixster/android/msk/MskController$Step;

    invoke-static {v3}, Lcom/flixster/android/msk/MskController;->convertToRequestCode(Lcom/flixster/android/msk/MskController$Step;)I

    move-result v3

    .line 198
    invoke-static {v1, v2, p1, v3}, Lnet/flixster/android/Starter;->launchMskHtmlPageForResult(Ljava/lang/String;ZLandroid/app/Activity;I)V

    goto/16 :goto_0

    .line 185
    :cond_3
    const-string v1, "&"

    goto :goto_1

    .line 188
    :cond_4
    const-string v1, "&"

    goto :goto_2

    .line 192
    :cond_5
    const-string v1, "&"

    goto :goto_3

    .line 196
    :cond_6
    const-string v1, "&"

    goto :goto_4

    .line 181
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method private handleNativeFbRequestStep(Lcom/flixster/android/msk/MskEntryActivity;[Ljava/lang/Object;)V
    .locals 1
    .parameter "activity"
    .parameter "friendsIds"

    .prologue
    .line 173
    new-instance v0, Lcom/flixster/android/msk/FacebookRequestController;

    invoke-direct {v0, p1, p2}, Lcom/flixster/android/msk/FacebookRequestController;-><init>(Landroid/app/Activity;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/flixster/android/msk/FacebookRequestController;->start()V

    .line 174
    return-void
.end method

.method private handleNativeStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;)V
    .locals 9
    .parameter "activity"
    .parameter "step"

    .prologue
    const/4 v8, 0x0

    .line 123
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v1

    .line 124
    .local v1, hasAnyUserSession:Z
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/flixster/android/data/AccountManager;->hasFacebookUserSession()Z

    move-result v2

    .line 125
    .local v2, hasFbUserSession:Z
    invoke-static {}, Lcom/flixster/android/msk/MskController;->$SWITCH_TABLE$com$flixster$android$msk$MskController$Step()[I

    move-result-object v6

    invoke-virtual {p2}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 170
    :goto_0
    return-void

    .line 128
    :pswitch_0
    if-eqz v1, :cond_0

    .line 129
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/msk/MskController;->skipStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;)V

    goto :goto_0

    .line 131
    :cond_0
    new-instance v4, Landroid/content/Intent;

    const-class v6, Lnet/flixster/android/FacebookAuth;

    invoke-direct {v4, p1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    .local v4, intent:Landroid/content/Intent;
    const-string v6, "KEY_SHOW_CONNECTED_DIALOG"

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 133
    invoke-static {p2}, Lcom/flixster/android/msk/MskController;->convertToRequestCode(Lcom/flixster/android/msk/MskController$Step;)I

    move-result v6

    invoke-virtual {p1, v4, v6}, Lcom/flixster/android/msk/MskEntryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 137
    .end local v4           #intent:Landroid/content/Intent;
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v6

    sget-object v7, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v7}, Lcom/flixster/android/msk/MskController$Step;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 138
    .local v5, url:Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 139
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v6

    sget-object v7, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v7}, Lcom/flixster/android/msk/MskController$Step;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, p1}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 140
    new-instance v0, Landroid/os/Handler;

    new-instance v6, Lcom/flixster/android/msk/MskController$FriendsIdsCallBack;

    invoke-direct {v6, p0, v8}, Lcom/flixster/android/msk/MskController$FriendsIdsCallBack;-><init>(Lcom/flixster/android/msk/MskController;Lcom/flixster/android/msk/MskController$FriendsIdsCallBack;)V

    invoke-direct {v0, v6}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    .line 141
    .local v0, handler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/flixster/android/activity/DeepLink;->isMskRequestTypePick(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 142
    invoke-direct {p0, p1, v8}, Lcom/flixster/android/msk/MskController;->handleNativeFbRequestStep(Lcom/flixster/android/msk/MskEntryActivity;[Ljava/lang/Object;)V

    goto :goto_0

    .line 144
    :cond_1
    invoke-static {v0, v0}, Lnet/flixster/android/data/ProfileDao;->getFacebookFriends(Landroid/os/Handler;Landroid/os/Handler;)V

    goto :goto_0

    .line 146
    .end local v0           #handler:Landroid/os/Handler;
    :cond_2
    if-eqz v1, :cond_3

    .line 147
    sget-object v6, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    invoke-direct {p0, p1, v6}, Lcom/flixster/android/msk/MskController;->skipStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;)V

    goto :goto_0

    .line 149
    :cond_3
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v6

    sget-object v7, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v7}, Lcom/flixster/android/msk/MskController$Step;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 150
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v6

    sget-object v7, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v7}, Lcom/flixster/android/msk/MskController$Step;->name()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v6, v7, v8}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 151
    sget-object v6, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    invoke-direct {p0, p1, v6}, Lcom/flixster/android/msk/MskController;->handleNativeStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;)V

    goto :goto_0

    .line 155
    .end local v5           #url:Ljava/lang/String;
    :pswitch_2
    new-instance v3, Landroid/content/Intent;

    const-class v6, Lcom/flixster/android/msk/FriendSelectPage;

    invoke-direct {v3, p1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 156
    .local v3, i:Landroid/content/Intent;
    const-string v6, "KEY_IS_MSK"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 157
    invoke-static {p2}, Lcom/flixster/android/msk/MskController;->convertToRequestCode(Lcom/flixster/android/msk/MskController$Step;)I

    move-result v6

    invoke-virtual {p1, v3, v6}, Lcom/flixster/android/msk/MskEntryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 158
    invoke-virtual {p0}, Lcom/flixster/android/msk/MskController;->trackTextRequest()V

    goto/16 :goto_0

    .line 162
    .end local v3           #i:Landroid/content/Intent;
    :pswitch_3
    if-eqz v1, :cond_4

    .line 163
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/msk/MskController;->skipStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;)V

    goto/16 :goto_0

    .line 165
    :cond_4
    new-instance v4, Landroid/content/Intent;

    const-class v6, Lnet/flixster/android/FlixsterLoginPage;

    invoke-direct {v4, p1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 166
    .restart local v4       #intent:Landroid/content/Intent;
    invoke-static {p2}, Lcom/flixster/android/msk/MskController;->convertToRequestCode(Lcom/flixster/android/msk/MskController$Step;)I

    move-result v6

    invoke-virtual {p1, v4, v6}, Lcom/flixster/android/msk/MskEntryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private handleStep(Lcom/flixster/android/msk/MskEntryActivity;Ljava/lang/String;)Z
    .locals 5
    .parameter "activity"
    .parameter "url"

    .prologue
    .line 91
    const/4 v1, 0x0

    .line 92
    .local v1, hasReachedEnd:Z
    if-eqz p2, :cond_0

    .line 93
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v0

    .line 94
    .local v0, hasAnyUserSession:Z
    invoke-static {p2}, Lcom/flixster/android/activity/DeepLink;->isFacebookLoginRequired(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    .line 95
    invoke-static {p2}, Lcom/flixster/android/activity/DeepLink;->removeLoginRequired(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 96
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v3

    sget-object v4, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v4}, Lcom/flixster/android/msk/MskController$Step;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p2}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 97
    sget-object v3, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FB_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    invoke-direct {p0, p1, v3}, Lcom/flixster/android/msk/MskController;->handleNativeStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;)V

    .line 119
    .end local v0           #hasAnyUserSession:Z
    :cond_0
    :goto_0
    return v1

    .line 98
    .restart local v0       #hasAnyUserSession:Z
    :cond_1
    invoke-static {p2}, Lcom/flixster/android/activity/DeepLink;->isFlixsterLoginRequired(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v0, :cond_2

    .line 99
    invoke-static {p2}, Lcom/flixster/android/activity/DeepLink;->removeLoginRequired(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 100
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v3

    sget-object v4, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FLX_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v4}, Lcom/flixster/android/msk/MskController$Step;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p2}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 101
    sget-object v3, Lcom/flixster/android/msk/MskController$Step;->NATIVE_IMPLICIT_FLX_LOGIN:Lcom/flixster/android/msk/MskController$Step;

    invoke-direct {p0, p1, v3}, Lcom/flixster/android/msk/MskController;->handleNativeStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;)V

    goto :goto_0

    .line 103
    :cond_2
    invoke-static {p2}, Lcom/flixster/android/activity/DeepLink;->isValid(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 104
    invoke-static {p2}, Lcom/flixster/android/activity/DeepLink;->convertToMskNativeStep(Ljava/lang/String;)Lcom/flixster/android/msk/MskController$Step;

    move-result-object v2

    .line 105
    .local v2, nativeStep:Lcom/flixster/android/msk/MskController$Step;
    sget-object v3, Lcom/flixster/android/msk/MskController$Step;->NATIVE_LASP:Lcom/flixster/android/msk/MskController$Step;

    if-ne v2, v3, :cond_3

    .line 106
    invoke-direct {p0, p1}, Lcom/flixster/android/msk/MskController;->handleLaspStep(Lcom/flixster/android/msk/MskEntryActivity;)V

    .line 107
    const/4 v1, 0x1

    goto :goto_0

    .line 109
    :cond_3
    sget-object v3, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    if-ne v2, v3, :cond_4

    .line 110
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v3

    sget-object v4, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    invoke-virtual {v4}, Lcom/flixster/android/msk/MskController$Step;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p2}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    :cond_4
    invoke-direct {p0, p1, v2}, Lcom/flixster/android/msk/MskController;->handleNativeStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;)V

    goto :goto_0

    .line 115
    .end local v2           #nativeStep:Lcom/flixster/android/msk/MskController$Step;
    :cond_5
    sget-object v3, Lcom/flixster/android/msk/MskController$Step;->MOBILE_WEB:Lcom/flixster/android/msk/MskController$Step;

    invoke-direct {p0, p1, v3, p2}, Lcom/flixster/android/msk/MskController;->handleMobileWebStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static instance()Lcom/flixster/android/msk/MskController;
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/flixster/android/msk/MskController$MskControllerSingletonHolder;->access$0()Lcom/flixster/android/msk/MskController;

    move-result-object v0

    return-object v0
.end method

.method public static isRequestCodeValid(I)Z
    .locals 2
    .parameter "requestCode"

    .prologue
    const v1, 0x3b9aca91

    .line 304
    if-lt p0, v1, :cond_0

    invoke-static {}, Lcom/flixster/android/msk/MskController$Step;->values()[Lcom/flixster/android/msk/MskController$Step;

    move-result-object v0

    array-length v0, v0

    add-int/2addr v0, v1

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private skipStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;)V
    .locals 3
    .parameter "activity"
    .parameter "step"

    .prologue
    .line 209
    invoke-static {p2}, Lcom/flixster/android/msk/MskController;->convertToRequestCode(Lcom/flixster/android/msk/MskController$Step;)I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/flixster/android/msk/MskController;->onActivityResult(Lcom/flixster/android/msk/MskEntryActivity;IILandroid/content/Intent;)Z

    .line 210
    return-void
.end method

.method private static trackCompletion(Landroid/content/Context;)V
    .locals 8
    .parameter "context"

    .prologue
    .line 512
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v7

    .line 513
    .local v7, p:Lnet/flixster/android/model/Property;
    if-eqz v7, :cond_0

    .line 514
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    .line 515
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "/msk"

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v1, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v1, :cond_1

    const-string v1, "/startup"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/accept-movie"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 516
    sget-boolean v3, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v3, :cond_2

    const-string v3, "StartUpMSK"

    :goto_1
    const-string v4, "SuccessfulRedemption"

    const-string v5, "SuccessfulRedemption"

    const/4 v6, 0x0

    .line 514
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 518
    :cond_0
    const-string v0, ""

    const-wide/16 v1, 0x0

    const-string v3, "USD"

    invoke-static {p0, v0, v1, v2, v3}, Lcom/fiksu/asotracking/FiksuTrackingManager;->uploadPurchaseEvent(Landroid/content/Context;Ljava/lang/String;DLjava/lang/String;)V

    .line 519
    return-void

    .line 515
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "/var"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v7, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 516
    :cond_2
    const-string v3, "MobileMSK"

    goto :goto_1
.end method

.method private trackMobileWebPageExit(Ljava/lang/String;)V
    .locals 1
    .parameter "propertyName"

    .prologue
    .line 343
    const-string v0, "msk.selection."

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    const-string v0, "msk.selection.send.exit"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    invoke-direct {p0}, Lcom/flixster/android/msk/MskController;->trackSelectionPageEventAll()V

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    const-string v0, "msk.selection.pick.exit"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 347
    invoke-direct {p0}, Lcom/flixster/android/msk/MskController;->trackSelectionPageEventPick()V

    goto :goto_0

    .line 348
    :cond_2
    const-string v0, "msk.selection.skip.exit"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    invoke-direct {p0}, Lcom/flixster/android/msk/MskController;->trackSelectionPageEventSkip()V

    goto :goto_0
.end method

.method private trackMskEntry()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 383
    sget-boolean v0, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v0, :cond_1

    .line 384
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/msk/startup/msk-entry"

    const-string v3, "StartUpMSK"

    const-string v4, "Impression"

    const-string v5, "Impression"

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 387
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const-string v1, "lasp"

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v7, 0x1

    .line 388
    .local v7, initiatedFromAd:Z
    :goto_1
    if-eqz v7, :cond_3

    const-string v5, "msk"

    .line 390
    .local v5, entryPage:Ljava/lang/String;
    :goto_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "/msk/var"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v3, v3, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/msk-entry"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MobileMSK"

    .line 391
    const-string v4, "MSKEntry"

    .line 390
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .end local v5           #entryPage:Ljava/lang/String;
    .end local v7           #initiatedFromAd:Z
    :cond_2
    move v7, v6

    .line 387
    goto :goto_1

    .line 388
    .restart local v7       #initiatedFromAd:Z
    :cond_3
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v0, v0, Lnet/flixster/android/model/Property;->mskAnonEntry:Ljava/lang/String;

    .line 389
    iget-object v1, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v1, v1, Lnet/flixster/android/model/Property;->mskAnonEntry:Ljava/lang/String;

    const-string v3, "flixster.com"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const-string v3, "flixster.com"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method private trackSelectionPageEventAll()V
    .locals 7

    .prologue
    .line 408
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 409
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/accept-movie"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "MobileMSK"

    .line 410
    const-string v4, "AcceptMovie"

    const-string v5, "AllFriends"

    const/4 v6, 0x0

    .line 409
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 412
    :cond_0
    return-void
.end method

.method private trackSelectionPageEventPick()V
    .locals 7

    .prologue
    .line 415
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 416
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/accept-movie"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "MobileMSK"

    .line 417
    const-string v4, "AcceptMovie"

    const-string v5, "SelectFriends"

    const/4 v6, 0x0

    .line 416
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 419
    :cond_0
    return-void
.end method

.method private trackSelectionPageEventSkip()V
    .locals 7

    .prologue
    .line 422
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 423
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    .line 424
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "/msk"

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v1, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v1, :cond_1

    const-string v1, "/startup"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/accept-movie"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 425
    sget-boolean v3, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v3, :cond_2

    const-string v3, "StartUpMSK"

    :goto_1
    const-string v4, "AcceptMovie"

    const-string v5, "SkipInvites"

    const/4 v6, 0x0

    .line 423
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 427
    :cond_0
    return-void

    .line 424
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "/var"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v3, v3, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 425
    :cond_2
    const-string v3, "MobileMSK"

    goto :goto_1
.end method


# virtual methods
.method public finishLasp(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    const/4 v2, 0x0

    .line 317
    invoke-static {p1}, Lcom/flixster/android/msk/MskController;->trackCompletion(Landroid/content/Context;)V

    .line 318
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v0

    .line 319
    .local v0, user:Lnet/flixster/android/model/User;
    if-nez v0, :cond_1

    .line 320
    const v1, 0x7f0c00f1

    const/4 v2, 0x1

    invoke-static {p1, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 337
    :cond_0
    :goto_0
    invoke-static {}, Lcom/flixster/android/activity/DeepLink;->clearDeepLinkParams()V

    .line 338
    return-void

    .line 322
    :cond_1
    iput-boolean v2, v0, Lnet/flixster/android/model/User;->isMskEligible:Z

    .line 323
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    .line 324
    iget-object v1, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v1, v1, Lnet/flixster/android/model/Property;->mskTextRequestBody:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/flixster/android/msk/MskController;->moviesEarned:I

    if-lez v1, :cond_2

    .line 325
    iput-boolean v2, v0, Lnet/flixster/android/model/User;->isMskSmsEligible:Z

    .line 327
    :cond_2
    iput-boolean v2, v0, Lnet/flixster/android/model/User;->isMskUvCreateEligible:Z

    .line 328
    iput-boolean v2, v0, Lnet/flixster/android/model/User;->isMskInstallMobileEligible:Z

    .line 329
    iget v1, v0, Lnet/flixster/android/model/User;->collectionsCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lnet/flixster/android/model/User;->collectionsCount:I

    .line 330
    invoke-virtual {v0}, Lnet/flixster/android/model/User;->resetLockerRights()V

    .line 333
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v1

    const-string v2, "lasp"

    invoke-virtual {v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 334
    invoke-static {p1}, Lnet/flixster/android/Starter;->deepLinkMyMoviesTab(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected handleNativeFbRequestStepCallback(Lcom/flixster/android/msk/MskEntryActivity;)V
    .locals 3
    .parameter "activity"

    .prologue
    .line 177
    sget-object v0, Lcom/flixster/android/msk/MskController$Step;->NATIVE_FB_REQUEST:Lcom/flixster/android/msk/MskController$Step;

    invoke-static {v0}, Lcom/flixster/android/msk/MskController;->convertToRequestCode(Lcom/flixster/android/msk/MskController$Step;)I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/flixster/android/msk/MskController;->onActivityResult(Lcom/flixster/android/msk/MskEntryActivity;IILandroid/content/Intent;)Z

    .line 178
    return-void
.end method

.method public isReady()Z
    .locals 3

    .prologue
    .line 70
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 71
    .local v0, temp:Lnet/flixster/android/model/Property;
    if-eqz v0, :cond_2

    sget-boolean v1, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v1, :cond_1

    iget-object v1, v0, Lnet/flixster/android/model/Property;->mskFirstLaunchEntry:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 72
    :cond_0
    iput-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    .line 73
    const/4 v1, 0x1

    .line 76
    :goto_0
    return v1

    .line 71
    :cond_1
    invoke-virtual {v0}, Lnet/flixster/android/model/Property;->getMskEntry()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 75
    :cond_2
    const-string v1, "FlxMain"

    const-string v2, "MskController.isReady properties null"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(Lcom/flixster/android/msk/MskEntryActivity;IILandroid/content/Intent;)Z
    .locals 13
    .parameter "activity"
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 218
    const/4 v3, 0x0

    .line 219
    .local v3, hasReachedEnd:Z
    invoke-static {p2}, Lcom/flixster/android/msk/MskController;->isRequestCodeValid(I)Z

    move-result v9

    if-eqz v9, :cond_9

    const/4 v9, -0x1

    move/from16 v0, p3

    if-ne v0, v9, :cond_9

    .line 220
    invoke-static {p2}, Lcom/flixster/android/msk/MskController;->convertToStep(I)Lcom/flixster/android/msk/MskController$Step;

    move-result-object v4

    .line 221
    .local v4, lastStep:Lcom/flixster/android/msk/MskController$Step;
    const-string v9, "FlxMain"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "MskController.onActivityResult "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-static {}, Lcom/flixster/android/msk/MskController;->$SWITCH_TABLE$com$flixster$android$msk$MskController$Step()[I

    move-result-object v9

    invoke-virtual {v4}, Lcom/flixster/android/msk/MskController$Step;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 282
    .end local v4           #lastStep:Lcom/flixster/android/msk/MskController$Step;
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 283
    const-wide/16 v9, 0x0

    iput-wide v9, p0, Lcom/flixster/android/msk/MskController;->movieId:J

    .line 284
    const/4 v9, 0x0

    iput v9, p0, Lcom/flixster/android/msk/MskController;->moviesEarned:I

    .line 286
    :cond_1
    return v3

    .line 225
    .restart local v4       #lastStep:Lcom/flixster/android/msk/MskController$Step;
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v9

    iget-boolean v9, v9, Lnet/flixster/android/model/User;->isMskEligible:Z

    if-eqz v9, :cond_2

    .line 226
    iget-object v9, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v9, v9, Lnet/flixster/android/model/Property;->mskFbLoginExit:Ljava/lang/String;

    invoke-direct {p0, p1, v9}, Lcom/flixster/android/msk/MskController;->handleStep(Lcom/flixster/android/msk/MskEntryActivity;Ljava/lang/String;)Z

    goto :goto_0

    .line 228
    :cond_2
    invoke-virtual {p1}, Lcom/flixster/android/msk/MskEntryActivity;->showMskIneligibleDialog()V

    goto :goto_0

    .line 232
    :pswitch_1
    iget-object v9, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v9, v9, Lnet/flixster/android/model/Property;->mskFbRequestExit:Ljava/lang/String;

    invoke-direct {p0, p1, v9}, Lcom/flixster/android/msk/MskController;->handleStep(Lcom/flixster/android/msk/MskEntryActivity;Ljava/lang/String;)Z

    goto :goto_0

    .line 235
    :pswitch_2
    const-string v9, "moviesEarned"

    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    iput v9, p0, Lcom/flixster/android/msk/MskController;->moviesEarned:I

    .line 236
    new-instance v8, Ljava/lang/StringBuilder;

    iget-object v9, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v9, v9, Lnet/flixster/android/model/Property;->mskTextRequestExit:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 237
    .local v8, urlBuilder:Ljava/lang/StringBuilder;
    const-string v9, "?"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_3

    const-string v9, "?"

    :goto_1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    const-string v9, "moviesEarned="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/flixster/android/msk/MskController;->moviesEarned:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 239
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, p1, v9}, Lcom/flixster/android/msk/MskController;->handleStep(Lcom/flixster/android/msk/MskEntryActivity;Ljava/lang/String;)Z

    goto :goto_0

    .line 237
    :cond_3
    const-string v9, "&"

    goto :goto_1

    .line 242
    .end local v8           #urlBuilder:Ljava/lang/StringBuilder;
    :pswitch_3
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v9

    iget-boolean v9, v9, Lnet/flixster/android/model/User;->isMskEligible:Z

    if-eqz v9, :cond_4

    .line 243
    iget-object v9, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v9, v9, Lnet/flixster/android/model/Property;->mskFlxLoginExit:Ljava/lang/String;

    invoke-direct {p0, p1, v9}, Lcom/flixster/android/msk/MskController;->handleStep(Lcom/flixster/android/msk/MskEntryActivity;Ljava/lang/String;)Z

    goto :goto_0

    .line 245
    :cond_4
    invoke-virtual {p1}, Lcom/flixster/android/msk/MskEntryActivity;->showMskIneligibleDialog()V

    goto :goto_0

    .line 250
    :pswitch_4
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/flixster/android/msk/MskController$Step;->name()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 251
    .local v1, exit:Ljava/lang/Object;
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v9

    iget-boolean v9, v9, Lnet/flixster/android/model/User;->isMskEligible:Z

    if-eqz v9, :cond_6

    .line 252
    instance-of v9, v1, Lcom/flixster/android/msk/MskController$Step;

    if-eqz v9, :cond_5

    .line 253
    check-cast v1, Lcom/flixster/android/msk/MskController$Step;

    .end local v1           #exit:Ljava/lang/Object;
    invoke-direct {p0, p1, v1}, Lcom/flixster/android/msk/MskController;->handleNativeStep(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskController$Step;)V

    goto/16 :goto_0

    .line 254
    .restart local v1       #exit:Ljava/lang/Object;
    :cond_5
    instance-of v9, v1, Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 255
    check-cast v1, Ljava/lang/String;

    .end local v1           #exit:Ljava/lang/Object;
    invoke-direct {p0, p1, v1}, Lcom/flixster/android/msk/MskController;->handleStep(Lcom/flixster/android/msk/MskEntryActivity;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 258
    .restart local v1       #exit:Ljava/lang/Object;
    :cond_6
    invoke-virtual {p1}, Lcom/flixster/android/msk/MskEntryActivity;->showMskIneligibleDialog()V

    goto/16 :goto_0

    .line 262
    .end local v1           #exit:Ljava/lang/Object;
    :pswitch_5
    invoke-virtual/range {p4 .. p4}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    .line 263
    .local v2, exitUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/flixster/android/activity/DeepLink;->getMovieId(Ljava/lang/String;)J

    move-result-wide v6

    .line 264
    .local v6, tempId:J
    const-wide/16 v9, 0x0

    cmp-long v9, v6, v9

    if-eqz v9, :cond_7

    .line 265
    iput-wide v6, p0, Lcom/flixster/android/msk/MskController;->movieId:J

    .line 267
    :cond_7
    invoke-static {v2}, Lcom/flixster/android/activity/DeepLink;->getExtraMovies(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/flixster/android/msk/MskController;->extraMovies:Ljava/lang/String;

    .line 268
    invoke-static {v2}, Lcom/flixster/android/activity/DeepLink;->convertToPropertyName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 269
    .local v5, propertyName:Ljava/lang/String;
    if-eqz v5, :cond_8

    .line 270
    iget-object v9, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    invoke-virtual {v9, v5}, Lnet/flixster/android/model/Property;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 271
    invoke-direct {p0, v5}, Lcom/flixster/android/msk/MskController;->trackMobileWebPageExit(Ljava/lang/String;)V

    .line 273
    :cond_8
    const-string v9, "FlxMain"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "MskController.onActivityResult "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v11, p0, Lcom/flixster/android/msk/MskController;->movieId:J

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-direct {p0, p1, v2}, Lcom/flixster/android/msk/MskController;->handleStep(Lcom/flixster/android/msk/MskEntryActivity;Ljava/lang/String;)Z

    move-result v3

    goto/16 :goto_0

    .line 278
    .end local v2           #exitUrl:Ljava/lang/String;
    .end local v4           #lastStep:Lcom/flixster/android/msk/MskController$Step;
    .end local v5           #propertyName:Ljava/lang/String;
    .end local v6           #tempId:J
    :cond_9
    const-string v9, "FlxMain"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "MskController.onActivityResult not handled "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 223
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public setFirstLaunchMode(Z)V
    .locals 0
    .parameter "isFirstLaunch"

    .prologue
    .line 65
    sput-boolean p1, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    .line 66
    return-void
.end method

.method protected startFlow(Lcom/flixster/android/msk/MskEntryActivity;)V
    .locals 1
    .parameter "activity"

    .prologue
    .line 82
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/flixster/android/msk/MskController;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v0, v0, Lnet/flixster/android/model/Property;->mskTextRequestRewards:Ljava/lang/String;

    iput-object v0, p0, Lcom/flixster/android/msk/MskController;->rewards:Ljava/lang/String;

    .line 84
    sget-boolean v0, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v0, v0, Lnet/flixster/android/model/Property;->mskFirstLaunchEntry:Ljava/lang/String;

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/flixster/android/msk/MskController;->handleStep(Lcom/flixster/android/msk/MskEntryActivity;Ljava/lang/String;)Z

    .line 85
    invoke-direct {p0}, Lcom/flixster/android/msk/MskController;->trackMskEntry()V

    .line 87
    :cond_1
    return-void

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    invoke-virtual {v0}, Lnet/flixster/android/model/Property;->getMskEntry()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public trackFbLoginAttempt()V
    .locals 4

    .prologue
    .line 355
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 356
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    .line 357
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "/msk"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v0, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v0, :cond_1

    const-string v0, "/startup"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/facebook/login/attempt"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    .line 356
    invoke-interface {v1, v0, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :cond_0
    return-void

    .line 357
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "/var"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v3, v3, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public trackFbLoginSuccess()V
    .locals 4

    .prologue
    .line 362
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 363
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    .line 364
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "/msk"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v0, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v0, :cond_1

    const-string v0, "/startup"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/facebook/login/success"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    .line 363
    invoke-interface {v1, v0, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    :cond_0
    return-void

    .line 364
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "/var"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v3, v3, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected trackFbRequestAll()V
    .locals 3

    .prologue
    .line 430
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 431
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/invite-all"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_0
    return-void
.end method

.method protected trackFbRequestAllEventSent(I)V
    .locals 7
    .parameter "numOfRecipients"

    .prologue
    .line 442
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 443
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/invite-all"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "MobileMSK"

    .line 444
    const-string v4, "AppRequest"

    const-string v5, "All"

    move v6, p1

    .line 443
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 446
    :cond_0
    return-void
.end method

.method protected trackFbRequestAllEventSentHisto(I)V
    .locals 7
    .parameter "numOfRecipients"

    .prologue
    .line 456
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 457
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/invite-all"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "MobileMSK"

    .line 458
    const-string v4, "AppRequestAllHisto"

    invoke-static {p1}, Lcom/flixster/android/msk/MskController;->getHistoLabel(I)Ljava/lang/String;

    move-result-object v5

    move v6, p1

    .line 457
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 460
    :cond_0
    return-void
.end method

.method protected trackFbRequestInvitesRewardEvent(I)V
    .locals 7
    .parameter "numOfRecipients"

    .prologue
    .line 470
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/rewards"

    const/4 v2, 0x0

    const-string v3, "FlixsterRewards"

    const-string v4, "FBInvites"

    const-string v5, "All"

    move v6, p1

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 471
    return-void
.end method

.method protected trackFbRequestPick()V
    .locals 3

    .prologue
    .line 436
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 437
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/invite-select"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    :cond_0
    return-void
.end method

.method protected trackFbRequestPickEventSent(I)V
    .locals 7
    .parameter "numOfRecipients"

    .prologue
    .line 449
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 450
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/invite-select"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "MobileMSK"

    .line 451
    const-string v4, "AppRequest"

    const-string v5, "Select"

    move v6, p1

    .line 450
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 453
    :cond_0
    return-void
.end method

.method protected trackFbRequestPickEventSentHisto(I)V
    .locals 7
    .parameter "numOfRecipients"

    .prologue
    .line 463
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 464
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/invite-select"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "MobileMSK"

    .line 465
    const-string v4, "AppRequestSelectHisto"

    invoke-static {p1}, Lcom/flixster/android/msk/MskController;->getHistoLabel(I)Ljava/lang/String;

    move-result-object v5

    move v6, p1

    .line 464
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 467
    :cond_0
    return-void
.end method

.method public trackFlxLoginAttempt()V
    .locals 4

    .prologue
    .line 369
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 370
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    .line 371
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "/msk"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v0, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v0, :cond_1

    const-string v0, "/startup"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/flixster/login/dialog"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    .line 370
    invoke-interface {v1, v0, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    :cond_0
    return-void

    .line 371
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "/var"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v3, v3, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public trackFlxLoginSuccess()V
    .locals 4

    .prologue
    .line 376
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 377
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    .line 378
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "/msk"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v0, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v0, :cond_1

    const-string v0, "/startup"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/flixster/login/success"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    .line 377
    invoke-interface {v1, v0, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    :cond_0
    return-void

    .line 378
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "/var"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v3, v3, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public trackRewardConfirmationPage()V
    .locals 3

    .prologue
    .line 506
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 507
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/reward-confirmation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    :cond_0
    return-void
.end method

.method public trackSelectionPage()V
    .locals 4

    .prologue
    .line 401
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 402
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "/msk"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v0, Lcom/flixster/android/msk/MskController;->firstLaunch:Z

    if-eqz v0, :cond_1

    const-string v0, "/startup"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/accept-movie"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 403
    const/4 v2, 0x0

    .line 402
    invoke-interface {v1, v0, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_0
    return-void

    .line 402
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "/var"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v3, v3, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public trackSkipFirstLaunchMsk()V
    .locals 7

    .prologue
    .line 397
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/msk/startup/skip"

    const/4 v2, 0x0

    const-string v3, "StartUpMSK"

    const-string v4, "Skip"

    const-string v5, "Skip"

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 398
    return-void
.end method

.method protected trackTextRequest()V
    .locals 3

    .prologue
    .line 474
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 475
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/select-contacts"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    :cond_0
    return-void
.end method

.method protected trackTextRequestAttemptEvent(II)V
    .locals 7
    .parameter "moviesEarned"
    .parameter "numOfRecipients"

    .prologue
    .line 492
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 493
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/text-request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "MobileMSK"

    .line 494
    const-string v4, "SendTextInvites"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    move v6, p2

    .line 493
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 496
    :cond_0
    return-void
.end method

.method protected trackTextRequestExceptionEvent()V
    .locals 8

    .prologue
    .line 480
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 482
    :try_start_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/text-request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "MobileMSK"

    .line 483
    const-string v4, "PermissionException"

    .line 484
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v6, Lcom/flixster/android/utils/F;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/flixster/android/utils/F;->API_LEVEL:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-static {v5, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 482
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 489
    :cond_0
    :goto_0
    return-void

    .line 485
    :catch_0
    move-exception v7

    .line 486
    .local v7, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v7}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method protected trackTextRequestSuccessEvent(I)V
    .locals 7
    .parameter "moviesEarned"

    .prologue
    .line 499
    iget-object v0, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    if-eqz v0, :cond_0

    .line 500
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/msk/var"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/flixster/android/msk/MskController;->p:Lnet/flixster/android/model/Property;

    iget-object v2, v2, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/text-request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "MobileMSK"

    .line 501
    const-string v4, "AdditionalMovies"

    const-string v5, "AdditionalMovies"

    move v6, p1

    .line 500
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 503
    :cond_0
    return-void
.end method
