.class Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;
.super Ljava/lang/Object;
.source "MskEntryActivity.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/msk/MskEntryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MskIneligibleDialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/msk/MskEntryActivity;


# direct methods
.method private constructor <init>(Lcom/flixster/android/msk/MskEntryActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;->this$0:Lcom/flixster/android/msk/MskEntryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;-><init>(Lcom/flixster/android/msk/MskEntryActivity;)V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 1
    .parameter "which"

    .prologue
    .line 64
    iget-object v0, p0, Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;->this$0:Lcom/flixster/android/msk/MskEntryActivity;

    invoke-virtual {v0}, Lcom/flixster/android/msk/MskEntryActivity;->finish()V

    .line 65
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 1
    .parameter "which"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;->this$0:Lcom/flixster/android/msk/MskEntryActivity;

    invoke-virtual {v0}, Lcom/flixster/android/msk/MskEntryActivity;->finish()V

    .line 60
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 1
    .parameter "which"

    .prologue
    .line 54
    iget-object v0, p0, Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;->this$0:Lcom/flixster/android/msk/MskEntryActivity;

    invoke-virtual {v0}, Lcom/flixster/android/msk/MskEntryActivity;->finish()V

    .line 55
    return-void
.end method
