.class public Lcom/flixster/android/msk/MskEntryActivity;
.super Lcom/flixster/android/activity/common/DecoratedActivity;
.source "MskEntryActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;
    }
.end annotation


# static fields
.field public static final REQUEST_CODE:Ljava/lang/String; = "MskEntryActivity.REQUEST_CODE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;-><init>()V

    return-void
.end method

.method private startMsk()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/flixster/android/msk/MskController;->startFlow(Lcom/flixster/android/msk/MskEntryActivity;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/flixster/android/activity/common/DecoratedActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 40
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/flixster/android/msk/MskController;->onActivityResult(Lcom/flixster/android/msk/MskEntryActivity;IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/flixster/android/msk/MskEntryActivity;->finish()V

    .line 43
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    const/4 v0, 0x7

    :goto_0
    invoke-virtual {p0, v0}, Lcom/flixster/android/msk/MskEntryActivity;->setRequestedOrientation(I)V

    .line 21
    const v0, 0x7f03001c

    invoke-virtual {p0, v0}, Lcom/flixster/android/msk/MskEntryActivity;->setContentView(I)V

    .line 22
    invoke-direct {p0}, Lcom/flixster/android/msk/MskEntryActivity;->startMsk()V

    .line 23
    return-void

    .line 20
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected showMskIneligibleDialog()V
    .locals 3

    .prologue
    .line 46
    const v0, 0x3b9acb90

    new-instance v1, Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;-><init>(Lcom/flixster/android/msk/MskEntryActivity;Lcom/flixster/android/msk/MskEntryActivity$MskIneligibleDialogListener;)V

    invoke-virtual {p0, v0, v1}, Lcom/flixster/android/msk/MskEntryActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 47
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .parameter "intent"
    .parameter "requestCode"

    .prologue
    .line 33
    const-string v0, "MskEntryActivity.REQUEST_CODE"

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 34
    invoke-super {p0, p1, p2}, Lcom/flixster/android/activity/common/DecoratedActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 35
    return-void
.end method
