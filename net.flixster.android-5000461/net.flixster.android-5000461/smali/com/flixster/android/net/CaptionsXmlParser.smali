.class public Lcom/flixster/android/net/CaptionsXmlParser;
.super Ljava/lang/Object;
.source "CaptionsXmlParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readTtml(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/List;
    .locals 20
    .parameter "parser"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/net/TimedTextElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    const/16 v16, 0x0

    .line 30
    .local v16, ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    const/4 v8, 0x0

    .local v8, dropMode:Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, frameRate:Ljava/lang/String;
    const/4 v10, 0x0

    .line 31
    .local v10, frameRateMultiplier:Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, begin:Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, end:Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, origin:Ljava/lang/String;
    const/4 v7, 0x0

    .line 32
    .local v7, text:Ljava/lang/String;
    const/4 v5, 0x0

    .line 33
    .local v5, region:I
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v12

    .local v12, eventType:I
    move-object/from16 v17, v16

    .line 34
    .end local v16           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .local v17, ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :goto_0
    const/4 v2, 0x1

    if-ne v12, v2, :cond_0

    move-object/from16 v16, v17

    .line 86
    .end local v12           #eventType:I
    .end local v17           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v16       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :goto_1
    return-object v16

    .line 35
    .end local v16           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v12       #eventType:I
    .restart local v17       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :cond_0
    packed-switch v12, :pswitch_data_0

    :cond_1
    :goto_2
    :pswitch_0
    move-object/from16 v16, v17

    .line 79
    .end local v17           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v16       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :goto_3
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v12

    move-object/from16 v17, v16

    .end local v16           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v17       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    goto :goto_0

    .line 37
    :pswitch_1
    :try_start_1
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 38
    .end local v17           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v16       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    goto :goto_3

    .line 40
    .end local v16           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v17       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :pswitch_2
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    .line 41
    .local v15, tagName:Ljava/lang/String;
    const-string v2, "tt"

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 42
    const/4 v2, 0x0

    const-string v18, "ttp:dropMode"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 43
    const/4 v2, 0x0

    const-string v18, "ttp:frameRate"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 44
    const/4 v2, 0x0

    .line 45
    const-string v18, "ttp:frameRateMultiplier"

    .line 44
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 46
    const-string v2, "FlxDrm"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "CaptionsXmlParser.readTtml: dropMode "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", frameRate "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 47
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", frameRateMultiplier "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 46
    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v16, v17

    .end local v17           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v16       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    goto :goto_3

    .line 48
    .end local v16           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v17       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :cond_2
    const-string v2, "p"

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 49
    const/4 v2, 0x0

    const-string v18, "begin"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 50
    .local v13, newBegin:Ljava/lang/String;
    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 51
    add-int/lit8 v5, v5, 0x1

    .line 55
    :goto_4
    move-object v3, v13

    .line 56
    const/4 v2, 0x0

    const-string v18, "end"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 57
    const/4 v2, 0x0

    const-string v18, "tts:origin"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 58
    const/4 v7, 0x0

    move-object/from16 v16, v17

    .line 60
    .end local v17           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v16       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    goto/16 :goto_3

    .line 53
    .end local v16           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v17       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :cond_3
    const/4 v5, 0x0

    goto :goto_4

    .line 62
    .end local v13           #newBegin:Ljava/lang/String;
    .end local v15           #tagName:Ljava/lang/String;
    :pswitch_3
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->isWhitespace()Z

    move-result v2

    if-nez v2, :cond_1

    .line 63
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v14

    .line 64
    .local v14, nextText:Ljava/lang/String;
    if-nez v7, :cond_4

    .line 65
    move-object v7, v14

    move-object/from16 v16, v17

    .end local v17           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v16       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    goto/16 :goto_3

    .line 67
    .end local v16           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v17       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v16, v17

    .line 70
    .end local v17           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v16       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    goto/16 :goto_3

    .line 72
    .end local v14           #nextText:Ljava/lang/String;
    .end local v16           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v17       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :pswitch_4
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    .line 73
    .restart local v15       #tagName:Ljava/lang/String;
    const-string v2, "p"

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 74
    new-instance v2, Lcom/flixster/android/net/TimedTextElement;

    .line 75
    invoke-direct/range {v2 .. v10}, Lcom/flixster/android/net/TimedTextElement;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_2

    .line 81
    .end local v15           #tagName:Ljava/lang/String;
    :catch_0
    move-exception v11

    move-object/from16 v16, v17

    .line 82
    .end local v12           #eventType:I
    .end local v17           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .local v11, e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v16       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :goto_5
    const-string v2, "FlxDrm"

    const-string v18, "CaptionsXmlParser.readTtml"

    move-object/from16 v0, v18

    invoke-static {v2, v0, v11}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 83
    .end local v11           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v11

    .line 84
    .local v11, e:Ljava/io/IOException;
    :goto_6
    const-string v2, "FlxDrm"

    const-string v18, "CaptionsXmlParser.readTtml"

    move-object/from16 v0, v18

    invoke-static {v2, v0, v11}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 83
    .end local v11           #e:Ljava/io/IOException;
    .end local v16           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v12       #eventType:I
    .restart local v17       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :catch_2
    move-exception v11

    move-object/from16 v16, v17

    .end local v17           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    .restart local v16       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    goto :goto_6

    .line 81
    .end local v12           #eventType:I
    :catch_3
    move-exception v11

    goto :goto_5

    .line 35
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public parse(Ljava/io/InputStream;)Ljava/util/List;
    .locals 5
    .parameter "is"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/net/TimedTextElement;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 18
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v3

    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 19
    .local v1, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v3, 0x0

    invoke-interface {v1, p1, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, v1}, Lcom/flixster/android/net/CaptionsXmlParser;->readTtml(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/List;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 23
    .end local v1           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :goto_0
    return-object v2

    .line 21
    :catch_0
    move-exception v0

    .line 22
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v3, "FlxDrm"

    const-string v4, "CaptionsXmlParser.parse"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
