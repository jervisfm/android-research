.class public Lcom/flixster/android/net/DownloadBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DownloadBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static extractRightId(Ljava/lang/String;)J
    .locals 9
    .parameter "localUri"

    .prologue
    const-wide/16 v5, 0x0

    const/4 v8, -0x1

    .line 62
    const-string v7, "/"

    invoke-virtual {p0, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 63
    .local v2, lastSlashIndex:I
    const-string v7, "."

    invoke-virtual {p0, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 64
    .local v1, lastDotIndex:I
    if-le v2, v8, :cond_0

    if-le v1, v8, :cond_0

    .line 65
    add-int/lit8 v7, v2, 0x1

    invoke-virtual {p0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, filename:Ljava/lang/String;
    const-string v7, "_"

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v4, v7, v8

    .line 68
    .local v4, right:Ljava/lang/String;
    :try_start_0
    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    .line 73
    .end local v0           #filename:Ljava/lang/String;
    .end local v4           #right:Ljava/lang/String;
    :cond_0
    :goto_0
    return-wide v5

    .line 69
    .restart local v0       #filename:Ljava/lang/String;
    .restart local v4       #right:Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 70
    .local v3, nfe:Ljava/lang/NumberFormatException;
    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 15
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 28
    const-string v12, "extra_download_id"

    const-wide/16 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    .line 29
    .local v7, downloadId:J
    const-string v12, "FlxMain"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "DownloadBroadcastReceiver.onReceive id "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    const-string v12, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 31
    invoke-static {v7, v8}, Lcom/flixster/android/net/DownloadHelper;->queryDownloadManagerForFile(J)Ljava/lang/String;

    move-result-object v9

    .line 32
    .local v9, localUri:Ljava/lang/String;
    if-eqz v9, :cond_0

    const-string v12, "net.flixster.android"

    invoke-virtual {v9, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 33
    const-string v12, ".ttml"

    invoke-virtual {v9, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 34
    invoke-static {v9}, Lcom/flixster/android/net/DownloadBroadcastReceiver;->extractRightId(Ljava/lang/String;)J

    move-result-wide v1

    .line 35
    .local v1, rightId:J
    new-instance v10, Lcom/flixster/android/drm/DownloadLock;

    invoke-direct {v10, v1, v2}, Lcom/flixster/android/drm/DownloadLock;-><init>(J)V

    .line 36
    .local v10, lock:Lcom/flixster/android/drm/DownloadLock;
    invoke-virtual {v10}, Lcom/flixster/android/drm/DownloadLock;->exists()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 38
    invoke-virtual {v10}, Lcom/flixster/android/drm/DownloadLock;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 39
    .local v3, title:Ljava/lang/String;
    invoke-virtual {v10}, Lcom/flixster/android/drm/DownloadLock;->getRightType()Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, rightType:Ljava/lang/String;
    invoke-virtual {v10}, Lcom/flixster/android/drm/DownloadLock;->getAssetId()J

    move-result-wide v5

    .line 43
    .local v5, assetId:J
    const v12, 0x7f0c0191

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v3, v13, v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    .line 44
    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 47
    invoke-virtual {v10}, Lcom/flixster/android/drm/DownloadLock;->delete()Z

    .line 50
    const-string v12, "FlxMain"

    const-string v13, "DownloadBroadcastReceiver.onReceive registering"

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-static/range {v1 .. v6}, Lnet/flixster/android/model/LockerRight;->getMockInstance(JLjava/lang/String;Ljava/lang/String;J)Lnet/flixster/android/model/LockerRight;

    move-result-object v11

    .line 52
    .local v11, mockRight:Lnet/flixster/android/model/LockerRight;
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v12

    invoke-interface {v12, v11}, Lcom/flixster/android/drm/PlaybackManager;->registerAssetOnDownloadComplete(Lnet/flixster/android/model/LockerRight;)V

    .line 58
    .end local v1           #rightId:J
    .end local v3           #title:Ljava/lang/String;
    .end local v4           #rightType:Ljava/lang/String;
    .end local v5           #assetId:J
    .end local v9           #localUri:Ljava/lang/String;
    .end local v10           #lock:Lcom/flixster/android/drm/DownloadLock;
    .end local v11           #mockRight:Lnet/flixster/android/model/LockerRight;
    :cond_0
    :goto_0
    return-void

    .line 54
    .restart local v1       #rightId:J
    .restart local v9       #localUri:Ljava/lang/String;
    .restart local v10       #lock:Lcom/flixster/android/drm/DownloadLock;
    :cond_1
    const-string v12, "FlxMain"

    const-string v13, "DownloadBroadcastReceiver.onReceive missing download lock"

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
