.class public Lcom/flixster/android/net/DownloadHelper;
.super Ljava/lang/Object;
.source "DownloadHelper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation


# static fields
.field public static final FILE_PROTOCOL:Ljava/lang/String; = "file://"

.field public static final WV_DIR:Ljava/lang/String; = "wv/"

.field private static final units:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 401
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "B"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "KB"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MB"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "GB"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "TB"

    aput-object v2, v0, v1

    sput-object v0, Lcom/flixster/android/net/DownloadHelper;->units:[Ljava/lang/String;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static cancelDownload(J)V
    .locals 4
    .parameter "downloadId"

    .prologue
    .line 302
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 303
    .local v0, context:Landroid/content/Context;
    const-string v2, "download"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/DownloadManager;

    .line 304
    .local v1, dm:Landroid/app/DownloadManager;
    const/4 v2, 0x1

    new-array v2, v2, [J

    const/4 v3, 0x0

    aput-wide p0, v2, v3

    invoke-virtual {v1, v2}, Landroid/app/DownloadManager;->remove([J)I

    .line 305
    return-void
.end method

.method public static declared-synchronized cancelMovieDownload(J)V
    .locals 6
    .parameter "rightId"

    .prologue
    .line 154
    const-class v3, Lcom/flixster/android/net/DownloadHelper;

    monitor-enter v3

    :try_start_0
    invoke-static {p0, p1}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 155
    new-instance v1, Lcom/flixster/android/drm/DownloadLock;

    invoke-direct {v1, p0, p1}, Lcom/flixster/android/drm/DownloadLock;-><init>(J)V

    .line 156
    .local v1, lock:Lcom/flixster/android/drm/DownloadLock;
    invoke-virtual {v1}, Lcom/flixster/android/drm/DownloadLock;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 157
    invoke-virtual {v1}, Lcom/flixster/android/drm/DownloadLock;->getDownloadId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/flixster/android/net/DownloadHelper;->cancelDownload(J)V

    .line 158
    invoke-virtual {v1}, Lcom/flixster/android/drm/DownloadLock;->delete()Z

    .line 160
    :cond_0
    invoke-static {p0, p1}, Lcom/flixster/android/net/DownloadHelper;->findDownloadCaptionsFile(J)Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, localUri:Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 162
    invoke-static {v0}, Lcom/flixster/android/storage/ExternalStorage;->deleteFile(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    .end local v0           #localUri:Ljava/lang/String;
    .end local v1           #lock:Lcom/flixster/android/drm/DownloadLock;
    :cond_1
    monitor-exit v3

    return-void

    .line 154
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static createAssetDownloadLock(Lnet/flixster/android/model/LockerRight;J)V
    .locals 3
    .parameter "right"
    .parameter "downloadId"

    .prologue
    .line 92
    new-instance v0, Lcom/flixster/android/drm/DownloadLock;

    iget-wide v1, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/drm/DownloadLock;-><init>(J)V

    .line 93
    .local v0, lock:Lcom/flixster/android/drm/DownloadLock;
    invoke-virtual {v0, p1, p2}, Lcom/flixster/android/drm/DownloadLock;->setDownloadId(J)V

    .line 94
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/drm/DownloadLock;->setTitle(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getRightType()Lnet/flixster/android/model/LockerRight$RightType;

    move-result-object v1

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$RightType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/drm/DownloadLock;->setRightType(Ljava/lang/String;)V

    .line 96
    iget-wide v1, p0, Lnet/flixster/android/model/LockerRight;->assetId:J

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/drm/DownloadLock;->setAssetId(J)V

    .line 97
    invoke-virtual {v0}, Lcom/flixster/android/drm/DownloadLock;->save()V

    .line 98
    return-void
.end method

.method private static createAssetFileName(JLjava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "rightId"
    .parameter "uri"

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/flixster/android/net/DownloadHelper;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createCaptionsFileName(J)Ljava/lang/String;
    .locals 2
    .parameter "rightId"

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".ttml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createSubDir(Ljava/lang/String;)V
    .locals 4
    .parameter "subDir"

    .prologue
    .line 334
    invoke-static {p0}, Lcom/flixster/android/storage/ExternalStorage;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 335
    .local v0, extSubDir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 336
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DownloadHelper.createSubDir created "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :cond_0
    return-void
.end method

.method public static deleteDownloadedMovie(Lnet/flixster/android/model/LockerRight;)Z
    .locals 4
    .parameter "right"

    .prologue
    const/4 v1, 0x0

    .line 271
    iget-wide v2, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v2, v3}, Lcom/flixster/android/net/DownloadHelper;->findDownloadCaptionsFile(J)Ljava/lang/String;

    move-result-object v0

    .line 272
    .local v0, localUri:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 273
    invoke-static {v0}, Lcom/flixster/android/storage/ExternalStorage;->deleteFile(Ljava/lang/String;)Z

    .line 276
    :cond_0
    iget-wide v2, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v2, v3, v1}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedAsset(JZ)Ljava/lang/String;

    move-result-object v0

    .line 277
    if-nez v0, :cond_1

    .line 281
    :goto_0
    return v1

    .line 280
    :cond_1
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/flixster/android/drm/PlaybackManager;->unregisterLocalAsset(Lnet/flixster/android/model/LockerRight;)V

    .line 281
    invoke-static {v0}, Lcom/flixster/android/storage/ExternalStorage;->deleteFile(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method protected static download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 8
    .parameter "uri"
    .parameter "subDir"
    .parameter "fileName"

    .prologue
    .line 287
    invoke-static {}, Lcom/flixster/android/storage/ExternalStorage;->isWriteable()Z

    move-result v5

    if-nez v5, :cond_0

    .line 288
    const-wide/16 v2, -0x1

    .line 297
    :goto_0
    return-wide v2

    .line 290
    :cond_0
    invoke-static {p1}, Lcom/flixster/android/net/DownloadHelper;->createSubDir(Ljava/lang/String;)V

    .line 291
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 292
    .local v0, context:Landroid/content/Context;
    const-string v5, "download"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/DownloadManager;

    .line 293
    .local v1, dm:Landroid/app/DownloadManager;
    new-instance v4, Landroid/app/DownloadManager$Request;

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 294
    .local v4, request:Landroid/app/DownloadManager$Request;
    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v0, v5, v6}, Landroid/app/DownloadManager$Request;->setDestinationInExternalFilesDir(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 295
    invoke-virtual {v1, v4}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v2

    .line 296
    .local v2, downloadId:J
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "DownloadHelper.download "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static declared-synchronized downloadMovie(Lnet/flixster/android/model/LockerRight;)V
    .locals 8
    .parameter "right"

    .prologue
    .line 48
    const-class v5, Lcom/flixster/android/net/DownloadHelper;

    monitor-enter v5

    :try_start_0
    iget-wide v6, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v6, v7}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 49
    const-string v4, "FlxMain"

    const-string v6, "DownloadHelper.downloadMovie aborted due to download in progress"

    invoke-static {v4, v6}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    :cond_0
    :goto_0
    monitor-exit v5

    return-void

    .line 52
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getDownloadUri()Ljava/lang/String;

    move-result-object v3

    .line 53
    .local v3, uri:Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 54
    iget-wide v6, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v6, v7, v3}, Lcom/flixster/android/net/DownloadHelper;->createAssetFileName(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, filename:Ljava/lang/String;
    const-string v4, "wv/"

    invoke-static {v3, v4, v2}, Lcom/flixster/android/net/DownloadHelper;->download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 56
    .local v0, downloadId:J
    const-wide/16 v6, -0x1

    cmp-long v4, v0, v6

    if-eqz v4, :cond_0

    .line 57
    invoke-static {p0, v0, v1}, Lcom/flixster/android/net/DownloadHelper;->createAssetDownloadLock(Lnet/flixster/android/model/LockerRight;J)V

    .line 58
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getDownloadCaptionsUri()Ljava/lang/String;

    move-result-object v3

    .line 59
    if-eqz v3, :cond_0

    const-string v4, ""

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 60
    iget-wide v6, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v6, v7}, Lcom/flixster/android/net/DownloadHelper;->createCaptionsFileName(J)Ljava/lang/String;

    move-result-object v2

    .line 61
    const-string v4, "wv/"

    invoke-static {v3, v4, v2}, Lcom/flixster/android/net/DownloadHelper;->download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 48
    .end local v0           #downloadId:J
    .end local v2           #filename:Ljava/lang/String;
    .end local v3           #uri:Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4
.end method

.method public static findDownloadCaptionsFile(J)Ljava/lang/String;
    .locals 7
    .parameter "rightId"

    .prologue
    .line 250
    const-string v4, "wv/"

    invoke-static {v4}, Lcom/flixster/android/storage/ExternalStorage;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 251
    .local v0, extSubDir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 252
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 253
    .local v2, files:[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 254
    const/4 v3, 0x0

    .line 255
    .local v3, localUri:Ljava/lang/String;
    array-length v5, v2

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_1

    .line 264
    .end local v2           #files:[Ljava/lang/String;
    .end local v3           #localUri:Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    :goto_1
    return-object v3

    .line 255
    .restart local v2       #files:[Ljava/lang/String;
    .restart local v3       #localUri:Ljava/lang/String;
    :cond_1
    aget-object v1, v2, v4

    .line 256
    .local v1, file:Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/flixster/android/net/DownloadHelper;->createCaptionsFileName(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 257
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file://"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 258
    const-string v4, "FlxDrm"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DownloadHelper.findDownloadCaptionsFile found "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 255
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private static findDownloadedAsset(JZ)Ljava/lang/String;
    .locals 3
    .parameter "rightId"
    .parameter "checkLimboLockFile"

    .prologue
    .line 216
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/flixster/android/net/DownloadHelper;->createAssetFileName(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "wv/"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/flixster/android/net/DownloadHelper;->searchForNonLockNonRightFile(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static findDownloadedMovie(J)Ljava/lang/String;
    .locals 1
    .parameter "rightId"

    .prologue
    .line 174
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedAsset(JZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static findDownloadedMovieSize(J)J
    .locals 4
    .parameter "rightId"

    .prologue
    .line 358
    const/4 v2, 0x0

    invoke-static {p0, p1, v2}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedAsset(JZ)Ljava/lang/String;

    move-result-object v1

    .line 359
    .local v1, localUri:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .end local v1           #localUri:Ljava/lang/String;
    :cond_0
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 360
    .local v0, f:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    return-wide v2
.end method

.method public static findRemainingSpace()J
    .locals 10

    .prologue
    .line 364
    new-instance v3, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 365
    .local v3, stat:Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v7

    int-to-long v7, v7

    mul-long v0, v5, v7

    .line 367
    .local v0, availableSpace:J
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v4

    .line 368
    .local v4, user:Lnet/flixster/android/model/User;
    if-eqz v4, :cond_1

    .line 369
    invoke-virtual {v4}, Lnet/flixster/android/model/User;->getLockerRights()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 378
    :cond_1
    return-wide v0

    .line 369
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/LockerRight;

    .line 370
    .local v2, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isMovie()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isEpisode()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 371
    :cond_3
    iget-wide v6, v2, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v6, v7}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 372
    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetSizeRaw()J

    move-result-wide v6

    .line 373
    iget-wide v8, v2, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v8, v9}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedMovieSize(J)J

    move-result-wide v8

    sub-long/2addr v6, v8

    sub-long/2addr v0, v6

    goto :goto_0
.end method

.method private static getFileExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter "uri"

    .prologue
    .line 79
    const-string v0, ""

    .line 80
    .local v0, extension:Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 81
    const-string v3, "\\?"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object p0, v3, v4

    .line 82
    const-string v3, "."

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 83
    .local v1, lastDotIndex:I
    const-string v3, "/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 84
    .local v2, lastSlashIndex:I
    if-ge v2, v1, :cond_0

    .line 85
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 88
    .end local v1           #lastDotIndex:I
    .end local v2           #lastSlashIndex:I
    :cond_0
    return-object v0
.end method

.method public static getMovieDownloadProgress(Lnet/flixster/android/model/LockerRight;)Ljava/lang/String;
    .locals 11
    .parameter "r"

    .prologue
    const-wide/16 v9, 0x0

    .line 343
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetSizeRaw()J

    move-result-wide v1

    .line 344
    .local v1, downloadSizeRaw:J
    cmp-long v7, v1, v9

    if-lez v7, :cond_0

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->readableFileSize(J)Ljava/lang/String;

    move-result-object v0

    .line 346
    .local v0, downloadSize:Ljava/lang/String;
    :goto_0
    iget-wide v7, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v7, v8}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedMovieSize(J)J

    move-result-wide v4

    .line 347
    .local v4, downloadedSizeRaw:J
    cmp-long v7, v4, v9

    if-lez v7, :cond_1

    .line 348
    const-string v7, " "

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 347
    invoke-static {v4, v5, v7}, Lcom/flixster/android/net/DownloadHelper;->readableFileSizeCustom(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 350
    .local v3, downloadedSize:Ljava/lang/String;
    :goto_1
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v7, " "

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 351
    const/4 v7, 0x0

    const-string v9, " "

    invoke-virtual {v3, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v3, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    :goto_2
    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 352
    const-string v7, " of "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 350
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 354
    .local v6, progressText:Ljava/lang/String;
    return-object v6

    .line 344
    .end local v0           #downloadSize:Ljava/lang/String;
    .end local v3           #downloadedSize:Ljava/lang/String;
    .end local v4           #downloadedSizeRaw:J
    .end local v6           #progressText:Ljava/lang/String;
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 348
    .restart local v0       #downloadSize:Ljava/lang/String;
    .restart local v4       #downloadedSizeRaw:J
    :cond_1
    const-string v3, ""

    goto :goto_1

    .line 351
    .restart local v3       #downloadedSize:Ljava/lang/String;
    :cond_2
    const-string v7, "0"

    goto :goto_2
.end method

.method public static getRemoteFileSize(Ljava/lang/String;Landroid/os/Handler;)V
    .locals 2
    .parameter "url"
    .parameter "callback"

    .prologue
    .line 384
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lcom/flixster/android/net/DownloadHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/flixster/android/net/DownloadHelper$1;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 399
    return-void
.end method

.method public static isDownloaded(J)Z
    .locals 2
    .parameter "rightId"

    .prologue
    const/4 v0, 0x0

    .line 207
    invoke-static {p0, p1, v0}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedAsset(JZ)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static isDownloaded(Lnet/flixster/android/model/LockerRight;)Z
    .locals 4
    .parameter "right"

    .prologue
    .line 186
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 187
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getChildRightIds()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 192
    const/4 v1, 0x0

    .line 194
    :goto_0
    return v1

    .line 187
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 188
    .local v0, epRightId:Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 189
    const/4 v1, 0x1

    goto :goto_0

    .line 194
    .end local v0           #epRightId:Ljava/lang/Long;
    :cond_2
    iget-wide v1, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v1

    goto :goto_0
.end method

.method public static isMovieDownloadInProgress()Z
    .locals 5

    .prologue
    .line 105
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v1

    .line 106
    .local v1, user:Lnet/flixster/android/model/User;
    if-eqz v1, :cond_1

    .line 107
    invoke-virtual {v1}, Lnet/flixster/android/model/User;->getLockerRights()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 115
    :cond_1
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 107
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 108
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isMovie()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isEpisode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 109
    :cond_3
    iget-wide v3, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v3, v4}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 110
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isMovieDownloadInProgress(J)Z
    .locals 1
    .parameter "rightId"

    .prologue
    .line 141
    invoke-static {p0, p1}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    new-instance v0, Lcom/flixster/android/drm/DownloadLock;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/drm/DownloadLock;-><init>(J)V

    invoke-virtual {v0}, Lcom/flixster/android/drm/DownloadLock;->exists()Z

    move-result v0

    .line 144
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMovieDownloadInProgress(Lnet/flixster/android/model/LockerRight;)Z
    .locals 4
    .parameter "right"

    .prologue
    .line 123
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 124
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getChildRightIds()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 129
    const/4 v1, 0x0

    .line 131
    :goto_0
    return v1

    .line 124
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 125
    .local v0, epRightId:Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 126
    const/4 v1, 0x1

    goto :goto_0

    .line 131
    .end local v0           #epRightId:Ljava/lang/Long;
    :cond_2
    iget-wide v1, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v1

    goto :goto_0
.end method

.method protected static queryDownloadManagerForFile(J)Ljava/lang/String;
    .locals 9
    .parameter "downloadId"

    .prologue
    .line 314
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 315
    .local v2, context:Landroid/content/Context;
    const-string v6, "download"

    invoke-virtual {v2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/DownloadManager;

    .line 316
    .local v3, dm:Landroid/app/DownloadManager;
    new-instance v5, Landroid/app/DownloadManager$Query;

    invoke-direct {v5}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 317
    .local v5, query:Landroid/app/DownloadManager$Query;
    const/4 v6, 0x1

    new-array v6, v6, [J

    const/4 v7, 0x0

    aput-wide p0, v6, v7

    invoke-virtual {v5, v6}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 318
    invoke-virtual {v3, v5}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 319
    .local v0, c:Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 320
    const-string v6, "status"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 321
    .local v1, columnIndex:I
    const/16 v6, 0x8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-ne v6, v7, :cond_0

    .line 322
    const-string v6, "local_uri"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 323
    .local v4, localUri:Ljava/lang/String;
    const-string v6, "FlxMain"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DownloadHelper.queryDownloadManagerForFile found "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    .end local v1           #columnIndex:I
    .end local v4           #localUri:Ljava/lang/String;
    :goto_0
    return-object v4

    .line 326
    .restart local v1       #columnIndex:I
    :cond_0
    const-string v6, "FlxMain"

    .line 327
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DownloadHelper.queryDownloadManagerForFile non-successful status "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 326
    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    .end local v1           #columnIndex:I
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static readableFileSize(J)Ljava/lang/String;
    .locals 1
    .parameter "size"

    .prologue
    .line 404
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/flixster/android/net/DownloadHelper;->readableFileSizeCustom(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static readableFileSizeCustom(JLjava/lang/String;)Ljava/lang/String;
    .locals 10
    .parameter "size"
    .parameter "forcedDigitGroups"

    .prologue
    const/4 v1, 0x3

    const-wide/high16 v8, 0x4090

    .line 408
    const-wide/16 v2, 0x0

    cmp-long v2, p0, v2

    if-gtz v2, :cond_0

    .line 409
    const-string v1, "0"

    .line 413
    :goto_0
    return-object v1

    .line 411
    :cond_0
    if-nez p2, :cond_1

    long-to-double v2, p0

    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    invoke-static {v8, v9}, Ljava/lang/Math;->log10(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-int v0, v2

    .line 413
    .local v0, digitGroups:I
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    new-instance v3, Ljava/text/DecimalFormat;

    if-ge v0, v1, :cond_3

    const-string v1, "##0"

    :goto_2
    invoke-direct {v3, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    long-to-double v4, p0

    int-to-double v6, v0

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 414
    sget-object v2, Lcom/flixster/android/net/DownloadHelper;->units:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 413
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 412
    .end local v0           #digitGroups:I
    :cond_1
    const-string v2, "MB"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1

    .line 413
    .restart local v0       #digitGroups:I
    :cond_3
    const-string v1, "#.#"

    goto :goto_2
.end method

.method private static searchForNonLockNonRightFile(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 10
    .parameter "fileNamePrefix"
    .parameter "subDir"
    .parameter "checkLimboLockFile"

    .prologue
    .line 221
    invoke-static {p1}, Lcom/flixster/android/storage/ExternalStorage;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 222
    .local v0, extSubDir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 223
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 224
    .local v2, files:[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 225
    const/4 v3, 0x0

    .local v3, localUri:Ljava/lang/String;
    const/4 v4, 0x0

    .line 226
    .local v4, lockFileUri:Ljava/lang/String;
    array-length v6, v2

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v6, :cond_1

    .line 237
    if-eqz p2, :cond_4

    if-nez v3, :cond_4

    if-eqz v4, :cond_4

    .line 239
    const-string v5, "FlxMain"

    const-string v6, "DownloadHelper.searchForNonLockNonRightFile removing lock file in limbo state"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    invoke-static {v4}, Lcom/flixster/android/storage/ExternalStorage;->deleteFile(Ljava/lang/String;)Z

    .line 246
    .end local v2           #files:[Ljava/lang/String;
    .end local v3           #localUri:Ljava/lang/String;
    .end local v4           #lockFileUri:Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    :goto_1
    return-object v3

    .line 226
    .restart local v2       #files:[Ljava/lang/String;
    .restart local v3       #localUri:Ljava/lang/String;
    .restart local v4       #lockFileUri:Ljava/lang/String;
    :cond_1
    aget-object v1, v2, v5

    .line 227
    .local v1, file:Ljava/lang/String;
    invoke-virtual {v1, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 228
    const-string v7, ".dlck"

    invoke-virtual {v1, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    const-string v7, ".rght"

    invoke-virtual {v1, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 229
    const-string v7, ".ttml"

    invoke-virtual {v1, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 230
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "file://"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 231
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "DownloadHelper.searchForNonLockNonRightFile found "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_2
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 232
    :cond_3
    const-string v7, ".dlck"

    invoke-virtual {v1, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 233
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "file://"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 241
    .end local v1           #file:Ljava/lang/String;
    :cond_4
    if-eqz v3, :cond_0

    goto :goto_1
.end method
