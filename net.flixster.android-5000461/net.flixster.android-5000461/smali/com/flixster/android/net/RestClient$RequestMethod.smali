.class public final enum Lcom/flixster/android/net/RestClient$RequestMethod;
.super Ljava/lang/Enum;
.source "RestClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/net/RestClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RequestMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flixster/android/net/RestClient$RequestMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/flixster/android/net/RestClient$RequestMethod;

.field public static final enum GET:Lcom/flixster/android/net/RestClient$RequestMethod;

.field public static final enum POST:Lcom/flixster/android/net/RestClient$RequestMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/flixster/android/net/RestClient$RequestMethod;

    const-string v1, "GET"

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/net/RestClient$RequestMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/RestClient$RequestMethod;->GET:Lcom/flixster/android/net/RestClient$RequestMethod;

    new-instance v0, Lcom/flixster/android/net/RestClient$RequestMethod;

    const-string v1, "POST"

    invoke-direct {v0, v1, v3}, Lcom/flixster/android/net/RestClient$RequestMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/RestClient$RequestMethod;->POST:Lcom/flixster/android/net/RestClient$RequestMethod;

    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/flixster/android/net/RestClient$RequestMethod;

    sget-object v1, Lcom/flixster/android/net/RestClient$RequestMethod;->GET:Lcom/flixster/android/net/RestClient$RequestMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flixster/android/net/RestClient$RequestMethod;->POST:Lcom/flixster/android/net/RestClient$RequestMethod;

    aput-object v1, v0, v3

    sput-object v0, Lcom/flixster/android/net/RestClient$RequestMethod;->ENUM$VALUES:[Lcom/flixster/android/net/RestClient$RequestMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flixster/android/net/RestClient$RequestMethod;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/flixster/android/net/RestClient$RequestMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/net/RestClient$RequestMethod;

    return-object v0
.end method

.method public static values()[Lcom/flixster/android/net/RestClient$RequestMethod;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/flixster/android/net/RestClient$RequestMethod;->ENUM$VALUES:[Lcom/flixster/android/net/RestClient$RequestMethod;

    array-length v1, v0

    new-array v2, v1, [Lcom/flixster/android/net/RestClient$RequestMethod;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
