.class final enum Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;
.super Ljava/lang/Enum;
.source "TimedTextElement.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/net/TimedTextElement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SmpteDropMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DROP_NTSC:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

.field public static final enum DROP_PAL:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

.field private static final synthetic ENUM$VALUES:[Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

.field public static final enum NON_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    const-string v1, "DROP_NTSC"

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->DROP_NTSC:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    new-instance v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    const-string v1, "DROP_PAL"

    invoke-direct {v0, v1, v3}, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->DROP_PAL:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    new-instance v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    const-string v1, "NON_DROP"

    invoke-direct {v0, v1, v4}, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->NON_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->DROP_NTSC:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->DROP_PAL:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->NON_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->ENUM$VALUES:[Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method protected static match(Ljava/lang/String;)Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;
    .locals 1
    .parameter "dropMode"

    .prologue
    .line 21
    const-string v0, "dropNTSC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    sget-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->DROP_NTSC:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    .line 28
    :goto_0
    return-object v0

    .line 23
    :cond_0
    const-string v0, "dropPAL"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24
    sget-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->DROP_PAL:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    goto :goto_0

    .line 25
    :cond_1
    const-string v0, "nonDrop"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 26
    sget-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->NON_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    goto :goto_0

    .line 28
    :cond_2
    sget-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->NON_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    return-object v0
.end method

.method public static values()[Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->ENUM$VALUES:[Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    array-length v1, v0

    new-array v2, v1, [Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
