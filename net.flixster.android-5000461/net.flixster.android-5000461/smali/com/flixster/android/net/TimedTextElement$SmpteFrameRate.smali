.class final enum Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;
.super Ljava/lang/Enum;
.source "TimedTextElement.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/net/TimedTextElement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SmpteFrameRate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

.field public static final enum SMPTE_2398:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

.field public static final enum SMPTE_24:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

.field public static final enum SMPTE_25:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

.field public static final enum SMPTE_2997_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

.field public static final enum SMPTE_2997_NONDROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

.field public static final enum SMPTE_30:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    const-string v1, "SMPTE_2398"

    invoke-direct {v0, v1, v3}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2398:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    .line 35
    new-instance v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    const-string v1, "SMPTE_24"

    invoke-direct {v0, v1, v4}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_24:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    .line 36
    new-instance v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    const-string v1, "SMPTE_25"

    invoke-direct {v0, v1, v5}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_25:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    .line 37
    new-instance v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    const-string v1, "SMPTE_2997_DROP"

    invoke-direct {v0, v1, v6}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2997_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    .line 38
    new-instance v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    const-string v1, "SMPTE_2997_NONDROP"

    invoke-direct {v0, v1, v7}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2997_NONDROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    .line 39
    new-instance v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    const-string v1, "SMPTE_30"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_30:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    .line 33
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2398:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_24:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    aput-object v1, v0, v4

    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_25:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    aput-object v1, v0, v5

    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2997_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    aput-object v1, v0, v6

    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2997_NONDROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_30:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    aput-object v2, v0, v1

    sput-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->ENUM$VALUES:[Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method protected static match(D)Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;
    .locals 3
    .parameter "frameRate"

    .prologue
    .line 42
    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v0, v1

    .line 43
    .local v0, rateFloored:I
    sparse-switch v0, :sswitch_data_0

    .line 61
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_30:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    :goto_0
    return-object v1

    .line 45
    :sswitch_0
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2398:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    goto :goto_0

    .line 47
    :sswitch_1
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_24:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    goto :goto_0

    .line 49
    :sswitch_2
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_25:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    goto :goto_0

    .line 51
    :sswitch_3
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2997_NONDROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    goto :goto_0

    .line 53
    :sswitch_4
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_30:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    goto :goto_0

    .line 55
    :sswitch_5
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_25:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    goto :goto_0

    .line 57
    :sswitch_6
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_30:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    goto :goto_0

    .line 59
    :sswitch_7
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2997_NONDROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    goto :goto_0

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x18 -> :sswitch_1
        0x19 -> :sswitch_2
        0x1d -> :sswitch_3
        0x1e -> :sswitch_4
        0x32 -> :sswitch_5
        0x3b -> :sswitch_7
        0x3c -> :sswitch_6
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    return-object v0
.end method

.method public static values()[Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->ENUM$VALUES:[Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    array-length v1, v0

    new-array v2, v1, [Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
