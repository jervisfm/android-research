.class public Lcom/flixster/android/net/TimedTextElement;
.super Ljava/lang/Object;
.source "TimedTextElement.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;,
        Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteDropMode:[I = null

.field private static synthetic $SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteFrameRate:[I = null

.field protected static final ATTR_BEGIN:Ljava/lang/String; = "begin"

.field protected static final ATTR_DROP_MODE:Ljava/lang/String; = "ttp:dropMode"

.field protected static final ATTR_END:Ljava/lang/String; = "end"

.field protected static final ATTR_FRAME_RATE:Ljava/lang/String; = "ttp:frameRate"

.field protected static final ATTR_FRAME_RATE_MULTIPLIER:Ljava/lang/String; = "ttp:frameRateMultiplier"

.field protected static final ATTR_ORIGIN:Ljava/lang/String; = "tts:origin"

.field public static final LOCAL_FILE_EXTENSION:Ljava/lang/String; = ".ttml"

.field protected static final TAG_P:Ljava/lang/String; = "p"

.field protected static final TAG_TT:Ljava/lang/String; = "tt"

.field public static final TEST_URL:Ljava/lang/String; = "http://us1cc.res.com.edgesuite.net/p/prometheus_19cf6ad4_cc_lid_0_1.xml"


# instance fields
.field public final begin:I

.field public final end:I

.field public final originX:I

.field public final originY:I

.field public final region:I

.field public final text:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteDropMode()[I
    .locals 3

    .prologue
    .line 3
    sget-object v0, Lcom/flixster/android/net/TimedTextElement;->$SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteDropMode:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->values()[Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->DROP_NTSC:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    invoke-virtual {v1}, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->DROP_PAL:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    invoke-virtual {v1}, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->NON_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    invoke-virtual {v1}, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/flixster/android/net/TimedTextElement;->$SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteDropMode:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteFrameRate()[I
    .locals 3

    .prologue
    .line 3
    sget-object v0, Lcom/flixster/android/net/TimedTextElement;->$SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteFrameRate:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->values()[Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2398:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    invoke-virtual {v1}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_24:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    invoke-virtual {v1}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_25:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    invoke-virtual {v1}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2997_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    invoke-virtual {v1}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2997_NONDROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    invoke-virtual {v1}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_30:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    invoke-virtual {v1}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/flixster/android/net/TimedTextElement;->$SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteFrameRate:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17
    .parameter "begin"
    .parameter "end"
    .parameter "region"
    .parameter "origin"
    .parameter "text"
    .parameter "dropMode"
    .parameter "frameRate"
    .parameter "frameRateMultiplier"

    .prologue
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const-wide/16 v12, 0x0

    .line 75
    .local v12, frameRateValue:D
    if-eqz p8, :cond_0

    .line 76
    const-string v2, " "

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 77
    .local v15, multiplierStr:[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v15, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 78
    .local v11, frameRateNumerator:I
    const/4 v2, 0x1

    aget-object v2, v15, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 79
    .local v10, frameRateDenominator:I
    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-double v2, v2

    int-to-double v4, v11

    mul-double/2addr v2, v4

    int-to-double v4, v10

    div-double v12, v2, v4

    .line 81
    .end local v10           #frameRateDenominator:I
    .end local v11           #frameRateNumerator:I
    .end local v15           #multiplierStr:[Ljava/lang/String;
    :cond_0
    invoke-static/range {p6 .. p6}, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->match(Ljava/lang/String;)Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;

    move-result-object v14

    .line 82
    .local v14, mode:Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;
    sget-object v7, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_30:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    .line 83
    .local v7, rate:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;
    invoke-static {}, Lcom/flixster/android/net/TimedTextElement;->$SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteDropMode()[I

    move-result-object v2

    invoke-virtual {v14}, Lcom/flixster/android/net/TimedTextElement$SmpteDropMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 93
    :goto_0
    const-string v2, ":"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 94
    .local v8, beginStr:[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v8, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v2, 0x1

    aget-object v2, v8, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 95
    const/4 v2, 0x2

    aget-object v2, v8, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v2, 0x3

    aget-object v2, v8, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v2, p0

    .line 94
    invoke-direct/range {v2 .. v7}, Lcom/flixster/android/net/TimedTextElement;->convertSmpteToAbsoluteTime(IIIILcom/flixster/android/net/TimedTextElement$SmpteFrameRate;)J

    move-result-wide v2

    long-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/flixster/android/net/TimedTextElement;->begin:I

    .line 96
    const-string v2, ":"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 97
    .local v9, endStr:[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v9, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v2, 0x1

    aget-object v2, v9, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 98
    const/4 v2, 0x2

    aget-object v2, v9, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v2, 0x3

    aget-object v2, v9, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v2, p0

    .line 97
    invoke-direct/range {v2 .. v7}, Lcom/flixster/android/net/TimedTextElement;->convertSmpteToAbsoluteTime(IIIILcom/flixster/android/net/TimedTextElement$SmpteFrameRate;)J

    move-result-wide v2

    long-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/flixster/android/net/TimedTextElement;->end:I

    .line 99
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/flixster/android/net/TimedTextElement;->region:I

    .line 100
    const-string v2, "%"

    const-string v3, ""

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 101
    .local v16, originStr:[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v16, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/flixster/android/net/TimedTextElement;->originX:I

    .line 102
    const/4 v2, 0x1

    aget-object v2, v16, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/flixster/android/net/TimedTextElement;->originY:I

    .line 103
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/flixster/android/net/TimedTextElement;->text:Ljava/lang/String;

    .line 104
    return-void

    .line 86
    .end local v8           #beginStr:[Ljava/lang/String;
    .end local v9           #endStr:[Ljava/lang/String;
    .end local v16           #originStr:[Ljava/lang/String;
    :pswitch_0
    sget-object v7, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->SMPTE_2997_DROP:Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    .line 87
    goto/16 :goto_0

    .line 89
    :pswitch_1
    invoke-static {v12, v13}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->match(D)Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;

    move-result-object v7

    goto/16 :goto_0

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private convertSmpteToAbsoluteTime(IIIILcom/flixster/android/net/TimedTextElement$SmpteFrameRate;)J
    .locals 2
    .parameter "hours"
    .parameter "minutes"
    .parameter "seconds"
    .parameter "frames"
    .parameter "rate"

    .prologue
    .line 107
    invoke-static {}, Lcom/flixster/android/net/TimedTextElement;->$SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteFrameRate()[I

    move-result-object v0

    invoke-virtual {p5}, Lcom/flixster/android/net/TimedTextElement$SmpteFrameRate;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 121
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    .line 109
    :pswitch_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/flixster/android/net/TimedTextElement;->smpte23_98_ToAbsoluteTime(IIII)J

    move-result-wide v0

    goto :goto_0

    .line 111
    :pswitch_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/flixster/android/net/TimedTextElement;->smpte24_ToAbsoluteTime(IIII)J

    move-result-wide v0

    goto :goto_0

    .line 113
    :pswitch_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/flixster/android/net/TimedTextElement;->smpte25_ToAbsoluteTime(IIII)J

    move-result-wide v0

    goto :goto_0

    .line 115
    :pswitch_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/flixster/android/net/TimedTextElement;->smpte29_97_Drop_ToAbsoluteTime(IIII)J

    move-result-wide v0

    goto :goto_0

    .line 117
    :pswitch_4
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/flixster/android/net/TimedTextElement;->smpte29_97_NonDrop_ToAbsoluteTime(IIII)J

    move-result-wide v0

    goto :goto_0

    .line 119
    :pswitch_5
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/flixster/android/net/TimedTextElement;->smpte30_ToAbsoluteTime(IIII)J

    move-result-wide v0

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private smpte23_98_ToAbsoluteTime(IIII)J
    .locals 6
    .parameter "hours"
    .parameter "minutes"
    .parameter "seconds"
    .parameter "frames"

    .prologue
    .line 126
    const-wide v0, 0x40ad538000000000L

    int-to-double v2, p4

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    const-wide/32 v2, 0x15fea

    mul-int/lit8 v4, p1, 0x3c

    add-int/2addr v4, p2

    mul-int/lit8 v4, v4, 0x3c

    add-int/2addr v4, p3

    int-to-long v4, v4

    mul-long/2addr v2, v4

    long-to-double v2, v2

    add-double/2addr v0, v2

    double-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x15f90

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private smpte24_ToAbsoluteTime(IIII)J
    .locals 6
    .parameter "hours"
    .parameter "minutes"
    .parameter "seconds"
    .parameter "frames"

    .prologue
    const-wide/32 v4, 0x15f90

    .line 130
    mul-int/lit16 v0, p4, 0xea6

    int-to-long v0, v0

    mul-int/lit8 v2, p1, 0x3c

    add-int/2addr v2, p2

    mul-int/lit8 v2, v2, 0x3c

    add-int/2addr v2, p3

    int-to-long v2, v2

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    div-long/2addr v0, v4

    return-wide v0
.end method

.method private smpte25_ToAbsoluteTime(IIII)J
    .locals 6
    .parameter "hours"
    .parameter "minutes"
    .parameter "seconds"
    .parameter "frames"

    .prologue
    const-wide/32 v4, 0x15f90

    .line 134
    mul-int/lit16 v0, p4, 0xe10

    int-to-long v0, v0

    mul-int/lit8 v2, p1, 0x3c

    add-int/2addr v2, p2

    mul-int/lit8 v2, v2, 0x3c

    add-int/2addr v2, p3

    int-to-long v2, v2

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    div-long/2addr v0, v4

    return-wide v0
.end method

.method private smpte29_97_Drop_ToAbsoluteTime(IIII)J
    .locals 6
    .parameter "hours"
    .parameter "minutes"
    .parameter "seconds"
    .parameter "frames"

    .prologue
    .line 138
    mul-int/lit16 v0, p4, 0xbbb

    const v1, 0x15fea

    mul-int/2addr v1, p3

    add-int/2addr v0, v1

    const v1, 0x19bfca5

    mul-int/2addr v1, p2

    div-int/lit8 v1, v1, 0x5

    add-int/2addr v0, v1

    int-to-long v0, v0

    const-wide/32 v2, 0x134fd7bc

    int-to-long v4, p1

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x15f90

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private smpte29_97_NonDrop_ToAbsoluteTime(IIII)J
    .locals 6
    .parameter "hours"
    .parameter "minutes"
    .parameter "seconds"
    .parameter "frames"

    .prologue
    .line 142
    mul-int/lit16 v0, p4, 0xbbb

    int-to-long v0, v0

    const-wide/32 v2, 0x15fea

    mul-int/lit8 v4, p1, 0x3c

    add-int/2addr v4, p2

    mul-int/lit8 v4, v4, 0x3c

    add-int/2addr v4, p3

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x15f90

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private smpte30_ToAbsoluteTime(IIII)J
    .locals 6
    .parameter "hours"
    .parameter "minutes"
    .parameter "seconds"
    .parameter "frames"

    .prologue
    const-wide/32 v4, 0x15f90

    .line 146
    mul-int/lit16 v0, p4, 0xbb8

    int-to-long v0, v0

    mul-int/lit8 v2, p1, 0x3c

    add-int/2addr v2, p2

    mul-int/lit8 v2, v2, 0x3c

    add-int/2addr v2, p3

    int-to-long v2, v2

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    div-long/2addr v0, v4

    return-wide v0
.end method
