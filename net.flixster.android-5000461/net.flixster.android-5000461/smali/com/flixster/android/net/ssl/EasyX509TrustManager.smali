.class public Lcom/flixster/android/net/ssl/EasyX509TrustManager;
.super Ljava/lang/Object;
.source "EasyX509TrustManager.java"

# interfaces
.implements Ljavax/net/ssl/X509TrustManager;


# instance fields
.field private final standardTrustManager:Ljavax/net/ssl/X509TrustManager;


# direct methods
.method public constructor <init>(Ljava/security/KeyStore;)V
    .locals 4
    .parameter "keystore"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0

    .line 23
    .local v0, factory:Ljavax/net/ssl/TrustManagerFactory;
    invoke-virtual {v0, p1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 24
    invoke-virtual {v0}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v1

    .line 25
    .local v1, trustmanagers:[Ljavax/net/ssl/TrustManager;
    array-length v2, v1

    if-nez v2, :cond_0

    .line 26
    new-instance v2, Ljava/security/NoSuchAlgorithmException;

    const-string v3, "no trust manager found"

    invoke-direct {v2, v3}, Ljava/security/NoSuchAlgorithmException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 28
    :cond_0
    const/4 v2, 0x0

    aget-object v2, v1, v2

    check-cast v2, Ljavax/net/ssl/X509TrustManager;

    iput-object v2, p0, Lcom/flixster/android/net/ssl/EasyX509TrustManager;->standardTrustManager:Ljavax/net/ssl/X509TrustManager;

    .line 29
    return-void
.end method


# virtual methods
.method public checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 1
    .parameter "certificates"
    .parameter "authType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/flixster/android/net/ssl/EasyX509TrustManager;->standardTrustManager:Ljavax/net/ssl/X509TrustManager;

    invoke-interface {v0, p1, p2}, Ljavax/net/ssl/X509TrustManager;->checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 9
    .parameter "certificates"
    .parameter "authType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 48
    array-length v0, p1

    .line 49
    .local v0, chainLength:I
    array-length v7, p1

    const/4 v8, 0x1

    if-le v7, v8, :cond_1

    .line 57
    const/4 v1, 0x0

    .local v1, currIndex:I
    :goto_0
    array-length v7, p1

    if-lt v1, v7, :cond_2

    .line 78
    :cond_0
    add-int/lit8 v0, v1, 0x1

    .line 79
    add-int/lit8 v7, v0, -0x1

    aget-object v3, p1, v7

    .line 80
    .local v3, lastCertificate:Ljava/security/cert/X509Certificate;
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    .line 81
    .local v5, now:Ljava/util/Date;
    invoke-virtual {v3}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v7

    invoke-virtual {v3}, Ljava/security/cert/X509Certificate;->getIssuerDN()Ljava/security/Principal;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/security/Principal;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 82
    invoke-virtual {v3}, Ljava/security/cert/X509Certificate;->getNotAfter()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 83
    add-int/lit8 v0, v0, -0x1

    .line 87
    .end local v1           #currIndex:I
    .end local v3           #lastCertificate:Ljava/security/cert/X509Certificate;
    .end local v5           #now:Ljava/util/Date;
    :cond_1
    iget-object v7, p0, Lcom/flixster/android/net/ssl/EasyX509TrustManager;->standardTrustManager:Ljavax/net/ssl/X509TrustManager;

    invoke-interface {v7, p1, p2}, Ljavax/net/ssl/X509TrustManager;->checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V

    .line 95
    return-void

    .line 58
    .restart local v1       #currIndex:I
    :cond_2
    const/4 v2, 0x0

    .line 59
    .local v2, foundNext:Z
    add-int/lit8 v4, v1, 0x1

    .local v4, nextIndex:I
    :goto_1
    array-length v7, p1

    if-lt v4, v7, :cond_4

    .line 71
    :cond_3
    :goto_2
    if-eqz v2, :cond_0

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60
    :cond_4
    aget-object v7, p1, v1

    invoke-virtual {v7}, Ljava/security/cert/X509Certificate;->getIssuerDN()Ljava/security/Principal;

    move-result-object v7

    aget-object v8, p1, v4

    invoke-virtual {v8}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/security/Principal;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 61
    const/4 v2, 0x1

    .line 63
    add-int/lit8 v7, v1, 0x1

    if-eq v4, v7, :cond_3

    .line 64
    aget-object v6, p1, v4

    .line 65
    .local v6, tempCertificate:Ljava/security/cert/X509Certificate;
    add-int/lit8 v7, v1, 0x1

    aget-object v7, p1, v7

    aput-object v7, p1, v4

    .line 66
    add-int/lit8 v7, v1, 0x1

    aput-object v6, p1, v7

    goto :goto_2

    .line 59
    .end local v6           #tempCertificate:Ljava/security/cert/X509Certificate;
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public getAcceptedIssuers()[Ljava/security/cert/X509Certificate;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/flixster/android/net/ssl/EasyX509TrustManager;->standardTrustManager:Ljavax/net/ssl/X509TrustManager;

    invoke-interface {v0}, Ljavax/net/ssl/X509TrustManager;->getAcceptedIssuers()[Ljava/security/cert/X509Certificate;

    move-result-object v0

    return-object v0
.end method
