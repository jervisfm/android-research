.class public abstract Lcom/flixster/android/storage/ExternalPreferenceFile;
.super Ljava/lang/Object;
.source "ExternalPreferenceFile.java"


# static fields
.field public static final DELIMITER:Ljava/lang/String; = "ZZZZ"

.field public static final NONE:Ljava/lang/String; = "."


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract createFileContent()Ljava/lang/String;
.end method

.method protected abstract createFileName()Ljava/lang/String;
.end method

.method protected abstract createFileSubDir()Ljava/lang/String;
.end method

.method public delete()Z
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/flixster/android/storage/ExternalPreferenceFile;->createFileSubDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flixster/android/storage/ExternalPreferenceFile;->createFileName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/storage/ExternalStorage;->deleteFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected abstract parseFileContent(Ljava/lang/String;)Z
.end method

.method public read()Z
    .locals 3

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/flixster/android/storage/ExternalPreferenceFile;->createFileSubDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/storage/ExternalPreferenceFile;->createFileName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/storage/ExternalStorage;->readFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    .local v0, content:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/flixster/android/storage/ExternalPreferenceFile;->parseFileContent(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public save()V
    .locals 3

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/flixster/android/storage/ExternalPreferenceFile;->createFileContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flixster/android/storage/ExternalPreferenceFile;->createFileSubDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/flixster/android/storage/ExternalPreferenceFile;->createFileName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/flixster/android/storage/ExternalStorage;->writeFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 28
    return-void
.end method

.method public split(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .parameter "content"

    .prologue
    .line 35
    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/String;

    .line 36
    .local v0, params:[Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 37
    const-string v1, "ZZZZ"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 39
    :cond_0
    return-object v0
.end method
