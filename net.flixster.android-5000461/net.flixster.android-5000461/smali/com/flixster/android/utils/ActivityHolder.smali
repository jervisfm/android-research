.class public Lcom/flixster/android/utils/ActivityHolder;
.super Ljava/lang/Object;
.source "ActivityHolder.java"


# static fields
.field private static final INSTANCE:Lcom/flixster/android/utils/ActivityHolder;


# instance fields
.field private ctr:I

.field private topLevel:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    new-instance v0, Lcom/flixster/android/utils/ActivityHolder;

    invoke-direct {v0}, Lcom/flixster/android/utils/ActivityHolder;-><init>()V

    sput-object v0, Lcom/flixster/android/utils/ActivityHolder;->INSTANCE:Lcom/flixster/android/utils/ActivityHolder;

    .line 5
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public static instance()Lcom/flixster/android/utils/ActivityHolder;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/flixster/android/utils/ActivityHolder;->INSTANCE:Lcom/flixster/android/utils/ActivityHolder;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 47
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/flixster/android/utils/ActivityHolder;->topLevel:Landroid/app/Activity;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/flixster/android/utils/ActivityHolder;->ctr:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    monitor-exit p0

    return-void

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTopLevelActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 22
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/utils/ActivityHolder;->topLevel:Landroid/app/Activity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeTopLevelActivity(I)V
    .locals 1
    .parameter "id"

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/flixster/android/utils/ActivityHolder;->ctr:I

    if-lt p1, v0, :cond_0

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/utils/ActivityHolder;->topLevel:Landroid/app/Activity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    :cond_0
    monitor-exit p0

    return-void

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setTopLevelActivity(Landroid/app/Activity;)I
    .locals 1
    .parameter "activity"

    .prologue
    .line 31
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/flixster/android/utils/ActivityHolder;->topLevel:Landroid/app/Activity;

    .line 32
    iget v0, p0, Lcom/flixster/android/utils/ActivityHolder;->ctr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/flixster/android/utils/ActivityHolder;->ctr:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
