.class public Lcom/flixster/android/utils/AppRater;
.super Ljava/lang/Object;
.source "AppRater.java"


# static fields
.field private static final AMAZON_APPSTORE_LINK:Ljava/lang/String; = "http://www.amazon.com/gp/mas/dl/android?p="

.field private static final APP_PNAME:Ljava/lang/String; = "net.flixster.android"

.field public static final IS_DISABLED:Z = false

.field private static final LAUNCHES_UNTIL_PROMPT:I = 0xa

.field private static final MARKET_DEEP_LINK:Ljava/lang/String; = "market://details?id="


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static appLaunched(Landroid/content/Context;)V
    .locals 13
    .parameter "context"

    .prologue
    const-wide/16 v11, 0x0

    const/4 v10, 0x0

    .line 29
    const/4 v2, 0x0

    .line 31
    .local v2, info:Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 37
    :goto_0
    const-string v6, "apprater"

    invoke-virtual {p0, v6, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 38
    .local v5, prefs:Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 40
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    const-string v6, "lastRunVersion"

    invoke-interface {v5, v6, v11, v12}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    iget v8, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    .line 41
    const-string v6, "dontshowagain"

    invoke-interface {v1, v6, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 42
    const-string v6, "launch_count"

    invoke-interface {v1, v6, v11, v12}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 43
    const-string v6, "lastRunVersion"

    iget v7, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v7, v7

    invoke-interface {v1, v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 44
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 47
    :cond_0
    const-string v6, "dontshowagain"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 48
    const-string v6, "FlxMain"

    const-string v7, "AppRater.appLaunched rate dialog disabled"

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :goto_1
    return-void

    .line 32
    .end local v1           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v5           #prefs:Landroid/content/SharedPreferences;
    :catch_0
    move-exception v0

    .line 34
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1       #editor:Landroid/content/SharedPreferences$Editor;
    .restart local v5       #prefs:Landroid/content/SharedPreferences;
    :cond_1
    const-string v6, "launch_count"

    invoke-interface {v5, v6, v11, v12}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-wide/16 v8, 0x1

    add-long v3, v6, v8

    .line 53
    .local v3, launch_count:J
    const-string v6, "FlxMain"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "AppRater.appLaunched launch count: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v6, "launch_count"

    invoke-interface {v1, v6, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 55
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1
.end method

.method public static rateThisApp(Landroid/content/Context;)V
    .locals 5
    .parameter "context"

    .prologue
    .line 116
    const-string v2, "apprater"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 117
    .local v1, prefs:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 120
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    const-string v4, "market://details?id=net.flixster.android"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 118
    invoke-static {p0, v2}, Lnet/flixster/android/Starter;->tryLaunch(Landroid/content/Context;Landroid/content/Intent;)V

    .line 122
    if-eqz v0, :cond_0

    .line 123
    const-string v2, "dontshowagain"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 124
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 126
    :cond_0
    return-void
.end method

.method public static showRateDialog(Landroid/content/Context;)V
    .locals 8
    .parameter "context"

    .prologue
    const/4 v6, 0x0

    .line 63
    const-string v5, "apprater"

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 64
    .local v4, prefs:Landroid/content/SharedPreferences;
    const-string v5, "dontshowagain"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    const-string v5, "launch_count"

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 69
    .local v2, launch_count:J
    const-wide/16 v5, 0xa

    cmp-long v5, v2, v5

    if-ltz v5, :cond_0

    .line 73
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 75
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v5

    const-string v6, "/rating-prompt"

    const-string v7, "Rating - Prompt"

    invoke-interface {v5, v6, v7}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0c01ab

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 78
    const v6, 0x7f0c01ac

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 79
    const v6, 0x7f0c01ae

    new-instance v7, Lcom/flixster/android/utils/AppRater$1;

    invoke-direct {v7, p0}, Lcom/flixster/android/utils/AppRater$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 86
    const v6, 0x7f0c01af

    new-instance v7, Lcom/flixster/android/utils/AppRater$2;

    invoke-direct {v7, v1}, Lcom/flixster/android/utils/AppRater$2;-><init>(Landroid/content/SharedPreferences$Editor;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 96
    const v6, 0x7f0c01b0

    new-instance v7, Lcom/flixster/android/utils/AppRater$3;

    invoke-direct {v7, v1}, Lcom/flixster/android/utils/AppRater$3;-><init>(Landroid/content/SharedPreferences$Editor;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 106
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 108
    .local v0, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
