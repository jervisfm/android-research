.class public Lcom/flixster/android/utils/CryptHelper;
.super Ljava/lang/Object;
.source "CryptHelper.java"


# static fields
.field private static final SHARED_KEY:Ljava/lang/String; = "799741c015681d32"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decryptAES([BLjava/lang/String;)Ljava/lang/String;
    .locals 14
    .parameter "key"
    .parameter "s"

    .prologue
    .line 52
    if-nez p0, :cond_0

    .line 53
    :try_start_0
    const-string v11, "AES"

    invoke-static {v11}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v5

    .line 54
    .local v5, kgen:Ljavax/crypto/KeyGenerator;
    const/16 v11, 0x80

    invoke-virtual {v5, v11}, Ljavax/crypto/KeyGenerator;->init(I)V

    .line 55
    invoke-virtual {v5}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v8

    .line 56
    .local v8, skey:Ljavax/crypto/SecretKey;
    invoke-interface {v8}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object p0

    .line 58
    .end local v5           #kgen:Ljavax/crypto/KeyGenerator;
    .end local v8           #skey:Ljavax/crypto/SecretKey;
    :cond_0
    new-instance v9, Ljavax/crypto/spec/SecretKeySpec;

    const-string v11, "AES"

    invoke-direct {v9, p0, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 59
    .local v9, skeySpec:Ljavax/crypto/spec/SecretKeySpec;
    const-string v11, "AES"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 60
    .local v1, cipher:Ljavax/crypto/Cipher;
    const/4 v11, 0x2

    invoke-virtual {v1, v11, v9}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 61
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v11}, Lcom/flixster/android/utils/Hex;->decodeHex(Ljava/lang/String;)[B

    move-result-object v10

    .line 62
    .local v10, unencoded:[B
    invoke-virtual {v1, v10}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v2

    .line 63
    .local v2, decrypted:[B
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4

    .line 75
    .end local v1           #cipher:Ljavax/crypto/Cipher;
    .end local v2           #decrypted:[B
    .end local v9           #skeySpec:Ljavax/crypto/spec/SecretKeySpec;
    .end local v10           #unencoded:[B
    :goto_0
    return-object v11

    .line 64
    :catch_0
    move-exception v6

    .line 65
    .local v6, nsae:Ljava/security/NoSuchAlgorithmException;
    const-string v11, "FlxMain"

    const-string v12, "AES algorithm not available"

    invoke-static {v11, v12, v6}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 75
    .end local v6           #nsae:Ljava/security/NoSuchAlgorithmException;
    :goto_1
    const/4 v11, 0x0

    goto :goto_0

    .line 66
    :catch_1
    move-exception v7

    .line 67
    .local v7, nspe:Ljavax/crypto/NoSuchPaddingException;
    const-string v11, "FlxMain"

    const-string v12, "AES padding not available"

    invoke-static {v11, v12, v7}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 68
    .end local v7           #nspe:Ljavax/crypto/NoSuchPaddingException;
    :catch_2
    move-exception v4

    .line 69
    .local v4, ike:Ljava/security/InvalidKeyException;
    const-string v11, "FlxMain"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Invalid key \'"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v4}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 70
    .end local v4           #ike:Ljava/security/InvalidKeyException;
    :catch_3
    move-exception v3

    .line 71
    .local v3, ibse:Ljavax/crypto/IllegalBlockSizeException;
    const-string v11, "FlxMain"

    const-string v12, "Illegal block size"

    invoke-static {v11, v12, v3}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 72
    .end local v3           #ibse:Ljavax/crypto/IllegalBlockSizeException;
    :catch_4
    move-exception v0

    .line 73
    .local v0, bpe:Ljavax/crypto/BadPaddingException;
    const-string v11, "FlxMain"

    const-string v12, "Bad padding"

    invoke-static {v11, v12, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static encryptAES([BLjava/lang/String;)Ljava/lang/String;
    .locals 14
    .parameter "key"
    .parameter "s"

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    :try_start_0
    const-string v11, "AES"

    invoke-static {v11}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v6

    .line 25
    .local v6, kgen:Ljavax/crypto/KeyGenerator;
    const/16 v11, 0x80

    invoke-virtual {v6, v11}, Ljavax/crypto/KeyGenerator;->init(I)V

    .line 26
    invoke-virtual {v6}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v9

    .line 27
    .local v9, skey:Ljavax/crypto/SecretKey;
    invoke-interface {v9}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object p0

    .line 29
    .end local v6           #kgen:Ljavax/crypto/KeyGenerator;
    .end local v9           #skey:Ljavax/crypto/SecretKey;
    :cond_0
    new-instance v10, Ljavax/crypto/spec/SecretKeySpec;

    const-string v11, "AES"

    invoke-direct {v10, p0, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 30
    .local v10, skeySpec:Ljavax/crypto/spec/SecretKeySpec;
    const-string v11, "AES"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 31
    .local v1, cipher:Ljavax/crypto/Cipher;
    const/4 v11, 0x1

    invoke-virtual {v1, v11, v10}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 32
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v1, v11}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v2

    .line 33
    .local v2, encrypted:[B
    const/4 v11, 0x0

    invoke-static {v2, v11}, Lcom/flixster/android/utils/Hex;->encodeHex([BZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v3

    .line 47
    .end local v1           #cipher:Ljavax/crypto/Cipher;
    .end local v2           #encrypted:[B
    .end local v10           #skeySpec:Ljavax/crypto/spec/SecretKeySpec;
    :goto_0
    return-object v3

    .line 36
    :catch_0
    move-exception v7

    .line 37
    .local v7, nsae:Ljava/security/NoSuchAlgorithmException;
    const-string v11, "FlxMain"

    const-string v12, "AES algorithm not available"

    invoke-static {v11, v12, v7}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 47
    .end local v7           #nsae:Ljava/security/NoSuchAlgorithmException;
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 38
    :catch_1
    move-exception v8

    .line 39
    .local v8, nspe:Ljavax/crypto/NoSuchPaddingException;
    const-string v11, "FlxMain"

    const-string v12, "AES padding not available"

    invoke-static {v11, v12, v8}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 40
    .end local v8           #nspe:Ljavax/crypto/NoSuchPaddingException;
    :catch_2
    move-exception v5

    .line 41
    .local v5, ike:Ljava/security/InvalidKeyException;
    const-string v11, "FlxMain"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Invalid key \'"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v5}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 42
    .end local v5           #ike:Ljava/security/InvalidKeyException;
    :catch_3
    move-exception v4

    .line 43
    .local v4, ibse:Ljavax/crypto/IllegalBlockSizeException;
    const-string v11, "FlxMain"

    const-string v12, "Illegal block size"

    invoke-static {v11, v12, v4}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 44
    .end local v4           #ibse:Ljavax/crypto/IllegalBlockSizeException;
    :catch_4
    move-exception v0

    .line 45
    .local v0, bpe:Ljavax/crypto/BadPaddingException;
    const-string v11, "FlxMain"

    const-string v12, "Bad padding"

    invoke-static {v11, v12, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static encryptEsig(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "s"

    .prologue
    .line 18
    const-string v0, "799741c015681d32"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, p0}, Lcom/flixster/android/utils/CryptHelper;->encryptAES([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static test()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    const-string v2, "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(),./;\'[]"

    .line 80
    .local v2, string:Ljava/lang/String;
    const-string v3, "799741c015681d32"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-static {v3, v2}, Lcom/flixster/android/utils/CryptHelper;->encryptAES([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, encrypted:Ljava/lang/String;
    const-string v3, "799741c015681d32"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-static {v3, v1}, Lcom/flixster/android/utils/CryptHelper;->decryptAES([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, decrypted:Ljava/lang/String;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Clear text = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Encrypted text (Hex encoded) = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Decrypted text (Hex decoded) = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v4, "FlxMain"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "success!"

    :goto_0
    invoke-static {v4, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    return-void

    .line 85
    :cond_0
    const-string v3, "failure!"

    goto :goto_0
.end method
