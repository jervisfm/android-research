.class public Lcom/flixster/android/utils/DateTimeHelper;
.super Lcom/flixster/android/utils/LocalizedHelper;
.source "DateTimeHelper.java"


# static fields
.field private static final API_HOUR_PARSER:Ljava/text/SimpleDateFormat; = null

.field public static final DAY_IN_WEEK_FORMATTER:Ljava/text/SimpleDateFormat; = null

.field private static final HOUR_FORMAT_12:Ljava/lang/String; = "hh:mm aa"

.field private static final HOUR_FORMAT_24:Ljava/lang/String; = "HH:mm"

.field private static final INSTANCE:Lcom/flixster/android/utils/DateTimeHelper;

.field private static final MONTH_DAY_FORMATTER:Ljava/text/SimpleDateFormat;

.field private static final MONTH_DAY_YEAR_FORMATTER:Ljava/text/SimpleDateFormat;


# instance fields
.field private final longDateFormatter:Ljava/text/DateFormat;

.field private final mediumDateFormatter:Ljava/text/DateFormat;

.field private final shortTimeFormatter:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/flixster/android/utils/DateTimeHelper;

    invoke-direct {v0}, Lcom/flixster/android/utils/DateTimeHelper;-><init>()V

    sput-object v0, Lcom/flixster/android/utils/DateTimeHelper;->INSTANCE:Lcom/flixster/android/utils/DateTimeHelper;

    .line 18
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "hh:mm aa"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/flixster/android/utils/DateTimeHelper;->API_HOUR_PARSER:Ljava/text/SimpleDateFormat;

    .line 19
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMMM d"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/flixster/android/utils/DateTimeHelper;->MONTH_DAY_FORMATTER:Ljava/text/SimpleDateFormat;

    .line 20
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMMM d, yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/flixster/android/utils/DateTimeHelper;->MONTH_DAY_YEAR_FORMATTER:Ljava/text/SimpleDateFormat;

    .line 21
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "E, MMM d"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/flixster/android/utils/DateTimeHelper;->DAY_IN_WEEK_FORMATTER:Ljava/text/SimpleDateFormat;

    .line 14
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/flixster/android/utils/LocalizedHelper;-><init>()V

    .line 26
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getLocale()Ljava/util/Locale;

    move-result-object v0

    .line 27
    .local v0, locale:Ljava/util/Locale;
    const/4 v1, 0x2

    invoke-static {v1, v0}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/utils/DateTimeHelper;->mediumDateFormatter:Ljava/text/DateFormat;

    .line 28
    const/4 v1, 0x1

    invoke-static {v1, v0}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/utils/DateTimeHelper;->longDateFormatter:Ljava/text/DateFormat;

    .line 29
    iget-boolean v1, p0, Lcom/flixster/android/utils/DateTimeHelper;->isFrance:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "HH:mm"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    :goto_0
    iput-object v1, p0, Lcom/flixster/android/utils/DateTimeHelper;->shortTimeFormatter:Ljava/text/DateFormat;

    .line 30
    return-void

    .line 29
    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "hh:mm aa"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static formatMonthDay(Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .parameter "date"

    .prologue
    .line 57
    sget-object v0, Lcom/flixster/android/utils/DateTimeHelper;->MONTH_DAY_FORMATTER:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatMonthDayYear(Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .parameter "date"

    .prologue
    .line 62
    sget-object v0, Lcom/flixster/android/utils/DateTimeHelper;->MONTH_DAY_YEAR_FORMATTER:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static longDateFormatter()Ljava/text/DateFormat;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/flixster/android/utils/DateTimeHelper;->INSTANCE:Lcom/flixster/android/utils/DateTimeHelper;

    iget-object v0, v0, Lcom/flixster/android/utils/DateTimeHelper;->longDateFormatter:Ljava/text/DateFormat;

    return-object v0
.end method

.method public static mediumDateFormatter()Ljava/text/DateFormat;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/flixster/android/utils/DateTimeHelper;->INSTANCE:Lcom/flixster/android/utils/DateTimeHelper;

    iget-object v0, v0, Lcom/flixster/android/utils/DateTimeHelper;->mediumDateFormatter:Ljava/text/DateFormat;

    return-object v0
.end method

.method public static shortTimeFormat(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "time"

    .prologue
    .line 42
    sget-object v2, Lcom/flixster/android/utils/DateTimeHelper;->INSTANCE:Lcom/flixster/android/utils/DateTimeHelper;

    iget-boolean v2, v2, Lcom/flixster/android/utils/DateTimeHelper;->isFrance:Z

    if-nez v2, :cond_1

    .line 52
    .end local p0
    :cond_0
    :goto_0
    return-object p0

    .line 46
    .restart local p0
    :cond_1
    const/4 v0, 0x0

    .line 48
    .local v0, datetime:Ljava/util/Date;
    :try_start_0
    sget-object v2, Lcom/flixster/android/utils/DateTimeHelper;->API_HOUR_PARSER:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 52
    :goto_1
    if-eqz v0, :cond_0

    sget-object v2, Lcom/flixster/android/utils/DateTimeHelper;->INSTANCE:Lcom/flixster/android/utils/DateTimeHelper;

    iget-object v2, v2, Lcom/flixster/android/utils/DateTimeHelper;->shortTimeFormatter:Ljava/text/DateFormat;

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 49
    :catch_0
    move-exception v1

    .line 50
    .local v1, e:Ljava/text/ParseException;
    const-string v2, "FlxMain"

    const-string v3, "DateTimeHelper.shortTimeFormat"

    invoke-static {v2, v3, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
