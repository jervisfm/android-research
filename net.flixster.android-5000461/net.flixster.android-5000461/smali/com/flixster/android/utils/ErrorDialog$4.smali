.class Lcom/flixster/android/utils/ErrorDialog$4;
.super Ljava/lang/Object;
.source "ErrorDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/DialogActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$activity:Lcom/flixster/android/activity/DialogActivity;

.field private final synthetic val$de:Lnet/flixster/android/data/DaoException;


# direct methods
.method constructor <init>(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/DialogActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    iput-object p2, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$activity:Lcom/flixster/android/activity/DialogActivity;

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const v5, 0x3b9acd89

    const v4, 0x3b9acd87

    const/4 v3, 0x0

    .line 123
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->NOT_LICENSED:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 124
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$activity:Lcom/flixster/android/activity/DialogActivity;

    const v1, 0x3b9acd8a

    invoke-virtual {v0, v1, v3}, Lcom/flixster/android/activity/DialogActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->NETWORK:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_3

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$activity:Lcom/flixster/android/activity/DialogActivity;

    const v1, 0x3b9acd84

    invoke-virtual {v0, v1, v3}, Lcom/flixster/android/activity/DialogActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 127
    :cond_3
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->NETWORK_UNSTABLE:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_4

    .line 128
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$activity:Lcom/flixster/android/activity/DialogActivity;

    const v1, 0x3b9acd8c

    invoke-virtual {v0, v1, v3}, Lcom/flixster/android/activity/DialogActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 129
    :cond_4
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->SERVER_DATA:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_5

    .line 130
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$activity:Lcom/flixster/android/activity/DialogActivity;

    const v1, 0x3b9acd85

    invoke-virtual {v0, v1, v3}, Lcom/flixster/android/activity/DialogActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 131
    :cond_5
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->SERVER_API:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_6

    .line 132
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$activity:Lcom/flixster/android/activity/DialogActivity;

    const v1, 0x3b9acd86

    invoke-virtual {v0, v1, v3}, Lcom/flixster/android/activity/DialogActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 133
    :cond_6
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->SERVER_ERROR_MSG:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_7

    .line 134
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v2}, Lnet/flixster/android/data/DaoException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 135
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$activity:Lcom/flixster/android/activity/DialogActivity;

    invoke-virtual {v0, v4, v3}, Lcom/flixster/android/activity/DialogActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 136
    :cond_7
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_8

    .line 137
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$activity:Lcom/flixster/android/activity/DialogActivity;

    const v1, 0x3b9acd88

    invoke-virtual {v0, v1, v3}, Lcom/flixster/android/activity/DialogActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto/16 :goto_0

    .line 138
    :cond_8
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->STREAM_CREATE:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_9

    .line 139
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v2}, Lnet/flixster/android/data/DaoException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$activity:Lcom/flixster/android/activity/DialogActivity;

    invoke-virtual {v0, v5, v3}, Lcom/flixster/android/activity/DialogActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto/16 :goto_0

    .line 141
    :cond_9
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->UNKNOWN:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$4;->val$activity:Lcom/flixster/android/activity/DialogActivity;

    const v1, 0x3b9acd8b

    invoke-virtual {v0, v1, v3}, Lcom/flixster/android/activity/DialogActivity;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto/16 :goto_0
.end method
