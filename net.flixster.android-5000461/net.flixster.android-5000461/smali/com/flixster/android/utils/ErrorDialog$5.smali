.class Lcom/flixster/android/utils/ErrorDialog$5;
.super Ljava/lang/Object;
.source "ErrorDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$activity:Landroid/app/Activity;

.field private final synthetic val$de:Lnet/flixster/android/data/DaoException;


# direct methods
.method constructor <init>(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    iput-object p2, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const v4, 0x3b9acd89

    const v3, 0x3b9acd87

    .line 161
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->NOT_LICENSED:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 162
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    const v1, 0x3b9acd8a

    invoke-static {v0, v1}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    #calls: Lcom/flixster/android/utils/ErrorDialog;->showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->access$0(Landroid/app/Dialog;Landroid/app/Activity;)V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->NETWORK:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_3

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    const v1, 0x3b9acd84

    invoke-static {v0, v1}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    #calls: Lcom/flixster/android/utils/ErrorDialog;->showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->access$0(Landroid/app/Dialog;Landroid/app/Activity;)V

    goto :goto_0

    .line 165
    :cond_3
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->NETWORK_UNSTABLE:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_4

    .line 166
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    const v1, 0x3b9acd8c

    invoke-static {v0, v1}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    .line 167
    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    .line 166
    #calls: Lcom/flixster/android/utils/ErrorDialog;->showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->access$0(Landroid/app/Dialog;Landroid/app/Activity;)V

    goto :goto_0

    .line 168
    :cond_4
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->SERVER_DATA:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_5

    .line 169
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    const v1, 0x3b9acd85

    invoke-static {v0, v1}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    #calls: Lcom/flixster/android/utils/ErrorDialog;->showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->access$0(Landroid/app/Dialog;Landroid/app/Activity;)V

    goto :goto_0

    .line 170
    :cond_5
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->SERVER_API:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_6

    .line 171
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    const v1, 0x3b9acd86

    invoke-static {v0, v1}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    #calls: Lcom/flixster/android/utils/ErrorDialog;->showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->access$0(Landroid/app/Dialog;Landroid/app/Activity;)V

    goto :goto_0

    .line 172
    :cond_6
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->SERVER_ERROR_MSG:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_7

    .line 173
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    .line 174
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v2}, Lnet/flixster/android/data/DaoException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 175
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    invoke-static {v0, v3}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    #calls: Lcom/flixster/android/utils/ErrorDialog;->showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->access$0(Landroid/app/Dialog;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 176
    :cond_7
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_8

    .line 177
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    const v1, 0x3b9acd88

    invoke-static {v0, v1}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    #calls: Lcom/flixster/android/utils/ErrorDialog;->showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->access$0(Landroid/app/Dialog;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 178
    :cond_8
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->STREAM_CREATE:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_9

    .line 179
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 180
    iget-object v2, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v2}, Lnet/flixster/android/data/DaoException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 179
    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 181
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    invoke-static {v0, v4}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    .line 181
    #calls: Lcom/flixster/android/utils/ErrorDialog;->showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->access$0(Landroid/app/Dialog;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 183
    :cond_9
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->UNKNOWN:Lnet/flixster/android/data/DaoException$Type;

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$de:Lnet/flixster/android/data/DaoException;

    invoke-virtual {v1}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    const v1, 0x3b9acd8b

    invoke-static {v0, v1}, Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/utils/ErrorDialog$5;->val$activity:Landroid/app/Activity;

    #calls: Lcom/flixster/android/utils/ErrorDialog;->showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->access$0(Landroid/app/Dialog;Landroid/app/Activity;)V

    goto/16 :goto_0
.end method
