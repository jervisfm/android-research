.class public Lcom/flixster/android/utils/ErrorDialog;
.super Ljava/lang/Object;
.source "ErrorDialog.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Landroid/app/Dialog;Landroid/app/Activity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 193
    invoke-static {p0, p1}, Lcom/flixster/android/utils/ErrorDialog;->showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V

    return-void
.end method

.method public static handleException(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;I)V
    .locals 1
    .parameter "de"
    .parameter "activity"
    .parameter "dummy"

    .prologue
    .line 154
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    instance-of v0, p1, Lcom/flixster/android/activity/DialogActivity;

    if-eqz v0, :cond_1

    .line 156
    check-cast p1, Lcom/flixster/android/activity/DialogActivity;

    .end local p1
    invoke-static {p0, p1}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/DialogActivity;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 158
    .restart local p1
    :cond_1
    new-instance v0, Lcom/flixster/android/utils/ErrorDialog$5;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/utils/ErrorDialog$5;-><init>(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static handleException(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;Lcom/flixster/android/activity/decorator/DialogDecorator;)V
    .locals 1
    .parameter "de"
    .parameter "activity"
    .parameter "decorator"

    .prologue
    .line 23
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/flixster/android/utils/ErrorDialog$1;

    invoke-direct {v0, p0, p2}, Lcom/flixster/android/utils/ErrorDialog$1;-><init>(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/decorator/DialogDecorator;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 51
    :cond_0
    return-void
.end method

.method public static handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/DialogActivity;)V
    .locals 1
    .parameter "de"
    .parameter "activity"

    .prologue
    .line 119
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/flixster/android/activity/DialogActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    new-instance v0, Lcom/flixster/android/utils/ErrorDialog$4;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/utils/ErrorDialog$4;-><init>(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/DialogActivity;)V

    invoke-virtual {p1, v0}, Lcom/flixster/android/activity/DialogActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 147
    :cond_0
    return-void
.end method

.method public static handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedActivity;)V
    .locals 1
    .parameter "de"
    .parameter "activity"

    .prologue
    .line 55
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/flixster/android/activity/common/DecoratedActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/flixster/android/utils/ErrorDialog$2;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/utils/ErrorDialog$2;-><init>(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedActivity;)V

    invoke-virtual {p1, v0}, Lcom/flixster/android/activity/common/DecoratedActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 83
    :cond_0
    return-void
.end method

.method public static handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedSherlockActivity;)V
    .locals 1
    .parameter "de"
    .parameter "activity"

    .prologue
    .line 87
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Lcom/flixster/android/utils/ErrorDialog$3;

    invoke-direct {v0, p0, p1}, Lcom/flixster/android/utils/ErrorDialog$3;-><init>(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedSherlockActivity;)V

    invoke-virtual {p1, v0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 115
    :cond_0
    return-void
.end method

.method private static showDialog(Landroid/app/Dialog;Landroid/app/Activity;)V
    .locals 1
    .parameter "dialog"
    .parameter "activity"

    .prologue
    .line 194
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Lcom/flixster/android/utils/ErrorDialog$6;

    invoke-direct {v0, p1, p0}, Lcom/flixster/android/utils/ErrorDialog$6;-><init>(Landroid/app/Activity;Landroid/app/Dialog;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 204
    :cond_0
    return-void
.end method
