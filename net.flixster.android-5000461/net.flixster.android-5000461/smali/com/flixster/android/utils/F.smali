.class public Lcom/flixster/android/utils/F;
.super Ljava/lang/Object;
.source "F.java"


# static fields
.field public static final ADMIN_UNLOCK_IOS:Ljava/lang/String; = "UUDDLRLRBA"

.field public static final ADMIN_UNLOCK_LONG:Ljava/lang/String; = "REINDEER FLOTILLA"

.field public static final ADMIN_UNLOCK_SHORT:Ljava/lang/String; = "FLX"

.field public static final AD_FETCH_INTERVAL_MS:J = 0x1b7740L

.field public static final AD_FREE_PERIOD_MS:J = 0xa4cb800L

#the value of this static final field might be set in the static constructor
.field public static final API_LEVEL:I = 0x0

.field public static final DAY_MS:J = 0x5265c00L

.field public static final DIAGNOSTIC_UNLOCK:Ljava/lang/String; = "DGNS"

.field public static final EXTRA_ID:Ljava/lang/String; = "net.flixster.android.EXTRA_ID"

.field public static final EXTRA_MOVIE_ID:Ljava/lang/String; = "net.flixster.android.EXTRA_MOVIE_ID"

.field public static final EXTRA_QUERY:Ljava/lang/String; = "net.flixster.android.EXTRA_QUERY"

.field public static final EXTRA_RIGHT_ID:Ljava/lang/String; = "net.flixster.android.EXTRA_RIGHT_ID"

.field public static final EXTRA_THEATER_ID:Ljava/lang/String; = "net.flixster.android.EXTRA_THEATER_ID"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "net.flixster.android.EXTRA_TITLE"

.field public static final IS_ADS_ENABLED:Z = true

.field public static final IS_AMAZON_BUILD:Z = false

#the value of this static final field might be set in the static constructor
.field public static final IS_API_LEVEL_HONEYCOMB:Z = false

.field public static final IS_DEV_BUILD:Z = false

#the value of this static final field might be set in the static constructor
.field public static final IS_NATIVE_HC_DRM_ENABLED:Z = false

.field public static final IS_NOOK_BUILD:Z = false

.field public static final IS_THIRD_PARTY_BUILD:Z = false

.field public static final IS_VODAFONE_BUILD:Z = false

.field public static final MANUFACTURER:Ljava/lang/String; = null

.field public static final MINUTE_MS:J = 0xea60L

.field public static final MODEL:Ljava/lang/String; = null

.field public static final MSK_PROMPT_DURATION:J = 0x1a90L

.field public static final PACKAGE:Ljava/lang/String; = "net.flixster.android"

.field public static final SPLASH_DURATION:J = 0x708L

.field public static final TAG:Ljava/lang/String; = "FlxMain"

.field public static final TAG_AD:Ljava/lang/String; = "FlxAd"

.field public static final TAG_API:Ljava/lang/String; = "FlxApi"

.field public static final TAG_DRM:Ljava/lang/String; = "FlxDrm"

.field public static final TAG_GA:Ljava/lang/String; = "FlxGa"

.field public static final TAG_NETFLIX:Ljava/lang/String; = "FlxNf"

.field public static final TAG_WIDGET:Ljava/lang/String; = "FlxWdgt"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    .line 13
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_1

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/flixster/android/utils/F;->IS_API_LEVEL_HONEYCOMB:Z

    .line 14
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/flixster/android/utils/F;->MANUFACTURER:Ljava/lang/String;

    .line 15
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sput-object v0, Lcom/flixster/android/utils/F;->MODEL:Ljava/lang/String;

    .line 20
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v3, 0xd

    if-lt v0, v3, :cond_2

    .line 21
    sget-object v0, Lcom/flixster/android/utils/F;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "acer"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/flixster/android/utils/F;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "htc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/flixster/android/utils/F;->MODEL:Ljava/lang/String;

    const-string v3, "LG-MS770"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22
    sget-object v0, Lcom/flixster/android/utils/F;->MODEL:Ljava/lang/String;

    const-string v3, "LG-P930"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/flixster/android/utils/F;->MODEL:Ljava/lang/String;

    const-string v3, "XT907"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/flixster/android/utils/F;->MODEL:Ljava/lang/String;

    const-string v3, "RAZR HD"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23
    sget-object v0, Lcom/flixster/android/utils/F;->MODEL:Ljava/lang/String;

    const-string v3, "RAZR MAXX"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/flixster/android/utils/F;->MODEL:Ljava/lang/String;

    const-string v3, "N861"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/flixster/android/utils/F;->MODEL:Ljava/lang/String;

    const-string v3, "LG-E970"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/flixster/android/utils/F;->MODEL:Ljava/lang/String;

    .line 24
    const-string v3, "LG-LS970"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20
    :cond_0
    :goto_1
    sput-boolean v1, Lcom/flixster/android/utils/F;->IS_NATIVE_HC_DRM_ENABLED:Z

    .line 8
    return-void

    :cond_1
    move v0, v2

    .line 13
    goto :goto_0

    :cond_2
    move v1, v2

    .line 24
    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
