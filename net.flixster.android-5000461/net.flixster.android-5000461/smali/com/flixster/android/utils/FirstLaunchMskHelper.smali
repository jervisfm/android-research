.class public Lcom/flixster/android/utils/FirstLaunchMskHelper;
.super Ljava/lang/Object;
.source "FirstLaunchMskHelper.java"


# static fields
.field private static final LAUNCHES_UNTIL_SHOWN:I = 0xa

.field private static final MAX_SHOWN_COUNT:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static appLaunched()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 16
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 17
    .local v0, context:Landroid/content/Context;
    const-string v4, "firstLaunchMsk"

    invoke-virtual {v0, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 18
    .local v3, prefs:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 19
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "shown_count"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x3

    if-lt v4, v5, :cond_0

    .line 26
    :goto_0
    return-void

    .line 22
    :cond_0
    const-string v4, "launch_count"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    add-int/lit8 v2, v4, 0x1

    .line 23
    .local v2, launch_count:I
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FirstLaunchMskHelper.appLaunched count: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const-string v4, "launch_count"

    invoke-interface {v1, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 25
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static shouldShow()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 29
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 30
    .local v0, context:Landroid/content/Context;
    const-string v8, "firstLaunchMsk"

    invoke-virtual {v0, v8, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 31
    .local v3, prefs:Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/flixster/android/utils/Properties;->getVisitType()Ljava/lang/String;

    move-result-object v5

    .line 32
    .local v5, visitType:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v4

    .line 33
    .local v4, user:Lnet/flixster/android/model/User;
    sget-object v8, Lcom/flixster/android/utils/Properties$VisitType;->INSTALL:Lcom/flixster/android/utils/Properties$VisitType;

    invoke-virtual {v8}, Lcom/flixster/android/utils/Properties$VisitType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 34
    sget-object v8, Lcom/flixster/android/utils/Properties$VisitType;->UPGRADE:Lcom/flixster/android/utils/Properties$VisitType;

    invoke-virtual {v8}, Lcom/flixster/android/utils/Properties$VisitType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    if-eqz v4, :cond_0

    iget-boolean v8, v4, Lnet/flixster/android/model/User;->isMskEligible:Z

    if-eqz v8, :cond_2

    .line 35
    :cond_0
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 36
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    const-string v8, "launch_count"

    invoke-interface {v1, v8, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 37
    const-string v8, "shown_count"

    invoke-interface {v1, v8, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 38
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move v6, v7

    .line 49
    .end local v1           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_1
    :goto_0
    return v6

    .line 42
    :cond_2
    const-string v8, "shown_count"

    invoke-interface {v3, v8, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    const/4 v9, 0x3

    if-ge v8, v9, :cond_1

    .line 45
    const-string v8, "launch_count"

    invoke-interface {v3, v8, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 46
    .local v2, launch_count:I
    const/16 v8, 0xa

    if-lt v2, v8, :cond_1

    move v6, v7

    .line 49
    goto :goto_0
.end method

.method public static shown()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 53
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 54
    .local v0, context:Landroid/content/Context;
    const-string v4, "firstLaunchMsk"

    invoke-virtual {v0, v4, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 55
    .local v2, prefs:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 56
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "shown_count"

    invoke-interface {v2, v4, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    add-int/lit8 v3, v4, 0x1

    .line 57
    .local v3, shown_count:I
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FirstLaunchMskHelper.shown count: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v4, "launch_count"

    invoke-interface {v1, v4, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 59
    const-string v4, "shown_count"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 60
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 61
    return-void
.end method
