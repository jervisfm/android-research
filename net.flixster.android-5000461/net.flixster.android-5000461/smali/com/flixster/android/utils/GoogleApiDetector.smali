.class public Lcom/flixster/android/utils/GoogleApiDetector;
.super Ljava/lang/Object;
.source "GoogleApiDetector.java"


# static fields
.field private static INSTANCE:Lcom/flixster/android/utils/GoogleApiDetector;


# instance fields
.field private isVanillaAndroid:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    new-instance v0, Lcom/flixster/android/utils/GoogleApiDetector;

    invoke-direct {v0}, Lcom/flixster/android/utils/GoogleApiDetector;-><init>()V

    sput-object v0, Lcom/flixster/android/utils/GoogleApiDetector;->INSTANCE:Lcom/flixster/android/utils/GoogleApiDetector;

    .line 3
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method

.method public static instance()Lcom/flixster/android/utils/GoogleApiDetector;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/flixster/android/utils/GoogleApiDetector;->INSTANCE:Lcom/flixster/android/utils/GoogleApiDetector;

    return-object v0
.end method


# virtual methods
.method public isVanillaAndroid()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 17
    iget-object v1, p0, Lcom/flixster/android/utils/GoogleApiDetector;->isVanillaAndroid:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 19
    :try_start_0
    const-string v1, "com.google.android.maps.MapActivity"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 20
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/utils/GoogleApiDetector;->isVanillaAndroid:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 29
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/flixster/android/utils/GoogleApiDetector;->isVanillaAndroid:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    return v1

    .line 21
    :catch_0
    move-exception v0

    .line 22
    .local v0, e:Ljava/lang/ClassNotFoundException;
    const-string v1, "FlxMain"

    const-string v2, "GoogleApiDetector.isVanillaAndroid true"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 23
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/utils/GoogleApiDetector;->isVanillaAndroid:Ljava/lang/Boolean;

    goto :goto_0

    .line 24
    .end local v0           #e:Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 25
    .local v0, e:Ljava/lang/Error;
    const-string v1, "FlxMain"

    const-string v2, "GoogleApiDetector.isVanillaAndroid true"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 26
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/flixster/android/utils/GoogleApiDetector;->isVanillaAndroid:Ljava/lang/Boolean;

    goto :goto_0
.end method
