.class Lcom/flixster/android/utils/ImageGetter$ImageRequestConsumer;
.super Lcom/flixster/android/utils/ImageGetter$RequestConsumer;
.source "ImageGetter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/ImageGetter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImageRequestConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/flixster/android/utils/ImageGetter$RequestConsumer",
        "<",
        "Lcom/flixster/android/utils/ImageGetter$ImageRequest;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Ljava/util/Queue;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/flixster/android/utils/ImageGetter$ImageRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, q:Ljava/util/Queue;,"Ljava/util/Queue<Lcom/flixster/android/utils/ImageGetter$ImageRequest;>;"
    invoke-direct {p0, p1}, Lcom/flixster/android/utils/ImageGetter$RequestConsumer;-><init>(Ljava/util/Queue;)V

    .line 87
    return-void
.end method


# virtual methods
.method protected consume(Lcom/flixster/android/utils/ImageGetter$ImageRequest;)V
    .locals 6
    .parameter "request"

    .prologue
    .line 93
    :try_start_0
    new-instance v2, Ljava/net/URL;

    iget-object v3, p1, Lcom/flixster/android/utils/ImageGetter$ImageRequest;->url:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .local v2, url:Ljava/net/URL;
    :try_start_1
    iget-object v3, p1, Lcom/flixster/android/utils/ImageGetter$ImageRequest;->handler:Landroid/os/Handler;

    if-nez v3, :cond_0

    .line 100
    invoke-static {v2}, Lnet/flixster/android/util/HttpImageHelper;->fetchImageDoNotCache(Ljava/net/URL;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 108
    .end local v2           #url:Ljava/net/URL;
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, e:Ljava/net/MalformedURLException;
    const-string v3, "FlxMain"

    const-string v4, "ImageRequestConsumer.consume"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 102
    .end local v0           #e:Ljava/net/MalformedURLException;
    .restart local v2       #url:Ljava/net/URL;
    :cond_0
    :try_start_2
    invoke-static {v2}, Lnet/flixster/android/util/HttpImageHelper;->fetchImage(Ljava/net/URL;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 103
    .local v1, image:Landroid/graphics/Bitmap;
    iget-object v3, p1, Lcom/flixster/android/utils/ImageGetter$ImageRequest;->handler:Landroid/os/Handler;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v5, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 105
    .end local v1           #image:Landroid/graphics/Bitmap;
    :catch_1
    move-exception v0

    .line 106
    .local v0, e:Ljava/io/IOException;
    const-string v3, "FlxMain"

    const-string v4, "ImageRequestConsumer.consume"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic consume(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Lcom/flixster/android/utils/ImageGetter$ImageRequest;

    invoke-virtual {p0, p1}, Lcom/flixster/android/utils/ImageGetter$ImageRequestConsumer;->consume(Lcom/flixster/android/utils/ImageGetter$ImageRequest;)V

    return-void
.end method
