.class abstract Lcom/flixster/android/utils/ImageGetter$RequestConsumer;
.super Ljava/lang/Object;
.source "ImageGetter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/ImageGetter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "RequestConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final queue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/util/Queue;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p0, this:Lcom/flixster/android/utils/ImageGetter$RequestConsumer;,"Lcom/flixster/android/utils/ImageGetter$RequestConsumer<TT;>;"
    .local p1, q:Ljava/util/Queue;,"Ljava/util/Queue<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/flixster/android/utils/ImageGetter$RequestConsumer;->queue:Ljava/util/Queue;

    .line 59
    return-void
.end method


# virtual methods
.method protected abstract consume(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public run()V
    .locals 5

    .prologue
    .line 65
    .local p0, this:Lcom/flixster/android/utils/ImageGetter$RequestConsumer;,"Lcom/flixster/android/utils/ImageGetter$RequestConsumer<TT;>;"
    :goto_0
    const/4 v1, 0x0

    .line 66
    .local v1, object:Ljava/lang/Object;,"TT;"
    :try_start_0
    iget-object v3, p0, Lcom/flixster/android/utils/ImageGetter$RequestConsumer;->queue:Ljava/util/Queue;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :try_start_1
    iget-object v2, p0, Lcom/flixster/android/utils/ImageGetter$RequestConsumer;->queue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 68
    const-string v2, "FlxMain"

    const-string v4, "RequestConsumer waiting"

    invoke-static {v2, v4}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v2, p0, Lcom/flixster/android/utils/ImageGetter$RequestConsumer;->queue:Ljava/util/Queue;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V

    .line 71
    :cond_0
    iget-object v2, p0, Lcom/flixster/android/utils/ImageGetter$RequestConsumer;->queue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    .line 66
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    :try_start_2
    const-string v2, "FlxMain"

    const-string v3, "RequestConsumer consuming"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0, v1}, Lcom/flixster/android/utils/ImageGetter$RequestConsumer;->consume(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 75
    .end local v1           #object:Ljava/lang/Object;,"TT;"
    :catch_0
    move-exception v0

    .line 76
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v2, "FlxMain"

    const-string v3, "RequestConsumer.run"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 66
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
.end method
