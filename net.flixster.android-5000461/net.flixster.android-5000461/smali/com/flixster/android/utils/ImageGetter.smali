.class public Lcom/flixster/android/utils/ImageGetter;
.super Ljava/lang/Object;
.source "ImageGetter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/utils/ImageGetter$ImageRequest;,
        Lcom/flixster/android/utils/ImageGetter$ImageRequestConsumer;,
        Lcom/flixster/android/utils/ImageGetter$RequestConsumer;
    }
.end annotation


# static fields
.field private static final INSTANCE:Lcom/flixster/android/utils/ImageGetter;


# instance fields
.field private final imageQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/flixster/android/utils/ImageGetter$ImageRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/flixster/android/utils/ImageGetter;

    invoke-direct {v0}, Lcom/flixster/android/utils/ImageGetter;-><init>()V

    sput-object v0, Lcom/flixster/android/utils/ImageGetter;->INSTANCE:Lcom/flixster/android/utils/ImageGetter;

    .line 20
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/utils/ImageGetter;->imageQueue:Ljava/util/Queue;

    .line 27
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/flixster/android/utils/ImageGetter$ImageRequestConsumer;

    iget-object v2, p0, Lcom/flixster/android/utils/ImageGetter;->imageQueue:Ljava/util/Queue;

    invoke-direct {v1, v2}, Lcom/flixster/android/utils/ImageGetter$ImageRequestConsumer;-><init>(Ljava/util/Queue;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 28
    return-void
.end method

.method public static instance()Lcom/flixster/android/utils/ImageGetter;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/flixster/android/utils/ImageGetter;->INSTANCE:Lcom/flixster/android/utils/ImageGetter;

    return-object v0
.end method


# virtual methods
.method public get(Ljava/lang/String;Landroid/os/Handler;)V
    .locals 4
    .parameter "url"
    .parameter "handler"

    .prologue
    .line 35
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ImageGetter requesting: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    iget-object v1, p0, Lcom/flixster/android/utils/ImageGetter;->imageQueue:Ljava/util/Queue;

    monitor-enter v1

    .line 37
    :try_start_0
    iget-object v0, p0, Lcom/flixster/android/utils/ImageGetter;->imageQueue:Ljava/util/Queue;

    new-instance v2, Lcom/flixster/android/utils/ImageGetter$ImageRequest;

    const/4 v3, 0x0

    invoke-direct {v2, p1, p2, v3}, Lcom/flixster/android/utils/ImageGetter$ImageRequest;-><init>(Ljava/lang/String;Landroid/os/Handler;Lcom/flixster/android/utils/ImageGetter$ImageRequest;)V

    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 38
    iget-object v0, p0, Lcom/flixster/android/utils/ImageGetter;->imageQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 39
    iget-object v0, p0, Lcom/flixster/android/utils/ImageGetter;->imageQueue:Ljava/util/Queue;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 36
    :cond_0
    monitor-exit v1

    .line 42
    return-void

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
