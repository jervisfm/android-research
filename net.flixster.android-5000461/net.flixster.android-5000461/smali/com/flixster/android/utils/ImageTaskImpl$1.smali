.class Lcom/flixster/android/utils/ImageTaskImpl$1;
.super Ljava/util/TimerTask;
.source "ImageTaskImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/utils/ImageTaskImpl;->startTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/utils/ImageTaskImpl;


# direct methods
.method constructor <init>(Lcom/flixster/android/utils/ImageTaskImpl;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/utils/ImageTaskImpl$1;->this$0:Lcom/flixster/android/utils/ImageTaskImpl;

    .line 57
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 59
    :goto_0
    iget-object v8, p0, Lcom/flixster/android/utils/ImageTaskImpl$1;->this$0:Lcom/flixster/android/utils/ImageTaskImpl;

    #getter for: Lcom/flixster/android/utils/ImageTaskImpl;->mRunImageTask:Z
    invoke-static {v8}, Lcom/flixster/android/utils/ImageTaskImpl;->access$0(Lcom/flixster/android/utils/ImageTaskImpl;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 114
    return-void

    .line 64
    :cond_0
    :try_start_0
    iget-object v8, p0, Lcom/flixster/android/utils/ImageTaskImpl$1;->this$0:Lcom/flixster/android/utils/ImageTaskImpl;

    #getter for: Lcom/flixster/android/utils/ImageTaskImpl;->mImageOrderQueue:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/flixster/android/utils/ImageTaskImpl;->access$1(Lcom/flixster/android/utils/ImageTaskImpl;)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/ImageOrder;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 65
    .local v5, imageOrder:Lnet/flixster/android/model/ImageOrder;
    if-nez v5, :cond_1

    .line 107
    .end local v5           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :goto_1
    const-wide/16 v8, 0x3e8

    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 108
    :catch_0
    move-exception v2

    .line 109
    .local v2, e:Ljava/lang/InterruptedException;
    const-string v8, "FlxMain"

    const-string v9, "sleep exception"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 68
    .end local v2           #e:Ljava/lang/InterruptedException;
    :catch_1
    move-exception v2

    .line 69
    .local v2, e:Ljava/lang/IndexOutOfBoundsException;
    goto :goto_1

    .line 71
    .end local v2           #e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v5       #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_1
    :try_start_2
    new-instance v8, Ljava/net/URL;

    iget-object v9, v5, Lnet/flixster/android/model/ImageOrder;->urlString:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Lnet/flixster/android/util/HttpImageHelper;->fetchImage(Ljava/net/URL;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 72
    .local v1, bitmap:Landroid/graphics/Bitmap;
    iget-object v8, v5, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    if-eqz v8, :cond_2

    .line 73
    iget v8, v5, Lnet/flixster/android/model/ImageOrder;->type:I

    packed-switch v8, :pswitch_data_0

    .line 101
    :goto_2
    :pswitch_0
    iget-object v8, v5, Lnet/flixster/android/model/ImageOrder;->refreshHandler:Landroid/os/Handler;

    if-eqz v8, :cond_2

    iget-object v8, v5, Lnet/flixster/android/model/ImageOrder;->movieView:Landroid/view/View;

    if-eqz v8, :cond_2

    .line 102
    iget-object v8, v5, Lnet/flixster/android/model/ImageOrder;->refreshHandler:Landroid/os/Handler;

    iget-object v9, v5, Lnet/flixster/android/model/ImageOrder;->refreshHandler:Landroid/os/Handler;

    const/4 v10, 0x0

    .line 103
    iget-object v11, v5, Lnet/flixster/android/model/ImageOrder;->movieView:Landroid/view/View;

    .line 102
    invoke-static {v9, v10, v11}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 61
    .end local v1           #bitmap:Landroid/graphics/Bitmap;
    .end local v5           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_2
    iget-object v8, p0, Lcom/flixster/android/utils/ImageTaskImpl$1;->this$0:Lcom/flixster/android/utils/ImageTaskImpl;

    #getter for: Lcom/flixster/android/utils/ImageTaskImpl;->mImageOrderQueue:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/flixster/android/utils/ImageTaskImpl;->access$1(Lcom/flixster/android/utils/ImageTaskImpl;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    goto :goto_1

    .line 75
    .restart local v1       #bitmap:Landroid/graphics/Bitmap;
    .restart local v5       #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :pswitch_1
    iget-object v8, v5, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    check-cast v8, Lnet/flixster/android/model/Movie;

    new-instance v9, Ljava/lang/ref/SoftReference;

    invoke-direct {v9, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v9, v8, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 110
    .end local v1           #bitmap:Landroid/graphics/Bitmap;
    .end local v5           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :catch_2
    move-exception v4

    .line 111
    .local v4, ie:Ljava/io/IOException;
    const-string v8, "FlxMain"

    const-string v9, "Failed to download a movie poster"

    invoke-static {v8, v9, v4}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 78
    .end local v4           #ie:Ljava/io/IOException;
    .restart local v1       #bitmap:Landroid/graphics/Bitmap;
    .restart local v5       #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :pswitch_2
    :try_start_3
    iget-object v7, v5, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    check-cast v7, Lnet/flixster/android/model/Photo;

    .line 79
    .local v7, photo:Lnet/flixster/android/model/Photo;
    iput-object v1, v7, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 82
    .end local v7           #photo:Lnet/flixster/android/model/Photo;
    :pswitch_3
    iget-object v3, v5, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    check-cast v3, Lnet/flixster/android/model/Photo;

    .line 83
    .local v3, galleryPhoto:Lnet/flixster/android/model/Photo;
    new-instance v8, Ljava/lang/ref/SoftReference;

    invoke-direct {v8, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v8, v3, Lnet/flixster/android/model/Photo;->galleryBitmap:Ljava/lang/ref/SoftReference;

    goto :goto_2

    .line 86
    .end local v3           #galleryPhoto:Lnet/flixster/android/model/Photo;
    :pswitch_4
    iget-object v6, v5, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    check-cast v6, Lnet/flixster/android/model/NewsStory;

    .line 87
    .local v6, ns:Lnet/flixster/android/model/NewsStory;
    new-instance v8, Ljava/lang/ref/SoftReference;

    invoke-direct {v8, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v8, v6, Lnet/flixster/android/model/NewsStory;->mSoftPhoto:Ljava/lang/ref/SoftReference;

    goto :goto_2

    .line 90
    .end local v6           #ns:Lnet/flixster/android/model/NewsStory;
    :pswitch_5
    const-string v8, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "FlixsterActivity mImageOrderQueue url:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v5, Lnet/flixster/android/model/ImageOrder;->urlString:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :pswitch_6
    iget-object v0, v5, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 95
    .local v0, actor:Lnet/flixster/android/model/Actor;
    iput-object v1, v0, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 98
    .end local v0           #actor:Lnet/flixster/android/model/Actor;
    :pswitch_7
    iget-object v8, v5, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    check-cast v8, Lnet/flixster/android/model/NetflixQueueItem;

    iput-object v1, v8, Lnet/flixster/android/model/NetflixQueueItem;->thumbnailBitmap:Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 73
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_7
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method
