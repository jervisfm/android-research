.class public Lcom/flixster/android/utils/ImageTaskImpl;
.super Ljava/lang/Object;
.source "ImageTaskImpl.java"

# interfaces
.implements Lcom/flixster/android/utils/ImageTask;


# static fields
.field private static instance:Lcom/flixster/android/utils/ImageTaskImpl;


# instance fields
.field private final mImageOrderQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/ImageOrder;",
            ">;"
        }
    .end annotation
.end field

.field private mImageTimer:Ljava/util/Timer;

.field private volatile mRunImageTask:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageOrderQueue:Ljava/util/ArrayList;

    .line 33
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/utils/ImageTaskImpl;)Z
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mRunImageTask:Z

    return v0
.end method

.method static synthetic access$1(Lcom/flixster/android/utils/ImageTaskImpl;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageOrderQueue:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static instance()Lcom/flixster/android/utils/ImageTaskImpl;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/flixster/android/utils/ImageTaskImpl;->instance:Lcom/flixster/android/utils/ImageTaskImpl;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-direct {v0}, Lcom/flixster/android/utils/ImageTaskImpl;-><init>()V

    sput-object v0, Lcom/flixster/android/utils/ImageTaskImpl;->instance:Lcom/flixster/android/utils/ImageTaskImpl;

    .line 40
    :cond_0
    sget-object v0, Lcom/flixster/android/utils/ImageTaskImpl;->instance:Lcom/flixster/android/utils/ImageTaskImpl;

    return-object v0
.end method


# virtual methods
.method public isRunning()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mRunImageTask:Z

    return v0
.end method

.method public orderImage(Lnet/flixster/android/model/ImageOrder;)V
    .locals 0
    .parameter "io"

    .prologue
    .line 134
    invoke-virtual {p0, p1}, Lcom/flixster/android/utils/ImageTaskImpl;->orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 135
    return-void
.end method

.method public orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V
    .locals 2
    .parameter "io"

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mRunImageTask:Z

    if-nez v0, :cond_0

    .line 152
    const-string v0, "FlxMain"

    const-string v1, "Illegal state: ImageTask has not started"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageOrderQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V
    .locals 2
    .parameter "io"

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mRunImageTask:Z

    if-nez v0, :cond_0

    .line 141
    const-string v0, "FlxMain"

    const-string v1, "Illegal state: ImageTask has not started"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageOrderQueue:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public declared-synchronized startTask()V
    .locals 4

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mRunImageTask:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 120
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 52
    :cond_1
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mRunImageTask:Z

    .line 53
    iget-object v1, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageTimer:Ljava/util/Timer;

    if-nez v1, :cond_2

    .line 54
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageTimer:Ljava/util/Timer;

    .line 57
    :cond_2
    new-instance v0, Lcom/flixster/android/utils/ImageTaskImpl$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/utils/ImageTaskImpl$1;-><init>(Lcom/flixster/android/utils/ImageTaskImpl;)V

    .line 117
    .local v0, imageTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 118
    iget-object v1, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 48
    .end local v0           #imageTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized stopTask()V
    .locals 1

    .prologue
    .line 123
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mRunImageTask:Z

    .line 124
    iget-object v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageOrderQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 125
    iget-object v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 127
    iget-object v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 129
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flixster/android/utils/ImageTaskImpl;->mImageTimer:Ljava/util/Timer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    monitor-exit p0

    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
