.class public Lcom/flixster/android/utils/LocalizedHelper;
.super Ljava/lang/Object;
.source "LocalizedHelper.java"


# instance fields
.field protected final isFrance:Z


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const-string v0, "FR"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/flixster/android/utils/LocalizedHelper;->isFrance:Z

    .line 11
    return-void
.end method
