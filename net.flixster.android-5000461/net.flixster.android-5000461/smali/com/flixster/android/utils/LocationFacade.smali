.class public Lcom/flixster/android/utils/LocationFacade;
.super Ljava/lang/Object;
.source "LocationFacade.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAvailableLatitude()D
    .locals 4

    .prologue
    .line 14
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v0

    .local v0, lat:D
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_0

    .line 15
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v0

    .line 17
    :cond_0
    return-wide v0
.end method

.method public static getAvailableLongitude()D
    .locals 4

    .prologue
    .line 22
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v0

    .local v0, lng:D
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_0

    .line 23
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v0

    .line 25
    :cond_0
    return-wide v0
.end method

.method public static getAvailableZip()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentZip()Ljava/lang/String;

    move-result-object v0

    .local v0, postal:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 31
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserZip()Ljava/lang/String;

    move-result-object v0

    .line 33
    :cond_0
    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 34
    :cond_1
    return-object v0
.end method

.method public static isAustraliaLocale()Z
    .locals 2

    .prologue
    .line 56
    const-string v0, "au"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isCanadaLocale()Z
    .locals 2

    .prologue
    .line 44
    const-string v0, "ca"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isFrenchLocale()Z
    .locals 2

    .prologue
    .line 40
    const-string v0, "fr"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isUkLocale()Z
    .locals 2

    .prologue
    .line 48
    const-string v0, "uk"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isUsLocale()Z
    .locals 2

    .prologue
    .line 52
    const-string v0, "us"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
