.class public Lcom/flixster/android/utils/MathHelper;
.super Ljava/lang/Object;
.source "MathHelper.java"


# static fields
.field private static r:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/flixster/android/utils/MathHelper;->r:Ljava/util/Random;

    .line 5
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static randomId()J
    .locals 2

    .prologue
    .line 9
    sget-object v0, Lcom/flixster/android/utils/MathHelper;->r:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    return-wide v0
.end method
