.class public final enum Lcom/flixster/android/utils/OsInfo$NetworkSpeed;
.super Ljava/lang/Enum;
.source "OsInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/OsInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NetworkSpeed"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flixster/android/utils/OsInfo$NetworkSpeed;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BASIC_LIKE_3G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

.field private static final synthetic ENUM$VALUES:[Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

.field public static final enum FASTER_LIKE_WIFI:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

.field public static final enum FAST_LIKE_4G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

.field public static final enum SLOW_LIKE_EDGE:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

.field public static final enum UNKNOWN:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    new-instance v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->UNKNOWN:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    new-instance v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    const-string v1, "SLOW_LIKE_EDGE"

    invoke-direct {v0, v1, v3}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->SLOW_LIKE_EDGE:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    new-instance v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    const-string v1, "BASIC_LIKE_3G"

    invoke-direct {v0, v1, v4}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->BASIC_LIKE_3G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    new-instance v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    const-string v1, "FAST_LIKE_4G"

    invoke-direct {v0, v1, v5}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FAST_LIKE_4G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    new-instance v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    const-string v1, "FASTER_LIKE_WIFI"

    invoke-direct {v0, v1, v6}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FASTER_LIKE_WIFI:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    .line 45
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    sget-object v1, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->UNKNOWN:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->SLOW_LIKE_EDGE:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->BASIC_LIKE_3G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    aput-object v1, v0, v4

    sget-object v1, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FAST_LIKE_4G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    aput-object v1, v0, v5

    sget-object v1, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FASTER_LIKE_WIFI:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    aput-object v1, v0, v6

    sput-object v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->ENUM$VALUES:[Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flixster/android/utils/OsInfo$NetworkSpeed;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    return-object v0
.end method

.method public static values()[Lcom/flixster/android/utils/OsInfo$NetworkSpeed;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->ENUM$VALUES:[Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    array-length v1, v0

    new-array v2, v1, [Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
