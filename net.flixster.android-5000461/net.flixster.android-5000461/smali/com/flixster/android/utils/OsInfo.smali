.class public Lcom/flixster/android/utils/OsInfo;
.super Ljava/lang/Object;
.source "OsInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/utils/OsInfo$NetworkSpeed;
    }
.end annotation


# static fields
.field private static final INSTANCE:Lcom/flixster/android/utils/OsInfo; = null

.field private static final KEY_ANDROID_ID:Ljava/lang/String; = "sai"

.field private static final KEY_DEVICE_ID:Ljava/lang/String; = "tdi"

.field private static final KEY_SERIAL:Ljava/lang/String; = "bsn"

.field private static final PREFERENCE_FILE:Ljava/lang/String; = "osinfo"


# instance fields
.field private cm:Landroid/net/ConnectivityManager;

.field private cr:Landroid/content/ContentResolver;

.field private encryptedAndroidId:Ljava/lang/String;

.field private encryptedDeviceId:Ljava/lang/String;

.field private encryptedSerialNumber:Ljava/lang/String;

.field private tm:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/flixster/android/utils/OsInfo;

    invoke-direct {v0}, Lcom/flixster/android/utils/OsInfo;-><init>()V

    sput-object v0, Lcom/flixster/android/utils/OsInfo;->INSTANCE:Lcom/flixster/android/utils/OsInfo;

    .line 13
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public static instance()Lcom/flixster/android/utils/OsInfo;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/flixster/android/utils/OsInfo;->INSTANCE:Lcom/flixster/android/utils/OsInfo;

    return-object v0
.end method

.method private static readPreference(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "key"

    .prologue
    .line 207
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 208
    .local v0, context:Landroid/content/Context;
    const-string v2, "osinfo"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 209
    .local v1, preferences:Landroid/content/SharedPreferences;
    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private retrieveAndEncryptIdentifiers()V
    .locals 5

    .prologue
    .line 170
    iget-object v3, p0, Lcom/flixster/android/utils/OsInfo;->tm:Landroid/telephony/TelephonyManager;

    invoke-static {v3}, Lcom/flixster/android/utils/VersionedUidHelper;->getTelephonyDeviceId(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v2

    .line 171
    .local v2, telephonyDeviceId:Ljava/lang/String;
    iget-object v3, p0, Lcom/flixster/android/utils/OsInfo;->cr:Landroid/content/ContentResolver;

    invoke-static {v3}, Lcom/flixster/android/utils/VersionedUidHelper;->getAndroidId(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, androidId:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/utils/VersionedUidHelper;->getSerialNumber()Ljava/lang/String;

    move-result-object v1

    .line 180
    .local v1, serialNumber:Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/flixster/android/utils/CryptHelper;->encryptEsig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/flixster/android/utils/OsInfo;->encryptedDeviceId:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 181
    const-string v3, "tdi"

    iget-object v4, p0, Lcom/flixster/android/utils/OsInfo;->encryptedDeviceId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/flixster/android/utils/OsInfo;->writePreference(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/flixster/android/utils/CryptHelper;->encryptEsig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/flixster/android/utils/OsInfo;->encryptedAndroidId:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 186
    const-string v3, "sai"

    iget-object v4, p0, Lcom/flixster/android/utils/OsInfo;->encryptedAndroidId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/flixster/android/utils/OsInfo;->writePreference(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :goto_1
    if-eqz v1, :cond_2

    invoke-static {v1}, Lcom/flixster/android/utils/CryptHelper;->encryptEsig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/flixster/android/utils/OsInfo;->encryptedSerialNumber:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 191
    const-string v3, "bsn"

    iget-object v4, p0, Lcom/flixster/android/utils/OsInfo;->encryptedSerialNumber:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/flixster/android/utils/OsInfo;->writePreference(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :goto_2
    return-void

    .line 183
    :cond_0
    const-string v3, "tdi"

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/flixster/android/utils/OsInfo;->writePreference(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 188
    :cond_1
    const-string v3, "sai"

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/flixster/android/utils/OsInfo;->writePreference(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 193
    :cond_2
    const-string v3, "bsn"

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/flixster/android/utils/OsInfo;->writePreference(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static writePreference(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "key"
    .parameter "value"

    .prologue
    .line 200
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 201
    .local v0, context:Landroid/content/Context;
    const-string v2, "osinfo"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 202
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 203
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 204
    return-void
.end method


# virtual methods
.method public getAdu()Ljava/lang/String;
    .locals 3

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/flixster/android/utils/OsInfo;->getEncryptedTelephonyDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, adu:Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 132
    sget v1, Lcom/flixster/android/utils/F;->API_LEVEL:I

    .line 133
    .local v1, sdkVersion:I
    const/16 v2, 0x8

    if-ge v1, v2, :cond_1

    .line 134
    const-string v0, ""

    .line 142
    .end local v1           #sdkVersion:I
    :cond_0
    :goto_0
    return-object v0

    .line 135
    .restart local v1       #sdkVersion:I
    :cond_1
    const/16 v2, 0xb

    if-gt v2, v1, :cond_2

    .line 136
    const/16 v2, 0xe

    if-ge v1, v2, :cond_2

    .line 137
    invoke-virtual {p0}, Lcom/flixster/android/utils/OsInfo;->getEncryptedSerialNumber()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 139
    :cond_2
    invoke-virtual {p0}, Lcom/flixster/android/utils/OsInfo;->getEncryptedAndroidId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEncryptedAndroidId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/flixster/android/utils/OsInfo;->encryptedAndroidId:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "sai"

    invoke-static {v0}, Lcom/flixster/android/utils/OsInfo;->readPreference(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/utils/OsInfo;->encryptedAndroidId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 156
    invoke-direct {p0}, Lcom/flixster/android/utils/OsInfo;->retrieveAndEncryptIdentifiers()V

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/utils/OsInfo;->encryptedAndroidId:Ljava/lang/String;

    return-object v0
.end method

.method public getEncryptedSerialNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/flixster/android/utils/OsInfo;->encryptedSerialNumber:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "bsn"

    invoke-static {v0}, Lcom/flixster/android/utils/OsInfo;->readPreference(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/utils/OsInfo;->encryptedSerialNumber:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 164
    invoke-direct {p0}, Lcom/flixster/android/utils/OsInfo;->retrieveAndEncryptIdentifiers()V

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/utils/OsInfo;->encryptedSerialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getEncryptedTelephonyDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/flixster/android/utils/OsInfo;->encryptedDeviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "tdi"

    invoke-static {v0}, Lcom/flixster/android/utils/OsInfo;->readPreference(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/utils/OsInfo;->encryptedDeviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/flixster/android/utils/OsInfo;->retrieveAndEncryptIdentifiers()V

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/utils/OsInfo;->encryptedDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSpeed()Lcom/flixster/android/utils/OsInfo$NetworkSpeed;
    .locals 6

    .prologue
    .line 51
    iget-object v3, p0, Lcom/flixster/android/utils/OsInfo;->cm:Landroid/net/ConnectivityManager;

    invoke-virtual {v3}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 52
    .local v0, info:Landroid/net/NetworkInfo;
    if-nez v0, :cond_0

    .line 53
    const-string v3, "FlxMain"

    const-string v4, "OsInfo.getNetworkSpeed unknown"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->UNKNOWN:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    .line 118
    :goto_0
    return-object v3

    .line 56
    :cond_0
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "OsInfo.getNetworkSpeed "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    .line 58
    .local v2, type:I
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v1

    .line 59
    .local v1, subType:I
    sparse-switch v2, :sswitch_data_0

    .line 118
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->UNKNOWN:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 61
    :sswitch_0
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FASTER_LIKE_WIFI:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 64
    :sswitch_1
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FAST_LIKE_4G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 67
    :sswitch_2
    packed-switch v1, :pswitch_data_0

    .line 115
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->UNKNOWN:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 69
    :pswitch_0
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->SLOW_LIKE_EDGE:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 72
    :pswitch_1
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->SLOW_LIKE_EDGE:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 75
    :pswitch_2
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->SLOW_LIKE_EDGE:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 78
    :pswitch_3
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->SLOW_LIKE_EDGE:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 81
    :pswitch_4
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->SLOW_LIKE_EDGE:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 84
    :pswitch_5
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->BASIC_LIKE_3G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 87
    :pswitch_6
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->BASIC_LIKE_3G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 90
    :pswitch_7
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->BASIC_LIKE_3G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 93
    :pswitch_8
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->BASIC_LIKE_3G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 96
    :pswitch_9
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->BASIC_LIKE_3G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 99
    :pswitch_a
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FAST_LIKE_4G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 102
    :pswitch_b
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FAST_LIKE_4G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 105
    :pswitch_c
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FAST_LIKE_4G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 108
    :pswitch_d
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FASTER_LIKE_WIFI:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 111
    :pswitch_e
    sget-object v3, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FASTER_LIKE_WIFI:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    goto :goto_0

    .line 59
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_b
        :pswitch_c
        :pswitch_7
        :pswitch_0
        :pswitch_a
        :pswitch_e
        :pswitch_9
        :pswitch_d
    .end packed-switch
.end method

.method public initialize(Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;Landroid/content/ContentResolver;)V
    .locals 0
    .parameter "cm"
    .parameter "tm"
    .parameter "cr"

    .prologue
    .line 33
    iput-object p1, p0, Lcom/flixster/android/utils/OsInfo;->cm:Landroid/net/ConnectivityManager;

    .line 34
    iput-object p2, p0, Lcom/flixster/android/utils/OsInfo;->tm:Landroid/telephony/TelephonyManager;

    .line 35
    iput-object p3, p0, Lcom/flixster/android/utils/OsInfo;->cr:Landroid/content/ContentResolver;

    .line 36
    return-void
.end method

.method public isConnected()Z
    .locals 2

    .prologue
    .line 41
    iget-object v1, p0, Lcom/flixster/android/utils/OsInfo;->cm:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 42
    .local v0, info:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
