.class Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;
.super Ljava/lang/Object;
.source "Properties.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/Properties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EclairTelephonyDetector"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;)V
    .locals 0
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;)Z
    .locals 1
    .parameter

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;->isTelephonySupported()Z

    move-result v0

    return v0
.end method

.method private isTelephonySupported()Z
    .locals 2

    .prologue
    .line 71
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 72
    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 71
    return v0
.end method
