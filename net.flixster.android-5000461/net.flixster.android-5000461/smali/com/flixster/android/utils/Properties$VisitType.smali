.class public final enum Lcom/flixster/android/utils/Properties$VisitType;
.super Ljava/lang/Enum;
.source "Properties.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/Properties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VisitType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flixster/android/utils/Properties$VisitType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/flixster/android/utils/Properties$VisitType;

.field public static final enum INSTALL:Lcom/flixster/android/utils/Properties$VisitType;

.field public static final enum SESSION:Lcom/flixster/android/utils/Properties$VisitType;

.field public static final enum UPGRADE:Lcom/flixster/android/utils/Properties$VisitType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 182
    new-instance v0, Lcom/flixster/android/utils/Properties$VisitType;

    const-string v1, "INSTALL"

    invoke-direct {v0, v1, v2}, Lcom/flixster/android/utils/Properties$VisitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/utils/Properties$VisitType;->INSTALL:Lcom/flixster/android/utils/Properties$VisitType;

    new-instance v0, Lcom/flixster/android/utils/Properties$VisitType;

    const-string v1, "UPGRADE"

    invoke-direct {v0, v1, v3}, Lcom/flixster/android/utils/Properties$VisitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/utils/Properties$VisitType;->UPGRADE:Lcom/flixster/android/utils/Properties$VisitType;

    new-instance v0, Lcom/flixster/android/utils/Properties$VisitType;

    const-string v1, "SESSION"

    invoke-direct {v0, v1, v4}, Lcom/flixster/android/utils/Properties$VisitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flixster/android/utils/Properties$VisitType;->SESSION:Lcom/flixster/android/utils/Properties$VisitType;

    .line 181
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/flixster/android/utils/Properties$VisitType;

    sget-object v1, Lcom/flixster/android/utils/Properties$VisitType;->INSTALL:Lcom/flixster/android/utils/Properties$VisitType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flixster/android/utils/Properties$VisitType;->UPGRADE:Lcom/flixster/android/utils/Properties$VisitType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flixster/android/utils/Properties$VisitType;->SESSION:Lcom/flixster/android/utils/Properties$VisitType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/flixster/android/utils/Properties$VisitType;->ENUM$VALUES:[Lcom/flixster/android/utils/Properties$VisitType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 181
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flixster/android/utils/Properties$VisitType;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/flixster/android/utils/Properties$VisitType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/utils/Properties$VisitType;

    return-object v0
.end method

.method public static values()[Lcom/flixster/android/utils/Properties$VisitType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/flixster/android/utils/Properties$VisitType;->ENUM$VALUES:[Lcom/flixster/android/utils/Properties$VisitType;

    array-length v1, v0

    new-array v2, v1, [Lcom/flixster/android/utils/Properties$VisitType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
