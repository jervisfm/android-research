.class public Lcom/flixster/android/utils/Properties;
.super Ljava/lang/Object;
.source "Properties.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;,
        Lcom/flixster/android/utils/Properties$VisitType;
    }
.end annotation


# static fields
.field private static final INSTANCE:Lcom/flixster/android/utils/Properties;


# instance fields
.field private hasUserSessionExpired:Z

.field private isAtLeastHdpi:Ljava/lang/Boolean;

.field private isGtv:Z

.field private isGtvIdentified:Z

.field private isHoneycombTablet:Z

.field private isHoneycombTabletIdentified:Z

.field private isMskFinishedDeepLinkInitiated:Z

.field private isScreenLarge:Ljava/lang/Boolean;

.field private isScreenXlarge:Ljava/lang/Boolean;

.field private property:Lnet/flixster/android/model/Property;

.field private shouldShowSplash:Z

.field private shouldUseLowBitrateForTrailer:Z

.field private visitType:Lcom/flixster/android/utils/Properties$VisitType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/flixster/android/utils/Properties;

    invoke-direct {v0}, Lcom/flixster/android/utils/Properties;-><init>()V

    sput-object v0, Lcom/flixster/android/utils/Properties;->INSTANCE:Lcom/flixster/android/utils/Properties;

    .line 16
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static instance()Lcom/flixster/android/utils/Properties;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/flixster/android/utils/Properties;->INSTANCE:Lcom/flixster/android/utils/Properties;

    return-object v0
.end method

.method private resetSplash()V
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isHoneycombTablet:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isGtv:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->shouldShowSplash:Z

    .line 117
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resetVisitType()V
    .locals 3

    .prologue
    .line 190
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v1

    const-class v2, Lcom/flixster/android/utils/Properties$VisitType;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 191
    .local v0, obj:Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/flixster/android/utils/Properties$VisitType;

    if-eqz v1, :cond_0

    .line 192
    check-cast v0, Lcom/flixster/android/utils/Properties$VisitType;

    .end local v0           #obj:Ljava/lang/Object;
    iput-object v0, p0, Lcom/flixster/android/utils/Properties;->visitType:Lcom/flixster/android/utils/Properties$VisitType;

    .line 196
    :goto_0
    return-void

    .line 194
    .restart local v0       #obj:Ljava/lang/Object;
    :cond_0
    sget-object v1, Lcom/flixster/android/utils/Properties$VisitType;->SESSION:Lcom/flixster/android/utils/Properties$VisitType;

    iput-object v1, p0, Lcom/flixster/android/utils/Properties;->visitType:Lcom/flixster/android/utils/Properties$VisitType;

    goto :goto_0
.end method


# virtual methods
.method public getProperties()Lnet/flixster/android/model/Property;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/flixster/android/utils/Properties;->property:Lnet/flixster/android/model/Property;

    return-object v0
.end method

.method public getVisitType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/flixster/android/utils/Properties;->visitType:Lcom/flixster/android/utils/Properties$VisitType;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/flixster/android/utils/Properties;->visitType:Lcom/flixster/android/utils/Properties$VisitType;

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties$VisitType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public hasUserSessionExpired()Z
    .locals 2

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->hasUserSessionExpired:Z

    .line 151
    .local v0, result:Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/flixster/android/utils/Properties;->hasUserSessionExpired:Z

    .line 152
    return v0
.end method

.method public identifyGoogleTv(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isGtvIdentified:Z

    if-eqz v0, :cond_0

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/VersionedDeviceDetector;->identifyGtv(Landroid/content/res/Resources;Landroid/content/pm/PackageManager;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isGtv:Z

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isGtvIdentified:Z

    goto :goto_0
.end method

.method public identifyHoneycombTablet(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isHoneycombTabletIdentified:Z

    if-eqz v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/utils/VersionedDeviceDetector;->identifyHcTablet(Landroid/content/res/Resources;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isHoneycombTablet:Z

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isHoneycombTabletIdentified:Z

    goto :goto_0
.end method

.method public isAtLeastHdpi()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/flixster/android/utils/Properties;->isAtLeastHdpi:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 80
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/utils/VersionedDeviceDetector;->isAtLeastHdpi(Landroid/content/res/Resources;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/utils/Properties;->isAtLeastHdpi:Ljava/lang/Boolean;

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/utils/Properties;->isAtLeastHdpi:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isGoogleTv()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isGtv:Z

    return v0
.end method

.method public isHoneycombTablet()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isHoneycombTablet:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isGtv:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMskEnabled()Z
    .locals 1

    .prologue
    .line 143
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isAdminEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isMskEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    :cond_0
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v0

    invoke-interface {v0}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceStreamingCompatible()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    .line 143
    goto :goto_0
.end method

.method public isMskFinishedDeepLinkInitiated()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isMskFinishedDeepLinkInitiated:Z

    return v0
.end method

.method public isScreenLarge()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/flixster/android/utils/Properties;->isScreenLarge:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 94
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/utils/VersionedDeviceDetector;->isScreenLarge(Landroid/content/res/Resources;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/utils/Properties;->isScreenLarge:Ljava/lang/Boolean;

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/utils/Properties;->isScreenLarge:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isScreenXlarge()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/flixster/android/utils/Properties;->isScreenXlarge:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 87
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/utils/VersionedDeviceDetector;->isScreenXlarge(Landroid/content/res/Resources;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/utils/Properties;->isScreenXlarge:Ljava/lang/Boolean;

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/utils/Properties;->isScreenXlarge:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isTelephonySupported()Z
    .locals 2

    .prologue
    .line 63
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 64
    const/4 v0, 0x1

    .line 66
    :goto_0
    return v0

    :cond_0
    new-instance v0, Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;-><init>(Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;)V

    #calls: Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;->isTelephonySupported()Z
    invoke-static {v0}, Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;->access$1(Lcom/flixster/android/utils/Properties$EclairTelephonyDetector;)Z

    move-result v0

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 200
    invoke-direct {p0}, Lcom/flixster/android/utils/Properties;->resetSplash()V

    .line 201
    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isMskFinishedDeepLinkInitiated:Z

    .line 202
    invoke-direct {p0}, Lcom/flixster/android/utils/Properties;->resetVisitType()V

    .line 203
    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->shouldUseLowBitrateForTrailer:Z

    .line 204
    return-void
.end method

.method public setMskFinishedDeepLinkInitiated()V
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->isMskFinishedDeepLinkInitiated:Z

    .line 167
    return-void
.end method

.method public setProperties(Lnet/flixster/android/model/Property;)V
    .locals 3
    .parameter "p"

    .prologue
    .line 127
    iput-object p1, p0, Lcom/flixster/android/utils/Properties;->property:Lnet/flixster/android/model/Property;

    .line 128
    iget-boolean v0, p1, Lnet/flixster/android/model/Property;->isMskEnabled:Z

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setMskEnabled(Z)V

    .line 129
    iget-object v0, p1, Lnet/flixster/android/model/Property;->mskAnonPromoUrl:Ljava/lang/String;

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setMskAnonPromoUrl(Ljava/lang/String;)V

    .line 130
    iget-object v0, p1, Lnet/flixster/android/model/Property;->mskUserPromoUrl:Ljava/lang/String;

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setMskUserPromoUrl(Ljava/lang/String;)V

    .line 131
    iget-object v0, p1, Lnet/flixster/android/model/Property;->mskTooltipUrl:Ljava/lang/String;

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setMskTooltipUrl(Ljava/lang/String;)V

    .line 132
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Properties.setProperties: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method public setUseLowBitrateForTrailer()V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->shouldUseLowBitrateForTrailer:Z

    .line 177
    return-void
.end method

.method public setUserSessionExpired()V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->hasUserSessionExpired:Z

    .line 157
    return-void
.end method

.method public shouldShowSplash()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->shouldShowSplash:Z

    return v0
.end method

.method public shouldUseLowBitrateForTrailer()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/flixster/android/utils/Properties;->shouldUseLowBitrateForTrailer:Z

    return v0
.end method

.method public splashRemoved()V
    .locals 2

    .prologue
    .line 111
    const-string v0, "FlxMain"

    const-string v1, "Splash removed"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->shouldShowSplash:Z

    .line 113
    return-void
.end method

.method public splashShown()V
    .locals 2

    .prologue
    .line 106
    const-string v0, "FlxMain"

    const-string v1, "Splash shown"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flixster/android/utils/Properties;->shouldShowSplash:Z

    .line 108
    return-void
.end method
