.class public Lcom/flixster/android/utils/Translator;
.super Lcom/flixster/android/utils/LocalizedHelper;
.source "Translator.java"


# static fields
.field private static instance:Lcom/flixster/android/utils/Translator;


# instance fields
.field private final englishGenres:[Ljava/lang/String;

.field private final frenchGenres:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x0

    sput-object v0, Lcom/flixster/android/utils/Translator;->instance:Lcom/flixster/android/utils/Translator;

    .line 6
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/flixster/android/utils/LocalizedHelper;-><init>()V

    .line 12
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/utils/Translator;->englishGenres:[Ljava/lang/String;

    .line 13
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flixster/android/utils/Translator;->frenchGenres:[Ljava/lang/String;

    .line 14
    return-void
.end method

.method public static declared-synchronized initialize(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 17
    const-class v1, Lcom/flixster/android/utils/Translator;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/flixster/android/utils/Translator;->instance:Lcom/flixster/android/utils/Translator;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/flixster/android/utils/Translator;

    invoke-direct {v0, p0}, Lcom/flixster/android/utils/Translator;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/flixster/android/utils/Translator;->instance:Lcom/flixster/android/utils/Translator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_0
    monitor-exit v1

    return-void

    .line 17
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static instance()Lcom/flixster/android/utils/Translator;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/flixster/android/utils/Translator;->instance:Lcom/flixster/android/utils/Translator;

    return-object v0
.end method


# virtual methods
.method public translateGenre(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "genre"

    .prologue
    .line 27
    iget-boolean v1, p0, Lcom/flixster/android/utils/Translator;->isFrance:Z

    if-nez v1, :cond_1

    .line 35
    .end local p1
    :cond_0
    :goto_0
    return-object p1

    .line 30
    .restart local p1
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/flixster/android/utils/Translator;->englishGenres:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 31
    iget-object v1, p0, Lcom/flixster/android/utils/Translator;->englishGenres:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 32
    iget-object v1, p0, Lcom/flixster/android/utils/Translator;->frenchGenres:[Ljava/lang/String;

    aget-object p1, v1, v0

    goto :goto_0

    .line 30
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
