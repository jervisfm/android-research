.class Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;
.super Ljava/lang/Object;
.source "VersionedDeviceDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/VersionedDeviceDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DonutScreenDetector"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;)V
    .locals 0
    .parameter

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isXlarge(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isLarge(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isAtLeastTvdpi(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isAtLeastHdpi(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method

.method private isAtLeastHdpi(Landroid/content/res/Resources;)Z
    .locals 2
    .parameter "res"

    .prologue
    .line 93
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v1, 0xf0

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isAtLeastTvdpi(Landroid/content/res/Resources;)Z
    .locals 3
    .parameter "res"

    .prologue
    .line 97
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DonutScreenDetector.densityDpi="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v1, 0xd5

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLarge(Landroid/content/res/Resources;)Z
    .locals 2
    .parameter "res"

    .prologue
    .line 89
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isXlarge(Landroid/content/res/Resources;)Z
    .locals 2
    .parameter "res"

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
