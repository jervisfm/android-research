.class Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;
.super Ljava/lang/Object;
.source "VersionedDeviceDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/VersionedDeviceDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EclairDetector"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;)V
    .locals 0
    .parameter

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;->hasSystemFeature(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private hasSystemFeature(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 1
    .parameter "packageManager"
    .parameter "featureName"

    .prologue
    .line 105
    invoke-virtual {p1, p2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
