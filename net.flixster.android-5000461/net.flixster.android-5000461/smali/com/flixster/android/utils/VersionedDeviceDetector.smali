.class public Lcom/flixster/android/utils/VersionedDeviceDetector;
.super Ljava/lang/Object;
.source "VersionedDeviceDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;,
        Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;
    }
.end annotation


# static fields
.field private static final FEATURE_GTV:Ljava/lang/String; = "com.google.android.tv"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static identifyGtv(Landroid/content/res/Resources;Landroid/content/pm/PackageManager;)Z
    .locals 7
    .parameter "res"
    .parameter "packageManager"

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 45
    sget v2, Lcom/flixster/android/utils/F;->API_LEVEL:I

    .line 46
    .local v2, sdkVersion:I
    const/16 v4, 0xc

    if-ge v2, v4, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v3

    .line 49
    :cond_1
    new-instance v4, Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;

    invoke-direct {v4, v6}, Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;-><init>(Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;)V

    const-string v5, "com.google.android.tv"

    #calls: Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;->hasSystemFeature(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    invoke-static {v4, p1, v5}, Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;->access$1(Lcom/flixster/android/utils/VersionedDeviceDetector$EclairDetector;Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    .line 50
    .local v0, hasGtvFeature:Z
    new-instance v1, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;

    invoke-direct {v1, v6}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;-><init>(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;)V

    .line 51
    .local v1, screenDetector:Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;
    if-eqz v0, :cond_0

    #calls: Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isLarge(Landroid/content/res/Resources;)Z
    invoke-static {v1, p0}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->access$2(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z

    move-result v4

    if-eqz v4, :cond_0

    #calls: Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isAtLeastTvdpi(Landroid/content/res/Resources;)Z
    invoke-static {v1, p0}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->access$3(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static identifyHc7InchesTablet(Landroid/content/res/Resources;)Z
    .locals 3
    .parameter "res"

    .prologue
    .line 34
    sget v1, Lcom/flixster/android/utils/F;->API_LEVEL:I

    .line 35
    .local v1, sdkVersion:I
    const/16 v2, 0xd

    if-ge v1, v2, :cond_0

    .line 36
    const/4 v2, 0x0

    .line 39
    :goto_0
    return v2

    .line 38
    :cond_0
    new-instance v0, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;-><init>(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;)V

    .line 39
    .local v0, screen:Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;
    #calls: Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isLarge(Landroid/content/res/Resources;)Z
    invoke-static {v0, p0}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->access$2(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z

    move-result v2

    goto :goto_0
.end method

.method public static identifyHcTablet(Landroid/content/res/Resources;)Z
    .locals 3
    .parameter "res"

    .prologue
    .line 23
    sget v1, Lcom/flixster/android/utils/F;->API_LEVEL:I

    .line 24
    .local v1, sdkVersion:I
    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    .line 25
    const/4 v2, 0x0

    .line 28
    :goto_0
    return v2

    .line 27
    :cond_0
    new-instance v0, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;-><init>(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;)V

    .line 28
    .local v0, screen:Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;
    #calls: Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isXlarge(Landroid/content/res/Resources;)Z
    invoke-static {v0, p0}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->access$1(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z

    move-result v2

    goto :goto_0
.end method

.method public static isAtLeastHdpi(Landroid/content/res/Resources;)Z
    .locals 3
    .parameter "res"

    .prologue
    .line 56
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    .line 57
    .local v0, sdkVersion:I
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 58
    const/4 v1, 0x0

    .line 60
    :goto_0
    return v1

    :cond_0
    new-instance v1, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;-><init>(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;)V

    #calls: Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isAtLeastHdpi(Landroid/content/res/Resources;)Z
    invoke-static {v1, p0}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->access$4(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isScreenLarge(Landroid/content/res/Resources;)Z
    .locals 3
    .parameter "res"

    .prologue
    .line 65
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    .line 66
    .local v0, sdkVersion:I
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 67
    const/4 v1, 0x0

    .line 69
    :goto_0
    return v1

    :cond_0
    new-instance v1, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;-><init>(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;)V

    #calls: Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isLarge(Landroid/content/res/Resources;)Z
    invoke-static {v1, p0}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->access$2(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isScreenXlarge(Landroid/content/res/Resources;)Z
    .locals 3
    .parameter "res"

    .prologue
    .line 74
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    .line 75
    .local v0, sdkVersion:I
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 76
    const/4 v1, 0x0

    .line 78
    :goto_0
    return v1

    :cond_0
    new-instance v1, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;-><init>(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;)V

    #calls: Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->isXlarge(Landroid/content/res/Resources;)Z
    invoke-static {v1, p0}, Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;->access$1(Lcom/flixster/android/utils/VersionedDeviceDetector$DonutScreenDetector;Landroid/content/res/Resources;)Z

    move-result v1

    goto :goto_0
.end method
