.class Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;
.super Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;
.source "VersionedUidHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/VersionedUidHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CupcakeDeviceIdentifier"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;-><init>(Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;)V
    .locals 0
    .parameter

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;-><init>()V

    return-void
.end method


# virtual methods
.method protected getAndroidId(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1
    .parameter "cr"

    .prologue
    .line 122
    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRecommendedId(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1
    .parameter "cr"

    .prologue
    .line 116
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSerialNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return-object v0
.end method
