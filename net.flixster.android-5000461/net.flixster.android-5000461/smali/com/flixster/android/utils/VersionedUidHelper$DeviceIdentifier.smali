.class abstract Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;
.super Ljava/lang/Object;
.source "VersionedUidHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/VersionedUidHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "DeviceIdentifier"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;)V
    .locals 0
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;-><init>()V

    return-void
.end method

.method protected static defaultId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "0"

    return-object v0
.end method

.method protected static getTelephonyId(Landroid/telephony/TelephonyManager;)Ljava/lang/String;
    .locals 1
    .parameter "tm"

    .prologue
    .line 84
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected abstract getAndroidId(Landroid/content/ContentResolver;)Ljava/lang/String;
.end method

.method protected abstract getRecommendedId(Landroid/content/ContentResolver;)Ljava/lang/String;
.end method

.method protected abstract getSerialNumber()Ljava/lang/String;
.end method

.method protected getUid(Landroid/telephony/TelephonyManager;Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1
    .parameter "tm"
    .parameter "cr"

    .prologue
    .line 59
    invoke-static {p1}, Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;->getTelephonyId(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v0

    .local v0, id:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 60
    invoke-virtual {p0, p2}, Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;->getRecommendedId(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 62
    invoke-static {}, Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;->defaultId()Ljava/lang/String;

    move-result-object v0

    .line 64
    :cond_0
    return-object v0
.end method
