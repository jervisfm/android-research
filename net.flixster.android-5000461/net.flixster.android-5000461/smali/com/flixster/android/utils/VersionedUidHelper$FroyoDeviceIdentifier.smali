.class Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;
.super Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;
.source "VersionedUidHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/VersionedUidHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FroyoDeviceIdentifier"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;-><init>(Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;)V
    .locals 0
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;-><init>()V

    return-void
.end method


# virtual methods
.method protected getRecommendedId(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1
    .parameter "cr"

    .prologue
    .line 109
    invoke-virtual {p0, p1}, Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;->getAndroidId(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
