.class Lcom/flixster/android/utils/VersionedUidHelper$GingerbreadDeviceIdentifier;
.super Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;
.source "VersionedUidHelper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/VersionedUidHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GingerbreadDeviceIdentifier"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;-><init>(Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/utils/VersionedUidHelper$GingerbreadDeviceIdentifier;)V
    .locals 0
    .parameter

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/flixster/android/utils/VersionedUidHelper$GingerbreadDeviceIdentifier;-><init>()V

    return-void
.end method


# virtual methods
.method protected getRecommendedId(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1
    .parameter "cr"

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/flixster/android/utils/VersionedUidHelper$GingerbreadDeviceIdentifier;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSerialNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    return-object v0
.end method
