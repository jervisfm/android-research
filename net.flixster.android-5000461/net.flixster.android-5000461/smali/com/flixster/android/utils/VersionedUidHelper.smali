.class public Lcom/flixster/android/utils/VersionedUidHelper;
.super Ljava/lang/Object;
.source "VersionedUidHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;,
        Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;,
        Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;,
        Lcom/flixster/android/utils/VersionedUidHelper$GingerbreadDeviceIdentifier;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertToNumericHash(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "s"

    .prologue
    .line 49
    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;->defaultId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getAndroidId(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1
    .parameter "cr"

    .prologue
    .line 26
    invoke-static {}, Lcom/flixster/android/utils/VersionedUidHelper;->getOsBasedDeviceIdentifier()Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;->getAndroidId(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getOsBasedDeviceIdentifier()Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 36
    sget v1, Lcom/flixster/android/utils/F;->API_LEVEL:I

    .line 38
    .local v1, sdkVersion:I
    const/16 v2, 0x8

    if-ge v1, v2, :cond_0

    .line 39
    new-instance v0, Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;

    invoke-direct {v0, v3, v3}, Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;-><init>(Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;Lcom/flixster/android/utils/VersionedUidHelper$CupcakeDeviceIdentifier;)V

    .line 45
    .local v0, identifier:Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;
    :goto_0
    return-object v0

    .line 40
    .end local v0           #identifier:Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;
    :cond_0
    const/16 v2, 0x9

    if-ge v1, v2, :cond_1

    .line 41
    new-instance v0, Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;

    invoke-direct {v0, v3, v3}, Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;-><init>(Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;Lcom/flixster/android/utils/VersionedUidHelper$FroyoDeviceIdentifier;)V

    .restart local v0       #identifier:Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;
    goto :goto_0

    .line 43
    .end local v0           #identifier:Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;
    :cond_1
    new-instance v0, Lcom/flixster/android/utils/VersionedUidHelper$GingerbreadDeviceIdentifier;

    invoke-direct {v0, v3}, Lcom/flixster/android/utils/VersionedUidHelper$GingerbreadDeviceIdentifier;-><init>(Lcom/flixster/android/utils/VersionedUidHelper$GingerbreadDeviceIdentifier;)V

    .restart local v0       #identifier:Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;
    goto :goto_0
.end method

.method public static getSerialNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lcom/flixster/android/utils/VersionedUidHelper;->getOsBasedDeviceIdentifier()Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTelephonyDeviceId(Landroid/telephony/TelephonyManager;)Ljava/lang/String;
    .locals 1
    .parameter "tm"

    .prologue
    .line 21
    invoke-static {p0}, Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;->getTelephonyId(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUdt(Landroid/telephony/TelephonyManager;Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1
    .parameter "tm"
    .parameter "cr"

    .prologue
    .line 16
    invoke-static {}, Lcom/flixster/android/utils/VersionedUidHelper;->getOsBasedDeviceIdentifier()Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/flixster/android/utils/VersionedUidHelper$DeviceIdentifier;->getUid(Landroid/telephony/TelephonyManager;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/utils/VersionedUidHelper;->convertToNumericHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
