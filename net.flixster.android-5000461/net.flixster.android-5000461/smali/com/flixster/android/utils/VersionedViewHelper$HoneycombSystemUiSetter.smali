.class Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;
.super Ljava/lang/Object;
.source "VersionedViewHelper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/utils/VersionedViewHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HoneycombSystemUiSetter"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;Landroid/view/View;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;->setSystemUiVisibility(Landroid/view/View;I)V

    return-void
.end method

.method private setSystemUiVisibility(Landroid/view/View;I)V
    .locals 0
    .parameter "view"
    .parameter "visibility"

    .prologue
    .line 28
    invoke-virtual {p1, p2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 29
    return-void
.end method
