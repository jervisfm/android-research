.class public Lcom/flixster/android/utils/VersionedViewHelper;
.super Ljava/lang/Object;
.source "VersionedViewHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dimSystemNavigation(Landroid/app/Activity;)V
    .locals 1
    .parameter "activity"

    .prologue
    .line 11
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/flixster/android/utils/VersionedViewHelper;->setSystemUiVisibility(Landroid/app/Activity;I)V

    .line 12
    return-void
.end method

.method public static hideSystemNavigation(Landroid/app/Activity;)V
    .locals 1
    .parameter "activity"

    .prologue
    .line 15
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/flixster/android/utils/VersionedViewHelper;->setSystemUiVisibility(Landroid/app/Activity;I)V

    .line 16
    return-void
.end method

.method protected static setSystemUiVisibility(Landroid/app/Activity;I)V
    .locals 2
    .parameter "activity"
    .parameter "visibility"

    .prologue
    .line 19
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 20
    new-instance v0, Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;-><init>(Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    #calls: Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;->setSystemUiVisibility(Landroid/view/View;I)V
    invoke-static {v0, v1, p1}, Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;->access$1(Lcom/flixster/android/utils/VersionedViewHelper$HoneycombSystemUiSetter;Landroid/view/View;I)V

    .line 22
    :cond_0
    return-void
.end method
