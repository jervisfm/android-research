.class Lcom/flixster/android/view/Carousel$1;
.super Ljava/lang/Object;
.source "Carousel.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/view/Carousel;->initializeGallery()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/view/Carousel;


# direct methods
.method constructor <init>(Lcom/flixster/android/view/Carousel;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/view/Carousel$1;->this$0:Lcom/flixster/android/view/Carousel;

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/flixster/android/view/Carousel$1;->this$0:Lcom/flixster/android/view/Carousel;

    #getter for: Lcom/flixster/android/view/Carousel;->caption:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/flixster/android/view/Carousel;->access$0(Lcom/flixster/android/view/Carousel;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, p0, Lcom/flixster/android/view/Carousel$1;->this$0:Lcom/flixster/android/view/Carousel;

    #getter for: Lcom/flixster/android/view/Carousel;->items:Ljava/util/List;
    invoke-static {v0}, Lcom/flixster/android/view/Carousel;->access$1(Lcom/flixster/android/view/Carousel;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/model/CarouselItem;

    invoke-virtual {v0}, Lcom/flixster/android/model/CarouselItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/flixster/android/view/Carousel$1;->this$0:Lcom/flixster/android/view/Carousel;

    #getter for: Lcom/flixster/android/view/Carousel;->dots:Lcom/flixster/android/view/PageIndicator;
    invoke-static {v0}, Lcom/flixster/android/view/Carousel;->access$2(Lcom/flixster/android/view/Carousel;)Lcom/flixster/android/view/PageIndicator;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/flixster/android/view/PageIndicator;->setCurrPageNum(I)V

    .line 118
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    return-void
.end method
