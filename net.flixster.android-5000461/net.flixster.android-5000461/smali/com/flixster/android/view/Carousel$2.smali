.class Lcom/flixster/android/view/Carousel$2;
.super Ljava/lang/Object;
.source "Carousel.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/view/Carousel;->initializeGallery()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/view/Carousel;


# direct methods
.method constructor <init>(Lcom/flixster/android/view/Carousel;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/view/Carousel$2;->this$0:Lcom/flixster/android/view/Carousel;

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/flixster/android/view/Carousel$2;->this$0:Lcom/flixster/android/view/Carousel;

    iget-object v1, p0, Lcom/flixster/android/view/Carousel$2;->this$0:Lcom/flixster/android/view/Carousel;

    #getter for: Lcom/flixster/android/view/Carousel;->items:Ljava/util/List;
    invoke-static {v1}, Lcom/flixster/android/view/Carousel;->access$1(Lcom/flixster/android/view/Carousel;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/Carousel;->setTag(Ljava/lang/Object;)V

    .line 128
    iget-object v0, p0, Lcom/flixster/android/view/Carousel$2;->this$0:Lcom/flixster/android/view/Carousel;

    #getter for: Lcom/flixster/android/view/Carousel;->galleryOnClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v0}, Lcom/flixster/android/view/Carousel;->access$3(Lcom/flixster/android/view/Carousel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/view/Carousel$2;->this$0:Lcom/flixster/android/view/Carousel;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 129
    return-void
.end method
