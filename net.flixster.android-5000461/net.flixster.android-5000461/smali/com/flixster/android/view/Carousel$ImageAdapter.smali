.class Lcom/flixster/android/view/Carousel$ImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "Carousel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/view/Carousel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImageAdapter"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final images:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/flixster/android/model/Image;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "Lcom/flixster/android/model/Image;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p2, images:Ljava/util/List;,"Ljava/util/List<+Lcom/flixster/android/model/Image;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 184
    iput-object p1, p0, Lcom/flixster/android/view/Carousel$ImageAdapter;->context:Landroid/content/Context;

    .line 185
    iput-object p2, p0, Lcom/flixster/android/view/Carousel$ImageAdapter;->images:Ljava/util/List;

    .line 186
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/flixster/android/view/Carousel$ImageAdapter;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/view/Carousel$ImageAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/flixster/android/view/Carousel$ImageAdapter;->images:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 214
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 219
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v4, -0x1

    .line 190
    if-eqz p2, :cond_0

    instance-of v1, p2, Landroid/widget/ImageView;

    if-nez v1, :cond_1

    .line 191
    :cond_0
    new-instance p2, Lcom/flixster/android/view/HighlightableImageView;

    .end local p2
    iget-object v1, p0, Lcom/flixster/android/view/Carousel$ImageAdapter;->context:Landroid/content/Context;

    invoke-direct {p2, v1}, Lcom/flixster/android/view/HighlightableImageView;-><init>(Landroid/content/Context;)V

    .line 193
    .restart local p2
    :cond_1
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Carousel.ImageAdapter.getView "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    new-instance v1, Landroid/widget/Gallery$LayoutParams;

    .line 195
    invoke-direct {v1, v4, v4}, Landroid/widget/Gallery$LayoutParams;-><init>(II)V

    .line 194
    invoke-virtual {p2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 196
    iget-object v1, p0, Lcom/flixster/android/view/Carousel$ImageAdapter;->images:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/model/Image;

    move-object v2, p2

    check-cast v2, Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2, v3}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;Landroid/widget/ImageView$ScaleType;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 197
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    move-object v1, p2

    .line 198
    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020134

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object v1, p2

    .line 199
    check-cast v1, Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 204
    :goto_0
    return-object p2

    :cond_2
    move-object v1, p2

    .line 201
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move-object v1, p2

    .line 202
    check-cast v1, Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0
.end method
