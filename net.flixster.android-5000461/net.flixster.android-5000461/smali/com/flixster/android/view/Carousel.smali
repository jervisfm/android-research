.class public Lcom/flixster/android/view/Carousel;
.super Landroid/widget/RelativeLayout;
.source "Carousel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/view/Carousel$ImageAdapter;
    }
.end annotation


# instance fields
.field private caption:Landroid/widget/TextView;

.field private final context:Landroid/content/Context;

.field private dots:Lcom/flixster/android/view/PageIndicator;

.field private gallery:Landroid/widget/Gallery;

.field private galleryOnClickListener:Landroid/view/View$OnClickListener;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/model/CarouselItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object p1, p0, Lcom/flixster/android/view/Carousel;->context:Landroid/content/Context;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    iput-object p1, p0, Lcom/flixster/android/view/Carousel;->context:Landroid/content/Context;

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    iput-object p1, p0, Lcom/flixster/android/view/Carousel;->context:Landroid/content/Context;

    .line 54
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/view/Carousel;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/flixster/android/view/Carousel;->caption:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/view/Carousel;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/flixster/android/view/Carousel;->items:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lcom/flixster/android/view/Carousel;)Lcom/flixster/android/view/PageIndicator;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/flixster/android/view/Carousel;->dots:Lcom/flixster/android/view/PageIndicator;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/view/Carousel;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/flixster/android/view/Carousel;->galleryOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private initializeGallery()V
    .locals 5

    .prologue
    .line 112
    iget-object v0, p0, Lcom/flixster/android/view/Carousel;->gallery:Landroid/widget/Gallery;

    new-instance v1, Lcom/flixster/android/view/Carousel$ImageAdapter;

    iget-object v2, p0, Lcom/flixster/android/view/Carousel;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/flixster/android/view/Carousel;->items:Ljava/util/List;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/flixster/android/view/Carousel$ImageAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/flixster/android/view/Carousel$ImageAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 113
    iget-object v0, p0, Lcom/flixster/android/view/Carousel;->gallery:Landroid/widget/Gallery;

    new-instance v1, Lcom/flixster/android/view/Carousel$1;

    invoke-direct {v1, p0}, Lcom/flixster/android/view/Carousel$1;-><init>(Lcom/flixster/android/view/Carousel;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 124
    iget-object v0, p0, Lcom/flixster/android/view/Carousel;->gallery:Landroid/widget/Gallery;

    new-instance v1, Lcom/flixster/android/view/Carousel$2;

    invoke-direct {v1, p0}, Lcom/flixster/android/view/Carousel$2;-><init>(Lcom/flixster/android/view/Carousel;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 177
    return-void
.end method


# virtual methods
.method public load(Ljava/util/List;Landroid/view/View$OnClickListener;)V
    .locals 12
    .parameter
    .parameter "onClickListener"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/model/CarouselItem;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, items:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/model/CarouselItem;>;"
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    iput-object v8, p0, Lcom/flixster/android/view/Carousel;->items:Ljava/util/List;

    .line 58
    iput-object p2, p0, Lcom/flixster/android/view/Carousel;->galleryOnClickListener:Landroid/view/View$OnClickListener;

    .line 60
    invoke-virtual {p0}, Lcom/flixster/android/view/Carousel;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a002e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 61
    .local v2, carouselHeight:I
    invoke-virtual {p0}, Lcom/flixster/android/view/Carousel;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0040

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 62
    .local v6, gradientHeight:I
    invoke-virtual {p0}, Lcom/flixster/android/view/Carousel;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0041

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 63
    .local v0, captionHeight:I
    invoke-virtual {p0}, Lcom/flixster/android/view/Carousel;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0042

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 66
    .local v3, dotsHeight:I
    new-instance v8, Lcom/flixster/android/view/GallerySansFling;

    iget-object v9, p0, Lcom/flixster/android/view/Carousel;->context:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/flixster/android/view/GallerySansFling;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/flixster/android/view/Carousel;->gallery:Landroid/widget/Gallery;

    .line 67
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->gallery:Landroid/widget/Gallery;

    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    .line 68
    invoke-direct {v9, v10, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 67
    invoke-virtual {v8, v9}, Landroid/widget/Gallery;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->gallery:Landroid/widget/Gallery;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/Gallery;->setHorizontalFadingEdgeEnabled(Z)V

    .line 70
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->gallery:Landroid/widget/Gallery;

    const/high16 v9, 0x3f80

    invoke-virtual {v8, v9}, Landroid/widget/Gallery;->setUnselectedAlpha(F)V

    .line 71
    invoke-direct {p0}, Lcom/flixster/android/view/Carousel;->initializeGallery()V

    .line 72
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->gallery:Landroid/widget/Gallery;

    invoke-virtual {p0, v8}, Lcom/flixster/android/view/Carousel;->addView(Landroid/view/View;)V

    .line 75
    new-instance v5, Landroid/widget/RelativeLayout;

    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->context:Landroid/content/Context;

    invoke-direct {v5, v8}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 76
    .local v5, gradient:Landroid/widget/RelativeLayout;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 77
    const/4 v8, -0x1

    .line 76
    invoke-direct {v7, v8, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 78
    .local v7, gradientLayout:Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xc

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 79
    const/16 v8, 0xe

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 80
    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 81
    invoke-virtual {p0}, Lcom/flixster/android/view/Carousel;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09001b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 82
    invoke-virtual {p0, v5}, Lcom/flixster/android/view/Carousel;->addView(Landroid/view/View;)V

    .line 85
    new-instance v8, Landroid/widget/TextView;

    iget-object v9, p0, Lcom/flixster/android/view/Carousel;->context:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/flixster/android/view/Carousel;->caption:Landroid/widget/TextView;

    .line 86
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 87
    const/4 v8, -0x2

    const/4 v9, -0x2

    .line 86
    invoke-direct {v1, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 88
    .local v1, captionLayout:Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xc

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 89
    const/16 v8, 0xe

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 90
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->caption:Landroid/widget/TextView;

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->caption:Landroid/widget/TextView;

    const/16 v9, 0xa

    const/4 v10, 0x0

    const/16 v11, 0xa

    invoke-virtual {v8, v9, v10, v11, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 92
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->caption:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/flixster/android/view/Carousel;->context:Landroid/content/Context;

    const v10, 0x7f0d0072

    invoke-virtual {v8, v9, v10}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 93
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->caption:Landroid/widget/TextView;

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 95
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->caption:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->setSingleLine()V

    .line 96
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->caption:Landroid/widget/TextView;

    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 97
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->caption:Landroid/widget/TextView;

    invoke-virtual {p0, v8}, Lcom/flixster/android/view/Carousel;->addView(Landroid/view/View;)V

    .line 100
    new-instance v8, Lcom/flixster/android/view/PageIndicator;

    iget-object v9, p0, Lcom/flixster/android/view/Carousel;->context:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/flixster/android/view/PageIndicator;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/flixster/android/view/Carousel;->dots:Lcom/flixster/android/view/PageIndicator;

    .line 101
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->dots:Lcom/flixster/android/view/PageIndicator;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/flixster/android/view/PageIndicator;->setNumOfPages(I)V

    .line 102
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 103
    const/4 v8, -0x2

    const/4 v9, -0x2

    .line 102
    invoke-direct {v4, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 104
    .local v4, dotsLayout:Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xc

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 105
    const/16 v8, 0xe

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 106
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->dots:Lcom/flixster/android/view/PageIndicator;

    invoke-virtual {v8, v4}, Lcom/flixster/android/view/PageIndicator;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->dots:Lcom/flixster/android/view/PageIndicator;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11, v3}, Lcom/flixster/android/view/PageIndicator;->setPadding(IIII)V

    .line 108
    iget-object v8, p0, Lcom/flixster/android/view/Carousel;->dots:Lcom/flixster/android/view/PageIndicator;

    invoke-virtual {p0, v8}, Lcom/flixster/android/view/Carousel;->addView(Landroid/view/View;)V

    .line 109
    return-void
.end method
