.class Lcom/flixster/android/view/DialogBuilder$14;
.super Ljava/lang/Object;
.source "DialogBuilder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/view/DialogBuilder;->showDialog(Landroid/app/AlertDialog$Builder;Landroid/app/Activity;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$activity:Landroid/app/Activity;

.field private final synthetic val$dialogBuilder:Landroid/app/AlertDialog$Builder;

.field private final synthetic val$isDialogClickable:Z


# direct methods
.method constructor <init>(Landroid/app/Activity;Landroid/app/AlertDialog$Builder;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/view/DialogBuilder$14;->val$activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/flixster/android/view/DialogBuilder$14;->val$dialogBuilder:Landroid/app/AlertDialog$Builder;

    iput-boolean p3, p0, Lcom/flixster/android/view/DialogBuilder$14;->val$isDialogClickable:Z

    .line 511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 514
    iget-object v2, p0, Lcom/flixster/android/view/DialogBuilder$14;->val$activity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 515
    iget-object v2, p0, Lcom/flixster/android/view/DialogBuilder$14;->val$dialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 516
    .local v0, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 517
    iget-boolean v2, p0, Lcom/flixster/android/view/DialogBuilder$14;->val$isDialogClickable:Z

    if-eqz v2, :cond_0

    .line 519
    const v2, 0x102000b

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 520
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    .line 519
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 521
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 522
    .local v1, positiveButton:Landroid/widget/Button;
    if-eqz v1, :cond_0

    .line 523
    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    .line 527
    .end local v0           #dialog:Landroid/app/AlertDialog;
    .end local v1           #positiveButton:Landroid/widget/Button;
    :cond_0
    return-void
.end method
