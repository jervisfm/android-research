.class Lcom/flixster/android/view/DialogBuilder$5;
.super Ljava/lang/Object;
.source "DialogBuilder.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/view/DialogBuilder;->createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .parameter "dialog"
    .parameter "whichButton"

    .prologue
    .line 95
    invoke-static {}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->instance()Lcom/flixster/android/view/DialogBuilder$DialogEvents;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->onPositiveButtonClick(I)V

    .line 96
    return-void
.end method
