.class public Lcom/flixster/android/view/DialogBuilder$DialogEvents;
.super Ljava/lang/Object;
.source "DialogBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/view/DialogBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DialogEvents"
.end annotation


# static fields
.field private static final INSTANCE:Lcom/flixster/android/view/DialogBuilder$DialogEvents;


# instance fields
.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/view/DialogBuilder$DialogListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 393
    new-instance v0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;

    invoke-direct {v0}, Lcom/flixster/android/view/DialogBuilder$DialogEvents;-><init>()V

    sput-object v0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->INSTANCE:Lcom/flixster/android/view/DialogBuilder$DialogEvents;

    .line 392
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->listeners:Ljava/util/List;

    .line 399
    return-void
.end method

.method public static instance()Lcom/flixster/android/view/DialogBuilder$DialogEvents;
    .locals 1

    .prologue
    .line 402
    sget-object v0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->INSTANCE:Lcom/flixster/android/view/DialogBuilder$DialogEvents;

    return-object v0
.end method


# virtual methods
.method public addListener(Lcom/flixster/android/view/DialogBuilder$DialogListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 408
    iget-object v0, p0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    return-void
.end method

.method public onNegativeButtonClick(I)V
    .locals 3
    .parameter "which"

    .prologue
    .line 431
    iget-object v1, p0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 434
    iget-object v1, p0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 435
    return-void

    .line 431
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 432
    .local v0, listener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-interface {v0, p1}, Lcom/flixster/android/view/DialogBuilder$DialogListener;->onNegativeButtonClick(I)V

    goto :goto_0
.end method

.method public onNeutralButtonClick(I)V
    .locals 3
    .parameter "which"

    .prologue
    .line 424
    iget-object v1, p0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 427
    iget-object v1, p0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 428
    return-void

    .line 424
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 425
    .local v0, listener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-interface {v0, p1}, Lcom/flixster/android/view/DialogBuilder$DialogListener;->onNeutralButtonClick(I)V

    goto :goto_0
.end method

.method public onPositiveButtonClick(I)V
    .locals 3
    .parameter "which"

    .prologue
    .line 417
    iget-object v1, p0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 420
    iget-object v1, p0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 421
    return-void

    .line 417
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 418
    .local v0, listener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-interface {v0, p1}, Lcom/flixster/android/view/DialogBuilder$DialogListener;->onPositiveButtonClick(I)V

    goto :goto_0
.end method

.method public removeListeners()V
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/flixster/android/view/DialogBuilder$DialogEvents;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 414
    return-void
.end method
