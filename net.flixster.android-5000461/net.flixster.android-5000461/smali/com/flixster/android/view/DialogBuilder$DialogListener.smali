.class public interface abstract Lcom/flixster/android/view/DialogBuilder$DialogListener;
.super Ljava/lang/Object;
.source "DialogBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/view/DialogBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DialogListener"
.end annotation


# virtual methods
.method public abstract onNegativeButtonClick(I)V
.end method

.method public abstract onNeutralButtonClick(I)V
.end method

.method public abstract onPositiveButtonClick(I)V
.end method
