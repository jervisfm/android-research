.class public Lcom/flixster/android/view/DialogBuilder;
.super Ljava/lang/Object;
.source "DialogBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/view/DialogBuilder$DialogEvents;,
        Lcom/flixster/android/view/DialogBuilder$DialogListener;
    }
.end annotation


# static fields
.field public static final DIAGNOSTIC_REPORT:I = 0x3b9acde8

.field public static final DOWNLOAD_CANCEL:I = 0x3b9acac9

.field public static final DOWNLOAD_CONFIRM:I = 0x3b9acac8

.field public static final DOWNLOAD_DELETE:I = 0x3b9acaca

.field public static final DOWNLOAD_INITIALIZING:I = 0x3b9acace

.field public static final DOWNLOAD_LICENSE_REFETCH_NEEDED:I = 0x3b9acacf

.field public static final DOWNLOAD_NO_SPACE:I = 0x3b9acacd

.field public static final DOWNLOAD_REACHED_LIMIT:I = 0x3b9acacc

.field public static final DOWNLOAD_STORAGE:I = 0x3b9acacb

.field public static final ERROR_NETWORK:I = 0x3b9acd84

.field public static final ERROR_NETWORK_UNSTABLE:I = 0x3b9acd8c

.field public static final ERROR_NOT_LICENSED:I = 0x3b9acd8a

.field public static final ERROR_SERVER_API:I = 0x3b9acd86

.field public static final ERROR_SERVER_DATA:I = 0x3b9acd85

.field public static final ERROR_SERVER_MSG:I = 0x3b9acd87

.field public static final ERROR_STREAM_CREATE:I = 0x3b9acd89

.field public static final ERROR_UNKNOWN:I = 0x3b9acd8b

.field public static final ERROR_USER_ACCT:I = 0x3b9acd88

.field public static final LOGIN_CHOICE:I = 0x3b9aca66

.field public static final LOGOUT_FACEBOOK:I = 0x3b9aca65

.field public static final LOGOUT_FLIXSTER:I = 0x3b9aca64

.field public static final MSK_INELIGIBLE:I = 0x3b9acb90

.field public static final MSK_TOOLTIP:I = 0x3b9acde9

.field public static final PRIVACY:I = 0x3b9aca0a

.field public static final REWARDS_MOVIE_EARNED:I = 0x3b9acb91

.field public static final SPLASH:I = 0x3b9ac9ff

.field public static final WATCHNOW_CONFIRM:I = 0x3b9acb2c

.field private static final dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

.field private static final dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

.field private static final dialogEventsNeutralClick:Landroid/content/DialogInterface$OnClickListener;

.field private static final dialogEventsPositiveClick:Landroid/content/DialogInterface$OnClickListener;

.field private static message:Ljava/lang/String;

.field private static movieDownloadAssetSizeRaw:J

.field private static movieDownloadedAssetSizeRaw:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-string v0, ""

    sput-object v0, Lcom/flixster/android/view/DialogBuilder;->message:Ljava/lang/String;

    .line 363
    new-instance v0, Lcom/flixster/android/view/DialogBuilder$1;

    invoke-direct {v0}, Lcom/flixster/android/view/DialogBuilder$1;-><init>()V

    sput-object v0, Lcom/flixster/android/view/DialogBuilder;->dialogEventsPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    .line 370
    new-instance v0, Lcom/flixster/android/view/DialogBuilder$2;

    invoke-direct {v0}, Lcom/flixster/android/view/DialogBuilder$2;-><init>()V

    sput-object v0, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    .line 377
    new-instance v0, Lcom/flixster/android/view/DialogBuilder$3;

    invoke-direct {v0}, Lcom/flixster/android/view/DialogBuilder$3;-><init>()V

    sput-object v0, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNeutralClick:Landroid/content/DialogInterface$OnClickListener;

    .line 384
    new-instance v0, Lcom/flixster/android/view/DialogBuilder$4;

    invoke-direct {v0}, Lcom/flixster/android/view/DialogBuilder$4;-><init>()V

    sput-object v0, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 370
    sget-object v0, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method static synthetic access$1()Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 363
    sget-object v0, Lcom/flixster/android/view/DialogBuilder;->dialogEventsPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method public static createDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
    .locals 19
    .parameter "activity"
    .parameter "id"

    .prologue
    .line 73
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 77
    .local v11, res:Landroid/content/res/Resources;
    sparse-switch p1, :sswitch_data_0

    .line 357
    const/4 v13, 0x0

    :goto_0
    return-object v13

    .line 79
    :sswitch_0
    new-instance v13, Landroid/app/Dialog;

    const v15, 0x7f0d0085

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v15}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 80
    .local v13, splash:Landroid/app/Dialog;
    const v15, 0x7f03007f

    invoke-virtual {v13, v15}, Landroid/app/Dialog;->setContentView(I)V

    .line 81
    const/4 v15, 0x1

    invoke-virtual {v13, v15}, Landroid/app/Dialog;->setCancelable(Z)V

    goto :goto_0

    .line 85
    .end local v13           #splash:Landroid/app/Dialog;
    :sswitch_1
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 86
    .local v6, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0c00ee

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    .line 87
    const-string v18, "Flixster"

    aput-object v18, v16, v17

    .line 86
    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 88
    .local v14, title:Ljava/lang/String;
    const v15, 0x7f0c00ef

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "Flixster"

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 89
    .local v8, msg:Ljava/lang/String;
    invoke-virtual {v6, v14}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 90
    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 91
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 92
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 93
    const v15, 0x7f0c0133

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    new-instance v16, Lcom/flixster/android/view/DialogBuilder$5;

    invoke-direct/range {v16 .. v16}, Lcom/flixster/android/view/DialogBuilder$5;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 98
    const v15, 0x7f0c004a

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 99
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 102
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    .end local v8           #msg:Ljava/lang/String;
    .end local v14           #title:Ljava/lang/String;
    :sswitch_2
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 103
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c00ee

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "Facebook"

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 104
    .restart local v14       #title:Ljava/lang/String;
    const v15, 0x7f0c00ef

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "Facebook"

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 105
    .restart local v8       #msg:Ljava/lang/String;
    invoke-virtual {v6, v14}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 106
    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 107
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 108
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 109
    const v15, 0x7f0c0133

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    new-instance v16, Lcom/flixster/android/view/DialogBuilder$6;

    invoke-direct/range {v16 .. v16}, Lcom/flixster/android/view/DialogBuilder$6;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 114
    const v15, 0x7f0c004a

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 115
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 118
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    .end local v8           #msg:Ljava/lang/String;
    .end local v14           #title:Ljava/lang/String;
    :sswitch_3
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 119
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v15

    const v16, 0x3b9aca66

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 120
    .restart local v14       #title:Ljava/lang/String;
    if-nez v14, :cond_0

    const v15, 0x7f0c01b9

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .end local v14           #title:Ljava/lang/String;
    :cond_0
    invoke-virtual {v6, v14}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 121
    const v15, 0x7f0e0010

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 122
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 123
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 124
    const v15, 0x7f0c004a

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 125
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 128
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_4
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 129
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c0192

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 130
    const v15, 0x7f0c0199

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 131
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 132
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 133
    const v15, 0x7f0c012d

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 134
    const v15, 0x7f0c012e

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 135
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 138
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_5
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 139
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c0193

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 140
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v15

    const v16, 0x3b9acac8

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/flixster/android/utils/ObjectHolder;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lnet/flixster/android/model/LockerRight;

    .line 141
    .local v12, right:Lnet/flixster/android/model/LockerRight;
    if-eqz v12, :cond_1

    .line 142
    invoke-virtual {v12}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetSizeRaw()J

    move-result-wide v15

    sput-wide v15, Lcom/flixster/android/view/DialogBuilder;->movieDownloadAssetSizeRaw:J

    .line 144
    :cond_1
    invoke-static {}, Lcom/flixster/android/net/DownloadHelper;->findRemainingSpace()J

    move-result-wide v15

    sget-wide v17, Lcom/flixster/android/view/DialogBuilder;->movieDownloadAssetSizeRaw:J

    sub-long v4, v15, v17

    .line 145
    .local v4, availableSpacePostDownload:J
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isWifi()Z

    move-result v15

    if-eqz v15, :cond_2

    const v15, 0x7f0c019a

    .line 146
    :goto_1
    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    .line 147
    invoke-static {v4, v5}, Lcom/flixster/android/net/DownloadHelper;->readableFileSize(J)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    .line 145
    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 148
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 149
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 150
    const v15, 0x7f0c012d

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 151
    const v15, 0x7f0c012e

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 152
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 146
    :cond_2
    const v15, 0x7f0c019b

    goto :goto_1

    .line 155
    .end local v4           #availableSpacePostDownload:J
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    .end local v12           #right:Lnet/flixster/android/model/LockerRight;
    :sswitch_6
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 156
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c0194

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 157
    const v15, 0x7f0c019c

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 158
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 159
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 160
    const v15, 0x7f0c012d

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 161
    const v15, 0x7f0c012e

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 162
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 165
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_7
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 166
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c0195

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 167
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v15

    const v16, 0x3b9acaca

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/flixster/android/utils/ObjectHolder;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lnet/flixster/android/model/LockerRight;

    .line 168
    .restart local v12       #right:Lnet/flixster/android/model/LockerRight;
    if-eqz v12, :cond_3

    .line 169
    iget-wide v15, v12, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static/range {v15 .. v16}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedMovieSize(J)J

    move-result-wide v15

    sput-wide v15, Lcom/flixster/android/view/DialogBuilder;->movieDownloadedAssetSizeRaw:J

    .line 171
    :cond_3
    invoke-static {}, Lcom/flixster/android/net/DownloadHelper;->findRemainingSpace()J

    move-result-wide v15

    sget-wide v17, Lcom/flixster/android/view/DialogBuilder;->movieDownloadedAssetSizeRaw:J

    add-long v2, v15, v17

    .line 172
    .local v2, availableSpacePostDelete:J
    const v15, 0x7f0c019d

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    .line 173
    invoke-static {v2, v3}, Lcom/flixster/android/net/DownloadHelper;->readableFileSize(J)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    .line 172
    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 174
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 175
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 176
    const v15, 0x7f0c012d

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 177
    const v15, 0x7f0c012e

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 178
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 181
    .end local v2           #availableSpacePostDelete:J
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    .end local v12           #right:Lnet/flixster/android/model/LockerRight;
    :sswitch_8
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 182
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c0197

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 183
    const v15, 0x7f0c019f

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 184
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 185
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 186
    const v15, 0x7f0c0049

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNeutralClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 187
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 190
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_9
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 191
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c0196

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 192
    const v15, 0x7f0c019e

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 193
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 194
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 195
    const v15, 0x7f0c0049

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNeutralClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 196
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 199
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_a
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 200
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c0198

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 201
    const v15, 0x7f0c01a0

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 202
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 203
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 204
    const v15, 0x7f0c0049

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNeutralClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 205
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 208
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_b
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 209
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c0134

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 210
    const/4 v15, 0x0

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 211
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 214
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_c
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 215
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c01a1

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 216
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 217
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 218
    const v15, 0x7f0c0049

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNeutralClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 219
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 222
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_d
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 223
    const-string v16, "The device has no connection to the Internet. Please check network settings."

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 224
    const-string v16, "No Network Connection"

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 225
    const-string v16, "Settings"

    new-instance v17, Lcom/flixster/android/view/DialogBuilder$7;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/flixster/android/view/DialogBuilder$7;-><init>(Landroid/app/Activity;)V

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 229
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0c004a

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 230
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 233
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_e
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 235
    const-string v16, "The device seems to have limited connection to the Internet. Please check network settings and try again."

    .line 234
    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 236
    const-string v16, "Limited Network Connection"

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 237
    const-string v16, "Settings"

    new-instance v17, Lcom/flixster/android/view/DialogBuilder$8;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/flixster/android/view/DialogBuilder$8;-><init>(Landroid/app/Activity;)V

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 241
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0c004a

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 242
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 245
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_f
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 247
    const-string v16, "Flixster servers have encountered an unexpected problem. Please try again momentarily."

    .line 246
    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 248
    const-string v16, "Server Data Problem"

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const-string v16, "OK"

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 249
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 252
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_10
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 254
    const-string v16, "Flixster servers have encountered an unexpected problem. Please try again momentarily."

    .line 253
    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 255
    const-string v16, "Server API Problem"

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const-string v16, "OK"

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 256
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 259
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_11
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v15

    const v16, 0x3b9acd87

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 260
    .restart local v8       #msg:Ljava/lang/String;
    if-eqz v8, :cond_4

    .line 261
    sput-object v8, Lcom/flixster/android/view/DialogBuilder;->message:Ljava/lang/String;

    .line 263
    :cond_4
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->message:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const-string v16, "Problem Encountered"

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 264
    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const-string v16, "OK"

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 265
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 268
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    .end local v8           #msg:Ljava/lang/String;
    :sswitch_12
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 269
    const-string v16, "Your account was logged out due to inactivity. Please log in again."

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 270
    const-string v16, "User Account Problem"

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const-string v16, "OK"

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 271
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 274
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_13
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v15

    const v16, 0x3b9acd89

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 275
    .restart local v8       #msg:Ljava/lang/String;
    if-eqz v8, :cond_5

    .line 276
    sput-object v8, Lcom/flixster/android/view/DialogBuilder;->message:Ljava/lang/String;

    .line 278
    :cond_5
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->message:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const-string v16, "Streaming Problem"

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 279
    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const-string v16, "OK"

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 280
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 283
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    .end local v8           #msg:Ljava/lang/String;
    :sswitch_14
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v16, 0x7f0c017e

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 284
    const-string v16, "Playback Problem"

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const-string v16, "OK"

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 285
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 288
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_15
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 290
    const-string v16, "The app has encountered an unexpected problem. Please try again or restart the app. If the problem persists, please contact us through the link at the bottom of the Home tab."

    .line 289
    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    .line 291
    const-string v16, "Problem Encountered"

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    const-string v16, "OK"

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 292
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 295
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_16
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 296
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c012b

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 297
    const v15, 0x7f0c012c

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 298
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 299
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 300
    const v15, 0x7f0c012d

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 301
    const v15, 0x7f0c012e

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 302
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 305
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_17
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 306
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c0183

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 307
    const v15, 0x7f0c018b

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 308
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 309
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 310
    const v15, 0x7f0c0189

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 311
    const v15, 0x7f0c018a

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 312
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 315
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_18
    new-instance v7, Landroid/app/Dialog;

    const v15, 0x7f0d0085

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v15}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 316
    .local v7, dialog:Landroid/app/Dialog;
    const v15, 0x7f03005d

    invoke-virtual {v7, v15}, Landroid/app/Dialog;->setContentView(I)V

    .line 317
    const/4 v15, 0x1

    invoke-virtual {v7, v15}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 318
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v7, v15}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 319
    const v15, 0x7f070199

    invoke-virtual {v7, v15}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v15

    new-instance v16, Lcom/flixster/android/view/DialogBuilder$9;

    invoke-direct/range {v16 .. v16}, Lcom/flixster/android/view/DialogBuilder$9;-><init>()V

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    const v15, 0x7f070198

    invoke-virtual {v7, v15}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 326
    .local v10, pickYourMovie:Landroid/widget/ImageView;
    new-instance v15, Lcom/flixster/android/view/DialogBuilder$10;

    invoke-direct {v15}, Lcom/flixster/android/view/DialogBuilder$10;-><init>()V

    invoke-virtual {v10, v15}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMskTooltipUrl()Ljava/lang/String;

    move-result-object v9

    .line 333
    .local v9, mskTooltipUrl:Ljava/lang/String;
    if-eqz v9, :cond_6

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_6

    .line 334
    new-instance v15, Lcom/flixster/android/model/Image;

    invoke-direct {v15, v9}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v10}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    :cond_6
    move-object v13, v7

    .line 336
    goto/16 :goto_0

    .line 339
    .end local v7           #dialog:Landroid/app/Dialog;
    .end local v9           #mskTooltipUrl:Ljava/lang/String;
    .end local v10           #pickYourMovie:Landroid/widget/ImageView;
    :sswitch_19
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 340
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c00f4

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 341
    const v15, 0x7f0c00f5

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 342
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 343
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 344
    const v15, 0x7f0c0049

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNeutralClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 345
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 348
    .end local v6           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_1a
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 349
    .restart local v6       #builder:Landroid/app/AlertDialog$Builder;
    const v15, 0x7f0c0156

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 350
    const v15, 0x7f0c0157

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 351
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 352
    sget-object v15, Lcom/flixster/android/view/DialogBuilder;->dialogEventsCancel:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 353
    const v15, 0x7f0c0049

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/flixster/android/view/DialogBuilder;->dialogEventsNeutralClick:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 354
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    goto/16 :goto_0

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x3b9ac9ff -> :sswitch_0
        0x3b9aca0a -> :sswitch_17
        0x3b9aca64 -> :sswitch_1
        0x3b9aca65 -> :sswitch_2
        0x3b9aca66 -> :sswitch_3
        0x3b9acac8 -> :sswitch_5
        0x3b9acac9 -> :sswitch_6
        0x3b9acaca -> :sswitch_7
        0x3b9acacb -> :sswitch_9
        0x3b9acacc -> :sswitch_a
        0x3b9acacd -> :sswitch_8
        0x3b9acace -> :sswitch_b
        0x3b9acacf -> :sswitch_c
        0x3b9acb2c -> :sswitch_4
        0x3b9acb90 -> :sswitch_19
        0x3b9acb91 -> :sswitch_1a
        0x3b9acd84 -> :sswitch_d
        0x3b9acd85 -> :sswitch_f
        0x3b9acd86 -> :sswitch_10
        0x3b9acd87 -> :sswitch_11
        0x3b9acd88 -> :sswitch_12
        0x3b9acd89 -> :sswitch_13
        0x3b9acd8a -> :sswitch_14
        0x3b9acd8b -> :sswitch_15
        0x3b9acd8c -> :sswitch_e
        0x3b9acde8 -> :sswitch_16
        0x3b9acde9 -> :sswitch_18
    .end sparse-switch
.end method

.method private static showDialog(Landroid/app/AlertDialog$Builder;Landroid/app/Activity;)V
    .locals 1
    .parameter "dialogBuilder"
    .parameter "activity"

    .prologue
    .line 504
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/flixster/android/view/DialogBuilder;->showDialog(Landroid/app/AlertDialog$Builder;Landroid/app/Activity;Z)V

    .line 505
    return-void
.end method

.method private static showDialog(Landroid/app/AlertDialog$Builder;Landroid/app/Activity;Z)V
    .locals 1
    .parameter "dialogBuilder"
    .parameter "activity"
    .parameter "isDialogClickable"

    .prologue
    .line 510
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 511
    new-instance v0, Lcom/flixster/android/view/DialogBuilder$14;

    invoke-direct {v0, p1, p0, p2}, Lcom/flixster/android/view/DialogBuilder$14;-><init>(Landroid/app/Activity;Landroid/app/AlertDialog$Builder;Z)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 530
    :cond_0
    return-void
.end method

.method private static showInfoDialog(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "activity"
    .parameter "title"
    .parameter "message"

    .prologue
    .line 495
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 501
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 499
    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "OK"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 500
    .local v0, dialogBuilder:Landroid/app/AlertDialog$Builder;
    invoke-static {v0, p0}, Lcom/flixster/android/view/DialogBuilder;->showDialog(Landroid/app/AlertDialog$Builder;Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public static showNonWifiStreamUnsupported(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 485
    const v0, 0x7f0c0176

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 486
    const v1, 0x7f0c0178

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 485
    invoke-static {p0, v0, v1}, Lcom/flixster/android/view/DialogBuilder;->showInfoDialog(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    return-void
.end method

.method public static showStreamUnsupported(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 480
    const v0, 0x7f0c0176

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 481
    const v1, 0x7f0c0177

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 480
    invoke-static {p0, v0, v1}, Lcom/flixster/android/view/DialogBuilder;->showInfoDialog(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    return-void
.end method

.method public static showStreamUnsupportedOnRootedDevices(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 490
    const v0, 0x7f0c0176

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 491
    const v1, 0x7f0c0179

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 490
    invoke-static {p0, v0, v1}, Lcom/flixster/android/view/DialogBuilder;->showInfoDialog(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    return-void
.end method

.method public static showTermsOfService(Landroid/app/Activity;Lcom/flixster/android/view/DialogBuilder$DialogListener;)V
    .locals 7
    .parameter "activity"
    .parameter "listener"

    .prologue
    const/4 v6, 0x1

    .line 453
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 477
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    const v3, 0x7f0c0184

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 458
    .local v1, msg:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    .line 460
    .local v2, s:Landroid/text/Spanned;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 461
    const v4, 0x7f0c0183

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 462
    const v4, 0x7f0c0185

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/flixster/android/view/DialogBuilder$11;

    invoke-direct {v5, p1}, Lcom/flixster/android/view/DialogBuilder$11;-><init>(Lcom/flixster/android/view/DialogBuilder$DialogListener;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 466
    const v4, 0x7f0c0186

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/flixster/android/view/DialogBuilder$12;

    invoke-direct {v5, p1}, Lcom/flixster/android/view/DialogBuilder$12;-><init>(Lcom/flixster/android/view/DialogBuilder$DialogListener;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 470
    new-instance v4, Lcom/flixster/android/view/DialogBuilder$13;

    invoke-direct {v4, p1}, Lcom/flixster/android/view/DialogBuilder$13;-><init>(Lcom/flixster/android/view/DialogBuilder$DialogListener;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 476
    .local v0, dialogBuilder:Landroid/app/AlertDialog$Builder;
    invoke-static {v0, p0, v6}, Lcom/flixster/android/view/DialogBuilder;->showDialog(Landroid/app/AlertDialog$Builder;Landroid/app/Activity;Z)V

    goto :goto_0
.end method
