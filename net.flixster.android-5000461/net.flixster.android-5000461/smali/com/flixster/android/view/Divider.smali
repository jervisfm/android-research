.class public Lcom/flixster/android/view/Divider;
.super Landroid/view/View;
.source "Divider.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 11
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 12
    invoke-virtual {p0}, Lcom/flixster/android/view/Divider;->load()V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    invoke-virtual {p0}, Lcom/flixster/android/view/Divider;->load()V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    invoke-virtual {p0}, Lcom/flixster/android/view/Divider;->load()V

    .line 23
    return-void
.end method


# virtual methods
.method public load()V
    .locals 4

    .prologue
    .line 26
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/flixster/android/view/Divider;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 27
    const v3, 0x7f0a0025

    .line 26
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/Divider;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 28
    const v0, 0x7f09001a

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/Divider;->setBackgroundResource(I)V

    .line 29
    return-void
.end method
