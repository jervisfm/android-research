.class Lcom/flixster/android/view/DownloadPanel$1;
.super Landroid/os/Handler;
.source "DownloadPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/view/DownloadPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/view/DownloadPanel;


# direct methods
.method constructor <init>(Lcom/flixster/android/view/DownloadPanel;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/view/DownloadPanel$1;->this$0:Lcom/flixster/android/view/DownloadPanel;

    .line 90
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    .line 93
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 94
    .local v1, size:I
    iget-object v2, p0, Lcom/flixster/android/view/DownloadPanel$1;->this$0:Lcom/flixster/android/view/DownloadPanel;

    #getter for: Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lcom/flixster/android/view/DownloadPanel;->access$0(Lcom/flixster/android/view/DownloadPanel;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    invoke-virtual {v2, v1}, Lnet/flixster/android/model/LockerRight;->setDownloadAssetSize(I)V

    .line 95
    iget-object v2, p0, Lcom/flixster/android/view/DownloadPanel$1;->this$0:Lcom/flixster/android/view/DownloadPanel;

    invoke-virtual {v2}, Lcom/flixster/android/view/DownloadPanel;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c018e

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/flixster/android/view/DownloadPanel$1;->this$0:Lcom/flixster/android/view/DownloadPanel;

    #getter for: Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v6}, Lcom/flixster/android/view/DownloadPanel;->access$0(Lcom/flixster/android/view/DownloadPanel;)Lnet/flixster/android/model/LockerRight;

    move-result-object v6

    invoke-virtual {v6}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetSize()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, message:Ljava/lang/String;
    iget-object v2, p0, Lcom/flixster/android/view/DownloadPanel$1;->this$0:Lcom/flixster/android/view/DownloadPanel;

    #getter for: Lcom/flixster/android/view/DownloadPanel;->downloadPanel:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/flixster/android/view/DownloadPanel;->access$1(Lcom/flixster/android/view/DownloadPanel;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    return-void
.end method
