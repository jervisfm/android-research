.class public Lcom/flixster/android/view/DownloadPanel;
.super Landroid/widget/RelativeLayout;
.source "DownloadPanel.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private downloadDelete:Landroid/widget/ImageButton;

.field private downloadPanel:Landroid/widget/TextView;

.field private final downloadSizeCallback:Landroid/os/Handler;

.field private right:Lnet/flixster/android/model/LockerRight;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 90
    new-instance v0, Lcom/flixster/android/view/DownloadPanel$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/DownloadPanel$1;-><init>(Lcom/flixster/android/view/DownloadPanel;)V

    iput-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->downloadSizeCallback:Landroid/os/Handler;

    .line 29
    iput-object p1, p0, Lcom/flixster/android/view/DownloadPanel;->context:Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Lcom/flixster/android/view/DownloadPanel;->initialize()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    new-instance v0, Lcom/flixster/android/view/DownloadPanel$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/DownloadPanel$1;-><init>(Lcom/flixster/android/view/DownloadPanel;)V

    iput-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->downloadSizeCallback:Landroid/os/Handler;

    .line 35
    iput-object p1, p0, Lcom/flixster/android/view/DownloadPanel;->context:Landroid/content/Context;

    .line 36
    invoke-direct {p0}, Lcom/flixster/android/view/DownloadPanel;->initialize()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 90
    new-instance v0, Lcom/flixster/android/view/DownloadPanel$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/DownloadPanel$1;-><init>(Lcom/flixster/android/view/DownloadPanel;)V

    iput-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->downloadSizeCallback:Landroid/os/Handler;

    .line 41
    iput-object p1, p0, Lcom/flixster/android/view/DownloadPanel;->context:Landroid/content/Context;

    .line 42
    invoke-direct {p0}, Lcom/flixster/android/view/DownloadPanel;->initialize()V

    .line 43
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/view/DownloadPanel;)Lnet/flixster/android/model/LockerRight;
    .locals 1
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/view/DownloadPanel;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->downloadPanel:Landroid/widget/TextView;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->context:Landroid/content/Context;

    const v1, 0x7f030024

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 47
    const v0, 0x7f07006c

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/DownloadPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->downloadPanel:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f07006d

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/DownloadPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->downloadDelete:Landroid/widget/ImageButton;

    .line 49
    return-void
.end method


# virtual methods
.method public load(Lnet/flixster/android/model/LockerRight;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 1
    .parameter "right"
    .parameter "downloadClickListener"
    .parameter "downloadDeleteClickListener"

    .prologue
    .line 53
    iput-object p1, p0, Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;

    .line 54
    iget-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->downloadPanel:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->downloadDelete:Landroid/widget/ImageButton;

    invoke-virtual {v0, p3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->downloadPanel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 57
    iget-object v0, p0, Lcom/flixster/android/view/DownloadPanel;->downloadDelete:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 58
    invoke-virtual {p0}, Lcom/flixster/android/view/DownloadPanel;->refresh()V

    .line 59
    return-void
.end method

.method public refresh()V
    .locals 10

    .prologue
    const v9, 0x7f0c018e

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 63
    invoke-virtual {p0}, Lcom/flixster/android/view/DownloadPanel;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 64
    .local v2, res:Landroid/content/res/Resources;
    const/4 v1, 0x0

    .line 65
    .local v1, message:Ljava/lang/String;
    const/4 v0, 0x0

    .line 66
    .local v0, isDeleteVisible:Z
    iget-object v5, p0, Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;

    iget-wide v5, v5, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v5, v6}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 67
    const v5, 0x7f0c018f

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 68
    const/4 v0, 0x1

    .line 86
    :goto_0
    iget-object v5, p0, Lcom/flixster/android/view/DownloadPanel;->downloadPanel:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v5, p0, Lcom/flixster/android/view/DownloadPanel;->downloadDelete:Landroid/widget/ImageButton;

    if-eqz v0, :cond_5

    :goto_1
    invoke-virtual {v5, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 88
    return-void

    .line 69
    :cond_0
    iget-object v5, p0, Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;

    iget-wide v5, v5, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v5, v6}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 70
    const v5, 0x7f0c0190

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v7}, Lnet/flixster/android/model/LockerRight;->getDownloadedAssetSize()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 71
    const/4 v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    iget-object v5, p0, Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v5}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetSize()Ljava/lang/String;

    move-result-object v3

    .line 74
    .local v3, size:Ljava/lang/String;
    if-eqz v3, :cond_2

    const-string v5, ""

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 75
    :cond_2
    iget-object v5, p0, Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v5}, Lnet/flixster/android/model/LockerRight;->getDownloadUri()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    .line 76
    iget-object v5, p0, Lcom/flixster/android/view/DownloadPanel;->downloadSizeCallback:Landroid/os/Handler;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v7}, Lnet/flixster/android/model/LockerRight;->getEstimatedDownloadFileSize()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v6, v4, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 77
    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, ""

    aput-object v6, v5, v4

    invoke-virtual {v2, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 79
    :cond_3
    iget-object v5, p0, Lcom/flixster/android/view/DownloadPanel;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v5}, Lnet/flixster/android/model/LockerRight;->getDownloadUri()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/flixster/android/view/DownloadPanel;->downloadSizeCallback:Landroid/os/Handler;

    invoke-static {v5, v6}, Lcom/flixster/android/net/DownloadHelper;->getRemoteFileSize(Ljava/lang/String;Landroid/os/Handler;)V

    .line 80
    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, ""

    aput-object v6, v5, v4

    invoke-virtual {v2, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 83
    :cond_4
    new-array v5, v8, [Ljava/lang/Object;

    aput-object v3, v5, v4

    invoke-virtual {v2, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 87
    .end local v3           #size:Ljava/lang/String;
    :cond_5
    const/16 v4, 0x8

    goto :goto_1
.end method
