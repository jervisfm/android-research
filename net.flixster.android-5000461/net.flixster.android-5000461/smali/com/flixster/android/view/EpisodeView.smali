.class public Lcom/flixster/android/view/EpisodeView;
.super Landroid/widget/LinearLayout;
.source "EpisodeView.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private downloadPanel:Lcom/flixster/android/view/DownloadPanel;

.field private episodeId:J

.field private subheader:Landroid/widget/TextView;

.field private synopsis:Lcom/flixster/android/view/SynopsisView;

.field private title:Landroid/widget/TextView;

.field private watchNowPanel:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 30
    iput-object p1, p0, Lcom/flixster/android/view/EpisodeView;->context:Landroid/content/Context;

    .line 31
    invoke-direct {p0}, Lcom/flixster/android/view/EpisodeView;->initialize()V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    iput-object p1, p0, Lcom/flixster/android/view/EpisodeView;->context:Landroid/content/Context;

    .line 37
    invoke-direct {p0}, Lcom/flixster/android/view/EpisodeView;->initialize()V

    .line 38
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/flixster/android/view/EpisodeView;->context:Landroid/content/Context;

    const v1, 0x7f030025

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 48
    const v0, 0x1080062

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/EpisodeView;->setBackgroundResource(I)V

    .line 49
    const v0, 0x7f07006e

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/EpisodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/EpisodeView;->subheader:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f07006f

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/EpisodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/EpisodeView;->title:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f070070

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/EpisodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/view/SynopsisView;

    iput-object v0, p0, Lcom/flixster/android/view/EpisodeView;->synopsis:Lcom/flixster/android/view/SynopsisView;

    .line 52
    const v0, 0x7f070071

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/EpisodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/EpisodeView;->watchNowPanel:Landroid/widget/TextView;

    .line 53
    const v0, 0x7f070072

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/EpisodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/view/DownloadPanel;

    iput-object v0, p0, Lcom/flixster/android/view/EpisodeView;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    .line 54
    return-void
.end method


# virtual methods
.method public load(Lnet/flixster/android/model/LockerRight;Lnet/flixster/android/model/Episode;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 9
    .parameter "seasonRight"
    .parameter "episode"
    .parameter "watchNowClickListener"
    .parameter "downloadClickListener"
    .parameter "downloadDeleteClickListener"

    .prologue
    .line 58
    invoke-virtual {p2}, Lnet/flixster/android/model/Episode;->getId()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/flixster/android/view/EpisodeView;->episodeId:J

    .line 59
    invoke-virtual {p2}, Lnet/flixster/android/model/Episode;->getEpisodeNumber()I

    move-result v1

    .line 60
    .local v1, episodeNum:I
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->subheader:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/flixster/android/view/EpisodeView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c01a7

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->title:Landroid/widget/TextView;

    invoke-virtual {p2}, Lnet/flixster/android/model/Episode;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->synopsis:Lcom/flixster/android/view/SynopsisView;

    invoke-virtual {p2}, Lnet/flixster/android/model/Episode;->getSynopsis()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/flixster/android/view/SynopsisView;->load(Ljava/lang/String;)V

    .line 63
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v2

    .line 64
    .local v2, user:Lnet/flixster/android/model/User;
    iget-wide v3, p0, Lcom/flixster/android/view/EpisodeView;->episodeId:J

    invoke-virtual {p1, v3, v4}, Lnet/flixster/android/model/LockerRight;->getChildRightId(J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lnet/flixster/android/model/User;->getLockerRightFromRightId(J)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    .line 66
    .local v0, epRight:Lnet/flixster/android/model/LockerRight;
    if-nez v0, :cond_0

    .line 67
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EpisodeView.load missing right for episode id "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, p0, Lcom/flixster/android/view/EpisodeView;->episodeId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " number "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/flixster/android/view/EpisodeView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c01aa

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 69
    const/4 v5, 0x0

    .line 68
    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 69
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 70
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->watchNowPanel:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/flixster/android/view/DownloadPanel;->setVisibility(I)V

    .line 90
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/flixster/android/drm/PlaybackLogic;->isAssetPlayable(Lnet/flixster/android/model/LockerRight;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->watchNowPanel:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 78
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->watchNowPanel:Landroid/widget/TextView;

    invoke-virtual {v3, p3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->watchNowPanel:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 84
    :goto_1
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/flixster/android/drm/PlaybackLogic;->isAssetDownloadable(Lnet/flixster/android/model/LockerRight;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 85
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/flixster/android/view/DownloadPanel;->setVisibility(I)V

    .line 86
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    invoke-virtual {v3, v0, p4, p5}, Lcom/flixster/android/view/DownloadPanel;->load(Lnet/flixster/android/model/LockerRight;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 81
    :cond_1
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->watchNowPanel:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 88
    :cond_2
    iget-object v3, p0, Lcom/flixster/android/view/EpisodeView;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/flixster/android/view/DownloadPanel;->setVisibility(I)V

    goto :goto_0
.end method

.method public refresh()V
    .locals 4

    .prologue
    .line 100
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EpisodeView.refresh "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/flixster/android/view/EpisodeView;->episodeId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/flixster/android/view/EpisodeView;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    invoke-virtual {v0}, Lcom/flixster/android/view/DownloadPanel;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/flixster/android/view/EpisodeView;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    invoke-virtual {v0}, Lcom/flixster/android/view/DownloadPanel;->refresh()V

    .line 104
    :cond_0
    return-void
.end method

.method public refresh(J)V
    .locals 2
    .parameter "assetId"

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/flixster/android/view/EpisodeView;->episodeId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/flixster/android/view/EpisodeView;->refresh()V

    .line 97
    :cond_0
    return-void
.end method
