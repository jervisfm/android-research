.class public Lcom/flixster/android/view/FriendActivity$LviMovieViewUpdateHandler;
.super Landroid/os/Handler;
.source "FriendActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/view/FriendActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LviMovieViewUpdateHandler"
.end annotation


# instance fields
.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 120
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 121
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/flixster/android/view/FriendActivity$LviMovieViewUpdateHandler;->view:Landroid/view/View;

    const v3, 0x7f070106

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 122
    .local v1, poster:Landroid/widget/ImageView;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 123
    iget-object v2, p0, Lcom/flixster/android/view/FriendActivity$LviMovieViewUpdateHandler;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 124
    return-void
.end method

.method public setView(Landroid/view/View;)V
    .locals 0
    .parameter "v"

    .prologue
    .line 116
    iput-object p1, p0, Lcom/flixster/android/view/FriendActivity$LviMovieViewUpdateHandler;->view:Landroid/view/View;

    .line 117
    return-void
.end method
