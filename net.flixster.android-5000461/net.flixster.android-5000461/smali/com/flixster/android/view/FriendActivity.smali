.class public Lcom/flixster/android/view/FriendActivity;
.super Landroid/widget/RelativeLayout;
.source "FriendActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/view/FriendActivity$LviMovieViewUpdateHandler;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private image:Landroid/widget/ImageView;

.field private movie:Landroid/widget/RelativeLayout;

.field private rating:Landroid/widget/ImageView;

.field private review:Landroid/widget/TextView;

.field private titleAction:Landroid/widget/TextView;

.field private titleName:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 30
    iput-object p1, p0, Lcom/flixster/android/view/FriendActivity;->context:Landroid/content/Context;

    .line 31
    invoke-direct {p0}, Lcom/flixster/android/view/FriendActivity;->initialize()V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    iput-object p1, p0, Lcom/flixster/android/view/FriendActivity;->context:Landroid/content/Context;

    .line 37
    invoke-direct {p0}, Lcom/flixster/android/view/FriendActivity;->initialize()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    iput-object p1, p0, Lcom/flixster/android/view/FriendActivity;->context:Landroid/content/Context;

    .line 43
    invoke-direct {p0}, Lcom/flixster/android/view/FriendActivity;->initialize()V

    .line 44
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/view/FriendActivity;)Landroid/content/Context;
    .locals 1
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, Lcom/flixster/android/view/FriendActivity;->context:Landroid/content/Context;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/flixster/android/view/FriendActivity;->context:Landroid/content/Context;

    const v1, 0x7f03002e

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 48
    const v0, 0x1080062

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/FriendActivity;->setBackgroundResource(I)V

    .line 49
    const v0, 0x7f07008b

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/FriendActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/FriendActivity;->image:Landroid/widget/ImageView;

    .line 50
    const v0, 0x7f07008d

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/FriendActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/FriendActivity;->titleName:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f07008e

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/FriendActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/FriendActivity;->titleAction:Landroid/widget/TextView;

    .line 52
    const v0, 0x7f07008f

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/FriendActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/FriendActivity;->rating:Landroid/widget/ImageView;

    .line 53
    const v0, 0x7f070090

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/FriendActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/FriendActivity;->review:Landroid/widget/TextView;

    .line 54
    const v0, 0x7f070091

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/FriendActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/flixster/android/view/FriendActivity;->movie:Landroid/widget/RelativeLayout;

    .line 55
    return-void
.end method


# virtual methods
.method public load(Lnet/flixster/android/model/Review;)V
    .locals 17
    .parameter "r"

    .prologue
    .line 58
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->image:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lnet/flixster/android/model/Review;->getReviewerBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 59
    .local v7, bitmap:Landroid/graphics/Bitmap;
    if-eqz v7, :cond_0

    .line 60
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->image:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 62
    :cond_0
    move-object/from16 v0, p1

    iget-wide v12, v0, Lnet/flixster/android/model/Review;->userId:J

    .line 63
    .local v12, reviewerId:J
    new-instance v14, Lcom/flixster/android/view/FriendActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v12, v13}, Lcom/flixster/android/view/FriendActivity$1;-><init>(Lcom/flixster/android/view/FriendActivity;J)V

    .line 69
    .local v14, userClickListener:Landroid/view/View$OnClickListener;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->image:Landroid/widget/ImageView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 70
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->image:Landroid/widget/ImageView;

    invoke-virtual {v2, v14}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->titleName:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v3, v0, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->titleName:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 73
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->titleName:Landroid/widget/TextView;

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->titleAction:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/flixster/android/view/FriendActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Review;->getActionId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->rating:Landroid/widget/ImageView;

    sget-object v3, Lnet/flixster/android/Flixster;->RATING_SMALL_R:[I

    move-object/from16 v0, p1

    iget-wide v4, v0, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v15, 0x4000

    mul-double/2addr v4, v15

    double-to-int v4, v4

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    move-object/from16 v0, p1

    iget-object v2, v0, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 78
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->review:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v3, v0, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v8

    .line 83
    .local v8, m:Lnet/flixster/android/model/Movie;
    if-eqz v8, :cond_1

    .line 84
    new-instance v1, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 85
    .local v1, lviMoview:Lnet/flixster/android/lvi/LviMovie;
    iput-object v8, v1, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 86
    new-instance v6, Lcom/flixster/android/view/FriendActivity$LviMovieViewUpdateHandler;

    invoke-direct {v6}, Lcom/flixster/android/view/FriendActivity$LviMovieViewUpdateHandler;-><init>()V

    .line 87
    .local v6, updateHandler:Lcom/flixster/android/view/FriendActivity$LviMovieViewUpdateHandler;
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flixster/android/view/FriendActivity;->context:Landroid/content/Context;

    move-object/from16 v4, p0

    invoke-virtual/range {v1 .. v6}, Lnet/flixster/android/lvi/LviMovie;->getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;

    move-result-object v11

    .line 88
    .local v11, movieView:Landroid/view/View;
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 89
    const v2, 0x1080062

    invoke-virtual {v11, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 90
    invoke-virtual {v8}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v9

    .line 91
    .local v9, movieId:J
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/flixster/android/view/FriendActivity;->setFocusable(Z)V

    .line 92
    new-instance v2, Lcom/flixster/android/view/FriendActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v9, v10}, Lcom/flixster/android/view/FriendActivity$2;-><init>(Lcom/flixster/android/view/FriendActivity;J)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/flixster/android/view/FriendActivity;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    invoke-virtual {v6, v11}, Lcom/flixster/android/view/FriendActivity$LviMovieViewUpdateHandler;->setView(Landroid/view/View;)V

    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->movie:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v11}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 101
    .end local v1           #lviMoview:Lnet/flixster/android/lvi/LviMovie;
    .end local v6           #updateHandler:Lcom/flixster/android/view/FriendActivity$LviMovieViewUpdateHandler;
    .end local v9           #movieId:J
    .end local v11           #movieView:Landroid/view/View;
    :cond_1
    return-void

    .line 80
    .end local v8           #m:Lnet/flixster/android/model/Movie;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flixster/android/view/FriendActivity;->review:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
