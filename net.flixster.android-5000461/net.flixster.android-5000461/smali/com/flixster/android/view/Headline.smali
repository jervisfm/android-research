.class public Lcom/flixster/android/view/Headline;
.super Landroid/widget/RelativeLayout;
.source "Headline.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private image:Landroid/widget/ImageView;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 23
    iput-object p1, p0, Lcom/flixster/android/view/Headline;->context:Landroid/content/Context;

    .line 24
    invoke-direct {p0}, Lcom/flixster/android/view/Headline;->initialize()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput-object p1, p0, Lcom/flixster/android/view/Headline;->context:Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Lcom/flixster/android/view/Headline;->initialize()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    iput-object p1, p0, Lcom/flixster/android/view/Headline;->context:Landroid/content/Context;

    .line 36
    invoke-direct {p0}, Lcom/flixster/android/view/Headline;->initialize()V

    .line 37
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/flixster/android/view/Headline;->context:Landroid/content/Context;

    const v1, 0x7f030034

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 41
    const v0, 0x1080062

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/Headline;->setBackgroundResource(I)V

    .line 42
    const v0, 0x7f0700b5

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/Headline;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/Headline;->image:Landroid/widget/ImageView;

    .line 43
    const v0, 0x7f0700b6

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/Headline;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/Headline;->title:Landroid/widget/TextView;

    .line 44
    return-void
.end method


# virtual methods
.method public load(Lcom/flixster/android/model/HeadlineItem;)V
    .locals 3
    .parameter "headline"

    .prologue
    .line 47
    iget-object v1, p0, Lcom/flixster/android/view/Headline;->image:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-interface {p1, v1, v2}, Lcom/flixster/android/model/HeadlineItem;->getBitmap(Landroid/widget/ImageView;Landroid/widget/ImageView$ScaleType;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 48
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 49
    iget-object v1, p0, Lcom/flixster/android/view/Headline;->image:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 50
    iget-object v1, p0, Lcom/flixster/android/view/Headline;->image:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/view/Headline;->title:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/flixster/android/model/HeadlineItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    return-void
.end method
