.class public Lcom/flixster/android/view/HighlightableImageView;
.super Landroid/widget/ImageView;
.source "HighlightableImageView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method


# virtual methods
.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 5
    .parameter "bm"

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/flixster/android/view/HighlightableImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 30
    .local v2, r:Landroid/content/res/Resources;
    const/4 v3, 0x2

    new-array v1, v3, [Landroid/graphics/drawable/Drawable;

    .line 31
    .local v1, layers:[Landroid/graphics/drawable/Drawable;
    const/4 v3, 0x0

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v4, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v4, v1, v3

    .line 32
    const/4 v3, 0x1

    const v4, 0x7f020116

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    aput-object v4, v1, v3

    .line 33
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 34
    .local v0, layerDrawable:Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {p0, v0}, Lcom/flixster/android/view/HighlightableImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 35
    return-void
.end method
