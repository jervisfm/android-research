.class public Lcom/flixster/android/view/LoadMore;
.super Landroid/widget/RelativeLayout;
.source "LoadMore.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private count:Landroid/widget/TextView;

.field private root:Landroid/widget/RelativeLayout;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 17
    iput-object p1, p0, Lcom/flixster/android/view/LoadMore;->context:Landroid/content/Context;

    .line 18
    invoke-direct {p0}, Lcom/flixster/android/view/LoadMore;->initialize()V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    iput-object p1, p0, Lcom/flixster/android/view/LoadMore;->context:Landroid/content/Context;

    .line 24
    invoke-direct {p0}, Lcom/flixster/android/view/LoadMore;->initialize()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-object p1, p0, Lcom/flixster/android/view/LoadMore;->context:Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Lcom/flixster/android/view/LoadMore;->initialize()V

    .line 31
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/flixster/android/view/LoadMore;->context:Landroid/content/Context;

    const v1, 0x7f03003f

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 35
    const v0, 0x7f0700dc

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/LoadMore;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/flixster/android/view/LoadMore;->root:Landroid/widget/RelativeLayout;

    .line 36
    const v0, 0x7f0700dd

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/LoadMore;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/LoadMore;->title:Landroid/widget/TextView;

    .line 37
    const v0, 0x7f0700de

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/LoadMore;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/LoadMore;->count:Landroid/widget/TextView;

    .line 38
    return-void
.end method


# virtual methods
.method public load(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter "title"
    .parameter "subtitle"

    .prologue
    .line 42
    if-eqz p1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/flixster/android/view/LoadMore;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    :cond_0
    if-eqz p2, :cond_1

    .line 46
    iget-object v0, p0, Lcom/flixster/android/view/LoadMore;->count:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    iget-object v0, p0, Lcom/flixster/android/view/LoadMore;->count:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/flixster/android/view/LoadMore;->count:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public load(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .parameter "title"
    .parameter "subtitle"
    .parameter "isMoviePadding"

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/flixster/android/view/LoadMore;->load(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    iget-object v1, p0, Lcom/flixster/android/view/LoadMore;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 56
    iget-object v2, p0, Lcom/flixster/android/view/LoadMore;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a002b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 55
    add-int v0, v1, v2

    .line 57
    .local v0, paddingPx:I
    iget-object v1, p0, Lcom/flixster/android/view/LoadMore;->root:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/flixster/android/view/LoadMore;->root:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/flixster/android/view/LoadMore;->root:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/flixster/android/view/LoadMore;->root:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 58
    return-void
.end method
