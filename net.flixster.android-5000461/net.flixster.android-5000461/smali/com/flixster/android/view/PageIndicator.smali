.class public Lcom/flixster/android/view/PageIndicator;
.super Landroid/view/View;
.source "PageIndicator.java"


# instance fields
.field private currPageNum:I

.field private dotDistance:I

.field private glowPaint:Landroid/graphics/Paint;

.field private normPaint:Landroid/graphics/Paint;

.field private numOfPages:I

.field private unitLength:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 18
    invoke-direct {p0}, Lcom/flixster/android/view/PageIndicator;->initialize()V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    invoke-direct {p0}, Lcom/flixster/android/view/PageIndicator;->initialize()V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    invoke-direct {p0}, Lcom/flixster/android/view/PageIndicator;->initialize()V

    .line 29
    return-void
.end method

.method private initialize()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 32
    invoke-virtual {p0}, Lcom/flixster/android/view/PageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/flixster/android/view/PageIndicator;->unitLength:I

    .line 33
    iget v0, p0, Lcom/flixster/android/view/PageIndicator;->unitLength:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/flixster/android/view/PageIndicator;->dotDistance:I

    .line 35
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/view/PageIndicator;->glowPaint:Landroid/graphics/Paint;

    .line 36
    iget-object v0, p0, Lcom/flixster/android/view/PageIndicator;->glowPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 37
    iget-object v0, p0, Lcom/flixster/android/view/PageIndicator;->glowPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 38
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/flixster/android/view/PageIndicator;->normPaint:Landroid/graphics/Paint;

    .line 39
    iget-object v0, p0, Lcom/flixster/android/view/PageIndicator;->normPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 40
    iget-object v0, p0, Lcom/flixster/android/view/PageIndicator;->normPaint:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 41
    return-void
.end method

.method private measureHeight(I)I
    .locals 5
    .parameter "measureSpec"

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 88
    .local v0, result:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 89
    .local v1, specMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 90
    .local v2, specSize:I
    const/high16 v3, 0x4000

    if-ne v3, v1, :cond_1

    .line 91
    move v0, v2

    .line 98
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    iget v3, p0, Lcom/flixster/android/view/PageIndicator;->unitLength:I

    mul-int/lit8 v3, v3, 0x4

    invoke-virtual {p0}, Lcom/flixster/android/view/PageIndicator;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/flixster/android/view/PageIndicator;->getPaddingBottom()I

    move-result v4

    add-int v0, v3, v4

    .line 94
    const/high16 v3, -0x8000

    if-ne v3, v1, :cond_0

    .line 95
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private measureWidth(I)I
    .locals 6
    .parameter "measureSpec"

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 73
    .local v0, result:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 74
    .local v1, specMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 75
    .local v2, specSize:I
    const/high16 v3, 0x4000

    if-ne v3, v1, :cond_1

    .line 76
    move v0, v2

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 78
    :cond_1
    iget v3, p0, Lcom/flixster/android/view/PageIndicator;->unitLength:I

    mul-int/lit8 v3, v3, 0x4

    iget v4, p0, Lcom/flixster/android/view/PageIndicator;->numOfPages:I

    iget v5, p0, Lcom/flixster/android/view/PageIndicator;->dotDistance:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/flixster/android/view/PageIndicator;->getPaddingLeft()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/flixster/android/view/PageIndicator;->getPaddingRight()I

    move-result v4

    add-int v0, v3, v4

    .line 79
    const/high16 v3, -0x8000

    if-ne v3, v1, :cond_0

    .line 80
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .parameter "canvas"

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 58
    iget v3, p0, Lcom/flixster/android/view/PageIndicator;->unitLength:I

    iget v4, p0, Lcom/flixster/android/view/PageIndicator;->unitLength:I

    add-int v2, v3, v4

    .line 59
    .local v2, y:I
    move v1, v2

    .line 60
    .local v1, x:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget v3, p0, Lcom/flixster/android/view/PageIndicator;->numOfPages:I

    if-lt v0, v3, :cond_0

    .line 64
    return-void

    .line 61
    :cond_0
    int-to-float v4, v1

    int-to-float v5, v2

    iget v3, p0, Lcom/flixster/android/view/PageIndicator;->unitLength:I

    int-to-float v6, v3

    iget v3, p0, Lcom/flixster/android/view/PageIndicator;->currPageNum:I

    if-ne v0, v3, :cond_1

    iget-object v3, p0, Lcom/flixster/android/view/PageIndicator;->glowPaint:Landroid/graphics/Paint;

    :goto_1
    invoke-virtual {p1, v4, v5, v6, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 62
    iget v3, p0, Lcom/flixster/android/view/PageIndicator;->dotDistance:I

    add-int/2addr v1, v3

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :cond_1
    iget-object v3, p0, Lcom/flixster/android/view/PageIndicator;->normPaint:Landroid/graphics/Paint;

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 2
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/flixster/android/view/PageIndicator;->measureWidth(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/flixster/android/view/PageIndicator;->measureHeight(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/flixster/android/view/PageIndicator;->setMeasuredDimension(II)V

    .line 69
    return-void
.end method

.method public setCurrPageNum(I)V
    .locals 1
    .parameter "n"

    .prologue
    .line 49
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/flixster/android/view/PageIndicator;->numOfPages:I

    if-ge p1, v0, :cond_0

    .line 50
    iput p1, p0, Lcom/flixster/android/view/PageIndicator;->currPageNum:I

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/flixster/android/view/PageIndicator;->invalidate()V

    .line 53
    return-void
.end method

.method public setNumOfPages(I)V
    .locals 0
    .parameter "n"

    .prologue
    .line 44
    iput p1, p0, Lcom/flixster/android/view/PageIndicator;->numOfPages:I

    .line 45
    return-void
.end method
