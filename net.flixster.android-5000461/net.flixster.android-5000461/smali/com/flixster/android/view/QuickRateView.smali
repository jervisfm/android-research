.class public Lcom/flixster/android/view/QuickRateView;
.super Landroid/widget/RelativeLayout;
.source "QuickRateView.java"

# interfaces
.implements Landroid/widget/RatingBar$OnRatingBarChangeListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final context:Landroid/content/Context;

.field private final errorHandler:Landroid/os/Handler;

.field private friendRtScore:Landroid/widget/TextView;

.field private final isWts:Z

.field private movie:Lnet/flixster/android/model/Movie;

.field private movieActors:Landroid/widget/TextView;

.field private moviePoster:Landroid/widget/ImageView;

.field private movieRtScore:Landroid/widget/TextView;

.field private movieTitle:Landroid/widget/TextView;

.field private final parentHandler:Landroid/os/Handler;

.field private rating:D

.field private ratingBar:Landroid/widget/RatingBar;

.field private final successHandler:Landroid/os/Handler;

.field private wtsButton:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLandroid/os/Handler;)V
    .locals 1
    .parameter "context"
    .parameter "isWts"
    .parameter "parentHandler"

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 176
    new-instance v0, Lcom/flixster/android/view/QuickRateView$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/QuickRateView$1;-><init>(Lcom/flixster/android/view/QuickRateView;)V

    iput-object v0, p0, Lcom/flixster/android/view/QuickRateView;->successHandler:Landroid/os/Handler;

    .line 181
    new-instance v0, Lcom/flixster/android/view/QuickRateView$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/QuickRateView$2;-><init>(Lcom/flixster/android/view/QuickRateView;)V

    iput-object v0, p0, Lcom/flixster/android/view/QuickRateView;->errorHandler:Landroid/os/Handler;

    .line 37
    iput-object p1, p0, Lcom/flixster/android/view/QuickRateView;->context:Landroid/content/Context;

    .line 38
    iput-boolean p2, p0, Lcom/flixster/android/view/QuickRateView;->isWts:Z

    .line 39
    iput-object p3, p0, Lcom/flixster/android/view/QuickRateView;->parentHandler:Landroid/os/Handler;

    .line 40
    invoke-direct {p0}, Lcom/flixster/android/view/QuickRateView;->initialize()V

    .line 41
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/flixster/android/view/QuickRateView;->context:Landroid/content/Context;

    const v1, 0x7f030070

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 45
    const v0, 0x7f070106

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/QuickRateView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/QuickRateView;->moviePoster:Landroid/widget/ImageView;

    .line 46
    const v0, 0x7f070108

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/QuickRateView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/QuickRateView;->movieTitle:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f07010b

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/QuickRateView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/QuickRateView;->movieActors:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f07010a

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/QuickRateView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/QuickRateView;->movieRtScore:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f070195

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/QuickRateView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/QuickRateView;->friendRtScore:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f07020f

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/QuickRateView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/flixster/android/view/QuickRateView;->ratingBar:Landroid/widget/RatingBar;

    .line 51
    iget-object v0, p0, Lcom/flixster/android/view/QuickRateView;->ratingBar:Landroid/widget/RatingBar;

    invoke-virtual {v0, p0}, Landroid/widget/RatingBar;->setOnRatingBarChangeListener(Landroid/widget/RatingBar$OnRatingBarChangeListener;)V

    .line 52
    const v0, 0x7f070210

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/QuickRateView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/QuickRateView;->wtsButton:Landroid/widget/ImageView;

    .line 53
    iget-object v0, p0, Lcom/flixster/android/view/QuickRateView;->wtsButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method

.method private postReview(D)V
    .locals 6
    .parameter "rating"

    .prologue
    .line 169
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterId()Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, flixsterId:Ljava/lang/String;
    new-instance v1, Lnet/flixster/android/model/Review;

    invoke-direct {v1}, Lnet/flixster/android/model/Review;-><init>()V

    .line 171
    .local v1, review:Lnet/flixster/android/model/Review;
    iput-wide p1, v1, Lnet/flixster/android/model/Review;->stars:D

    .line 173
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->successHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/flixster/android/view/QuickRateView;->errorHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/flixster/android/view/QuickRateView;->movie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v0, v4, v1}, Lnet/flixster/android/data/ProfileDao;->postUserMovieReview(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/model/Review;)V

    .line 174
    return-void
.end method

.method private setFriendScore()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 105
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->movie:Lnet/flixster/android/model/Movie;

    const-string v7, "FRIENDS_RATED_COUNT"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->movie:Lnet/flixster/android/model/Movie;

    const-string v7, "FRIENDS_RATED_COUNT"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-lez v6, :cond_1

    .line 106
    invoke-virtual {p0}, Lcom/flixster/android/view/QuickRateView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0200eb

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 107
    .local v3, reviewIcon:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    invoke-virtual {v3, v8, v8, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 108
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->friendRtScore:Landroid/widget/TextView;

    invoke-virtual {v6, v3, v9, v9, v9}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 109
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->movie:Lnet/flixster/android/model/Movie;

    const-string v7, "FRIENDS_RATED_COUNT"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 110
    .local v0, friendsRatedCount:I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    .local v2, ratingCountBuilder:Ljava/lang/StringBuilder;
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    if-le v0, v10, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/flixster/android/view/QuickRateView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0090

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    :goto_0
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->friendRtScore:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->friendRtScore:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 137
    .end local v0           #friendsRatedCount:I
    .end local v2           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v3           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :goto_1
    return-void

    .line 115
    .restart local v0       #friendsRatedCount:I
    .restart local v2       #ratingCountBuilder:Ljava/lang/StringBuilder;
    .restart local v3       #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {p0}, Lcom/flixster/android/view/QuickRateView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c008f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 119
    .end local v0           #friendsRatedCount:I
    .end local v2           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v3           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_1
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->movie:Lnet/flixster/android/model/Movie;

    const-string v7, "FRIENDS_WTS_COUNT"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 120
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->movie:Lnet/flixster/android/model/Movie;

    const-string v7, "FRIENDS_WTS_COUNT"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-lez v6, :cond_3

    .line 121
    invoke-virtual {p0}, Lcom/flixster/android/view/QuickRateView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0200f7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 122
    .local v5, wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    invoke-virtual {v5, v8, v8, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 123
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->friendRtScore:Landroid/widget/TextView;

    invoke-virtual {v6, v5, v9, v9, v9}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 124
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->movie:Lnet/flixster/android/model/Movie;

    const-string v7, "FRIENDS_WTS_COUNT"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 125
    .local v1, friendsWantToSeeCount:I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .local v4, wantToSeeCountBuilder:Ljava/lang/StringBuilder;
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    if-le v1, v10, :cond_2

    .line 128
    invoke-virtual {p0}, Lcom/flixster/android/view/QuickRateView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0094

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :goto_2
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->friendRtScore:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->friendRtScore:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 130
    :cond_2
    invoke-virtual {p0}, Lcom/flixster/android/view/QuickRateView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0093

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 135
    .end local v1           #friendsWantToSeeCount:I
    .end local v4           #wantToSeeCountBuilder:Ljava/lang/StringBuilder;
    .end local v5           #wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    :cond_3
    iget-object v6, p0, Lcom/flixster/android/view/QuickRateView;->friendRtScore:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method private setRtScore()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 89
    iget-object v5, p0, Lcom/flixster/android/view/QuickRateView;->movie:Lnet/flixster/android/model/Movie;

    const-string v6, "rottenTomatoes"

    invoke-virtual {v5, v6}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 90
    iget-object v5, p0, Lcom/flixster/android/view/QuickRateView;->movie:Lnet/flixster/android/model/Movie;

    const-string v6, "rottenTomatoes"

    invoke-virtual {v5, v6}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 91
    .local v3, rtScore:I
    const/16 v5, 0x3c

    if-ge v3, v5, :cond_0

    const/4 v1, 0x1

    .line 92
    .local v1, isRotten:Z
    :goto_0
    iget-object v5, p0, Lcom/flixster/android/view/QuickRateView;->movieRtScore:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    if-eqz v1, :cond_1

    const v0, 0x7f0200ed

    .line 95
    .local v0, iconId:I
    :goto_1
    invoke-virtual {p0}, Lcom/flixster/android/view/QuickRateView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 96
    .local v2, rtIcon:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v2, v4, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 97
    iget-object v5, p0, Lcom/flixster/android/view/QuickRateView;->movieRtScore:Landroid/widget/TextView;

    invoke-virtual {v5, v2, v8, v8, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 98
    iget-object v5, p0, Lcom/flixster/android/view/QuickRateView;->movieRtScore:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    .end local v0           #iconId:I
    .end local v1           #isRotten:Z
    .end local v2           #rtIcon:Landroid/graphics/drawable/Drawable;
    .end local v3           #rtScore:I
    :goto_2
    return-void

    .restart local v3       #rtScore:I
    :cond_0
    move v1, v4

    .line 91
    goto :goto_0

    .line 94
    .restart local v1       #isRotten:Z
    :cond_1
    const v0, 0x7f0200de

    goto :goto_1

    .line 100
    .end local v1           #isRotten:Z
    .end local v3           #rtScore:I
    :cond_2
    iget-object v4, p0, Lcom/flixster/android/view/QuickRateView;->movieRtScore:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method


# virtual methods
.method public load(Lnet/flixster/android/model/Movie;)V
    .locals 6
    .parameter "movie"

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 57
    iput-object p1, p0, Lcom/flixster/android/view/QuickRateView;->movie:Lnet/flixster/android/model/Movie;

    .line 60
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getProfilePoster()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    .line 61
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->moviePoster:Landroid/widget/ImageView;

    const v3, 0x7f02014f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 66
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->movieTitle:Landroid/widget/TextView;

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    const-string v2, "MOVIE_ACTORS_SHORT"

    invoke-virtual {p1, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, actors:Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 70
    :cond_1
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->movieActors:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 76
    :goto_1
    invoke-direct {p0}, Lcom/flixster/android/view/QuickRateView;->setRtScore()V

    .line 77
    invoke-direct {p0}, Lcom/flixster/android/view/QuickRateView;->setFriendScore()V

    .line 79
    iget-boolean v2, p0, Lcom/flixster/android/view/QuickRateView;->isWts:Z

    if-eqz v2, :cond_4

    .line 80
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->ratingBar:Landroid/widget/RatingBar;

    invoke-virtual {v2, v5}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 81
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->wtsButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    :goto_2
    return-void

    .line 62
    .end local v0           #actors:Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->moviePoster:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Lnet/flixster/android/model/Movie;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v1

    .local v1, bitmap:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    .line 63
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->moviePoster:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 72
    .end local v1           #bitmap:Landroid/graphics/Bitmap;
    .restart local v0       #actors:Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->movieActors:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->movieActors:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 83
    :cond_4
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->ratingBar:Landroid/widget/RatingBar;

    invoke-virtual {v2, v4}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 84
    iget-object v2, p0, Lcom/flixster/android/view/QuickRateView;->wtsButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    const-wide/16 v2, 0x0

    .line 152
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 166
    :goto_0
    return-void

    .line 154
    :pswitch_0
    iget-wide v0, p0, Lcom/flixster/android/view/QuickRateView;->rating:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/flixster/android/view/QuickRateView;->wtsButton:Landroid/widget/ImageView;

    const v1, 0x7f0201de

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 156
    const-wide/high16 v0, 0x4016

    iput-wide v0, p0, Lcom/flixster/android/view/QuickRateView;->rating:D

    .line 157
    iget-object v0, p0, Lcom/flixster/android/view/QuickRateView;->parentHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/view/QuickRateView;->parentHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 163
    :goto_1
    iget-wide v0, p0, Lcom/flixster/android/view/QuickRateView;->rating:D

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/view/QuickRateView;->postReview(D)V

    goto :goto_0

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/flixster/android/view/QuickRateView;->wtsButton:Landroid/widget/ImageView;

    const v1, 0x7f0201df

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 160
    iput-wide v2, p0, Lcom/flixster/android/view/QuickRateView;->rating:D

    .line 161
    iget-object v0, p0, Lcom/flixster/android/view/QuickRateView;->parentHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/view/QuickRateView;->parentHandler:Landroid/os/Handler;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_1

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x7f070210
        :pswitch_0
    .end packed-switch
.end method

.method public onRatingChanged(Landroid/widget/RatingBar;FZ)V
    .locals 4
    .parameter "ratingBar"
    .parameter "rating"
    .parameter "fromUser"

    .prologue
    .line 141
    iget-wide v0, p0, Lcom/flixster/android/view/QuickRateView;->rating:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/flixster/android/view/QuickRateView;->parentHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/view/QuickRateView;->parentHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 146
    :cond_0
    :goto_0
    float-to-double v0, p2

    iput-wide v0, p0, Lcom/flixster/android/view/QuickRateView;->rating:D

    .line 147
    float-to-double v0, p2

    invoke-direct {p0, v0, v1}, Lcom/flixster/android/view/QuickRateView;->postReview(D)V

    .line 148
    return-void

    .line 143
    :cond_1
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/flixster/android/view/QuickRateView;->parentHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/flixster/android/view/QuickRateView;->parentHandler:Landroid/os/Handler;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
