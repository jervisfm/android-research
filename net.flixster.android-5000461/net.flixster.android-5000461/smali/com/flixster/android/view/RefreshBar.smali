.class public Lcom/flixster/android/view/RefreshBar;
.super Landroid/widget/RelativeLayout;
.source "RefreshBar.java"


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 16
    iput-object p1, p0, Lcom/flixster/android/view/RefreshBar;->context:Landroid/content/Context;

    .line 17
    invoke-direct {p0}, Lcom/flixster/android/view/RefreshBar;->load()V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    iput-object p1, p0, Lcom/flixster/android/view/RefreshBar;->context:Landroid/content/Context;

    .line 23
    invoke-direct {p0}, Lcom/flixster/android/view/RefreshBar;->load()V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    iput-object p1, p0, Lcom/flixster/android/view/RefreshBar;->context:Landroid/content/Context;

    .line 29
    invoke-direct {p0}, Lcom/flixster/android/view/RefreshBar;->load()V

    .line 30
    return-void
.end method

.method private load()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 33
    invoke-virtual {p0}, Lcom/flixster/android/view/RefreshBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200ce

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 34
    .local v0, refreshIcon:Landroid/graphics/drawable/Drawable;
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/flixster/android/view/RefreshBar;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 35
    .local v1, refreshText:Landroid/widget/TextView;
    const v2, 0x7f0c0118

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 36
    invoke-virtual {v1, v0, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 37
    invoke-virtual {p0}, Lcom/flixster/android/view/RefreshBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a001c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 38
    iget-object v2, p0, Lcom/flixster/android/view/RefreshBar;->context:Landroid/content/Context;

    const v3, 0x7f0d006f

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 40
    invoke-virtual {p0, v1}, Lcom/flixster/android/view/RefreshBar;->addView(Landroid/view/View;)V

    .line 41
    const v2, 0x7f020075

    invoke-virtual {p0, v2}, Lcom/flixster/android/view/RefreshBar;->setBackgroundResource(I)V

    .line 42
    const/16 v2, 0x11

    invoke-virtual {p0, v2}, Lcom/flixster/android/view/RefreshBar;->setGravity(I)V

    .line 43
    invoke-virtual {p0}, Lcom/flixster/android/view/RefreshBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/flixster/android/view/RefreshBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 44
    const v4, 0x7f0a001f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 43
    invoke-virtual {p0, v5, v2, v5, v3}, Lcom/flixster/android/view/RefreshBar;->setPadding(IIII)V

    .line 45
    return-void
.end method


# virtual methods
.method public setListener(Landroid/view/View$OnClickListener;)Lcom/flixster/android/view/RefreshBar;
    .locals 0
    .parameter "listener"

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/flixster/android/view/RefreshBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    return-object p0
.end method
