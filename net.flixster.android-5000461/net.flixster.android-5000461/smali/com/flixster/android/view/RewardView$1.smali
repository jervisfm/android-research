.class Lcom/flixster/android/view/RewardView$1;
.super Ljava/lang/Object;
.source "RewardView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flixster/android/view/RewardView;->load(ILjava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/view/RewardView;

.field private final synthetic val$rewardType:I


# direct methods
.method constructor <init>(Lcom/flixster/android/view/RewardView;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/view/RewardView$1;->this$0:Lcom/flixster/android/view/RewardView;

    iput p2, p0, Lcom/flixster/android/view/RewardView$1;->val$rewardType:I

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    const/4 v4, 0x0

    .line 110
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v1

    .line 112
    .local v1, user:Lnet/flixster/android/model/User;
    iget v2, p0, Lcom/flixster/android/view/RewardView$1;->val$rewardType:I

    packed-switch v2, :pswitch_data_0

    .line 141
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 114
    :pswitch_1
    iget-boolean v2, v1, Lnet/flixster/android/model/User;->isMskRateEligible:Z

    if-eqz v2, :cond_0

    .line 115
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/flixster/android/view/RewardView$1;->this$0:Lcom/flixster/android/view/RewardView;

    #getter for: Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/flixster/android/view/RewardView;->access$0(Lcom/flixster/android/view/RewardView;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lnet/flixster/android/QuickRatePage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 116
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "KEY_IS_WTS"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 117
    iget-object v2, p0, Lcom/flixster/android/view/RewardView$1;->this$0:Lcom/flixster/android/view/RewardView;

    #getter for: Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/flixster/android/view/RewardView;->access$0(Lcom/flixster/android/view/RewardView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 121
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_2
    iget-boolean v2, v1, Lnet/flixster/android/model/User;->isMskWtsEligible:Z

    if-eqz v2, :cond_0

    .line 122
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/flixster/android/view/RewardView$1;->this$0:Lcom/flixster/android/view/RewardView;

    #getter for: Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/flixster/android/view/RewardView;->access$0(Lcom/flixster/android/view/RewardView;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lnet/flixster/android/QuickRatePage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 123
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v2, "KEY_IS_WTS"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 124
    iget-object v2, p0, Lcom/flixster/android/view/RewardView$1;->this$0:Lcom/flixster/android/view/RewardView;

    #getter for: Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/flixster/android/view/RewardView;->access$0(Lcom/flixster/android/view/RewardView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 128
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_3
    iget-boolean v2, v1, Lnet/flixster/android/model/User;->isMskSmsEligible:Z

    if-eqz v2, :cond_0

    .line 129
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/flixster/android/view/RewardView$1;->this$0:Lcom/flixster/android/view/RewardView;

    #getter for: Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/flixster/android/view/RewardView;->access$0(Lcom/flixster/android/view/RewardView;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/flixster/android/msk/FriendSelectPage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 130
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v2, "KEY_IS_MSK"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 131
    iget-object v2, p0, Lcom/flixster/android/view/RewardView$1;->this$0:Lcom/flixster/android/view/RewardView;

    #getter for: Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/flixster/android/view/RewardView;->access$0(Lcom/flixster/android/view/RewardView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 135
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_4
    iget-boolean v2, v1, Lnet/flixster/android/model/User;->isMskFbInviteEligible:Z

    if-eqz v2, :cond_0

    .line 136
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/flixster/android/view/RewardView$1;->this$0:Lcom/flixster/android/view/RewardView;

    #getter for: Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/flixster/android/view/RewardView;->access$0(Lcom/flixster/android/view/RewardView;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/flixster/android/msk/FacebookInvitePage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/flixster/android/view/RewardView$1;->this$0:Lcom/flixster/android/view/RewardView;

    #getter for: Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/flixster/android/view/RewardView;->access$0(Lcom/flixster/android/view/RewardView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
