.class public Lcom/flixster/android/view/RewardView;
.super Landroid/widget/RelativeLayout;
.source "RewardView.java"


# static fields
.field public static final REWARD_FB_INVITE:I = 0x6

.field public static final REWARD_INSTALL_MOBILE:I = 0x5

.field public static final REWARD_RATE:I = 0x1

.field public static final REWARD_SMS:I = 0x3

.field public static final REWARD_UV_CREATE:I = 0x4

.field public static final REWARD_WTS:I = 0x2


# instance fields
.field private final context:Landroid/content/Context;

.field private description:Landroid/widget/TextView;

.field private image:Landroid/widget/ImageView;

.field private incentive:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 37
    iput-object p1, p0, Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Lcom/flixster/android/view/RewardView;->initialize()V

    .line 39
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/view/RewardView;)Landroid/content/Context;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flixster/android/view/RewardView;->context:Landroid/content/Context;

    const v1, 0x7f030074

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 43
    const v0, 0x1080062

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/RewardView;->setBackgroundResource(I)V

    .line 44
    const v0, 0x7f07019e

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/RewardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/RewardView;->image:Landroid/widget/ImageView;

    .line 45
    const v0, 0x7f07022d

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/RewardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/RewardView;->description:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f07022e

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/RewardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/RewardView;->incentive:Landroid/widget/TextView;

    .line 47
    return-void
.end method


# virtual methods
.method public load(ILjava/util/List;)V
    .locals 13
    .parameter "rewardType"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p2, earnedMovies:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v4

    .line 51
    .local v4, user:Lnet/flixster/android/model/User;
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v5

    invoke-virtual {v5}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 53
    .local v0, p:Lnet/flixster/android/model/Property;
    packed-switch p1, :pswitch_data_0

    .line 84
    :goto_0
    if-nez p2, :cond_c

    .line 85
    const/4 v5, 0x3

    if-eq p1, v5, :cond_0

    const/4 v5, 0x6

    if-ne p1, v5, :cond_b

    .line 86
    :cond_0
    iget-object v5, p0, Lcom/flixster/android/view/RewardView;->incentive:Landroid/widget/TextView;

    const v6, 0x7f0c014a

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 108
    :goto_1
    new-instance v5, Lcom/flixster/android/view/RewardView$1;

    invoke-direct {v5, p0, p1}, Lcom/flixster/android/view/RewardView$1;-><init>(Lcom/flixster/android/view/RewardView;I)V

    invoke-virtual {p0, v5}, Lcom/flixster/android/view/RewardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    return-void

    .line 55
    :pswitch_0
    iget-object v6, p0, Lcom/flixster/android/view/RewardView;->image:Landroid/widget/ImageView;

    iget-boolean v5, v4, Lnet/flixster/android/model/User;->isMskRateEligible:Z

    if-eqz v5, :cond_3

    const v5, 0x7f020172

    :goto_2
    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 56
    if-eqz v0, :cond_1

    iget v2, v0, Lnet/flixster/android/model/Property;->rewardsQuickRate:I

    .local v2, rewardThreshold:I
    if-nez v2, :cond_2

    .end local v2           #rewardThreshold:I
    :cond_1
    const/16 v2, 0x19

    .line 58
    .restart local v2       #rewardThreshold:I
    :cond_2
    iget-object v5, p0, Lcom/flixster/android/view/RewardView;->description:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/flixster/android/view/RewardView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c014d

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 55
    .end local v2           #rewardThreshold:I
    :cond_3
    const v5, 0x7f020173

    goto :goto_2

    .line 61
    :pswitch_1
    iget-object v6, p0, Lcom/flixster/android/view/RewardView;->image:Landroid/widget/ImageView;

    iget-boolean v5, v4, Lnet/flixster/android/model/User;->isMskWtsEligible:Z

    if-eqz v5, :cond_6

    const v5, 0x7f020178

    :goto_3
    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 62
    if-eqz v0, :cond_4

    iget v2, v0, Lnet/flixster/android/model/Property;->rewardsQuickWts:I

    .restart local v2       #rewardThreshold:I
    if-nez v2, :cond_5

    .end local v2           #rewardThreshold:I
    :cond_4
    const/16 v2, 0x19

    .line 64
    .restart local v2       #rewardThreshold:I
    :cond_5
    iget-object v5, p0, Lcom/flixster/android/view/RewardView;->description:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/flixster/android/view/RewardView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c014e

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 61
    .end local v2           #rewardThreshold:I
    :cond_6
    const v5, 0x7f020179

    goto :goto_3

    .line 67
    :pswitch_2
    iget-object v6, p0, Lcom/flixster/android/view/RewardView;->image:Landroid/widget/ImageView;

    iget-boolean v5, v4, Lnet/flixster/android/model/User;->isMskSmsEligible:Z

    if-eqz v5, :cond_7

    const v5, 0x7f020174

    :goto_4
    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 68
    iget-object v5, p0, Lcom/flixster/android/view/RewardView;->description:Landroid/widget/TextView;

    const v6, 0x7f0c014f

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 67
    :cond_7
    const v5, 0x7f020175

    goto :goto_4

    .line 71
    :pswitch_3
    iget-object v6, p0, Lcom/flixster/android/view/RewardView;->image:Landroid/widget/ImageView;

    iget-boolean v5, v4, Lnet/flixster/android/model/User;->isMskUvCreateEligible:Z

    if-eqz v5, :cond_8

    const v5, 0x7f020176

    :goto_5
    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 72
    iget-object v5, p0, Lcom/flixster/android/view/RewardView;->description:Landroid/widget/TextView;

    const v6, 0x7f0c0151

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 71
    :cond_8
    const v5, 0x7f020177

    goto :goto_5

    .line 75
    :pswitch_4
    iget-object v6, p0, Lcom/flixster/android/view/RewardView;->image:Landroid/widget/ImageView;

    iget-boolean v5, v4, Lnet/flixster/android/model/User;->isMskInstallMobileEligible:Z

    if-eqz v5, :cond_9

    const v5, 0x7f020170

    :goto_6
    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    iget-object v5, p0, Lcom/flixster/android/view/RewardView;->description:Landroid/widget/TextView;

    const v6, 0x7f0c0152

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 76
    :cond_9
    const v5, 0x7f020171

    goto :goto_6

    .line 80
    :pswitch_5
    iget-object v6, p0, Lcom/flixster/android/view/RewardView;->image:Landroid/widget/ImageView;

    iget-boolean v5, v4, Lnet/flixster/android/model/User;->isMskFbInviteEligible:Z

    if-eqz v5, :cond_a

    const v5, 0x7f0200ba

    :goto_7
    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 81
    iget-object v5, p0, Lcom/flixster/android/view/RewardView;->description:Landroid/widget/TextView;

    const v6, 0x7f0c0150

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 80
    :cond_a
    const v5, 0x7f0200bb

    goto :goto_7

    .line 88
    :cond_b
    iget-object v5, p0, Lcom/flixster/android/view/RewardView;->incentive:Landroid/widget/TextView;

    const v6, 0x7f0c0149

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 90
    :cond_c
    const/4 v5, 0x6

    if-ne p1, v5, :cond_e

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x3

    if-ge v5, v6, :cond_e

    .line 91
    iget-object v6, p0, Lcom/flixster/android/view/RewardView;->incentive:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/flixster/android/view/RewardView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 92
    const v8, 0x7f0c014b

    const/4 v5, 0x1

    new-array v9, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 93
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v5, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 94
    const-string v11, " "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 95
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    const/4 v12, 0x1

    if-ne v5, v12, :cond_d

    invoke-virtual {p0}, Lcom/flixster/android/view/RewardView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v12, 0x7f0c00fb

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_8
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 93
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v9, v10

    .line 91
    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 96
    :cond_d
    invoke-virtual {p0}, Lcom/flixster/android/view/RewardView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v12, 0x7f0c00fa

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_8

    .line 98
    :cond_e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .local v3, sb:Ljava/lang/StringBuilder;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_f

    .line 105
    iget-object v5, p0, Lcom/flixster/android/view/RewardView;->incentive:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/flixster/android/view/RewardView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c014c

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 99
    :cond_f
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/LockerRight;

    .line 100
    .local v1, r:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_10

    .line 101
    const-string v6, ", "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    :cond_10
    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
