.class public Lcom/flixster/android/view/SubHeader;
.super Landroid/widget/TextView;
.source "SubHeader.java"


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 15
    iput-object p1, p0, Lcom/flixster/android/view/SubHeader;->context:Landroid/content/Context;

    .line 16
    invoke-direct {p0}, Lcom/flixster/android/view/SubHeader;->initialize()V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput-object p1, p0, Lcom/flixster/android/view/SubHeader;->context:Landroid/content/Context;

    .line 22
    invoke-direct {p0}, Lcom/flixster/android/view/SubHeader;->initialize()V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    iput-object p1, p0, Lcom/flixster/android/view/SubHeader;->context:Landroid/content/Context;

    .line 28
    invoke-direct {p0}, Lcom/flixster/android/view/SubHeader;->initialize()V

    .line 29
    return-void
.end method

.method private initialize()V
    .locals 6

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/flixster/android/view/SubHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0056

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 33
    .local v1, l:I
    invoke-virtual {p0}, Lcom/flixster/android/view/SubHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0057

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 34
    .local v0, b:I
    invoke-virtual {p0}, Lcom/flixster/android/view/SubHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0058

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 35
    .local v2, r:I
    invoke-virtual {p0}, Lcom/flixster/android/view/SubHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0059

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 37
    .local v3, t:I
    const v4, 0x7f0201b5

    invoke-virtual {p0, v4}, Lcom/flixster/android/view/SubHeader;->setBackgroundResource(I)V

    .line 38
    const/16 v4, 0x10

    invoke-virtual {p0, v4}, Lcom/flixster/android/view/SubHeader;->setGravity(I)V

    .line 39
    invoke-virtual {p0, v1, v3, v2, v0}, Lcom/flixster/android/view/SubHeader;->setPadding(IIII)V

    .line 40
    iget-object v4, p0, Lcom/flixster/android/view/SubHeader;->context:Landroid/content/Context;

    const v5, 0x7f0d007f

    invoke-virtual {p0, v4, v5}, Lcom/flixster/android/view/SubHeader;->setTextAppearance(Landroid/content/Context;I)V

    .line 45
    return-void
.end method


# virtual methods
.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1
    .parameter "text"
    .parameter "type"

    .prologue
    .line 49
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 50
    return-void
.end method
