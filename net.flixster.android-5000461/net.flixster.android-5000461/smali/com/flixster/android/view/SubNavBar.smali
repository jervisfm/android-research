.class public Lcom/flixster/android/view/SubNavBar;
.super Landroid/widget/LinearLayout;
.source "SubNavBar.java"


# instance fields
.field private button1:Landroid/widget/TextView;

.field private button2:Landroid/widget/TextView;

.field private button3:Landroid/widget/TextView;

.field private button4:Landroid/widget/TextView;

.field private callback:Landroid/view/View$OnClickListener;

.field private final context:Landroid/content/Context;

.field private final internalListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 96
    new-instance v0, Lcom/flixster/android/view/SubNavBar$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/SubNavBar$1;-><init>(Lcom/flixster/android/view/SubNavBar;)V

    iput-object v0, p0, Lcom/flixster/android/view/SubNavBar;->internalListener:Landroid/view/View$OnClickListener;

    .line 17
    iput-object p1, p0, Lcom/flixster/android/view/SubNavBar;->context:Landroid/content/Context;

    .line 18
    invoke-direct {p0}, Lcom/flixster/android/view/SubNavBar;->initialize()V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    new-instance v0, Lcom/flixster/android/view/SubNavBar$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/SubNavBar$1;-><init>(Lcom/flixster/android/view/SubNavBar;)V

    iput-object v0, p0, Lcom/flixster/android/view/SubNavBar;->internalListener:Landroid/view/View$OnClickListener;

    .line 23
    iput-object p1, p0, Lcom/flixster/android/view/SubNavBar;->context:Landroid/content/Context;

    .line 24
    invoke-direct {p0}, Lcom/flixster/android/view/SubNavBar;->initialize()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 96
    new-instance v0, Lcom/flixster/android/view/SubNavBar$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/SubNavBar$1;-><init>(Lcom/flixster/android/view/SubNavBar;)V

    iput-object v0, p0, Lcom/flixster/android/view/SubNavBar;->internalListener:Landroid/view/View$OnClickListener;

    .line 29
    iput-object p1, p0, Lcom/flixster/android/view/SubNavBar;->context:Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Lcom/flixster/android/view/SubNavBar;->initialize()V

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/view/SubNavBar;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 13
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->callback:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->context:Landroid/content/Context;

    const v1, 0x7f030081

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 35
    const v0, 0x7f070258

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/SubNavBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button1:Landroid/widget/TextView;

    .line 36
    const v0, 0x7f070259

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/SubNavBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button2:Landroid/widget/TextView;

    .line 37
    const v0, 0x7f07025a

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/SubNavBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button3:Landroid/widget/TextView;

    .line 38
    const v0, 0x7f07025b

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/SubNavBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button4:Landroid/widget/TextView;

    .line 39
    return-void
.end method


# virtual methods
.method public load(Landroid/view/View$OnClickListener;II)V
    .locals 6
    .parameter "listener"
    .parameter "buttonLabel1"
    .parameter "buttonLabel2"

    .prologue
    const/4 v4, 0x0

    .line 42
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;IIII)V

    .line 43
    return-void
.end method

.method public load(Landroid/view/View$OnClickListener;III)V
    .locals 6
    .parameter "listener"
    .parameter "buttonLabel1"
    .parameter "buttonLabel2"
    .parameter "buttonLabel3"

    .prologue
    .line 46
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;IIII)V

    .line 47
    return-void
.end method

.method public load(Landroid/view/View$OnClickListener;IIII)V
    .locals 3
    .parameter "listener"
    .parameter "buttonLabel1"
    .parameter "buttonLabel2"
    .parameter "buttonLabel3"
    .parameter "buttonLabel4"

    .prologue
    const/4 v2, 0x0

    .line 50
    iput-object p1, p0, Lcom/flixster/android/view/SubNavBar;->callback:Landroid/view/View$OnClickListener;

    .line 51
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button1:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 52
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button2:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 53
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/flixster/android/view/SubNavBar;->internalListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/flixster/android/view/SubNavBar;->internalListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    if-lez p4, :cond_0

    .line 56
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button3:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(I)V

    .line 57
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button3:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/flixster/android/view/SubNavBar;->internalListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button3:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 60
    :cond_0
    if-lez p5, :cond_1

    .line 61
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button4:Landroid/widget/TextView;

    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(I)V

    .line 62
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button4:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/flixster/android/view/SubNavBar;->internalListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button4:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 65
    :cond_1
    return-void
.end method

.method public setSelectedButton(I)V
    .locals 3
    .parameter "selected"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 68
    packed-switch p1, :pswitch_data_0

    .line 94
    :goto_0
    return-void

    .line 70
    :pswitch_0
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button1:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 71
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button2:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 72
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button3:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 73
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button4:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_0

    .line 76
    :pswitch_1
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button1:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 77
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button2:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 78
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button3:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 79
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button4:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_0

    .line 82
    :pswitch_2
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button1:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 83
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button2:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 84
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button3:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 85
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button4:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_0

    .line 88
    :pswitch_3
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button1:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 89
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button2:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 90
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button3:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 91
    iget-object v0, p0, Lcom/flixster/android/view/SubNavBar;->button4:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
