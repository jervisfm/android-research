.class public Lcom/flixster/android/view/SynopsisView;
.super Landroid/widget/TextView;
.source "SynopsisView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final SYNOPSIS_LONG_LENGTH:I = 0xaa

.field public static final SYNOPSIS_SHORT_LENGTH:I = 0x46


# instance fields
.field private showLabel:Z

.field private synopsis:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-direct {p0}, Lcom/flixster/android/view/SynopsisView;->initialize()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-direct {p0}, Lcom/flixster/android/view/SynopsisView;->initialize()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-direct {p0}, Lcom/flixster/android/view/SynopsisView;->initialize()V

    .line 36
    return-void
.end method

.method private initialize()V
    .locals 0

    .prologue
    .line 39
    invoke-virtual {p0, p0}, Lcom/flixster/android/view/SynopsisView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    return-void
.end method

.method protected static setBoldSpan(Landroid/text/Spannable;II)Landroid/text/Spannable;
    .locals 2
    .parameter "spannable"
    .parameter "start"
    .parameter "end"

    .prologue
    .line 89
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 90
    .local v0, style:Landroid/text/style/StyleSpan;
    const/16 v1, 0x11

    invoke-interface {p0, v0, p1, p2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 91
    return-object p0
.end method

.method protected static setColorSpan(Landroid/text/Spannable;III)Landroid/text/Spannable;
    .locals 2
    .parameter "spannable"
    .parameter "start"
    .parameter "end"
    .parameter "color"

    .prologue
    .line 83
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 84
    .local v0, style:Landroid/text/style/ForegroundColorSpan;
    const/16 v1, 0x11

    invoke-interface {p0, v0, p1, p2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 85
    return-object p0
.end method


# virtual methods
.method public load(Ljava/lang/String;)V
    .locals 2
    .parameter "s"

    .prologue
    .line 44
    const/16 v0, 0x46

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/flixster/android/view/SynopsisView;->load(Ljava/lang/String;IZ)V

    .line 45
    return-void
.end method

.method public load(Ljava/lang/String;IZ)V
    .locals 6
    .parameter "s"
    .parameter "length"
    .parameter "showLabel"

    .prologue
    .line 48
    iput-boolean p3, p0, Lcom/flixster/android/view/SynopsisView;->showLabel:Z

    .line 49
    iput-object p1, p0, Lcom/flixster/android/view/SynopsisView;->synopsis:Ljava/lang/String;

    .line 50
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 51
    :cond_0
    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lcom/flixster/android/view/SynopsisView;->setVisibility(I)V

    .line 67
    :goto_0
    return-void

    .line 52
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v3, p2, :cond_2

    .line 53
    invoke-virtual {p0, p0}, Lcom/flixster/android/view/SynopsisView;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 55
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1, p2}, Lcom/flixster/android/utils/StringHelper;->ellipsize(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " More"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, ellipsizedText:Ljava/lang/String;
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 57
    .local v2, spannable:Landroid/text/Spannable;
    if-eqz p3, :cond_3

    .line 58
    invoke-virtual {p0}, Lcom/flixster/android/view/SynopsisView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0021

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, label:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    new-instance v2, Landroid/text/SpannableString;

    .end local v2           #spannable:Landroid/text/Spannable;
    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 61
    .restart local v2       #spannable:Landroid/text/Spannable;
    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/flixster/android/view/SynopsisView;->setBoldSpan(Landroid/text/Spannable;II)Landroid/text/Spannable;

    .line 63
    .end local v1           #label:Ljava/lang/String;
    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p2

    .line 64
    add-int/lit8 v3, p2, -0x4

    invoke-virtual {p0}, Lcom/flixster/android/view/SynopsisView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09001f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v2, v3, p2, v4}, Lcom/flixster/android/view/SynopsisView;->setColorSpan(Landroid/text/Spannable;III)Landroid/text/Spannable;

    .line 65
    invoke-virtual {p0, v2}, Lcom/flixster/android/view/SynopsisView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    .line 71
    iget-boolean v2, p0, Lcom/flixster/android/view/SynopsisView;->showLabel:Z

    if-eqz v2, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/flixster/android/view/SynopsisView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0021

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .local v0, label:Ljava/lang/String;
    move-object v1, v0

    .line 74
    .local v1, text:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 75
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/flixster/android/view/SynopsisView;->synopsis:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 76
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/flixster/android/view/SynopsisView;->setBoldSpan(Landroid/text/Spannable;II)Landroid/text/Spannable;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/flixster/android/view/SynopsisView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    .end local v0           #label:Ljava/lang/String;
    .end local v1           #text:Ljava/lang/String;
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v2, p0, Lcom/flixster/android/view/SynopsisView;->synopsis:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/flixster/android/view/SynopsisView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
