.class public Lcom/flixster/android/view/UnfulfillableFooter;
.super Landroid/widget/TextView;
.source "UnfulfillableFooter.java"


# static fields
.field private static final PADDING_10_DP:F = 10.0f

.field private static final PADDING_5_DP:F = 5.0f


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 17
    iput-object p1, p0, Lcom/flixster/android/view/UnfulfillableFooter;->context:Landroid/content/Context;

    .line 18
    invoke-virtual {p0}, Lcom/flixster/android/view/UnfulfillableFooter;->load()V

    .line 19
    return-void
.end method


# virtual methods
.method public load()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f00

    .line 22
    invoke-virtual {p0}, Lcom/flixster/android/view/UnfulfillableFooter;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    .line 23
    .local v0, densityScale:F
    const/high16 v3, 0x40a0

    mul-float/2addr v3, v0

    add-float/2addr v3, v4

    float-to-int v2, v3

    .line 24
    .local v2, padding5:I
    const/high16 v3, 0x4120

    mul-float/2addr v3, v0

    add-float/2addr v3, v4

    float-to-int v1, v3

    .line 25
    .local v1, padding10:I
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v2, v3}, Lcom/flixster/android/view/UnfulfillableFooter;->setPadding(IIII)V

    .line 27
    invoke-virtual {p0}, Lcom/flixster/android/view/UnfulfillableFooter;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c018c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/flixster/android/view/UnfulfillableFooter;->setText(Ljava/lang/CharSequence;)V

    .line 28
    iget-object v3, p0, Lcom/flixster/android/view/UnfulfillableFooter;->context:Landroid/content/Context;

    const v4, 0x7f0d007c

    invoke-virtual {p0, v3, v4}, Lcom/flixster/android/view/UnfulfillableFooter;->setTextAppearance(Landroid/content/Context;I)V

    .line 29
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/flixster/android/view/UnfulfillableFooter;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 30
    return-void
.end method
