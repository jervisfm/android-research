.class public Lcom/flixster/android/view/UvFooter;
.super Landroid/widget/TextView;
.source "UvFooter.java"


# static fields
.field private static final PADDING_10_DP:F = 10.0f

.field private static final PADDING_5_DP:F = 5.0f


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object p1, p0, Lcom/flixster/android/view/UvFooter;->context:Landroid/content/Context;

    .line 20
    invoke-virtual {p0}, Lcom/flixster/android/view/UvFooter;->load()V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    iput-object p1, p0, Lcom/flixster/android/view/UvFooter;->context:Landroid/content/Context;

    .line 26
    invoke-virtual {p0}, Lcom/flixster/android/view/UvFooter;->load()V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    iput-object p1, p0, Lcom/flixster/android/view/UvFooter;->context:Landroid/content/Context;

    .line 32
    invoke-virtual {p0}, Lcom/flixster/android/view/UvFooter;->load()V

    .line 33
    return-void
.end method


# virtual methods
.method public load()V
    .locals 7

    .prologue
    const/high16 v5, 0x3f00

    const/4 v6, 0x0

    .line 36
    invoke-virtual {p0}, Lcom/flixster/android/view/UvFooter;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v0, v4, Landroid/util/DisplayMetrics;->density:F

    .line 37
    .local v0, densityScale:F
    const/high16 v4, 0x40a0

    mul-float/2addr v4, v0

    add-float/2addr v4, v5

    float-to-int v2, v4

    .line 38
    .local v2, padding5:I
    const/high16 v4, 0x4120

    mul-float/2addr v4, v0

    add-float/2addr v4, v5

    float-to-int v1, v4

    .line 40
    .local v1, padding10:I
    invoke-virtual {p0}, Lcom/flixster/android/view/UvFooter;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0201db

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 41
    .local v3, uvLogo:Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0, v3, v6, v6, v6}, Lcom/flixster/android/view/UvFooter;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 42
    invoke-virtual {p0, v1}, Lcom/flixster/android/view/UvFooter;->setCompoundDrawablePadding(I)V

    .line 43
    invoke-virtual {p0, v1, v2, v2, v2}, Lcom/flixster/android/view/UvFooter;->setPadding(IIII)V

    .line 45
    invoke-virtual {p0}, Lcom/flixster/android/view/UvFooter;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c018d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/flixster/android/view/UvFooter;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget-object v4, p0, Lcom/flixster/android/view/UvFooter;->context:Landroid/content/Context;

    const v5, 0x7f0d007c

    invoke-virtual {p0, v4, v5}, Lcom/flixster/android/view/UvFooter;->setTextAppearance(Landroid/content/Context;I)V

    .line 47
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/flixster/android/view/UvFooter;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 48
    return-void
.end method
