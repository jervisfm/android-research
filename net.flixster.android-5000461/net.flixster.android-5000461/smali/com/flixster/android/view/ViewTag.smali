.class public Lcom/flixster/android/view/ViewTag;
.super Ljava/lang/Object;
.source "ViewTag.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Landroid/widget/ImageView;)Ljava/lang/String;
    .locals 4
    .parameter "imageView"

    .prologue
    .line 11
    invoke-virtual {p0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 12
    .local v0, tag:Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 13
    check-cast v0, Ljava/lang/String;

    .line 16
    .end local v0           #tag:Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 15
    .restart local v0       #tag:Ljava/lang/Object;
    :cond_0
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ViewTag.get type mismatch: expected String, actual "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static set(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 0
    .parameter "imageView"
    .parameter "imageUrl"

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 22
    return-void
.end method
