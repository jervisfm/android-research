.class public Lcom/flixster/android/view/gtv/CriticReview;
.super Landroid/widget/RelativeLayout;
.source "CriticReview.java"


# instance fields
.field private author:Landroid/widget/TextView;

.field private final context:Landroid/content/Context;

.field private icon:Landroid/widget/ImageView;

.field private review:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 20
    iput-object p1, p0, Lcom/flixster/android/view/gtv/CriticReview;->context:Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/CriticReview;->initialize()V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    iput-object p1, p0, Lcom/flixster/android/view/gtv/CriticReview;->context:Landroid/content/Context;

    .line 27
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/CriticReview;->initialize()V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    iput-object p1, p0, Lcom/flixster/android/view/gtv/CriticReview;->context:Landroid/content/Context;

    .line 33
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/CriticReview;->initialize()V

    .line 34
    return-void
.end method

.method private getAuthorString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "name"
    .parameter "source"

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "- "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 53
    .local v0, sb:Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    if-eqz p2, :cond_0

    .line 56
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    :cond_0
    if-eqz p2, :cond_1

    .line 60
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/flixster/android/view/gtv/CriticReview;->context:Landroid/content/Context;

    const v1, 0x7f030021

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    const v0, 0x7f070062

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/CriticReview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/CriticReview;->review:Landroid/widget/TextView;

    .line 39
    const v0, 0x7f070063

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/CriticReview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/CriticReview;->author:Landroid/widget/TextView;

    .line 40
    const v0, 0x7f070061

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/CriticReview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/CriticReview;->icon:Landroid/widget/ImageView;

    .line 41
    return-void
.end method


# virtual methods
.method public load(Lnet/flixster/android/model/Review;)V
    .locals 4
    .parameter "r"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/flixster/android/view/gtv/CriticReview;->review:Landroid/widget/TextView;

    iget-object v1, p1, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p0, Lcom/flixster/android/view/gtv/CriticReview;->author:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p1, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    iget-object v3, p1, Lnet/flixster/android/model/Review;->source:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/flixster/android/view/gtv/CriticReview;->getAuthorString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget v0, p1, Lnet/flixster/android/model/Review;->score:I

    const/16 v1, 0x3c

    if-ge v0, v1, :cond_0

    .line 47
    iget-object v0, p0, Lcom/flixster/android/view/gtv/CriticReview;->icon:Landroid/widget/ImageView;

    const v1, 0x7f0200ec

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 49
    :cond_0
    return-void
.end method
