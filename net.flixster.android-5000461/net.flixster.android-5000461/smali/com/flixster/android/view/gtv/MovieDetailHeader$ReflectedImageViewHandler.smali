.class Lcom/flixster/android/view/gtv/MovieDetailHeader$ReflectedImageViewHandler;
.super Lcom/flixster/android/activity/ImageViewHandler;
.source "MovieDetailHeader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/view/gtv/MovieDetailHeader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReflectedImageViewHandler"
.end annotation


# instance fields
.field private final reflectionView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 0
    .parameter "imageView"
    .parameter "imageUrl"
    .parameter "reflectionView"

    .prologue
    .line 122
    invoke-direct {p0, p1, p2}, Lcom/flixster/android/activity/ImageViewHandler;-><init>(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 123
    iput-object p3, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader$ReflectedImageViewHandler;->reflectionView:Landroid/widget/ImageView;

    .line 124
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/flixster/android/activity/ImageViewHandler;->handleMessage(Landroid/os/Message;)V

    .line 129
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 130
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 131
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader$ReflectedImageViewHandler;->reflectionView:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/flixster/android/utils/BitmapHelper;->createReflection(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 133
    :cond_0
    return-void
.end method
