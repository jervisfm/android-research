.class public Lcom/flixster/android/view/gtv/MovieDetailHeader;
.super Landroid/widget/RelativeLayout;
.source "MovieDetailHeader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/view/gtv/MovieDetailHeader$ReflectedImageViewHandler;
    }
.end annotation


# instance fields
.field private audienceIcon:Landroid/widget/ImageView;

.field private audienceLayout:Landroid/widget/LinearLayout;

.field private audienceScore:Landroid/widget/TextView;

.field private cast:Landroid/widget/TextView;

.field private final context:Landroid/content/Context;

.field private mpaaRuntime:Landroid/widget/TextView;

.field private poster:Landroid/widget/ImageView;

.field private reflection:Landroid/widget/ImageView;

.field private synopsis:Landroid/widget/TextView;

.field private tomatometerIcon:Landroid/widget/ImageView;

.field private tomatometerLayout:Landroid/widget/LinearLayout;

.field private tomatometerScore:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->context:Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->initialize()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->context:Landroid/content/Context;

    .line 36
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->initialize()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->context:Landroid/content/Context;

    .line 42
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->initialize()V

    .line 43
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->context:Landroid/content/Context;

    const v1, 0x7f030052

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 47
    const v0, 0x7f070124

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->cast:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f070125

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->mpaaRuntime:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f070070

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->synopsis:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f070126

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->tomatometerLayout:Landroid/widget/LinearLayout;

    .line 51
    const v0, 0x7f070127

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->tomatometerIcon:Landroid/widget/ImageView;

    .line 52
    const v0, 0x7f070128

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->tomatometerScore:Landroid/widget/TextView;

    .line 53
    const v0, 0x7f070129

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->audienceLayout:Landroid/widget/LinearLayout;

    .line 54
    const v0, 0x7f07012a

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->audienceIcon:Landroid/widget/ImageView;

    .line 55
    const v0, 0x7f07012b

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->audienceScore:Landroid/widget/TextView;

    .line 56
    const v0, 0x7f070122

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->poster:Landroid/widget/ImageView;

    .line 57
    const v0, 0x7f070123

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieDetailHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->reflection:Landroid/widget/ImageView;

    .line 58
    return-void
.end method


# virtual methods
.method public load(Lnet/flixster/android/model/Movie;Lnet/flixster/android/model/LockerRight;)V
    .locals 13
    .parameter "movie"
    .parameter "right"

    .prologue
    .line 61
    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->cast:Landroid/widget/TextView;

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getCastShort()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->mpaaRuntime:Landroid/widget/TextView;

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getMpaaRuntime()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTomatometer()I

    move-result v5

    .line 66
    .local v5, tomatometer:I
    const/4 v6, -0x1

    if-le v5, v6, :cond_3

    .line 67
    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->tomatometerScore:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTomatometer()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->context:Landroid/content/Context;

    const v9, 0x7f0c0162

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 68
    iget-object v8, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->context:Landroid/content/Context;

    const v9, 0x7f0c01b4

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getNumCriticReviews()I

    move-result v12

    invoke-static {v12}, Lcom/flixster/android/utils/StringHelper;->formatInt(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 67
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->isFresh()Z

    move-result v6

    if-nez v6, :cond_0

    .line 70
    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->tomatometerIcon:Landroid/widget/ImageView;

    const v7, 0x7f0200ec

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getAudienceScore()I

    move-result v3

    .line 78
    .local v3, popcornScore:I
    if-lez v3, :cond_6

    .line 79
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->isReleased()Z

    move-result v2

    .line 80
    .local v2, isReleased:Z
    iget-object v7, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->audienceScore:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v8, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 81
    iget-object v9, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->context:Landroid/content/Context;

    if-eqz v2, :cond_4

    const v6, 0x7f0c0163

    :goto_1
    invoke-virtual {v9, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 82
    iget-object v8, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->context:Landroid/content/Context;

    const v9, 0x7f0c01b5

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getNumUserRatings()I

    move-result v12

    invoke-static {v12}, Lcom/flixster/android/utils/StringHelper;->formatInt(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 80
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    if-eqz v2, :cond_1

    .line 84
    iget-object v7, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->audienceIcon:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->isSpilled()Z

    move-result v6

    if-eqz v6, :cond_5

    const v6, 0x7f0200ee

    :goto_2
    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 91
    .end local v2           #isReleased:Z
    :cond_1
    :goto_3
    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->synopsis:Landroid/widget/TextView;

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getSynopsis()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    if-eqz p2, :cond_8

    .line 96
    invoke-virtual {p2}, Lnet/flixster/android/model/LockerRight;->getDetailPoster()Ljava/lang/String;

    move-result-object v4

    .line 97
    .local v4, posterUrl:Ljava/lang/String;
    if-nez v4, :cond_7

    .line 98
    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->poster:Landroid/widget/ImageView;

    const v7, 0x7f02014f

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 116
    :cond_2
    :goto_4
    return-void

    .line 73
    .end local v3           #popcornScore:I
    .end local v4           #posterUrl:Ljava/lang/String;
    :cond_3
    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->tomatometerLayout:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 81
    .restart local v2       #isReleased:Z
    .restart local v3       #popcornScore:I
    :cond_4
    const v6, 0x7f0c0164

    goto :goto_1

    .line 85
    :cond_5
    const v6, 0x7f0200e9

    goto :goto_2

    .line 88
    .end local v2           #isReleased:Z
    :cond_6
    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->audienceLayout:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 100
    .restart local v4       #posterUrl:Ljava/lang/String;
    :cond_7
    new-instance v1, Lcom/flixster/android/view/gtv/MovieDetailHeader$ReflectedImageViewHandler;

    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->poster:Landroid/widget/ImageView;

    .line 101
    iget-object v7, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->reflection:Landroid/widget/ImageView;

    invoke-direct {v1, v6, v4, v7}, Lcom/flixster/android/view/gtv/MovieDetailHeader$ReflectedImageViewHandler;-><init>(Landroid/widget/ImageView;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 100
    .local v1, handler:Lcom/flixster/android/activity/ImageViewHandler;
    invoke-virtual {p2, v1}, Lnet/flixster/android/model/LockerRight;->getDetailBitmap(Lcom/flixster/android/activity/ImageViewHandler;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 102
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v6, v7, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/flixster/android/activity/ImageViewHandler;->handleMessage(Landroid/os/Message;)V

    goto :goto_4

    .line 106
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v1           #handler:Lcom/flixster/android/activity/ImageViewHandler;
    .end local v4           #posterUrl:Ljava/lang/String;
    :cond_8
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getDetailPoster()Ljava/lang/String;

    move-result-object v4

    .line 107
    .restart local v4       #posterUrl:Ljava/lang/String;
    if-nez v4, :cond_9

    .line 108
    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->poster:Landroid/widget/ImageView;

    const v7, 0x7f02014f

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4

    .line 110
    :cond_9
    new-instance v1, Lcom/flixster/android/view/gtv/MovieDetailHeader$ReflectedImageViewHandler;

    iget-object v6, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->poster:Landroid/widget/ImageView;

    .line 111
    iget-object v7, p0, Lcom/flixster/android/view/gtv/MovieDetailHeader;->reflection:Landroid/widget/ImageView;

    invoke-direct {v1, v6, v4, v7}, Lcom/flixster/android/view/gtv/MovieDetailHeader$ReflectedImageViewHandler;-><init>(Landroid/widget/ImageView;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 110
    .restart local v1       #handler:Lcom/flixster/android/activity/ImageViewHandler;
    invoke-virtual {p1, v1}, Lnet/flixster/android/model/Movie;->getDetailBitmap(Lcom/flixster/android/activity/ImageViewHandler;)Landroid/graphics/Bitmap;

    move-result-object v0

    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 112
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v6, v7, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/flixster/android/activity/ImageViewHandler;->handleMessage(Landroid/os/Message;)V

    goto :goto_4
.end method
