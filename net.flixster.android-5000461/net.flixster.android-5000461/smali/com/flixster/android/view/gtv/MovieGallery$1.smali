.class Lcom/flixster/android/view/gtv/MovieGallery$1;
.super Ljava/lang/Object;
.source "MovieGallery.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/view/gtv/MovieGallery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/view/gtv/MovieGallery;


# direct methods
.method constructor <init>(Lcom/flixster/android/view/gtv/MovieGallery;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieGallery$1;->this$0:Lcom/flixster/android/view/gtv/MovieGallery;

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter "v"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery$1;->this$0:Lcom/flixster/android/view/gtv/MovieGallery;

    #getter for: Lcom/flixster/android/view/gtv/MovieGallery;->callback:Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;
    invoke-static {v0}, Lcom/flixster/android/view/gtv/MovieGallery;->access$0(Lcom/flixster/android/view/gtv/MovieGallery;)Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery$1;->this$0:Lcom/flixster/android/view/gtv/MovieGallery;

    #getter for: Lcom/flixster/android/view/gtv/MovieGallery;->callback:Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;
    invoke-static {v0}, Lcom/flixster/android/view/gtv/MovieGallery;->access$0(Lcom/flixster/android/view/gtv/MovieGallery;)Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGallery$1;->this$0:Lcom/flixster/android/view/gtv/MovieGallery;

    #getter for: Lcom/flixster/android/view/gtv/MovieGallery;->movies:Ljava/util/List;
    invoke-static {v1}, Lcom/flixster/android/view/gtv/MovieGallery;->access$1(Lcom/flixster/android/view/gtv/MovieGallery;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;->onClick(Ljava/lang/Object;)V

    .line 75
    :cond_0
    return-void
.end method
