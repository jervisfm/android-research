.class Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;
.super Landroid/widget/BaseAdapter;
.source "MovieGallery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/view/gtv/MovieGallery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MovieGalleryAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final movies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>.MovieGalleryAdapter<TE;>;"
    .local p2, movies:Ljava/util/List;,"Ljava/util/List<TE;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;->context:Landroid/content/Context;

    .line 84
    iput-object p2, p0, Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;->movies:Ljava/util/List;

    .line 85
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 106
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>.MovieGalleryAdapter<TE;>;"
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;->movies:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 110
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>.MovieGalleryAdapter<TE;>;"
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;->movies:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 114
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>.MovieGalleryAdapter<TE;>;"
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 88
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>.MovieGalleryAdapter<TE;>;"
    if-eqz p2, :cond_0

    instance-of v1, p2, Lcom/flixster/android/view/gtv/MovieGalleryItem;

    if-nez v1, :cond_1

    .line 89
    :cond_0
    new-instance p2, Lcom/flixster/android/view/gtv/MovieGalleryItem;

    .end local p2
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;->context:Landroid/content/Context;

    invoke-direct {p2, v1}, Lcom/flixster/android/view/gtv/MovieGalleryItem;-><init>(Landroid/content/Context;)V

    .line 94
    .restart local p2
    :goto_0
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;->movies:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 95
    .local v0, item:Ljava/lang/Object;,"TE;"
    instance-of v1, v0, Lnet/flixster/android/model/Movie;

    if-eqz v1, :cond_2

    move-object v1, p2

    .line 96
    check-cast v1, Lcom/flixster/android/view/gtv/MovieGalleryItem;

    check-cast v0, Lnet/flixster/android/model/Movie;

    .end local v0           #item:Ljava/lang/Object;,"TE;"
    invoke-virtual {v1, v0}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->load(Lnet/flixster/android/model/Movie;)V

    .line 102
    :goto_1
    return-object p2

    :cond_1
    move-object v1, p2

    .line 91
    check-cast v1, Lcom/flixster/android/view/gtv/MovieGalleryItem;

    invoke-virtual {v1}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->reset()V

    goto :goto_0

    .line 97
    .restart local v0       #item:Ljava/lang/Object;,"TE;"
    :cond_2
    instance-of v1, v0, Lnet/flixster/android/model/LockerRight;

    if-eqz v1, :cond_3

    move-object v1, p2

    .line 98
    check-cast v1, Lcom/flixster/android/view/gtv/MovieGalleryItem;

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .end local v0           #item:Ljava/lang/Object;,"TE;"
    invoke-virtual {v1, v0}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->load(Lnet/flixster/android/model/LockerRight;)V

    goto :goto_1

    .line 100
    .restart local v0       #item:Ljava/lang/Object;,"TE;"
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1
.end method
