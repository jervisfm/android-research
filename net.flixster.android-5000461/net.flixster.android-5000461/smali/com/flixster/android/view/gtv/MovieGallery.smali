.class public Lcom/flixster/android/view/gtv/MovieGallery;
.super Landroid/widget/RelativeLayout;
.source "MovieGallery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;,
        Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/RelativeLayout;"
    }
.end annotation


# instance fields
.field private callback:Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private grid:Landroid/widget/GridView;

.field private final itemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private movies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 25
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>;"
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 69
    new-instance v0, Lcom/flixster/android/view/gtv/MovieGallery$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/gtv/MovieGallery$1;-><init>(Lcom/flixster/android/view/gtv/MovieGallery;)V

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->itemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 26
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieGallery;->context:Landroid/content/Context;

    .line 27
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/MovieGallery;->initialize()V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 31
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>;"
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    new-instance v0, Lcom/flixster/android/view/gtv/MovieGallery$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/gtv/MovieGallery$1;-><init>(Lcom/flixster/android/view/gtv/MovieGallery;)V

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->itemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 32
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieGallery;->context:Landroid/content/Context;

    .line 33
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/MovieGallery;->initialize()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 37
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    new-instance v0, Lcom/flixster/android/view/gtv/MovieGallery$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/gtv/MovieGallery$1;-><init>(Lcom/flixster/android/view/gtv/MovieGallery;)V

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->itemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 38
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieGallery;->context:Landroid/content/Context;

    .line 39
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/MovieGallery;->initialize()V

    .line 40
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/view/gtv/MovieGallery;)Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->callback:Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/view/gtv/MovieGallery;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->movies:Ljava/util/List;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 43
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>;"
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->context:Landroid/content/Context;

    const v1, 0x7f030054

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 45
    const v0, 0x7f07010f

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieGallery;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->grid:Landroid/widget/GridView;

    .line 46
    return-void
.end method


# virtual methods
.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 58
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>;"
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->grid:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public load(Ljava/util/List;Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;)V
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TE;>;",
            "Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>;"
    .local p1, movies:Ljava/util/List;,"Ljava/util/List<TE;>;"
    .local p2, callback:Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;,"Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener<TE;>;"
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieGallery;->movies:Ljava/util/List;

    .line 51
    iput-object p2, p0, Lcom/flixster/android/view/gtv/MovieGallery;->callback:Lcom/flixster/android/view/gtv/MovieGallery$OnMovieClickListener;

    .line 52
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->grid:Landroid/widget/GridView;

    new-instance v1, Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;

    iget-object v2, p0, Lcom/flixster/android/view/gtv/MovieGallery;->context:Landroid/content/Context;

    invoke-direct {v1, v2, p1}, Lcom/flixster/android/view/gtv/MovieGallery$MovieGalleryAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 53
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->grid:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGallery;->itemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGallery;->grid:Landroid/widget/GridView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setClickable(Z)V

    .line 55
    return-void
.end method

.method public setSelectedItem(I)V
    .locals 2
    .parameter "position"

    .prologue
    .line 62
    .local p0, this:Lcom/flixster/android/view/gtv/MovieGallery;,"Lcom/flixster/android/view/gtv/MovieGallery<TE;>;"
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGallery;->grid:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getCount()I

    move-result v0

    .line 63
    .local v0, gridCount:I
    if-lt p1, v0, :cond_0

    .line 64
    add-int/lit8 p1, v0, -0x1

    .line 66
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGallery;->grid:Landroid/widget/GridView;

    invoke-virtual {v1, p1}, Landroid/widget/GridView;->setSelection(I)V

    .line 67
    return-void
.end method
