.class public Lcom/flixster/android/view/gtv/MovieGalleryItem;
.super Landroid/widget/RelativeLayout;
.source "MovieGalleryItem.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private poster:Landroid/widget/ImageView;

.field private title:Landroid/widget/TextView;

.field private tomatometerIcon:Landroid/widget/ImageView;

.field private tomatometerScore:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 26
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->context:Landroid/content/Context;

    .line 27
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->initialize()V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->context:Landroid/content/Context;

    .line 33
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->initialize()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iput-object p1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->context:Landroid/content/Context;

    .line 39
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->initialize()V

    .line 40
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->context:Landroid/content/Context;

    const v1, 0x7f030056

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 45
    const v0, 0x7f070135

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->poster:Landroid/widget/ImageView;

    .line 46
    const v0, 0x7f070136

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->title:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f070127

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerIcon:Landroid/widget/ImageView;

    .line 48
    const v0, 0x7f070128

    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerScore:Landroid/widget/TextView;

    .line 49
    return-void
.end method

.method private loadCaption(Lnet/flixster/android/model/Movie;)V
    .locals 6
    .parameter "movie"

    .prologue
    const/4 v0, 0x0

    const/16 v5, 0x8

    .line 76
    iget-object v3, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerScore:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    iget-object v3, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 78
    iget-boolean v3, p1, Lnet/flixster/android/model/Movie;->isUpcoming:Z

    if-eqz v3, :cond_3

    .line 79
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v1

    .line 80
    .local v1, releaseDate:Ljava/util/Date;
    if-eqz v1, :cond_2

    .line 82
    invoke-virtual {v1}, Ljava/util/Date;->getYear()I

    move-result v3

    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getYear()I

    move-result v4

    if-le v3, v4, :cond_0

    const/4 v0, 0x1

    .line 83
    .local v0, isNextYear:Z
    :cond_0
    iget-object v4, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerScore:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-static {v1}, Lcom/flixster/android/utils/DateTimeHelper;->formatMonthDayYear(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v3, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    .end local v0           #isNextYear:Z
    .end local v1           #releaseDate:Ljava/util/Date;
    :goto_1
    return-void

    .line 84
    .restart local v0       #isNextYear:Z
    .restart local v1       #releaseDate:Ljava/util/Date;
    :cond_1
    invoke-static {v1}, Lcom/flixster/android/utils/DateTimeHelper;->formatMonthDay(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 87
    .end local v0           #isNextYear:Z
    :cond_2
    iget-object v3, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerScore:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    iget-object v3, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 91
    .end local v1           #releaseDate:Ljava/util/Date;
    :cond_3
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTomatometer()I

    move-result v2

    .line 92
    .local v2, tomatometer:I
    const/4 v3, -0x1

    if-le v2, v3, :cond_5

    .line 93
    iget-object v3, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerScore:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTomatometer()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v4, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerIcon:Landroid/widget/ImageView;

    .line 95
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->isFresh()Z

    move-result v3

    if-eqz v3, :cond_4

    const v3, 0x7f0200dd

    :goto_2
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_4
    const v3, 0x7f0200ec

    goto :goto_2

    .line 97
    :cond_5
    iget-object v3, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerScore:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    iget-object v3, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->tomatometerIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method protected load(Lnet/flixster/android/model/LockerRight;)V
    .locals 3
    .parameter "right"

    .prologue
    .line 64
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->title:Landroid/widget/TextView;

    invoke-virtual {p1}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    invoke-virtual {p1}, Lnet/flixster/android/model/LockerRight;->getDetailPoster()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 67
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->poster:Landroid/widget/ImageView;

    const v2, 0x7f02014f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 72
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lnet/flixster/android/model/LockerRight;->getAsset()Lnet/flixster/android/model/Movie;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->loadCaption(Lnet/flixster/android/model/Movie;)V

    .line 73
    return-void

    .line 68
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->poster:Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Lnet/flixster/android/model/LockerRight;->getDetailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 69
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->poster:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected load(Lnet/flixster/android/model/Movie;)V
    .locals 3
    .parameter "movie"

    .prologue
    .line 52
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->title:Landroid/widget/TextView;

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getDetailPoster()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 55
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->poster:Landroid/widget/ImageView;

    const v2, 0x7f02014f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 60
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/flixster/android/view/gtv/MovieGalleryItem;->loadCaption(Lnet/flixster/android/model/Movie;)V

    .line 61
    return-void

    .line 56
    :cond_1
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->poster:Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Lnet/flixster/android/model/Movie;->getDetailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 57
    iget-object v1, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->poster:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected reset()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/flixster/android/view/gtv/MovieGalleryItem;->poster:Landroid/widget/ImageView;

    const v1, 0x7f020154

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 106
    return-void
.end method
