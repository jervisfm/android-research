.class public Lcom/flixster/android/view/gtv/Subheader;
.super Landroid/widget/TextView;
.source "Subheader.java"


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 16
    iput-object p1, p0, Lcom/flixster/android/view/gtv/Subheader;->context:Landroid/content/Context;

    .line 17
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/Subheader;->initialize()V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    iput-object p1, p0, Lcom/flixster/android/view/gtv/Subheader;->context:Landroid/content/Context;

    .line 23
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/Subheader;->initialize()V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    iput-object p1, p0, Lcom/flixster/android/view/gtv/Subheader;->context:Landroid/content/Context;

    .line 29
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/Subheader;->initialize()V

    .line 30
    return-void
.end method

.method private initialize()V
    .locals 7

    .prologue
    const/high16 v6, 0x4000

    const/4 v5, 0x0

    .line 33
    invoke-virtual {p0}, Lcom/flixster/android/view/gtv/Subheader;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 34
    .local v2, spacingSmxPx:I
    invoke-virtual {p0}, Lcom/flixster/android/view/gtv/Subheader;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a005d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 36
    .local v1, spacingSmPx:I
    const/16 v3, 0x10

    invoke-virtual {p0, v3}, Lcom/flixster/android/view/gtv/Subheader;->setGravity(I)V

    .line 37
    const v3, 0x7f0201b4

    invoke-virtual {p0, v3}, Lcom/flixster/android/view/gtv/Subheader;->setBackgroundResource(I)V

    .line 38
    invoke-virtual {p0, v1, v2, v2, v2}, Lcom/flixster/android/view/gtv/Subheader;->setPadding(IIII)V

    .line 39
    const/high16 v3, 0x40a0

    const v4, 0x7f090023

    invoke-virtual {p0, v3, v6, v6, v4}, Lcom/flixster/android/view/gtv/Subheader;->setShadowLayer(FFFI)V

    .line 40
    iget-object v3, p0, Lcom/flixster/android/view/gtv/Subheader;->context:Landroid/content/Context;

    const v4, 0x7f0d0071

    invoke-virtual {p0, v3, v4}, Lcom/flixster/android/view/gtv/Subheader;->setTextAppearance(Landroid/content/Context;I)V

    .line 42
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 43
    .local v0, layoutParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v5, v5, v5, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 44
    invoke-virtual {p0, v0}, Lcom/flixster/android/view/gtv/Subheader;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 45
    return-void
.end method
