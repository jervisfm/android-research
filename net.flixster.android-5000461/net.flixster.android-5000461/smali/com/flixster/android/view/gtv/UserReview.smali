.class public Lcom/flixster/android/view/gtv/UserReview;
.super Landroid/widget/RelativeLayout;
.source "UserReview.java"


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 13
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 14
    iput-object p1, p0, Lcom/flixster/android/view/gtv/UserReview;->context:Landroid/content/Context;

    .line 15
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/UserReview;->initialize()V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput-object p1, p0, Lcom/flixster/android/view/gtv/UserReview;->context:Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/UserReview;->initialize()V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    iput-object p1, p0, Lcom/flixster/android/view/gtv/UserReview;->context:Landroid/content/Context;

    .line 27
    invoke-direct {p0}, Lcom/flixster/android/view/gtv/UserReview;->initialize()V

    .line 28
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/flixster/android/view/gtv/UserReview;->context:Landroid/content/Context;

    const v1, 0x7f03008e

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 32
    return-void
.end method


# virtual methods
.method public load()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method
