.class Lcom/flixster/android/widget/WidgetConfigChangeService$1;
.super Ljava/lang/Object;
.source "WidgetConfigChangeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/widget/WidgetConfigChangeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/widget/WidgetConfigChangeService;


# direct methods
.method constructor <init>(Lcom/flixster/android/widget/WidgetConfigChangeService;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/widget/WidgetConfigChangeService$1;->this$0:Lcom/flixster/android/widget/WidgetConfigChangeService;

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 48
    :try_start_0
    iget-object v1, p0, Lcom/flixster/android/widget/WidgetConfigChangeService$1;->this$0:Lcom/flixster/android/widget/WidgetConfigChangeService;

    #getter for: Lcom/flixster/android/widget/WidgetConfigChangeService;->broadcastDelay:I
    invoke-static {v1}, Lcom/flixster/android/widget/WidgetConfigChangeService;->access$0(Lcom/flixster/android/widget/WidgetConfigChangeService;)I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/flixster/android/widget/WidgetConfigChangeService$1;->this$0:Lcom/flixster/android/widget/WidgetConfigChangeService;

    const-class v2, Lcom/flixster/android/widget/WidgetProvider;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "flixster.intent.action.widget"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string v1, "KEY_REFRESH"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 54
    iget-object v1, p0, Lcom/flixster/android/widget/WidgetConfigChangeService$1;->this$0:Lcom/flixster/android/widget/WidgetConfigChangeService;

    invoke-virtual {v1, v0}, Lcom/flixster/android/widget/WidgetConfigChangeService;->sendBroadcast(Landroid/content/Intent;)V

    .line 55
    return-void

    .line 49
    .end local v0           #intent:Landroid/content/Intent;
    :catch_0
    move-exception v1

    goto :goto_0
.end method
