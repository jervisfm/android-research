.class public Lcom/flixster/android/widget/WidgetConfigChangeService;
.super Landroid/app/Service;
.source "WidgetConfigChangeService.java"


# instance fields
.field private broadcastDelay:I

.field private final delayedBroadcaster:Ljava/lang/Runnable;

.field private oldOrientation:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 45
    new-instance v0, Lcom/flixster/android/widget/WidgetConfigChangeService$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/widget/WidgetConfigChangeService$1;-><init>(Lcom/flixster/android/widget/WidgetConfigChangeService;)V

    iput-object v0, p0, Lcom/flixster/android/widget/WidgetConfigChangeService;->delayedBroadcaster:Ljava/lang/Runnable;

    .line 13
    return-void
.end method

.method static synthetic access$0(Lcom/flixster/android/widget/WidgetConfigChangeService;)I
    .locals 1
    .parameter

    .prologue
    .line 15
    iget v0, p0, Lcom/flixster/android/widget/WidgetConfigChangeService;->broadcastDelay:I

    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .parameter "intent"

    .prologue
    .line 30
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .parameter "newConfig"

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 36
    const-string v0, "FlxWdgt"

    const-string v1, "WidgetConfigChangeService.onStart.onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/flixster/android/widget/WidgetConfigChangeService;->oldOrientation:I

    if-eq v0, v1, :cond_0

    .line 38
    const-string v0, "FlxWdgt"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WidgetConfigChangeService.onStart.onConfigurationChanged "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/flixster/android/widget/WidgetConfigChangeService;->oldOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 39
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 38
    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/flixster/android/widget/WidgetConfigChangeService;->oldOrientation:I

    .line 41
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    iget-object v1, p0, Lcom/flixster/android/widget/WidgetConfigChangeService;->delayedBroadcaster:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 43
    :cond_0
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 3
    .parameter "intent"
    .parameter "startId"

    .prologue
    .line 20
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 21
    invoke-virtual {p0}, Lcom/flixster/android/widget/WidgetConfigChangeService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/flixster/android/widget/WidgetConfigChangeService;->oldOrientation:I

    .line 22
    const-string v0, "FlxWdgt"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WidgetConfigChangeService.onStart "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/flixster/android/widget/WidgetConfigChangeService;->oldOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/flixster/android/utils/Properties;->identifyHoneycombTablet(Landroid/content/Context;)V

    .line 24
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/flixster/android/utils/Properties;->identifyGoogleTv(Landroid/content/Context;)V

    .line 25
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9c4

    :goto_0
    iput v0, p0, Lcom/flixster/android/widget/WidgetConfigChangeService;->broadcastDelay:I

    .line 26
    return-void

    .line 25
    :cond_0
    const/16 v0, 0x4e2

    goto :goto_0
.end method
