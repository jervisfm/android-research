.class Lcom/flixster/android/widget/WidgetProvider$1;
.super Ljava/lang/Object;
.source "WidgetProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/widget/WidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/widget/WidgetProvider;


# direct methods
.method constructor <init>(Lcom/flixster/android/widget/WidgetProvider;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/widget/WidgetProvider$1;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 98
    const-string v1, "FlxWdgt"

    const-string v2, "WidgetProvider.boxOfficeMoviesFetcher"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :try_start_0
    invoke-static {}, Lcom/flixster/android/widget/WidgetProvider;->access$0()Ljava/util/List;

    move-result-object v1

    const/16 v2, 0xa

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lnet/flixster/android/data/MovieDao;->fetchBoxOfficeForWidget(Ljava/util/List;IZ)V

    .line 101
    iget-object v1, p0, Lcom/flixster/android/widget/WidgetProvider$1;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->successHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/widget/WidgetProvider;->access$1(Lcom/flixster/android/widget/WidgetProvider;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxWdgt"

    const-string v2, "WidgetProvider.boxOfficeMoviesFetcher DaoException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 105
    const-wide/16 v1, 0x1f4

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 108
    :goto_1
    iget-object v1, p0, Lcom/flixster/android/widget/WidgetProvider$1;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->errorHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/flixster/android/widget/WidgetProvider;->access$2(Lcom/flixster/android/widget/WidgetProvider;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2, v4, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 106
    :catch_1
    move-exception v1

    goto :goto_1
.end method
