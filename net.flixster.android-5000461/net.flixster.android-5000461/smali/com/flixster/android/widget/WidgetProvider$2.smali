.class Lcom/flixster/android/widget/WidgetProvider$2;
.super Landroid/os/Handler;
.source "WidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/widget/WidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/widget/WidgetProvider;


# direct methods
.method constructor <init>(Lcom/flixster/android/widget/WidgetProvider;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/widget/WidgetProvider$2;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    .line 113
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 115
    iget-object v2, p0, Lcom/flixster/android/widget/WidgetProvider$2;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #calls: Lcom/flixster/android/widget/WidgetProvider;->getMovie()Lnet/flixster/android/model/Movie;
    invoke-static {v2}, Lcom/flixster/android/widget/WidgetProvider;->access$3(Lcom/flixster/android/widget/WidgetProvider;)Lnet/flixster/android/model/Movie;

    move-result-object v0

    .line 116
    .local v0, movie:Lnet/flixster/android/model/Movie;
    if-eqz v0, :cond_0

    .line 119
    iget-object v2, p0, Lcom/flixster/android/widget/WidgetProvider$2;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #calls: Lcom/flixster/android/widget/WidgetProvider;->updateWidgets(Lnet/flixster/android/model/Movie;)V
    invoke-static {v2, v0}, Lcom/flixster/android/widget/WidgetProvider;->access$4(Lcom/flixster/android/widget/WidgetProvider;Lnet/flixster/android/model/Movie;)V

    .line 122
    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->getProfilePoster()Ljava/lang/String;

    move-result-object v1

    .line 123
    .local v1, posterUrl:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 124
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/widget/WidgetProvider$2;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->posterHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/flixster/android/widget/WidgetProvider;->access$5(Lcom/flixster/android/widget/WidgetProvider;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    .line 127
    .end local v1           #posterUrl:Ljava/lang/String;
    :cond_0
    return-void
.end method
