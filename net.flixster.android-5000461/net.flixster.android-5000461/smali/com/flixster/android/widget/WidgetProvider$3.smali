.class Lcom/flixster/android/widget/WidgetProvider$3;
.super Landroid/os/Handler;
.source "WidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/widget/WidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/widget/WidgetProvider;


# direct methods
.method constructor <init>(Lcom/flixster/android/widget/WidgetProvider;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/widget/WidgetProvider$3;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    .line 130
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 132
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/data/DaoException;

    .line 134
    .local v0, de:Lnet/flixster/android/data/DaoException;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lnet/flixster/android/data/DaoException$Type;->NETWORK:Lnet/flixster/android/data/DaoException$Type;

    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v4

    if-ne v3, v4, :cond_1

    .line 135
    :cond_0
    iget-object v3, p0, Lcom/flixster/android/widget/WidgetProvider$3;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/flixster/android/widget/WidgetProvider;->access$6(Lcom/flixster/android/widget/WidgetProvider;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0168

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, text:Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/flixster/android/widget/WidgetProvider$3;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/flixster/android/widget/WidgetProvider;->access$6(Lcom/flixster/android/widget/WidgetProvider;)Landroid/content/Context;

    move-result-object v3

    #calls: Lcom/flixster/android/widget/WidgetProvider;->createErrorMessageViews(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/RemoteViews;
    invoke-static {v3, v1}, Lcom/flixster/android/widget/WidgetProvider;->access$7(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v2

    .line 140
    .local v2, views:Landroid/widget/RemoteViews;
    iget-object v3, p0, Lcom/flixster/android/widget/WidgetProvider$3;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;
    invoke-static {v3}, Lcom/flixster/android/widget/WidgetProvider;->access$8(Lcom/flixster/android/widget/WidgetProvider;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    iget-object v4, p0, Lcom/flixster/android/widget/WidgetProvider$3;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->appWidgetIds:[I
    invoke-static {v4}, Lcom/flixster/android/widget/WidgetProvider;->access$9(Lcom/flixster/android/widget/WidgetProvider;)[I

    move-result-object v4

    #calls: Lcom/flixster/android/widget/WidgetProvider;->updateWidgets(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V
    invoke-static {v3, v4, v2}, Lcom/flixster/android/widget/WidgetProvider;->access$10(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V

    .line 141
    return-void

    .line 137
    .end local v1           #text:Ljava/lang/String;
    .end local v2           #views:Landroid/widget/RemoteViews;
    :cond_1
    iget-object v3, p0, Lcom/flixster/android/widget/WidgetProvider$3;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/flixster/android/widget/WidgetProvider;->access$6(Lcom/flixster/android/widget/WidgetProvider;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0169

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1       #text:Ljava/lang/String;
    goto :goto_0
.end method
