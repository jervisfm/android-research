.class Lcom/flixster/android/widget/WidgetProvider$4;
.super Landroid/os/Handler;
.source "WidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flixster/android/widget/WidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flixster/android/widget/WidgetProvider;


# direct methods
.method constructor <init>(Lcom/flixster/android/widget/WidgetProvider;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/flixster/android/widget/WidgetProvider$4;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    .line 144
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 146
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/flixster/android/widget/WidgetProvider$4;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/flixster/android/widget/WidgetProvider;->access$6(Lcom/flixster/android/widget/WidgetProvider;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030094

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 147
    .local v1, views:Landroid/widget/RemoteViews;
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 148
    .local v0, posterBitmap:Landroid/graphics/Bitmap;
    const v2, 0x7f0702ec

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 149
    iget-object v2, p0, Lcom/flixster/android/widget/WidgetProvider$4;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;
    invoke-static {v2}, Lcom/flixster/android/widget/WidgetProvider;->access$8(Lcom/flixster/android/widget/WidgetProvider;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    iget-object v3, p0, Lcom/flixster/android/widget/WidgetProvider$4;->this$0:Lcom/flixster/android/widget/WidgetProvider;

    #getter for: Lcom/flixster/android/widget/WidgetProvider;->appWidgetIds:[I
    invoke-static {v3}, Lcom/flixster/android/widget/WidgetProvider;->access$9(Lcom/flixster/android/widget/WidgetProvider;)[I

    move-result-object v3

    #calls: Lcom/flixster/android/widget/WidgetProvider;->updateWidgets(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V
    invoke-static {v2, v3, v1}, Lcom/flixster/android/widget/WidgetProvider;->access$10(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V

    .line 150
    return-void
.end method
