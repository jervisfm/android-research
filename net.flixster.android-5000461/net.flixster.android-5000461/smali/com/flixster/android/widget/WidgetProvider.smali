.class public Lcom/flixster/android/widget/WidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "WidgetProvider.java"


# static fields
.field protected static final ACTION_WIDGET:Ljava/lang/String; = "flixster.intent.action.widget"

.field private static final KEY_INDEX:Ljava/lang/String; = "KEY_INDEX"

.field protected static final KEY_REFRESH:Ljava/lang/String; = "KEY_REFRESH"

.field private static final KEY_TRACK_EVENT:Ljava/lang/String; = "KEY_TRACK_EVENT"

.field private static final KEY_UP:Ljava/lang/String; = "KEY_UP"

.field private static final NUM_WIDGET_MOVIES:I = 0xa

.field private static final moviesForWidget:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private appWidgetIds:[I

.field private appWidgetManager:Landroid/appwidget/AppWidgetManager;

.field private final boxOfficeMoviesFetcher:Ljava/lang/Runnable;

.field private context:Landroid/content/Context;

.field private currIndex:I

.field private final errorHandler:Landroid/os/Handler;

.field private isDirectionUp:Ljava/lang/Boolean;

.field private final posterHandler:Landroid/os/Handler;

.field private final successHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/flixster/android/widget/WidgetProvider;->moviesForWidget:Ljava/util/List;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 95
    new-instance v0, Lcom/flixster/android/widget/WidgetProvider$1;

    invoke-direct {v0, p0}, Lcom/flixster/android/widget/WidgetProvider$1;-><init>(Lcom/flixster/android/widget/WidgetProvider;)V

    iput-object v0, p0, Lcom/flixster/android/widget/WidgetProvider;->boxOfficeMoviesFetcher:Ljava/lang/Runnable;

    .line 113
    new-instance v0, Lcom/flixster/android/widget/WidgetProvider$2;

    invoke-direct {v0, p0}, Lcom/flixster/android/widget/WidgetProvider$2;-><init>(Lcom/flixster/android/widget/WidgetProvider;)V

    iput-object v0, p0, Lcom/flixster/android/widget/WidgetProvider;->successHandler:Landroid/os/Handler;

    .line 130
    new-instance v0, Lcom/flixster/android/widget/WidgetProvider$3;

    invoke-direct {v0, p0}, Lcom/flixster/android/widget/WidgetProvider$3;-><init>(Lcom/flixster/android/widget/WidgetProvider;)V

    iput-object v0, p0, Lcom/flixster/android/widget/WidgetProvider;->errorHandler:Landroid/os/Handler;

    .line 144
    new-instance v0, Lcom/flixster/android/widget/WidgetProvider$4;

    invoke-direct {v0, p0}, Lcom/flixster/android/widget/WidgetProvider$4;-><init>(Lcom/flixster/android/widget/WidgetProvider;)V

    iput-object v0, p0, Lcom/flixster/android/widget/WidgetProvider;->posterHandler:Landroid/os/Handler;

    .line 33
    return-void
.end method

.method static synthetic access$0()Ljava/util/List;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/flixster/android/widget/WidgetProvider;->moviesForWidget:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lcom/flixster/android/widget/WidgetProvider;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Lcom/flixster/android/widget/WidgetProvider;->successHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$10(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 191
    invoke-static {p0, p1, p2}, Lcom/flixster/android/widget/WidgetProvider;->updateWidgets(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V

    return-void
.end method

.method static synthetic access$2(Lcom/flixster/android/widget/WidgetProvider;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 130
    iget-object v0, p0, Lcom/flixster/android/widget/WidgetProvider;->errorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/flixster/android/widget/WidgetProvider;)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/flixster/android/widget/WidgetProvider;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4(Lcom/flixster/android/widget/WidgetProvider;Lnet/flixster/android/model/Movie;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/flixster/android/widget/WidgetProvider;->updateWidgets(Lnet/flixster/android/model/Movie;)V

    return-void
.end method

.method static synthetic access$5(Lcom/flixster/android/widget/WidgetProvider;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/flixster/android/widget/WidgetProvider;->posterHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6(Lcom/flixster/android/widget/WidgetProvider;)Landroid/content/Context;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/flixster/android/widget/WidgetProvider;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$7(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/RemoteViews;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 204
    invoke-static {p0, p1}, Lcom/flixster/android/widget/WidgetProvider;->createErrorMessageViews(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8(Lcom/flixster/android/widget/WidgetProvider;)Landroid/appwidget/AppWidgetManager;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    return-object v0
.end method

.method static synthetic access$9(Lcom/flixster/android/widget/WidgetProvider;)[I
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetIds:[I

    return-object v0
.end method

.method private static createAppIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 4
    .parameter "context"

    .prologue
    const/4 v3, 0x0

    .line 284
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/bootstrap/BootstrapActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 285
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "KEY_TRACK_EVENT"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 286
    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private static createErrorMessageViews(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/RemoteViews;
    .locals 4
    .parameter "context"
    .parameter "msg"

    .prologue
    const v3, 0x7f0702ee

    .line 205
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030094

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 206
    .local v0, views:Landroid/widget/RemoteViews;
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/flixster/android/widget/WidgetProvider;->toggleLoadingVisibility(Landroid/widget/RemoteViews;Z)V

    .line 207
    invoke-virtual {v0, v3, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 208
    invoke-static {p0}, Lcom/flixster/android/widget/WidgetProvider;->createSelfRefreshIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 209
    return-object v0
.end method

.method private static createLoadingViews(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 4
    .parameter "context"

    .prologue
    .line 198
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030094

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 199
    .local v0, views:Landroid/widget/RemoteViews;
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/flixster/android/widget/WidgetProvider;->toggleLoadingVisibility(Landroid/widget/RemoteViews;Z)V

    .line 200
    const v1, 0x7f0702ee

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0135

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 201
    return-object v0
.end method

.method private static createMovieDetailIntent(Landroid/content/Context;J)Landroid/app/PendingIntent;
    .locals 4
    .parameter "context"
    .parameter "movieId"

    .prologue
    const/4 v3, 0x0

    .line 290
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/bootstrap/BootstrapActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 291
    .local v0, intent:Landroid/content/Intent;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://internal-deeplink.flixster.com?movie="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 292
    const-string v1, "KEY_TRACK_EVENT"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 293
    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private static createMovieViews(Landroid/content/Context;Lnet/flixster/android/model/Movie;I)Landroid/widget/RemoteViews;
    .locals 13
    .parameter "context"
    .parameter "movie"
    .parameter "currIndex"

    .prologue
    const v12, 0x7f0702f2

    const v11, 0x7f0702f1

    const v10, 0x7f0702f0

    const/4 v9, 0x0

    const/16 v8, 0x8

    .line 213
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f030094

    invoke-direct {v4, v5, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 214
    .local v4, views:Landroid/widget/RemoteViews;
    invoke-static {v4, v9}, Lcom/flixster/android/widget/WidgetProvider;->toggleLoadingVisibility(Landroid/widget/RemoteViews;Z)V

    .line 217
    const v5, 0x7f0702ed

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v6

    invoke-static {p0, v6, v7}, Lcom/flixster/android/widget/WidgetProvider;->createMovieDetailIntent(Landroid/content/Context;J)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 218
    const v5, 0x7f0702eb

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v6

    invoke-static {p0, v6, v7}, Lcom/flixster/android/widget/WidgetProvider;->createMovieDetailIntent(Landroid/content/Context;J)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 219
    const v5, 0x7f0702e9

    invoke-static {p0}, Lcom/flixster/android/widget/WidgetProvider;->createAppIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 220
    const v5, 0x7f0702ea

    invoke-static {p0}, Lcom/flixster/android/widget/WidgetProvider;->createSearchIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 223
    const v5, 0x7f0702f6

    const/4 v6, 0x1

    invoke-static {p0, p2, v6}, Lcom/flixster/android/widget/WidgetProvider;->createSelfBrowseIntent(Landroid/content/Context;IZ)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 224
    const v5, 0x7f0702f7

    invoke-static {p0, p2, v9}, Lcom/flixster/android/widget/WidgetProvider;->createSelfBrowseIntent(Landroid/content/Context;IZ)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 227
    const v5, 0x7f0702ef

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 228
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTomatometer()I

    move-result v3

    .line 229
    .local v3, tomatometer:I
    if-lez v3, :cond_1

    .line 230
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v11, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 231
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTomatometerIconId()I

    move-result v5

    invoke-static {p0, v5}, Lcom/flixster/android/widget/WidgetProvider;->decodeBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v4, v10, v5}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 236
    :goto_0
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getAudienceScore()I

    move-result v0

    .line 237
    .local v0, audienceScore:I
    if-lez v0, :cond_2

    .line 238
    const v5, 0x7f0702f3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 240
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getAudienceScoreIconId()I

    move-result v5

    invoke-static {p0, v5}, Lcom/flixster/android/widget/WidgetProvider;->decodeBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 239
    invoke-virtual {v4, v12, v5}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 247
    :goto_1
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getProfilePoster()Ljava/lang/String;

    move-result-object v2

    .line 248
    .local v2, posterUrl:Ljava/lang/String;
    if-nez v2, :cond_0

    .line 250
    const v5, 0x7f0702ec

    const v6, 0x7f020154

    invoke-static {p0, v6}, Lcom/flixster/android/widget/WidgetProvider;->decodeBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 254
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {p1, v5}, Lnet/flixster/android/model/Movie;->getFriendStat(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 255
    .local v1, friendStat:Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 256
    const v5, 0x7f0702f4

    invoke-virtual {v4, v5, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 261
    :goto_2
    return-object v4

    .line 233
    .end local v0           #audienceScore:I
    .end local v1           #friendStat:Ljava/lang/String;
    .end local v2           #posterUrl:Ljava/lang/String;
    :cond_1
    invoke-virtual {v4, v11, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 234
    invoke-virtual {v4, v10, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    .line 242
    .restart local v0       #audienceScore:I
    :cond_2
    const v5, 0x7f0702f3

    invoke-virtual {v4, v5, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 243
    invoke-virtual {v4, v12, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1

    .line 258
    .restart local v1       #friendStat:Ljava/lang/String;
    .restart local v2       #posterUrl:Ljava/lang/String;
    :cond_3
    const v5, 0x7f0702f4

    invoke-virtual {v4, v5, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_2
.end method

.method private static createSearchIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 4
    .parameter "context"

    .prologue
    const/4 v3, 0x0

    .line 297
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/bootstrap/BootstrapActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 298
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "http://internal-deeplink.flixster.com?search=1"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 299
    const-string v1, "KEY_TRACK_EVENT"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 300
    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private static createSelfBrowseIntent(Landroid/content/Context;IZ)Landroid/app/PendingIntent;
    .locals 3
    .parameter "context"
    .parameter "currIndex"
    .parameter "isDirectionUp"

    .prologue
    .line 305
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/widget/WidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 306
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "flixster.intent.action.widget"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 307
    const-string v1, "KEY_INDEX"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 308
    const-string v1, "KEY_UP"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 309
    invoke-static {}, Lcom/flixster/android/utils/MathHelper;->randomId()J

    move-result-wide v1

    long-to-int v1, v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private static createSelfRefreshIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3
    .parameter "context"

    .prologue
    .line 314
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/widget/WidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 315
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "flixster.intent.action.widget"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 316
    const-string v1, "KEY_REFRESH"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 317
    invoke-static {}, Lcom/flixster/android/utils/MathHelper;->randomId()J

    move-result-wide v1

    long-to-int v1, v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private static decodeBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 1
    .parameter "context"
    .parameter "drawable"

    .prologue
    .line 321
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private getMovie()Lnet/flixster/android/model/Movie;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 154
    const-string v3, "FlxWdgt"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "WidgetProvider.getMovie "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 155
    iget-object v1, p0, Lcom/flixster/android/widget/WidgetProvider;->isDirectionUp:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    const-string v1, "null"

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 154
    invoke-static {v3, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    sget-object v3, Lcom/flixster/android/widget/WidgetProvider;->moviesForWidget:Ljava/util/List;

    monitor-enter v3

    .line 157
    :try_start_0
    sget-object v1, Lcom/flixster/android/widget/WidgetProvider;->moviesForWidget:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 158
    .local v0, size:I
    if-nez v0, :cond_2

    .line 159
    const-string v1, "FlxWdgt"

    const-string v2, "WidgetProvider.getMovie list empty"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    .line 177
    :goto_1
    return-object v1

    .line 155
    .end local v0           #size:I
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/widget/WidgetProvider;->isDirectionUp:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "up"

    goto :goto_0

    :cond_1
    const-string v1, "down"

    goto :goto_0

    .line 163
    .restart local v0       #size:I
    :cond_2
    :try_start_1
    iget v1, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    add-int/lit8 v1, v1, 0x1

    if-le v1, v0, :cond_4

    move v1, v2

    :goto_2
    iput v1, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    .line 166
    iget-object v1, p0, Lcom/flixster/android/widget/WidgetProvider;->isDirectionUp:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 167
    iget-object v1, p0, Lcom/flixster/android/widget/WidgetProvider;->isDirectionUp:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 168
    iget v1, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    if-gez v1, :cond_3

    .line 169
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    .line 177
    :cond_3
    :goto_3
    sget-object v1, Lcom/flixster/android/widget/WidgetProvider;->moviesForWidget:Ljava/util/List;

    iget v2, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    monitor-exit v3

    goto :goto_1

    .line 156
    .end local v0           #size:I
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 163
    .restart local v0       #size:I
    :cond_4
    :try_start_2
    iget v1, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    goto :goto_2

    .line 172
    :cond_5
    iget v1, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    add-int/lit8 v1, v1, 0x1

    if-le v1, v0, :cond_3

    .line 173
    const/4 v1, 0x0

    iput v1, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method public static isOriginatedFromWidget(Landroid/content/Intent;)Z
    .locals 3
    .parameter "intent"

    .prologue
    .line 326
    const-string v1, "KEY_TRACK_EVENT"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 327
    .local v0, result:Z
    if-eqz v0, :cond_0

    .line 328
    const-string v1, "KEY_TRACK_EVENT"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 330
    :cond_0
    return v0
.end method

.method public static propagateIntentExtras(Landroid/content/Intent;Landroid/content/Intent;)V
    .locals 1
    .parameter "originalIntent"
    .parameter "propagatingIntent"

    .prologue
    .line 335
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 336
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 338
    invoke-static {p0}, Lcom/flixster/android/widget/WidgetProvider;->isOriginatedFromWidget(Landroid/content/Intent;)Z

    .line 340
    :cond_0
    return-void
.end method

.method private static toggleLoadingVisibility(Landroid/widget/RemoteViews;Z)V
    .locals 3
    .parameter "views"
    .parameter "isLoadingVisible"

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 265
    if-eqz p1, :cond_0

    move v0, v2

    .line 266
    .local v0, loadingVisibility:I
    :goto_0
    if-eqz p1, :cond_1

    .line 269
    .local v1, movieVisibility:I
    :goto_1
    const v2, 0x7f0702ee

    invoke-virtual {p0, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 272
    const v2, 0x7f0702f6

    invoke-virtual {p0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 273
    const v2, 0x7f0702f7

    invoke-virtual {p0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 274
    const v2, 0x7f0702ec

    invoke-virtual {p0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 275
    const v2, 0x7f0702ef

    invoke-virtual {p0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 276
    const v2, 0x7f0702f4

    invoke-virtual {p0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 277
    const v2, 0x7f0702f1

    invoke-virtual {p0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 278
    const v2, 0x7f0702f0

    invoke-virtual {p0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 279
    const v2, 0x7f0702f3

    invoke-virtual {p0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 280
    const v2, 0x7f0702f2

    invoke-virtual {p0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 281
    return-void

    .end local v0           #loadingVisibility:I
    .end local v1           #movieVisibility:I
    :cond_0
    move v0, v1

    .line 265
    goto :goto_0

    .restart local v0       #loadingVisibility:I
    :cond_1
    move v1, v2

    .line 266
    goto :goto_1
.end method

.method private static updateWidgets(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V
    .locals 3
    .parameter "appWidgetManager"
    .parameter "appWidgetIds"
    .parameter "views"

    .prologue
    .line 192
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_0

    .line 195
    return-void

    .line 192
    :cond_0
    aget v0, p1, v1

    .line 193
    .local v0, appWidgetId:I
    invoke-virtual {p0, v0, p2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 192
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private updateWidgets(Lnet/flixster/android/model/Movie;)V
    .locals 3
    .parameter "movie"

    .prologue
    .line 183
    const-string v1, "FlxWdgt"

    const-string v2, "WidgetProvider.updateWidgets"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v1, p0, Lcom/flixster/android/widget/WidgetProvider;->context:Landroid/content/Context;

    iget v2, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    invoke-static {v1, p1, v2}, Lcom/flixster/android/widget/WidgetProvider;->createMovieViews(Landroid/content/Context;Lnet/flixster/android/model/Movie;I)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 185
    .local v0, views:Landroid/widget/RemoteViews;
    if-nez v0, :cond_0

    .line 189
    :goto_0
    return-void

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    iget-object v2, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetIds:[I

    invoke-static {v1, v2, v0}, Lcom/flixster/android/widget/WidgetProvider;->updateWidgets(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 52
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 53
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, action:Ljava/lang/String;
    const-string v3, "FlxWdgt"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "WidgetProvider.onReceive "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v3, "flixster.intent.action.widget"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 76
    :goto_0
    return-void

    .line 59
    :cond_0
    new-instance v1, Landroid/content/ComponentName;

    const-class v3, Lcom/flixster/android/widget/WidgetProvider;

    invoke-direct {v1, p1, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v1, cn:Landroid/content/ComponentName;
    iput-object p1, p0, Lcom/flixster/android/widget/WidgetProvider;->context:Landroid/content/Context;

    .line 61
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    iput-object v3, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    .line 62
    iget-object v3, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v3, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v3

    iput-object v3, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetIds:[I

    .line 64
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 65
    .local v2, extras:Landroid/os/Bundle;
    const-string v3, "KEY_REFRESH"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 66
    iget-object v3, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    iget-object v4, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetIds:[I

    invoke-virtual {p0, p1, v3, v4}, Lcom/flixster/android/widget/WidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 68
    :cond_1
    const-string v3, "KEY_INDEX"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/flixster/android/widget/WidgetProvider;->currIndex:I

    .line 69
    const-string v3, "KEY_UP"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/flixster/android/widget/WidgetProvider;->isDirectionUp:Ljava/lang/Boolean;

    .line 70
    sget-object v3, Lcom/flixster/android/widget/WidgetProvider;->moviesForWidget:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 71
    iget-object v3, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    iget-object v4, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetIds:[I

    invoke-virtual {p0, p1, v3, v4}, Lcom/flixster/android/widget/WidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 73
    :cond_2
    iget-object v3, p0, Lcom/flixster/android/widget/WidgetProvider;->successHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 3
    .parameter "context"
    .parameter "appWidgetManager"
    .parameter "appWidgetIds"

    .prologue
    .line 80
    const-string v1, "FlxWdgt"

    const-string v2, "WidgetProvider.onUpdate"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iput-object p1, p0, Lcom/flixster/android/widget/WidgetProvider;->context:Landroid/content/Context;

    .line 82
    iput-object p2, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    .line 83
    iput-object p3, p0, Lcom/flixster/android/widget/WidgetProvider;->appWidgetIds:[I

    .line 85
    invoke-static {p1}, Lcom/flixster/android/widget/WidgetProvider;->createLoadingViews(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 86
    .local v0, views:Landroid/widget/RemoteViews;
    invoke-static {p2, p3, v0}, Lcom/flixster/android/widget/WidgetProvider;->updateWidgets(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V

    .line 88
    sget-object v2, Lcom/flixster/android/widget/WidgetProvider;->moviesForWidget:Ljava/util/List;

    monitor-enter v2

    .line 89
    :try_start_0
    sget-object v1, Lcom/flixster/android/widget/WidgetProvider;->moviesForWidget:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 88
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v1

    iget-object v2, p0, Lcom/flixster/android/widget/WidgetProvider;->boxOfficeMoviesFetcher:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 92
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/flixster/android/widget/WidgetConfigChangeService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 93
    return-void

    .line 88
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
