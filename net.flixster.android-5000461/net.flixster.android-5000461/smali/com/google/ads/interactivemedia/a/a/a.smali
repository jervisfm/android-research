.class public final Lcom/google/ads/interactivemedia/a/a/a;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/a/a;


# instance fields
.field private final a:Lcom/google/ads/interactivemedia/a/a/d;

.field private final b:Landroid/content/Context;

.field private c:Lcom/google/ads/interactivemedia/a/a$c;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/a/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private volatile e:Z

.field private final f:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/ads/interactivemedia/a/a/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 54
    new-instance v0, Lcom/google/ads/interactivemedia/a/a/d;

    invoke-direct {v0, p1}, Lcom/google/ads/interactivemedia/a/a/d;-><init>(Landroid/content/Context;)V

    const-string v1, "http://s0.2mdn.net/instream/html5/native/native_sdk.html"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/ads/interactivemedia/a/a/a;-><init>(Landroid/content/Context;Lcom/google/ads/interactivemedia/a/a/d;Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/ads/interactivemedia/a/a/d;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->d:Ljava/util/Map;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->e:Z

    .line 51
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->f:Ljava/util/concurrent/BlockingQueue;

    .line 58
    iput-object p2, p0, Lcom/google/ads/interactivemedia/a/a/a;->a:Lcom/google/ads/interactivemedia/a/a/d;

    .line 59
    iput-object p1, p0, Lcom/google/ads/interactivemedia/a/a/a;->b:Landroid/content/Context;

    .line 61
    new-instance v0, Lcom/google/ads/interactivemedia/a/a/a$1;

    invoke-direct {v0, p0}, Lcom/google/ads/interactivemedia/a/a/a$1;-><init>(Lcom/google/ads/interactivemedia/a/a/a;)V

    invoke-virtual {p2, v0}, Lcom/google/ads/interactivemedia/a/a/d;->a(Lcom/google/ads/interactivemedia/a/a/d$a;)V

    .line 68
    invoke-virtual {p2, p3}, Lcom/google/ads/interactivemedia/a/a/d;->a(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method private static a(Lorg/json/JSONObject;)Ljava/util/Map;
    .locals 13
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/a/a$a;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 191
    const-string v0, "companions"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 211
    :goto_0
    return-object v0

    .line 195
    :cond_0
    const-string v0, "companions"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 196
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 197
    invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 199
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 200
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 201
    :goto_2
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v1, v8, :cond_1

    .line 202
    invoke-virtual {v6, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 203
    new-instance v9, Lcom/google/ads/interactivemedia/a/a$a;

    const-string v10, "companionId"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    const-string v11, "src"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "clickThru"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v10, v0, v11, v8}, Lcom/google/ads/interactivemedia/a/a$a;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 209
    :cond_1
    invoke-interface {v3, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    move-object v0, v3

    .line 211
    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 98
    .line 99
    :goto_0
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/a/a/b;

    if-eqz v0, :cond_0

    .line 100
    iget-object v1, p0, Lcom/google/ads/interactivemedia/a/a/a;->a:Lcom/google/ads/interactivemedia/a/a/d;

    invoke-virtual {v1, v0}, Lcom/google/ads/interactivemedia/a/a/d;->a(Lcom/google/ads/interactivemedia/a/a/b;)V

    goto :goto_0

    .line 102
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/ads/interactivemedia/a/a$c;)V
    .locals 0
    .parameter

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/ads/interactivemedia/a/a/a;->c:Lcom/google/ads/interactivemedia/a/a$c;

    .line 74
    return-void
.end method

.method protected final a(Lcom/google/ads/interactivemedia/a/a/b;)V
    .locals 6
    .parameter

    .prologue
    .line 161
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/a/a/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 162
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/a/a/b;->b()Ljava/util/Map;

    move-result-object v1

    .line 163
    const-string v2, "error"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 164
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/a/a/b;->b()Ljava/util/Map;

    move-result-object v2

    const-string v0, "type"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "adLoadError"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/ads/interactivemedia/a/a/a;->c:Lcom/google/ads/interactivemedia/a/a$c;

    const-string v0, "userContext"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "code"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v4, "message"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v3, v0, v1, v2}, Lcom/google/ads/interactivemedia/a/a$c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :goto_0
    return-void

    .line 164
    :cond_0
    const-string v1, "adPlayError"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->d:Ljava/util/Map;

    const-string v1, "adsManagerId"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/a/a$b;

    if-nez v0, :cond_1

    const-string v0, "IMASDK"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error for unknown manager: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v1, "code"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "message"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/ads/interactivemedia/a/a$b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "IMASDK"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 165
    :cond_3
    const-string v2, "log"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 166
    const-string v2, "IMASDK"

    const-string v0, "logMessage"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 167
    :cond_4
    const-string v2, "webViewLoaded"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 168
    const-string v0, "IMASDK"

    const-string v1, "SDK loaded"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->e:Z

    .line 170
    invoke-direct {p0}, Lcom/google/ads/interactivemedia/a/a/a;->a()V

    goto/16 :goto_0

    .line 171
    :cond_5
    const-string v2, "video"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 173
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    const-string v0, "responseJSON"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 174
    const-string v0, "adsManagerId"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2}, Lcom/google/ads/interactivemedia/a/a/a;->a(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v1

    new-instance v3, Lcom/google/ads/interactivemedia/a/a$a;

    const-string v4, "url"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "clickThru"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/ads/interactivemedia/a/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/ads/interactivemedia/a/a/a;->c:Lcom/google/ads/interactivemedia/a/a$c;

    const-string v5, "userContext"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2, v0, v3, v1}, Lcom/google/ads/interactivemedia/a/a$c;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ads/interactivemedia/a/a$a;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 176
    :catch_0
    move-exception v0

    const-string v0, "IMASDK"

    const-string v1, "Error parsing video ad response"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 179
    :cond_6
    const-string v0, "IMASDK"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5
    .parameter

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->a:Lcom/google/ads/interactivemedia/a/a/d;

    const-string v1, "unloadAd"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "adsManagerId"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/ads/interactivemedia/a/a/b;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/ads/interactivemedia/a/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ads/interactivemedia/a/a/d;->a(Lcom/google/ads/interactivemedia/a/a/b;)V

    .line 129
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->a:Lcom/google/ads/interactivemedia/a/a/d;

    const-string v1, "companionViewEvent"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "adsManagerId"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const-string v4, "companionId"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/ads/interactivemedia/a/a/b;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/ads/interactivemedia/a/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ads/interactivemedia/a/a/d;->a(Lcom/google/ads/interactivemedia/a/a/b;)V

    .line 117
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/ads/interactivemedia/a/a$a;Landroid/view/ViewGroup;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 240
    invoke-virtual {p3}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 241
    new-instance v0, Lcom/google/ads/interactivemedia/a/a/c;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/ads/interactivemedia/a/a/c;-><init>(Landroid/content/Context;Lcom/google/ads/interactivemedia/a/a;Ljava/lang/String;Lcom/google/ads/interactivemedia/a/a$a;)V

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 243
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/ads/interactivemedia/a/a$b;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->a:Lcom/google/ads/interactivemedia/a/a/d;

    const-string v1, "vastEvent"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "adsManagerId"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const-string v4, "eventType"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Lcom/google/ads/interactivemedia/a/a/b;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/ads/interactivemedia/a/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ads/interactivemedia/a/a/d;->a(Lcom/google/ads/interactivemedia/a/a/b;)V

    .line 110
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 11
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 83
    const-string v1, "requestAd"

    const/16 v0, 0x8

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "userContext"

    aput-object v0, v2, v7

    aput-object p1, v2, v8

    const-string v0, "mimeType"

    aput-object v0, v2, v9

    const-string v0, "video/mp4"

    aput-object v0, v2, v10

    const/4 v0, 0x4

    const-string v3, "network"

    aput-object v3, v2, v0

    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->b:Landroid/content/Context;

    const-string v4, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "IMASDK"

    const-string v4, "Host application doesn\'t have ACCESS_NETWORK_STATE permission"

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "android:0"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x6

    const-string v3, "env"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    const-string v3, "android%s:%s:%s"

    new-array v4, v10, [Ljava/lang/Object;

    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v5, v4, v7

    const-string v5, "0.1"

    aput-object v5, v4, v8

    iget-object v5, p0, Lcom/google/ads/interactivemedia/a/a/a;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/google/ads/interactivemedia/a/a/b;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/ads/interactivemedia/a/a/b;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/a/a/b;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 90
    iget-object v1, p0, Lcom/google/ads/interactivemedia/a/a/a;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 92
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->e:Z

    if-eqz v0, :cond_0

    .line 93
    invoke-direct {p0}, Lcom/google/ads/interactivemedia/a/a/a;->a()V

    .line 95
    :cond_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/a/a;->b:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "android:0"

    goto :goto_0

    :cond_2
    const-string v4, "android:%d:%d"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 134
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 135
    iget-object v1, p0, Lcom/google/ads/interactivemedia/a/a/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 136
    return-void
.end method
