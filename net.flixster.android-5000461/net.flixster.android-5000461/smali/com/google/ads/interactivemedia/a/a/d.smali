.class public final Lcom/google/ads/interactivemedia/a/a/d;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/interactivemedia/a/a/d$a;
    }
.end annotation


# instance fields
.field private a:Lcom/google/ads/interactivemedia/a/a/d$a;

.field private b:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 29
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/google/ads/interactivemedia/a/a/d;-><init>(Landroid/webkit/WebView;)V

    .line 30
    return-void
.end method

.method private constructor <init>(Landroid/webkit/WebView;)V
    .locals 2
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/ads/interactivemedia/a/a/d;->b:Landroid/webkit/WebView;

    .line 34
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 35
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 36
    new-instance v0, Lcom/google/ads/interactivemedia/a/a/d$1;

    invoke-direct {v0, p0}, Lcom/google/ads/interactivemedia/a/a/d$1;-><init>(Lcom/google/ads/interactivemedia/a/a/d;)V

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 63
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/ads/interactivemedia/a/a/b;)V
    .locals 3
    .parameter

    .prologue
    .line 74
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/a/a/b;->c()Ljava/lang/String;

    move-result-object v0

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sending gmsg: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 76
    iget-object v1, p0, Lcom/google/ads/interactivemedia/a/a/d;->b:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public final a(Lcom/google/ads/interactivemedia/a/a/d$a;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/ads/interactivemedia/a/a/d;->a:Lcom/google/ads/interactivemedia/a/a/d$a;

    .line 67
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/a/d;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 80
    invoke-static {p1}, Lcom/google/ads/interactivemedia/a/a/b;->a(Ljava/lang/String;)Lcom/google/ads/interactivemedia/a/a/b;

    move-result-object v0

    .line 81
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received gmsg: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/google/ads/interactivemedia/a/a/d;->a:Lcom/google/ads/interactivemedia/a/a/d$a;

    invoke-interface {v1, v0}, Lcom/google/ads/interactivemedia/a/a/d$a;->a(Lcom/google/ads/interactivemedia/a/a/b;)V

    .line 83
    return-void
.end method
