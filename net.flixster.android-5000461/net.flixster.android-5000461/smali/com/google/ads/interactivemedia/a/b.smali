.class public final Lcom/google/ads/interactivemedia/a/b;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/api/AdErrorListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/a/b;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 32
    return-void
.end method

.method public final a(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError;

    invoke-direct {v0, p2, p3, p4}, Lcom/google/ads/interactivemedia/api/AdError;-><init>(Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;Ljava/lang/String;)V

    .line 42
    new-instance v1, Lcom/google/ads/interactivemedia/api/AdErrorEvent;

    invoke-direct {v1, p1, v0}, Lcom/google/ads/interactivemedia/api/AdErrorEvent;-><init>(Ljava/lang/Object;Lcom/google/ads/interactivemedia/api/AdError;)V

    .line 43
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/AdErrorListener;

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/api/AdErrorListener;->onAdError(Lcom/google/ads/interactivemedia/api/AdErrorEvent;)V

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method
