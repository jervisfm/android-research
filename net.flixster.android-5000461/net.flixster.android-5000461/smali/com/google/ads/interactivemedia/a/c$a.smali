.class final enum Lcom/google/ads/interactivemedia/a/c$a;
.super Ljava/lang/Enum;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/interactivemedia/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/ads/interactivemedia/a/c$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum b:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum c:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum d:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum e:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum f:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum g:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum h:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum i:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum j:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum k:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum l:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum m:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum n:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum o:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum p:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum q:Lcom/google/ads/interactivemedia/a/c$a;

.field public static final enum r:Lcom/google/ads/interactivemedia/a/c$a;

.field private static final synthetic u:[Lcom/google/ads/interactivemedia/a/c$a;


# instance fields
.field private s:Z

.field private t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 23
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "CREATIVE_VIEW"

    const-string v2, "creativeview"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->a:Lcom/google/ads/interactivemedia/a/c$a;

    .line 24
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "START"

    const-string v2, "start"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->b:Lcom/google/ads/interactivemedia/a/c$a;

    .line 25
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "MIDPOINT"

    const-string v2, "midpoint"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->c:Lcom/google/ads/interactivemedia/a/c$a;

    .line 26
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "FIRST_QUARTILE"

    const-string v2, "firstquartile"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->d:Lcom/google/ads/interactivemedia/a/c$a;

    .line 27
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "THIRD_QUARTILE"

    const-string v2, "thirdquartile"

    invoke-direct {v0, v1, v8, v2}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->e:Lcom/google/ads/interactivemedia/a/c$a;

    .line 28
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "COMPLETE"

    const/4 v2, 0x5

    const-string v3, "complete"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->f:Lcom/google/ads/interactivemedia/a/c$a;

    .line 29
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "MUTE"

    const/4 v2, 0x6

    const-string v3, "mute"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->g:Lcom/google/ads/interactivemedia/a/c$a;

    .line 30
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "UNMUTE"

    const/4 v2, 0x7

    const-string v3, "unmute"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->h:Lcom/google/ads/interactivemedia/a/c$a;

    .line 31
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "PAUSE"

    const/16 v2, 0x8

    const-string v3, "pause"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->i:Lcom/google/ads/interactivemedia/a/c$a;

    .line 32
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "REWIND"

    const/16 v2, 0x9

    const-string v3, "rewind"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->j:Lcom/google/ads/interactivemedia/a/c$a;

    .line 33
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "RESUME"

    const/16 v2, 0xa

    const-string v3, "resume"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->k:Lcom/google/ads/interactivemedia/a/c$a;

    .line 34
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "FULLSCREEN"

    const/16 v2, 0xb

    const-string v3, "fullscreen"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->l:Lcom/google/ads/interactivemedia/a/c$a;

    .line 35
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "EXPAND"

    const/16 v2, 0xc

    const-string v3, "expand"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->m:Lcom/google/ads/interactivemedia/a/c$a;

    .line 36
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "COLLAPSE"

    const/16 v2, 0xd

    const-string v3, "collapse"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->n:Lcom/google/ads/interactivemedia/a/c$a;

    .line 37
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "CLOSE"

    const/16 v2, 0xe

    const-string v3, "close"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->o:Lcom/google/ads/interactivemedia/a/c$a;

    .line 38
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "ACCEPT_INVITATION"

    const/16 v2, 0xf

    const-string v3, "acceptinvitation"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->p:Lcom/google/ads/interactivemedia/a/c$a;

    .line 39
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "CLICK"

    const-string v2, "click"

    invoke-direct {v0, v1, v2}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->q:Lcom/google/ads/interactivemedia/a/c$a;

    .line 40
    new-instance v0, Lcom/google/ads/interactivemedia/a/c$a;

    const-string v1, "ERROR"

    const/16 v2, 0x11

    const-string v3, "error"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->r:Lcom/google/ads/interactivemedia/a/c$a;

    .line 22
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/google/ads/interactivemedia/a/c$a;

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->a:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->b:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->c:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->d:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->e:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->f:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->g:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->h:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->i:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->j:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->k:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->l:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->m:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->n:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->o:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->p:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->q:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/ads/interactivemedia/a/c$a;->r:Lcom/google/ads/interactivemedia/a/c$a;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/ads/interactivemedia/a/c$a;->u:[Lcom/google/ads/interactivemedia/a/c$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ads/interactivemedia/a/c$a;->s:Z

    .line 52
    iput-object p3, p0, Lcom/google/ads/interactivemedia/a/c$a;->t:Ljava/lang/String;

    .line 53
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    const/16 v0, 0x10

    invoke-direct {p0, p1, v0, p2}, Lcom/google/ads/interactivemedia/a/c$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ads/interactivemedia/a/c$a;->s:Z

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/ads/interactivemedia/a/c$a;
    .locals 1
    .parameter "name"

    .prologue
    .line 22
    const-class v0, Lcom/google/ads/interactivemedia/a/c$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/a/c$a;

    return-object v0
.end method

.method public static values()[Lcom/google/ads/interactivemedia/a/c$a;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/ads/interactivemedia/a/c$a;->u:[Lcom/google/ads/interactivemedia/a/c$a;

    invoke-virtual {v0}, [Lcom/google/ads/interactivemedia/a/c$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ads/interactivemedia/a/c$a;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/a/c$a;->s:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/c$a;->t:Ljava/lang/String;

    return-object v0
.end method
