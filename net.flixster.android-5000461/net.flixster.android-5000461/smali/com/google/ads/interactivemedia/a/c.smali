.class public final Lcom/google/ads/interactivemedia/a/c;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/interactivemedia/a/c$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/ads/interactivemedia/a/c$a;",
            "Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/google/ads/interactivemedia/a/a;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/ads/interactivemedia/a/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;

.field private g:Z

.field private h:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 68
    sput-object v0, Lcom/google/ads/interactivemedia/a/c;->a:Ljava/util/Map;

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->q:Lcom/google/ads/interactivemedia/a/c$a;

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->CLICK:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/google/ads/interactivemedia/a/c;->a:Ljava/util/Map;

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->f:Lcom/google/ads/interactivemedia/a/c$a;

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->COMPLETE:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/google/ads/interactivemedia/a/c;->a:Ljava/util/Map;

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->d:Lcom/google/ads/interactivemedia/a/c$a;

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->FIRST_QUARTILE:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/google/ads/interactivemedia/a/c;->a:Ljava/util/Map;

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->c:Lcom/google/ads/interactivemedia/a/c$a;

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->MIDPOINT:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/google/ads/interactivemedia/a/c;->a:Ljava/util/Map;

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->i:Lcom/google/ads/interactivemedia/a/c$a;

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->PAUSED:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/google/ads/interactivemedia/a/c;->a:Ljava/util/Map;

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->b:Lcom/google/ads/interactivemedia/a/c$a;

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->STARTED:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/google/ads/interactivemedia/a/c;->a:Ljava/util/Map;

    sget-object v1, Lcom/google/ads/interactivemedia/a/c$a;->e:Lcom/google/ads/interactivemedia/a/c$a;

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->THIRD_QUARTILE:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/google/ads/interactivemedia/a/a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const-class v0, Lcom/google/ads/interactivemedia/a/c$a;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ads/interactivemedia/a/c;->e:Ljava/util/EnumSet;

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ads/interactivemedia/a/c;->g:Z

    .line 89
    iput-object p1, p0, Lcom/google/ads/interactivemedia/a/c;->b:Lcom/google/ads/interactivemedia/a/a;

    .line 90
    iput-object p2, p0, Lcom/google/ads/interactivemedia/a/c;->c:Ljava/lang/String;

    .line 91
    iput-object p3, p0, Lcom/google/ads/interactivemedia/a/c;->d:Ljava/lang/String;

    .line 92
    return-void
.end method

.method private a(JJDLcom/google/ads/interactivemedia/a/c$a;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 156
    long-to-double v0, p3

    cmpg-double v0, v0, p5

    if-gez v0, :cond_0

    long-to-double v0, p1

    cmpl-double v0, v0, p5

    if-ltz v0, :cond_0

    .line 157
    invoke-direct {p0, p7}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/a/c$a;)V

    .line 159
    :cond_0
    return-void
.end method

.method private a(Lcom/google/ads/interactivemedia/a/c$a;)V
    .locals 3
    .parameter

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/c;->e:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/a/c$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/c;->e:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/c;->b:Lcom/google/ads/interactivemedia/a/a;

    iget-object v1, p0, Lcom/google/ads/interactivemedia/a/c;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/a/c$a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/ads/interactivemedia/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_1
    sget-object v0, Lcom/google/ads/interactivemedia/a/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 168
    iget-object v1, p0, Lcom/google/ads/interactivemedia/a/c;->f:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;

    new-instance v2, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;

    sget-object v0, Lcom/google/ads/interactivemedia/a/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-direct {v2, v0}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;-><init>(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;)V

    invoke-interface {v1, v2}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;->onAdEvent(Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;)V

    .line 170
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;)V
    .locals 0
    .parameter

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/ads/interactivemedia/a/c;->f:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;

    .line 96
    return-void
.end method

.method public final onClick()V
    .locals 2

    .prologue
    .line 145
    sget-object v0, Lcom/google/ads/interactivemedia/a/c$a;->q:Lcom/google/ads/interactivemedia/a/c$a;

    invoke-direct {p0, v0}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/a/c$a;)V

    .line 146
    iget-object v0, p0, Lcom/google/ads/interactivemedia/a/c;->b:Lcom/google/ads/interactivemedia/a/a;

    iget-object v1, p0, Lcom/google/ads/interactivemedia/a/c;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/a/a;->b(Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method public final onComplete()V
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/google/ads/interactivemedia/a/c$a;->f:Lcom/google/ads/interactivemedia/a/c$a;

    invoke-direct {p0, v0}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/a/c$a;)V

    .line 152
    return-void
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/google/ads/interactivemedia/a/c$a;->i:Lcom/google/ads/interactivemedia/a/c$a;

    invoke-direct {p0, v0}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/a/c$a;)V

    .line 107
    return-void
.end method

.method public final onPlay()V
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/google/ads/interactivemedia/a/c$a;->a:Lcom/google/ads/interactivemedia/a/c$a;

    invoke-direct {p0, v0}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/a/c$a;)V

    .line 101
    sget-object v0, Lcom/google/ads/interactivemedia/a/c$a;->b:Lcom/google/ads/interactivemedia/a/c$a;

    invoke-direct {p0, v0}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/a/c$a;)V

    .line 102
    return-void
.end method

.method public final onProgress(JJ)V
    .locals 10
    .parameter "currentTime"
    .parameter "totalTime"

    .prologue
    .line 129
    iget-wide v3, p0, Lcom/google/ads/interactivemedia/a/c;->h:J

    .line 130
    iput-wide p1, p0, Lcom/google/ads/interactivemedia/a/c;->h:J

    .line 132
    cmp-long v0, p1, v3

    if-gez v0, :cond_0

    .line 133
    sget-object v0, Lcom/google/ads/interactivemedia/a/c$a;->j:Lcom/google/ads/interactivemedia/a/c$a;

    invoke-direct {p0, v0}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/a/c$a;)V

    .line 141
    :goto_0
    return-void

    .line 137
    :cond_0
    long-to-double v0, p3

    const-wide/high16 v5, 0x4010

    div-double v8, v0, v5

    .line 138
    const-wide/high16 v0, 0x3ff0

    mul-double v5, v8, v0

    sget-object v7, Lcom/google/ads/interactivemedia/a/c$a;->d:Lcom/google/ads/interactivemedia/a/c$a;

    move-object v0, p0

    move-wide v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/ads/interactivemedia/a/c;->a(JJDLcom/google/ads/interactivemedia/a/c$a;)V

    .line 139
    const-wide/high16 v0, 0x4000

    mul-double v5, v8, v0

    sget-object v7, Lcom/google/ads/interactivemedia/a/c$a;->c:Lcom/google/ads/interactivemedia/a/c$a;

    move-object v0, p0

    move-wide v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/ads/interactivemedia/a/c;->a(JJDLcom/google/ads/interactivemedia/a/c$a;)V

    .line 140
    const-wide/high16 v0, 0x4008

    mul-double v5, v8, v0

    sget-object v7, Lcom/google/ads/interactivemedia/a/c$a;->e:Lcom/google/ads/interactivemedia/a/c$a;

    move-object v0, p0

    move-wide v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/ads/interactivemedia/a/c;->a(JJDLcom/google/ads/interactivemedia/a/c$a;)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/google/ads/interactivemedia/a/c$a;->k:Lcom/google/ads/interactivemedia/a/c$a;

    invoke-direct {p0, v0}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/a/c$a;)V

    .line 112
    return-void
.end method

.method public final onVolumeChanged(I)V
    .locals 1
    .parameter "percentage"

    .prologue
    .line 116
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/a/c;->g:Z

    if-nez v0, :cond_0

    .line 117
    sget-object v0, Lcom/google/ads/interactivemedia/a/c$a;->g:Lcom/google/ads/interactivemedia/a/c$a;

    invoke-direct {p0, v0}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/a/c$a;)V

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ads/interactivemedia/a/c;->g:Z

    .line 121
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/a/c;->g:Z

    if-eqz v0, :cond_1

    .line 122
    sget-object v0, Lcom/google/ads/interactivemedia/a/c$a;->h:Lcom/google/ads/interactivemedia/a/c$a;

    invoke-direct {p0, v0}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/a/c$a;)V

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ads/interactivemedia/a/c;->g:Z

    .line 125
    :cond_1
    return-void
.end method
