.class public final enum Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;
.super Ljava/lang/Enum;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/interactivemedia/api/AdError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AdErrorCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ADSLOT_NOT_VISIBLE:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum COMPANION_AD_LOADING_FAILED:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum FAILED_TO_REQUEST_ADS:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum INTERNAL_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum INVALID_ARGUMENTS:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum OVERLAY_AD_LOADING_FAILED:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum OVERLAY_AD_PLAYING_FAILED:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum UNKNOWN_AD_RESPONSE:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum UNKNOWN_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum VAST_ASSET_MISMATCH:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum VAST_ASSET_NOT_FOUND:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum VAST_INVALID_URL:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum VAST_LOAD_TIMEOUT:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum VAST_MALFORMED_RESPONSE:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum VAST_MEDIA_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum VAST_TOO_MANY_REDIRECTS:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field public static final enum VIDEO_PLAY_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field private static final synthetic b:[Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 44
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "INTERNAL_ERROR"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->INTERNAL_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 52
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "VIDEO_PLAY_ERROR"

    const/16 v2, 0x3eb

    invoke-direct {v0, v1, v5, v2}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VIDEO_PLAY_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 54
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "FAILED_TO_REQUEST_ADS"

    const/16 v2, 0x3ec

    invoke-direct {v0, v1, v6, v2}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->FAILED_TO_REQUEST_ADS:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 59
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "VAST_LOAD_TIMEOUT"

    const/16 v2, 0xbb9

    invoke-direct {v0, v1, v7, v2}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_LOAD_TIMEOUT:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 64
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "VAST_INVALID_URL"

    const/16 v2, 0xbba

    invoke-direct {v0, v1, v8, v2}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_INVALID_URL:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 66
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "VAST_MALFORMED_RESPONSE"

    const/4 v2, 0x5

    const/16 v3, 0xbbb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_MALFORMED_RESPONSE:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 68
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "VAST_MEDIA_ERROR"

    const/4 v2, 0x6

    const/16 v3, 0xbbc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_MEDIA_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 70
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "VAST_TOO_MANY_REDIRECTS"

    const/4 v2, 0x7

    const/16 v3, 0xbbd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_TOO_MANY_REDIRECTS:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 75
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "VAST_ASSET_MISMATCH"

    const/16 v2, 0x8

    const/16 v3, 0xbbe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_ASSET_MISMATCH:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 77
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "VAST_ASSET_NOT_FOUND"

    const/16 v2, 0x9

    const/16 v3, 0xbbf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_ASSET_NOT_FOUND:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 79
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "INVALID_ARGUMENTS"

    const/16 v2, 0xa

    const/16 v3, 0xc1d

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->INVALID_ARGUMENTS:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 81
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "COMPANION_AD_LOADING_FAILED"

    const/16 v2, 0xb

    const/16 v3, 0xc1e

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->COMPANION_AD_LOADING_FAILED:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 83
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "UNKNOWN_AD_RESPONSE"

    const/16 v2, 0xc

    const/16 v3, 0xc1f

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->UNKNOWN_AD_RESPONSE:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 88
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "UNKNOWN_ERROR"

    const/16 v2, 0xd

    const/16 v3, 0xc20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->UNKNOWN_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 90
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "OVERLAY_AD_LOADING_FAILED"

    const/16 v2, 0xe

    const/16 v3, 0xc21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->OVERLAY_AD_LOADING_FAILED:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 92
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "OVERLAY_AD_PLAYING_FAILED"

    const/16 v2, 0xf

    const/16 v3, 0xc22

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->OVERLAY_AD_PLAYING_FAILED:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 94
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    const-string v1, "ADSLOT_NOT_VISIBLE"

    const/16 v2, 0x10

    const/16 v3, 0xc23

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->ADSLOT_NOT_VISIBLE:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 41
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    sget-object v1, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->INTERNAL_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VIDEO_PLAY_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->FAILED_TO_REQUEST_ADS:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_LOAD_TIMEOUT:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_INVALID_URL:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_MALFORMED_RESPONSE:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_MEDIA_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_TOO_MANY_REDIRECTS:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_ASSET_MISMATCH:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->VAST_ASSET_NOT_FOUND:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->INVALID_ARGUMENTS:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->COMPANION_AD_LOADING_FAILED:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->UNKNOWN_AD_RESPONSE:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->UNKNOWN_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->OVERLAY_AD_LOADING_FAILED:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->OVERLAY_AD_PLAYING_FAILED:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->ADSLOT_NOT_VISIBLE:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->b:[Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .parameter
    .parameter
    .parameter "errorNumber"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 100
    iput p3, p0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->a:I

    .line 101
    return-void
.end method

.method static a(I)Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;
    .locals 5
    .parameter

    .prologue
    .line 108
    invoke-static {}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->values()[Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 109
    iget v4, v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->a:I

    if-ne v4, p0, :cond_0

    .line 114
    :goto_1
    return-object v0

    .line 108
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 114
    :cond_1
    sget-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->UNKNOWN_ERROR:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;
    .locals 1
    .parameter "name"

    .prologue
    .line 41
    const-class v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    return-object v0
.end method

.method public static values()[Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->b:[Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    invoke-virtual {v0}, [Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    return-object v0
.end method
