.class public Lcom/google/ads/interactivemedia/api/AdError;
.super Ljava/lang/Exception;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;,
        Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

.field private final b:Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;


# direct methods
.method public constructor <init>(Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;Ljava/lang/String;)V
    .locals 0
    .parameter "errorType"
    .parameter "errorCode"
    .parameter "detailMessage"

    .prologue
    .line 122
    invoke-direct {p0, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 123
    iput-object p1, p0, Lcom/google/ads/interactivemedia/api/AdError;->b:Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;

    .line 124
    iput-object p2, p0, Lcom/google/ads/interactivemedia/api/AdError;->a:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    .line 125
    return-void
.end method


# virtual methods
.method public getErrorCode()Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdError;->a:Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    return-object v0
.end method

.method public getErrorType()Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdError;->b:Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
