.class public Lcom/google/ads/interactivemedia/api/AdErrorEvent;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:Lcom/google/ads/interactivemedia/api/AdError;

.field private final b:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/ads/interactivemedia/api/AdError;)V
    .locals 1
    .parameter "error"

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/ads/interactivemedia/api/AdErrorEvent;->a:Lcom/google/ads/interactivemedia/api/AdError;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ads/interactivemedia/api/AdErrorEvent;->b:Ljava/lang/Object;

    .line 17
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/google/ads/interactivemedia/api/AdError;)V
    .locals 0
    .parameter "userRequestContext"
    .parameter "error"

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p2, p0, Lcom/google/ads/interactivemedia/api/AdErrorEvent;->a:Lcom/google/ads/interactivemedia/api/AdError;

    .line 21
    iput-object p1, p0, Lcom/google/ads/interactivemedia/api/AdErrorEvent;->b:Ljava/lang/Object;

    .line 22
    return-void
.end method


# virtual methods
.method public getError()Lcom/google/ads/interactivemedia/api/AdError;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdErrorEvent;->a:Lcom/google/ads/interactivemedia/api/AdError;

    return-object v0
.end method

.method public getUserRequestContext()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdErrorEvent;->b:Ljava/lang/Object;

    return-object v0
.end method
