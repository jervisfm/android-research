.class final Lcom/google/ads/interactivemedia/api/AdsLoader$1;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/a/a$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/ads/interactivemedia/api/AdsLoader;-><init>(Lcom/google/ads/interactivemedia/a/a;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/ads/interactivemedia/a/a;

.field final synthetic b:Lcom/google/ads/interactivemedia/api/AdsLoader;


# direct methods
.method constructor <init>(Lcom/google/ads/interactivemedia/api/AdsLoader;Lcom/google/ads/interactivemedia/a/a;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$1;->b:Lcom/google/ads/interactivemedia/api/AdsLoader;

    iput-object p2, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$1;->a:Lcom/google/ads/interactivemedia/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ads/interactivemedia/a/a$a;Ljava/util/Map;)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/a/a$a;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/a/a$a;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$1;->b:Lcom/google/ads/interactivemedia/api/AdsLoader;

    iget-object v0, v0, Lcom/google/ads/interactivemedia/api/AdsLoader;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/ads/interactivemedia/api/AdsRequest;

    .line 77
    new-instance v0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;

    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$1;->a:Lcom/google/ads/interactivemedia/a/a;

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;-><init>(Lcom/google/ads/interactivemedia/a/a;Ljava/lang/String;Lcom/google/ads/interactivemedia/api/AdsRequest;Lcom/google/ads/interactivemedia/a/a$a;Ljava/util/Map;)V

    .line 79
    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$1;->b:Lcom/google/ads/interactivemedia/api/AdsLoader;

    iget-object v1, v1, Lcom/google/ads/interactivemedia/api/AdsLoader;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedListener;

    .line 80
    new-instance v4, Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;

    invoke-virtual {v3}, Lcom/google/ads/interactivemedia/api/AdsRequest;->getUserRequestContext()Ljava/lang/Object;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;-><init>(Ljava/lang/Object;Lcom/google/ads/interactivemedia/api/AdsManager;)V

    invoke-interface {v1, v4}, Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedListener;->onAdsLoaded(Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;)V

    goto :goto_0

    .line 82
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$1;->b:Lcom/google/ads/interactivemedia/api/AdsLoader;

    iget-object v0, v0, Lcom/google/ads/interactivemedia/api/AdsLoader;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/AdsRequest;

    .line 87
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;->a(I)Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;

    move-result-object v1

    .line 88
    iget-object v2, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$1;->b:Lcom/google/ads/interactivemedia/api/AdsLoader;

    iget-object v2, v2, Lcom/google/ads/interactivemedia/api/AdsLoader;->a:Lcom/google/ads/interactivemedia/a/b;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/api/AdsRequest;->getUserRequestContext()Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;

    invoke-virtual {v2, v0, v3, v1, p3}, Lcom/google/ads/interactivemedia/a/b;->a(Ljava/lang/Object;Lcom/google/ads/interactivemedia/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/api/AdError$AdErrorCode;Ljava/lang/String;)V

    .line 90
    return-void
.end method
