.class public Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/interactivemedia/api/AdsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AdsLoadedEvent"
.end annotation


# instance fields
.field private final a:Lcom/google/ads/interactivemedia/api/AdsManager;

.field private final b:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/google/ads/interactivemedia/api/AdsManager;)V
    .locals 0
    .parameter "userRequestContext"
    .parameter "manager"

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p2, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;->a:Lcom/google/ads/interactivemedia/api/AdsManager;

    .line 44
    iput-object p1, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;->b:Ljava/lang/Object;

    .line 45
    return-void
.end method


# virtual methods
.method public getManager()Lcom/google/ads/interactivemedia/api/AdsManager;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;->a:Lcom/google/ads/interactivemedia/api/AdsManager;

    return-object v0
.end method

.method public getUserRequestContext()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;->b:Ljava/lang/Object;

    return-object v0
.end method
