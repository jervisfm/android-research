.class public Lcom/google/ads/interactivemedia/api/AdsLoader;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedEvent;,
        Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedListener;
    }
.end annotation


# instance fields
.field final a:Lcom/google/ads/interactivemedia/a/b;

.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedListener;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/api/AdsRequest",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/ads/interactivemedia/a/a;

.field private e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    .line 65
    new-instance v0, Lcom/google/ads/interactivemedia/a/a/a;

    invoke-direct {v0, p1}, Lcom/google/ads/interactivemedia/a/a/a;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/ads/interactivemedia/api/AdsLoader;-><init>(Lcom/google/ads/interactivemedia/a/a;J)V

    .line 66
    return-void
.end method

.method private constructor <init>(Lcom/google/ads/interactivemedia/a/a;J)V
    .locals 2
    .parameter "server"
    .parameter "contextIdBase"

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lcom/google/ads/interactivemedia/a/b;

    invoke-direct {v0}, Lcom/google/ads/interactivemedia/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->a:Lcom/google/ads/interactivemedia/a/b;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->b:Ljava/util/List;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->c:Ljava/util/Map;

    .line 69
    iput-object p1, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->d:Lcom/google/ads/interactivemedia/a/a;

    .line 70
    iput-wide p2, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->e:J

    .line 72
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdsLoader$1;

    invoke-direct {v0, p0, p1}, Lcom/google/ads/interactivemedia/api/AdsLoader$1;-><init>(Lcom/google/ads/interactivemedia/api/AdsLoader;Lcom/google/ads/interactivemedia/a/a;)V

    invoke-interface {p1, v0}, Lcom/google/ads/interactivemedia/a/a;->a(Lcom/google/ads/interactivemedia/a/a$c;)V

    .line 92
    return-void
.end method


# virtual methods
.method public addAdErrorListener(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V
    .locals 1
    .parameter "errorListener"

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->a:Lcom/google/ads/interactivemedia/a/b;

    invoke-virtual {v0, p1}, Lcom/google/ads/interactivemedia/a/b;->a(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V

    .line 104
    return-void
.end method

.method public addAdsLoadedListener(Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedListener;)V
    .locals 1
    .parameter "loadedListener"

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    return-void
.end method

.method public removeAdErrorListener(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V
    .locals 1
    .parameter "errorListener"

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->a:Lcom/google/ads/interactivemedia/a/b;

    invoke-virtual {v0, p1}, Lcom/google/ads/interactivemedia/a/b;->b(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V

    .line 108
    return-void
.end method

.method public removeAdsLoadedListener(Lcom/google/ads/interactivemedia/api/AdsLoader$AdsLoadedListener;)V
    .locals 1
    .parameter "loadedListener"

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method

.method public requestAds(Lcom/google/ads/interactivemedia/api/AdsRequest;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/ads/interactivemedia/api/AdsRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, request:Lcom/google/ads/interactivemedia/api/AdsRequest;,"Lcom/google/ads/interactivemedia/api/AdsRequest<*>;"
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "and-ctx-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->e:J

    const-wide/16 v3, 0x1

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->e:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->c:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/AdsLoader;->d:Lcom/google/ads/interactivemedia/a/a;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/api/AdsRequest;->getParameters()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/ads/interactivemedia/a/a;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 114
    return-void
.end method

.method public requestAds(Lcom/google/ads/interactivemedia/api/AdsRequest;Ljava/lang/Object;)V
    .locals 0
    .parameter
    .parameter "userRequestContext"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/ads/interactivemedia/api/AdsRequest",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, request:Lcom/google/ads/interactivemedia/api/AdsRequest;,"Lcom/google/ads/interactivemedia/api/AdsRequest<*>;"
    invoke-virtual {p1, p2}, Lcom/google/ads/interactivemedia/api/AdsRequest;->setUserRequestContext(Ljava/lang/Object;)V

    .line 119
    invoke-virtual {p0, p1}, Lcom/google/ads/interactivemedia/api/AdsLoader;->requestAds(Lcom/google/ads/interactivemedia/api/AdsRequest;)V

    .line 120
    return-void
.end method
