.class public Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/interactivemedia/api/AdsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AdEvent"
.end annotation


# instance fields
.field private final a:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;


# direct methods
.method public constructor <init>(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;)V
    .locals 0
    .parameter "eventType"

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;->a:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    .line 61
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .parameter "obj"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 74
    if-ne p0, p1, :cond_1

    .line 87
    .end local p1
    :cond_0
    :goto_0
    return v0

    .line 77
    .restart local p1
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 81
    goto :goto_0

    .line 83
    :cond_3
    check-cast p1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;

    .line 84
    .end local p1
    iget-object v2, p0, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;->a:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    iget-object v3, p1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;->a:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 85
    goto :goto_0
.end method

.method public getEventType()Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;->a:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<AdEvent "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;->a:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
