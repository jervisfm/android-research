.class public abstract Lcom/google/ads/interactivemedia/api/AdsManager;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;,
        Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;,
        Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;
    }
.end annotation


# instance fields
.field final a:Lcom/google/ads/interactivemedia/a/b;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/ads/interactivemedia/api/AdsRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/ads/interactivemedia/api/AdsRequest",
            "<*>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/a/a$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field private f:Lcom/google/ads/interactivemedia/a/a;


# direct methods
.method protected constructor <init>(Lcom/google/ads/interactivemedia/a/a;Ljava/lang/String;Lcom/google/ads/interactivemedia/api/AdsRequest;Ljava/util/Map;)V
    .locals 2
    .parameter "server"
    .parameter "adsManagerId"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/ads/interactivemedia/a/a;",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/api/AdsRequest",
            "<*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/a/a$a;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p3, adsRequest:Lcom/google/ads/interactivemedia/api/AdsRequest;,"Lcom/google/ads/interactivemedia/api/AdsRequest<*>;"
    .local p4, companions:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/google/ads/interactivemedia/a/a$a;>;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Lcom/google/ads/interactivemedia/a/b;

    invoke-direct {v0}, Lcom/google/ads/interactivemedia/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->a:Lcom/google/ads/interactivemedia/a/b;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->b:Ljava/util/List;

    .line 106
    iput-object p1, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->f:Lcom/google/ads/interactivemedia/a/a;

    .line 107
    iput-object p2, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->e:Ljava/lang/String;

    .line 108
    iput-object p3, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->c:Lcom/google/ads/interactivemedia/api/AdsRequest;

    .line 109
    iput-object p4, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->d:Ljava/util/Map;

    .line 110
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdsManager$1;

    invoke-direct {v0, p0}, Lcom/google/ads/interactivemedia/api/AdsManager$1;-><init>(Lcom/google/ads/interactivemedia/api/AdsManager;)V

    invoke-interface {p1, p2, v0}, Lcom/google/ads/interactivemedia/a/a;->a(Ljava/lang/String;Lcom/google/ads/interactivemedia/a/a$b;)V

    .line 117
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method protected final a(Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;

    .line 175
    invoke-interface {v0, p1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;->onAdEvent(Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;)V

    goto :goto_0

    .line 177
    :cond_0
    return-void
.end method

.method protected final a(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;)V
    .locals 1
    .parameter

    .prologue
    .line 170
    new-instance v0, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;

    invoke-direct {v0, p1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;-><init>(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;)V

    invoke-virtual {p0, v0}, Lcom/google/ads/interactivemedia/api/AdsManager;->a(Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;)V

    .line 171
    return-void
.end method

.method public addAdErrorListener(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V
    .locals 1
    .parameter "errorListener"

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/ads/interactivemedia/api/AdsManager;->d()V

    .line 121
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->a:Lcom/google/ads/interactivemedia/a/b;

    invoke-virtual {v0, p1}, Lcom/google/ads/interactivemedia/a/b;->a(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V

    .line 122
    return-void
.end method

.method public addAdEventListener(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;)V
    .locals 1
    .parameter "adEventListener"

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/ads/interactivemedia/api/AdsManager;->d()V

    .line 131
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    return-void
.end method

.method protected final b()Lcom/google/ads/interactivemedia/a/a;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->f:Lcom/google/ads/interactivemedia/a/a;

    return-object v0
.end method

.method protected final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->f:Lcom/google/ads/interactivemedia/a/a;

    if-nez v0, :cond_0

    .line 165
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "AdsManager is unloaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_0
    return-void
.end method

.method protected final e()V
    .locals 7

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->c:Lcom/google/ads/interactivemedia/api/AdsRequest;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/api/AdsRequest;->a()Ljava/util/Map;

    move-result-object v2

    .line 181
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 182
    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 183
    iget-object v4, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->f:Lcom/google/ads/interactivemedia/a/a;

    iget-object v5, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ads/interactivemedia/a/a$a;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-interface {v4, v5, v1, v0}, Lcom/google/ads/interactivemedia/a/a;->a(Ljava/lang/String;Lcom/google/ads/interactivemedia/a/a$a;Landroid/view/ViewGroup;)V

    goto :goto_0

    .line 189
    :cond_1
    return-void
.end method

.method public removeAdErrorListener(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V
    .locals 1
    .parameter "errorListener"

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/ads/interactivemedia/api/AdsManager;->d()V

    .line 126
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->a:Lcom/google/ads/interactivemedia/a/b;

    invoke-virtual {v0, p1}, Lcom/google/ads/interactivemedia/a/b;->b(Lcom/google/ads/interactivemedia/api/AdErrorListener;)V

    .line 127
    return-void
.end method

.method public removeAdEventListener(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;)V
    .locals 1
    .parameter "adEventListener"

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/ads/interactivemedia/api/AdsManager;->d()V

    .line 136
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 137
    return-void
.end method

.method public unload()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->f:Lcom/google/ads/interactivemedia/a/a;

    if-nez v0, :cond_0

    .line 150
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-virtual {p0}, Lcom/google/ads/interactivemedia/api/AdsManager;->a()V

    .line 146
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->f:Lcom/google/ads/interactivemedia/a/a;

    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/a/a;->a(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->a:Lcom/google/ads/interactivemedia/a/b;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/a/b;->a()V

    .line 148
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ads/interactivemedia/api/AdsManager;->f:Lcom/google/ads/interactivemedia/a/a;

    goto :goto_0
.end method
