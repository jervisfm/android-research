.class public Lcom/google/ads/interactivemedia/api/CompanionAdSlot;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/CompanionAdSlot;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/ads/interactivemedia/api/CompanionAdSlot;->b:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/google/ads/interactivemedia/api/CompanionAdSlot;->a:I

    return v0
.end method

.method public setContainer(Landroid/view/ViewGroup;)V
    .locals 0
    .parameter "container"

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/ads/interactivemedia/api/CompanionAdSlot;->c:Landroid/view/ViewGroup;

    .line 54
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .parameter "height"

    .prologue
    .line 37
    iput p1, p0, Lcom/google/ads/interactivemedia/api/CompanionAdSlot;->b:I

    .line 38
    return-void
.end method

.method public setSize(II)V
    .locals 0
    .parameter "width"
    .parameter "height"

    .prologue
    .line 41
    iput p1, p0, Lcom/google/ads/interactivemedia/api/CompanionAdSlot;->a:I

    .line 42
    iput p2, p0, Lcom/google/ads/interactivemedia/api/CompanionAdSlot;->b:I

    .line 43
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .parameter "width"

    .prologue
    .line 26
    iput p1, p0, Lcom/google/ads/interactivemedia/api/CompanionAdSlot;->a:I

    .line 27
    return-void
.end method
