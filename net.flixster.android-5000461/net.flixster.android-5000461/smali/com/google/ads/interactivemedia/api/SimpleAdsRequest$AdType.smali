.class public final enum Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;
.super Ljava/lang/Enum;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/interactivemedia/api/SimpleAdsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum VIDEO:Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

.field private static final synthetic a:[Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1}, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;->VIDEO:Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

    .line 19
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;->VIDEO:Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;->a:[Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;
    .locals 1
    .parameter "name"

    .prologue
    .line 19
    const-class v0, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

    return-object v0
.end method

.method public static values()[Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;->a:[Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

    invoke-virtual {v0}, [Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

    return-object v0
.end method
