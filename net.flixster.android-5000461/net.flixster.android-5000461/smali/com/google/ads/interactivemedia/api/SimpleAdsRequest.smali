.class public Lcom/google/ads/interactivemedia/api/SimpleAdsRequest;
.super Lcom/google/ads/interactivemedia/api/AdsRequest;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/ads/interactivemedia/api/AdsRequest",
        "<",
        "Lcom/google/ads/interactivemedia/api/CompanionAdSlot;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/ads/interactivemedia/api/AdsRequest;-><init>()V

    .line 27
    const-string v0, "companionSizes"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest;->setRequestParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    return-void
.end method


# virtual methods
.method public getAdTagUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string v0, "adTagUrl"

    invoke-virtual {p0, v0}, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest;->getRequestParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAdType()Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;
    .locals 2

    .prologue
    .line 42
    const-string v0, "adType"

    invoke-virtual {p0, v0}, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest;->getRequestParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;->valueOf(Ljava/lang/String;)Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;

    move-result-object v0

    return-object v0
.end method

.method public setAdTagUrl(Ljava/lang/String;)V
    .locals 1
    .parameter "adTagUrl"

    .prologue
    .line 38
    const-string v0, "adTagUrl"

    invoke-virtual {p0, v0, p1}, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest;->setRequestParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public setAdType(Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;)V
    .locals 3
    .parameter "adType"

    .prologue
    .line 50
    const-string v0, "adType"

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest$AdType;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest;->setRequestParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public setCompanions(Ljava/util/Collection;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/ads/interactivemedia/api/CompanionAdSlot;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, companionSlots:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/ads/interactivemedia/api/CompanionAdSlot;>;"
    invoke-super {p0, p1}, Lcom/google/ads/interactivemedia/api/AdsRequest;->setCompanions(Ljava/util/Collection;)V

    .line 56
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    const/4 v0, 0x1

    .line 58
    invoke-virtual {p0}, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 59
    if-nez v1, :cond_0

    .line 60
    const-string v1, ","

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    :cond_0
    const/4 v1, 0x0

    .line 63
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 65
    :cond_1
    const-string v0, "companionSizes"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/ads/interactivemedia/api/SimpleAdsRequest;->setRequestParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    return-void
.end method
