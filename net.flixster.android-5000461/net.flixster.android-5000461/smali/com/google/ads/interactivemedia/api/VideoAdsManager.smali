.class public Lcom/google/ads/interactivemedia/api/VideoAdsManager;
.super Lcom/google/ads/interactivemedia/api/AdsManager;
.source "IMASDK"


# instance fields
.field private final b:Lcom/google/ads/interactivemedia/a/a$a;

.field private c:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

.field private d:Lcom/google/ads/interactivemedia/a/c;


# direct methods
.method constructor <init>(Lcom/google/ads/interactivemedia/a/a;Ljava/lang/String;Lcom/google/ads/interactivemedia/api/AdsRequest;Lcom/google/ads/interactivemedia/a/a$a;Ljava/util/Map;)V
    .locals 0
    .parameter "server"
    .parameter "adManagerId"
    .parameter
    .parameter "master"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/ads/interactivemedia/a/a;",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/api/AdsRequest",
            "<*>;",
            "Lcom/google/ads/interactivemedia/a/a$a;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/a/a$a;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p3, adsRequest:Lcom/google/ads/interactivemedia/api/AdsRequest;,"Lcom/google/ads/interactivemedia/api/AdsRequest<*>;"
    .local p5, companions:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/google/ads/interactivemedia/a/a$a;>;>;"
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/google/ads/interactivemedia/api/AdsManager;-><init>(Lcom/google/ads/interactivemedia/a/a;Ljava/lang/String;Lcom/google/ads/interactivemedia/api/AdsRequest;Ljava/util/Map;)V

    .line 27
    iput-object p4, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->b:Lcom/google/ads/interactivemedia/a/a$a;

    .line 28
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->c:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

    if-nez v0, :cond_0

    .line 67
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->c:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->d:Lcom/google/ads/interactivemedia/a/c;

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;->removeCallback(Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V

    .line 65
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->c:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;->stopAd()V

    .line 66
    sget-object v0, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->CONTENT_RESUME_REQUESTED:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {p0, v0}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->a(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;)V

    goto :goto_0
.end method

.method public play(Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;)V
    .locals 4
    .parameter "player"

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->d()V

    .line 35
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->c:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

    if-eqz v0, :cond_0

    .line 36
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ad is already playing on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->c:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iput-object p1, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->c:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

    .line 40
    new-instance v0, Lcom/google/ads/interactivemedia/a/c;

    invoke-virtual {p0}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->b()Lcom/google/ads/interactivemedia/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->b:Lcom/google/ads/interactivemedia/a/a$a;

    invoke-virtual {v3}, Lcom/google/ads/interactivemedia/a/a$a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/a/c;-><init>(Lcom/google/ads/interactivemedia/a/a;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->d:Lcom/google/ads/interactivemedia/a/c;

    .line 42
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->d:Lcom/google/ads/interactivemedia/a/c;

    new-instance v1, Lcom/google/ads/interactivemedia/api/VideoAdsManager$1;

    invoke-direct {v1, p0}, Lcom/google/ads/interactivemedia/api/VideoAdsManager$1;-><init>(Lcom/google/ads/interactivemedia/api/VideoAdsManager;)V

    invoke-virtual {v0, v1}, Lcom/google/ads/interactivemedia/a/c;->a(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;)V

    .line 52
    sget-object v0, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->CONTENT_PAUSE_REQUESTED:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {p0, v0}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->a(Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;)V

    .line 53
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->c:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->d:Lcom/google/ads/interactivemedia/a/c;

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;->addCallback(Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V

    .line 54
    iget-object v0, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->c:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;

    iget-object v1, p0, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->b:Lcom/google/ads/interactivemedia/a/a$a;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/a/a$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;->playAd(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/ads/interactivemedia/api/VideoAdsManager;->e()V

    .line 56
    return-void
.end method
