.class public interface abstract Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VideoAdPlayerCallback"
.end annotation


# virtual methods
.method public abstract onClick()V
.end method

.method public abstract onComplete()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onPlay()V
.end method

.method public abstract onProgress(JJ)V
.end method

.method public abstract onResume()V
.end method

.method public abstract onVolumeChanged(I)V
.end method
