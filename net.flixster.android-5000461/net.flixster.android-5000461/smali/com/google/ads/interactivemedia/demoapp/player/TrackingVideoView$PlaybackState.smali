.class final enum Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;
.super Ljava/lang/Enum;
.source "TrackingVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "PlaybackState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

.field public static final enum PAUSED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

.field public static final enum PLAYING:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

.field public static final enum STOPPED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v2}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->STOPPED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    new-instance v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->PAUSED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    new-instance v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v4}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->PLAYING:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    sget-object v1, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->STOPPED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->PAUSED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->PLAYING:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->ENUM$VALUES:[Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    return-object v0
.end method

.method public static values()[Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->ENUM$VALUES:[Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    array-length v1, v0

    new-array v2, v1, [Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
