.class public Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;
.super Landroid/widget/VideoView;
.source "TrackingVideoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$google$ads$interactivemedia$demoapp$player$TrackingVideoView$PlaybackState:[I


# instance fields
.field private final callbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;",
            ">;"
        }
    .end annotation
.end field

.field private isTrackingEnabled:Z

.field private progressThread:Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;

.field private state:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;


# direct methods
.method static synthetic $SWITCH_TABLE$com$google$ads$interactivemedia$demoapp$player$TrackingVideoView$PlaybackState()[I
    .locals 3

    .prologue
    .line 21
    sget-object v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->$SWITCH_TABLE$com$google$ads$interactivemedia$demoapp$player$TrackingVideoView$PlaybackState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->values()[Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->PAUSED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->PLAYING:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->STOPPED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->$SWITCH_TABLE$com$google$ads$interactivemedia$demoapp$player$TrackingVideoView$PlaybackState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    .line 28
    sget-object v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->STOPPED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    iput-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->state:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    .line 28
    sget-object v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->STOPPED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    iput-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->state:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    .line 28
    sget-object v0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->STOPPED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    iput-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->state:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    .line 48
    return-void
.end method

.method private onStop()V
    .locals 3

    .prologue
    .line 120
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->state:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    sget-object v2, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->STOPPED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    if-ne v1, v2, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    sget-object v1, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->STOPPED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    iput-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->state:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    .line 125
    iget-boolean v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->isTrackingEnabled:Z

    if-eqz v1, :cond_0

    .line 126
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->progressThread:Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->quit()V

    .line 128
    :try_start_0
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->progressThread:Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->progressThread:Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;

    goto :goto_0

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, e:Ljava/lang/InterruptedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public addCallback(Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V
    .locals 1
    .parameter "callback"

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    return-void
.end method

.method public disableTracking()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->isTrackingEnabled:Z

    .line 58
    return-void
.end method

.method public enableTracking()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->isTrackingEnabled:Z

    .line 53
    return-void
.end method

.method public onClick()V
    .locals 3

    .prologue
    .line 65
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 68
    return-void

    .line 65
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 66
    .local v0, callback:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onClick()V

    goto :goto_0
.end method

.method public onCompletion()V
    .locals 3

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->onStop()V

    .line 142
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 145
    return-void

    .line 142
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 143
    .local v0, callback:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onComplete()V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 105
    invoke-super {p0}, Landroid/widget/VideoView;->pause()V

    .line 106
    sget-object v1, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->PAUSED:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    iput-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->state:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    .line 108
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 111
    return-void

    .line 108
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 109
    .local v0, callback:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onPause()V

    goto :goto_0
.end method

.method public removeCallback(Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V
    .locals 1
    .parameter "callback"

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 153
    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 157
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 158
    return-void
.end method

.method public start()V
    .locals 4

    .prologue
    .line 78
    invoke-super {p0}, Landroid/widget/VideoView;->start()V

    .line 79
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->state:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    .line 80
    .local v1, oldState:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;
    sget-object v2, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->PLAYING:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    iput-object v2, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->state:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;

    .line 82
    iget-boolean v2, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->isTrackingEnabled:Z

    if-eqz v2, :cond_0

    .line 83
    invoke-static {}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->$SWITCH_TABLE$com$google$ads$interactivemedia$demoapp$player$TrackingVideoView$PlaybackState()[I

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView$PlaybackState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 101
    :cond_0
    return-void

    .line 85
    :pswitch_0
    new-instance v2, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;

    iget-object v3, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    invoke-direct {v2, p0, v3}, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;-><init>(Landroid/widget/VideoView;Ljava/util/List;)V

    iput-object v2, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->progressThread:Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;

    .line 86
    iget-object v2, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->progressThread:Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;

    invoke-virtual {v2}, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->start()V

    .line 88
    iget-object v2, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 89
    .local v0, callback:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onPlay()V

    goto :goto_0

    .line 93
    .end local v0           #callback:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    :pswitch_1
    iget-object v2, p0, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->callbacks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 94
    .restart local v0       #callback:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onResume()V

    goto :goto_1

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public stopPlayback()V
    .locals 0

    .prologue
    .line 115
    invoke-super {p0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 116
    invoke-direct {p0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->onStop()V

    .line 117
    return-void
.end method
