.class public Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;
.super Ljava/lang/Thread;
.source "VideoViewProgressThread.java"


# static fields
.field private static final QUIT:I = 0x2

.field private static final UPDATE:I = 0x1


# instance fields
.field private final callbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;",
            ">;"
        }
    .end annotation
.end field

.field private threadHandler:Landroid/os/Handler;

.field private uiHandler:Landroid/os/Handler;

.field private final video:Landroid/widget/VideoView;


# direct methods
.method public constructor <init>(Landroid/widget/VideoView;Ljava/util/List;)V
    .locals 2
    .parameter "video"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/VideoView;",
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, callbacks:Ljava/util/List;,"Ljava/util/List<Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;>;"
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->video:Landroid/widget/VideoView;

    .line 41
    iput-object p2, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->callbacks:Ljava/util/List;

    .line 42
    const-string v0, "VideoViewProgressThread"

    invoke-virtual {p0, v0}, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->setName(Ljava/lang/String;)V

    .line 43
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread$1;

    invoke-direct {v1, p0}, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread$1;-><init>(Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->uiHandler:Landroid/os/Handler;

    .line 49
    return-void
.end method

.method private update()V
    .locals 6

    .prologue
    .line 91
    :try_start_0
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->video:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 100
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->uiHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->uiHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->video:Landroid/widget/VideoView;

    invoke-virtual {v4}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v4

    iget-object v5, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->video:Landroid/widget/VideoView;

    invoke-virtual {v5}, Landroid/widget/VideoView;->getDuration()I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    .line 98
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v1, "FlxAd"

    const-string v2, "VideoViewProgressThread"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method protected handleThreadMessage(Landroid/os/Message;)Z
    .locals 4
    .parameter "msg"

    .prologue
    const/4 v0, 0x1

    .line 75
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 85
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 77
    :pswitch_0
    invoke-direct {p0}, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->update()V

    .line 78
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->threadHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 81
    :pswitch_1
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->threadHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 82
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected handleUiMessage(Landroid/os/Message;)Z
    .locals 6
    .parameter "msg"

    .prologue
    .line 104
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 111
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 106
    :pswitch_0
    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->callbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 109
    const/4 v1, 0x1

    goto :goto_0

    .line 106
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 107
    .local v0, callback:Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    iget v2, p1, Landroid/os/Message;->arg1:I

    int-to-long v2, v2

    iget v4, p1, Landroid/os/Message;->arg2:I

    int-to-long v4, v4

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onProgress(JJ)V

    goto :goto_1

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public quit()V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->threadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->threadHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 54
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 59
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 60
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread$2;

    invoke-direct {v1, p0}, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread$2;-><init>(Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->threadHandler:Landroid/os/Handler;

    .line 66
    iget-object v0, p0, Lcom/google/ads/interactivemedia/demoapp/player/VideoViewProgressThread;->threadHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 67
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 68
    return-void
.end method
