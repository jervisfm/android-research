.class final enum Lcom/google/tv/leftnavbar/HomeDisplay$Mode;
.super Ljava/lang/Enum;
.source "HomeDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/tv/leftnavbar/HomeDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/tv/leftnavbar/HomeDisplay$Mode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BOTH:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

.field private static final synthetic ENUM$VALUES:[Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

.field public static final enum ICON:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

.field public static final enum LOGO:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-instance v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    const-string v1, "ICON"

    invoke-direct {v0, v1, v2}, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->ICON:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    new-instance v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    const-string v1, "LOGO"

    invoke-direct {v0, v1, v3}, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->LOGO:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    new-instance v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v4}, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->BOTH:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    sget-object v1, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->ICON:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->LOGO:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->BOTH:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->ENUM$VALUES:[Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/tv/leftnavbar/HomeDisplay$Mode;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    return-object v0
.end method

.method public static values()[Lcom/google/tv/leftnavbar/HomeDisplay$Mode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->ENUM$VALUES:[Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    array-length v1, v0

    new-array v2, v1, [Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
