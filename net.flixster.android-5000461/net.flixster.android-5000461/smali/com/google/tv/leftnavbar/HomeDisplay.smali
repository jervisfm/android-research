.class final Lcom/google/tv/leftnavbar/HomeDisplay;
.super Ljava/lang/Object;
.source "HomeDisplay.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/tv/leftnavbar/HomeDisplay$Mode;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LeftNavBar-Home"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mExpanded:Z

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mLogo:Landroid/graphics/drawable/Drawable;

.field private mMode:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

.field private mView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V
    .locals 3
    .parameter "context"
    .parameter "parent"
    .parameter "attributes"

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mContext:Landroid/content/Context;

    .line 54
    sget-object v2, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->ICON:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    iput-object v2, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mMode:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 56
    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 57
    .local v1, pm:Landroid/content/pm/PackageManager;
    invoke-direct {p0, p3, v1, v0}, Lcom/google/tv/leftnavbar/HomeDisplay;->loadLogo(Landroid/content/res/TypedArray;Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V

    .line 58
    invoke-direct {p0, p3, v1, v0}, Lcom/google/tv/leftnavbar/HomeDisplay;->loadIcon(Landroid/content/res/TypedArray;Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V

    .line 59
    invoke-direct {p0, p2, p3}, Lcom/google/tv/leftnavbar/HomeDisplay;->createView(Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V

    .line 60
    return-void
.end method

.method private createView(Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V
    .locals 3
    .parameter "parent"
    .parameter "attributes"

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030039

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mView:Landroid/view/View;

    .line 94
    return-void
.end method

.method private loadIcon(Landroid/content/res/TypedArray;Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V
    .locals 3
    .parameter "a"
    .parameter "pm"
    .parameter "appInfo"

    .prologue
    .line 76
    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mContext:Landroid/content/Context;

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mIcon:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mIcon:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 84
    invoke-virtual {p3, p2}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 86
    :cond_1
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "LeftNavBar-Home"

    const-string v2, "Failed to load app icon."

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private loadLogo(Landroid/content/res/TypedArray;Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V
    .locals 3
    .parameter "a"
    .parameter "pm"
    .parameter "appInfo"

    .prologue
    .line 63
    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mContext:Landroid/content/Context;

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 65
    :try_start_0
    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/content/pm/PackageManager;->getActivityLogo(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mLogo:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mLogo:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 71
    invoke-virtual {p3, p2}, Landroid/content/pm/ApplicationInfo;->loadLogo(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mLogo:Landroid/graphics/drawable/Drawable;

    .line 73
    :cond_1
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "LeftNavBar-Home"

    const-string v2, "Failed to load app logo."

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateImage()V
    .locals 3

    .prologue
    .line 97
    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mMode:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    sget-object v2, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->ICON:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mLogo:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mMode:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    sget-object v2, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->BOTH:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mExpanded:Z

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 98
    .local v0, useIcon:Z
    :goto_0
    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mView:Landroid/view/View;

    const v2, 0x7f0700d7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mIcon:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    return-void

    .line 97
    .end local v0           #useIcon:Z
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 98
    .restart local v0       #useIcon:Z
    :cond_2
    iget-object v2, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mLogo:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method


# virtual methods
.method getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mView:Landroid/view/View;

    return-object v0
.end method

.method isVisible()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setAsUp(Z)Lcom/google/tv/leftnavbar/HomeDisplay;
    .locals 2
    .parameter "asUp"

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mView:Landroid/view/View;

    const v1, 0x7f0700d8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 128
    return-object p0

    .line 127
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method setExpanded(Z)Lcom/google/tv/leftnavbar/HomeDisplay;
    .locals 0
    .parameter "expanded"

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mExpanded:Z

    .line 116
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/HomeDisplay;->updateImage()V

    .line 117
    return-object p0
.end method

.method setImageMode(Lcom/google/tv/leftnavbar/HomeDisplay$Mode;)Lcom/google/tv/leftnavbar/HomeDisplay;
    .locals 0
    .parameter "mode"

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mMode:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    .line 122
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/HomeDisplay;->updateImage()V

    .line 123
    return-object p0
.end method

.method public setOnClickHomeListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    return-void
.end method

.method setVisible(Z)Lcom/google/tv/leftnavbar/HomeDisplay;
    .locals 2
    .parameter "visible"

    .prologue
    .line 106
    iget-object v1, p0, Lcom/google/tv/leftnavbar/HomeDisplay;->mView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 107
    return-object p0

    .line 106
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
