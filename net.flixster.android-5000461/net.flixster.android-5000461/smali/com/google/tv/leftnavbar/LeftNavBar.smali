.class public final Lcom/google/tv/leftnavbar/LeftNavBar;
.super Landroid/app/ActionBar;
.source "LeftNavBar.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final DEFAULT_DISPLAY_OPTIONS:I = 0x2b

.field public static final DISPLAY_ALWAYS_EXPANDED:I = 0x20

.field public static final DISPLAY_AUTO_EXPAND:I = 0x40

.field public static final DISPLAY_SHOW_INDETERMINATE_PROGRESS:I = 0x100

.field public static final DISPLAY_USE_LOGO_WHEN_EXPANDED:I = 0x80


# instance fields
.field private mContent:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mIsOverlay:Z

.field private mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

.field private mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .parameter "activity"

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/app/ActionBar;-><init>()V

    .line 74
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->initialize(Landroid/view/Window;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/app/Dialog;)V
    .locals 1
    .parameter "dialog"

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/app/ActionBar;-><init>()V

    .line 78
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->initialize(Landroid/view/Window;)V

    .line 79
    return-void
.end method

.method private convertTab(Landroid/app/ActionBar$Tab;)Lcom/google/tv/leftnavbar/TabImpl;
    .locals 2
    .parameter "tab"

    .prologue
    .line 203
    if-nez p1, :cond_0

    .line 204
    const/4 p1, 0x0

    .line 209
    .end local p1
    :goto_0
    return-object p1

    .line 206
    .restart local p1
    :cond_0
    instance-of v0, p1, Lcom/google/tv/leftnavbar/TabImpl;

    if-nez v0, :cond_1

    .line 207
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid tab object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_1
    check-cast p1, Lcom/google/tv/leftnavbar/TabImpl;

    goto :goto_0
.end method

.method private static has(II)Z
    .locals 1
    .parameter "changes"
    .parameter "option"

    .prologue
    .line 339
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initialize(Landroid/view/Window;)V
    .locals 4
    .parameter "window"

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 83
    .local v0, decor:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mContext:Landroid/content/Context;

    .line 84
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mIsOverlay:Z

    .line 85
    const v1, 0x7f0700b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/tv/leftnavbar/TitleBarView;

    iput-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    .line 86
    const v1, 0x7f0700b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/tv/leftnavbar/LeftNavView;

    iput-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    .line 87
    const v1, 0x7f0700b0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mContent:Landroid/view/View;

    .line 89
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    if-nez v1, :cond_1

    .line 90
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ": incompatible window decor!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 93
    :cond_1
    const/16 v1, 0x2b

    invoke-virtual {p0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setDisplayOptions(I)V

    .line 94
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->showOptionsMenu(Z)V

    .line 95
    return-void
.end method

.method private setLeftMargin(Landroid/view/View;I)V
    .locals 1
    .parameter "view"
    .parameter "margin"

    .prologue
    .line 116
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 117
    .local v0, params:Landroid/view/ViewGroup$MarginLayoutParams;
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 118
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 119
    return-void
.end method

.method private setTopMargin(Landroid/view/View;I)V
    .locals 1
    .parameter "view"
    .parameter "margin"

    .prologue
    .line 122
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 123
    .local v0, params:Landroid/view/ViewGroup$MarginLayoutParams;
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 124
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    return-void
.end method

.method private setVisible(Z)V
    .locals 2
    .parameter "visible"

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mIsOverlay:Z

    .line 142
    .local v0, shouldAnimate:Z
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v1, p1, v0}, Lcom/google/tv/leftnavbar/LeftNavView;->setVisible(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    invoke-direct {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->updateWindowLayout(Z)V

    .line 145
    :cond_0
    return-void
.end method

.method private updateTitleBar(Z)V
    .locals 6
    .parameter "animated"

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getDisplayOptions()I

    move-result v1

    .line 108
    .local v1, options:I
    const/16 v4, 0x8

    invoke-static {v1, v4}, Lcom/google/tv/leftnavbar/LeftNavBar;->has(II)Z

    move-result v3

    .line 109
    .local v3, titleVisible:Z
    const/16 v4, 0x100

    invoke-static {v1, v4}, Lcom/google/tv/leftnavbar/LeftNavBar;->has(II)Z

    move-result v2

    .line 110
    .local v2, progressVisible:Z
    iget-object v4, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    invoke-virtual {v4}, Lcom/google/tv/leftnavbar/TitleBarView;->isHorizontalProgressVisible()Z

    move-result v0

    .line 111
    .local v0, horizontalProgressVisible:Z
    iget-object v5, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v3, :cond_0

    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_0
    invoke-virtual {v5, v4, p1}, Lcom/google/tv/leftnavbar/TitleBarView;->setVisible(ZZ)V

    .line 112
    iget-object v4, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    invoke-virtual {v4, v2}, Lcom/google/tv/leftnavbar/TitleBarView;->setProgressVisible(Z)V

    .line 113
    return-void

    .line 111
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private updateWindowLayout(Z)V
    .locals 3
    .parameter "animated"

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/LeftNavBar;->updateTitleBar(Z)V

    .line 99
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->getApparentWidth(Z)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setLeftMargin(Landroid/view/View;I)V

    .line 100
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mIsOverlay:Z

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mContent:Landroid/view/View;

    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->getApparentWidth(Z)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setLeftMargin(Landroid/view/View;I)V

    .line 102
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mContent:Landroid/view/View;

    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/TitleBarView;->getApparentHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setTopMargin(Landroid/view/View;I)V

    .line 104
    :cond_0
    return-void
.end method


# virtual methods
.method public addOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 421
    return-void
.end method

.method public addTab(Landroid/app/ActionBar$Tab;)V
    .locals 1
    .parameter "tab"

    .prologue
    .line 214
    const/4 v0, -0x2

    invoke-virtual {p0, p1, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->addTab(Landroid/app/ActionBar$Tab;I)V

    .line 215
    return-void
.end method

.method public addTab(Landroid/app/ActionBar$Tab;I)V
    .locals 1
    .parameter "tab"
    .parameter "position"

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getTabCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->addTab(Landroid/app/ActionBar$Tab;IZ)V

    .line 225
    return-void

    .line 224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addTab(Landroid/app/ActionBar$Tab;IZ)V
    .locals 2
    .parameter "tab"
    .parameter "position"
    .parameter "setSelected"

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getTabs()Lcom/google/tv/leftnavbar/TabDisplay;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/LeftNavBar;->convertTab(Landroid/app/ActionBar$Tab;)Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/tv/leftnavbar/TabDisplay;->add(Lcom/google/tv/leftnavbar/TabImpl;IZ)V

    .line 230
    return-void
.end method

.method public addTab(Landroid/app/ActionBar$Tab;Z)V
    .locals 1
    .parameter "tab"
    .parameter "setSelected"

    .prologue
    .line 219
    const/4 v0, -0x2

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/tv/leftnavbar/LeftNavBar;->addTab(Landroid/app/ActionBar$Tab;IZ)V

    .line 220
    return-void
.end method

.method public dispatchMenuVisibilityChanged(Z)V
    .locals 0
    .parameter "visible"

    .prologue
    .line 451
    return-void
.end method

.method public getCustomView()Landroid/view/View;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getCustomView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayOptions()I
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getDisplayOptions()I

    move-result v0

    return v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->getApparentWidth(Z)I

    move-result v0

    return v0
.end method

.method public getNavigationItemCount()I
    .locals 3

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getNavigationMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 280
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No count available for mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getNavigationMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getTabCount()I

    move-result v0

    .line 277
    :goto_0
    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getSpinner()Lcom/google/tv/leftnavbar/SpinnerDisplay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->getCount()I

    move-result v0

    goto :goto_0

    .line 272
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getNavigationMode()I
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getNavigationMode()I

    move-result v0

    return v0
.end method

.method public getSelectedNavigationIndex()I
    .locals 4

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getNavigationMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 300
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No selection available for mode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getNavigationMode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 293
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    .line 294
    .local v0, selected:Landroid/app/ActionBar$Tab;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v1

    .line 297
    .end local v0           #selected:Landroid/app/ActionBar$Tab;
    :goto_0
    return v1

    .line 294
    .restart local v0       #selected:Landroid/app/ActionBar$Tab;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0

    .line 297
    .end local v0           #selected:Landroid/app/ActionBar$Tab;
    :pswitch_1
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/LeftNavView;->getSpinner()Lcom/google/tv/leftnavbar/SpinnerDisplay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->getSelected()I

    move-result v1

    goto :goto_0

    .line 291
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getSelectedTab()Landroid/app/ActionBar$Tab;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getTabs()Lcom/google/tv/leftnavbar/TabDisplay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/TabDisplay;->getSelected()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/TitleBarView;->getSubtitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTabAt(I)Landroid/app/ActionBar$Tab;
    .locals 1
    .parameter "index"

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getTabs()Lcom/google/tv/leftnavbar/TabDisplay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TabDisplay;->get(I)Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    return-object v0
.end method

.method public getTabCount()I
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getTabs()Lcom/google/tv/leftnavbar/TabDisplay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/TabDisplay;->getCount()I

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/TitleBarView;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->setVisible(Z)V

    .line 138
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->isVisible()Z

    move-result v0

    return v0
.end method

.method public newTab()Landroid/app/ActionBar$Tab;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Lcom/google/tv/leftnavbar/LeftNavBar$1;

    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar$1;-><init>(Lcom/google/tv/leftnavbar/LeftNavBar;Landroid/content/Context;)V

    return-object v0
.end method

.method public removeAllTabs()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getTabs()Lcom/google/tv/leftnavbar/TabDisplay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/TabDisplay;->removeAll()V

    .line 250
    return-void
.end method

.method public removeOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 426
    return-void
.end method

.method public removeTab(Landroid/app/ActionBar$Tab;)V
    .locals 2
    .parameter "tab"

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getTabs()Lcom/google/tv/leftnavbar/TabDisplay;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/LeftNavBar;->convertTab(Landroid/app/ActionBar$Tab;)Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/tv/leftnavbar/TabDisplay;->remove(Lcom/google/tv/leftnavbar/TabImpl;)V

    .line 255
    return-void
.end method

.method public removeTabAt(I)V
    .locals 1
    .parameter "position"

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getTabs()Lcom/google/tv/leftnavbar/TabDisplay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TabDisplay;->remove(I)V

    .line 260
    return-void
.end method

.method public selectTab(Landroid/app/ActionBar$Tab;)V
    .locals 2
    .parameter "tab"

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getTabs()Lcom/google/tv/leftnavbar/TabDisplay;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/LeftNavBar;->convertTab(Landroid/app/ActionBar$Tab;)Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/tv/leftnavbar/TabDisplay;->select(Lcom/google/tv/leftnavbar/TabImpl;)V

    .line 265
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .parameter "d"

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/LeftNavView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 432
    return-void
.end method

.method public setCustomView(I)V
    .locals 3
    .parameter "resId"

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->setCustomView(Landroid/view/View;)V

    .line 413
    return-void
.end method

.method public setCustomView(Landroid/view/View;)V
    .locals 1
    .parameter "view"

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/LeftNavView;->setCustomView(Landroid/view/View;)V

    .line 402
    return-void
.end method

.method public setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V
    .locals 0
    .parameter "view"
    .parameter "layoutParams"

    .prologue
    .line 406
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 407
    invoke-virtual {p0, p1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setCustomView(Landroid/view/View;)V

    .line 408
    return-void
.end method

.method public setDisplayHomeAsUpEnabled(Z)V
    .locals 2
    .parameter "showHomeAsUp"

    .prologue
    const/4 v1, 0x4

    .line 360
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setDisplayOptions(II)V

    .line 361
    return-void

    .line 360
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDisplayOptions(I)V
    .locals 2
    .parameter "options"

    .prologue
    .line 344
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v1, p1}, Lcom/google/tv/leftnavbar/LeftNavView;->setDisplayOptions(I)I

    move-result v0

    .line 345
    .local v0, changes:I
    const/16 v1, 0x20

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->has(II)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->has(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 346
    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->has(II)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x100

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->has(II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 347
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->updateWindowLayout(Z)V

    .line 349
    :cond_1
    return-void
.end method

.method public setDisplayOptions(II)V
    .locals 4
    .parameter "options"
    .parameter "mask"

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getDisplayOptions()I

    move-result v0

    .line 354
    .local v0, current:I
    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v3, v0

    or-int v1, v2, v3

    .line 355
    .local v1, updated:I
    invoke-virtual {p0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setDisplayOptions(I)V

    .line 356
    return-void
.end method

.method public setDisplayShowCustomEnabled(Z)V
    .locals 2
    .parameter "showCustom"

    .prologue
    const/16 v1, 0x10

    .line 365
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setDisplayOptions(II)V

    .line 366
    return-void

    .line 365
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDisplayShowHomeEnabled(Z)V
    .locals 2
    .parameter "showHome"

    .prologue
    const/4 v1, 0x2

    .line 370
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setDisplayOptions(II)V

    .line 371
    return-void

    .line 370
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDisplayShowTitleEnabled(Z)V
    .locals 2
    .parameter "showTitle"

    .prologue
    const/16 v1, 0x8

    .line 375
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setDisplayOptions(II)V

    .line 376
    return-void

    .line 375
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDisplayUseLogoEnabled(Z)V
    .locals 2
    .parameter "useLogo"

    .prologue
    const/4 v1, 0x1

    .line 380
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/google/tv/leftnavbar/LeftNavBar;->setDisplayOptions(II)V

    .line 381
    return-void

    .line 380
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIcon(I)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 470
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 476
    return-void
.end method

.method public setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V
    .locals 1
    .parameter "adapter"
    .parameter "callback"

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getSpinner()Lcom/google/tv/leftnavbar/SpinnerDisplay;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->setContent(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 307
    return-void
.end method

.method public setLogo(I)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 482
    return-void
.end method

.method public setLogo(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 488
    return-void
.end method

.method public setNavigationMode(I)V
    .locals 1
    .parameter "mode"

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/LeftNavView;->setNavigationMode(I)V

    .line 312
    return-void
.end method

.method public setOnClickHomeListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/LeftNavView;->setOnClickHomeListener(Landroid/view/View$OnClickListener;)V

    .line 464
    return-void
.end method

.method public setSelectedNavigationItem(I)V
    .locals 3
    .parameter "position"

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getNavigationMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 326
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot set selection on mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavBar;->getNavigationMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/tv/leftnavbar/LeftNavBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    .line 328
    :goto_0
    return-void

    .line 322
    :pswitch_1
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView;->getSpinner()Lcom/google/tv/leftnavbar/SpinnerDisplay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->setSelected(I)V

    goto :goto_0

    .line 316
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setShowHideAnimationEnabled(Z)V
    .locals 1
    .parameter "enabled"

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/LeftNavView;->setAnimationsEnabled(Z)V

    .line 447
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TitleBarView;->setAnimationsEnabled(Z)V

    .line 448
    return-void
.end method

.method public setShowHorizontalProgress(I)V
    .locals 1
    .parameter "value"

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TitleBarView;->setHorizontalProgress(I)V

    .line 388
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->updateWindowLayout(Z)V

    .line 389
    return-void
.end method

.method public setSubtitle(I)V
    .locals 1
    .parameter "resId"

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 178
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1
    .parameter "subtitle"

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TitleBarView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 173
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .parameter "resId"

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 163
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .parameter "title"

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mTitleBar:Lcom/google/tv/leftnavbar/TitleBarView;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TitleBarView;->setTitle(Ljava/lang/CharSequence;)V

    .line 158
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavBar;->setVisible(Z)V

    .line 133
    return-void
.end method

.method public showOptionsMenu(Z)V
    .locals 2
    .parameter "show"

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavBar;->mLeftNav:Lcom/google/tv/leftnavbar/LeftNavView;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->showOptionsMenu(Ljava/lang/Boolean;)V

    .line 460
    return-void
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1
    .parameter "callback"

    .prologue
    .line 454
    const/4 v0, 0x0

    return-object v0
.end method
