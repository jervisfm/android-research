.class Lcom/google/tv/leftnavbar/LeftNavView$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "LeftNavView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/tv/leftnavbar/LeftNavView;->setExpanded(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/tv/leftnavbar/LeftNavView;

.field private final synthetic val$expanded:Z


# direct methods
.method constructor <init>(Lcom/google/tv/leftnavbar/LeftNavView;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/tv/leftnavbar/LeftNavView$2;->this$0:Lcom/google/tv/leftnavbar/LeftNavView;

    iput-boolean p2, p0, Lcom/google/tv/leftnavbar/LeftNavView$2;->val$expanded:Z

    .line 245
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .parameter "animator"

    .prologue
    .line 256
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/LeftNavView$2;->val$expanded:Z

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView$2;->this$0:Lcom/google/tv/leftnavbar/LeftNavView;

    const/4 v1, 0x1

    #calls: Lcom/google/tv/leftnavbar/LeftNavView;->setContentExpanded(Z)V
    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->access$6(Lcom/google/tv/leftnavbar/LeftNavView;Z)V

    .line 259
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .parameter "animator"

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/LeftNavView$2;->val$expanded:Z

    if-nez v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView$2;->this$0:Lcom/google/tv/leftnavbar/LeftNavView;

    const/4 v1, 0x0

    #calls: Lcom/google/tv/leftnavbar/LeftNavView;->setContentExpanded(Z)V
    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->access$6(Lcom/google/tv/leftnavbar/LeftNavView;Z)V

    .line 252
    :cond_0
    return-void
.end method
