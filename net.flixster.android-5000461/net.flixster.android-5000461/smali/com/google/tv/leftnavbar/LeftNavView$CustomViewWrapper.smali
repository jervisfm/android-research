.class final Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;
.super Landroid/view/ViewGroup;
.source "LeftNavView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/tv/leftnavbar/LeftNavView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CustomViewWrapper"
.end annotation


# instance fields
.field private final mView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .parameter "context"
    .parameter "view"

    .prologue
    const/4 v1, -0x1

    .line 413
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 414
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 415
    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 414
    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 416
    iput-object p2, p0, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->mView:Landroid/view/View;

    .line 417
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/app/ActionBar$LayoutParams;

    if-nez v0, :cond_0

    .line 418
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 420
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->addView(Landroid/view/View;)V

    .line 421
    return-void
.end method

.method private checkDimensionsConsistency(II)V
    .locals 2
    .parameter "value"
    .parameter "expected"

    .prologue
    .line 470
    if-eq p1, p2, :cond_0

    .line 471
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Inconsistent dimensions!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 473
    :cond_0
    return-void
.end method

.method private findBottomOfAvailableSpace(Lcom/google/tv/leftnavbar/LeftNavView;)I
    .locals 3
    .parameter "parent"

    .prologue
    .line 462
    invoke-virtual {p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getPaddingBottom()I

    move-result v2

    sub-int v0, v1, v2

    .line 463
    .local v0, bottom:I
    #getter for: Lcom/google/tv/leftnavbar/LeftNavView;->mOptions:Lcom/google/tv/leftnavbar/OptionsDisplay;
    invoke-static {p1}, Lcom/google/tv/leftnavbar/LeftNavView;->access$4(Lcom/google/tv/leftnavbar/LeftNavView;)Lcom/google/tv/leftnavbar/OptionsDisplay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/OptionsDisplay;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 464
    #getter for: Lcom/google/tv/leftnavbar/LeftNavView;->mOptions:Lcom/google/tv/leftnavbar/OptionsDisplay;
    invoke-static {p1}, Lcom/google/tv/leftnavbar/LeftNavView;->access$4(Lcom/google/tv/leftnavbar/LeftNavView;)Lcom/google/tv/leftnavbar/OptionsDisplay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/OptionsDisplay;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 466
    :cond_0
    return v0
.end method

.method private findTopOfAvailableSpace(Lcom/google/tv/leftnavbar/LeftNavView;)I
    .locals 2
    .parameter "parent"

    .prologue
    .line 437
    invoke-virtual {p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getPaddingTop()I

    move-result v0

    .line 438
    .local v0, top:I
    #getter for: Lcom/google/tv/leftnavbar/LeftNavView;->mHome:Lcom/google/tv/leftnavbar/HomeDisplay;
    invoke-static {p1}, Lcom/google/tv/leftnavbar/LeftNavView;->access$0(Lcom/google/tv/leftnavbar/LeftNavView;)Lcom/google/tv/leftnavbar/HomeDisplay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/HomeDisplay;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 439
    #getter for: Lcom/google/tv/leftnavbar/LeftNavView;->mHome:Lcom/google/tv/leftnavbar/HomeDisplay;
    invoke-static {p1}, Lcom/google/tv/leftnavbar/LeftNavView;->access$0(Lcom/google/tv/leftnavbar/LeftNavView;)Lcom/google/tv/leftnavbar/HomeDisplay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/HomeDisplay;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 441
    :cond_0
    #getter for: Lcom/google/tv/leftnavbar/LeftNavView;->mNavigationMode:I
    invoke-static {p1}, Lcom/google/tv/leftnavbar/LeftNavView;->access$1(Lcom/google/tv/leftnavbar/LeftNavView;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 453
    :goto_0
    return v0

    .line 443
    :pswitch_0
    #getter for: Lcom/google/tv/leftnavbar/LeftNavView;->mTabs:Lcom/google/tv/leftnavbar/TabDisplay;
    invoke-static {p1}, Lcom/google/tv/leftnavbar/LeftNavView;->access$2(Lcom/google/tv/leftnavbar/LeftNavView;)Lcom/google/tv/leftnavbar/TabDisplay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/TabDisplay;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 444
    goto :goto_0

    .line 447
    :pswitch_1
    #getter for: Lcom/google/tv/leftnavbar/LeftNavView;->mSpinner:Lcom/google/tv/leftnavbar/SpinnerDisplay;
    invoke-static {p1}, Lcom/google/tv/leftnavbar/LeftNavView;->access$3(Lcom/google/tv/leftnavbar/LeftNavView;)Lcom/google/tv/leftnavbar/SpinnerDisplay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 448
    goto :goto_0

    .line 441
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method detach()V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->mView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->removeView(Landroid/view/View;)V

    .line 429
    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 604
    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->mView:Landroid/view/View;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 530
    return-void
.end method

.method onPostLayout(Lcom/google/tv/leftnavbar/LeftNavView;)V
    .locals 15
    .parameter "parent"

    .prologue
    .line 536
    iget-object v12, p0, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->mView:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 537
    .local v9, width:I
    iget-object v12, p0, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->mView:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 538
    .local v3, height:I
    iget-object v12, p0, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->mView:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/app/ActionBar$LayoutParams;

    .line 541
    .local v4, params:Landroid/app/ActionBar$LayoutParams;
    const/4 v10, 0x0

    .line 544
    .local v10, xPosition:I
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getRight()I

    move-result v12

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getLeft()I

    move-result v13

    sub-int v2, v12, v13

    .line 545
    .local v2, containerWidth:I
    iget v12, v4, Landroid/app/ActionBar$LayoutParams;->gravity:I

    and-int/lit8 v12, v12, 0x7

    packed-switch v12, :pswitch_data_0

    .line 557
    :goto_0
    :pswitch_0
    iget v12, v4, Landroid/app/ActionBar$LayoutParams;->gravity:I

    and-int/lit8 v8, v12, 0x70

    .line 563
    .local v8, vGravity:I
    invoke-virtual/range {p1 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getBottom()I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getTop()I

    move-result v13

    sub-int/2addr v12, v13

    invoke-virtual/range {p1 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getPaddingTop()I

    move-result v13

    sub-int/2addr v12, v13

    .line 564
    invoke-virtual/range {p1 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getPaddingBottom()I

    move-result v13

    .line 563
    sub-int v6, v12, v13

    .line 565
    .local v6, superContainerHeight:I
    sub-int v12, v6, v3

    div-int/lit8 v12, v12, 0x2

    invoke-virtual/range {p1 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getPaddingTop()I

    move-result v13

    add-int v5, v12, v13

    .line 567
    .local v5, superCenteredTop:I
    invoke-direct/range {p0 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->findTopOfAvailableSpace(Lcom/google/tv/leftnavbar/LeftNavView;)I

    move-result v7

    .line 568
    .local v7, top:I
    invoke-direct/range {p0 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->findBottomOfAvailableSpace(Lcom/google/tv/leftnavbar/LeftNavView;)I

    move-result v0

    .line 571
    .local v0, bottom:I
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getBottom()I

    move-result v12

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getTop()I

    move-result v13

    sub-int/2addr v12, v13

    if-eqz v12, :cond_0

    .line 572
    sub-int v12, v0, v7

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getBottom()I

    move-result v13

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getTop()I

    move-result v14

    sub-int/2addr v13, v14

    invoke-direct {p0, v12, v13}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->checkDimensionsConsistency(II)V

    .line 576
    :cond_0
    const/16 v12, 0x10

    if-ne v8, v12, :cond_1

    .line 577
    if-ge v5, v7, :cond_2

    .line 578
    const/16 v8, 0x30

    .line 585
    :cond_1
    :goto_1
    const/4 v11, 0x0

    .line 586
    .local v11, yPosition:I
    sub-int v1, v0, v7

    .line 587
    .local v1, containerHeight:I
    sparse-switch v8, :sswitch_data_0

    .line 599
    :goto_2
    iget-object v12, p0, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->mView:Landroid/view/View;

    add-int v13, v10, v9

    add-int v14, v11, v3

    invoke-virtual {v12, v10, v11, v13, v14}, Landroid/view/View;->layout(IIII)V

    .line 600
    return-void

    .line 547
    .end local v0           #bottom:I
    .end local v1           #containerHeight:I
    .end local v5           #superCenteredTop:I
    .end local v6           #superContainerHeight:I
    .end local v7           #top:I
    .end local v8           #vGravity:I
    .end local v11           #yPosition:I
    :pswitch_1
    sub-int v12, v2, v9

    div-int/lit8 v10, v12, 0x2

    .line 548
    goto :goto_0

    .line 550
    :pswitch_2
    iget v10, v4, Landroid/app/ActionBar$LayoutParams;->leftMargin:I

    .line 551
    goto :goto_0

    .line 553
    :pswitch_3
    sub-int v12, v2, v9

    iget v13, v4, Landroid/app/ActionBar$LayoutParams;->rightMargin:I

    sub-int v10, v12, v13

    goto :goto_0

    .line 579
    .restart local v0       #bottom:I
    .restart local v5       #superCenteredTop:I
    .restart local v6       #superContainerHeight:I
    .restart local v7       #top:I
    .restart local v8       #vGravity:I
    :cond_2
    add-int v12, v5, v3

    if-le v12, v0, :cond_1

    .line 580
    const/16 v8, 0x50

    goto :goto_1

    .line 589
    .restart local v1       #containerHeight:I
    .restart local v11       #yPosition:I
    :sswitch_0
    sub-int v11, v5, v7

    .line 590
    goto :goto_2

    .line 592
    :sswitch_1
    iget v11, v4, Landroid/app/ActionBar$LayoutParams;->topMargin:I

    .line 593
    goto :goto_2

    .line 595
    :sswitch_2
    sub-int v12, v1, v3

    iget v13, v4, Landroid/app/ActionBar$LayoutParams;->bottomMargin:I

    sub-int v11, v12, v13

    goto :goto_2

    .line 545
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 587
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x30 -> :sswitch_1
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method onPostMeasure(Lcom/google/tv/leftnavbar/LeftNavView;)V
    .locals 21
    .parameter "parent"

    .prologue
    .line 481
    invoke-virtual/range {p1 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getMeasuredWidth()I

    move-result v13

    .line 482
    .local v13, totalWidth:I
    invoke-virtual/range {p1 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getMeasuredHeight()I

    move-result v12

    .line 484
    .local v12, totalHeight:I
    invoke-direct/range {p0 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->findTopOfAvailableSpace(Lcom/google/tv/leftnavbar/LeftNavView;)I

    move-result v11

    .line 485
    .local v11, topOfAvailableSpace:I
    invoke-direct/range {p0 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->findBottomOfAvailableSpace(Lcom/google/tv/leftnavbar/LeftNavView;)I

    move-result v6

    .line 487
    .local v6, bottomOfAvailableSpace:I
    invoke-virtual/range {p1 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getPaddingLeft()I

    move-result v18

    sub-int v18, v13, v18

    invoke-virtual/range {p1 .. p1}, Lcom/google/tv/leftnavbar/LeftNavView;->getPaddingRight()I

    move-result v19

    sub-int v5, v18, v19

    .line 488
    .local v5, availableWidth:I
    sub-int v2, v6, v11

    .line 493
    .local v2, availableHeight:I
    div-int/lit8 v18, v12, 0x2

    sub-int v4, v18, v11

    .line 494
    .local v4, availableInTopHalf:I
    div-int/lit8 v18, v12, 0x2

    sub-int v3, v6, v18

    .line 497
    .local v3, availableInBottomHalf:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getMeasuredWidth()I

    move-result v18

    if-eqz v18, :cond_0

    .line 498
    invoke-virtual/range {p0 .. p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getMeasuredWidth()I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v5, v1}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->checkDimensionsConsistency(II)V

    .line 500
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getMeasuredHeight()I

    move-result v18

    if-eqz v18, :cond_1

    .line 501
    invoke-virtual/range {p0 .. p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getMeasuredHeight()I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v2, v1}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->checkDimensionsConsistency(II)V

    .line 504
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->mView:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/app/ActionBar$LayoutParams;

    .line 505
    .local v10, params:Landroid/app/ActionBar$LayoutParams;
    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->leftMargin:I

    move/from16 v18, v0

    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->rightMargin:I

    move/from16 v19, v0

    add-int v9, v18, v19

    .line 506
    .local v9, horizontalMargin:I
    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->topMargin:I

    move/from16 v18, v0

    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->bottomMargin:I

    move/from16 v19, v0

    add-int v15, v18, v19

    .line 508
    .local v15, verticalMargin:I
    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->width:I

    move/from16 v18, v0

    const/16 v19, -0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_3

    const/high16 v16, 0x4000

    .line 509
    .local v16, widthMode:I
    :goto_0
    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->width:I

    move/from16 v18, v0

    if-ltz v18, :cond_4

    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->width:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 510
    .local v17, widthValue:I
    :goto_1
    const/16 v18, 0x0

    sub-int v19, v17, v9

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 512
    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->height:I

    move/from16 v18, v0

    const/16 v19, -0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_5

    const/high16 v7, 0x4000

    .line 513
    .local v7, heightMode:I
    :goto_2
    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->height:I

    move/from16 v18, v0

    if-ltz v18, :cond_6

    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->height:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 514
    .local v8, heightValue:I
    :goto_3
    const/16 v18, 0x0

    sub-int v19, v8, v15

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 516
    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->gravity:I

    move/from16 v18, v0

    and-int/lit8 v14, v18, 0x70

    .line 517
    .local v14, vGravity:I
    const/16 v18, 0x10

    move/from16 v0, v18

    if-ne v14, v0, :cond_2

    iget v0, v10, Landroid/app/ActionBar$LayoutParams;->height:I

    move/from16 v18, v0

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 518
    if-lez v4, :cond_2

    if-lez v3, :cond_2

    .line 520
    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v18

    mul-int/lit8 v8, v18, 0x2

    .line 523
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->mView:Landroid/view/View;

    move-object/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v16

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    .line 524
    invoke-static {v8, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    .line 523
    invoke-virtual/range {v18 .. v20}, Landroid/view/View;->measure(II)V

    .line 525
    return-void

    .line 508
    .end local v7           #heightMode:I
    .end local v8           #heightValue:I
    .end local v14           #vGravity:I
    .end local v16           #widthMode:I
    .end local v17           #widthValue:I
    :cond_3
    const/high16 v16, -0x8000

    goto :goto_0

    .restart local v16       #widthMode:I
    :cond_4
    move/from16 v17, v5

    .line 509
    goto :goto_1

    .line 512
    .restart local v17       #widthValue:I
    :cond_5
    const/high16 v7, -0x8000

    goto :goto_2

    .restart local v7       #heightMode:I
    :cond_6
    move v8, v2

    .line 513
    goto :goto_3
.end method
