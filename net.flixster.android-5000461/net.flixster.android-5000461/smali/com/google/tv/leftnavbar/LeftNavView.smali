.class public final Lcom/google/tv/leftnavbar/LeftNavView;
.super Landroid/widget/LinearLayout;
.source "LeftNavView.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;
    }
.end annotation


# instance fields
.field private final mAnimationDuration:I

.field private mAnimationsEnabled:Z

.field private final mApparentWidthCollapsed:I

.field private final mApparentWidthExpanded:I

.field private mDisplayOptions:I

.field private mExpanded:Z

.field private final mHome:Lcom/google/tv/leftnavbar/HomeDisplay;

.field private mNavigationMode:I

.field private final mOptions:Lcom/google/tv/leftnavbar/OptionsDisplay;

.field private final mSpinner:Lcom/google/tv/leftnavbar/SpinnerDisplay;

.field private final mTabs:Lcom/google/tv/leftnavbar/TabDisplay;

.field private final mVisibilityController:Lcom/google/tv/leftnavbar/VisibilityController;

.field private mWidthAnimator:Landroid/animation/ValueAnimator;

.field private final mWidthCollapsed:I

.field private final mWidthExpanded:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    new-instance v1, Lcom/google/tv/leftnavbar/VisibilityController;

    invoke-direct {v1, p0}, Lcom/google/tv/leftnavbar/VisibilityController;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mVisibilityController:Lcom/google/tv/leftnavbar/VisibilityController;

    .line 63
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030038

    invoke-virtual {v1, v2, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 64
    invoke-virtual {p0, v5}, Lcom/google/tv/leftnavbar/LeftNavView;->setOrientation(I)V

    .line 66
    new-instance v1, Lcom/google/tv/leftnavbar/HomeDisplay;

    invoke-direct {v1, p1, p0, v4}, Lcom/google/tv/leftnavbar/HomeDisplay;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V

    invoke-virtual {v1, v3}, Lcom/google/tv/leftnavbar/HomeDisplay;->setVisible(Z)Lcom/google/tv/leftnavbar/HomeDisplay;

    move-result-object v1

    iput-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mHome:Lcom/google/tv/leftnavbar/HomeDisplay;

    .line 67
    new-instance v1, Lcom/google/tv/leftnavbar/TabDisplay;

    invoke-direct {v1, p1, p0, v4}, Lcom/google/tv/leftnavbar/TabDisplay;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V

    invoke-virtual {v1, v3}, Lcom/google/tv/leftnavbar/TabDisplay;->setVisible(Z)Lcom/google/tv/leftnavbar/TabDisplay;

    move-result-object v1

    iput-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mTabs:Lcom/google/tv/leftnavbar/TabDisplay;

    .line 68
    new-instance v1, Lcom/google/tv/leftnavbar/OptionsDisplay;

    invoke-direct {v1, p1, p0, v4}, Lcom/google/tv/leftnavbar/OptionsDisplay;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V

    invoke-virtual {v1, v3}, Lcom/google/tv/leftnavbar/OptionsDisplay;->setVisible(Z)Lcom/google/tv/leftnavbar/OptionsDisplay;

    move-result-object v1

    iput-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mOptions:Lcom/google/tv/leftnavbar/OptionsDisplay;

    .line 69
    new-instance v1, Lcom/google/tv/leftnavbar/SpinnerDisplay;

    invoke-direct {v1, p1, p0, v4}, Lcom/google/tv/leftnavbar/SpinnerDisplay;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V

    invoke-virtual {v1, v3}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->setVisible(Z)Lcom/google/tv/leftnavbar/SpinnerDisplay;

    move-result-object v1

    iput-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mSpinner:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 72
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f0a0063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthCollapsed:I

    .line 73
    const v1, 0x7f0a0065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthExpanded:I

    .line 74
    const v1, 0x7f0a0064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mApparentWidthCollapsed:I

    .line 75
    const v1, 0x7f0a0066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mApparentWidthExpanded:I

    .line 76
    const/high16 v1, 0x10e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mAnimationDuration:I

    .line 78
    iput v3, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mNavigationMode:I

    .line 79
    invoke-virtual {p0, v3}, Lcom/google/tv/leftnavbar/LeftNavView;->setNavigationMode(I)V

    .line 80
    return-void
.end method

.method static synthetic access$0(Lcom/google/tv/leftnavbar/LeftNavView;)Lcom/google/tv/leftnavbar/HomeDisplay;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mHome:Lcom/google/tv/leftnavbar/HomeDisplay;

    return-object v0
.end method

.method static synthetic access$1(Lcom/google/tv/leftnavbar/LeftNavView;)I
    .locals 1
    .parameter

    .prologue
    .line 55
    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mNavigationMode:I

    return v0
.end method

.method static synthetic access$2(Lcom/google/tv/leftnavbar/LeftNavView;)Lcom/google/tv/leftnavbar/TabDisplay;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mTabs:Lcom/google/tv/leftnavbar/TabDisplay;

    return-object v0
.end method

.method static synthetic access$3(Lcom/google/tv/leftnavbar/LeftNavView;)Lcom/google/tv/leftnavbar/SpinnerDisplay;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mSpinner:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    return-object v0
.end method

.method static synthetic access$4(Lcom/google/tv/leftnavbar/LeftNavView;)Lcom/google/tv/leftnavbar/OptionsDisplay;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mOptions:Lcom/google/tv/leftnavbar/OptionsDisplay;

    return-object v0
.end method

.method static synthetic access$5(Lcom/google/tv/leftnavbar/LeftNavView;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 279
    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/LeftNavView;->setViewWidth(I)V

    return-void
.end method

.method static synthetic access$6(Lcom/google/tv/leftnavbar/LeftNavView;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 269
    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/LeftNavView;->setContentExpanded(Z)V

    return-void
.end method

.method private getCustomViewWrapper()Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;
    .locals 3

    .prologue
    .line 373
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getMainSection()Landroid/view/ViewGroup;

    move-result-object v0

    .line 375
    .local v0, main:Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 376
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;

    .line 378
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getMainSection()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 98
    const v0, 0x7f0700d5

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private static has(II)Z
    .locals 1
    .parameter "changes"
    .parameter "option"

    .prologue
    .line 302
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasCustomView()Z
    .locals 1

    .prologue
    .line 360
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getCustomViewWrapper()Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasVisibleCustomView()Z
    .locals 1

    .prologue
    .line 364
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->hasCustomView()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getCustomViewWrapper()Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setContentExpanded(Z)V
    .locals 1
    .parameter "expanded"

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mTabs:Lcom/google/tv/leftnavbar/TabDisplay;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TabDisplay;->setExpanded(Z)Lcom/google/tv/leftnavbar/TabDisplay;

    .line 271
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mOptions:Lcom/google/tv/leftnavbar/OptionsDisplay;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/OptionsDisplay;->setExpanded(Z)Lcom/google/tv/leftnavbar/OptionsDisplay;

    .line 272
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mHome:Lcom/google/tv/leftnavbar/HomeDisplay;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/HomeDisplay;->setExpanded(Z)Lcom/google/tv/leftnavbar/HomeDisplay;

    .line 273
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mSpinner:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->setExpanded(Z)Lcom/google/tv/leftnavbar/SpinnerDisplay;

    .line 274
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->hasCustomView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getCustomView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setActivated(Z)V

    .line 277
    :cond_0
    return-void
.end method

.method private setCustomViewVisibility(Z)V
    .locals 2
    .parameter "visible"

    .prologue
    .line 382
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getCustomViewWrapper()Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;

    move-result-object v0

    .line 383
    .local v0, current:Landroid/view/View;
    if-eqz v0, :cond_0

    .line 384
    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 386
    :cond_0
    return-void

    .line 384
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method

.method private setExpanded(Z)V
    .locals 1
    .parameter "expanded"

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mAnimationsEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/tv/leftnavbar/LeftNavView;->setExpanded(ZZ)V

    .line 225
    return-void

    .line 224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setExpanded(ZZ)V
    .locals 3
    .parameter "expanded"
    .parameter "animated"

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mExpanded:Z

    if-ne v0, p1, :cond_0

    .line 267
    :goto_0
    return-void

    .line 231
    :cond_0
    if-eqz p2, :cond_3

    .line 232
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 235
    :cond_1
    const/4 v0, 0x2

    new-array v1, v0, [I

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    aput v2, v1, v0

    const/4 v2, 0x1

    .line 236
    if-eqz p1, :cond_2

    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthExpanded:I

    :goto_1
    aput v0, v1, v2

    .line 235
    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthAnimator:Landroid/animation/ValueAnimator;

    .line 237
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthAnimator:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mAnimationDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 238
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/tv/leftnavbar/LeftNavView$1;

    invoke-direct {v1, p0}, Lcom/google/tv/leftnavbar/LeftNavView$1;-><init>(Lcom/google/tv/leftnavbar/LeftNavView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 245
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/tv/leftnavbar/LeftNavView$2;

    invoke-direct {v1, p0, p1}, Lcom/google/tv/leftnavbar/LeftNavView$2;-><init>(Lcom/google/tv/leftnavbar/LeftNavView;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 261
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 266
    :goto_2
    iput-boolean p1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mExpanded:Z

    goto :goto_0

    .line 236
    :cond_2
    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthCollapsed:I

    goto :goto_1

    .line 263
    :cond_3
    if-eqz p1, :cond_4

    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthExpanded:I

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavView;->setViewWidth(I)V

    .line 264
    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/LeftNavView;->setContentExpanded(Z)V

    goto :goto_2

    .line 263
    :cond_4
    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mWidthCollapsed:I

    goto :goto_3
.end method

.method private setExpandedState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 216
    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->hasFocus()Z

    move-result v0

    invoke-direct {p0, v0, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->setExpanded(ZZ)V

    .line 221
    :goto_0
    return-void

    .line 219
    :cond_0
    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    const/16 v1, 0x20

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v0

    invoke-direct {p0, v0, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->setExpanded(ZZ)V

    goto :goto_0
.end method

.method private setHomeMode()V
    .locals 3

    .prologue
    .line 205
    iget v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    const/16 v2, 0x80

    invoke-static {v1, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    sget-object v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->BOTH:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    .line 212
    .local v0, mode:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;
    :goto_0
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mHome:Lcom/google/tv/leftnavbar/HomeDisplay;

    invoke-virtual {v1, v0}, Lcom/google/tv/leftnavbar/HomeDisplay;->setImageMode(Lcom/google/tv/leftnavbar/HomeDisplay$Mode;)Lcom/google/tv/leftnavbar/HomeDisplay;

    .line 213
    return-void

    .line 207
    .end local v0           #mode:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;
    :cond_0
    iget v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 208
    sget-object v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->LOGO:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    .restart local v0       #mode:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;
    goto :goto_0

    .line 210
    .end local v0           #mode:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;
    :cond_1
    sget-object v0, Lcom/google/tv/leftnavbar/HomeDisplay$Mode;->ICON:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;

    .restart local v0       #mode:Lcom/google/tv/leftnavbar/HomeDisplay$Mode;
    goto :goto_0
.end method

.method private setNavigationModeVisibility(IZ)V
    .locals 1
    .parameter "mode"
    .parameter "visible"

    .prologue
    .line 323
    packed-switch p1, :pswitch_data_0

    .line 335
    :goto_0
    return-void

    .line 325
    :pswitch_0
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mTabs:Lcom/google/tv/leftnavbar/TabDisplay;

    invoke-virtual {v0, p2}, Lcom/google/tv/leftnavbar/TabDisplay;->setVisible(Z)Lcom/google/tv/leftnavbar/TabDisplay;

    goto :goto_0

    .line 329
    :pswitch_1
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mSpinner:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    invoke-virtual {v0, p2}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->setVisible(Z)Lcom/google/tv/leftnavbar/SpinnerDisplay;

    goto :goto_0

    .line 323
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setViewWidth(I)V
    .locals 1
    .parameter "width"

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 281
    .local v0, params:Landroid/view/ViewGroup$LayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 282
    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/LeftNavView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 283
    return-void
.end method


# virtual methods
.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 2
    .parameter
    .parameter "direction"
    .parameter "focusableMode"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v1, 0x2

    if-ne p2, v1, :cond_1

    .line 118
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->addFocusables(Ljava/util/ArrayList;II)V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    const/16 v1, 0x11

    if-eq p2, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    :cond_2
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_4

    .line 128
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 129
    .local v0, initialCount:I
    iget v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mNavigationMode:I

    packed-switch v1, :pswitch_data_0

    .line 139
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->hasCustomView()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 140
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getCustomView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 144
    :cond_3
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt v1, v0, :cond_0

    .line 149
    .end local v0           #initialCount:I
    :cond_4
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_0

    .line 131
    .restart local v0       #initialCount:I
    :pswitch_0
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mTabs:Lcom/google/tv/leftnavbar/TabDisplay;

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/TabDisplay;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_1

    .line 135
    :pswitch_1
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mSpinner:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_1

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 1
    .parameter "focused"
    .parameter "direction"

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x42

    if-eq p2, v0, :cond_0

    .line 157
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 159
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getApparentWidth(Z)I
    .locals 4
    .parameter "ignoreHiddenState"

    .prologue
    const/4 v1, 0x0

    .line 289
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->isVisible()Z

    move-result v2

    if-nez v2, :cond_0

    if-nez p1, :cond_0

    .line 294
    :goto_0
    return v1

    .line 292
    :cond_0
    iget v2, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    const/16 v3, 0x40

    invoke-static {v2, v3}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v2

    if-nez v2, :cond_1

    .line 293
    iget v2, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    const/16 v3, 0x20

    invoke-static {v2, v3}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 294
    .local v0, isCollapsed:Z
    :goto_1
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mApparentWidthCollapsed:I

    goto :goto_0

    .line 293
    .end local v0           #isCollapsed:Z
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 294
    .restart local v0       #isCollapsed:Z
    :cond_2
    iget v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mApparentWidthExpanded:I

    goto :goto_0
.end method

.method public getCustomView()Landroid/view/View;
    .locals 2

    .prologue
    .line 368
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getCustomViewWrapper()Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;

    move-result-object v0

    .line 369
    .local v0, wrapper:Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->getView()Landroid/view/View;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDisplayOptions()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    return v0
.end method

.method public getNavigationMode()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mNavigationMode:I

    return v0
.end method

.method public getSpinner()Lcom/google/tv/leftnavbar/SpinnerDisplay;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mSpinner:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    return-object v0
.end method

.method public getTabs()Lcom/google/tv/leftnavbar/TabDisplay;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mTabs:Lcom/google/tv/leftnavbar/TabDisplay;

    return-object v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mVisibilityController:Lcom/google/tv/leftnavbar/VisibilityController;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/VisibilityController;->isVisible()Z

    move-result v0

    return v0
.end method

.method protected onDescendantFocusChanged(Z)V
    .locals 2
    .parameter "hasFocus"

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowFocusChanged(Z)V

    .line 165
    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/LeftNavView;->setExpanded(Z)V

    .line 168
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 84
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 87
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mHome:Lcom/google/tv/leftnavbar/HomeDisplay;

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/HomeDisplay;->getView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->addView(Landroid/view/View;I)V

    .line 89
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mOptions:Lcom/google/tv/leftnavbar/OptionsDisplay;

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/OptionsDisplay;->getView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->addView(Landroid/view/View;I)V

    .line 92
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getMainSection()Landroid/view/ViewGroup;

    move-result-object v0

    .line 93
    .local v0, main:Landroid/view/ViewGroup;
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mTabs:Lcom/google/tv/leftnavbar/TabDisplay;

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/TabDisplay;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 94
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mSpinner:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 95
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 398
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 399
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->hasVisibleCustomView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getCustomViewWrapper()Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->onPostLayout(Lcom/google/tv/leftnavbar/LeftNavView;)V

    .line 402
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 390
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 391
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->hasVisibleCustomView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getCustomViewWrapper()Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->onPostMeasure(Lcom/google/tv/leftnavbar/LeftNavView;)V

    .line 394
    :cond_0
    return-void
.end method

.method public setAnimationsEnabled(Z)V
    .locals 0
    .parameter "enabled"

    .prologue
    .line 298
    iput-boolean p1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mAnimationsEnabled:Z

    .line 299
    return-void
.end method

.method public setCustomView(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getMainSection()Landroid/view/ViewGroup;

    move-result-object v1

    .line 347
    .local v1, main:Landroid/view/ViewGroup;
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getCustomViewWrapper()Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;

    move-result-object v0

    .line 348
    .local v0, current:Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;
    if-eqz v0, :cond_0

    .line 349
    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;->detach()V

    .line 350
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 352
    :cond_0
    if-eqz p1, :cond_1

    .line 353
    iget-boolean v2, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mExpanded:Z

    invoke-virtual {p1, v2}, Landroid/view/View;->setActivated(Z)V

    .line 354
    new-instance v2, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Lcom/google/tv/leftnavbar/LeftNavView$CustomViewWrapper;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 355
    iget v2, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    const/16 v3, 0x10

    invoke-static {v2, v3}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->setCustomViewVisibility(Z)V

    .line 357
    :cond_1
    return-void
.end method

.method public setDisplayOptions(I)I
    .locals 5
    .parameter "options"

    .prologue
    const/16 v4, 0x10

    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 183
    iget v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    xor-int v0, p1, v1

    .line 184
    .local v0, changes:I
    iput p1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    .line 185
    invoke-static {v0, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mHome:Lcom/google/tv/leftnavbar/HomeDisplay;

    invoke-static {p1, v2}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/tv/leftnavbar/HomeDisplay;->setVisible(Z)Lcom/google/tv/leftnavbar/HomeDisplay;

    .line 188
    :cond_0
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x80

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 189
    :cond_1
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->setHomeMode()V

    .line 191
    :cond_2
    invoke-static {v0, v3}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 192
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mHome:Lcom/google/tv/leftnavbar/HomeDisplay;

    invoke-static {p1, v3}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/tv/leftnavbar/HomeDisplay;->setAsUp(Z)Lcom/google/tv/leftnavbar/HomeDisplay;

    .line 194
    :cond_3
    invoke-static {v0, v4}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 195
    iget v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mDisplayOptions:I

    invoke-static {v1, v4}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->setCustomViewVisibility(Z)V

    .line 197
    :cond_4
    const/16 v1, 0x40

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v1

    if-nez v1, :cond_5

    const/16 v1, 0x20

    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->has(II)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 198
    :cond_5
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/LeftNavView;->setExpandedState()V

    .line 200
    :cond_6
    return v0
.end method

.method public setNavigationMode(I)V
    .locals 2
    .parameter "mode"

    .prologue
    .line 314
    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mNavigationMode:I

    if-ne v0, p1, :cond_0

    .line 320
    :goto_0
    return-void

    .line 317
    :cond_0
    iget v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mNavigationMode:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/tv/leftnavbar/LeftNavView;->setNavigationModeVisibility(IZ)V

    .line 318
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/tv/leftnavbar/LeftNavView;->setNavigationModeVisibility(IZ)V

    .line 319
    iput p1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mNavigationMode:I

    goto :goto_0
.end method

.method public setOnClickHomeListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mHome:Lcom/google/tv/leftnavbar/HomeDisplay;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/HomeDisplay;->setOnClickHomeListener(Landroid/view/View$OnClickListener;)V

    .line 111
    return-void
.end method

.method public setVisible(ZZ)Z
    .locals 2
    .parameter "visible"
    .parameter "animated"

    .prologue
    .line 102
    iget-object v1, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mVisibilityController:Lcom/google/tv/leftnavbar/VisibilityController;

    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mAnimationsEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/google/tv/leftnavbar/VisibilityController;->setVisible(ZZ)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showOptionsMenu(Ljava/lang/Boolean;)V
    .locals 2
    .parameter "show"

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/tv/leftnavbar/LeftNavView;->mOptions:Lcom/google/tv/leftnavbar/OptionsDisplay;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/tv/leftnavbar/OptionsDisplay;->setVisible(Z)Lcom/google/tv/leftnavbar/OptionsDisplay;

    .line 343
    return-void
.end method
