.class final Lcom/google/tv/leftnavbar/OptionsDisplay;
.super Ljava/lang/Object;
.source "OptionsDisplay.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mExpanded:Z

.field private mView:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V
    .locals 0
    .parameter "context"
    .parameter "parent"
    .parameter "attributes"

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mContext:Landroid/content/Context;

    .line 37
    invoke-direct {p0, p2, p3}, Lcom/google/tv/leftnavbar/OptionsDisplay;->createView(Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V

    .line 38
    return-void
.end method

.method static synthetic access$0(Lcom/google/tv/leftnavbar/OptionsDisplay;)Landroid/content/Context;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private configureOption(Landroid/view/View;Ljava/lang/CharSequence;Z)Landroid/view/View;
    .locals 2
    .parameter "option"
    .parameter "title"
    .parameter "active"

    .prologue
    .line 105
    invoke-static {p1}, Lcom/google/tv/leftnavbar/OptionsDisplay;->getOptionIcon(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v0

    .line 106
    .local v0, iconView:Landroid/widget/ImageView;
    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 107
    invoke-static {p1}, Lcom/google/tv/leftnavbar/OptionsDisplay;->getOptionTitle(Landroid/view/View;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-object p1
.end method

.method private createView(Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V
    .locals 5
    .parameter "parent"
    .parameter "attributes"

    .prologue
    const/4 v4, 0x1

    .line 70
    iget-object v1, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03003b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mView:Landroid/view/ViewGroup;

    .line 71
    iget-object v1, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mView:Landroid/view/ViewGroup;

    const v2, 0x7f0700da

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 73
    .local v0, menuOption:Landroid/view/View;
    iget-object v1, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c01bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, v4}, Lcom/google/tv/leftnavbar/OptionsDisplay;->configureOption(Landroid/view/View;Ljava/lang/CharSequence;Z)Landroid/view/View;

    .line 74
    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 75
    invoke-virtual {v0, v4}, Landroid/view/View;->setFocusable(Z)V

    .line 76
    new-instance v1, Lcom/google/tv/leftnavbar/OptionsDisplay$1;

    invoke-direct {v1, p0}, Lcom/google/tv/leftnavbar/OptionsDisplay$1;-><init>(Lcom/google/tv/leftnavbar/OptionsDisplay;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    invoke-static {v0}, Lcom/google/tv/leftnavbar/OptionsDisplay;->getOptionIcon(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/tv/leftnavbar/OptionsDisplay;->setDuplicateParentState(Landroid/view/View;)V

    .line 86
    invoke-static {v0}, Lcom/google/tv/leftnavbar/OptionsDisplay;->getOptionTitle(Landroid/view/View;)Landroid/widget/TextView;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/tv/leftnavbar/OptionsDisplay;->setDuplicateParentState(Landroid/view/View;)V

    .line 87
    return-void
.end method

.method private static getOptionIcon(Landroid/view/View;)Landroid/widget/ImageView;
    .locals 1
    .parameter "option"

    .prologue
    .line 116
    const v0, 0x7f070061

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private static getOptionTitle(Landroid/view/View;)Landroid/widget/TextView;
    .locals 1
    .parameter "option"

    .prologue
    .line 120
    const v0, 0x7f07006f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private getOptionsContainer()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mView:Landroid/view/ViewGroup;

    const v1, 0x7f0700d9

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private refreshExpandedState()V
    .locals 4

    .prologue
    .line 61
    iget-object v2, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mView:Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mExpanded:Z

    invoke-static {v2, v3}, Lcom/google/tv/leftnavbar/OptionsDisplay;->setOptionExpanded(Landroid/view/View;Z)V

    .line 63
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/OptionsDisplay;->getOptionsContainer()Landroid/view/ViewGroup;

    move-result-object v1

    .line 64
    .local v1, optionsContainer:Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 67
    return-void

    .line 65
    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mExpanded:Z

    invoke-static {v2, v3}, Lcom/google/tv/leftnavbar/OptionsDisplay;->setOptionExpanded(Landroid/view/View;Z)V

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setDuplicateParentState(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 94
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/View;->setDuplicateParentStateEnabled(Z)V

    .line 95
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 96
    .local v1, parent:Landroid/view/ViewGroup;
    if-nez v1, :cond_0

    .line 102
    :goto_0
    return-void

    .line 99
    :cond_0
    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 100
    .local v0, index:I
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 101
    invoke-virtual {v1, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private static setOptionExpanded(Landroid/view/View;Z)V
    .locals 2
    .parameter "option"
    .parameter "expanded"

    .prologue
    .line 112
    invoke-static {p0}, Lcom/google/tv/leftnavbar/OptionsDisplay;->getOptionTitle(Landroid/view/View;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 113
    return-void

    .line 112
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method isVisible()Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setExpanded(Z)Lcom/google/tv/leftnavbar/OptionsDisplay;
    .locals 0
    .parameter "expanded"

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mExpanded:Z

    .line 55
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/OptionsDisplay;->refreshExpandedState()V

    .line 56
    return-object p0
.end method

.method setVisible(Z)Lcom/google/tv/leftnavbar/OptionsDisplay;
    .locals 2
    .parameter "visible"

    .prologue
    .line 45
    iget-object v1, p0, Lcom/google/tv/leftnavbar/OptionsDisplay;->mView:Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 46
    return-object p0

    .line 45
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
