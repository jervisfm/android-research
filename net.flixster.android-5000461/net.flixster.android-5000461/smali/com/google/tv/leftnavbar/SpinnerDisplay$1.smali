.class Lcom/google/tv/leftnavbar/SpinnerDisplay$1;
.super Ljava/lang/Object;
.source "SpinnerDisplay.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/tv/leftnavbar/SpinnerDisplay;->createView(Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/tv/leftnavbar/SpinnerDisplay;


# direct methods
.method constructor <init>(Lcom/google/tv/leftnavbar/SpinnerDisplay;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay$1;->this$0:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay$1;->this$0:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    #getter for: Lcom/google/tv/leftnavbar/SpinnerDisplay;->mListener:Landroid/app/ActionBar$OnNavigationListener;
    invoke-static {v0}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->access$0(Lcom/google/tv/leftnavbar/SpinnerDisplay;)Landroid/app/ActionBar$OnNavigationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay$1;->this$0:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    #getter for: Lcom/google/tv/leftnavbar/SpinnerDisplay;->mListener:Landroid/app/ActionBar$OnNavigationListener;
    invoke-static {v0}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->access$0(Lcom/google/tv/leftnavbar/SpinnerDisplay;)Landroid/app/ActionBar$OnNavigationListener;

    move-result-object v0

    invoke-interface {v0, p3, p4, p5}, Landroid/app/ActionBar$OnNavigationListener;->onNavigationItemSelected(IJ)Z

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay$1;->this$0:Lcom/google/tv/leftnavbar/SpinnerDisplay;

    #calls: Lcom/google/tv/leftnavbar/SpinnerDisplay;->refreshSelectedItem()V
    invoke-static {v0}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->access$1(Lcom/google/tv/leftnavbar/SpinnerDisplay;)V

    .line 95
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    return-void
.end method
