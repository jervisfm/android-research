.class final Lcom/google/tv/leftnavbar/SpinnerDisplay;
.super Ljava/lang/Object;
.source "SpinnerDisplay.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mExpanded:Z

.field private mListener:Landroid/app/ActionBar$OnNavigationListener;

.field private mView:Landroid/widget/Spinner;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V
    .locals 0
    .parameter "context"
    .parameter "parent"
    .parameter "attributes"

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mContext:Landroid/content/Context;

    .line 41
    invoke-direct {p0, p2, p3}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->createView(Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V

    .line 42
    return-void
.end method

.method static synthetic access$0(Lcom/google/tv/leftnavbar/SpinnerDisplay;)Landroid/app/ActionBar$OnNavigationListener;
    .locals 1
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mListener:Landroid/app/ActionBar$OnNavigationListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/google/tv/leftnavbar/SpinnerDisplay;)V
    .locals 0
    .parameter

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->refreshSelectedItem()V

    return-void
.end method

.method private createView(Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V
    .locals 3
    .parameter "parent"
    .parameter "attributes"

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mView:Landroid/widget/Spinner;

    .line 87
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mView:Landroid/widget/Spinner;

    new-instance v1, Lcom/google/tv/leftnavbar/SpinnerDisplay$1;

    invoke-direct {v1, p0}, Lcom/google/tv/leftnavbar/SpinnerDisplay$1;-><init>(Lcom/google/tv/leftnavbar/SpinnerDisplay;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 101
    return-void
.end method

.method private refreshSelectedItem()V
    .locals 2

    .prologue
    .line 78
    iget-object v1, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mView:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, selected:Landroid/view/View;
    if-nez v0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-boolean v1, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mExpanded:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    goto :goto_0
.end method


# virtual methods
.method getCount()I
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mView:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    return v0
.end method

.method getSelected()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mView:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mView:Landroid/widget/Spinner;

    return-object v0
.end method

.method setContent(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V
    .locals 1
    .parameter "adapter"
    .parameter "listener"

    .prologue
    .line 60
    iput-object p2, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mListener:Landroid/app/ActionBar$OnNavigationListener;

    .line 61
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mView:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 62
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->refreshSelectedItem()V

    .line 63
    return-void
.end method

.method setExpanded(Z)Lcom/google/tv/leftnavbar/SpinnerDisplay;
    .locals 0
    .parameter "expanded"

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mExpanded:Z

    .line 55
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/SpinnerDisplay;->refreshSelectedItem()V

    .line 56
    return-object p0
.end method

.method setSelected(I)V
    .locals 1
    .parameter "position"

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mView:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 67
    return-void
.end method

.method setVisible(Z)Lcom/google/tv/leftnavbar/SpinnerDisplay;
    .locals 2
    .parameter "visible"

    .prologue
    .line 49
    iget-object v1, p0, Lcom/google/tv/leftnavbar/SpinnerDisplay;->mView:Landroid/widget/Spinner;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 50
    return-object p0

    .line 49
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
