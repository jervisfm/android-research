.class final Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;
.super Landroid/widget/ArrayAdapter;
.source "TabDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/tv/leftnavbar/TabDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TabAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/tv/leftnavbar/TabImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCachedViews:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/tv/leftnavbar/TabImpl;",
            "Lcom/google/tv/leftnavbar/TabFrame;",
            ">;"
        }
    .end annotation
.end field

.field private mIsSelectionActive:Z

.field private mSavedSelection:Lcom/google/tv/leftnavbar/TabImpl;

.field private mSelection:Lcom/google/tv/leftnavbar/TabImpl;

.field final synthetic this$0:Lcom/google/tv/leftnavbar/TabDisplay;


# direct methods
.method constructor <init>(Lcom/google/tv/leftnavbar/TabDisplay;Landroid/content/Context;)V
    .locals 1
    .parameter
    .parameter "context"

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->this$0:Lcom/google/tv/leftnavbar/TabDisplay;

    .line 187
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 188
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mCachedViews:Ljava/util/Map;

    .line 189
    invoke-static {}, Lcom/google/tv/leftnavbar/TabDisplay;->access$0()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSelection:Lcom/google/tv/leftnavbar/TabImpl;

    .line 190
    invoke-static {}, Lcom/google/tv/leftnavbar/TabDisplay;->access$0()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSavedSelection:Lcom/google/tv/leftnavbar/TabImpl;

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mIsSelectionActive:Z

    .line 192
    return-void
.end method

.method private isSelected(Lcom/google/tv/leftnavbar/TabImpl;)Z
    .locals 1
    .parameter "tab"

    .prologue
    .line 231
    invoke-static {}, Lcom/google/tv/leftnavbar/TabDisplay;->access$0()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getSelected()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setSelectionState(Lcom/google/tv/leftnavbar/TabImpl;Z)V
    .locals 1
    .parameter "tab"
    .parameter "selected"

    .prologue
    .line 235
    invoke-static {}, Lcom/google/tv/leftnavbar/TabDisplay;->access$0()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mCachedViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mCachedViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/tv/leftnavbar/TabFrame;

    invoke-virtual {v0, p2}, Lcom/google/tv/leftnavbar/TabFrame;->select(Z)V

    .line 238
    :cond_0
    return-void
.end method

.method private updatePositions(Z)V
    .locals 3
    .parameter "allInvalid"

    .prologue
    .line 314
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 317
    return-void

    .line 315
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/tv/leftnavbar/TabImpl;

    if-eqz p1, :cond_1

    const/4 v2, -0x1

    :goto_1
    invoke-virtual {v1, v2}, Lcom/google/tv/leftnavbar/TabImpl;->setPosition(I)V

    .line 314
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v0

    .line 315
    goto :goto_1
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 273
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->updatePositions(Z)V

    .line 274
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 277
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mCachedViews:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 278
    invoke-static {}, Lcom/google/tv/leftnavbar/TabDisplay;->access$0()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->setSelected(Lcom/google/tv/leftnavbar/TabImpl;)V

    .line 279
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 280
    return-void

    .line 275
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/tv/leftnavbar/TabImpl;

    invoke-virtual {v1}, Lcom/google/tv/leftnavbar/TabImpl;->getCustomView()Landroid/view/View;

    move-result-object v1

    #calls: Lcom/google/tv/leftnavbar/TabDisplay;->detachFromParent(Landroid/view/View;)V
    invoke-static {v1}, Lcom/google/tv/leftnavbar/TabDisplay;->access$3(Landroid/view/View;)V

    .line 274
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .parameter "position"

    .prologue
    .line 249
    const/4 v0, -0x1

    return v0
.end method

.method public getSelected()Lcom/google/tv/leftnavbar/TabImpl;
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mIsSelectionActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSelection:Lcom/google/tv/leftnavbar/TabImpl;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSavedSelection:Lcom/google/tv/leftnavbar/TabImpl;

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 284
    invoke-virtual {p0, p1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/tv/leftnavbar/TabImpl;

    .line 285
    .local v2, tab:Lcom/google/tv/leftnavbar/TabImpl;
    iget-object v3, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mCachedViews:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 286
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03003d

    .line 287
    const/4 v5, 0x0

    .line 286
    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/tv/leftnavbar/TabFrame;

    .line 288
    .local v0, frame:Lcom/google/tv/leftnavbar/TabFrame;
    invoke-virtual {v2}, Lcom/google/tv/leftnavbar/TabImpl;->hasCustomView()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 289
    invoke-virtual {v2}, Lcom/google/tv/leftnavbar/TabImpl;->getCustomView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/tv/leftnavbar/TabFrame;->configureCustom(Landroid/view/View;)V

    .line 293
    :goto_0
    new-instance v3, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter$1;

    invoke-direct {v3, p0, v2}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter$1;-><init>(Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;Lcom/google/tv/leftnavbar/TabImpl;)V

    invoke-virtual {v0, v3}, Lcom/google/tv/leftnavbar/TabFrame;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    iget-object v3, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mCachedViews:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    .end local v0           #frame:Lcom/google/tv/leftnavbar/TabFrame;
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->isSelected(Lcom/google/tv/leftnavbar/TabImpl;)Z

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->setSelectionState(Lcom/google/tv/leftnavbar/TabImpl;Z)V

    .line 303
    iget-object v3, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mCachedViews:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/tv/leftnavbar/TabFrame;

    .line 304
    .local v1, result:Lcom/google/tv/leftnavbar/TabFrame;
    iget-object v3, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->this$0:Lcom/google/tv/leftnavbar/TabDisplay;

    #getter for: Lcom/google/tv/leftnavbar/TabDisplay;->mExpanded:Z
    invoke-static {v3}, Lcom/google/tv/leftnavbar/TabDisplay;->access$2(Lcom/google/tv/leftnavbar/TabDisplay;)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/tv/leftnavbar/TabFrame;->expand(Z)V

    .line 305
    return-object v1

    .line 291
    .end local v1           #result:Lcom/google/tv/leftnavbar/TabFrame;
    .restart local v0       #frame:Lcom/google/tv/leftnavbar/TabFrame;
    :cond_1
    invoke-virtual {v2}, Lcom/google/tv/leftnavbar/TabImpl;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/tv/leftnavbar/TabImpl;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/tv/leftnavbar/TabFrame;->configureNormal(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public insert(Lcom/google/tv/leftnavbar/TabImpl;I)V
    .locals 1
    .parameter "tab"
    .parameter "position"

    .prologue
    .line 254
    invoke-super {p0, p1, p2}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    .line 255
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->updatePositions(Z)V

    .line 256
    return-void
.end method

.method public bridge synthetic insert(Ljava/lang/Object;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lcom/google/tv/leftnavbar/TabImpl;

    invoke-virtual {p0, p1, p2}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->insert(Lcom/google/tv/leftnavbar/TabImpl;I)V

    return-void
.end method

.method public refresh()V
    .locals 3

    .prologue
    .line 241
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mCachedViews:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 244
    return-void

    .line 241
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/tv/leftnavbar/TabFrame;

    .line 242
    .local v0, frame:Lcom/google/tv/leftnavbar/TabFrame;
    iget-object v2, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->this$0:Lcom/google/tv/leftnavbar/TabDisplay;

    #getter for: Lcom/google/tv/leftnavbar/TabDisplay;->mExpanded:Z
    invoke-static {v2}, Lcom/google/tv/leftnavbar/TabDisplay;->access$2(Lcom/google/tv/leftnavbar/TabDisplay;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/tv/leftnavbar/TabFrame;->expand(Z)V

    goto :goto_0
.end method

.method public remove(Lcom/google/tv/leftnavbar/TabImpl;)V
    .locals 2
    .parameter "tab"

    .prologue
    const/4 v1, 0x0

    .line 261
    invoke-virtual {p1}, Lcom/google/tv/leftnavbar/TabImpl;->getCustomView()Landroid/view/View;

    move-result-object v0

    #calls: Lcom/google/tv/leftnavbar/TabDisplay;->detachFromParent(Landroid/view/View;)V
    invoke-static {v0}, Lcom/google/tv/leftnavbar/TabDisplay;->access$3(Landroid/view/View;)V

    .line 262
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mCachedViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    .line 264
    invoke-direct {p0, v1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->updatePositions(Z)V

    .line 265
    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->isSelected(Lcom/google/tv/leftnavbar/TabImpl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/tv/leftnavbar/TabDisplay;->access$0()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->setSelected(Lcom/google/tv/leftnavbar/TabImpl;)V

    .line 268
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/google/tv/leftnavbar/TabImpl;->setPosition(I)V

    .line 269
    return-void

    .line 266
    :cond_1
    invoke-virtual {p1}, Lcom/google/tv/leftnavbar/TabImpl;->getPosition()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/tv/leftnavbar/TabImpl;

    goto :goto_0
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Lcom/google/tv/leftnavbar/TabImpl;

    invoke-virtual {p0, p1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->remove(Lcom/google/tv/leftnavbar/TabImpl;)V

    return-void
.end method

.method public setSelected(Lcom/google/tv/leftnavbar/TabImpl;)V
    .locals 3
    .parameter "tab"

    .prologue
    .line 212
    iget-boolean v1, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mIsSelectionActive:Z

    if-nez v1, :cond_0

    .line 214
    iput-object p1, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSavedSelection:Lcom/google/tv/leftnavbar/TabImpl;

    .line 224
    :goto_0
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSelection:Lcom/google/tv/leftnavbar/TabImpl;

    .line 218
    .local v0, oldSelection:Lcom/google/tv/leftnavbar/TabImpl;
    iput-object p1, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSelection:Lcom/google/tv/leftnavbar/TabImpl;

    .line 219
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSelection:Lcom/google/tv/leftnavbar/TabImpl;

    if-eq v0, v1, :cond_1

    .line 220
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->setSelectionState(Lcom/google/tv/leftnavbar/TabImpl;Z)V

    .line 221
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSelection:Lcom/google/tv/leftnavbar/TabImpl;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->setSelectionState(Lcom/google/tv/leftnavbar/TabImpl;Z)V

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->this$0:Lcom/google/tv/leftnavbar/TabDisplay;

    iget-object v2, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSelection:Lcom/google/tv/leftnavbar/TabImpl;

    #calls: Lcom/google/tv/leftnavbar/TabDisplay;->onSelectionChanged(Lcom/google/tv/leftnavbar/TabImpl;Lcom/google/tv/leftnavbar/TabImpl;)V
    invoke-static {v1, v0, v2}, Lcom/google/tv/leftnavbar/TabDisplay;->access$1(Lcom/google/tv/leftnavbar/TabDisplay;Lcom/google/tv/leftnavbar/TabImpl;Lcom/google/tv/leftnavbar/TabImpl;)V

    goto :goto_0
.end method

.method public setSelectionActive(Z)V
    .locals 1
    .parameter "active"

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mIsSelectionActive:Z

    if-ne p1, v0, :cond_0

    .line 209
    :goto_0
    return-void

    .line 198
    :cond_0
    if-eqz p1, :cond_1

    .line 200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mIsSelectionActive:Z

    .line 201
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSavedSelection:Lcom/google/tv/leftnavbar/TabImpl;

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->setSelected(Lcom/google/tv/leftnavbar/TabImpl;)V

    .line 202
    invoke-static {}, Lcom/google/tv/leftnavbar/TabDisplay;->access$0()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSavedSelection:Lcom/google/tv/leftnavbar/TabImpl;

    goto :goto_0

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSelection:Lcom/google/tv/leftnavbar/TabImpl;

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mSavedSelection:Lcom/google/tv/leftnavbar/TabImpl;

    .line 206
    invoke-static {}, Lcom/google/tv/leftnavbar/TabDisplay;->access$0()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->setSelected(Lcom/google/tv/leftnavbar/TabImpl;)V

    .line 207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->mIsSelectionActive:Z

    goto :goto_0
.end method
