.class final Lcom/google/tv/leftnavbar/TabDisplay;
.super Ljava/lang/Object;
.source "TabDisplay.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;
    }
.end annotation


# static fields
.field public static final LAST_POSITION:I = -0x2

.field private static final NONE:Lcom/google/tv/leftnavbar/TabImpl;


# instance fields
.field private final mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

.field private final mContext:Landroid/content/Context;

.field private mExpanded:Z

.field private mList:Lcom/google/tv/leftnavbar/TabListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/google/tv/leftnavbar/TabDisplay;->NONE:Lcom/google/tv/leftnavbar/TabImpl;

    .line 35
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/content/res/TypedArray;)V
    .locals 1
    .parameter "context"
    .parameter "parent"
    .parameter "attributes"

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mContext:Landroid/content/Context;

    .line 54
    new-instance v0, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-direct {v0, p0, p1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;-><init>(Lcom/google/tv/leftnavbar/TabDisplay;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    .line 55
    invoke-direct {p0, p2}, Lcom/google/tv/leftnavbar/TabDisplay;->createView(Landroid/view/ViewGroup;)V

    .line 56
    return-void
.end method

.method static synthetic access$0()Lcom/google/tv/leftnavbar/TabImpl;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/tv/leftnavbar/TabDisplay;->NONE:Lcom/google/tv/leftnavbar/TabImpl;

    return-object v0
.end method

.method static synthetic access$1(Lcom/google/tv/leftnavbar/TabDisplay;Lcom/google/tv/leftnavbar/TabImpl;Lcom/google/tv/leftnavbar/TabImpl;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Lcom/google/tv/leftnavbar/TabDisplay;->onSelectionChanged(Lcom/google/tv/leftnavbar/TabImpl;Lcom/google/tv/leftnavbar/TabImpl;)V

    return-void
.end method

.method static synthetic access$2(Lcom/google/tv/leftnavbar/TabDisplay;)Z
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mExpanded:Z

    return v0
.end method

.method static synthetic access$3(Landroid/view/View;)V
    .locals 0
    .parameter

    .prologue
    .line 150
    invoke-static {p0}, Lcom/google/tv/leftnavbar/TabDisplay;->detachFromParent(Landroid/view/View;)V

    return-void
.end method

.method private createView(Landroid/view/ViewGroup;)V
    .locals 3
    .parameter "parent"

    .prologue
    .line 59
    const v0, 0x7f03003e

    .line 60
    .local v0, resource:I
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/tv/leftnavbar/TabListView;

    iput-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mList:Lcom/google/tv/leftnavbar/TabListView;

    .line 61
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mList:Lcom/google/tv/leftnavbar/TabListView;

    iget-object v2, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v1, v2}, Lcom/google/tv/leftnavbar/TabListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 62
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mList:Lcom/google/tv/leftnavbar/TabListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/tv/leftnavbar/TabListView;->setItemsCanFocus(Z)V

    .line 63
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mList:Lcom/google/tv/leftnavbar/TabListView;

    const/high16 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/tv/leftnavbar/TabListView;->setDescendantFocusability(I)V

    .line 64
    return-void
.end method

.method private static detachFromParent(Landroid/view/View;)V
    .locals 1
    .parameter "view"

    .prologue
    .line 151
    if-nez p0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 155
    .local v0, parent:Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private onSelectionChanged(Lcom/google/tv/leftnavbar/TabImpl;Lcom/google/tv/leftnavbar/TabImpl;)V
    .locals 3
    .parameter "oldSelection"
    .parameter "newSelection"

    .prologue
    .line 108
    const/4 v0, 0x0

    .line 109
    .local v0, transaction:Landroid/app/FragmentTransaction;
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mContext:Landroid/content/Context;

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 110
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->disallowAddToBackStack()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 112
    :cond_0
    if-ne p1, p2, :cond_3

    .line 113
    sget-object v1, Lcom/google/tv/leftnavbar/TabDisplay;->NONE:Lcom/google/tv/leftnavbar/TabImpl;

    if-eq p2, v1, :cond_1

    invoke-virtual {p2}, Lcom/google/tv/leftnavbar/TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 114
    invoke-virtual {p2}, Lcom/google/tv/leftnavbar/TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    move-result-object v1

    invoke-interface {v1, p2, v0}, Landroid/app/ActionBar$TabListener;->onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V

    .line 124
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 125
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 128
    :cond_2
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mList:Lcom/google/tv/leftnavbar/TabListView;

    iget-object v2, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v2, p2}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/tv/leftnavbar/TabListView;->setHighlighted(I)V

    .line 129
    return-void

    .line 117
    :cond_3
    sget-object v1, Lcom/google/tv/leftnavbar/TabDisplay;->NONE:Lcom/google/tv/leftnavbar/TabImpl;

    if-eq p1, v1, :cond_4

    invoke-virtual {p1}, Lcom/google/tv/leftnavbar/TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 118
    invoke-virtual {p1}, Lcom/google/tv/leftnavbar/TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/app/ActionBar$TabListener;->onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V

    .line 120
    :cond_4
    sget-object v1, Lcom/google/tv/leftnavbar/TabDisplay;->NONE:Lcom/google/tv/leftnavbar/TabImpl;

    if-eq p2, v1, :cond_1

    invoke-virtual {p2}, Lcom/google/tv/leftnavbar/TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 121
    invoke-virtual {p2}, Lcom/google/tv/leftnavbar/TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    move-result-object v1

    invoke-interface {v1, p2, v0}, Landroid/app/ActionBar$TabListener;->onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V

    goto :goto_0
.end method


# virtual methods
.method add(Lcom/google/tv/leftnavbar/TabImpl;IZ)V
    .locals 1
    .parameter "tab"
    .parameter "position"
    .parameter "setSelected"

    .prologue
    .line 86
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getCount()I

    move-result p2

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->insert(Lcom/google/tv/leftnavbar/TabImpl;I)V

    .line 90
    if-eqz p3, :cond_1

    .line 91
    invoke-virtual {p0, p1}, Lcom/google/tv/leftnavbar/TabDisplay;->select(Lcom/google/tv/leftnavbar/TabImpl;)V

    .line 93
    :cond_1
    return-void
.end method

.method get(I)Lcom/google/tv/leftnavbar/TabImpl;
    .locals 1
    .parameter "position"

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/tv/leftnavbar/TabImpl;

    return-object v0
.end method

.method getCount()I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method getSelected()Lcom/google/tv/leftnavbar/TabImpl;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getSelected()Lcom/google/tv/leftnavbar/TabImpl;

    move-result-object v0

    return-object v0
.end method

.method getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mList:Lcom/google/tv/leftnavbar/TabListView;

    return-object v0
.end method

.method remove(I)V
    .locals 1
    .parameter "position"

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/tv/leftnavbar/TabImpl;

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabDisplay;->remove(Lcom/google/tv/leftnavbar/TabImpl;)V

    .line 145
    return-void
.end method

.method remove(Lcom/google/tv/leftnavbar/TabImpl;)V
    .locals 1
    .parameter "tab"

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->remove(Lcom/google/tv/leftnavbar/TabImpl;)V

    .line 141
    return-void
.end method

.method removeAll()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->clear()V

    .line 137
    return-void
.end method

.method select(Lcom/google/tv/leftnavbar/TabImpl;)V
    .locals 1
    .parameter "tab"

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->setSelected(Lcom/google/tv/leftnavbar/TabImpl;)V

    .line 101
    return-void
.end method

.method setExpanded(Z)Lcom/google/tv/leftnavbar/TabDisplay;
    .locals 1
    .parameter "expanded"

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mExpanded:Z

    .line 78
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->refresh()V

    .line 79
    return-object p0
.end method

.method setVisible(Z)Lcom/google/tv/leftnavbar/TabDisplay;
    .locals 2
    .parameter "visible"

    .prologue
    .line 71
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mList:Lcom/google/tv/leftnavbar/TabListView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/tv/leftnavbar/TabListView;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabDisplay;->mAdapter:Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;

    invoke-virtual {v0, p1}, Lcom/google/tv/leftnavbar/TabDisplay$TabAdapter;->setSelectionActive(Z)V

    .line 73
    return-object p0

    .line 71
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
