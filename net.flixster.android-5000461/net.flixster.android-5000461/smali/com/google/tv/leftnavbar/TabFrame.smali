.class final Lcom/google/tv/leftnavbar/TabFrame;
.super Landroid/widget/LinearLayout;
.source "TabFrame.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private mConfigured:Z

.field private mIsCustom:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attributes"

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method private getIcon()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 69
    const v0, 0x7f070061

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabFrame;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private getTitle()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 73
    const v0, 0x7f07006f

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabFrame;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private markConfigured(Z)V
    .locals 2
    .parameter "isCustom"

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/TabFrame;->mConfigured:Z

    if-eqz v0, :cond_0

    .line 101
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Frame already configured."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/tv/leftnavbar/TabFrame;->mConfigured:Z

    .line 104
    iput-boolean p1, p0, Lcom/google/tv/leftnavbar/TabFrame;->mIsCustom:Z

    .line 105
    return-void
.end method


# virtual methods
.method public configureCustom(Landroid/view/View;)V
    .locals 3
    .parameter "content"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 84
    invoke-direct {p0, v2}, Lcom/google/tv/leftnavbar/TabFrame;->markConfigured(Z)V

    .line 87
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabFrame;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 90
    invoke-virtual {p1, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 91
    invoke-virtual {p1, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 92
    invoke-virtual {p1, v1}, Landroid/view/View;->setClickable(Z)V

    .line 93
    invoke-virtual {p1, v2}, Landroid/view/View;->setDuplicateParentStateEnabled(Z)V

    .line 95
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabFrame;->removeAllViews()V

    .line 96
    invoke-virtual {p0, p1}, Lcom/google/tv/leftnavbar/TabFrame;->addView(Landroid/view/View;)V

    .line 97
    return-void
.end method

.method public configureNormal(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V
    .locals 1
    .parameter "icon"
    .parameter "text"

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/tv/leftnavbar/TabFrame;->markConfigured(Z)V

    .line 78
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/TabFrame;->getIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 79
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/TabFrame;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    return-void
.end method

.method public expand(Z)V
    .locals 2
    .parameter "expanded"

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/TabFrame;->mIsCustom:Z

    if-nez v0, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/TabFrame;->getTitle()Landroid/widget/TextView;

    move-result-object v1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 65
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/tv/leftnavbar/TabFrame;->setActivated(Z)V

    .line 66
    return-void

    .line 63
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public select(Z)V
    .locals 0
    .parameter "selected"

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 51
    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .parameter "selected"

    .prologue
    .line 44
    return-void
.end method
