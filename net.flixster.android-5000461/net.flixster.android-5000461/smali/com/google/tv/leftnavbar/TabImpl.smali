.class abstract Lcom/google/tv/leftnavbar/TabImpl;
.super Landroid/app/ActionBar$Tab;
.source "TabImpl.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private mCallback:Landroid/app/ActionBar$TabListener;

.field private final mContext:Landroid/content/Context;

.field private mCustomView:Landroid/view/View;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mPosition:I

.field private mTag:Ljava/lang/Object;

.field private mText:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/ActionBar$Tab;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/tv/leftnavbar/TabImpl;->mContext:Landroid/content/Context;

    .line 43
    return-void
.end method


# virtual methods
.method public getCallback()Landroid/app/ActionBar$TabListener;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mCallback:Landroid/app/ActionBar$TabListener;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCustomView()Landroid/view/View;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mCustomView:Landroid/view/View;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mPosition:I

    return v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mTag:Ljava/lang/Object;

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method hasCustomView()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mCustomView:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContentDescription(I)Landroid/app/ActionBar$Tab;
    .locals 1
    .parameter "arg0"

    .prologue
    .line 145
    const/4 v0, 0x0

    return-object v0
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;
    .locals 1
    .parameter "arg0"

    .prologue
    .line 151
    const/4 v0, 0x0

    return-object v0
.end method

.method public setCustomView(I)Landroid/app/ActionBar$Tab;
    .locals 2
    .parameter "layoutResId"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabImpl;->setCustomView(Landroid/view/View;)Landroid/app/ActionBar$Tab;

    move-result-object v0

    return-object v0
.end method

.method public setCustomView(Landroid/view/View;)Landroid/app/ActionBar$Tab;
    .locals 0
    .parameter "view"

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/tv/leftnavbar/TabImpl;->mCustomView:Landroid/view/View;

    .line 70
    return-object p0
.end method

.method public setIcon(I)Landroid/app/ActionBar$Tab;
    .locals 1
    .parameter "resId"

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabImpl;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/ActionBar$Tab;

    move-result-object v0

    return-object v0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/ActionBar$Tab;
    .locals 0
    .parameter "icon"

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/tv/leftnavbar/TabImpl;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 100
    return-object p0
.end method

.method public setPosition(I)V
    .locals 0
    .parameter "position"

    .prologue
    .line 89
    iput p1, p0, Lcom/google/tv/leftnavbar/TabImpl;->mPosition:I

    .line 90
    return-void
.end method

.method public setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;
    .locals 0
    .parameter "callback"

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/tv/leftnavbar/TabImpl;->mCallback:Landroid/app/ActionBar$TabListener;

    .line 59
    return-object p0
.end method

.method public setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;
    .locals 0
    .parameter "tag"

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/tv/leftnavbar/TabImpl;->mTag:Ljava/lang/Object;

    .line 53
    return-object p0
.end method

.method public setText(I)Landroid/app/ActionBar$Tab;
    .locals 1
    .parameter "resId"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabImpl;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v0

    return-object v0
.end method

.method public setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;
    .locals 0
    .parameter "text"

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/tv/leftnavbar/TabImpl;->mText:Ljava/lang/CharSequence;

    .line 111
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 132
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TabImpl;->mTag:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mTag:Ljava/lang/Object;

    .line 133
    .local v0, source:Ljava/lang/Object;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "Tab:"

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 132
    .end local v0           #source:Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TabImpl;->mText:Ljava/lang/CharSequence;

    goto :goto_0

    .line 133
    .restart local v0       #source:Ljava/lang/Object;
    :cond_1
    const-string v1, "<no id>"

    goto :goto_1
.end method
