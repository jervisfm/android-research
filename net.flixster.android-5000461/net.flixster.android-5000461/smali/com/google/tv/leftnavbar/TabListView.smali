.class public final Lcom/google/tv/leftnavbar/TabListView;
.super Landroid/widget/ListView;
.source "TabListView.java"


# instance fields
.field private mClearingFocus:Z

.field private mHighlighted:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method static synthetic access$0(Lcom/google/tv/leftnavbar/TabListView;)I
    .locals 1
    .parameter

    .prologue
    .line 31
    iget v0, p0, Lcom/google/tv/leftnavbar/TabListView;->mHighlighted:I

    return v0
.end method


# virtual methods
.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 3
    .parameter
    .parameter "direction"
    .parameter "focusableMode"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    iget v1, p0, Lcom/google/tv/leftnavbar/TabListView;->mHighlighted:I

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int v0, v1, v2

    .line 48
    .local v0, selectedIndex:I
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/16 v1, 0x11

    if-ne p2, v1, :cond_0

    .line 50
    iget v1, p0, Lcom/google/tv/leftnavbar/TabListView;->mHighlighted:I

    invoke-virtual {p0, v1}, Lcom/google/tv/leftnavbar/TabListView;->setSelection(I)V

    .line 51
    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ListView;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_0
.end method

.method public clearChildFocus(Landroid/view/View;)V
    .locals 1
    .parameter "child"

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/TabListView;->mClearingFocus:Z

    if-eqz v0, :cond_0

    .line 71
    invoke-super {p0, p1}, Landroid/widget/ListView;->clearChildFocus(Landroid/view/View;)V

    .line 81
    :goto_0
    return-void

    .line 73
    :cond_0
    new-instance v0, Lcom/google/tv/leftnavbar/TabListView$1;

    invoke-direct {v0, p0}, Lcom/google/tv/leftnavbar/TabListView$1;-><init>(Lcom/google/tv/leftnavbar/TabListView;)V

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TabListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public clearFocus()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/tv/leftnavbar/TabListView;->mClearingFocus:Z

    .line 60
    invoke-super {p0}, Landroid/widget/ListView;->clearFocus()V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/tv/leftnavbar/TabListView;->mClearingFocus:Z

    .line 62
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .parameter "canvas"

    .prologue
    .line 88
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 90
    .local v1, divider:Landroid/graphics/drawable/Drawable;
    if-nez v1, :cond_0

    .line 107
    :goto_0
    return-void

    .line 94
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 95
    .local v0, bounds:Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getPaddingLeft()I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 96
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 98
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getPaddingTop()I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 99
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getDividerHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 100
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 101
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 103
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getBottom()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getDividerHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 104
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getBottom()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TabListView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 105
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 106
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public setHighlighted(I)V
    .locals 0
    .parameter "index"

    .prologue
    .line 42
    iput p1, p0, Lcom/google/tv/leftnavbar/TabListView;->mHighlighted:I

    .line 43
    return-void
.end method
