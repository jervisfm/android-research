.class public final Lcom/google/tv/leftnavbar/TitleBarView;
.super Landroid/widget/RelativeLayout;
.source "TitleBarView.java"


# instance fields
.field private mAnimationsEnabled:Z

.field private mCircularProgress:Landroid/widget/ProgressBar;

.field private mHorizontalProgress:Landroid/widget/ProgressBar;

.field private mIsLegacy:Z

.field private mLeftIcon:Landroid/widget/ImageView;

.field private mRightIcon:Landroid/widget/ImageView;

.field private mSubtitle:Landroid/widget/TextView;

.field private mSubtitleResource:I

.field private mTitle:Landroid/widget/TextView;

.field private mTitleResource:I

.field private final mVisibilityController:Lcom/google/tv/leftnavbar/VisibilityController;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 55
    invoke-direct {p0, p1, p2, v4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    new-instance v1, Lcom/google/tv/leftnavbar/VisibilityController;

    invoke-direct {v1, p0}, Lcom/google/tv/leftnavbar/VisibilityController;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mVisibilityController:Lcom/google/tv/leftnavbar/VisibilityController;

    .line 57
    new-array v1, v2, [I

    fill-array-data v1, :array_0

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 59
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mIsLegacy:Z

    .line 60
    iget-boolean v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mIsLegacy:Z

    if-eqz v1, :cond_0

    .line 61
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mTitleResource:I

    .line 69
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 70
    return-void

    .line 63
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 64
    const/4 v1, 0x0

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    .line 65
    const v3, 0x10102ce

    .line 64
    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 66
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mTitleResource:I

    .line 67
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mSubtitleResource:I

    goto :goto_0

    .line 57
    nop

    :array_0
    .array-data 0x4
        0x5bt 0x0t 0x1t 0x1t
        0xedt 0x1t 0x1t 0x1t
    .end array-data

    .line 64
    :array_1
    .array-data 0x4
        0xf8t 0x2t 0x1t 0x1t
        0xf9t 0x2t 0x1t 0x1t
    .end array-data
.end method

.method private disableSubtitle()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mSubtitle:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/tv/leftnavbar/TitleBarView;->removeFromParent(Landroid/view/View;)V

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mSubtitle:Landroid/widget/TextView;

    .line 210
    return-void
.end method

.method private static removeFromParent(Landroid/view/View;)V
    .locals 1
    .parameter "view"

    .prologue
    .line 213
    if-nez p0, :cond_1

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 217
    .local v0, parent:Landroid/view/ViewParent;
    if-eqz v0, :cond_0

    .line 218
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0           #parent:Landroid/view/ViewParent;
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private setIcon(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;I)V
    .locals 1
    .parameter "view"
    .parameter "drawable"
    .parameter "alpha"

    .prologue
    .line 122
    if-nez p1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 125
    :cond_0
    if-eqz p2, :cond_1

    .line 126
    invoke-virtual {p2, p3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 127
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 128
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 130
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setTextStyle(Landroid/widget/TextView;I)V
    .locals 1
    .parameter "view"
    .parameter "style"

    .prologue
    .line 100
    if-eqz p2, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TitleBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 103
    :cond_0
    return-void
.end method


# virtual methods
.method public disableCircularProgress()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mCircularProgress:Landroid/widget/ProgressBar;

    invoke-static {v0}, Lcom/google/tv/leftnavbar/TitleBarView;->removeFromParent(Landroid/view/View;)V

    .line 204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mCircularProgress:Landroid/widget/ProgressBar;

    .line 205
    return-void
.end method

.method public disableHorizontalProgress()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    invoke-static {v0}, Lcom/google/tv/leftnavbar/TitleBarView;->removeFromParent(Landroid/view/View;)V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    .line 200
    return-void
.end method

.method public disableLeftIcon()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mLeftIcon:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/tv/leftnavbar/TitleBarView;->removeFromParent(Landroid/view/View;)V

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mLeftIcon:Landroid/widget/ImageView;

    .line 190
    return-void
.end method

.method public disableRightIcon()V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mRightIcon:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/tv/leftnavbar/TitleBarView;->removeFromParent(Landroid/view/View;)V

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mRightIcon:Landroid/widget/ImageView;

    .line 195
    return-void
.end method

.method public getApparentHeight()I
    .locals 2

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TitleBarView;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TitleBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public isHorizontalProgressVisible()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mVisibilityController:Lcom/google/tv/leftnavbar/VisibilityController;

    invoke-virtual {v0}, Lcom/google/tv/leftnavbar/VisibilityController;->isVisible()Z

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 74
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 75
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TitleBarView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TitleBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03008a

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 79
    :cond_0
    const v0, 0x7f07006f

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mTitle:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0702c9

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mSubtitle:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0702c8

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mLeftIcon:Landroid/widget/ImageView;

    .line 82
    const v0, 0x7f0702cb

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mRightIcon:Landroid/widget/ImageView;

    .line 83
    const v0, 0x7f0702ca

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mCircularProgress:Landroid/widget/ProgressBar;

    .line 84
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mCircularProgress:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mCircularProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 87
    :cond_1
    const v0, 0x7f0702cc

    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    .line 88
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mIsLegacy:Z

    if-eqz v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mTitle:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mTitleResource:I

    invoke-direct {p0, v0, v1}, Lcom/google/tv/leftnavbar/TitleBarView;->setTextStyle(Landroid/widget/TextView;I)V

    .line 90
    invoke-direct {p0}, Lcom/google/tv/leftnavbar/TitleBarView;->disableSubtitle()V

    .line 97
    :goto_0
    return-void

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mTitle:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mTitleResource:I

    invoke-direct {p0, v0, v1}, Lcom/google/tv/leftnavbar/TitleBarView;->setTextStyle(Landroid/widget/TextView;I)V

    .line 93
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mSubtitle:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mSubtitleResource:I

    invoke-direct {p0, v0, v1}, Lcom/google/tv/leftnavbar/TitleBarView;->setTextStyle(Landroid/widget/TextView;I)V

    .line 94
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TitleBarView;->disableLeftIcon()V

    .line 95
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/TitleBarView;->disableRightIcon()V

    goto :goto_0
.end method

.method public setAnimationsEnabled(Z)V
    .locals 0
    .parameter "enabled"

    .prologue
    .line 239
    iput-boolean p1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mAnimationsEnabled:Z

    .line 240
    return-void
.end method

.method public setCircularProgress(I)V
    .locals 2
    .parameter "value"

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mCircularProgress:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 173
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 179
    :pswitch_0
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mCircularProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 175
    :pswitch_1
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mCircularProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setHorizontalProgress(I)V
    .locals 2
    .parameter "value"

    .prologue
    const/4 v1, 0x0

    .line 135
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 156
    if-ltz p1, :cond_2

    const/16 v0, 0x2710

    if-gt p1, v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    add-int/lit8 v1, p1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 140
    :pswitch_0
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 144
    :pswitch_1
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 148
    :pswitch_2
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 152
    :pswitch_3
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 158
    :cond_2
    const/16 v0, 0x4e20

    if-gt v0, p1, :cond_0

    const/16 v0, 0x7530

    if-gt p1, v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mHorizontalProgress:Landroid/widget/ProgressBar;

    add-int/lit16 v1, p1, -0x4e20

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    goto :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setLeftIcon(Landroid/graphics/drawable/Drawable;I)V
    .locals 1
    .parameter "drawable"
    .parameter "alpha"

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mLeftIcon:Landroid/widget/ImageView;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/tv/leftnavbar/TitleBarView;->setIcon(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;I)V

    .line 115
    return-void
.end method

.method public setProgressVisible(Z)V
    .locals 1
    .parameter "visible"

    .prologue
    .line 255
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/tv/leftnavbar/TitleBarView;->setCircularProgress(I)V

    .line 256
    return-void

    .line 255
    :cond_0
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public setRightIcon(Landroid/graphics/drawable/Drawable;I)V
    .locals 1
    .parameter "drawable"
    .parameter "alpha"

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mRightIcon:Landroid/widget/ImageView;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/tv/leftnavbar/TitleBarView;->setIcon(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;I)V

    .line 119
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 2
    .parameter "text"

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mSubtitle:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    return-void

    .line 231
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .parameter "text"

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    return-void
.end method

.method public setTitleColor(I)V
    .locals 1
    .parameter "color"

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 111
    return-void
.end method

.method public setVisible(ZZ)V
    .locals 2
    .parameter "visible"
    .parameter "animated"

    .prologue
    .line 243
    iget-object v1, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mVisibilityController:Lcom/google/tv/leftnavbar/VisibilityController;

    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/TitleBarView;->mAnimationsEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/google/tv/leftnavbar/VisibilityController;->setVisible(ZZ)Z

    .line 244
    return-void

    .line 243
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
