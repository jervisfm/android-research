.class Lcom/google/tv/leftnavbar/VisibilityController$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "VisibilityController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/tv/leftnavbar/VisibilityController;->setVisible(ZZ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/tv/leftnavbar/VisibilityController;

.field private final synthetic val$visible:Z


# direct methods
.method constructor <init>(Lcom/google/tv/leftnavbar/VisibilityController;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/tv/leftnavbar/VisibilityController$1;->this$0:Lcom/google/tv/leftnavbar/VisibilityController;

    iput-boolean p2, p0, Lcom/google/tv/leftnavbar/VisibilityController$1;->val$visible:Z

    .line 48
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .parameter "animator"

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/VisibilityController$1;->val$visible:Z

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/tv/leftnavbar/VisibilityController$1;->this$0:Lcom/google/tv/leftnavbar/VisibilityController;

    const/4 v1, 0x0

    #calls: Lcom/google/tv/leftnavbar/VisibilityController;->setViewVisible(Z)V
    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/VisibilityController;->access$0(Lcom/google/tv/leftnavbar/VisibilityController;Z)V

    .line 62
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .parameter "animator"

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/VisibilityController$1;->val$visible:Z

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/tv/leftnavbar/VisibilityController$1;->this$0:Lcom/google/tv/leftnavbar/VisibilityController;

    const/4 v1, 0x1

    #calls: Lcom/google/tv/leftnavbar/VisibilityController;->setViewVisible(Z)V
    invoke-static {v0, v1}, Lcom/google/tv/leftnavbar/VisibilityController;->access$0(Lcom/google/tv/leftnavbar/VisibilityController;Z)V

    .line 55
    :cond_0
    return-void
.end method
