.class public final Lcom/google/tv/leftnavbar/VisibilityController;
.super Ljava/lang/Object;
.source "VisibilityController.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# instance fields
.field private final mAnimationDuration:I

.field private final mView:Landroid/view/View;

.field private mVisible:Z


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 2
    .parameter "view"

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/tv/leftnavbar/VisibilityController;->mView:Landroid/view/View;

    .line 33
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/tv/leftnavbar/VisibilityController;->mAnimationDuration:I

    .line 34
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/tv/leftnavbar/VisibilityController;->mVisible:Z

    .line 35
    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/google/tv/leftnavbar/VisibilityController;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/VisibilityController;->setViewVisible(Z)V

    return-void
.end method

.method private setViewVisible(Z)V
    .locals 2
    .parameter "visible"

    .prologue
    .line 71
    iget-object v1, p0, Lcom/google/tv/leftnavbar/VisibilityController;->mView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 72
    return-void

    .line 71
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method isVisible()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/tv/leftnavbar/VisibilityController;->mVisible:Z

    return v0
.end method

.method setVisible(ZZ)Z
    .locals 4
    .parameter "visible"
    .parameter "animated"

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/tv/leftnavbar/VisibilityController;->isVisible()Z

    move-result v1

    if-ne v1, p1, :cond_0

    .line 43
    const/4 v1, 0x0

    .line 67
    :goto_0
    return v1

    .line 45
    :cond_0
    iput-boolean p1, p0, Lcom/google/tv/leftnavbar/VisibilityController;->mVisible:Z

    .line 46
    if-eqz p2, :cond_2

    .line 47
    if-eqz p1, :cond_1

    const/high16 v0, 0x3f80

    .line 48
    .local v0, toAlpha:F
    :goto_1
    iget-object v1, p0, Lcom/google/tv/leftnavbar/VisibilityController;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v2, p0, Lcom/google/tv/leftnavbar/VisibilityController;->mAnimationDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/google/tv/leftnavbar/VisibilityController$1;

    invoke-direct {v2, p0, p1}, Lcom/google/tv/leftnavbar/VisibilityController$1;-><init>(Lcom/google/tv/leftnavbar/VisibilityController;Z)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 67
    .end local v0           #toAlpha:F
    :goto_2
    const/4 v1, 0x1

    goto :goto_0

    .line 47
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 65
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/tv/leftnavbar/VisibilityController;->setViewVisible(Z)V

    goto :goto_2
.end method
