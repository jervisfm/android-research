.class Lcom/jayway/android/robotium/solo/ActivityUtils;
.super Ljava/lang/Object;
.source "ActivityUtils.java"


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field private final MINISLEEP:I

.field private activity:Landroid/app/Activity;

.field private activityList:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private activityMonitor:Landroid/app/Instrumentation$ActivityMonitor;

.field private final inst:Landroid/app/Instrumentation;

.field private final sleeper:Lcom/jayway/android/robotium/solo/Sleeper;


# direct methods
.method public constructor <init>(Landroid/app/Instrumentation;Landroid/app/Activity;Lcom/jayway/android/robotium/solo/Sleeper;)V
    .locals 1
    .parameter "inst"
    .parameter "activity"
    .parameter "sleeper"

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "Robotium"

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->LOG_TAG:Ljava/lang/String;

    .line 29
    const/16 v0, 0x64

    iput v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->MINISLEEP:I

    .line 41
    iput-object p1, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->inst:Landroid/app/Instrumentation;

    .line 42
    iput-object p2, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activity:Landroid/app/Activity;

    .line 43
    iput-object p3, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    .line 44
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityList:Ljava/util/LinkedHashSet;

    .line 45
    invoke-direct {p0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->setupActivityMonitor()V

    .line 46
    return-void
.end method

.method private finishActivity(Landroid/app/Activity;)V
    .locals 1
    .parameter "activity"

    .prologue
    .line 251
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    :goto_0
    return-void

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, e:Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private setupActivityMonitor()V
    .locals 5

    .prologue
    .line 70
    const/4 v1, 0x0

    .line 71
    .local v1, filter:Landroid/content/IntentFilter;
    :try_start_0
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->inst:Landroid/app/Instrumentation;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/app/Instrumentation;->addMonitor(Landroid/content/IntentFilter;Landroid/app/Instrumentation$ActivityResult;Z)Landroid/app/Instrumentation$ActivityMonitor;

    move-result-object v2

    iput-object v2, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityMonitor:Landroid/app/Instrumentation$ActivityMonitor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private final waitForActivityIfNotAvailable()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activity:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityMonitor:Landroid/app/Instrumentation$ActivityMonitor;

    if-eqz v0, :cond_0

    .line 120
    :goto_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityMonitor:Landroid/app/Instrumentation$ActivityMonitor;

    invoke-virtual {v0}, Landroid/app/Instrumentation$ActivityMonitor;->getLastActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/Sleeper;->sleepMini()V

    goto :goto_0

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/Sleeper;->sleepMini()V

    .line 126
    invoke-direct {p0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->setupActivityMonitor()V

    .line 127
    invoke-direct {p0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->waitForActivityIfNotAvailable()V

    .line 130
    :cond_1
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 210
    :try_start_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityMonitor:Landroid/app/Instrumentation$ActivityMonitor;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->inst:Landroid/app/Instrumentation;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityMonitor:Landroid/app/Instrumentation$ActivityMonitor;

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->removeMonitor(Landroid/app/Instrumentation$ActivityMonitor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :cond_0
    :goto_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 215
    return-void

    .line 213
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public finishOpenedActivities()V
    .locals 4

    .prologue
    const/16 v3, 0x64

    .line 224
    invoke-virtual {p0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getAllOpenedActivities()Ljava/util/ArrayList;

    move-result-object v0

    .line 226
    .local v0, activitiesOpened:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/Activity;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .local v1, i:I
    :goto_0
    if-ltz v1, :cond_0

    .line 227
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v2, v3}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep(I)V

    .line 228
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-direct {p0, v2}, Lcom/jayway/android/robotium/solo/ActivityUtils;->finishActivity(Landroid/app/Activity;)V

    .line 226
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 231
    :cond_0
    invoke-virtual {p0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jayway/android/robotium/solo/ActivityUtils;->finishActivity(Landroid/app/Activity;)V

    .line 232
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v2}, Lcom/jayway/android/robotium/solo/Sleeper;->sleepMini()V

    .line 234
    :try_start_0
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->inst:Landroid/app/Instrumentation;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/app/Instrumentation;->sendKeyDownUpSync(I)V

    .line 235
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep(I)V

    .line 236
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->inst:Landroid/app/Instrumentation;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/app/Instrumentation;->sendKeyDownUpSync(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    :goto_1
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityList:Ljava/util/LinkedHashSet;

    invoke-virtual {v2}, Ljava/util/LinkedHashSet;->clear()V

    .line 241
    return-void

    .line 237
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public getActivityMonitor()Landroid/app/Instrumentation$ActivityMonitor;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityMonitor:Landroid/app/Instrumentation$ActivityMonitor;

    return-object v0
.end method

.method public getAllOpenedActivities()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityList:Ljava/util/LinkedHashSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getCurrentActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getCurrentActivity(Z)Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentActivity(Z)Landroid/app/Activity;
    .locals 2
    .parameter "shouldSleepFirst"

    .prologue
    .line 141
    if-eqz p1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 144
    :cond_0
    invoke-direct {p0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->waitForActivityIfNotAvailable()V

    .line 146
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityMonitor:Landroid/app/Instrumentation$ActivityMonitor;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityMonitor:Landroid/app/Instrumentation$ActivityMonitor;

    invoke-virtual {v0}, Landroid/app/Instrumentation$ActivityMonitor;->getLastActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityMonitor:Landroid/app/Instrumentation$ActivityMonitor;

    invoke-virtual {v0}, Landroid/app/Instrumentation$ActivityMonitor;->getLastActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activity:Landroid/app/Activity;

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activityList:Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2
    .parameter "resId"

    .prologue
    .line 197
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getCurrentActivity(Z)Landroid/app/Activity;

    move-result-object v0

    .line 198
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public goBackToActivity(Ljava/lang/String;)V
    .locals 6
    .parameter "name"

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getAllOpenedActivities()Ljava/util/ArrayList;

    move-result-object v0

    .line 165
    .local v0, activitiesOpened:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/Activity;>;"
    const/4 v1, 0x0

    .line 166
    .local v1, found:Z
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 167
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 168
    const/4 v1, 0x1

    .line 172
    :cond_0
    if-eqz v1, :cond_2

    .line 173
    :goto_1
    invoke-virtual {p0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 176
    :try_start_0
    iget-object v3, p0, Lcom/jayway/android/robotium/solo/ActivityUtils;->inst:Landroid/app/Instrumentation;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/app/Instrumentation;->sendKeyDownUpSync(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 177
    :catch_0
    move-exception v3

    goto :goto_1

    .line 166
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 181
    :cond_2
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 182
    const-string v4, "Robotium"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Activity priorly opened: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 183
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No Activity named "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has been priorly opened"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    .line 185
    :cond_4
    return-void
.end method

.method public setActivityOrientation(I)V
    .locals 1
    .parameter "orientation"

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    .line 97
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 98
    return-void
.end method
