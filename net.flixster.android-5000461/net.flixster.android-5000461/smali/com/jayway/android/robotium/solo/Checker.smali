.class Lcom/jayway/android/robotium/solo/Checker;
.super Ljava/lang/Object;
.source "Checker.java"


# instance fields
.field private final viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

.field private final waiter:Lcom/jayway/android/robotium/solo/Waiter;


# direct methods
.method public constructor <init>(Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Waiter;)V
    .locals 0
    .parameter "viewFetcher"
    .parameter "waiter"

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/jayway/android/robotium/solo/Checker;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    .line 31
    iput-object p2, p0, Lcom/jayway/android/robotium/solo/Checker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    .line 32
    return-void
.end method


# virtual methods
.method public isButtonChecked(Ljava/lang/Class;I)Z
    .locals 1
    .parameter
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/CompoundButton;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;I)Z"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, expectedClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Checker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v0, p2, p1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public isButtonChecked(Ljava/lang/Class;Ljava/lang/String;)Z
    .locals 7
    .parameter
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/CompoundButton;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, expectedClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v3, 0x0

    .line 58
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Checker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-wide/16 v5, 0x2710

    invoke-virtual {v4, p2, v3, v5, v6}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJ)Z

    .line 59
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Checker;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    invoke-virtual {v4, p1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v2

    .line 60
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    .line 61
    .local v0, button:Landroid/widget/CompoundButton;,"TT;"
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 62
    const/4 v3, 0x1

    .line 64
    .end local v0           #button:Landroid/widget/CompoundButton;,"TT;"
    :cond_1
    return v3
.end method

.method public isCheckedTextChecked(Ljava/lang/String;)Z
    .locals 7
    .parameter "text"

    .prologue
    const/4 v3, 0x0

    .line 77
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Checker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-wide/16 v5, 0x2710

    invoke-virtual {v4, p1, v3, v5, v6}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJ)Z

    .line 78
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Checker;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v5, Landroid/widget/CheckedTextView;

    invoke-virtual {v4, v5}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v2

    .line 79
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/CheckedTextView;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 80
    .local v0, checkedText:Landroid/widget/CheckedTextView;
    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 81
    const/4 v3, 0x1

    .line 83
    .end local v0           #checkedText:Landroid/widget/CheckedTextView;
    :cond_1
    return v3
.end method

.method public isSpinnerTextSelected(ILjava/lang/String;)Z
    .locals 5
    .parameter "spinnerIndex"
    .parameter "text"

    .prologue
    const/4 v2, 0x0

    .line 115
    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Checker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v4, Landroid/widget/Spinner;

    invoke-virtual {v3, p1, v4}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 117
    .local v0, spinner:Landroid/widget/Spinner;
    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 118
    .local v1, textView:Landroid/widget/TextView;
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 119
    const/4 v2, 0x1

    .line 121
    :cond_0
    return v2
.end method

.method public isSpinnerTextSelected(Ljava/lang/String;)Z
    .locals 5
    .parameter "text"

    .prologue
    const/4 v2, 0x0

    .line 96
    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Checker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v4, Landroid/widget/Spinner;

    invoke-virtual {v3, v2, v4}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    .line 98
    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Checker;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v4, Landroid/widget/Spinner;

    invoke-virtual {v3, v4}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    .line 99
    .local v1, spinnerList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/Spinner;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 100
    invoke-virtual {p0, v0, p1}, Lcom/jayway/android/robotium/solo/Checker;->isSpinnerTextSelected(ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 101
    const/4 v2, 0x1

    .line 103
    :cond_0
    return v2

    .line 99
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
