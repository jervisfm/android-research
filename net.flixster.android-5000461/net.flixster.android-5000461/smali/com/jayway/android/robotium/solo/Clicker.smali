.class Lcom/jayway/android/robotium/solo/Clicker;
.super Ljava/lang/Object;
.source "Clicker.java"


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field private final MINISLEEP:I

.field private final TIMEOUT:I

.field private final inst:Landroid/app/Instrumentation;

.field private final robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

.field private final scroller:Lcom/jayway/android/robotium/solo/Scroller;

.field private final sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

.field uniqueTextViews:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

.field private final waiter:Lcom/jayway/android/robotium/solo/Waiter;


# direct methods
.method public constructor <init>(Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Scroller;Lcom/jayway/android/robotium/solo/RobotiumUtils;Landroid/app/Instrumentation;Lcom/jayway/android/robotium/solo/Sleeper;Lcom/jayway/android/robotium/solo/Waiter;)V
    .locals 1
    .parameter "viewFetcher"
    .parameter "scroller"
    .parameter "robotiumUtils"
    .parameter "inst"
    .parameter "sleeper"
    .parameter "waiter"

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, "Robotium"

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->LOG_TAG:Ljava/lang/String;

    .line 34
    const/16 v0, 0x2710

    iput v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->TIMEOUT:I

    .line 35
    const/16 v0, 0x64

    iput v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->MINISLEEP:I

    .line 53
    iput-object p1, p0, Lcom/jayway/android/robotium/solo/Clicker;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    .line 54
    iput-object p2, p0, Lcom/jayway/android/robotium/solo/Clicker;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    .line 55
    iput-object p3, p0, Lcom/jayway/android/robotium/solo/Clicker;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    .line 56
    iput-object p4, p0, Lcom/jayway/android/robotium/solo/Clicker;->inst:Landroid/app/Instrumentation;

    .line 57
    iput-object p5, p0, Lcom/jayway/android/robotium/solo/Clicker;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    .line 58
    iput-object p6, p0, Lcom/jayway/android/robotium/solo/Clicker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    .line 59
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->uniqueTextViews:Ljava/util/Set;

    .line 60
    return-void
.end method


# virtual methods
.method public clickInList(I)Ljava/util/ArrayList;
    .locals 1
    .parameter "line"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 336
    invoke-virtual {p0, p1, v0, v0, v0}, Lcom/jayway/android/robotium/solo/Clicker;->clickInList(IIZI)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public clickInList(IIZI)Ljava/util/ArrayList;
    .locals 5
    .parameter "line"
    .parameter "index"
    .parameter "longClick"
    .parameter "time"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZI)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 349
    add-int/lit8 p1, p1, -0x1

    .line 350
    if-gez p1, :cond_0

    .line 351
    const/4 p1, 0x0

    .line 353
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 354
    .local v2, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Clicker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v4, Landroid/widget/ListView;

    invoke-virtual {v3, p2, v4}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 355
    .local v0, listView:Landroid/widget/ListView;
    if-nez v0, :cond_1

    .line 356
    const-string v3, "ListView is null!"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    .line 358
    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 359
    .local v1, view:Landroid/view/View;
    if-eqz v1, :cond_2

    .line 360
    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Clicker;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getViews(Landroid/view/View;Z)Ljava/util/ArrayList;

    move-result-object v2

    .line 361
    invoke-static {v2}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->removeInvisibleViews(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 362
    invoke-virtual {p0, v1, p3, p4}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(Landroid/view/View;ZI)V

    .line 364
    :cond_2
    const-class v3, Landroid/widget/TextView;

    invoke-static {v3, v2}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->filterViews(Ljava/lang/Class;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    return-object v3
.end method

.method public clickLongOnScreen(FFI)V
    .locals 10
    .parameter "x"
    .parameter "y"
    .parameter "time"

    .prologue
    .line 96
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 97
    .local v0, downTime:J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 98
    .local v2, eventTime:J
    const/4 v4, 0x0

    const/4 v7, 0x0

    move v5, p1

    move v6, p2

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v9

    .line 100
    .local v9, event:Landroid/view/MotionEvent;
    :try_start_0
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Clicker;->inst:Landroid/app/Instrumentation;

    invoke-virtual {v4, v9}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 105
    const/4 v4, 0x2

    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v5, p1

    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    add-float/2addr v6, p2

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v9

    .line 108
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Clicker;->inst:Landroid/app/Instrumentation;

    invoke-virtual {v4, v9}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 109
    if-lez p3, :cond_0

    .line 110
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Clicker;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v4, p3}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep(I)V

    .line 114
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 115
    const/4 v4, 0x1

    const/4 v7, 0x0

    move v5, p1

    move v6, p2

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v9

    .line 116
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Clicker;->inst:Landroid/app/Instrumentation;

    invoke-virtual {v4, v9}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 117
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Clicker;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v4}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 119
    return-void

    .line 101
    :catch_0
    move-exception v8

    .line 102
    .local v8, e:Ljava/lang/SecurityException;
    const-string v4, "Click can not be completed! Something is in the way e.g. the keyboard."

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    goto :goto_0

    .line 112
    .end local v8           #e:Ljava/lang/SecurityException;
    :cond_0
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Clicker;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x4020

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep(I)V

    goto :goto_1
.end method

.method public clickLongOnTextAndPress(Ljava/lang/String;I)V
    .locals 9
    .parameter "text"
    .parameter "index"

    .prologue
    const/16 v8, 0x14

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 172
    move-object v0, p0

    move-object v1, p1

    move v4, v2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    .line 174
    :try_start_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->inst:Landroid/app/Instrumentation;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendKeyDownUpSync(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_0
    const/4 v7, 0x0

    .local v7, i:I
    :goto_1
    if-ge v7, p2, :cond_0

    .line 180
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/Sleeper;->sleepMini()V

    .line 181
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->inst:Landroid/app/Instrumentation;

    invoke-virtual {v0, v8}, Landroid/app/Instrumentation;->sendKeyDownUpSync(I)V

    .line 178
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 175
    .end local v7           #i:I
    :catch_0
    move-exception v6

    .line 176
    .local v6, e:Ljava/lang/SecurityException;
    const-string v0, "Can not press the context menu!"

    invoke-static {v0, v3}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    goto :goto_0

    .line 183
    .end local v6           #e:Ljava/lang/SecurityException;
    .restart local v7       #i:I
    :cond_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->inst:Landroid/app/Instrumentation;

    const/16 v1, 0x42

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendKeyDownUpSync(I)V

    .line 184
    return-void
.end method

.method public clickOn(Ljava/lang/Class;I)V
    .locals 1
    .parameter
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 323
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v0, p2, p1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(Landroid/view/View;)V

    .line 324
    return-void
.end method

.method public clickOn(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 12
    .parameter
    .parameter "nameRegex"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/TextView;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 292
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v8

    .line 293
    .local v8, pattern:Ljava/util/regex/Pattern;
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-wide/16 v3, 0x2710

    move-object v1, p2

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJZZ)Z

    .line 294
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v11

    .line 295
    .local v11, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    invoke-static {v11}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->removeInvisibleViews(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v11

    .line 296
    const/4 v10, 0x0

    .line 297
    .local v10, viewToClick:Landroid/widget/TextView;,"TT;"
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 298
    .local v9, view:Landroid/widget/TextView;,"TT;"
    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    move-object v10, v9

    .line 300
    invoke-virtual {v10}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    .end local v9           #view:Landroid/widget/TextView;,"TT;"
    :cond_1
    if-eqz v10, :cond_2

    .line 305
    invoke-virtual {p0, v10}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(Landroid/view/View;)V

    .line 314
    :goto_0
    return-void

    .line 306
    :cond_2
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    invoke-virtual {v0, v2}, Lcom/jayway/android/robotium/solo/Scroller;->scroll(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 307
    invoke-virtual {p0, p1, p2}, Lcom/jayway/android/robotium/solo/Clicker;->clickOn(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 309
    :cond_3
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 310
    .restart local v9       #view:Landroid/widget/TextView;,"TT;"
    const-string v0, "Robotium"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " not found. Have found: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 312
    .end local v9           #view:Landroid/widget/TextView;,"TT;"
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with the text: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not found!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public clickOnMenuItem(Ljava/lang/String;)V
    .locals 7
    .parameter "text"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 194
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 196
    :try_start_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    const/16 v1, 0x52

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->sendKeyCode(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v2

    .line 200
    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    .line 201
    return-void

    .line 197
    :catch_0
    move-exception v6

    .line 198
    .local v6, e:Ljava/lang/SecurityException;
    const-string v0, "Can not open the menu!"

    invoke-static {v0, v2}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public clickOnMenuItem(Ljava/lang/String;Z)V
    .locals 13
    .parameter "text"
    .parameter "subMenu"

    .prologue
    .line 213
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 214
    const/4 v8, 0x0

    .line 215
    .local v8, textMore:Landroid/widget/TextView;
    const/4 v0, 0x2

    new-array v11, v0, [I

    .line 216
    .local v11, xy:[I
    const/4 v10, 0x0

    .line 217
    .local v10, x:I
    const/4 v12, 0x0

    .line 220
    .local v12, y:I
    :try_start_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    const/16 v1, 0x52

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->sendKeyCode(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_0
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/4 v2, 0x1

    const-wide/16 v3, 0x5dc

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 225
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Clicker;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 226
    .local v9, textView:Landroid/widget/TextView;
    const/4 v0, 0x0

    aget v10, v11, v0

    .line 227
    const/4 v0, 0x1

    aget v12, v11, v0

    .line 228
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 230
    const/4 v0, 0x0

    aget v0, v11, v0

    if-gt v0, v10, :cond_1

    const/4 v0, 0x1

    aget v0, v11, v0

    if-le v0, v12, :cond_0

    .line 231
    :cond_1
    move-object v8, v9

    goto :goto_1

    .line 221
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v9           #textView:Landroid/widget/TextView;
    :catch_0
    move-exception v6

    .line 222
    .local v6, e:Ljava/lang/SecurityException;
    const-string v0, "Can not open the menu!"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    goto :goto_0

    .line 234
    .end local v6           #e:Ljava/lang/SecurityException;
    :cond_2
    if-eqz v8, :cond_3

    .line 235
    invoke-virtual {p0, v8}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(Landroid/view/View;)V

    .line 237
    :cond_3
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    .line 238
    return-void
.end method

.method public clickOnScreen(FF)V
    .locals 11
    .parameter "x"
    .parameter "y"

    .prologue
    .line 71
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 72
    .local v0, downTime:J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 73
    .local v2, eventTime:J
    const/4 v4, 0x0

    const/4 v7, 0x0

    move v5, p1

    move v6, p2

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v9

    .line 75
    .local v9, event:Landroid/view/MotionEvent;
    const/4 v4, 0x1

    const/4 v7, 0x0

    move v5, p1

    move v6, p2

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v10

    .line 78
    .local v10, event2:Landroid/view/MotionEvent;
    :try_start_0
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Clicker;->inst:Landroid/app/Instrumentation;

    invoke-virtual {v4, v9}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 79
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Clicker;->inst:Landroid/app/Instrumentation;

    invoke-virtual {v4, v10}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 80
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Clicker;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    const/16 v5, 0x64

    invoke-virtual {v4, v5}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v8

    .line 82
    .local v8, e:Ljava/lang/SecurityException;
    const-string v4, "Click can not be completed!"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public clickOnScreen(Landroid/view/View;)V
    .locals 1
    .parameter "view"

    .prologue
    const/4 v0, 0x0

    .line 130
    invoke-virtual {p0, p1, v0, v0}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(Landroid/view/View;ZI)V

    .line 131
    return-void
.end method

.method public clickOnScreen(Landroid/view/View;ZI)V
    .locals 8
    .parameter "view"
    .parameter "longClick"
    .parameter "time"

    .prologue
    const/4 v6, 0x0

    const/high16 v7, 0x4000

    .line 143
    if-nez p1, :cond_0

    .line 144
    const-string v5, "View is null and can therefore not be clicked!"

    invoke-static {v5, v6}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    .line 145
    :cond_0
    const/4 v5, 0x2

    new-array v3, v5, [I

    .line 147
    .local v3, xy:[I
    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 149
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 150
    .local v1, viewWidth:I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 151
    .local v0, viewHeight:I
    aget v5, v3, v6

    int-to-float v5, v5

    int-to-float v6, v1

    div-float/2addr v6, v7

    add-float v2, v5, v6

    .line 152
    .local v2, x:F
    const/4 v5, 0x1

    aget v5, v3, v5

    int-to-float v5, v5

    int-to-float v6, v0

    div-float/2addr v6, v7

    add-float v4, v5, v6

    .line 154
    .local v4, y:F
    if-eqz p2, :cond_1

    .line 155
    invoke-virtual {p0, v2, v4, p3}, Lcom/jayway/android/robotium/solo/Clicker;->clickLongOnScreen(FFI)V

    .line 158
    :goto_0
    return-void

    .line 157
    :cond_1
    invoke-virtual {p0, v2, v4}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(FF)V

    goto :goto_0
.end method

.method public clickOnText(Ljava/lang/String;ZIZI)V
    .locals 13
    .parameter "regex"
    .parameter "longClick"
    .parameter "match"
    .parameter "scroll"
    .parameter "time"

    .prologue
    .line 252
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Clicker;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/4 v3, 0x0

    const-wide/16 v4, 0x2710

    const/4 v7, 0x1

    move-object v2, p1

    move/from16 v6, p4

    invoke-virtual/range {v1 .. v7}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJZZ)Z

    .line 253
    const/4 v11, 0x0

    .line 254
    .local v11, textToClick:Landroid/widget/TextView;
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Clicker;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v2, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v8

    .line 255
    .local v8, allTextViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/TextView;>;"
    invoke-static {v8}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->removeInvisibleViews(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    .line 256
    if-nez p3, :cond_0

    .line 257
    const/16 p3, 0x1

    .line 259
    :cond_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, i$:Ljava/util/Iterator;
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 260
    .local v12, textView:Landroid/widget/TextView;
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Clicker;->uniqueTextViews:Ljava/util/Set;

    invoke-static {p1, v12, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->checkAndGetMatches(Ljava/lang/String;Landroid/widget/TextView;Ljava/util/Set;)I

    move-result v1

    move/from16 v0, p3

    if-ne v1, v0, :cond_1

    .line 261
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Clicker;->uniqueTextViews:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 262
    move-object v11, v12

    .line 266
    .end local v12           #textView:Landroid/widget/TextView;
    :cond_2
    if-eqz v11, :cond_3

    .line 267
    move/from16 v0, p5

    invoke-virtual {p0, v11, p2, v0}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(Landroid/view/View;ZI)V

    .line 282
    :goto_0
    return-void

    .line 268
    :cond_3
    if-eqz p4, :cond_4

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Clicker;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/jayway/android/robotium/solo/Scroller;->scroll(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 269
    invoke-virtual/range {p0 .. p5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    goto :goto_0

    .line 271
    :cond_4
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Clicker;->uniqueTextViews:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v10

    .line 272
    .local v10, sizeOfUniqueTextViews:I
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Clicker;->uniqueTextViews:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 273
    if-lez v10, :cond_5

    .line 274
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There are only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " matches of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    goto :goto_0

    .line 276
    :cond_5
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 277
    .restart local v12       #textView:Landroid/widget/TextView;
    const-string v1, "Robotium"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found. Have found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 279
    .end local v12           #textView:Landroid/widget/TextView;
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The text: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not found!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method
