.class Lcom/jayway/android/robotium/solo/DialogUtils;
.super Ljava/lang/Object;
.source "DialogUtils.java"


# instance fields
.field private final sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

.field private final viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;


# direct methods
.method public constructor <init>(Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Sleeper;)V
    .locals 0
    .parameter "viewFetcher"
    .parameter "sleeper"

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/jayway/android/robotium/solo/DialogUtils;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    .line 26
    iput-object p2, p0, Lcom/jayway/android/robotium/solo/DialogUtils;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    .line 27
    return-void
.end method


# virtual methods
.method public waitForDialogToClose(J)Z
    .locals 7
    .parameter "timeout"

    .prologue
    .line 38
    iget-object v6, p0, Lcom/jayway/android/robotium/solo/DialogUtils;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v6}, Lcom/jayway/android/robotium/solo/Sleeper;->sleepMini()V

    .line 39
    iget-object v6, p0, Lcom/jayway/android/robotium/solo/DialogUtils;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    invoke-virtual {v6}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getWindowDecorViews()[Landroid/view/View;

    move-result-object v6

    array-length v0, v6

    .line 40
    .local v0, elementsBefore:I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 41
    .local v4, now:J
    add-long v2, v4, p1

    .line 43
    .local v2, endTime:J
    :goto_0
    cmp-long v6, v4, v2

    if-gez v6, :cond_1

    .line 44
    iget-object v6, p0, Lcom/jayway/android/robotium/solo/DialogUtils;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    invoke-virtual {v6}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getWindowDecorViews()[Landroid/view/View;

    move-result-object v6

    array-length v1, v6

    .line 45
    .local v1, elementsNow:I
    if-ge v0, v1, :cond_0

    .line 46
    move v0, v1

    .line 48
    :cond_0
    if-le v0, v1, :cond_2

    .line 55
    .end local v1           #elementsNow:I
    :cond_1
    cmp-long v6, v4, v2

    if-lez v6, :cond_3

    .line 56
    const/4 v6, 0x0

    .line 58
    :goto_1
    return v6

    .line 51
    .restart local v1       #elementsNow:I
    :cond_2
    iget-object v6, p0, Lcom/jayway/android/robotium/solo/DialogUtils;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v6}, Lcom/jayway/android/robotium/solo/Sleeper;->sleepMini()V

    .line 52
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    goto :goto_0

    .line 58
    .end local v1           #elementsNow:I
    :cond_3
    const/4 v6, 0x1

    goto :goto_1
.end method
