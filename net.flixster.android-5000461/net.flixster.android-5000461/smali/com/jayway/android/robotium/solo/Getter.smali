.class Lcom/jayway/android/robotium/solo/Getter;
.super Ljava/lang/Object;
.source "Getter.java"


# instance fields
.field private final activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

.field private final viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

.field private final waiter:Lcom/jayway/android/robotium/solo/Waiter;


# direct methods
.method public constructor <init>(Lcom/jayway/android/robotium/solo/ActivityUtils;Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Waiter;)V
    .locals 0
    .parameter "activityUtils"
    .parameter "viewFetcher"
    .parameter "waiter"

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/jayway/android/robotium/solo/Getter;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    .line 34
    iput-object p2, p0, Lcom/jayway/android/robotium/solo/Getter;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    .line 35
    iput-object p3, p0, Lcom/jayway/android/robotium/solo/Getter;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    .line 36
    return-void
.end method


# virtual methods
.method public getView(I)Landroid/view/View;
    .locals 4
    .parameter "id"

    .prologue
    .line 84
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Getter;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getCurrentActivity(Z)Landroid/app/Activity;

    move-result-object v0

    .line 86
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 87
    .local v1, view:Landroid/view/View;
    if-eqz v1, :cond_0

    .line 90
    .end local v1           #view:Landroid/view/View;
    :goto_0
    return-object v1

    .restart local v1       #view:Landroid/view/View;
    :cond_0
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Getter;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v2, p1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(I)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public getView(Ljava/lang/Class;I)Landroid/view/View;
    .locals 1
    .parameter
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;I)TT;"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, classToFilterBy:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Getter;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v0, p2, p1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getView(Ljava/lang/Class;Ljava/lang/String;Z)Landroid/widget/TextView;
    .locals 11
    .parameter
    .parameter "text"
    .parameter "onlyVisible"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/TextView;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "Z)TT;"
        }
    .end annotation

    .prologue
    .local p1, classToFilterBy:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v2, 0x0

    .line 61
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Getter;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-wide/16 v3, 0x2710

    move-object v1, p2

    move v5, v2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJZZ)Z

    .line 62
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Getter;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v10

    .line 63
    .local v10, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    if-eqz p3, :cond_0

    .line 64
    invoke-static {v10}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->removeInvisibleViews(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v10

    .line 65
    :cond_0
    const/4 v9, 0x0

    .line 66
    .local v9, viewToReturn:Landroid/widget/TextView;,"TT;"
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 67
    .local v8, view:Landroid/widget/TextView;,"TT;"
    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    move-object v9, v8

    goto :goto_0

    .line 70
    .end local v8           #view:Landroid/widget/TextView;,"TT;"
    :cond_2
    if-nez v9, :cond_3

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with text "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is found!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    .line 73
    :cond_3
    return-object v9
.end method
