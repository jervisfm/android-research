.class public final enum Lcom/jayway/android/robotium/solo/Scroller$Direction;
.super Ljava/lang/Enum;
.source "Scroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jayway/android/robotium/solo/Scroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Direction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/jayway/android/robotium/solo/Scroller$Direction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/jayway/android/robotium/solo/Scroller$Direction;

.field public static final enum DOWN:Lcom/jayway/android/robotium/solo/Scroller$Direction;

.field public static final enum UP:Lcom/jayway/android/robotium/solo/Scroller$Direction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/jayway/android/robotium/solo/Scroller$Direction;

    const-string v1, "UP"

    invoke-direct {v0, v1, v2}, Lcom/jayway/android/robotium/solo/Scroller$Direction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/jayway/android/robotium/solo/Scroller$Direction;->UP:Lcom/jayway/android/robotium/solo/Scroller$Direction;

    new-instance v0, Lcom/jayway/android/robotium/solo/Scroller$Direction;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v3}, Lcom/jayway/android/robotium/solo/Scroller$Direction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/jayway/android/robotium/solo/Scroller$Direction;->DOWN:Lcom/jayway/android/robotium/solo/Scroller$Direction;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jayway/android/robotium/solo/Scroller$Direction;

    sget-object v1, Lcom/jayway/android/robotium/solo/Scroller$Direction;->UP:Lcom/jayway/android/robotium/solo/Scroller$Direction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/jayway/android/robotium/solo/Scroller$Direction;->DOWN:Lcom/jayway/android/robotium/solo/Scroller$Direction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/jayway/android/robotium/solo/Scroller$Direction;->$VALUES:[Lcom/jayway/android/robotium/solo/Scroller$Direction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jayway/android/robotium/solo/Scroller$Direction;
    .locals 1
    .parameter "name"

    .prologue
    .line 24
    const-class v0, Lcom/jayway/android/robotium/solo/Scroller$Direction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jayway/android/robotium/solo/Scroller$Direction;

    return-object v0
.end method

.method public static values()[Lcom/jayway/android/robotium/solo/Scroller$Direction;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/jayway/android/robotium/solo/Scroller$Direction;->$VALUES:[Lcom/jayway/android/robotium/solo/Scroller$Direction;

    invoke-virtual {v0}, [Lcom/jayway/android/robotium/solo/Scroller$Direction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jayway/android/robotium/solo/Scroller$Direction;

    return-object v0
.end method
