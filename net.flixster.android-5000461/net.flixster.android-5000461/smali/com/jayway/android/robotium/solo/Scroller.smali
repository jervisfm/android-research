.class Lcom/jayway/android/robotium/solo/Scroller;
.super Ljava/lang/Object;
.source "Scroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jayway/android/robotium/solo/Scroller$Side;,
        Lcom/jayway/android/robotium/solo/Scroller$Direction;
    }
.end annotation


# static fields
.field public static final DOWN:I = 0x0

.field public static final UP:I = 0x1


# instance fields
.field private final activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

.field private final inst:Landroid/app/Instrumentation;

.field private final sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

.field private final viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;


# direct methods
.method public constructor <init>(Landroid/app/Instrumentation;Lcom/jayway/android/robotium/solo/ActivityUtils;Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Sleeper;)V
    .locals 0
    .parameter "inst"
    .parameter "activityUtils"
    .parameter "viewFetcher"
    .parameter "sleeper"

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/jayway/android/robotium/solo/Scroller;->inst:Landroid/app/Instrumentation;

    .line 45
    iput-object p2, p0, Lcom/jayway/android/robotium/solo/Scroller;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    .line 46
    iput-object p3, p0, Lcom/jayway/android/robotium/solo/Scroller;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    .line 47
    iput-object p4, p0, Lcom/jayway/android/robotium/solo/Scroller;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    .line 48
    return-void
.end method

.method private scrollListToLine(Landroid/widget/AbsListView;I)V
    .locals 3
    .parameter
    .parameter "line"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/AbsListView;",
            ">(TT;I)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, view:Landroid/widget/AbsListView;,"TT;"
    instance-of v1, p1, Landroid/widget/GridView;

    if-eqz v1, :cond_0

    .line 236
    add-int/lit8 v0, p2, 0x1

    .line 240
    .local v0, lineToMoveTo:I
    :goto_0
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Scroller;->inst:Landroid/app/Instrumentation;

    new-instance v2, Lcom/jayway/android/robotium/solo/Scroller$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/jayway/android/robotium/solo/Scroller$2;-><init>(Lcom/jayway/android/robotium/solo/Scroller;Landroid/widget/AbsListView;I)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->runOnMainSync(Ljava/lang/Runnable;)V

    .line 245
    return-void

    .line 238
    .end local v0           #lineToMoveTo:I
    :cond_0
    move v0, p2

    .restart local v0       #lineToMoveTo:I
    goto :goto_0
.end method

.method private scrollScrollView(ILjava/util/ArrayList;)Z
    .locals 8
    .parameter "direction"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ScrollView;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, scrollViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ScrollView;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 102
    iget-object v6, p0, Lcom/jayway/android/robotium/solo/Scroller;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v7, Landroid/widget/ScrollView;

    invoke-virtual {v6, v7, p2}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getView(Ljava/lang/Class;Ljava/util/ArrayList;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    .line 103
    .local v1, scroll:Landroid/widget/ScrollView;
    const/4 v2, 0x0

    .line 105
    .local v2, scrollAmount:I
    if-eqz v1, :cond_1

    .line 106
    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v0

    .line 107
    .local v0, height:I
    add-int/lit8 v0, v0, -0x1

    .line 108
    const/4 v3, 0x0

    .line 110
    .local v3, scrollTo:I
    if-nez p1, :cond_2

    .line 111
    move v3, v0

    .line 117
    :cond_0
    :goto_0
    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    .line 118
    invoke-direct {p0, v1, v4, v3}, Lcom/jayway/android/robotium/solo/Scroller;->scrollScrollViewTo(Landroid/widget/ScrollView;II)V

    .line 119
    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v6

    if-ne v2, v6, :cond_3

    .line 126
    .end local v0           #height:I
    .end local v3           #scrollTo:I
    :cond_1
    :goto_1
    return v4

    .line 114
    .restart local v0       #height:I
    .restart local v3       #scrollTo:I
    :cond_2
    if-ne p1, v5, :cond_0

    .line 115
    neg-int v3, v0

    goto :goto_0

    :cond_3
    move v4, v5

    .line 123
    goto :goto_1
.end method

.method private scrollScrollViewTo(Landroid/widget/ScrollView;II)V
    .locals 2
    .parameter "scrollView"
    .parameter "x"
    .parameter "y"

    .prologue
    .line 137
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Scroller;->inst:Landroid/app/Instrumentation;

    new-instance v1, Lcom/jayway/android/robotium/solo/Scroller$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/jayway/android/robotium/solo/Scroller$1;-><init>(Lcom/jayway/android/robotium/solo/Scroller;Landroid/widget/ScrollView;II)V

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->runOnMainSync(Ljava/lang/Runnable;)V

    .line 142
    return-void
.end method


# virtual methods
.method public drag(FFFFI)V
    .locals 23
    .parameter "fromX"
    .parameter "toX"
    .parameter "fromY"
    .parameter "toY"
    .parameter "stepCount"

    .prologue
    .line 66
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 67
    .local v2, downTime:J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 68
    .local v4, eventTime:J
    move/from16 v21, p3

    .line 69
    .local v21, y:F
    move/from16 v19, p1

    .line 70
    .local v19, x:F
    sub-float v6, p4, p3

    move/from16 v0, p5

    int-to-float v9, v0

    div-float v22, v6, v9

    .line 71
    .local v22, yStep:F
    sub-float v6, p2, p1

    move/from16 v0, p5

    int-to-float v9, v0

    div-float v20, v6, v9

    .line 72
    .local v20, xStep:F
    const/4 v6, 0x0

    const/4 v9, 0x0

    move/from16 v7, p1

    move/from16 v8, p3

    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v17

    .line 74
    .local v17, event:Landroid/view/MotionEvent;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jayway/android/robotium/solo/Scroller;->inst:Landroid/app/Instrumentation;

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    .line 76
    :goto_0
    const/16 v18, 0x0

    .local v18, i:I
    move/from16 v7, v19

    .end local v19           #x:F
    .local v7, x:F
    move/from16 v8, v21

    .end local v21           #y:F
    .local v8, y:F
    :goto_1
    move/from16 v0, v18

    move/from16 v1, p5

    if-ge v0, v1, :cond_0

    .line 77
    add-float v8, v8, v22

    .line 78
    add-float v7, v7, v20

    .line 79
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 80
    const/4 v6, 0x2

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v17

    .line 82
    :try_start_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jayway/android/robotium/solo/Scroller;->inst:Landroid/app/Instrumentation;

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    .line 76
    :goto_2
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 85
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 86
    const/4 v13, 0x1

    const/16 v16, 0x0

    move-wide v9, v2

    move-wide v11, v4

    move/from16 v14, p2

    move/from16 v15, p4

    invoke-static/range {v9 .. v16}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v17

    .line 88
    :try_start_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jayway/android/robotium/solo/Scroller;->inst:Landroid/app/Instrumentation;

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0

    .line 90
    :goto_3
    return-void

    .line 89
    :catch_0
    move-exception v6

    goto :goto_3

    .line 83
    :catch_1
    move-exception v6

    goto :goto_2

    .line 75
    .end local v7           #x:F
    .end local v8           #y:F
    .end local v18           #i:I
    .restart local v19       #x:F
    .restart local v21       #y:F
    :catch_2
    move-exception v6

    goto :goto_0
.end method

.method public scroll(I)Z
    .locals 7
    .parameter "direction"

    .prologue
    const/4 v6, 0x0

    .line 154
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Scroller;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const/4 v5, 0x1

    invoke-virtual {v4, v6, v5}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getViews(Landroid/view/View;Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v4}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->removeInvisibleViews(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 155
    .local v3, viewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    const-class v4, Landroid/widget/ListView;

    invoke-static {v4, v3}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->filterViews(Ljava/lang/Class;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 157
    .local v1, listViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 158
    const-class v4, Landroid/widget/ListView;

    invoke-virtual {p0, v4, v6, p1, v1}, Lcom/jayway/android/robotium/solo/Scroller;->scrollList(Ljava/lang/Class;Landroid/widget/AbsListView;ILjava/util/ArrayList;)Z

    move-result v4

    .line 172
    :goto_0
    return v4

    .line 161
    :cond_0
    const-class v4, Landroid/widget/GridView;

    invoke-static {v4, v3}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->filterViews(Ljava/lang/Class;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 163
    .local v0, gridViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/GridView;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 164
    const-class v4, Landroid/widget/GridView;

    invoke-virtual {p0, v4, v6, p1, v0}, Lcom/jayway/android/robotium/solo/Scroller;->scrollList(Ljava/lang/Class;Landroid/widget/AbsListView;ILjava/util/ArrayList;)Z

    move-result v4

    goto :goto_0

    .line 167
    :cond_1
    const-class v4, Landroid/widget/ScrollView;

    invoke-static {v4, v3}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->filterViews(Ljava/lang/Class;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 169
    .local v2, scrollViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ScrollView;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 170
    invoke-direct {p0, p1, v2}, Lcom/jayway/android/robotium/solo/Scroller;->scrollScrollView(ILjava/util/ArrayList;)Z

    move-result v4

    goto :goto_0

    .line 172
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public scrollList(Ljava/lang/Class;Landroid/widget/AbsListView;ILjava/util/ArrayList;)Z
    .locals 6
    .parameter
    .parameter
    .parameter "direction"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/AbsListView;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;I",
            "Ljava/util/ArrayList",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .local p1, classToFilterBy:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    .local p2, absListView:Landroid/widget/AbsListView;,"TT;"
    .local p4, listViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 186
    if-nez p2, :cond_0

    .line 187
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Scroller;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    invoke-virtual {v4, p1, p4}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getView(Ljava/lang/Class;Ljava/util/ArrayList;)Landroid/view/View;

    move-result-object p2

    .end local p2           #absListView:Landroid/widget/AbsListView;,"TT;"
    check-cast p2, Landroid/widget/AbsListView;

    .line 189
    .restart local p2       #absListView:Landroid/widget/AbsListView;,"TT;"
    :cond_0
    if-nez p2, :cond_1

    .line 222
    :goto_0
    return v2

    .line 192
    :cond_1
    if-nez p3, :cond_5

    .line 193
    invoke-virtual {p2}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v4

    invoke-virtual {p2}, Landroid/widget/AbsListView;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-lt v4, v5, :cond_2

    .line 194
    invoke-virtual {p2}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v3

    invoke-direct {p0, p2, v3}, Lcom/jayway/android/robotium/solo/Scroller;->scrollListToLine(Landroid/widget/AbsListView;I)V

    goto :goto_0

    .line 198
    :cond_2
    invoke-virtual {p2}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {p2}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v4

    if-eq v2, v4, :cond_4

    .line 199
    invoke-virtual {p2}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v2

    invoke-direct {p0, p2, v2}, Lcom/jayway/android/robotium/solo/Scroller;->scrollListToLine(Landroid/widget/AbsListView;I)V

    .line 221
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Scroller;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v2}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    move v2, v3

    .line 222
    goto :goto_0

    .line 202
    :cond_4
    invoke-virtual {p2}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, p2, v2}, Lcom/jayway/android/robotium/solo/Scroller;->scrollListToLine(Landroid/widget/AbsListView;I)V

    goto :goto_1

    .line 204
    :cond_5
    if-ne p3, v3, :cond_3

    .line 205
    invoke-virtual {p2}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_6

    .line 206
    invoke-direct {p0, p2, v2}, Lcom/jayway/android/robotium/solo/Scroller;->scrollListToLine(Landroid/widget/AbsListView;I)V

    goto :goto_0

    .line 210
    :cond_6
    invoke-virtual {p2}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v2

    invoke-virtual {p2}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v4

    sub-int v1, v2, v4

    .line 211
    .local v1, lines:I
    invoke-virtual {p2}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int v0, v2, v1

    .line 213
    .local v0, lineToScrollTo:I
    invoke-virtual {p2}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v2

    if-ne v0, v2, :cond_7

    .line 214
    add-int/lit8 v0, v0, -0x1

    .line 216
    :cond_7
    if-gez v0, :cond_8

    .line 217
    const/4 v0, 0x0

    .line 219
    :cond_8
    invoke-direct {p0, p2, v0}, Lcom/jayway/android/robotium/solo/Scroller;->scrollListToLine(Landroid/widget/AbsListView;I)V

    goto :goto_1
.end method

.method public scrollToSide(Lcom/jayway/android/robotium/solo/Scroller$Side;)V
    .locals 14
    .parameter "side"

    .prologue
    const/16 v5, 0x28

    const/high16 v6, 0x4000

    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Scroller;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v12

    .line 258
    .local v12, screenHeight:I
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Scroller;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getCurrentActivity(Z)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v13

    .line 260
    .local v13, screenWidth:I
    int-to-float v0, v13

    div-float v2, v0, v6

    .line 261
    .local v2, x:F
    int-to-float v0, v12

    div-float v3, v0, v6

    .line 262
    .local v3, y:F
    sget-object v0, Lcom/jayway/android/robotium/solo/Scroller$Side;->LEFT:Lcom/jayway/android/robotium/solo/Scroller$Side;

    if-ne p1, v0, :cond_1

    move-object v0, p0

    move v4, v3

    .line 263
    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Scroller;->drag(FFFFI)V

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    sget-object v0, Lcom/jayway/android/robotium/solo/Scroller$Side;->RIGHT:Lcom/jayway/android/robotium/solo/Scroller$Side;

    if-ne p1, v0, :cond_0

    move-object v6, p0

    move v7, v2

    move v8, v1

    move v9, v3

    move v10, v3

    move v11, v5

    .line 265
    invoke-virtual/range {v6 .. v11}, Lcom/jayway/android/robotium/solo/Scroller;->drag(FFFFI)V

    goto :goto_0
.end method
