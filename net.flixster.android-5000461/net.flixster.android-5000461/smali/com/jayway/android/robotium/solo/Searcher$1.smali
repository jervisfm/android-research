.class Lcom/jayway/android/robotium/solo/Searcher$1;
.super Ljava/lang/Object;
.source "Searcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jayway/android/robotium/solo/Searcher;->searchFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/Collection",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jayway/android/robotium/solo/Searcher;

.field final synthetic val$onlyVisible:Z

.field final synthetic val$viewClass:Ljava/lang/Class;


# direct methods
.method constructor <init>(Lcom/jayway/android/robotium/solo/Searcher;ZLjava/lang/Class;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 93
    iput-object p1, p0, Lcom/jayway/android/robotium/solo/Searcher$1;->this$0:Lcom/jayway/android/robotium/solo/Searcher;

    iput-boolean p2, p0, Lcom/jayway/android/robotium/solo/Searcher$1;->val$onlyVisible:Z

    iput-object p3, p0, Lcom/jayway/android/robotium/solo/Searcher$1;->val$viewClass:Ljava/lang/Class;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/jayway/android/robotium/solo/Searcher$1;->call()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Searcher$1;->this$0:Lcom/jayway/android/robotium/solo/Searcher;

    #getter for: Lcom/jayway/android/robotium/solo/Searcher;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;
    invoke-static {v0}, Lcom/jayway/android/robotium/solo/Searcher;->access$000(Lcom/jayway/android/robotium/solo/Searcher;)Lcom/jayway/android/robotium/solo/Sleeper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 97
    iget-boolean v0, p0, Lcom/jayway/android/robotium/solo/Searcher$1;->val$onlyVisible:Z

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Searcher$1;->this$0:Lcom/jayway/android/robotium/solo/Searcher;

    #getter for: Lcom/jayway/android/robotium/solo/Searcher;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;
    invoke-static {v0}, Lcom/jayway/android/robotium/solo/Searcher;->access$100(Lcom/jayway/android/robotium/solo/Searcher;)Lcom/jayway/android/robotium/solo/ViewFetcher;

    move-result-object v0

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Searcher$1;->val$viewClass:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->removeInvisibleViews(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Searcher$1;->this$0:Lcom/jayway/android/robotium/solo/Searcher;

    #getter for: Lcom/jayway/android/robotium/solo/Searcher;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;
    invoke-static {v0}, Lcom/jayway/android/robotium/solo/Searcher;->access$100(Lcom/jayway/android/robotium/solo/Searcher;)Lcom/jayway/android/robotium/solo/ViewFetcher;

    move-result-object v0

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Searcher$1;->val$viewClass:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method
