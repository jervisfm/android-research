.class Lcom/jayway/android/robotium/solo/Searcher;
.super Ljava/lang/Object;
.source "Searcher.java"


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field private final TIMEOUT:I

.field private numberOfUniqueViews:I

.field private final scroller:Lcom/jayway/android/robotium/solo/Scroller;

.field private final sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

.field uniqueTextViews:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;


# direct methods
.method public constructor <init>(Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Scroller;Lcom/jayway/android/robotium/solo/Sleeper;)V
    .locals 1
    .parameter "viewFetcher"
    .parameter "scroller"
    .parameter "sleeper"

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/16 v0, 0x1388

    iput v0, p0, Lcom/jayway/android/robotium/solo/Searcher;->TIMEOUT:I

    .line 26
    const-string v0, "Robotium"

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Searcher;->LOG_TAG:Ljava/lang/String;

    .line 39
    iput-object p1, p0, Lcom/jayway/android/robotium/solo/Searcher;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    .line 40
    iput-object p2, p0, Lcom/jayway/android/robotium/solo/Searcher;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    .line 41
    iput-object p3, p0, Lcom/jayway/android/robotium/solo/Searcher;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Searcher;->uniqueTextViews:Ljava/util/Set;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/jayway/android/robotium/solo/Searcher;)Lcom/jayway/android/robotium/solo/Sleeper;
    .locals 1
    .parameter "x0"

    .prologue
    .line 20
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Searcher;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/jayway/android/robotium/solo/Searcher;)Lcom/jayway/android/robotium/solo/ViewFetcher;
    .locals 1
    .parameter "x0"

    .prologue
    .line 20
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Searcher;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    return-object v0
.end method

.method private logMatchesFoundAndReturnFalse(Ljava/lang/String;)Z
    .locals 3
    .parameter "regex"

    .prologue
    .line 243
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Searcher;->uniqueTextViews:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 244
    const-string v0, "Robotium"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " There are only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Searcher;->uniqueTextViews:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " matches of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Searcher;->uniqueTextViews:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 247
    const/4 v0, 0x0

    return v0
.end method

.method private setArrayToNullAndReturn(ZLjava/util/ArrayList;)Z
    .locals 0
    .parameter "booleanToReturn"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(Z",
            "Ljava/util/ArrayList",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 144
    .local p2, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    const/4 p2, 0x0

    .line 145
    return p1
.end method


# virtual methods
.method public getNumberOfUniqueViews()I
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Lcom/jayway/android/robotium/solo/Searcher;->numberOfUniqueViews:I

    return v0
.end method

.method public getNumberOfUniqueViews(Ljava/util/Set;Ljava/util/ArrayList;)I
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/util/Set",
            "<TT;>;",
            "Ljava/util/ArrayList",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 218
    .local p1, uniqueViews:Ljava/util/Set;,"Ljava/util/Set<TT;>;"
    .local p2, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 219
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 218
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    iput v1, p0, Lcom/jayway/android/robotium/solo/Searcher;->numberOfUniqueViews:I

    .line 222
    iget v1, p0, Lcom/jayway/android/robotium/solo/Searcher;->numberOfUniqueViews:I

    return v1
.end method

.method public searchFor(Landroid/view/View;)Z
    .locals 5
    .parameter "view"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 157
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Searcher;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    invoke-virtual {v4, v3}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getAllViews(Z)Ljava/util/ArrayList;

    move-result-object v2

    .line 158
    .local v2, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 159
    .local v1, v:Landroid/view/View;
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 163
    .end local v1           #v:Landroid/view/View;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public searchFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z
    .locals 3
    .parameter
    .parameter "regex"
    .parameter "expectedMinimumNumberOfMatches"
    .parameter "scroll"
    .parameter "onlyVisible"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/TextView;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "IZZ)Z"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    new-instance v1, Lcom/jayway/android/robotium/solo/Searcher$1;

    invoke-direct {v1, p0, p5, p1}, Lcom/jayway/android/robotium/solo/Searcher$1;-><init>(Lcom/jayway/android/robotium/solo/Searcher;ZLjava/lang/Class;)V

    .line 104
    .local v1, viewFetcherCallback:Ljava/util/concurrent/Callable;,"Ljava/util/concurrent/Callable<Ljava/util/Collection<TT;>;>;"
    :try_start_0
    invoke-virtual {p0, v1, p2, p3, p4}, Lcom/jayway/android/robotium/solo/Searcher;->searchFor(Ljava/util/concurrent/Callable;Ljava/lang/String;IZ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public searchFor(Ljava/util/Set;Ljava/lang/Class;I)Z
    .locals 4
    .parameter
    .parameter
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/util/Set",
            "<TT;>;",
            "Ljava/lang/Class",
            "<TT;>;I)Z"
        }
    .end annotation

    .prologue
    .local p1, uniqueViews:Ljava/util/Set;,"Ljava/util/Set<TT;>;"
    .local p2, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v3, 0x1

    .line 122
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Searcher;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    invoke-virtual {v2, p2}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->removeInvisibleViews(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 124
    .local v0, allViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    invoke-virtual {p0, p1, v0}, Lcom/jayway/android/robotium/solo/Searcher;->getNumberOfUniqueViews(Ljava/util/Set;Ljava/util/ArrayList;)I

    move-result v1

    .line 126
    .local v1, uniqueViewsFound:I
    if-lez v1, :cond_0

    if-ge p3, v1, :cond_0

    .line 127
    invoke-direct {p0, v3, v0}, Lcom/jayway/android/robotium/solo/Searcher;->setArrayToNullAndReturn(ZLjava/util/ArrayList;)Z

    move-result v2

    .line 132
    :goto_0
    return v2

    .line 129
    :cond_0
    if-lez v1, :cond_1

    if-nez p3, :cond_1

    .line 130
    invoke-direct {p0, v3, v0}, Lcom/jayway/android/robotium/solo/Searcher;->setArrayToNullAndReturn(ZLjava/util/ArrayList;)Z

    move-result v2

    goto :goto_0

    .line 132
    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, v2, v0}, Lcom/jayway/android/robotium/solo/Searcher;->setArrayToNullAndReturn(ZLjava/util/ArrayList;)Z

    move-result v2

    goto :goto_0
.end method

.method public searchFor(Ljava/util/concurrent/Callable;Ljava/lang/String;IZ)Z
    .locals 6
    .parameter
    .parameter "regex"
    .parameter "expectedMinimumNumberOfMatches"
    .parameter "scroll"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/TextView;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/util/Collection",
            "<TT;>;>;",
            "Ljava/lang/String;",
            "IZ)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .local p1, viewFetcherCallback:Ljava/util/concurrent/Callable;,"Ljava/util/concurrent/Callable<Ljava/util/Collection<TT;>;>;"
    const/4 v3, 0x1

    .line 184
    if-ge p3, v3, :cond_0

    .line 185
    const/4 p3, 0x1

    .line 189
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    .line 190
    .local v2, views:Ljava/util/Collection;,"Ljava/util/Collection<TT;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 191
    .local v1, view:Landroid/widget/TextView;
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Searcher;->uniqueTextViews:Ljava/util/Set;

    invoke-static {p2, v1, v4}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->checkAndGetMatches(Ljava/lang/String;Landroid/widget/TextView;Ljava/util/Set;)I

    move-result v4

    if-ne v4, p3, :cond_1

    .line 192
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Searcher;->uniqueTextViews:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 200
    .end local v1           #view:Landroid/widget/TextView;
    :goto_1
    return v3

    .line 196
    :cond_2
    if-eqz p4, :cond_3

    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Searcher;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/jayway/android/robotium/solo/Scroller;->scroll(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 197
    invoke-direct {p0, p2}, Lcom/jayway/android/robotium/solo/Searcher;->logMatchesFoundAndReturnFalse(Ljava/lang/String;)Z

    move-result v3

    goto :goto_1

    .line 199
    :cond_3
    if-nez p4, :cond_4

    .line 200
    invoke-direct {p0, p2}, Lcom/jayway/android/robotium/solo/Searcher;->logMatchesFoundAndReturnFalse(Ljava/lang/String;)Z

    move-result v3

    goto :goto_1

    .line 202
    :cond_4
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Searcher;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v4}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    goto :goto_0
.end method

.method public searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z
    .locals 7
    .parameter
    .parameter "regex"
    .parameter "expectedMinimumNumberOfMatches"
    .parameter "scroll"
    .parameter "onlyVisible"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/widget/TextView;",
            ">;",
            "Ljava/lang/String;",
            "IZZ)Z"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<+Landroid/widget/TextView;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x1388

    add-long v0, v3, v5

    .line 65
    .local v0, endTime:J
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v3, v3, v0

    if-gez v3, :cond_1

    .line 66
    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Searcher;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v3}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 67
    invoke-virtual/range {p0 .. p5}, Lcom/jayway/android/robotium/solo/Searcher;->searchFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v2

    .line 68
    .local v2, foundAnyMatchingView:Z
    if-eqz v2, :cond_0

    .line 69
    const/4 v3, 0x1

    .line 72
    .end local v2           #foundAnyMatchingView:Z
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method
