.class public Lcom/jayway/android/robotium/solo/Solo;
.super Ljava/lang/Object;
.source "Solo.java"


# static fields
.field public static final CLOSED:I = 0x0

.field public static final DELETE:I = 0x43

.field public static final DOWN:I = 0x14

.field public static final ENTER:I = 0x42

.field public static final LANDSCAPE:I = 0x0

.field public static final LEFT:I = 0x15

.field public static final MENU:I = 0x52

.field public static final OPENED:I = 0x1

.field public static final PORTRAIT:I = 0x1

.field public static final RIGHT:I = 0x16

.field public static final UP:I = 0x13


# instance fields
.field private final SMALLTIMEOUT:I

.field private final TIMEOUT:I

.field private final activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

.field private final asserter:Lcom/jayway/android/robotium/solo/Asserter;

.field private final checker:Lcom/jayway/android/robotium/solo/Checker;

.field private final clicker:Lcom/jayway/android/robotium/solo/Clicker;

.field private final dialogUtils:Lcom/jayway/android/robotium/solo/DialogUtils;

.field private final getter:Lcom/jayway/android/robotium/solo/Getter;

.field private final presser:Lcom/jayway/android/robotium/solo/Presser;

.field private final robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

.field private final scroller:Lcom/jayway/android/robotium/solo/Scroller;

.field private final searcher:Lcom/jayway/android/robotium/solo/Searcher;

.field private final setter:Lcom/jayway/android/robotium/solo/Setter;

.field private final sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

.field private final textEnterer:Lcom/jayway/android/robotium/solo/TextEnterer;

.field private final viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

.field private final waiter:Lcom/jayway/android/robotium/solo/Waiter;


# direct methods
.method public constructor <init>(Landroid/app/Instrumentation;)V
    .locals 1
    .parameter "instrumentation"

    .prologue
    .line 135
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jayway/android/robotium/solo/Solo;-><init>(Landroid/app/Instrumentation;Landroid/app/Activity;)V

    .line 136
    return-void
.end method

.method public constructor <init>(Landroid/app/Instrumentation;Landroid/app/Activity;)V
    .locals 7
    .parameter "instrumentation"
    .parameter "activity"

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/jayway/android/robotium/solo/Solo;->TIMEOUT:I

    .line 85
    const/16 v0, 0x2710

    iput v0, p0, Lcom/jayway/android/robotium/solo/Solo;->SMALLTIMEOUT:I

    .line 109
    new-instance v0, Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-direct {v0}, Lcom/jayway/android/robotium/solo/Sleeper;-><init>()V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    .line 110
    new-instance v0, Lcom/jayway/android/robotium/solo/ActivityUtils;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-direct {v0, p1, p2, v1}, Lcom/jayway/android/robotium/solo/ActivityUtils;-><init>(Landroid/app/Instrumentation;Landroid/app/Activity;Lcom/jayway/android/robotium/solo/Sleeper;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    .line 111
    new-instance v0, Lcom/jayway/android/robotium/solo/ViewFetcher;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-direct {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;-><init>(Lcom/jayway/android/robotium/solo/ActivityUtils;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    .line 112
    new-instance v0, Lcom/jayway/android/robotium/solo/DialogUtils;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-direct {v0, v1, v2}, Lcom/jayway/android/robotium/solo/DialogUtils;-><init>(Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Sleeper;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->dialogUtils:Lcom/jayway/android/robotium/solo/DialogUtils;

    .line 113
    new-instance v0, Lcom/jayway/android/robotium/solo/Scroller;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/jayway/android/robotium/solo/Scroller;-><init>(Landroid/app/Instrumentation;Lcom/jayway/android/robotium/solo/ActivityUtils;Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Sleeper;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    .line 114
    new-instance v0, Lcom/jayway/android/robotium/solo/Searcher;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-direct {v0, v1, v2, v3}, Lcom/jayway/android/robotium/solo/Searcher;-><init>(Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Scroller;Lcom/jayway/android/robotium/solo/Sleeper;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    .line 115
    new-instance v0, Lcom/jayway/android/robotium/solo/Waiter;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    iget-object v5, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-direct/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Waiter;-><init>(Lcom/jayway/android/robotium/solo/ActivityUtils;Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Searcher;Lcom/jayway/android/robotium/solo/Scroller;Lcom/jayway/android/robotium/solo/Sleeper;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    .line 116
    new-instance v0, Lcom/jayway/android/robotium/solo/Setter;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-direct {v0, v1}, Lcom/jayway/android/robotium/solo/Setter;-><init>(Lcom/jayway/android/robotium/solo/ActivityUtils;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->setter:Lcom/jayway/android/robotium/solo/Setter;

    .line 117
    new-instance v0, Lcom/jayway/android/robotium/solo/Getter;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-direct {v0, v1, v2, v3}, Lcom/jayway/android/robotium/solo/Getter;-><init>(Lcom/jayway/android/robotium/solo/ActivityUtils;Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Waiter;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    .line 118
    new-instance v0, Lcom/jayway/android/robotium/solo/Asserter;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-direct {v0, v1, v2}, Lcom/jayway/android/robotium/solo/Asserter;-><init>(Lcom/jayway/android/robotium/solo/ActivityUtils;Lcom/jayway/android/robotium/solo/Waiter;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->asserter:Lcom/jayway/android/robotium/solo/Asserter;

    .line 119
    new-instance v0, Lcom/jayway/android/robotium/solo/Checker;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-direct {v0, v1, v2}, Lcom/jayway/android/robotium/solo/Checker;-><init>(Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Waiter;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    .line 120
    new-instance v0, Lcom/jayway/android/robotium/solo/RobotiumUtils;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-direct {v0, p1, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;-><init>(Landroid/app/Instrumentation;Lcom/jayway/android/robotium/solo/Sleeper;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    .line 121
    new-instance v0, Lcom/jayway/android/robotium/solo/Clicker;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    iget-object v5, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    iget-object v6, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/jayway/android/robotium/solo/Clicker;-><init>(Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Scroller;Lcom/jayway/android/robotium/solo/RobotiumUtils;Landroid/app/Instrumentation;Lcom/jayway/android/robotium/solo/Sleeper;Lcom/jayway/android/robotium/solo/Waiter;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    .line 122
    new-instance v0, Lcom/jayway/android/robotium/solo/Presser;

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/jayway/android/robotium/solo/Presser;-><init>(Lcom/jayway/android/robotium/solo/Clicker;Landroid/app/Instrumentation;Lcom/jayway/android/robotium/solo/Sleeper;Lcom/jayway/android/robotium/solo/Waiter;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->presser:Lcom/jayway/android/robotium/solo/Presser;

    .line 123
    new-instance v0, Lcom/jayway/android/robotium/solo/TextEnterer;

    invoke-direct {v0, p1}, Lcom/jayway/android/robotium/solo/TextEnterer;-><init>(Landroid/app/Instrumentation;)V

    iput-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->textEnterer:Lcom/jayway/android/robotium/solo/TextEnterer;

    .line 124
    return-void
.end method


# virtual methods
.method public assertCurrentActivity(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .parameter "message"
    .parameter "expectedClass"

    .prologue
    .line 569
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->asserter:Lcom/jayway/android/robotium/solo/Asserter;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Asserter;->assertCurrentActivity(Ljava/lang/String;Ljava/lang/Class;)V

    .line 571
    return-void
.end method

.method public assertCurrentActivity(Ljava/lang/String;Ljava/lang/Class;Z)V
    .locals 1
    .parameter "message"
    .parameter "expectedClass"
    .parameter "isNewInstance"

    .prologue
    .line 601
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->asserter:Lcom/jayway/android/robotium/solo/Asserter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jayway/android/robotium/solo/Asserter;->assertCurrentActivity(Ljava/lang/String;Ljava/lang/Class;Z)V

    .line 602
    return-void
.end method

.method public assertCurrentActivity(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "message"
    .parameter "name"

    .prologue
    .line 555
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->asserter:Lcom/jayway/android/robotium/solo/Asserter;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Asserter;->assertCurrentActivity(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    return-void
.end method

.method public assertCurrentActivity(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .parameter "message"
    .parameter "name"
    .parameter "isNewInstance"

    .prologue
    .line 585
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->asserter:Lcom/jayway/android/robotium/solo/Asserter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jayway/android/robotium/solo/Asserter;->assertCurrentActivity(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 586
    return-void
.end method

.method public assertMemoryNotLow()V
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->asserter:Lcom/jayway/android/robotium/solo/Asserter;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/Asserter;->assertMemoryNotLow()V

    .line 612
    return-void
.end method

.method public clearEditText(I)V
    .locals 3
    .parameter "index"

    .prologue
    .line 1257
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->textEnterer:Lcom/jayway/android/robotium/solo/TextEnterer;

    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v2, Landroid/widget/EditText;

    invoke-virtual {v0, p1, v2}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Lcom/jayway/android/robotium/solo/TextEnterer;->setEditText(Landroid/widget/EditText;Ljava/lang/String;)V

    .line 1258
    return-void
.end method

.method public clearEditText(Landroid/widget/EditText;)V
    .locals 2
    .parameter "editText"

    .prologue
    .line 1268
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/16 v1, 0x2710

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;I)Z

    .line 1269
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->textEnterer:Lcom/jayway/android/robotium/solo/TextEnterer;

    const-string v1, ""

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/TextEnterer;->setEditText(Landroid/widget/EditText;Ljava/lang/String;)V

    .line 1270
    return-void
.end method

.method public clickInList(I)Ljava/util/ArrayList;
    .locals 1
    .parameter "line"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 975
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickInList(I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public clickInList(II)Ljava/util/ArrayList;
    .locals 2
    .parameter "line"
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 989
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    invoke-virtual {v0, p1, p2, v1, v1}, Lcom/jayway/android/robotium/solo/Clicker;->clickInList(IIZI)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public clickLongInList(I)Ljava/util/ArrayList;
    .locals 3
    .parameter "line"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1001
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v2, v1, v2}, Lcom/jayway/android/robotium/solo/Clicker;->clickInList(IIZI)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public clickLongInList(II)Ljava/util/ArrayList;
    .locals 3
    .parameter "line"
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/jayway/android/robotium/solo/Clicker;->clickInList(IIZI)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public clickLongInList(III)Ljava/util/ArrayList;
    .locals 2
    .parameter "line"
    .parameter "index"
    .parameter "time"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/jayway/android/robotium/solo/Clicker;->clickInList(IIZI)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public clickLongOnScreen(FF)V
    .locals 2
    .parameter "x"
    .parameter "y"

    .prologue
    .line 659
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/jayway/android/robotium/solo/Clicker;->clickLongOnScreen(FFI)V

    .line 660
    return-void
.end method

.method public clickLongOnScreen(FFI)V
    .locals 1
    .parameter "x"
    .parameter "y"
    .parameter "time"

    .prologue
    .line 672
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jayway/android/robotium/solo/Clicker;->clickLongOnScreen(FFI)V

    .line 673
    return-void
.end method

.method public clickLongOnText(Ljava/lang/String;)V
    .locals 6
    .parameter "text"

    .prologue
    const/4 v2, 0x1

    .line 862
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const/4 v5, 0x0

    move-object v1, p1

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    .line 863
    return-void
.end method

.method public clickLongOnText(Ljava/lang/String;I)V
    .locals 6
    .parameter "text"
    .parameter "match"

    .prologue
    const/4 v2, 0x1

    .line 876
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const/4 v5, 0x0

    move-object v1, p1

    move v3, p2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    .line 877
    return-void
.end method

.method public clickLongOnText(Ljava/lang/String;II)V
    .locals 6
    .parameter "text"
    .parameter "match"
    .parameter "time"

    .prologue
    const/4 v2, 0x1

    .line 905
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    move-object v1, p1

    move v3, p2

    move v4, v2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    .line 906
    return-void
.end method

.method public clickLongOnText(Ljava/lang/String;IZ)V
    .locals 6
    .parameter "text"
    .parameter "match"
    .parameter "scroll"

    .prologue
    .line 891
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const/4 v2, 0x1

    const/4 v5, 0x0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    .line 892
    return-void
.end method

.method public clickLongOnTextAndPress(Ljava/lang/String;I)V
    .locals 1
    .parameter "text"
    .parameter "index"

    .prologue
    .line 918
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Clicker;->clickLongOnTextAndPress(Ljava/lang/String;I)V

    .line 919
    return-void
.end method

.method public clickLongOnView(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 797
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(Landroid/view/View;ZI)V

    .line 799
    return-void
.end method

.method public clickLongOnView(Landroid/view/View;I)V
    .locals 2
    .parameter "view"
    .parameter "time"

    .prologue
    .line 810
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(Landroid/view/View;ZI)V

    .line 812
    return-void
.end method

.method public clickOnButton(I)V
    .locals 2
    .parameter "index"

    .prologue
    .line 929
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const-class v1, Landroid/widget/Button;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickOn(Ljava/lang/Class;I)V

    .line 930
    return-void
.end method

.method public clickOnButton(Ljava/lang/String;)V
    .locals 2
    .parameter "name"

    .prologue
    .line 684
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const-class v1, Landroid/widget/Button;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickOn(Ljava/lang/Class;Ljava/lang/String;)V

    .line 686
    return-void
.end method

.method public clickOnCheckBox(I)V
    .locals 2
    .parameter "index"

    .prologue
    .line 951
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const-class v1, Landroid/widget/CheckBox;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickOn(Ljava/lang/Class;I)V

    .line 952
    return-void
.end method

.method public clickOnEditText(I)V
    .locals 2
    .parameter "index"

    .prologue
    .line 962
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const-class v1, Landroid/widget/EditText;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickOn(Ljava/lang/Class;I)V

    .line 963
    return-void
.end method

.method public clickOnImage(I)V
    .locals 2
    .parameter "index"

    .prologue
    .line 1280
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const-class v1, Landroid/widget/ImageView;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickOn(Ljava/lang/Class;I)V

    .line 1281
    return-void
.end method

.method public clickOnImageButton(I)V
    .locals 2
    .parameter "index"

    .prologue
    .line 696
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const-class v1, Landroid/widget/ImageButton;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickOn(Ljava/lang/Class;I)V

    .line 697
    return-void
.end method

.method public clickOnMenuItem(Ljava/lang/String;)V
    .locals 1
    .parameter "text"

    .prologue
    .line 718
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnMenuItem(Ljava/lang/String;)V

    .line 719
    return-void
.end method

.method public clickOnMenuItem(Ljava/lang/String;Z)V
    .locals 1
    .parameter "text"
    .parameter "subMenu"

    .prologue
    .line 731
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnMenuItem(Ljava/lang/String;Z)V

    .line 732
    return-void
.end method

.method public clickOnRadioButton(I)V
    .locals 2
    .parameter "index"

    .prologue
    .line 940
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const-class v1, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickOn(Ljava/lang/Class;I)V

    .line 941
    return-void
.end method

.method public clickOnScreen(FF)V
    .locals 1
    .parameter "x"
    .parameter "y"

    .prologue
    .line 647
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 648
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(FF)V

    .line 649
    return-void
.end method

.method public clickOnText(Ljava/lang/String;)V
    .locals 6
    .parameter "text"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 823
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    move-object v1, p1

    move v4, v3

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    .line 824
    return-void
.end method

.method public clickOnText(Ljava/lang/String;I)V
    .locals 6
    .parameter "text"
    .parameter "match"

    .prologue
    const/4 v2, 0x0

    .line 835
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const/4 v4, 0x1

    move-object v1, p1

    move v3, p2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    .line 836
    return-void
.end method

.method public clickOnText(Ljava/lang/String;IZ)V
    .locals 6
    .parameter "text"
    .parameter "match"
    .parameter "scroll"

    .prologue
    const/4 v2, 0x0

    .line 848
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnText(Ljava/lang/String;ZIZI)V

    .line 849
    return-void
.end method

.method public clickOnToggleButton(Ljava/lang/String;)V
    .locals 2
    .parameter "name"

    .prologue
    .line 707
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    const-class v1, Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickOn(Ljava/lang/Class;Ljava/lang/String;)V

    .line 708
    return-void
.end method

.method public clickOnView(Landroid/view/View;)V
    .locals 2
    .parameter "view"

    .prologue
    .line 784
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/16 v1, 0x2710

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;I)Z

    .line 785
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->clicker:Lcom/jayway/android/robotium/solo/Clicker;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/Clicker;->clickOnScreen(Landroid/view/View;)V

    .line 786
    return-void
.end method

.method public drag(FFFFI)V
    .locals 6
    .parameter "fromX"
    .parameter "toX"
    .parameter "fromY"
    .parameter "toY"
    .parameter "stepCount"

    .prologue
    .line 1047
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Scroller;->drag(FFFFI)V

    .line 1048
    return-void
.end method

.method public enterText(ILjava/lang/String;)V
    .locals 3
    .parameter "index"
    .parameter "text"

    .prologue
    .line 1233
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->textEnterer:Lcom/jayway/android/robotium/solo/TextEnterer;

    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v2, Landroid/widget/EditText;

    invoke-virtual {v0, p1, v2}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v1, v0, p2}, Lcom/jayway/android/robotium/solo/TextEnterer;->setEditText(Landroid/widget/EditText;Ljava/lang/String;)V

    .line 1234
    return-void
.end method

.method public enterText(Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 2
    .parameter "editText"
    .parameter "text"

    .prologue
    .line 1245
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/16 v1, 0x2710

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;I)Z

    .line 1246
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->textEnterer:Lcom/jayway/android/robotium/solo/TextEnterer;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/TextEnterer;->setEditText(Landroid/widget/EditText;Ljava/lang/String;)V

    .line 1247
    return-void
.end method

.method public finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 1903
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->finalize()V

    .line 1904
    return-void
.end method

.method public finishOpenedActivities()V
    .locals 1

    .prologue
    .line 1913
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->finishOpenedActivities()V

    .line 1914
    return-void
.end method

.method public getActivityMonitor()Landroid/app/Instrumentation$ActivityMonitor;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getActivityMonitor()Landroid/app/Instrumentation$ActivityMonitor;

    move-result-object v0

    return-object v0
.end method

.method public getAllOpenedActivities()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 531
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getAllOpenedActivities()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getButton(I)Landroid/widget/Button;
    .locals 2
    .parameter "index"

    .prologue
    .line 1304
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/Button;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public getButton(Ljava/lang/String;)Landroid/widget/Button;
    .locals 3
    .parameter "text"

    .prologue
    .line 1377
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public getButton(Ljava/lang/String;Z)Landroid/widget/Button;
    .locals 2
    .parameter "text"
    .parameter "onlyVisible"

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/Button;

    invoke-virtual {v0, v1, p1, p2}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public getCurrentActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentButtons()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/Button;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentCheckBoxes()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/CheckBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1601
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentDatePickers()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/DatePicker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1627
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/DatePicker;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentEditTexts()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/EditText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1479
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentGridViews()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/GridView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentImageButtons()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1614
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentImageViews()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1465
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentListViews()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1492
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentProgressBars()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1666
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentRadioButtons()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/RadioButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1588
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentScrollViews()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ScrollView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1505
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentSlidingDrawers()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/SlidingDrawer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1653
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/SlidingDrawer;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentSpinners()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/Spinner;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1519
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentTextViews(Landroid/view/View;)Ljava/util/ArrayList;
    .locals 2
    .parameter "parent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1535
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentTimePickers()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TimePicker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1640
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/TimePicker;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentToggleButtons()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ToggleButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1575
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v1, Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentViews()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getViews(Landroid/view/View;Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getEditText(I)Landroid/widget/EditText;
    .locals 2
    .parameter "index"

    .prologue
    .line 1292
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/EditText;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method public getEditText(Ljava/lang/String;)Landroid/widget/EditText;
    .locals 3
    .parameter "text"

    .prologue
    .line 1402
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method public getEditText(Ljava/lang/String;Z)Landroid/widget/EditText;
    .locals 2
    .parameter "text"
    .parameter "onlyVisible"

    .prologue
    .line 1415
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/EditText;

    invoke-virtual {v0, v1, p1, p2}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method public getImage(I)Landroid/widget/ImageView;
    .locals 2
    .parameter "index"

    .prologue
    .line 1328
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/ImageView;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public getImageButton(I)Landroid/widget/ImageButton;
    .locals 2
    .parameter "index"

    .prologue
    .line 1340
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/ImageButton;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .parameter "resId"

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getText(I)Landroid/widget/TextView;
    .locals 2
    .parameter "index"

    .prologue
    .line 1316
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public getText(Ljava/lang/String;)Landroid/widget/TextView;
    .locals 3
    .parameter "text"

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public getText(Ljava/lang/String;Z)Landroid/widget/TextView;
    .locals 2
    .parameter "text"
    .parameter "onlyVisible"

    .prologue
    .line 1365
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    const-class v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1, p1, p2}, Lcom/jayway/android/robotium/solo/Getter;->getView(Ljava/lang/Class;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public getTopParent(Landroid/view/View;)Landroid/view/View;
    .locals 2
    .parameter "view"

    .prologue
    .line 191
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    invoke-virtual {v1, p1}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getTopParent(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 192
    .local v0, topParent:Landroid/view/View;
    return-object v0
.end method

.method public getView(I)Landroid/view/View;
    .locals 1
    .parameter "id"

    .prologue
    .line 1427
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->getter:Lcom/jayway/android/robotium/solo/Getter;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/Getter;->getView(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getView(Ljava/lang/Class;I)Landroid/view/View;
    .locals 1
    .parameter
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;I)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 1439
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v0, p2, p1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViews()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 158
    :try_start_0
    iget-object v2, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getViews(Landroid/view/View;Z)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 161
    :goto_0
    return-object v1

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getViews(Landroid/view/View;)Ljava/util/ArrayList;
    .locals 3
    .parameter "parent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    :try_start_0
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getViews(Landroid/view/View;Z)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 178
    :goto_0
    return-object v1

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 178
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public goBack()V
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->goBack()V

    .line 636
    return-void
.end method

.method public goBackToActivity(Ljava/lang/String;)V
    .locals 1
    .parameter "name"

    .prologue
    .line 1839
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/ActivityUtils;->goBackToActivity(Ljava/lang/String;)V

    .line 1840
    return-void
.end method

.method public isCheckBoxChecked(I)Z
    .locals 2
    .parameter "index"

    .prologue
    .line 1705
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    const-class v1, Landroid/widget/CheckBox;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Checker;->isButtonChecked(Ljava/lang/Class;I)Z

    move-result v0

    return v0
.end method

.method public isCheckBoxChecked(Ljava/lang/String;)Z
    .locals 2
    .parameter "text"

    .prologue
    .line 1744
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    const-class v1, Landroid/widget/CheckBox;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Checker;->isButtonChecked(Ljava/lang/Class;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isRadioButtonChecked(I)Z
    .locals 2
    .parameter "index"

    .prologue
    .line 1679
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    const-class v1, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Checker;->isButtonChecked(Ljava/lang/Class;I)Z

    move-result v0

    return v0
.end method

.method public isRadioButtonChecked(Ljava/lang/String;)Z
    .locals 2
    .parameter "text"

    .prologue
    .line 1692
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    const-class v1, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Checker;->isButtonChecked(Ljava/lang/Class;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSpinnerTextSelected(ILjava/lang/String;)Z
    .locals 1
    .parameter "index"
    .parameter "text"

    .prologue
    .line 1789
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Checker;->isSpinnerTextSelected(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSpinnerTextSelected(Ljava/lang/String;)Z
    .locals 1
    .parameter "text"

    .prologue
    .line 1776
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/Checker;->isSpinnerTextSelected(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isTextChecked(Ljava/lang/String;)Z
    .locals 4
    .parameter "text"

    .prologue
    const/4 v0, 0x1

    .line 1755
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v2, Landroid/widget/CheckedTextView;

    const-class v3, Landroid/widget/CompoundButton;

    invoke-virtual {v1, v2, v3}, Lcom/jayway/android/robotium/solo/Waiter;->waitForViews(Ljava/lang/Class;Ljava/lang/Class;)Z

    .line 1757
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v2, Landroid/widget/CheckedTextView;

    invoke-virtual {v1, v2}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    invoke-virtual {v1, p1}, Lcom/jayway/android/robotium/solo/Checker;->isCheckedTextChecked(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1763
    :cond_0
    :goto_0
    return v0

    .line 1760
    :cond_1
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const-class v2, Landroid/widget/CompoundButton;

    invoke-virtual {v1, v2}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    const-class v2, Landroid/widget/CompoundButton;

    invoke-virtual {v1, v2, p1}, Lcom/jayway/android/robotium/solo/Checker;->isButtonChecked(Ljava/lang/Class;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1763
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isToggleButtonChecked(I)Z
    .locals 2
    .parameter "index"

    .prologue
    .line 1731
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    const-class v1, Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Checker;->isButtonChecked(Ljava/lang/Class;I)Z

    move-result v0

    return v0
.end method

.method public isToggleButtonChecked(Ljava/lang/String;)Z
    .locals 2
    .parameter "text"

    .prologue
    .line 1718
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->checker:Lcom/jayway/android/robotium/solo/Checker;

    const-class v1, Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1, p1}, Lcom/jayway/android/robotium/solo/Checker;->isButtonChecked(Ljava/lang/Class;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public pressMenuItem(I)V
    .locals 1
    .parameter "index"

    .prologue
    .line 744
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->presser:Lcom/jayway/android/robotium/solo/Presser;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/Presser;->pressMenuItem(I)V

    .line 745
    return-void
.end method

.method public pressMenuItem(II)V
    .locals 1
    .parameter "index"
    .parameter "itemsPerRow"

    .prologue
    .line 758
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->presser:Lcom/jayway/android/robotium/solo/Presser;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Presser;->pressMenuItem(II)V

    .line 759
    return-void
.end method

.method public pressSpinnerItem(II)V
    .locals 1
    .parameter "spinnerIndex"
    .parameter "itemIndex"

    .prologue
    .line 772
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->presser:Lcom/jayway/android/robotium/solo/Presser;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Presser;->pressSpinnerItem(II)V

    .line 773
    return-void
.end method

.method public scrollDown()Z
    .locals 3

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v1, Landroid/widget/AbsListView;

    const-class v2, Landroid/widget/ScrollView;

    invoke-virtual {v0, v1, v2}, Lcom/jayway/android/robotium/solo/Waiter;->waitForViews(Ljava/lang/Class;Ljava/lang/Class;)Z

    .line 1060
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/Scroller;->scroll(I)Z

    move-result v0

    return v0
.end method

.method public scrollDownList(I)Z
    .locals 5
    .parameter "index"

    .prologue
    .line 1086
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    const-class v2, Landroid/widget/ListView;

    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v3, Landroid/widget/ListView;

    invoke-virtual {v0, p1, v3}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/jayway/android/robotium/solo/Scroller;->scrollList(Ljava/lang/Class;Landroid/widget/AbsListView;ILjava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method public scrollToSide(I)V
    .locals 2
    .parameter "side"

    .prologue
    .line 1109
    packed-switch p1, :pswitch_data_0

    .line 1113
    :goto_0
    return-void

    .line 1110
    :pswitch_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    sget-object v1, Lcom/jayway/android/robotium/solo/Scroller$Side;->RIGHT:Lcom/jayway/android/robotium/solo/Scroller$Side;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/Scroller;->scrollToSide(Lcom/jayway/android/robotium/solo/Scroller$Side;)V

    goto :goto_0

    .line 1111
    :pswitch_1
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    sget-object v1, Lcom/jayway/android/robotium/solo/Scroller$Side;->LEFT:Lcom/jayway/android/robotium/solo/Scroller$Side;

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/Scroller;->scrollToSide(Lcom/jayway/android/robotium/solo/Scroller$Side;)V

    goto :goto_0

    .line 1109
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public scrollUp()Z
    .locals 3

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v1, Landroid/widget/AbsListView;

    const-class v2, Landroid/widget/ScrollView;

    invoke-virtual {v0, v1, v2}, Lcom/jayway/android/robotium/solo/Waiter;->waitForViews(Ljava/lang/Class;Ljava/lang/Class;)Z

    .line 1074
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/Scroller;->scroll(I)Z

    move-result v0

    return v0
.end method

.method public scrollUpList(I)Z
    .locals 5
    .parameter "index"

    .prologue
    .line 1098
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    const-class v2, Landroid/widget/ListView;

    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v3, Landroid/widget/ListView;

    invoke-virtual {v0, p1, v3}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/jayway/android/robotium/solo/Scroller;->scrollList(Ljava/lang/Class;Landroid/widget/AbsListView;ILjava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method public searchButton(Ljava/lang/String;)Z
    .locals 6
    .parameter "text"

    .prologue
    const/4 v3, 0x0

    .line 352
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/Button;

    const/4 v4, 0x1

    move-object v2, p1

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchButton(Ljava/lang/String;I)Z
    .locals 6
    .parameter "text"
    .parameter "minimumNumberOfMatches"

    .prologue
    .line 395
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/Button;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchButton(Ljava/lang/String;IZ)Z
    .locals 6
    .parameter "text"
    .parameter "minimumNumberOfMatches"
    .parameter "onlyVisible"

    .prologue
    .line 412
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/Button;

    const/4 v4, 0x1

    move-object v2, p1

    move v3, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchButton(Ljava/lang/String;Z)Z
    .locals 6
    .parameter "text"
    .parameter "onlyVisible"

    .prologue
    .line 366
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/Button;

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v2, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchEditText(Ljava/lang/String;)Z
    .locals 6
    .parameter "text"

    .prologue
    const/4 v3, 0x1

    .line 338
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/EditText;

    const/4 v5, 0x0

    move-object v2, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchText(Ljava/lang/String;)Z
    .locals 6
    .parameter "text"

    .prologue
    const/4 v3, 0x0

    .line 441
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/TextView;

    const/4 v4, 0x1

    move-object v2, p1

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchText(Ljava/lang/String;I)Z
    .locals 6
    .parameter "text"
    .parameter "minimumNumberOfMatches"

    .prologue
    .line 471
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/TextView;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchText(Ljava/lang/String;IZ)Z
    .locals 6
    .parameter "text"
    .parameter "minimumNumberOfMatches"
    .parameter "scroll"

    .prologue
    .line 488
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/TextView;

    const/4 v5, 0x0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchText(Ljava/lang/String;IZZ)Z
    .locals 6
    .parameter "text"
    .parameter "minimumNumberOfMatches"
    .parameter "scroll"
    .parameter "onlyVisible"

    .prologue
    .line 506
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/TextView;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchText(Ljava/lang/String;Z)Z
    .locals 6
    .parameter "text"
    .parameter "onlyVisible"

    .prologue
    .line 455
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v2, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchToggleButton(Ljava/lang/String;)Z
    .locals 6
    .parameter "text"

    .prologue
    const/4 v3, 0x0

    .line 379
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/ToggleButton;

    const/4 v4, 0x1

    move-object v2, p1

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public searchToggleButton(Ljava/lang/String;I)Z
    .locals 6
    .parameter "text"
    .parameter "minimumNumberOfMatches"

    .prologue
    .line 428
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/ToggleButton;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchWithTimeoutFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public sendKey(I)V
    .locals 2
    .parameter "key"

    .prologue
    .line 1802
    sparse-switch p1, :sswitch_data_0

    .line 1825
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->sendKeyCode(I)V

    .line 1828
    :goto_0
    return-void

    .line 1804
    :sswitch_0
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->sendKeyCode(I)V

    goto :goto_0

    .line 1807
    :sswitch_1
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->sendKeyCode(I)V

    goto :goto_0

    .line 1810
    :sswitch_2
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->sendKeyCode(I)V

    goto :goto_0

    .line 1813
    :sswitch_3
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->sendKeyCode(I)V

    goto :goto_0

    .line 1816
    :sswitch_4
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    const/16 v1, 0x42

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->sendKeyCode(I)V

    goto :goto_0

    .line 1819
    :sswitch_5
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    const/16 v1, 0x52

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->sendKeyCode(I)V

    goto :goto_0

    .line 1822
    :sswitch_6
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->robotiumUtils:Lcom/jayway/android/robotium/solo/RobotiumUtils;

    const/16 v1, 0x43

    invoke-virtual {v0, v1}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->sendKeyCode(I)V

    goto :goto_0

    .line 1802
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
        0x42 -> :sswitch_4
        0x43 -> :sswitch_6
        0x52 -> :sswitch_5
    .end sparse-switch
.end method

.method public setActivityOrientation(I)V
    .locals 1
    .parameter "orientation"

    .prologue
    .line 519
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/ActivityUtils;->setActivityOrientation(I)V

    .line 520
    return-void
.end method

.method public setDatePicker(IIII)V
    .locals 2
    .parameter "index"
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    .prologue
    .line 1127
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v1, Landroid/widget/DatePicker;

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DatePicker;

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/jayway/android/robotium/solo/Solo;->setDatePicker(Landroid/widget/DatePicker;III)V

    .line 1128
    return-void
.end method

.method public setDatePicker(Landroid/widget/DatePicker;III)V
    .locals 2
    .parameter "datePicker"
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/16 v1, 0x2710

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;I)Z

    .line 1142
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->setter:Lcom/jayway/android/robotium/solo/Setter;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/jayway/android/robotium/solo/Setter;->setDatePicker(Landroid/widget/DatePicker;III)V

    .line 1143
    return-void
.end method

.method public setProgressBar(II)V
    .locals 2
    .parameter "index"
    .parameter "progress"

    .prologue
    .line 1181
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v1, Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0, p2}, Lcom/jayway/android/robotium/solo/Solo;->setProgressBar(Landroid/widget/ProgressBar;I)V

    .line 1182
    return-void
.end method

.method public setProgressBar(Landroid/widget/ProgressBar;I)V
    .locals 2
    .parameter "progressBar"
    .parameter "progress"

    .prologue
    .line 1193
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/16 v1, 0x2710

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;I)Z

    .line 1194
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->setter:Lcom/jayway/android/robotium/solo/Setter;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Setter;->setProgressBar(Landroid/widget/ProgressBar;I)V

    .line 1195
    return-void
.end method

.method public setSlidingDrawer(II)V
    .locals 2
    .parameter "index"
    .parameter "status"

    .prologue
    .line 1206
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v1, Landroid/widget/SlidingDrawer;

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SlidingDrawer;

    invoke-virtual {p0, v0, p2}, Lcom/jayway/android/robotium/solo/Solo;->setSlidingDrawer(Landroid/widget/SlidingDrawer;I)V

    .line 1207
    return-void
.end method

.method public setSlidingDrawer(Landroid/widget/SlidingDrawer;I)V
    .locals 2
    .parameter "slidingDrawer"
    .parameter "status"

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/16 v1, 0x2710

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;I)Z

    .line 1219
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->setter:Lcom/jayway/android/robotium/solo/Setter;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Setter;->setSlidingDrawer(Landroid/widget/SlidingDrawer;I)V

    .line 1220
    return-void
.end method

.method public setTimePicker(III)V
    .locals 2
    .parameter "index"
    .parameter "hour"
    .parameter "minute"

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const-class v1, Landroid/widget/TimePicker;

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TimePicker;

    invoke-virtual {p0, v0, p2, p3}, Lcom/jayway/android/robotium/solo/Solo;->setTimePicker(Landroid/widget/TimePicker;II)V

    .line 1156
    return-void
.end method

.method public setTimePicker(Landroid/widget/TimePicker;II)V
    .locals 2
    .parameter "timePicker"
    .parameter "hour"
    .parameter "minute"

    .prologue
    .line 1168
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/16 v1, 0x2710

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;I)Z

    .line 1169
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->setter:Lcom/jayway/android/robotium/solo/Setter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jayway/android/robotium/solo/Setter;->setTimePicker(Landroid/widget/TimePicker;II)V

    .line 1170
    return-void
.end method

.method public sleep(I)V
    .locals 1
    .parameter "time"

    .prologue
    .line 1891
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep(I)V

    .line 1892
    return-void
.end method

.method public waitForActivity(Ljava/lang/String;)Z
    .locals 2
    .parameter "name"

    .prologue
    .line 1851
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/16 v1, 0x4e20

    invoke-virtual {v0, p1, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForActivity(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public waitForActivity(Ljava/lang/String;I)Z
    .locals 1
    .parameter "name"
    .parameter "timeout"

    .prologue
    .line 1865
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/Waiter;->waitForActivity(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public waitForDialogToClose(J)Z
    .locals 1
    .parameter "timeout"

    .prologue
    .line 624
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->dialogUtils:Lcom/jayway/android/robotium/solo/DialogUtils;

    invoke-virtual {v0, p1, p2}, Lcom/jayway/android/robotium/solo/DialogUtils;->waitForDialogToClose(J)Z

    move-result v0

    return v0
.end method

.method public waitForText(Ljava/lang/String;)Z
    .locals 1
    .parameter "text"

    .prologue
    .line 205
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public waitForText(Ljava/lang/String;IJ)Z
    .locals 1
    .parameter "text"
    .parameter "minimumNumberOfMatches"
    .parameter "timeout"

    .prologue
    .line 220
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJ)Z

    move-result v0

    return v0
.end method

.method public waitForText(Ljava/lang/String;IJZ)Z
    .locals 6
    .parameter "text"
    .parameter "minimumNumberOfMatches"
    .parameter "timeout"
    .parameter "scroll"

    .prologue
    .line 235
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJZ)Z

    move-result v0

    return v0
.end method

.method public waitForText(Ljava/lang/String;IJZZ)Z
    .locals 7
    .parameter "text"
    .parameter "minimumNumberOfMatches"
    .parameter "timeout"
    .parameter "scroll"
    .parameter "onlyVisible"

    .prologue
    .line 251
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJZZ)Z

    move-result v0

    return v0
.end method

.method public waitForView(Landroid/view/View;)Z
    .locals 1
    .parameter "view"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v0, p1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public waitForView(Landroid/view/View;IZ)Z
    .locals 1
    .parameter "view"
    .parameter "timeout"
    .parameter "scroll"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "IZ)Z"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;IZ)Z

    move-result v0

    return v0
.end method

.method public waitForView(Ljava/lang/Class;)Z
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    const/4 v1, 0x0

    const/16 v2, 0x4e20

    const/4 v3, 0x1

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Ljava/lang/Class;IIZ)Z

    move-result v0

    return v0
.end method

.method public waitForView(Ljava/lang/Class;II)Z
    .locals 3
    .parameter
    .parameter "minimumNumberOfMatches"
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;II)Z"
        }
    .end annotation

    .prologue
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v2, 0x1

    .line 301
    add-int/lit8 v0, p2, -0x1

    .line 303
    .local v0, index:I
    if-ge v0, v2, :cond_0

    .line 304
    const/4 v0, 0x0

    .line 306
    :cond_0
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v1, p1, v0, p3, v2}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Ljava/lang/Class;IIZ)Z

    move-result v1

    return v1
.end method

.method public waitForView(Ljava/lang/Class;IIZ)Z
    .locals 2
    .parameter
    .parameter "minimumNumberOfMatches"
    .parameter "timeout"
    .parameter "scroll"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;IIZ)Z"
        }
    .end annotation

    .prologue
    .line 320
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    add-int/lit8 v0, p2, -0x1

    .line 322
    .local v0, index:I
    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 323
    const/4 v0, 0x0

    .line 325
    :cond_0
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/Solo;->waiter:Lcom/jayway/android/robotium/solo/Waiter;

    invoke-virtual {v1, p1, v0, p3, p4}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Ljava/lang/Class;IIZ)Z

    move-result v1

    return v1
.end method
