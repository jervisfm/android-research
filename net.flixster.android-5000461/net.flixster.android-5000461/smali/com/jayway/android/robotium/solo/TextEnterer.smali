.class Lcom/jayway/android/robotium/solo/TextEnterer;
.super Ljava/lang/Object;
.source "TextEnterer.java"


# instance fields
.field private final inst:Landroid/app/Instrumentation;


# direct methods
.method public constructor <init>(Landroid/app/Instrumentation;)V
    .locals 0
    .parameter "inst"

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/jayway/android/robotium/solo/TextEnterer;->inst:Landroid/app/Instrumentation;

    .line 28
    return-void
.end method


# virtual methods
.method public setEditText(Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 3
    .parameter "editText"
    .parameter "text"

    .prologue
    .line 39
    if-eqz p1, :cond_1

    .line 40
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, previousText:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/widget/EditText;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 42
    const-string v1, "Edit text is not enabled!"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    .line 44
    :cond_0
    iget-object v1, p0, Lcom/jayway/android/robotium/solo/TextEnterer;->inst:Landroid/app/Instrumentation;

    new-instance v2, Lcom/jayway/android/robotium/solo/TextEnterer$1;

    invoke-direct {v2, p0, p1, p2, v0}, Lcom/jayway/android/robotium/solo/TextEnterer$1;-><init>(Lcom/jayway/android/robotium/solo/TextEnterer;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->runOnMainSync(Ljava/lang/Runnable;)V

    .line 59
    .end local v0           #previousText:Ljava/lang/String;
    :cond_1
    return-void
.end method
