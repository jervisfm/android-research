.class Lcom/jayway/android/robotium/solo/Waiter;
.super Ljava/lang/Object;
.source "Waiter.java"


# instance fields
.field private final SMALLTIMEOUT:I

.field private final TIMEOUT:I

.field private final activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

.field private final scroller:Lcom/jayway/android/robotium/solo/Scroller;

.field private final searcher:Lcom/jayway/android/robotium/solo/Searcher;

.field private final sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

.field private final viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;


# direct methods
.method public constructor <init>(Lcom/jayway/android/robotium/solo/ActivityUtils;Lcom/jayway/android/robotium/solo/ViewFetcher;Lcom/jayway/android/robotium/solo/Searcher;Lcom/jayway/android/robotium/solo/Scroller;Lcom/jayway/android/robotium/solo/Sleeper;)V
    .locals 1
    .parameter "activityUtils"
    .parameter "viewFetcher"
    .parameter "searcher"
    .parameter "scroller"
    .parameter "sleeper"

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/jayway/android/robotium/solo/Waiter;->TIMEOUT:I

    .line 23
    const/16 v0, 0x2710

    iput v0, p0, Lcom/jayway/android/robotium/solo/Waiter;->SMALLTIMEOUT:I

    .line 40
    iput-object p1, p0, Lcom/jayway/android/robotium/solo/Waiter;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    .line 41
    iput-object p2, p0, Lcom/jayway/android/robotium/solo/Waiter;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    .line 42
    iput-object p3, p0, Lcom/jayway/android/robotium/solo/Waiter;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    .line 43
    iput-object p4, p0, Lcom/jayway/android/robotium/solo/Waiter;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    .line 44
    iput-object p5, p0, Lcom/jayway/android/robotium/solo/Waiter;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    .line 45
    return-void
.end method


# virtual methods
.method public waitForActivity(Ljava/lang/String;)Z
    .locals 1
    .parameter "name"

    .prologue
    .line 56
    const/16 v0, 0x2710

    invoke-virtual {p0, p1, v0}, Lcom/jayway/android/robotium/solo/Waiter;->waitForActivity(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public waitForActivity(Ljava/lang/String;I)Z
    .locals 6
    .parameter "name"
    .parameter "timeout"

    .prologue
    .line 70
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 71
    .local v2, now:J
    int-to-long v4, p2

    add-long v0, v2, v4

    .line 72
    .local v0, endTime:J
    :goto_0
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Waiter;->activityUtils:Lcom/jayway/android/robotium/solo/ActivityUtils;

    invoke-virtual {v4}, Lcom/jayway/android/robotium/solo/ActivityUtils;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    cmp-long v4, v2, v0

    if-gez v4, :cond_0

    .line 74
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    goto :goto_0

    .line 76
    :cond_0
    cmp-long v4, v2, v0

    if-gez v4, :cond_1

    .line 77
    const/4 v4, 0x1

    .line 80
    :goto_1
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public waitForAndGetView(ILjava/lang/Class;)Landroid/view/View;
    .locals 13
    .parameter "index"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .local p2, classToFilterBy:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v12, 0x1

    .line 346
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x2710

    add-long v1, v8, v10

    .line 347
    .local v1, endTime:J
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v8, v8, v1

    if-gtz v8, :cond_1

    invoke-virtual {p0, p2, p1, v12, v12}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Ljava/lang/Class;IZZ)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 348
    :cond_1
    iget-object v8, p0, Lcom/jayway/android/robotium/solo/Waiter;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    invoke-virtual {v8}, Lcom/jayway/android/robotium/solo/Searcher;->getNumberOfUniqueViews()I

    move-result v5

    .line 349
    .local v5, numberOfUniqueViews:I
    iget-object v8, p0, Lcom/jayway/android/robotium/solo/Waiter;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    invoke-virtual {v8, p2}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getCurrentViews(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-static {v8}, Lcom/jayway/android/robotium/solo/RobotiumUtils;->removeInvisibleViews(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v7

    .line 351
    .local v7, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v8, v5, :cond_2

    .line 352
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    sub-int v8, v5, v8

    sub-int v4, p1, v8

    .line 353
    .local v4, newIndex:I
    if-ltz v4, :cond_2

    .line 354
    move p1, v4

    .line 357
    .end local v4           #newIndex:I
    :cond_2
    const/4 v6, 0x0

    .line 359
    .local v6, view:Landroid/view/View;,"TT;"
    :try_start_0
    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Landroid/view/View;

    move-object v6, v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 363
    :goto_0
    const/4 v7, 0x0

    .line 364
    return-object v6

    .line 360
    :catch_0
    move-exception v3

    .line 361
    .local v3, exception:Ljava/lang/IndexOutOfBoundsException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " with index "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is not available!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public waitForText(Ljava/lang/String;)Z
    .locals 6
    .parameter "text"

    .prologue
    .line 257
    const/4 v2, 0x0

    const-wide/16 v3, 0x4e20

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJZ)Z

    move-result v0

    return v0
.end method

.method public waitForText(Ljava/lang/String;I)Z
    .locals 6
    .parameter "text"
    .parameter "expectedMinimumNumberOfMatches"

    .prologue
    .line 271
    const-wide/16 v3, 0x4e20

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJZ)Z

    move-result v0

    return v0
.end method

.method public waitForText(Ljava/lang/String;IJ)Z
    .locals 6
    .parameter "text"
    .parameter "expectedMinimumNumberOfMatches"
    .parameter "timeout"

    .prologue
    .line 286
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJZ)Z

    move-result v0

    return v0
.end method

.method public waitForText(Ljava/lang/String;IJZ)Z
    .locals 7
    .parameter "text"
    .parameter "expectedMinimumNumberOfMatches"
    .parameter "timeout"
    .parameter "scroll"

    .prologue
    .line 301
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/jayway/android/robotium/solo/Waiter;->waitForText(Ljava/lang/String;IJZZ)Z

    move-result v0

    return v0
.end method

.method public waitForText(Ljava/lang/String;IJZZ)Z
    .locals 10
    .parameter "text"
    .parameter "expectedMinimumNumberOfMatches"
    .parameter "timeout"
    .parameter "scroll"
    .parameter "onlyVisible"

    .prologue
    .line 317
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long v6, v0, p3

    .line 320
    .local v6, endTime:J
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    const/4 v9, 0x1

    .line 321
    .local v9, timedOut:Z
    :goto_0
    if-eqz v9, :cond_2

    .line 322
    const/4 v0, 0x0

    .line 330
    :goto_1
    return v0

    .line 320
    .end local v9           #timedOut:Z
    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    .line 325
    .restart local v9       #timedOut:Z
    :cond_2
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Waiter;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v0}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 327
    iget-object v0, p0, Lcom/jayway/android/robotium/solo/Waiter;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    const-class v1, Landroid/widget/TextView;

    move-object v2, p1

    move v3, p2

    move v4, p5

    move/from16 v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/jayway/android/robotium/solo/Searcher;->searchFor(Ljava/lang/Class;Ljava/lang/String;IZZ)Z

    move-result v8

    .line 329
    .local v8, foundAnyTextView:Z
    if-eqz v8, :cond_0

    .line 330
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public waitForView(I)Landroid/view/View;
    .locals 9
    .parameter "id"

    .prologue
    .line 231
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 232
    .local v6, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 233
    .local v3, startTime:J
    const-wide/16 v7, 0x2710

    add-long v0, v3, v7

    .line 234
    .local v0, endTime:J
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    cmp-long v7, v7, v0

    if-gtz v7, :cond_2

    .line 235
    iget-object v7, p0, Lcom/jayway/android/robotium/solo/Waiter;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v7}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 236
    iget-object v7, p0, Lcom/jayway/android/robotium/solo/Waiter;->viewFetcher:Lcom/jayway/android/robotium/solo/ViewFetcher;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/jayway/android/robotium/solo/ViewFetcher;->getAllViews(Z)Ljava/util/ArrayList;

    move-result-object v6

    .line 237
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 238
    .local v5, v:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v7

    if-ne v7, p1, :cond_1

    .line 239
    const/4 v6, 0x0

    .line 244
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v5           #v:Landroid/view/View;
    :goto_0
    return-object v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public waitForView(Landroid/view/View;)Z
    .locals 2
    .parameter "view"

    .prologue
    .line 177
    const/16 v0, 0x4e20

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;IZ)Z

    move-result v0

    return v0
.end method

.method public waitForView(Landroid/view/View;I)Z
    .locals 1
    .parameter "view"
    .parameter "timeout"

    .prologue
    .line 190
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Landroid/view/View;IZ)Z

    move-result v0

    return v0
.end method

.method public waitForView(Landroid/view/View;IZ)Z
    .locals 8
    .parameter "view"
    .parameter "timeout"
    .parameter "scroll"

    .prologue
    const/4 v5, 0x0

    .line 204
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 205
    .local v3, startTime:J
    int-to-long v6, p2

    add-long v0, v3, v6

    .line 207
    .local v0, endTime:J
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v6, v6, v0

    if-gez v6, :cond_1

    .line 208
    iget-object v6, p0, Lcom/jayway/android/robotium/solo/Waiter;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v6}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 210
    iget-object v6, p0, Lcom/jayway/android/robotium/solo/Waiter;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    invoke-virtual {v6, p1}, Lcom/jayway/android/robotium/solo/Searcher;->searchFor(Landroid/view/View;)Z

    move-result v2

    .line 212
    .local v2, foundAnyMatchingView:Z
    if-eqz v2, :cond_2

    .line 213
    const/4 v5, 0x1

    .line 219
    .end local v2           #foundAnyMatchingView:Z
    :cond_1
    return v5

    .line 216
    .restart local v2       #foundAnyMatchingView:Z
    :cond_2
    if-eqz p3, :cond_0

    .line 217
    iget-object v6, p0, Lcom/jayway/android/robotium/solo/Waiter;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    invoke-virtual {v6, v5}, Lcom/jayway/android/robotium/solo/Scroller;->scroll(I)Z

    goto :goto_0
.end method

.method public waitForView(Ljava/lang/Class;IIZ)Z
    .locals 9
    .parameter
    .parameter "index"
    .parameter "timeout"
    .parameter "scroll"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;IIZ)Z"
        }
    .end annotation

    .prologue
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v4, 0x0

    .line 123
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 124
    .local v3, uniqueViews:Ljava/util/Set;,"Ljava/util/Set<TT;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    int-to-long v7, p3

    add-long v0, v5, v7

    .line 127
    .local v0, endTime:J
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    cmp-long v5, v5, v0

    if-gez v5, :cond_1

    .line 128
    iget-object v5, p0, Lcom/jayway/android/robotium/solo/Waiter;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v5}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 130
    iget-object v5, p0, Lcom/jayway/android/robotium/solo/Waiter;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    invoke-virtual {v5, v3, p1, p2}, Lcom/jayway/android/robotium/solo/Searcher;->searchFor(Ljava/util/Set;Ljava/lang/Class;I)Z

    move-result v2

    .line 132
    .local v2, foundMatchingView:Z
    if-eqz v2, :cond_2

    .line 133
    const/4 v4, 0x1

    .line 138
    .end local v2           #foundMatchingView:Z
    :cond_1
    return v4

    .line 135
    .restart local v2       #foundMatchingView:Z
    :cond_2
    if-eqz p4, :cond_0

    .line 136
    iget-object v5, p0, Lcom/jayway/android/robotium/solo/Waiter;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    invoke-virtual {v5, v4}, Lcom/jayway/android/robotium/solo/Scroller;->scroll(I)Z

    goto :goto_0
.end method

.method public waitForView(Ljava/lang/Class;IZZ)Z
    .locals 4
    .parameter
    .parameter "index"
    .parameter "sleep"
    .parameter "scroll"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;IZZ)Z"
        }
    .end annotation

    .prologue
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v2, 0x0

    .line 92
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 96
    .local v1, uniqueViews:Ljava/util/Set;,"Ljava/util/Set<TT;>;"
    :cond_0
    if-eqz p3, :cond_1

    .line 97
    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Waiter;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v3}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    .line 99
    :cond_1
    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Waiter;->searcher:Lcom/jayway/android/robotium/solo/Searcher;

    invoke-virtual {v3, v1, p1, p2}, Lcom/jayway/android/robotium/solo/Searcher;->searchFor(Ljava/util/Set;Ljava/lang/Class;I)Z

    move-result v0

    .line 101
    .local v0, foundMatchingView:Z
    if-eqz v0, :cond_3

    .line 102
    const/4 v2, 0x1

    .line 108
    :cond_2
    :goto_0
    return v2

    .line 104
    :cond_3
    if-eqz p4, :cond_4

    iget-object v3, p0, Lcom/jayway/android/robotium/solo/Waiter;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    invoke-virtual {v3, v2}, Lcom/jayway/android/robotium/solo/Scroller;->scroll(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 107
    :cond_4
    if-nez p4, :cond_0

    goto :goto_0
.end method

.method public waitForViews(Ljava/lang/Class;Ljava/lang/Class;)Z
    .locals 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/view/View;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, viewClass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    .local p2, viewClass2:Ljava/lang/Class;,"Ljava/lang/Class<+Landroid/view/View;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 150
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x2710

    add-long v0, v4, v6

    .line 152
    .local v0, endTime:J
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v4, v4, v0

    if-gez v4, :cond_2

    .line 154
    invoke-virtual {p0, p1, v3, v3, v3}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Ljava/lang/Class;IZZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 164
    :cond_0
    :goto_1
    return v2

    .line 158
    :cond_1
    invoke-virtual {p0, p2, v3, v3, v3}, Lcom/jayway/android/robotium/solo/Waiter;->waitForView(Ljava/lang/Class;IZZ)Z

    move-result v4

    if-nez v4, :cond_0

    .line 161
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Waiter;->scroller:Lcom/jayway/android/robotium/solo/Scroller;

    invoke-virtual {v4, v3}, Lcom/jayway/android/robotium/solo/Scroller;->scroll(I)Z

    .line 162
    iget-object v4, p0, Lcom/jayway/android/robotium/solo/Waiter;->sleeper:Lcom/jayway/android/robotium/solo/Sleeper;

    invoke-virtual {v4}, Lcom/jayway/android/robotium/solo/Sleeper;->sleep()V

    goto :goto_0

    :cond_2
    move v2, v3

    .line 164
    goto :goto_1
.end method
