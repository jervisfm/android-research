.class public Lcom/widevine/drm/internal/JNI;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/widevine/drm/internal/JNI;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/widevine/drm/internal/JNI;
    .locals 1

    sget-object v0, Lcom/widevine/drm/internal/JNI;->a:Lcom/widevine/drm/internal/JNI;

    if-nez v0, :cond_0

    new-instance v0, Lcom/widevine/drm/internal/JNI;

    invoke-direct {v0}, Lcom/widevine/drm/internal/JNI;-><init>()V

    sput-object v0, Lcom/widevine/drm/internal/JNI;->a:Lcom/widevine/drm/internal/JNI;

    :cond_0
    sget-object v0, Lcom/widevine/drm/internal/JNI;->a:Lcom/widevine/drm/internal/JNI;

    return-object v0
.end method

.method private native cs(IILjava/lang/String;I)[Ljava/lang/String;
.end method

.method private native d(IILjava/nio/ByteBuffer;I)[Ljava/lang/String;
.end method

.method private native da(Ljava/lang/String;)V
.end method

.method private native gms(II[I)V
.end method

.method private native gn(II)I
.end method

.method private native gsid()I
.end method

.method private native hhe(IILjava/lang/String;)V
.end method

.method private native hhr(ILjava/lang/String;)I
.end method

.method private native ir()Z
.end method

.method private native os(IILjava/lang/String;IZ)[Ljava/lang/String;
.end method

.method private native qa(IILjava/lang/String;)I
.end method

.method private native qas(II)I
.end method

.method private native qra(IILjava/lang/String;)[Ljava/lang/String;
.end method

.method private native qras(II)[Ljava/lang/String;
.end method

.method private native ra(IILjava/lang/String;)I
.end method

.method private native rl(IIJJJ)I
.end method

.method private native rsid(I)V
.end method

.method private native sr(II[I)I
.end method

.method private native sr()Ljava/lang/String;
.end method

.method private native ss(Ljava/lang/String;)I
.end method

.method private native ua(IILjava/lang/String;)I
.end method

.method private native uc(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
.end method


# virtual methods
.method public final a(II)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/widevine/drm/internal/JNI;->gn(II)I

    move-result v0

    return v0
.end method

.method public final a(IIJJJ)I
    .locals 1

    invoke-direct/range {p0 .. p8}, Lcom/widevine/drm/internal/JNI;->rl(IIJJJ)I

    move-result v0

    return v0
.end method

.method public final a(IILjava/lang/String;)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/widevine/drm/internal/JNI;->ra(IILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(II[I)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/widevine/drm/internal/JNI;->sr(II[I)I

    move-result v0

    return v0
.end method

.method public final a(ILjava/lang/String;)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/widevine/drm/internal/JNI;->hhr(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/JNI;->ss(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/JNI;->rsid(I)V

    return-void
.end method

.method public final a(IILjava/lang/String;I)[Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/widevine/drm/internal/JNI;->cs(IILjava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(IILjava/lang/String;IZ)[Ljava/lang/String;
    .locals 1

    invoke-direct/range {p0 .. p5}, Lcom/widevine/drm/internal/JNI;->os(IILjava/lang/String;IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(IILjava/nio/ByteBuffer;I)[Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/widevine/drm/internal/JNI;->d(IILjava/nio/ByteBuffer;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    invoke-direct/range {p0 .. p15}, Lcom/widevine/drm/internal/JNI;->uc(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    invoke-direct {p0}, Lcom/widevine/drm/internal/JNI;->gsid()I

    move-result v0

    return v0
.end method

.method public final b(II)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/widevine/drm/internal/JNI;->qas(II)I

    move-result v0

    return v0
.end method

.method public final b(IILjava/lang/String;)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/widevine/drm/internal/JNI;->ua(IILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final b(II[I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/widevine/drm/internal/JNI;->gms(II[I)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/JNI;->da(Ljava/lang/String;)V

    return-void
.end method

.method public final c(IILjava/lang/String;)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/widevine/drm/internal/JNI;->qa(IILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/widevine/drm/internal/JNI;->sr()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(II)[Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/widevine/drm/internal/JNI;->qras(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    invoke-direct {p0}, Lcom/widevine/drm/internal/JNI;->ir()Z

    move-result v0

    return v0
.end method

.method public final d(IILjava/lang/String;)[Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/widevine/drm/internal/JNI;->qra(IILjava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/widevine/drm/internal/JNI;->hhe(IILjava/lang/String;)V

    return-void
.end method

.method public httpRequestCallback(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/widevine/drm/internal/h;

    invoke-direct {v0, p1, p2, p3}, Lcom/widevine/drm/internal/h;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public keyReceivedCallback(II)V
    .locals 2

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/widevine/drm/internal/a;->a(II)Lcom/widevine/drm/internal/x;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No task associated with event "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (hd:krc)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/widevine/drm/internal/m;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/widevine/drm/internal/a;->toString()Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->d()V

    goto :goto_0
.end method

.method public reportEventCallback(IIIILjava/lang/String;ZIZJJJZJJJJLjava/lang/String;)V
    .locals 6

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/widevine/drm/internal/a;->a(II)Lcom/widevine/drm/internal/x;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No task associated with event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p24

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (hd:rec)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/widevine/drm/internal/m;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/widevine/drm/internal/a;->toString()Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    invoke-static {p3}, Lcom/widevine/drm/internal/t;->a(I)Lcom/widevine/drm/internal/t;

    move-result-object v2

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    if-eqz p15, :cond_1

    const-string v4, "WVLicenseDurationRemainingKey"

    invoke-static/range {p20 .. p21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "WVPurchaseDurationRemainingKey"

    invoke-static/range {p18 .. p19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "WVPlaybackElapsedTimeKey"

    invoke-static/range {p22 .. p23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    if-eqz p8, :cond_2

    const-string v4, "WVSystemIDKey"

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "WVAssetIDKey"

    invoke-static/range {p11 .. p12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "WVKeyIDKey"

    invoke-static/range {p13 .. p14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "WVIsEncryptedKey"

    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_3

    const-string v4, "WVAssetPathKey"

    invoke-virtual {v3, v4, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "http"

    invoke-virtual {p5, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "WVAssetTypeKey"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_1
    if-lez p7, :cond_4

    const/4 v4, 0x3

    if-gt p7, v4, :cond_4

    const-string v4, "WVLicenseTypeKey"

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-virtual {v1, p6}, Lcom/widevine/drm/internal/x;->a(Z)V

    invoke-static {p4}, Lcom/widevine/drmapi/android/WVStatus;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/widevine/drm/internal/x;->a(Lcom/widevine/drm/internal/t;Lcom/widevine/drmapi/android/WVStatus;)Lcom/widevine/drmapi/android/WVEvent;

    move-result-object v2

    move-object/from16 v0, p24

    invoke-virtual {v1, v2, v4, v0, v3}, Lcom/widevine/drm/internal/x;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :cond_5
    const-string v4, "WVAssetTypeKey"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
