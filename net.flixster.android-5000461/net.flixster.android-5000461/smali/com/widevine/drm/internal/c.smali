.class public abstract Lcom/widevine/drm/internal/c;
.super Lcom/widevine/drm/internal/x;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/widevine/drm/internal/u;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/x;-><init>(Lcom/widevine/drm/internal/u;)V

    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/String;Lcom/widevine/drm/internal/t;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 6

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/c;->g()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {p2}, Lcom/widevine/drm/internal/t;->ordinal()I

    move-result v4

    invoke-virtual {p0}, Lcom/widevine/drm/internal/c;->h()Z

    move-result v5

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/widevine/drm/internal/JNI;->a(IILjava/lang/String;IZ)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->SystemCallError:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/widevine/drm/internal/c;->a:Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/widevine/drmapi/android/WVStatus;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/widevine/drm/internal/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected final a(Ljava/nio/ByteBuffer;I)[Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/c;->g()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/widevine/drm/internal/JNI;->a(IILjava/nio/ByteBuffer;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/String;Lcom/widevine/drm/internal/t;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 4

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/c;->g()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {p2}, Lcom/widevine/drm/internal/t;->ordinal()I

    move-result v3

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/widevine/drm/internal/JNI;->a(IILjava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->SystemCallError:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/widevine/drm/internal/c;->a:Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/widevine/drmapi/android/WVStatus;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    goto :goto_0
.end method
