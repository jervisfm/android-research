.class final Lcom/widevine/drm/internal/e;
.super Ljava/lang/Thread;


# instance fields
.field private a:Lcom/widevine/drm/internal/x;

.field private synthetic b:Lcom/widevine/drm/internal/d;


# direct methods
.method constructor <init>(Lcom/widevine/drm/internal/d;Lcom/widevine/drm/internal/x;)V
    .locals 0

    iput-object p1, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/widevine/drm/internal/e;->a:Lcom/widevine/drm/internal/x;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/widevine/drm/internal/d;->e:Z

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/widevine/drm/internal/d;->d:Z

    const/4 v0, 0x0

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v2

    iget-object v3, p0, Lcom/widevine/drm/internal/e;->a:Lcom/widevine/drm/internal/x;

    invoke-virtual {v2, v3}, Lcom/widevine/drm/internal/a;->a(Lcom/widevine/drm/internal/x;)Z

    iget-object v2, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-object v3, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-object v3, v3, Lcom/widevine/drm/internal/d;->h:Ljava/lang/String;

    sget-object v4, Lcom/widevine/drm/internal/t;->a:Lcom/widevine/drm/internal/t;

    invoke-virtual {v2, v3, v4}, Lcom/widevine/drm/internal/d;->a(Ljava/lang/String;Lcom/widevine/drm/internal/t;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v2

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v2, v3, :cond_0

    iget-object v3, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-virtual {v4}, Lcom/widevine/drm/internal/d;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/widevine/drm/internal/d;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-boolean v2, v2, Lcom/widevine/drm/internal/d;->e:Z

    if-eqz v2, :cond_f

    const/4 v4, 0x0

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-object v3, v3, Lcom/widevine/drm/internal/d;->a:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v2

    :try_start_1
    invoke-virtual {v2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    iget-object v3, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-object v3, v3, Lcom/widevine/drm/internal/d;->a:Ljava/net/ServerSocket;

    const/16 v5, 0x1388

    invoke-virtual {v3, v5}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    const/high16 v3, 0x1

    new-array v9, v3, [B

    iget-object v3, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    const/4 v5, 0x0

    iput-boolean v5, v3, Lcom/widevine/drm/internal/d;->j:Z
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    const/4 v3, 0x1

    :try_start_2
    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    const-wide/16 v5, 0x0

    iput-wide v5, v0, Lcom/widevine/drm/internal/d;->k:J

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/d;->a_()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v6, 0x1

    :cond_1
    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-static {v0}, Lcom/widevine/drm/internal/d;->a(Lcom/widevine/drm/internal/d;)V

    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-static {v0, v9}, Lcom/widevine/drm/internal/d;->a(Lcom/widevine/drm/internal/d;[B)I

    move-result v0

    if-lez v0, :cond_2

    iget-object v10, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-virtual {v10, v9, v0}, Lcom/widevine/drm/internal/d;->b([BI)I

    :cond_2
    move v0, v4

    move v4, v5

    :goto_1
    iget-object v5, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-boolean v5, v5, Lcom/widevine/drm/internal/d;->e:Z

    if-eqz v5, :cond_e

    if-nez v4, :cond_12

    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-virtual {v4}, Lcom/widevine/drm/internal/d;->a_()Z

    move-result v4

    if-nez v4, :cond_12

    add-int/lit8 v5, v6, 0x1

    const/4 v10, 0x5

    if-le v6, v10, :cond_3

    iget-object v6, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    sget-object v10, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v11, "serror (42)"

    invoke-virtual {v6, v10, v11}, Lcom/widevine/drm/internal/d;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_3
    move v6, v5

    move v5, v4

    :goto_2
    invoke-virtual {v7}, Ljava/io/InputStream;->available()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {v7, v9}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v10, -0x1

    if-eq v4, v10, :cond_e

    if-lez v4, :cond_4

    iget-object v10, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-static {v10, v9, v4}, Lcom/widevine/drm/internal/d;->a(Lcom/widevine/drm/internal/d;[BI)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    const/4 v0, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v8, v10, v11, v12}, Ljava/io/OutputStream;->write([BII)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "HTTP media player response:\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_4
    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-boolean v4, v4, Lcom/widevine/drm/internal/d;->c:Z

    if-nez v4, :cond_a

    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-static {v4, v9}, Lcom/widevine/drm/internal/d;->a(Lcom/widevine/drm/internal/d;[B)I

    move-result v4

    if-gez v4, :cond_9

    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-virtual {v4}, Lcom/widevine/drm/internal/d;->b()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v4, v10, v12

    if-lez v4, :cond_7

    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-static {v4}, Lcom/widevine/drm/internal/d;->b(Lcom/widevine/drm/internal/d;)J

    move-result-wide v12

    cmp-long v4, v12, v10

    if-lez v4, :cond_5

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/widevine/drm/internal/d;->j:Z

    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    :cond_5
    :goto_3
    const/4 v4, 0x0

    :goto_4
    if-lez v4, :cond_6

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_6
    move v4, v0

    :try_start_3
    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-boolean v0, v0, Lcom/widevine/drm/internal/d;->d:Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    if-nez v0, :cond_c

    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0xc8

    if-ge v0, v1, :cond_d

    const-wide/16 v10, 0xc8

    :try_start_4
    invoke-static {v10, v11}, Lcom/widevine/drm/internal/e;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    move v1, v0

    move v0, v4

    move v4, v5

    goto/16 :goto_1

    :cond_7
    :try_start_5
    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-static {v4}, Lcom/widevine/drm/internal/d;->b(Lcom/widevine/drm/internal/d;)J

    move-result-wide v10

    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-static {v4}, Lcom/widevine/drm/internal/d;->c(Lcom/widevine/drm/internal/d;)J

    move-result-wide v12

    cmp-long v4, v10, v12

    if-ltz v4, :cond_5

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/widevine/drm/internal/d;->j:Z

    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_3

    :catch_0
    move-exception v0

    move-object v0, v2

    move v2, v1

    move v1, v3

    :goto_5
    iget-object v3, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-wide v4, v3, Lcom/widevine/drm/internal/d;->k:J

    const-wide/16 v6, 0x1388

    add-long/2addr v4, v6

    iput-wide v4, v3, Lcom/widevine/drm/internal/d;->k:J

    if-nez v1, :cond_8

    iget-object v3, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    sget-object v4, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    const-string v5, "Mediaplayer initial connection timeout"

    invoke-virtual {v3, v4, v5}, Lcom/widevine/drm/internal/d;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_8
    move-object v14, v0

    move v0, v1

    move v1, v2

    move-object v2, v14

    :goto_6
    if-eqz v2, :cond_0

    :try_start_6
    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/widevine/drm/internal/m;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    :try_start_7
    iget-object v10, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    invoke-virtual {v10, v9, v4}, Lcom/widevine/drm/internal/d;->b([BI)I

    move-result v4

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "decrypt1: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto/16 :goto_4

    :catch_2
    move-exception v0

    :goto_7
    move v0, v3

    goto :goto_6

    :cond_a
    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-boolean v4, v4, Lcom/widevine/drm/internal/d;->d:Z

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    const/4 v10, 0x0

    invoke-virtual {v4, v9, v10}, Lcom/widevine/drm/internal/d;->b([BI)I

    move-result v4

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "decrypt2: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_4

    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_c
    move v0, v1

    :cond_d
    const-wide/16 v10, 0xa

    :try_start_8
    invoke-static {v10, v11}, Lcom/widevine/drm/internal/e;->sleep(J)V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    move v1, v0

    move v0, v4

    move v4, v5

    goto/16 :goto_1

    :catch_3
    move-exception v0

    move v0, v1

    :goto_8
    move v1, v0

    move v0, v4

    move v4, v5

    goto/16 :goto_1

    :cond_e
    :try_start_9
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    move v0, v3

    goto/16 :goto_6

    :cond_f
    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-boolean v0, v0, Lcom/widevine/drm/internal/d;->g:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-object v1, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-object v1, v1, Lcom/widevine/drm/internal/d;->h:Ljava/lang/String;

    sget-object v2, Lcom/widevine/drm/internal/t;->a:Lcom/widevine/drm/internal/t;

    invoke-virtual {v0, v1, v2}, Lcom/widevine/drm/internal/d;->b(Ljava/lang/String;Lcom/widevine/drm/internal/t;)Lcom/widevine/drmapi/android/WVStatus;

    :cond_10
    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-boolean v0, v0, Lcom/widevine/drm/internal/d;->f:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/widevine/drm/internal/d;->f:Z

    iget-object v0, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-object v1, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-object v1, v1, Lcom/widevine/drm/internal/d;->l:Lcom/widevine/drmapi/android/WVStatus;

    iget-object v2, p0, Lcom/widevine/drm/internal/e;->b:Lcom/widevine/drm/internal/d;

    iget-object v2, v2, Lcom/widevine/drm/internal/d;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/widevine/drm/internal/d;->b(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_11
    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v0

    iget-object v1, p0, Lcom/widevine/drm/internal/e;->a:Lcom/widevine/drm/internal/x;

    invoke-virtual {v0, v1}, Lcom/widevine/drm/internal/a;->b(Lcom/widevine/drm/internal/x;)Z

    return-void

    :catch_4
    move-exception v3

    move v3, v0

    goto :goto_7

    :catch_5
    move-exception v1

    move v1, v0

    goto :goto_7

    :catch_6
    move-exception v3

    move-object v14, v2

    move v2, v1

    move v1, v0

    move-object v0, v14

    goto/16 :goto_5

    :catch_7
    move-exception v3

    move-object v14, v2

    move v2, v1

    move v1, v0

    move-object v0, v14

    goto/16 :goto_5

    :catch_8
    move-exception v1

    move v1, v3

    move-object v14, v2

    move v2, v0

    move-object v0, v14

    goto/16 :goto_5

    :catch_9
    move-exception v1

    goto :goto_8

    :cond_12
    move v5, v4

    goto/16 :goto_2
.end method
