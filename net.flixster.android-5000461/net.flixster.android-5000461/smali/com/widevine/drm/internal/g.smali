.class final Lcom/widevine/drm/internal/g;
.super Ljava/lang/Thread;


# instance fields
.field private a:Lcom/widevine/drm/internal/x;

.field private synthetic b:Lcom/widevine/drm/internal/f;


# direct methods
.method constructor <init>(Lcom/widevine/drm/internal/f;Lcom/widevine/drm/internal/x;)V
    .locals 0

    iput-object p1, p0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/widevine/drm/internal/g;->a:Lcom/widevine/drm/internal/x;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 25

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/widevine/drm/internal/f;->e:Z

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/widevine/drm/internal/g;->a:Lcom/widevine/drm/internal/x;

    invoke-virtual {v7, v8}, Lcom/widevine/drm/internal/a;->a(Lcom/widevine/drm/internal/x;)Z

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-object v8, v8, Lcom/widevine/drm/internal/f;->h:Ljava/lang/String;

    sget-object v9, Lcom/widevine/drm/internal/t;->a:Lcom/widevine/drm/internal/t;

    invoke-virtual {v7, v8, v9}, Lcom/widevine/drm/internal/f;->a(Ljava/lang/String;Lcom/widevine/drm/internal/t;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v7

    sget-object v8, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v7, v8, :cond_0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-virtual {v9}, Lcom/widevine/drm/internal/f;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-boolean v7, v7, Lcom/widevine/drm/internal/f;->e:Z

    if-eqz v7, :cond_17

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    const/4 v8, 0x0

    iput-boolean v8, v7, Lcom/widevine/drm/internal/f;->c:Z

    const/4 v9, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    const/4 v10, 0x0

    iput-boolean v10, v7, Lcom/widevine/drm/internal/f;->d:Z

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    const-wide/16 v10, 0x0

    invoke-static {v7, v10, v11}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drm/internal/f;J)J

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-static {v7}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drm/internal/f;)J

    :try_start_0
    new-instance v7, Ljava/net/Socket;

    invoke-direct {v7}, Ljava/net/Socket;-><init>()V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_11
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_a

    :try_start_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-static {v8}, Lcom/widevine/drm/internal/f;->b(Lcom/widevine/drm/internal/f;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->getPort()I

    move-result v8

    const/4 v10, -0x1

    if-ne v8, v10, :cond_1

    const/16 v8, 0x50

    :cond_1
    new-instance v10, Ljava/net/InetSocketAddress;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-static {v11}, Lcom/widevine/drm/internal/f;->b(Lcom/widevine/drm/internal/f;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v8}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    const/16 v8, 0x1388

    invoke-virtual {v7, v10, v8}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-object v8, v8, Lcom/widevine/drm/internal/f;->a:Ljava/net/ServerSocket;

    invoke-virtual {v8}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_12
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b

    move-result-object v9

    :try_start_2
    invoke-virtual {v9}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v19

    invoke-virtual {v9}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v20

    invoke-virtual {v7}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-virtual {v7}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-object v11, v11, Lcom/widevine/drm/internal/f;->a:Ljava/net/ServerSocket;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    const/high16 v11, 0x1

    new-array v0, v11, [B

    move-object/from16 v18, v0

    const/4 v11, 0x2

    new-array v0, v11, [J

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/widevine/drm/internal/f;->j:Z
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_13
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b

    const/4 v11, 0x1

    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    const-wide/16 v12, 0x0

    iput-wide v12, v4, Lcom/widevine/drm/internal/f;->k:J

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-virtual {v12}, Lcom/widevine/drm/internal/f;->a_()Z

    move-result v16

    if-nez v16, :cond_2

    const/4 v4, 0x1

    :cond_2
    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    array-length v13, v5

    invoke-virtual {v12, v5, v13}, Lcom/widevine/drm/internal/f;->b([BI)I
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_14
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_c

    :cond_3
    const/4 v12, 0x0

    const/4 v15, 0x0

    move/from16 v13, v16

    move-object/from16 v24, v15

    move v15, v4

    move-object v4, v8

    move v8, v6

    move-object v6, v7

    move-object v7, v5

    move-object v5, v10

    move v10, v3

    move-object/from16 v3, v24

    :goto_1
    :try_start_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-boolean v14, v14, Lcom/widevine/drm/internal/f;->e:Z

    if-eqz v14, :cond_1d

    if-nez v13, :cond_1e

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-virtual {v13}, Lcom/widevine/drm/internal/f;->a_()Z

    move-result v13

    if-nez v13, :cond_1e

    add-int/lit8 v14, v15, 0x1

    const/16 v16, 0x5

    move/from16 v0, v16

    if-le v15, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    sget-object v16, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v17, "serror (32)"

    invoke-virtual/range {v15 .. v17}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_4
    move/from16 v16, v13

    move/from16 v17, v14

    :goto_2
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->available()I

    move-result v13

    if-gtz v13, :cond_5

    if-eqz v12, :cond_1c

    :cond_5
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->available()I

    move-result v13

    if-lez v13, :cond_7

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v13

    :goto_3
    const/4 v14, -0x1

    if-eq v13, v14, :cond_1d

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-static {v14, v0, v13, v1}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drm/internal/f;[BI[J)Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_1c

    new-array v3, v13, [B

    const/4 v12, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, v18

    invoke-static {v0, v12, v3, v15, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v4, v12, v13, v15}, Ljava/io/OutputStream;->write([BII)V

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "HTTP remote server request:\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/4 v12, 0x0

    move-object v15, v3

    move v3, v12

    :goto_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-boolean v12, v12, Lcom/widevine/drm/internal/f;->c:Z

    if-nez v12, :cond_13

    invoke-virtual {v5}, Ljava/io/InputStream;->available()I

    move-result v12

    if-lez v12, :cond_f

    const/4 v10, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    move-result v13

    if-lez v13, :cond_e

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/widevine/drm/internal/f;->a([BI)I

    move-result v12

    if-lez v12, :cond_1b

    new-instance v14, Ljava/lang/String;

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-direct {v14, v0, v1, v12}, Ljava/lang/String;-><init>([BII)V

    invoke-static {v14}, Lcom/widevine/drm/internal/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "HTTP redirection URL: "

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    if-eqz v22, :cond_1b

    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x1

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :try_start_5
    invoke-virtual {v6}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    new-instance v3, Ljava/net/Socket;

    invoke-direct {v3}, Ljava/net/Socket;-><init>()V
    :try_end_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :try_start_7
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v6, v0}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drm/internal/f;Landroid/net/Uri;)Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-static {v6}, Lcom/widevine/drm/internal/f;->b(Lcom/widevine/drm/internal/f;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-static {v6}, Lcom/widevine/drm/internal/f;->b(Lcom/widevine/drm/internal/f;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->getPort()I

    move-result v6

    if-nez v23, :cond_9

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    sget-object v12, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToMediaServer:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Invalid redirection: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_f
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_8

    move-object v6, v3

    move v3, v10

    :goto_6
    :try_start_8
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V

    invoke-virtual/range {v20 .. v20}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_10
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_9

    move v4, v11

    move-object v5, v7

    move-object v7, v6

    move v6, v8

    :goto_7
    if-eqz v9, :cond_6

    :try_start_9
    invoke-virtual {v9}, Ljava/net/Socket;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    :cond_6
    :goto_8
    if-eqz v7, :cond_0

    :try_start_a
    invoke-virtual {v7}, Ljava/net/Socket;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v7

    goto/16 :goto_0

    :cond_7
    :try_start_b
    array-length v13, v3

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, v18

    invoke-static {v3, v14, v0, v15, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_b
    .catch Ljava/net/SocketTimeoutException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3

    goto/16 :goto_3

    :catch_1
    move-exception v3

    move-object v3, v9

    move v4, v10

    move v5, v11

    :goto_9
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-wide v10, v9, Lcom/widevine/drm/internal/f;->k:J

    const-wide/16 v12, 0x1388

    add-long/2addr v10, v12

    iput-wide v10, v9, Lcom/widevine/drm/internal/f;->k:J

    if-nez v5, :cond_8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    sget-object v10, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    const-string v11, "Mediaplayer initial connection timeout"

    invoke-virtual {v9, v10, v11}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_8
    move-object v9, v3

    move v3, v4

    move v4, v5

    move-object v5, v7

    move-object v7, v6

    move v6, v8

    goto :goto_7

    :cond_9
    const/4 v4, -0x1

    if-ne v6, v4, :cond_1a

    const/16 v4, 0x50

    :goto_a
    :try_start_c
    new-instance v5, Ljava/net/InetSocketAddress;

    move-object/from16 v0, v23

    invoke-direct {v5, v0, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x1388

    invoke-virtual {v3, v5, v4}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-virtual {v3}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_c
    .catch Ljava/net/SocketTimeoutException; {:try_start_c .. :try_end_c} :catch_f
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    move-result-object v4

    move-object v6, v3

    move v3, v14

    move v14, v12

    move v12, v13

    :goto_b
    const/4 v13, 0x0

    if-lez v14, :cond_a

    :try_start_d
    new-instance v13, Ljava/lang/String;

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-direct {v13, v0, v1, v14}, Ljava/lang/String;-><init>([BII)V

    sub-int/2addr v12, v14

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v18

    move/from16 v2, v22

    invoke-static {v0, v14, v1, v2, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    if-lez v12, :cond_c

    if-nez v7, :cond_b

    move-object/from16 v7, v18

    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    move-object/from16 v0, v18

    invoke-virtual {v14, v0, v12}, Lcom/widevine/drm/internal/f;->b([BI)I

    move-result v12

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "decrypt: "

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_c
    if-eqz v13, :cond_d

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    move-object/from16 v0, v21

    invoke-static {v14, v13, v0}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drm/internal/f;Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_d

    invoke-virtual {v13}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    const/16 v22, 0x0

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v23

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v14, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/widevine/drm/internal/f;->c(Lcom/widevine/drm/internal/f;)J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-static {v14, v0, v1}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drm/internal/f;J)J

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "HTTP media player response:\n"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_d
    if-lez v12, :cond_e

    const/4 v13, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v13, v12}, Ljava/io/OutputStream;->write([BII)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    int-to-long v0, v12

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    invoke-static {v13, v0, v1}, Lcom/widevine/drm/internal/f;->b(Lcom/widevine/drm/internal/f;J)J
    :try_end_d
    .catch Ljava/net/SocketTimeoutException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3

    :cond_e
    move v12, v3

    move-object/from16 v24, v4

    move v4, v10

    move-object v10, v5

    move-object/from16 v5, v24

    :goto_c
    :try_start_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-boolean v3, v3, Lcom/widevine/drm/internal/f;->d:Z
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_e .. :try_end_e} :catch_d
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6

    if-nez v3, :cond_15

    add-int/lit8 v3, v8, 0x1

    const/16 v8, 0xc8

    if-ge v3, v8, :cond_16

    const-wide/16 v13, 0xc8

    :try_start_f
    invoke-static {v13, v14}, Lcom/widevine/drm/internal/g;->sleep(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_15
    .catch Ljava/net/SocketTimeoutException; {:try_start_f .. :try_end_f} :catch_e
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_7

    move/from16 v13, v16

    move v8, v3

    move-object v3, v15

    move/from16 v15, v17

    move-object/from16 v24, v10

    move v10, v4

    move-object v4, v5

    move-object/from16 v5, v24

    goto/16 :goto_1

    :cond_f
    :try_start_10
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-virtual {v12}, Lcom/widevine/drm/internal/f;->b()J

    move-result-wide v12

    const-wide/16 v22, 0x0

    cmp-long v14, v12, v22

    if-nez v14, :cond_10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-static {v12}, Lcom/widevine/drm/internal/f;->d(Lcom/widevine/drm/internal/f;)J

    move-result-wide v12

    :cond_10
    const-wide/16 v22, 0x0

    cmp-long v14, v12, v22

    if-lez v14, :cond_11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    invoke-static {v14}, Lcom/widevine/drm/internal/f;->e(Lcom/widevine/drm/internal/f;)J

    move-result-wide v22

    cmp-long v12, v22, v12

    if-ltz v12, :cond_11

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V

    invoke-virtual/range {v20 .. v20}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    const/4 v13, 0x1

    iput-boolean v13, v12, Lcom/widevine/drm/internal/f;->j:Z

    :cond_11
    add-int/lit8 v10, v10, 0xa

    const/16 v12, 0x4e20

    if-le v10, v12, :cond_12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    sget-object v13, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    const-string v14, "Media server connection timeout"

    invoke-virtual {v12, v13, v14}, Lcom/widevine/drm/internal/f;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_12
    move v12, v3

    move-object/from16 v24, v4

    move v4, v10

    move-object v10, v5

    move-object/from16 v5, v24

    goto :goto_c

    :cond_13
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-boolean v12, v12, Lcom/widevine/drm/internal/f;->d:Z

    if-eqz v12, :cond_14

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    const/4 v13, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v12, v0, v13}, Lcom/widevine/drm/internal/f;->b([BI)I

    move-result v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "decrypt (key received): "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    if-lez v12, :cond_14

    const/4 v13, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v13, v12}, Ljava/io/OutputStream;->write([BII)V
    :try_end_10
    .catch Ljava/net/SocketTimeoutException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_3

    :cond_14
    move v12, v3

    move-object/from16 v24, v4

    move v4, v10

    move-object v10, v5

    move-object/from16 v5, v24

    goto/16 :goto_c

    :cond_15
    move v3, v8

    :cond_16
    const-wide/16 v13, 0xa

    :try_start_11
    invoke-static {v13, v14}, Lcom/widevine/drm/internal/g;->sleep(J)V
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_11} :catch_15
    .catch Ljava/net/SocketTimeoutException; {:try_start_11 .. :try_end_11} :catch_e
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_7

    move/from16 v13, v16

    move v8, v3

    move-object v3, v15

    move/from16 v15, v17

    move-object/from16 v24, v10

    move v10, v4

    move-object v4, v5

    move-object/from16 v5, v24

    goto/16 :goto_1

    :catch_2
    move-exception v3

    move v3, v8

    :goto_d
    move/from16 v13, v16

    move v8, v3

    move-object v3, v15

    move/from16 v15, v17

    move-object/from16 v24, v10

    move v10, v4

    move-object v4, v5

    move-object/from16 v5, v24

    goto/16 :goto_1

    :catch_3
    move-exception v3

    :goto_e
    move v3, v10

    move v4, v11

    move-object v5, v7

    move-object v7, v6

    move v6, v8

    goto/16 :goto_7

    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-boolean v3, v3, Lcom/widevine/drm/internal/f;->g:Z

    if-eqz v3, :cond_18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-object v4, v4, Lcom/widevine/drm/internal/f;->h:Ljava/lang/String;

    sget-object v5, Lcom/widevine/drm/internal/t;->a:Lcom/widevine/drm/internal/t;

    invoke-virtual {v3, v4, v5}, Lcom/widevine/drm/internal/f;->b(Ljava/lang/String;Lcom/widevine/drm/internal/t;)Lcom/widevine/drmapi/android/WVStatus;

    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-boolean v3, v3, Lcom/widevine/drm/internal/f;->f:Z

    if-eqz v3, :cond_19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/widevine/drm/internal/f;->f:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-object v4, v4, Lcom/widevine/drm/internal/f;->l:Lcom/widevine/drmapi/android/WVStatus;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/widevine/drm/internal/g;->b:Lcom/widevine/drm/internal/f;

    iget-object v5, v5, Lcom/widevine/drm/internal/f;->m:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/widevine/drm/internal/f;->b(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_19
    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/g;->a:Lcom/widevine/drm/internal/x;

    invoke-virtual {v3, v4}, Lcom/widevine/drm/internal/a;->b(Lcom/widevine/drm/internal/x;)Z

    return-void

    :catch_4
    move-exception v3

    goto/16 :goto_5

    :catch_5
    move-exception v8

    goto/16 :goto_8

    :catch_6
    move-exception v3

    move v10, v4

    goto :goto_e

    :catch_7
    move-exception v5

    move v10, v4

    move v8, v3

    goto :goto_e

    :catch_8
    move-exception v4

    move-object v6, v3

    goto :goto_e

    :catch_9
    move-exception v4

    move v10, v3

    goto :goto_e

    :catch_a
    move-exception v7

    move v10, v3

    move v11, v4

    move-object v7, v5

    move-object/from16 v24, v8

    move v8, v6

    move-object/from16 v6, v24

    goto :goto_e

    :catch_b
    move-exception v8

    move v10, v3

    move v11, v4

    move v8, v6

    move-object v6, v7

    move-object v7, v5

    goto :goto_e

    :catch_c
    move-exception v4

    move v10, v3

    move v8, v6

    move-object v6, v7

    move-object v7, v5

    goto :goto_e

    :catch_d
    move-exception v3

    move-object v3, v9

    move v5, v11

    goto/16 :goto_9

    :catch_e
    move-exception v5

    move v5, v11

    move v8, v3

    move-object v3, v9

    goto/16 :goto_9

    :catch_f
    move-exception v4

    move-object v6, v3

    move v4, v10

    move v5, v11

    move-object v3, v9

    goto/16 :goto_9

    :catch_10
    move-exception v4

    move v4, v3

    move v5, v11

    move-object v3, v9

    goto/16 :goto_9

    :catch_11
    move-exception v7

    move-object v7, v5

    move v5, v4

    move v4, v3

    move-object v3, v9

    move-object/from16 v24, v8

    move v8, v6

    move-object/from16 v6, v24

    goto/16 :goto_9

    :catch_12
    move-exception v8

    move v8, v6

    move-object v6, v7

    move-object v7, v5

    move v5, v4

    move v4, v3

    move-object v3, v9

    goto/16 :goto_9

    :catch_13
    move-exception v8

    move v8, v6

    move-object v6, v7

    move-object v7, v5

    move v5, v4

    move v4, v3

    move-object v3, v9

    goto/16 :goto_9

    :catch_14
    move-exception v4

    move v4, v3

    move v8, v6

    move-object v6, v7

    move-object v3, v9

    move-object v7, v5

    move v5, v11

    goto/16 :goto_9

    :catch_15
    move-exception v8

    goto/16 :goto_d

    :cond_1a
    move v4, v6

    goto/16 :goto_a

    :cond_1b
    move v14, v12

    move v12, v13

    goto/16 :goto_b

    :cond_1c
    move-object v15, v3

    move v3, v12

    goto/16 :goto_4

    :cond_1d
    move v3, v10

    goto/16 :goto_6

    :cond_1e
    move/from16 v16, v13

    move/from16 v17, v15

    goto/16 :goto_2
.end method
