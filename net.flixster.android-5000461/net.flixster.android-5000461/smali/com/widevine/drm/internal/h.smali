.class public final Lcom/widevine/drm/internal/h;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:Ljava/net/URL;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/widevine/drm/internal/h;->a:I

    iput-object p3, p0, Lcom/widevine/drm/internal/h;->c:Ljava/lang/String;

    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/widevine/drm/internal/h;->b:Ljava/net/URL;

    new-instance v0, Lcom/widevine/drm/internal/i;

    invoke-direct {v0, p0}, Lcom/widevine/drm/internal/i;-><init>(Lcom/widevine/drm/internal/h;)V

    iput-object v0, p0, Lcom/widevine/drm/internal/h;->d:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/widevine/drm/internal/h;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "Malformed URL (hr:hr)"

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v1

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToDrmServer:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v2

    iget v3, p0, Lcom/widevine/drm/internal/h;->a:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/widevine/drm/internal/JNI;->e(IILjava/lang/String;)V

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 7

    new-array v2, p0, [B

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p0, :cond_2

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v3

    const-wide/high16 v5, 0x404f

    mul-double/2addr v3, v5

    double-to-int v0, v3

    int-to-byte v0, v0

    const/16 v3, 0x1a

    if-ge v0, v3, :cond_0

    add-int/lit8 v0, v0, 0x61

    int-to-byte v0, v0

    :goto_1
    aput-byte v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/16 v3, 0x34

    if-ge v0, v3, :cond_1

    add-int/lit8 v0, v0, -0x1a

    add-int/lit8 v0, v0, 0x41

    int-to-byte v0, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, -0x34

    add-int/lit8 v0, v0, 0x30

    int-to-byte v0, v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method static synthetic a(Lcom/widevine/drm/internal/h;)Ljava/net/URL;
    .locals 1

    iget-object v0, p0, Lcom/widevine/drm/internal/h;->b:Ljava/net/URL;

    return-object v0
.end method

.method static synthetic b(Lcom/widevine/drm/internal/h;)I
    .locals 1

    iget v0, p0, Lcom/widevine/drm/internal/h;->a:I

    return v0
.end method

.method static synthetic c(Lcom/widevine/drm/internal/h;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/widevine/drm/internal/h;->c:Ljava/lang/String;

    return-object v0
.end method
