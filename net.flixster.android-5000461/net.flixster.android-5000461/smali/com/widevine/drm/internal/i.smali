.class final Lcom/widevine/drm/internal/i;
.super Ljava/lang/Thread;


# instance fields
.field private synthetic a:Lcom/widevine/drm/internal/h;


# direct methods
.method synthetic constructor <init>(Lcom/widevine/drm/internal/h;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/widevine/drm/internal/i;-><init>(Lcom/widevine/drm/internal/h;B)V

    return-void
.end method

.method private constructor <init>(Lcom/widevine/drm/internal/h;B)V
    .locals 0

    iput-object p1, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v0}, Lcom/widevine/drm/internal/h;->a(Lcom/widevine/drm/internal/h;)Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getPort()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v0}, Lcom/widevine/drm/internal/h;->a(Lcom/widevine/drm/internal/h;)Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getDefaultPort()I

    move-result v0

    move v1, v0

    :goto_0
    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocketFactory;

    new-instance v2, Ljava/net/Socket;

    invoke-direct {v2}, Ljava/net/Socket;-><init>()V

    :try_start_0
    iget-object v3, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v3}, Lcom/widevine/drm/internal/h;->a(Lcom/widevine/drm/internal/h;)Ljava/net/URL;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    const-string v4, "https"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v2}, Lcom/widevine/drm/internal/h;->a(Lcom/widevine/drm/internal/h;)Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    const/4 v1, 0x0

    :try_start_1
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    const-string v5, "UTF8"

    invoke-direct {v2, v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    const/16 v4, 0x800

    invoke-direct {v3, v2, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "POST "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v4}, Lcom/widevine/drm/internal/h;->a(Lcom/widevine/drm/internal/h;)Ljava/net/URL;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " HTTP/1.0\r\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Content-Length: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v4}, Lcom/widevine/drm/internal/h;->c(Lcom/widevine/drm/internal/h;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\r\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Content-Type: application/x-www-form-urlencoded\r\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Host: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v4}, Lcom/widevine/drm/internal/h;->a(Lcom/widevine/drm/internal/h;)Ljava/net/URL;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\r\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\r\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V

    iget-object v4, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v4}, Lcom/widevine/drm/internal/h;->c(Lcom/widevine/drm/internal/h;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HTTPRequester (HTTP header):\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HTTPRequester (HTTP postdata): "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v4}, Lcom/widevine/drm/internal/h;->c(Lcom/widevine/drm/internal/h;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    new-instance v4, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v5, 0x800

    invoke-direct {v4, v2, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    const-string v1, "HTTP/1.[0-9][ \t]*20[0-9].*"

    invoke-virtual {v2, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    :goto_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    if-eqz v2, :cond_3

    move-object v1, v2

    goto :goto_2

    :cond_0
    :try_start_2
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v3}, Lcom/widevine/drm/internal/h;->a(Lcom/widevine/drm/internal/h;)Ljava/net/URL;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3, v1}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    const/16 v1, 0x1388

    invoke-virtual {v2, v0, v1}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v2

    goto/16 :goto_1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v1}, Lcom/widevine/drm/internal/h;->a(Lcom/widevine/drm/internal/h;)Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTPS setup failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (hr:r)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v1

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToDrmServer:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v3}, Lcom/widevine/drm/internal/h;->b(Lcom/widevine/drm/internal/h;)I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/widevine/drm/internal/JNI;->e(IILjava/lang/String;)V

    :goto_4
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTP setup failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (hr:r)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_2
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HTTP Error response from DRM server: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (hr:r)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v1

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->LicenseDenied:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v2

    iget-object v5, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v5}, Lcom/widevine/drm/internal/h;->b(Lcom/widevine/drm/internal/h;)I

    move-result v5

    invoke-virtual {v1, v2, v5, v0}, Lcom/widevine/drm/internal/JNI;->e(IILjava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTPS server request/response failed: ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), (hr:r)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v1

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToDrmServer:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v3}, Lcom/widevine/drm/internal/h;->b(Lcom/widevine/drm/internal/h;)I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/widevine/drm/internal/JNI;->e(IILjava/lang/String;)V

    goto/16 :goto_4

    :cond_3
    :try_start_4
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HTTPRequester: resp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :try_start_5
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :goto_5
    if-eqz v1, :cond_4

    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    const-string v0, "Empty response from DRM server (hr:r)"

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v1

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->LicenseDenied:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v3}, Lcom/widevine/drm/internal/h;->b(Lcom/widevine/drm/internal/h;)I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/widevine/drm/internal/JNI;->e(IILjava/lang/String;)V

    goto/16 :goto_4

    :cond_5
    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    iget-object v2, p0, Lcom/widevine/drm/internal/i;->a:Lcom/widevine/drm/internal/h;

    invoke-static {v2}, Lcom/widevine/drm/internal/h;->b(Lcom/widevine/drm/internal/h;)I

    move-result v2

    invoke-virtual {v0, v2, v1}, Lcom/widevine/drm/internal/JNI;->a(ILjava/lang/String;)I

    goto/16 :goto_4

    :catch_2
    move-exception v0

    goto :goto_5

    :cond_6
    move v1, v0

    goto/16 :goto_0
.end method
