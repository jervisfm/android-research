.class public final Lcom/widevine/drm/internal/j;
.super Lcom/widevine/drm/internal/x;


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/widevine/drm/internal/u;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/widevine/drm/internal/u;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/x;-><init>(Lcom/widevine/drm/internal/u;)V

    iput-object p2, p0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    return-void
.end method

.method private static a(I)C
    .locals 2

    rem-int/lit8 v0, p0, 0x3e

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_0

    add-int/lit8 v0, v0, 0x41

    :goto_0
    int-to-char v0, v0

    return v0

    :cond_0
    const/16 v1, 0x34

    if-ge v0, v1, :cond_1

    add-int/lit8 v0, v0, 0x47

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, -0x4

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/widevine/drm/internal/aa;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVPortalKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVPortalKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVDRMServer"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVDRMServer"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, ""

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVAssetDBPathKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVAssetDBPathKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    :cond_0
    const-string v6, ""

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVCAUserDataKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVCAUserDataKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v6, v1

    :cond_1
    const-string v7, ""

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVDeviceIDKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVDeviceIDKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v7, v1

    :cond_2
    const-string v8, ""

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVStreamIDKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVStreamIDKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v8, v1

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVLicenseTypeKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVLicenseTypeKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    :goto_0
    const-string v10, ""

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVAndroidIDKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVAndroidIDKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v10, v1

    :cond_4
    const-string v11, ""

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVIMEIKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVIMEIKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v11, v1

    :cond_5
    const-string v12, ""

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVWifiMacKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVWifiMacKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v12, v1

    :cond_6
    const-string v13, ""

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVHWDeviceKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVHWDeviceKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v13, v1

    :cond_7
    const-string v14, ""

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVHWModelKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVHWModelKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v14, v1

    :cond_8
    const-string v15, ""

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVUIDKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v2, "WVUIDKey"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v15, v1

    :cond_9
    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/widevine/drm/internal/j;->n:Lcom/widevine/drm/internal/u;

    invoke-virtual {v2}, Lcom/widevine/drm/internal/u;->g()I

    move-result v2

    move-object/from16 v16, p1

    invoke-virtual/range {v1 .. v16}, Lcom/widevine/drm/internal/JNI;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x2

    if-ge v2, v3, :cond_d

    new-instance v1, Lcom/widevine/drm/internal/aa;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->SystemCallError:Lcom/widevine/drmapi/android/WVStatus;

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Lcom/widevine/drm/internal/aa;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v1

    :cond_a
    new-instance v1, Lcom/widevine/drm/internal/aa;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->MandatorySettingAbsent:Lcom/widevine/drmapi/android/WVStatus;

    const-string v3, "WVPortalKey absent"

    invoke-direct {v1, v2, v3}, Lcom/widevine/drm/internal/aa;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v1

    :cond_b
    new-instance v1, Lcom/widevine/drm/internal/aa;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->MandatorySettingAbsent:Lcom/widevine/drmapi/android/WVStatus;

    const-string v3, "WVDRMServer absent"

    invoke-direct {v1, v2, v3}, Lcom/widevine/drm/internal/aa;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v1

    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_0

    :cond_d
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/widevine/drmapi/android/WVStatus;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v2

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v2, v3, :cond_e

    new-instance v3, Lcom/widevine/drm/internal/aa;

    const/4 v4, 0x1

    aget-object v1, v1, v4

    invoke-direct {v3, v2, v1}, Lcom/widevine/drm/internal/aa;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v3

    :cond_e
    return-void
.end method


# virtual methods
.method protected final a(Lcom/widevine/drm/internal/t;Lcom/widevine/drmapi/android/WVStatus;)Lcom/widevine/drmapi/android/WVEvent;
    .locals 1

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Initialized:Lcom/widevine/drmapi/android/WVEvent;

    return-object v0
.end method

.method public final a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/widevine/drm/internal/aa;
        }
    .end annotation

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0xe

    if-ge v0, v3, :cond_0

    const/16 v3, 0x3e

    invoke-virtual {v1, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/widevine/drm/internal/j;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final run()V
    .locals 10

    const/high16 v9, 0xf00

    const/high16 v8, -0x1000

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v0, "WVVersionKey"

    const-string v2, "5.0.0.5582"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/widevine/drm/internal/v;

    invoke-direct {v2}, Lcom/widevine/drm/internal/v;-><init>()V

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    iget-object v0, p0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v4, "WVAndroidIDKey"

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drm/internal/j;->a:Ljava/util/HashMap;

    const-string v4, "WVAndroidIDKey"

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    new-instance v4, Ljava/util/zip/CRC32;

    invoke-direct {v4}, Ljava/util/zip/CRC32;-><init>()V

    const/4 v5, 0x0

    array-length v6, v0

    invoke-virtual {v4, v0, v5, v6}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v4}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    and-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/util/Random;->setSeed(J)V

    :cond_0
    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    iget-object v4, p0, Lcom/widevine/drm/internal/j;->n:Lcom/widevine/drm/internal/u;

    invoke-virtual {v4}, Lcom/widevine/drm/internal/u;->g()I

    move-result v4

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lcom/widevine/drm/internal/JNI;->a(II)I

    move-result v0

    :try_start_0
    invoke-virtual {v2, v0}, Lcom/widevine/drm/internal/v;->a(I)I

    move-result v4

    if-nez v4, :cond_1

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v3, "serror (11) "

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/widevine/drm/internal/j;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v2, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "serror (12) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/widevine/drm/internal/j;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/widevine/drm/internal/j;->n:Lcom/widevine/drm/internal/u;

    invoke-virtual {v5}, Lcom/widevine/drm/internal/u;->e()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Lcom/widevine/drm/internal/v;->a(ILandroid/content/pm/ApplicationInfo;)I

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v3, "serror (13) "

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/widevine/drm/internal/j;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v2, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "serror (14) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/widevine/drm/internal/j;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto :goto_0

    :cond_2
    :try_start_2
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    and-int/lit8 v5, v0, 0xf

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/16 v5, 0x3e

    invoke-virtual {v3, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int v5, v4, v9

    ushr-int/lit8 v5, v5, 0x18

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/lit16 v5, v4, 0xf0

    ushr-int/lit8 v5, v5, 0x4

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/16 v5, 0x3e

    invoke-virtual {v3, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int v5, v0, v9

    ushr-int/lit8 v5, v5, 0x18

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/lit16 v5, v4, 0x3f00

    ushr-int/lit8 v5, v5, 0x8

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/lit16 v5, v0, 0xf0

    ushr-int/lit8 v5, v5, 0x4

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int v5, v4, v8

    ushr-int/lit8 v5, v5, 0x1c

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/16 v5, 0x3e

    invoke-virtual {v3, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/lit16 v5, v0, 0x3f00

    ushr-int/lit8 v5, v5, 0x8

    invoke-static {v5}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/lit8 v4, v4, 0xf

    invoke-static {v4}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/2addr v0, v8

    ushr-int/lit8 v0, v0, 0x1c

    invoke-static {v0}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/16 v0, 0x3e

    invoke-virtual {v3, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/widevine/drm/internal/j;->a(I)C

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/widevine/drm/internal/j;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/widevine/drm/internal/aa; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v0, p0, Lcom/widevine/drm/internal/j;->n:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->c()V

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Initialized:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/widevine/drm/internal/j;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    sget-object v2, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    iget-object v3, v0, Lcom/widevine/drm/internal/aa;->a:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/aa;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/widevine/drm/internal/j;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0
.end method
