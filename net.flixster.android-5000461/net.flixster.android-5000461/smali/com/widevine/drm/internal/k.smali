.class public abstract Lcom/widevine/drm/internal/k;
.super Lcom/widevine/drm/internal/c;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Lcom/widevine/drm/internal/u;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/c;-><init>(Lcom/widevine/drm/internal/u;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/widevine/drm/internal/k;->a:Z

    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/String;JJJ)V
    .locals 10

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->h()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToDrmServer:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Network inaccessible"

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/widevine/drm/internal/t;->b:Lcom/widevine/drm/internal/t;

    invoke-virtual {p0, p1, v0}, Lcom/widevine/drm/internal/k;->a(Ljava/lang/String;Lcom/widevine/drm/internal/t;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v9

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v9, v0, :cond_2

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {p0, v0, v9, v1, v2}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/widevine/drm/internal/k;->a:Z

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->g()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    move-wide v3, p2

    move-wide v5, p4

    move-wide/from16 v7, p6

    invoke-virtual/range {v0 .. v8}, Lcom/widevine/drm/internal/JNI;->a(IIJJJ)I

    move-result v0

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v9, v1, :cond_0

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-static {v0}, Lcom/widevine/drmapi/android/WVStatus;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    const-string v2, "License refresh failed (rlt:rl)"

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    const/16 v0, 0x1400

    const/16 v2, 0x2f

    const/4 v3, 0x1

    const/4 v1, 0x0

    new-array v5, v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->h()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToDrmServer:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Network inaccessible (rt:rl)"

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_2
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPort()I

    move-result v0

    const/4 v4, -0x1

    if-ne v0, v4, :cond_3

    const/16 v0, 0x50

    :cond_3
    :try_start_0
    new-instance v4, Ljava/net/Socket;

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7, v0}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GET "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "?"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " HTTP/1.1\r\nHost: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\r\nRange: bytes=0-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x1000

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\r\n\r\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_1
    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/4 v8, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v7, v2, v8, v0}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    invoke-virtual {v4}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v8

    move v4, v1

    move v0, v1

    :goto_1
    const/16 v2, 0xc00

    if-ge v0, v2, :cond_15

    :try_start_3
    invoke-virtual {v8}, Ljava/io/InputStream;->available()I

    move-result v2

    if-lez v2, :cond_5

    array-length v2, v5

    sub-int/2addr v2, v0

    invoke-virtual {v8, v5, v0, v2}, Ljava/io/InputStream;->read([BII)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    if-lez v0, :cond_6

    invoke-static {v5, v0}, Lcom/widevine/drm/internal/f;->a([BI)I

    move-result v2

    if-lez v2, :cond_6

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v5, v1, v2}, Ljava/lang/String;-><init>([BII)V

    invoke-static {v9}, Lcom/widevine/drm/internal/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    move v0, v1

    move-object p1, v2

    move v2, v3

    :goto_2
    :try_start_4
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b

    :goto_3
    if-nez v2, :cond_2

    invoke-static {v5, v0}, Lcom/widevine/drm/internal/f;->a([BI)I

    move-result v2

    sub-int/2addr v0, v2

    if-gtz v2, :cond_7

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No content "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "-"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " (rt:rl1)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {p0, v1, v3, v0, v2}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToMediaServer:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown host: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (rt:rl)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToMediaServer:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "new socket IOException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (rt:rl)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException while writing to media server: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (rt:rl)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :catch_3
    move-exception v0

    :try_start_5
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_a

    :goto_4
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException while retrieving input stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (rt:rl)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :catch_4
    move-exception v0

    :try_start_6
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_d

    :goto_5
    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException while retrieving data: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (rt:rl)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :cond_6
    const-wide/16 v9, 0xa

    :try_start_7
    invoke-static {v9, v10}, Lcom/widevine/drm/internal/k;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_5

    add-int/lit8 v2, v4, 0xa

    :goto_6
    const/16 v4, 0x2710

    if-le v2, v4, :cond_16

    :try_start_8
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_c

    :goto_7
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Media server taking too long to respond: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (rt:rl)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :catch_5
    move-exception v2

    move v2, v4

    goto :goto_6

    :cond_7
    if-gtz v0, :cond_8

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No content "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (rt:rl1)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :cond_8
    array-length v4, v5

    sub-int/2addr v4, v2

    if-le v0, v4, :cond_9

    array-length v0, v5

    sub-int/2addr v0, v2

    :cond_9
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    if-le v0, v4, :cond_a

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    :cond_a
    :try_start_9
    invoke-virtual {v6, v5, v2, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
    :try_end_9
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_9 .. :try_end_9} :catch_6

    :goto_8
    sget-object v2, Lcom/widevine/drm/internal/t;->b:Lcom/widevine/drm/internal/t;

    invoke-virtual {p0, p1, v2}, Lcom/widevine/drm/internal/k;->a(Ljava/lang/String;Lcom/widevine/drm/internal/t;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v2

    sget-object v4, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v2, v4, :cond_13

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v1, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :catch_6
    move-exception v1

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Index out of bounds "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "-"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " (rt:rl1)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {p0, v1, v3, v0, v2}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :cond_b
    const-string v0, "file://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v2, :cond_f

    :cond_c
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v2, :cond_f

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_d
    :goto_9
    :try_start_a
    const-string v0, "file://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "file://"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_e
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v0, "r"

    invoke-direct {v2, p1, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v0

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_7
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    if-gtz v0, :cond_10

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No data present in file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (rt:rl)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_9

    :catch_7
    move-exception v0

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->FileNotPresent:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "File "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not found, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (rt:rl)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :catch_8
    move-exception v0

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->FileNotPresent:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ") during file "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " read (rt:rl)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :cond_10
    array-length v2, v5

    if-le v0, v2, :cond_11

    array-length v0, v5

    :cond_11
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-le v0, v2, :cond_12

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    :cond_12
    const/4 v2, 0x0

    :try_start_b
    invoke-virtual {v6, v5, v2, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
    :try_end_b
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_b .. :try_end_b} :catch_9

    goto/16 :goto_8

    :catch_9
    move-exception v1

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Index ouf of bounds in file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (rt:rl)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :cond_13
    iput-boolean v3, p0, Lcom/widevine/drm/internal/k;->a:Z

    invoke-virtual {p0, v6, v0}, Lcom/widevine/drm/internal/k;->a(Ljava/nio/ByteBuffer;I)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    const/4 v4, 0x3

    if-ge v2, v4, :cond_14

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Decrypt call Error (rt:rl)"

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :cond_14
    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v2, v0, v3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x2

    aget-object v0, v0, v3

    packed-switch v1, :pswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Decryption Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (rt:rl)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :pswitch_0
    if-gez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Decryption Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (rt:rl)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :pswitch_1
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Content is not encrypted"

    invoke-virtual {p0}, Lcom/widevine/drm/internal/k;->c()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/widevine/drm/internal/k;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0

    :catch_a
    move-exception v1

    goto/16 :goto_4

    :catch_b
    move-exception v4

    goto/16 :goto_3

    :catch_c
    move-exception v0

    goto/16 :goto_7

    :catch_d
    move-exception v1

    goto/16 :goto_5

    :cond_15
    move v2, v1

    goto/16 :goto_2

    :cond_16
    move v4, v2

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/widevine/drm/internal/k;->a:Z

    return v0
.end method

.method protected abstract c()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method
