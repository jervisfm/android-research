.class public abstract Lcom/widevine/drm/internal/l;
.super Lcom/widevine/drm/internal/c;


# instance fields
.field protected a:Ljava/net/ServerSocket;

.field protected b:Ljava/lang/String;

.field protected c:Z

.field protected d:Z

.field protected e:Z

.field protected f:Z

.field protected g:Z

.field protected h:Ljava/lang/String;

.field protected i:Z

.field protected j:Z

.field protected k:J

.field protected l:Lcom/widevine/drmapi/android/WVStatus;

.field protected m:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:J

.field private q:Ljava/nio/ByteBuffer;

.field private r:J

.field private s:J

.field private t:Z


# direct methods
.method public constructor <init>(Lcom/widevine/drm/internal/u;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/widevine/drm/internal/aa;
        }
    .end annotation

    const/16 v5, 0xa

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/c;-><init>(Lcom/widevine/drm/internal/u;)V

    iput-wide v3, p0, Lcom/widevine/drm/internal/l;->p:J

    iput-boolean v2, p0, Lcom/widevine/drm/internal/l;->c:Z

    iput-boolean v2, p0, Lcom/widevine/drm/internal/l;->d:Z

    iput-boolean v2, p0, Lcom/widevine/drm/internal/l;->e:Z

    iput-boolean v2, p0, Lcom/widevine/drm/internal/l;->f:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/widevine/drm/internal/l;->g:Z

    iput-wide v3, p0, Lcom/widevine/drm/internal/l;->r:J

    const-wide/16 v0, 0x14

    iput-wide v0, p0, Lcom/widevine/drm/internal/l;->s:J

    iput-boolean v2, p0, Lcom/widevine/drm/internal/l;->t:Z

    iput-boolean v2, p0, Lcom/widevine/drm/internal/l;->j:Z

    iput-wide v3, p0, Lcom/widevine/drm/internal/l;->k:J

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    iput-object v0, p0, Lcom/widevine/drm/internal/l;->l:Lcom/widevine/drmapi/android/WVStatus;

    const-string v0, ""

    iput-object v0, p0, Lcom/widevine/drm/internal/l;->m:Ljava/lang/String;

    :try_start_0
    new-instance v0, Ljava/net/ServerSocket;

    invoke-direct {v0}, Ljava/net/ServerSocket;-><init>()V

    iput-object v0, p0, Lcom/widevine/drm/internal/l;->a:Ljava/net/ServerSocket;

    iget-object v0, p0, Lcom/widevine/drm/internal/l;->a:Ljava/net/ServerSocket;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    invoke-virtual {p1}, Lcom/widevine/drm/internal/u;->h()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drm/internal/l;->a:Ljava/net/ServerSocket;

    invoke-virtual {p1}, Lcom/widevine/drm/internal/u;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput-object p2, p0, Lcom/widevine/drm/internal/l;->h:Ljava/lang/String;

    const/16 v0, 0x14

    invoke-static {v0}, Lcom/widevine/drm/internal/h;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/widevine/drm/internal/l;->b:Ljava/lang/String;

    invoke-static {v5}, Lcom/widevine/drm/internal/h;->a(I)Ljava/lang/String;

    invoke-static {v5}, Lcom/widevine/drm/internal/h;->a(I)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://127.0.0.1:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/widevine/drm/internal/l;->a:Ljava/net/ServerSocket;

    invoke-virtual {v1}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/widevine/drm/internal/l;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/widevine/drm/internal/l;->o:Ljava/lang/String;

    const/high16 v0, 0x1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/widevine/drm/internal/l;->a:Ljava/net/ServerSocket;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->setSoTimeout(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Server socket error. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/widevine/drm/internal/aa;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->SystemCallError:Lcom/widevine/drmapi/android/WVStatus;

    invoke-direct {v1, v2, v0}, Lcom/widevine/drm/internal/aa;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v1
.end method

.method private a(Ljava/lang/String;)J
    .locals 8

    const-wide/16 v0, -0x1

    const-string v2, "[,:\\s\\t]+"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v3, v2, v3

    const/4 v4, 0x3

    aget-object v4, v2, v4

    const/4 v5, 0x4

    aget-object v5, v2, v5

    const/4 v6, 0x5

    aget-object v6, v2, v6

    const/16 v7, 0xc

    aget-object v2, v2, v7

    const-string v7, "0000000000000000FFFF00000100007F"

    invoke-virtual {v3, v7}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "0100007F"

    invoke-virtual {v3, v7}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    const-string v3, "0000000000000000FFFF00000100007F"

    invoke-virtual {v5, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "0100007F"

    invoke-virtual {v5, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    const/16 v3, 0x3f5

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/widevine/drm/internal/l;->a:Ljava/net/ServerSocket;

    invoke-virtual {v7}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x10

    :try_start_0
    invoke-static {v4, v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    const-wide v2, 0x100000000L

    or-long/2addr v0, v2

    :cond_2
    :goto_0
    return-wide v0

    :cond_3
    invoke-virtual {v2, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x10

    :try_start_1
    invoke-static {v6, v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v0

    const-wide v2, 0x200000000L

    or-long/2addr v0, v2

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method protected static a(Ljava/lang/String;[J)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "Range: bytes="

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v2, v2, 0xd

    const/16 v3, 0x2d

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    const/16 v4, 0xd

    invoke-virtual {p0, v4, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    if-eq v3, v1, :cond_2

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    aput-wide v5, p1, v0

    add-int/lit8 v2, v3, 0x1

    if-ge v2, v4, :cond_1

    add-int/lit8 v2, v3, 0x1

    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    aget-wide v4, p1, v0

    cmp-long v0, v4, v2

    if-gez v0, :cond_1

    aget-wide v4, p1, v1

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    aput-wide v2, p1, v1

    :cond_1
    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, p1, v0

    goto :goto_1
.end method


# virtual methods
.method protected final a(Lcom/widevine/drm/internal/t;Lcom/widevine/drmapi/android/WVStatus;)Lcom/widevine/drmapi/android/WVEvent;
    .locals 1

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne p2, v0, :cond_0

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Playing:Lcom/widevine/drmapi/android/WVEvent;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->PlayFailed:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0
.end method

.method public a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput-boolean v0, p0, Lcom/widevine/drm/internal/l;->t:Z

    iput-boolean v0, p0, Lcom/widevine/drm/internal/l;->f:Z

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->AlreadyPlaying:Lcom/widevine/drmapi/android/WVStatus;

    if-ne p1, v0, :cond_0

    iput-boolean v1, p0, Lcom/widevine/drm/internal/l;->g:Z

    :cond_0
    iput-boolean v1, p0, Lcom/widevine/drm/internal/l;->e:Z

    iput-object p1, p0, Lcom/widevine/drm/internal/l;->l:Lcom/widevine/drmapi/android/WVStatus;

    iput-object p2, p0, Lcom/widevine/drm/internal/l;->m:Ljava/lang/String;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/widevine/drm/internal/l;->i:Z

    return-void
.end method

.method protected final a([J)V
    .locals 11

    const/4 v10, 0x2

    const/16 v9, 0x20

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-wide/16 v5, -0x1

    const/4 v0, 0x4

    new-array v0, v0, [I

    aget-wide v1, p1, v7

    shr-long/2addr v1, v9

    and-long/2addr v1, v5

    long-to-int v1, v1

    aput v1, v0, v7

    aget-wide v1, p1, v7

    and-long/2addr v1, v5

    long-to-int v1, v1

    aput v1, v0, v8

    aget-wide v1, p1, v8

    shr-long/2addr v1, v9

    and-long/2addr v1, v5

    long-to-int v1, v1

    aput v1, v0, v10

    const/4 v1, 0x3

    aget-wide v2, p1, v8

    and-long/2addr v2, v5

    long-to-int v2, v2

    aput v2, v0, v1

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v1

    invoke-virtual {p0}, Lcom/widevine/drm/internal/c;->g()I

    move-result v2

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/widevine/drm/internal/JNI;->a(II[I)I

    aget v1, v0, v7

    int-to-long v1, v1

    and-long/2addr v1, v5

    shl-long/2addr v1, v9

    aget v3, v0, v8

    int-to-long v3, v3

    and-long/2addr v3, v5

    add-long/2addr v1, v3

    aput-wide v1, p1, v7

    aget v1, v0, v10

    int-to-long v1, v1

    and-long/2addr v1, v5

    shl-long/2addr v1, v9

    const/4 v3, 0x3

    aget v0, v0, v3

    int-to-long v3, v0

    and-long/2addr v3, v5

    add-long v0, v1, v3

    aput-wide v0, p1, v8

    return-void
.end method

.method protected final a_()Z
    .locals 20

    const/4 v1, 0x2

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/proc/net/tcp"

    aput-object v2, v7, v1

    const/4 v1, 0x1

    const-string v2, "/proc/net/tcp6"

    aput-object v2, v7, v1

    const-wide/16 v4, -0x1

    const-wide/16 v2, -0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LocalHostProxy: mediaPlayerProcessCheck: Pid: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ", Uid: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ", port: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/widevine/drm/internal/l;->a:Ljava/net/ServerSocket;

    invoke-virtual {v6}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    array-length v6, v7

    if-ge v1, v6, :cond_1

    :try_start_0
    new-instance v8, Ljava/io/RandomAccessFile;

    aget-object v6, v7, v1

    const-string v9, "r"

    invoke-direct {v8, v6, v9}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v6

    :goto_1
    if-eqz v6, :cond_0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "LocalHostProxy: mediaPlayerProcessCheck: line: \n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/widevine/drm/internal/l;->a(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v6, v9, v11

    if-ltz v6, :cond_5

    const-wide v11, 0x100000000L

    and-long/2addr v11, v9

    const-wide/16 v13, 0x0

    cmp-long v6, v11, v13

    if-eqz v6, :cond_4

    const-wide v4, 0x100000000L

    xor-long v5, v9, v4

    :goto_2
    const-wide v11, 0x200000000L

    and-long/2addr v11, v9

    const-wide/16 v13, 0x0

    cmp-long v4, v11, v13

    if-eqz v4, :cond_3

    const-wide v2, 0x200000000L

    xor-long/2addr v2, v9

    move-wide v3, v2

    :goto_3
    :try_start_1
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v2

    move-object v15, v2

    move-wide/from16 v16, v3

    move-wide/from16 v2, v16

    move-wide/from16 v18, v5

    move-wide/from16 v4, v18

    move-object v6, v15

    goto :goto_1

    :cond_0
    :try_start_2
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v6

    :goto_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/widevine/drm/internal/m;->a(Ljava/lang/String;)V

    goto :goto_4

    :catch_1
    move-exception v6

    :goto_6
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/widevine/drm/internal/m;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_2

    cmp-long v1, v4, v2

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_7
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_7

    :catch_2
    move-exception v2

    move-object v15, v2

    move-wide/from16 v16, v3

    move-wide/from16 v2, v16

    move-wide/from16 v18, v5

    move-wide/from16 v4, v18

    move-object v6, v15

    goto :goto_6

    :catch_3
    move-exception v2

    move-object v15, v2

    move-wide/from16 v16, v3

    move-wide/from16 v2, v16

    move-wide/from16 v18, v5

    move-wide/from16 v4, v18

    move-object v6, v15

    goto :goto_5

    :cond_3
    move-wide v3, v2

    goto/16 :goto_3

    :cond_4
    move-wide v5, v4

    goto/16 :goto_2

    :cond_5
    move-wide v15, v2

    move-wide/from16 v17, v4

    move-wide/from16 v5, v17

    move-wide v3, v15

    goto/16 :goto_3
.end method

.method protected final b([BI)I
    .locals 10

    const/4 v0, -0x1

    const/high16 v9, -0x100

    const/4 v8, 0x1

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/widevine/drm/internal/l;->r:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/widevine/drm/internal/l;->r:J

    iget-wide v4, p0, Lcom/widevine/drm/internal/l;->s:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    new-instance v2, Lcom/widevine/drm/internal/v;

    invoke-direct {v2}, Lcom/widevine/drm/internal/v;-><init>()V

    :try_start_0
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/widevine/drm/internal/v;->a(I)I

    move-result v2

    if-nez v2, :cond_0

    sget-object v4, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v5, "serror (21)"

    invoke-virtual {p0, v4, v5}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_0
    and-int/lit16 v4, v2, 0x3f00

    ushr-int/lit8 v4, v4, 0x8

    rem-int/lit8 v5, v4, 0x5

    if-eq v5, v8, :cond_1

    sget-object v5, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v6, "serror (25)"

    invoke-virtual {p0, v5, v6}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_1
    const v5, 0xff00

    and-int/2addr v5, v3

    ushr-int/lit8 v5, v5, 0x8

    add-int/lit8 v5, v5, 0x0

    rem-int/lit16 v5, v5, 0xff

    add-int/lit8 v6, v5, 0x0

    rem-int/lit16 v6, v6, 0xff

    const/high16 v7, 0xff

    and-int/2addr v7, v3

    ushr-int/lit8 v7, v7, 0x10

    add-int/2addr v5, v7

    rem-int/lit16 v5, v5, 0xff

    add-int/2addr v6, v5

    rem-int/lit16 v6, v6, 0xff

    and-int/lit16 v7, v3, 0xff

    add-int/2addr v5, v7

    rem-int/lit16 v5, v5, 0xff

    add-int/2addr v6, v5

    rem-int/lit16 v6, v6, 0xff

    and-int/2addr v3, v9

    ushr-int/lit8 v3, v3, 0x18

    add-int/2addr v3, v5

    rem-int/lit16 v3, v3, 0xff

    add-int v5, v6, v3

    rem-int/lit16 v5, v5, 0xff

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v3, v4

    rem-int/lit16 v3, v3, 0xff

    add-int v4, v5, v3

    rem-int/lit16 v4, v4, 0xff

    add-int/lit8 v3, v3, 0x7b

    rem-int/lit16 v3, v3, 0xff

    add-int/2addr v4, v3

    rem-int/lit16 v4, v4, 0xff

    and-int v5, v2, v9

    ushr-int/lit8 v5, v5, 0x18

    if-eq v5, v3, :cond_2

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v5, "serror (26)"

    invoke-virtual {p0, v3, v5}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_2
    and-int/lit16 v2, v2, 0xff

    if-eq v2, v4, :cond_3

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v3, "serror (27)"

    invoke-virtual {p0, v2, v3}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    iget-wide v2, p0, Lcom/widevine/drm/internal/l;->s:J

    const-wide/16 v4, 0x9f

    cmp-long v2, v2, v4

    if-gez v2, :cond_4

    iget-wide v2, p0, Lcom/widevine/drm/internal/l;->s:J

    const-wide/16 v4, 0x2

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lcom/widevine/drm/internal/l;->s:J

    :cond_4
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/widevine/drm/internal/l;->r:J

    :cond_5
    iget-object v2, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v2, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    if-gez p2, :cond_7

    :cond_6
    :goto_1
    :sswitch_0
    return v0

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "serror (22): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v3, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    array-length v2, p1

    if-le p2, v2, :cond_8

    array-length p2, p1

    :cond_8
    iget-object v2, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-le p2, v2, :cond_9

    iget-object v2, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result p2

    :cond_9
    iget-object v2, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, p1, v1, p2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v2, p2}, Lcom/widevine/drm/internal/l;->a(Ljava/nio/ByteBuffer;I)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x3

    if-lt v3, v4, :cond_6

    aget-object v0, v2, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aget-object v0, v2, v8

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v4, 0x2

    aget-object v2, v2, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "decrypt: parseResult: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bytes: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sparse-switch v3, :sswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "decrypt returned error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (lhp:d)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/widevine/drm/internal/m;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_1
    iget-object v2, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    if-gt v0, v2, :cond_a

    if-gez v0, :cond_b

    :cond_a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "decrypt error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " (lhp:d)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/widevine/drm/internal/m;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    move v0, v1

    :goto_2
    iput-boolean v1, p0, Lcom/widevine/drm/internal/l;->c:Z

    goto/16 :goto_1

    :cond_b
    iget-object v2, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v2, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v2, p0, Lcom/widevine/drm/internal/l;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, p1, v1, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    goto :goto_2

    :sswitch_2
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Unsupported file format (lhp:d)"

    invoke-virtual {p0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_3
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Unsupported data format (lhp:d)"

    invoke-virtual {p0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_4
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Decode error (lhp:d)"

    invoke-virtual {p0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_5
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "serror (23) (lhp:d)"

    invoke-virtual {p0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_6
    iput-boolean v8, p0, Lcom/widevine/drm/internal/l;->c:Z

    goto/16 :goto_1

    :sswitch_7
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OutOfMemoryError:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Write error (lhp:d)"

    invoke-virtual {p0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_8
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OutOfMemoryError:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Unable to reserve bytes (lhp:d)"

    invoke-virtual {p0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_9
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->LicenseExpired:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {p0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_a
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->ClockTamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "serror (24) (lhp:d)"

    invoke-virtual {p0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_b
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->HeartbeatError:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {p0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_c
    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->AlreadyPlaying:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Another playback command has been requested"

    invoke-virtual {p0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_8
        0x9 -> :sswitch_7
        0x10 -> :sswitch_0
        0x64 -> :sswitch_9
        0x65 -> :sswitch_a
        0x66 -> :sswitch_b
        0x67 -> :sswitch_b
        0x68 -> :sswitch_b
        0x6a -> :sswitch_c
    .end sparse-switch
.end method

.method protected final b()J
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-wide v0, p0, Lcom/widevine/drm/internal/l;->p:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [I

    aput v4, v0, v4

    aput v4, v0, v5

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v1

    invoke-virtual {p0}, Lcom/widevine/drm/internal/l;->g()I

    move-result v2

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/widevine/drm/internal/JNI;->b(II[I)V

    aget v1, v0, v4

    int-to-long v1, v1

    const/16 v3, 0x20

    shl-long/2addr v1, v3

    aget v0, v0, v5

    int-to-long v3, v0

    add-long v0, v1, v3

    iput-wide v0, p0, Lcom/widevine/drm/internal/l;->p:J

    :cond_0
    iget-wide v0, p0, Lcom/widevine/drm/internal/l;->p:J

    return-wide v0
.end method

.method public final b(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/widevine/drm/internal/l;->t:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "WVAssetPathKey"

    iget-object v2, p0, Lcom/widevine/drm/internal/l;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVIsEncryptedKey"

    iget-boolean v2, p0, Lcom/widevine/drm/internal/l;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Stopped:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {p0, v1, p1, p2, v0}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/widevine/drm/internal/l;->o:Ljava/lang/String;

    return-object v0
.end method

.method protected final d()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/widevine/drm/internal/l;->d:Z

    return-void
.end method

.method public final f()Z
    .locals 4

    iget-boolean v0, p0, Lcom/widevine/drm/internal/l;->t:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/widevine/drm/internal/l;->j:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/widevine/drm/internal/l;->k:J

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
