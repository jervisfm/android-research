.class public final Lcom/widevine/drm/internal/o;
.super Lcom/widevine/drm/internal/k;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:J

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(Lcom/widevine/drm/internal/u;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/k;-><init>(Lcom/widevine/drm/internal/u;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/widevine/drm/internal/o;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/widevine/drm/internal/o;->b:Z

    return-void
.end method


# virtual methods
.method protected final a(Lcom/widevine/drm/internal/t;Lcom/widevine/drmapi/android/WVStatus;)Lcom/widevine/drmapi/android/WVEvent;
    .locals 1

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne p2, v0, :cond_0

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseReceived:Lcom/widevine/drmapi/android/WVEvent;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0
.end method

.method protected final c()Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const-wide/16 v3, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/widevine/drm/internal/o;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/widevine/drm/internal/o;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    const-string v1, "WVAssetPathKey"

    iget-object v2, p0, Lcom/widevine/drm/internal/o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/widevine/drm/internal/o;->c:Ljava/lang/String;

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "WVAssetTypeKey"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-wide v1, p0, Lcom/widevine/drm/internal/o;->d:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p0, Lcom/widevine/drm/internal/o;->e:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p0, Lcom/widevine/drm/internal/o;->f:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    :cond_0
    const-string v1, "WVSystemIDKey"

    iget-wide v2, p0, Lcom/widevine/drm/internal/o;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVAssetIDKey"

    iget-wide v2, p0, Lcom/widevine/drm/internal/o;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVKeyIDKey"

    iget-wide v2, p0, Lcom/widevine/drm/internal/o;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    const-string v1, "WVAssetTypeKey"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final e()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/widevine/drm/internal/o;->b:Z

    return-void
.end method

.method public final run()V
    .locals 10

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/widevine/drm/internal/a;->a(Lcom/widevine/drm/internal/x;)Z

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/o;->g()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/widevine/drm/internal/JNI;->c(II)[Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    array-length v0, v9

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->EndOfList:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OutOfMemoryError:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "JNI call failed( rlt:r)"

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/widevine/drm/internal/o;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/widevine/drm/internal/a;->b(Lcom/widevine/drm/internal/x;)Z

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NowOnlineTask: results = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, v9

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/widevine/drm/internal/m;->b(Ljava/lang/String;)V

    array-length v0, v9

    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->EndOfList:Lcom/widevine/drmapi/android/WVEvent;

    const/4 v1, 0x0

    aget-object v1, v9, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/widevine/drmapi/android/WVStatus;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v2, v9, v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/widevine/drm/internal/o;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/widevine/drm/internal/a;->b(Lcom/widevine/drm/internal/x;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    move v8, v0

    :goto_1
    array-length v0, v9

    if-ge v8, v0, :cond_7

    array-length v0, v9

    sub-int/2addr v0, v8

    const/4 v1, 0x5

    if-lt v0, v1, :cond_6

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/widevine/drm/internal/o;->b:Z

    add-int/lit8 v0, v8, 0x0

    aget-object v0, v9, v0

    iput-object v0, p0, Lcom/widevine/drm/internal/o;->c:Ljava/lang/String;

    add-int/lit8 v0, v8, 0x1

    aget-object v0, v9, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_4

    add-int/lit8 v0, v8, 0x2

    aget-object v0, v9, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/widevine/drm/internal/o;->d:J

    add-int/lit8 v0, v8, 0x3

    aget-object v0, v9, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/widevine/drm/internal/o;->e:J

    add-int/lit8 v0, v8, 0x4

    aget-object v0, v9, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/widevine/drm/internal/o;->f:J

    iget-object v1, p0, Lcom/widevine/drm/internal/o;->c:Ljava/lang/String;

    iget-wide v2, p0, Lcom/widevine/drm/internal/o;->d:J

    iget-wide v4, p0, Lcom/widevine/drm/internal/o;->e:J

    iget-wide v6, p0, Lcom/widevine/drm/internal/o;->f:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lcom/widevine/drm/internal/o;->a(Ljava/lang/String;JJJ)V

    :goto_3
    const/4 v0, 0x0

    :goto_4
    const/16 v1, 0x4b0

    if-ge v0, v1, :cond_5

    iget-boolean v1, p0, Lcom/widevine/drm/internal/o;->b:Z

    if-nez v1, :cond_5

    const-wide/16 v1, 0x64

    :try_start_0
    invoke-static {v1, v2}, Lcom/widevine/drm/internal/o;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/widevine/drm/internal/o;->d:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/widevine/drm/internal/o;->e:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/widevine/drm/internal/o;->f:J

    iget-object v0, p0, Lcom/widevine/drm/internal/o;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/widevine/drm/internal/o;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/widevine/drm/internal/o;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Lcom/widevine/drm/internal/o;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/widevine/drm/internal/o;->c:Ljava/lang/String;

    sget-object v1, Lcom/widevine/drm/internal/t;->b:Lcom/widevine/drm/internal/t;

    invoke-virtual {p0, v0, v1}, Lcom/widevine/drm/internal/o;->b(Ljava/lang/String;Lcom/widevine/drm/internal/t;)Lcom/widevine/drmapi/android/WVStatus;

    :cond_6
    add-int/lit8 v0, v8, 0x5

    move v8, v0

    goto/16 :goto_1

    :cond_7
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->EndOfList:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/widevine/drm/internal/o;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/widevine/drm/internal/a;->b(Lcom/widevine/drm/internal/x;)Z

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_5
.end method
