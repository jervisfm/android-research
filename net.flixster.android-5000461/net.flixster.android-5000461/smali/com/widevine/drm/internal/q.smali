.class public final Lcom/widevine/drm/internal/q;
.super Lcom/widevine/drm/internal/x;


# direct methods
.method public constructor <init>(Lcom/widevine/drm/internal/u;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/x;-><init>(Lcom/widevine/drm/internal/u;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/widevine/drm/internal/t;Lcom/widevine/drmapi/android/WVStatus;)Lcom/widevine/drmapi/android/WVEvent;
    .locals 1

    sget-object v0, Lcom/widevine/drm/internal/t;->c:Lcom/widevine/drm/internal/t;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->EndOfList:Lcom/widevine/drmapi/android/WVEvent;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->QueryStatus:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0
.end method

.method public final run()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/widevine/drm/internal/a;->a(Lcom/widevine/drm/internal/x;)Z

    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    invoke-virtual {p0}, Lcom/widevine/drm/internal/q;->g()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/widevine/drm/internal/JNI;->b(II)I

    move-result v0

    invoke-static {v0}, Lcom/widevine/drmapi/android/WVStatus;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->EndOfList:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {p0, v1, v0, v3, v3}, Lcom/widevine/drm/internal/q;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/widevine/drm/internal/a;->b(Lcom/widevine/drm/internal/x;)Z

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/widevine/drm/internal/a;->a()Lcom/widevine/drm/internal/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/widevine/drm/internal/a;->b(Lcom/widevine/drm/internal/x;)Z

    goto :goto_0
.end method
