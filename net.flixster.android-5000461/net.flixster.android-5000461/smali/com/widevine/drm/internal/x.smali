.class public abstract Lcom/widevine/drm/internal/x;
.super Ljava/lang/Thread;


# instance fields
.field protected n:Lcom/widevine/drm/internal/u;


# direct methods
.method public constructor <init>(Lcom/widevine/drm/internal/u;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p1, p0, Lcom/widevine/drm/internal/x;->n:Lcom/widevine/drm/internal/u;

    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/widevine/drm/internal/t;Lcom/widevine/drmapi/android/WVStatus;)Lcom/widevine/drmapi/android/WVEvent;
.end method

.method protected final a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/widevine/drmapi/android/WVEvent;",
            "Lcom/widevine/drmapi/android/WVStatus;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    if-nez p4, :cond_0

    new-instance p4, Ljava/util/HashMap;

    invoke-direct {p4}, Ljava/util/HashMap;-><init>()V

    :cond_0
    const-string v0, "WVStatusKey"

    invoke-virtual {p4, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    const-string v0, "WVErrorKey"

    invoke-virtual {p4, v0, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/widevine/drm/internal/x;->n:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->d()Lcom/widevine/drmapi/android/WVEventListener;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/widevine/drm/internal/x;->n:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->d()Lcom/widevine/drmapi/android/WVEventListener;

    move-result-object v0

    invoke-interface {v0, p1, p4}, Lcom/widevine/drmapi/android/WVEventListener;->onEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    :cond_2
    invoke-virtual {p0}, Lcom/widevine/drm/internal/x;->e()V

    return-void
.end method

.method protected a(Z)V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method public final g()I
    .locals 1

    iget-object v0, p0, Lcom/widevine/drm/internal/x;->n:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->f()I

    move-result v0

    return v0
.end method

.method protected final h()Z
    .locals 1

    iget-object v0, p0, Lcom/widevine/drm/internal/x;->n:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->i()Z

    move-result v0

    return v0
.end method
