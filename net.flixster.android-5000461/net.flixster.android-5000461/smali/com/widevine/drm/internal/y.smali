.class public final Lcom/widevine/drm/internal/y;
.super Lcom/widevine/drm/internal/x;


# direct methods
.method public constructor <init>(Lcom/widevine/drm/internal/u;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/x;-><init>(Lcom/widevine/drm/internal/u;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/widevine/drm/internal/t;Lcom/widevine/drmapi/android/WVStatus;)Lcom/widevine/drmapi/android/WVEvent;
    .locals 1

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Terminated:Lcom/widevine/drmapi/android/WVEvent;

    return-object v0
.end method

.method public final run()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Terminated:Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/widevine/drm/internal/y;->a(Lcom/widevine/drmapi/android/WVEvent;Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;Ljava/util/HashMap;)V

    iget-object v0, p0, Lcom/widevine/drm/internal/y;->n:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->a()V

    return-void
.end method
