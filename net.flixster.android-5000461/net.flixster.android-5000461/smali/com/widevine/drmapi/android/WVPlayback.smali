.class public Lcom/widevine/drmapi/android/WVPlayback;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/widevine/drm/internal/l;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/widevine/drm/internal/u;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(JJJZ)Lcom/widevine/drmapi/android/WVStatus;
    .locals 8

    const-wide/16 v1, 0x0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_1
    cmp-long v0, p1, v1

    if-ltz v0, :cond_2

    cmp-long v0, p3, v1

    if-ltz v0, :cond_2

    cmp-long v0, p5, v1

    if-gez v0, :cond_3

    :cond_2
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotLicensed:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/widevine/drm/internal/s;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/widevine/drm/internal/s;-><init>(Lcom/widevine/drm/internal/u;JJJ)V

    if-eqz p7, :cond_4

    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->start()V

    :goto_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->run()V

    goto :goto_1
.end method

.method private a(Landroid/content/Context;Ljava/util/HashMap;Lcom/widevine/drmapi/android/WVEventListener;Z)Lcom/widevine/drmapi/android/WVStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/widevine/drmapi/android/WVEventListener;",
            "Z)",
            "Lcom/widevine/drmapi/android/WVStatus;"
        }
    .end annotation

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->AlreadyInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/widevine/drmapi/android/WVPlayback;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->c:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IMEI: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/widevine/drmapi/android/WVPlayback;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Android ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/widevine/drmapi/android/WVPlayback;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->d:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Wifi MAC: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->f:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Default asset DB Path: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/widevine/drmapi/android/WVPlayback;->a(Ljava/util/HashMap;)V

    new-instance v0, Lcom/widevine/drm/internal/u;

    invoke-direct {v0, p3, p1}, Lcom/widevine/drm/internal/u;-><init>(Lcom/widevine/drmapi/android/WVEventListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    new-instance v1, Lcom/widevine/drm/internal/j;

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {p0, p2}, Lcom/widevine/drmapi/android/WVPlayback;->b(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/widevine/drm/internal/j;-><init>(Lcom/widevine/drm/internal/u;Ljava/util/HashMap;)V

    const-string v0, "WVInitialMediaplayerConnectionTimeout"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    const-string v0, "WVInitialMediaplayerConnectionTimeout"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    invoke-virtual {v2, v0}, Lcom/widevine/drm/internal/u;->a(I)V

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {v1}, Lcom/widevine/drm/internal/x;->start()V

    :goto_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/widevine/drm/internal/x;->run()V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;
    .locals 2

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/widevine/drm/internal/r;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v0, v1, p1}, Lcom/widevine/drm/internal/r;-><init>(Lcom/widevine/drm/internal/u;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->start()V

    :goto_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->run()V

    goto :goto_1
.end method

.method private a(Z)Lcom/widevine/drmapi/android/WVStatus;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Terminate command received"

    invoke-virtual {v0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    :cond_0
    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lcom/widevine/drm/internal/y;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v0, v1}, Lcom/widevine/drm/internal/y;-><init>(Lcom/widevine/drm/internal/u;)V

    if-eqz p1, :cond_3

    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->start()V

    :goto_1
    iput-object v3, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->run()V

    goto :goto_1
.end method

.method private a(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "WVAssetRootKey"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "WVAssetRootKey"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->e:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "/sdcard/media/"

    iput-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v1}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "WVStatusKey"

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "WVAssetPathKey"

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "WVErrorKey"

    const-string v3, "Unable to play. Not initialized (wp:p)"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v2}, Lcom/widevine/drm/internal/u;->d()Lcom/widevine/drmapi/android/WVEventListener;

    move-result-object v2

    sget-object v3, Lcom/widevine/drmapi/android/WVEvent;->PlayFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-interface {v2, v3, v1}, Lcom/widevine/drmapi/android/WVEventListener;->onEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    invoke-virtual {v1}, Lcom/widevine/drm/internal/l;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "WVStatusKey"

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->NotPlaying:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "WVAssetPathKey"

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "WVErrorKey"

    const-string v3, "Unable to play. Previous command still being processed (wp:p)"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v2}, Lcom/widevine/drm/internal/u;->d()Lcom/widevine/drmapi/android/WVEventListener;

    move-result-object v2

    sget-object v3, Lcom/widevine/drmapi/android/WVEvent;->PlayFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-interface {v2, v3, v1}, Lcom/widevine/drmapi/android/WVEventListener;->onEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;
    .locals 2

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/widevine/drm/internal/z;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v0, v1, p1}, Lcom/widevine/drm/internal/z;-><init>(Lcom/widevine/drm/internal/u;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->start()V

    :goto_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->run()V

    goto :goto_1
.end method

.method private b(Z)Lcom/widevine/drmapi/android/WVStatus;
    .locals 2

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/widevine/drm/internal/q;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v0, v1}, Lcom/widevine/drm/internal/q;-><init>(Lcom/widevine/drm/internal/u;)V

    if-eqz p1, :cond_2

    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->start()V

    :goto_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->run()V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/16 v2, 0x2f

    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "file://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v2, :cond_2

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v2, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_1
    :goto_0
    return-object p1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private b(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    const-string v1, "WVAndroidIDKey"

    iget-object v2, p0, Lcom/widevine/drmapi/android/WVPlayback;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVIMEIKey"

    iget-object v2, p0, Lcom/widevine/drmapi/android/WVPlayback;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVWifiMacKey"

    iget-object v2, p0, Lcom/widevine/drmapi/android/WVPlayback;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVHWDeviceKey"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v2}, Lcom/widevine/drmapi/android/WVPlayback;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVHWModelKey"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v2}, Lcom/widevine/drmapi/android/WVPlayback;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVUIDKey"

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVAssetDBPathKey"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "WVAssetDBPathKey"

    iget-object v2, p0, Lcom/widevine/drmapi/android/WVPlayback;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private c(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;
    .locals 2

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/widevine/drm/internal/p;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v0, v1, p1}, Lcom/widevine/drm/internal/p;-><init>(Lcom/widevine/drm/internal/u;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->start()V

    :goto_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->run()V

    goto :goto_1
.end method

.method private c(Z)Lcom/widevine/drmapi/android/WVStatus;
    .locals 2

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/widevine/drm/internal/o;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v0, v1}, Lcom/widevine/drm/internal/o;-><init>(Lcom/widevine/drm/internal/u;)V

    if-eqz p1, :cond_2

    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->start()V

    :goto_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->run()V

    goto :goto_1
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-nez p0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "[^a-zA-Z0-9]"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private d(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;
    .locals 2

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/widevine/drm/internal/s;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v0, v1, p1}, Lcom/widevine/drm/internal/s;-><init>(Lcom/widevine/drm/internal/u;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->start()V

    :goto_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/widevine/drm/internal/x;->run()V

    goto :goto_1
.end method

.method public static deleteAssetDB(Landroid/content/Context;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/widevine/drmapi/android/WVPlayback;->deleteAssetDB(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public static deleteAssetDB(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->FileSystemError:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/widevine/drm/internal/JNI;->b(Ljava/lang/String;)V

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0
.end method


# virtual methods
.method public initialize(Landroid/content/Context;Ljava/util/HashMap;Lcom/widevine/drmapi/android/WVEventListener;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/widevine/drmapi/android/WVEventListener;",
            ")",
            "Lcom/widevine/drmapi/android/WVStatus;"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/widevine/drmapi/android/WVPlayback;->a(Landroid/content/Context;Ljava/util/HashMap;Lcom/widevine/drmapi/android/WVEventListener;Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public initializeSynchronous(Landroid/content/Context;Ljava/util/HashMap;Lcom/widevine/drmapi/android/WVEventListener;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/widevine/drmapi/android/WVEventListener;",
            ")",
            "Lcom/widevine/drmapi/android/WVStatus;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/widevine/drmapi/android/WVPlayback;->a(Landroid/content/Context;Ljava/util/HashMap;Lcom/widevine/drmapi/android/WVEventListener;Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public isRooted()Z
    .locals 1

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/widevine/drm/internal/u;->j()V

    :cond_1
    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/widevine/drm/internal/JNI;->d()Z

    move-result v0

    return v0
.end method

.method public logDebugInfo()V
    .locals 0

    return-void
.end method

.method public nowOnline()Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/widevine/drmapi/android/WVPlayback;->c(Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public nowOnlineSynchronous()Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/widevine/drmapi/android/WVPlayback;->c(Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public play(Lcom/widevine/drmapi/android/a;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const-string v1, "ChunkedRandomAccessFile"

    invoke-direct {p0, v1}, Lcom/widevine/drmapi/android/WVPlayback;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v1, Lcom/widevine/drm/internal/d;

    iget-object v2, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v1, v2, p1}, Lcom/widevine/drm/internal/d;-><init>(Lcom/widevine/drm/internal/u;Lcom/widevine/drmapi/android/a;)V

    iput-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;
    :try_end_0
    .catch Lcom/widevine/drm/internal/aa; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/l;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "WVStatusKey"

    sget-object v4, Lcom/widevine/drmapi/android/WVStatus;->NotPlaying:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "WVAssetPathKey"

    const-string v4, "ChunkedRandomAccessFile"

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "WVErrorKey"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/widevine/drm/internal/aa;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " (wp:p)"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v1}, Lcom/widevine/drm/internal/u;->d()Lcom/widevine/drmapi/android/WVEventListener;

    move-result-object v1

    sget-object v3, Lcom/widevine/drmapi/android/WVEvent;->PlayFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-interface {v1, v3, v2}, Lcom/widevine/drmapi/android/WVEventListener;->onEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0
.end method

.method public play(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/widevine/drmapi/android/WVPlayback;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/widevine/drmapi/android/WVPlayback;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :try_start_0
    const-string v1, "http://"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/widevine/drm/internal/f;

    iget-object v3, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v1, v3, v2}, Lcom/widevine/drm/internal/f;-><init>(Lcom/widevine/drm/internal/u;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;
    :try_end_0
    .catch Lcom/widevine/drm/internal/aa; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/l;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v1, Lcom/widevine/drm/internal/d;

    iget-object v3, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    const-wide/16 v4, 0x0

    invoke-direct {v1, v3, v2, v4, v5}, Lcom/widevine/drm/internal/d;-><init>(Lcom/widevine/drm/internal/u;Ljava/lang/String;J)V

    iput-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;
    :try_end_1
    .catch Lcom/widevine/drm/internal/aa; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v4, "WVStatusKey"

    sget-object v5, Lcom/widevine/drmapi/android/WVStatus;->NotPlaying:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "WVAssetPathKey"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "WVErrorKey"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/widevine/drm/internal/aa;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " (wp:p)"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v1}, Lcom/widevine/drm/internal/u;->d()Lcom/widevine/drmapi/android/WVEventListener;

    move-result-object v1

    sget-object v2, Lcom/widevine/drmapi/android/WVEvent;->PlayFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-interface {v1, v2, v3}, Lcom/widevine/drmapi/android/WVEventListener;->onEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0
.end method

.method public play(Ljava/lang/String;J)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/widevine/drmapi/android/WVPlayback;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/widevine/drmapi/android/WVPlayback;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :try_start_0
    const-string v1, "http://"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/widevine/drm/internal/f;

    iget-object v3, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v1, v3, v2}, Lcom/widevine/drm/internal/f;-><init>(Lcom/widevine/drm/internal/u;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;
    :try_end_0
    .catch Lcom/widevine/drm/internal/aa; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/l;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v1, Lcom/widevine/drm/internal/d;

    iget-object v3, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {v1, v3, v2, p2, p3}, Lcom/widevine/drm/internal/d;-><init>(Lcom/widevine/drm/internal/u;Ljava/lang/String;J)V

    iput-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;
    :try_end_1
    .catch Lcom/widevine/drm/internal/aa; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v4, "WVStatusKey"

    sget-object v5, Lcom/widevine/drmapi/android/WVStatus;->NotPlaying:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "WVAssetPathKey"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "WVErrorKey"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/widevine/drm/internal/aa;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " (wp:p)"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v1}, Lcom/widevine/drm/internal/u;->d()Lcom/widevine/drmapi/android/WVEventListener;

    move-result-object v1

    sget-object v2, Lcom/widevine/drmapi/android/WVEvent;->PlayFailed:Lcom/widevine/drmapi/android/WVEvent;

    invoke-interface {v1, v2, v3}, Lcom/widevine/drmapi/android/WVEventListener;->onEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0
.end method

.method public queryAssetStatus(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/widevine/drmapi/android/WVPlayback;->c(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public queryAssetStatusSynchronous(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/widevine/drmapi/android/WVPlayback;->c(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public queryAssetsStatus()Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/widevine/drmapi/android/WVPlayback;->b(Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public queryAssetsStatusSynchronous()Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/widevine/drmapi/android/WVPlayback;->b(Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public registerAsset(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/widevine/drmapi/android/WVPlayback;->a(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public registerAssetSynchronous(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/widevine/drmapi/android/WVPlayback;->a(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public requestLicense(JJJ)Lcom/widevine/drmapi/android/WVStatus;
    .locals 8

    const/4 v7, 0x1

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/widevine/drmapi/android/WVPlayback;->a(JJJZ)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public requestLicense(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/widevine/drmapi/android/WVPlayback;->d(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public requestLicenseSynchronous(JJJ)Lcom/widevine/drmapi/android/WVStatus;
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/widevine/drmapi/android/WVPlayback;->a(JJJZ)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public requestLicenseSynchronous(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/widevine/drmapi/android/WVPlayback;->d(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public secureRetrieve()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v1}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "WVStatusKey"

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "WVErrorKey"

    const-string v3, "Not initialized (wp:sr)"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v2}, Lcom/widevine/drm/internal/u;->d()Lcom/widevine/drmapi/android/WVEventListener;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v2}, Lcom/widevine/drm/internal/u;->d()Lcom/widevine/drmapi/android/WVEventListener;

    move-result-object v2

    sget-object v3, Lcom/widevine/drmapi/android/WVEvent;->SecureStore:Lcom/widevine/drmapi/android/WVEvent;

    invoke-interface {v2, v3, v1}, Lcom/widevine/drmapi/android/WVEventListener;->onEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/widevine/drm/internal/JNI;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public secureStore(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/widevine/drm/internal/JNI;->a()Lcom/widevine/drm/internal/JNI;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/widevine/drm/internal/JNI;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/widevine/drmapi/android/WVStatus;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public setCredentials(Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/widevine/drmapi/android/WVStatus;"
        }
    .end annotation

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/widevine/drmapi/android/WVPlayback;->a(Ljava/util/HashMap;)V

    const-string v0, "WVInitialMediaplayerConnectionTimeout"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    const-string v0, "WVInitialMediaplayerConnectionTimeout"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/widevine/drm/internal/u;->a(I)V

    :cond_2
    new-instance v0, Lcom/widevine/drm/internal/j;

    iget-object v1, p0, Lcom/widevine/drmapi/android/WVPlayback;->g:Lcom/widevine/drm/internal/u;

    invoke-direct {p0, p1}, Lcom/widevine/drmapi/android/WVPlayback;->b(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/j;-><init>(Lcom/widevine/drm/internal/u;Ljava/util/HashMap;)V

    :try_start_0
    invoke-virtual {v0}, Lcom/widevine/drm/internal/j;->a()V
    :try_end_0
    .catch Lcom/widevine/drm/internal/aa; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/widevine/drm/internal/aa;->a()Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public stop()Lcom/widevine/drmapi/android/WVStatus;
    .locals 3

    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    if-nez v0, :cond_0

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotPlaying:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    const-string v2, "Stop command received"

    invoke-virtual {v0, v1, v2}, Lcom/widevine/drm/internal/l;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/widevine/drmapi/android/WVPlayback;->a:Lcom/widevine/drm/internal/l;

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0
.end method

.method public terminate()Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/widevine/drmapi/android/WVPlayback;->a(Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public terminateSynchronous()Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/widevine/drmapi/android/WVPlayback;->a(Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public unregisterAsset(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/widevine/drmapi/android/WVPlayback;->b(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method

.method public unregisterAssetSynchronous(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/widevine/drmapi/android/WVPlayback;->b(Ljava/lang/String;Z)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    return-object v0
.end method
