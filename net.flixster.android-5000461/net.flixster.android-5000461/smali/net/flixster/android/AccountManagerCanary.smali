.class public Lnet/flixster/android/AccountManagerCanary;
.super Ljava/lang/Object;
.source "AccountManagerCanary.java"


# static fields
.field private static accounts:[Landroid/accounts/Account;

.field private static name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFirstAccountName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "context"

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    sput-object v1, Lnet/flixster/android/AccountManagerCanary;->accounts:[Landroid/accounts/Account;

    .line 23
    sget-object v1, Lnet/flixster/android/AccountManagerCanary;->accounts:[Landroid/accounts/Account;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 24
    sget-object v1, Lnet/flixster/android/AccountManagerCanary;->accounts:[Landroid/accounts/Account;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sput-object v1, Lnet/flixster/android/AccountManagerCanary;->name:Ljava/lang/String;

    .line 25
    sput-object v0, Lnet/flixster/android/AccountManagerCanary;->accounts:[Landroid/accounts/Account;

    .line 26
    sget-object v0, Lnet/flixster/android/AccountManagerCanary;->name:Ljava/lang/String;

    .line 29
    :cond_0
    return-object v0
.end method
