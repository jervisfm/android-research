.class Lnet/flixster/android/ActorGalleryPage$1;
.super Landroid/os/Handler;
.source "ActorGalleryPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ActorGalleryPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ActorGalleryPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ActorGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ActorGalleryPage$1;->this$0:Lnet/flixster/android/ActorGalleryPage;

    .line 117
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "message"

    .prologue
    .line 121
    const-string v0, "FlxMain"

    const-string v1, "ActorGalleryPage.updateHandler"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage$1;->this$0:Lnet/flixster/android/ActorGalleryPage;

    #getter for: Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v0}, Lnet/flixster/android/ActorGalleryPage;->access$0(Lnet/flixster/android/ActorGalleryPage;)Lnet/flixster/android/model/Actor;

    move-result-object v0

    iget-object v0, v0, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage$1;->this$0:Lnet/flixster/android/ActorGalleryPage;

    #calls: Lnet/flixster/android/ActorGalleryPage;->updatePage()V
    invoke-static {v0}, Lnet/flixster/android/ActorGalleryPage;->access$1(Lnet/flixster/android/ActorGalleryPage;)V

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage$1;->this$0:Lnet/flixster/android/ActorGalleryPage;

    invoke-virtual {v0}, Lnet/flixster/android/ActorGalleryPage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage$1;->this$0:Lnet/flixster/android/ActorGalleryPage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/ActorGalleryPage;->showDialog(I)V

    goto :goto_0
.end method
