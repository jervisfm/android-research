.class Lnet/flixster/android/ActorGalleryPage$3;
.super Ljava/lang/Object;
.source "ActorGalleryPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ActorGalleryPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ActorGalleryPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ActorGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ActorGalleryPage$3;->this$0:Lnet/flixster/android/ActorGalleryPage;

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "view"

    .prologue
    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Photo;

    .line 164
    .local v2, photo:Lnet/flixster/android/model/Photo;
    if-eqz v2, :cond_1

    .line 165
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage$3;->this$0:Lnet/flixster/android/ActorGalleryPage;

    #getter for: Lnet/flixster/android/ActorGalleryPage;->photos:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/flixster/android/ActorGalleryPage;->access$2(Lnet/flixster/android/ActorGalleryPage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 166
    .local v0, index:I
    if-gez v0, :cond_0

    .line 167
    const/4 v0, 0x0

    .line 169
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/actor/photo"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Actor Photo Page for photo:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v2, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    new-instance v1, Landroid/content/Intent;

    const-string v3, "PHOTO"

    const/4 v4, 0x0

    iget-object v5, p0, Lnet/flixster/android/ActorGalleryPage$3;->this$0:Lnet/flixster/android/ActorGalleryPage;

    invoke-virtual {v5}, Lnet/flixster/android/ActorGalleryPage;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lnet/flixster/android/ScrollGalleryPage;

    invoke-direct {v1, v3, v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 172
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "PHOTO_INDEX"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    const-string v3, "ACTOR_ID"

    iget-object v4, p0, Lnet/flixster/android/ActorGalleryPage$3;->this$0:Lnet/flixster/android/ActorGalleryPage;

    #getter for: Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v4}, Lnet/flixster/android/ActorGalleryPage;->access$0(Lnet/flixster/android/ActorGalleryPage;)Lnet/flixster/android/model/Actor;

    move-result-object v4

    iget-wide v4, v4, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 175
    const-string v3, "ACTOR_NAME"

    iget-object v4, p0, Lnet/flixster/android/ActorGalleryPage$3;->this$0:Lnet/flixster/android/ActorGalleryPage;

    #getter for: Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v4}, Lnet/flixster/android/ActorGalleryPage;->access$0(Lnet/flixster/android/ActorGalleryPage;)Lnet/flixster/android/model/Actor;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage$3;->this$0:Lnet/flixster/android/ActorGalleryPage;

    invoke-virtual {v3, v1}, Lnet/flixster/android/ActorGalleryPage;->startActivity(Landroid/content/Intent;)V

    .line 178
    .end local v0           #index:I
    .end local v1           #intent:Landroid/content/Intent;
    :cond_1
    return-void
.end method
