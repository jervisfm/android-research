.class Lnet/flixster/android/ActorGalleryPage$5;
.super Ljava/util/TimerTask;
.source "ActorGalleryPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/ActorGalleryPage;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ActorGalleryPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ActorGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ActorGalleryPage$5;->this$0:Lnet/flixster/android/ActorGalleryPage;

    .line 98
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 100
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage$5;->this$0:Lnet/flixster/android/ActorGalleryPage;

    #getter for: Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v3}, Lnet/flixster/android/ActorGalleryPage;->access$0(Lnet/flixster/android/ActorGalleryPage;)Lnet/flixster/android/model/Actor;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    .line 101
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage$5;->this$0:Lnet/flixster/android/ActorGalleryPage;

    #getter for: Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v3}, Lnet/flixster/android/ActorGalleryPage;->access$0(Lnet/flixster/android/ActorGalleryPage;)Lnet/flixster/android/model/Actor;

    move-result-object v3

    iget-wide v0, v3, Lnet/flixster/android/model/Actor;->id:J

    .line 103
    .local v0, actorId:J
    :try_start_0
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage$5;->this$0:Lnet/flixster/android/ActorGalleryPage;

    #getter for: Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v3}, Lnet/flixster/android/ActorGalleryPage;->access$0(Lnet/flixster/android/ActorGalleryPage;)Lnet/flixster/android/model/Actor;

    move-result-object v3

    invoke-static {v0, v1}, Lnet/flixster/android/data/PhotoDao;->getActorPhotos(J)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v3, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    .end local v0           #actorId:J
    :cond_0
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage$5;->this$0:Lnet/flixster/android/ActorGalleryPage;

    #getter for: Lnet/flixster/android/ActorGalleryPage;->updateHandler:Landroid/os/Handler;
    invoke-static {v3}, Lnet/flixster/android/ActorGalleryPage;->access$4(Lnet/flixster/android/ActorGalleryPage;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 110
    return-void

    .line 104
    .restart local v0       #actorId:J
    :catch_0
    move-exception v2

    .line 105
    .local v2, de:Lnet/flixster/android/data/DaoException;
    const-string v3, "FlxMain"

    const-string v4, "ActorGalleryPage.scheduleUpdatePageTask:failed to get photos"

    invoke-static {v3, v4, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 106
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage$5;->this$0:Lnet/flixster/android/ActorGalleryPage;

    #getter for: Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v3}, Lnet/flixster/android/ActorGalleryPage;->access$0(Lnet/flixster/android/ActorGalleryPage;)Lnet/flixster/android/model/Actor;

    move-result-object v3

    const/4 v4, 0x0

    iput-object v4, v3, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    goto :goto_0
.end method
