.class public Lnet/flixster/android/ActorGalleryPage;
.super Lnet/flixster/android/FlixsterActivity;
.source "ActorGalleryPage.java"


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field private static final MAX_PHOTOS:I = 0x32


# instance fields
.field private actor:Lnet/flixster/android/model/Actor;

.field private mDefaultAd:Lnet/flixster/android/ads/AdView;

.field private photoClickListener:Landroid/view/View$OnClickListener;

.field private photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private photos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private timer:Ljava/util/Timer;

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 117
    new-instance v0, Lnet/flixster/android/ActorGalleryPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorGalleryPage$1;-><init>(Lnet/flixster/android/ActorGalleryPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->updateHandler:Landroid/os/Handler;

    .line 155
    new-instance v0, Lnet/flixster/android/ActorGalleryPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorGalleryPage$2;-><init>(Lnet/flixster/android/ActorGalleryPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 161
    new-instance v0, Lnet/flixster/android/ActorGalleryPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorGalleryPage$3;-><init>(Lnet/flixster/android/ActorGalleryPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->photoClickListener:Landroid/view/View$OnClickListener;

    .line 27
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/ActorGalleryPage;)Lnet/flixster/android/model/Actor;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/ActorGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 132
    invoke-direct {p0}, Lnet/flixster/android/ActorGalleryPage;->updatePage()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/ActorGalleryPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->photos:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/ActorGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 97
    invoke-direct {p0}, Lnet/flixster/android/ActorGalleryPage;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/ActorGalleryPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 98
    new-instance v0, Lnet/flixster/android/ActorGalleryPage$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorGalleryPage$5;-><init>(Lnet/flixster/android/ActorGalleryPage;)V

    .line 112
    .local v0, updatePageTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/ActorGalleryPage;->timer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lnet/flixster/android/ActorGalleryPage;->timer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 115
    :cond_0
    return-void
.end method

.method private updatePage()V
    .locals 7

    .prologue
    .line 133
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    if-eqz v3, :cond_3

    .line 134
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 135
    new-instance v3, Ljava/lang/StringBuilder;

    const v4, 0x7f0c003d

    invoke-virtual {p0, v4}, Lnet/flixster/android/ActorGalleryPage;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lnet/flixster/android/ActorGalleryPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 138
    :cond_0
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 139
    const v3, 0x7f07003a

    invoke-virtual {p0, v3}, Lnet/flixster/android/ActorGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 140
    .local v0, galleryView:Landroid/widget/GridView;
    invoke-virtual {v0}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    if-nez v3, :cond_2

    .line 141
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lnet/flixster/android/ActorGalleryPage;->photos:Ljava/util/ArrayList;

    .line 142
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    const/16 v3, 0x32

    if-lt v1, v3, :cond_4

    .line 146
    :cond_1
    new-instance v3, Lnet/flixster/android/PhotosListAdapter;

    iget-object v4, p0, Lnet/flixster/android/ActorGalleryPage;->photos:Ljava/util/ArrayList;

    iget-object v5, p0, Lnet/flixster/android/ActorGalleryPage;->photoClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v3, p0, v4, v5}, Lnet/flixster/android/PhotosListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 147
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 148
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setClickable(Z)V

    .line 150
    .end local v1           #i:I
    :cond_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/photo/gallery/actor"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Photo Gallery - Actor - "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v6, v6, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .end local v0           #galleryView:Landroid/widget/GridView;
    :cond_3
    return-void

    .line 143
    .restart local v0       #galleryView:Landroid/widget/GridView;
    .restart local v1       #i:I
    :cond_4
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 144
    .local v2, photo:Ljava/lang/Object;
    iget-object v3, p0, Lnet/flixster/android/ActorGalleryPage;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedState"

    .prologue
    .line 38
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v1, 0x7f030019

    invoke-virtual {p0, v1}, Lnet/flixster/android/ActorGalleryPage;->setContentView(I)V

    .line 40
    invoke-virtual {p0}, Lnet/flixster/android/ActorGalleryPage;->createActionBar()V

    .line 42
    const v1, 0x7f07002a

    invoke-virtual {p0, v1}, Lnet/flixster/android/ActorGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/ads/AdView;

    iput-object v1, p0, Lnet/flixster/android/ActorGalleryPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    .line 43
    invoke-virtual {p0}, Lnet/flixster/android/ActorGalleryPage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 44
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 45
    new-instance v1, Lnet/flixster/android/model/Actor;

    invoke-direct {v1}, Lnet/flixster/android/model/Actor;-><init>()V

    iput-object v1, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    .line 46
    iget-object v1, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    const-string v2, "ACTOR_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lnet/flixster/android/model/Actor;->id:J

    .line 47
    iget-object v1, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    const-string v2, "ACTOR_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    .line 49
    :cond_0
    invoke-direct {p0}, Lnet/flixster/android/ActorGalleryPage;->updatePage()V

    .line 50
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "dialogId"

    .prologue
    .line 78
    packed-switch p1, :pswitch_data_0

    .line 93
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    .line 80
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 81
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v1, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 82
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 83
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 84
    const-string v1, "Retry"

    new-instance v2, Lnet/flixster/android/ActorGalleryPage$4;

    invoke-direct {v2, p0}, Lnet/flixster/android/ActorGalleryPage$4;-><init>(Lnet/flixster/android/ActorGalleryPage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 89
    invoke-virtual {p0}, Lnet/flixster/android/ActorGalleryPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 90
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onDestroy()V

    .line 69
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 71
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 73
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->timer:Ljava/util/Timer;

    .line 74
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 55
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->timer:Ljava/util/Timer;

    .line 58
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->actor:Lnet/flixster/android/model/Actor;

    const/4 v1, 0x0

    iput-object v1, v0, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    .line 61
    :cond_1
    invoke-direct {p0}, Lnet/flixster/android/ActorGalleryPage;->scheduleUpdatePageTask()V

    .line 63
    iget-object v0, p0, Lnet/flixster/android/ActorGalleryPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 64
    return-void
.end method
