.class Lnet/flixster/android/ActorPage$1;
.super Landroid/os/Handler;
.source "ActorPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ActorPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ActorPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ActorPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ActorPage$1;->this$0:Lnet/flixster/android/ActorPage;

    .line 152
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "message"

    .prologue
    .line 156
    iget-object v0, p0, Lnet/flixster/android/ActorPage$1;->this$0:Lnet/flixster/android/ActorPage;

    #getter for: Lnet/flixster/android/ActorPage;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;
    invoke-static {v0}, Lnet/flixster/android/ActorPage;->access$0(Lnet/flixster/android/ActorPage;)Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->isPausing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/ActorPage$1;->this$0:Lnet/flixster/android/ActorPage;

    invoke-virtual {v0}, Lnet/flixster/android/ActorPage;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    const-string v0, "FlxMain"

    const-string v1, "ActorPage.updateHandler"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lnet/flixster/android/ActorPage$1;->this$0:Lnet/flixster/android/ActorPage;

    iget-object v0, v0, Lnet/flixster/android/ActorPage;->throbberHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 162
    iget-object v0, p0, Lnet/flixster/android/ActorPage$1;->this$0:Lnet/flixster/android/ActorPage;

    #getter for: Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v0}, Lnet/flixster/android/ActorPage;->access$1(Lnet/flixster/android/ActorPage;)Lnet/flixster/android/model/Actor;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Lnet/flixster/android/ActorPage$1;->this$0:Lnet/flixster/android/ActorPage;

    #calls: Lnet/flixster/android/ActorPage;->updatePage()V
    invoke-static {v0}, Lnet/flixster/android/ActorPage;->access$2(Lnet/flixster/android/ActorPage;)V

    goto :goto_0

    .line 165
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/ActorPage$1;->this$0:Lnet/flixster/android/ActorPage;

    invoke-virtual {v0}, Lnet/flixster/android/ActorPage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    iget-object v0, p0, Lnet/flixster/android/ActorPage$1;->this$0:Lnet/flixster/android/ActorPage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/ActorPage;->showDialog(I)V

    goto :goto_0
.end method
