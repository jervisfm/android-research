.class Lnet/flixster/android/ActorPage$11;
.super Ljava/util/TimerTask;
.source "ActorPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/ActorPage;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ActorPage;

.field private final synthetic val$currResumeCtr:I


# direct methods
.method constructor <init>(Lnet/flixster/android/ActorPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ActorPage$11;->this$0:Lnet/flixster/android/ActorPage;

    iput p2, p0, Lnet/flixster/android/ActorPage$11;->val$currResumeCtr:I

    .line 128
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 130
    iget-object v1, p0, Lnet/flixster/android/ActorPage$11;->this$0:Lnet/flixster/android/ActorPage;

    #getter for: Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v1}, Lnet/flixster/android/ActorPage;->access$1(Lnet/flixster/android/ActorPage;)Lnet/flixster/android/model/Actor;

    move-result-object v1

    if-nez v1, :cond_0

    .line 131
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ActorPage.scheduleUpdatePageTask.run actorId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/ActorPage$11;->this$0:Lnet/flixster/android/ActorPage;

    #getter for: Lnet/flixster/android/ActorPage;->actorId:J
    invoke-static {v3}, Lnet/flixster/android/ActorPage;->access$9(Lnet/flixster/android/ActorPage;)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/ActorPage$11;->this$0:Lnet/flixster/android/ActorPage;

    iget-object v2, p0, Lnet/flixster/android/ActorPage$11;->this$0:Lnet/flixster/android/ActorPage;

    #getter for: Lnet/flixster/android/ActorPage;->actorId:J
    invoke-static {v2}, Lnet/flixster/android/ActorPage;->access$9(Lnet/flixster/android/ActorPage;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lnet/flixster/android/data/ActorDao;->getActor(J)Lnet/flixster/android/model/Actor;

    move-result-object v2

    #setter for: Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v1, v2}, Lnet/flixster/android/ActorPage;->access$10(Lnet/flixster/android/ActorPage;Lnet/flixster/android/model/Actor;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_0
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/ActorPage$11;->this$0:Lnet/flixster/android/ActorPage;

    iget v2, p0, Lnet/flixster/android/ActorPage$11;->val$currResumeCtr:I

    invoke-virtual {v1, v2}, Lnet/flixster/android/ActorPage;->shouldSkipBackgroundTask(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 145
    :goto_1
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "ActorPage.scheduleUpdatePageTask DaoException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 136
    iget-object v1, p0, Lnet/flixster/android/ActorPage$11;->this$0:Lnet/flixster/android/ActorPage;

    const/4 v2, 0x0

    #setter for: Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;
    invoke-static {v1, v2}, Lnet/flixster/android/ActorPage;->access$10(Lnet/flixster/android/ActorPage;Lnet/flixster/android/model/Actor;)V

    goto :goto_0

    .line 144
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/ActorPage$11;->this$0:Lnet/flixster/android/ActorPage;

    #getter for: Lnet/flixster/android/ActorPage;->updateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/ActorPage;->access$11(Lnet/flixster/android/ActorPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
