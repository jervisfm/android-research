.class Lnet/flixster/android/ActorPage$2;
.super Landroid/os/Handler;
.source "ActorPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ActorPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ActorPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ActorPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ActorPage$2;->this$0:Lnet/flixster/android/ActorPage;

    .line 297
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 300
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/widget/ImageView;

    .line 301
    .local v2, imageView:Landroid/widget/ImageView;
    if-eqz v2, :cond_0

    .line 302
    invoke-virtual {v2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 303
    .local v0, actor:Lnet/flixster/android/model/Actor;
    if-eqz v0, :cond_0

    .line 304
    iget-object v1, v0, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    .line 305
    .local v1, bitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 306
    invoke-virtual {v2}, Landroid/widget/ImageView;->invalidate()V

    .line 309
    .end local v0           #actor:Lnet/flixster/android/model/Actor;
    .end local v1           #bitmap:Landroid/graphics/Bitmap;
    :cond_0
    return-void
.end method
