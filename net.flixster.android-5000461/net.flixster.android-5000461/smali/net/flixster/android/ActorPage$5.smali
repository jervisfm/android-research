.class Lnet/flixster/android/ActorPage$5;
.super Ljava/lang/Object;
.source "ActorPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ActorPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ActorPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ActorPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ActorPage$5;->this$0:Lnet/flixster/android/ActorPage;

    .line 347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .parameter "view"

    .prologue
    const/4 v5, 0x0

    .line 349
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    .line 350
    .local v1, movie:Lnet/flixster/android/model/Movie;
    if-eqz v1, :cond_2

    .line 351
    new-instance v0, Landroid/content/Intent;

    const-string v4, "DETAILS"

    const/4 v6, 0x0

    iget-object v7, p0, Lnet/flixster/android/ActorPage$5;->this$0:Lnet/flixster/android/ActorPage;

    invoke-virtual {v7}, Lnet/flixster/android/ActorPage;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-class v8, Lnet/flixster/android/MovieDetails;

    invoke-direct {v0, v4, v6, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 352
    .local v0, intent:Landroid/content/Intent;
    const-string v4, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v1}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v6

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 353
    const-string v4, "MOVIE_IN_THEATER_FLAG"

    iget-boolean v6, v1, Lnet/flixster/android/model/Movie;->isMIT:Z

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 354
    const-string v6, "MOVIE_THUMBNAIL"

    iget-object v4, v1, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Parcelable;

    invoke-virtual {v0, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 355
    const/16 v4, 0xc

    new-array v2, v4, [Ljava/lang/String;

    const-string v4, "title"

    aput-object v4, v2, v5

    const/4 v4, 0x1

    const-string v6, "high"

    aput-object v6, v2, v4

    const/4 v4, 0x2

    const-string v6, "mpaa"

    aput-object v6, v2, v4

    const/4 v4, 0x3

    .line 356
    const-string v6, "MOVIE_ACTORS"

    aput-object v6, v2, v4

    const/4 v4, 0x4

    const-string v6, "thumbnail"

    aput-object v6, v2, v4

    const/4 v4, 0x5

    const-string v6, "runningTime"

    aput-object v6, v2, v4

    const/4 v4, 0x6

    const-string v6, "directors"

    aput-object v6, v2, v4

    const/4 v4, 0x7

    .line 357
    const-string v6, "theaterReleaseDate"

    aput-object v6, v2, v4

    const/16 v4, 0x8

    const-string v6, "genre"

    aput-object v6, v2, v4

    const/16 v4, 0x9

    const-string v6, "status"

    aput-object v6, v2, v4

    const/16 v4, 0xa

    .line 358
    const-string v6, "MOVIE_ACTORS_SHORT"

    aput-object v6, v2, v4

    const/16 v4, 0xb

    const-string v6, "meta"

    aput-object v6, v2, v4

    .line 359
    .local v2, movieProperties:[Ljava/lang/String;
    array-length v6, v2

    move v4, v5

    :goto_0
    if-lt v4, v6, :cond_3

    .line 362
    const-string v4, "boxOffice"

    const-string v5, "boxOffice"

    invoke-virtual {v1, v5}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 363
    const-string v4, "popcornScore"

    invoke-virtual {v1, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 364
    const-string v4, "popcornScore"

    const-string v5, "popcornScore"

    invoke-virtual {v1, v5}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 366
    :cond_0
    const-string v4, "rottenTomatoes"

    invoke-virtual {v1, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 367
    const-string v4, "rottenTomatoes"

    const-string v5, "rottenTomatoes"

    invoke-virtual {v1, v5}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 369
    :cond_1
    iget-object v4, p0, Lnet/flixster/android/ActorPage$5;->this$0:Lnet/flixster/android/ActorPage;

    invoke-virtual {v4, v0}, Lnet/flixster/android/ActorPage;->startActivity(Landroid/content/Intent;)V

    .line 371
    .end local v0           #intent:Landroid/content/Intent;
    .end local v2           #movieProperties:[Ljava/lang/String;
    :cond_2
    return-void

    .line 359
    .restart local v0       #intent:Landroid/content/Intent;
    .restart local v2       #movieProperties:[Ljava/lang/String;
    :cond_3
    aget-object v3, v2, v4

    .line 360
    .local v3, propertyKey:Ljava/lang/String;
    invoke-virtual {v1, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
