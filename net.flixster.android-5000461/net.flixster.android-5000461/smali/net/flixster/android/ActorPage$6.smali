.class Lnet/flixster/android/ActorPage$6;
.super Ljava/lang/Object;
.source "ActorPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ActorPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ActorPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ActorPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ActorPage$6;->this$0:Lnet/flixster/android/ActorPage;

    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "view"

    .prologue
    .line 376
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .line 377
    .local v0, movie:Lnet/flixster/android/model/Movie;
    if-eqz v0, :cond_0

    .line 378
    iget-object v1, p0, Lnet/flixster/android/ActorPage$6;->this$0:Lnet/flixster/android/ActorPage;

    invoke-static {v0, v1}, Lnet/flixster/android/Starter;->launchTrailer(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V

    .line 380
    :cond_0
    return-void
.end method
