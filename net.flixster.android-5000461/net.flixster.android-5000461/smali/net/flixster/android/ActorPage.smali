.class public Lnet/flixster/android/ActorPage;
.super Lnet/flixster/android/FlixsterActivity;
.source "ActorPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/ActorPage$MovieViewHolder;
    }
.end annotation


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field protected static final HIDE_THROBBER:I = 0x0

.field public static final KEY_ACTOR_ID:Ljava/lang/String; = "ACTOR_ID"

.field public static final KEY_ACTOR_NAME:Ljava/lang/String; = "ACTOR_NAME"

.field private static final MAX_MOVIES:I = 0x3

.field private static final MAX_PHOTOS:I = 0xf

.field private static final MAX_PHOTO_COUNT:I = 0x32

.field protected static final SHOW_THROBBER:I = 0x1


# instance fields
.field private actor:Lnet/flixster/android/model/Actor;

.field private actorId:J

.field private final actorThumbnailHandler:Landroid/os/Handler;

.field private filmographyLayout:Landroid/widget/LinearLayout;

.field private inflater:Landroid/view/LayoutInflater;

.field private mAdView:Lnet/flixster/android/ads/AdView;

.field private moreFilmographyLayout:Landroid/widget/RelativeLayout;

.field private moreFilmographyListener:Landroid/view/View$OnClickListener;

.field private movieClickListener:Landroid/view/View$OnClickListener;

.field private final movieThumbnailHandler:Landroid/os/Handler;

.field private photoClickListener:Landroid/view/View$OnClickListener;

.field private photoHandler:Landroid/os/Handler;

.field private resources:Landroid/content/res/Resources;

.field private throbber:Landroid/view/View;

.field protected final throbberHandler:Landroid/os/Handler;

.field private timer:Ljava/util/Timer;

.field private trailerClickListener:Landroid/view/View$OnClickListener;

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 152
    new-instance v0, Lnet/flixster/android/ActorPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorPage$1;-><init>(Lnet/flixster/android/ActorPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->updateHandler:Landroid/os/Handler;

    .line 297
    new-instance v0, Lnet/flixster/android/ActorPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorPage$2;-><init>(Lnet/flixster/android/ActorPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->actorThumbnailHandler:Landroid/os/Handler;

    .line 312
    new-instance v0, Lnet/flixster/android/ActorPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorPage$3;-><init>(Lnet/flixster/android/ActorPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->photoHandler:Landroid/os/Handler;

    .line 328
    new-instance v0, Lnet/flixster/android/ActorPage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorPage$4;-><init>(Lnet/flixster/android/ActorPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->photoClickListener:Landroid/view/View$OnClickListener;

    .line 347
    new-instance v0, Lnet/flixster/android/ActorPage$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorPage$5;-><init>(Lnet/flixster/android/ActorPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->movieClickListener:Landroid/view/View$OnClickListener;

    .line 374
    new-instance v0, Lnet/flixster/android/ActorPage$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorPage$6;-><init>(Lnet/flixster/android/ActorPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->trailerClickListener:Landroid/view/View$OnClickListener;

    .line 383
    new-instance v0, Lnet/flixster/android/ActorPage$7;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorPage$7;-><init>(Lnet/flixster/android/ActorPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->moreFilmographyListener:Landroid/view/View$OnClickListener;

    .line 571
    new-instance v0, Lnet/flixster/android/ActorPage$8;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorPage$8;-><init>(Lnet/flixster/android/ActorPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->movieThumbnailHandler:Landroid/os/Handler;

    .line 617
    new-instance v0, Lnet/flixster/android/ActorPage$9;

    invoke-direct {v0, p0}, Lnet/flixster/android/ActorPage$9;-><init>(Lnet/flixster/android/ActorPage;)V

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->throbberHandler:Landroid/os/Handler;

    .line 41
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/ActorPage;)Lcom/flixster/android/activity/decorator/TopLevelDecorator;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lnet/flixster/android/ActorPage;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/ActorPage;)Lnet/flixster/android/model/Actor;
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    return-object v0
.end method

.method static synthetic access$10(Lnet/flixster/android/ActorPage;Lnet/flixster/android/model/Actor;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    return-void
.end method

.method static synthetic access$11(Lnet/flixster/android/ActorPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 152
    iget-object v0, p0, Lnet/flixster/android/ActorPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/ActorPage;)V
    .locals 0
    .parameter

    .prologue
    .line 172
    invoke-direct {p0}, Lnet/flixster/android/ActorPage;->updatePage()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/ActorPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lnet/flixster/android/ActorPage;->filmographyLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/ActorPage;Lnet/flixster/android/model/Movie;Landroid/view/View;)Landroid/view/View;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 401
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/ActorPage;->getMovieView(Lnet/flixster/android/model/Movie;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/ActorPage;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lnet/flixster/android/ActorPage;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/ActorPage;)V
    .locals 0
    .parameter

    .prologue
    .line 632
    invoke-direct {p0}, Lnet/flixster/android/ActorPage;->showLoading()V

    return-void
.end method

.method static synthetic access$7(Lnet/flixster/android/ActorPage;)V
    .locals 0
    .parameter

    .prologue
    .line 636
    invoke-direct {p0}, Lnet/flixster/android/ActorPage;->hideLoading()V

    return-void
.end method

.method static synthetic access$8(Lnet/flixster/android/ActorPage;)V
    .locals 0
    .parameter

    .prologue
    .line 124
    invoke-direct {p0}, Lnet/flixster/android/ActorPage;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/ActorPage;)J
    .locals 2
    .parameter

    .prologue
    .line 44
    iget-wide v0, p0, Lnet/flixster/android/ActorPage;->actorId:J

    return-wide v0
.end method

.method private addScore(ILandroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V
    .locals 4
    .parameter "score"
    .parameter "scoreIcon"
    .parameter "scoreView"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 565
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 566
    invoke-virtual {p3, p2, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 567
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 568
    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 569
    return-void
.end method

.method private getMovieView(Lnet/flixster/android/model/Movie;Landroid/view/View;)Landroid/view/View;
    .locals 29
    .parameter "movie"
    .parameter "movieView"

    .prologue
    .line 403
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->inflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030053

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 404
    new-instance v25, Lnet/flixster/android/ActorPage$MovieViewHolder;

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Lnet/flixster/android/ActorPage$MovieViewHolder;-><init>(Lnet/flixster/android/ActorPage$MovieViewHolder;)V

    .line 405
    .local v25, viewHolder:Lnet/flixster/android/ActorPage$MovieViewHolder;
    const v4, 0x7f07012d

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->movieLayout:Landroid/widget/RelativeLayout;

    .line 406
    const v4, 0x7f070108

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->titleView:Landroid/widget/TextView;

    .line 407
    const v4, 0x7f07012e

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    .line 408
    const v4, 0x7f07012f

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->trailerView:Landroid/widget/ImageView;

    .line 409
    const v4, 0x7f070132

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->scoreView:Landroid/widget/TextView;

    .line 410
    const v4, 0x7f070133

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    .line 411
    const v4, 0x7f07010c

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->metaView:Landroid/widget/TextView;

    .line 412
    const v4, 0x7f070131

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->charView:Landroid/widget/TextView;

    .line 413
    const v4, 0x7f07010d

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->releaseView:Landroid/widget/TextView;

    .line 417
    const-string v4, "title"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 418
    .local v22, title:Ljava/lang/String;
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->titleView:Landroid/widget/TextView;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 420
    move-object/from16 v0, p1

    iget-object v4, v0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 421
    move-object/from16 v0, v25

    iget-object v5, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    iget-object v4, v0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 432
    :cond_0
    :goto_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lnet/flixster/android/model/Movie;->mActorPageChars:Ljava/lang/String;

    if-eqz v4, :cond_7

    move-object/from16 v0, p1

    iget-object v4, v0, Lnet/flixster/android/model/Movie;->mActorPageChars:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_7

    .line 433
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->charView:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v5, v0, Lnet/flixster/android/model/Movie;->mActorPageChars:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 434
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->charView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 441
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Movie;->hasTrailer()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 442
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->trailerView:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 443
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->trailerView:Landroid/widget/ImageView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 444
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->trailerView:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ActorPage;->trailerClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 445
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->trailerView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 447
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 448
    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-nez v4, :cond_8

    const/4 v13, 0x0

    .line 449
    .local v13, isReleased:Z
    :goto_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v17

    .line 450
    .local v17, ratingType:I
    packed-switch v17, :pswitch_data_0

    .line 483
    :cond_2
    :goto_3
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_d

    .line 484
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 536
    :goto_4
    const-string v4, "meta"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 537
    .local v15, meta:Ljava/lang/String;
    if-eqz v15, :cond_3

    .line 538
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->metaView:Landroid/widget/TextView;

    invoke-virtual {v4, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 539
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->metaView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 542
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v18

    .line 543
    .local v18, releaseDate:Ljava/util/Date;
    if-nez v18, :cond_4

    .line 544
    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Movie;->getDvdReleaseDate()Ljava/util/Date;

    move-result-object v18

    .line 547
    :cond_4
    const/16 v19, 0x0

    .line 548
    .local v19, releaseYear:Ljava/lang/String;
    if-eqz v18, :cond_5

    .line 550
    invoke-virtual/range {v18 .. v18}, Ljava/util/Date;->getYear()I

    move-result v4

    add-int/lit16 v4, v4, 0x76c

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    .line 551
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "title"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 552
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->titleView:Landroid/widget/TextView;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 557
    :cond_5
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->releaseView:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 559
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->movieLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 560
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->movieLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ActorPage;->movieClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 561
    return-object p2

    .line 423
    .end local v13           #isReleased:Z
    .end local v15           #meta:Ljava/lang/String;
    .end local v17           #ratingType:I
    .end local v18           #releaseDate:Ljava/util/Date;
    .end local v19           #releaseYear:Ljava/lang/String;
    :cond_6
    const-string v4, "thumbnail"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 424
    .local v6, thumbnailUrl:Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 425
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 426
    new-instance v3, Lnet/flixster/android/model/ImageOrder;

    const/4 v4, 0x0

    .line 427
    move-object/from16 v0, v25

    iget-object v7, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lnet/flixster/android/ActorPage;->movieThumbnailHandler:Landroid/os/Handler;

    move-object/from16 v5, p1

    .line 426
    invoke-direct/range {v3 .. v8}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 428
    .local v3, imageOrder:Lnet/flixster/android/model/ImageOrder;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lnet/flixster/android/ActorPage;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    goto/16 :goto_0

    .line 437
    .end local v3           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .end local v6           #thumbnailUrl:Ljava/lang/String;
    :cond_7
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->charView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 438
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->charView:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 448
    :cond_8
    const/4 v13, 0x1

    goto/16 :goto_2

    .line 454
    .restart local v13       #isReleased:Z
    .restart local v17       #ratingType:I
    :pswitch_0
    const-string v4, "popcornScore"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 455
    const-string v4, "popcornScore"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 456
    .local v9, flixsterScore:I
    if-lez v9, :cond_2

    .line 457
    const/16 v4, 0x3c

    if-ge v9, v4, :cond_9

    const/4 v14, 0x1

    .line 458
    .local v14, isSpilled:Z
    :goto_5
    if-nez v13, :cond_a

    const v12, 0x7f0200f6

    .line 460
    .local v12, iconId:I
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    invoke-virtual {v4, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v21

    .line 461
    .local v21, scoreIcon:Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->scoreView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v9, v1, v4}, Lnet/flixster/android/ActorPage;->addScore(ILandroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 457
    .end local v12           #iconId:I
    .end local v14           #isSpilled:Z
    .end local v21           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_9
    const/4 v14, 0x0

    goto :goto_5

    .line 458
    .restart local v14       #isSpilled:Z
    :cond_a
    if-eqz v14, :cond_b

    const v12, 0x7f0200ef

    goto :goto_6

    .line 459
    :cond_b
    const v12, 0x7f0200ea

    goto :goto_6

    .line 468
    .end local v9           #flixsterScore:I
    .end local v14           #isSpilled:Z
    :pswitch_1
    const-string v4, "rottenTomatoes"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 469
    const-string v4, "rottenTomatoes"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v24

    .line 470
    .local v24, tomatoScore:I
    if-lez v24, :cond_2

    .line 472
    const/16 v4, 0x3c

    move/from16 v0, v24

    if-ge v0, v4, :cond_c

    .line 473
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0200ed

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .line 477
    .local v23, tomatoIcon:Landroid/graphics/drawable/Drawable;
    :goto_7
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->scoreView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move/from16 v1, v24

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2, v4}, Lnet/flixster/android/ActorPage;->addScore(ILandroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 475
    .end local v23           #tomatoIcon:Landroid/graphics/drawable/Drawable;
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0200de

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .restart local v23       #tomatoIcon:Landroid/graphics/drawable/Drawable;
    goto :goto_7

    .line 485
    .end local v23           #tomatoIcon:Landroid/graphics/drawable/Drawable;
    .end local v24           #tomatoScore:I
    :cond_d
    const-string v4, "FRIENDS_RATED_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 486
    const-string v4, "FRIENDS_RATED_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_f

    .line 487
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0200eb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    .line 488
    .local v20, reviewIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 489
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v4, v0, v5, v7, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 490
    const-string v4, "FRIENDS_RATED_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 491
    .local v10, friendsRatedCount:I
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 492
    .local v16, ratingCountBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    const/4 v4, 0x1

    if-le v10, v4, :cond_e

    .line 494
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c0090

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 498
    :goto_8
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 499
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 496
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c008f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 500
    .end local v10           #friendsRatedCount:I
    .end local v16           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v20           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_f
    const-string v4, "FRIENDS_WTS_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 501
    const-string v4, "FRIENDS_WTS_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_13

    .line 502
    const-string v4, "FRIENDS_WTS_COUNT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 503
    .local v11, friendsWantToSeeCount:I
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    .line 504
    .local v26, wantToSeeCountBuilder:Ljava/lang/StringBuilder;
    if-nez v13, :cond_11

    if-nez v17, :cond_11

    .line 505
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v28, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v4, v5, v7, v8, v0}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 506
    const-string v4, "("

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 507
    const/4 v4, 0x1

    if-le v11, v4, :cond_10

    .line 508
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c008e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    :goto_9
    const-string v4, ")"

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    :goto_a
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 525
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 510
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c008d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 514
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0200f7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v27

    .line 515
    .local v27, wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v5, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 516
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v4, v0, v5, v7, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 517
    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 518
    const/4 v4, 0x1

    if-le v11, v4, :cond_12

    .line 519
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c0094

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_a

    .line 521
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0c0093

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_a

    .line 527
    .end local v11           #friendsWantToSeeCount:I
    .end local v26           #wantToSeeCountBuilder:Ljava/lang/StringBuilder;
    .end local v27           #wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    :cond_13
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/ActorPage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 450
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private hideLoading()V
    .locals 2

    .prologue
    .line 637
    iget-object v0, p0, Lnet/flixster/android/ActorPage;->throbber:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 638
    return-void
.end method

.method private scheduleUpdatePageTask()V
    .locals 5

    .prologue
    .line 125
    iget-object v2, p0, Lnet/flixster/android/ActorPage;->throbberHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 127
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    .line 128
    .local v0, currResumeCtr:I
    new-instance v1, Lnet/flixster/android/ActorPage$11;

    invoke-direct {v1, p0, v0}, Lnet/flixster/android/ActorPage$11;-><init>(Lnet/flixster/android/ActorPage;I)V

    .line 147
    .local v1, updatePageTask:Ljava/util/TimerTask;
    iget-object v2, p0, Lnet/flixster/android/ActorPage;->timer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 148
    iget-object v2, p0, Lnet/flixster/android/ActorPage;->timer:Ljava/util/Timer;

    const-wide/16 v3, 0x64

    invoke-virtual {v2, v1, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 150
    :cond_0
    return-void
.end method

.method private showLoading()V
    .locals 2

    .prologue
    .line 633
    iget-object v0, p0, Lnet/flixster/android/ActorPage;->throbber:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 634
    return-void
.end method

.method private updatePage()V
    .locals 34

    .prologue
    .line 173
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    if-eqz v4, :cond_4

    .line 174
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lnet/flixster/android/ActorPage;->inflater:Landroid/view/LayoutInflater;

    .line 175
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ActorPage;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    .line 176
    const v4, 0x7f070028

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/ActorPage;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    .line 177
    .local v17, actorLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f07002b

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 178
    .local v7, thumbnailView:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_5

    .line 179
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 192
    :goto_0
    const v4, 0x7f07002c

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 193
    .local v18, actorName:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    const v4, 0x7f07002d

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 195
    .local v15, actorBirthday:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->birthDay:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 196
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Birthday: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v5, v5, Lnet/flixster/android/model/Actor;->birthDay:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    const/4 v4, 0x0

    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 201
    :goto_1
    const v4, 0x7f07002e

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 202
    .local v16, actorBirthplace:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    if-eqz v4, :cond_8

    const-string v4, ""

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v5, v5, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 203
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Birthplace: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v5, v5, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 208
    :goto_2
    const v4, 0x7f07002f

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 209
    .local v19, biographyTitle:Landroid/widget/TextView;
    const v4, 0x7f070030

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/flixster/android/view/SynopsisView;

    .line 210
    .local v14, actorBiography:Lcom/flixster/android/view/SynopsisView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    if-eqz v4, :cond_9

    const-string v4, ""

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v5, v5, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 211
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 212
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Lcom/flixster/android/view/SynopsisView;->setVisibility(I)V

    .line 213
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    const/16 v5, 0xaa

    const/4 v8, 0x0

    invoke-virtual {v14, v4, v5, v8}, Lcom/flixster/android/view/SynopsisView;->load(Ljava/lang/String;IZ)V

    .line 218
    :goto_3
    const v4, 0x7f070031

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/TextView;

    .line 219
    .local v28, photosTitle:Landroid/widget/TextView;
    const v4, 0x7f070033

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/TextView;

    .line 220
    .local v30, viewPhotos:Landroid/widget/TextView;
    const v4, 0x7f070032

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/LinearLayout;

    .line 221
    .local v25, photoLayout:Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v0, v4, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    .line 222
    .local v27, photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    if-eqz v27, :cond_d

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_d

    .line 223
    invoke-virtual/range {v25 .. v25}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 224
    const/16 v20, 0x0

    .local v20, i:I
    :goto_4
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v20

    if-ge v0, v4, :cond_0

    const/16 v4, 0xf

    move/from16 v0, v20

    if-lt v0, v4, :cond_a

    .line 251
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 253
    new-instance v26, Ljava/lang/StringBuilder;

    const v4, 0x7f0c0040

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/ActorPage;->getResourceString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 254
    .local v26, photoLinkBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget v0, v4, Lnet/flixster/android/model/Actor;->photoCount:I

    move/from16 v24, v0

    .line 255
    .local v24, photoCount:I
    const/16 v4, 0x32

    move/from16 v0, v24

    if-le v0, v4, :cond_1

    .line 256
    const/16 v24, 0x32

    .line 258
    :cond_1
    const-string v4, " ("

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    const/4 v4, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 261
    const/4 v4, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 262
    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    .end local v20           #i:I
    .end local v24           #photoCount:I
    .end local v26           #photoLinkBuilder:Ljava/lang/StringBuilder;
    :goto_5
    const v4, 0x7f070035

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lnet/flixster/android/ActorPage;->filmographyLayout:Landroid/widget/LinearLayout;

    .line 269
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 270
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->filmographyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 271
    const v4, 0x7f070034

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    .line 272
    .local v31, wantToSeeText:Landroid/widget/TextView;
    const/4 v4, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 273
    const/16 v23, 0x0

    .line 274
    .local v23, movieView:Landroid/view/View;
    const/16 v20, 0x0

    .restart local v20       #i:I
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v20

    if-ge v0, v4, :cond_2

    const/4 v4, 0x3

    move/from16 v0, v20

    if-lt v0, v4, :cond_e

    .line 279
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x3

    if-le v4, v5, :cond_3

    .line 280
    const v4, 0x7f070036

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lnet/flixster/android/ActorPage;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    .line 281
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 282
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    .line 283
    .local v29, totalBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 284
    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ActorPage;->resources:Landroid/content/res/Resources;

    const v8, 0x7f0c0080

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    .line 286
    const v5, 0x7f070038

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v21

    .line 285
    check-cast v21, Landroid/widget/TextView;

    .line 287
    .local v21, moreFilmographyLabel:Landroid/widget/TextView;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 289
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 290
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->moreFilmographyLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ActorPage;->moreFilmographyListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    .end local v20           #i:I
    .end local v21           #moreFilmographyLabel:Landroid/widget/TextView;
    .end local v23           #movieView:Landroid/view/View;
    .end local v29           #totalBuilder:Ljava/lang/StringBuilder;
    .end local v31           #wantToSeeText:Landroid/widget/TextView;
    :cond_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v4

    const-string v5, "/actor"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Actor Actor Page for actor:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-wide v0, v9, Lnet/flixster/android/model/Actor;->id:J

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v5, v8}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    .end local v7           #thumbnailView:Landroid/widget/ImageView;
    .end local v14           #actorBiography:Lcom/flixster/android/view/SynopsisView;
    .end local v15           #actorBirthday:Landroid/widget/TextView;
    .end local v16           #actorBirthplace:Landroid/widget/TextView;
    .end local v17           #actorLayout:Landroid/widget/LinearLayout;
    .end local v18           #actorName:Landroid/widget/TextView;
    .end local v19           #biographyTitle:Landroid/widget/TextView;
    .end local v25           #photoLayout:Landroid/widget/LinearLayout;
    .end local v27           #photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    .end local v28           #photosTitle:Landroid/widget/TextView;
    .end local v30           #viewPhotos:Landroid/widget/TextView;
    :cond_4
    return-void

    .line 181
    .restart local v7       #thumbnailView:Landroid/widget/ImageView;
    .restart local v17       #actorLayout:Landroid/widget/LinearLayout;
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v6, v4, Lnet/flixster/android/model/Actor;->largeUrl:Ljava/lang/String;

    .line 182
    .local v6, thumbnailUrl:Ljava/lang/String;
    if-eqz v6, :cond_6

    .line 183
    const v4, 0x7f020151

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 184
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 185
    new-instance v3, Lnet/flixster/android/model/ImageOrder;

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    .line 186
    move-object/from16 v0, p0

    iget-object v8, v0, Lnet/flixster/android/ActorPage;->actorThumbnailHandler:Landroid/os/Handler;

    .line 185
    invoke-direct/range {v3 .. v8}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 187
    .local v3, imageOrder:Lnet/flixster/android/model/ImageOrder;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lnet/flixster/android/ActorPage;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    goto/16 :goto_0

    .line 189
    .end local v3           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_6
    const v4, 0x7f0201da

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 199
    .end local v6           #thumbnailUrl:Ljava/lang/String;
    .restart local v15       #actorBirthday:Landroid/widget/TextView;
    .restart local v18       #actorName:Landroid/widget/TextView;
    :cond_7
    const/16 v4, 0x8

    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 206
    .restart local v16       #actorBirthplace:Landroid/widget/TextView;
    :cond_8
    const/16 v4, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 215
    .restart local v14       #actorBiography:Lcom/flixster/android/view/SynopsisView;
    .restart local v19       #biographyTitle:Landroid/widget/TextView;
    :cond_9
    const/16 v4, 0x8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 216
    const/16 v4, 0x8

    invoke-virtual {v14, v4}, Lcom/flixster/android/view/SynopsisView;->setVisibility(I)V

    goto/16 :goto_3

    .line 225
    .restart local v20       #i:I
    .restart local v25       #photoLayout:Landroid/widget/LinearLayout;
    .restart local v27       #photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    .restart local v28       #photosTitle:Landroid/widget/TextView;
    .restart local v30       #viewPhotos:Landroid/widget/TextView;
    :cond_a
    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lnet/flixster/android/model/Photo;

    .line 226
    .local v10, photo:Lnet/flixster/android/model/Photo;
    new-instance v12, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 228
    .local v12, photoView:Landroid/widget/ImageView;
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ActorPage;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 229
    const v8, 0x7f0a0037

    .line 228
    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 229
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ActorPage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 230
    const v9, 0x7f0a0037

    .line 229
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    invoke-direct {v4, v5, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 228
    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 231
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ActorPage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a002b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v12, v4, v5, v8, v9}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 236
    invoke-virtual {v12, v10}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 237
    iget-object v4, v10, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_c

    .line 238
    iget-object v4, v10, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 247
    :cond_b
    :goto_7
    const/4 v4, 0x1

    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 248
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->photoClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 224
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_4

    .line 240
    :cond_c
    const v4, 0x7f020151

    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 241
    iget-object v4, v10, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    if-eqz v4, :cond_b

    iget-object v4, v10, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    const-string v5, "http://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 242
    new-instance v3, Lnet/flixster/android/model/ImageOrder;

    const/4 v9, 0x3

    .line 243
    iget-object v11, v10, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lnet/flixster/android/ActorPage;->photoHandler:Landroid/os/Handler;

    move-object v8, v3

    .line 242
    invoke-direct/range {v8 .. v13}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 244
    .restart local v3       #imageOrder:Lnet/flixster/android/model/ImageOrder;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lnet/flixster/android/ActorPage;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_7

    .line 264
    .end local v3           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .end local v10           #photo:Lnet/flixster/android/model/Photo;
    .end local v12           #photoView:Landroid/widget/ImageView;
    .end local v20           #i:I
    :cond_d
    const/16 v4, 0x8

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 265
    const/16 v4, 0x8

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 266
    const/16 v4, 0x8

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 275
    .restart local v20       #i:I
    .restart local v23       #movieView:Landroid/view/View;
    .restart local v31       #wantToSeeText:Landroid/widget/TextView;
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lnet/flixster/android/model/Movie;

    .line 276
    .local v22, movie:Lnet/flixster/android/model/Movie;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/ActorPage;->getMovieView(Lnet/flixster/android/model/Movie;Landroid/view/View;)Landroid/view/View;

    move-result-object v23

    .line 277
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ActorPage;->filmographyLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 274
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_6
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 600
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 610
    :cond_0
    :goto_0
    return-void

    .line 602
    :pswitch_0
    iget-object v1, p0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    if-eqz v1, :cond_0

    .line 603
    new-instance v0, Landroid/content/Intent;

    const-string v1, "PHOTOS"

    const/4 v2, 0x0

    const-class v3, Lnet/flixster/android/ActorGalleryPage;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 604
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "ACTOR_ID"

    iget-object v2, p0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-wide v2, v2, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 605
    const-string v1, "ACTOR_NAME"

    iget-object v2, p0, Lnet/flixster/android/ActorPage;->actor:Lnet/flixster/android/model/Actor;

    iget-object v2, v2, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 606
    invoke-virtual {p0, v0}, Lnet/flixster/android/ActorPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 600
    :pswitch_data_0
    .packed-switch 0x7f070033
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedState"

    .prologue
    .line 61
    const-string v0, "FlxMain"

    const-string v1, "ActorPage.onCreate"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f030018

    invoke-virtual {p0, v0}, Lnet/flixster/android/ActorPage;->setContentView(I)V

    .line 64
    invoke-virtual {p0}, Lnet/flixster/android/ActorPage;->createActionBar()V

    .line 66
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Lnet/flixster/android/ActorPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->throbber:Landroid/view/View;

    .line 67
    const v0, 0x7f07002a

    invoke-virtual {p0, v0}, Lnet/flixster/android/ActorPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/AdView;

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->mAdView:Lnet/flixster/android/ads/AdView;

    .line 68
    invoke-direct {p0}, Lnet/flixster/android/ActorPage;->updatePage()V

    .line 69
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "dialogId"

    .prologue
    .line 94
    packed-switch p1, :pswitch_data_0

    .line 108
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    .line 96
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 97
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v1, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 98
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 99
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 100
    const-string v1, "Retry"

    new-instance v2, Lnet/flixster/android/ActorPage$10;

    invoke-direct {v2, p0}, Lnet/flixster/android/ActorPage$10;-><init>(Lnet/flixster/android/ActorPage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 105
    invoke-virtual {p0}, Lnet/flixster/android/ActorPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 106
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 114
    const-string v0, "FlxMain"

    const-string v1, "ActorPage.onDestroy"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onDestroy()V

    .line 117
    iget-object v0, p0, Lnet/flixster/android/ActorPage;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lnet/flixster/android/ActorPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 119
    iget-object v0, p0, Lnet/flixster/android/ActorPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 121
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/ActorPage;->timer:Ljava/util/Timer;

    .line 122
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    .line 73
    const-string v3, "FlxMain"

    const-string v4, "ActorPage.onResume"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 75
    iget-wide v3, p0, Lnet/flixster/android/ActorPage;->actorId:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    .line 76
    invoke-virtual {p0}, Lnet/flixster/android/ActorPage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 77
    .local v1, extras:Landroid/os/Bundle;
    const-string v3, "ACTOR_ID"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lnet/flixster/android/ActorPage;->actorId:J

    .line 78
    const-string v3, "ACTOR_NAME"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 79
    const-string v3, "ACTOR_NAME"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, name:Ljava/lang/String;
    invoke-virtual {p0, v2}, Lnet/flixster/android/ActorPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 82
    .end local v2           #name:Ljava/lang/String;
    :cond_0
    new-instance v0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    iget-wide v3, p0, Lnet/flixster/android/ActorPage;->actorId:J

    invoke-direct {v0, v3, v4}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;-><init>(J)V

    .line 83
    .local v0, dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;
    iget-object v3, p0, Lnet/flixster/android/ActorPage;->mAdView:Lnet/flixster/android/ads/AdView;

    iput-object v0, v3, Lnet/flixster/android/ads/AdView;->dfpCustomTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    .line 85
    .end local v0           #dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;
    .end local v1           #extras:Landroid/os/Bundle;
    :cond_1
    iget-object v3, p0, Lnet/flixster/android/ActorPage;->timer:Ljava/util/Timer;

    if-nez v3, :cond_2

    .line 86
    new-instance v3, Ljava/util/Timer;

    invoke-direct {v3}, Ljava/util/Timer;-><init>()V

    iput-object v3, p0, Lnet/flixster/android/ActorPage;->timer:Ljava/util/Timer;

    .line 88
    :cond_2
    invoke-direct {p0}, Lnet/flixster/android/ActorPage;->scheduleUpdatePageTask()V

    .line 89
    iget-object v3, p0, Lnet/flixster/android/ActorPage;->mAdView:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v3}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 90
    return-void
.end method
