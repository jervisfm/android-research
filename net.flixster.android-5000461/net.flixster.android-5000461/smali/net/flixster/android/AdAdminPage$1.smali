.class Lnet/flixster/android/AdAdminPage$1;
.super Ljava/lang/Object;
.source "AdAdminPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/AdAdminPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/AdAdminPage;

.field private final synthetic val$doubleclickTestEntryView:Landroid/view/View;


# direct methods
.method constructor <init>(Lnet/flixster/android/AdAdminPage;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/AdAdminPage$1;->this$0:Lnet/flixster/android/AdAdminPage;

    iput-object p2, p0, Lnet/flixster/android/AdAdminPage$1;->val$doubleclickTestEntryView:Landroid/view/View;

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "whichButton"

    .prologue
    .line 84
    iget-object v1, p0, Lnet/flixster/android/AdAdminPage$1;->val$doubleclickTestEntryView:Landroid/view/View;

    .line 85
    const v2, 0x7f07006b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 84
    check-cast v0, Landroid/widget/EditText;

    .line 86
    .local v0, et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setAdAdminDoubleclickTest(Ljava/lang/String;)V

    .line 88
    iget-object v1, p0, Lnet/flixster/android/AdAdminPage$1;->this$0:Lnet/flixster/android/AdAdminPage;

    #getter for: Lnet/flixster/android/AdAdminPage;->mDoubleClickTestButton:Landroid/widget/Button;
    invoke-static {v1}, Lnet/flixster/android/AdAdminPage;->access$0(Lnet/flixster/android/AdAdminPage;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 89
    return-void
.end method
