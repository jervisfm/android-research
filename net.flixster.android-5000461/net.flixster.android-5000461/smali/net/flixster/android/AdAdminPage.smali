.class public Lnet/flixster/android/AdAdminPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "AdAdminPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final DIALOG_DOUBLECLICK_TEST:I = 0x3e9


# instance fields
.field private disableLaunchCap:Landroid/widget/CheckBox;

.field private disablePrerollCap:Landroid/widget/CheckBox;

.field private mDoubleClickTestButton:Landroid/widget/Button;

.field private mPosixInstallStamp:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/AdAdminPage;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lnet/flixster/android/AdAdminPage;->mDoubleClickTestButton:Landroid/widget/Button;

    return-object v0
.end method

.method private setDelaySubheader()V
    .locals 8

    .prologue
    .line 59
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sget-wide v4, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    .line 60
    const-wide v4, 0x4194997000000000L

    .line 59
    div-double v0, v2, v4

    .line 61
    .local v0, daysLeft:D
    iget-object v2, p0, Lnet/flixster/android/AdAdminPage;->mPosixInstallStamp:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Days Installed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "%2.2f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 96
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 117
    .end local p1
    :goto_0
    :pswitch_0
    return-void

    .line 98
    .restart local p1
    :pswitch_1
    const/16 v0, 0x3e9

    invoke-virtual {p0, v0}, Lnet/flixster/android/AdAdminPage;->showDialog(I)V

    goto :goto_0

    .line 101
    :pswitch_2
    sget-wide v0, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    .line 102
    const-wide/32 v2, 0xf731400

    .line 101
    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lnet/flixster/android/FlixsterApplication;->setPosixInstallTime(J)V

    .line 103
    invoke-direct {p0}, Lnet/flixster/android/AdAdminPage;->setDelaySubheader()V

    goto :goto_0

    .line 107
    :pswitch_3
    sget-wide v0, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    const-wide/32 v2, 0x2932e00

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lnet/flixster/android/FlixsterApplication;->setPosixInstallTime(J)V

    .line 108
    invoke-direct {p0}, Lnet/flixster/android/AdAdminPage;->setDelaySubheader()V

    goto :goto_0

    .line 111
    :pswitch_4
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setAdAdminDisableLaunchCap(Z)V

    goto :goto_0

    .line 114
    .restart local p1
    :pswitch_5
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setAdAdminDisablePrerollCap(Z)V

    goto :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x7f07003d
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const v3, 0x7f03001a

    invoke-virtual {p0, v3}, Lnet/flixster/android/AdAdminPage;->setContentView(I)V

    .line 31
    invoke-virtual {p0}, Lnet/flixster/android/AdAdminPage;->createActionBar()V

    .line 32
    const-string v3, "Ads Admin Page"

    invoke-virtual {p0, v3}, Lnet/flixster/android/AdAdminPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 34
    const v3, 0x7f070040

    invoke-virtual {p0, v3}, Lnet/flixster/android/AdAdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lnet/flixster/android/AdAdminPage;->mDoubleClickTestButton:Landroid/widget/Button;

    .line 35
    iget-object v3, p0, Lnet/flixster/android/AdAdminPage;->mDoubleClickTestButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    iget-object v3, p0, Lnet/flixster/android/AdAdminPage;->mDoubleClickTestButton:Landroid/widget/Button;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAdAdminDoubleClickTest()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 38
    const v3, 0x7f07003d

    invoke-virtual {p0, v3}, Lnet/flixster/android/AdAdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 39
    .local v0, mInstall3Days:Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    const v3, 0x7f07003e

    invoke-virtual {p0, v3}, Lnet/flixster/android/AdAdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 42
    .local v1, mInstallAdd12Hours:Landroid/widget/Button;
    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    const v3, 0x7f07003c

    invoke-virtual {p0, v3}, Lnet/flixster/android/AdAdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lnet/flixster/android/AdAdminPage;->mPosixInstallStamp:Landroid/widget/TextView;

    .line 45
    invoke-direct {p0}, Lnet/flixster/android/AdAdminPage;->setDelaySubheader()V

    .line 47
    const v3, 0x7f070044

    invoke-virtual {p0, v3}, Lnet/flixster/android/AdAdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 48
    .local v2, mPayloadText:Landroid/widget/TextView;
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/ads/AdManager;->getPayloadDump()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    const v3, 0x7f070042

    invoke-virtual {p0, v3}, Lnet/flixster/android/AdAdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lnet/flixster/android/AdAdminPage;->disableLaunchCap:Landroid/widget/CheckBox;

    .line 51
    iget-object v3, p0, Lnet/flixster/android/AdAdminPage;->disableLaunchCap:Landroid/widget/CheckBox;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isAdAdminLaunchCapDisabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 52
    iget-object v3, p0, Lnet/flixster/android/AdAdminPage;->disableLaunchCap:Landroid/widget/CheckBox;

    invoke-virtual {v3, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    const v3, 0x7f070043

    invoke-virtual {p0, v3}, Lnet/flixster/android/AdAdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lnet/flixster/android/AdAdminPage;->disablePrerollCap:Landroid/widget/CheckBox;

    .line 54
    iget-object v3, p0, Lnet/flixster/android/AdAdminPage;->disablePrerollCap:Landroid/widget/CheckBox;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isAdAdminPrerollCapDisabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 55
    iget-object v3, p0, Lnet/flixster/android/AdAdminPage;->disablePrerollCap:Landroid/widget/CheckBox;

    invoke-virtual {v3, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "id"

    .prologue
    const/4 v2, 0x0

    .line 76
    packed-switch p1, :pswitch_data_0

    .line 92
    :goto_0
    return-object v2

    .line 78
    :pswitch_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 79
    .local v1, factory:Landroid/view/LayoutInflater;
    const v3, 0x7f030023

    invoke-virtual {v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 80
    .local v0, doubleclickTestEntryView:Landroid/view/View;
    const/16 v3, 0x12c

    invoke-virtual {v0, v3}, Landroid/view/View;->setMinimumWidth(I)V

    .line 81
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 82
    const v4, 0x7f0c0049

    new-instance v5, Lnet/flixster/android/AdAdminPage$1;

    invoke-direct {v5, p0, v0}, Lnet/flixster/android/AdAdminPage$1;-><init>(Lnet/flixster/android/AdAdminPage;Landroid/view/View;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 90
    const v4, 0x7f0c004a

    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 121
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 67
    invoke-virtual {p0}, Lnet/flixster/android/AdAdminPage;->populatePage()V

    .line 68
    return-void
.end method

.method populatePage()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method
