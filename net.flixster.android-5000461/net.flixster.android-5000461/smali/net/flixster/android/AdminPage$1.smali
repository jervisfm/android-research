.class Lnet/flixster/android/AdminPage$1;
.super Ljava/lang/Object;
.source "AdminPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/AdminPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/AdminPage;

.field private final synthetic val$doubleclickTestEntryView:Landroid/view/View;


# direct methods
.method constructor <init>(Lnet/flixster/android/AdminPage;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/AdminPage$1;->this$0:Lnet/flixster/android/AdminPage;

    iput-object p2, p0, Lnet/flixster/android/AdminPage$1;->val$doubleclickTestEntryView:Landroid/view/View;

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "whichButton"

    .prologue
    .line 180
    iget-object v1, p0, Lnet/flixster/android/AdminPage$1;->val$doubleclickTestEntryView:Landroid/view/View;

    .line 181
    const v2, 0x7f07006b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 180
    check-cast v0, Landroid/widget/EditText;

    .line 182
    .local v0, et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setAdAdminDoubleclickTest(Ljava/lang/String;)V

    .line 184
    iget-object v1, p0, Lnet/flixster/android/AdminPage$1;->this$0:Lnet/flixster/android/AdminPage;

    #getter for: Lnet/flixster/android/AdminPage;->mDoubleClickTestButton:Landroid/widget/Button;
    invoke-static {v1}, Lnet/flixster/android/AdminPage;->access$0(Lnet/flixster/android/AdminPage;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 185
    return-void
.end method
