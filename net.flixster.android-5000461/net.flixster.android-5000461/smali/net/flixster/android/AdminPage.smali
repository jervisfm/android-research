.class public Lnet/flixster/android/AdminPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "AdminPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DIALOG_DOUBLECLICK_TEST:I = 0x3e9


# instance fields
.field private mApiSourceProduction:Landroid/widget/RadioButton;

.field private mApiSourceQaPreview:Landroid/widget/RadioButton;

.field private mApiSourceQaTrunk:Landroid/widget/RadioButton;

.field private mApiSourceStaging:Landroid/widget/RadioButton;

.field private mDoubleClickTestButton:Landroid/widget/Button;

.field private mPosixInstallStamp:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/AdminPage;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lnet/flixster/android/AdminPage;->mDoubleClickTestButton:Landroid/widget/Button;

    return-object v0
.end method

.method private populatePage()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 94
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAdminApiSource()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 108
    :goto_0
    return-void

    .line 96
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/AdminPage;->mApiSourceProduction:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 99
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/AdminPage;->mApiSourceStaging:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 102
    :pswitch_2
    iget-object v0, p0, Lnet/flixster/android/AdminPage;->mApiSourceQaTrunk:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 105
    :pswitch_3
    iget-object v0, p0, Lnet/flixster/android/AdminPage;->mApiSourceQaPreview:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setDelaySubheader()V
    .locals 8

    .prologue
    .line 111
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sget-wide v4, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    .line 112
    const-wide v4, 0x4194997000000000L

    .line 111
    div-double v0, v2, v4

    .line 113
    .local v0, daysLeft:D
    iget-object v2, p0, Lnet/flixster/android/AdminPage;->mPosixInstallStamp:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Days Installed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "%2.2f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    .line 118
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 168
    .end local p1
    :goto_0
    :pswitch_0
    return-void

    .line 120
    .restart local p1
    :pswitch_1
    const-string v1, "FlxMain"

    const-string v2, "AdminPage.onClick setAdminState:0"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const/4 v1, 0x0

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setAdminApiSource(I)V

    goto :goto_0

    .line 124
    :pswitch_2
    const-string v1, "FlxMain"

    const-string v2, "AdminPage.onClick setAdminState:1"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const/4 v1, 0x1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setAdminApiSource(I)V

    goto :goto_0

    .line 128
    :pswitch_3
    const-string v1, "FlxMain"

    const-string v2, "AdminPage.onClick setAdminState:2"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const/4 v1, 0x2

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setAdminApiSource(I)V

    goto :goto_0

    .line 132
    :pswitch_4
    const-string v1, "FlxMain"

    const-string v2, "AdminPage.onClick setAdminState:3"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const/4 v1, 0x3

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setAdminApiSource(I)V

    goto :goto_0

    .line 136
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    const-string v1, "ADADMIN"

    const/4 v2, 0x0

    const-class v3, Lnet/flixster/android/AdAdminPage;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    .local v0, adAdminIntent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lnet/flixster/android/AdminPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 140
    .end local v0           #adAdminIntent:Landroid/content/Intent;
    :pswitch_6
    invoke-static {p0}, Lcom/flixster/android/bootstrap/BootstrapActivity;->getMainIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/support/v4/app/NavUtils;->navigateUpTo(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 141
    invoke-static {}, Lnet/flixster/android/Starter;->forceClose()V

    goto :goto_0

    .line 146
    :pswitch_7
    const/16 v1, 0x3e9

    invoke-virtual {p0, v1}, Lnet/flixster/android/AdminPage;->showDialog(I)V

    goto :goto_0

    .line 149
    :pswitch_8
    sget-wide v1, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    .line 150
    const-wide/32 v3, 0xf731400

    .line 149
    sub-long/2addr v1, v3

    invoke-static {v1, v2}, Lnet/flixster/android/FlixsterApplication;->setPosixInstallTime(J)V

    .line 151
    invoke-direct {p0}, Lnet/flixster/android/AdminPage;->setDelaySubheader()V

    goto :goto_0

    .line 155
    :pswitch_9
    sget-wide v1, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    const-wide/32 v3, 0x2932e00

    add-long/2addr v1, v3

    invoke-static {v1, v2}, Lnet/flixster/android/FlixsterApplication;->setPosixInstallTime(J)V

    .line 156
    invoke-direct {p0}, Lnet/flixster/android/AdminPage;->setDelaySubheader()V

    goto :goto_0

    .line 159
    :pswitch_a
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setAdAdminDisableLaunchCap(Z)V

    goto :goto_0

    .line 162
    .restart local p1
    :pswitch_b
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setAdAdminDisablePrerollCap(Z)V

    goto :goto_0

    .line 165
    .restart local p1
    :pswitch_c
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setAdAdminDisablePostitialCap(Z)V

    goto/16 :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x7f07003d
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_c
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .parameter "savedInstanceState"

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v8, 0x7f03001b

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->setContentView(I)V

    .line 41
    invoke-virtual {p0}, Lnet/flixster/android/AdminPage;->createActionBar()V

    .line 42
    const-string v8, "Admin Page"

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 44
    const v8, 0x7f070048

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioButton;

    iput-object v8, p0, Lnet/flixster/android/AdminPage;->mApiSourceProduction:Landroid/widget/RadioButton;

    .line 45
    iget-object v8, p0, Lnet/flixster/android/AdminPage;->mApiSourceProduction:Landroid/widget/RadioButton;

    invoke-virtual {v8, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    const v8, 0x7f070049

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioButton;

    iput-object v8, p0, Lnet/flixster/android/AdminPage;->mApiSourceStaging:Landroid/widget/RadioButton;

    .line 47
    iget-object v8, p0, Lnet/flixster/android/AdminPage;->mApiSourceStaging:Landroid/widget/RadioButton;

    invoke-virtual {v8, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    const v8, 0x7f07004a

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioButton;

    iput-object v8, p0, Lnet/flixster/android/AdminPage;->mApiSourceQaTrunk:Landroid/widget/RadioButton;

    .line 49
    iget-object v8, p0, Lnet/flixster/android/AdminPage;->mApiSourceQaTrunk:Landroid/widget/RadioButton;

    invoke-virtual {v8, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    const v8, 0x7f07004b

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioButton;

    iput-object v8, p0, Lnet/flixster/android/AdminPage;->mApiSourceQaPreview:Landroid/widget/RadioButton;

    .line 51
    iget-object v8, p0, Lnet/flixster/android/AdminPage;->mApiSourceQaPreview:Landroid/widget/RadioButton;

    invoke-virtual {v8, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    const v8, 0x7f070045

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 54
    .local v3, mAdsMenuButton:Landroid/widget/Button;
    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const v8, 0x7f070046

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 56
    .local v4, mForceCloseButton:Landroid/widget/Button;
    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    const v8, 0x7f070040

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lnet/flixster/android/AdminPage;->mDoubleClickTestButton:Landroid/widget/Button;

    .line 60
    iget-object v8, p0, Lnet/flixster/android/AdminPage;->mDoubleClickTestButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v8, p0, Lnet/flixster/android/AdminPage;->mDoubleClickTestButton:Landroid/widget/Button;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAdAdminDoubleClickTest()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 63
    const v8, 0x7f07003d

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 64
    .local v5, mInstall3Days:Landroid/widget/Button;
    invoke-virtual {v5, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v8, 0x7f07003e

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 67
    .local v6, mInstallAdd12Hours:Landroid/widget/Button;
    invoke-virtual {v6, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    const v8, 0x7f07003c

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/AdminPage;->mPosixInstallStamp:Landroid/widget/TextView;

    .line 70
    invoke-direct {p0}, Lnet/flixster/android/AdminPage;->setDelaySubheader()V

    .line 72
    const v8, 0x7f070044

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 73
    .local v7, mPayloadText:Landroid/widget/TextView;
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v8

    invoke-virtual {v8}, Lnet/flixster/android/ads/AdManager;->getPayloadDump()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    const v8, 0x7f070042

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 77
    .local v0, disableLaunchCap:Landroid/widget/CheckBox;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isAdAdminLaunchCapDisabled()Z

    move-result v8

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 78
    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v8, 0x7f070043

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 80
    .local v2, disablePrerollCap:Landroid/widget/CheckBox;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isAdAdminPrerollCapDisabled()Z

    move-result v8

    invoke-virtual {v2, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 81
    invoke-virtual {v2, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v8, 0x7f07004d

    invoke-virtual {p0, v8}, Lnet/flixster/android/AdminPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 83
    .local v1, disablePostitialCap:Landroid/widget/CheckBox;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isAdAdminPostitialCapDisabled()Z

    move-result v8

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 84
    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "id"

    .prologue
    const/4 v2, 0x0

    .line 172
    packed-switch p1, :pswitch_data_0

    .line 188
    :goto_0
    return-object v2

    .line 174
    :pswitch_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 175
    .local v1, factory:Landroid/view/LayoutInflater;
    const v3, 0x7f030023

    invoke-virtual {v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 176
    .local v0, doubleclickTestEntryView:Landroid/view/View;
    const/16 v3, 0x12c

    invoke-virtual {v0, v3}, Landroid/view/View;->setMinimumWidth(I)V

    .line 177
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 178
    const v4, 0x7f0c0049

    new-instance v5, Lnet/flixster/android/AdminPage$1;

    invoke-direct {v5, p0, v0}, Lnet/flixster/android/AdminPage$1;-><init>(Lnet/flixster/android/AdminPage;Landroid/view/View;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 186
    const v4, 0x7f0c004a

    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 193
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 89
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 90
    invoke-direct {p0}, Lnet/flixster/android/AdminPage;->populatePage()V

    .line 91
    return-void
.end method
