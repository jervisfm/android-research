.class Lnet/flixster/android/BoxOfficePage$2;
.super Ljava/util/TimerTask;
.source "BoxOfficePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/BoxOfficePage;->ScheduleLoadItemsTask(IIJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/BoxOfficePage;

.field private final synthetic val$currResumeCtr:I

.field private final synthetic val$navSelection:I

.field private final synthetic val$sortOption:I


# direct methods
.method constructor <init>(Lnet/flixster/android/BoxOfficePage;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    iput p2, p0, Lnet/flixster/android/BoxOfficePage$2;->val$navSelection:I

    iput p3, p0, Lnet/flixster/android/BoxOfficePage$2;->val$currResumeCtr:I

    iput p4, p0, Lnet/flixster/android/BoxOfficePage$2;->val$sortOption:I

    .line 95
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    const-string v3, "FlxMain"

    const-string v4, "BoxOfficePage.ScheduleLoadItemsTask.run"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :try_start_0
    iget v3, p0, Lnet/flixster/android/BoxOfficePage$2;->val$navSelection:I

    const v4, 0x7f070258

    if-ne v3, v4, :cond_4

    .line 101
    iget-object v3, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOtw:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/flixster/android/BoxOfficePage;->access$4(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 102
    iget-object v3, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mBoxOfficeFeatured:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/flixster/android/BoxOfficePage;->access$5(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOtw:Ljava/util/ArrayList;
    invoke-static {v4}, Lnet/flixster/android/BoxOfficePage;->access$4(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mBoxOfficeTbo:Ljava/util/ArrayList;
    invoke-static {v5}, Lnet/flixster/android/BoxOfficePage;->access$6(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOthers:Ljava/util/ArrayList;
    invoke-static {v6}, Lnet/flixster/android/BoxOfficePage;->access$7(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;

    move-result-object v6

    .line 103
    iget-object v7, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget v7, v7, Lnet/flixster/android/BoxOfficePage;->mRetryCount:I

    if-nez v7, :cond_2

    .line 102
    :goto_0
    invoke-static {v3, v4, v5, v6, v1}, Lnet/flixster/android/data/MovieDao;->fetchBoxOffice(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V

    .line 106
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget v3, p0, Lnet/flixster/android/BoxOfficePage$2;->val$currResumeCtr:I

    invoke-virtual {v1, v3}, Lnet/flixster/android/BoxOfficePage;->shouldSkipBackgroundTask(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_3

    .line 143
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v1, v1, Lnet/flixster/android/BoxOfficePage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 145
    :goto_1
    return-void

    :cond_2
    move v1, v2

    .line 103
    goto :goto_0

    .line 110
    :cond_3
    :try_start_1
    iget v1, p0, Lnet/flixster/android/BoxOfficePage$2;->val$sortOption:I

    packed-switch v1, :pswitch_data_0

    .line 136
    :goto_2
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    invoke-virtual {v1}, Lnet/flixster/android/BoxOfficePage;->trackPage()V

    .line 137
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v1, v1, Lnet/flixster/android/BoxOfficePage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 138
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #calls: Lnet/flixster/android/BoxOfficePage;->checkAndShowLaunchAd()V
    invoke-static {v1}, Lnet/flixster/android/BoxOfficePage;->access$15(Lnet/flixster/android/BoxOfficePage;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    .line 143
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v1, v1, Lnet/flixster/android/BoxOfficePage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 112
    :pswitch_0
    :try_start_2
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    const/4 v3, 0x1

    #setter for: Lnet/flixster/android/BoxOfficePage;->mSortOption:I
    invoke-static {v1, v3}, Lnet/flixster/android/BoxOfficePage;->access$8(Lnet/flixster/android/BoxOfficePage;I)V

    .line 113
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #calls: Lnet/flixster/android/BoxOfficePage;->setPopularLviList()V
    invoke-static {v1}, Lnet/flixster/android/BoxOfficePage;->access$9(Lnet/flixster/android/BoxOfficePage;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_3
    const-string v1, "FlxMain"

    const-string v3, "BoxOfficePage.ScheduleLoadItemsTask.run DaoException"

    invoke-static {v1, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 141
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    invoke-virtual {v1, v0}, Lnet/flixster/android/BoxOfficePage;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 143
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v1, v1, Lnet/flixster/android/BoxOfficePage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 116
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_1
    :try_start_4
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    const/4 v3, 0x2

    #setter for: Lnet/flixster/android/BoxOfficePage;->mSortOption:I
    invoke-static {v1, v3}, Lnet/flixster/android/BoxOfficePage;->access$8(Lnet/flixster/android/BoxOfficePage;I)V

    .line 117
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #calls: Lnet/flixster/android/BoxOfficePage;->setRatingLviList()V
    invoke-static {v1}, Lnet/flixster/android/BoxOfficePage;->access$10(Lnet/flixster/android/BoxOfficePage;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 142
    :catchall_0
    move-exception v1

    .line 143
    iget-object v3, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v3, v3, Lnet/flixster/android/BoxOfficePage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 144
    throw v1

    .line 120
    :pswitch_2
    :try_start_5
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    const/4 v3, 0x3

    #setter for: Lnet/flixster/android/BoxOfficePage;->mSortOption:I
    invoke-static {v1, v3}, Lnet/flixster/android/BoxOfficePage;->access$8(Lnet/flixster/android/BoxOfficePage;I)V

    .line 121
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #calls: Lnet/flixster/android/BoxOfficePage;->setAlphaLviList()V
    invoke-static {v1}, Lnet/flixster/android/BoxOfficePage;->access$11(Lnet/flixster/android/BoxOfficePage;)V

    goto :goto_2

    .line 125
    :cond_4
    iget-object v3, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mUpcoming:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/flixster/android/BoxOfficePage;->access$12(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 126
    iget-object v3, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mUpcomingFeatured:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/flixster/android/BoxOfficePage;->access$13(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mUpcoming:Ljava/util/ArrayList;
    invoke-static {v4}, Lnet/flixster/android/BoxOfficePage;->access$12(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget v5, v5, Lnet/flixster/android/BoxOfficePage;->mRetryCount:I

    if-nez v5, :cond_6

    :goto_3
    invoke-static {v3, v4, v1}, Lnet/flixster/android/data/MovieDao;->fetchUpcoming(Ljava/util/List;Ljava/util/List;Z)V

    .line 129
    :cond_5
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget v3, p0, Lnet/flixster/android/BoxOfficePage$2;->val$currResumeCtr:I

    invoke-virtual {v1, v3}, Lnet/flixster/android/BoxOfficePage;->shouldSkipBackgroundTask(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 133
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$2;->this$0:Lnet/flixster/android/BoxOfficePage;

    #calls: Lnet/flixster/android/BoxOfficePage;->setUpcomingLviList()V
    invoke-static {v1}, Lnet/flixster/android/BoxOfficePage;->access$14(Lnet/flixster/android/BoxOfficePage;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_2

    :cond_6
    move v1, v2

    .line 126
    goto :goto_3

    .line 110
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
