.class Lnet/flixster/android/BoxOfficePage$3;
.super Ljava/lang/Object;
.source "BoxOfficePage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/BoxOfficePage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/BoxOfficePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/BoxOfficePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/BoxOfficePage$3;->this$0:Lnet/flixster/android/BoxOfficePage;

    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .parameter "dialog"
    .parameter "which"

    .prologue
    const-wide/16 v4, 0x64

    const/4 v3, 0x0

    .line 384
    packed-switch p2, :pswitch_data_0

    .line 400
    :goto_0
    return-void

    .line 386
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage$3;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$3;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mNavSelect:I
    invoke-static {v1}, Lnet/flixster/android/BoxOfficePage;->access$1(Lnet/flixster/android/BoxOfficePage;)I

    move-result v1

    const/4 v2, 0x1

    #calls: Lnet/flixster/android/BoxOfficePage;->ScheduleLoadItemsTask(IIJ)V
    invoke-static {v0, v1, v2, v4, v5}, Lnet/flixster/android/BoxOfficePage;->access$3(Lnet/flixster/android/BoxOfficePage;IIJ)V

    .line 388
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage$3;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v0, v0, Lnet/flixster/android/BoxOfficePage;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    .line 391
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage$3;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$3;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mNavSelect:I
    invoke-static {v1}, Lnet/flixster/android/BoxOfficePage;->access$1(Lnet/flixster/android/BoxOfficePage;)I

    move-result v1

    const/4 v2, 0x2

    #calls: Lnet/flixster/android/BoxOfficePage;->ScheduleLoadItemsTask(IIJ)V
    invoke-static {v0, v1, v2, v4, v5}, Lnet/flixster/android/BoxOfficePage;->access$3(Lnet/flixster/android/BoxOfficePage;IIJ)V

    .line 392
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage$3;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v0, v0, Lnet/flixster/android/BoxOfficePage;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    .line 395
    :pswitch_2
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage$3;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage$3;->this$0:Lnet/flixster/android/BoxOfficePage;

    #getter for: Lnet/flixster/android/BoxOfficePage;->mNavSelect:I
    invoke-static {v1}, Lnet/flixster/android/BoxOfficePage;->access$1(Lnet/flixster/android/BoxOfficePage;)I

    move-result v1

    const/4 v2, 0x3

    #calls: Lnet/flixster/android/BoxOfficePage;->ScheduleLoadItemsTask(IIJ)V
    invoke-static {v0, v1, v2, v4, v5}, Lnet/flixster/android/BoxOfficePage;->access$3(Lnet/flixster/android/BoxOfficePage;IIJ)V

    .line 396
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage$3;->this$0:Lnet/flixster/android/BoxOfficePage;

    iget-object v0, v0, Lnet/flixster/android/BoxOfficePage;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    .line 384
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
