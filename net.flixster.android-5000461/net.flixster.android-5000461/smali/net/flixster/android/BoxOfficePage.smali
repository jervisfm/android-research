.class public Lnet/flixster/android/BoxOfficePage;
.super Lnet/flixster/android/LviActivityCopy;
.source "BoxOfficePage.java"


# static fields
.field public static final LISTSTATE_SORT:Ljava/lang/String; = "LISTSTATE_SORT"

.field public static final SORT_ALPHA:I = 0x3

.field public static final SORT_POPULAR:I = 0x1

.field public static final SORT_RATING:I = 0x2

.field public static final SORT_START:I


# instance fields
.field private final mBoxOfficeFeatured:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mBoxOfficeOthers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mBoxOfficeOtw:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mBoxOfficeTbo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private mNavListener:Landroid/view/View$OnClickListener;

.field private mNavSelect:I

.field private mSortOption:I

.field private final mUpcoming:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mUpcomingFeatured:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private navBar:Lcom/flixster/android/view/SubNavBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lnet/flixster/android/LviActivityCopy;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOtw:Ljava/util/ArrayList;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeTbo:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOthers:Ljava/util/ArrayList;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mUpcoming:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mUpcomingFeatured:Ljava/util/ArrayList;

    .line 422
    new-instance v0, Lnet/flixster/android/BoxOfficePage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/BoxOfficePage$1;-><init>(Lnet/flixster/android/BoxOfficePage;)V

    iput-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mNavListener:Landroid/view/View$OnClickListener;

    .line 34
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(IIJ)V
    .locals 4
    .parameter "navSelection"
    .parameter "sortOption"
    .parameter "delay"

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/BoxOfficePage;->throbberHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 94
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    .line 95
    .local v0, currResumeCtr:I
    new-instance v1, Lnet/flixster/android/BoxOfficePage$2;

    invoke-direct {v1, p0, p1, v0, p2}, Lnet/flixster/android/BoxOfficePage$2;-><init>(Lnet/flixster/android/BoxOfficePage;III)V

    .line 149
    .local v1, loadMoviesTask:Ljava/util/TimerTask;
    iget-object v2, p0, Lnet/flixster/android/BoxOfficePage;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 150
    iget-object v2, p0, Lnet/flixster/android/BoxOfficePage;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v1, p3, p4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    :cond_0
    monitor-exit p0

    return-void

    .line 92
    .end local v0           #currResumeCtr:I
    .end local v1           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lnet/flixster/android/BoxOfficePage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/BoxOfficePage;)I
    .locals 1
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    return v0
.end method

.method static synthetic access$10(Lnet/flixster/android/BoxOfficePage;)V
    .locals 0
    .parameter

    .prologue
    .line 206
    invoke-direct {p0}, Lnet/flixster/android/BoxOfficePage;->setRatingLviList()V

    return-void
.end method

.method static synthetic access$11(Lnet/flixster/android/BoxOfficePage;)V
    .locals 0
    .parameter

    .prologue
    .line 249
    invoke-direct {p0}, Lnet/flixster/android/BoxOfficePage;->setAlphaLviList()V

    return-void
.end method

.method static synthetic access$12(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mUpcoming:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$13(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mUpcomingFeatured:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$14(Lnet/flixster/android/BoxOfficePage;)V
    .locals 0
    .parameter

    .prologue
    .line 297
    invoke-direct {p0}, Lnet/flixster/android/BoxOfficePage;->setUpcomingLviList()V

    return-void
.end method

.method static synthetic access$15(Lnet/flixster/android/BoxOfficePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->checkAndShowLaunchAd()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/BoxOfficePage;)I
    .locals 1
    .parameter

    .prologue
    .line 49
    iget v0, p0, Lnet/flixster/android/BoxOfficePage;->mSortOption:I

    return v0
.end method

.method static synthetic access$3(Lnet/flixster/android/BoxOfficePage;IIJ)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3, p4}, Lnet/flixster/android/BoxOfficePage;->ScheduleLoadItemsTask(IIJ)V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOtw:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeTbo:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$7(Lnet/flixster/android/BoxOfficePage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOthers:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/BoxOfficePage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput p1, p0, Lnet/flixster/android/BoxOfficePage;->mSortOption:I

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/BoxOfficePage;)V
    .locals 0
    .parameter

    .prologue
    .line 159
    invoke-direct {p0}, Lnet/flixster/android/BoxOfficePage;->setPopularLviList()V

    return-void
.end method

.method private setAlphaLviList()V
    .locals 11

    .prologue
    .line 250
    const-string v8, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    iget-object v10, p0, Lnet/flixster/android/BoxOfficePage;->className:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, ".setAlphaLviList"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 256
    .local v0, completeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 257
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOtw:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 258
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeTbo:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 259
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOthers:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 261
    new-instance v7, Lnet/flixster/android/model/MovieTitleComparator;

    invoke-direct {v7}, Lnet/flixster/android/model/MovieTitleComparator;-><init>()V

    .line 262
    .local v7, movieTitleComparator:Lnet/flixster/android/model/MovieTitleComparator;
    invoke-static {v0, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 264
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 266
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->destroyExistingLviAd()V

    .line 267
    new-instance v8, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v8, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 268
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v9, "MoviesTab"

    iput-object v9, v8, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 269
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    const/4 v3, 0x0

    .line 272
    .local v3, indexChar:C
    const/4 v1, 0x0

    .line 274
    .local v1, firstLetter:C
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_0

    .line 292
    new-instance v2, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 293
    .local v2, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    return-void

    .line 274
    .end local v2           #footer:Lnet/flixster/android/lvi/LviFooter;
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lnet/flixster/android/model/Movie;

    .line 276
    .local v6, movie:Lnet/flixster/android/model/Movie;
    const-string v9, "title"

    invoke-virtual {v6, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 277
    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v9

    if-nez v9, :cond_1

    .line 278
    const/16 v1, 0x23

    .line 280
    :cond_1
    if-eq v3, v1, :cond_2

    .line 281
    new-instance v5, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 282
    .local v5, lviSubHeader:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 283
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    move v3, v1

    .line 286
    .end local v5           #lviSubHeader:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_2
    new-instance v4, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 287
    .local v4, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v6, v4, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 288
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v9

    iput-object v9, v4, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 289
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private setPopularLviList()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 160
    const-string v8, "FlxMain"

    new-instance v11, Ljava/lang/StringBuilder;

    iget-object v12, p0, Lnet/flixster/android/BoxOfficePage;->className:Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, ".setPopularLviList"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 166
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->destroyExistingLviAd()V

    .line 167
    new-instance v8, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v8, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 168
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v11, "MoviesTab"

    iput-object v11, v8, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 169
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v11, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .local v0, boxOfficeLists:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;>;"
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    invoke-static {v8}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOtw:Ljava/util/ArrayList;

    invoke-static {v8}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeTbo:Ljava/util/ArrayList;

    invoke-static {v8}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOthers:Ljava/util/ArrayList;

    invoke-static {v8}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    new-array v7, v13, [Ljava/lang/String;

    .line 178
    .local v7, subheaderList:[Ljava/lang/String;
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v11, 0x7f0c00be

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    .line 179
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v11, 0x7f0c00bf

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    .line 180
    const/4 v8, 0x2

    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c00c0

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v7, v8

    .line 181
    const/4 v8, 0x3

    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c00c1

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v7, v8

    .line 185
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-lt v2, v13, :cond_0

    .line 202
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 203
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    return-void

    .line 186
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 187
    .local v5, movieList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 188
    new-instance v6, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 189
    .local v6, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    aget-object v8, v7, v2

    iput-object v8, v6, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 190
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 185
    .end local v6           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 192
    .restart local v6       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Movie;

    .line 193
    .local v4, movie:Lnet/flixster/android/model/Movie;
    new-instance v3, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 194
    .local v3, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v4, v3, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 195
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v8

    iput-object v8, v3, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 196
    if-nez v2, :cond_3

    move v8, v9

    :goto_2
    iput-boolean v8, v3, Lnet/flixster/android/lvi/LviMovie;->mIsFeatured:Z

    .line 197
    iget-object v8, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move v8, v10

    .line 196
    goto :goto_2
.end method

.method private setRatingLviList()V
    .locals 12

    .prologue
    .line 207
    const-string v9, "FlxMain"

    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lnet/flixster/android/BoxOfficePage;->className:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, ".setRatingLviList"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 212
    .local v0, completeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 213
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOtw:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 214
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeTbo:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 215
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOthers:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 217
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v8

    .line 218
    .local v8, rt:I
    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 219
    new-instance v7, Lnet/flixster/android/model/MovieRottenScoreComparator;

    invoke-direct {v7}, Lnet/flixster/android/model/MovieRottenScoreComparator;-><init>()V

    .line 220
    .local v7, movieRottenScoreComparator:Lnet/flixster/android/model/MovieRottenScoreComparator;
    invoke-static {v0, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 226
    .end local v7           #movieRottenScoreComparator:Lnet/flixster/android/model/MovieRottenScoreComparator;
    :goto_0
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 228
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->destroyExistingLviAd()V

    .line 229
    new-instance v9, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v9}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v9, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 230
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v10, "MoviesTab"

    iput-object v10, v9, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 231
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v10, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    const-wide/16 v2, 0x0

    .line 234
    .local v2, lastMovieId:J
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 244
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 245
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    return-void

    .line 222
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    .end local v2           #lastMovieId:J
    :cond_1
    new-instance v6, Lnet/flixster/android/model/MoviePopcornComparator;

    invoke-direct {v6}, Lnet/flixster/android/model/MoviePopcornComparator;-><init>()V

    .line 223
    .local v6, moviePopcornComparator:Lnet/flixster/android/model/MoviePopcornComparator;
    invoke-static {v0, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    .line 234
    .end local v6           #moviePopcornComparator:Lnet/flixster/android/model/MoviePopcornComparator;
    .restart local v2       #lastMovieId:J
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Movie;

    .line 235
    .local v5, movie:Lnet/flixster/android/model/Movie;
    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v10

    cmp-long v10, v2, v10

    if-eqz v10, :cond_0

    .line 236
    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v2

    .line 237
    new-instance v4, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 238
    .local v4, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v5, v4, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 239
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v4, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 240
    iget-object v10, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private setUpcomingLviList()V
    .locals 11

    .prologue
    .line 298
    const-string v9, "FlxMain"

    const-string v10, "BoxOfficePage.setUpcomingLviList "

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 304
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->destroyExistingLviAd()V

    .line 305
    new-instance v9, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v9}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v9, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 306
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v10, "UpcomingTab"

    iput-object v10, v9, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 307
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v10, p0, Lnet/flixster/android/BoxOfficePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mUpcomingFeatured:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 311
    new-instance v6, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 312
    .local v6, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c00be

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 313
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mUpcomingFeatured:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    .line 315
    .local v4, mUpcomingFeaturedCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 325
    .end local v4           #mUpcomingFeaturedCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .end local v6           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 326
    .local v0, date:Ljava/util/Date;
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mUpcoming:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 327
    .local v3, mUpcomingCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 342
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 343
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v9, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    return-void

    .line 315
    .end local v0           #date:Ljava/util/Date;
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    .end local v3           #mUpcomingCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .restart local v4       #mUpcomingFeaturedCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .restart local v6       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Movie;

    .line 316
    .local v5, movie:Lnet/flixster/android/model/Movie;
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 317
    .local v2, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v5, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 318
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v2, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 319
    iget-object v10, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 327
    .end local v2           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v4           #mUpcomingFeaturedCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .end local v5           #movie:Lnet/flixster/android/model/Movie;
    .end local v6           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    .restart local v0       #date:Ljava/util/Date;
    .restart local v3       #mUpcomingCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Movie;

    .line 328
    .restart local v5       #movie:Lnet/flixster/android/model/Movie;
    const-string v10, "theaterReleaseDate"

    invoke-virtual {v5, v10}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 329
    .local v8, theaterReleaseString:Ljava/lang/String;
    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v7

    .line 330
    .local v7, theaterRelease:Ljava/util/Date;
    if-eqz v7, :cond_3

    invoke-virtual {v0, v7}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 331
    new-instance v6, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 332
    .restart local v6       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    iput-object v8, v6, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 333
    iget-object v10, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    move-object v0, v7

    .line 336
    .end local v6           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_3
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 337
    .restart local v2       #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v5, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 338
    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v2, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 339
    iget-object v10, p0, Lnet/flixster/android/BoxOfficePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method protected getAnalyticsAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 481
    const-string v0, "Logo"

    return-object v0
.end method

.method protected getAnalyticsCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 476
    const-string v0, "WidgetEntrance"

    return-object v0
.end method

.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 3

    .prologue
    .line 434
    const/4 v0, 0x0

    .line 435
    .local v0, tag:Ljava/lang/String;
    iget v1, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    const v2, 0x7f070258

    if-ne v1, v2, :cond_0

    .line 436
    iget v1, p0, Lnet/flixster/android/BoxOfficePage;->mSortOption:I

    packed-switch v1, :pswitch_data_0

    .line 450
    :goto_0
    return-object v0

    .line 438
    :pswitch_0
    const-string v0, "/boxoffice/popular"

    .line 439
    goto :goto_0

    .line 441
    :pswitch_1
    const-string v0, "/boxoffice/rating"

    .line 442
    goto :goto_0

    .line 444
    :pswitch_2
    const-string v0, "/boxoffice/title"

    goto :goto_0

    .line 448
    :cond_0
    const-string v0, "/upcoming"

    goto :goto_0

    .line 436
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 455
    const/4 v0, 0x0

    .line 456
    .local v0, title:Ljava/lang/String;
    iget v1, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    const v2, 0x7f070258

    if-ne v1, v2, :cond_0

    .line 457
    iget v1, p0, Lnet/flixster/android/BoxOfficePage;->mSortOption:I

    packed-switch v1, :pswitch_data_0

    .line 471
    :goto_0
    return-object v0

    .line 459
    :pswitch_0
    const-string v0, "Box Office - Popular"

    .line 460
    goto :goto_0

    .line 462
    :pswitch_1
    const-string v0, "Box Office - Rating"

    .line 463
    goto :goto_0

    .line 465
    :pswitch_2
    const-string v0, "Box Office - Title"

    goto :goto_0

    .line 469
    :cond_0
    const-string v0, "Upcoming Movies"

    goto :goto_0

    .line 457
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected getFeaturedMovies()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 486
    iget v0, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    const v1, 0x7f070258

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mUpcomingFeatured:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedState"

    .prologue
    const v5, 0x7f070258

    .line 56
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onCreate(Landroid/os/Bundle;)V

    .line 57
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/BoxOfficePage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 58
    const v1, 0x7f0700f3

    invoke-virtual {p0, v1}, Lnet/flixster/android/BoxOfficePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 60
    .local v0, mNavbarHolder:Landroid/widget/LinearLayout;
    new-instance v1, Lcom/flixster/android/view/SubNavBar;

    invoke-direct {v1, p0}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lnet/flixster/android/BoxOfficePage;->navBar:Lcom/flixster/android/view/SubNavBar;

    .line 61
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget-object v2, p0, Lnet/flixster/android/BoxOfficePage;->mNavListener:Landroid/view/View$OnClickListener;

    const v3, 0x7f0c0138

    const v4, 0x7f0c0139

    invoke-virtual {v1, v2, v3, v4}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;II)V

    .line 62
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage;->navBar:Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {v1, v5}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 63
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage;->navBar:Lcom/flixster/android/view/SubNavBar;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 65
    iput v5, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    .line 66
    const/4 v1, 0x1

    iput v1, p0, Lnet/flixster/android/BoxOfficePage;->mSortOption:I

    .line 68
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    const-string v2, "MoviesTabStickyTop"

    invoke-virtual {v1, v2}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 69
    iget-object v1, p0, Lnet/flixster/android/BoxOfficePage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    const-string v2, "MoviesTabStickyBottom"

    invoke-virtual {v1, v2}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "id"

    .prologue
    .line 379
    packed-switch p1, :pswitch_data_0

    .line 405
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 381
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c0020

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 382
    const v1, 0x7f0e0004

    new-instance v2, Lnet/flixster/android/BoxOfficePage$3;

    invoke-direct {v2, p0}, Lnet/flixster/android/BoxOfficePage$3;-><init>(Lnet/flixster/android/BoxOfficePage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 401
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 379
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Lnet/flixster/android/LviActivityCopy;->onPause()V

    .line 82
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 83
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOtw:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 84
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeTbo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 85
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mBoxOfficeOthers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 87
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mUpcoming:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 88
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->mUpcomingFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 89
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 417
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 418
    const-string v0, "LISTSTATE_SORT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/BoxOfficePage;->mSortOption:I

    .line 419
    const-string v0, "LISTSTATE_NAV"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    .line 420
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 74
    invoke-super {p0}, Lnet/flixster/android/LviActivityCopy;->onResume()V

    .line 75
    iget-object v0, p0, Lnet/flixster/android/BoxOfficePage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget v1, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 76
    iget v0, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    iget v1, p0, Lnet/flixster/android/BoxOfficePage;->mSortOption:I

    const-wide/16 v2, 0x64

    invoke-direct {p0, v0, v1, v2, v3}, Lnet/flixster/android/BoxOfficePage;->ScheduleLoadItemsTask(IIJ)V

    .line 77
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 410
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 411
    const-string v0, "LISTSTATE_SORT"

    iget v1, p0, Lnet/flixster/android/BoxOfficePage;->mSortOption:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 412
    const-string v0, "LISTSTATE_NAV"

    iget v1, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 413
    return-void
.end method

.method protected retryAction()V
    .locals 4

    .prologue
    .line 156
    iget v0, p0, Lnet/flixster/android/BoxOfficePage;->mNavSelect:I

    iget v1, p0, Lnet/flixster/android/BoxOfficePage;->mSortOption:I

    const-wide/16 v2, 0x3e8

    invoke-direct {p0, v0, v1, v2, v3}, Lnet/flixster/android/BoxOfficePage;->ScheduleLoadItemsTask(IIJ)V

    .line 157
    return-void
.end method
