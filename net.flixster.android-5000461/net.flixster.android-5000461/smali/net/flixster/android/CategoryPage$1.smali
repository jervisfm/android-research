.class Lnet/flixster/android/CategoryPage$1;
.super Ljava/util/TimerTask;
.source "CategoryPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/CategoryPage;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/CategoryPage;

.field private final synthetic val$currResumeCtr:I


# direct methods
.method constructor <init>(Lnet/flixster/android/CategoryPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iput p2, p0, Lnet/flixster/android/CategoryPage$1;->val$currResumeCtr:I

    .line 57
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 62
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    #getter for: Lnet/flixster/android/CategoryPage;->mCategory:Ljava/util/ArrayList;
    invoke-static {v1}, Lnet/flixster/android/CategoryPage;->access$0(Lnet/flixster/android/CategoryPage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    #getter for: Lnet/flixster/android/CategoryPage;->mCategory:Ljava/util/ArrayList;
    invoke-static {v1}, Lnet/flixster/android/CategoryPage;->access$0(Lnet/flixster/android/CategoryPage;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    #getter for: Lnet/flixster/android/CategoryPage;->mCategoryFilter:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/CategoryPage;->access$1(Lnet/flixster/android/CategoryPage;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget v1, v1, Lnet/flixster/android/CategoryPage;->mRetryCount:I

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v3, v4, v1}, Lnet/flixster/android/data/MovieDao;->fetchDvdCategory(Ljava/util/List;Ljava/lang/String;Z)V

    .line 66
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget v3, p0, Lnet/flixster/android/CategoryPage$1;->val$currResumeCtr:I

    invoke-virtual {v1, v3}, Lnet/flixster/android/CategoryPage;->shouldSkipBackgroundTask(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_2

    .line 85
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget-object v1, v1, Lnet/flixster/android/CategoryPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 87
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 63
    goto :goto_0

    .line 70
    :cond_2
    :try_start_1
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    #calls: Lnet/flixster/android/CategoryPage;->setMovieLviList()V
    invoke-static {v1}, Lnet/flixster/android/CategoryPage;->access$2(Lnet/flixster/android/CategoryPage;)V

    .line 72
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget-object v1, v1, Lnet/flixster/android/CategoryPage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    .line 85
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget-object v1, v1, Lnet/flixster/android/CategoryPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 73
    :catch_0
    move-exception v0

    .line 75
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v1, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CategoryPage.ScheduleLoadItemsTask.run() mRetryCount:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget v4, v4, Lnet/flixster/android/CategoryPage;->mRetryCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->printStackTrace()V

    .line 77
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget v3, v1, Lnet/flixster/android/CategoryPage;->mRetryCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lnet/flixster/android/CategoryPage;->mRetryCount:I

    .line 78
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget v1, v1, Lnet/flixster/android/CategoryPage;->mRetryCount:I

    const/4 v3, 0x3

    if-ge v1, v3, :cond_3

    .line 79
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    const-wide/16 v3, 0x3e8

    #calls: Lnet/flixster/android/CategoryPage;->ScheduleLoadItemsTask(J)V
    invoke-static {v1, v3, v4}, Lnet/flixster/android/CategoryPage;->access$3(Lnet/flixster/android/CategoryPage;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 85
    :goto_2
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget-object v1, v1, Lnet/flixster/android/CategoryPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 81
    :cond_3
    :try_start_3
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    const/4 v3, 0x0

    iput v3, v1, Lnet/flixster/android/CategoryPage;->mRetryCount:I

    .line 82
    iget-object v1, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget-object v1, v1, Lnet/flixster/android/CategoryPage;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 84
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catchall_0
    move-exception v1

    .line 85
    iget-object v3, p0, Lnet/flixster/android/CategoryPage$1;->this$0:Lnet/flixster/android/CategoryPage;

    iget-object v3, v3, Lnet/flixster/android/CategoryPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 86
    throw v1
.end method
