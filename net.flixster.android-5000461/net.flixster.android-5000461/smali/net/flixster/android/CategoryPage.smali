.class public Lnet/flixster/android/CategoryPage;
.super Lnet/flixster/android/LviActivity;
.source "CategoryPage.java"


# static fields
.field public static final CATEGORY_FILTER:Ljava/lang/String; = "CATEGORY_FILTER"

.field public static final CATEGORY_TITLE:Ljava/lang/String; = "CATEGORY_TITLE"


# instance fields
.field private final mCategory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryFilter:Ljava/lang/String;

.field private mCategoryTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lnet/flixster/android/LviActivity;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/CategoryPage;->mCategory:Ljava/util/ArrayList;

    .line 26
    const-string v0, "Flixster Top 100"

    iput-object v0, p0, Lnet/flixster/android/CategoryPage;->mCategoryTitle:Ljava/lang/String;

    .line 27
    const-string v0, "&filter=flixster-top-100"

    iput-object v0, p0, Lnet/flixster/android/CategoryPage;->mCategoryFilter:Ljava/lang/String;

    .line 20
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 4
    .parameter "delay"

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/CategoryPage;->throbberHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 56
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    .line 57
    .local v0, currResumeCtr:I
    new-instance v1, Lnet/flixster/android/CategoryPage$1;

    invoke-direct {v1, p0, v0}, Lnet/flixster/android/CategoryPage$1;-><init>(Lnet/flixster/android/CategoryPage;I)V

    .line 90
    .local v1, loadMoviesTask:Ljava/util/TimerTask;
    const-string v2, "FlxMain"

    const-string v3, "CategoryPage.ScheduleLoadItemsTask() loading item"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v2, p0, Lnet/flixster/android/CategoryPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 93
    iget-object v2, p0, Lnet/flixster/android/CategoryPage;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v1, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :cond_0
    monitor-exit p0

    return-void

    .line 54
    .end local v0           #currResumeCtr:I
    .end local v1           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lnet/flixster/android/CategoryPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lnet/flixster/android/CategoryPage;->mCategory:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/CategoryPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lnet/flixster/android/CategoryPage;->mCategoryFilter:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/CategoryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 97
    invoke-direct {p0}, Lnet/flixster/android/CategoryPage;->setMovieLviList()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/CategoryPage;J)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/CategoryPage;->ScheduleLoadItemsTask(J)V

    return-void
.end method

.method private setMovieLviList()V
    .locals 5

    .prologue
    .line 98
    const-string v3, "FlxMain"

    const-string v4, "CategoryPage.setPopularLviList "

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v3, p0, Lnet/flixster/android/CategoryPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 103
    invoke-virtual {p0}, Lnet/flixster/android/CategoryPage;->destroyExistingLviAd()V

    .line 104
    new-instance v3, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v3, p0, Lnet/flixster/android/CategoryPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 105
    iget-object v3, p0, Lnet/flixster/android/CategoryPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v4, "Category"

    iput-object v4, v3, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 106
    iget-object v3, p0, Lnet/flixster/android/CategoryPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v4, p0, Lnet/flixster/android/CategoryPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    iget-object v3, p0, Lnet/flixster/android/CategoryPage;->mCategory:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 115
    new-instance v0, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 116
    .local v0, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v3, p0, Lnet/flixster/android/CategoryPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    return-void

    .line 108
    .end local v0           #footer:Lnet/flixster/android/lvi/LviFooter;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Movie;

    .line 109
    .local v2, movie:Lnet/flixster/android/model/Movie;
    new-instance v1, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 110
    .local v1, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v2, v1, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 111
    invoke-virtual {p0}, Lnet/flixster/android/CategoryPage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    iput-object v4, v1, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 112
    iget-object v4, p0, Lnet/flixster/android/CategoryPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedState"

    .prologue
    .line 31
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-virtual {p0}, Lnet/flixster/android/CategoryPage;->createActionBar()V

    .line 34
    iget-object v1, p0, Lnet/flixster/android/CategoryPage;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/CategoryPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 35
    iget-object v1, p0, Lnet/flixster/android/CategoryPage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    const-string v2, "CategoryStickyTop"

    invoke-virtual {v1, v2}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 36
    iget-object v1, p0, Lnet/flixster/android/CategoryPage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    const-string v2, "CategoryStickyBottom"

    invoke-virtual {v1, v2}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0}, Lnet/flixster/android/CategoryPage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 39
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 40
    const-string v1, "CATEGORY_FILTER"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/CategoryPage;->mCategoryFilter:Ljava/lang/String;

    .line 41
    const-string v1, "CATEGORY_TITLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/CategoryPage;->mCategoryTitle:Ljava/lang/String;

    .line 42
    iget-object v1, p0, Lnet/flixster/android/CategoryPage;->mCategoryTitle:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lnet/flixster/android/CategoryPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 44
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 48
    invoke-super {p0}, Lnet/flixster/android/LviActivity;->onResume()V

    .line 49
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/dvds/browse/genre"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Category "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/CategoryPage;->mCategoryTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lnet/flixster/android/CategoryPage;->ScheduleLoadItemsTask(J)V

    .line 51
    return-void
.end method
