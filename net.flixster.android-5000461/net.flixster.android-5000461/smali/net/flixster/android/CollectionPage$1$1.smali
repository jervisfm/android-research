.class Lnet/flixster/android/CollectionPage$1$1;
.super Ljava/lang/Object;
.source "CollectionPage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/CollectionPage$1;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lnet/flixster/android/CollectionPage$1;


# direct methods
.method constructor <init>(Lnet/flixster/android/CollectionPage$1;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/CollectionPage$1$1;->this$1:Lnet/flixster/android/CollectionPage$1;

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 131
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 132
    .local v0, currentThread:Ljava/lang/Thread;
    :cond_0
    :goto_0
    invoke-static {}, Lnet/flixster/android/CollectionPage;->access$6()Ljava/lang/Thread;

    move-result-object v4

    if-eq v4, v0, :cond_1

    .line 143
    return-void

    .line 134
    :cond_1
    const-wide/16 v4, 0x7d0

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 135
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    iget-object v4, p0, Lnet/flixster/android/CollectionPage$1$1;->this$1:Lnet/flixster/android/CollectionPage$1;

    #getter for: Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;
    invoke-static {v4}, Lnet/flixster/android/CollectionPage$1;->access$0(Lnet/flixster/android/CollectionPage$1;)Lnet/flixster/android/CollectionPage;

    move-result-object v4

    #getter for: Lnet/flixster/android/CollectionPage;->gridView:Landroid/widget/GridView;
    invoke-static {v4}, Lnet/flixster/android/CollectionPage;->access$2(Lnet/flixster/android/CollectionPage;)Landroid/widget/GridView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/GridView;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 136
    iget-object v4, p0, Lnet/flixster/android/CollectionPage$1$1;->this$1:Lnet/flixster/android/CollectionPage$1;

    #getter for: Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;
    invoke-static {v4}, Lnet/flixster/android/CollectionPage$1;->access$0(Lnet/flixster/android/CollectionPage$1;)Lnet/flixster/android/CollectionPage;

    move-result-object v4

    #getter for: Lnet/flixster/android/CollectionPage;->gridView:Landroid/widget/GridView;
    invoke-static {v4}, Lnet/flixster/android/CollectionPage;->access$2(Lnet/flixster/android/CollectionPage;)Landroid/widget/GridView;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/MovieCollectionItem;

    iget-object v3, v4, Lnet/flixster/android/MovieCollectionItem;->progressHandler:Landroid/os/Handler;

    .line 137
    .local v3, progressHandler:Landroid/os/Handler;
    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 139
    .end local v2           #i:I
    .end local v3           #progressHandler:Landroid/os/Handler;
    :catch_0
    move-exception v1

    .line 140
    .local v1, e:Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
