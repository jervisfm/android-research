.class Lnet/flixster/android/CollectionPage$1;
.super Landroid/os/Handler;
.source "CollectionPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/CollectionPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/CollectionPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/CollectionPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    .line 75
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/CollectionPage$1;)Lnet/flixster/android/CollectionPage;
    .locals 1
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .parameter "msg"

    .prologue
    .line 77
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v8}, Lnet/flixster/android/CollectionPage;->access$0(Lnet/flixster/android/CollectionPage;)Landroid/widget/ProgressBar;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 78
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    const v9, 0x7f07010e

    invoke-virtual {v8, v9}, Lnet/flixster/android/CollectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 79
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_5

    const/16 v8, 0x8

    .line 78
    :goto_0
    invoke-virtual {v9, v8}, Landroid/view/View;->setVisibility(I)V

    .line 81
    const/4 v1, 0x0

    .local v1, insertionIndex:I
    const/4 v0, 0x0

    .local v0, downloadInsertionIndex:I
    const/4 v4, 0x0

    .line 82
    .local v4, unfulfillableCount:I
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->movieAndSeasonRights:Ljava/util/List;
    invoke-static {v8}, Lnet/flixster/android/CollectionPage;->access$1(Lnet/flixster/android/CollectionPage;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 84
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v8

    invoke-virtual {v8}, Lnet/flixster/android/model/User;->getLockerRights()Ljava/util/List;

    move-result-object v3

    .line 85
    .local v3, rights:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_6

    .line 101
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->gridView:Landroid/widget/GridView;
    invoke-static {v8}, Lnet/flixster/android/CollectionPage;->access$2(Lnet/flixster/android/CollectionPage;)Landroid/widget/GridView;

    move-result-object v8

    new-instance v9, Lnet/flixster/android/LockerRightGridViewAdapter;

    iget-object v10, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    iget-object v11, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->movieAndSeasonRights:Ljava/util/List;
    invoke-static {v11}, Lnet/flixster/android/CollectionPage;->access$1(Lnet/flixster/android/CollectionPage;)Ljava/util/List;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lnet/flixster/android/LockerRightGridViewAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v8, v9}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 102
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->gridView:Landroid/widget/GridView;
    invoke-static {v8}, Lnet/flixster/android/CollectionPage;->access$2(Lnet/flixster/android/CollectionPage;)Landroid/widget/GridView;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/GridView;->setClickable(Z)V

    .line 103
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->gridView:Landroid/widget/GridView;
    invoke-static {v8}, Lnet/flixster/android/CollectionPage;->access$2(Lnet/flixster/android/CollectionPage;)Landroid/widget/GridView;

    move-result-object v8

    iget-object v9, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->movieDetailClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v9}, Lnet/flixster/android/CollectionPage;->access$3(Lnet/flixster/android/CollectionPage;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 105
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    const v9, 0x7f070110

    invoke-virtual {v8, v9}, Lnet/flixster/android/CollectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 106
    .local v6, unfulfillablePanel:Landroid/widget/TextView;
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    const v9, 0x7f070111

    invoke-virtual {v8, v9}, Lnet/flixster/android/CollectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 107
    .local v5, unfulfillableDivider:Landroid/widget/ImageView;
    if-lez v4, :cond_c

    .line 109
    const/4 v8, 0x1

    if-ne v4, v8, :cond_b

    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    const v9, 0x7f0c0073

    invoke-virtual {v8, v9}, Lnet/flixster/android/CollectionPage;->getString(I)Ljava/lang/String;

    move-result-object v8

    :goto_2
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->unfulfillablePanelClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v8}, Lnet/flixster/android/CollectionPage;->access$4(Lnet/flixster/android/CollectionPage;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 113
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 119
    :goto_3
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->scrollLayout:Landroid/widget/LinearLayout;
    invoke-static {v8}, Lnet/flixster/android/CollectionPage;->access$5(Lnet/flixster/android/CollectionPage;)Landroid/widget/LinearLayout;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    .line 120
    .local v7, viewCount:I
    const/4 v8, 0x5

    if-le v7, v8, :cond_1

    .line 121
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->scrollLayout:Landroid/widget/LinearLayout;
    invoke-static {v8}, Lnet/flixster/android/CollectionPage;->access$5(Lnet/flixster/android/CollectionPage;)Landroid/widget/LinearLayout;

    move-result-object v8

    const/4 v9, 0x5

    add-int/lit8 v10, v7, -0x5

    invoke-virtual {v8, v9, v10}, Landroid/widget/LinearLayout;->removeViews(II)V

    .line 123
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 124
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->scrollLayout:Landroid/widget/LinearLayout;
    invoke-static {v8}, Lnet/flixster/android/CollectionPage;->access$5(Lnet/flixster/android/CollectionPage;)Landroid/widget/LinearLayout;

    move-result-object v8

    new-instance v9, Lcom/flixster/android/view/UvFooter;

    iget-object v10, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    invoke-direct {v9, v10}, Lcom/flixster/android/view/UvFooter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 128
    :cond_2
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v8

    invoke-interface {v8}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceDownloadCompatible()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 129
    new-instance v8, Ljava/lang/Thread;

    new-instance v9, Lnet/flixster/android/CollectionPage$1$1;

    invoke-direct {v9, p0}, Lnet/flixster/android/CollectionPage$1$1;-><init>(Lnet/flixster/android/CollectionPage$1;)V

    invoke-direct {v8, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-static {v8}, Lnet/flixster/android/CollectionPage;->access$7(Ljava/lang/Thread;)V

    .line 145
    invoke-static {}, Lnet/flixster/android/CollectionPage;->access$6()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    .line 149
    :cond_3
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v8

    if-nez v8, :cond_4

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->hasMigratedToRights()Z

    move-result v8

    if-nez v8, :cond_4

    .line 150
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    const v9, 0x3b9acacf

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lnet/flixster/android/CollectionPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 152
    :cond_4
    return-void

    .line 79
    .end local v0           #downloadInsertionIndex:I
    .end local v1           #insertionIndex:I
    .end local v3           #rights:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .end local v4           #unfulfillableCount:I
    .end local v5           #unfulfillableDivider:Landroid/widget/ImageView;
    .end local v6           #unfulfillablePanel:Landroid/widget/TextView;
    .end local v7           #viewCount:I
    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 85
    .restart local v0       #downloadInsertionIndex:I
    .restart local v1       #insertionIndex:I
    .restart local v3       #rights:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .restart local v4       #unfulfillableCount:I
    :cond_6
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/LockerRight;

    .line 86
    .local v2, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isMovie()Z

    move-result v9

    if-eqz v9, :cond_7

    iget-boolean v9, v2, Lnet/flixster/android/model/LockerRight;->isStreamingSupported:Z

    if-nez v9, :cond_8

    iget-boolean v9, v2, Lnet/flixster/android/model/LockerRight;->isDownloadSupported:Z

    if-nez v9, :cond_8

    :cond_7
    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 87
    :cond_8
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v9

    if-nez v9, :cond_9

    invoke-static {v2}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(Lnet/flixster/android/model/LockerRight;)Z

    move-result v9

    if-nez v9, :cond_9

    .line 88
    invoke-static {v2}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(Lnet/flixster/android/model/LockerRight;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 89
    iget-object v9, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->movieAndSeasonRights:Ljava/util/List;
    invoke-static {v9}, Lnet/flixster/android/CollectionPage;->access$1(Lnet/flixster/android/CollectionPage;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v0, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 90
    add-int/lit8 v0, v0, 0x1

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 93
    :cond_9
    iget-object v9, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->movieAndSeasonRights:Ljava/util/List;
    invoke-static {v9}, Lnet/flixster/android/CollectionPage;->access$1(Lnet/flixster/android/CollectionPage;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 96
    :cond_a
    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isUnfulfillable()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 97
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 110
    .end local v2           #right:Lnet/flixster/android/model/LockerRight;
    .restart local v5       #unfulfillableDivider:Landroid/widget/ImageView;
    .restart local v6       #unfulfillablePanel:Landroid/widget/TextView;
    :cond_b
    iget-object v8, p0, Lnet/flixster/android/CollectionPage$1;->this$0:Lnet/flixster/android/CollectionPage;

    const v9, 0x7f0c0074

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Lnet/flixster/android/CollectionPage;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_2

    .line 115
    :cond_c
    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    const/16 v8, 0x8

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3
.end method
