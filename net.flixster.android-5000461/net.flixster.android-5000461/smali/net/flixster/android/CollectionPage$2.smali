.class Lnet/flixster/android/CollectionPage$2;
.super Landroid/os/Handler;
.source "CollectionPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/CollectionPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/CollectionPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/CollectionPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/CollectionPage$2;->this$0:Lnet/flixster/android/CollectionPage;

    .line 155
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "msg"

    .prologue
    .line 157
    iget-object v0, p0, Lnet/flixster/android/CollectionPage$2;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lnet/flixster/android/CollectionPage;->access$0(Lnet/flixster/android/CollectionPage;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lnet/flixster/android/CollectionPage$2;->this$0:Lnet/flixster/android/CollectionPage;

    const v1, 0x7f070112

    invoke-virtual {v0, v1}, Lnet/flixster/android/CollectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 159
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lnet/flixster/android/data/DaoException;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/data/DaoException;

    iget-object v1, p0, Lnet/flixster/android/CollectionPage$2;->this$0:Lnet/flixster/android/CollectionPage;

    invoke-static {v0, v1}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/DialogActivity;)V

    .line 162
    :cond_0
    return-void
.end method
