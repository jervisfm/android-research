.class Lnet/flixster/android/CollectionPage$4;
.super Ljava/lang/Object;
.source "CollectionPage.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/CollectionPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/CollectionPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/CollectionPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/CollectionPage$4;->this$0:Lnet/flixster/android/CollectionPage;

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .parameter
    .parameter "v"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 175
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lnet/flixster/android/CollectionPage$4;->this$0:Lnet/flixster/android/CollectionPage;

    #getter for: Lnet/flixster/android/CollectionPage;->movieAndSeasonRights:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/CollectionPage;->access$1(Lnet/flixster/android/CollectionPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 176
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->assetId:J

    iget-wide v3, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    iget-object v5, p0, Lnet/flixster/android/CollectionPage$4;->this$0:Lnet/flixster/android/CollectionPage;

    invoke-static {v1, v2, v3, v4, v5}, Lnet/flixster/android/Starter;->launchSeasonDetail(JJLandroid/content/Context;)V

    .line 181
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->assetId:J

    iget-wide v3, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    iget-object v5, p0, Lnet/flixster/android/CollectionPage$4;->this$0:Lnet/flixster/android/CollectionPage;

    invoke-static {v1, v2, v3, v4, v5}, Lnet/flixster/android/Starter;->launchMovieDetail(JJLandroid/content/Context;)V

    goto :goto_0
.end method
