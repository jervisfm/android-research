.class public Lnet/flixster/android/CollectionPage;
.super Lcom/flixster/android/activity/NonTabbedActivity;
.source "CollectionPage.java"


# static fields
.field private static volatile sProgressMonitorThread:Ljava/lang/Thread;


# instance fields
.field private final errorHandler:Landroid/os/Handler;

.field private gridView:Landroid/widget/GridView;

.field private movieAndSeasonRights:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;"
        }
    .end annotation
.end field

.field private final movieDetailClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private scrollLayout:Landroid/widget/LinearLayout;

.field private final successHandler:Landroid/os/Handler;

.field private throbber:Landroid/widget/ProgressBar;

.field private final unfulfillablePanelClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/flixster/android/activity/NonTabbedActivity;-><init>()V

    .line 75
    new-instance v0, Lnet/flixster/android/CollectionPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/CollectionPage$1;-><init>(Lnet/flixster/android/CollectionPage;)V

    iput-object v0, p0, Lnet/flixster/android/CollectionPage;->successHandler:Landroid/os/Handler;

    .line 155
    new-instance v0, Lnet/flixster/android/CollectionPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/CollectionPage$2;-><init>(Lnet/flixster/android/CollectionPage;)V

    iput-object v0, p0, Lnet/flixster/android/CollectionPage;->errorHandler:Landroid/os/Handler;

    .line 165
    new-instance v0, Lnet/flixster/android/CollectionPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/CollectionPage$3;-><init>(Lnet/flixster/android/CollectionPage;)V

    iput-object v0, p0, Lnet/flixster/android/CollectionPage;->unfulfillablePanelClickListener:Landroid/view/View$OnClickListener;

    .line 173
    new-instance v0, Lnet/flixster/android/CollectionPage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/CollectionPage$4;-><init>(Lnet/flixster/android/CollectionPage;)V

    iput-object v0, p0, Lnet/flixster/android/CollectionPage;->movieDetailClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 36
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/CollectionPage;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lnet/flixster/android/CollectionPage;->throbber:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/CollectionPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lnet/flixster/android/CollectionPage;->movieAndSeasonRights:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/CollectionPage;)Landroid/widget/GridView;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lnet/flixster/android/CollectionPage;->gridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/CollectionPage;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lnet/flixster/android/CollectionPage;->movieDetailClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/CollectionPage;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 165
    iget-object v0, p0, Lnet/flixster/android/CollectionPage;->unfulfillablePanelClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/CollectionPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lnet/flixster/android/CollectionPage;->scrollLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$6()Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lnet/flixster/android/CollectionPage;->sProgressMonitorThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$7(Ljava/lang/Thread;)V
    .locals 0
    .parameter

    .prologue
    .line 41
    sput-object p0, Lnet/flixster/android/CollectionPage;->sProgressMonitorThread:Ljava/lang/Thread;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/flixster/android/activity/NonTabbedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f03004f

    invoke-virtual {p0, v0}, Lnet/flixster/android/CollectionPage;->setContentView(I)V

    .line 48
    const v0, 0x7f070073

    invoke-virtual {p0, v0}, Lnet/flixster/android/CollectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lnet/flixster/android/CollectionPage;->scrollLayout:Landroid/widget/LinearLayout;

    .line 49
    const v0, 0x7f07010f

    invoke-virtual {p0, v0}, Lnet/flixster/android/CollectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lnet/flixster/android/CollectionPage;->gridView:Landroid/widget/GridView;

    .line 50
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Lnet/flixster/android/CollectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lnet/flixster/android/CollectionPage;->throbber:Landroid/widget/ProgressBar;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/CollectionPage;->movieAndSeasonRights:Ljava/util/List;

    .line 52
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lcom/flixster/android/activity/NonTabbedActivity;->onPause()V

    .line 72
    const/4 v0, 0x0

    sput-object v0, Lnet/flixster/android/CollectionPage;->sProgressMonitorThread:Ljava/lang/Thread;

    .line 73
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 56
    invoke-super {p0}, Lcom/flixster/android/activity/NonTabbedActivity;->onResume()V

    .line 58
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v0

    .line 59
    .local v0, user:Lnet/flixster/android/model/User;
    if-eqz v0, :cond_1

    .line 60
    invoke-virtual {v0}, Lnet/flixster/android/model/User;->isLockerRightsFetched()Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    iget-object v1, p0, Lnet/flixster/android/CollectionPage;->throbber:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 63
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/CollectionPage;->successHandler:Landroid/os/Handler;

    iget-object v2, p0, Lnet/flixster/android/CollectionPage;->errorHandler:Landroid/os/Handler;

    invoke-static {v1, v2}, Lnet/flixster/android/data/ProfileDao;->getUserLockerRights(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 66
    :cond_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/mymovies/mycollection"

    const-string v3, "My Movies - My Collection"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-void
.end method
