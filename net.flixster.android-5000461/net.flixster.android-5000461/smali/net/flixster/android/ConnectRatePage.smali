.class public Lnet/flixster/android/ConnectRatePage;
.super Lnet/flixster/android/FlixsterActivity;
.source "ConnectRatePage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field protected static final REQUESTCODE_FRIENDPROMT:I = 0x2

.field protected static final REQUESTCODE_RATEPROMT:I = 0x1

.field protected static final REQUESTCODE_WTSPROMT:I = 0x3


# instance fields
.field mRequestCode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 23
    const/4 v0, 0x1

    iput v0, p0, Lnet/flixster/android/ConnectRatePage;->mRequestCode:I

    .line 18
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v3, -0x1

    .line 134
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ConnectRatePage requestCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    if-ne p2, v3, :cond_0

    const/16 v0, 0x7b

    if-ne p1, v0, :cond_0

    .line 136
    invoke-virtual {p0, v3}, Lnet/flixster/android/ConnectRatePage;->setResult(I)V

    .line 137
    invoke-virtual {p0}, Lnet/flixster/android/ConnectRatePage;->finish()V

    .line 139
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    const/16 v3, 0x7b

    .line 120
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 130
    :goto_0
    return-void

    .line 122
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/FacebookAuth;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 123
    .local v0, facebookIntent:Landroid/content/Intent;
    invoke-virtual {p0, v0, v3}, Lnet/flixster/android/ConnectRatePage;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 126
    .end local v0           #facebookIntent:Landroid/content/Intent;
    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/FlixsterLoginPage;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 127
    .local v1, flixsterIntent:Landroid/content/Intent;
    invoke-virtual {p0, v1, v3}, Lnet/flixster/android/ConnectRatePage;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x7f070058
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 15
    .parameter "savedState"

    .prologue
    .line 27
    const-string v11, "FlxMain"

    const-string v12, "ConnectRatePage.onCreate"

    invoke-static {v11, v12}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-super/range {p0 .. p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v11, 0x7f03001e

    invoke-virtual {p0, v11}, Lnet/flixster/android/ConnectRatePage;->setContentView(I)V

    .line 30
    invoke-virtual {p0}, Lnet/flixster/android/ConnectRatePage;->createActionBar()V

    .line 31
    const v11, 0x7f0c0065

    invoke-virtual {p0, v11}, Lnet/flixster/android/ConnectRatePage;->setActionBarTitle(I)V

    .line 33
    const v11, 0x7f070052

    invoke-virtual {p0, v11}, Lnet/flixster/android/ConnectRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 34
    .local v7, rl:Landroid/widget/RelativeLayout;
    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 36
    invoke-virtual {p0}, Lnet/flixster/android/ConnectRatePage;->getIntent()Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 37
    .local v2, extras:Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 38
    const-string v11, "net.flixster.RequestCode"

    invoke-virtual {v2, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 39
    .local v3, i:Ljava/lang/Integer;
    if-eqz v3, :cond_0

    .line 40
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v11

    iput v11, p0, Lnet/flixster/android/ConnectRatePage;->mRequestCode:I

    .line 44
    .end local v3           #i:Ljava/lang/Integer;
    :cond_0
    const v11, 0x7f070053

    invoke-virtual {p0, v11}, Lnet/flixster/android/ConnectRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 45
    .local v5, leadText:Landroid/widget/TextView;
    const v11, 0x7f070055

    invoke-virtual {p0, v11}, Lnet/flixster/android/ConnectRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 46
    .local v8, tv1:Landroid/widget/TextView;
    const v11, 0x7f070056

    invoke-virtual {p0, v11}, Lnet/flixster/android/ConnectRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 47
    .local v9, tv2:Landroid/widget/TextView;
    const v11, 0x7f070057

    invoke-virtual {p0, v11}, Lnet/flixster/android/ConnectRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 48
    .local v10, tv3:Landroid/widget/TextView;
    const v11, 0x7f070054

    invoke-virtual {p0, v11}, Lnet/flixster/android/ConnectRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 49
    .local v4, iv:Landroid/widget/ImageView;
    iget v11, p0, Lnet/flixster/android/ConnectRatePage;->mRequestCode:I

    packed-switch v11, :pswitch_data_0

    .line 96
    :goto_0
    :pswitch_0
    const v11, 0x7f070058

    invoke-virtual {p0, v11}, Lnet/flixster/android/ConnectRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 97
    .local v0, connectButton:Landroid/widget/ImageButton;
    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const v11, 0x7f070059

    invoke-virtual {p0, v11}, Lnet/flixster/android/ConnectRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 99
    .local v1, connectLink:Landroid/widget/TextView;
    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    return-void

    .line 53
    .end local v0           #connectButton:Landroid/widget/ImageButton;
    .end local v1           #connectLink:Landroid/widget/TextView;
    :pswitch_1
    const v11, 0x7f02017c

    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 55
    const v11, 0x7f0c00a9

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setText(I)V

    .line 56
    const v11, 0x7f020081

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 57
    const v11, 0x7f0c0087

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setText(I)V

    .line 58
    const/16 v11, 0xc

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v8, v11, v12, v13, v14}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 60
    const v11, 0x7f020082

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 61
    const v11, 0x7f0c0088

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(I)V

    .line 62
    const/16 v11, 0xc

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v9, v11, v12, v13, v14}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 64
    const v11, 0x7f020083

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 65
    const v11, 0x7f0c0089

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    .line 66
    const/16 v11, 0xc

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v10, v11, v12, v13, v14}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0

    .line 70
    :pswitch_2
    const v11, 0x7f02017b

    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 72
    const v11, 0x7f0c00aa

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setText(I)V

    .line 73
    const v11, 0x7f020064

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 74
    const v11, 0x7f0c0086

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setText(I)V

    .line 75
    const/16 v11, 0x14

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v8, v11, v12, v13, v14}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 77
    invoke-virtual {v8}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 78
    .local v6, lp:Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v11, 0x1e

    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 80
    const v11, 0x7f020063

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 81
    const v11, 0x7f0c006a

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(I)V

    .line 82
    const/16 v11, 0x14

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v9, v11, v12, v13, v14}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 83
    invoke-virtual {v9}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .end local v6           #lp:Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 84
    .restart local v6       #lp:Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v11, 0xf

    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 86
    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setHeight(I)V

    .line 88
    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setMaxHeight(I)V

    .line 89
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v10, v11, v12, v13, v14}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 90
    invoke-virtual {v10}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .end local v6           #lp:Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 91
    .restart local v6       #lp:Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v11, 0x0

    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 143
    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 104
    const-string v0, "FlxMain"

    const-string v1, "ConnectRatePage.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 106
    iget v0, p0, Lnet/flixster/android/ConnectRatePage;->mRequestCode:I

    packed-switch v0, :pswitch_data_0

    .line 117
    :goto_0
    return-void

    .line 108
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/promo/rating"

    const-string v2, "Promo - Rating"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/promo/wanttosee"

    const-string v2, "Promo - WTS"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :pswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/promo/friendsrating"

    const-string v2, "Promo - Friends"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
