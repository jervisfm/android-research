.class Lnet/flixster/android/DvdPage$2;
.super Ljava/lang/Object;
.source "DvdPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/DvdPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/DvdPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/DvdPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/DvdPage$2;->this$0:Lnet/flixster/android/DvdPage;

    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 291
    iget-object v0, p0, Lnet/flixster/android/DvdPage$2;->this$0:Lnet/flixster/android/DvdPage;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    #setter for: Lnet/flixster/android/DvdPage;->mNavSelect:I
    invoke-static {v0, v1}, Lnet/flixster/android/DvdPage;->access$0(Lnet/flixster/android/DvdPage;I)V

    .line 292
    iget-object v0, p0, Lnet/flixster/android/DvdPage$2;->this$0:Lnet/flixster/android/DvdPage;

    #getter for: Lnet/flixster/android/DvdPage;->mNavSelect:I
    invoke-static {v0}, Lnet/flixster/android/DvdPage;->access$1(Lnet/flixster/android/DvdPage;)I

    move-result v0

    const v1, 0x7f07025a

    if-ne v0, v1, :cond_0

    .line 293
    iget-object v0, p0, Lnet/flixster/android/DvdPage$2;->this$0:Lnet/flixster/android/DvdPage;

    iget-object v0, v0, Lnet/flixster/android/DvdPage;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lnet/flixster/android/DvdPage$2;->this$0:Lnet/flixster/android/DvdPage;

    #getter for: Lnet/flixster/android/DvdPage;->mCategoryListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v1}, Lnet/flixster/android/DvdPage;->access$2(Lnet/flixster/android/DvdPage;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 297
    :goto_0
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->incrementResumeCtr()V

    .line 298
    iget-object v0, p0, Lnet/flixster/android/DvdPage$2;->this$0:Lnet/flixster/android/DvdPage;

    iget-object v1, p0, Lnet/flixster/android/DvdPage$2;->this$0:Lnet/flixster/android/DvdPage;

    #getter for: Lnet/flixster/android/DvdPage;->mNavSelect:I
    invoke-static {v1}, Lnet/flixster/android/DvdPage;->access$1(Lnet/flixster/android/DvdPage;)I

    move-result v1

    const-wide/16 v2, 0x64

    #calls: Lnet/flixster/android/DvdPage;->ScheduleLoadItemsTask(IJ)V
    invoke-static {v0, v1, v2, v3}, Lnet/flixster/android/DvdPage;->access$3(Lnet/flixster/android/DvdPage;IJ)V

    .line 299
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/DvdPage$2;->this$0:Lnet/flixster/android/DvdPage;

    iget-object v0, v0, Lnet/flixster/android/DvdPage;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lnet/flixster/android/DvdPage$2;->this$0:Lnet/flixster/android/DvdPage;

    invoke-virtual {v1}, Lnet/flixster/android/DvdPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method
