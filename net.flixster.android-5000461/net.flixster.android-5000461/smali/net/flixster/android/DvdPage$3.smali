.class Lnet/flixster/android/DvdPage$3;
.super Ljava/util/TimerTask;
.source "DvdPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/DvdPage;->ScheduleLoadItemsTask(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/DvdPage;

.field private final synthetic val$currResumeCtr:I

.field private final synthetic val$navSelection:I


# direct methods
.method constructor <init>(Lnet/flixster/android/DvdPage;II)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    iput p2, p0, Lnet/flixster/android/DvdPage$3;->val$navSelection:I

    iput p3, p0, Lnet/flixster/android/DvdPage$3;->val$currResumeCtr:I

    .line 98
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DvdPage.ScheduleLoadItemsTask.run navSelection:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lnet/flixster/android/DvdPage$3;->val$navSelection:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :try_start_0
    iget v3, p0, Lnet/flixster/android/DvdPage$3;->val$navSelection:I

    packed-switch v3, :pswitch_data_0

    .line 131
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    invoke-virtual {v1}, Lnet/flixster/android/DvdPage;->trackPage()V

    .line 132
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    iget-object v1, v1, Lnet/flixster/android/DvdPage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 133
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    #calls: Lnet/flixster/android/DvdPage;->checkAndShowLaunchAd()V
    invoke-static {v1}, Lnet/flixster/android/DvdPage;->access$11(Lnet/flixster/android/DvdPage;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    iget-object v1, v1, Lnet/flixster/android/DvdPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 140
    :goto_1
    return-void

    .line 105
    :pswitch_0
    :try_start_1
    iget-object v3, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    #getter for: Lnet/flixster/android/DvdPage;->mNewReleases:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/flixster/android/DvdPage;->access$4(Lnet/flixster/android/DvdPage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 106
    iget-object v3, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    #getter for: Lnet/flixster/android/DvdPage;->mNewReleasesFeatured:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/flixster/android/DvdPage;->access$5(Lnet/flixster/android/DvdPage;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    #getter for: Lnet/flixster/android/DvdPage;->mNewReleases:Ljava/util/ArrayList;
    invoke-static {v4}, Lnet/flixster/android/DvdPage;->access$4(Lnet/flixster/android/DvdPage;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    iget v5, v5, Lnet/flixster/android/DvdPage;->mRetryCount:I

    if-nez v5, :cond_2

    :goto_2
    invoke-static {v3, v4, v1}, Lnet/flixster/android/data/MovieDao;->fetchDvdNewRelease(Ljava/util/List;Ljava/util/List;Z)V

    .line 109
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    iget v3, p0, Lnet/flixster/android/DvdPage$3;->val$currResumeCtr:I

    invoke-virtual {v1, v3}, Lnet/flixster/android/DvdPage;->shouldSkipBackgroundTask(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    if-eqz v1, :cond_3

    .line 138
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    iget-object v1, v1, Lnet/flixster/android/DvdPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_2
    move v1, v2

    .line 106
    goto :goto_2

    .line 113
    :cond_3
    :try_start_2
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    #calls: Lnet/flixster/android/DvdPage;->setNewReleasesLviList()V
    invoke-static {v1}, Lnet/flixster/android/DvdPage;->access$6(Lnet/flixster/android/DvdPage;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_3
    const-string v1, "FlxMain"

    const-string v3, "DvdPage.ScheduleLoadItemsTask.run DaoException"

    invoke-static {v1, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 136
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    invoke-virtual {v1, v0}, Lnet/flixster/android/DvdPage;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 138
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    iget-object v1, v1, Lnet/flixster/android/DvdPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 116
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_1
    :try_start_4
    iget-object v3, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    #getter for: Lnet/flixster/android/DvdPage;->mComingSoon:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/flixster/android/DvdPage;->access$7(Lnet/flixster/android/DvdPage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 117
    iget-object v3, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    #getter for: Lnet/flixster/android/DvdPage;->mComingSoonFeatured:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/flixster/android/DvdPage;->access$8(Lnet/flixster/android/DvdPage;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    #getter for: Lnet/flixster/android/DvdPage;->mComingSoon:Ljava/util/ArrayList;
    invoke-static {v4}, Lnet/flixster/android/DvdPage;->access$7(Lnet/flixster/android/DvdPage;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    iget v5, v5, Lnet/flixster/android/DvdPage;->mRetryCount:I

    if-nez v5, :cond_5

    :goto_3
    invoke-static {v3, v4, v1}, Lnet/flixster/android/data/MovieDao;->fetchDvdComingSoon(Ljava/util/List;Ljava/util/List;Z)V

    .line 120
    :cond_4
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    iget v3, p0, Lnet/flixster/android/DvdPage$3;->val$currResumeCtr:I

    invoke-virtual {v1, v3}, Lnet/flixster/android/DvdPage;->shouldSkipBackgroundTask(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 124
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    #calls: Lnet/flixster/android/DvdPage;->setComingSoonLviList()V
    invoke-static {v1}, Lnet/flixster/android/DvdPage;->access$9(Lnet/flixster/android/DvdPage;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 137
    :catchall_0
    move-exception v1

    .line 138
    iget-object v3, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    iget-object v3, v3, Lnet/flixster/android/DvdPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 139
    throw v1

    :cond_5
    move v1, v2

    .line 117
    goto :goto_3

    .line 127
    :pswitch_2
    :try_start_5
    iget-object v1, p0, Lnet/flixster/android/DvdPage$3;->this$0:Lnet/flixster/android/DvdPage;

    #calls: Lnet/flixster/android/DvdPage;->setBrowseLviList()V
    invoke-static {v1}, Lnet/flixster/android/DvdPage;->access$10(Lnet/flixster/android/DvdPage;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
