.class public Lnet/flixster/android/DvdPage;
.super Lnet/flixster/android/LviActivityCopy;
.source "DvdPage.java"


# instance fields
.field private mCategoryListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private final mComingSoon:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mComingSoonFeatured:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private mNavListener:Landroid/view/View$OnClickListener;

.field private mNavSelect:I

.field private final mNewReleases:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mNewReleasesFeatured:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private navBar:Lcom/flixster/android/view/SubNavBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lnet/flixster/android/LviActivityCopy;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/DvdPage;->mNewReleases:Ljava/util/ArrayList;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/DvdPage;->mComingSoon:Ljava/util/ArrayList;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/DvdPage;->mNewReleasesFeatured:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/DvdPage;->mComingSoonFeatured:Ljava/util/ArrayList;

    .line 275
    new-instance v0, Lnet/flixster/android/DvdPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/DvdPage$1;-><init>(Lnet/flixster/android/DvdPage;)V

    iput-object v0, p0, Lnet/flixster/android/DvdPage;->mCategoryListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 289
    new-instance v0, Lnet/flixster/android/DvdPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/DvdPage$2;-><init>(Lnet/flixster/android/DvdPage;)V

    iput-object v0, p0, Lnet/flixster/android/DvdPage;->mNavListener:Landroid/view/View$OnClickListener;

    .line 32
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(IJ)V
    .locals 4
    .parameter "navSelection"
    .parameter "delay"

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/DvdPage;->throbberHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 97
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    .line 98
    .local v0, currResumeCtr:I
    new-instance v1, Lnet/flixster/android/DvdPage$3;

    invoke-direct {v1, p0, p1, v0}, Lnet/flixster/android/DvdPage$3;-><init>(Lnet/flixster/android/DvdPage;II)V

    .line 143
    .local v1, loadMoviesTask:Ljava/util/TimerTask;
    iget-object v2, p0, Lnet/flixster/android/DvdPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 144
    iget-object v2, p0, Lnet/flixster/android/DvdPage;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v1, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :cond_0
    monitor-exit p0

    return-void

    .line 95
    .end local v0           #currResumeCtr:I
    .end local v1           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lnet/flixster/android/DvdPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39
    iput p1, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/DvdPage;)I
    .locals 1
    .parameter

    .prologue
    .line 39
    iget v0, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    return v0
.end method

.method static synthetic access$10(Lnet/flixster/android/DvdPage;)V
    .locals 0
    .parameter

    .prologue
    .line 251
    invoke-direct {p0}, Lnet/flixster/android/DvdPage;->setBrowseLviList()V

    return-void
.end method

.method static synthetic access$11(Lnet/flixster/android/DvdPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->checkAndShowLaunchAd()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/DvdPage;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .parameter

    .prologue
    .line 275
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mCategoryListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/DvdPage;IJ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    invoke-direct {p0, p1, p2, p3}, Lnet/flixster/android/DvdPage;->ScheduleLoadItemsTask(IJ)V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/DvdPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mNewReleases:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/DvdPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mNewReleasesFeatured:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/DvdPage;)V
    .locals 0
    .parameter

    .prologue
    .line 153
    invoke-direct {p0}, Lnet/flixster/android/DvdPage;->setNewReleasesLviList()V

    return-void
.end method

.method static synthetic access$7(Lnet/flixster/android/DvdPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mComingSoon:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/DvdPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mComingSoonFeatured:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$9(Lnet/flixster/android/DvdPage;)V
    .locals 0
    .parameter

    .prologue
    .line 201
    invoke-direct {p0}, Lnet/flixster/android/DvdPage;->setComingSoonLviList()V

    return-void
.end method

.method private setBrowseLviList()V
    .locals 7

    .prologue
    .line 254
    iget-object v5, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 256
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->destroyExistingLviAd()V

    .line 257
    new-instance v5, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v5, p0, Lnet/flixster/android/DvdPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 258
    iget-object v5, p0, Lnet/flixster/android/DvdPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v6, "DVDTab"

    iput-object v6, v5, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 259
    iget-object v5, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v6, p0, Lnet/flixster/android/DvdPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0008

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, categories:[Ljava/lang/String;
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0009

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 263
    .local v4, tags:[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_0

    .line 270
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 271
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v5, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    return-void

    .line 264
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    :cond_0
    new-instance v3, Lnet/flixster/android/lvi/LviCategory;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviCategory;-><init>()V

    .line 265
    .local v3, lviCategory:Lnet/flixster/android/lvi/LviCategory;
    aget-object v5, v0, v2

    iput-object v5, v3, Lnet/flixster/android/lvi/LviCategory;->mCategory:Ljava/lang/String;

    .line 266
    aget-object v5, v4, v2

    iput-object v5, v3, Lnet/flixster/android/lvi/LviCategory;->mFilter:Ljava/lang/String;

    .line 267
    iget-object v5, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private setComingSoonLviList()V
    .locals 11

    .prologue
    .line 204
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 206
    .local v0, date:Ljava/util/Date;
    iget-object v9, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 208
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->destroyExistingLviAd()V

    .line 209
    new-instance v9, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v9}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v9, p0, Lnet/flixster/android/DvdPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 210
    iget-object v9, p0, Lnet/flixster/android/DvdPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v10, "DVDTab"

    iput-object v10, v9, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 211
    iget-object v9, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v10, p0, Lnet/flixster/android/DvdPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v9, p0, Lnet/flixster/android/DvdPage;->mComingSoonFeatured:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    .line 215
    .local v4, mComingSoonFeaturedCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 216
    new-instance v8, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 217
    .local v8, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c00be

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 218
    iget-object v9, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 229
    .end local v8           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_0
    const-string v9, "FlxMain"

    const-string v10, "DvdPage.setUpcomingLviLlist"

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v9, p0, Lnet/flixster/android/DvdPage;->mComingSoon:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 231
    .local v3, mComingSoonCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 246
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 247
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v9, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    return-void

    .line 219
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    .end local v3           #mComingSoonCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .restart local v8       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Movie;

    .line 220
    .local v5, movie:Lnet/flixster/android/model/Movie;
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 221
    .local v2, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v5, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 222
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v2, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 223
    iget-object v10, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 231
    .end local v2           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v5           #movie:Lnet/flixster/android/model/Movie;
    .end local v8           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    .restart local v3       #mComingSoonCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Movie;

    .line 232
    .restart local v5       #movie:Lnet/flixster/android/model/Movie;
    const-string v10, "dvdReleaseDate"

    invoke-virtual {v5, v10}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 233
    .local v7, releaseDateString:Ljava/lang/String;
    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getDvdReleaseDate()Ljava/util/Date;

    move-result-object v6

    .line 234
    .local v6, releaseDate:Ljava/util/Date;
    if-eqz v6, :cond_3

    invoke-virtual {v0, v6}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 235
    new-instance v8, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 236
    .restart local v8       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    iput-object v7, v8, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 237
    iget-object v10, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    move-object v0, v6

    .line 240
    .end local v8           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_3
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 241
    .restart local v2       #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v5, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 242
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v2, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 243
    iget-object v10, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private setNewReleasesLviList()V
    .locals 12

    .prologue
    .line 157
    iget-object v10, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 159
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->destroyExistingLviAd()V

    .line 160
    new-instance v10, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v10}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v10, p0, Lnet/flixster/android/DvdPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 161
    iget-object v10, p0, Lnet/flixster/android/DvdPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v11, "DVDTab"

    iput-object v11, v10, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 162
    iget-object v10, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v11, p0, Lnet/flixster/android/DvdPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object v10, p0, Lnet/flixster/android/DvdPage;->mNewReleasesFeatured:Ljava/util/ArrayList;

    invoke-static {v10}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v7

    .line 166
    .local v7, mNewReleasesFeaturedCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    .line 167
    new-instance v9, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v9}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 168
    .local v9, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c00be

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 169
    iget-object v10, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_1

    .line 178
    .end local v9           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 179
    .local v0, date:Ljava/util/Date;
    const/4 v4, 0x1

    .line 180
    .local v4, isFirst:Z
    iget-object v10, p0, Lnet/flixster/android/DvdPage;->mNewReleases:Ljava/util/ArrayList;

    invoke-static {v10}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v6

    .line 181
    .local v6, mNewReleasesCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_2

    .line 197
    new-instance v3, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 198
    .local v3, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v10, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    return-void

    .line 170
    .end local v0           #date:Ljava/util/Date;
    .end local v3           #footer:Lnet/flixster/android/lvi/LviFooter;
    .end local v4           #isFirst:Z
    .end local v6           #mNewReleasesCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .restart local v9       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lnet/flixster/android/model/Movie;

    .line 171
    .local v8, movie:Lnet/flixster/android/model/Movie;
    new-instance v5, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 172
    .local v5, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v8, v5, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 173
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v11

    iput-object v11, v5, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 174
    iget-object v11, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 181
    .end local v5           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v8           #movie:Lnet/flixster/android/model/Movie;
    .end local v9           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    .restart local v0       #date:Ljava/util/Date;
    .restart local v4       #isFirst:Z
    .restart local v6       #mNewReleasesCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lnet/flixster/android/model/Movie;

    .line 182
    .restart local v8       #movie:Lnet/flixster/android/model/Movie;
    const-string v11, "dvdReleaseDate"

    invoke-virtual {v8, v11}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 183
    .local v2, dvdReleaseString:Ljava/lang/String;
    invoke-virtual {v8}, Lnet/flixster/android/model/Movie;->getDvdReleaseDate()Ljava/util/Date;

    move-result-object v1

    .line 184
    .local v1, dvdRelease:Ljava/util/Date;
    if-nez v4, :cond_3

    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 185
    :cond_3
    const/4 v4, 0x0

    .line 186
    new-instance v9, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v9}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 187
    .restart local v9       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    iput-object v2, v9, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 188
    iget-object v11, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    move-object v0, v1

    .line 191
    .end local v9           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_4
    new-instance v5, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 192
    .restart local v5       #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v8, v5, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 193
    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v11

    iput-object v11, v5, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 194
    iget-object v11, p0, Lnet/flixster/android/DvdPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method protected getAnalyticsAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 345
    const-string v0, "Logo"

    return-object v0
.end method

.method protected getAnalyticsCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    const-string v0, "WidgetEntrance"

    return-object v0
.end method

.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 306
    const/4 v0, 0x0

    .line 307
    .local v0, tag:Ljava/lang/String;
    iget v1, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    packed-switch v1, :pswitch_data_0

    .line 318
    :goto_0
    return-object v0

    .line 309
    :pswitch_0
    const-string v0, "/dvds/new-releases"

    .line 310
    goto :goto_0

    .line 312
    :pswitch_1
    const-string v0, "/dvds/coming-soon"

    .line 313
    goto :goto_0

    .line 315
    :pswitch_2
    const-string v0, "/dvds/browse"

    goto :goto_0

    .line 307
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 323
    const/4 v0, 0x0

    .line 324
    .local v0, title:Ljava/lang/String;
    iget v1, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    packed-switch v1, :pswitch_data_0

    .line 335
    :goto_0
    return-object v0

    .line 326
    :pswitch_0
    const-string v0, "Dvds - New Releases"

    .line 327
    goto :goto_0

    .line 329
    :pswitch_1
    const-string v0, "Dvds - Coming Soon"

    .line 330
    goto :goto_0

    .line 332
    :pswitch_2
    const-string v0, "Dvds - Browse"

    goto :goto_0

    .line 324
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected getFeaturedMovies()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 350
    const/4 v0, 0x0

    .line 351
    .local v0, featured:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    iget v1, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    packed-switch v1, :pswitch_data_0

    .line 359
    :goto_0
    return-object v0

    .line 353
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mNewReleasesFeatured:Ljava/util/ArrayList;

    .line 354
    goto :goto_0

    .line 356
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mComingSoonFeatured:Ljava/util/ArrayList;

    goto :goto_0

    .line 351
    nop

    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .parameter "savedState"

    .prologue
    const v6, 0x7f070258

    .line 44
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onCreate(Landroid/os/Bundle;)V

    .line 45
    iget-object v1, p0, Lnet/flixster/android/DvdPage;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 46
    const v1, 0x7f0700f3

    invoke-virtual {p0, v1}, Lnet/flixster/android/DvdPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 48
    .local v0, mNavbarHolder:Landroid/widget/LinearLayout;
    new-instance v1, Lcom/flixster/android/view/SubNavBar;

    invoke-direct {v1, p0}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lnet/flixster/android/DvdPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    .line 49
    iget-object v1, p0, Lnet/flixster/android/DvdPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget-object v2, p0, Lnet/flixster/android/DvdPage;->mNavListener:Landroid/view/View$OnClickListener;

    const v3, 0x7f0c002e

    const v4, 0x7f0c002d

    const v5, 0x7f0c0029

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;III)V

    .line 50
    iget-object v1, p0, Lnet/flixster/android/DvdPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {v1, v6}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 51
    iget-object v1, p0, Lnet/flixster/android/DvdPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 53
    iput v6, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    .line 55
    iget-object v1, p0, Lnet/flixster/android/DvdPage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    const-string v2, "DVDTabStickyTop"

    invoke-virtual {v1, v2}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 56
    iget-object v1, p0, Lnet/flixster/android/DvdPage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    const-string v2, "DVDTabStickyBottom"

    invoke-virtual {v1, v2}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lnet/flixster/android/LviActivityCopy;->onPause()V

    .line 74
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mNewReleases:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 75
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mComingSoon:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 76
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mNewReleasesFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 77
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mComingSoonFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 78
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 89
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 90
    const-string v0, "FlxMain"

    const-string v1, "DvdPage.onRestoreInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v0, "LISTSTATE_NAV"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    .line 92
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0}, Lnet/flixster/android/LviActivityCopy;->onResume()V

    .line 62
    iget v0, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    const v1, 0x7f07025a

    if-ne v0, v1, :cond_0

    .line 63
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lnet/flixster/android/DvdPage;->mCategoryListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 67
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget v1, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 68
    iget v0, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    const-wide/16 v1, 0x64

    invoke-direct {p0, v0, v1, v2}, Lnet/flixster/android/DvdPage;->ScheduleLoadItemsTask(IJ)V

    .line 69
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/DvdPage;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/DvdPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 82
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 83
    const-string v0, "FlxMain"

    const-string v1, "DvdPage.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v0, "LISTSTATE_NAV"

    iget v1, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 85
    return-void
.end method

.method protected retryAction()V
    .locals 3

    .prologue
    .line 150
    iget v0, p0, Lnet/flixster/android/DvdPage;->mNavSelect:I

    const-wide/16 v1, 0x3e8

    invoke-direct {p0, v0, v1, v2}, Lnet/flixster/android/DvdPage;->ScheduleLoadItemsTask(IJ)V

    .line 151
    return-void
.end method
