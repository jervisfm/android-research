.class Lnet/flixster/android/FacebookAuth$1;
.super Ljava/lang/Object;
.source "FacebookAuth.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FacebookAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FacebookAuth;


# direct methods
.method constructor <init>(Lnet/flixster/android/FacebookAuth;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FacebookAuth$1;->this$0:Lnet/flixster/android/FacebookAuth;

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 1
    .parameter "which"

    .prologue
    .line 97
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$1;->this$0:Lnet/flixster/android/FacebookAuth;

    invoke-virtual {v0}, Lnet/flixster/android/FacebookAuth;->finish()V

    .line 98
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 93
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 6
    .parameter "which"

    .prologue
    .line 84
    const/4 v0, 0x1

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setTosAccepted(Z)V

    .line 85
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth.onCreate pre .authorize"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    iget-object v2, p0, Lnet/flixster/android/FacebookAuth$1;->this$0:Lnet/flixster/android/FacebookAuth;

    .line 87
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lnet/flixster/android/FacebookAuth;->access$0()[Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v3, Lnet/flixster/android/FacebookAuth$LoginDialogListener;

    iget-object v4, p0, Lnet/flixster/android/FacebookAuth$1;->this$0:Lnet/flixster/android/FacebookAuth;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lnet/flixster/android/FacebookAuth$LoginDialogListener;-><init>(Lnet/flixster/android/FacebookAuth;Lnet/flixster/android/FacebookAuth$LoginDialogListener;)V

    .line 86
    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/android/Facebook;->authorize(Landroid/app/Activity;[Ljava/lang/String;Lcom/facebook/android/Facebook$DialogListener;)V

    .line 88
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth.onCreate post .authorize"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    return-void

    .line 87
    :cond_0
    invoke-static {}, Lnet/flixster/android/FacebookAuth;->access$1()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
