.class Lnet/flixster/android/FacebookAuth$2;
.super Landroid/os/Handler;
.source "FacebookAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FacebookAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FacebookAuth;


# direct methods
.method constructor <init>(Lnet/flixster/android/FacebookAuth;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FacebookAuth$2;->this$0:Lnet/flixster/android/FacebookAuth;

    .line 208
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "msg"

    .prologue
    .line 212
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth  mFlixsterConnectedHandler yay... connect "

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$2;->this$0:Lnet/flixster/android/FacebookAuth;

    invoke-virtual {v0}, Lnet/flixster/android/FacebookAuth;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 215
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$2;->this$0:Lnet/flixster/android/FacebookAuth;

    #getter for: Lnet/flixster/android/FacebookAuth;->requestCode:I
    invoke-static {v0}, Lnet/flixster/android/FacebookAuth;->access$2(Lnet/flixster/android/FacebookAuth;)I

    move-result v0

    invoke-static {v0}, Lcom/flixster/android/msk/MskController;->isRequestCodeValid(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/msk/MskController;->trackFbLoginSuccess()V

    .line 218
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$2;->this$0:Lnet/flixster/android/FacebookAuth;

    #getter for: Lnet/flixster/android/FacebookAuth;->showConnectedDialog:Z
    invoke-static {v0}, Lnet/flixster/android/FacebookAuth;->access$3(Lnet/flixster/android/FacebookAuth;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 219
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$2;->this$0:Lnet/flixster/android/FacebookAuth;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/FacebookAuth;->showDialog(I)V

    .line 226
    :cond_1
    :goto_0
    return-void

    .line 221
    :cond_2
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->release()V

    .line 222
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$2;->this$0:Lnet/flixster/android/FacebookAuth;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/FacebookAuth;->setResult(I)V

    .line 223
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$2;->this$0:Lnet/flixster/android/FacebookAuth;

    invoke-virtual {v0}, Lnet/flixster/android/FacebookAuth;->finish()V

    goto :goto_0
.end method
