.class public Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;
.super Lcom/facebook/android/BaseRequestListener;
.source "FacebookAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FacebookAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FbLoginRequestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FacebookAuth;


# direct methods
.method public constructor <init>(Lnet/flixster/android/FacebookAuth;)V
    .locals 0
    .parameter

    .prologue
    .line 182
    iput-object p1, p0, Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;->this$0:Lnet/flixster/android/FacebookAuth;

    invoke-direct {p0}, Lcom/facebook/android/BaseRequestListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 7
    .parameter "response"
    .parameter "state"

    .prologue
    const/4 v6, 0x0

    .line 187
    :try_start_0
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FacebookAuth.FbLoginRequestListener Response: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    invoke-static {p1}, Lcom/facebook/android/Util;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 189
    .local v2, json:Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, id:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v3

    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v3, v4, v1}, Lcom/flixster/android/data/AccountManager;->onFbLogin(Lcom/facebook/android/Facebook;Ljava/lang/String;)V

    .line 191
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->resetUser()V

    .line 192
    iget-object v3, p0, Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;->this$0:Lnet/flixster/android/FacebookAuth;

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;

    move-result-object v4

    #setter for: Lnet/flixster/android/FacebookAuth;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3, v4}, Lnet/flixster/android/FacebookAuth;->access$4(Lnet/flixster/android/FacebookAuth;Lnet/flixster/android/model/User;)V

    .line 193
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;->this$0:Lnet/flixster/android/FacebookAuth;

    #getter for: Lnet/flixster/android/FacebookAuth;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v4}, Lnet/flixster/android/FacebookAuth;->access$5(Lnet/flixster/android/FacebookAuth;)Lnet/flixster/android/model/User;

    move-result-object v4

    const-string v5, "displayName"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/User;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;->this$0:Lnet/flixster/android/FacebookAuth;

    #getter for: Lnet/flixster/android/FacebookAuth;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v5}, Lnet/flixster/android/FacebookAuth;->access$5(Lnet/flixster/android/FacebookAuth;)Lnet/flixster/android/model/User;

    move-result-object v5

    iget-object v5, v5, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/flixster/android/data/AccountManager;->onFbLogin(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v3, p0, Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;->this$0:Lnet/flixster/android/FacebookAuth;

    #getter for: Lnet/flixster/android/FacebookAuth;->mFacebookConnectedDialogHandler:Landroid/os/Handler;
    invoke-static {v3}, Lnet/flixster/android/FacebookAuth;->access$6(Lnet/flixster/android/FacebookAuth;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/facebook/android/FacebookError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 205
    .end local v1           #id:Ljava/lang/String;
    .end local v2           #json:Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, e:Lorg/json/JSONException;
    const-string v3, "FlxMain"

    const-string v4, "JSON Error in response"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    .end local v0           #e:Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 198
    .local v0, e:Lcom/facebook/android/FacebookError;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Facebook Error: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/android/FacebookError;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v3, p0, Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;->this$0:Lnet/flixster/android/FacebookAuth;

    #getter for: Lnet/flixster/android/FacebookAuth;->mFacebookFubarDialogHandler:Landroid/os/Handler;
    invoke-static {v3}, Lnet/flixster/android/FacebookAuth;->access$7(Lnet/flixster/android/FacebookAuth;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 201
    .end local v0           #e:Lcom/facebook/android/FacebookError;
    :catch_2
    move-exception v0

    .line 202
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception Facebook Error: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v3, p0, Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;->this$0:Lnet/flixster/android/FacebookAuth;

    #getter for: Lnet/flixster/android/FacebookAuth;->mFacebookErrorDialogHandler:Landroid/os/Handler;
    invoke-static {v3}, Lnet/flixster/android/FacebookAuth;->access$8(Lnet/flixster/android/FacebookAuth;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
