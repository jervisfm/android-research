.class final Lnet/flixster/android/FacebookAuth$LoginDialogListener;
.super Ljava/lang/Object;
.source "FacebookAuth.java"

# interfaces
.implements Lcom/facebook/android/Facebook$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FacebookAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LoginDialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FacebookAuth;


# direct methods
.method private constructor <init>(Lnet/flixster/android/FacebookAuth;)V
    .locals 0
    .parameter

    .prologue
    .line 146
    iput-object p1, p0, Lnet/flixster/android/FacebookAuth$LoginDialogListener;->this$0:Lnet/flixster/android/FacebookAuth;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lnet/flixster/android/FacebookAuth;Lnet/flixster/android/FacebookAuth$LoginDialogListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lnet/flixster/android/FacebookAuth$LoginDialogListener;-><init>(Lnet/flixster/android/FacebookAuth;)V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    .prologue
    .line 176
    const-string v0, "Action Canceled"

    invoke-static {v0}, Lcom/facebook/android/SessionEvents;->onLoginError(Ljava/lang/String;)V

    .line 177
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth.LoginDialogListener.onCancel"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$LoginDialogListener;->this$0:Lnet/flixster/android/FacebookAuth;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lnet/flixster/android/FacebookAuth;->showDialog(I)V

    .line 179
    return-void
.end method

.method public onComplete(Landroid/os/Bundle;)V
    .locals 5
    .parameter "values"

    .prologue
    .line 148
    invoke-static {}, Lcom/facebook/android/SessionEvents;->onLoginSuccess()V

    .line 149
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    .line 150
    .local v1, fb:Lcom/facebook/android/Facebook;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FacebookAuth.LoginDialogListener.onComplete "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->sd(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    new-instance v0, Lcom/facebook/android/AsyncFacebookRunner;

    invoke-direct {v0, v1}, Lcom/facebook/android/AsyncFacebookRunner;-><init>(Lcom/facebook/android/Facebook;)V

    .line 152
    .local v0, asyncRunner:Lcom/facebook/android/AsyncFacebookRunner;
    const-string v2, "me"

    new-instance v3, Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;

    iget-object v4, p0, Lnet/flixster/android/FacebookAuth$LoginDialogListener;->this$0:Lnet/flixster/android/FacebookAuth;

    invoke-direct {v3, v4}, Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;-><init>(Lnet/flixster/android/FacebookAuth;)V

    invoke-virtual {v0, v2, v3}, Lcom/facebook/android/AsyncFacebookRunner;->request(Ljava/lang/String;Lcom/facebook/android/AsyncFacebookRunner$RequestListener;)V

    .line 153
    return-void
.end method

.method public onError(Lcom/facebook/android/DialogError;)V
    .locals 2
    .parameter "error"

    .prologue
    .line 169
    invoke-virtual {p1}, Lcom/facebook/android/DialogError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/android/SessionEvents;->onLoginError(Ljava/lang/String;)V

    .line 170
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth.LoginDialogListener.onError"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$LoginDialogListener;->this$0:Lnet/flixster/android/FacebookAuth;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lnet/flixster/android/FacebookAuth;->showDialog(I)V

    .line 172
    return-void
.end method

.method public onFacebookError(Lcom/facebook/android/FacebookError;)V
    .locals 3
    .parameter "error"

    .prologue
    .line 157
    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/android/SessionEvents;->onLoginError(Ljava/lang/String;)V

    .line 158
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FacebookAuth.LoginDialogListener.onFacebookError code:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 159
    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->getErrorType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " message:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 158
    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "user_denied"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$LoginDialogListener;->this$0:Lnet/flixster/android/FacebookAuth;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lnet/flixster/android/FacebookAuth;->showDialog(I)V

    .line 165
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth$LoginDialogListener;->this$0:Lnet/flixster/android/FacebookAuth;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lnet/flixster/android/FacebookAuth;->showDialog(I)V

    goto :goto_0
.end method
