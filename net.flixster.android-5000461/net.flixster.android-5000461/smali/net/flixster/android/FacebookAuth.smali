.class public Lnet/flixster/android/FacebookAuth;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "FacebookAuth.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/FacebookAuth$FbLoginRequestListener;,
        Lnet/flixster/android/FacebookAuth$LoginDialogListener;
    }
.end annotation


# static fields
.field private static final DIALOGKEY_FB_CANCELED:I = 0x8

.field private static final DIALOGKEY_FB_CONNECTED:I = 0x1

.field private static final DIALOGKEY_FB_DENIED:I = 0x7

.field private static final DIALOGKEY_FB_ERROR:I = 0x3

.field private static final DIALOGKEY_FB_FUBAR:I = 0x6

.field private static final GTV_PERMISSIONS:[Ljava/lang/String; = null

.field public static final KEY_SHOW_CONNECTED_DIALOG:Ljava/lang/String; = "KEY_SHOW_CONNECTED_DIALOG"

.field private static final PERMISSIONS:[Ljava/lang/String; = null

.field public static final REQUESTCODE_CONNECT:I = 0x7b


# instance fields
.field private mFacebookConnectedDialogHandler:Landroid/os/Handler;

.field private mFacebookErrorDialogHandler:Landroid/os/Handler;

.field private mFacebookFubarDialogHandler:Landroid/os/Handler;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mUser:Lnet/flixster/android/model/User;

.field private requestCode:I

.field private showConnectedDialog:Z

.field private final tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "publish_actions"

    aput-object v1, v0, v3

    const-string v1, "email"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "offline_access"

    aput-object v2, v0, v1

    sput-object v0, Lnet/flixster/android/FacebookAuth;->PERMISSIONS:[Ljava/lang/String;

    .line 43
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "offline_access"

    aput-object v1, v0, v3

    sput-object v0, Lnet/flixster/android/FacebookAuth;->GTV_PERMISSIONS:[Ljava/lang/String;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/FacebookAuth;->showConnectedDialog:Z

    .line 81
    new-instance v0, Lnet/flixster/android/FacebookAuth$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/FacebookAuth$1;-><init>(Lnet/flixster/android/FacebookAuth;)V

    iput-object v0, p0, Lnet/flixster/android/FacebookAuth;->tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 208
    new-instance v0, Lnet/flixster/android/FacebookAuth$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/FacebookAuth$2;-><init>(Lnet/flixster/android/FacebookAuth;)V

    iput-object v0, p0, Lnet/flixster/android/FacebookAuth;->mFacebookConnectedDialogHandler:Landroid/os/Handler;

    .line 229
    new-instance v0, Lnet/flixster/android/FacebookAuth$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/FacebookAuth$3;-><init>(Lnet/flixster/android/FacebookAuth;)V

    iput-object v0, p0, Lnet/flixster/android/FacebookAuth;->mFacebookErrorDialogHandler:Landroid/os/Handler;

    .line 240
    new-instance v0, Lnet/flixster/android/FacebookAuth$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/FacebookAuth$4;-><init>(Lnet/flixster/android/FacebookAuth;)V

    iput-object v0, p0, Lnet/flixster/android/FacebookAuth;->mFacebookFubarDialogHandler:Landroid/os/Handler;

    .line 38
    return-void
.end method

.method static synthetic access$0()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lnet/flixster/android/FacebookAuth;->GTV_PERMISSIONS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lnet/flixster/android/FacebookAuth;->PERMISSIONS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/FacebookAuth;)I
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lnet/flixster/android/FacebookAuth;->requestCode:I

    return v0
.end method

.method static synthetic access$3(Lnet/flixster/android/FacebookAuth;)Z
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-boolean v0, p0, Lnet/flixster/android/FacebookAuth;->showConnectedDialog:Z

    return v0
.end method

.method static synthetic access$4(Lnet/flixster/android/FacebookAuth;Lnet/flixster/android/model/User;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lnet/flixster/android/FacebookAuth;->mUser:Lnet/flixster/android/model/User;

    return-void
.end method

.method static synthetic access$5(Lnet/flixster/android/FacebookAuth;)Lnet/flixster/android/model/User;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth;->mUser:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/FacebookAuth;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 208
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth;->mFacebookConnectedDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$7(Lnet/flixster/android/FacebookAuth;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 240
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth;->mFacebookFubarDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/FacebookAuth;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 229
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth;->mFacebookErrorDialogHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 134
    invoke-super {p0, p1, p2, p3}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 135
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FacebookAuth.onActivityResult pre .authorizeCallback requestCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/android/Facebook;->authorizeCallback(IILandroid/content/Intent;)V

    .line 137
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth.onActivityResult post .authorizeCallback "

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth.onActivityResult session valid"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_0
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth.onActivityResult session not valid"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .parameter "config"

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 127
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth.onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->dismissDialog()V

    .line 129
    iget-object v0, p0, Lnet/flixster/android/FacebookAuth;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lnet/flixster/android/FacebookAuth;->onCreate(Landroid/os/Bundle;)V

    .line 130
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    iput-object p1, p0, Lnet/flixster/android/FacebookAuth;->mSavedInstanceState:Landroid/os/Bundle;

    .line 60
    invoke-virtual {p0}, Lnet/flixster/android/FacebookAuth;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 62
    const-string v1, "KEY_SHOW_CONNECTED_DIALOG"

    iget-boolean v2, p0, Lnet/flixster/android/FacebookAuth;->showConnectedDialog:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lnet/flixster/android/FacebookAuth;->showConnectedDialog:Z

    .line 63
    const-string v1, "MskEntryActivity.REQUEST_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lnet/flixster/android/FacebookAuth;->requestCode:I

    .line 66
    :cond_0
    const v1, 0x7f03001c

    invoke-virtual {p0, v1}, Lnet/flixster/android/FacebookAuth;->setContentView(I)V

    .line 67
    invoke-virtual {p0}, Lnet/flixster/android/FacebookAuth;->createActionBar()V

    .line 68
    const v1, 0x7f0c0065

    invoke-virtual {p0, v1}, Lnet/flixster/android/FacebookAuth;->setActionBarTitle(I)V

    .line 70
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->hasAcceptedTos()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 71
    iget-object v1, p0, Lnet/flixster/android/FacebookAuth;->tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/flixster/android/view/DialogBuilder$DialogListener;->onPositiveButtonClick(I)V

    .line 76
    :goto_0
    iget v1, p0, Lnet/flixster/android/FacebookAuth;->requestCode:I

    invoke-static {v1}, Lcom/flixster/android/msk/MskController;->isRequestCodeValid(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController;->trackFbLoginAttempt()V

    .line 79
    :cond_1
    return-void

    .line 73
    :cond_2
    iget-object v1, p0, Lnet/flixster/android/FacebookAuth;->tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    invoke-static {p0, v1}, Lcom/flixster/android/view/DialogBuilder;->showTermsOfService(Landroid/app/Activity;Lcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .parameter "id"

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 254
    packed-switch p1, :pswitch_data_0

    .line 313
    :pswitch_0
    const-string v1, "FlixsterCache"

    const-string v2, "onCreateDialog() default:fell through dialog switch"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :goto_0
    return-object v0

    .line 257
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 258
    invoke-virtual {p0}, Lnet/flixster/android/FacebookAuth;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 259
    invoke-virtual {p0}, Lnet/flixster/android/FacebookAuth;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0131

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lnet/flixster/android/FacebookAuth;->mUser:Lnet/flixster/android/model/User;

    iget-object v3, v3, Lnet/flixster/android/model/User;->firstName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 260
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lnet/flixster/android/FacebookAuth$5;

    invoke-direct {v2, p0}, Lnet/flixster/android/FacebookAuth$5;-><init>(Lnet/flixster/android/FacebookAuth;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 268
    :pswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 269
    invoke-virtual {p0}, Lnet/flixster/android/FacebookAuth;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 270
    invoke-virtual {p0}, Lnet/flixster/android/FacebookAuth;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 271
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lnet/flixster/android/FacebookAuth$6;

    invoke-direct {v2, p0}, Lnet/flixster/android/FacebookAuth$6;-><init>(Lnet/flixster/android/FacebookAuth;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 277
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 279
    :pswitch_3
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 280
    const-string v1, "We were unable to log you in to Facebook."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Connection Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 281
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lnet/flixster/android/FacebookAuth$7;

    invoke-direct {v2, p0}, Lnet/flixster/android/FacebookAuth$7;-><init>(Lnet/flixster/android/FacebookAuth;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 287
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 290
    :pswitch_4
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 291
    invoke-virtual {p0}, Lnet/flixster/android/FacebookAuth;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00ec

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 292
    invoke-virtual {p0}, Lnet/flixster/android/FacebookAuth;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00eb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 293
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lnet/flixster/android/FacebookAuth$8;

    invoke-direct {v2, p0}, Lnet/flixster/android/FacebookAuth$8;-><init>(Lnet/flixster/android/FacebookAuth;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 299
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 302
    :pswitch_5
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 303
    invoke-virtual {p0}, Lnet/flixster/android/FacebookAuth;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 304
    const-string v1, "Facebook Canceled"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 305
    const-string v1, "OK"

    new-instance v2, Lnet/flixster/android/FacebookAuth$9;

    invoke-direct {v2, p0}, Lnet/flixster/android/FacebookAuth$9;-><init>(Lnet/flixster/android/FacebookAuth;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 311
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 254
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 321
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 115
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 116
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lnet/flixster/android/FacebookAuth;->setResult(I)V

    .line 117
    invoke-virtual {p0}, Lnet/flixster/android/FacebookAuth;->finish()V

    .line 118
    const/4 v0, 0x1

    .line 121
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 109
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onPause()V

    .line 110
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth.onPause"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 104
    const-string v0, "FlxMain"

    const-string v1, "FacebookAuth.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method
