.class final Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "FeedPermissionPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FeedPermissionPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FlixsterWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FeedPermissionPage;


# direct methods
.method private constructor <init>(Lnet/flixster/android/FeedPermissionPage;)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;->this$0:Lnet/flixster/android/FeedPermissionPage;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lnet/flixster/android/FeedPermissionPage;Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;-><init>(Lnet/flixster/android/FeedPermissionPage;)V

    return-void
.end method


# virtual methods
.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .parameter "view"
    .parameter "url"

    .prologue
    .line 67
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;->this$0:Lnet/flixster/android/FeedPermissionPage;

    #getter for: Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lnet/flixster/android/FeedPermissionPage;->access$0(Lnet/flixster/android/FeedPermissionPage;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;->this$0:Lnet/flixster/android/FeedPermissionPage;

    #getter for: Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lnet/flixster/android/FeedPermissionPage;->access$0(Lnet/flixster/android/FeedPermissionPage;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 70
    :cond_0
    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;->this$0:Lnet/flixster/android/FeedPermissionPage;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lnet/flixster/android/FeedPermissionPage;->showDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :cond_1
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .parameter "view"
    .parameter "url"

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;->this$0:Lnet/flixster/android/FeedPermissionPage;

    #getter for: Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lnet/flixster/android/FeedPermissionPage;->access$0(Lnet/flixster/android/FeedPermissionPage;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;->this$0:Lnet/flixster/android/FeedPermissionPage;

    #getter for: Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lnet/flixster/android/FeedPermissionPage;->access$0(Lnet/flixster/android/FeedPermissionPage;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;->this$0:Lnet/flixster/android/FeedPermissionPage;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lnet/flixster/android/FeedPermissionPage;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 84
    :catch_0
    move-exception v0

    goto :goto_0
.end method
