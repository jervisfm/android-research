.class public Lnet/flixster/android/FeedPermissionPage;
.super Lcom/actionbarsherlock/app/SherlockActivity;
.source "FeedPermissionPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/FeedPermissionPage$FlixsterJavascriptInterface;,
        Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;
    }
.end annotation


# static fields
.field private static final DIALOG_LOADING:I = 0x4


# instance fields
.field private dialog:Landroid/app/ProgressDialog;

.field private intent:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/FeedPermissionPage;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 19
    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .parameter "savedState"

    .prologue
    const/4 v6, 0x0

    .line 24
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    const v5, 0x7f03002a

    invoke-virtual {p0, v5}, Lnet/flixster/android/FeedPermissionPage;->setContentView(I)V

    .line 26
    const v5, 0x7f07007f

    invoke-virtual {p0, v5}, Lnet/flixster/android/FeedPermissionPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/webkit/WebView;

    .line 27
    .local v4, webView:Landroid/webkit/WebView;
    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    .line 28
    .local v3, webSettings:Landroid/webkit/WebSettings;
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 29
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 30
    new-instance v2, Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;

    invoke-direct {v2, p0, v6}, Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;-><init>(Lnet/flixster/android/FeedPermissionPage;Lnet/flixster/android/FeedPermissionPage$FlixsterWebViewClient;)V

    .line 31
    .local v2, webClient:Landroid/webkit/WebViewClient;
    invoke-virtual {v4, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 32
    new-instance v5, Lnet/flixster/android/FeedPermissionPage$FlixsterJavascriptInterface;

    invoke-direct {v5, p0, v6}, Lnet/flixster/android/FeedPermissionPage$FlixsterJavascriptInterface;-><init>(Lnet/flixster/android/FeedPermissionPage;Lnet/flixster/android/FeedPermissionPage$FlixsterJavascriptInterface;)V

    const-string v6, "flixster"

    invoke-virtual {v4, v5, v6}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p0}, Lnet/flixster/android/FeedPermissionPage;->getIntent()Landroid/content/Intent;

    move-result-object v5

    iput-object v5, p0, Lnet/flixster/android/FeedPermissionPage;->intent:Landroid/content/Intent;

    .line 34
    iget-object v5, p0, Lnet/flixster/android/FeedPermissionPage;->intent:Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 35
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 36
    const-string v5, "net.flixster.FeedUrl"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, feedUrl:Ljava/lang/String;
    invoke-virtual {v4, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 39
    .end local v1           #feedUrl:Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "dialogId"

    .prologue
    const/4 v3, 0x1

    .line 44
    packed-switch p1, :pswitch_data_0

    .line 53
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 46
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;

    .line 47
    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lnet/flixster/android/FeedPermissionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 48
    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 49
    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 50
    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 51
    iget-object v0, p0, Lnet/flixster/android/FeedPermissionPage;->dialog:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 58
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockActivity;->onResume()V

    .line 59
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/facebook/feed/permission"

    const-string v2, "Facebook - Feed Permissions"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-void
.end method
