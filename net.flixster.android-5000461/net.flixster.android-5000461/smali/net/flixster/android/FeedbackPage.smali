.class public Lnet/flixster/android/FeedbackPage;
.super Landroid/app/Activity;
.source "FeedbackPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field mBodyArray:[Ljava/lang/String;

.field mLine1Array:[Ljava/lang/String;

.field mLine2Array:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "view"

    .prologue
    .line 62
    const-string v3, "FlxMain"

    const-string v4, "FeedbackPage.onClick"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 64
    .local v0, index:Ljava/lang/Integer;
    invoke-virtual {p0}, Lnet/flixster/android/FeedbackPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lnet/flixster/android/FeedbackPage;->mBodyArray:[Ljava/lang/String;

    .line 66
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 67
    .local v1, intent:Landroid/content/Intent;
    const/high16 v3, 0x100

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 68
    const/high16 v3, 0x200

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 69
    const-string v3, "message/rfc822"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    const-string v3, "android.intent.extra.EMAIL"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->supportEmail()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v3, "android.intent.extra.SUBJECT"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Android Feedback -"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/FeedbackPage;->mLine1Array:[Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentZip()Ljava/lang/String;

    move-result-object v2

    .line 74
    .local v2, zip:Ljava/lang/String;
    const-string v3, "android.intent.extra.TEXT"

    .line 75
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lnet/flixster/android/FeedbackPage;->mBodyArray:[Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aget-object v5, v5, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "\n\n\n\n_____________________\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserAgent()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 74
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    invoke-static {p0, v1}, Lnet/flixster/android/Starter;->tryLaunch(Landroid/content/Context;Landroid/content/Intent;)V

    .line 78
    invoke-virtual {p0}, Lnet/flixster/android/FeedbackPage;->finish()V

    .line 79
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .parameter "savedInstanceState"

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v7, 0x7f03002b

    invoke-virtual {p0, v7}, Lnet/flixster/android/FeedbackPage;->setContentView(I)V

    .line 32
    invoke-virtual {p0}, Lnet/flixster/android/FeedbackPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e000d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lnet/flixster/android/FeedbackPage;->mLine1Array:[Ljava/lang/String;

    .line 33
    invoke-virtual {p0}, Lnet/flixster/android/FeedbackPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e000e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lnet/flixster/android/FeedbackPage;->mLine2Array:[Ljava/lang/String;

    .line 34
    iget-object v7, p0, Lnet/flixster/android/FeedbackPage;->mLine1Array:[Ljava/lang/String;

    array-length v2, v7

    .line 36
    .local v2, length:I
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 37
    .local v3, li:Landroid/view/LayoutInflater;
    const v7, 0x7f070080

    invoke-virtual {p0, v7}, Lnet/flixster/android/FeedbackPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 41
    .local v6, listLayout:Landroid/widget/LinearLayout;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 51
    return-void

    .line 42
    :cond_0
    const v7, 0x7f030079

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 43
    .local v1, item:Landroid/view/View;
    const v7, 0x7f0701d7

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 44
    .local v4, line1:Landroid/widget/TextView;
    iget-object v7, p0, Lnet/flixster/android/FeedbackPage;->mLine1Array:[Ljava/lang/String;

    aget-object v7, v7, v0

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    const v7, 0x7f070236

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 46
    .local v5, line2:Landroid/widget/TextView;
    iget-object v7, p0, Lnet/flixster/android/FeedbackPage;->mLine2Array:[Ljava/lang/String;

    aget-object v7, v7, v0

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 49
    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 55
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 56
    const-string v0, "FlxMain"

    const-string v1, "SettingsPage.onResume()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/feedback"

    const-string v2, "Feedback Page"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void
.end method
