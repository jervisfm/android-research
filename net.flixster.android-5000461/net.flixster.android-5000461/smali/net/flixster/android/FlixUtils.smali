.class public Lnet/flixster/android/FlixUtils;
.super Ljava/lang/Object;
.source "FlixUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "string"

    .prologue
    .line 7
    if-nez p0, :cond_0

    .line 8
    const/4 v1, 0x0

    .line 12
    :goto_0
    return-object v1

    .line 10
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 11
    .local v0, tempChars:[C
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v1

    .line 12
    .local v1, tempString:Ljava/lang/String;
    goto :goto_0
.end method
