.class Lnet/flixster/android/Flixster$2;
.super Ljava/lang/Object;
.source "Flixster.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/Flixster;->showDiagnosticsOrRateDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/Flixster;


# direct methods
.method constructor <init>(Lnet/flixster/android/Flixster;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/Flixster$2;->this$0:Lnet/flixster/android/Flixster;

    .line 496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 5
    .parameter "which"

    .prologue
    .line 514
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->setMskPromptShown()V

    .line 515
    iget-object v0, p0, Lnet/flixster/android/Flixster$2;->this$0:Lnet/flixster/android/Flixster;

    const v1, 0x3b9acde9

    invoke-virtual {v0, v1}, Lnet/flixster/android/Flixster;->removeDialog(I)V

    .line 516
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/msk/tooltip"

    const-string v2, "MSK Tooltip"

    const-string v3, "MskTooltip"

    const-string v4, "Dismiss"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 5
    .parameter "which"

    .prologue
    .line 507
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->setMskPromptShown()V

    .line 508
    iget-object v0, p0, Lnet/flixster/android/Flixster$2;->this$0:Lnet/flixster/android/Flixster;

    const v1, 0x3b9acde9

    invoke-virtual {v0, v1}, Lnet/flixster/android/Flixster;->removeDialog(I)V

    .line 509
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/msk/tooltip"

    const-string v2, "MSK Tooltip"

    const-string v3, "MskTooltip"

    const-string v4, "Dismiss"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 5
    .parameter "which"

    .prologue
    .line 499
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->setMskPromptShown()V

    .line 500
    iget-object v0, p0, Lnet/flixster/android/Flixster$2;->this$0:Lnet/flixster/android/Flixster;

    const v1, 0x3b9acde9

    invoke-virtual {v0, v1}, Lnet/flixster/android/Flixster;->removeDialog(I)V

    .line 501
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/msk/tooltip"

    const-string v2, "MSK Tooltip"

    const-string v3, "MskTooltip"

    const-string v4, "Click"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    iget-object v0, p0, Lnet/flixster/android/Flixster$2;->this$0:Lnet/flixster/android/Flixster;

    invoke-static {v0}, Lnet/flixster/android/Starter;->launchMsk(Landroid/content/Context;)V

    .line 503
    return-void
.end method
