.class Lnet/flixster/android/Flixster$3;
.super Ljava/lang/Object;
.source "Flixster.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/Flixster;->showDiagnosticsOrRateDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/Flixster;

.field private final synthetic val$intentz:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lnet/flixster/android/Flixster;Landroid/content/Intent;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/Flixster$3;->this$0:Lnet/flixster/android/Flixster;

    iput-object p2, p0, Lnet/flixster/android/Flixster$3;->val$intentz:Landroid/content/Intent;

    .line 524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 536
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 532
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 527
    iget-object v0, p0, Lnet/flixster/android/Flixster$3;->this$0:Lnet/flixster/android/Flixster;

    iget-object v1, p0, Lnet/flixster/android/Flixster$3;->val$intentz:Landroid/content/Intent;

    invoke-static {v0, v1}, Lnet/flixster/android/Starter;->tryLaunch(Landroid/content/Context;Landroid/content/Intent;)V

    .line 528
    return-void
.end method
