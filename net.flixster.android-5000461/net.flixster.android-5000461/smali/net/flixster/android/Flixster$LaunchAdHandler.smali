.class Lnet/flixster/android/Flixster$LaunchAdHandler;
.super Landroid/os/Handler;
.source "Flixster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/Flixster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LaunchAdHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/Flixster;


# direct methods
.method private constructor <init>(Lnet/flixster/android/Flixster;)V
    .locals 0
    .parameter

    .prologue
    .line 562
    iput-object p1, p0, Lnet/flixster/android/Flixster$LaunchAdHandler;->this$0:Lnet/flixster/android/Flixster;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lnet/flixster/android/Flixster;Lnet/flixster/android/Flixster$LaunchAdHandler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 562
    invoke-direct {p0, p1}, Lnet/flixster/android/Flixster$LaunchAdHandler;-><init>(Lnet/flixster/android/Flixster;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 565
    iget-object v0, p0, Lnet/flixster/android/Flixster$LaunchAdHandler;->this$0:Lnet/flixster/android/Flixster;

    invoke-virtual {v0}, Lnet/flixster/android/Flixster;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 575
    :cond_0
    :goto_0
    return-void

    .line 569
    :cond_1
    const-string v0, "FlxMain"

    const-string v1, "Flixster.LaunchAdHandler.handleMessage"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->launch()Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->shouldShow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lnet/flixster/android/Flixster$LaunchAdHandler;->this$0:Lnet/flixster/android/Flixster;

    #calls: Lnet/flixster/android/Flixster;->removeSplashScreen()V
    invoke-static {v0}, Lnet/flixster/android/Flixster;->access$0(Lnet/flixster/android/Flixster;)V

    .line 572
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->launch()Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->shown()V

    .line 573
    new-instance v0, Lcom/flixster/android/ads/DfpLaunchInterstitial;

    invoke-direct {v0}, Lcom/flixster/android/ads/DfpLaunchInterstitial;-><init>()V

    iget-object v1, p0, Lnet/flixster/android/Flixster$LaunchAdHandler;->this$0:Lnet/flixster/android/Flixster;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/ads/DfpLaunchInterstitial;->loadAd(Landroid/app/Activity;Lnet/flixster/android/model/Movie;)V

    goto :goto_0
.end method
