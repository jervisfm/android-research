.class public Lnet/flixster/android/Flixster;
.super Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;
.source "Flixster.java"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;
.implements Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/Flixster$LaunchAdHandler;,
        Lnet/flixster/android/Flixster$RemoveMskPromptTask;,
        Lnet/flixster/android/Flixster$RemoveSplashTask;,
        Lnet/flixster/android/Flixster$StartActivityHandler;
    }
.end annotation


# static fields
.field private static final KEY_DL_THEATERS_TAB:Ljava/lang/String; = "KEY_DL_THEATERS"

.field private static final KEY_DL_WIDGET_LOGO:Ljava/lang/String; = "KEY_DL_LOGO"

.field public static final KEY_MOVIE_ID:Ljava/lang/String; = "net.flixster.android.EXTRA_MOVIE_ID"

.field public static final KEY_THEATER_ID:Ljava/lang/String; = "net.flixster.android.EXTRA_THEATER_ID"

.field public static final RATING_LARGE_R:[I = null

.field public static final RATING_SMALL_R:[I = null

.field public static final TAB_BOXOFFICE:Ljava/lang/String; = "boxoffice"

.field public static final TAB_DVD:Ljava/lang/String; = "dvd"

.field public static final TAB_HOME:Ljava/lang/String; = "home"

.field public static final TAB_MYMOVIES:Ljava/lang/String; = "mymovies"

.field public static final TAB_THEATERS:Ljava/lang/String; = "theaters"

.field public static final TAB_UPCOMING:Ljava/lang/String; = "upcoming"

.field private static instanceCount:I


# instance fields
.field private final launchAdHandler:Lnet/flixster/android/Flixster$LaunchAdHandler;

.field private mTabHost:Landroid/widget/TabHost;

.field private skipRatePrompt:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xd

    .line 64
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lnet/flixster/android/Flixster;->RATING_LARGE_R:[I

    .line 70
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lnet/flixster/android/Flixster;->RATING_SMALL_R:[I

    .line 76
    const/4 v0, 0x0

    sput v0, Lnet/flixster/android/Flixster;->instanceCount:I

    .line 53
    return-void

    .line 64
    :array_0
    .array-data 0x4
        0x9dt 0x1t 0x2t 0x7ft
        0x9et 0x1t 0x2t 0x7ft
        0x9ft 0x1t 0x2t 0x7ft
        0xa0t 0x1t 0x2t 0x7ft
        0xa1t 0x1t 0x2t 0x7ft
        0xa2t 0x1t 0x2t 0x7ft
        0xa3t 0x1t 0x2t 0x7ft
        0xa4t 0x1t 0x2t 0x7ft
        0xa5t 0x1t 0x2t 0x7ft
        0xa6t 0x1t 0x2t 0x7ft
        0xa7t 0x1t 0x2t 0x7ft
        0x6dt 0x1t 0x2t 0x7ft
        0x69t 0x1t 0x2t 0x7ft
    .end array-data

    .line 70
    :array_1
    .array-data 0x4
        0xa8t 0x1t 0x2t 0x7ft
        0xa9t 0x1t 0x2t 0x7ft
        0xaat 0x1t 0x2t 0x7ft
        0xabt 0x1t 0x2t 0x7ft
        0xact 0x1t 0x2t 0x7ft
        0xadt 0x1t 0x2t 0x7ft
        0xaet 0x1t 0x2t 0x7ft
        0xaft 0x1t 0x2t 0x7ft
        0xb0t 0x1t 0x2t 0x7ft
        0xb1t 0x1t 0x2t 0x7ft
        0xb2t 0x1t 0x2t 0x7ft
        0x6at 0x1t 0x2t 0x7ft
        0x66t 0x1t 0x2t 0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;-><init>()V

    .line 559
    new-instance v0, Lnet/flixster/android/Flixster$LaunchAdHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lnet/flixster/android/Flixster$LaunchAdHandler;-><init>(Lnet/flixster/android/Flixster;Lnet/flixster/android/Flixster$LaunchAdHandler;)V

    iput-object v0, p0, Lnet/flixster/android/Flixster;->launchAdHandler:Lnet/flixster/android/Flixster$LaunchAdHandler;

    .line 53
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/Flixster;)V
    .locals 0
    .parameter

    .prologue
    .line 470
    invoke-direct {p0}, Lnet/flixster/android/Flixster;->removeSplashScreen()V

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/Flixster;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 475
    invoke-direct {p0, p1}, Lnet/flixster/android/Flixster;->tryRemoveDialog(I)V

    return-void
.end method

.method private checkDeepLink()Ljava/util/Set;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 345
    .local v13, results:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/Flixster;->getIntent()Landroid/content/Intent;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v16

    .line 346
    .local v16, uri:Landroid/net/Uri;
    const-string v17, "FlxMain"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Flixster.checkDeepLink "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const/4 v5, 0x0

    .line 348
    .local v5, deepLink:Ljava/lang/String;
    const/4 v7, 0x1

    .line 349
    .local v7, isPossibleWidgetLogoEntrance:Z
    if-eqz v16, :cond_5

    .line 350
    const-string v17, "movie"

    invoke-virtual/range {v16 .. v17}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 351
    .local v10, movieDeepLink:Ljava/lang/String;
    const-string v17, "page"

    invoke-virtual/range {v16 .. v17}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 352
    const-string v17, "theater"

    invoke-virtual/range {v16 .. v17}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 353
    .local v15, theaterDeepLink:Ljava/lang/String;
    const-string v17, "postalcode"

    invoke-virtual/range {v16 .. v17}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 354
    .local v12, postalcode:Ljava/lang/String;
    const-string v17, "actor"

    invoke-virtual/range {v16 .. v17}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 355
    .local v2, actorDeepLink:Ljava/lang/String;
    const-string v17, "search"

    invoke-virtual/range {v16 .. v17}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 356
    .local v14, searchDeepLink:Ljava/lang/String;
    const/4 v11, 0x0

    .line 360
    .local v11, play_trailer:Z
    :try_start_0
    const-string v17, "FlxMain"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Flixster.onCreate() postalcode:"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    if-eqz v12, :cond_0

    .line 362
    invoke-static {v12}, Lnet/flixster/android/util/PostalCodeUtils;->parseZipcodeForShowtimes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 363
    .local v3, cleanZip:Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 364
    const-string v17, "FlxMain"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Flixster.onCreate() yay! cleanZip:"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    invoke-static {v3}, Lnet/flixster/android/data/LocationDao;->getLocations(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 366
    .local v9, locations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Location;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_0

    .line 367
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lnet/flixster/android/model/Location;

    .line 368
    .local v8, location:Lnet/flixster/android/model/Location;
    iget-wide v0, v8, Lnet/flixster/android/model/Location;->latitude:D

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Lnet/flixster/android/FlixsterApplication;->setUserLatitude(D)V

    .line 369
    iget-wide v0, v8, Lnet/flixster/android/model/Location;->longitude:D

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Lnet/flixster/android/FlixsterApplication;->setUserLongitude(D)V

    .line 370
    iget-object v0, v8, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lnet/flixster/android/FlixsterApplication;->setUserZip(Ljava/lang/String;)V

    .line 371
    iget-object v0, v8, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lnet/flixster/android/FlixsterApplication;->setUserCity(Ljava/lang/String;)V

    .line 372
    iget-object v0, v8, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lnet/flixster/android/FlixsterApplication;->setUserLocation(Ljava/lang/String;)V

    .line 373
    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Lnet/flixster/android/FlixsterApplication;->setUseLocationServiceFlag(Z)V

    .line 374
    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Lnet/flixster/android/FlixsterApplication;->setLocationPolicy(I)V

    .line 379
    .end local v3           #cleanZip:Ljava/lang/String;
    .end local v8           #location:Lnet/flixster/android/model/Location;
    .end local v9           #locations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Location;>;"
    :cond_0
    if-eqz v15, :cond_1

    .line 380
    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/Flixster;->removeSplashScreen()V

    .line 381
    new-instance v6, Landroid/content/Intent;

    const-class v17, Lnet/flixster/android/TheaterInfoPage;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 382
    .local v6, intent:Landroid/content/Intent;
    const-string v17, "net.flixster.android.EXTRA_THEATER_ID"

    invoke-static {v15}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 383
    new-instance v17, Lnet/flixster/android/Flixster$StartActivityHandler;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Lnet/flixster/android/Flixster$StartActivityHandler;-><init>(Lnet/flixster/android/Flixster;Landroid/content/Intent;)V

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lnet/flixster/android/Flixster$StartActivityHandler;->sendEmptyMessage(I)Z

    .line 386
    .end local v6           #intent:Landroid/content/Intent;
    :cond_1
    if-eqz v5, :cond_2

    const-string v17, "actor"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_2

    if-eqz v2, :cond_2

    .line 387
    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/Flixster;->removeSplashScreen()V

    .line 388
    new-instance v6, Landroid/content/Intent;

    const-class v17, Lnet/flixster/android/ActorPage;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 389
    .restart local v6       #intent:Landroid/content/Intent;
    const-string v17, "ACTOR_ID"

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 390
    new-instance v17, Lnet/flixster/android/Flixster$StartActivityHandler;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Lnet/flixster/android/Flixster$StartActivityHandler;-><init>(Lnet/flixster/android/Flixster;Landroid/content/Intent;)V

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lnet/flixster/android/Flixster$StartActivityHandler;->sendEmptyMessage(I)Z

    .line 393
    .end local v6           #intent:Landroid/content/Intent;
    :cond_2
    if-eqz v10, :cond_4

    .line 394
    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/Flixster;->removeSplashScreen()V

    .line 395
    new-instance v6, Landroid/content/Intent;

    const-class v17, Lnet/flixster/android/MovieDetails;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 396
    .restart local v6       #intent:Landroid/content/Intent;
    if-eqz v5, :cond_3

    const-string v17, "trailer"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 397
    const/4 v11, 0x1

    .line 399
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/Flixster;->getIntent()Landroid/content/Intent;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v0, v6}, Lcom/flixster/android/widget/WidgetProvider;->propagateIntentExtras(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 400
    const/4 v7, 0x0

    .line 401
    const-string v17, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 402
    const-string v17, "net.flixster.PLAY_TRAILER"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 403
    new-instance v17, Lnet/flixster/android/Flixster$StartActivityHandler;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Lnet/flixster/android/Flixster$StartActivityHandler;-><init>(Lnet/flixster/android/Flixster;Landroid/content/Intent;)V

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lnet/flixster/android/Flixster$StartActivityHandler;->sendEmptyMessage(I)Z

    .line 406
    .end local v6           #intent:Landroid/content/Intent;
    :cond_4
    if-eqz v14, :cond_5

    .line 407
    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/Flixster;->removeSplashScreen()V

    .line 408
    new-instance v6, Landroid/content/Intent;

    const-class v17, Lnet/flixster/android/SearchPage;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 409
    .restart local v6       #intent:Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/Flixster;->getIntent()Landroid/content/Intent;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v0, v6}, Lcom/flixster/android/widget/WidgetProvider;->propagateIntentExtras(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 410
    const/4 v7, 0x0

    .line 411
    new-instance v17, Lnet/flixster/android/Flixster$StartActivityHandler;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Lnet/flixster/android/Flixster$StartActivityHandler;-><init>(Lnet/flixster/android/Flixster;Landroid/content/Intent;)V

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lnet/flixster/android/Flixster$StartActivityHandler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    .end local v2           #actorDeepLink:Ljava/lang/String;
    .end local v6           #intent:Landroid/content/Intent;
    .end local v10           #movieDeepLink:Ljava/lang/String;
    .end local v11           #play_trailer:Z
    .end local v12           #postalcode:Ljava/lang/String;
    .end local v14           #searchDeepLink:Ljava/lang/String;
    .end local v15           #theaterDeepLink:Ljava/lang/String;
    :cond_5
    :goto_0
    if-eqz v5, :cond_6

    const-string v17, "theaters"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 419
    const-string v17, "KEY_DL_THEATERS"

    move-object/from16 v0, v17

    invoke-interface {v13, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 421
    :cond_6
    if-eqz v7, :cond_7

    .line 422
    const-string v17, "KEY_DL_LOGO"

    move-object/from16 v0, v17

    invoke-interface {v13, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 424
    :cond_7
    return-object v13

    .line 413
    .restart local v2       #actorDeepLink:Ljava/lang/String;
    .restart local v10       #movieDeepLink:Ljava/lang/String;
    .restart local v11       #play_trailer:Z
    .restart local v12       #postalcode:Ljava/lang/String;
    .restart local v14       #searchDeepLink:Ljava/lang/String;
    .restart local v15       #theaterDeepLink:Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 414
    .local v4, de:Lnet/flixster/android/data/DaoException;
    invoke-virtual {v4}, Lnet/flixster/android/data/DaoException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkFlixsterDeepLinks(Landroid/net/Uri;)V
    .locals 2
    .parameter "uri"

    .prologue
    .line 442
    invoke-static {p1}, Lcom/flixster/android/activity/DeepLink;->isFlixsterDeepLink(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    invoke-direct {p0}, Lnet/flixster/android/Flixster;->removeSplashScreen()V

    .line 444
    new-instance v0, Lnet/flixster/android/Flixster$1;

    invoke-direct {v0, p0, p1}, Lnet/flixster/android/Flixster$1;-><init>(Lnet/flixster/android/Flixster;Landroid/net/Uri;)V

    .line 448
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lnet/flixster/android/Flixster$1;->sendEmptyMessage(I)Z

    .line 450
    :cond_0
    return-void
.end method

.method public static getInstanceCount()I
    .locals 1

    .prologue
    .line 216
    sget v0, Lnet/flixster/android/Flixster;->instanceCount:I

    return v0
.end method

.method private removeSplashScreen()V
    .locals 1

    .prologue
    .line 471
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->splashRemoved()V

    .line 472
    const v0, 0x3b9ac9ff

    invoke-direct {p0, v0}, Lnet/flixster/android/Flixster;->tryRemoveDialog(I)V

    .line 473
    return-void
.end method

.method private setupTabsAndCheckLegacyDeepLinks()V
    .locals 15

    .prologue
    .line 221
    new-instance v3, Landroid/content/Intent;

    const-class v13, Lnet/flixster/android/BoxOfficePage;

    invoke-direct {v3, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 222
    .local v3, mBoxofficeIntent:Landroid/content/Intent;
    new-instance v6, Landroid/content/Intent;

    const-class v13, Lnet/flixster/android/MyMoviesPage;

    invoke-direct {v6, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 223
    .local v6, mMyMoviesIntent:Landroid/content/Intent;
    new-instance v4, Landroid/content/Intent;

    const-class v13, Lnet/flixster/android/DvdPage;

    invoke-direct {v4, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 224
    .local v4, mDvdIntent:Landroid/content/Intent;
    new-instance v7, Landroid/content/Intent;

    const-class v13, Lnet/flixster/android/TheaterListPage;

    invoke-direct {v7, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 225
    .local v7, mTheatersIntent:Landroid/content/Intent;
    new-instance v5, Landroid/content/Intent;

    const-class v13, Lnet/flixster/android/Homepage;

    invoke-direct {v5, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 227
    .local v5, mHomeIntent:Landroid/content/Intent;
    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 234
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v13, 0x7f030082

    const/4 v14, 0x0

    invoke-virtual {v1, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    .line 235
    .local v9, rl:Landroid/widget/RelativeLayout;
    const v13, 0x7f07025d

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 236
    .local v2, iv:Landroid/widget/ImageView;
    const v13, 0x7f0201bd

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 237
    const v13, 0x7f07025e

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 238
    .local v12, tv:Landroid/widget/TextView;
    const v13, 0x7f0c0114

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 240
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    const-string v14, "home"

    invoke-virtual {v13, v14}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    .line 241
    .local v11, ts:Landroid/widget/TabHost$TabSpec;
    invoke-virtual {v11, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    .line 242
    invoke-virtual {v11, v5}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    .line 243
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v13, v11}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 248
    const v13, 0x7f030082

    const/4 v14, 0x0

    invoke-virtual {v1, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .end local v9           #rl:Landroid/widget/RelativeLayout;
    check-cast v9, Landroid/widget/RelativeLayout;

    .line 249
    .restart local v9       #rl:Landroid/widget/RelativeLayout;
    const v13, 0x7f07025d

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2           #iv:Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 250
    .restart local v2       #iv:Landroid/widget/ImageView;
    const v13, 0x7f0201bb

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 251
    const v13, 0x7f07025e

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .end local v12           #tv:Landroid/widget/TextView;
    check-cast v12, Landroid/widget/TextView;

    .line 252
    .restart local v12       #tv:Landroid/widget/TextView;
    const v13, 0x7f0c010f

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 254
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    const-string v14, "boxoffice"

    invoke-virtual {v13, v14}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    .line 255
    invoke-virtual {v11, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    .line 256
    invoke-virtual {v11, v3}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    .line 257
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v13, v11}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 260
    const v13, 0x7f030082

    const/4 v14, 0x0

    invoke-virtual {v1, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .end local v9           #rl:Landroid/widget/RelativeLayout;
    check-cast v9, Landroid/widget/RelativeLayout;

    .line 261
    .restart local v9       #rl:Landroid/widget/RelativeLayout;
    const v13, 0x7f07025d

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2           #iv:Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 262
    .restart local v2       #iv:Landroid/widget/ImageView;
    const v13, 0x7f0201be

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 263
    const v13, 0x7f07025e

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .end local v12           #tv:Landroid/widget/TextView;
    check-cast v12, Landroid/widget/TextView;

    .line 264
    .restart local v12       #tv:Landroid/widget/TextView;
    const v13, 0x7f0c0113

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 266
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    const-string v14, "mymovies"

    invoke-virtual {v13, v14}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    .line 267
    invoke-virtual {v11, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    .line 268
    invoke-virtual {v11, v6}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    .line 269
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v13, v11}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 272
    const v13, 0x7f030082

    const/4 v14, 0x0

    invoke-virtual {v1, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .end local v9           #rl:Landroid/widget/RelativeLayout;
    check-cast v9, Landroid/widget/RelativeLayout;

    .line 273
    .restart local v9       #rl:Landroid/widget/RelativeLayout;
    const v13, 0x7f07025d

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2           #iv:Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 274
    .restart local v2       #iv:Landroid/widget/ImageView;
    const v13, 0x7f0201bf

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 275
    const v13, 0x7f07025e

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .end local v12           #tv:Landroid/widget/TextView;
    check-cast v12, Landroid/widget/TextView;

    .line 276
    .restart local v12       #tv:Landroid/widget/TextView;
    const v13, 0x7f0c0110

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 278
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    const-string v14, "theaters"

    invoke-virtual {v13, v14}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    .line 279
    invoke-virtual {v11, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    .line 280
    invoke-virtual {v11, v7}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    .line 281
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v13, v11}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 284
    const v13, 0x7f030083

    const/4 v14, 0x0

    invoke-virtual {v1, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .end local v9           #rl:Landroid/widget/RelativeLayout;
    check-cast v9, Landroid/widget/RelativeLayout;

    .line 285
    .restart local v9       #rl:Landroid/widget/RelativeLayout;
    const v13, 0x7f07025d

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2           #iv:Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 286
    .restart local v2       #iv:Landroid/widget/ImageView;
    const v13, 0x7f0201bc

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 287
    const v13, 0x7f07025e

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .end local v12           #tv:Landroid/widget/TextView;
    check-cast v12, Landroid/widget/TextView;

    .line 288
    .restart local v12       #tv:Landroid/widget/TextView;
    const v13, 0x7f0c0112

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 290
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    const-string v14, "dvd"

    invoke-virtual {v13, v14}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    .line 291
    invoke-virtual {v11, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    .line 292
    invoke-virtual {v11, v4}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    .line 293
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v13, v11}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 295
    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v13

    const v14, 0x7f09002d

    invoke-virtual {v13, v14}, Landroid/widget/TabWidget;->setBackgroundResource(I)V

    .line 297
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v13, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 299
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getLastTab()Ljava/lang/String;

    move-result-object v10

    .line 300
    .local v10, tabTag:Ljava/lang/String;
    invoke-direct {p0}, Lnet/flixster/android/Flixster;->checkDeepLink()Ljava/util/Set;

    move-result-object v0

    .line 301
    .local v0, deepLinks:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const-string v13, "KEY_DL_THEATERS"

    invoke-interface {v0, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 302
    const-string v10, "theaters"

    .line 304
    :cond_0
    const-string v13, "KEY_DL_LOGO"

    invoke-interface {v0, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 305
    const/4 v8, 0x0

    .line 306
    .local v8, propagatingIntent:Landroid/content/Intent;
    const-string v13, "boxoffice"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 307
    move-object v8, v3

    .line 317
    :cond_1
    :goto_0
    if-eqz v8, :cond_2

    .line 318
    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->getIntent()Landroid/content/Intent;

    move-result-object v13

    invoke-static {v13, v8}, Lcom/flixster/android/widget/WidgetProvider;->propagateIntentExtras(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 325
    .end local v8           #propagatingIntent:Landroid/content/Intent;
    :cond_2
    iget-object v13, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v13, v10}, Landroid/widget/TabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    .line 334
    return-void

    .line 308
    .restart local v8       #propagatingIntent:Landroid/content/Intent;
    :cond_3
    const-string v13, "theaters"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 309
    move-object v8, v7

    goto :goto_0

    .line 310
    :cond_4
    const-string v13, "mymovies"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 311
    move-object v8, v6

    goto :goto_0

    .line 312
    :cond_5
    const-string v13, "dvd"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 313
    move-object v8, v4

    goto :goto_0

    .line 314
    :cond_6
    const-string v13, "home"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 315
    move-object v8, v5

    goto :goto_0
.end method

.method private showDiagnosticsOrRateDialog()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 492
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->hasUserSessionExpired()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 493
    const v2, 0x3b9acd88

    invoke-virtual {p0, v2, v7}, Lnet/flixster/android/Flixster;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 541
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->isMskEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "mymovies"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getLastTab()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 495
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->hasMskPromptShown()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->isMskFinishedDeepLinkInitiated()Z

    move-result v2

    if-nez v2, :cond_2

    .line 496
    const v2, 0x3b9acde9

    new-instance v3, Lnet/flixster/android/Flixster$2;

    invoke-direct {v3, p0}, Lnet/flixster/android/Flixster$2;-><init>(Lnet/flixster/android/Flixster;)V

    invoke-virtual {p0, v2, v3}, Lnet/flixster/android/Flixster;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 519
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/msk/tooltip"

    const-string v4, "MSK Tooltip"

    const-string v5, "MskTooltip"

    const-string v6, "Impression"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-object v2, p0, Lnet/flixster/android/Flixster;->timerDecorater:Lcom/flixster/android/activity/decorator/TimerDecorator;

    new-instance v3, Lnet/flixster/android/Flixster$RemoveMskPromptTask;

    invoke-direct {v3, p0, v7}, Lnet/flixster/android/Flixster$RemoveMskPromptTask;-><init>(Lnet/flixster/android/Flixster;Lnet/flixster/android/Flixster$RemoveMskPromptTask;)V

    const-wide/16 v4, 0x1a90

    invoke-virtual {v2, v3, v4, v5}, Lcom/flixster/android/activity/decorator/TimerDecorator;->scheduleTask(Ljava/util/TimerTask;J)V

    goto :goto_0

    .line 521
    :cond_2
    invoke-static {}, Lnet/flixster/android/util/ErrorHandler;->instance()Lnet/flixster/android/util/ErrorHandler;

    move-result-object v2

    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lnet/flixster/android/util/ErrorHandler;->checkForAppCrash(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .local v0, intent:Landroid/content/Intent;
    if-eqz v0, :cond_3

    .line 522
    move-object v1, v0

    .line 523
    .local v1, intentz:Landroid/content/Intent;
    const/4 v2, 0x1

    iput-boolean v2, p0, Lnet/flixster/android/Flixster;->skipRatePrompt:Z

    .line 524
    const v2, 0x3b9acde8

    new-instance v3, Lnet/flixster/android/Flixster$3;

    invoke-direct {v3, p0, v1}, Lnet/flixster/android/Flixster$3;-><init>(Lnet/flixster/android/Flixster;Landroid/content/Intent;)V

    invoke-virtual {p0, v2, v3}, Lnet/flixster/android/Flixster;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 538
    .end local v1           #intentz:Landroid/content/Intent;
    :cond_3
    iget-boolean v2, p0, Lnet/flixster/android/Flixster;->skipRatePrompt:Z

    if-nez v2, :cond_0

    .line 539
    invoke-static {p0}, Lcom/flixster/android/utils/AppRater;->showRateDialog(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private showSplashScreen()V
    .locals 4

    .prologue
    .line 455
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->shouldShowSplash()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->splashShown()V

    .line 457
    const v0, 0x3b9ac9ff

    invoke-virtual {p0, v0}, Lnet/flixster/android/Flixster;->showDialog(I)V

    .line 458
    iget-object v0, p0, Lnet/flixster/android/Flixster;->timerDecorater:Lcom/flixster/android/activity/decorator/TimerDecorator;

    new-instance v1, Lnet/flixster/android/Flixster$RemoveSplashTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lnet/flixster/android/Flixster$RemoveSplashTask;-><init>(Lnet/flixster/android/Flixster;Lnet/flixster/android/Flixster$RemoveSplashTask;)V

    const-wide/16 v2, 0x708

    invoke-virtual {v0, v1, v2, v3}, Lcom/flixster/android/activity/decorator/TimerDecorator;->scheduleTask(Ljava/util/TimerTask;J)V

    .line 460
    :cond_0
    return-void
.end method

.method private tryRemoveDialog(I)V
    .locals 4
    .parameter "id"

    .prologue
    .line 477
    :try_start_0
    invoke-virtual {p0, p1}, Lnet/flixster/android/Flixster;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    :goto_0
    return-void

    .line 478
    :catch_0
    move-exception v0

    .line 479
    .local v0, viewNotAttachedToWindowManager:Ljava/lang/IllegalArgumentException;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Flixster.tryRemoveDialog id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public checkAndShowLaunchAd()V
    .locals 2

    .prologue
    .line 554
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->launch()Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->shouldShow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lnet/flixster/android/Flixster;->launchAdHandler:Lnet/flixster/android/Flixster$LaunchAdHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lnet/flixster/android/Flixster$LaunchAdHandler;->sendEmptyMessage(I)Z

    .line 557
    :cond_0
    return-void
.end method

.method public onAdsLoaded()V
    .locals 2

    .prologue
    .line 627
    const-string v0, "FlxAd"

    const-string v1, "Flixster.onAdsLoaded"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    sget v1, Lnet/flixster/android/Flixster;->instanceCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lnet/flixster/android/Flixster;->instanceCount:I

    .line 87
    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lnet/flixster/android/Flixster;->skipRatePrompt:Z

    .line 89
    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 90
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Flixster.onCreate intent:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Intent;->filterHashCode()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 91
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "null"

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-static {v2, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->createActionBar()V

    .line 96
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/flixster/android/activity/DeepLink;->checkMskFinished(Landroid/net/Uri;)V

    .line 99
    invoke-direct {p0}, Lnet/flixster/android/Flixster;->showDiagnosticsOrRateDialog()V

    .line 102
    invoke-direct {p0}, Lnet/flixster/android/Flixster;->showSplashScreen()V

    .line 105
    invoke-static {}, Lcom/flixster/android/bootstrap/Startup;->instance()Lcom/flixster/android/bootstrap/Startup;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/flixster/android/bootstrap/Startup;->onStartup(Landroid/app/Activity;)V

    .line 108
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lnet/flixster/android/ads/AdManager;->addDaoListener(Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;)V

    .line 111
    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->getTabHost()Landroid/widget/TabHost;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    .line 112
    invoke-direct {p0}, Lnet/flixster/android/Flixster;->setupTabsAndCheckLegacyDeepLinks()V

    .line 115
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lnet/flixster/android/Flixster;->checkFlixsterDeepLinks(Landroid/net/Uri;)V

    .line 118
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 119
    return-void

    .line 87
    .end local v0           #intent:Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 91
    .restart local v0       #intent:Landroid/content/Intent;
    :cond_1
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 599
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z

    .line 600
    const-string v0, "mymovies"

    iget-object v1, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v0

    if-nez v0, :cond_1

    .line 601
    :cond_0
    const v0, 0x7f070307

    invoke-interface {p1, v0}, Lcom/actionbarsherlock/view/Menu;->removeItem(I)V

    .line 603
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    .line 166
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->onDestroy()V

    .line 167
    const-string v2, "FlxMain"

    const-string v3, "Flixster.onDestroy"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    sget v2, Lnet/flixster/android/Flixster;->instanceCount:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Lnet/flixster/android/Flixster;->instanceCount:I

    .line 171
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v2

    invoke-virtual {v2, p0}, Lnet/flixster/android/ads/AdManager;->removeDaoListener(Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;)V

    .line 174
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->stopLocationRequest()V

    .line 177
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/ActivityHolder;->getTopLevelActivity()Landroid/app/Activity;

    move-result-object v0

    .line 178
    .local v0, activity:Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 179
    const-string v2, "FlxMain"

    const-string v3, "ActivityHolder sanity check passed"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :goto_0
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/ObjectHolder;->size()I

    move-result v1

    .line 185
    .local v1, size:I
    if-nez v1, :cond_1

    .line 186
    const-string v2, "FlxMain"

    const-string v3, "ObjectHolder sanity check passed"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :goto_1
    return-void

    .line 181
    .end local v1           #size:I
    :cond_0
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ActivityHolder sanity check failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/ActivityHolder;->clear()V

    goto :goto_0

    .line 188
    .restart local v1       #size:I
    :cond_1
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ObjectHolder sanity check failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/ObjectHolder;->clear()V

    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keycode"
    .parameter "event"

    .prologue
    .line 582
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 583
    const-string v0, "FlxMain"

    const-string v1, "Flixster.onKeyDown - call finish"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->finish()V

    .line 586
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .parameter "intent"

    .prologue
    .line 200
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 201
    const-string v1, "FlxMain"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Flixster.onNewIntent intent:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->filterHashCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 202
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 201
    invoke-static {v1, v0}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0, p1}, Lnet/flixster/android/Flixster;->setIntent(Landroid/content/Intent;)V

    .line 206
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/activity/DeepLink;->checkMskFinished(Landroid/net/Uri;)V

    .line 209
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lnet/flixster/android/Flixster;->checkFlixsterDeepLinks(Landroid/net/Uri;)V

    .line 212
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 213
    return-void

    .line 202
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 5
    .parameter "item"

    .prologue
    const/4 v2, 0x1

    .line 608
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 620
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v2

    :cond_0
    :goto_0
    return v2

    .line 610
    :pswitch_0
    const-string v3, "mymovies"

    iget-object v4, p0, Lnet/flixster/android/Flixster;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v4}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 611
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v1

    .line 612
    .local v1, user:Lnet/flixster/android/model/User;
    invoke-virtual {v1}, Lnet/flixster/android/model/User;->resetLockerRights()V

    .line 613
    iput-boolean v2, v1, Lnet/flixster/android/model/User;->refreshRequired:Z

    .line 614
    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->getLocalActivityManager()Landroid/app/LocalActivityManager;

    move-result-object v3

    const-string v4, "mymovies"

    invoke-virtual {v3, v4}, Landroid/app/LocalActivityManager;->getActivity(Ljava/lang/String;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/MyMoviesPage;

    .line 615
    .local v0, myMoviesTab:Lnet/flixster/android/MyMoviesPage;
    invoke-virtual {v0}, Lnet/flixster/android/MyMoviesPage;->onResume()V

    goto :goto_0

    .line 608
    :pswitch_data_0
    .packed-switch 0x7f070307
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 153
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->onPause()V

    .line 154
    const-string v0, "FlxMain"

    const-string v1, "Flixster.onPause"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-direct {p0}, Lnet/flixster/android/Flixster;->removeSplashScreen()V

    .line 156
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 130
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->onResume()V

    .line 131
    const-string v0, "FlxMain"

    const-string v1, "Flixster.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-direct {p0}, Lnet/flixster/android/Flixster;->showSplashScreen()V

    .line 137
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sNetflixStartupValidate:Z

    if-nez v0, :cond_0

    .line 138
    const/4 v0, 0x1

    sput-boolean v0, Lnet/flixster/android/FlixsterApplication;->sNetflixStartupValidate:Z

    .line 140
    :try_start_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    const-string v0, "12"

    invoke-static {v0}, Lnet/flixster/android/data/NetflixDao;->fetchTitleState(Ljava/lang/String;)Lnet/flixster/android/model/NetflixTitleState;

    .line 142
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/startupvalidate"

    const-string v2, "User token valid"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lnet/flixster/android/Flixster;->skipRatePrompt:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->onStart()V

    .line 124
    const-string v0, "FlxMain"

    const-string v1, "Flixster.onStart"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 126
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 160
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockTabActivity;->onStop()V

    .line 161
    const-string v0, "FlxMain"

    const-string v1, "Flixster.onStop"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 3
    .parameter "tabId"

    .prologue
    .line 590
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Flixster.onTabChanged:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    invoke-static {p1}, Lnet/flixster/android/FlixsterApplication;->setLastTab(Ljava/lang/String;)V

    .line 592
    invoke-virtual {p0}, Lnet/flixster/android/Flixster;->invalidateOptionsMenu()V

    .line 593
    return-void
.end method
