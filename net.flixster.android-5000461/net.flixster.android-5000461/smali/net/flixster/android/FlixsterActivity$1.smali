.class Lnet/flixster/android/FlixsterActivity$1;
.super Landroid/os/Handler;
.source "FlixsterActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FlixsterActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FlixsterActivity;


# direct methods
.method constructor <init>(Lnet/flixster/android/FlixsterActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FlixsterActivity$1;->this$0:Lnet/flixster/android/FlixsterActivity;

    .line 112
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 116
    iget-object v1, p0, Lnet/flixster/android/FlixsterActivity$1;->this$0:Lnet/flixster/android/FlixsterActivity;

    invoke-virtual {v1}, Lnet/flixster/android/FlixsterActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 118
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/FlixsterActivity$1;->this$0:Lnet/flixster/android/FlixsterActivity;

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Lnet/flixster/android/FlixsterActivity;->showDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, didNotCreateADialog:Ljava/lang/IllegalArgumentException;
    const-string v1, "FlxMain"

    const-string v2, "FlixsterActivity.mShowDialogHandler"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
