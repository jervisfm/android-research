.class Lnet/flixster/android/FlixsterActivity$3;
.super Ljava/lang/Object;
.source "FlixsterActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/FlixsterActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FlixsterActivity;


# direct methods
.method constructor <init>(Lnet/flixster/android/FlixsterActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FlixsterActivity$3;->this$0:Lnet/flixster/android/FlixsterActivity;

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 149
    const-string v1, "Flixster"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ShowtimesList DIALOG_DATE_SELECT which:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 151
    .local v0, date:Ljava/util/Calendar;
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->add(II)V

    .line 152
    const-string v1, "Flixster"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ShowtimesList DIALOG_DATE_SELECT date:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setShowtimesDate(Ljava/util/Date;)V

    .line 154
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/showtimes/selectDate"

    const-string v3, "Select Date"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v1, p0, Lnet/flixster/android/FlixsterActivity$3;->this$0:Lnet/flixster/android/FlixsterActivity;

    invoke-virtual {v1}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 156
    iget-object v1, p0, Lnet/flixster/android/FlixsterActivity$3;->this$0:Lnet/flixster/android/FlixsterActivity;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lnet/flixster/android/FlixsterActivity;->removeDialog(I)V

    .line 157
    return-void
.end method
