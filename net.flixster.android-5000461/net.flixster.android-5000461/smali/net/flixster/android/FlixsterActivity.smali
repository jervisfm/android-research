.class public Lnet/flixster/android/FlixsterActivity;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "FlixsterActivity.java"

# interfaces
.implements Lcom/flixster/android/utils/ImageTask;


# static fields
.field protected static final ACTIVITY_RESULT_EMAIL:I = 0x0

.field protected static final ACTIVITY_RESULT_INFO:I = 0x1

.field public static final DIALOGKEY_DATESELECT:I = 0x6

.field public static final DIALOGKEY_NETWORKERROR:I = 0x2

.field public static final DIALOGKEY_SORTSELECT:I = 0x5

.field protected static final KEY_PLATFORM_ID:Ljava/lang/String; = "PLATFORM_ID"

.field protected static final LISTSTATE_POSITION:Ljava/lang/String; = "LISTSTATE_POSITION"

.field protected static final SWIPE_MAX_OFF_PATH:I = 0xfa

.field protected static final SWIPE_MIN_DISTANCE:I = 0x78

.field protected static final SWIPE_THRESHOLD_VELOCITY:I = 0xc8


# instance fields
.field protected final className:Ljava/lang/String;

.field private mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

.field private mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

.field protected mLoadingDialog:Landroid/app/Dialog;

.field protected mPageTimer:Ljava/util/Timer;

.field protected final mRemoveDialogHandler:Landroid/os/Handler;

.field protected mRetryCount:I

.field protected final mShowDialogHandler:Landroid/os/Handler;

.field private mStartSettingsClickListener:Landroid/view/View$OnClickListener;

.field protected mStickyBottomAd:Lnet/flixster/android/ads/AdView;

.field protected mStickyTopAd:Lnet/flixster/android/ads/AdView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 46
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivity;->className:Ljava/lang/String;

    .line 48
    iput-object v1, p0, Lnet/flixster/android/FlixsterActivity;->mLoadingDialog:Landroid/app/Dialog;

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/FlixsterActivity;->mRetryCount:I

    .line 112
    new-instance v0, Lnet/flixster/android/FlixsterActivity$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterActivity$1;-><init>(Lnet/flixster/android/FlixsterActivity;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mShowDialogHandler:Landroid/os/Handler;

    .line 126
    new-instance v0, Lnet/flixster/android/FlixsterActivity$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterActivity$2;-><init>(Lnet/flixster/android/FlixsterActivity;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mRemoveDialogHandler:Landroid/os/Handler;

    .line 201
    iput-object v1, p0, Lnet/flixster/android/FlixsterActivity;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

    .line 218
    iput-object v1, p0, Lnet/flixster/android/FlixsterActivity;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;

    .line 33
    return-void
.end method


# virtual methods
.method protected declared-synchronized getDateSelectOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Lnet/flixster/android/FlixsterActivity$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterActivity$5;-><init>(Lnet/flixster/android/FlixsterActivity;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

    .line 215
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getResourceString(I)Ljava/lang/String;
    .locals 1
    .parameter "resourceId"

    .prologue
    .line 234
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getStartSettingsClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 222
    new-instance v0, Lnet/flixster/android/FlixsterActivity$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterActivity$6;-><init>(Lnet/flixster/android/FlixsterActivity;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;

    .line 230
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-static {p0}, Lnet/flixster/android/FlixsterApplication;->setCurrentDimensions(Landroid/app/Activity;)V

    .line 69
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "id"

    .prologue
    .line 142
    sparse-switch p1, :sswitch_data_0

    .line 169
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 144
    :sswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c012f

    invoke-virtual {p0, v1}, Lnet/flixster/android/FlixsterActivity;->getResourceString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 145
    invoke-static {p0}, Lnet/flixster/android/util/DialogUtils;->getShowtimesDateOptions(Landroid/content/Context;)[Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lnet/flixster/android/FlixsterActivity$3;

    invoke-direct {v2, p0}, Lnet/flixster/android/FlixsterActivity$3;-><init>(Lnet/flixster/android/FlixsterActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 160
    :sswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 161
    const-string v1, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 162
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 163
    const-string v1, "Retry"

    new-instance v2, Lnet/flixster/android/FlixsterActivity$4;

    invoke-direct {v2, p0}, Lnet/flixster/android/FlixsterActivity$4;-><init>(Lnet/flixster/android/FlixsterActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 167
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 142
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x6 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 106
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 109
    :cond_1
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onDestroy()V

    .line 110
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onLowMemory()V

    .line 61
    const-string v0, "FlxMain"

    const-string v1, "FlixsterActivity.onLowMemory"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->clearBitmapCache()V

    .line 63
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onPause()V

    .line 89
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mPageTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 91
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 92
    iput-object v1, p0, Lnet/flixster/android/FlixsterActivity;->mPageTimer:Ljava/util/Timer;

    .line 95
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v0}, Lcom/flixster/android/utils/ImageTaskImpl;->stopTask()V

    .line 97
    iput-object v1, p0, Lnet/flixster/android/FlixsterActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    .line 99
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 73
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 75
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mPageTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mPageTimer:Ljava/util/Timer;

    .line 78
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-nez v0, :cond_1

    .line 79
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lnet/flixster/android/FlixsterActivity;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onResume start ImageTask"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    new-instance v0, Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-direct {v0}, Lcom/flixster/android/utils/ImageTaskImpl;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    .line 81
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v0}, Lcom/flixster/android/utils/ImageTaskImpl;->startTask()V

    .line 83
    :cond_1
    return-void
.end method

.method public orderImage(Lnet/flixster/android/model/ImageOrder;)V
    .locals 0
    .parameter "io"

    .prologue
    .line 184
    invoke-virtual {p0, p1}, Lnet/flixster/android/FlixsterActivity;->orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 185
    return-void
.end method

.method public orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V
    .locals 1
    .parameter "io"

    .prologue
    .line 196
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v0, p1}, Lcom/flixster/android/utils/ImageTaskImpl;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 199
    :cond_0
    return-void
.end method

.method public orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V
    .locals 1
    .parameter "io"

    .prologue
    .line 189
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v0, p1}, Lcom/flixster/android/utils/ImageTaskImpl;->orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 192
    :cond_0
    return-void
.end method

.method public refreshSticky()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 173
    invoke-static {p0}, Lnet/flixster/android/FlixsterApplication;->isLandscape(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0, v1}, Lnet/flixster/android/ads/AdView;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0, v1}, Lnet/flixster/android/ads/AdView;->setVisibility(I)V

    .line 180
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 178
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    goto :goto_0
.end method

.method protected retryAction()V
    .locals 0

    .prologue
    .line 254
    return-void
.end method

.method protected retryLogic(Lnet/flixster/android/data/DaoException;)V
    .locals 3
    .parameter "de"

    .prologue
    const/4 v2, 0x0

    .line 239
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    iput v2, p0, Lnet/flixster/android/FlixsterActivity;->mRetryCount:I

    .line 241
    invoke-static {p1, p0}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedSherlockActivity;)V

    .line 249
    :goto_0
    return-void

    .line 242
    :cond_0
    iget v0, p0, Lnet/flixster/android/FlixsterActivity;->mRetryCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 243
    iget v0, p0, Lnet/flixster/android/FlixsterActivity;->mRetryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lnet/flixster/android/FlixsterActivity;->mRetryCount:I

    .line 244
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterActivity;->retryAction()V

    goto :goto_0

    .line 246
    :cond_1
    iput v2, p0, Lnet/flixster/android/FlixsterActivity;->mRetryCount:I

    .line 247
    invoke-static {p1, p0}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedSherlockActivity;)V

    goto :goto_0
.end method

.method protected shouldSkipBackgroundTask(I)Z
    .locals 3
    .parameter "currResumeCtr"

    .prologue
    .line 276
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivity;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    invoke-virtual {v0}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->isPausing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 277
    :cond_0
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lnet/flixster/android/FlixsterActivity;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".shouldSkipBackgroundTask #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x1

    .line 280
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
