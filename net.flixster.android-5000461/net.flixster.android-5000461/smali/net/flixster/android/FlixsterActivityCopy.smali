.class public Lnet/flixster/android/FlixsterActivityCopy;
.super Lcom/flixster/android/activity/TabbedActivity;
.source "FlixsterActivityCopy.java"

# interfaces
.implements Lcom/flixster/android/utils/ImageTask;


# static fields
.field protected static final ACTIVITY_RESULT_EMAIL:I = 0x0

.field protected static final ACTIVITY_RESULT_INFO:I = 0x1

.field public static final DIALOGKEY_DATESELECT:I = 0x6

.field public static final DIALOGKEY_NETWORKERROR:I = 0x2

.field public static final DIALOGKEY_SORTSELECT:I = 0x5

.field protected static final KEY_PLATFORM_ID:Ljava/lang/String; = "PLATFORM_ID"

.field protected static final LISTSTATE_POSITION:Ljava/lang/String; = "LISTSTATE_POSITION"

.field protected static final SWIPE_MAX_OFF_PATH:I = 0xfa

.field protected static final SWIPE_MIN_DISTANCE:I = 0x78

.field protected static final SWIPE_THRESHOLD_VELOCITY:I = 0xc8


# instance fields
.field private mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

.field private mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

.field protected mLoadingDialog:Landroid/app/Dialog;

.field protected mPageTimer:Ljava/util/Timer;

.field protected final mRemoveDialogHandler:Landroid/os/Handler;

.field protected mRetryCount:I

.field protected final mShowDialogHandler:Landroid/os/Handler;

.field private mStartSettingsClickListener:Landroid/view/View$OnClickListener;

.field protected mStickyBottomAd:Lnet/flixster/android/ads/AdView;

.field protected mStickyTopAd:Lnet/flixster/android/ads/AdView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Lcom/flixster/android/activity/TabbedActivity;-><init>()V

    .line 50
    iput-object v1, p0, Lnet/flixster/android/FlixsterActivityCopy;->mLoadingDialog:Landroid/app/Dialog;

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mRetryCount:I

    .line 113
    new-instance v0, Lnet/flixster/android/FlixsterActivityCopy$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterActivityCopy$1;-><init>(Lnet/flixster/android/FlixsterActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mShowDialogHandler:Landroid/os/Handler;

    .line 127
    new-instance v0, Lnet/flixster/android/FlixsterActivityCopy$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterActivityCopy$2;-><init>(Lnet/flixster/android/FlixsterActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mRemoveDialogHandler:Landroid/os/Handler;

    .line 202
    iput-object v1, p0, Lnet/flixster/android/FlixsterActivityCopy;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

    .line 219
    iput-object v1, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;

    .line 37
    return-void
.end method


# virtual methods
.method protected declared-synchronized getDateSelectOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 207
    new-instance v0, Lnet/flixster/android/FlixsterActivityCopy$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterActivityCopy$5;-><init>(Lnet/flixster/android/FlixsterActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;

    .line 216
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mDateSelectOnClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getFeaturedMovies()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 272
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getResourceString(I)Ljava/lang/String;
    .locals 1
    .parameter "resourceId"

    .prologue
    .line 235
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterActivityCopy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getStartSettingsClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 222
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lnet/flixster/android/FlixsterActivityCopy$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterActivityCopy$6;-><init>(Lnet/flixster/android/FlixsterActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;

    .line 231
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStartSettingsClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/flixster/android/activity/TabbedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-static {p0}, Lnet/flixster/android/FlixsterApplication;->setCurrentDimensions(Landroid/app/Activity;)V

    .line 71
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "id"

    .prologue
    .line 143
    sparse-switch p1, :sswitch_data_0

    .line 170
    invoke-super {p0, p1}, Lcom/flixster/android/activity/TabbedActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 145
    :sswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c012f

    invoke-virtual {p0, v1}, Lnet/flixster/android/FlixsterActivityCopy;->getResourceString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 146
    invoke-static {p0}, Lnet/flixster/android/util/DialogUtils;->getShowtimesDateOptions(Landroid/content/Context;)[Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lnet/flixster/android/FlixsterActivityCopy$3;

    invoke-direct {v2, p0}, Lnet/flixster/android/FlixsterActivityCopy$3;-><init>(Lnet/flixster/android/FlixsterActivityCopy;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 161
    :sswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 162
    const-string v1, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 163
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 164
    const-string v1, "Retry"

    new-instance v2, Lnet/flixster/android/FlixsterActivityCopy$4;

    invoke-direct {v2, p0}, Lnet/flixster/android/FlixsterActivityCopy$4;-><init>(Lnet/flixster/android/FlixsterActivityCopy;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 168
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterActivityCopy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 143
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x6 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 107
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 110
    :cond_1
    invoke-super {p0}, Lcom/flixster/android/activity/TabbedActivity;->onDestroy()V

    .line 111
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/flixster/android/activity/TabbedActivity;->onLowMemory()V

    .line 63
    const-string v0, "FlxMain"

    const-string v1, "FlixsterActivity.onLowMemory"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->clearBitmapCache()V

    .line 65
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-super {p0}, Lcom/flixster/android/activity/TabbedActivity;->onPause()V

    .line 90
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mPageTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 92
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 93
    iput-object v1, p0, Lnet/flixster/android/FlixsterActivityCopy;->mPageTimer:Ljava/util/Timer;

    .line 96
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v0}, Lcom/flixster/android/utils/ImageTaskImpl;->stopTask()V

    .line 98
    iput-object v1, p0, Lnet/flixster/android/FlixsterActivityCopy;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    .line 100
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 75
    invoke-super {p0}, Lcom/flixster/android/activity/TabbedActivity;->onResume()V

    .line 76
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mPageTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mPageTimer:Ljava/util/Timer;

    .line 79
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-nez v0, :cond_1

    .line 80
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lnet/flixster/android/FlixsterActivityCopy;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onResume start ImageTask"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    new-instance v0, Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-direct {v0}, Lcom/flixster/android/utils/ImageTaskImpl;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    .line 82
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v0}, Lcom/flixster/android/utils/ImageTaskImpl;->startTask()V

    .line 84
    :cond_1
    return-void
.end method

.method public orderImage(Lnet/flixster/android/model/ImageOrder;)V
    .locals 0
    .parameter "io"

    .prologue
    .line 185
    invoke-virtual {p0, p1}, Lnet/flixster/android/FlixsterActivityCopy;->orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 186
    return-void
.end method

.method public orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V
    .locals 1
    .parameter "io"

    .prologue
    .line 197
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v0, p1}, Lcom/flixster/android/utils/ImageTaskImpl;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 200
    :cond_0
    return-void
.end method

.method public orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V
    .locals 1
    .parameter "io"

    .prologue
    .line 190
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v0, p1}, Lcom/flixster/android/utils/ImageTaskImpl;->orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 193
    :cond_0
    return-void
.end method

.method public refreshSticky()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 174
    invoke-static {p0}, Lnet/flixster/android/FlixsterApplication;->isLandscape(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0, v1}, Lnet/flixster/android/ads/AdView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0, v1}, Lnet/flixster/android/ads/AdView;->setVisibility(I)V

    .line 181
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 179
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    goto :goto_0
.end method

.method protected retryAction()V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method protected retryLogic(Lnet/flixster/android/data/DaoException;)V
    .locals 3
    .parameter "de"

    .prologue
    const/4 v2, 0x0

    .line 240
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    iput v2, p0, Lnet/flixster/android/FlixsterActivityCopy;->mRetryCount:I

    .line 242
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-static {p1, p0, v0}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;Lcom/flixster/android/activity/decorator/DialogDecorator;)V

    .line 250
    :goto_0
    return-void

    .line 243
    :cond_0
    iget v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mRetryCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 244
    iget v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mRetryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->mRetryCount:I

    .line 245
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterActivityCopy;->retryAction()V

    goto :goto_0

    .line 247
    :cond_1
    iput v2, p0, Lnet/flixster/android/FlixsterActivityCopy;->mRetryCount:I

    .line 248
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-static {p1, p0, v0}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;Lcom/flixster/android/activity/decorator/DialogDecorator;)V

    goto :goto_0
.end method

.method protected shouldSkipBackgroundTask(I)Z
    .locals 3
    .parameter "currResumeCtr"

    .prologue
    .line 277
    iget-object v0, p0, Lnet/flixster/android/FlixsterActivityCopy;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    invoke-virtual {v0}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->isPausing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 278
    :cond_0
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lnet/flixster/android/FlixsterActivityCopy;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".shouldSkipBackgroundTask #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const/4 v0, 0x1

    .line 281
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected trackPage()V
    .locals 8

    .prologue
    .line 260
    invoke-super {p0}, Lcom/flixster/android/activity/TabbedActivity;->trackPage()V

    .line 261
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterActivityCopy;->getFeaturedMovies()Ljava/util/List;

    move-result-object v0

    .line 262
    .local v0, featuredMovies:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 263
    invoke-static {v0}, Lcom/flixster/android/utils/ListHelper;->copy(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 264
    .local v1, featuredMoviesCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 268
    .end local v1           #featuredMoviesCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    :cond_0
    return-void

    .line 264
    .restart local v1       #featuredMoviesCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Movie;

    .line 265
    .local v2, movie:Lnet/flixster/android/model/Movie;
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v4

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFeaturedId()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const-string v6, "Impression"

    const-string v7, ""

    invoke-virtual {v4, v5, v6, v7}, Lnet/flixster/android/ads/AdManager;->trackEvent(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
