.class public Lnet/flixster/android/FlixsterApplication$UserLocationListener;
.super Ljava/lang/Object;
.source "FlixsterApplication.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FlixsterApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserLocationListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 4
    .parameter "location"

    .prologue
    const/4 v3, 0x1

    .line 579
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterApplication.LocationListener.onLocationChanged "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    invoke-static {p1}, Lnet/flixster/android/FlixsterApplication;->access$0(Landroid/location/Location;)V

    .line 581
    invoke-static {v3}, Lnet/flixster/android/FlixsterApplication;->access$1(Z)V

    .line 582
    invoke-static {v3}, Lnet/flixster/android/FlixsterApplication;->access$2(Z)V

    .line 583
    invoke-static {v3}, Lnet/flixster/android/FlixsterApplication;->access$3(Z)V

    .line 584
    invoke-static {v3}, Lnet/flixster/android/FlixsterApplication;->access$4(Z)V

    .line 585
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .parameter "provider"

    .prologue
    .line 588
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .parameter "provider"

    .prologue
    .line 591
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    .prologue
    .line 596
    return-void
.end method
