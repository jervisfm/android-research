.class public Lnet/flixster/android/FlixsterApplication;
.super Landroid/app/Application;
.source "FlixsterApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/FlixsterApplication$UserLocationListener;
    }
.end annotation


# static fields
.field public static final DISTANCE_TYPE_KM:I = 0x1

.field public static final DISTANCE_TYPE_MILE:I = 0x0

.field public static final KMPM:F = 1.609f

.field public static final LOCATION_POLICY_NETWORK_GPS:I = 0x2

.field public static final LOCATION_POLICY_NETWORK_ONLY:I = 0x1

.field public static final LOCATION_POLICY_OFF:I = 0x0

.field private static final MIN_LOCATION_DISTANCE_CHANGE_M:J = 0x7d0L

.field private static final MIN_LOCATION_REQUEST_TIME:J = 0x124f80L

.field public static final PLATFORM_FACEBOOK:Ljava/lang/String; = "FBK"

.field public static final PLATFORM_FLIXSTER:Ljava/lang/String; = "FLX"

.field private static final PREFERENCE_THEATER_DISTANCE:Ljava/lang/String; = "PREFERENCE_THEATER_DISTANCE"

.field private static final PREFS_ADADMIN_DISABLE_LAUNCH_CAP:Ljava/lang/String; = "PREFS_ADADMIN_DISABLE_LAUNCH_CAP"

.field private static final PREFS_ADADMIN_DISABLE_POSTITIAL_CAP:Ljava/lang/String; = "PREFS_ADADMIN_DISABLE_POSTITIAL_CAP"

.field private static final PREFS_ADADMIN_DISABLE_PREROLL_CAP:Ljava/lang/String; = "PREFS_ADADMIN_DISABLE_PREROLL_CAP"

.field private static final PREFS_ADADMIN_DOUBLECLICK_TEST:Ljava/lang/String; = "PREFS_ADADMIN_DOUBLECLICK_TEST"

.field private static final PREFS_ADMIN_APISOURCE:Ljava/lang/String; = "PREFS_ADMIN_APISOURCE"

.field private static final PREFS_ADMIN_STATE:Ljava/lang/String; = "PREFS_ADMIN_STATE"

.field private static final PREFS_CACHEPOLICY:Ljava/lang/String; = "PREFS_CACHEPOLICY"

.field private static final PREFS_CAPTIONSENABLED:Ljava/lang/String; = "PREFS_CAPTIONSENABLED"

.field private static final PREFS_DIAGNOSTIC_MODE:Ljava/lang/String; = "PREFS_DIAGNOSTIC_MODE"

.field private static final PREFS_FAVORITETHEATERS:Ljava/lang/String; = "PREFS_FAVORITETHEATERS"

.field private static final PREFS_FB_USERID:Ljava/lang/String; = "PREFS_FB_USERID"

.field private static final PREFS_FB_USERNAME:Ljava/lang/String; = "PREFS_FB_USERNAME"

.field private static final PREFS_FLIXSTER_ID:Ljava/lang/String; = "PREFS_FLIXSTER_ID"

.field private static final PREFS_FLIXSTER_SESSION_KEY:Ljava/lang/String; = "PREFS_FLIXSTER_SESSION_KEY"

.field private static final PREFS_FLIXSTER_USERNAME:Ljava/lang/String; = "PREFS_FLIXSTER_USERNAME"

.field private static final PREFS_FLX_HASH:Ljava/lang/String; = "PREFS_FLX_HASH"

.field private static final PREFS_GAFIRSTSESSION:Ljava/lang/String; = "PREFS_GAFIRSTSESSION"

.field private static final PREFS_GALASTSESSION:Ljava/lang/String; = "PREFS_GALASTSESSION"

.field private static final PREFS_GATHISSESSION:Ljava/lang/String; = "PREFS_GATHISSESSION"

.field public static final PREFS_INSTALLMSPOSIXTIME:Ljava/lang/String; = "PREFS_INSTALLMSPOSIXTIME"

.field private static final PREFS_INSTALL_REFERRER:Ljava/lang/String; = "PREFS_INSTALL_REFERRER"

.field private static final PREFS_LASTVERSION:Ljava/lang/String; = "PREFS_LASTVERSION"

.field public static final PREFS_LAST_TAB:Ljava/lang/String; = "PREFS_LAST_TAB"

.field private static final PREFS_LOCATIONPOLICY:Ljava/lang/String; = "PREFS_LOCATIONPOLICY"

.field private static final PREFS_MIGRATION_RIGHTS:Ljava/lang/String; = "PREFS_MIGRATION_RIGHTS"

.field private static final PREFS_MOVIERATINGOPTION:Ljava/lang/String; = "PREFS_MOVIERATINGOPTION"

.field private static final PREFS_MSK_PROMPT:Ljava/lang/String; = "PREFS_MSK_PROMPT_4.1"

.field private static final PREFS_NAME:Ljava/lang/String; = "FlixsterPrefs"

.field public static final PREFS_NETFLIX_OAUTH_TOKEN:Ljava/lang/String; = "PREFS_NETFLIX_OAUTH_TOKEN"

.field public static final PREFS_NETFLIX_OAUTH_TOKEN_SECRET:Ljava/lang/String; = "PREFS_NETFLIX_OAUTH_TOKEN_SECRET"

.field public static final PREFS_NETFLIX_USER_ID:Ljava/lang/String; = "PREFS_NETFLIX_USER_ID"

.field private static final PREFS_PLATFORM:Ljava/lang/String; = "PREFS_PLATFORM"

.field private static final PREFS_SESSIONCOUNT:Ljava/lang/String; = "PREFS_SESSIONCOUNT"

.field private static final PREFS_STREAM_DELETE_ERROR:Ljava/lang/String; = "PREFS_STREAM_DELETE_ERROR"

.field private static final PREFS_STREAM_STOP_ERROR:Ljava/lang/String; = "PREFS_STREAM_STOP_ERROR"

.field public static final PREFS_TICKET_EMAIL:Ljava/lang/String; = "TICKET_EMAIL"

.field private static final PREFS_TOS_ACCEPT:Ljava/lang/String; = "PREFS_TOS_ACCEPT_3.7.2"

.field private static final PREFS_USEAUTOLOCATE:Ljava/lang/String; = "PREFS_USEAUTOLOCATE"

.field private static final PREFS_USER_CITY:Ljava/lang/String; = "PREFS_USER_CITY"

.field private static final PREFS_USER_LATITUDE:Ljava/lang/String; = "PREFS_LATITUDE"

.field private static final PREFS_USER_LOCATION:Ljava/lang/String; = "PREFS_MANUALZIPCODE"

.field private static final PREFS_USER_LONGIITUDE:Ljava/lang/String; = "PREFS_LONGITUDE"

.field private static final PREFS_USER_ZIP:Ljava/lang/String; = "PREFS_USER_ZIP"

.field private static final PROPS_IS_MSK_ENABLED:Ljava/lang/String; = "PROPS_IS_MSK_ENABLED"

.field private static final PROPS_MSK_ANON_PROMO_URL:Ljava/lang/String; = "PROPS_MSK_ANON_PROMO_URL"

.field private static final PROPS_MSK_TOOLTIP_URL:Ljava/lang/String; = "PROPS_MSK_TOOLTIP_URL"

.field private static final PROPS_MSK_USER_PROMO_URL:Ljava/lang/String; = "PROPS_MSK_USER_PROMO_URL"

.field private static final THEATER_DISTANCE_DEFAULT:I = 0x19

.field private static connectivityManager:Landroid/net/ConnectivityManager;

.field private static flixsterId:Ljava/lang/String;

.field private static flixsterSessionKey:Ljava/lang/String;

.field private static flixsterUsername:Ljava/lang/String;

.field private static hasAcceptedTos:Z

.field private static hasMigratedToRights:Z

.field private static hasMskPromptShown:Z

.field private static isDiagnosticMode:Z

.field private static isMskEnabled:Z

.field public static mGeocoder:Landroid/location/Geocoder;

.field private static mskAnonPromoUrl:Ljava/lang/String;

.field private static mskTooltipUrl:Ljava/lang/String;

.field private static mskUserPromoUrl:Ljava/lang/String;

.field private static sAdAdminDisableLaunchCap:Z

.field private static sAdAdminDisablePostitialCap:Z

.field private static sAdAdminDisablePrerollCap:Z

.field private static sAdAdminDoubleclickTest:Ljava/lang/String;

.field private static sAdminApiSource:I

.field private static sAdminBaseFbIdArray:[Ljava/lang/String;

.field private static sAdminState:I

.field private static sApplicationContext:Landroid/content/Context;

.field public static sCachePolicy:I

.field private static sCaptionsEnabled:Z

.field private static sCountryCode:Ljava/lang/String;

.field private static sCurrentCity:Ljava/lang/String;

.field private static sCurrentCityRefresh:Z

.field private static sCurrentDimensions:Ljava/lang/String;

.field private static sCurrentLocation:Ljava/lang/String;

.field private static sCurrentLocationRefresh:Z

.field private static sCurrentState:Ljava/lang/String;

.field private static sCurrentStateRefresh:Z

.field private static sCurrentZip:Ljava/lang/String;

.field private static sCurrentZipRefresh:Z

.field public static sDartAdTest:I

.field public static sDay:I

.field private static sDistanceType:I

.field private static sEditor:Landroid/content/SharedPreferences$Editor;

.field public static sFacebook:Lcom/facebook/android/Facebook;

.field private static sFacebookId:Ljava/lang/String;

.field private static sFacebookUsername:Ljava/lang/String;

.field private static sFavoriteTheaterHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static sFavoriteTheaterString:Ljava/lang/String;

.field private static sFirstSessionIndex:J

.field public static sFlixsterCacheManager:Lnet/flixster/android/FlixsterCacheManager;

.field private static sFlixsterInfo:Landroid/content/pm/PackageInfo;

.field public static sInstallMsPosixTime:J

.field private static sLastSessionIndex:J

.field private static sLastTab:Ljava/lang/String;

.field private static sLocale:Ljava/util/Locale;

.field private static sLocation:Landroid/location/Location;

.field private static sLocationManager:Landroid/location/LocationManager;

.field public static sLocationPolicy:I

.field public static sLocationServiceOption:I

.field public static sMovieRatingType:I

.field private static sNetflixEtag:Ljava/lang/String;

.field public static sNetflixListIsDirty:[Z

.field private static sNetflixOAuthToken:Ljava/lang/String;

.field private static sNetflixOAuthTokenSecret:Ljava/lang/String;

.field public static sNetflixStartupValidate:Z

.field private static sNetflixUserId:Ljava/lang/String;

.field public static sNetworkName:[Ljava/lang/String;

.field private static sPlatform:Ljava/lang/String;

.field public static sRelativePixel:F

.field private static sSessionIndex:J

.field private static sSettings:Landroid/content/SharedPreferences;

.field private static sShowtimesDate:Ljava/util/Date;

.field private static sThisSessionIndex:J

.field private static sTicketEmail:Ljava/lang/String;

.field public static sToday:Ljava/util/Date;

.field public static sUseLocationServiceFlag:Z

.field private static sUserAgent:Ljava/lang/String;

.field private static sUserLocationListener:Lnet/flixster/android/FlixsterApplication$UserLocationListener;

.field private static sVersionCode:I

.field private static sVersionName:Ljava/lang/String;

.field private static streamDeleteErrorState:Ljava/lang/String;

.field private static streamStopErrorState:Ljava/lang/String;

.field private static telephonyManager:Landroid/telephony/TelephonyManager;

.field private static udt:Ljava/lang/String;

.field private static userCity:Ljava/lang/String;

.field private static userLatitude:D

.field public static userLocation:Ljava/lang/String;

.field private static userLongitude:D

.field private static userZip:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 69
    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sUserAgent:Ljava/lang/String;

    .line 70
    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sFlixsterInfo:Landroid/content/pm/PackageInfo;

    .line 71
    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sVersionName:Ljava/lang/String;

    .line 72
    sput v4, Lnet/flixster/android/FlixsterApplication;->sVersionCode:I

    .line 74
    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sCountryCode:Ljava/lang/String;

    .line 141
    sput v4, Lnet/flixster/android/FlixsterApplication;->sDistanceType:I

    .line 146
    sput v4, Lnet/flixster/android/FlixsterApplication;->sDartAdTest:I

    .line 162
    const-string v0, "unknown"

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentDimensions:Ljava/lang/String;

    .line 187
    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sCurrentState:Ljava/lang/String;

    .line 188
    sput-boolean v5, Lnet/flixster/android/FlixsterApplication;->sCurrentStateRefresh:Z

    .line 190
    sput-boolean v5, Lnet/flixster/android/FlixsterApplication;->sCurrentCityRefresh:Z

    .line 192
    sput-boolean v5, Lnet/flixster/android/FlixsterApplication;->sCurrentZipRefresh:Z

    .line 194
    sput-boolean v5, Lnet/flixster/android/FlixsterApplication;->sCurrentLocationRefresh:Z

    .line 209
    const-string v0, "FBK"

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    .line 212
    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sTicketEmail:Ljava/lang/String;

    .line 229
    sput v4, Lnet/flixster/android/FlixsterApplication;->sAdminState:I

    .line 230
    sput v4, Lnet/flixster/android/FlixsterApplication;->sAdminApiSource:I

    .line 242
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "2558160538"

    aput-object v1, v0, v4

    const-string v1, "110356772396380"

    aput-object v1, v0, v5

    const-string v1, "2446021478"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const-string v2, "55312797380"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 243
    const-string v2, "28053928260"

    aput-object v2, v0, v1

    const-string v1, "2451978556"

    aput-object v1, v0, v6

    const/4 v1, 0x6

    const-string v2, "2451978556"

    aput-object v2, v0, v1

    .line 242
    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sAdminBaseFbIdArray:[Ljava/lang/String;

    .line 251
    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sNetflixOAuthToken:Ljava/lang/String;

    .line 252
    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sNetflixOAuthTokenSecret:Ljava/lang/String;

    .line 253
    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sNetflixUserId:Ljava/lang/String;

    .line 254
    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sNetflixEtag:Ljava/lang/String;

    .line 255
    sput-boolean v4, Lnet/flixster/android/FlixsterApplication;->sNetflixStartupValidate:Z

    .line 257
    new-array v0, v6, [Z

    fill-array-data v0, :array_0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    .line 615
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Unknown"

    aput-object v1, v0, v4

    const-string v1, "GPRS"

    aput-object v1, v0, v5

    const-string v1, "Edge"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const-string v2, "UMTS"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CDMA"

    aput-object v2, v0, v1

    const-string v1, "EVDO_0"

    aput-object v1, v0, v6

    const/4 v1, 0x6

    const-string v2, "EVDO_A"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "1xRTT"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 616
    const-string v2, "HSDPA"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "HSUPA"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "HSPA"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "IDEN"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "EVDO_B"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "LTE"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "EHRPD"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "HSPAP"

    aput-object v2, v0, v1

    .line 615
    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sNetworkName:[Ljava/lang/String;

    .line 56
    return-void

    .line 257
    nop

    :array_0
    .array-data 0x1
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method static synthetic access$0(Landroid/location/Location;)V
    .locals 0
    .parameter

    .prologue
    .line 183
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    return-void
.end method

.method static synthetic access$1(Z)V
    .locals 0
    .parameter

    .prologue
    .line 192
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->sCurrentZipRefresh:Z

    return-void
.end method

.method static synthetic access$2(Z)V
    .locals 0
    .parameter

    .prologue
    .line 190
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->sCurrentCityRefresh:Z

    return-void
.end method

.method static synthetic access$3(Z)V
    .locals 0
    .parameter

    .prologue
    .line 188
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->sCurrentStateRefresh:Z

    return-void
.end method

.method static synthetic access$4(Z)V
    .locals 0
    .parameter

    .prologue
    .line 194
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->sCurrentLocationRefresh:Z

    return-void
.end method

.method public static addCurrentUserPairs(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 645
    .local p0, pairs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    const-string v0, "FBK"

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 646
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebookId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 647
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "platform"

    const-string v2, "FBK"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 648
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "platform_user"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sFacebookId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 649
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "platform_session"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v2}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 656
    :cond_0
    :goto_0
    return-object p0

    .line 651
    :cond_1
    const-string v0, "FLX"

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 652
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "platform"

    const-string v2, "FLX"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "auth_user"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->flixsterId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 654
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "auth_token"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->flixsterSessionKey:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 1
    .parameter "urlBuilder"

    .prologue
    .line 660
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lnet/flixster/android/FlixsterApplication;->addUserParameters(Ljava/lang/String;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static addFavoriteTheater(Ljava/lang/String;)V
    .locals 4
    .parameter "theaterId"

    .prologue
    .line 708
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->loadFavoriteTheaterHash()V

    .line 709
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 710
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 712
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 713
    .local v1, theaterSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, ","

    invoke-static {v2, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 714
    .local v0, favString:Ljava/lang/String;
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setFavoriteTheaterString(Ljava/lang/String;)V

    .line 717
    .end local v0           #favString:Ljava/lang/String;
    .end local v1           #theaterSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    return-void
.end method

.method public static addUserParameters(Ljava/lang/String;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 2
    .parameter "userId"
    .parameter "urlBuilder"

    .prologue
    .line 664
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 665
    const-string v0, "?"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_1

    .line 666
    const-string v0, "?"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 670
    :goto_0
    const-string v0, "platform="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671
    const-string v0, "FBK"

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 672
    if-eqz p0, :cond_0

    .line 673
    const-string v0, "&platform_user="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 675
    const-string v0, "&platform_session="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v1}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    :cond_0
    :goto_1
    return-object p1

    .line 668
    :cond_1
    const-string v0, "&"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 678
    :cond_2
    const-string v0, "FLX"

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 679
    if-eqz p0, :cond_3

    .line 680
    const-string v0, "&auth_user="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    :cond_3
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->flixsterSessionKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 683
    const-string v0, "&auth_token="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->flixsterSessionKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static enableAdminMode()V
    .locals 1

    .prologue
    .line 1321
    const/4 v0, 0x1

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setAdminState(I)V

    .line 1322
    return-void
.end method

.method public static enableDiagnosticMode()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 852
    sput-boolean v3, Lnet/flixster/android/FlixsterApplication;->isDiagnosticMode:Z

    .line 853
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PREFS_DIAGNOSTIC_MODE"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lnet/flixster/android/FlixsterApplication;->sDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 854
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 855
    return-void
.end method

.method public static favoriteTheaterFlag(Ljava/lang/String;)Z
    .locals 1
    .parameter "theaterId"

    .prologue
    .line 701
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->loadFavoriteTheaterHash()V

    .line 704
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static favoriteTheatersEmptyFlag()Z
    .locals 1

    .prologue
    .line 696
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->loadFavoriteTheaterHash()V

    .line 697
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public static getAdAdminDoubleClickTest()Ljava/lang/String;
    .locals 1

    .prologue
    .line 824
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sAdAdminDoubleclickTest:Ljava/lang/String;

    return-object v0
.end method

.method public static getAdminApiSource()I
    .locals 1

    .prologue
    .line 820
    sget v0, Lnet/flixster/android/FlixsterApplication;->sAdminApiSource:I

    return v0
.end method

.method public static getAdminState()I
    .locals 1

    .prologue
    .line 828
    sget v0, Lnet/flixster/android/FlixsterApplication;->sAdminState:I

    return v0
.end method

.method public static getAndroidBuildInt()I
    .locals 1

    .prologue
    .line 1483
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    return v0
.end method

.method public static getCachePolicy()I
    .locals 1

    .prologue
    .line 1154
    sget v0, Lnet/flixster/android/FlixsterApplication;->sCachePolicy:I

    return v0
.end method

.method public static getCaptionsEnabled()Z
    .locals 1

    .prologue
    .line 1158
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sCaptionsEnabled:Z

    return v0
.end method

.method public static getConnectionType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 636
    const-string v1, "Unknown"

    .line 637
    .local v1, type:Ljava/lang/String;
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 638
    .local v0, info:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    .line 639
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    .line 641
    :cond_0
    return-object v1
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1492
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1457
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method public static getCurrentCity()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1038
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    if-eqz v0, :cond_2

    .line 1041
    :try_start_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentCity:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sCurrentCityRefresh:Z

    if-eqz v0, :cond_2

    .line 1042
    :cond_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->mGeocoder:Landroid/location/Geocoder;

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    const/4 v5, 0x5

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 1043
    .local v7, addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1044
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_3

    .line 1058
    :cond_2
    :goto_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentCity:Ljava/lang/String;

    return-object v0

    .line 1044
    :cond_3
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    .line 1045
    .local v6, a:Landroid/location/Address;
    invoke-virtual {v6}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1047
    invoke-virtual {v6}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentCity:Ljava/lang/String;

    .line 1048
    const/4 v0, 0x0

    sput-boolean v0, Lnet/flixster/android/FlixsterApplication;->sCurrentCityRefresh:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1054
    .end local v6           #a:Landroid/location/Address;
    :catch_0
    move-exception v8

    .line 1055
    .local v8, e:Ljava/lang/Exception;
    const-string v0, "default location exception using gps"

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getCurrentDimensions()Ljava/lang/String;
    .locals 1

    .prologue
    .line 878
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentDimensions:Ljava/lang/String;

    return-object v0
.end method

.method public static getCurrentLatitude()D
    .locals 2

    .prologue
    .line 976
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 977
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    .line 979
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static getCurrentLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 972
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    return-object v0
.end method

.method public static getCurrentLocationDisplay()Ljava/lang/String;
    .locals 16

    .prologue
    .line 1087
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 1088
    .local v13, locationBuilder:Ljava/lang/StringBuilder;
    const-wide/16 v10, 0x0

    .line 1089
    .local v10, latitude:D
    const-wide/16 v14, 0x0

    .line 1090
    .local v14, longitude:D
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    if-eqz v0, :cond_4

    .line 1093
    :try_start_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentLocation:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sCurrentLocationRefresh:Z

    if-eqz v0, :cond_1

    .line 1094
    :cond_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->mGeocoder:Landroid/location/Geocoder;

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    const/4 v5, 0x5

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    .line 1096
    :cond_1
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->mGeocoder:Landroid/location/Geocoder;

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    const/4 v5, 0x5

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 1097
    .local v7, addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1099
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1116
    :cond_3
    :goto_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    .line 1117
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v14

    .line 1122
    .end local v7           #addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    :cond_4
    :goto_1
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_5

    const-wide/16 v0, 0x0

    cmpl-double v0, v10, v0

    if-nez v0, :cond_c

    const-wide/16 v0, 0x0

    cmpl-double v0, v14, v0

    if-nez v0, :cond_c

    .line 1123
    :cond_5
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentLocation:Ljava/lang/String;

    if-eqz v0, :cond_6

    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentLocation:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    .line 1124
    :cond_6
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentLocation:Ljava/lang/String;

    .line 1130
    :cond_7
    :goto_2
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentLocation:Ljava/lang/String;

    return-object v0

    .line 1099
    .restart local v7       #addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    :cond_8
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    .line 1100
    .local v6, a:Landroid/location/Address;
    const/4 v9, 0x1

    .local v9, i:I
    :goto_3
    invoke-virtual {v6}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v1

    if-le v9, v1, :cond_9

    .line 1110
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    goto :goto_0

    .line 1101
    :cond_9
    invoke-virtual {v6, v9}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v12

    .line 1102
    .local v12, line:Ljava/lang/String;
    if-eqz v12, :cond_b

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "UK"

    invoke-virtual {v12, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1103
    const-string v1, "USA"

    invoke-virtual {v12, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "CAN"

    invoke-virtual {v12, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1104
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_a

    .line 1105
    const-string v1, ", "

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1107
    :cond_a
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1100
    :cond_b
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 1118
    .end local v6           #a:Landroid/location/Address;
    .end local v7           #addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    .end local v9           #i:I
    .end local v12           #line:Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 1119
    .local v8, e:Ljava/lang/Exception;
    const-string v0, "FlxMain"

    const-string v1, "FlixsterApplication.getCurrentLocationDisplay"

    invoke-static {v0, v1, v8}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 1127
    .end local v8           #e:Ljava/lang/Exception;
    :cond_c
    const/4 v0, 0x0

    sput-boolean v0, Lnet/flixster/android/FlixsterApplication;->sCurrentLocationRefresh:Z

    .line 1128
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentLocation:Ljava/lang/String;

    goto :goto_2
.end method

.method public static getCurrentLongitude()D
    .locals 2

    .prologue
    .line 983
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 984
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    .line 986
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static getCurrentShortLocationString(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .parameter "context"

    .prologue
    const v4, 0x7f0c00df

    .line 1062
    const-string v1, ""

    .line 1063
    .local v1, result:Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1064
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentZip()Ljava/lang/String;

    move-result-object v2

    .line 1065
    .local v2, zip:Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1066
    move-object v1, v2

    .line 1083
    :goto_0
    return-object v1

    .line 1068
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1071
    .end local v2           #zip:Ljava/lang/String;
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserZip()Ljava/lang/String;

    move-result-object v2

    .line 1072
    .restart local v2       #zip:Ljava/lang/String;
    if-eqz v2, :cond_2

    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1073
    move-object v1, v2

    goto :goto_0

    .line 1075
    :cond_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserCity()Ljava/lang/String;

    move-result-object v0

    .line 1076
    .local v0, city:Ljava/lang/String;
    if-eqz v0, :cond_3

    const-string v3, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1077
    move-object v1, v0

    goto :goto_0

    .line 1079
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getCurrentState()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1015
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    if-eqz v0, :cond_2

    .line 1018
    :try_start_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentState:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sCurrentStateRefresh:Z

    if-eqz v0, :cond_2

    .line 1019
    :cond_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->mGeocoder:Landroid/location/Geocoder;

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    const/4 v5, 0x5

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 1020
    .local v7, addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1021
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_3

    .line 1034
    :cond_2
    :goto_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentState:Ljava/lang/String;

    return-object v0

    .line 1021
    :cond_3
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    .line 1022
    .local v6, a:Landroid/location/Address;
    invoke-virtual {v6}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1023
    invoke-virtual {v6}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentState:Ljava/lang/String;

    .line 1024
    const/4 v0, 0x0

    sput-boolean v0, Lnet/flixster/android/FlixsterApplication;->sCurrentStateRefresh:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1030
    .end local v6           #a:Landroid/location/Address;
    :catch_0
    move-exception v8

    .line 1031
    .local v8, e:Ljava/lang/Exception;
    const-string v0, "default location exception using gps"

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getCurrentZip()Ljava/lang/String;
    .locals 9

    .prologue
    .line 990
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    if-eqz v0, :cond_2

    .line 994
    :try_start_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentZip:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sCurrentZipRefresh:Z

    if-eqz v0, :cond_2

    .line 995
    :cond_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->mGeocoder:Landroid/location/Geocoder;

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    const/4 v5, 0x5

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 996
    .local v7, addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 998
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_3

    .line 1011
    :cond_2
    :goto_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentZip:Ljava/lang/String;

    return-object v0

    .line 998
    :cond_3
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    .line 999
    .local v6, a:Landroid/location/Address;
    invoke-virtual {v6}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1000
    invoke-virtual {v6}, Landroid/location/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sCurrentZip:Ljava/lang/String;

    .line 1001
    const/4 v0, 0x0

    sput-boolean v0, Lnet/flixster/android/FlixsterApplication;->sCurrentZipRefresh:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1007
    .end local v6           #a:Landroid/location/Address;
    :catch_0
    move-exception v8

    .line 1008
    .local v8, e:Ljava/lang/Exception;
    const-string v0, "default location exception using gps"

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getDistanceType()I
    .locals 1

    .prologue
    .line 1166
    sget v0, Lnet/flixster/android/FlixsterApplication;->sDistanceType:I

    return v0
.end method

.method public static getFacebookId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 903
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebookId:Ljava/lang/String;

    return-object v0
.end method

.method public static getFacebookUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 942
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebookUsername:Ljava/lang/String;

    return-object v0
.end method

.method public static getFavoriteTheaterString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 950
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterMovieService.getFavoriteTheaterString() sFavoriteTheaterString:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 951
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 950
    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterString:Ljava/lang/String;

    return-object v0
.end method

.method public static getFavoriteTheatersList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 691
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->loadFavoriteTheaterHash()V

    .line 692
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    return-object v0
.end method

.method public static getFbAppId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 236
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sAdminBaseFbIdArray:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static getFlixsterId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 907
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->flixsterId:Ljava/lang/String;

    return-object v0
.end method

.method public static getFlixsterSessionKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 922
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->flixsterSessionKey:Ljava/lang/String;

    return-object v0
.end method

.method public static getFlixsterUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 946
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->flixsterUsername:Ljava/lang/String;

    return-object v0
.end method

.method public static getGaFirstSession()J
    .locals 2

    .prologue
    .line 956
    sget-wide v0, Lnet/flixster/android/FlixsterApplication;->sFirstSessionIndex:J

    return-wide v0
.end method

.method public static getGaLastSession()J
    .locals 2

    .prologue
    .line 960
    sget-wide v0, Lnet/flixster/android/FlixsterApplication;->sLastSessionIndex:J

    return-wide v0
.end method

.method public static getGaSessionCount()J
    .locals 2

    .prologue
    .line 964
    sget-wide v0, Lnet/flixster/android/FlixsterApplication;->sSessionIndex:J

    return-wide v0
.end method

.method public static getGaThisSession()J
    .locals 2

    .prologue
    .line 968
    sget-wide v0, Lnet/flixster/android/FlixsterApplication;->sThisSessionIndex:J

    return-wide v0
.end method

.method public static getHashedUid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1179
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->udt:Ljava/lang/String;

    return-object v0
.end method

.method public static getLastTab()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1174
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLastTab:Ljava/lang/String;

    return-object v0
.end method

.method public static getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 1487
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public static getLocationPolicy()I
    .locals 1

    .prologue
    .line 1187
    sget v0, Lnet/flixster/android/FlixsterApplication;->sLocationPolicy:I

    return v0
.end method

.method public static getMovieRatingType()I
    .locals 1

    .prologue
    .line 1162
    sget v0, Lnet/flixster/android/FlixsterApplication;->sMovieRatingType:I

    return v0
.end method

.method public static getMskAnonPromoUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1243
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->mskAnonPromoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public static getMskTooltipUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1263
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->mskTooltipUrl:Ljava/lang/String;

    return-object v0
.end method

.method public static getMskUserPromoUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1253
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->mskUserPromoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public static getNetflixEtag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 778
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixEtag:Ljava/lang/String;

    return-object v0
.end method

.method public static getNetflixOAuthToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 734
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixOAuthToken:Ljava/lang/String;

    return-object v0
.end method

.method public static getNetflixOAuthTokenSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 767
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixOAuthTokenSecret:Ljava/lang/String;

    return-object v0
.end method

.method public static getNetflixUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 747
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixUserId:Ljava/lang/String;

    return-object v0
.end method

.method public static getNetworkType()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 619
    const-string v3, "Unknown"

    .line 620
    .local v3, type:Ljava/lang/String;
    sget-object v4, Lnet/flixster/android/FlixsterApplication;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 621
    .local v0, info:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-ne v4, v1, :cond_0

    .line 622
    .local v1, iswifi:Z
    :goto_0
    if-eqz v1, :cond_1

    .line 623
    const-string v3, "WIFI"

    .line 632
    :goto_1
    return-object v3

    .line 621
    .end local v1           #iswifi:Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 625
    .restart local v1       #iswifi:Z
    :cond_1
    sget-object v4, Lnet/flixster/android/FlixsterApplication;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v2

    .line 626
    .local v2, netType:I
    const/16 v4, 0xa

    if-gt v2, v4, :cond_2

    .line 627
    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sNetworkName:[Ljava/lang/String;

    aget-object v3, v4, v2

    goto :goto_1

    .line 629
    :cond_2
    const-string v3, "Unknown(too new)"

    goto :goto_1
.end method

.method public static getPlatform()Ljava/lang/String;
    .locals 1

    .prologue
    .line 882
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    return-object v0
.end method

.method public static getPlatformId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 893
    const-string v0, "FBK"

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 894
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebookId:Ljava/lang/String;

    .line 898
    :goto_0
    return-object v0

    .line 895
    :cond_0
    const-string v0, "FLX"

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 896
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->flixsterId:Ljava/lang/String;

    goto :goto_0

    .line 898
    :cond_1
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebookId:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getPlatformSessionKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 912
    const-string v0, "FBK"

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 913
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    .line 917
    :goto_0
    return-object v0

    .line 914
    :cond_0
    const-string v0, "FLX"

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 915
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->flixsterSessionKey:Ljava/lang/String;

    goto :goto_0

    .line 917
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getPlatformUsername()Ljava/lang/String;
    .locals 2

    .prologue
    .line 932
    const-string v0, "FBK"

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 933
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebookUsername:Ljava/lang/String;

    .line 937
    :goto_0
    return-object v0

    .line 934
    :cond_0
    const-string v0, "FLX"

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 935
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->flixsterUsername:Ljava/lang/String;

    goto :goto_0

    .line 937
    :cond_1
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sFacebookUsername:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getReferrer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1474
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v1, "PREFS_INSTALL_REFERRER"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getShowtimesDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 1170
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sShowtimesDate:Ljava/util/Date;

    return-object v0
.end method

.method public static getStreamDeleteError()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1273
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->streamDeleteErrorState:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1274
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->streamDeleteErrorState:Ljava/lang/String;

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1276
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getStreamStopError()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1291
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->streamStopErrorState:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1292
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->streamStopErrorState:Ljava/lang/String;

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1294
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getTheaterDistance()I
    .locals 3

    .prologue
    .line 1461
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v1, "PREFERENCE_THEATER_DISTANCE"

    const/16 v2, 0x19

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getTicketEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 787
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sTicketEmail:Ljava/lang/String;

    return-object v0
.end method

.method public static getUseLocationServiceFlag()Z
    .locals 1

    .prologue
    .line 1183
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sUseLocationServiceFlag:Z

    return v0
.end method

.method public static getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1191
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sUserAgent:Ljava/lang/String;

    return-object v0
.end method

.method public static getUserCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1150
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->userCity:Ljava/lang/String;

    return-object v0
.end method

.method public static getUserLatitude()D
    .locals 2

    .prologue
    .line 1134
    sget-wide v0, Lnet/flixster/android/FlixsterApplication;->userLatitude:D

    return-wide v0
.end method

.method public static getUserLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1142
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->userLocation:Ljava/lang/String;

    return-object v0
.end method

.method public static getUserLongitude()D
    .locals 2

    .prologue
    .line 1138
    sget-wide v0, Lnet/flixster/android/FlixsterApplication;->userLongitude:D

    return-wide v0
.end method

.method public static getUserZip()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1146
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->userZip:Ljava/lang/String;

    return-object v0
.end method

.method public static getVersionCode()I
    .locals 1

    .prologue
    .line 1195
    sget v0, Lnet/flixster/android/FlixsterApplication;->sVersionCode:I

    return v0
.end method

.method public static getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1199
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sVersionName:Ljava/lang/String;

    return-object v0
.end method

.method public static hasAcceptedTos()Z
    .locals 1

    .prologue
    .line 1203
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->hasAcceptedTos:Z

    return v0
.end method

.method public static hasMigratedToRights()Z
    .locals 1

    .prologue
    .line 1213
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->hasMigratedToRights:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/flixster/android/utils/F;->IS_NATIVE_HC_DRM_ENABLED:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static hasMskPromptShown()Z
    .locals 1

    .prologue
    .line 1223
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->hasMskPromptShown:Z

    return v0
.end method

.method public static isAdAdminLaunchCapDisabled()Z
    .locals 1

    .prologue
    .line 836
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sAdAdminDisableLaunchCap:Z

    return v0
.end method

.method public static isAdAdminPostitialCapDisabled()Z
    .locals 1

    .prologue
    .line 844
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sAdAdminDisablePostitialCap:Z

    return v0
.end method

.method public static isAdAdminPrerollCapDisabled()Z
    .locals 1

    .prologue
    .line 840
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->sAdAdminDisablePrerollCap:Z

    return v0
.end method

.method public static isAdminEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 832
    sget v1, Lnet/flixster/android/FlixsterApplication;->sAdminState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isConnected()Z
    .locals 2

    .prologue
    .line 604
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 605
    .local v0, info:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isDiagnosticMode()Z
    .locals 1

    .prologue
    .line 848
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->isDiagnosticMode:Z

    return v0
.end method

.method public static isLandscape(Landroid/app/Activity;)Z
    .locals 4
    .parameter "activity"

    .prologue
    .line 871
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 872
    .local v0, d:Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    .line 873
    .local v2, width:I
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    .line 874
    .local v1, height:I
    if-ge v1, v2, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isMskEnabled()Z
    .locals 1

    .prologue
    .line 1233
    sget-boolean v0, Lnet/flixster/android/FlixsterApplication;->isMskEnabled:Z

    return v0
.end method

.method public static isSharedPrefInitialized()Z
    .locals 1

    .prologue
    .line 1470
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWifi()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 609
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 610
    .local v0, info:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 611
    .local v1, iswifi:Z
    :goto_0
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FlixsterApplication.isWifi:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    return v1

    .line 610
    .end local v1           #iswifi:Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static loadFavoriteTheaterHash()V
    .locals 7

    .prologue
    .line 799
    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    if-nez v3, :cond_1

    .line 800
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    .line 802
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheaterString()Ljava/lang/String;

    move-result-object v0

    .line 805
    .local v0, favString:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 808
    new-instance v0, Ljava/lang/String;

    .end local v0           #favString:Ljava/lang/String;
    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 810
    .restart local v0       #favString:Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 811
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 812
    .local v1, favoriteTheatersStringArray:[Ljava/lang/String;
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_2

    .line 817
    .end local v1           #favoriteTheatersStringArray:[Ljava/lang/String;
    :cond_1
    return-void

    .line 812
    .restart local v1       #favoriteTheatersStringArray:[Ljava/lang/String;
    :cond_2
    aget-object v2, v1, v3

    .line 813
    .local v2, s:Ljava/lang/String;
    sget-object v5, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 812
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static removeFavoriteTheater(Ljava/lang/String;)V
    .locals 5
    .parameter "theaterId"

    .prologue
    .line 720
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->loadFavoriteTheaterHash()V

    .line 721
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 722
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 724
    :cond_0
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterHash:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 725
    .local v1, theaterSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, ","

    invoke-static {v2, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 727
    .local v0, favString:Ljava/lang/String;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FlixsterApplication.removeFavoriteTheaterHash() favString:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setFavoriteTheaterString(Ljava/lang/String;)V

    .line 729
    return-void
.end method

.method private static restoreUserCredential()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 486
    new-instance v1, Lcom/facebook/android/Facebook;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFbAppId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/android/Facebook;-><init>(Ljava/lang/String;)V

    sput-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    .line 487
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/android/SessionStore;->restore(Lcom/facebook/android/Facebook;Landroid/content/Context;)Z

    .line 488
    new-instance v1, Lcom/flixster/android/data/AccountManager$FbAuthListener;

    invoke-direct {v1}, Lcom/flixster/android/data/AccountManager$FbAuthListener;-><init>()V

    invoke-static {v1}, Lcom/facebook/android/SessionEvents;->addAuthListener(Lcom/facebook/android/SessionEvents$AuthListener;)V

    .line 489
    new-instance v1, Lcom/flixster/android/data/AccountManager$FbLogoutListener;

    invoke-direct {v1}, Lcom/flixster/android/data/AccountManager$FbLogoutListener;-><init>()V

    invoke-static {v1}, Lcom/facebook/android/SessionEvents;->addLogoutListener(Lcom/facebook/android/SessionEvents$LogoutListener;)V

    .line 491
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v2, "PREFS_FB_USERNAME"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebookUsername:Ljava/lang/String;

    .line 492
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v2, "PREFS_FB_USERID"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebookId:Ljava/lang/String;

    .line 493
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v1}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    .line 494
    .local v0, token:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 495
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v2, "PREFS_FB_SESSIONKEY"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 496
    if-eqz v0, :cond_0

    .line 497
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-virtual {v1, v0}, Lcom/facebook/android/Facebook;->setAccessToken(Ljava/lang/String;)V

    .line 498
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sFacebook:Lcom/facebook/android/Facebook;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/android/SessionStore;->save(Lcom/facebook/android/Facebook;Landroid/content/Context;)Z

    .line 499
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "PREFS_FB_SESSIONKEY"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 500
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 501
    const-string v1, "FlxMain"

    const-string v2, "FlixsterApplication.restoreUserCredential legacy fb restored"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    :cond_0
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v2, "PREFS_FLIXSTER_USERNAME"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lnet/flixster/android/FlixsterApplication;->flixsterUsername:Ljava/lang/String;

    .line 506
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v2, "PREFS_PLATFORM"

    const-string v3, "FBK"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    .line 507
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v2, "PREFS_FLIXSTER_ID"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lnet/flixster/android/FlixsterApplication;->flixsterId:Ljava/lang/String;

    .line 508
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v2, "PREFS_FLIXSTER_SESSION_KEY"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lnet/flixster/android/FlixsterApplication;->flixsterSessionKey:Ljava/lang/String;

    .line 510
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/data/AccountManager;->hasUserCredential()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 511
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/data/AccountManager;->logUserCredential()V

    .line 515
    :goto_0
    return-void

    .line 513
    :cond_1
    const-string v1, "FlxMain"

    const-string v2, "FlixsterApplication.restoreUserCredential unavailable"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setAdAdminDisableLaunchCap(Z)V
    .locals 2
    .parameter "disable"

    .prologue
    .line 1331
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->sAdAdminDisableLaunchCap:Z

    .line 1332
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_ADADMIN_DISABLE_LAUNCH_CAP"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1333
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1334
    return-void
.end method

.method public static setAdAdminDisablePostitialCap(Z)V
    .locals 2
    .parameter "disable"

    .prologue
    .line 1343
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->sAdAdminDisablePostitialCap:Z

    .line 1344
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_ADADMIN_DISABLE_POSTITIAL_CAP"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1345
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1346
    return-void
.end method

.method public static setAdAdminDisablePrerollCap(Z)V
    .locals 2
    .parameter "disable"

    .prologue
    .line 1337
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->sAdAdminDisablePrerollCap:Z

    .line 1338
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_ADADMIN_DISABLE_PREROLL_CAP"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1339
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1340
    return-void
.end method

.method public static setAdAdminDoubleclickTest(Ljava/lang/String;)V
    .locals 2
    .parameter "field"

    .prologue
    .line 1315
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sAdAdminDoubleclickTest:Ljava/lang/String;

    .line 1316
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_ADADMIN_DOUBLECLICK_TEST"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1317
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1318
    return-void
.end method

.method public static setAdminApiSource(I)V
    .locals 2
    .parameter "apiSource"

    .prologue
    .line 1309
    sput p0, Lnet/flixster/android/FlixsterApplication;->sAdminApiSource:I

    .line 1310
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_ADMIN_APISOURCE"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1311
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1312
    return-void
.end method

.method public static setAdminState(I)V
    .locals 2
    .parameter "adminState"

    .prologue
    .line 1325
    sput p0, Lnet/flixster/android/FlixsterApplication;->sAdminState:I

    .line 1326
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_ADMIN_STATE"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1327
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1328
    return-void
.end method

.method public static setCachePolicy(I)V
    .locals 3
    .parameter "policy"

    .prologue
    .line 1422
    sput p0, Lnet/flixster/android/FlixsterApplication;->sCachePolicy:I

    .line 1423
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_CACHEPOLICY"

    sget v2, Lnet/flixster/android/FlixsterApplication;->sCachePolicy:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1424
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1425
    return-void
.end method

.method public static setCaptionsEnabled(Z)V
    .locals 3
    .parameter "enabled"

    .prologue
    .line 1428
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->sCaptionsEnabled:Z

    .line 1429
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_CAPTIONSENABLED"

    sget-boolean v2, Lnet/flixster/android/FlixsterApplication;->sCaptionsEnabled:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1430
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1431
    return-void
.end method

.method public static setCurrentDimensions(Landroid/app/Activity;)V
    .locals 3
    .parameter "activity"

    .prologue
    .line 859
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 860
    .local v1, d:Landroid/view/Display;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 861
    .local v0, currentDimension:Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 862
    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 863
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 864
    const-string v2, "__"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 865
    invoke-virtual {v1}, Landroid/view/Display;->getOrientation()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 866
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lnet/flixster/android/FlixsterApplication;->sCurrentDimensions:Ljava/lang/String;

    .line 867
    return-void
.end method

.method public static setFacebookId(Ljava/lang/String;)V
    .locals 2
    .parameter "id"

    .prologue
    .line 1349
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sFacebookId:Ljava/lang/String;

    .line 1350
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_FB_USERID"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1351
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1352
    return-void
.end method

.method public static setFacebookUsername(Ljava/lang/String;)V
    .locals 2
    .parameter "username"

    .prologue
    .line 1355
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sFacebookUsername:Ljava/lang/String;

    .line 1356
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_FB_USERNAME"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1357
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1358
    return-void
.end method

.method public static setFavoriteTheaterString(Ljava/lang/String;)V
    .locals 3
    .parameter "favoriteTheaterString"

    .prologue
    .line 1385
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterApplication.setFavoriteTheaterString() :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1386
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterString:Ljava/lang/String;

    .line 1387
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_FAVORITETHEATERS"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterString:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1388
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1389
    return-void
.end method

.method public static setFlixsterId(Ljava/lang/String;)V
    .locals 3
    .parameter "id"

    .prologue
    .line 1361
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->flixsterId:Ljava/lang/String;

    .line 1362
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_FLIXSTER_ID"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->flixsterId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1363
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1364
    return-void
.end method

.method public static setFlixsterSessionKey(Ljava/lang/String;)V
    .locals 2
    .parameter "key"

    .prologue
    .line 1373
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->flixsterSessionKey:Ljava/lang/String;

    .line 1374
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_FLIXSTER_SESSION_KEY"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1375
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1376
    return-void
.end method

.method public static setFlixsterUsername(Ljava/lang/String;)V
    .locals 2
    .parameter "username"

    .prologue
    .line 1379
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->flixsterUsername:Ljava/lang/String;

    .line 1380
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_FLIXSTER_USERNAME"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1381
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1382
    return-void
.end method

.method public static setLastTab(Ljava/lang/String;)V
    .locals 2
    .parameter "tag"

    .prologue
    .line 1367
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sLastTab:Ljava/lang/String;

    .line 1368
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_LAST_TAB"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1369
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1370
    return-void
.end method

.method public static setLocationPolicy(I)V
    .locals 2
    .parameter "policy"

    .prologue
    .line 1451
    sput p0, Lnet/flixster/android/FlixsterApplication;->sLocationPolicy:I

    .line 1452
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_LOCATIONPOLICY"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1453
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1454
    return-void
.end method

.method public static setMigratedToRights(Z)V
    .locals 2
    .parameter "b"

    .prologue
    .line 1217
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->hasMigratedToRights:Z

    .line 1218
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_MIGRATION_RIGHTS"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1219
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1220
    return-void
.end method

.method public static setMovieRatingType(I)V
    .locals 2
    .parameter "ratingType"

    .prologue
    .line 1434
    sput p0, Lnet/flixster/android/FlixsterApplication;->sMovieRatingType:I

    .line 1435
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_MOVIERATINGOPTION"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1436
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1437
    return-void
.end method

.method public static setMskAnonPromoUrl(Ljava/lang/String;)V
    .locals 2
    .parameter "s"

    .prologue
    .line 1247
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->mskAnonPromoUrl:Ljava/lang/String;

    .line 1248
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PROPS_MSK_ANON_PROMO_URL"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1249
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1250
    return-void
.end method

.method public static setMskEnabled(Z)V
    .locals 2
    .parameter "b"

    .prologue
    .line 1237
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->isMskEnabled:Z

    .line 1238
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PROPS_IS_MSK_ENABLED"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1239
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1240
    return-void
.end method

.method public static setMskPromptShown()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1227
    sput-boolean v2, Lnet/flixster/android/FlixsterApplication;->hasMskPromptShown:Z

    .line 1228
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_MSK_PROMPT_4.1"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1229
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1230
    return-void
.end method

.method public static setMskTooltipUrl(Ljava/lang/String;)V
    .locals 2
    .parameter "s"

    .prologue
    .line 1267
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->mskTooltipUrl:Ljava/lang/String;

    .line 1268
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PROPS_MSK_TOOLTIP_URL"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1269
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1270
    return-void
.end method

.method public static setMskUserPromoUrl(Ljava/lang/String;)V
    .locals 2
    .parameter "s"

    .prologue
    .line 1257
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->mskUserPromoUrl:Ljava/lang/String;

    .line 1258
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PROPS_MSK_USER_PROMO_URL"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1259
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1260
    return-void
.end method

.method public static setNetflixEtag(Ljava/lang/String;)V
    .locals 0
    .parameter "etag"

    .prologue
    .line 782
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sNetflixEtag:Ljava/lang/String;

    .line 783
    return-void
.end method

.method public static setNetflixOAuthToken(Ljava/lang/String;)V
    .locals 3
    .parameter "token"

    .prologue
    .line 738
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterApplication.setNetflixOAuthToken() token:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sNetflixOAuthToken:Ljava/lang/String;

    .line 740
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_NETFLIX_OAUTH_TOKEN"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sNetflixOAuthToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 741
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 742
    return-void
.end method

.method public static setNetflixOAuthTokenSecret(Ljava/lang/String;)V
    .locals 3
    .parameter "token"

    .prologue
    .line 771
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterApplication.setNetflixOAuthTokenSecret() sNetflixOAuthTokenSecret:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sNetflixOAuthTokenSecret:Ljava/lang/String;

    .line 773
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_NETFLIX_OAUTH_TOKEN_SECRET"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sNetflixOAuthTokenSecret:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 774
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 775
    return-void
.end method

.method public static setNetflixUserId(Ljava/lang/String;)V
    .locals 3
    .parameter "token"

    .prologue
    .line 751
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterApplication.setNetflixUserId() sNetflixUserId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sNetflixUserId:Ljava/lang/String;

    .line 753
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_NETFLIX_USER_ID"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sNetflixUserId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 754
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 755
    return-void
.end method

.method public static setPlatform(Ljava/lang/String;)V
    .locals 3
    .parameter "platform"

    .prologue
    .line 886
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    .line 887
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_PLATFORM"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sPlatform:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 888
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 889
    return-void
.end method

.method public static setPosixInstallTime(J)V
    .locals 4
    .parameter "posixInstallTime"

    .prologue
    .line 758
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterApplication.setPosixInstallTime() posixInstallTime:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    sput-wide p0, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    .line 760
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_INSTALLMSPOSIXTIME"

    sget-wide v2, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 761
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 762
    return-void
.end method

.method public static setReferrer(Ljava/lang/String;)V
    .locals 2
    .parameter "referrer"

    .prologue
    .line 1478
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_INSTALL_REFERRER"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1479
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1480
    return-void
.end method

.method public static setShowtimesDate(Ljava/util/Date;)V
    .locals 3
    .parameter "date"

    .prologue
    .line 1440
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterApplication.setShowtimesDate date:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1441
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sShowtimesDate:Ljava/util/Date;

    .line 1442
    return-void
.end method

.method public static setStreamDeleteError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "movieId"
    .parameter "streamId"

    .prologue
    .line 1281
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 1282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->streamDeleteErrorState:Ljava/lang/String;

    .line 1286
    :goto_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_STREAM_DELETE_ERROR"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->streamDeleteErrorState:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1287
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1288
    return-void

    .line 1284
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->streamDeleteErrorState:Ljava/lang/String;

    goto :goto_0
.end method

.method public static setStreamStopError(Ljava/lang/Long;Ljava/lang/String;)V
    .locals 3
    .parameter "rightId"
    .parameter "streamId"

    .prologue
    .line 1299
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 1300
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ZZZZ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->streamStopErrorState:Ljava/lang/String;

    .line 1304
    :goto_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_STREAM_STOP_ERROR"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->streamStopErrorState:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1305
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1306
    return-void

    .line 1302
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->streamStopErrorState:Ljava/lang/String;

    goto :goto_0
.end method

.method public static setTheaterDistance(I)V
    .locals 2
    .parameter "distance"

    .prologue
    .line 1465
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFERENCE_THEATER_DISTANCE"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1466
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1467
    return-void
.end method

.method public static setTicketEmail(Ljava/lang/String;)V
    .locals 3
    .parameter "email"

    .prologue
    .line 792
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->sTicketEmail:Ljava/lang/String;

    .line 793
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "TICKET_EMAIL"

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sTicketEmail:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 794
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 795
    return-void
.end method

.method public static setTosAccepted(Z)V
    .locals 2
    .parameter "b"

    .prologue
    .line 1207
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->hasAcceptedTos:Z

    .line 1208
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_TOS_ACCEPT_3.7.2"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1209
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1210
    return-void
.end method

.method public static setUseLocationServiceFlag(Z)V
    .locals 2
    .parameter "flag"

    .prologue
    .line 1445
    sput-boolean p0, Lnet/flixster/android/FlixsterApplication;->sUseLocationServiceFlag:Z

    .line 1446
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_USEAUTOLOCATE"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1447
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1448
    return-void
.end method

.method public static setUserCity(Ljava/lang/String;)V
    .locals 2
    .parameter "city"

    .prologue
    .line 1416
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->userCity:Ljava/lang/String;

    .line 1417
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_USER_CITY"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1418
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1419
    return-void
.end method

.method public static setUserLatitude(D)V
    .locals 3
    .parameter "latitude"

    .prologue
    .line 1398
    sput-wide p0, Lnet/flixster/android/FlixsterApplication;->userLatitude:D

    .line 1399
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_LATITUDE"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1400
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1401
    return-void
.end method

.method public static setUserLocation(Ljava/lang/String;)V
    .locals 2
    .parameter "location"

    .prologue
    .line 1392
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->userLocation:Ljava/lang/String;

    .line 1393
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_MANUALZIPCODE"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1394
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1395
    return-void
.end method

.method public static setUserLongitude(D)V
    .locals 3
    .parameter "longitude"

    .prologue
    .line 1404
    sput-wide p0, Lnet/flixster/android/FlixsterApplication;->userLongitude:D

    .line 1405
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_LONGITUDE"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1406
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1407
    return-void
.end method

.method public static setUserZip(Ljava/lang/String;)V
    .locals 2
    .parameter "zip"

    .prologue
    .line 1410
    sput-object p0, Lnet/flixster/android/FlixsterApplication;->userZip:Ljava/lang/String;

    .line 1411
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREFS_USER_ZIP"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1412
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1413
    return-void
.end method

.method public static setupLocationServices()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v11, 0x1

    .line 521
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocationManager:Landroid/location/LocationManager;

    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sUserLocationListener:Lnet/flixster/android/FlixsterApplication$UserLocationListener;

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 523
    new-instance v6, Landroid/location/Criteria;

    invoke-direct {v6}, Landroid/location/Criteria;-><init>()V

    .line 524
    .local v6, c:Landroid/location/Criteria;
    invoke-virtual {v6, v3}, Landroid/location/Criteria;->setAltitudeRequired(Z)V

    .line 525
    invoke-virtual {v6, v3}, Landroid/location/Criteria;->setCostAllowed(Z)V

    .line 526
    invoke-virtual {v6, v3}, Landroid/location/Criteria;->setSpeedRequired(Z)V

    .line 528
    const/4 v1, 0x0

    .line 529
    .local v1, providerName:Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getLocationPolicy()I

    move-result v9

    .line 530
    .local v9, policy:I
    const-string v0, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FlixsterApplication.setupLocationServices policy:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    packed-switch v9, :pswitch_data_0

    .line 552
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 553
    const-string v0, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FlixsterApplication.setupLocationServices requesting provider:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    :try_start_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocationManager:Landroid/location/LocationManager;

    const-wide/32 v2, 0x124f80

    .line 556
    const/high16 v4, 0x44fa

    sget-object v5, Lnet/flixster/android/FlixsterApplication;->sUserLocationListener:Lnet/flixster/android/FlixsterApplication$UserLocationListener;

    .line 555
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 563
    :cond_1
    :goto_1
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, v11}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v10

    .line 564
    .local v10, providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v8, v0, -0x1

    .local v8, i:I
    :goto_2
    if-gez v8, :cond_4

    .line 570
    :cond_2
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    if-nez v0, :cond_3

    .line 571
    const-string v0, "FlxMain"

    const-string v2, "FlixsterApplication.SetupLocationServices sLocation was null"

    invoke-static {v0, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    new-instance v0, Landroid/location/Location;

    const-string v2, "network"

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    .line 574
    :cond_3
    const-string v0, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FlixsterApplication.SetupLocationServices cached providers:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    .end local v8           #i:I
    .end local v10           #providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :pswitch_0
    return-void

    .line 535
    :pswitch_1
    const/4 v0, 0x2

    invoke-virtual {v6, v0}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 536
    invoke-virtual {v6, v11}, Landroid/location/Criteria;->setPowerRequirement(I)V

    .line 537
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, v6, v11}, Landroid/location/LocationManager;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    move-result-object v1

    .line 538
    if-nez v1, :cond_0

    .line 539
    const-string v1, "network"

    .line 541
    goto :goto_0

    .line 543
    :pswitch_2
    invoke-virtual {v6, v11}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 544
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, v6, v11}, Landroid/location/LocationManager;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    move-result-object v1

    .line 545
    if-nez v1, :cond_0

    .line 546
    const-string v1, "gps"

    goto/16 :goto_0

    .line 557
    :catch_0
    move-exception v7

    .line 558
    .local v7, e:Ljava/lang/Exception;
    const-string v0, "FlxMain"

    const-string v2, "FlixsterApplication.setupLocationServices requestLocationUpdates"

    invoke-static {v0, v2, v7}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 565
    .end local v7           #e:Ljava/lang/Exception;
    .restart local v8       #i:I
    .restart local v10       #providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sLocationManager:Landroid/location/LocationManager;

    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    .line 566
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocation:Landroid/location/Location;

    if-nez v0, :cond_2

    .line 564
    add-int/lit8 v8, v8, -0x1

    goto :goto_2

    .line 531
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static stopLocationRequest()V
    .locals 2

    .prologue
    .line 600
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sLocationManager:Landroid/location/LocationManager;

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sUserLocationListener:Lnet/flixster/android/FlixsterApplication$UserLocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 601
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 12

    .prologue
    .line 270
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 271
    const-string v8, "FlxMain"

    const-string v9, "FlixsterApplication.onCreate"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :try_start_0
    new-instance v8, Landroid/location/Geocoder;

    invoke-direct {v8, p0}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->mGeocoder:Landroid/location/Geocoder;

    .line 274
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sApplicationContext:Landroid/content/Context;

    .line 276
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0022

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    sput v8, Lnet/flixster/android/FlixsterApplication;->sRelativePixel:F

    .line 278
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 280
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    const/4 v9, 0x6

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    sput v8, Lnet/flixster/android/FlixsterApplication;->sDay:I

    .line 284
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sFlixsterInfo:Landroid/content/pm/PackageInfo;

    .line 285
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget-object v8, v8, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sLocale:Ljava/util/Locale;

    .line 287
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sLocale:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v6

    .line 288
    .local v6, isoCountryCode:Ljava/lang/String;
    const-string v8, "US"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 289
    const-string v8, "us"

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sCountryCode:Ljava/lang/String;

    .line 290
    const/4 v8, 0x0

    sput v8, Lnet/flixster/android/FlixsterApplication;->sDistanceType:I

    .line 305
    :cond_0
    :goto_0
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sFlixsterInfo:Landroid/content/pm/PackageInfo;

    iget-object v8, v8, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sVersionName:Ljava/lang/String;

    .line 306
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sFlixsterInfo:Landroid/content/pm/PackageInfo;

    iget v8, v8, Landroid/content/pm/PackageInfo;->versionCode:I

    sput v8, Lnet/flixster/android/FlixsterApplication;->sVersionCode:I

    .line 311
    const-string v8, "FlixsterPrefs"

    const/4 v9, 0x0

    invoke-virtual {p0, v8, v9}, Lnet/flixster/android/FlixsterApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    .line 312
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    .line 314
    const/4 v5, 0x0

    .line 315
    .local v5, isUpgrade:Z
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_INSTALLMSPOSIXTIME"

    const-wide/16 v10, 0x0

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    sput-wide v8, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    .line 316
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_LASTVERSION"

    sget v10, Lnet/flixster/android/FlixsterApplication;->sVersionCode:I

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 317
    .local v7, lastVersionCode:I
    sget-wide v8, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_a

    .line 318
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v8

    const-class v9, Lcom/flixster/android/utils/Properties$VisitType;

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/flixster/android/utils/Properties$VisitType;->INSTALL:Lcom/flixster/android/utils/Properties$VisitType;

    invoke-virtual {v8, v9, v10}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 319
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    sput-wide v8, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    .line 320
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "PREFS_INSTALLMSPOSIXTIME"

    sget-wide v10, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 321
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "PREFS_LASTVERSION"

    sget v10, Lnet/flixster/android/FlixsterApplication;->sVersionCode:I

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 332
    :goto_1
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_USEAUTOLOCATE"

    const/4 v10, 0x1

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->sUseLocationServiceFlag:Z

    .line 333
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_LOCATIONPOLICY"

    const/4 v10, 0x1

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    sput v8, Lnet/flixster/android/FlixsterApplication;->sLocationPolicy:I

    .line 334
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_MANUALZIPCODE"

    const-string v10, ""

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->userLocation:Ljava/lang/String;

    .line 335
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_CACHEPOLICY"

    const/4 v10, 0x2

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    sput v8, Lnet/flixster/android/FlixsterApplication;->sCachePolicy:I

    .line 336
    new-instance v8, Lnet/flixster/android/FlixsterCacheManager;

    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lnet/flixster/android/FlixsterCacheManager;-><init>(Landroid/content/Context;)V

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sFlixsterCacheManager:Lnet/flixster/android/FlixsterCacheManager;

    .line 337
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_MOVIERATINGOPTION"

    const/4 v10, 0x1

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    sput v8, Lnet/flixster/android/FlixsterApplication;->sMovieRatingType:I

    .line 338
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_CAPTIONSENABLED"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->sCaptionsEnabled:Z

    .line 339
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_ADMIN_STATE"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    sput v8, Lnet/flixster/android/FlixsterApplication;->sAdminState:I

    .line 340
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_ADMIN_APISOURCE"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    sput v8, Lnet/flixster/android/FlixsterApplication;->sAdminApiSource:I

    .line 341
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_ADADMIN_DOUBLECLICK_TEST"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sAdAdminDoubleclickTest:Ljava/lang/String;

    .line 342
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_ADADMIN_DISABLE_LAUNCH_CAP"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->sAdAdminDisableLaunchCap:Z

    .line 343
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_ADADMIN_DISABLE_PREROLL_CAP"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->sAdAdminDisablePrerollCap:Z

    .line 344
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_ADADMIN_DISABLE_POSTITIAL_CAP"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->sAdAdminDisablePostitialCap:Z

    .line 345
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "PREFS_DIAGNOSTIC_MODE"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v10, Lnet/flixster/android/FlixsterApplication;->sDay:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->isDiagnosticMode:Z

    .line 347
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_LAST_TAB"

    const-string v10, "boxoffice"

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sLastTab:Ljava/lang/String;

    .line 349
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_LATITUDE"

    const-string v10, "0"

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    sput-wide v8, Lnet/flixster/android/FlixsterApplication;->userLatitude:D

    .line 350
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_LONGITUDE"

    const-string v10, "0"

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    sput-wide v8, Lnet/flixster/android/FlixsterApplication;->userLongitude:D

    .line 351
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_USER_ZIP"

    const-string v10, ""

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->userZip:Ljava/lang/String;

    .line 352
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_USER_CITY"

    const-string v10, ""

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->userCity:Ljava/lang/String;

    .line 355
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_FAVORITETHEATERS"

    const-string v10, ""

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sFavoriteTheaterString:Ljava/lang/String;

    .line 358
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->restoreUserCredential()V

    .line 361
    sget-object v9, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v10, "PREFS_TOS_ACCEPT_3.7.2"

    const/16 v8, 0x17c

    if-lt v7, v8, :cond_d

    const/4 v8, 0x1

    :goto_2
    invoke-interface {v9, v10, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->hasAcceptedTos:Z

    .line 362
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_MSK_PROMPT_4.1"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->hasMskPromptShown:Z

    .line 363
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PROPS_IS_MSK_ENABLED"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->isMskEnabled:Z

    .line 364
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PROPS_MSK_ANON_PROMO_URL"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->mskAnonPromoUrl:Ljava/lang/String;

    .line 365
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PROPS_MSK_USER_PROMO_URL"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->mskUserPromoUrl:Ljava/lang/String;

    .line 366
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PROPS_MSK_TOOLTIP_URL"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->mskTooltipUrl:Ljava/lang/String;

    .line 368
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_STREAM_DELETE_ERROR"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->streamDeleteErrorState:Ljava/lang/String;

    .line 369
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_STREAM_STOP_ERROR"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->streamStopErrorState:Ljava/lang/String;

    .line 371
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_MIGRATION_RIGHTS"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->hasMigratedToRights:Z

    .line 372
    if-eqz v5, :cond_1

    sget-boolean v8, Lnet/flixster/android/FlixsterApplication;->hasMigratedToRights:Z

    if-eqz v8, :cond_1

    .line 373
    const/4 v8, 0x0

    sput-boolean v8, Lnet/flixster/android/FlixsterApplication;->hasMigratedToRights:Z

    .line 374
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "PREFS_MIGRATION_RIGHTS"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 377
    :cond_1
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "TICKET_EMAIL"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sTicketEmail:Ljava/lang/String;

    .line 378
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_NETFLIX_OAUTH_TOKEN"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sNetflixOAuthToken:Ljava/lang/String;

    .line 379
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_NETFLIX_USER_ID"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sNetflixUserId:Ljava/lang/String;

    .line 380
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_NETFLIX_OAUTH_TOKEN_SECRET"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sNetflixOAuthTokenSecret:Ljava/lang/String;

    .line 382
    const-string v8, "phone"

    invoke-virtual {p0, v8}, Lnet/flixster/android/FlixsterApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 383
    const-string v8, "connectivity"

    invoke-virtual {p0, v8}, Lnet/flixster/android/FlixsterApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/ConnectivityManager;

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 384
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 385
    .local v1, contentResolver:Landroid/content/ContentResolver;
    invoke-static {}, Lcom/flixster/android/utils/OsInfo;->instance()Lcom/flixster/android/utils/OsInfo;

    move-result-object v8

    sget-object v9, Lnet/flixster/android/FlixsterApplication;->connectivityManager:Landroid/net/ConnectivityManager;

    sget-object v10, Lnet/flixster/android/FlixsterApplication;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v8, v9, v10, v1}, Lcom/flixster/android/utils/OsInfo;->initialize(Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;Landroid/content/ContentResolver;)V

    .line 387
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_FLX_HASH"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->udt:Ljava/lang/String;

    .line 388
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->udt:Ljava/lang/String;

    if-nez v8, :cond_2

    .line 389
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-static {v8, v1}, Lcom/flixster/android/utils/VersionedUidHelper;->getUdt(Landroid/telephony/TelephonyManager;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->udt:Ljava/lang/String;

    .line 390
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "PREFS_FLX_HASH"

    sget-object v10, Lnet/flixster/android/FlixsterApplication;->udt:Ljava/lang/String;

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 393
    :cond_2
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v2, v8, v10

    .line 394
    .local v2, currSecond:J
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_GAFIRSTSESSION"

    invoke-interface {v8, v9, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    sput-wide v8, Lnet/flixster/android/FlixsterApplication;->sFirstSessionIndex:J

    .line 395
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_GATHISSESSION"

    sget-wide v10, Lnet/flixster/android/FlixsterApplication;->sFirstSessionIndex:J

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    sput-wide v8, Lnet/flixster/android/FlixsterApplication;->sThisSessionIndex:J

    .line 396
    sget-wide v8, Lnet/flixster/android/FlixsterApplication;->sThisSessionIndex:J

    sput-wide v8, Lnet/flixster/android/FlixsterApplication;->sLastSessionIndex:J

    .line 397
    sput-wide v2, Lnet/flixster/android/FlixsterApplication;->sThisSessionIndex:J

    .line 398
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sSettings:Landroid/content/SharedPreferences;

    const-string v9, "PREFS_SESSIONCOUNT"

    const-wide/16 v10, 0x0

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    sput-wide v8, Lnet/flixster/android/FlixsterApplication;->sSessionIndex:J

    .line 399
    sget-wide v8, Lnet/flixster/android/FlixsterApplication;->sSessionIndex:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    sput-wide v8, Lnet/flixster/android/FlixsterApplication;->sSessionIndex:J

    .line 401
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "PREFS_GAFIRSTSESSION"

    sget-wide v10, Lnet/flixster/android/FlixsterApplication;->sFirstSessionIndex:J

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 402
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "PREFS_GALASTSESSION"

    sget-wide v10, Lnet/flixster/android/FlixsterApplication;->sLastSessionIndex:J

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 403
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "PREFS_GATHISSESSION"

    sget-wide v10, Lnet/flixster/android/FlixsterApplication;->sThisSessionIndex:J

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 404
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "PREFS_SESSIONCOUNT"

    sget-wide v10, Lnet/flixster/android/FlixsterApplication;->sSessionIndex:J

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 405
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 407
    const-string v8, "location"

    invoke-virtual {p0, v8}, Lnet/flixster/android/FlixsterApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/location/LocationManager;

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sLocationManager:Landroid/location/LocationManager;

    .line 408
    new-instance v8, Lnet/flixster/android/FlixsterApplication$UserLocationListener;

    invoke-direct {v8}, Lnet/flixster/android/FlixsterApplication$UserLocationListener;-><init>()V

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sUserLocationListener:Lnet/flixster/android/FlixsterApplication$UserLocationListener;

    .line 410
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Android/"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v9, Lnet/flixster/android/FlixsterApplication;->sVersionName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " (Linux; U; Android "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lnet/flixster/android/FlixsterApplication;->sLocale:Ljava/util/Locale;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 411
    const-string v9, "; "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 410
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sUserAgent:Ljava/lang/String;

    .line 413
    const-string v8, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Build.VERSION.SDK:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "  Build.VERSION.RELEASE:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 414
    sget-object v10, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 413
    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-string v8, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "FlixsterApplication.onCreate sUserAgent:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v10, Lnet/flixster/android/FlixsterApplication;->sUserAgent:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string v8, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "FlixsterApplication.onCreate sCountryCode:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v10, Lnet/flixster/android/FlixsterApplication;->sCountryCode:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sShowtimesDate:Ljava/util/Date;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    const/4 v0, 0x0

    .line 422
    .local v0, accountName:Ljava/lang/String;
    :try_start_1
    invoke-static {p0}, Lnet/flixster/android/AccountManagerCanary;->getFirstAccountName(Landroid/content/Context;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/VerifyError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 427
    :goto_3
    if-eqz v0, :cond_3

    :try_start_2
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sTicketEmail:Ljava/lang/String;

    if-nez v8, :cond_3

    .line 428
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setTicketEmail(Ljava/lang/String;)V

    .line 432
    :cond_3
    invoke-static {}, Lnet/flixster/android/util/ErrorHandler;->instance()Lnet/flixster/android/util/ErrorHandler;

    move-result-object v8

    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9}, Lnet/flixster/android/util/ErrorHandler;->setDefaultUncaughtExceptionHandler(Landroid/content/Context;)V

    .line 435
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/flixster/android/utils/Translator;->initialize(Landroid/content/Context;)V

    .line 438
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v8

    invoke-virtual {v8, p0}, Lcom/flixster/android/utils/Properties;->identifyHoneycombTablet(Landroid/content/Context;)V

    .line 439
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v8

    invoke-virtual {v8, p0}, Lcom/flixster/android/utils/Properties;->identifyGoogleTv(Landroid/content/Context;)V

    .line 442
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 443
    const-string v8, "FlxMain"

    const-string v9, "Disabling widget"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 445
    new-instance v9, Landroid/content/ComponentName;

    const-class v10, Lcom/flixster/android/widget/WidgetProvider;

    invoke-direct {v9, p0, v10}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 446
    const/4 v10, 0x2

    const/4 v11, 0x1

    .line 444
    invoke-virtual {v8, v9, v10, v11}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 447
    const-string v8, "FlxMain"

    const-string v9, "Disabling widget service"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 449
    new-instance v9, Landroid/content/ComponentName;

    const-class v10, Lcom/flixster/android/widget/WidgetConfigChangeService;

    invoke-direct {v9, p0, v10}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 450
    const/4 v10, 0x2

    const/4 v11, 0x1

    .line 448
    invoke-virtual {v8, v9, v10, v11}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 451
    const-string v8, "FlxMain"

    const-string v9, "Disabled"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    :cond_4
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/flixster/android/data/AccountManager;->restoreUserSession()V

    .line 456
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/flixster/android/data/AccountManager;->setSkipRefreshUserSession()V

    .line 459
    invoke-static {p0}, Lcom/fiksu/asotracking/FiksuTrackingManager;->initialize(Landroid/app/Application;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    .line 466
    .end local v0           #accountName:Ljava/lang/String;
    .end local v1           #contentResolver:Landroid/content/ContentResolver;
    .end local v2           #currSecond:J
    .end local v5           #isUpgrade:Z
    .end local v6           #isoCountryCode:Ljava/lang/String;
    .end local v7           #lastVersionCode:I
    :goto_4
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v8

    invoke-interface {v8}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceStreamingCompatible()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 467
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v8

    invoke-virtual {p0}, Lnet/flixster/android/FlixsterApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/flixster/android/drm/PlaybackManager;->initializeDRM(Landroid/content/Context;)V

    .line 481
    :cond_5
    return-void

    .line 291
    .restart local v6       #isoCountryCode:Ljava/lang/String;
    :cond_6
    :try_start_3
    const-string v8, "GB"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 292
    const-string v8, "uk"

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sCountryCode:Ljava/lang/String;

    .line 293
    const/4 v8, 0x0

    sput v8, Lnet/flixster/android/FlixsterApplication;->sDistanceType:I
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 461
    .end local v6           #isoCountryCode:Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 462
    .local v4, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "FlxMain"

    const-string v9, "Package name not found"

    invoke-static {v8, v9, v4}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 294
    .end local v4           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v6       #isoCountryCode:Ljava/lang/String;
    :cond_7
    :try_start_4
    const-string v8, "CA"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 295
    const-string v8, "ca"

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sCountryCode:Ljava/lang/String;

    .line 296
    const/4 v8, 0x1

    sput v8, Lnet/flixster/android/FlixsterApplication;->sDistanceType:I

    goto/16 :goto_0

    .line 297
    :cond_8
    const-string v8, "AU"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 298
    const-string v8, "au"

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sCountryCode:Ljava/lang/String;

    .line 299
    const/4 v8, 0x1

    sput v8, Lnet/flixster/android/FlixsterApplication;->sDistanceType:I

    goto/16 :goto_0

    .line 300
    :cond_9
    const-string v8, "FR"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 301
    const-string v8, "fr"

    sput-object v8, Lnet/flixster/android/FlixsterApplication;->sCountryCode:Ljava/lang/String;

    .line 302
    const/4 v8, 0x1

    sput v8, Lnet/flixster/android/FlixsterApplication;->sDistanceType:I

    goto/16 :goto_0

    .line 322
    .restart local v5       #isUpgrade:Z
    .restart local v7       #lastVersionCode:I
    :cond_a
    sget v8, Lnet/flixster/android/FlixsterApplication;->sVersionCode:I

    if-le v8, v7, :cond_b

    .line 323
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v8

    const-class v9, Lcom/flixster/android/utils/Properties$VisitType;

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/flixster/android/utils/Properties$VisitType;->UPGRADE:Lcom/flixster/android/utils/Properties$VisitType;

    invoke-virtual {v8, v9, v10}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 324
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "PREFS_LASTVERSION"

    sget v10, Lnet/flixster/android/FlixsterApplication;->sVersionCode:I

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 325
    const/4 v5, 0x1

    goto/16 :goto_1

    .line 326
    :cond_b
    sget v8, Lnet/flixster/android/FlixsterApplication;->sVersionCode:I

    if-ge v8, v7, :cond_c

    .line 327
    sget-object v8, Lnet/flixster/android/FlixsterApplication;->sEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "PREFS_LASTVERSION"

    sget v10, Lnet/flixster/android/FlixsterApplication;->sVersionCode:I

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_1

    .line 329
    :cond_c
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v8

    const-class v9, Lcom/flixster/android/utils/Properties$VisitType;

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/flixster/android/utils/Properties$VisitType;->SESSION:Lcom/flixster/android/utils/Properties$VisitType;

    invoke-virtual {v8, v9, v10}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 361
    :cond_d
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 424
    .restart local v0       #accountName:Ljava/lang/String;
    .restart local v1       #contentResolver:Landroid/content/ContentResolver;
    .restart local v2       #currSecond:J
    :catch_1
    move-exception v4

    .line 425
    .local v4, e:Ljava/lang/VerifyError;
    const-string v8, "FlxMain"

    const-string v9, "FlixsterApplication AccountManager not available"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_3
.end method
