.class Lnet/flixster/android/FlixsterCacheManager$1;
.super Ljava/util/TimerTask;
.source "FlixsterCacheManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/FlixsterCacheManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FlixsterCacheManager;


# direct methods
.method constructor <init>(Lnet/flixster/android/FlixsterCacheManager;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FlixsterCacheManager$1;->this$0:Lnet/flixster/android/FlixsterCacheManager;

    .line 55
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    .local v0, activeState:Z
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mounted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 64
    .local v1, sdDir:Ljava/io/File;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FlixsterCacheManager sdDir:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/data/flixster/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lnet/flixster/android/FlixsterCacheManager;->access$0(Ljava/io/File;)V

    .line 66
    invoke-static {}, Lnet/flixster/android/FlixsterCacheManager;->access$1()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lnet/flixster/android/FlixsterCacheManager;->access$1()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lnet/flixster/android/FlixsterCacheManager;->access$1()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 67
    const-string v2, "FlxMain"

    const-string v3, "FlixsterCacheManager  mCacheDir exists == true"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v2, p0, Lnet/flixster/android/FlixsterCacheManager$1;->this$0:Lnet/flixster/android/FlixsterCacheManager;

    invoke-virtual {v2}, Lnet/flixster/android/FlixsterCacheManager;->buildCacheIndex()V

    .line 69
    const/4 v0, 0x1

    .line 79
    .end local v1           #sdDir:Ljava/io/File;
    :cond_0
    :goto_0
    invoke-static {}, Lnet/flixster/android/FlixsterCacheManager;->SetCacheLimit()V

    .line 80
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Lnet/flixster/android/FlixsterCacheManager;->TrimCache(J)V

    .line 81
    invoke-static {v0}, Lnet/flixster/android/FlixsterCacheManager;->access$2(Z)V

    .line 82
    return-void

    .line 71
    .restart local v1       #sdDir:Ljava/io/File;
    :cond_1
    const-string v2, "FlxMain"

    const-string v3, "FlixsterCacheManager mCacheDir mkdirs()"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-static {}, Lnet/flixster/android/FlixsterCacheManager;->access$1()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    const/4 v0, 0x1

    goto :goto_0
.end method
