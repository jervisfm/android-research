.class public Lnet/flixster/android/FlixsterCacheManager;
.super Ljava/lang/Object;
.source "FlixsterCacheManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/FlixsterCacheManager$CacheItem;
    }
.end annotation


# static fields
.field public static final CACHE_TRIMPADDING:J = 0x1388L

.field public static final POLICY_LARGE:I = 0x3

.field public static final POLICY_MEDIUM:I = 0x2

.field public static final POLICY_OFF:I = 0x0

.field public static final POLICY_SMALL:I = 0x1

.field public static final STATE_INVALID:I = 0x0

.field public static final STATE_VALID:I = 0x2

.field public static final STATE_WRITING:I = 0x1

.field private static sCacheByteSize:J

.field private static sCacheDir:Ljava/io/File;

.field private static sCacheHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lnet/flixster/android/FlixsterCacheManager$CacheItem;",
            ">;"
        }
    .end annotation
.end field

.field private static sCacheItemSize:I

.field public static sCacheLimit:J

.field private static sFileInputStream:Ljava/io/FileInputStream;

.field private static sFileOutputStream:Ljava/io/FileOutputStream;

.field private static sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

.field private static sIsActive:Z

.field private static sTail:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

.field private static sTimer:Ljava/util/Timer;

.field private static sTimerTask:Ljava/util/TimerTask;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 26
    sput-wide v2, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    .line 39
    sput-object v1, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    .line 40
    sput-object v1, Lnet/flixster/android/FlixsterCacheManager;->sTail:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    .line 41
    sput v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheItemSize:I

    .line 42
    sput-wide v2, Lnet/flixster/android/FlixsterCacheManager;->sCacheByteSize:J

    .line 47
    sput-boolean v0, Lnet/flixster/android/FlixsterCacheManager;->sIsActive:Z

    .line 20
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 4
    .parameter "applicationContext"

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lnet/flixster/android/FlixsterCacheManager;->mContext:Landroid/content/Context;

    .line 51
    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0xa

    const/high16 v2, 0x3f00

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    sput-object v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheHash:Ljava/util/HashMap;

    .line 53
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lnet/flixster/android/FlixsterCacheManager;->sTimer:Ljava/util/Timer;

    .line 55
    new-instance v0, Lnet/flixster/android/FlixsterCacheManager$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterCacheManager$1;-><init>(Lnet/flixster/android/FlixsterCacheManager;)V

    sput-object v0, Lnet/flixster/android/FlixsterCacheManager;->sTimerTask:Ljava/util/TimerTask;

    .line 85
    sget-object v0, Lnet/flixster/android/FlixsterCacheManager;->sTimer:Ljava/util/Timer;

    sget-object v1, Lnet/flixster/android/FlixsterCacheManager;->sTimerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 87
    return-void
.end method

.method private AppendCacheItem(Lnet/flixster/android/FlixsterCacheManager$CacheItem;)V
    .locals 4
    .parameter "item"

    .prologue
    .line 256
    sget-object v0, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    if-nez v0, :cond_0

    .line 257
    sput-object p1, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    .line 258
    sget-object v0, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    sput-object v0, Lnet/flixster/android/FlixsterCacheManager;->sTail:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    .line 265
    :goto_0
    sget-object v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheHash:Ljava/util/HashMap;

    iget v1, p1, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->key:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lnet/flixster/android/FlixsterCacheManager;->sTail:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-wide v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheByteSize:J

    iget-wide v2, p1, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->size:J

    add-long/2addr v0, v2

    sput-wide v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheByteSize:J

    .line 267
    sget v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheItemSize:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheItemSize:I

    .line 268
    return-void

    .line 260
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p1, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->next:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    .line 261
    sget-object v0, Lnet/flixster/android/FlixsterCacheManager;->sTail:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    iput-object v0, p1, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->prev:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    .line 262
    sget-object v0, Lnet/flixster/android/FlixsterCacheManager;->sTail:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    iput-object p1, v0, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->next:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    .line 263
    sput-object p1, Lnet/flixster/android/FlixsterCacheManager;->sTail:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    goto :goto_0
.end method

.method private static DecapCacheItem(Lnet/flixster/android/FlixsterCacheManager$CacheItem;)V
    .locals 4
    .parameter "topItem"

    .prologue
    const/4 v3, 0x0

    .line 273
    if-nez p0, :cond_0

    .line 274
    const-string v0, "FlxMain"

    const-string v1, "FlixsterCacheManager.DecapCacheItem topItem is null"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :goto_0
    return-void

    .line 276
    :cond_0
    sget-object v0, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    sget-object v1, Lnet/flixster/android/FlixsterCacheManager;->sTail:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    if-ne v0, v1, :cond_1

    .line 277
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterCacheManager.DecapCacheItem empty? mHead:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tail:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lnet/flixster/android/FlixsterCacheManager;->sTail:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    sput-object v3, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    .line 279
    sput-object v3, Lnet/flixster/android/FlixsterCacheManager;->sTail:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    .line 284
    :goto_1
    sget-object v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheHash:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-wide v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheByteSize:J

    iget-wide v2, p0, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->size:J

    sub-long/2addr v0, v2

    sput-wide v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheByteSize:J

    .line 286
    sget v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheItemSize:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheItemSize:I

    goto :goto_0

    .line 281
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->next:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    sput-object v0, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    .line 282
    sget-object v0, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    iput-object v3, v0, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->prev:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    goto :goto_1
.end method

.method public static FilenameToHash(Ljava/lang/String;)I
    .locals 3
    .parameter "filename"

    .prologue
    .line 298
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 299
    .local v0, numberString:Ljava/lang/String;
    const-string v1, "_"

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1
.end method

.method public static HashToFilename(I)Ljava/lang/String;
    .locals 4
    .parameter "hashCode"

    .prologue
    .line 294
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cache"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2d

    const/16 v3, 0x5f

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static SetCacheLimit()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 90
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterCacheManager SetCacheLimit() before sCacheLimit:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v2, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " getCachePolicy:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 91
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCachePolicy()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCachePolicy()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 106
    sput-wide v4, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    .line 108
    :goto_0
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FlixsterCacheManager SetCacheLimit() after sCacheLimit:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v2, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    return-void

    .line 94
    :pswitch_0
    sput-wide v4, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    goto :goto_0

    .line 97
    :pswitch_1
    const-wide/32 v0, 0x1e8480

    sput-wide v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    goto :goto_0

    .line 100
    :pswitch_2
    const-wide/32 v0, 0x3d0900

    sput-wide v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    goto :goto_0

    .line 103
    :pswitch_3
    const-wide/32 v0, 0x7a1200

    sput-wide v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static declared-synchronized TrimCache(J)V
    .locals 9
    .parameter "trimSize"

    .prologue
    const-wide/16 v7, 0x1388

    .line 178
    const-class v2, Lnet/flixster/android/FlixsterCacheManager;

    monitor-enter v2

    :try_start_0
    sget-wide v3, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    cmp-long v1, p0, v3

    if-lez v1, :cond_1

    .line 179
    const-string v1, "FlxMain"

    const-string v3, "trimSize size exceeds cache size!!"

    invoke-static {v1, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 185
    :cond_1
    :try_start_1
    sget-wide v3, Lnet/flixster/android/FlixsterCacheManager;->sCacheByteSize:J

    add-long/2addr v3, p0

    add-long/2addr v3, v7

    sget-wide v5, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    cmp-long v1, v3, v5

    if-lez v1, :cond_0

    .line 186
    :goto_1
    sget-wide v3, Lnet/flixster/android/FlixsterCacheManager;->sCacheByteSize:J

    add-long/2addr v3, p0

    add-long/2addr v3, v7

    sget-wide v5, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    cmp-long v1, v3, v5

    if-lez v1, :cond_2

    sget-object v1, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    if-nez v1, :cond_3

    .line 194
    :cond_2
    const-string v1, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FlixsterCacheManager.put TRIM, mCacheByteSize:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v4, Lnet/flixster/android/FlixsterCacheManager;->sCacheByteSize:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mCacheItemSize:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 195
    sget v4, Lnet/flixster/android/FlixsterCacheManager;->sCacheItemSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sCacheLimit:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-wide v4, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 194
    invoke-static {v1, v3}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 178
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 188
    :cond_3
    :try_start_2
    new-instance v0, Ljava/io/File;

    sget-object v1, Lnet/flixster/android/FlixsterCacheManager;->sCacheDir:Ljava/io/File;

    sget-object v3, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    iget v3, v3, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->key:I

    invoke-static {v3}, Lnet/flixster/android/FlixsterCacheManager;->HashToFilename(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 189
    .local v0, fileObj:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 192
    sget-object v1, Lnet/flixster/android/FlixsterCacheManager;->sHead:Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    invoke-static {v1}, Lnet/flixster/android/FlixsterCacheManager;->DecapCacheItem(Lnet/flixster/android/FlixsterCacheManager$CacheItem;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method static synthetic access$0(Ljava/io/File;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    sput-object p0, Lnet/flixster/android/FlixsterCacheManager;->sCacheDir:Ljava/io/File;

    return-void
.end method

.method static synthetic access$1()Ljava/io/File;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lnet/flixster/android/FlixsterCacheManager;->sCacheDir:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$2(Z)V
    .locals 0
    .parameter

    .prologue
    .line 47
    sput-boolean p0, Lnet/flixster/android/FlixsterCacheManager;->sIsActive:Z

    return-void
.end method


# virtual methods
.method public buildCacheIndex()V
    .locals 8

    .prologue
    .line 112
    const-string v4, "FlxMain"

    const-string v5, "FlixsterCacheManager.buildCacheIndex start"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v4, p0, Lnet/flixster/android/FlixsterCacheManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    sput-object v4, Lnet/flixster/android/FlixsterCacheManager;->sCacheDir:Ljava/io/File;

    .line 116
    sget-object v4, Lnet/flixster/android/FlixsterCacheManager;->sCacheDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, files:[Ljava/lang/String;
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FlixsterCacheManager.buildCacheIndex files:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    if-eqz v2, :cond_0

    .line 121
    array-length v5, v2

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_1

    .line 137
    :cond_0
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FlixsterCacheManager.buildCacheIndex mCacheByteSize:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v6, Lnet/flixster/android/FlixsterCacheManager;->sCacheByteSize:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mCacheItemSize:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 138
    sget v6, Lnet/flixster/android/FlixsterCacheManager;->sCacheItemSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sCacheLimit:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-wide v6, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 137
    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void

    .line 121
    :cond_1
    aget-object v0, v2, v4

    .line 125
    .local v0, file:Ljava/lang/String;
    const-string v6, "cache"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 126
    new-instance v1, Ljava/io/File;

    sget-object v6, Lnet/flixster/android/FlixsterCacheManager;->sCacheDir:Ljava/io/File;

    invoke-direct {v1, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 128
    .local v1, fileObj:Ljava/io/File;
    new-instance v3, Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    invoke-direct {v3, p0}, Lnet/flixster/android/FlixsterCacheManager$CacheItem;-><init>(Lnet/flixster/android/FlixsterCacheManager;)V

    .line 129
    .local v3, tempItem:Lnet/flixster/android/FlixsterCacheManager$CacheItem;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    iput-wide v6, v3, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->size:J

    .line 130
    const/4 v6, 0x2

    iput v6, v3, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->state:I

    .line 131
    invoke-static {v0}, Lnet/flixster/android/FlixsterCacheManager;->FilenameToHash(Ljava/lang/String;)I

    move-result v6

    iput v6, v3, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->key:I

    .line 132
    invoke-direct {p0, v3}, Lnet/flixster/android/FlixsterCacheManager;->AppendCacheItem(Lnet/flixster/android/FlixsterCacheManager$CacheItem;)V

    .line 121
    .end local v1           #fileObj:Ljava/io/File;
    .end local v3           #tempItem:Lnet/flixster/android/FlixsterCacheManager$CacheItem;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public get(I)[B
    .locals 9
    .parameter "urlHashCode"

    .prologue
    const/4 v4, 0x0

    .line 143
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "cache"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x2d

    const/16 v8, 0x5f

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 147
    .local v2, filename:Ljava/lang/String;
    sget-boolean v5, Lnet/flixster/android/FlixsterCacheManager;->sIsActive:Z

    if-nez v5, :cond_0

    .line 148
    const-string v5, "FlxMain"

    const-string v6, "FlixsterCacheManager.get NOT ACTIVE YET"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v4

    .line 171
    :goto_0
    return-object v3

    .line 152
    :cond_0
    :try_start_0
    sget-object v5, Lnet/flixster/android/FlixsterCacheManager;->sCacheHash:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 155
    new-instance v1, Ljava/io/File;

    sget-object v5, Lnet/flixster/android/FlixsterCacheManager;->sCacheDir:Ljava/io/File;

    invoke-direct {v1, v5, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 156
    .local v1, fileObj:Ljava/io/File;
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    sput-object v5, Lnet/flixster/android/FlixsterCacheManager;->sFileInputStream:Ljava/io/FileInputStream;

    .line 157
    sget-object v5, Lnet/flixster/android/FlixsterCacheManager;->sFileInputStream:Ljava/io/FileInputStream;

    invoke-static {v5}, Lnet/flixster/android/util/HttpHelper;->streamToByteArray(Ljava/io/InputStream;)[B

    move-result-object v3

    .line 158
    .local v3, results:[B
    sget-object v5, Lnet/flixster/android/FlixsterCacheManager;->sFileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 165
    .end local v1           #fileObj:Ljava/io/File;
    .end local v3           #results:[B
    :catch_0
    move-exception v0

    .line 166
    .local v0, e:Ljava/io/FileNotFoundException;
    const-string v5, "FlxMain"

    const-string v6, "FlixsterCacheManager.get"

    invoke-static {v5, v6, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v0           #e:Ljava/io/FileNotFoundException;
    :cond_1
    :goto_1
    move-object v3, v4

    .line 171
    goto :goto_0

    .line 167
    :catch_1
    move-exception v0

    .line 168
    .local v0, e:Ljava/io/IOException;
    const-string v5, "FlxMain"

    const-string v6, "FlixsterCacheManager.get"

    invoke-static {v5, v6, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public declared-synchronized put(I[B)V
    .locals 8
    .parameter "urlHashCode"
    .parameter "byteArray"

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cache"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x2d

    const/16 v7, 0x5f

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 205
    .local v2, filename:Ljava/lang/String;
    sget-boolean v4, Lnet/flixster/android/FlixsterCacheManager;->sIsActive:Z

    if-nez v4, :cond_1

    .line 206
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FlixsterCacheManager.put NOT ACTIVE YET byteArray.length:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v6, p2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 251
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 210
    :cond_1
    :try_start_1
    sget-wide v4, Lnet/flixster/android/FlixsterCacheManager;->sCacheLimit:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 213
    array-length v4, p2

    int-to-long v4, v4

    const-wide/16 v6, 0x1388

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Lnet/flixster/android/FlixsterCacheManager;->TrimCache(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 222
    :try_start_2
    new-instance v1, Ljava/io/File;

    sget-object v4, Lnet/flixster/android/FlixsterCacheManager;->sCacheDir:Ljava/io/File;

    invoke-direct {v1, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 223
    .local v1, fileObj:Ljava/io/File;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    sput-object v4, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;

    .line 225
    iget-object v4, p0, Lnet/flixster/android/FlixsterCacheManager;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-virtual {v4, v2, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    sput-object v4, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;

    .line 226
    sget-object v4, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;

    const/4 v5, 0x0

    array-length v6, p2

    invoke-virtual {v4, p2, v5, v6}, Ljava/io/FileOutputStream;->write([BII)V

    .line 227
    sget-object v4, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    .line 229
    new-instance v3, Lnet/flixster/android/FlixsterCacheManager$CacheItem;

    invoke-direct {v3, p0}, Lnet/flixster/android/FlixsterCacheManager$CacheItem;-><init>(Lnet/flixster/android/FlixsterCacheManager;)V

    .line 230
    .local v3, tempItem:Lnet/flixster/android/FlixsterCacheManager$CacheItem;
    iput p1, v3, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->key:I

    .line 231
    const/4 v4, 0x2

    iput v4, v3, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->state:I

    .line 232
    array-length v4, p2

    int-to-long v4, v4

    iput-wide v4, v3, Lnet/flixster/android/FlixsterCacheManager$CacheItem;->size:J

    .line 233
    invoke-direct {p0, v3}, Lnet/flixster/android/FlixsterCacheManager;->AppendCacheItem(Lnet/flixster/android/FlixsterCacheManager$CacheItem;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 243
    :try_start_3
    sget-object v4, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v4, :cond_0

    .line 245
    :try_start_4
    sget-object v4, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 246
    :catch_0
    move-exception v4

    goto :goto_0

    .line 236
    .end local v1           #fileObj:Ljava/io/File;
    .end local v3           #tempItem:Lnet/flixster/android/FlixsterCacheManager$CacheItem;
    :catch_1
    move-exception v0

    .line 237
    .local v0, e:Ljava/io/FileNotFoundException;
    :try_start_5
    const-string v4, "FlxMain"

    const-string v5, "FlixsterCacheManager put FileNotFoundException"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 243
    :try_start_6
    sget-object v4, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v4, :cond_0

    .line 245
    :try_start_7
    sget-object v4, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_0

    .line 246
    :catch_2
    move-exception v4

    goto :goto_0

    .line 239
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v0

    .line 240
    .local v0, e:Ljava/io/IOException;
    :try_start_8
    const-string v4, "FlxMain"

    const-string v5, "FlixsterCacheManager put IOException"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 243
    :try_start_9
    sget-object v4, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v4, :cond_0

    .line 245
    :try_start_a
    sget-object v4, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    goto :goto_0

    .line 246
    :catch_4
    move-exception v4

    goto :goto_0

    .line 242
    .end local v0           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 243
    :try_start_b
    sget-object v5, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    if-eqz v5, :cond_2

    .line 245
    :try_start_c
    sget-object v5, Lnet/flixster/android/FlixsterCacheManager;->sFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5

    .line 250
    :cond_2
    :goto_1
    :try_start_d
    throw v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 200
    .end local v2           #filename:Ljava/lang/String;
    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4

    .line 246
    .restart local v2       #filename:Ljava/lang/String;
    :catch_5
    move-exception v5

    goto :goto_1
.end method
