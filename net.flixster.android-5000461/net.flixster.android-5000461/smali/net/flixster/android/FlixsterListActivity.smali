.class public Lnet/flixster/android/FlixsterListActivity;
.super Lcom/actionbarsherlock/app/SherlockListActivity;
.source "FlixsterListActivity.java"

# interfaces
.implements Lcom/flixster/android/utils/ImageTask;


# instance fields
.field private final className:Ljava/lang/String;

.field private decorators:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/flixster/android/activity/decorator/ActivityDecorator;",
            ">;"
        }
    .end annotation
.end field

.field protected dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

.field private mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

.field protected mMovieHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockListActivity;-><init>()V

    .line 32
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/FlixsterListActivity;->className:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method protected addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V
    .locals 1
    .parameter "decorator"

    .prologue
    .line 47
    iget-object v0, p0, Lnet/flixster/android/FlixsterListActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 48
    return-void
.end method

.method protected createActionBar()V
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterListActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 142
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 143
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 144
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->decorators:Ljava/util/Collection;

    .line 39
    new-instance v1, Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-direct {v1, p0}, Lcom/flixster/android/activity/decorator/DialogDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {p0, v1}, Lnet/flixster/android/FlixsterListActivity;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 41
    iget-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 44
    return-void

    .line 41
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 42
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onCreate()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .parameter "id"

    .prologue
    .line 134
    iget-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {v1, p1}, Lcom/flixster/android/activity/decorator/DialogDecorator;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 135
    .local v0, dialog:Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .end local v0           #dialog:Landroid/app/Dialog;
    :goto_0
    return-object v0

    .restart local v0       #dialog:Landroid/app/Dialog;
    :cond_0
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockListActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 97
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListActivity;->onDestroy()V

    .line 99
    iget-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 103
    const/4 v1, 0x0

    iput-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->mMovieHash:Ljava/util/HashMap;

    .line 104
    return-void

    .line 99
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 100
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onDestroy()V

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListActivity;->onLowMemory()V

    .line 52
    const-string v0, "FlxMain"

    const-string v1, "Low memory!"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->clearBitmapCache()V

    .line 54
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 153
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 160
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockListActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 156
    :pswitch_0
    invoke-static {p0}, Lcom/flixster/android/bootstrap/BootstrapActivity;->getMainIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/support/v4/app/NavUtils;->navigateUpTo(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 157
    const/4 v0, 0x1

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 78
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListActivity;->onPause()V

    .line 80
    iget-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 84
    iget-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v1}, Lcom/flixster/android/utils/ImageTaskImpl;->stopTask()V

    .line 86
    const/4 v1, 0x0

    iput-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    .line 88
    :cond_0
    return-void

    .line 80
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 81
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onPause()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 58
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListActivity;->onResume()V

    .line 59
    const-string v1, "FlxMain"

    const-string v2, "FlixsterListActivity.onResume"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->decorators:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 65
    iget-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->mMovieHash:Ljava/util/HashMap;

    if-nez v1, :cond_0

    .line 66
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->mMovieHash:Ljava/util/HashMap;

    .line 69
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-nez v1, :cond_1

    .line 70
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lnet/flixster/android/FlixsterListActivity;->className:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".onResume start ImageTask"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    new-instance v1, Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-direct {v1}, Lcom/flixster/android/utils/ImageTaskImpl;-><init>()V

    iput-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    .line 72
    iget-object v1, p0, Lnet/flixster/android/FlixsterListActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v1}, Lcom/flixster/android/utils/ImageTaskImpl;->startTask()V

    .line 74
    :cond_1
    return-void

    .line 61
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/activity/decorator/ActivityDecorator;

    .line 62
    .local v0, decorator:Lcom/flixster/android/activity/decorator/ActivityDecorator;
    invoke-interface {v0}, Lcom/flixster/android/activity/decorator/ActivityDecorator;->onResume()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListActivity;->onStop()V

    .line 93
    return-void
.end method

.method public orderImage(Lnet/flixster/android/model/ImageOrder;)V
    .locals 0
    .parameter "io"

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lnet/flixster/android/FlixsterListActivity;->orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 108
    return-void
.end method

.method public orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V
    .locals 1
    .parameter "io"

    .prologue
    .line 119
    iget-object v0, p0, Lnet/flixster/android/FlixsterListActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lnet/flixster/android/FlixsterListActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v0, p1}, Lcom/flixster/android/utils/ImageTaskImpl;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 122
    :cond_0
    return-void
.end method

.method public orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V
    .locals 1
    .parameter "io"

    .prologue
    .line 112
    iget-object v0, p0, Lnet/flixster/android/FlixsterListActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lnet/flixster/android/FlixsterListActivity;->mImageTask:Lcom/flixster/android/utils/ImageTaskImpl;

    invoke-virtual {v0, p1}, Lcom/flixster/android/utils/ImageTaskImpl;->orderImageLifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 115
    :cond_0
    return-void
.end method

.method protected setActionBarTitle(I)V
    .locals 2
    .parameter "resId"

    .prologue
    .line 147
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterListActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 148
    .local v0, bar:Lcom/actionbarsherlock/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 149
    invoke-virtual {v0, p1}, Lcom/actionbarsherlock/app/ActionBar;->setTitle(I)V

    .line 150
    return-void
.end method

.method public showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V
    .locals 1
    .parameter "id"
    .parameter "listener"

    .prologue
    .line 128
    iget-object v0, p0, Lnet/flixster/android/FlixsterListActivity;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    invoke-virtual {v0, p1, p2}, Lcom/flixster/android/activity/decorator/DialogDecorator;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 129
    return-void
.end method
