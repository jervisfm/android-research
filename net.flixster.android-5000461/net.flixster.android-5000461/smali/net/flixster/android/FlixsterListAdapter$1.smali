.class Lnet/flixster/android/FlixsterListAdapter$1;
.super Ljava/lang/Object;
.source "FlixsterListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FlixsterListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FlixsterListAdapter;


# direct methods
.method constructor <init>(Lnet/flixster/android/FlixsterListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FlixsterListAdapter$1;->this$0:Lnet/flixster/android/FlixsterListAdapter;

    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 408
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    .line 409
    .local v1, movie:Lnet/flixster/android/model/Movie;
    if-eqz v1, :cond_0

    .line 410
    iget-object v2, p0, Lnet/flixster/android/FlixsterListAdapter$1;->this$0:Lnet/flixster/android/FlixsterListAdapter;

    #calls: Lnet/flixster/android/FlixsterListAdapter;->getMovieIntent(Lnet/flixster/android/model/Movie;)Landroid/content/Intent;
    invoke-static {v2, v1}, Lnet/flixster/android/FlixsterListAdapter;->access$0(Lnet/flixster/android/FlixsterListAdapter;Lnet/flixster/android/model/Movie;)Landroid/content/Intent;

    move-result-object v0

    .line 411
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Lnet/flixster/android/FlixsterListAdapter$1;->this$0:Lnet/flixster/android/FlixsterListAdapter;

    iget-object v2, v2, Lnet/flixster/android/FlixsterListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    invoke-virtual {v2, v0}, Lnet/flixster/android/FlixsterListActivity;->startActivity(Landroid/content/Intent;)V

    .line 413
    .end local v0           #intent:Landroid/content/Intent;
    :cond_0
    return-void
.end method
