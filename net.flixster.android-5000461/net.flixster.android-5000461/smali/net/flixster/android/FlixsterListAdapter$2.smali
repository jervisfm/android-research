.class Lnet/flixster/android/FlixsterListAdapter$2;
.super Ljava/lang/Object;
.source "FlixsterListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FlixsterListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FlixsterListAdapter;


# direct methods
.method constructor <init>(Lnet/flixster/android/FlixsterListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FlixsterListAdapter$2;->this$0:Lnet/flixster/android/FlixsterListAdapter;

    .line 416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 418
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Review;

    .line 419
    .local v2, review:Lnet/flixster/android/model/Review;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 420
    invoke-virtual {v2}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v1

    .line 421
    .local v1, movie:Lnet/flixster/android/model/Movie;
    iget-object v3, p0, Lnet/flixster/android/FlixsterListAdapter$2;->this$0:Lnet/flixster/android/FlixsterListAdapter;

    #calls: Lnet/flixster/android/FlixsterListAdapter;->getMovieIntent(Lnet/flixster/android/model/Movie;)Landroid/content/Intent;
    invoke-static {v3, v1}, Lnet/flixster/android/FlixsterListAdapter;->access$0(Lnet/flixster/android/FlixsterListAdapter;Lnet/flixster/android/model/Movie;)Landroid/content/Intent;

    move-result-object v0

    .line 422
    .local v0, intent:Landroid/content/Intent;
    iget-object v3, p0, Lnet/flixster/android/FlixsterListAdapter$2;->this$0:Lnet/flixster/android/FlixsterListAdapter;

    iget-object v3, v3, Lnet/flixster/android/FlixsterListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    invoke-virtual {v3, v0}, Lnet/flixster/android/FlixsterListActivity;->startActivity(Landroid/content/Intent;)V

    .line 424
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #movie:Lnet/flixster/android/model/Movie;
    :cond_0
    return-void
.end method
