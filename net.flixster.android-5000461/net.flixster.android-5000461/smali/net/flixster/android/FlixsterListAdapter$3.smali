.class Lnet/flixster/android/FlixsterListAdapter$3;
.super Landroid/os/Handler;
.source "FlixsterListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FlixsterListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FlixsterListAdapter;


# direct methods
.method constructor <init>(Lnet/flixster/android/FlixsterListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FlixsterListAdapter$3;->this$0:Lnet/flixster/android/FlixsterListAdapter;

    .line 470
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "message"

    .prologue
    .line 473
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/widget/ImageView;

    .line 474
    .local v1, imageView:Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 475
    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Movie;

    .line 476
    .local v2, movie:Lnet/flixster/android/model/Movie;
    if-eqz v2, :cond_0

    .line 477
    iget-object v3, v2, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 478
    .local v0, bitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 479
    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 482
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v2           #movie:Lnet/flixster/android/model/Movie;
    :cond_0
    return-void
.end method
