.class public final Lnet/flixster/android/FlixsterListAdapter$MovieViewHolder;
.super Ljava/lang/Object;
.source "FlixsterListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FlixsterListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "MovieViewHolder"
.end annotation


# instance fields
.field actorsView:Landroid/widget/TextView;

.field friendScore:Landroid/widget/TextView;

.field metaView:Landroid/widget/TextView;

.field movieLayout:Landroid/widget/RelativeLayout;

.field releaseView:Landroid/widget/TextView;

.field scoreView:Landroid/widget/TextView;

.field thumbnailView:Landroid/widget/ImageView;

.field titleView:Landroid/widget/TextView;

.field trailerView:Landroid/widget/ImageView;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
