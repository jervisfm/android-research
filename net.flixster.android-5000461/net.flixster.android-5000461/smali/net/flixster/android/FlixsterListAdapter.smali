.class public abstract Lnet/flixster/android/FlixsterListAdapter;
.super Landroid/widget/BaseAdapter;
.source "FlixsterListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/FlixsterListAdapter$MovieViewHolder;,
        Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;,
        Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;
    }
.end annotation


# static fields
.field protected static final VIEW_TYPE_ADVIEW:I = 0x7

.field protected static final VIEW_TYPE_LETTER:I = 0x8

.field protected static final VIEW_TYPE_MOVIE:I = 0x4

.field protected static final VIEW_TYPE_REVIEW:I = 0x5

.field protected static final VIEW_TYPE_TEXT:I = 0x2

.field protected static final VIEW_TYPE_TITLE:I = 0x1

.field protected static final VIEW_TYPE_UNSUPPORTED:I = 0x0

.field protected static final VIEW_TYPE_USER:I = 0x3

.field protected static final VIEW_TYPE_VIEW:I = 0x6

.field protected static data:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected context:Lnet/flixster/android/FlixsterListActivity;

.field protected inflater:Landroid/view/LayoutInflater;

.field protected movieClickListener:Landroid/view/View$OnClickListener;

.field private final movieThumbnailHandler:Landroid/os/Handler;

.field protected resources:Landroid/content/res/Resources;

.field protected reviewClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lnet/flixster/android/FlixsterListActivity;Ljava/util/ArrayList;)V
    .locals 2
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/flixster/android/FlixsterListActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p2, data:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 406
    new-instance v1, Lnet/flixster/android/FlixsterListAdapter$1;

    invoke-direct {v1, p0}, Lnet/flixster/android/FlixsterListAdapter$1;-><init>(Lnet/flixster/android/FlixsterListAdapter;)V

    iput-object v1, p0, Lnet/flixster/android/FlixsterListAdapter;->movieClickListener:Landroid/view/View$OnClickListener;

    .line 416
    new-instance v1, Lnet/flixster/android/FlixsterListAdapter$2;

    invoke-direct {v1, p0}, Lnet/flixster/android/FlixsterListAdapter$2;-><init>(Lnet/flixster/android/FlixsterListAdapter;)V

    iput-object v1, p0, Lnet/flixster/android/FlixsterListAdapter;->reviewClickListener:Landroid/view/View$OnClickListener;

    .line 470
    new-instance v1, Lnet/flixster/android/FlixsterListAdapter$3;

    invoke-direct {v1, p0}, Lnet/flixster/android/FlixsterListAdapter$3;-><init>(Lnet/flixster/android/FlixsterListAdapter;)V

    iput-object v1, p0, Lnet/flixster/android/FlixsterListAdapter;->movieThumbnailHandler:Landroid/os/Handler;

    .line 50
    iput-object p1, p0, Lnet/flixster/android/FlixsterListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    .line 51
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/FlixsterListAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 52
    invoke-virtual {p1}, Lnet/flixster/android/FlixsterListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/FlixsterListAdapter;->resources:Landroid/content/res/Resources;

    .line 53
    sput-object p2, Lnet/flixster/android/FlixsterListAdapter;->data:Ljava/util/ArrayList;

    .line 54
    invoke-virtual {p1}, Lnet/flixster/android/FlixsterListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 55
    .local v0, listView:Landroid/widget/ListView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAddStatesFromChildren(Z)V

    .line 56
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 57
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/FlixsterListAdapter;Lnet/flixster/android/model/Movie;)Landroid/content/Intent;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 448
    invoke-direct {p0, p1}, Lnet/flixster/android/FlixsterListAdapter;->getMovieIntent(Lnet/flixster/android/model/Movie;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getMovieIntent(Lnet/flixster/android/model/Movie;)Landroid/content/Intent;
    .locals 8
    .parameter "movie"

    .prologue
    const/4 v4, 0x0

    .line 449
    new-instance v0, Landroid/content/Intent;

    const-string v3, "DETAILS"

    const/4 v5, 0x0

    iget-object v6, p0, Lnet/flixster/android/FlixsterListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    const-class v7, Lnet/flixster/android/MovieDetails;

    invoke-direct {v0, v3, v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 450
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v5

    invoke-virtual {v0, v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 451
    const-string v3, "MOVIE_IN_THEATER_FLAG"

    iget-boolean v5, p1, Lnet/flixster/android/model/Movie;->isMIT:Z

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 452
    const-string v5, "MOVIE_THUMBNAIL"

    iget-object v3, p1, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 453
    const/16 v3, 0xc

    new-array v1, v3, [Ljava/lang/String;

    const-string v3, "title"

    aput-object v3, v1, v4

    const/4 v3, 0x1

    const-string v5, "high"

    aput-object v5, v1, v3

    const/4 v3, 0x2

    const-string v5, "mpaa"

    aput-object v5, v1, v3

    const/4 v3, 0x3

    .line 454
    const-string v5, "MOVIE_ACTORS"

    aput-object v5, v1, v3

    const/4 v3, 0x4

    const-string v5, "thumbnail"

    aput-object v5, v1, v3

    const/4 v3, 0x5

    const-string v5, "runningTime"

    aput-object v5, v1, v3

    const/4 v3, 0x6

    const-string v5, "directors"

    aput-object v5, v1, v3

    const/4 v3, 0x7

    .line 455
    const-string v5, "theaterReleaseDate"

    aput-object v5, v1, v3

    const/16 v3, 0x8

    const-string v5, "genre"

    aput-object v5, v1, v3

    const/16 v3, 0x9

    const-string v5, "status"

    aput-object v5, v1, v3

    const/16 v3, 0xa

    .line 456
    const-string v5, "MOVIE_ACTORS_SHORT"

    aput-object v5, v1, v3

    const/16 v3, 0xb

    const-string v5, "meta"

    aput-object v5, v1, v3

    .line 457
    .local v1, movieProperties:[Ljava/lang/String;
    array-length v5, v1

    move v3, v4

    :goto_0
    if-lt v3, v5, :cond_2

    .line 460
    const-string v3, "boxOffice"

    const-string v4, "boxOffice"

    invoke-virtual {p1, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 461
    const-string v3, "popcornScore"

    invoke-virtual {p1, v3}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 462
    const-string v3, "popcornScore"

    const-string v4, "popcornScore"

    invoke-virtual {p1, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 464
    :cond_0
    const-string v3, "rottenTomatoes"

    invoke-virtual {p1, v3}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 465
    const-string v3, "rottenTomatoes"

    const-string v4, "rottenTomatoes"

    invoke-virtual {p1, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 467
    :cond_1
    return-object v0

    .line 457
    :cond_2
    aget-object v2, v1, v3

    .line 458
    .local v2, propertyKey:Ljava/lang/String;
    invoke-virtual {p1, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lnet/flixster/android/FlixsterListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 68
    sget-object v0, Lnet/flixster/android/FlixsterListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 72
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4
    .parameter "position"

    .prologue
    .line 86
    sget-object v1, Lnet/flixster/android/FlixsterListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 87
    .local v0, dataObject:Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 88
    if-nez p1, :cond_0

    .line 89
    const/4 v1, 0x1

    .line 103
    :goto_0
    return v1

    .line 91
    :cond_0
    const/4 v1, 0x2

    goto :goto_0

    .line 93
    :cond_1
    instance-of v1, v0, Lnet/flixster/android/model/User;

    if-eqz v1, :cond_2

    .line 94
    const/4 v1, 0x3

    goto :goto_0

    .line 95
    :cond_2
    instance-of v1, v0, Lnet/flixster/android/model/Review;

    if-eqz v1, :cond_3

    .line 96
    const/4 v1, 0x5

    goto :goto_0

    .line 97
    :cond_3
    instance-of v1, v0, Lnet/flixster/android/ads/AdView;

    if-eqz v1, :cond_4

    .line 98
    const/4 v1, 0x7

    goto :goto_0

    .line 99
    :cond_4
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_5

    .line 100
    const/4 v1, 0x6

    goto :goto_0

    .line 102
    :cond_5
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FlixsterListAdapter.getItemViewType UNSUPPORTED TYPE position:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getProfileView(Lnet/flixster/android/model/User;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .parameter "user"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 132
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;

    if-nez v8, :cond_1

    .line 133
    :cond_0
    iget-object v8, p0, Lnet/flixster/android/FlixsterListAdapter;->inflater:Landroid/view/LayoutInflater;

    const v9, 0x7f03006e

    const/4 v10, 0x0

    invoke-virtual {v8, v9, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 135
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;

    .line 136
    .local v6, viewHolder:Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;
    if-nez v6, :cond_2

    .line 137
    new-instance v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;

    .end local v6           #viewHolder:Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;
    const/4 v8, 0x0

    invoke-direct {v6, v8}, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;-><init>(Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;)V

    .line 138
    .restart local v6       #viewHolder:Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;
    const v8, 0x7f07020a

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->thumbnailView:Landroid/widget/ImageView;

    .line 139
    const v8, 0x7f07020b

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->thumbnailFrameView:Landroid/widget/ImageView;

    .line 140
    const v8, 0x7f07020c

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->titleView:Landroid/widget/TextView;

    .line 141
    const v8, 0x7f0700a5

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->friendView:Landroid/widget/TextView;

    .line 142
    const v8, 0x7f0700a4

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->ratingView:Landroid/widget/TextView;

    .line 143
    const v8, 0x7f0700a3

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->wtsView:Landroid/widget/TextView;

    .line 144
    const v8, 0x7f0700a6

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->collectionsView:Landroid/widget/TextView;

    .line 145
    invoke-virtual {p2, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 149
    :cond_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v4

    .line 150
    .local v4, platform:Ljava/lang/String;
    const-string v8, "FLX"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 151
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->thumbnailFrameView:Landroid/widget/ImageView;

    const v9, 0x7f0200bc

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 156
    :cond_3
    :goto_0
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {p1, v8}, Lnet/flixster/android/model/User;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 157
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    .line 158
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 161
    :cond_4
    iget-object v2, p1, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    .line 162
    .local v2, displayName:Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 163
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->titleView:Landroid/widget/TextView;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->titleView:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    :cond_5
    iget v3, p1, Lnet/flixster/android/model/User;->friendCount:I

    .line 167
    .local v3, friendCount:I
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->friendView:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, ""

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 168
    iget-object v10, p0, Lnet/flixster/android/FlixsterListAdapter;->resources:Landroid/content/res/Resources;

    const v11, 0x7f0c0082

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 167
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget v5, p1, Lnet/flixster/android/model/User;->ratingCount:I

    .line 170
    .local v5, ratingCount:I
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->ratingView:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, ""

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 171
    iget-object v10, p0, Lnet/flixster/android/FlixsterListAdapter;->resources:Landroid/content/res/Resources;

    const v11, 0x7f0c0083

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 170
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget v7, p1, Lnet/flixster/android/model/User;->wtsCount:I

    .line 173
    .local v7, wtsCount:I
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->wtsView:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, ""

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 174
    iget-object v10, p0, Lnet/flixster/android/FlixsterListAdapter;->resources:Landroid/content/res/Resources;

    const v11, 0x7f0c0084

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 173
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget v1, p1, Lnet/flixster/android/model/User;->collectionsCount:I

    .line 176
    .local v1, collectionsCount:I
    if-lez v1, :cond_6

    .line 177
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->collectionsView:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, ""

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 178
    iget-object v10, p0, Lnet/flixster/android/FlixsterListAdapter;->resources:Landroid/content/res/Resources;

    const v11, 0x7f0c0085

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 177
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->collectionsView:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    :cond_6
    return-object p2

    .line 152
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v1           #collectionsCount:I
    .end local v2           #displayName:Ljava/lang/String;
    .end local v3           #friendCount:I
    .end local v5           #ratingCount:I
    .end local v7           #wtsCount:I
    :cond_7
    const-string v8, "FBK"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 153
    iget-object v8, v6, Lnet/flixster/android/FlixsterListAdapter$ProfileViewHolder;->thumbnailFrameView:Landroid/widget/ImageView;

    const v9, 0x7f020149

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method

.method protected getReviewView(Lnet/flixster/android/model/Review;ZLandroid/view/View;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 13
    .parameter "review"
    .parameter "showAuthor"
    .parameter "convertView"
    .parameter "parent"
    .parameter "clickListener"

    .prologue
    .line 187
    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;

    if-nez v2, :cond_1

    .line 188
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/FlixsterListAdapter;->inflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030059

    const/4 v6, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v2, v5, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 190
    :cond_1
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;

    .line 191
    .local v8, viewHolder:Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;
    if-nez v8, :cond_2

    .line 192
    new-instance v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;

    .end local v8           #viewHolder:Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;
    invoke-direct {v8}, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;-><init>()V

    .line 193
    .restart local v8       #viewHolder:Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;
    const v2, 0x7f070139

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->reviewLayout:Landroid/widget/RelativeLayout;

    .line 194
    const v2, 0x7f07013e

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->authorView:Landroid/widget/TextView;

    .line 195
    const v2, 0x7f07013d

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->starsView:Landroid/widget/ImageView;

    .line 196
    const v2, 0x7f07013f

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->commentView:Landroid/widget/TextView;

    .line 197
    const v2, 0x7f07013b

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->titleView:Landroid/widget/TextView;

    .line 198
    const v2, 0x7f07013a

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->movieView:Landroid/widget/ImageView;

    .line 199
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 203
    :cond_2
    if-eqz p2, :cond_3

    .line 204
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->authorView:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "by "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p1, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->authorView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 207
    :cond_3
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->starsView:Landroid/widget/ImageView;

    sget-object v5, Lnet/flixster/android/Flixster;->RATING_SMALL_R:[I

    iget-wide v9, p1, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v11, 0x4000

    mul-double/2addr v9, v11

    double-to-int v6, v9

    aget v5, v5, v6

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 208
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->commentView:Landroid/widget/TextView;

    iget-object v5, p1, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    invoke-virtual {p1}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v3

    .line 211
    .local v3, movie:Lnet/flixster/android/model/Movie;
    if-eqz v3, :cond_4

    .line 212
    const-string v2, "title"

    invoke-virtual {v3, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 213
    .local v7, title:Ljava/lang/String;
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->titleView:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v2, v3, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 215
    iget-object v5, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->movieView:Landroid/widget/ImageView;

    iget-object v2, v3, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 230
    .end local v7           #title:Ljava/lang/String;
    :cond_4
    :goto_0
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->reviewLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 231
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->reviewLayout:Landroid/widget/RelativeLayout;

    const v5, 0x7f030059

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 232
    if-eqz p5, :cond_7

    .line 233
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->reviewLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    :goto_1
    return-object p3

    .line 217
    .restart local v7       #title:Ljava/lang/String;
    :cond_5
    const-string v2, "thumbnail"

    invoke-virtual {v3, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 218
    .local v4, thumbnailUrl:Ljava/lang/String;
    if-eqz v4, :cond_6

    .line 219
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->movieView:Landroid/widget/ImageView;

    const v5, 0x7f020154

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 220
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->movieView:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 221
    new-instance v1, Lnet/flixster/android/model/ImageOrder;

    const/4 v2, 0x0

    .line 222
    iget-object v5, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->movieView:Landroid/widget/ImageView;

    iget-object v6, p0, Lnet/flixster/android/FlixsterListAdapter;->movieThumbnailHandler:Landroid/os/Handler;

    .line 221
    invoke-direct/range {v1 .. v6}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 223
    .local v1, imageOrder:Lnet/flixster/android/model/ImageOrder;
    iget-object v2, p0, Lnet/flixster/android/FlixsterListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    invoke-virtual {v2, v1}, Lnet/flixster/android/FlixsterListActivity;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_0

    .line 225
    .end local v1           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_6
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->movieView:Landroid/widget/ImageView;

    const v5, 0x7f02014f

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 235
    .end local v4           #thumbnailUrl:Ljava/lang/String;
    .end local v7           #title:Ljava/lang/String;
    :cond_7
    iget-object v2, v8, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->reviewLayout:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lnet/flixster/android/FlixsterListAdapter;->reviewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method protected getTextView(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;
    .locals 4
    .parameter "text"
    .parameter "convertView"

    .prologue
    const/16 v3, 0xa

    .line 120
    if-eqz p2, :cond_0

    instance-of v1, p2, Landroid/widget/TextView;

    if-nez v1, :cond_1

    .line 121
    :cond_0
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lnet/flixster/android/FlixsterListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 122
    .local v0, textView:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v1, p0, Lnet/flixster/android/FlixsterListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    invoke-virtual {v1}, Lnet/flixster/android/FlixsterListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 124
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 125
    move-object p2, v0

    .line 127
    .end local v0           #textView:Landroid/widget/TextView;
    :cond_1
    return-object p2
.end method

.method protected getTitleView(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;
    .locals 4
    .parameter "title"
    .parameter "convertView"

    .prologue
    const/16 v3, 0xa

    .line 108
    if-eqz p2, :cond_0

    instance-of v1, p2, Landroid/widget/TextView;

    if-nez v1, :cond_1

    .line 109
    :cond_0
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lnet/flixster/android/FlixsterListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 110
    .local v0, titleView:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    const v1, 0x7f0201d4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 112
    iget-object v1, p0, Lnet/flixster/android/FlixsterListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    const v2, 0x7f0d007e

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 113
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 114
    move-object p2, v0

    .line 116
    .end local v0           #titleView:Landroid/widget/TextView;
    :cond_1
    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 64
    const/16 v0, 0x9

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .parameter "position"

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lnet/flixster/android/FlixsterListAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
