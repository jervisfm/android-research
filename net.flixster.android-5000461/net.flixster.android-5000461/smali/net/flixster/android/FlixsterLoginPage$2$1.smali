.class Lnet/flixster/android/FlixsterLoginPage$2$1;
.super Ljava/lang/Thread;
.source "FlixsterLoginPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/FlixsterLoginPage$2;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lnet/flixster/android/FlixsterLoginPage$2;

.field private final synthetic val$fPassword:Ljava/lang/String;

.field private final synthetic val$fUsername:Ljava/lang/String;


# direct methods
.method constructor <init>(Lnet/flixster/android/FlixsterLoginPage$2;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FlixsterLoginPage$2$1;->this$1:Lnet/flixster/android/FlixsterLoginPage$2;

    iput-object p2, p0, Lnet/flixster/android/FlixsterLoginPage$2$1;->val$fUsername:Ljava/lang/String;

    iput-object p3, p0, Lnet/flixster/android/FlixsterLoginPage$2$1;->val$fPassword:Ljava/lang/String;

    .line 104
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 107
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 109
    .local v1, msg:Landroid/os/Message;
    :try_start_0
    iget-object v3, p0, Lnet/flixster/android/FlixsterLoginPage$2$1;->val$fUsername:Ljava/lang/String;

    iget-object v4, p0, Lnet/flixster/android/FlixsterLoginPage$2$1;->val$fPassword:Ljava/lang/String;

    .line 110
    const-string v5, "FLX"

    .line 109
    invoke-static {v3, v4, v5}, Lnet/flixster/android/data/ProfileDao;->loginUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/User;

    move-result-object v2

    .line 111
    .local v2, user:Lnet/flixster/android/model/User;
    if-eqz v2, :cond_0

    iget-object v3, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 112
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 114
    :cond_0
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    .end local v2           #user:Lnet/flixster/android/model/User;
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/FlixsterLoginPage$2$1;->this$1:Lnet/flixster/android/FlixsterLoginPage$2;

    #getter for: Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;
    invoke-static {v3}, Lnet/flixster/android/FlixsterLoginPage$2;->access$0(Lnet/flixster/android/FlixsterLoginPage$2;)Lnet/flixster/android/FlixsterLoginPage;

    move-result-object v3

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->loginUserHandler:Landroid/os/Handler;
    invoke-static {v3}, Lnet/flixster/android/FlixsterLoginPage;->access$5(Lnet/flixster/android/FlixsterLoginPage;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 119
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, de:Lnet/flixster/android/data/DaoException;
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_0
.end method
