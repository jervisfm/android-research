.class Lnet/flixster/android/FlixsterLoginPage$2;
.super Landroid/os/Handler;
.source "FlixsterLoginPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FlixsterLoginPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FlixsterLoginPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/FlixsterLoginPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    .line 79
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/FlixsterLoginPage$2;)Lnet/flixster/android/FlixsterLoginPage;
    .locals 1
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .parameter "message"

    .prologue
    const/4 v8, 0x0

    .line 82
    iget-object v6, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    const v7, 0x7f070087

    invoke-virtual {v6, v7}, Lnet/flixster/android/FlixsterLoginPage;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    .line 83
    .local v5, usernameField:Landroid/widget/EditText;
    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    .line 84
    .local v4, username:Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 85
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 89
    :goto_0
    iget-object v6, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    const v7, 0x7f070088

    invoke-virtual {v6, v7}, Lnet/flixster/android/FlixsterLoginPage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 90
    .local v3, passwordField:Landroid/widget/EditText;
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91
    .local v2, password:Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 92
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 96
    :goto_1
    iget-object v6, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->isLoggingIn:Z
    invoke-static {v6}, Lnet/flixster/android/FlixsterLoginPage;->access$1(Lnet/flixster/android/FlixsterLoginPage;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 97
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 98
    iget-object v6, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    const/4 v7, 0x1

    #setter for: Lnet/flixster/android/FlixsterLoginPage;->isLoggingIn:Z
    invoke-static {v6, v7}, Lnet/flixster/android/FlixsterLoginPage;->access$2(Lnet/flixster/android/FlixsterLoginPage;Z)V

    .line 99
    move-object v1, v4

    .line 100
    .local v1, fUsername:Ljava/lang/String;
    move-object v0, v2

    .line 101
    .local v0, fPassword:Ljava/lang/String;
    iget-object v6, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->errorText:Landroid/widget/TextView;
    invoke-static {v6}, Lnet/flixster/android/FlixsterLoginPage;->access$3(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/TextView;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    iget-object v6, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v6}, Lnet/flixster/android/FlixsterLoginPage;->access$4(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/ProgressBar;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 104
    new-instance v6, Lnet/flixster/android/FlixsterLoginPage$2$1;

    invoke-direct {v6, p0, v1, v0}, Lnet/flixster/android/FlixsterLoginPage$2$1;-><init>(Lnet/flixster/android/FlixsterLoginPage$2;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {v6}, Lnet/flixster/android/FlixsterLoginPage$2$1;->start()V

    .line 128
    .end local v0           #fPassword:Ljava/lang/String;
    .end local v1           #fUsername:Ljava/lang/String;
    :cond_0
    :goto_2
    return-void

    .line 87
    .end local v2           #password:Ljava/lang/String;
    .end local v3           #passwordField:Landroid/widget/EditText;
    :cond_1
    const-string v4, ""

    goto :goto_0

    .line 94
    .restart local v2       #password:Ljava/lang/String;
    .restart local v3       #passwordField:Landroid/widget/EditText;
    :cond_2
    const-string v2, ""

    goto :goto_1

    .line 122
    :cond_3
    iget-object v6, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->errorText:Landroid/widget/TextView;
    invoke-static {v6}, Lnet/flixster/android/FlixsterLoginPage;->access$3(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    iget-object v6, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->errorText:Landroid/widget/TextView;
    invoke-static {v6}, Lnet/flixster/android/FlixsterLoginPage;->access$3(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/TextView;

    move-result-object v6

    const-string v7, "The device has no connection to the Internet. Please verify the settings."

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v6, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->errorText:Landroid/widget/TextView;
    invoke-static {v6}, Lnet/flixster/android/FlixsterLoginPage;->access$3(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->invalidate()V

    .line 125
    iget-object v6, p0, Lnet/flixster/android/FlixsterLoginPage$2;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #setter for: Lnet/flixster/android/FlixsterLoginPage;->isLoggingIn:Z
    invoke-static {v6, v8}, Lnet/flixster/android/FlixsterLoginPage;->access$2(Lnet/flixster/android/FlixsterLoginPage;Z)V

    goto :goto_2
.end method
