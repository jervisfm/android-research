.class Lnet/flixster/android/FlixsterLoginPage$3;
.super Landroid/os/Handler;
.source "FlixsterLoginPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FlixsterLoginPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FlixsterLoginPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/FlixsterLoginPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    .line 131
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .parameter "msg"

    .prologue
    const/16 v4, 0x8

    const/4 v5, 0x0

    .line 133
    iget-object v2, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lnet/flixster/android/FlixsterLoginPage;->access$4(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 134
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 135
    iget-object v2, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->errorText:Landroid/widget/TextView;
    invoke-static {v2}, Lnet/flixster/android/FlixsterLoginPage;->access$3(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 136
    iget-object v2, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lnet/flixster/android/FlixsterLoginPage;->setResult(I)V

    .line 137
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/flixster/login/dialog"

    const-string v4, "Flixster - Login"

    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v2, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->requestCode:I
    invoke-static {v2}, Lnet/flixster/android/FlixsterLoginPage;->access$6(Lnet/flixster/android/FlixsterLoginPage;)I

    move-result v2

    invoke-static {v2}, Lcom/flixster/android/msk/MskController;->isRequestCodeValid(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/msk/MskController;->trackFlxLoginSuccess()V

    .line 141
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    invoke-virtual {v2}, Lnet/flixster/android/FlixsterLoginPage;->finish()V

    .line 149
    :cond_1
    :goto_0
    iget-object v2, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #setter for: Lnet/flixster/android/FlixsterLoginPage;->isLoggingIn:Z
    invoke-static {v2, v5}, Lnet/flixster/android/FlixsterLoginPage;->access$2(Lnet/flixster/android/FlixsterLoginPage;Z)V

    .line 150
    return-void

    .line 142
    :cond_2
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v2, v2, Lnet/flixster/android/data/DaoException;

    if-eqz v2, :cond_1

    .line 143
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/data/DaoException;

    .line 144
    .local v0, de:Lnet/flixster/android/data/DaoException;
    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->getType()Lnet/flixster/android/data/DaoException$Type;

    move-result-object v2

    sget-object v3, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    if-ne v2, v3, :cond_3

    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, error:Ljava/lang/String;
    :goto_1
    iget-object v2, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->errorText:Landroid/widget/TextView;
    invoke-static {v2}, Lnet/flixster/android/FlixsterLoginPage;->access$3(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 146
    iget-object v2, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    #getter for: Lnet/flixster/android/FlixsterLoginPage;->errorText:Landroid/widget/TextView;
    invoke-static {v2}, Lnet/flixster/android/FlixsterLoginPage;->access$3(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v2, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    invoke-virtual {v2, v5}, Lnet/flixster/android/FlixsterLoginPage;->setResult(I)V

    goto :goto_0

    .line 144
    .end local v1           #error:Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lnet/flixster/android/FlixsterLoginPage$3;->this$0:Lnet/flixster/android/FlixsterLoginPage;

    const v3, 0x7f0c0161

    invoke-virtual {v2, v3}, Lnet/flixster/android/FlixsterLoginPage;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
