.class public Lnet/flixster/android/FlixsterLoginPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "FlixsterLoginPage.java"


# instance fields
.field private errorText:Landroid/widget/TextView;

.field private isLoggingIn:Z

.field private final loginHandler:Landroid/os/Handler;

.field private final loginUserHandler:Landroid/os/Handler;

.field private requestCode:I

.field private throbber:Landroid/widget/ProgressBar;

.field private final tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 63
    new-instance v0, Lnet/flixster/android/FlixsterLoginPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterLoginPage$1;-><init>(Lnet/flixster/android/FlixsterLoginPage;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterLoginPage;->tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 79
    new-instance v0, Lnet/flixster/android/FlixsterLoginPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterLoginPage$2;-><init>(Lnet/flixster/android/FlixsterLoginPage;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterLoginPage;->loginHandler:Landroid/os/Handler;

    .line 131
    new-instance v0, Lnet/flixster/android/FlixsterLoginPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/FlixsterLoginPage$3;-><init>(Lnet/flixster/android/FlixsterLoginPage;)V

    iput-object v0, p0, Lnet/flixster/android/FlixsterLoginPage;->loginUserHandler:Landroid/os/Handler;

    .line 27
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/FlixsterLoginPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lnet/flixster/android/FlixsterLoginPage;->loginHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/FlixsterLoginPage;)Z
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-boolean v0, p0, Lnet/flixster/android/FlixsterLoginPage;->isLoggingIn:Z

    return v0
.end method

.method static synthetic access$2(Lnet/flixster/android/FlixsterLoginPage;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28
    iput-boolean p1, p0, Lnet/flixster/android/FlixsterLoginPage;->isLoggingIn:Z

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lnet/flixster/android/FlixsterLoginPage;->errorText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/FlixsterLoginPage;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lnet/flixster/android/FlixsterLoginPage;->throbber:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/FlixsterLoginPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lnet/flixster/android/FlixsterLoginPage;->loginUserHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/FlixsterLoginPage;)I
    .locals 1
    .parameter

    .prologue
    .line 31
    iget v0, p0, Lnet/flixster/android/FlixsterLoginPage;->requestCode:I

    return v0
.end method

.method private login()V
    .locals 2

    .prologue
    .line 56
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->hasAcceptedTos()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lnet/flixster/android/FlixsterLoginPage;->tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/flixster/android/view/DialogBuilder$DialogListener;->onPositiveButtonClick(I)V

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FlixsterLoginPage;->tosDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    invoke-static {p0, v0}, Lcom/flixster/android/view/DialogBuilder;->showTermsOfService(Landroid/app/Activity;Lcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedState"

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    const-string v1, "FlxMain"

    const-string v2, "FlixsterLoginPage.onCreate"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterLoginPage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 39
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 40
    const-string v1, "MskEntryActivity.REQUEST_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lnet/flixster/android/FlixsterLoginPage;->requestCode:I

    .line 43
    :cond_0
    const v1, 0x7f03002d

    invoke-virtual {p0, v1}, Lnet/flixster/android/FlixsterLoginPage;->setContentView(I)V

    .line 44
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterLoginPage;->createActionBar()V

    .line 45
    const v1, 0x7f0c0061

    invoke-virtual {p0, v1}, Lnet/flixster/android/FlixsterLoginPage;->setActionBarTitle(I)V

    .line 47
    const v1, 0x7f070089

    invoke-virtual {p0, v1}, Lnet/flixster/android/FlixsterLoginPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/FlixsterLoginPage;->errorText:Landroid/widget/TextView;

    .line 48
    const v1, 0x7f070039

    invoke-virtual {p0, v1}, Lnet/flixster/android/FlixsterLoginPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lnet/flixster/android/FlixsterLoginPage;->throbber:Landroid/widget/ProgressBar;

    .line 50
    iget v1, p0, Lnet/flixster/android/FlixsterLoginPage;->requestCode:I

    invoke-static {v1}, Lcom/flixster/android/msk/MskController;->isRequestCodeValid(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 51
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController;->trackFlxLoginAttempt()V

    .line 53
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 155
    invoke-virtual {p0}, Lnet/flixster/android/FlixsterLoginPage;->getSupportMenuInflater()Lcom/actionbarsherlock/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0008

    invoke-virtual {v0, v1, p1}, Lcom/actionbarsherlock/view/MenuInflater;->inflate(ILcom/actionbarsherlock/view/Menu;)V

    .line 156
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 161
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 166
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 163
    :pswitch_0
    invoke-direct {p0}, Lnet/flixster/android/FlixsterLoginPage;->login()V

    .line 164
    const/4 v0, 0x1

    goto :goto_0

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x7f070302
        :pswitch_0
    .end packed-switch
.end method
