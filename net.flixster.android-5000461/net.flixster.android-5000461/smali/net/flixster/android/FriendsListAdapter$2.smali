.class Lnet/flixster/android/FriendsListAdapter$2;
.super Ljava/lang/Object;
.source "FriendsListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FriendsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FriendsListAdapter;


# direct methods
.method constructor <init>(Lnet/flixster/android/FriendsListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FriendsListAdapter$2;->this$0:Lnet/flixster/android/FriendsListAdapter;

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "view"

    .prologue
    .line 112
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/User;

    .line 113
    .local v1, user:Lnet/flixster/android/model/User;
    if-eqz v1, :cond_0

    .line 114
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/mymovies/profile"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "User Profile Page for user:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v1, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v3, v1, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, userId:Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lnet/flixster/android/FriendsListAdapter$2;->this$0:Lnet/flixster/android/FriendsListAdapter;

    iget-object v3, v3, Lnet/flixster/android/FriendsListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    const-class v4, Lnet/flixster/android/UserProfilePage;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 117
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "PLATFORM_ID"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    iget-object v3, p0, Lnet/flixster/android/FriendsListAdapter$2;->this$0:Lnet/flixster/android/FriendsListAdapter;

    iget-object v3, v3, Lnet/flixster/android/FriendsListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    invoke-virtual {v3, v0}, Lnet/flixster/android/FlixsterListActivity;->startActivity(Landroid/content/Intent;)V

    .line 120
    .end local v0           #intent:Landroid/content/Intent;
    .end local v2           #userId:Ljava/lang/String;
    :cond_0
    return-void
.end method
