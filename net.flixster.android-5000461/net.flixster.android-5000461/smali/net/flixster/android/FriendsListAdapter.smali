.class public Lnet/flixster/android/FriendsListAdapter;
.super Lnet/flixster/android/FlixsterListAdapter;
.source "FriendsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;
    }
.end annotation


# static fields
.field private static final MAX_LENGTH_FULL_NAME:I = 0xf


# instance fields
.field private friendClickListener:Landroid/view/View$OnClickListener;

.field private friendItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mRefreshAd:Z


# direct methods
.method public constructor <init>(Lnet/flixster/android/FlixsterListActivity;Ljava/util/ArrayList;)V
    .locals 2
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/flixster/android/FlixsterListActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, data:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/FlixsterListAdapter;-><init>(Lnet/flixster/android/FlixsterListActivity;Ljava/util/ArrayList;)V

    .line 100
    new-instance v0, Lnet/flixster/android/FriendsListAdapter$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/FriendsListAdapter$1;-><init>(Lnet/flixster/android/FriendsListAdapter;)V

    iput-object v0, p0, Lnet/flixster/android/FriendsListAdapter;->friendItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 110
    new-instance v0, Lnet/flixster/android/FriendsListAdapter$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/FriendsListAdapter$2;-><init>(Lnet/flixster/android/FriendsListAdapter;)V

    iput-object v0, p0, Lnet/flixster/android/FriendsListAdapter;->friendClickListener:Landroid/view/View$OnClickListener;

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/FriendsListAdapter;->mRefreshAd:Z

    .line 31
    invoke-virtual {p1}, Lnet/flixster/android/FlixsterListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/FriendsListAdapter;->friendItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 32
    return-void
.end method

.method private getFriendView(Lnet/flixster/android/model/User;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .parameter "user"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v10, 0x0

    .line 135
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;

    if-nez v8, :cond_1

    .line 136
    :cond_0
    iget-object v8, p0, Lnet/flixster/android/FriendsListAdapter;->inflater:Landroid/view/LayoutInflater;

    const v9, 0x7f03002f

    invoke-virtual {v8, v9, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 138
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;

    .line 139
    .local v7, viewHolder:Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;
    if-nez v7, :cond_2

    .line 140
    new-instance v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;

    .end local v7           #viewHolder:Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;
    const/4 v8, 0x0

    invoke-direct {v7, v8}, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;-><init>(Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;)V

    .line 141
    .restart local v7       #viewHolder:Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;
    const v8, 0x7f070093

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->friendLayout:Landroid/widget/RelativeLayout;

    .line 142
    const v8, 0x7f070094

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->friendImage:Landroid/widget/ImageView;

    .line 143
    const v8, 0x7f070095

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->firstNameView:Landroid/widget/TextView;

    .line 144
    const v8, 0x7f070096

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->lastNameView:Landroid/widget/TextView;

    .line 145
    const v8, 0x7f070097

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->ratedView:Landroid/widget/TextView;

    .line 146
    invoke-virtual {p2, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 149
    :cond_2
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->friendImage:Landroid/widget/ImageView;

    invoke-virtual {p1, v8}, Lnet/flixster/android/model/User;->getThumbnailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 150
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_7

    .line 151
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->friendImage:Landroid/widget/ImageView;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 156
    :cond_3
    :goto_0
    const/4 v4, 0x0

    .line 157
    .local v4, nameLength:I
    iget-object v1, p1, Lnet/flixster/android/model/User;->firstName:Ljava/lang/String;

    .line 158
    .local v1, firstName:Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 159
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v4, v8

    .line 160
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->firstNameView:Landroid/widget/TextView;

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->firstNameView:Landroid/widget/TextView;

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    :cond_4
    iget-object v2, p1, Lnet/flixster/android/model/User;->lastName:Ljava/lang/String;

    .line 164
    .local v2, lastName:Ljava/lang/String;
    if-eqz v2, :cond_6

    .line 165
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    .line 166
    .local v3, lastNameLength:I
    add-int/2addr v4, v3

    .line 167
    add-int/lit8 v5, v4, -0xf

    .line 168
    .local v5, nameOverflowLength:I
    if-lez v5, :cond_5

    if-le v3, v5, :cond_5

    .line 169
    sub-int v8, v3, v5

    invoke-virtual {v2, v10, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 170
    const-string v8, "..."

    invoke-virtual {v2, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 172
    :cond_5
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->lastNameView:Landroid/widget/TextView;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->lastNameView:Landroid/widget/TextView;

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 175
    .end local v3           #lastNameLength:I
    .end local v5           #nameOverflowLength:I
    :cond_6
    iget v6, p1, Lnet/flixster/android/model/User;->ratedCount:I

    .line 176
    .local v6, reviewCount:I
    if-lez v6, :cond_8

    .line 177
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->ratedView:Landroid/widget/TextView;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->ratedView:Landroid/widget/TextView;

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    :goto_1
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->friendLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, p1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 183
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->friendLayout:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lnet/flixster/android/FriendsListAdapter;->friendClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    return-object p2

    .line 152
    .end local v1           #firstName:Ljava/lang/String;
    .end local v2           #lastName:Ljava/lang/String;
    .end local v4           #nameLength:I
    .end local v6           #reviewCount:I
    :cond_7
    invoke-virtual {p1}, Lnet/flixster/android/model/User;->isThumbnailUrlAvailable()Z

    move-result v8

    if-nez v8, :cond_3

    .line 153
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->friendImage:Landroid/widget/ImageView;

    const v9, 0x7f0201da

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 180
    .restart local v1       #firstName:Ljava/lang/String;
    .restart local v2       #lastName:Ljava/lang/String;
    .restart local v4       #nameLength:I
    .restart local v6       #reviewCount:I
    :cond_8
    iget-object v8, v7, Lnet/flixster/android/FriendsListAdapter$FriendViewHolder;->ratedView:Landroid/widget/TextView;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private getLetterView(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;
    .locals 3
    .parameter "letter"
    .parameter "convertView"

    .prologue
    .line 124
    if-eqz p2, :cond_0

    instance-of v2, p2, Landroid/widget/TextView;

    if-nez v2, :cond_1

    .line 125
    :cond_0
    new-instance v1, Lcom/flixster/android/view/SubHeader;

    iget-object v2, p0, Lnet/flixster/android/FriendsListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    invoke-direct {v1, v2}, Lcom/flixster/android/view/SubHeader;-><init>(Landroid/content/Context;)V

    .line 126
    .local v1, textView:Landroid/widget/TextView;
    move-object p2, v1

    .end local v1           #textView:Landroid/widget/TextView;
    :cond_1
    move-object v0, p2

    .line 128
    check-cast v0, Landroid/widget/TextView;

    .line 129
    .local v0, letterView:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    return-object p2
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 4
    .parameter "position"

    .prologue
    const/4 v2, 0x1

    .line 78
    sget-object v3, Lnet/flixster/android/FriendsListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 79
    .local v0, dataObject:Ljava/lang/Object;
    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    move-object v1, v0

    .line 80
    check-cast v1, Ljava/lang/String;

    .line 81
    .local v1, text:Ljava/lang/String;
    if-nez p1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v2, :cond_0

    .line 96
    .end local v1           #text:Ljava/lang/String;
    :goto_0
    return v2

    .line 84
    .restart local v1       #text:Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v2, :cond_1

    .line 85
    const/16 v2, 0x8

    goto :goto_0

    .line 87
    :cond_1
    const/4 v2, 0x2

    goto :goto_0

    .line 90
    .end local v1           #text:Ljava/lang/String;
    :cond_2
    instance-of v2, v0, Lnet/flixster/android/model/User;

    if-eqz v2, :cond_3

    .line 91
    const/4 v2, 0x3

    goto :goto_0

    .line 92
    :cond_3
    instance-of v2, v0, Lnet/flixster/android/ads/AdView;

    if-eqz v2, :cond_4

    .line 93
    const/4 v2, 0x7

    goto :goto_0

    .line 96
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lnet/flixster/android/FriendsListAdapter;->getItemViewType(I)I

    move-result v6

    .line 40
    .local v6, viewType:I
    sget-object v7, Lnet/flixster/android/FriendsListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 41
    .local v0, dataObject:Ljava/lang/Object;
    packed-switch v6, :pswitch_data_0

    :goto_0
    :pswitch_0
    move-object v5, p2

    .line 73
    :cond_0
    :goto_1
    return-object v5

    :pswitch_1
    move-object v5, p2

    .line 43
    check-cast v5, Lnet/flixster/android/ads/AdView;

    .line 44
    .local v5, view:Lnet/flixster/android/ads/AdView;
    if-nez v5, :cond_1

    .line 45
    new-instance v5, Lnet/flixster/android/ads/AdView;

    .end local v5           #view:Lnet/flixster/android/ads/AdView;
    iget-object v7, p0, Lnet/flixster/android/FriendsListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    const-string v8, "MoviesIveRated"

    invoke-direct {v5, v7, v8}, Lnet/flixster/android/ads/AdView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 46
    .restart local v5       #view:Lnet/flixster/android/ads/AdView;
    const/16 v7, 0x11

    invoke-virtual {v5, v7}, Lnet/flixster/android/ads/AdView;->setGravity(I)V

    .line 47
    const v7, -0x444445

    invoke-virtual {v5, v7}, Lnet/flixster/android/ads/AdView;->setBackgroundColor(I)V

    .line 49
    :cond_1
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "FriendsListAdapter.getView: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-boolean v7, p0, Lnet/flixster/android/FriendsListAdapter;->mRefreshAd:Z

    if-eqz v7, :cond_0

    .line 51
    invoke-virtual {v5}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 52
    const/4 v7, 0x0

    iput-boolean v7, p0, Lnet/flixster/android/FriendsListAdapter;->mRefreshAd:Z

    .line 53
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "FriendsListAdapter.refresh: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .end local v5           #view:Lnet/flixster/android/ads/AdView;
    :pswitch_2
    move-object v3, v0

    .line 57
    check-cast v3, Ljava/lang/String;

    .line 58
    .local v3, title:Ljava/lang/String;
    invoke-virtual {p0, v3, p2}, Lnet/flixster/android/FriendsListAdapter;->getTitleView(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;

    move-result-object p2

    .line 59
    goto :goto_0

    .end local v3           #title:Ljava/lang/String;
    :pswitch_3
    move-object v1, v0

    .line 61
    check-cast v1, Ljava/lang/String;

    .line 62
    .local v1, letter:Ljava/lang/String;
    invoke-direct {p0, v1, p2}, Lnet/flixster/android/FriendsListAdapter;->getLetterView(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;

    move-result-object p2

    .line 63
    goto :goto_0

    .end local v1           #letter:Ljava/lang/String;
    :pswitch_4
    move-object v4, v0

    .line 65
    check-cast v4, Lnet/flixster/android/model/User;

    .line 66
    .local v4, user:Lnet/flixster/android/model/User;
    invoke-direct {p0, v4, p2, p3}, Lnet/flixster/android/FriendsListAdapter;->getFriendView(Lnet/flixster/android/model/User;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 67
    goto :goto_0

    .end local v4           #user:Lnet/flixster/android/model/User;
    :pswitch_5
    move-object v2, v0

    .line 69
    check-cast v2, Ljava/lang/String;

    .line 70
    .local v2, text:Ljava/lang/String;
    invoke-virtual {p0, v2, p2}, Lnet/flixster/android/FriendsListAdapter;->getTextView(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setAdRefresh(Z)V
    .locals 0
    .parameter "bool"

    .prologue
    .line 35
    iput-boolean p1, p0, Lnet/flixster/android/FriendsListAdapter;->mRefreshAd:Z

    .line 36
    return-void
.end method
