.class Lnet/flixster/android/FriendsPage$1;
.super Landroid/os/Handler;
.source "FriendsPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FriendsPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FriendsPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/FriendsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FriendsPage$1;->this$0:Lnet/flixster/android/FriendsPage;

    .line 251
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "message"

    .prologue
    .line 255
    const-string v0, "FlxMain"

    const-string v1, "FriendsPage.updateHandler"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lnet/flixster/android/FriendsPage$1;->this$0:Lnet/flixster/android/FriendsPage;

    #getter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/FriendsPage;->access$0(Lnet/flixster/android/FriendsPage;)Lnet/flixster/android/model/User;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lnet/flixster/android/FriendsPage$1;->this$0:Lnet/flixster/android/FriendsPage;

    #getter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/FriendsPage;->access$0(Lnet/flixster/android/FriendsPage;)Lnet/flixster/android/model/User;

    move-result-object v0

    iget-object v0, v0, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lnet/flixster/android/FriendsPage$1;->this$0:Lnet/flixster/android/FriendsPage;

    #calls: Lnet/flixster/android/FriendsPage;->updatePage()V
    invoke-static {v0}, Lnet/flixster/android/FriendsPage;->access$1(Lnet/flixster/android/FriendsPage;)V

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/FriendsPage$1;->this$0:Lnet/flixster/android/FriendsPage;

    invoke-virtual {v0}, Lnet/flixster/android/FriendsPage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    iget-object v0, p0, Lnet/flixster/android/FriendsPage$1;->this$0:Lnet/flixster/android/FriendsPage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/FriendsPage;->showDialog(I)V

    goto :goto_0
.end method
