.class Lnet/flixster/android/FriendsPage$3;
.super Ljava/util/TimerTask;
.source "FriendsPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/FriendsPage;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FriendsPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/FriendsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    .line 223
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 225
    iget-object v1, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    #getter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/FriendsPage;->access$0(Lnet/flixster/android/FriendsPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    #getter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/FriendsPage;->access$0(Lnet/flixster/android/FriendsPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    if-nez v1, :cond_1

    .line 227
    :cond_0
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;

    move-result-object v2

    #setter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1, v2}, Lnet/flixster/android/FriendsPage;->access$3(Lnet/flixster/android/FriendsPage;Lnet/flixster/android/model/User;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :cond_1
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    #getter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/FriendsPage;->access$0(Lnet/flixster/android/FriendsPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    #getter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/FriendsPage;->access$0(Lnet/flixster/android/FriendsPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 235
    iget-object v1, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    #getter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/FriendsPage;->access$0(Lnet/flixster/android/FriendsPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    if-nez v1, :cond_2

    .line 237
    :try_start_1
    iget-object v1, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    #getter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/FriendsPage;->access$0(Lnet/flixster/android/FriendsPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    #getter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/FriendsPage;->access$0(Lnet/flixster/android/FriendsPage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getFriends(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    iput-object v2, v1, Lnet/flixster/android/model/User;->friends:Ljava/util/List;
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_1

    .line 243
    :cond_2
    :goto_1
    iget-object v1, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    #getter for: Lnet/flixster/android/FriendsPage;->updateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/FriendsPage;->access$4(Lnet/flixster/android/FriendsPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 244
    return-void

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "FriendsPage.scheduleUpdatePageTask (failed to get user data)"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 230
    iget-object v1, p0, Lnet/flixster/android/FriendsPage$3;->this$0:Lnet/flixster/android/FriendsPage;

    const/4 v2, 0x0

    #setter for: Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1, v2}, Lnet/flixster/android/FriendsPage;->access$3(Lnet/flixster/android/FriendsPage;Lnet/flixster/android/model/User;)V

    goto :goto_0

    .line 238
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catch_1
    move-exception v0

    .line 239
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "FriendsPage.scheduleUpdatePageTask (failed to get review data)"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
