.class public Lnet/flixster/android/FriendsPage;
.super Lnet/flixster/android/FlixsterListActivity;
.source "FriendsPage.java"


# static fields
.field private static final DIALOG_FRIENDS:I = 0x2

.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field public static final KEY_SORT_FRIENDS:Ljava/lang/String; = "SORT_FRIENDS"

.field public static final SORT_ALPHABETICAL:I = 0x1

.field public static final SORT_REVIEW_COUNT:I = 0x2


# instance fields
.field private isConnected:Ljava/lang/Boolean;

.field private mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mFriendsList:Landroid/widget/ListView;

.field private sortType:I

.field private timer:Ljava/util/Timer;

.field private updateHandler:Landroid/os/Handler;

.field private user:Lnet/flixster/android/model/User;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lnet/flixster/android/FlixsterListActivity;-><init>()V

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lnet/flixster/android/FriendsPage;->sortType:I

    .line 251
    new-instance v0, Lnet/flixster/android/FriendsPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/FriendsPage$1;-><init>(Lnet/flixster/android/FriendsPage;)V

    iput-object v0, p0, Lnet/flixster/android/FriendsPage;->updateHandler:Landroid/os/Handler;

    .line 33
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/FriendsPage;)Lnet/flixster/android/model/User;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/FriendsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 267
    invoke-direct {p0}, Lnet/flixster/android/FriendsPage;->updatePage()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/FriendsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 222
    invoke-direct {p0}, Lnet/flixster/android/FriendsPage;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/FriendsPage;Lnet/flixster/android/model/User;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/FriendsPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 251
    iget-object v0, p0, Lnet/flixster/android/FriendsPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 223
    new-instance v0, Lnet/flixster/android/FriendsPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/FriendsPage$3;-><init>(Lnet/flixster/android/FriendsPage;)V

    .line 246
    .local v0, updatePageTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/FriendsPage;->timer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 247
    iget-object v1, p0, Lnet/flixster/android/FriendsPage;->timer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 249
    :cond_0
    return-void
.end method

.method private sortUsers(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/User;",
            ">;)",
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation

    .prologue
    .local p1, friends:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/User;>;"
    const/4 v8, 0x0

    .line 289
    const/4 v6, 0x2

    iget v7, p0, Lnet/flixster/android/FriendsPage;->sortType:I

    if-ne v6, v7, :cond_0

    .line 290
    new-instance v6, Lnet/flixster/android/model/RatedCountComparator;

    invoke-direct {v6}, Lnet/flixster/android/model/RatedCountComparator;-><init>()V

    invoke-static {p1, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 318
    .end local p1           #friends:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/User;>;"
    :goto_0
    return-object p1

    .line 293
    .restart local p1       #friends:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/User;>;"
    :cond_0
    new-instance v6, Lnet/flixster/android/model/LastNameComparator;

    invoke-direct {v6}, Lnet/flixster/android/model/LastNameComparator;-><init>()V

    invoke-static {p1, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 294
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 295
    .local v5, sortedFriends:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 296
    .local v4, nonAlphabeticalFriends:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    const-string v3, "#"

    .line 297
    .local v3, letter:Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, friendItr:Ljava/util/Iterator;,"Ljava/util/Iterator<Lnet/flixster/android/model/User;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 314
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 315
    const-string v6, "#"

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    invoke-interface {v5, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    move-object p1, v5

    .line 318
    goto :goto_0

    .line 298
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/User;

    .line 299
    .local v0, friend:Lnet/flixster/android/model/User;
    iget-object v6, v0, Lnet/flixster/android/model/User;->lastName:Ljava/lang/String;

    if-eqz v6, :cond_5

    iget-object v6, v0, Lnet/flixster/android/model/User;->lastName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_5

    .line 300
    iget-object v6, v0, Lnet/flixster/android/model/User;->lastName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 301
    .local v2, friendLetter:Ljava/lang/String;
    invoke-virtual {v2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isLetter(C)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 302
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 303
    move-object v3, v2

    .line 304
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    :cond_3
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 308
    :cond_4
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 311
    .end local v2           #friendLetter:Ljava/lang/String;
    :cond_5
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private updatePage()V
    .locals 4

    .prologue
    .line 268
    iget-object v1, p0, Lnet/flixster/android/FriendsPage;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 269
    iget-object v1, p0, Lnet/flixster/android/FriendsPage;->mData:Ljava/util/ArrayList;

    new-instance v2, Lnet/flixster/android/ads/AdView;

    const-string v3, "MyFriends"

    invoke-direct {v2, p0, v3}, Lnet/flixster/android/ads/AdView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    iget-object v1, p0, Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;

    iget-object v1, v1, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;

    iget-object v1, v1, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 273
    iget-object v1, p0, Lnet/flixster/android/FriendsPage;->user:Lnet/flixster/android/model/User;

    iget-object v1, v1, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    invoke-direct {p0, v1}, Lnet/flixster/android/FriendsPage;->sortUsers(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 274
    .local v0, friends:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    iget-object v1, p0, Lnet/flixster/android/FriendsPage;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 280
    .end local v0           #friends:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    :goto_0
    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->invalidateViews()V

    .line 283
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lnet/flixster/android/FriendsPage;->removeDialog(I)V

    .line 284
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/friendsmovies/friends"

    const-string v3, "Friends Page"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    return-void

    .line 276
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/FriendsPage;->mData:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c008c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 88
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 89
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/FriendsPage;->isConnected:Ljava/lang/Boolean;

    .line 90
    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 91
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "net.flixster.IsConnected"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 96
    .end local v0           #intent:Landroid/content/Intent;
    :goto_0
    return-void

    .line 93
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lnet/flixster/android/FriendsPage;->setResult(I)V

    .line 94
    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .parameter "savedState"

    .prologue
    const v6, 0x7f09000b

    const/4 v5, 0x2

    .line 49
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const-string v3, "FlxMain"

    const-string v4, "FriendsPage.onCreate"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->createActionBar()V

    .line 52
    const v3, 0x7f0c006f

    invoke-virtual {p0, v3}, Lnet/flixster/android/FriendsPage;->setActionBarTitle(I)V

    .line 54
    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 55
    .local v2, extras:Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 56
    const-string v3, "SORT_FRIENDS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 57
    const-string v3, "SORT_FRIENDS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lnet/flixster/android/FriendsPage;->sortType:I

    .line 58
    iget v3, p0, Lnet/flixster/android/FriendsPage;->sortType:I

    if-gtz v3, :cond_0

    .line 59
    const/4 v3, 0x1

    iput v3, p0, Lnet/flixster/android/FriendsPage;->sortType:I

    .line 64
    :cond_0
    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->getListView()Landroid/widget/ListView;

    move-result-object v3

    iput-object v3, p0, Lnet/flixster/android/FriendsPage;->mFriendsList:Landroid/widget/ListView;

    .line 65
    iget-object v3, p0, Lnet/flixster/android/FriendsPage;->mFriendsList:Landroid/widget/ListView;

    invoke-virtual {v3, v6}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 66
    iget-object v3, p0, Lnet/flixster/android/FriendsPage;->mFriendsList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 67
    iget-object v3, p0, Lnet/flixster/android/FriendsPage;->mFriendsList:Landroid/widget/ListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 69
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lnet/flixster/android/FriendsPage;->mData:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Lnet/flixster/android/FriendsListAdapter;

    iget-object v3, p0, Lnet/flixster/android/FriendsPage;->mData:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v3}, Lnet/flixster/android/FriendsListAdapter;-><init>(Lnet/flixster/android/FlixsterListActivity;Ljava/util/ArrayList;)V

    .line 71
    .local v0, adapter:Landroid/widget/ListAdapter;
    invoke-virtual {p0, v0}, Lnet/flixster/android/FriendsPage;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 73
    if-eqz v2, :cond_3

    iget-object v3, p0, Lnet/flixster/android/FriendsPage;->isConnected:Ljava/lang/Boolean;

    if-nez v3, :cond_3

    .line 74
    const-string v3, "FlxMain"

    const-string v4, "FriendsPage.onCreate has extras"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v3, "net.flixster.IsConnected"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lnet/flixster/android/FriendsPage;->isConnected:Ljava/lang/Boolean;

    .line 76
    iget-object v3, p0, Lnet/flixster/android/FriendsPage;->isConnected:Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lnet/flixster/android/FriendsPage;->isConnected:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_2

    .line 77
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lnet/flixster/android/ConnectRatePage;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    .local v1, connectRate:Landroid/content/Intent;
    const-string v3, "net.flixster.RequestCode"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 79
    invoke-virtual {p0, v1, v5}, Lnet/flixster/android/FriendsPage;->startActivityForResult(Landroid/content/Intent;I)V

    .line 84
    .end local v1           #connectRate:Landroid/content/Intent;
    :cond_2
    :goto_0
    return-void

    .line 82
    :cond_3
    invoke-direct {p0}, Lnet/flixster/android/FriendsPage;->updatePage()V

    goto :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "dialogId"

    .prologue
    const v5, 0x7f0c0135

    const/4 v4, 0x1

    .line 134
    packed-switch p1, :pswitch_data_0

    .line 158
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 159
    .local v1, defaultDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 160
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 161
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 162
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    move-object v2, v1

    .line 163
    .end local v1           #defaultDialog:Landroid/app/ProgressDialog;
    :goto_0
    return-object v2

    .line 137
    :pswitch_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 138
    .local v2, ratingsDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 139
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 140
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 141
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 144
    .end local v2           #ratingsDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 145
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v3, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 146
    const-string v3, "Network Error"

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 147
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 148
    const-string v3, "Retry"

    new-instance v4, Lnet/flixster/android/FriendsPage$2;

    invoke-direct {v4, p0}, Lnet/flixster/android/FriendsPage$2;-><init>(Lnet/flixster/android/FriendsPage;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 155
    invoke-virtual {p0}, Lnet/flixster/android/FriendsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 156
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 123
    const-string v0, "FlxMain"

    const-string v1, "FriendsPage.onDestroy"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-super {p0}, Lnet/flixster/android/FlixsterListActivity;->onDestroy()V

    .line 125
    iget-object v0, p0, Lnet/flixster/android/FriendsPage;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lnet/flixster/android/FriendsPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 127
    iget-object v0, p0, Lnet/flixster/android/FriendsPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 129
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/FriendsPage;->timer:Ljava/util/Timer;

    .line 130
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 100
    const-string v0, "FlxMain"

    const-string v1, "FriendsPage.onPause"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-super {p0}, Lnet/flixster/android/FlixsterListActivity;->onPause()V

    .line 103
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 108
    const-string v0, "FlxMain"

    const-string v1, "FriendsPage.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-super {p0}, Lnet/flixster/android/FlixsterListActivity;->onResume()V

    .line 111
    iget-object v0, p0, Lnet/flixster/android/FriendsPage;->isConnected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/FriendsPage;->timer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 115
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/FriendsPage;->timer:Ljava/util/Timer;

    .line 117
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lnet/flixster/android/FriendsPage;->showDialog(I)V

    .line 118
    invoke-direct {p0}, Lnet/flixster/android/FriendsPage;->scheduleUpdatePageTask()V

    goto :goto_0
.end method
