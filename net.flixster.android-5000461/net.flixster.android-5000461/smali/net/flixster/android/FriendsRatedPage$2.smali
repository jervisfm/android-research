.class Lnet/flixster/android/FriendsRatedPage$2;
.super Landroid/os/Handler;
.source "FriendsRatedPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FriendsRatedPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FriendsRatedPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/FriendsRatedPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    .line 105
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .parameter "msg"

    .prologue
    const/16 v8, 0xa

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 107
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->friendsRatedLayout:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lnet/flixster/android/FriendsRatedPage;->access$0(Lnet/flixster/android/FriendsRatedPage;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 108
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->friendsRatedLayout:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lnet/flixster/android/FriendsRatedPage;->access$0(Lnet/flixster/android/FriendsRatedPage;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 109
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v7

    iget-object v7, v7, Lnet/flixster/android/model/User;->friendsActivity:Ljava/util/List;

    #setter for: Lnet/flixster/android/FriendsRatedPage;->reviews:Ljava/util/List;
    invoke-static {v6, v7}, Lnet/flixster/android/FriendsRatedPage;->access$1(Lnet/flixster/android/FriendsRatedPage;Ljava/util/List;)V

    .line 110
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->reviews:Ljava/util/List;
    invoke-static {v6}, Lnet/flixster/android/FriendsRatedPage;->access$2(Lnet/flixster/android/FriendsRatedPage;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_0

    if-lt v1, v8, :cond_3

    .line 118
    :cond_0
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->reviews:Ljava/util/List;
    invoke-static {v6}, Lnet/flixster/android/FriendsRatedPage;->access$2(Lnet/flixster/android/FriendsRatedPage;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-le v6, v8, :cond_1

    .line 119
    new-instance v3, Lcom/flixster/android/view/LoadMore;

    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    invoke-direct {v3, v6}, Lcom/flixster/android/view/LoadMore;-><init>(Landroid/content/Context;)V

    .line 120
    .local v3, more:Lcom/flixster/android/view/LoadMore;
    const v6, 0x7f0700dc

    invoke-virtual {v3, v6}, Lcom/flixster/android/view/LoadMore;->setId(I)V

    .line 121
    const v6, 0x7f0700dd

    invoke-virtual {v3, v6}, Lcom/flixster/android/view/LoadMore;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 122
    .local v2, loadReviewsText:Landroid/widget/TextView;
    const v6, 0x7f0c002b

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(I)V

    .line 123
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->friendsRatedLayout:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lnet/flixster/android/FriendsRatedPage;->access$0(Lnet/flixster/android/FriendsRatedPage;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 124
    const/4 v6, 0x0

    iget-object v7, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    const v8, 0x7f0c0081

    invoke-virtual {v7, v8}, Lnet/flixster/android/FriendsRatedPage;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    iget-object v9, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->reviews:Ljava/util/List;
    invoke-static {v9}, Lnet/flixster/android/FriendsRatedPage;->access$2(Lnet/flixster/android/FriendsRatedPage;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/flixster/android/view/LoadMore;->load(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual {v3, v10}, Lcom/flixster/android/view/LoadMore;->setFocusable(Z)V

    .line 126
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->moreReviewsListener:Landroid/view/View$OnClickListener;
    invoke-static {v6}, Lnet/flixster/android/FriendsRatedPage;->access$3(Lnet/flixster/android/FriendsRatedPage;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/flixster/android/view/LoadMore;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    .end local v2           #loadReviewsText:Landroid/widget/TextView;
    .end local v3           #more:Lcom/flixster/android/view/LoadMore;
    :cond_1
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->reviews:Ljava/util/List;
    invoke-static {v6}, Lnet/flixster/android/FriendsRatedPage;->access$2(Lnet/flixster/android/FriendsRatedPage;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_2

    .line 130
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    const v7, 0x7f0700a8

    invoke-virtual {v6, v7}, Lnet/flixster/android/FriendsRatedPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 131
    .local v4, placeholder:Landroid/widget/TextView;
    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 133
    .end local v4           #placeholder:Landroid/widget/TextView;
    :cond_2
    return-void

    .line 111
    :cond_3
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->reviews:Ljava/util/List;
    invoke-static {v6}, Lnet/flixster/android/FriendsRatedPage;->access$2(Lnet/flixster/android/FriendsRatedPage;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Review;

    .line 112
    .local v5, review:Lnet/flixster/android/model/Review;
    new-instance v0, Lcom/flixster/android/view/FriendActivity;

    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    invoke-direct {v0, v6}, Lcom/flixster/android/view/FriendActivity;-><init>(Landroid/content/Context;)V

    .line 113
    .local v0, activity:Lcom/flixster/android/view/FriendActivity;
    invoke-virtual {v0, v10}, Lcom/flixster/android/view/FriendActivity;->setFocusable(Z)V

    .line 114
    iget-object v6, p0, Lnet/flixster/android/FriendsRatedPage$2;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->friendsRatedLayout:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lnet/flixster/android/FriendsRatedPage;->access$0(Lnet/flixster/android/FriendsRatedPage;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 115
    invoke-virtual {v0, v5}, Lcom/flixster/android/view/FriendActivity;->load(Lnet/flixster/android/model/Review;)V

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method
