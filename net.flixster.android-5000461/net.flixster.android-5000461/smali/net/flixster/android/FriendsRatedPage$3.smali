.class Lnet/flixster/android/FriendsRatedPage$3;
.super Ljava/lang/Object;
.source "FriendsRatedPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/FriendsRatedPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/FriendsRatedPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/FriendsRatedPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/FriendsRatedPage$3;->this$0:Lnet/flixster/android/FriendsRatedPage;

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 138
    iget-object v3, p0, Lnet/flixster/android/FriendsRatedPage$3;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->friendsRatedLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lnet/flixster/android/FriendsRatedPage;->access$0(Lnet/flixster/android/FriendsRatedPage;)Landroid/widget/LinearLayout;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 139
    const/16 v1, 0xa

    .local v1, i:I
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/FriendsRatedPage$3;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->reviews:Ljava/util/List;
    invoke-static {v3}, Lnet/flixster/android/FriendsRatedPage;->access$2(Lnet/flixster/android/FriendsRatedPage;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 146
    return-void

    .line 140
    :cond_0
    iget-object v3, p0, Lnet/flixster/android/FriendsRatedPage$3;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->reviews:Ljava/util/List;
    invoke-static {v3}, Lnet/flixster/android/FriendsRatedPage;->access$2(Lnet/flixster/android/FriendsRatedPage;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Review;

    .line 141
    .local v2, review:Lnet/flixster/android/model/Review;
    new-instance v0, Lcom/flixster/android/view/FriendActivity;

    iget-object v3, p0, Lnet/flixster/android/FriendsRatedPage$3;->this$0:Lnet/flixster/android/FriendsRatedPage;

    invoke-direct {v0, v3}, Lcom/flixster/android/view/FriendActivity;-><init>(Landroid/content/Context;)V

    .line 142
    .local v0, activity:Lcom/flixster/android/view/FriendActivity;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/flixster/android/view/FriendActivity;->setFocusable(Z)V

    .line 143
    iget-object v3, p0, Lnet/flixster/android/FriendsRatedPage$3;->this$0:Lnet/flixster/android/FriendsRatedPage;

    #getter for: Lnet/flixster/android/FriendsRatedPage;->friendsRatedLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lnet/flixster/android/FriendsRatedPage;->access$0(Lnet/flixster/android/FriendsRatedPage;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 144
    invoke-virtual {v0, v2}, Lcom/flixster/android/view/FriendActivity;->load(Lnet/flixster/android/model/Review;)V

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
