.class public Lnet/flixster/android/FriendsRatedPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "FriendsRatedPage.java"


# static fields
.field protected static final MAX_REVIEWS:I = 0xa


# instance fields
.field private final errorHandler:Landroid/os/Handler;

.field private final friendsActivitySuccessHandler:Landroid/os/Handler;

.field private friendsRatedLayout:Landroid/widget/LinearLayout;

.field private moreReviewsListener:Landroid/view/View$OnClickListener;

.field private reviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field private user:Lnet/flixster/android/model/User;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 56
    new-instance v0, Lnet/flixster/android/FriendsRatedPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/FriendsRatedPage$1;-><init>(Lnet/flixster/android/FriendsRatedPage;)V

    iput-object v0, p0, Lnet/flixster/android/FriendsRatedPage;->errorHandler:Landroid/os/Handler;

    .line 105
    new-instance v0, Lnet/flixster/android/FriendsRatedPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/FriendsRatedPage$2;-><init>(Lnet/flixster/android/FriendsRatedPage;)V

    iput-object v0, p0, Lnet/flixster/android/FriendsRatedPage;->friendsActivitySuccessHandler:Landroid/os/Handler;

    .line 136
    new-instance v0, Lnet/flixster/android/FriendsRatedPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/FriendsRatedPage$3;-><init>(Lnet/flixster/android/FriendsRatedPage;)V

    iput-object v0, p0, Lnet/flixster/android/FriendsRatedPage;->moreReviewsListener:Landroid/view/View$OnClickListener;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/FriendsRatedPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lnet/flixster/android/FriendsRatedPage;->friendsRatedLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/FriendsRatedPage;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lnet/flixster/android/FriendsRatedPage;->reviews:Ljava/util/List;

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/FriendsRatedPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lnet/flixster/android/FriendsRatedPage;->reviews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/FriendsRatedPage;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 136
    iget-object v0, p0, Lnet/flixster/android/FriendsRatedPage;->moreReviewsListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private initializeViews()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 67
    const-string v10, "FlxMain"

    const-string v11, "FriendsRatedPage.initializeViews"

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v10, p0, Lnet/flixster/android/FriendsRatedPage;->friendsActivitySuccessHandler:Landroid/os/Handler;

    iget-object v11, p0, Lnet/flixster/android/FriendsRatedPage;->errorHandler:Landroid/os/Handler;

    invoke-static {v10, v11}, Lnet/flixster/android/data/ProfileDao;->getFriendsActivity(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 69
    const v10, 0x7f07009e

    invoke-virtual {p0, v10}, Lnet/flixster/android/FriendsRatedPage;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    .line 70
    .local v9, userProfile:Landroid/widget/RelativeLayout;
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v10

    iput-object v10, p0, Lnet/flixster/android/FriendsRatedPage;->user:Lnet/flixster/android/model/User;

    .line 73
    invoke-virtual {v9, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 74
    const v10, 0x7f0700a2

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 75
    .local v2, headerStatus:Landroid/widget/TextView;
    iget-object v10, p0, Lnet/flixster/android/FriendsRatedPage;->user:Lnet/flixster/android/model/User;

    iget-object v10, v10, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    const v10, 0x7f0700a5

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 78
    .local v4, profileFriendsText:Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lnet/flixster/android/FriendsRatedPage;->user:Lnet/flixster/android/model/User;

    iget v11, v11, Lnet/flixster/android/model/User;->friendCount:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0c0082

    invoke-virtual {p0, v11}, Lnet/flixster/android/FriendsRatedPage;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    const v10, 0x7f0700a4

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 80
    .local v5, profileRatedText:Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lnet/flixster/android/FriendsRatedPage;->user:Lnet/flixster/android/model/User;

    iget v11, v11, Lnet/flixster/android/model/User;->ratingCount:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0c0083

    invoke-virtual {p0, v11}, Lnet/flixster/android/FriendsRatedPage;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    const v10, 0x7f0700a3

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 82
    .local v6, profileWantToSeeText:Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lnet/flixster/android/FriendsRatedPage;->user:Lnet/flixster/android/model/User;

    iget v11, v11, Lnet/flixster/android/model/User;->wtsCount:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0c0084

    invoke-virtual {p0, v11}, Lnet/flixster/android/FriendsRatedPage;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v10, p0, Lnet/flixster/android/FriendsRatedPage;->user:Lnet/flixster/android/model/User;

    iget v1, v10, Lnet/flixster/android/model/User;->collectionsCount:I

    .line 85
    .local v1, collectionCount:I
    if-lez v1, :cond_0

    .line 86
    const v10, 0x7f0700a6

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 87
    .local v3, profileCollectionsText:Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f0c0085

    invoke-virtual {p0, v11}, Lnet/flixster/android/FriendsRatedPage;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    .end local v3           #profileCollectionsText:Landroid/widget/TextView;
    :cond_0
    const v10, 0x7f0700a0

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 92
    .local v7, thumbnailFrame:Landroid/widget/ImageView;
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/flixster/android/data/AccountManager;->isPlatformFlixster()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 93
    const v10, 0x7f0200bc

    invoke-virtual {v7, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 98
    :cond_1
    :goto_0
    const v10, 0x7f07009f

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 99
    .local v8, userIcon:Landroid/widget/ImageView;
    iget-object v10, p0, Lnet/flixster/android/FriendsRatedPage;->user:Lnet/flixster/android/model/User;

    invoke-virtual {v10, v8}, Lnet/flixster/android/model/User;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 100
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 101
    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 103
    :cond_2
    return-void

    .line 94
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v8           #userIcon:Landroid/widget/ImageView;
    :cond_3
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/flixster/android/data/AccountManager;->isPlatformFacebook()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 95
    const v10, 0x7f020149

    invoke-virtual {v7, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    const-string v0, "/friendsmovies/friendsrated"

    return-object v0
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Friends Rated Page for user:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lnet/flixster/android/FriendsRatedPage;->user:Lnet/flixster/android/model/User;

    iget-object v1, v1, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v0, 0x7f030031

    invoke-virtual {p0, v0}, Lnet/flixster/android/FriendsRatedPage;->setContentView(I)V

    .line 43
    invoke-virtual {p0}, Lnet/flixster/android/FriendsRatedPage;->createActionBar()V

    .line 44
    const v0, 0x7f0c016c

    invoke-virtual {p0, v0}, Lnet/flixster/android/FriendsRatedPage;->setActionBarTitle(I)V

    .line 46
    const v0, 0x7f0700a7

    invoke-virtual {p0, v0}, Lnet/flixster/android/FriendsRatedPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lnet/flixster/android/FriendsRatedPage;->friendsRatedLayout:Landroid/widget/LinearLayout;

    .line 47
    invoke-direct {p0}, Lnet/flixster/android/FriendsRatedPage;->initializeViews()V

    .line 48
    return-void
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 151
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 53
    invoke-virtual {p0}, Lnet/flixster/android/FriendsRatedPage;->trackPage()V

    .line 54
    return-void
.end method

.method protected trackPage()V
    .locals 3

    .prologue
    .line 164
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    invoke-virtual {p0}, Lnet/flixster/android/FriendsRatedPage;->getAnalyticsTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lnet/flixster/android/FriendsRatedPage;->getAnalyticsTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    return-void
.end method
