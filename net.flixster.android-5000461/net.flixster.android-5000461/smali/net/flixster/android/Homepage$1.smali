.class Lnet/flixster/android/Homepage$1;
.super Landroid/os/Handler;
.source "Homepage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/Homepage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/Homepage;


# direct methods
.method constructor <init>(Lnet/flixster/android/Homepage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/Homepage$1;->this$0:Lnet/flixster/android/Homepage;

    .line 128
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 130
    const-string v2, "FlxMain"

    const-string v3, "Homepage.promoSuccessHandler"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v2, p0, Lnet/flixster/android/Homepage$1;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lnet/flixster/android/Homepage;->access$0(Lnet/flixster/android/Homepage;)Landroid/widget/ProgressBar;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 132
    iget-object v2, p0, Lnet/flixster/android/Homepage$1;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->carousel:Lcom/flixster/android/view/Carousel;
    invoke-static {v2}, Lnet/flixster/android/Homepage;->access$1(Lnet/flixster/android/Homepage;)Lcom/flixster/android/view/Carousel;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/Homepage$1;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->featuredItems:Ljava/util/List;
    invoke-static {v3}, Lnet/flixster/android/Homepage;->access$2(Lnet/flixster/android/Homepage;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/Homepage$1;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->carouselOnClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v4}, Lnet/flixster/android/Homepage;->access$3(Lnet/flixster/android/Homepage;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/flixster/android/view/Carousel;->load(Ljava/util/List;Landroid/view/View$OnClickListener;)V

    .line 133
    iget-object v2, p0, Lnet/flixster/android/Homepage$1;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->hotTodayItems:Ljava/util/List;
    invoke-static {v2}, Lnet/flixster/android/Homepage;->access$4(Lnet/flixster/android/Homepage;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 141
    iget-object v2, p0, Lnet/flixster/android/Homepage$1;->this$0:Lnet/flixster/android/Homepage;

    #calls: Lnet/flixster/android/Homepage;->checkAndShowLaunchAd()V
    invoke-static {v2}, Lnet/flixster/android/Homepage;->access$7(Lnet/flixster/android/Homepage;)V

    .line 142
    return-void

    .line 133
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/model/PromoItem;

    .line 134
    .local v1, item:Lcom/flixster/android/model/PromoItem;
    new-instance v0, Lcom/flixster/android/view/Headline;

    iget-object v3, p0, Lnet/flixster/android/Homepage$1;->this$0:Lnet/flixster/android/Homepage;

    invoke-direct {v0, v3}, Lcom/flixster/android/view/Headline;-><init>(Landroid/content/Context;)V

    .line 135
    .local v0, headline:Lcom/flixster/android/view/Headline;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/flixster/android/view/Headline;->setFocusable(Z)V

    .line 136
    iget-object v3, p0, Lnet/flixster/android/Homepage$1;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->hotTodayLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lnet/flixster/android/Homepage;->access$5(Lnet/flixster/android/Homepage;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 137
    invoke-virtual {v0, v1}, Lcom/flixster/android/view/Headline;->setTag(Ljava/lang/Object;)V

    .line 138
    iget-object v3, p0, Lnet/flixster/android/Homepage$1;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->headlineOnClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v3}, Lnet/flixster/android/Homepage;->access$6(Lnet/flixster/android/Homepage;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/flixster/android/view/Headline;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    invoke-virtual {v0, v1}, Lcom/flixster/android/view/Headline;->load(Lcom/flixster/android/model/HeadlineItem;)V

    goto :goto_0
.end method
