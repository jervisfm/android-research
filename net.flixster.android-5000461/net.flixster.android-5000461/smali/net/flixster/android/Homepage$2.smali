.class Lnet/flixster/android/Homepage$2;
.super Landroid/os/Handler;
.source "Homepage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/Homepage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/Homepage;


# direct methods
.method constructor <init>(Lnet/flixster/android/Homepage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/Homepage$2;->this$0:Lnet/flixster/android/Homepage;

    .line 145
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    const/16 v3, 0x8

    .line 147
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Homepage.errorHandler: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 159
    iget-object v0, p0, Lnet/flixster/android/Homepage$2;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lnet/flixster/android/Homepage;->access$0(Lnet/flixster/android/Homepage;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 163
    :goto_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lnet/flixster/android/data/DaoException;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/data/DaoException;

    iget-object v1, p0, Lnet/flixster/android/Homepage$2;->this$0:Lnet/flixster/android/Homepage;

    iget-object v2, p0, Lnet/flixster/android/Homepage$2;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;
    invoke-static {v2}, Lnet/flixster/android/Homepage;->access$12(Lnet/flixster/android/Homepage;)Lcom/flixster/android/activity/decorator/DialogDecorator;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;Lcom/flixster/android/activity/decorator/DialogDecorator;)V

    .line 166
    :cond_0
    return-void

    .line 151
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/Homepage$2;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->headerNetflixDvd:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/Homepage;->access$8(Lnet/flixster/android/Homepage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 152
    iget-object v0, p0, Lnet/flixster/android/Homepage$2;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->throbberNetflixDvd:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lnet/flixster/android/Homepage;->access$9(Lnet/flixster/android/Homepage;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 155
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/Homepage$2;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->headerNetflixInstant:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/Homepage;->access$10(Lnet/flixster/android/Homepage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lnet/flixster/android/Homepage$2;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->throbberNetflixInstant:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lnet/flixster/android/Homepage;->access$11(Lnet/flixster/android/Homepage;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
