.class Lnet/flixster/android/Homepage$3;
.super Ljava/lang/Object;
.source "Homepage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/Homepage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/Homepage;


# direct methods
.method constructor <init>(Lnet/flixster/android/Homepage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/Homepage$3;->this$0:Lnet/flixster/android/Homepage;

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 172
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_1

    .line 173
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    .line 174
    .local v1, movie:Lnet/flixster/android/model/Movie;
    instance-of v2, v1, Lnet/flixster/android/model/Season;

    if-eqz v2, :cond_0

    .line 175
    invoke-virtual {v1}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v2

    iget-object v4, p0, Lnet/flixster/android/Homepage$3;->this$0:Lnet/flixster/android/Homepage;

    invoke-static {v2, v3, v4}, Lnet/flixster/android/Starter;->launchSeasonDetail(JLandroid/content/Context;)V

    .line 184
    .end local v1           #movie:Lnet/flixster/android/model/Movie;
    :goto_0
    return-void

    .line 177
    .restart local v1       #movie:Lnet/flixster/android/model/Movie;
    :cond_0
    invoke-virtual {v1}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v2

    iget-object v4, p0, Lnet/flixster/android/Homepage$3;->this$0:Lnet/flixster/android/Homepage;

    invoke-static {v2, v3, v4}, Lnet/flixster/android/Starter;->launchMovieDetail(JLandroid/content/Context;)V

    goto :goto_0

    .line 180
    .end local v1           #movie:Lnet/flixster/android/model/Movie;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lnet/flixster/android/Homepage$3;->this$0:Lnet/flixster/android/Homepage;

    const-class v3, Lnet/flixster/android/NetflixQueuePage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 181
    .local v0, intent:Landroid/content/Intent;
    const/high16 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 182
    iget-object v2, p0, Lnet/flixster/android/Homepage$3;->this$0:Lnet/flixster/android/Homepage;

    invoke-virtual {v2, v0}, Lnet/flixster/android/Homepage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
