.class Lnet/flixster/android/Homepage$5;
.super Ljava/lang/Object;
.source "Homepage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/Homepage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/Homepage;


# direct methods
.method constructor <init>(Lnet/flixster/android/Homepage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/Homepage$5;->this$0:Lnet/flixster/android/Homepage;

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .parameter "v"

    .prologue
    .line 202
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/flixster/android/model/PromoItem;

    .line 203
    .local v7, pi:Lcom/flixster/android/model/PromoItem;
    invoke-virtual {v7}, Lcom/flixster/android/model/PromoItem;->getLinkUrl()Ljava/lang/String;

    move-result-object v8

    .line 204
    .local v8, url:Ljava/lang/String;
    iget-object v0, p0, Lnet/flixster/android/Homepage$5;->this$0:Lnet/flixster/android/Homepage;

    #calls: Lnet/flixster/android/Homepage;->onPromoLinkClick(Ljava/lang/String;)V
    invoke-static {v0, v8}, Lnet/flixster/android/Homepage;->access$13(Lnet/flixster/android/Homepage;Ljava/lang/String;)V

    .line 206
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/homepage"

    const-string v2, "Homepage"

    const-string v3, "Homepage"

    const-string v4, "HotToday"

    .line 207
    invoke-virtual {v7}, Lcom/flixster/android/model/PromoItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/flixster/android/utils/StringHelper;->getFirstTwoWords(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 206
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 208
    return-void
.end method
