.class Lnet/flixster/android/Homepage$7;
.super Landroid/os/Handler;
.source "Homepage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/Homepage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/Homepage;


# direct methods
.method constructor <init>(Lnet/flixster/android/Homepage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/Homepage$7;->this$0:Lnet/flixster/android/Homepage;

    .line 342
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .parameter "msg"

    .prologue
    const/16 v7, 0x8

    const/4 v8, 0x4

    .line 344
    iget-object v6, p0, Lnet/flixster/android/Homepage$7;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->throbberNetflixInstant:Landroid/widget/ProgressBar;
    invoke-static {v6}, Lnet/flixster/android/Homepage;->access$11(Lnet/flixster/android/Homepage;)Landroid/widget/ProgressBar;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 345
    iget-object v6, p0, Lnet/flixster/android/Homepage$7;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->galleryNetflixInstant:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lnet/flixster/android/Homepage;->access$16(Lnet/flixster/android/Homepage;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 346
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lnet/flixster/android/model/NetflixQueue;

    .line 348
    .local v5, queue:Lnet/flixster/android/model/NetflixQueue;
    const/4 v0, 0x0

    .line 349
    .local v0, index:I
    invoke-virtual {v5}, Lnet/flixster/android/model/NetflixQueue;->getNetflixQueueItemList()Ljava/util/ArrayList;

    move-result-object v4

    .line 350
    .local v4, nqiList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NetflixQueueItem;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 351
    iget-object v6, p0, Lnet/flixster/android/Homepage$7;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->headerNetflixInstant:Landroid/widget/TextView;
    invoke-static {v6}, Lnet/flixster/android/Homepage;->access$10(Lnet/flixster/android/Homepage;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 376
    :goto_0
    return-void

    .line 354
    :cond_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 370
    :goto_1
    new-instance v2, Lnet/flixster/android/MovieCollectionItem;

    iget-object v6, p0, Lnet/flixster/android/Homepage$7;->this$0:Lnet/flixster/android/Homepage;

    invoke-direct {v2, v6}, Lnet/flixster/android/MovieCollectionItem;-><init>(Landroid/content/Context;)V

    .local v2, movieView:Landroid/view/View;
    move-object v6, v2

    .line 371
    check-cast v6, Lnet/flixster/android/MovieCollectionItem;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v8}, Lnet/flixster/android/MovieCollectionItem;->load(Lnet/flixster/android/model/Movie;I)V

    .line 372
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 373
    iget-object v6, p0, Lnet/flixster/android/Homepage$7;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->movieGalleryClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v6}, Lnet/flixster/android/Homepage;->access$15(Lnet/flixster/android/Homepage;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 374
    iget-object v6, p0, Lnet/flixster/android/Homepage$7;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->galleryNetflixInstant:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lnet/flixster/android/Homepage;->access$16(Lnet/flixster/android/Homepage;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 354
    .end local v2           #movieView:Landroid/view/View;
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/NetflixQueueItem;

    .line 355
    .local v3, nqi:Lnet/flixster/android/model/NetflixQueueItem;
    iget-object v1, v3, Lnet/flixster/android/model/NetflixQueueItem;->mMovie:Lnet/flixster/android/model/Movie;

    .line 356
    .local v1, movie:Lnet/flixster/android/model/Movie;
    if-eqz v1, :cond_3

    .line 357
    new-instance v2, Lnet/flixster/android/MovieCollectionItem;

    iget-object v6, p0, Lnet/flixster/android/Homepage$7;->this$0:Lnet/flixster/android/Homepage;

    invoke-direct {v2, v6}, Lnet/flixster/android/MovieCollectionItem;-><init>(Landroid/content/Context;)V

    .restart local v2       #movieView:Landroid/view/View;
    move-object v6, v2

    .line 358
    check-cast v6, Lnet/flixster/android/MovieCollectionItem;

    invoke-virtual {v6, v1, v8}, Lnet/flixster/android/MovieCollectionItem;->load(Lnet/flixster/android/model/Movie;I)V

    .line 359
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 360
    iget-object v6, p0, Lnet/flixster/android/Homepage$7;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->movieGalleryClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v6}, Lnet/flixster/android/Homepage;->access$15(Lnet/flixster/android/Homepage;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 361
    iget-object v6, p0, Lnet/flixster/android/Homepage$7;->this$0:Lnet/flixster/android/Homepage;

    #getter for: Lnet/flixster/android/Homepage;->galleryNetflixInstant:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lnet/flixster/android/Homepage;->access$16(Lnet/flixster/android/Homepage;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 362
    add-int/lit8 v0, v0, 0x1

    .line 365
    .end local v2           #movieView:Landroid/view/View;
    :cond_3
    const/16 v6, 0xa

    if-lt v0, v6, :cond_1

    goto :goto_1
.end method
