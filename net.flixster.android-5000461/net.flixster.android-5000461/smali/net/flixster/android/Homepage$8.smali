.class Lnet/flixster/android/Homepage$8;
.super Ljava/lang/Object;
.source "Homepage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/Homepage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/Homepage;


# direct methods
.method constructor <init>(Lnet/flixster/android/Homepage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/Homepage$8;->this$0:Lnet/flixster/android/Homepage;

    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "v"

    .prologue
    .line 384
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 385
    .local v1, isLoggedIn:Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 458
    :goto_1
    return-void

    .line 384
    .end local v1           #isLoggedIn:Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 389
    .restart local v1       #isLoggedIn:Z
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lnet/flixster/android/Homepage$8;->this$0:Lnet/flixster/android/Homepage;

    const-class v3, Lnet/flixster/android/TopPhotosPage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 390
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "KEY_PHOTO_FILTER"

    const-string v3, "top-actresses"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 391
    iget-object v2, p0, Lnet/flixster/android/Homepage$8;->this$0:Lnet/flixster/android/Homepage;

    invoke-virtual {v2, v0}, Lnet/flixster/android/Homepage;->startActivity(Landroid/content/Intent;)V

    .line 392
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/homepage"

    const-string v4, "Homepage"

    const-string v5, "Homepage"

    const-string v6, "FunStuffTopPhotos"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 395
    .end local v0           #intent:Landroid/content/Intent;
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lnet/flixster/android/Homepage$8;->this$0:Lnet/flixster/android/Homepage;

    const-class v3, Lnet/flixster/android/TopActorsPage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 397
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v2, p0, Lnet/flixster/android/Homepage$8;->this$0:Lnet/flixster/android/Homepage;

    invoke-virtual {v2, v0}, Lnet/flixster/android/Homepage;->startActivity(Landroid/content/Intent;)V

    .line 398
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/homepage"

    const-string v4, "Homepage"

    const-string v5, "Homepage"

    const-string v6, "FunStuffTopActors"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 446
    .end local v0           #intent:Landroid/content/Intent;
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lnet/flixster/android/Homepage$8;->this$0:Lnet/flixster/android/Homepage;

    const-class v3, Lnet/flixster/android/FriendsRatedPage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 447
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v2, "net.flixster.IsConnected"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 448
    iget-object v2, p0, Lnet/flixster/android/Homepage$8;->this$0:Lnet/flixster/android/Homepage;

    invoke-virtual {v2, v0}, Lnet/flixster/android/Homepage;->startActivity(Landroid/content/Intent;)V

    .line 449
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/homepage"

    const-string v4, "Homepage"

    const-string v5, "Homepage"

    const-string v6, "FriendActivitySeeMore"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 453
    .end local v0           #intent:Landroid/content/Intent;
    :sswitch_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/mymovies/netflix/login"

    const-string v4, "My Movies - Netflix Login"

    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lnet/flixster/android/Homepage$8;->this$0:Lnet/flixster/android/Homepage;

    const-class v3, Lnet/flixster/android/NetflixAuth;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 455
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v2, p0, Lnet/flixster/android/Homepage$8;->this$0:Lnet/flixster/android/Homepage;

    invoke-virtual {v2, v0}, Lnet/flixster/android/Homepage;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 385
    :sswitch_data_0
    .sparse-switch
        0x7f0700c4 -> :sswitch_3
        0x7f0700ca -> :sswitch_0
        0x7f0700cb -> :sswitch_1
        0x7f0700dc -> :sswitch_2
    .end sparse-switch
.end method
