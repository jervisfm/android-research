.class public Lnet/flixster/android/Homepage;
.super Lcom/flixster/android/activity/TabbedActivity;
.source "Homepage.java"


# static fields
.field private static final MAX_MOVIES_DISPLAYED:I = 0xa


# instance fields
.field private adView:Lnet/flixster/android/ads/AdView;

.field private carousel:Lcom/flixster/android/view/Carousel;

.field private final carouselOnClickListener:Landroid/view/View$OnClickListener;

.field private final errorHandler:Landroid/os/Handler;

.field private final featuredItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/model/CarouselItem;",
            ">;"
        }
    .end annotation
.end field

.field private galleryNetflixDvd:Landroid/widget/LinearLayout;

.field private galleryNetflixInstant:Landroid/widget/LinearLayout;

.field private hasCreated:Z

.field private headerNetflixDvd:Landroid/widget/TextView;

.field private headerNetflixInstant:Landroid/widget/TextView;

.field private headerNetflixLogin:Landroid/widget/TextView;

.field private final headlineOnClickListener:Landroid/view/View$OnClickListener;

.field private final homepageOnClickListener:Landroid/view/View$OnClickListener;

.field private final hotTodayItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flixster/android/model/PromoItem;",
            ">;"
        }
    .end annotation
.end field

.field private hotTodayLayout:Landroid/widget/LinearLayout;

.field private lastNetflixUserId:Ljava/lang/String;

.field mAsyncRunner:Lcom/facebook/android/AsyncFacebookRunner;

.field private final movieGalleryClickListener:Landroid/view/View$OnClickListener;

.field private final netflixDvdQueueSuccessHandler:Landroid/os/Handler;

.field private final netflixInstantQueueSuccessHandler:Landroid/os/Handler;

.field private netflixLoginDescription:Landroid/widget/TextView;

.field private final promoSuccessHandler:Landroid/os/Handler;

.field private throbber:Landroid/widget/ProgressBar;

.field private throbberNetflixDvd:Landroid/widget/ProgressBar;

.field private throbberNetflixInstant:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/flixster/android/activity/TabbedActivity;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/Homepage;->featuredItems:Ljava/util/List;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/Homepage;->hotTodayItems:Ljava/util/List;

    .line 128
    new-instance v0, Lnet/flixster/android/Homepage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/Homepage$1;-><init>(Lnet/flixster/android/Homepage;)V

    iput-object v0, p0, Lnet/flixster/android/Homepage;->promoSuccessHandler:Landroid/os/Handler;

    .line 145
    new-instance v0, Lnet/flixster/android/Homepage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/Homepage$2;-><init>(Lnet/flixster/android/Homepage;)V

    iput-object v0, p0, Lnet/flixster/android/Homepage;->errorHandler:Landroid/os/Handler;

    .line 169
    new-instance v0, Lnet/flixster/android/Homepage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/Homepage$3;-><init>(Lnet/flixster/android/Homepage;)V

    iput-object v0, p0, Lnet/flixster/android/Homepage;->movieGalleryClickListener:Landroid/view/View$OnClickListener;

    .line 187
    new-instance v0, Lnet/flixster/android/Homepage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/Homepage$4;-><init>(Lnet/flixster/android/Homepage;)V

    iput-object v0, p0, Lnet/flixster/android/Homepage;->carouselOnClickListener:Landroid/view/View$OnClickListener;

    .line 199
    new-instance v0, Lnet/flixster/android/Homepage$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/Homepage$5;-><init>(Lnet/flixster/android/Homepage;)V

    iput-object v0, p0, Lnet/flixster/android/Homepage;->headlineOnClickListener:Landroid/view/View$OnClickListener;

    .line 305
    new-instance v0, Lnet/flixster/android/Homepage$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/Homepage$6;-><init>(Lnet/flixster/android/Homepage;)V

    iput-object v0, p0, Lnet/flixster/android/Homepage;->netflixDvdQueueSuccessHandler:Landroid/os/Handler;

    .line 342
    new-instance v0, Lnet/flixster/android/Homepage$7;

    invoke-direct {v0, p0}, Lnet/flixster/android/Homepage$7;-><init>(Lnet/flixster/android/Homepage;)V

    iput-object v0, p0, Lnet/flixster/android/Homepage;->netflixInstantQueueSuccessHandler:Landroid/os/Handler;

    .line 379
    new-instance v0, Lnet/flixster/android/Homepage$8;

    invoke-direct {v0, p0}, Lnet/flixster/android/Homepage$8;-><init>(Lnet/flixster/android/Homepage;)V

    iput-object v0, p0, Lnet/flixster/android/Homepage;->homepageOnClickListener:Landroid/view/View$OnClickListener;

    .line 42
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/Homepage;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lnet/flixster/android/Homepage;->throbber:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/Homepage;)Lcom/flixster/android/view/Carousel;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lnet/flixster/android/Homepage;->carousel:Lcom/flixster/android/view/Carousel;

    return-object v0
.end method

.method static synthetic access$10(Lnet/flixster/android/Homepage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lnet/flixster/android/Homepage;->headerNetflixInstant:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$11(Lnet/flixster/android/Homepage;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lnet/flixster/android/Homepage;->throbberNetflixInstant:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$12(Lnet/flixster/android/Homepage;)Lcom/flixster/android/activity/decorator/DialogDecorator;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lnet/flixster/android/Homepage;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    return-object v0
.end method

.method static synthetic access$13(Lnet/flixster/android/Homepage;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 211
    invoke-direct {p0, p1}, Lnet/flixster/android/Homepage;->onPromoLinkClick(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$14(Lnet/flixster/android/Homepage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lnet/flixster/android/Homepage;->galleryNetflixDvd:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$15(Lnet/flixster/android/Homepage;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 169
    iget-object v0, p0, Lnet/flixster/android/Homepage;->movieGalleryClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$16(Lnet/flixster/android/Homepage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lnet/flixster/android/Homepage;->galleryNetflixInstant:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/Homepage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lnet/flixster/android/Homepage;->featuredItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/Homepage;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, Lnet/flixster/android/Homepage;->carouselOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/Homepage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lnet/flixster/android/Homepage;->hotTodayItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/Homepage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lnet/flixster/android/Homepage;->hotTodayLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/Homepage;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 199
    iget-object v0, p0, Lnet/flixster/android/Homepage;->headlineOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$7(Lnet/flixster/android/Homepage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lnet/flixster/android/Homepage;->checkAndShowLaunchAd()V

    return-void
.end method

.method static synthetic access$8(Lnet/flixster/android/Homepage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lnet/flixster/android/Homepage;->headerNetflixDvd:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$9(Lnet/flixster/android/Homepage;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lnet/flixster/android/Homepage;->throbberNetflixDvd:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private initializeNetflixViews()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 263
    const v3, 0x7f0700c4

    invoke-virtual {p0, v3}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 264
    .local v0, netflixLogin:Landroid/widget/ImageView;
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isUsLocale()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isCanadaLocale()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 265
    :cond_0
    iget-object v3, p0, Lnet/flixster/android/Homepage;->homepageOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 266
    const v3, 0x7f0700c9

    invoke-virtual {p0, v3}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 267
    .local v2, netflixLoginMessage:Landroid/widget/TextView;
    const v3, 0x7f0700c8

    invoke-virtual {p0, v3}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 268
    .local v1, netflixLoginButton:Landroid/widget/ImageView;
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getNetflixUserId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lnet/flixster/android/Homepage;->lastNetflixUserId:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 270
    iget-object v3, p0, Lnet/flixster/android/Homepage;->headerNetflixDvd:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    iget-object v3, p0, Lnet/flixster/android/Homepage;->throbberNetflixDvd:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 272
    iget-object v3, p0, Lnet/flixster/android/Homepage;->netflixDvdQueueSuccessHandler:Landroid/os/Handler;

    iget-object v4, p0, Lnet/flixster/android/Homepage;->errorHandler:Landroid/os/Handler;

    const-string v5, "/queues/disc/available"

    invoke-static {v3, v4, v5}, Lnet/flixster/android/data/NetflixDao;->fetchQueue(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    .line 275
    iget-object v3, p0, Lnet/flixster/android/Homepage;->headerNetflixInstant:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 276
    iget-object v3, p0, Lnet/flixster/android/Homepage;->throbberNetflixInstant:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 277
    iget-object v3, p0, Lnet/flixster/android/Homepage;->netflixInstantQueueSuccessHandler:Landroid/os/Handler;

    iget-object v4, p0, Lnet/flixster/android/Homepage;->errorHandler:Landroid/os/Handler;

    const-string v5, "/queues/instant/available"

    invoke-static {v3, v4, v5}, Lnet/flixster/android/data/NetflixDao;->fetchQueue(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    .line 279
    iget-object v3, p0, Lnet/flixster/android/Homepage;->headerNetflixLogin:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 280
    invoke-virtual {p0}, Lnet/flixster/android/Homepage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00d0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    const v3, 0x7f02013a

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 282
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 283
    iget-object v3, p0, Lnet/flixster/android/Homepage;->netflixLoginDescription:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 303
    .end local v1           #netflixLoginButton:Landroid/widget/ImageView;
    .end local v2           #netflixLoginMessage:Landroid/widget/TextView;
    :goto_0
    return-void

    .line 285
    .restart local v1       #netflixLoginButton:Landroid/widget/ImageView;
    .restart local v2       #netflixLoginMessage:Landroid/widget/TextView;
    :cond_1
    iget-object v3, p0, Lnet/flixster/android/Homepage;->headerNetflixDvd:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 286
    iget-object v3, p0, Lnet/flixster/android/Homepage;->headerNetflixInstant:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    iget-object v3, p0, Lnet/flixster/android/Homepage;->galleryNetflixDvd:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 289
    iget-object v3, p0, Lnet/flixster/android/Homepage;->galleryNetflixInstant:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 291
    iget-object v3, p0, Lnet/flixster/android/Homepage;->headerNetflixLogin:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 293
    iget-object v3, p0, Lnet/flixster/android/Homepage;->netflixLoginDescription:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 296
    .end local v1           #netflixLoginButton:Landroid/widget/ImageView;
    .end local v2           #netflixLoginMessage:Landroid/widget/TextView;
    :cond_2
    iget-object v3, p0, Lnet/flixster/android/Homepage;->headerNetflixDvd:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 297
    iget-object v3, p0, Lnet/flixster/android/Homepage;->headerNetflixInstant:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    iget-object v3, p0, Lnet/flixster/android/Homepage;->headerNetflixLogin:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 301
    iget-object v3, p0, Lnet/flixster/android/Homepage;->netflixLoginDescription:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private initializeStaticViews()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 220
    const-string v3, "FlxMain"

    const-string v4, "Homepage.initializeStaticViews"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const v3, 0x7f0700ca

    invoke-virtual {p0, v3}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 224
    .local v2, topPhotos:Landroid/widget/TextView;
    iget-object v3, p0, Lnet/flixster/android/Homepage;->homepageOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 226
    const v3, 0x7f0700cb

    invoke-virtual {p0, v3}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 227
    .local v0, topActors:Landroid/widget/TextView;
    iget-object v3, p0, Lnet/flixster/android/Homepage;->homepageOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 229
    const v3, 0x7f0700cc

    invoke-virtual {p0, v3}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 232
    .local v1, topNews:Landroid/widget/TextView;
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 259
    invoke-direct {p0}, Lnet/flixster/android/Homepage;->initializeNetflixViews()V

    .line 260
    return-void
.end method

.method private onPromoLinkClick(Ljava/lang/String;)V
    .locals 1
    .parameter "url"

    .prologue
    .line 212
    invoke-static {p1}, Lcom/flixster/android/activity/DeepLink;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    invoke-static {p1, p0}, Lcom/flixster/android/activity/DeepLink;->launch(Ljava/lang/String;Landroid/content/Context;)Z

    .line 217
    :goto_0
    return-void

    .line 215
    :cond_0
    const v0, 0x7f0c0135

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p0}, Lnet/flixster/android/Starter;->launchFlxHtmlPage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private onRealCreate()V
    .locals 4

    .prologue
    .line 72
    iget-boolean v0, p0, Lnet/flixster/android/Homepage;->hasCreated:Z

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/Homepage;->hasCreated:Z

    .line 74
    const-string v0, "FlxMain"

    const-string v1, "Homepage.onRealCreate"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const v0, 0x7f030035

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->setContentView(I)V

    .line 77
    const v0, 0x7f0700b9

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/AdView;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->adView:Lnet/flixster/android/ads/AdView;

    .line 78
    iget-object v0, p0, Lnet/flixster/android/Homepage;->adView:Lnet/flixster/android/ads/AdView;

    const-string v1, "Homepage"

    invoke-virtual {v0, v1}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 79
    const v0, 0x7f0700ba

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/view/Carousel;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->carousel:Lcom/flixster/android/view/Carousel;

    .line 80
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->throbber:Landroid/widget/ProgressBar;

    .line 81
    const v0, 0x7f0700bb

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->hotTodayLayout:Landroid/widget/LinearLayout;

    .line 83
    const v0, 0x7f0700bc

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->headerNetflixDvd:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0700bf

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->headerNetflixInstant:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0700c2

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->headerNetflixLogin:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0700c3

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->netflixLoginDescription:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0700bd

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->throbberNetflixDvd:Landroid/widget/ProgressBar;

    .line 89
    const v0, 0x7f0700c0

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->throbberNetflixInstant:Landroid/widget/ProgressBar;

    .line 91
    const v0, 0x7f0700be

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->galleryNetflixDvd:Landroid/widget/LinearLayout;

    .line 92
    const v0, 0x7f0700c1

    invoke-virtual {p0, v0}, Lnet/flixster/android/Homepage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lnet/flixster/android/Homepage;->galleryNetflixInstant:Landroid/widget/LinearLayout;

    .line 94
    iget-object v0, p0, Lnet/flixster/android/Homepage;->featuredItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 95
    iget-object v0, p0, Lnet/flixster/android/Homepage;->hotTodayItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 96
    iget-object v0, p0, Lnet/flixster/android/Homepage;->throbber:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lnet/flixster/android/Homepage;->featuredItems:Ljava/util/List;

    iget-object v1, p0, Lnet/flixster/android/Homepage;->hotTodayItems:Ljava/util/List;

    iget-object v2, p0, Lnet/flixster/android/Homepage;->promoSuccessHandler:Landroid/os/Handler;

    iget-object v3, p0, Lnet/flixster/android/Homepage;->errorHandler:Landroid/os/Handler;

    invoke-static {v0, v1, v2, v3}, Lcom/flixster/android/data/PromoDao;->get(Ljava/util/List;Ljava/util/List;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 99
    invoke-direct {p0}, Lnet/flixster/android/Homepage;->initializeStaticViews()V

    .line 101
    :cond_0
    return-void
.end method


# virtual methods
.method protected getAnalyticsAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 498
    const-string v0, "Logo"

    return-object v0
.end method

.method protected getAnalyticsCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 493
    const-string v0, "WidgetEntrance"

    return-object v0
.end method

.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 483
    const-string v0, "/homepage"

    return-object v0
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 488
    const-string v0, "Homepage"

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/flixster/android/activity/TabbedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    const-string v0, "FlxMain"

    const-string v1, "Homepage.onDummyCreate"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lnet/flixster/android/Homepage;->adView:Lnet/flixster/android/ads/AdView;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lnet/flixster/android/Homepage;->adView:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 125
    :cond_0
    invoke-super {p0}, Lcom/flixster/android/activity/TabbedActivity;->onDestroy()V

    .line 126
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 105
    invoke-direct {p0}, Lnet/flixster/android/Homepage;->onRealCreate()V

    .line 106
    invoke-super {p0}, Lcom/flixster/android/activity/TabbedActivity;->onResume()V

    .line 107
    const-string v1, "FlxMain"

    const-string v2, "Homepage.onResume"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getNetflixUserId()Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, currNetflixUserId:Ljava/lang/String;
    if-nez v0, :cond_0

    iget-object v1, p0, Lnet/flixster/android/Homepage;->lastNetflixUserId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 111
    :cond_0
    if-eqz v0, :cond_2

    iget-object v1, p0, Lnet/flixster/android/Homepage;->lastNetflixUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 112
    :cond_1
    iput-object v0, p0, Lnet/flixster/android/Homepage;->lastNetflixUserId:Ljava/lang/String;

    .line 113
    invoke-direct {p0}, Lnet/flixster/android/Homepage;->initializeNetflixViews()V

    .line 116
    :cond_2
    invoke-virtual {p0}, Lnet/flixster/android/Homepage;->trackPage()V

    .line 117
    iget-object v1, p0, Lnet/flixster/android/Homepage;->adView:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v1}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 118
    return-void
.end method
