.class Lnet/flixster/android/LocationMapPage$1;
.super Landroid/os/Handler;
.source "LocationMapPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/LocationMapPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/LocationMapPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/LocationMapPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    .line 124
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .parameter "msg"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 128
    iget-object v3, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/util/ArrayList;

    #setter for: Lnet/flixster/android/LocationMapPage;->locations:Ljava/util/ArrayList;
    invoke-static {v3, v2}, Lnet/flixster/android/LocationMapPage;->access$0(Lnet/flixster/android/LocationMapPage;Ljava/util/ArrayList;)V

    .line 129
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    #getter for: Lnet/flixster/android/LocationMapPage;->locations:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/LocationMapPage;->access$1(Lnet/flixster/android/LocationMapPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 130
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    invoke-virtual {v2}, Lnet/flixster/android/LocationMapPage;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 131
    const-string v3, "Please type a more specific location and try again"

    .line 130
    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 132
    .local v1, marker:Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 150
    .end local v1           #marker:Landroid/widget/Toast;
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    #getter for: Lnet/flixster/android/LocationMapPage;->locations:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/LocationMapPage;->access$1(Lnet/flixster/android/LocationMapPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v5, :cond_2

    .line 134
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    #getter for: Lnet/flixster/android/LocationMapPage;->locations:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/LocationMapPage;->access$1(Lnet/flixster/android/LocationMapPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Location;

    .line 135
    .local v0, location:Lnet/flixster/android/model/Location;
    iget-wide v2, v0, Lnet/flixster/android/model/Location;->latitude:D

    invoke-static {v2, v3}, Lnet/flixster/android/FlixsterApplication;->setUserLatitude(D)V

    .line 136
    iget-wide v2, v0, Lnet/flixster/android/model/Location;->longitude:D

    invoke-static {v2, v3}, Lnet/flixster/android/FlixsterApplication;->setUserLongitude(D)V

    .line 137
    iget-object v2, v0, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->setUserZip(Ljava/lang/String;)V

    .line 138
    iget-object v2, v0, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->setUserCity(Ljava/lang/String;)V

    .line 139
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    #calls: Lnet/flixster/android/LocationMapPage;->getLocationItemDisplay(Lnet/flixster/android/model/Location;)Ljava/lang/String;
    invoke-static {v2, v0}, Lnet/flixster/android/LocationMapPage;->access$2(Lnet/flixster/android/LocationMapPage;Lnet/flixster/android/model/Location;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->setUserLocation(Ljava/lang/String;)V

    .line 140
    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setUseLocationServiceFlag(Z)V

    .line 141
    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setLocationPolicy(I)V

    .line 142
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    const/4 v3, -0x1

    iget-object v4, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    invoke-virtual {v4}, Lnet/flixster/android/LocationMapPage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lnet/flixster/android/LocationMapPage;->setResult(ILandroid/content/Intent;)V

    .line 144
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    invoke-virtual {v2}, Lnet/flixster/android/LocationMapPage;->finish()V

    goto :goto_0

    .line 145
    .end local v0           #location:Lnet/flixster/android/model/Location;
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    #getter for: Lnet/flixster/android/LocationMapPage;->locations:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/LocationMapPage;->access$1(Lnet/flixster/android/LocationMapPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v5, :cond_0

    .line 146
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    invoke-virtual {v2}, Lnet/flixster/android/LocationMapPage;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 147
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage$1;->this$0:Lnet/flixster/android/LocationMapPage;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lnet/flixster/android/LocationMapPage;->showDialog(I)V

    goto :goto_0
.end method
