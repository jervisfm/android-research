.class Lnet/flixster/android/LocationMapPage$3;
.super Ljava/lang/Thread;
.source "LocationMapPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/LocationMapPage;->locationSelection()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/LocationMapPage;

.field private final synthetic val$isGeoPointMatched:Z

.field private final synthetic val$latitude:D

.field private final synthetic val$longitude:D

.field private final synthetic val$query:Ljava/lang/String;


# direct methods
.method constructor <init>(Lnet/flixster/android/LocationMapPage;ZDDLjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/LocationMapPage$3;->this$0:Lnet/flixster/android/LocationMapPage;

    iput-boolean p2, p0, Lnet/flixster/android/LocationMapPage$3;->val$isGeoPointMatched:Z

    iput-wide p3, p0, Lnet/flixster/android/LocationMapPage$3;->val$latitude:D

    iput-wide p5, p0, Lnet/flixster/android/LocationMapPage$3;->val$longitude:D

    iput-object p7, p0, Lnet/flixster/android/LocationMapPage$3;->val$query:Ljava/lang/String;

    .line 106
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 110
    :try_start_0
    iget-boolean v3, p0, Lnet/flixster/android/LocationMapPage$3;->val$isGeoPointMatched:Z

    if-eqz v3, :cond_1

    iget-wide v3, p0, Lnet/flixster/android/LocationMapPage$3;->val$latitude:D

    iget-wide v5, p0, Lnet/flixster/android/LocationMapPage$3;->val$longitude:D

    invoke-static {v3, v4, v5, v6}, Lnet/flixster/android/data/LocationDao;->getLocations(DD)Ljava/util/ArrayList;

    move-result-object v1

    .line 112
    .local v1, l:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Location;>;"
    :goto_0
    if-eqz v1, :cond_0

    .line 113
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 114
    .local v2, m:Landroid/os/Message;
    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 115
    iget-object v3, p0, Lnet/flixster/android/LocationMapPage$3;->this$0:Lnet/flixster/android/LocationMapPage;

    #getter for: Lnet/flixster/android/LocationMapPage;->locationSelectionHandler:Landroid/os/Handler;
    invoke-static {v3}, Lnet/flixster/android/LocationMapPage;->access$4(Lnet/flixster/android/LocationMapPage;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 120
    .end local v1           #l:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Location;>;"
    .end local v2           #m:Landroid/os/Message;
    :cond_0
    :goto_1
    return-void

    .line 111
    :cond_1
    iget-object v3, p0, Lnet/flixster/android/LocationMapPage$3;->val$query:Ljava/lang/String;

    invoke-static {v3}, Lnet/flixster/android/data/LocationDao;->getLocations(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v3, "FlxMain"

    const-string v4, "problem retrieving location"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
