.class public Lnet/flixster/android/LocationMapPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;
.source "LocationMapPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# static fields
.field private static final DIALOG_LOCATION:I = 0x2

.field private static final DIALOG_LOCATION_CHOICES:I = 0x3

.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field private static final MAP_ZOOM_DEFAULT:I = 0x10


# instance fields
.field private geoPointPattern:Ljava/util/regex/Pattern;

.field private locationSelectionHandler:Landroid/os/Handler;

.field private locationView:Landroid/widget/TextView;

.field private locations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Location;",
            ">;"
        }
    .end annotation
.end field

.field private mGoButton:Landroid/widget/Button;

.field private mLocationLabel:Landroid/widget/TextView;

.field private mLocationText:Landroid/widget/EditText;

.field private mapController:Lcom/google/android/maps/MapController;

.field private mapView:Lcom/google/android/maps/MapView;

.field private timer:Ljava/util/Timer;

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;-><init>()V

    .line 48
    const-string v0, "[(][-+]?[0-9]*\\.?[0-9]*[,] [-+]?[0-9]*\\.?[0-9]*[)]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/LocationMapPage;->geoPointPattern:Ljava/util/regex/Pattern;

    .line 124
    new-instance v0, Lnet/flixster/android/LocationMapPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/LocationMapPage$1;-><init>(Lnet/flixster/android/LocationMapPage;)V

    iput-object v0, p0, Lnet/flixster/android/LocationMapPage;->locationSelectionHandler:Landroid/os/Handler;

    .line 250
    new-instance v0, Lnet/flixster/android/LocationMapPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/LocationMapPage$2;-><init>(Lnet/flixster/android/LocationMapPage;)V

    iput-object v0, p0, Lnet/flixster/android/LocationMapPage;->updateHandler:Landroid/os/Handler;

    .line 38
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/LocationMapPage;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lnet/flixster/android/LocationMapPage;->locations:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/LocationMapPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->locations:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/LocationMapPage;Lnet/flixster/android/model/Location;)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 309
    invoke-direct {p0, p1}, Lnet/flixster/android/LocationMapPage;->getLocationItemDisplay(Lnet/flixster/android/model/Location;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/LocationMapPage;)V
    .locals 0
    .parameter

    .prologue
    .line 261
    invoke-direct {p0}, Lnet/flixster/android/LocationMapPage;->updatePage()V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/LocationMapPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 124
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->locationSelectionHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/LocationMapPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 250
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getLocationItemDisplay(Lnet/flixster/android/model/Location;)Ljava/lang/String;
    .locals 3
    .parameter "location"

    .prologue
    .line 310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 311
    .local v0, locationBuilder:Ljava/lang/StringBuilder;
    iget-object v1, p1, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ""

    iget-object v2, p1, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 312
    iget-object v1, p1, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    :cond_0
    iget-object v1, p1, Lnet/flixster/android/model/Location;->stateCode:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ""

    iget-object v2, p1, Lnet/flixster/android/model/Location;->stateCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 315
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 316
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    :cond_1
    iget-object v1, p1, Lnet/flixster/android/model/Location;->stateCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    :cond_2
    iget-object v1, p1, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ""

    iget-object v2, p1, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 321
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 322
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    :cond_3
    iget-object v1, p1, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    :cond_4
    iget-object v1, p1, Lnet/flixster/android/model/Location;->country:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ""

    iget-object v2, p1, Lnet/flixster/android/model/Location;->country:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "United States"

    iget-object v2, p1, Lnet/flixster/android/model/Location;->country:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 327
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_5

    .line 328
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    :cond_5
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lnet/flixster/android/model/Location;->country:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private locationSelection()V
    .locals 8

    .prologue
    .line 100
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->mLocationText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 101
    .local v7, query:Ljava/lang/String;
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->geoPointPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    .line 102
    .local v2, isGeoPointMatched:Z
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v3

    .line 103
    .local v3, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v5

    .line 106
    .local v5, longitude:D
    new-instance v0, Lnet/flixster/android/LocationMapPage$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lnet/flixster/android/LocationMapPage$3;-><init>(Lnet/flixster/android/LocationMapPage;ZDDLjava/lang/String;)V

    .line 121
    invoke-virtual {v0}, Lnet/flixster/android/LocationMapPage$3;->start()V

    .line 122
    return-void
.end method

.method private showCurrentLocation(DDLjava/lang/String;)V
    .locals 10
    .parameter "latitude"
    .parameter "longitude"
    .parameter "location"

    .prologue
    .line 283
    const-wide/16 v7, 0x0

    cmpl-double v7, p1, v7

    if-nez v7, :cond_0

    const-wide/16 v7, 0x0

    cmpl-double v7, p3, v7

    if-nez v7, :cond_0

    .line 284
    invoke-virtual {p0}, Lnet/flixster/android/LocationMapPage;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 285
    const-string v8, "Please enable a My Location source in system settings"

    const/4 v9, 0x0

    .line 284
    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 286
    .local v4, marker:Landroid/widget/Toast;
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 307
    .end local v4           #marker:Landroid/widget/Toast;
    :goto_0
    return-void

    .line 288
    :cond_0
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v5

    .line 289
    .local v5, numberFormat:Ljava/text/NumberFormat;
    const/4 v7, 0x4

    invoke-virtual {v5, v7}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 291
    :try_start_0
    iget-object v7, p0, Lnet/flixster/android/LocationMapPage;->mLocationLabel:Landroid/widget/TextView;

    invoke-virtual {v7, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    new-instance v1, Lcom/google/android/maps/GeoPoint;

    const-wide v7, 0x412e848000000000L

    mul-double/2addr v7, p1

    double-to-int v7, v7

    const-wide v8, 0x412e848000000000L

    mul-double/2addr v8, p3

    double-to-int v8, v8

    invoke-direct {v1, v7, v8}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 293
    .local v1, geoPoint:Lcom/google/android/maps/GeoPoint;
    iget-object v7, p0, Lnet/flixster/android/LocationMapPage;->mapController:Lcom/google/android/maps/MapController;

    invoke-virtual {v7, v1}, Lcom/google/android/maps/MapController;->setCenter(Lcom/google/android/maps/GeoPoint;)V

    .line 294
    new-instance v3, Lnet/flixster/android/TheaterItemizedOverlay;

    invoke-virtual {p0}, Lnet/flixster/android/LocationMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 295
    const v8, 0x7f0200ad

    .line 294
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-direct {v3, p0, v7}, Lnet/flixster/android/TheaterItemizedOverlay;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 296
    .local v3, locationOverlays:Lnet/flixster/android/TheaterItemizedOverlay;
    new-instance v2, Lcom/google/android/maps/OverlayItem;

    const-string v7, "Current Location"

    const/4 v8, 0x0

    invoke-direct {v2, v1, v7, v8}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    .local v2, locationOverlay:Lcom/google/android/maps/OverlayItem;
    invoke-virtual {v3, v2}, Lnet/flixster/android/TheaterItemizedOverlay;->addOverlay(Lcom/google/android/maps/OverlayItem;)V

    .line 298
    iget-object v7, p0, Lnet/flixster/android/LocationMapPage;->mapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v7}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v6

    .line 299
    .local v6, overlays:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 300
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    iget-object v7, p0, Lnet/flixster/android/LocationMapPage;->mapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v7}, Lcom/google/android/maps/MapView;->invalidate()V

    .line 302
    iget-object v7, p0, Lnet/flixster/android/LocationMapPage;->locationView:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 303
    .end local v1           #geoPoint:Lcom/google/android/maps/GeoPoint;
    .end local v2           #locationOverlay:Lcom/google/android/maps/OverlayItem;
    .end local v3           #locationOverlays:Lnet/flixster/android/TheaterItemizedOverlay;
    .end local v6           #overlays:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    :catch_0
    move-exception v0

    .line 304
    .local v0, e:Ljava/lang/Exception;
    const-string v7, "problem retrieving location"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updatePage()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 265
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v1

    .line 267
    .local v1, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v3

    .line 268
    .local v3, longitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLocationDisplay()Ljava/lang/String;

    move-result-object v5

    .local v5, location:Ljava/lang/String;
    :cond_0
    :goto_0
    move-object v0, p0

    .line 279
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/LocationMapPage;->showCurrentLocation(DDLjava/lang/String;)V

    .line 280
    return-void

    .line 270
    .end local v1           #latitude:D
    .end local v3           #longitude:D
    .end local v5           #location:Ljava/lang/String;
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v1

    .line 271
    .restart local v1       #latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v3

    .line 272
    .restart local v3       #longitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v5

    .line 273
    .restart local v5       #location:Ljava/lang/String;
    cmpl-double v0, v1, v6

    if-nez v0, :cond_0

    cmpl-double v0, v3, v6

    if-nez v0, :cond_0

    .line 274
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v1

    .line 275
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v3

    .line 276
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLocationDisplay()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "view"

    .prologue
    .line 154
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 173
    :goto_0
    :pswitch_0
    return-void

    .line 170
    :pswitch_1
    invoke-direct {p0}, Lnet/flixster/android/LocationMapPage;->locationSelection()V

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x7f0700e0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v2, 0x7f030041

    invoke-virtual {p0, v2}, Lnet/flixster/android/LocationMapPage;->setContentView(I)V

    .line 60
    invoke-virtual {p0}, Lnet/flixster/android/LocationMapPage;->createActionBar()V

    .line 61
    const v2, 0x7f0c0042

    invoke-virtual {p0, v2}, Lnet/flixster/android/LocationMapPage;->setActionBarTitle(I)V

    .line 63
    const v2, 0x7f0700df

    invoke-virtual {p0, v2}, Lnet/flixster/android/LocationMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lnet/flixster/android/LocationMapPage;->mLocationLabel:Landroid/widget/TextView;

    .line 64
    const v2, 0x7f0700e0

    invoke-virtual {p0, v2}, Lnet/flixster/android/LocationMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lnet/flixster/android/LocationMapPage;->mLocationText:Landroid/widget/EditText;

    .line 66
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage;->mLocationLabel:Landroid/widget/TextView;

    const-string v3, "?"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage;->mLocationText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lnet/flixster/android/LocationMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0130

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage;->mLocationText:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 70
    const v2, 0x7f0700e2

    invoke-virtual {p0, v2}, Lnet/flixster/android/LocationMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lnet/flixster/android/LocationMapPage;->locationView:Landroid/widget/TextView;

    .line 71
    const v2, 0x7f0700e4

    invoke-virtual {p0, v2}, Lnet/flixster/android/LocationMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/maps/MapView;

    iput-object v2, p0, Lnet/flixster/android/LocationMapPage;->mapView:Lcom/google/android/maps/MapView;

    .line 72
    const v2, 0x7f0700e5

    invoke-virtual {p0, v2}, Lnet/flixster/android/LocationMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 73
    .local v0, zoomLayout:Landroid/widget/LinearLayout;
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage;->mapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getZoomControls()Landroid/view/View;

    move-result-object v1

    .line 74
    .local v1, zoomView:Landroid/view/View;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 75
    const v2, 0x7f0700e1

    invoke-virtual {p0, v2}, Lnet/flixster/android/LocationMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lnet/flixster/android/LocationMapPage;->mGoButton:Landroid/widget/Button;

    .line 76
    iget-object v2, p0, Lnet/flixster/android/LocationMapPage;->mGoButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 11
    .parameter "dialogId"

    .prologue
    const v10, 0x7f0c0135

    const v9, 0x7f0c004a

    const/4 v8, 0x1

    .line 177
    packed-switch p1, :pswitch_data_0

    .line 229
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 230
    .local v1, defaultDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lnet/flixster/android/LocationMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 231
    invoke-virtual {v1, v8}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 232
    invoke-virtual {v1, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 233
    invoke-virtual {v1, v8}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    move-object v6, v1

    .line 234
    .end local v1           #defaultDialog:Landroid/app/ProgressDialog;
    :goto_0
    return-object v6

    .line 180
    :pswitch_0
    new-instance v6, Landroid/app/ProgressDialog;

    invoke-direct {v6, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 181
    .local v6, ratingsDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lnet/flixster/android/LocationMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 182
    invoke-virtual {v6, v8}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 183
    invoke-virtual {v6, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 184
    invoke-virtual {v6, v8}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 187
    .end local v6           #ratingsDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 188
    .local v4, locationChoiceBuilder:Landroid/app/AlertDialog$Builder;
    const-string v7, "Select a Location"

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 189
    iget-object v7, p0, Lnet/flixster/android/LocationMapPage;->locations:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v5, v7, [Ljava/lang/CharSequence;

    .line 190
    .local v5, locationItems:[Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    iget-object v7, p0, Lnet/flixster/android/LocationMapPage;->locations:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v2, v7, :cond_0

    .line 194
    new-instance v7, Lnet/flixster/android/LocationMapPage$4;

    invoke-direct {v7, p0}, Lnet/flixster/android/LocationMapPage$4;-><init>(Lnet/flixster/android/LocationMapPage;)V

    invoke-virtual {v4, v5, v7}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 208
    invoke-virtual {p0}, Lnet/flixster/android/LocationMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 209
    new-instance v8, Lnet/flixster/android/LocationMapPage$5;

    invoke-direct {v8, p0}, Lnet/flixster/android/LocationMapPage$5;-><init>(Lnet/flixster/android/LocationMapPage;)V

    .line 208
    invoke-virtual {v4, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 215
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    goto :goto_0

    .line 191
    :cond_0
    iget-object v7, p0, Lnet/flixster/android/LocationMapPage;->locations:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Location;

    .line 192
    .local v3, location:Lnet/flixster/android/model/Location;
    invoke-direct {p0, v3}, Lnet/flixster/android/LocationMapPage;->getLocationItemDisplay(Lnet/flixster/android/model/Location;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v2

    .line 190
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 217
    .end local v2           #i:I
    .end local v3           #location:Lnet/flixster/android/model/Location;
    .end local v4           #locationChoiceBuilder:Landroid/app/AlertDialog$Builder;
    .end local v5           #locationItems:[Ljava/lang/CharSequence;
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 218
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v7, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 219
    const-string v7, "Network Error"

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 220
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 221
    const-string v7, "Retry"

    new-instance v8, Lnet/flixster/android/LocationMapPage$6;

    invoke-direct {v8, p0}, Lnet/flixster/android/LocationMapPage$6;-><init>(Lnet/flixster/android/LocationMapPage;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 226
    invoke-virtual {p0}, Lnet/flixster/android/LocationMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 227
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    goto/16 :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 240
    const-string v0, "FlxMain"

    const-string v1, "LocationPage.onDestroy"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onDestroy()V

    .line 243
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 245
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 247
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/LocationMapPage;->timer:Ljava/util/Timer;

    .line 248
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "textView"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 95
    invoke-direct {p0}, Lnet/flixster/android/LocationMapPage;->locationSelection()V

    .line 96
    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 82
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onResume()V

    .line 83
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/LocationMapPage;->timer:Ljava/util/Timer;

    .line 86
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->mapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/LocationMapPage;->mapController:Lcom/google/android/maps/MapController;

    .line 87
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->mapController:Lcom/google/android/maps/MapController;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 88
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->mapView:Lcom/google/android/maps/MapView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapView;->displayZoomControls(Z)V

    .line 89
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/showtimes/location"

    const-string v2, "Movie - "

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lnet/flixster/android/LocationMapPage;->updateHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 92
    return-void
.end method
