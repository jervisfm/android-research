.class Lnet/flixster/android/LocationPage$1;
.super Landroid/os/Handler;
.source "LocationPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/LocationPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/LocationPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/LocationPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    .line 101
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .parameter "msg"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 105
    iget-object v3, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/util/ArrayList;

    #setter for: Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;
    invoke-static {v3, v2}, Lnet/flixster/android/LocationPage;->access$0(Lnet/flixster/android/LocationPage;Ljava/util/ArrayList;)V

    .line 106
    iget-object v2, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    #getter for: Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/LocationPage;->access$1(Lnet/flixster/android/LocationPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 107
    iget-object v2, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    invoke-virtual {v2}, Lnet/flixster/android/LocationPage;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 108
    const-string v3, "Please type a more specific location and try again"

    .line 107
    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 109
    .local v1, marker:Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 127
    .end local v1           #marker:Landroid/widget/Toast;
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    #getter for: Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/LocationPage;->access$1(Lnet/flixster/android/LocationPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v5, :cond_2

    .line 111
    iget-object v2, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    #getter for: Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/LocationPage;->access$1(Lnet/flixster/android/LocationPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Location;

    .line 112
    .local v0, location:Lnet/flixster/android/model/Location;
    iget-wide v2, v0, Lnet/flixster/android/model/Location;->latitude:D

    invoke-static {v2, v3}, Lnet/flixster/android/FlixsterApplication;->setUserLatitude(D)V

    .line 113
    iget-wide v2, v0, Lnet/flixster/android/model/Location;->longitude:D

    invoke-static {v2, v3}, Lnet/flixster/android/FlixsterApplication;->setUserLongitude(D)V

    .line 114
    iget-object v2, v0, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->setUserZip(Ljava/lang/String;)V

    .line 115
    iget-object v2, v0, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->setUserCity(Ljava/lang/String;)V

    .line 116
    iget-object v2, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    #calls: Lnet/flixster/android/LocationPage;->getLocationItemDisplay(Lnet/flixster/android/model/Location;)Ljava/lang/String;
    invoke-static {v2, v0}, Lnet/flixster/android/LocationPage;->access$2(Lnet/flixster/android/LocationPage;Lnet/flixster/android/model/Location;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->setUserLocation(Ljava/lang/String;)V

    .line 117
    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setUseLocationServiceFlag(Z)V

    .line 118
    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setLocationPolicy(I)V

    .line 119
    iget-object v2, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    const/4 v3, -0x1

    iget-object v4, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    invoke-virtual {v4}, Lnet/flixster/android/LocationPage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lnet/flixster/android/LocationPage;->setResult(ILandroid/content/Intent;)V

    .line 121
    iget-object v2, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    invoke-virtual {v2}, Lnet/flixster/android/LocationPage;->finish()V

    goto :goto_0

    .line 122
    .end local v0           #location:Lnet/flixster/android/model/Location;
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    #getter for: Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/LocationPage;->access$1(Lnet/flixster/android/LocationPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v5, :cond_0

    .line 123
    iget-object v2, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    invoke-virtual {v2}, Lnet/flixster/android/LocationPage;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 124
    iget-object v2, p0, Lnet/flixster/android/LocationPage$1;->this$0:Lnet/flixster/android/LocationPage;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lnet/flixster/android/LocationPage;->showDialog(I)V

    goto :goto_0
.end method
