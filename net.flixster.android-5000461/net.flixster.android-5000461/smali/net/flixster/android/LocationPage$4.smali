.class Lnet/flixster/android/LocationPage$4;
.super Ljava/lang/Object;
.source "LocationPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/LocationPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/LocationPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/LocationPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/LocationPage$4;->this$0:Lnet/flixster/android/LocationPage;

    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v4, 0x0

    .line 173
    iget-object v1, p0, Lnet/flixster/android/LocationPage$4;->this$0:Lnet/flixster/android/LocationPage;

    #getter for: Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;
    invoke-static {v1}, Lnet/flixster/android/LocationPage;->access$1(Lnet/flixster/android/LocationPage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Location;

    .line 174
    .local v0, location:Lnet/flixster/android/model/Location;
    iget-wide v1, v0, Lnet/flixster/android/model/Location;->latitude:D

    invoke-static {v1, v2}, Lnet/flixster/android/FlixsterApplication;->setUserLatitude(D)V

    .line 175
    iget-wide v1, v0, Lnet/flixster/android/model/Location;->longitude:D

    invoke-static {v1, v2}, Lnet/flixster/android/FlixsterApplication;->setUserLongitude(D)V

    .line 176
    iget-object v1, v0, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setUserZip(Ljava/lang/String;)V

    .line 177
    iget-object v1, v0, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setUserCity(Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lnet/flixster/android/LocationPage$4;->this$0:Lnet/flixster/android/LocationPage;

    #calls: Lnet/flixster/android/LocationPage;->getLocationItemDisplay(Lnet/flixster/android/model/Location;)Ljava/lang/String;
    invoke-static {v1, v0}, Lnet/flixster/android/LocationPage;->access$2(Lnet/flixster/android/LocationPage;Lnet/flixster/android/model/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setUserLocation(Ljava/lang/String;)V

    .line 179
    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setUseLocationServiceFlag(Z)V

    .line 180
    iget-object v1, p0, Lnet/flixster/android/LocationPage$4;->this$0:Lnet/flixster/android/LocationPage;

    const/4 v2, -0x1

    iget-object v3, p0, Lnet/flixster/android/LocationPage$4;->this$0:Lnet/flixster/android/LocationPage;

    invoke-virtual {v3}, Lnet/flixster/android/LocationPage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lnet/flixster/android/LocationPage;->setResult(ILandroid/content/Intent;)V

    .line 181
    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setLocationPolicy(I)V

    .line 182
    iget-object v1, p0, Lnet/flixster/android/LocationPage$4;->this$0:Lnet/flixster/android/LocationPage;

    invoke-virtual {v1}, Lnet/flixster/android/LocationPage;->finish()V

    .line 183
    return-void
.end method
