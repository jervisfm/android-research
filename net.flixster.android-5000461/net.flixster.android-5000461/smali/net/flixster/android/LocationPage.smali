.class public Lnet/flixster/android/LocationPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "LocationPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# static fields
.field private static final DIALOG_LOCATION:I = 0x2

.field private static final DIALOG_LOCATION_CHOICES:I = 0x3

.field private static final DIALOG_NETWORK_FAIL:I = 0x1


# instance fields
.field private geoPointPattern:Ljava/util/regex/Pattern;

.field private locationSelectionHandler:Landroid/os/Handler;

.field private locations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Location;",
            ">;"
        }
    .end annotation
.end field

.field private mGoButton:Landroid/widget/Button;

.field private mLocationLabel:Landroid/widget/TextView;

.field private mLocationText:Landroid/widget/EditText;

.field private timer:Ljava/util/Timer;

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 40
    const-string v0, "[(][-+]?[0-9]*\\.?[0-9]*[,] [-+]?[0-9]*\\.?[0-9]*[)]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/LocationPage;->geoPointPattern:Ljava/util/regex/Pattern;

    .line 101
    new-instance v0, Lnet/flixster/android/LocationPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/LocationPage$1;-><init>(Lnet/flixster/android/LocationPage;)V

    iput-object v0, p0, Lnet/flixster/android/LocationPage;->locationSelectionHandler:Landroid/os/Handler;

    .line 227
    new-instance v0, Lnet/flixster/android/LocationPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/LocationPage$2;-><init>(Lnet/flixster/android/LocationPage;)V

    iput-object v0, p0, Lnet/flixster/android/LocationPage;->updateHandler:Landroid/os/Handler;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/LocationPage;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 41
    iput-object p1, p0, Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/LocationPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/LocationPage;Lnet/flixster/android/model/Location;)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 268
    invoke-direct {p0, p1}, Lnet/flixster/android/LocationPage;->getLocationItemDisplay(Lnet/flixster/android/model/Location;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/LocationPage;)V
    .locals 0
    .parameter

    .prologue
    .line 238
    invoke-direct {p0}, Lnet/flixster/android/LocationPage;->updatePage()V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/LocationPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->locationSelectionHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/LocationPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 227
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getLocationItemDisplay(Lnet/flixster/android/model/Location;)Ljava/lang/String;
    .locals 3
    .parameter "location"

    .prologue
    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 270
    .local v0, locationBuilder:Ljava/lang/StringBuilder;
    iget-object v1, p1, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ""

    iget-object v2, p1, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 271
    iget-object v1, p1, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :cond_0
    iget-object v1, p1, Lnet/flixster/android/model/Location;->stateCode:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ""

    iget-object v2, p1, Lnet/flixster/android/model/Location;->stateCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 274
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 275
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    :cond_1
    iget-object v1, p1, Lnet/flixster/android/model/Location;->stateCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    :cond_2
    iget-object v1, p1, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ""

    iget-object v2, p1, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 280
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 281
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    :cond_3
    iget-object v1, p1, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    :cond_4
    iget-object v1, p1, Lnet/flixster/android/model/Location;->country:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ""

    iget-object v2, p1, Lnet/flixster/android/model/Location;->country:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "United States"

    iget-object v2, p1, Lnet/flixster/android/model/Location;->country:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 286
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_5

    .line 287
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    :cond_5
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lnet/flixster/android/model/Location;->country:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private locationSelection()V
    .locals 8

    .prologue
    .line 77
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->mLocationText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 78
    .local v7, query:Ljava/lang/String;
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->geoPointPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    .line 79
    .local v2, isGeoPointMatched:Z
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v3

    .line 80
    .local v3, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v5

    .line 83
    .local v5, longitude:D
    new-instance v0, Lnet/flixster/android/LocationPage$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lnet/flixster/android/LocationPage$3;-><init>(Lnet/flixster/android/LocationPage;ZDDLjava/lang/String;)V

    .line 98
    invoke-virtual {v0}, Lnet/flixster/android/LocationPage$3;->start()V

    .line 99
    return-void
.end method

.method private showCurrentLocation(DDLjava/lang/String;)V
    .locals 3
    .parameter "latitude"
    .parameter "longitude"
    .parameter "location"

    .prologue
    const-wide/16 v1, 0x0

    .line 260
    cmpl-double v0, p1, v1

    if-nez v0, :cond_0

    cmpl-double v0, p3, v1

    if-nez v0, :cond_0

    .line 261
    invoke-virtual {p0}, Lnet/flixster/android/LocationPage;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Please enable a My Location source in system settings"

    .line 262
    const/4 v2, 0x0

    .line 261
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 262
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 266
    :goto_0
    return-void

    .line 264
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->mLocationLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updatePage()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 242
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v1

    .line 244
    .local v1, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v3

    .line 245
    .local v3, longitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLocationDisplay()Ljava/lang/String;

    move-result-object v5

    .local v5, location:Ljava/lang/String;
    :cond_0
    :goto_0
    move-object v0, p0

    .line 256
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/LocationPage;->showCurrentLocation(DDLjava/lang/String;)V

    .line 257
    return-void

    .line 247
    .end local v1           #latitude:D
    .end local v3           #longitude:D
    .end local v5           #location:Ljava/lang/String;
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v1

    .line 248
    .restart local v1       #latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v3

    .line 249
    .restart local v3       #longitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v5

    .line 250
    .restart local v5       #location:Ljava/lang/String;
    cmpl-double v0, v1, v6

    if-nez v0, :cond_0

    cmpl-double v0, v3, v6

    if-nez v0, :cond_0

    .line 251
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v1

    .line 252
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v3

    .line 253
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLocationDisplay()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "view"

    .prologue
    .line 131
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 150
    :goto_0
    :pswitch_0
    return-void

    .line 147
    :pswitch_1
    invoke-direct {p0}, Lnet/flixster/android/LocationPage;->locationSelection()V

    goto :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x7f0700e0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v0, 0x7f030042

    invoke-virtual {p0, v0}, Lnet/flixster/android/LocationPage;->setContentView(I)V

    .line 47
    invoke-virtual {p0}, Lnet/flixster/android/LocationPage;->createActionBar()V

    .line 48
    const v0, 0x7f0c0042

    invoke-virtual {p0, v0}, Lnet/flixster/android/LocationPage;->setActionBarTitle(I)V

    .line 50
    const v0, 0x7f0700df

    invoke-virtual {p0, v0}, Lnet/flixster/android/LocationPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/LocationPage;->mLocationLabel:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f0700e0

    invoke-virtual {p0, v0}, Lnet/flixster/android/LocationPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lnet/flixster/android/LocationPage;->mLocationText:Landroid/widget/EditText;

    .line 53
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->mLocationLabel:Landroid/widget/TextView;

    const-string v1, "?"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->mLocationText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lnet/flixster/android/LocationPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0130

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->mLocationText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 57
    const v0, 0x7f0700e1

    invoke-virtual {p0, v0}, Lnet/flixster/android/LocationPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lnet/flixster/android/LocationPage;->mGoButton:Landroid/widget/Button;

    .line 58
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->mGoButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 11
    .parameter "dialogId"

    .prologue
    const v10, 0x7f0c0135

    const v9, 0x7f0c004a

    const/4 v8, 0x1

    .line 154
    packed-switch p1, :pswitch_data_0

    .line 206
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 207
    .local v1, defaultDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lnet/flixster/android/LocationPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 208
    invoke-virtual {v1, v8}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 209
    invoke-virtual {v1, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 210
    invoke-virtual {v1, v8}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    move-object v6, v1

    .line 211
    .end local v1           #defaultDialog:Landroid/app/ProgressDialog;
    :goto_0
    return-object v6

    .line 157
    :pswitch_0
    new-instance v6, Landroid/app/ProgressDialog;

    invoke-direct {v6, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 158
    .local v6, ratingsDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lnet/flixster/android/LocationPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 159
    invoke-virtual {v6, v8}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 160
    invoke-virtual {v6, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 161
    invoke-virtual {v6, v8}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 164
    .end local v6           #ratingsDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 165
    .local v4, locationChoiceBuilder:Landroid/app/AlertDialog$Builder;
    const-string v7, "Select a Location"

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 166
    iget-object v7, p0, Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v5, v7, [Ljava/lang/CharSequence;

    .line 167
    .local v5, locationItems:[Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    iget-object v7, p0, Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v2, v7, :cond_0

    .line 171
    new-instance v7, Lnet/flixster/android/LocationPage$4;

    invoke-direct {v7, p0}, Lnet/flixster/android/LocationPage$4;-><init>(Lnet/flixster/android/LocationPage;)V

    invoke-virtual {v4, v5, v7}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 185
    invoke-virtual {p0}, Lnet/flixster/android/LocationPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 186
    new-instance v8, Lnet/flixster/android/LocationPage$5;

    invoke-direct {v8, p0}, Lnet/flixster/android/LocationPage$5;-><init>(Lnet/flixster/android/LocationPage;)V

    .line 185
    invoke-virtual {v4, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 192
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    goto :goto_0

    .line 168
    :cond_0
    iget-object v7, p0, Lnet/flixster/android/LocationPage;->locations:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Location;

    .line 169
    .local v3, location:Lnet/flixster/android/model/Location;
    invoke-direct {p0, v3}, Lnet/flixster/android/LocationPage;->getLocationItemDisplay(Lnet/flixster/android/model/Location;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v2

    .line 167
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 194
    .end local v2           #i:I
    .end local v3           #location:Lnet/flixster/android/model/Location;
    .end local v4           #locationChoiceBuilder:Landroid/app/AlertDialog$Builder;
    .end local v5           #locationItems:[Ljava/lang/CharSequence;
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 195
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v7, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 196
    const-string v7, "Network Error"

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 197
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 198
    const-string v7, "Retry"

    new-instance v8, Lnet/flixster/android/LocationPage$6;

    invoke-direct {v8, p0}, Lnet/flixster/android/LocationPage$6;-><init>(Lnet/flixster/android/LocationPage;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 203
    invoke-virtual {p0}, Lnet/flixster/android/LocationPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 204
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    goto/16 :goto_0

    .line 154
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 296
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 217
    const-string v0, "FlxMain"

    const-string v1, "LocationPage.onDestroy"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onDestroy()V

    .line 220
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 222
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 224
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/LocationPage;->timer:Ljava/util/Timer;

    .line 225
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "textView"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 72
    invoke-direct {p0}, Lnet/flixster/android/LocationPage;->locationSelection()V

    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 63
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 64
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/LocationPage;->timer:Ljava/util/Timer;

    .line 67
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/showtimes/location"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lnet/flixster/android/LocationPage;->updateHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 69
    return-void
.end method
