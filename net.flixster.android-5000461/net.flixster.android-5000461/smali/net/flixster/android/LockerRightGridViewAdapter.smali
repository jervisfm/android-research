.class public Lnet/flixster/android/LockerRightGridViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "LockerRightGridViewAdapter.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final rights:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p2, rights:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 16
    iput-object p1, p0, Lnet/flixster/android/LockerRightGridViewAdapter;->context:Landroid/content/Context;

    .line 17
    iput-object p2, p0, Lnet/flixster/android/LockerRightGridViewAdapter;->rights:Ljava/util/List;

    .line 18
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lnet/flixster/android/LockerRightGridViewAdapter;->rights:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 36
    iget-object v0, p0, Lnet/flixster/android/LockerRightGridViewAdapter;->rights:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 40
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 21
    if-eqz p2, :cond_0

    instance-of v0, p2, Lnet/flixster/android/MovieCollectionItem;

    if-nez v0, :cond_1

    .line 22
    :cond_0
    new-instance p2, Lnet/flixster/android/MovieCollectionItem;

    .end local p2
    iget-object v0, p0, Lnet/flixster/android/LockerRightGridViewAdapter;->context:Landroid/content/Context;

    invoke-direct {p2, v0}, Lnet/flixster/android/MovieCollectionItem;-><init>(Landroid/content/Context;)V

    .restart local p2
    :goto_0
    move-object v0, p2

    .line 27
    check-cast v0, Lnet/flixster/android/MovieCollectionItem;

    iget-object v1, p0, Lnet/flixster/android/LockerRightGridViewAdapter;->rights:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieCollectionItem;->load(Lnet/flixster/android/model/LockerRight;)V

    .line 28
    return-object p2

    :cond_1
    move-object v0, p2

    .line 24
    check-cast v0, Lnet/flixster/android/MovieCollectionItem;

    invoke-virtual {v0}, Lnet/flixster/android/MovieCollectionItem;->reset()V

    goto :goto_0
.end method
