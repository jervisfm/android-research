.class Lnet/flixster/android/LviActivity$3;
.super Ljava/lang/Object;
.source "LviActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/LviActivity;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/LviActivity;


# direct methods
.method constructor <init>(Lnet/flixster/android/LviActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/LviActivity$3;->this$0:Lnet/flixster/android/LviActivity;

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .parameter
    .parameter "v"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v8, 0x1

    .line 187
    iget-object v4, p0, Lnet/flixster/android/LviActivity$3;->this$0:Lnet/flixster/android/LviActivity;

    iget-object v4, v4, Lnet/flixster/android/LviActivity;->mData:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/lvi/Lvi;

    .line 188
    .local v2, lviObject:Lnet/flixster/android/lvi/Lvi;
    invoke-virtual {v2}, Lnet/flixster/android/lvi/Lvi;->getItemViewType()I

    move-result v4

    sget v5, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_MOVIE:I

    if-ne v4, v5, :cond_3

    .line 189
    check-cast v2, Lnet/flixster/android/lvi/LviMovie;

    .end local v2           #lviObject:Lnet/flixster/android/lvi/Lvi;
    iget-object v3, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 190
    .local v3, movie:Lnet/flixster/android/model/Movie;
    if-eqz v3, :cond_2

    .line 191
    const-string v4, "featuredId"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 192
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v4

    const-string v5, "featuredId"

    invoke-virtual {v3, v5}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 193
    const-string v6, "Click"

    const-string v7, "MoviesTab"

    .line 192
    invoke-virtual {v4, v5, v6, v7}, Lnet/flixster/android/ads/AdManager;->trackEvent(ILjava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    iget-object v4, p0, Lnet/flixster/android/LviActivity$3;->this$0:Lnet/flixster/android/LviActivity;

    iget-object v5, p0, Lnet/flixster/android/LviActivity$3;->this$0:Lnet/flixster/android/LviActivity;

    iget-object v5, v5, Lnet/flixster/android/LviActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v5

    iput v5, v4, Lnet/flixster/android/LviActivity;->mPositionLast:I

    .line 197
    iget-object v4, p0, Lnet/flixster/android/LviActivity$3;->this$0:Lnet/flixster/android/LviActivity;

    iput-boolean v8, v4, Lnet/flixster/android/LviActivity;->mPositionRecover:Z

    .line 199
    new-instance v1, Landroid/content/Intent;

    const-string v4, "DETAILS"

    const/4 v5, 0x0

    iget-object v6, p0, Lnet/flixster/android/LviActivity$3;->this$0:Lnet/flixster/android/LviActivity;

    const-class v7, Lnet/flixster/android/MovieDetails;

    invoke-direct {v1, v4, v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 200
    .local v1, i:Landroid/content/Intent;
    const-string v4, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v5

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 201
    iget-object v4, p0, Lnet/flixster/android/LviActivity$3;->this$0:Lnet/flixster/android/LviActivity;

    invoke-virtual {v4, v1}, Lnet/flixster/android/LviActivity;->startActivity(Landroid/content/Intent;)V

    .line 213
    .end local v1           #i:Landroid/content/Intent;
    .end local v3           #movie:Lnet/flixster/android/model/Movie;
    :cond_1
    :goto_0
    return-void

    .line 204
    .restart local v3       #movie:Lnet/flixster/android/model/Movie;
    :cond_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v4

    if-ne v4, v8, :cond_1

    .line 205
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    iget-object v5, p0, Lnet/flixster/android/LviActivity$3;->this$0:Lnet/flixster/android/LviActivity;

    .line 206
    invoke-virtual {v5}, Lnet/flixster/android/LviActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0054

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 205
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 207
    .local v0, critic:Landroid/content/Intent;
    iget-object v4, p0, Lnet/flixster/android/LviActivity$3;->this$0:Lnet/flixster/android/LviActivity;

    invoke-virtual {v4, v0}, Lnet/flixster/android/LviActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 210
    .end local v0           #critic:Landroid/content/Intent;
    .end local v3           #movie:Lnet/flixster/android/model/Movie;
    .restart local v2       #lviObject:Lnet/flixster/android/lvi/Lvi;
    :cond_3
    invoke-virtual {v2}, Lnet/flixster/android/lvi/Lvi;->getItemViewType()I

    sget v4, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_FOOTER:I

    goto :goto_0
.end method
