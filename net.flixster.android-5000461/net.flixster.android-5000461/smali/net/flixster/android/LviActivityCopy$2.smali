.class Lnet/flixster/android/LviActivityCopy$2;
.super Landroid/os/Handler;
.source "LviActivityCopy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/LviActivityCopy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/LviActivityCopy;


# direct methods
.method constructor <init>(Lnet/flixster/android/LviActivityCopy;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    .line 161
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 164
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    invoke-virtual {v1}, Lnet/flixster/android/LviActivityCopy;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 165
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    iget-object v1, v1, Lnet/flixster/android/LviActivityCopy;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 166
    .local v0, dataHolderSize:I
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    #getter for: Lnet/flixster/android/LviActivityCopy;->className:Ljava/lang/String;
    invoke-static {v3}, Lnet/flixster/android/LviActivityCopy;->access$2(Lnet/flixster/android/LviActivityCopy;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".mUpdateHandler mDataHolder.size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    if-lez v0, :cond_1

    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    #getter for: Lnet/flixster/android/LviActivityCopy;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;
    invoke-static {v1}, Lnet/flixster/android/LviActivityCopy;->access$3(Lnet/flixster/android/LviActivityCopy;)Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->isPausing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 168
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    iget-object v1, v1, Lnet/flixster/android/LviActivityCopy;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 169
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    iget-object v1, v1, Lnet/flixster/android/LviActivityCopy;->mData:Ljava/util/ArrayList;

    iget-object v2, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    iget-object v2, v2, Lnet/flixster/android/LviActivityCopy;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 170
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    iget-object v1, v1, Lnet/flixster/android/LviActivityCopy;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 171
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    iget-object v1, v1, Lnet/flixster/android/LviActivityCopy;->mLviListPageAdapter:Lnet/flixster/android/lvi/LviAdapter;

    invoke-virtual {v1}, Lnet/flixster/android/lvi/LviAdapter;->notifyDataSetChanged()V

    .line 172
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    invoke-virtual {v1}, Lnet/flixster/android/LviActivityCopy;->refreshSticky()V

    .line 177
    .end local v0           #dataHolderSize:I
    :cond_0
    :goto_0
    return-void

    .line 173
    .restart local v0       #dataHolderSize:I
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    #getter for: Lnet/flixster/android/LviActivityCopy;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;
    invoke-static {v1}, Lnet/flixster/android/LviActivityCopy;->access$3(Lnet/flixster/android/LviActivityCopy;)Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->isPausing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lnet/flixster/android/LviActivityCopy$2;->this$0:Lnet/flixster/android/LviActivityCopy;

    #getter for: Lnet/flixster/android/LviActivityCopy;->className:Ljava/lang/String;
    invoke-static {v3}, Lnet/flixster/android/LviActivityCopy;->access$2(Lnet/flixster/android/LviActivityCopy;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".mUpdateHandler pausing"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
