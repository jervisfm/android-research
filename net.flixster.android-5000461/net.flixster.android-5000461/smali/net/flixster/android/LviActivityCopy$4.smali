.class Lnet/flixster/android/LviActivityCopy$4;
.super Ljava/lang/Object;
.source "LviActivityCopy.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/LviActivityCopy;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/LviActivityCopy;


# direct methods
.method constructor <init>(Lnet/flixster/android/LviActivityCopy;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/LviActivityCopy$4;->this$0:Lnet/flixster/android/LviActivityCopy;

    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 226
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$4;->this$0:Lnet/flixster/android/LviActivityCopy;

    iget-object v2, p0, Lnet/flixster/android/LviActivityCopy$4;->this$0:Lnet/flixster/android/LviActivityCopy;

    iget-object v2, v2, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    iput v2, v1, Lnet/flixster/android/LviActivityCopy;->mPositionLast:I

    .line 227
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$4;->this$0:Lnet/flixster/android/LviActivityCopy;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lnet/flixster/android/LviActivityCopy;->mPositionRecover:Z

    .line 229
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lnet/flixster/android/LviActivityCopy$4;->this$0:Lnet/flixster/android/LviActivityCopy;

    #getter for: Lnet/flixster/android/LviActivityCopy;->className:Ljava/lang/String;
    invoke-static {v3}, Lnet/flixster/android/LviActivityCopy;->access$2(Lnet/flixster/android/LviActivityCopy;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".mTrailerClick.onClick"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .line 231
    .local v0, movie:Lnet/flixster/android/model/Movie;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->hasTrailer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy$4;->this$0:Lnet/flixster/android/LviActivityCopy;

    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lnet/flixster/android/Starter;->launchTrailerForResult(Lnet/flixster/android/model/Movie;Landroid/app/Activity;I)V

    .line 234
    :cond_0
    return-void
.end method
