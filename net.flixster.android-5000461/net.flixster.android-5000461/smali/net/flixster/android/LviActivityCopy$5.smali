.class Lnet/flixster/android/LviActivityCopy$5;
.super Ljava/lang/Object;
.source "LviActivityCopy.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/LviActivityCopy;->getTheaterClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/LviActivityCopy;


# direct methods
.method constructor <init>(Lnet/flixster/android/LviActivityCopy;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/LviActivityCopy$5;->this$0:Lnet/flixster/android/LviActivityCopy;

    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 255
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;

    .line 256
    .local v1, theaterHolder:Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;
    if-eqz v1, :cond_0

    .line 257
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lnet/flixster/android/LviActivityCopy$5;->this$0:Lnet/flixster/android/LviActivityCopy;

    const-class v3, Lnet/flixster/android/TheaterInfoPage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 258
    .local v0, i:Landroid/content/Intent;
    const-string v2, "net.flixster.android.EXTRA_THEATER_ID"

    iget-object v3, v1, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->theater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v3}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 259
    iget-object v2, p0, Lnet/flixster/android/LviActivityCopy$5;->this$0:Lnet/flixster/android/LviActivityCopy;

    invoke-virtual {v2, v0}, Lnet/flixster/android/LviActivityCopy;->startActivity(Landroid/content/Intent;)V

    .line 261
    .end local v0           #i:Landroid/content/Intent;
    :cond_0
    return-void
.end method
