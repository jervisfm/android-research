.class Lnet/flixster/android/LviActivityCopy$6;
.super Ljava/lang/Object;
.source "LviActivityCopy.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/LviActivityCopy;->getShowtimesClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/LviActivityCopy;


# direct methods
.method constructor <init>(Lnet/flixster/android/LviActivityCopy;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/LviActivityCopy$6;->this$0:Lnet/flixster/android/LviActivityCopy;

    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 274
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lnet/flixster/android/LviActivityCopy$6;->this$0:Lnet/flixster/android/LviActivityCopy;

    #getter for: Lnet/flixster/android/LviActivityCopy;->className:Ljava/lang/String;
    invoke-static {v4}, Lnet/flixster/android/LviActivityCopy;->access$2(Lnet/flixster/android/LviActivityCopy;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ".getShowtimesClickListener.onClick"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/lvi/LviShowtimes;

    .line 276
    .local v1, lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    if-eqz v1, :cond_0

    .line 278
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lnet/flixster/android/LviActivityCopy$6;->this$0:Lnet/flixster/android/LviActivityCopy;

    const-class v3, Lnet/flixster/android/TicketInfoPage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 279
    .local v0, i:Landroid/content/Intent;
    const-string v2, "MOVIE_ID"

    iget-object v3, v1, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    iget-wide v3, v3, Lnet/flixster/android/model/Showtimes;->mMovieId:J

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 280
    const-string v2, "id"

    iget-object v3, v1, Lnet/flixster/android/lvi/LviShowtimes;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v3}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 281
    const-string v2, "SHOWTIMES_TITLE"

    iget-object v3, v1, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    iget-object v3, v3, Lnet/flixster/android/model/Showtimes;->mShowtimesTitle:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    iget-object v2, p0, Lnet/flixster/android/LviActivityCopy$6;->this$0:Lnet/flixster/android/LviActivityCopy;

    invoke-virtual {v2, v0}, Lnet/flixster/android/LviActivityCopy;->startActivity(Landroid/content/Intent;)V

    .line 285
    .end local v0           #i:Landroid/content/Intent;
    :cond_0
    return-void
.end method
