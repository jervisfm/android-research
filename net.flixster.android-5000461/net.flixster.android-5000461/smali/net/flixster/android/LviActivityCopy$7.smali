.class Lnet/flixster/android/LviActivityCopy$7;
.super Ljava/lang/Object;
.source "LviActivityCopy.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/LviActivityCopy;->getStarTheaterClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/LviActivityCopy;


# direct methods
.method constructor <init>(Lnet/flixster/android/LviActivityCopy;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/LviActivityCopy$7;->this$0:Lnet/flixster/android/LviActivityCopy;

    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    .line 299
    move-object v0, p1

    check-cast v0, Landroid/widget/CheckBox;

    .line 300
    .local v0, cb:Landroid/widget/CheckBox;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Theater;

    .line 301
    .local v1, t:Lnet/flixster/android/model/Theater;
    if-eqz v1, :cond_0

    .line 302
    invoke-virtual {v1}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 303
    .local v2, theaterIdString:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 304
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lnet/flixster/android/LviActivityCopy$7;->this$0:Lnet/flixster/android/LviActivityCopy;

    #getter for: Lnet/flixster/android/LviActivityCopy;->className:Ljava/lang/String;
    invoke-static {v5}, Lnet/flixster/android/LviActivityCopy;->access$2(Lnet/flixster/android/LviActivityCopy;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".StarTheaterListener selected"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->addFavoriteTheater(Ljava/lang/String;)V

    .line 311
    .end local v2           #theaterIdString:Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 307
    .restart local v2       #theaterIdString:Ljava/lang/String;
    :cond_1
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lnet/flixster/android/LviActivityCopy$7;->this$0:Lnet/flixster/android/LviActivityCopy;

    #getter for: Lnet/flixster/android/LviActivityCopy;->className:Ljava/lang/String;
    invoke-static {v5}, Lnet/flixster/android/LviActivityCopy;->access$2(Lnet/flixster/android/LviActivityCopy;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".StarTheaterListener unselected"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->removeFavoriteTheater(Ljava/lang/String;)V

    goto :goto_0
.end method
