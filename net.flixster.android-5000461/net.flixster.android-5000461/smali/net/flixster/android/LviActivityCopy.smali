.class public Lnet/flixster/android/LviActivityCopy;
.super Lnet/flixster/android/FlixsterActivityCopy;
.source "LviActivityCopy.java"


# static fields
.field protected static final HIDE_THROBBER:I = 0x0

.field public static final LISTSTATE_NAV:Ljava/lang/String; = "LISTSTATE_NAV"

.field protected static final SHOW_THROBBER:I = 0x1


# instance fields
.field protected lviAd:Lnet/flixster/android/lvi/LviAd;

.field protected final mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/lvi/Lvi;",
            ">;"
        }
    .end annotation
.end field

.field protected final mDataHolder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/lvi/Lvi;",
            ">;"
        }
    .end annotation
.end field

.field protected mListView:Landroid/widget/ListView;

.field protected mLviListPageAdapter:Lnet/flixster/android/lvi/LviAdapter;

.field private mMovieItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field protected mPositionLast:I

.field protected mPositionRecover:Z

.field private mShowtimesClickListener:Landroid/view/View$OnClickListener;

.field private mStarTheaterClickListener:Landroid/view/View$OnClickListener;

.field private mTheaterClickListener:Landroid/view/View$OnClickListener;

.field protected mTrailerClick:Landroid/view/View$OnClickListener;

.field protected final mUpdateHandler:Landroid/os/Handler;

.field private throbber:Landroid/view/View;

.field protected final throbberHandler:Landroid/os/Handler;

.field private throbberLayout:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivityCopy;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mData:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mDataHolder:Ljava/util/ArrayList;

    .line 146
    new-instance v0, Lnet/flixster/android/LviActivityCopy$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/LviActivityCopy$1;-><init>(Lnet/flixster/android/LviActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->throbberHandler:Landroid/os/Handler;

    .line 161
    new-instance v0, Lnet/flixster/android/LviActivityCopy$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/LviActivityCopy$2;-><init>(Lnet/flixster/android/LviActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mUpdateHandler:Landroid/os/Handler;

    .line 248
    iput-object v1, p0, Lnet/flixster/android/LviActivityCopy;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    .line 267
    iput-object v1, p0, Lnet/flixster/android/LviActivityCopy;->mShowtimesClickListener:Landroid/view/View$OnClickListener;

    .line 292
    iput-object v1, p0, Lnet/flixster/android/LviActivityCopy;->mStarTheaterClickListener:Landroid/view/View$OnClickListener;

    .line 33
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/LviActivityCopy;)V
    .locals 0
    .parameter

    .prologue
    .line 131
    invoke-direct {p0}, Lnet/flixster/android/LviActivityCopy;->showLoading()V

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/LviActivityCopy;)V
    .locals 0
    .parameter

    .prologue
    .line 137
    invoke-direct {p0}, Lnet/flixster/android/LviActivityCopy;->hideLoading()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/LviActivityCopy;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->className:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/LviActivityCopy;)Lcom/flixster/android/activity/decorator/TopLevelDecorator;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    return-object v0
.end method

.method private hideLoading()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 138
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->throbber:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->throbberLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 141
    return-void
.end method

.method private showLoading()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 132
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->throbberLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->throbber:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 135
    return-void
.end method


# virtual methods
.method protected destroyExistingLviAd()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->lviAd:Lnet/flixster/android/lvi/LviAd;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAd;->destroy()V

    .line 95
    :cond_0
    return-void
.end method

.method protected declared-synchronized getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mMovieItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    if-nez v0, :cond_0

    .line 185
    new-instance v0, Lnet/flixster/android/LviActivityCopy$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/LviActivityCopy$3;-><init>(Lnet/flixster/android/LviActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mMovieItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 216
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mMovieItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getShowtimesClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 270
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mShowtimesClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 271
    new-instance v0, Lnet/flixster/android/LviActivityCopy$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/LviActivityCopy$6;-><init>(Lnet/flixster/android/LviActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mShowtimesClickListener:Landroid/view/View$OnClickListener;

    .line 289
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mShowtimesClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getStarTheaterClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mStarTheaterClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 296
    new-instance v0, Lnet/flixster/android/LviActivityCopy$7;

    invoke-direct {v0, p0}, Lnet/flixster/android/LviActivityCopy$7;-><init>(Lnet/flixster/android/LviActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mStarTheaterClickListener:Landroid/view/View$OnClickListener;

    .line 315
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mStarTheaterClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getTheaterClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 251
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 252
    new-instance v0, Lnet/flixster/android/LviActivityCopy$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/LviActivityCopy$5;-><init>(Lnet/flixster/android/LviActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    .line 264
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mTheaterClickListener:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getTrailerOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 222
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mTrailerClick:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lnet/flixster/android/LviActivityCopy$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/LviActivityCopy$4;-><init>(Lnet/flixster/android/LviActivityCopy;)V

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 237
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mTrailerClick:Landroid/view/View$OnClickListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 242
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->shouldShowPostitial()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->postitialShown()V

    .line 244
    new-instance v0, Lcom/flixster/android/ads/DfpPostTrailerInterstitial;

    invoke-direct {v0}, Lcom/flixster/android/ads/DfpPostTrailerInterstitial;-><init>()V

    int-to-long v1, p1

    invoke-static {v1, v2}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/flixster/android/ads/DfpPostTrailerInterstitial;->loadAd(Landroid/app/Activity;Lnet/flixster/android/model/Movie;)V

    .line 246
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedState"

    .prologue
    .line 49
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivityCopy;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v0, 0x7f030049

    invoke-virtual {p0, v0}, Lnet/flixster/android/LviActivityCopy;->setContentView(I)V

    .line 52
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Lnet/flixster/android/LviActivityCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->throbber:Landroid/view/View;

    .line 53
    const v0, 0x7f0700fb

    invoke-virtual {p0, v0}, Lnet/flixster/android/LviActivityCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->throbberLayout:Landroid/view/View;

    .line 55
    const v0, 0x7f0700fa

    invoke-virtual {p0, v0}, Lnet/flixster/android/LviActivityCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/AdView;

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    .line 56
    const v0, 0x7f0700fc

    invoke-virtual {p0, v0}, Lnet/flixster/android/LviActivityCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/AdView;

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    .line 58
    const v0, 0x7f0700f4

    invoke-virtual {p0, v0}, Lnet/flixster/android/LviActivityCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    .line 59
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 61
    new-instance v0, Lnet/flixster/android/lvi/LviAdapter;

    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy;->mData:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lnet/flixster/android/lvi/LviAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mLviListPageAdapter:Lnet/flixster/android/lvi/LviAdapter;

    .line 62
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lnet/flixster/android/LviActivityCopy;->mLviListPageAdapter:Lnet/flixster/android/lvi/LviAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->clearFocus()V

    .line 86
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->removeAllViewsInLayout()V

    .line 87
    invoke-virtual {p0}, Lnet/flixster/android/LviActivityCopy;->destroyExistingLviAd()V

    .line 88
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivityCopy;->onDestroy()V

    .line 89
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 74
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivityCopy;->onPause()V

    .line 75
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 76
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 77
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mLviListPageAdapter:Lnet/flixster/android/lvi/LviAdapter;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter;->notifyDataSetInvalidated()V

    .line 79
    invoke-virtual {p0}, Lnet/flixster/android/LviActivityCopy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PositionLast"

    iget v2, p0, Lnet/flixster/android/LviActivityCopy;->mPositionLast:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 80
    invoke-virtual {p0}, Lnet/flixster/android/LviActivityCopy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PositionRecover"

    iget-boolean v2, p0, Lnet/flixster/android/LviActivityCopy;->mPositionRecover:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 81
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 124
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivityCopy;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 125
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lnet/flixster/android/LviActivityCopy;->className:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".onRestoreInstanceState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v0, "LISTSTATE_POSITION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/LviActivityCopy;->mPositionLast:I

    .line 127
    invoke-virtual {p0}, Lnet/flixster/android/LviActivityCopy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PositionLast"

    iget v2, p0, Lnet/flixster/android/LviActivityCopy;->mPositionLast:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/LviActivityCopy;->mPositionRecover:Z

    .line 129
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivityCopy;->onResume()V

    .line 68
    invoke-virtual {p0}, Lnet/flixster/android/LviActivityCopy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PositionLast"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/LviActivityCopy;->mPositionLast:I

    .line 69
    invoke-virtual {p0}, Lnet/flixster/android/LviActivityCopy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PositionRecover"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lnet/flixster/android/LviActivityCopy;->mPositionRecover:Z

    .line 70
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    const/4 v1, 0x1

    .line 114
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivityCopy;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 115
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 116
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lnet/flixster/android/LviActivityCopy;->mPositionLast:I

    .line 117
    iput-boolean v1, p0, Lnet/flixster/android/LviActivityCopy;->mPositionRecover:Z

    .line 118
    const-string v0, "LISTSTATE_POSITION"

    iget v1, p0, Lnet/flixster/android/LviActivityCopy;->mPositionLast:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 120
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .parameter "hasFocus"

    .prologue
    .line 99
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivityCopy;->onWindowFocusChanged(Z)V

    .line 102
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mLviListPageAdapter:Lnet/flixster/android/lvi/LviAdapter;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter;->notifyDataSetChanged()V

    .line 104
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lnet/flixster/android/LviActivityCopy;->mPositionRecover:Z

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    iget v1, p0, Lnet/flixster/android/LviActivityCopy;->mPositionLast:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 106
    iget-object v0, p0, Lnet/flixster/android/LviActivityCopy;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/flixster/android/LviActivityCopy;->mPositionRecover:Z

    .line 110
    :cond_0
    return-void
.end method
