.class Lnet/flixster/android/MovieCollectionItem$1;
.super Landroid/os/Handler;
.source "MovieCollectionItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieCollectionItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieCollectionItem;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieCollectionItem;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieCollectionItem$1;->this$0:Lnet/flixster/android/MovieCollectionItem;

    .line 163
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 165
    iget-object v0, p0, Lnet/flixster/android/MovieCollectionItem$1;->this$0:Lnet/flixster/android/MovieCollectionItem;

    #getter for: Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lnet/flixster/android/MovieCollectionItem;->access$0(Lnet/flixster/android/MovieCollectionItem;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 166
    iget-object v0, p0, Lnet/flixster/android/MovieCollectionItem$1;->this$0:Lnet/flixster/android/MovieCollectionItem;

    #getter for: Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lnet/flixster/android/MovieCollectionItem;->access$0(Lnet/flixster/android/MovieCollectionItem;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(Lnet/flixster/android/model/LockerRight;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/MovieCollectionItem$1;->this$0:Lnet/flixster/android/MovieCollectionItem;

    #getter for: Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lnet/flixster/android/MovieCollectionItem;->access$0(Lnet/flixster/android/MovieCollectionItem;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(Lnet/flixster/android/model/LockerRight;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/MovieCollectionItem$1;->this$0:Lnet/flixster/android/MovieCollectionItem;

    #getter for: Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/MovieCollectionItem;->access$1(Lnet/flixster/android/MovieCollectionItem;)Landroid/widget/TextView;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 168
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/MovieCollectionItem$1;->this$0:Lnet/flixster/android/MovieCollectionItem;

    #calls: Lnet/flixster/android/MovieCollectionItem;->setDownloadControls()V
    invoke-static {v0}, Lnet/flixster/android/MovieCollectionItem;->access$2(Lnet/flixster/android/MovieCollectionItem;)V

    .line 170
    :cond_1
    return-void
.end method
