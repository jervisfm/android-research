.class Lnet/flixster/android/MovieCollectionItem$3;
.super Landroid/os/Handler;
.source "MovieCollectionItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieCollectionItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieCollectionItem;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieCollectionItem;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieCollectionItem$3;->this$0:Lnet/flixster/android/MovieCollectionItem;

    .line 181
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    .line 183
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/model/Review;

    .line 184
    .local v0, userMovieReview:Lnet/flixster/android/model/Review;
    if-eqz v0, :cond_0

    iget-wide v1, v0, Lnet/flixster/android/model/Review;->stars:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem$3;->this$0:Lnet/flixster/android/MovieCollectionItem;

    #getter for: Lnet/flixster/android/MovieCollectionItem;->rating:Landroid/widget/ImageView;
    invoke-static {v1}, Lnet/flixster/android/MovieCollectionItem;->access$3(Lnet/flixster/android/MovieCollectionItem;)Landroid/widget/ImageView;

    move-result-object v1

    sget-object v2, Lnet/flixster/android/Flixster;->RATING_SMALL_R:[I

    iget-wide v3, v0, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v5, 0x4000

    mul-double/2addr v3, v5

    double-to-int v3, v3

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 186
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem$3;->this$0:Lnet/flixster/android/MovieCollectionItem;

    #getter for: Lnet/flixster/android/MovieCollectionItem;->rating:Landroid/widget/ImageView;
    invoke-static {v1}, Lnet/flixster/android/MovieCollectionItem;->access$3(Lnet/flixster/android/MovieCollectionItem;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 188
    :cond_0
    return-void
.end method
