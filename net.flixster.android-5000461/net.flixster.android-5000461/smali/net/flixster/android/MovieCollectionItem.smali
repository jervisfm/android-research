.class public Lnet/flixster/android/MovieCollectionItem;
.super Landroid/widget/RelativeLayout;
.source "MovieCollectionItem.java"


# static fields
.field public static final MOVIE_TYPE_COLLECTION:I = 0x0

.field public static final MOVIE_TYPE_NETFLIXDVD:I = 0x3

.field public static final MOVIE_TYPE_NETFLIXINSTANT:I = 0x4

.field public static final MOVIE_TYPE_RATED:I = 0x2

.field public static final MOVIE_TYPE_WTS:I = 0x1


# instance fields
.field private final context:Landroid/content/Context;

.field private downloadProgress:Landroid/widget/ProgressBar;

.field private final downloadSizeHandler:Landroid/os/Handler;

.field private downloadStatus:Landroid/widget/TextView;

.field private final errorHandler:Landroid/os/Handler;

.field private poster:Landroid/widget/ImageView;

.field final progressHandler:Landroid/os/Handler;

.field private rating:Landroid/widget/ImageView;

.field private final ratingSuccessHandler:Landroid/os/Handler;

.field private rentalExpiration:Landroid/widget/TextView;

.field private right:Lnet/flixster/android/model/LockerRight;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 163
    new-instance v0, Lnet/flixster/android/MovieCollectionItem$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieCollectionItem$1;-><init>(Lnet/flixster/android/MovieCollectionItem;)V

    iput-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->progressHandler:Landroid/os/Handler;

    .line 173
    new-instance v0, Lnet/flixster/android/MovieCollectionItem$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieCollectionItem$2;-><init>(Lnet/flixster/android/MovieCollectionItem;)V

    iput-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->downloadSizeHandler:Landroid/os/Handler;

    .line 181
    new-instance v0, Lnet/flixster/android/MovieCollectionItem$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieCollectionItem$3;-><init>(Lnet/flixster/android/MovieCollectionItem;)V

    iput-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->ratingSuccessHandler:Landroid/os/Handler;

    .line 191
    new-instance v0, Lnet/flixster/android/MovieCollectionItem$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieCollectionItem$4;-><init>(Lnet/flixster/android/MovieCollectionItem;)V

    iput-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->errorHandler:Landroid/os/Handler;

    .line 38
    iput-object p1, p0, Lnet/flixster/android/MovieCollectionItem;->context:Landroid/content/Context;

    .line 39
    invoke-direct {p0}, Lnet/flixster/android/MovieCollectionItem;->initialize()V

    .line 40
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/MovieCollectionItem;)Lnet/flixster/android/model/LockerRight;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/MovieCollectionItem;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/MovieCollectionItem;)V
    .locals 0
    .parameter

    .prologue
    .line 120
    invoke-direct {p0}, Lnet/flixster/android/MovieCollectionItem;->setDownloadControls()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/MovieCollectionItem;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->rating:Landroid/widget/ImageView;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->context:Landroid/content/Context;

    const v1, 0x7f030050

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 44
    const v0, 0x1080062

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieCollectionItem;->setBackgroundResource(I)V

    .line 45
    const v0, 0x7f070113

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieCollectionItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->poster:Landroid/widget/ImageView;

    .line 46
    const v0, 0x7f070114

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieCollectionItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    .line 47
    const v0, 0x7f070115

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieCollectionItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->title:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f070117

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieCollectionItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->rentalExpiration:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f070118

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieCollectionItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f070116

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieCollectionItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->rating:Landroid/widget/ImageView;

    .line 51
    return-void
.end method

.method private setDownloadControls()V
    .locals 11

    .prologue
    const/4 v8, 0x4

    const/16 v7, 0x8

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 121
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->isMovie()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    iget-boolean v4, v4, Lnet/flixster/android/model/LockerRight;->isDownloadSupported:Z

    if-nez v4, :cond_1

    :cond_0
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 122
    :cond_1
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    iget-wide v4, v4, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v4, v5}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v4

    if-nez v4, :cond_5

    .line 123
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetSize()Ljava/lang/String;

    move-result-object v1

    .line 124
    .local v1, downloadSize:Ljava/lang/String;
    if-eqz v1, :cond_2

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 125
    :cond_2
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->getDownloadUri()Ljava/lang/String;

    move-result-object v2

    .line 126
    .local v2, downloadUri:Ljava/lang/String;
    if-nez v2, :cond_3

    .line 127
    new-instance v4, Lcom/flixster/android/drm/DownloadRight;

    iget-object v5, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    iget-wide v5, v5, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-direct {v4, v5, v6}, Lcom/flixster/android/drm/DownloadRight;-><init>(J)V

    invoke-virtual {v4}, Lcom/flixster/android/drm/DownloadRight;->getDownloadUri()Ljava/lang/String;

    move-result-object v2

    .line 129
    :cond_3
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadSizeHandler:Landroid/os/Handler;

    invoke-static {v2, v4}, Lcom/flixster/android/net/DownloadHelper;->getRemoteFileSize(Ljava/lang/String;Landroid/os/Handler;)V

    .line 131
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 132
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    .end local v1           #downloadSize:Ljava/lang/String;
    .end local v2           #downloadUri:Ljava/lang/String;
    :goto_0
    return-void

    .line 134
    .restart local v1       #downloadSize:Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    iget-wide v4, v4, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v4, v5}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedMovieSize(J)J

    move-result-wide v4

    const-wide/16 v6, 0x64

    mul-long/2addr v4, v6

    iget-object v6, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    .line 135
    invoke-virtual {v6}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetSizeRaw()J

    move-result-wide v6

    .line 134
    div-long/2addr v4, v6

    long-to-int v3, v4

    .line 136
    .local v3, progress:I
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 137
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    const/16 v5, 0x64

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 138
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 140
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    iget-object v5, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    invoke-static {v5}, Lcom/flixster/android/net/DownloadHelper;->getMovieDownloadProgress(Lnet/flixster/android/model/LockerRight;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {v4, v9, v9, v9, v9}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 142
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 144
    .end local v1           #downloadSize:Ljava/lang/String;
    .end local v3           #progress:I
    :cond_5
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    invoke-static {v4}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(Lnet/flixster/android/model/LockerRight;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 145
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 147
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lnet/flixster/android/MovieCollectionItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0190

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const-string v8, ""

    aput-object v8, v7, v10

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    invoke-virtual {p0}, Lnet/flixster/android/MovieCollectionItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0200b1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 149
    .local v0, downloadIcon:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v0, v10, v10, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 150
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {v4, v0, v9, v9, v9}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 151
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 153
    .end local v0           #downloadIcon:Landroid/graphics/drawable/Drawable;
    :cond_6
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 154
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 157
    :cond_7
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 158
    iget-object v4, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public load(Lnet/flixster/android/model/LockerRight;)V
    .locals 8
    .parameter "right"

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 55
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->rating:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 57
    iput-object p1, p0, Lnet/flixster/android/MovieCollectionItem;->right:Lnet/flixster/android/model/LockerRight;

    .line 59
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lnet/flixster/android/model/LockerRight;->getAsset()Lnet/flixster/android/model/Movie;

    move-result-object v2

    if-nez v2, :cond_1

    .line 60
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->poster:Landroid/widget/ImageView;

    const v3, 0x7f0201dc

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 61
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->title:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 63
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p1}, Lnet/flixster/android/model/LockerRight;->getProfilePoster()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 66
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->poster:Landroid/widget/ImageView;

    const v3, 0x7f02014f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 70
    :cond_2
    :goto_1
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->title:Landroid/widget/TextView;

    invoke-virtual {p1}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-virtual {p1}, Lnet/flixster/android/model/LockerRight;->getViewingExpirationString()Ljava/lang/String;

    move-result-object v1

    .local v1, expirationText:Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 73
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->rentalExpiration:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->rentalExpiration:Landroid/widget/TextView;

    invoke-virtual {p0}, Lnet/flixster/android/MovieCollectionItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09002b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 75
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->rentalExpiration:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    :goto_2
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v2

    invoke-interface {v2}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceDownloadCompatible()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 84
    invoke-direct {p0}, Lnet/flixster/android/MovieCollectionItem;->setDownloadControls()V

    goto :goto_0

    .line 67
    .end local v1           #expirationText:Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->poster:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Lnet/flixster/android/model/LockerRight;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 68
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->poster:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 76
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .restart local v1       #expirationText:Ljava/lang/String;
    :cond_4
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lnet/flixster/android/model/LockerRight;->getRentalExpirationString(Z)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 77
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->rentalExpiration:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->rentalExpiration:Landroid/widget/TextView;

    invoke-virtual {p0}, Lnet/flixster/android/MovieCollectionItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->rentalExpiration:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 81
    :cond_5
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->rentalExpiration:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 86
    :cond_6
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 87
    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public load(Lnet/flixster/android/model/Movie;I)V
    .locals 5
    .parameter "movie"
    .parameter "movieType"

    .prologue
    const/16 v3, 0x8

    .line 94
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->downloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 97
    if-nez p1, :cond_0

    .line 98
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->poster:Landroid/widget/ImageView;

    const v2, 0x7f0201dc

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 99
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->title:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->rentalExpiration:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->rating:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getProfilePoster()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 105
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->poster:Landroid/widget/ImageView;

    const v2, 0x7f02014f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 109
    :cond_1
    :goto_1
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->title:Landroid/widget/TextView;

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->rentalExpiration:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->downloadStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    const/4 v1, 0x2

    if-ne p2, v1, :cond_3

    .line 113
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->ratingSuccessHandler:Landroid/os/Handler;

    iget-object v2, p0, Lnet/flixster/android/MovieCollectionItem;->errorHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lnet/flixster/android/data/ProfileDao;->getUserMovieReview(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :cond_2
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->poster:Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Lnet/flixster/android/model/Movie;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 107
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->poster:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 115
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    :cond_3
    iget-object v1, p0, Lnet/flixster/android/MovieCollectionItem;->rating:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lnet/flixster/android/MovieCollectionItem;->poster:Landroid/widget/ImageView;

    const v1, 0x7f020154

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 202
    return-void
.end method
