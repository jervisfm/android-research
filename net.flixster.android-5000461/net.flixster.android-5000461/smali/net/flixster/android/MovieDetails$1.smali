.class Lnet/flixster/android/MovieDetails$1;
.super Ljava/lang/Object;
.source "MovieDetails.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$1;->this$0:Lnet/flixster/android/MovieDetails;

    .line 805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 808
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$1;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$0(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    iget-wide v1, v1, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 809
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$1;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$0(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    iget-wide v1, v1, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(J)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 810
    .local v0, isLocalPlayback:Z
    :goto_0
    if-nez v0, :cond_1

    invoke-static {}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 811
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$1;->this$0:Lnet/flixster/android/MovieDetails;

    const v2, 0x3b9acb2c

    iget-object v3, p0, Lnet/flixster/android/MovieDetails$1;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;
    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->access$1(Lnet/flixster/android/MovieDetails;)Lcom/flixster/android/view/DialogBuilder$DialogListener;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lnet/flixster/android/MovieDetails;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 815
    :goto_1
    return-void

    .line 809
    .end local v0           #isLocalPlayback:Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 813
    .restart local v0       #isLocalPlayback:Z
    :cond_1
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/MovieDetails$1;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$0(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/flixster/android/drm/PlaybackManager;->playMovie(Lnet/flixster/android/model/LockerRight;)V

    goto :goto_1
.end method
