.class Lnet/flixster/android/MovieDetails$10;
.super Ljava/lang/Object;
.source "MovieDetails.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$10;->this$0:Lnet/flixster/android/MovieDetails;

    .line 960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 976
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acaca

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 977
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 971
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acaca

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 972
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 5
    .parameter "which"

    .prologue
    .line 963
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acaca

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 964
    iget-object v0, p0, Lnet/flixster/android/MovieDetails$10;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lnet/flixster/android/MovieDetails;->access$0(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/net/DownloadHelper;->deleteDownloadedMovie(Lnet/flixster/android/model/LockerRight;)Z

    .line 965
    iget-object v0, p0, Lnet/flixster/android/MovieDetails$10;->this$0:Lnet/flixster/android/MovieDetails;

    #calls: Lnet/flixster/android/MovieDetails;->delayedStreamingUiUpdate()V
    invoke-static {v0}, Lnet/flixster/android/MovieDetails;->access$6(Lnet/flixster/android/MovieDetails;)V

    .line 966
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/download"

    const-string v2, "Download"

    const-string v3, "Download"

    const-string v4, "Delete"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    return-void
.end method
