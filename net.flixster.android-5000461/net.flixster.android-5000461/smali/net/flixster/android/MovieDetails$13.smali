.class Lnet/flixster/android/MovieDetails$13;
.super Landroid/os/Handler;
.source "MovieDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    .line 1344
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 1347
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$11(Lnet/flixster/android/MovieDetails;)Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->isPausing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-virtual {v1}, Lnet/flixster/android/MovieDetails;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1348
    :cond_0
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.mUpdateHandler pausing"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1372
    :cond_1
    :goto_0
    return-void

    .line 1352
    :cond_2
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.refreshHandler run..."

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovieDetails:Lnet/flixster/android/MovieDetails;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$12(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/MovieDetails;

    move-result-object v1

    #calls: Lnet/flixster/android/MovieDetails;->populatePage()V
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$13(Lnet/flixster/android/MovieDetails;)V

    .line 1359
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MovieDetails.refreshHandler check deep trailer link playTrailer:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mPlayTrailer:Ljava/lang/Boolean;
    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->access$14(Lnet/flixster/android/MovieDetails;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1360
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mPlayTrailer:Ljava/lang/Boolean;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$14(Lnet/flixster/android/MovieDetails;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1361
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MovieDetails.refreshHandler check deep trailer link mMovie:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1363
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v1

    const-string v2, "high"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1364
    .local v0, movieTrailerUrl:Ljava/lang/String;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MovieDetails.refreshHandler check deep trailer link movieTrailerUrl:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1365
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1364
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1366
    if-eqz v0, :cond_1

    const-string v1, "http:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1367
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    #setter for: Lnet/flixster/android/MovieDetails;->mPlayTrailer:Ljava/lang/Boolean;
    invoke-static {v1, v2}, Lnet/flixster/android/MovieDetails;->access$16(Lnet/flixster/android/MovieDetails;Ljava/lang/Boolean;)V

    .line 1368
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/MovieDetails$13;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-static {v1, v2}, Lnet/flixster/android/Starter;->launchTrailer(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V

    goto/16 :goto_0
.end method
