.class Lnet/flixster/android/MovieDetails$14;
.super Landroid/os/Handler;
.source "MovieDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$14;->this$0:Lnet/flixster/android/MovieDetails;

    .line 1375
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 1378
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/widget/ImageView;

    .line 1379
    .local v1, photoView:Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 1380
    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Photo;

    .line 1381
    .local v0, photo:Lnet/flixster/android/model/Photo;
    if-eqz v0, :cond_0

    .line 1382
    iget-object v2, v0, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 1383
    iget-object v2, v0, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1384
    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 1388
    .end local v0           #photo:Lnet/flixster/android/model/Photo;
    :cond_0
    return-void
.end method
