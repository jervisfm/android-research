.class Lnet/flixster/android/MovieDetails$16;
.super Ljava/lang/Object;
.source "MovieDetails.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    .line 1607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "view"

    .prologue
    .line 1609
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->directorsLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$19(Lnet/flixster/android/MovieDetails;)Landroid/widget/LinearLayout;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 1610
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x3

    if-le v4, v5, :cond_0

    .line 1613
    const/4 v1, 0x0

    .line 1614
    .local v1, directorView:Landroid/view/View;
    const/4 v3, 0x3

    .local v3, i:I
    :goto_0
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_2

    .line 1627
    .end local v1           #directorView:Landroid/view/View;
    .end local v3           #i:I
    :cond_0
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->moreDirectorsLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$21(Lnet/flixster/android/MovieDetails;)Landroid/widget/RelativeLayout;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1628
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->moreDirectorsLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$21(Lnet/flixster/android/MovieDetails;)Landroid/widget/RelativeLayout;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1630
    :cond_1
    return-void

    .line 1615
    .restart local v1       #directorView:Landroid/view/View;
    .restart local v3       #i:I
    :cond_2
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 1616
    .local v0, director:Lnet/flixster/android/model/Actor;
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #calls: Lnet/flixster/android/MovieDetails;->getActorView(Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;
    invoke-static {v4, v0, v1}, Lnet/flixster/android/MovieDetails;->access$20(Lnet/flixster/android/MovieDetails;Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 1618
    new-instance v2, Landroid/widget/ImageView;

    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-direct {v2, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1619
    .local v2, dividerView:Landroid/widget/ImageView;
    const v4, 0x7f09001a

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1620
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 v5, -0x1

    iget-object v6, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-virtual {v6}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 1621
    const v7, 0x7f0a0025

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    invoke-direct {v4, v5, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1620
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1623
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->directorsLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$19(Lnet/flixster/android/MovieDetails;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1624
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$16;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->directorsLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$19(Lnet/flixster/android/MovieDetails;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1614
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
