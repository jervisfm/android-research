.class Lnet/flixster/android/MovieDetails$18;
.super Ljava/lang/Object;
.source "MovieDetails.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$18;->this$0:Lnet/flixster/android/MovieDetails;

    .line 1661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "view"

    .prologue
    .line 1663
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 1664
    .local v0, actor:Lnet/flixster/android/model/Actor;
    if-eqz v0, :cond_0

    .line 1665
    new-instance v1, Landroid/content/Intent;

    const-string v2, "TOP_ACTOR"

    const/4 v3, 0x0

    iget-object v4, p0, Lnet/flixster/android/MovieDetails$18;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-virtual {v4}, Lnet/flixster/android/MovieDetails;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lnet/flixster/android/ActorPage;

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 1666
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "ACTOR_ID"

    iget-wide v3, v0, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1667
    const-string v2, "ACTOR_NAME"

    iget-object v3, v0, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1668
    iget-object v2, p0, Lnet/flixster/android/MovieDetails$18;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-virtual {v2, v1}, Lnet/flixster/android/MovieDetails;->startActivity(Landroid/content/Intent;)V

    .line 1670
    .end local v1           #intent:Landroid/content/Intent;
    :cond_0
    return-void
.end method
