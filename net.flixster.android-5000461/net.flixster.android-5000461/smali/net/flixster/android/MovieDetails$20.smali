.class Lnet/flixster/android/MovieDetails$20;
.super Landroid/os/Handler;
.source "MovieDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    .line 1687
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .parameter "message"

    .prologue
    const v10, 0x7f0c00d1

    const/4 v9, 0x1

    const/4 v8, 0x0

    const v7, 0x7f0c00d3

    .line 1690
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1691
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    const v5, 0x7f070159

    invoke-virtual {v4, v5}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1692
    .local v2, netflixText:Landroid/widget/TextView;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1693
    .local v1, menuActions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1694
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "MovieDetails.mNetflixTitleStateHandler mNetflixTitleState:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v6}, Lnet/flixster/android/MovieDetails;->access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1695
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(I)V

    .line 1696
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v4

    iget v4, v4, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_4

    .line 1697
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1698
    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1699
    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1700
    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1708
    :cond_0
    :goto_0
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v4

    iget v4, v4, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_5

    .line 1709
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1710
    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1711
    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1712
    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1724
    :cond_1
    :goto_1
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v4

    iget v4, v4, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v4, v4, 0x9

    if-eqz v4, :cond_7

    .line 1725
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1732
    :cond_2
    :goto_2
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1734
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1735
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovieDetails:Lnet/flixster/android/MovieDetails;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$12(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/MovieDetails;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1736
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 1738
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1739
    .local v3, totalActions:I
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "menuActions:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1740
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    new-array v5, v3, [I

    #setter for: Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I
    invoke-static {v4, v5}, Lnet/flixster/android/MovieDetails;->access$25(Lnet/flixster/android/MovieDetails;[I)V

    .line 1741
    const/4 v0, 0x0

    .local v0, i:I
    :goto_3
    if-lt v0, v3, :cond_9

    .line 1745
    .end local v0           #i:I
    .end local v1           #menuActions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v2           #netflixText:Landroid/widget/TextView;
    .end local v3           #totalActions:I
    :cond_3
    return-void

    .line 1703
    .restart local v1       #menuActions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v2       #netflixText:Landroid/widget/TextView;
    :cond_4
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v4

    iget v4, v4, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_0

    .line 1704
    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1705
    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1716
    :cond_5
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v4

    iget v4, v4, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_6

    .line 1717
    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1718
    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1719
    :cond_6
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v4

    iget v4, v4, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_1

    .line 1720
    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1726
    :cond_7
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v4

    iget v4, v4, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_8

    .line 1727
    const v4, 0x7f0c00d2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 1728
    :cond_8
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v4

    iget v4, v4, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    and-int/lit8 v4, v4, 0x12

    if-eqz v4, :cond_2

    .line 1729
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 1742
    .restart local v0       #i:I
    .restart local v3       #totalActions:I
    :cond_9
    iget-object v4, p0, Lnet/flixster/android/MovieDetails$20;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$26(Lnet/flixster/android/MovieDetails;)[I

    move-result-object v5

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v5, v0

    .line 1741
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3
.end method
