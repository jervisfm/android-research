.class Lnet/flixster/android/MovieDetails$22;
.super Ljava/util/TimerTask;
.source "MovieDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/MovieDetails;->ScheduleNetflixTitleState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$22;->this$0:Lnet/flixster/android/MovieDetails;

    .line 1145
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1148
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$22;->this$0:Lnet/flixster/android/MovieDetails;

    iget-object v2, p0, Lnet/flixster/android/MovieDetails$22;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v2

    const-string v3, "netflix"

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/data/NetflixDao;->fetchTitleState(Ljava/lang/String;)Lnet/flixster/android/model/NetflixTitleState;

    move-result-object v2

    #setter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-static {v1, v2}, Lnet/flixster/android/MovieDetails;->access$28(Lnet/flixster/android/MovieDetails;Lnet/flixster/android/model/NetflixTitleState;)V

    .line 1149
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$22;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixTitleStateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$29(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_4

    .line 1163
    :goto_0
    return-void

    .line 1151
    :catch_0
    move-exception v0

    .line 1152
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.ScheduleNetflixTitleState OAuthMessageSignerException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1153
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v0

    .line 1154
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.ScheduleNetflixTitleState OAuthExpectationFailedException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1155
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_2
    move-exception v0

    .line 1156
    .local v0, e:Ljava/io/IOException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.ScheduleNetflixTitleState IOException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1157
    .end local v0           #e:Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 1158
    .local v0, e:Lorg/json/JSONException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.ScheduleNetflixTitleState JSONException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1159
    .end local v0           #e:Lorg/json/JSONException;
    :catch_4
    move-exception v0

    .line 1161
    .local v0, e:Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_0
.end method
