.class Lnet/flixster/android/MovieDetails$23;
.super Ljava/util/TimerTask;
.source "MovieDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/MovieDetails;->ScheduleAddToQueue(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;

.field private final synthetic val$nPosition:I

.field private final synthetic val$queueType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$23;->this$0:Lnet/flixster/android/MovieDetails;

    iput p2, p0, Lnet/flixster/android/MovieDetails$23;->val$nPosition:I

    iput-object p3, p0, Lnet/flixster/android/MovieDetails$23;->val$queueType:Ljava/lang/String;

    .line 1171
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1174
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$23;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v1

    const-string v2, "netflix"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lnet/flixster/android/MovieDetails$23;->val$nPosition:I

    iget-object v3, p0, Lnet/flixster/android/MovieDetails$23;->val$queueType:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lnet/flixster/android/data/NetflixDao;->postQueueItem(Ljava/lang/String;ILjava/lang/String;)V

    .line 1176
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$23;->this$0:Lnet/flixster/android/MovieDetails;

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleNetflixTitleState()V
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$30(Lnet/flixster/android/MovieDetails;)V

    .line 1177
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/netflix/addtoqueue"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/MovieDetails$23;->val$queueType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "position:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lnet/flixster/android/MovieDetails$23;->val$nPosition:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lnet/flixster/android/model/NetflixException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_7

    .line 1200
    :goto_0
    return-void

    .line 1179
    :catch_0
    move-exception v0

    .line 1180
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.ScheduleAddToQueue OAuthMessageSignerException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1181
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v0

    .line 1182
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.ScheduleAddToQueue OAuthExpectationFailedException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1183
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_2
    move-exception v0

    .line 1184
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.ScheduleAddToQueue ClientProtocolException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1185
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_3
    move-exception v0

    .line 1186
    .local v0, e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.ScheduleAddToQueue OAuthNotAuthorizedException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1187
    .end local v0           #e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_4
    move-exception v0

    .line 1188
    .local v0, e:Lnet/flixster/android/model/NetflixException;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MovieDetails.ScheduleAddToQueue NetflixException message:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lnet/flixster/android/model/NetflixException;->mMessage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1189
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$23;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->netflixHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$31(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1190
    .end local v0           #e:Lnet/flixster/android/model/NetflixException;
    :catch_5
    move-exception v0

    .line 1191
    .local v0, e:Ljava/io/IOException;
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.ScheduleAddToQueue IOException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1192
    .end local v0           #e:Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 1194
    .local v0, e:Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_0

    .line 1195
    .end local v0           #e:Loauth/signpost/exception/OAuthCommunicationException;
    :catch_7
    move-exception v0

    .line 1197
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
