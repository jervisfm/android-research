.class Lnet/flixster/android/MovieDetails$24;
.super Ljava/util/TimerTask;
.source "MovieDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/MovieDetails;->ScheduleMoveToBottom(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;

.field private final synthetic val$queueType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$24;->this$0:Lnet/flixster/android/MovieDetails;

    iput-object p2, p0, Lnet/flixster/android/MovieDetails$24;->val$queueType:Ljava/lang/String;

    .line 1208
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1211
    :try_start_0
    iget-object v3, p0, Lnet/flixster/android/MovieDetails$24;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v3

    const-string v4, "netflix"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1212
    .local v1, movieUrl:Ljava/lang/String;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieDetails.ScheduleMoveToBottom movieUrl:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " queueType:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1213
    iget-object v5, p0, Lnet/flixster/android/MovieDetails$24;->val$queueType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1212
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214
    const/16 v3, 0x25

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1215
    .local v2, movieUrlId:Ljava/lang/String;
    iget-object v3, p0, Lnet/flixster/android/MovieDetails$24;->val$queueType:Ljava/lang/String;

    invoke-static {v2, v3}, Lnet/flixster/android/data/NetflixDao;->deleteQueueItem(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    iget-object v3, p0, Lnet/flixster/android/MovieDetails$24;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;

    move-result-object v3

    const-string v4, "netflix"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lnet/flixster/android/MovieDetails$24;->val$queueType:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lnet/flixster/android/data/NetflixDao;->postQueueItem(Ljava/lang/String;ILjava/lang/String;)V

    .line 1217
    iget-object v3, p0, Lnet/flixster/android/MovieDetails$24;->this$0:Lnet/flixster/android/MovieDetails;

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleNetflixTitleState()V
    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->access$30(Lnet/flixster/android/MovieDetails;)V

    .line 1218
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/netflix/movetobottom"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/MovieDetails$24;->val$queueType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "position:"

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lnet/flixster/android/model/NetflixException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_7

    .line 1241
    .end local v1           #movieUrl:Ljava/lang/String;
    .end local v2           #movieUrlId:Ljava/lang/String;
    :goto_0
    return-void

    .line 1220
    :catch_0
    move-exception v0

    .line 1221
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetails.ScheduleMoveToBottom OAuthMessageSignerException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1222
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v0

    .line 1223
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetails.ScheduleMoveToBottom OAuthExpectationFailedException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1224
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_2
    move-exception v0

    .line 1225
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetails.ScheduleMoveToBottom ClientProtocolException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1226
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_3
    move-exception v0

    .line 1227
    .local v0, e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetails.ScheduleMoveToBottom OAuthNotAuthorizedException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1228
    .end local v0           #e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_4
    move-exception v0

    .line 1229
    .local v0, e:Lnet/flixster/android/model/NetflixException;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieDetails.ScheduleAddToQueue NetflixException message:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, Lnet/flixster/android/model/NetflixException;->mMessage:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1230
    iget-object v3, p0, Lnet/flixster/android/MovieDetails$24;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->netflixHandler:Landroid/os/Handler;
    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->access$31(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1231
    .end local v0           #e:Lnet/flixster/android/model/NetflixException;
    :catch_5
    move-exception v0

    .line 1232
    .local v0, e:Ljava/io/IOException;
    const-string v3, "FlxMain"

    const-string v4, "MovieDetails.ScheduleMoveToBottom IOException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1233
    .end local v0           #e:Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 1235
    .local v0, e:Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_0

    .line 1236
    .end local v0           #e:Loauth/signpost/exception/OAuthCommunicationException;
    :catch_7
    move-exception v0

    .line 1238
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
