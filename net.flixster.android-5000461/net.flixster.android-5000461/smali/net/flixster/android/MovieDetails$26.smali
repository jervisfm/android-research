.class Lnet/flixster/android/MovieDetails$26;
.super Ljava/util/TimerTask;
.source "MovieDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/MovieDetails;->ScheduleLoadMovieTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;

.field private final synthetic val$currResumeCtr:I


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    iput p2, p0, Lnet/flixster/android/MovieDetails$26;->val$currResumeCtr:I

    .line 1287
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1290
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieDetails.ScheduleLoadMovieTask.run mMovieId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovieId:J
    invoke-static {v4}, Lnet/flixster/android/MovieDetails;->access$32(Lnet/flixster/android/MovieDetails;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1292
    :try_start_0
    iget-object v3, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    iget-object v2, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->isSeason:Z
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$33(Lnet/flixster/android/MovieDetails;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovieId:J
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$32(Lnet/flixster/android/MovieDetails;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lnet/flixster/android/data/MovieDao;->getSeasonDetailLegacy(J)Lnet/flixster/android/model/Season;

    move-result-object v2

    :goto_0
    #setter for: Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v3, v2}, Lnet/flixster/android/MovieDetails;->access$34(Lnet/flixster/android/MovieDetails;Lnet/flixster/android/model/Movie;)V

    .line 1294
    iget-object v2, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    iget v3, p0, Lnet/flixster/android/MovieDetails$26;->val$currResumeCtr:I

    invoke-virtual {v2, v3}, Lnet/flixster/android/MovieDetails;->shouldSkipBackgroundTask(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1321
    :goto_1
    return-void

    .line 1292
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovieId:J
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$32(Lnet/flixster/android/MovieDetails;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lnet/flixster/android/data/MovieDao;->getMovieDetail(J)Lnet/flixster/android/model/Movie;

    move-result-object v2

    goto :goto_0

    .line 1298
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1299
    iget-object v2, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleNetflixTitleState()V
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$30(Lnet/flixster/android/MovieDetails;)V

    .line 1302
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    const/4 v3, 0x0

    iput v3, v2, Lnet/flixster/android/MovieDetails;->mRetryCount:I

    .line 1303
    iget-object v2, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovieDetails:Lnet/flixster/android/MovieDetails;
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$12(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/MovieDetails;

    move-result-object v2

    #getter for: Lnet/flixster/android/MovieDetails;->mFromRating:Z
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$35(Lnet/flixster/android/MovieDetails;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1304
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1305
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterId()Ljava/lang/String;
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1307
    .local v1, flixsterId:Ljava/lang/String;
    :try_start_1
    iget-object v2, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    iget-object v3, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovieId:J
    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->access$32(Lnet/flixster/android/MovieDetails;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lnet/flixster/android/data/ProfileDao;->getUserMovieReview(Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/Review;

    move-result-object v3

    #setter for: Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;
    invoke-static {v2, v3}, Lnet/flixster/android/MovieDetails;->access$36(Lnet/flixster/android/MovieDetails;Lnet/flixster/android/model/Review;)V
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1313
    .end local v1           #flixsterId:Ljava/lang/String;
    :cond_3
    :goto_2
    :try_start_2
    iget-object v2, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovieDetails:Lnet/flixster/android/MovieDetails;
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$12(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/MovieDetails;

    move-result-object v2

    #getter for: Lnet/flixster/android/MovieDetails;->refreshHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$37(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catch Lnet/flixster/android/data/DaoException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1317
    :catch_0
    move-exception v0

    .line 1318
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    const-string v3, "MovieDetails.ScheduleLoadMovieTask.run DaoException"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1319
    iget-object v2, p0, Lnet/flixster/android/MovieDetails$26;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-virtual {v2, v0}, Lnet/flixster/android/MovieDetails;->retryLogic(Lnet/flixster/android/data/DaoException;)V

    goto :goto_1

    .line 1308
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    .restart local v1       #flixsterId:Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 1309
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    :try_start_3
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "The user has not rated this movie: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2
.end method
