.class Lnet/flixster/android/MovieDetails$27;
.super Ljava/lang/Object;
.source "MovieDetails.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/MovieDetails;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    .line 1806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1810
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MovieDetails.onCreateDialog mNetflixMenuToAction[which]:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1811
    iget-object v3, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I
    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->access$26(Lnet/flixster/android/MovieDetails;)[I

    move-result-object v3

    aget v3, v3, p2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1810
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$26(Lnet/flixster/android/MovieDetails;)[I

    move-result-object v1

    aget v1, v1, p2

    packed-switch v1, :pswitch_data_0

    .line 1866
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.onCreateDialog -default-"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1870
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-virtual {v1, v5}, Lnet/flixster/android/MovieDetails;->removeDialog(I)V

    .line 1873
    return-void

    .line 1816
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->mMovieDetails:Lnet/flixster/android/MovieDetails;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$12(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/MovieDetails;

    move-result-object v1

    const-class v2, Lnet/flixster/android/NetflixQueuePage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1817
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1818
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-virtual {v1, v0}, Lnet/flixster/android/MovieDetails;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1822
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_2
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    const-string v2, "/queues/disc/available"

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleAddToQueue(ILjava/lang/String;)V
    invoke-static {v1, v4, v2}, Lnet/flixster/android/MovieDetails;->access$38(Lnet/flixster/android/MovieDetails;ILjava/lang/String;)V

    .line 1823
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v4

    goto :goto_0

    .line 1827
    :pswitch_3
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    const-string v2, "/queues/disc/available"

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleAddToQueue(ILjava/lang/String;)V
    invoke-static {v1, v5, v2}, Lnet/flixster/android/MovieDetails;->access$38(Lnet/flixster/android/MovieDetails;ILjava/lang/String;)V

    .line 1828
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v4

    goto :goto_0

    .line 1833
    :pswitch_4
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    const-string v2, "/queues/disc/available"

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleMoveToBottom(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lnet/flixster/android/MovieDetails;->access$39(Lnet/flixster/android/MovieDetails;Ljava/lang/String;)V

    .line 1834
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v4

    goto :goto_0

    .line 1838
    :pswitch_5
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    const-string v2, "/queues/instant/available"

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleAddToQueue(ILjava/lang/String;)V
    invoke-static {v1, v4, v2}, Lnet/flixster/android/MovieDetails;->access$38(Lnet/flixster/android/MovieDetails;ILjava/lang/String;)V

    .line 1839
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v6

    goto :goto_0

    .line 1842
    :pswitch_6
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    const-string v2, "/queues/instant/available"

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleAddToQueue(ILjava/lang/String;)V
    invoke-static {v1, v5, v2}, Lnet/flixster/android/MovieDetails;->access$38(Lnet/flixster/android/MovieDetails;ILjava/lang/String;)V

    .line 1843
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v6

    goto :goto_0

    .line 1848
    :pswitch_7
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    const-string v2, "/queues/instant/available"

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleMoveToBottom(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lnet/flixster/android/MovieDetails;->access$39(Lnet/flixster/android/MovieDetails;Ljava/lang/String;)V

    .line 1849
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v6

    goto :goto_0

    .line 1852
    :pswitch_8
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    const-string v2, "/queues/disc/saved"

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleAddToQueue(ILjava/lang/String;)V
    invoke-static {v1, v5, v2}, Lnet/flixster/android/MovieDetails;->access$38(Lnet/flixster/android/MovieDetails;ILjava/lang/String;)V

    .line 1853
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v7

    goto :goto_0

    .line 1857
    :pswitch_9
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    const-string v2, "/queues/disc/available"

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleRemoveFromQueue(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lnet/flixster/android/MovieDetails;->access$40(Lnet/flixster/android/MovieDetails;Ljava/lang/String;)V

    .line 1858
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v4

    .line 1859
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v7

    goto/16 :goto_0

    .line 1862
    :pswitch_a
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$27;->this$0:Lnet/flixster/android/MovieDetails;

    const-string v2, "/queues/instant/available"

    #calls: Lnet/flixster/android/MovieDetails;->ScheduleRemoveFromQueue(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lnet/flixster/android/MovieDetails;->access$40(Lnet/flixster/android/MovieDetails;Ljava/lang/String;)V

    .line 1863
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v6

    goto/16 :goto_0

    .line 1812
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_9
        :pswitch_2
        :pswitch_3
        :pswitch_8
        :pswitch_a
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method
