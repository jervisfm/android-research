.class Lnet/flixster/android/MovieDetails$4;
.super Landroid/os/Handler;
.source "MovieDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$4;->this$0:Lnet/flixster/android/MovieDetails;

    .line 854
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 856
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$4;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-virtual {v1}, Lnet/flixster/android/MovieDetails;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 874
    :goto_0
    return-void

    .line 860
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$4;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$5(Lnet/flixster/android/MovieDetails;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 862
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$4;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$5(Lnet/flixster/android/MovieDetails;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 868
    :cond_1
    :goto_1
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$4;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$0(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight;->getDownloadsRemain()I

    move-result v1

    if-lez v1, :cond_2

    .line 869
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$4;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$0(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    invoke-static {v1}, Lcom/flixster/android/net/DownloadHelper;->downloadMovie(Lnet/flixster/android/model/LockerRight;)V

    .line 870
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$4;->this$0:Lnet/flixster/android/MovieDetails;

    #calls: Lnet/flixster/android/MovieDetails;->delayedStreamingUiUpdate()V
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$6(Lnet/flixster/android/MovieDetails;)V

    goto :goto_0

    .line 863
    :catch_0
    move-exception v0

    .line 865
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 872
    .end local v0           #e:Ljava/lang/Exception;
    :cond_2
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$4;->this$0:Lnet/flixster/android/MovieDetails;

    const v2, 0x3b9acacc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lnet/flixster/android/MovieDetails;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0
.end method
