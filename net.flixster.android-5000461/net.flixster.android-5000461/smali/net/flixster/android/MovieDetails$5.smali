.class Lnet/flixster/android/MovieDetails$5;
.super Landroid/os/Handler;
.source "MovieDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$5;->this$0:Lnet/flixster/android/MovieDetails;

    .line 877
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 879
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$5;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-virtual {v1}, Lnet/flixster/android/MovieDetails;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 894
    :cond_0
    :goto_0
    return-void

    .line 883
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$5;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$5(Lnet/flixster/android/MovieDetails;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 885
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/MovieDetails$5;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$5(Lnet/flixster/android/MovieDetails;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 891
    :cond_2
    :goto_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v1, v1, Lnet/flixster/android/data/DaoException;

    if-eqz v1, :cond_0

    .line 892
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lnet/flixster/android/data/DaoException;

    iget-object v2, p0, Lnet/flixster/android/MovieDetails$5;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-static {v1, v2}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Lcom/flixster/android/activity/common/DecoratedSherlockActivity;)V

    goto :goto_0

    .line 886
    :catch_0
    move-exception v0

    .line 888
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
