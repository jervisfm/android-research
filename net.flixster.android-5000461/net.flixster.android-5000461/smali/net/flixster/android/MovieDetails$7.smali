.class Lnet/flixster/android/MovieDetails$7;
.super Ljava/lang/Object;
.source "MovieDetails.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$7;->this$0:Lnet/flixster/android/MovieDetails;

    .line 903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 915
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 911
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 906
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MovieDetails$7;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$0(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/LockerRight;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/flixster/android/drm/PlaybackManager;->playMovie(Lnet/flixster/android/model/LockerRight;)V

    .line 907
    return-void
.end method
