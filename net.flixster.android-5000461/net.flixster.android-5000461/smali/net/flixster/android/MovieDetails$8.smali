.class Lnet/flixster/android/MovieDetails$8;
.super Ljava/lang/Object;
.source "MovieDetails.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$8;->this$0:Lnet/flixster/android/MovieDetails;

    .line 918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 939
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acac8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 940
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 2
    .parameter "which"

    .prologue
    .line 934
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acac8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 935
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 5
    .parameter "which"

    .prologue
    .line 921
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const v1, 0x3b9acac8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 922
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/download"

    const-string v2, "Download"

    const-string v3, "Download"

    const-string v4, "Confirm"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    iget-object v0, p0, Lnet/flixster/android/MovieDetails$8;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lnet/flixster/android/MovieDetails;->access$5(Lnet/flixster/android/MovieDetails;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 925
    iget-object v0, p0, Lnet/flixster/android/MovieDetails$8;->this$0:Lnet/flixster/android/MovieDetails;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lnet/flixster/android/MovieDetails$8;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #setter for: Lnet/flixster/android/MovieDetails;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lnet/flixster/android/MovieDetails;->access$8(Lnet/flixster/android/MovieDetails;Landroid/app/ProgressDialog;)V

    .line 926
    iget-object v0, p0, Lnet/flixster/android/MovieDetails$8;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lnet/flixster/android/MovieDetails;->access$5(Lnet/flixster/android/MovieDetails;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MovieDetails$8;->this$0:Lnet/flixster/android/MovieDetails;

    invoke-virtual {v1}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0134

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 928
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/MovieDetails$8;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->downloadInitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lnet/flixster/android/MovieDetails;->access$5(Lnet/flixster/android/MovieDetails;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 929
    iget-object v0, p0, Lnet/flixster/android/MovieDetails$8;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->canDownloadSuccessHandler:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/MovieDetails;->access$9(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MovieDetails$8;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->errorHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/MovieDetails;->access$10(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/MovieDetails$8;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v2}, Lnet/flixster/android/MovieDetails;->access$0(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/LockerRight;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lnet/flixster/android/data/ProfileDao;->canDownload(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V

    .line 930
    return-void
.end method
