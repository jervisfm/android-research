.class Lnet/flixster/android/MovieDetails$9;
.super Ljava/lang/Object;
.source "MovieDetails.java"

# interfaces
.implements Lcom/flixster/android/view/DialogBuilder$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieDetails;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieDetails$9;->this$0:Lnet/flixster/android/MovieDetails;

    .line 943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 957
    return-void
.end method

.method public onNeutralButtonClick(I)V
    .locals 0
    .parameter "which"

    .prologue
    .line 953
    return-void
.end method

.method public onPositiveButtonClick(I)V
    .locals 5
    .parameter "which"

    .prologue
    .line 946
    iget-object v0, p0, Lnet/flixster/android/MovieDetails$9;->this$0:Lnet/flixster/android/MovieDetails;

    #getter for: Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;
    invoke-static {v0}, Lnet/flixster/android/MovieDetails;->access$0(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    iget-wide v0, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v0, v1}, Lcom/flixster/android/net/DownloadHelper;->cancelMovieDownload(J)V

    .line 947
    iget-object v0, p0, Lnet/flixster/android/MovieDetails$9;->this$0:Lnet/flixster/android/MovieDetails;

    #calls: Lnet/flixster/android/MovieDetails;->delayedStreamingUiUpdate()V
    invoke-static {v0}, Lnet/flixster/android/MovieDetails;->access$6(Lnet/flixster/android/MovieDetails;)V

    .line 948
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/download"

    const-string v2, "Download"

    const-string v3, "Download"

    const-string v4, "Cancel"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    return-void
.end method
