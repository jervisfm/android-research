.class public Lnet/flixster/android/MovieDetails;
.super Lnet/flixster/android/FlixsterActivity;
.source "MovieDetails.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/MovieDetails$ActorViewHolder;
    }
.end annotation


# static fields
.field private static final DIALOG_NETFLIX_ACTION:I = 0x0

.field private static final DIALOG_NETFLIX_EXCEPTION:I = 0x1

.field public static final KEY_IS_SEASON:Ljava/lang/String; = "KEY_IS_SEASON"

.field public static final KEY_RIGHT_ID:Ljava/lang/String; = "KEY_RIGHT_ID"

.field private static final MAX_ACTORS:I = 0x3

.field private static final MAX_DIRECTORS:I = 0x3

.field private static final MAX_PHOTOS:I = 0xf

.field private static final MAX_PHOTO_COUNT:I = 0x32

.field private static final NETFLIX_ACTION_CANCEL:I = 0x0

.field private static final NETFLIX_ACTION_DVD_ADD_BOTTOM:I = 0x4

.field private static final NETFLIX_ACTION_DVD_ADD_TOP:I = 0x3

.field private static final NETFLIX_ACTION_DVD_MOVE_BOTTOM:I = 0xa

.field private static final NETFLIX_ACTION_DVD_MOVE_TOP:I = 0x9

.field private static final NETFLIX_ACTION_DVD_REMOVE:I = 0x2

.field private static final NETFLIX_ACTION_DVD_SAVE:I = 0x5

.field private static final NETFLIX_ACTION_INSTANT_ADD_BOTTOM:I = 0x8

.field private static final NETFLIX_ACTION_INSTANT_ADD_TOP:I = 0x7

.field private static final NETFLIX_ACTION_INSTANT_MOVE_BOTTOM:I = 0xc

.field private static final NETFLIX_ACTION_INSTANT_MOVE_TOP:I = 0xb

.field private static final NETFLIX_ACTION_INSTANT_REMOVE:I = 0x6

.field private static final NETFLIX_ACTION_VIEW_QUEUE:I = 0x1

.field private static final REQUEST_CODE_REVIEW:I = 0x3f2

.field private static final REQUEST_CODE_TRAILER:I = 0x3e8


# instance fields
.field private final REVIEW_SUBSET_COUNT:I

.field private actorClickListener:Landroid/view/View$OnClickListener;

.field private actorHandler:Landroid/os/Handler;

.field private actorsLayout:Landroid/widget/LinearLayout;

.field private final canDownloadSuccessHandler:Landroid/os/Handler;

.field private final collectedMoviesSuccessHandler:Landroid/os/Handler;

.field private directorsLayout:Landroid/widget/LinearLayout;

.field private final downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private final downloadClickListener:Landroid/view/View$OnClickListener;

.field private final downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private final downloadDeleteClickListener:Landroid/view/View$OnClickListener;

.field private final downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private downloadInitDialog:Landroid/app/ProgressDialog;

.field private downloadPanel:Lcom/flixster/android/view/DownloadPanel;

.field private final errorHandler:Landroid/os/Handler;

.field private inflater:Landroid/view/LayoutInflater;

.field private isSeason:Z

.field private mExtras:Landroid/os/Bundle;

.field private mFromRating:Z

.field private mMovie:Lnet/flixster/android/model/Movie;

.field private mMovieDetails:Lnet/flixster/android/MovieDetails;

.field private mMovieId:J

.field private mNetflixMenuToAction:[I

.field private mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;

.field private mNetflixTitleStateHandler:Landroid/os/Handler;

.field private mPlayTrailer:Ljava/lang/Boolean;

.field private mReview:Lnet/flixster/android/model/Review;

.field private mScrollAd:Lnet/flixster/android/ads/AdView;

.field private moreActorsLayout:Landroid/widget/RelativeLayout;

.field private moreActorsListener:Landroid/view/View$OnClickListener;

.field private moreDirectorsLayout:Landroid/widget/RelativeLayout;

.field private moreDirectorsListener:Landroid/view/View$OnClickListener;

.field private netflixHandler:Landroid/os/Handler;

.field private photoClickListener:Landroid/view/View$OnClickListener;

.field private photoHandler:Landroid/os/Handler;

.field private final populateStreamingUiHandler:Landroid/os/Handler;

.field private refreshHandler:Landroid/os/Handler;

.field private right:Lnet/flixster/android/model/LockerRight;

.field private showGetGlue:Z

.field private final watchNowClickListener:Landroid/view/View$OnClickListener;

.field private final watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private watchNowPanel:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 119
    const/4 v0, 0x3

    iput v0, p0, Lnet/flixster/android/MovieDetails;->REVIEW_SUBSET_COUNT:I

    .line 125
    iput-object v1, p0, Lnet/flixster/android/MovieDetails;->mPlayTrailer:Ljava/lang/Boolean;

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/flixster/android/MovieDetails;->mFromRating:Z

    .line 127
    iput-object v1, p0, Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I

    .line 805
    new-instance v0, Lnet/flixster/android/MovieDetails$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$1;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->watchNowClickListener:Landroid/view/View$OnClickListener;

    .line 818
    new-instance v0, Lnet/flixster/android/MovieDetails$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$2;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadClickListener:Landroid/view/View$OnClickListener;

    .line 840
    new-instance v0, Lnet/flixster/android/MovieDetails$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$3;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadDeleteClickListener:Landroid/view/View$OnClickListener;

    .line 854
    new-instance v0, Lnet/flixster/android/MovieDetails$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$4;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->canDownloadSuccessHandler:Landroid/os/Handler;

    .line 877
    new-instance v0, Lnet/flixster/android/MovieDetails$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$5;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->errorHandler:Landroid/os/Handler;

    .line 897
    new-instance v0, Lnet/flixster/android/MovieDetails$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$6;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->collectedMoviesSuccessHandler:Landroid/os/Handler;

    .line 903
    new-instance v0, Lnet/flixster/android/MovieDetails$7;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$7;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 918
    new-instance v0, Lnet/flixster/android/MovieDetails$8;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$8;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 943
    new-instance v0, Lnet/flixster/android/MovieDetails$9;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$9;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 960
    new-instance v0, Lnet/flixster/android/MovieDetails$10;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$10;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 991
    new-instance v0, Lnet/flixster/android/MovieDetails$11;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$11;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->populateStreamingUiHandler:Landroid/os/Handler;

    .line 1334
    new-instance v0, Lnet/flixster/android/MovieDetails$12;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$12;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->netflixHandler:Landroid/os/Handler;

    .line 1344
    new-instance v0, Lnet/flixster/android/MovieDetails$13;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$13;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->refreshHandler:Landroid/os/Handler;

    .line 1375
    new-instance v0, Lnet/flixster/android/MovieDetails$14;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$14;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->photoHandler:Landroid/os/Handler;

    .line 1587
    new-instance v0, Lnet/flixster/android/MovieDetails$15;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$15;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->photoClickListener:Landroid/view/View$OnClickListener;

    .line 1607
    new-instance v0, Lnet/flixster/android/MovieDetails$16;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$16;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->moreDirectorsListener:Landroid/view/View$OnClickListener;

    .line 1633
    new-instance v0, Lnet/flixster/android/MovieDetails$17;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$17;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->moreActorsListener:Landroid/view/View$OnClickListener;

    .line 1661
    new-instance v0, Lnet/flixster/android/MovieDetails$18;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$18;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->actorClickListener:Landroid/view/View$OnClickListener;

    .line 1673
    new-instance v0, Lnet/flixster/android/MovieDetails$19;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$19;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->actorHandler:Landroid/os/Handler;

    .line 1687
    new-instance v0, Lnet/flixster/android/MovieDetails$20;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$20;-><init>(Lnet/flixster/android/MovieDetails;)V

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->mNetflixTitleStateHandler:Landroid/os/Handler;

    .line 80
    return-void
.end method

.method private ScheduleAddToQueue(ILjava/lang/String;)V
    .locals 4
    .parameter "nPosition"
    .parameter "queueType"

    .prologue
    .line 1171
    new-instance v0, Lnet/flixster/android/MovieDetails$23;

    invoke-direct {v0, p0, p1, p2}, Lnet/flixster/android/MovieDetails$23;-><init>(Lnet/flixster/android/MovieDetails;ILjava/lang/String;)V

    .line 1202
    .local v0, netflixAddQueueTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 1203
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1205
    :cond_0
    return-void
.end method

.method private ScheduleLoadMovieTask(J)V
    .locals 3
    .parameter "delay"

    .prologue
    .line 1286
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    .line 1287
    .local v0, currResumeCtr:I
    new-instance v1, Lnet/flixster/android/MovieDetails$26;

    invoke-direct {v1, p0, v0}, Lnet/flixster/android/MovieDetails$26;-><init>(Lnet/flixster/android/MovieDetails;I)V

    .line 1324
    .local v1, loadMovieTask:Ljava/util/TimerTask;
    iget-object v2, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 1325
    iget-object v2, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v1, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1327
    :cond_0
    return-void
.end method

.method private ScheduleMoveToBottom(Ljava/lang/String;)V
    .locals 4
    .parameter "queueType"

    .prologue
    .line 1208
    new-instance v0, Lnet/flixster/android/MovieDetails$24;

    invoke-direct {v0, p0, p1}, Lnet/flixster/android/MovieDetails$24;-><init>(Lnet/flixster/android/MovieDetails;Ljava/lang/String;)V

    .line 1243
    .local v0, netflixMoveToBottom:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 1244
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1246
    :cond_0
    return-void
.end method

.method private ScheduleNetflixTitleState()V
    .locals 4

    .prologue
    .line 1145
    new-instance v0, Lnet/flixster/android/MovieDetails$22;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieDetails$22;-><init>(Lnet/flixster/android/MovieDetails;)V

    .line 1165
    .local v0, checkNetflixQueueTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 1166
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1168
    :cond_0
    return-void
.end method

.method private ScheduleRemoveFromQueue(Ljava/lang/String;)V
    .locals 4
    .parameter "qtype"

    .prologue
    .line 1249
    new-instance v0, Lnet/flixster/android/MovieDetails$25;

    invoke-direct {v0, p0, p1}, Lnet/flixster/android/MovieDetails$25;-><init>(Lnet/flixster/android/MovieDetails;Ljava/lang/String;)V

    .line 1280
    .local v0, netflixDeleteQueueTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 1281
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1283
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/LockerRight;
    .locals 1
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/MovieDetails;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 903
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->watchNowConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$10(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 877
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->errorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$11(Lnet/flixster/android/MovieDetails;)Lcom/flixster/android/activity/decorator/TopLevelDecorator;
    .locals 1
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->topLevelDecorator:Lcom/flixster/android/activity/decorator/TopLevelDecorator;

    return-object v0
.end method

.method static synthetic access$12(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/MovieDetails;
    .locals 1
    .parameter

    .prologue
    .line 118
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mMovieDetails:Lnet/flixster/android/MovieDetails;

    return-object v0
.end method

.method static synthetic access$13(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 229
    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->populatePage()V

    return-void
.end method

.method static synthetic access$14(Lnet/flixster/android/MovieDetails;)Ljava/lang/Boolean;
    .locals 1
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mPlayTrailer:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$15(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter

    .prologue
    .line 122
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    return-object v0
.end method

.method static synthetic access$16(Lnet/flixster/android/MovieDetails;Ljava/lang/Boolean;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 125
    iput-object p1, p0, Lnet/flixster/android/MovieDetails;->mPlayTrailer:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$17(Lnet/flixster/android/MovieDetails;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 1891
    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$18(Lnet/flixster/android/MovieDetails;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 1895
    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$19(Lnet/flixster/android/MovieDetails;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->directorsLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/MovieDetails;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 918
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadConfirmDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$20(Lnet/flixster/android/MovieDetails;Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1748
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/MovieDetails;->getActorView(Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$21(Lnet/flixster/android/MovieDetails;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 110
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$22(Lnet/flixster/android/MovieDetails;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->actorsLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$23(Lnet/flixster/android/MovieDetails;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 112
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->moreActorsLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$24(Lnet/flixster/android/MovieDetails;)Lnet/flixster/android/model/NetflixTitleState;
    .locals 1
    .parameter

    .prologue
    .line 124
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;

    return-object v0
.end method

.method static synthetic access$25(Lnet/flixster/android/MovieDetails;[I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 127
    iput-object p1, p0, Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I

    return-void
.end method

.method static synthetic access$26(Lnet/flixster/android/MovieDetails;)[I
    .locals 1
    .parameter

    .prologue
    .line 127
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I

    return-object v0
.end method

.method static synthetic access$27(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 991
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->populateStreamingUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$28(Lnet/flixster/android/MovieDetails;Lnet/flixster/android/model/NetflixTitleState;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 124
    iput-object p1, p0, Lnet/flixster/android/MovieDetails;->mNetflixTitleState:Lnet/flixster/android/model/NetflixTitleState;

    return-void
.end method

.method static synthetic access$29(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1687
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mNetflixTitleStateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/MovieDetails;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 943
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadCancelDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$30(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 1144
    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->ScheduleNetflixTitleState()V

    return-void
.end method

.method static synthetic access$31(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1334
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->netflixHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$32(Lnet/flixster/android/MovieDetails;)J
    .locals 2
    .parameter

    .prologue
    .line 123
    iget-wide v0, p0, Lnet/flixster/android/MovieDetails;->mMovieId:J

    return-wide v0
.end method

.method static synthetic access$33(Lnet/flixster/android/MovieDetails;)Z
    .locals 1
    .parameter

    .prologue
    .line 130
    iget-boolean v0, p0, Lnet/flixster/android/MovieDetails;->isSeason:Z

    return v0
.end method

.method static synthetic access$34(Lnet/flixster/android/MovieDetails;Lnet/flixster/android/model/Movie;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    return-void
.end method

.method static synthetic access$35(Lnet/flixster/android/MovieDetails;)Z
    .locals 1
    .parameter

    .prologue
    .line 126
    iget-boolean v0, p0, Lnet/flixster/android/MovieDetails;->mFromRating:Z

    return v0
.end method

.method static synthetic access$36(Lnet/flixster/android/MovieDetails;Lnet/flixster/android/model/Review;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 121
    iput-object p1, p0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    return-void
.end method

.method static synthetic access$37(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1344
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->refreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$38(Lnet/flixster/android/MovieDetails;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1170
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/MovieDetails;->ScheduleAddToQueue(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$39(Lnet/flixster/android/MovieDetails;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1207
    invoke-direct {p0, p1}, Lnet/flixster/android/MovieDetails;->ScheduleMoveToBottom(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/MovieDetails;)Lcom/flixster/android/view/DialogBuilder$DialogListener;
    .locals 1
    .parameter

    .prologue
    .line 960
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadDeleteDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    return-object v0
.end method

.method static synthetic access$40(Lnet/flixster/android/MovieDetails;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1248
    invoke-direct {p0, p1}, Lnet/flixster/android/MovieDetails;->ScheduleRemoveFromQueue(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$5(Lnet/flixster/android/MovieDetails;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadInitDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 980
    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->delayedStreamingUiUpdate()V

    return-void
.end method

.method static synthetic access$7(Lnet/flixster/android/MovieDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 786
    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->populateStreamingUi()V

    return-void
.end method

.method static synthetic access$8(Lnet/flixster/android/MovieDetails;Landroid/app/ProgressDialog;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 116
    iput-object p1, p0, Lnet/flixster/android/MovieDetails;->downloadInitDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/MovieDetails;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 854
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->canDownloadSuccessHandler:Landroid/os/Handler;

    return-object v0
.end method

.method protected static createMovieSummary(Lnet/flixster/android/model/Movie;)Ljava/lang/String;
    .locals 3
    .parameter "m"

    .prologue
    .line 1929
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1930
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1931
    const-string v1, "MOVIE_ACTORS_SHORT"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1932
    const-string v1, "MOVIE_ACTORS_SHORT"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1934
    :cond_0
    const-string v1, "dvdReleaseDate"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1935
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DVD release: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "dvdReleaseDate"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1939
    :cond_1
    :goto_0
    const-string v1, "meta"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1940
    const-string v1, "meta"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1942
    :cond_2
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1943
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1936
    :cond_3
    const-string v1, "theaterReleaseDate"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1937
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Theater release: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "theaterReleaseDate"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private delayedStreamingUiUpdate()V
    .locals 4

    .prologue
    .line 981
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 982
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mPageTimer:Ljava/util/Timer;

    new-instance v1, Lnet/flixster/android/MovieDetails$21;

    invoke-direct {v1, p0}, Lnet/flixster/android/MovieDetails$21;-><init>(Lnet/flixster/android/MovieDetails;)V

    .line 987
    const-wide/16 v2, 0x7d0

    .line 982
    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 989
    :cond_0
    return-void
.end method

.method private getActorView(Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;
    .locals 9
    .parameter "actor"
    .parameter "actorView"

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f0201da

    .line 1750
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03004d

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1751
    new-instance v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;

    invoke-direct {v8, v4}, Lnet/flixster/android/MovieDetails$ActorViewHolder;-><init>(Lnet/flixster/android/MovieDetails$ActorViewHolder;)V

    .line 1752
    .local v8, viewHolder:Lnet/flixster/android/MovieDetails$ActorViewHolder;
    const v1, 0x7f070028

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    .line 1753
    const v1, 0x7f07002c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->nameView:Landroid/widget/TextView;

    .line 1754
    const v1, 0x7f070104

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->charsView:Landroid/widget/TextView;

    .line 1755
    const v1, 0x7f07002b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    .line 1757
    if-eqz p1, :cond_1

    .line 1758
    iget-object v7, p1, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    .line 1759
    .local v7, name:Ljava/lang/String;
    iget-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->nameView:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1761
    iget-object v6, p1, Lnet/flixster/android/model/Actor;->chars:Ljava/lang/String;

    .line 1762
    .local v6, chars:Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 1763
    iget-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->charsView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1764
    iget-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->charsView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1767
    :cond_0
    iget-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1768
    iget-object v1, p1, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 1769
    iget-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    iget-object v2, p1, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1778
    :goto_0
    iget-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 1779
    iget-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lnet/flixster/android/MovieDetails;->actorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1781
    .end local v6           #chars:Ljava/lang/String;
    .end local v7           #name:Ljava/lang/String;
    :cond_1
    return-object p2

    .line 1770
    .restart local v6       #chars:Ljava/lang/String;
    .restart local v7       #name:Ljava/lang/String;
    :cond_2
    iget-object v1, p1, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1771
    iget-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1772
    new-instance v0, Lnet/flixster/android/model/ImageOrder;

    const/4 v1, 0x4

    iget-object v3, p1, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    .line 1773
    iget-object v4, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->actorHandler:Landroid/os/Handler;

    move-object v2, p1

    .line 1772
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 1774
    .local v0, imageOrder:Lnet/flixster/android/model/ImageOrder;
    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieDetails;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_0

    .line 1776
    .end local v0           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_3
    iget-object v1, v8, Lnet/flixster/android/MovieDetails$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private getGaTagPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1892
    iget-boolean v0, p0, Lnet/flixster/android/MovieDetails;->isSeason:Z

    if-eqz v0, :cond_0

    const-string v0, "/tv-season"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "/movie"

    goto :goto_0
.end method

.method private getGaTitlePrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1896
    iget-boolean v0, p0, Lnet/flixster/android/MovieDetails;->isSeason:Z

    if-eqz v0, :cond_0

    const-string v0, "TV Season "

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Movie "

    goto :goto_0
.end method

.method private loadCriticReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V
    .locals 16
    .parameter "startIndex"
    .parameter "endIndex"
    .parameter "startTagIndex"
    .parameter "ll"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1040
    .local p5, reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    move/from16 v4, p3

    .line 1042
    .local v4, iTagIndex:I
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 1051
    .local v6, li:Landroid/view/LayoutInflater;
    const/4 v3, 0x0

    .line 1053
    .local v3, iReviewCount:I
    move/from16 v3, p1

    :goto_0
    move/from16 v0, p2

    if-lt v3, v0, :cond_0

    .line 1086
    return-void

    .line 1054
    :cond_0
    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lnet/flixster/android/model/Review;

    .line 1056
    .local v8, r:Lnet/flixster/android/model/Review;
    const v13, 0x7f030022

    const/4 v14, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v6, v13, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 1057
    .local v5, itemView:Landroid/view/View;
    const v13, 0x7f030022

    invoke-virtual {v5, v13}, Landroid/view/View;->setId(I)V

    .line 1059
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Landroid/view/View;->setFocusable(Z)V

    .line 1060
    const v13, 0x7f070068

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 1061
    .local v11, ri:Landroid/widget/ImageView;
    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1062
    const v13, 0x7f070065

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1063
    .local v2, bylineView:Landroid/widget/TextView;
    iget-object v13, v8, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1064
    const v13, 0x7f070067

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 1065
    .local v12, sourceView:Landroid/widget/TextView;
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, ", "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v14, v8, Lnet/flixster/android/model/Review;->source:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1066
    const v13, 0x7f070069

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 1067
    .local v10, reviewView:Landroid/widget/TextView;
    const/16 v13, 0xa

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1068
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "     "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v14, v8, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1069
    iget v13, v8, Lnet/flixster/android/model/Review;->score:I

    const/16 v14, 0x3c

    if-ge v13, v14, :cond_1

    .line 1070
    const v13, 0x7f070066

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 1071
    .local v9, reviewIconView:Landroid/widget/ImageView;
    const v13, 0x7f0200ed

    invoke-virtual {v9, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1074
    .end local v9           #reviewIconView:Landroid/widget/ImageView;
    :cond_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1075
    const-string v13, "FlxMain"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "MovieDetails.loadCriticReviews iTagIndex:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    add-int/lit8 v4, v4, 0x1

    .line 1077
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1079
    const v13, 0x7f070064

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1080
    .local v7, mugImageView:Landroid/widget/ImageView;
    invoke-virtual {v8, v7}, Lnet/flixster/android/model/Review;->getReviewerBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1081
    .local v1, bitmap:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_2

    .line 1082
    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1084
    :cond_2
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1053
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0
.end method

.method private loadFriendReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V
    .locals 17
    .parameter "begin"
    .parameter "end"
    .parameter "start"
    .parameter "reviewLayout"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1007
    .local p5, reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    move/from16 v11, p3

    .line 1009
    .local v11, tag:I
    move/from16 v5, p1

    .local v5, i:I
    :goto_0
    move/from16 v0, p2

    if-lt v5, v0, :cond_0

    .line 1034
    return-void

    .line 1010
    :cond_0
    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/model/Review;

    .line 1011
    .local v7, review:Lnet/flixster/android/model/Review;
    move-object/from16 v0, p0

    iget-object v12, v0, Lnet/flixster/android/MovieDetails;->inflater:Landroid/view/LayoutInflater;

    const v13, 0x7f030022

    const/4 v14, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v12, v13, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 1012
    .local v9, reviewView:Landroid/view/View;
    const v12, 0x7f030022

    invoke-virtual {v9, v12}, Landroid/view/View;->setId(I)V

    .line 1013
    const/4 v12, 0x1

    invoke-virtual {v9, v12}, Landroid/view/View;->setFocusable(Z)V

    .line 1014
    const v12, 0x7f070068

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 1015
    .local v6, imageView:Landroid/widget/ImageView;
    sget-object v12, Lnet/flixster/android/Flixster;->RATING_SMALL_R:[I

    iget-wide v13, v7, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v15, 0x4000

    mul-double/2addr v13, v15

    double-to-int v13, v13

    aget v12, v12, v13

    invoke-virtual {v6, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1016
    const v12, 0x7f070065

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1017
    .local v1, authorView:Landroid/widget/TextView;
    iget-object v12, v7, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1018
    const v12, 0x7f070067

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 1019
    .local v10, sourceView:Landroid/widget/TextView;
    const/16 v12, 0x8

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1020
    const v12, 0x7f070069

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1021
    .local v3, commentView:Landroid/widget/TextView;
    iget-object v12, v7, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1022
    const v12, 0x7f070066

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 1023
    .local v8, reviewIconView:Landroid/widget/ImageView;
    const/16 v12, 0x8

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1024
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1025
    add-int/lit8 v11, v11, 0x1

    .line 1026
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1027
    const v12, 0x7f070064

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 1028
    .local v4, friendImage:Landroid/widget/ImageView;
    invoke-virtual {v7, v4}, Lnet/flixster/android/model/Review;->getReviewerBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1029
    .local v2, bitmap:Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1

    .line 1030
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1032
    :cond_1
    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1009
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0
.end method

.method private loadUserReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V
    .locals 18
    .parameter "startIndex"
    .parameter "endIndex"
    .parameter "startTagIndex"
    .parameter "ll"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1093
    .local p5, reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    move/from16 v4, p3

    .line 1094
    .local v4, iTagIndex:I
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 1103
    .local v6, li:Landroid/view/LayoutInflater;
    const/4 v3, 0x0

    .line 1105
    .local v3, iReviewCount:I
    move/from16 v3, p1

    :goto_0
    move/from16 v0, p2

    if-lt v3, v0, :cond_0

    .line 1142
    return-void

    .line 1108
    :cond_0
    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lnet/flixster/android/model/Review;

    .line 1110
    .local v8, r:Lnet/flixster/android/model/Review;
    const v13, 0x7f030022

    const/4 v14, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v6, v13, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 1111
    .local v5, itemView:Landroid/view/View;
    const v13, 0x7f030022

    invoke-virtual {v5, v13}, Landroid/view/View;->setId(I)V

    .line 1112
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Landroid/view/View;->setFocusable(Z)V

    .line 1114
    const v13, 0x7f070068

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 1115
    .local v11, ri:Landroid/widget/ImageView;
    sget-object v13, Lnet/flixster/android/Flixster;->RATING_SMALL_R:[I

    iget-wide v14, v8, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v16, 0x4000

    mul-double v14, v14, v16

    double-to-int v14, v14

    aget v13, v13, v14

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1117
    const v13, 0x7f070065

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1118
    .local v2, bylineView:Landroid/widget/TextView;
    iget-object v13, v8, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1119
    const v13, 0x7f070067

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 1121
    .local v12, sourceView:Landroid/widget/TextView;
    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1122
    const v13, 0x7f070069

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 1123
    .local v10, reviewView:Landroid/widget/TextView;
    iget-object v13, v8, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1126
    const v13, 0x7f070066

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 1127
    .local v9, reviewIconView:Landroid/widget/ImageView;
    const/16 v13, 0x8

    invoke-virtual {v9, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1129
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1131
    add-int/lit8 v4, v4, 0x1

    .line 1132
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1135
    const v13, 0x7f070064

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1136
    .local v7, mugImageView:Landroid/widget/ImageView;
    invoke-virtual {v8, v7}, Lnet/flixster/android/model/Review;->getReviewerBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1137
    .local v1, bitmap:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_1

    .line 1138
    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1140
    :cond_1
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1105
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0
.end method

.method private populatePage()V
    .locals 111

    .prologue
    .line 230
    const-string v4, "FlxMain"

    const-string v6, "MovieDetails.populatePage()"

    invoke-static {v4, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-nez v4, :cond_0

    .line 232
    const-string v4, "FlxMain"

    const-string v6, "MovieDetails.populatePage() mMovie == null, skip return early from populatePage()"

    invoke-static {v4, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    :goto_0
    return-void

    .line 235
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->inflater:Landroid/view/LayoutInflater;

    if-nez v4, :cond_1

    .line 236
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lnet/flixster/android/MovieDetails;->inflater:Landroid/view/LayoutInflater;

    .line 239
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v4, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v100

    .line 240
    .local v100, title:Ljava/lang/String;
    :goto_1
    if-eqz v100, :cond_2

    .line 241
    const v4, 0x7f070147

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v73

    check-cast v73, Landroid/widget/TextView;

    .line 242
    .local v73, movieTitleView:Landroid/widget/TextView;
    move-object/from16 v0, v73

    move-object/from16 v1, v100

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    move-object/from16 v0, p0

    move-object/from16 v1, v100

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieDetails;->setActionBarTitle(Ljava/lang/String;)V

    .line 246
    .end local v73           #movieTitleView:Landroid/widget/TextView;
    :cond_2
    const v4, 0x7f070155

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v81

    check-cast v81, Landroid/widget/RelativeLayout;

    .line 247
    .local v81, ratingLayout:Landroid/widget/RelativeLayout;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lnet/flixster/android/MovieDetails;->isSeason:Z

    if-eqz v4, :cond_12

    .line 248
    const/16 v4, 0x8

    move-object/from16 v0, v81

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 264
    :goto_2
    const v4, 0x7f070153

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v94

    check-cast v94, Landroid/widget/TextView;

    .line 266
    .local v94, showtimesTextView:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-boolean v4, v4, Lnet/flixster/android/model/Movie;->isMIT:Z

    if-eqz v4, :cond_14

    .line 267
    const/4 v4, 0x0

    move-object/from16 v0, v94

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    :goto_3
    const v4, 0x7f070158

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v52

    check-cast v52, Landroid/widget/TextView;

    .line 273
    .local v52, getGlue:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lnet/flixster/android/MovieDetails;->showGetGlue:Z

    if-eqz v4, :cond_15

    .line 274
    const/4 v4, 0x0

    move-object/from16 v0, v52

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 275
    const/4 v4, 0x1

    move-object/from16 v0, v52

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 276
    move-object/from16 v0, v52

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    :goto_4
    const v4, 0x7f070162

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v76

    check-cast v76, Landroid/widget/LinearLayout;

    .line 282
    .local v76, photoLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f070154

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v107

    check-cast v107, Landroid/widget/TextView;

    .line 283
    .local v107, viewPhotos:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v0, v4, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    move-object/from16 v78, v0

    .line 284
    .local v78, photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    if-eqz v78, :cond_1c

    .line 285
    invoke-virtual/range {v78 .. v78}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 286
    const/16 v4, 0x8

    move-object/from16 v0, v107

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 331
    :goto_5
    const v4, 0x7f070145

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v61

    check-cast v61, Landroid/widget/ImageView;

    .line 332
    .local v61, iv:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v4, :cond_1e

    .line 333
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->getProfilePoster()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1d

    .line 334
    const v4, 0x7f02014f

    move-object/from16 v0, v61

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 352
    :cond_3
    :goto_6
    const v4, 0x7f070164

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lnet/flixster/android/MovieDetails;->directorsLayout:Landroid/widget/LinearLayout;

    .line 353
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 354
    const v4, 0x7f070163

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/TextView;

    .line 355
    .local v43, directorsHeader:Landroid/widget/TextView;
    const/4 v4, 0x0

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    const/16 v42, 0x0

    .line 357
    .local v42, directorView:Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->directorsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 358
    const/16 v54, 0x0

    .local v54, i:I
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v54

    if-ge v0, v4, :cond_4

    const/4 v4, 0x3

    move/from16 v0, v54

    if-lt v0, v4, :cond_20

    .line 363
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_5

    .line 364
    const v4, 0x7f070165

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lnet/flixster/android/MovieDetails;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    .line 365
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 366
    new-instance v101, Ljava/lang/StringBuilder;

    invoke-direct/range {v101 .. v101}, Ljava/lang/StringBuilder;-><init>()V

    .line 367
    .local v101, totalBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, v101

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " total"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    .line 369
    const v6, 0x7f070167

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v69

    .line 368
    check-cast v69, Landroid/widget/TextView;

    .line 370
    .local v69, moreDirectorsLabel:Landroid/widget/TextView;
    invoke-virtual/range {v101 .. v101}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 371
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 372
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 373
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->moreDirectorsLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/MovieDetails;->moreDirectorsListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 377
    .end local v42           #directorView:Landroid/view/View;
    .end local v43           #directorsHeader:Landroid/widget/TextView;
    .end local v54           #i:I
    .end local v69           #moreDirectorsLabel:Landroid/widget/TextView;
    .end local v101           #totalBuilder:Ljava/lang/StringBuilder;
    :cond_5
    const v4, 0x7f07010b

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lnet/flixster/android/MovieDetails;->actorsLayout:Landroid/widget/LinearLayout;

    .line 378
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    .line 379
    const v4, 0x7f070168

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/TextView;

    .line 380
    .local v33, actorsHeader:Landroid/widget/TextView;
    const/4 v4, 0x0

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 381
    const/16 v32, 0x0

    .line 382
    .local v32, actorView:Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->actorsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 383
    const/16 v54, 0x0

    .restart local v54       #i:I
    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v54

    if-ge v0, v4, :cond_6

    const/4 v4, 0x3

    move/from16 v0, v54

    if-lt v0, v4, :cond_21

    .line 388
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_7

    .line 389
    const v4, 0x7f070169

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lnet/flixster/android/MovieDetails;->moreActorsLayout:Landroid/widget/RelativeLayout;

    .line 390
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->moreActorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 391
    new-instance v101, Ljava/lang/StringBuilder;

    invoke-direct/range {v101 .. v101}, Ljava/lang/StringBuilder;-><init>()V

    .line 392
    .restart local v101       #totalBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, v101

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " total"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->moreActorsLayout:Landroid/widget/RelativeLayout;

    const v6, 0x7f07016b

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v68

    check-cast v68, Landroid/widget/TextView;

    .line 394
    .local v68, moreActorsLabel:Landroid/widget/TextView;
    invoke-virtual/range {v101 .. v101}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v68

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->moreActorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 396
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->moreActorsLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 397
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->moreActorsLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/MovieDetails;->moreActorsListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    .end local v32           #actorView:Landroid/view/View;
    .end local v33           #actorsHeader:Landroid/widget/TextView;
    .end local v54           #i:I
    .end local v68           #moreActorsLabel:Landroid/widget/TextView;
    .end local v101           #totalBuilder:Ljava/lang/StringBuilder;
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->isDetailsApiParsed()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 402
    const/4 v4, 0x5

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v79, v0

    const/4 v4, 0x0

    const-string v6, "genre"

    aput-object v6, v79, v4

    const/4 v4, 0x1

    const-string v6, "runningTime"

    aput-object v6, v79, v4

    const/4 v4, 0x2

    const-string v6, "theaterReleaseDate"

    aput-object v6, v79, v4

    const/4 v4, 0x3

    .line 403
    const-string v6, "dvdReleaseDate"

    aput-object v6, v79, v4

    const/4 v4, 0x4

    const-string v6, "mpaa"

    aput-object v6, v79, v4

    .line 404
    .local v79, propArray:[Ljava/lang/String;
    const/4 v4, 0x5

    new-array v0, v4, [I

    move-object/from16 v99, v0

    fill-array-data v99, :array_0

    .line 407
    .local v99, textViewIdArray:[I
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v84

    .line 408
    .local v84, res:Landroid/content/res/Resources;
    const/4 v4, 0x5

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v62, v0

    const/4 v4, 0x0

    const v6, 0x7f0c0041

    move-object/from16 v0, v84

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v62, v4

    const/4 v4, 0x1

    const v6, 0x7f0c0023

    move-object/from16 v0, v84

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v62, v4

    const/4 v4, 0x2

    .line 409
    const v6, 0x7f0c0024

    move-object/from16 v0, v84

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v62, v4

    const/4 v4, 0x3

    const v6, 0x7f0c0028

    move-object/from16 v0, v84

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v62, v4

    const/4 v4, 0x4

    .line 410
    const v6, 0x7f0c0022

    move-object/from16 v0, v84

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v62, v4

    .line 412
    .local v62, labelArray:[Ljava/lang/String;
    move-object/from16 v0, v79

    array-length v0, v0

    move/from16 v63, v0

    .line 415
    .local v63, length:I
    const/16 v54, 0x0

    .restart local v54       #i:I
    :goto_9
    move/from16 v0, v54

    move/from16 v1, v63

    if-lt v0, v1, :cond_22

    .line 432
    const v4, 0x7f07015c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v97

    check-cast v97, Lcom/flixster/android/view/SynopsisView;

    .line 433
    .local v97, synopsis:Lcom/flixster/android/view/SynopsisView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "synopsis"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0xaa

    const/4 v8, 0x1

    move-object/from16 v0, v97

    invoke-virtual {v0, v4, v6, v8}, Lcom/flixster/android/view/SynopsisView;->load(Ljava/lang/String;IZ)V

    .line 436
    .end local v54           #i:I
    .end local v62           #labelArray:[Ljava/lang/String;
    .end local v63           #length:I
    .end local v79           #propArray:[Ljava/lang/String;
    .end local v84           #res:Landroid/content/res/Resources;
    .end local v97           #synopsis:Lcom/flixster/android/view/SynopsisView;
    .end local v99           #textViewIdArray:[I
    :cond_8
    const v4, 0x7f070149

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v89

    check-cast v89, Landroid/widget/TextView;

    .line 437
    .local v89, rtScoreline:Landroid/widget/TextView;
    const v4, 0x7f07017b

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v40

    check-cast v40, Landroid/widget/ImageView;

    .line 438
    .local v40, criticsImage:Landroid/widget/ImageView;
    const v4, 0x7f07017a

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v87

    check-cast v87, Landroid/widget/TextView;

    .line 439
    .local v87, rottenScore:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "rottenTomatoes"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 440
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "rottenTomatoes"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v56

    .line 442
    .local v56, iRS:I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v56 .. v56}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f0c0162

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v89

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 445
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v56 .. v56}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "%"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v87

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    const/16 v4, 0x3c

    move/from16 v0, v56

    if-ge v0, v4, :cond_9

    .line 449
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0200ed

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v86

    .line 450
    .local v86, rotten:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v86 .. v86}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual/range {v86 .. v86}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    move-object/from16 v0, v86

    invoke-virtual {v0, v4, v6, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 451
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v89

    move-object/from16 v1, v86

    invoke-virtual {v0, v1, v4, v6, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 454
    const v4, 0x7f0200ec

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 462
    .end local v56           #iRS:I
    .end local v86           #rotten:Landroid/graphics/drawable/Drawable;
    :cond_9
    :goto_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v4

    if-eqz v4, :cond_25

    .line 463
    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v6}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-nez v4, :cond_25

    const/16 v59, 0x0

    .line 465
    .local v59, isReleased:Z
    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "popcornScore"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 466
    const v4, 0x7f070148

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v91

    check-cast v91, Landroid/widget/TextView;

    .line 467
    .local v91, scoreTextView:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "popcornScore"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v55

    .line 468
    .local v55, iFS:I
    const/16 v4, 0x3c

    move/from16 v0, v55

    if-ge v0, v4, :cond_26

    const/16 v60, 0x1

    .line 471
    .local v60, isSpilled:Z
    :goto_c
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0163

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v64

    .line 472
    .local v64, liked:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0164

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v110

    .line 473
    .local v110, wts:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v55 .. v55}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v59, :cond_27

    .end local v64           #liked:Ljava/lang/String;
    :goto_d
    move-object/from16 v0, v64

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v91

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 474
    if-nez v59, :cond_28

    const v57, 0x7f0200f6

    .line 476
    .local v57, iconId:I
    :goto_e
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move/from16 v0, v57

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v90

    .line 477
    .local v90, scoreIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v90 .. v90}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual/range {v90 .. v90}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    move-object/from16 v0, v90

    invoke-virtual {v0, v4, v6, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 478
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v91

    move-object/from16 v1, v90

    invoke-virtual {v0, v1, v4, v6, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 481
    const v4, 0x7f070183

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v91

    .end local v91           #scoreTextView:Landroid/widget/TextView;
    check-cast v91, Landroid/widget/TextView;

    .line 482
    .restart local v91       #scoreTextView:Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v55 .. v55}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "%"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v91

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 483
    const v4, 0x7f070184

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v53

    check-cast v53, Landroid/widget/ImageView;

    .line 484
    .local v53, headerIcon:Landroid/widget/ImageView;
    if-nez v59, :cond_2a

    const v4, 0x7f0200f5

    :goto_f
    move-object/from16 v0, v53

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 493
    .end local v53           #headerIcon:Landroid/widget/ImageView;
    .end local v55           #iFS:I
    .end local v57           #iconId:I
    .end local v60           #isSpilled:Z
    .end local v90           #scoreIcon:Landroid/graphics/drawable/Drawable;
    .end local v110           #wts:Ljava/lang/String;
    :goto_10
    const v4, 0x7f07014a

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v47

    check-cast v47, Landroid/widget/TextView;

    .line 494
    .local v47, friendScore:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_2e

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2e

    .line 495
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v48

    .line 496
    .local v48, friendsRatedCount:I
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0200eb

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v85

    .line 497
    .local v85, reviewIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v85 .. v85}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual/range {v85 .. v85}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    move-object/from16 v0, v85

    invoke-virtual {v0, v4, v6, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 498
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v47

    move-object/from16 v1, v85

    invoke-virtual {v0, v1, v4, v6, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 499
    new-instance v80, Ljava/lang/StringBuilder;

    invoke-direct/range {v80 .. v80}, Ljava/lang/StringBuilder;-><init>()V

    .line 500
    .local v80, ratingCountBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, v80

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 501
    const/4 v4, 0x1

    move/from16 v0, v48

    if-le v0, v4, :cond_2d

    .line 502
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0090

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v80

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    :goto_11
    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v47

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 507
    const/4 v4, 0x0

    move-object/from16 v0, v47

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 525
    .end local v48           #friendsRatedCount:I
    .end local v80           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v85           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :goto_12
    const v4, 0x7f07014b

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/TextView;

    .line 526
    .local v34, actorsShort:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_31

    .line 527
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 528
    const/4 v4, 0x0

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 533
    :goto_13
    const v4, 0x7f07014c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v67

    check-cast v67, Landroid/widget/TextView;

    .line 534
    .local v67, metaLine:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "meta"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_32

    .line 535
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "meta"

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v67

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 536
    const/4 v4, 0x0

    move-object/from16 v0, v67

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 541
    :goto_14
    const v4, 0x7f07014d

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v83

    check-cast v83, Landroid/widget/TextView;

    .line 542
    .local v83, rentalExpiration:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v4, :cond_35

    .line 544
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->getViewingExpirationString()Ljava/lang/String;

    move-result-object v44

    .local v44, expirationText:Ljava/lang/String;
    if-eqz v44, :cond_33

    .line 545
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0200b9

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v45

    .line 546
    .local v45, expireIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v83

    move-object/from16 v1, v45

    invoke-virtual {v0, v1, v4, v6, v8}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 547
    move-object/from16 v0, v83

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 548
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f09002b

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v83

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 549
    const/4 v4, 0x0

    move-object/from16 v0, v83

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 564
    .end local v44           #expirationText:Ljava/lang/String;
    .end local v45           #expireIcon:Landroid/graphics/drawable/Drawable;
    :goto_15
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v4

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v4, :cond_a

    .line 565
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->collectedMoviesSuccessHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/MovieDetails;->errorHandler:Landroid/os/Handler;

    invoke-static {v4, v6}, Lnet/flixster/android/data/ProfileDao;->getUserLockerRights(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 568
    :cond_a
    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->populateSeasonUi()V

    .line 570
    const/16 v92, 0x0

    .line 572
    .local v92, showMovieWebsitesHeaderFlag:Z
    const v4, 0x7f07018b

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v46

    check-cast v46, Landroid/widget/TextView;

    .line 573
    .local v46, flixsterPanel:Landroid/widget/TextView;
    if-eqz v92, :cond_36

    .line 574
    const/16 v92, 0x1

    .line 575
    const/4 v4, 0x1

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 576
    const/4 v4, 0x1

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 577
    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 578
    const/4 v4, 0x0

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 585
    :goto_16
    const v4, 0x7f07018c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v88

    check-cast v88, Landroid/widget/TextView;

    .line 586
    .local v88, rottentomatoesPanel:Landroid/widget/TextView;
    if-eqz v92, :cond_37

    .line 587
    const/16 v92, 0x1

    .line 588
    const/4 v4, 0x1

    move-object/from16 v0, v88

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 589
    const/4 v4, 0x1

    move-object/from16 v0, v88

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 590
    move-object/from16 v0, v88

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591
    const/4 v4, 0x0

    move-object/from16 v0, v88

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 597
    :goto_17
    const v4, 0x7f07018d

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v58

    check-cast v58, Landroid/widget/TextView;

    .line 598
    .local v58, imdbTextView:Landroid/widget/TextView;
    if-eqz v92, :cond_38

    .line 599
    const/16 v92, 0x1

    .line 600
    const/4 v4, 0x1

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 601
    const/4 v4, 0x1

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 602
    move-object/from16 v0, v58

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 603
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 610
    :goto_18
    const v4, 0x7f07018a

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v65

    check-cast v65, Landroid/widget/TextView;

    .line 611
    .local v65, linksHeader:Landroid/widget/TextView;
    if-eqz v92, :cond_39

    .line 612
    const/4 v4, 0x0

    move-object/from16 v0, v65

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 618
    :goto_19
    const v4, 0x7f070178

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/RelativeLayout;

    .line 619
    .local v37, criticHeader:Landroid/widget/RelativeLayout;
    const v4, 0x7f07016c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v51

    check-cast v51, Landroid/widget/TextView;

    .line 620
    .local v51, friendsWantToSeeHeader:Landroid/widget/TextView;
    const v4, 0x7f07016d

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 621
    .local v12, friendsWantToSeeLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f07016e

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v71

    check-cast v71, Landroid/widget/LinearLayout;

    .line 622
    .local v71, moreFriendsWantToSeeLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f070172

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v49

    check-cast v49, Landroid/widget/TextView;

    .line 623
    .local v49, friendsRatingsHeader:Landroid/widget/TextView;
    const v4, 0x7f070173

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    .line 624
    .local v18, friendsRatingsLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f070174

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v70

    check-cast v70, Landroid/widget/LinearLayout;

    .line 625
    .local v70, moreFriendsRatingsLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f070181

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v105

    check-cast v105, Landroid/widget/RelativeLayout;

    .line 626
    .local v105, userReviewHeader:Landroid/widget/RelativeLayout;
    const v4, 0x7f070185

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/LinearLayout;

    .line 627
    .local v23, userReviewLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f070186

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v106

    check-cast v106, Landroid/widget/LinearLayout;

    .line 628
    .local v106, userReviewMoreLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f07017c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/LinearLayout;

    .line 629
    .local v29, criticLayout:Landroid/widget/LinearLayout;
    const v4, 0x7f07017d

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/LinearLayout;

    .line 630
    .local v38, criticLayoutMore:Landroid/widget/LinearLayout;
    const/4 v11, 0x0

    .line 632
    .local v11, iTagIndex:I
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v13

    .line 633
    .local v13, reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    if-eqz v13, :cond_c

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_c

    .line 634
    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-nez v4, :cond_b

    .line 635
    const/4 v4, 0x0

    invoke-virtual {v12, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 636
    const/16 v4, 0x8

    move-object/from16 v0, v71

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 637
    const/4 v4, 0x0

    move-object/from16 v0, v51

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 638
    const/4 v9, 0x0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v10

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v13}, Lnet/flixster/android/MovieDetails;->loadFriendReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 640
    const v4, 0x7f07016f

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v72

    check-cast v72, Landroid/widget/RelativeLayout;

    .line 641
    .local v72, moreReviewsLayout:Landroid/widget/RelativeLayout;
    const/16 v4, 0x8

    move-object/from16 v0, v72

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 642
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_3a

    .line 643
    const v4, 0x7f070171

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v102

    check-cast v102, Landroid/widget/TextView;

    .line 644
    .local v102, totalView:Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " friends total"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v102

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 645
    move-object/from16 v0, v72

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 646
    const/4 v4, 0x1

    move-object/from16 v0, v72

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 647
    const/4 v4, 0x0

    move-object/from16 v0, v72

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 652
    .end local v72           #moreReviewsLayout:Landroid/widget/RelativeLayout;
    .end local v102           #totalView:Landroid/widget/TextView;
    :cond_b
    :goto_1a
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/2addr v11, v4

    .line 655
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v13

    .line 656
    if-eqz v13, :cond_e

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_e

    .line 657
    invoke-virtual/range {v18 .. v18}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-nez v4, :cond_d

    .line 658
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 659
    const/16 v4, 0x8

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 660
    const/4 v4, 0x0

    move-object/from16 v0, v49

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 661
    const/4 v15, 0x0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v16

    move-object/from16 v14, p0

    move/from16 v17, v11

    move-object/from16 v19, v13

    invoke-direct/range {v14 .. v19}, Lnet/flixster/android/MovieDetails;->loadFriendReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 663
    const v4, 0x7f070175

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v72

    check-cast v72, Landroid/widget/RelativeLayout;

    .line 664
    .restart local v72       #moreReviewsLayout:Landroid/widget/RelativeLayout;
    const/16 v4, 0x8

    move-object/from16 v0, v72

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 665
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_3b

    .line 666
    const v4, 0x7f070177

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v102

    check-cast v102, Landroid/widget/TextView;

    .line 667
    .restart local v102       #totalView:Landroid/widget/TextView;
    const v4, 0x7f0c0081

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->getResourceString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v102

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 668
    move-object/from16 v0, v72

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 669
    const/4 v4, 0x1

    move-object/from16 v0, v72

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 670
    const/4 v4, 0x0

    move-object/from16 v0, v72

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 675
    .end local v72           #moreReviewsLayout:Landroid/widget/RelativeLayout;
    .end local v102           #totalView:Landroid/widget/TextView;
    :cond_d
    :goto_1b
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/2addr v11, v4

    .line 678
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getUserReviewsList()Ljava/util/ArrayList;

    move-result-object v24

    .line 679
    .local v24, reviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    if-eqz v24, :cond_3d

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_3d

    .line 680
    invoke-virtual/range {v23 .. v23}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-nez v4, :cond_f

    .line 681
    const/4 v4, 0x0

    move-object/from16 v0, v105

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 682
    const/4 v4, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 683
    const/16 v4, 0x8

    move-object/from16 v0, v106

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 685
    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 686
    const/16 v20, 0x0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v21

    move-object/from16 v19, p0

    move/from16 v22, v11

    invoke-direct/range {v19 .. v24}, Lnet/flixster/android/MovieDetails;->loadUserReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 689
    const v4, 0x7f070187

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v104

    check-cast v104, Landroid/widget/RelativeLayout;

    .line 690
    .local v104, userLoadMorePanel:Landroid/widget/RelativeLayout;
    const/16 v4, 0x8

    move-object/from16 v0, v104

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 692
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_3c

    .line 693
    const v4, 0x7f070189

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v66

    check-cast v66, Landroid/widget/TextView;

    .line 695
    .local v66, loadCount:Landroid/widget/TextView;
    const v4, 0x7f0c0081

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->getResourceString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 696
    move-object/from16 v0, v104

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 697
    const/4 v4, 0x1

    move-object/from16 v0, v104

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 698
    const/4 v4, 0x0

    move-object/from16 v0, v104

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 703
    .end local v66           #loadCount:Landroid/widget/TextView;
    .end local v104           #userLoadMorePanel:Landroid/widget/RelativeLayout;
    :cond_f
    :goto_1c
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/2addr v11, v4

    .line 710
    :goto_1d
    const v4, 0x7f07017e

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v39

    check-cast v39, Landroid/widget/RelativeLayout;

    .line 711
    .local v39, criticLoadMoreLayout:Landroid/widget/RelativeLayout;
    invoke-virtual/range {v29 .. v29}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-nez v4, :cond_10

    .line 712
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getCriticReviewsList()Ljava/util/ArrayList;

    move-result-object v30

    .line 713
    .local v30, criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    if-eqz v30, :cond_3f

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_3f

    .line 714
    const/4 v4, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 715
    const/16 v4, 0x8

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 716
    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 717
    const/16 v26, 0x0

    const/4 v4, 0x3

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v27

    move-object/from16 v25, p0

    move/from16 v28, v11

    invoke-direct/range {v25 .. v30}, Lnet/flixster/android/MovieDetails;->loadCriticReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 720
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x3

    if-le v4, v6, :cond_3e

    .line 721
    const v4, 0x7f070180

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v66

    check-cast v66, Landroid/widget/TextView;

    .line 722
    .restart local v66       #loadCount:Landroid/widget/TextView;
    const v4, 0x7f0c0081

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->getResourceString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 723
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v8

    .line 722
    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 724
    move-object/from16 v0, v39

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 725
    const/4 v4, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 726
    const/4 v4, 0x0

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 739
    .end local v30           #criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .end local v66           #loadCount:Landroid/widget/TextView;
    :cond_10
    :goto_1e
    const v4, 0x7f070152

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v103

    check-cast v103, Landroid/widget/TextView;

    .line 740
    .local v103, trailerPanel:Landroid/widget/TextView;
    const v4, 0x7f07014f

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v109

    check-cast v109, Landroid/widget/TextView;

    .line 741
    .local v109, watchNow:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->hasTrailer()Z

    move-result v4

    if-eqz v4, :cond_40

    invoke-virtual/range {v109 .. v109}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_40

    .line 742
    const/4 v4, 0x0

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 743
    const/4 v4, 0x1

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 744
    move-object/from16 v0, v103

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 749
    :goto_1f
    const v4, 0x7f070153

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v93

    check-cast v93, Landroid/widget/TextView;

    .line 750
    .local v93, showtimesPanel:Landroid/widget/TextView;
    const/4 v4, 0x1

    move-object/from16 v0, v93

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 751
    move-object/from16 v0, v93

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 754
    const v4, 0x7f070159

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v74

    check-cast v74, Landroid/widget/TextView;

    .line 755
    .local v74, netflixText:Landroid/widget/TextView;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_41

    .line 757
    move-object/from16 v0, v74

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 758
    const/4 v4, 0x1

    move-object/from16 v0, v74

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    goto/16 :goto_0

    .line 239
    .end local v11           #iTagIndex:I
    .end local v12           #friendsWantToSeeLayout:Landroid/widget/LinearLayout;
    .end local v13           #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .end local v18           #friendsRatingsLayout:Landroid/widget/LinearLayout;
    .end local v23           #userReviewLayout:Landroid/widget/LinearLayout;
    .end local v24           #reviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .end local v29           #criticLayout:Landroid/widget/LinearLayout;
    .end local v34           #actorsShort:Landroid/widget/TextView;
    .end local v37           #criticHeader:Landroid/widget/RelativeLayout;
    .end local v38           #criticLayoutMore:Landroid/widget/LinearLayout;
    .end local v39           #criticLoadMoreLayout:Landroid/widget/RelativeLayout;
    .end local v40           #criticsImage:Landroid/widget/ImageView;
    .end local v46           #flixsterPanel:Landroid/widget/TextView;
    .end local v47           #friendScore:Landroid/widget/TextView;
    .end local v49           #friendsRatingsHeader:Landroid/widget/TextView;
    .end local v51           #friendsWantToSeeHeader:Landroid/widget/TextView;
    .end local v52           #getGlue:Landroid/widget/TextView;
    .end local v58           #imdbTextView:Landroid/widget/TextView;
    .end local v59           #isReleased:Z
    .end local v61           #iv:Landroid/widget/ImageView;
    .end local v65           #linksHeader:Landroid/widget/TextView;
    .end local v67           #metaLine:Landroid/widget/TextView;
    .end local v70           #moreFriendsRatingsLayout:Landroid/widget/LinearLayout;
    .end local v71           #moreFriendsWantToSeeLayout:Landroid/widget/LinearLayout;
    .end local v74           #netflixText:Landroid/widget/TextView;
    .end local v76           #photoLayout:Landroid/widget/LinearLayout;
    .end local v78           #photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    .end local v81           #ratingLayout:Landroid/widget/RelativeLayout;
    .end local v83           #rentalExpiration:Landroid/widget/TextView;
    .end local v87           #rottenScore:Landroid/widget/TextView;
    .end local v88           #rottentomatoesPanel:Landroid/widget/TextView;
    .end local v89           #rtScoreline:Landroid/widget/TextView;
    .end local v91           #scoreTextView:Landroid/widget/TextView;
    .end local v92           #showMovieWebsitesHeaderFlag:Z
    .end local v93           #showtimesPanel:Landroid/widget/TextView;
    .end local v94           #showtimesTextView:Landroid/widget/TextView;
    .end local v100           #title:Ljava/lang/String;
    .end local v103           #trailerPanel:Landroid/widget/TextView;
    .end local v105           #userReviewHeader:Landroid/widget/RelativeLayout;
    .end local v106           #userReviewMoreLayout:Landroid/widget/LinearLayout;
    .end local v107           #viewPhotos:Landroid/widget/TextView;
    .end local v109           #watchNow:Landroid/widget/TextView;
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v100

    goto/16 :goto_1

    .line 250
    .restart local v81       #ratingLayout:Landroid/widget/RelativeLayout;
    .restart local v100       #title:Ljava/lang/String;
    :cond_12
    const v4, 0x7f070156

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v82

    check-cast v82, Landroid/widget/TextView;

    .line 251
    .local v82, ratingView:Landroid/widget/TextView;
    const/4 v4, 0x1

    move-object/from16 v0, v82

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 252
    move-object/from16 v0, v82

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovieDetails:Lnet/flixster/android/MovieDetails;

    const v6, 0x7f070157

    invoke-virtual {v4, v6}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v61

    check-cast v61, Landroid/widget/ImageView;

    .line 254
    .restart local v61       #iv:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    if-eqz v4, :cond_13

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    iget-wide v8, v4, Lnet/flixster/android/model/Review;->stars:D

    const-wide/16 v14, 0x0

    cmpl-double v4, v8, v14

    if-eqz v4, :cond_13

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_13

    .line 255
    const/4 v4, 0x0

    move-object/from16 v0, v61

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 256
    sget-object v4, Lnet/flixster/android/Flixster;->RATING_SMALL_R:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    iget-wide v8, v6, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v14, 0x4000

    mul-double/2addr v8, v14

    double-to-int v6, v8

    aget v4, v4, v6

    move-object/from16 v0, v61

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 257
    const v4, 0x7f0c003f

    move-object/from16 v0, v82

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 259
    :cond_13
    const/16 v4, 0x8

    move-object/from16 v0, v61

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 260
    const v4, 0x7f0c003e

    move-object/from16 v0, v82

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 269
    .end local v61           #iv:Landroid/widget/ImageView;
    .end local v82           #ratingView:Landroid/widget/TextView;
    .restart local v94       #showtimesTextView:Landroid/widget/TextView;
    :cond_14
    const/16 v4, 0x8

    move-object/from16 v0, v94

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 278
    .restart local v52       #getGlue:Landroid/widget/TextView;
    :cond_15
    const/16 v4, 0x8

    move-object/from16 v0, v52

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 289
    .restart local v76       #photoLayout:Landroid/widget/LinearLayout;
    .restart local v78       #photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    .restart local v107       #viewPhotos:Landroid/widget/TextView;
    :cond_16
    invoke-virtual/range {v76 .. v76}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 290
    const/16 v54, 0x0

    .restart local v54       #i:I
    :goto_20
    invoke-virtual/range {v78 .. v78}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v54

    if-ge v0, v4, :cond_17

    const/16 v4, 0xf

    move/from16 v0, v54

    if-lt v0, v4, :cond_19

    .line 314
    :cond_17
    const/4 v4, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 315
    new-instance v77, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0040

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v77

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 316
    .local v77, photoLinkBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget v0, v4, Lnet/flixster/android/model/Movie;->photoCount:I

    move/from16 v75, v0

    .line 317
    .local v75, photoCount:I
    const/16 v4, 0x32

    move/from16 v0, v75

    if-le v0, v4, :cond_18

    .line 318
    const/16 v75, 0x32

    .line 320
    :cond_18
    const-string v4, " ("

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v75

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ")"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    invoke-virtual/range {v77 .. v77}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v107

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    const/4 v4, 0x0

    move-object/from16 v0, v107

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 323
    const/4 v4, 0x1

    move-object/from16 v0, v107

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 324
    move-object/from16 v0, v107

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_5

    .line 291
    .end local v75           #photoCount:I
    .end local v77           #photoLinkBuilder:Ljava/lang/StringBuilder;
    :cond_19
    move-object/from16 v0, v78

    move/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Photo;

    .line 292
    .local v5, photo:Lnet/flixster/android/model/Photo;
    new-instance v7, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 293
    .local v7, photoView:Landroid/widget/ImageView;
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 294
    const v8, 0x7f0a0037

    .line 293
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    .line 294
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 295
    const v9, 0x7f0a0037

    .line 294
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    invoke-direct {v4, v6, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 293
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 296
    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f0a002c

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    .line 297
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a002b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    const/4 v9, 0x0

    .line 296
    invoke-virtual {v7, v4, v6, v8, v9}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 298
    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 299
    iget-object v4, v5, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_1b

    .line 300
    iget-object v4, v5, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 309
    :cond_1a
    :goto_21
    const/4 v4, 0x1

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 310
    const/4 v4, 0x1

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 311
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->photoClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 312
    move-object/from16 v0, v76

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 290
    add-int/lit8 v54, v54, 0x1

    goto/16 :goto_20

    .line 302
    :cond_1b
    const v4, 0x7f020151

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 303
    iget-object v4, v5, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    if-eqz v4, :cond_1a

    iget-object v4, v5, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    const-string v6, "http://"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 304
    new-instance v3, Lnet/flixster/android/model/ImageOrder;

    const/4 v4, 0x3

    .line 305
    iget-object v6, v5, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lnet/flixster/android/MovieDetails;->photoHandler:Landroid/os/Handler;

    .line 304
    invoke-direct/range {v3 .. v8}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 306
    .local v3, imageOrder:Lnet/flixster/android/model/ImageOrder;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lnet/flixster/android/MovieDetails;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_21

    .line 327
    .end local v3           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .end local v5           #photo:Lnet/flixster/android/model/Photo;
    .end local v7           #photoView:Landroid/widget/ImageView;
    .end local v54           #i:I
    :cond_1c
    const/16 v4, 0x8

    move-object/from16 v0, v76

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 328
    const/16 v4, 0x8

    move-object/from16 v0, v107

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 336
    .restart local v61       #iv:Landroid/widget/ImageView;
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    move-object/from16 v0, v61

    invoke-virtual {v4, v0}, Lnet/flixster/android/model/LockerRight;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v35

    .line 337
    .local v35, bitmap:Landroid/graphics/Bitmap;
    if-eqz v35, :cond_3

    .line 338
    move-object/from16 v0, v61

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_6

    .line 342
    .end local v35           #bitmap:Landroid/graphics/Bitmap;
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getThumbnailPoster()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1f

    .line 343
    const v4, 0x7f02014f

    move-object/from16 v0, v61

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_6

    .line 345
    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    move-object/from16 v0, v61

    invoke-virtual {v4, v0}, Lnet/flixster/android/model/Movie;->getThumbnailBackedProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v35

    .line 346
    .restart local v35       #bitmap:Landroid/graphics/Bitmap;
    if-eqz v35, :cond_3

    .line 347
    move-object/from16 v0, v61

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_6

    .line 359
    .end local v35           #bitmap:Landroid/graphics/Bitmap;
    .restart local v42       #directorView:Landroid/view/View;
    .restart local v43       #directorsHeader:Landroid/widget/TextView;
    .restart local v54       #i:I
    :cond_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    move/from16 v0, v54

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lnet/flixster/android/model/Actor;

    .line 360
    .local v41, director:Lnet/flixster/android/model/Actor;
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    move-object/from16 v2, v42

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/MovieDetails;->getActorView(Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;

    move-result-object v42

    .line 361
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->directorsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, v42

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 358
    add-int/lit8 v54, v54, 0x1

    goto/16 :goto_7

    .line 384
    .end local v41           #director:Lnet/flixster/android/model/Actor;
    .end local v42           #directorView:Landroid/view/View;
    .end local v43           #directorsHeader:Landroid/widget/TextView;
    .restart local v32       #actorView:Landroid/view/View;
    .restart local v33       #actorsHeader:Landroid/widget/TextView;
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v4, v4, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    move/from16 v0, v54

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lnet/flixster/android/model/Actor;

    .line 385
    .local v31, actor:Lnet/flixster/android/model/Actor;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/MovieDetails;->getActorView(Lnet/flixster/android/model/Actor;Landroid/view/View;)Landroid/view/View;

    move-result-object v32

    .line 386
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->actorsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 383
    add-int/lit8 v54, v54, 0x1

    goto/16 :goto_8

    .line 416
    .end local v31           #actor:Lnet/flixster/android/model/Actor;
    .end local v32           #actorView:Landroid/view/View;
    .end local v33           #actorsHeader:Landroid/widget/TextView;
    .restart local v62       #labelArray:[Ljava/lang/String;
    .restart local v63       #length:I
    .restart local v79       #propArray:[Ljava/lang/String;
    .restart local v84       #res:Landroid/content/res/Resources;
    .restart local v99       #textViewIdArray:[I
    :cond_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    aget-object v6, v79, v54

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 418
    .local v36, content:Ljava/lang/String;
    aget v4, v99, v54

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v98

    check-cast v98, Landroid/widget/TextView;

    .line 419
    .local v98, textView:Landroid/widget/TextView;
    if-eqz v36, :cond_23

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_23

    .line 422
    new-instance v4, Ljava/lang/StringBuilder;

    aget-object v6, v62, v54

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    move-object/from16 v0, v98

    invoke-virtual {v0, v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 423
    invoke-virtual/range {v98 .. v98}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v95

    check-cast v95, Landroid/text/Spannable;

    .line 424
    .local v95, spannableText:Landroid/text/Spannable;
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v6, 0x1

    invoke-direct {v4, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v6, 0x0

    aget-object v8, v62, v54

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    .line 425
    const/16 v9, 0x21

    .line 424
    move-object/from16 v0, v95

    invoke-interface {v0, v4, v6, v8, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 426
    const/4 v4, 0x0

    move-object/from16 v0, v98

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 415
    .end local v95           #spannableText:Landroid/text/Spannable;
    :goto_22
    add-int/lit8 v54, v54, 0x1

    goto/16 :goto_9

    .line 428
    :cond_23
    const/16 v4, 0x8

    move-object/from16 v0, v98

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_22

    .line 457
    .end local v36           #content:Ljava/lang/String;
    .end local v54           #i:I
    .end local v62           #labelArray:[Ljava/lang/String;
    .end local v63           #length:I
    .end local v79           #propArray:[Ljava/lang/String;
    .end local v84           #res:Landroid/content/res/Resources;
    .end local v98           #textView:Landroid/widget/TextView;
    .end local v99           #textViewIdArray:[I
    .restart local v40       #criticsImage:Landroid/widget/ImageView;
    .restart local v87       #rottenScore:Landroid/widget/TextView;
    .restart local v89       #rtScoreline:Landroid/widget/TextView;
    :cond_24
    const/16 v4, 0x8

    move-object/from16 v0, v89

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 458
    const/16 v4, 0x8

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 459
    const/16 v4, 0x8

    move-object/from16 v0, v87

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_a

    .line 463
    :cond_25
    const/16 v59, 0x1

    goto/16 :goto_b

    .line 468
    .restart local v55       #iFS:I
    .restart local v59       #isReleased:Z
    .restart local v91       #scoreTextView:Landroid/widget/TextView;
    :cond_26
    const/16 v60, 0x0

    goto/16 :goto_c

    .restart local v60       #isSpilled:Z
    .restart local v64       #liked:Ljava/lang/String;
    .restart local v110       #wts:Ljava/lang/String;
    :cond_27
    move-object/from16 v64, v110

    .line 473
    goto/16 :goto_d

    .line 474
    .end local v64           #liked:Ljava/lang/String;
    :cond_28
    if-eqz v60, :cond_29

    const v57, 0x7f0200ef

    goto/16 :goto_e

    .line 475
    :cond_29
    const v57, 0x7f0200ea

    goto/16 :goto_e

    .line 484
    .restart local v53       #headerIcon:Landroid/widget/ImageView;
    .restart local v57       #iconId:I
    .restart local v90       #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_2a
    if-eqz v60, :cond_2b

    const v4, 0x7f0200ee

    goto/16 :goto_f

    .line 485
    :cond_2b
    const v4, 0x7f0200e9

    goto/16 :goto_f

    .line 487
    .end local v53           #headerIcon:Landroid/widget/ImageView;
    .end local v55           #iFS:I
    .end local v57           #iconId:I
    .end local v60           #isSpilled:Z
    .end local v90           #scoreIcon:Landroid/graphics/drawable/Drawable;
    .end local v91           #scoreTextView:Landroid/widget/TextView;
    .end local v110           #wts:Ljava/lang/String;
    :cond_2c
    const v4, 0x7f070148

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v91

    check-cast v91, Landroid/widget/TextView;

    .line 488
    .restart local v91       #scoreTextView:Landroid/widget/TextView;
    const/16 v4, 0x8

    move-object/from16 v0, v91

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 489
    const v4, 0x7f070183

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v91

    .end local v91           #scoreTextView:Landroid/widget/TextView;
    check-cast v91, Landroid/widget/TextView;

    .line 490
    .restart local v91       #scoreTextView:Landroid/widget/TextView;
    const/4 v4, 0x4

    move-object/from16 v0, v91

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_10

    .line 504
    .restart local v47       #friendScore:Landroid/widget/TextView;
    .restart local v48       #friendsRatedCount:I
    .restart local v80       #ratingCountBuilder:Ljava/lang/StringBuilder;
    .restart local v85       #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_2d
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c008f

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v80

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_11

    .line 508
    .end local v48           #friendsRatedCount:I
    .end local v80           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v85           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_2e
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_30

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_30

    .line 509
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v50

    .line 510
    .local v50, friendsWantToSeeCount:I
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0200f7

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v108

    .line 511
    .local v108, wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v108 .. v108}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual/range {v108 .. v108}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    move-object/from16 v0, v108

    invoke-virtual {v0, v4, v6, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 512
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v47

    move-object/from16 v1, v108

    invoke-virtual {v0, v1, v4, v6, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 513
    const/16 v110, 0x0

    .line 514
    .restart local v110       #wts:Ljava/lang/String;
    const/4 v4, 0x1

    move/from16 v0, v50

    if-le v0, v4, :cond_2f

    .line 515
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0166

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v110

    .line 519
    :goto_23
    move-object/from16 v0, v47

    move-object/from16 v1, v110

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 520
    const/4 v4, 0x0

    move-object/from16 v0, v47

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_12

    .line 517
    :cond_2f
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0165

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v110

    goto :goto_23

    .line 522
    .end local v50           #friendsWantToSeeCount:I
    .end local v108           #wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    .end local v110           #wts:Ljava/lang/String;
    :cond_30
    const/16 v4, 0x8

    move-object/from16 v0, v47

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_12

    .line 530
    .restart local v34       #actorsShort:Landroid/widget/TextView;
    :cond_31
    const/16 v4, 0x8

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_13

    .line 538
    .restart local v67       #metaLine:Landroid/widget/TextView;
    :cond_32
    const/16 v4, 0x8

    move-object/from16 v0, v67

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_14

    .line 550
    .restart local v44       #expirationText:Ljava/lang/String;
    .restart local v83       #rentalExpiration:Landroid/widget/TextView;
    :cond_33
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lnet/flixster/android/model/LockerRight;->getRentalExpirationString(Z)Ljava/lang/String;

    move-result-object v44

    if-eqz v44, :cond_34

    .line 551
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0201b3

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v96

    .line 552
    .local v96, startWatchIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v83

    move-object/from16 v1, v96

    invoke-virtual {v0, v1, v4, v6, v8}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 553
    move-object/from16 v0, v83

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 554
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f09001e

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v83

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 555
    const/4 v4, 0x0

    move-object/from16 v0, v83

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_15

    .line 557
    .end local v96           #startWatchIcon:Landroid/graphics/drawable/Drawable;
    :cond_34
    const/16 v4, 0x8

    move-object/from16 v0, v83

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_15

    .line 560
    .end local v44           #expirationText:Ljava/lang/String;
    :cond_35
    const/16 v4, 0x8

    move-object/from16 v0, v83

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_15

    .line 582
    .restart local v46       #flixsterPanel:Landroid/widget/TextView;
    .restart local v92       #showMovieWebsitesHeaderFlag:Z
    :cond_36
    const/16 v4, 0x8

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_16

    .line 593
    .restart local v88       #rottentomatoesPanel:Landroid/widget/TextView;
    :cond_37
    const/16 v4, 0x8

    move-object/from16 v0, v88

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_17

    .line 605
    .restart local v58       #imdbTextView:Landroid/widget/TextView;
    :cond_38
    const/16 v4, 0x8

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_18

    .line 614
    .restart local v65       #linksHeader:Landroid/widget/TextView;
    :cond_39
    const/16 v4, 0x8

    move-object/from16 v0, v65

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_19

    .line 649
    .restart local v11       #iTagIndex:I
    .restart local v12       #friendsWantToSeeLayout:Landroid/widget/LinearLayout;
    .restart local v13       #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .restart local v18       #friendsRatingsLayout:Landroid/widget/LinearLayout;
    .restart local v23       #userReviewLayout:Landroid/widget/LinearLayout;
    .restart local v29       #criticLayout:Landroid/widget/LinearLayout;
    .restart local v37       #criticHeader:Landroid/widget/RelativeLayout;
    .restart local v38       #criticLayoutMore:Landroid/widget/LinearLayout;
    .restart local v49       #friendsRatingsHeader:Landroid/widget/TextView;
    .restart local v51       #friendsWantToSeeHeader:Landroid/widget/TextView;
    .restart local v70       #moreFriendsRatingsLayout:Landroid/widget/LinearLayout;
    .restart local v71       #moreFriendsWantToSeeLayout:Landroid/widget/LinearLayout;
    .restart local v72       #moreReviewsLayout:Landroid/widget/RelativeLayout;
    .restart local v105       #userReviewHeader:Landroid/widget/RelativeLayout;
    .restart local v106       #userReviewMoreLayout:Landroid/widget/LinearLayout;
    :cond_3a
    const/16 v4, 0x8

    move-object/from16 v0, v72

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1a

    .line 672
    :cond_3b
    const/16 v4, 0x8

    move-object/from16 v0, v72

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1b

    .line 700
    .end local v72           #moreReviewsLayout:Landroid/widget/RelativeLayout;
    .restart local v24       #reviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .restart local v104       #userLoadMorePanel:Landroid/widget/RelativeLayout;
    :cond_3c
    const/16 v4, 0x8

    move-object/from16 v0, v104

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1c

    .line 705
    .end local v104           #userLoadMorePanel:Landroid/widget/RelativeLayout;
    :cond_3d
    const/16 v4, 0x8

    move-object/from16 v0, v105

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 706
    const/16 v4, 0x8

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1d

    .line 728
    .restart local v30       #criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .restart local v39       #criticLoadMoreLayout:Landroid/widget/RelativeLayout;
    :cond_3e
    const/16 v4, 0x8

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1e

    .line 731
    :cond_3f
    const/16 v4, 0x8

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 732
    const/16 v4, 0x8

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 733
    const/16 v4, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 734
    const/16 v4, 0x8

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1e

    .line 746
    .end local v30           #criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    .restart local v103       #trailerPanel:Landroid/widget/TextView;
    .restart local v109       #watchNow:Landroid/widget/TextView;
    :cond_40
    const/16 v4, 0x8

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1f

    .line 760
    .restart local v74       #netflixText:Landroid/widget/TextView;
    .restart local v93       #showtimesPanel:Landroid/widget/TextView;
    :cond_41
    const/16 v4, 0x8

    move-object/from16 v0, v74

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 404
    :array_0
    .array-data 0x4
        0x5et 0x1t 0x7t 0x7ft
        0x5ft 0x1t 0x7t 0x7ft
        0x60t 0x1t 0x7t 0x7ft
        0x61t 0x1t 0x7t 0x7ft
        0x5dt 0x1t 0x7t 0x7ft
    .end array-data
.end method

.method private populateSeasonUi()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 766
    const v4, 0x7f070151

    invoke-virtual {p0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 767
    .local v3, viewEpisodes:Landroid/widget/TextView;
    iget-boolean v4, p0, Lnet/flixster/android/MovieDetails;->isSeason:Z

    if-eqz v4, :cond_1

    .line 768
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 769
    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 770
    const v4, 0x7f07015a

    invoke-virtual {p0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 771
    .local v2, synopsisHeader:Landroid/widget/TextView;
    const v4, 0x7f0c01a9

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 772
    const v4, 0x7f07014c

    invoke-virtual {p0, v4}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 773
    .local v0, metaLine:Landroid/widget/TextView;
    iget-object v4, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    check-cast v4, Lnet/flixster/android/model/Season;

    invoke-virtual {v4}, Lnet/flixster/android/model/Season;->getNetwork()Ljava/lang/String;

    move-result-object v1

    .line 774
    .local v1, network:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 775
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 776
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 783
    .end local v0           #metaLine:Landroid/widget/TextView;
    .end local v1           #network:Ljava/lang/String;
    .end local v2           #synopsisHeader:Landroid/widget/TextView;
    :goto_0
    return-void

    .line 778
    .restart local v0       #metaLine:Landroid/widget/TextView;
    .restart local v1       #network:Ljava/lang/String;
    .restart local v2       #synopsisHeader:Landroid/widget/TextView;
    :cond_0
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 781
    .end local v0           #metaLine:Landroid/widget/TextView;
    .end local v1           #network:Ljava/lang/String;
    .end local v2           #synopsisHeader:Landroid/widget/TextView;
    :cond_1
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private populateStreamingUi()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 787
    const v0, 0x7f07014f

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->watchNowPanel:Landroid/widget/TextView;

    .line 788
    const v0, 0x7f070150

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/flixster/android/view/DownloadPanel;

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    .line 790
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    invoke-interface {v0, v1}, Lcom/flixster/android/drm/PlaybackLogic;->isAssetPlayable(Lnet/flixster/android/model/LockerRight;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->watchNowPanel:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 792
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->watchNowPanel:Landroid/widget/TextView;

    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->watchNowClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 797
    :goto_0
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    invoke-interface {v0, v1}, Lcom/flixster/android/drm/PlaybackLogic;->isAssetDownloadable(Lnet/flixster/android/model/LockerRight;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 798
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    invoke-virtual {v0, v2}, Lcom/flixster/android/view/DownloadPanel;->setVisibility(I)V

    .line 799
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    iget-object v2, p0, Lnet/flixster/android/MovieDetails;->downloadClickListener:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lnet/flixster/android/MovieDetails;->downloadDeleteClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Lcom/flixster/android/view/DownloadPanel;->load(Lnet/flixster/android/model/LockerRight;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 803
    :goto_1
    return-void

    .line 794
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->watchNowPanel:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 801
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->downloadPanel:Lcom/flixster/android/view/DownloadPanel;

    invoke-virtual {v0, v3}, Lcom/flixster/android/view/DownloadPanel;->setVisibility(I)V

    goto :goto_1
.end method

.method private readExtras()V
    .locals 9

    .prologue
    .line 149
    invoke-virtual {p0}, Lnet/flixster/android/MovieDetails;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    iput-object v5, p0, Lnet/flixster/android/MovieDetails;->mExtras:Landroid/os/Bundle;

    .line 150
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mExtras:Landroid/os/Bundle;

    if-eqz v5, :cond_2

    .line 151
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mExtras:Landroid/os/Bundle;

    const-string v6, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    iput-wide v5, p0, Lnet/flixster/android/MovieDetails;->mMovieId:J

    .line 152
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mExtras:Landroid/os/Bundle;

    const-string v6, "KEY_IS_SEASON"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lnet/flixster/android/MovieDetails;->isSeason:Z

    .line 153
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mExtras:Landroid/os/Bundle;

    const-string v6, "KEY_RIGHT_ID"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 154
    .local v2, rightId:J
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mPlayTrailer:Ljava/lang/Boolean;

    if-nez v5, :cond_0

    .line 155
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mExtras:Landroid/os/Bundle;

    const-string v6, "net.flixster.PLAY_TRAILER"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lnet/flixster/android/MovieDetails;->mPlayTrailer:Ljava/lang/Boolean;

    .line 157
    :cond_0
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "MovieDetails.readExtras mMovieId:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v7, p0, Lnet/flixster/android/MovieDetails;->mMovieId:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-boolean v5, p0, Lnet/flixster/android/MovieDetails;->isSeason:Z

    if-eqz v5, :cond_3

    iget-wide v5, p0, Lnet/flixster/android/MovieDetails;->mMovieId:J

    invoke-static {v5, v6}, Lnet/flixster/android/data/MovieDao;->getSeason(J)Lnet/flixster/android/model/Season;

    move-result-object v5

    :goto_0
    iput-object v5, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    .line 160
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v4

    .line 161
    .local v4, user:Lnet/flixster/android/model/User;
    if-eqz v4, :cond_1

    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-lez v5, :cond_1

    .line 162
    invoke-virtual {v4, v2, v3}, Lnet/flixster/android/model/User;->getLockerRightFromRightId(J)Lnet/flixster/android/model/LockerRight;

    move-result-object v5

    iput-object v5, p0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    .line 165
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v5

    invoke-virtual {v5}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v1

    .line 166
    .local v1, p:Lnet/flixster/android/model/Property;
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    if-nez v5, :cond_4

    if-eqz v1, :cond_4

    iget-boolean v5, v1, Lnet/flixster/android/model/Property;->isGetGlueEnabled:Z

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    :goto_1
    iput-boolean v5, p0, Lnet/flixster/android/MovieDetails;->showGetGlue:Z

    .line 168
    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->populatePage()V

    .line 171
    .end local v1           #p:Lnet/flixster/android/model/Property;
    .end local v2           #rightId:J
    .end local v4           #user:Lnet/flixster/android/model/User;
    :cond_2
    new-instance v0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-direct {v0, v5}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;-><init>(Lnet/flixster/android/model/Movie;)V

    .line 172
    .local v0, dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    iput-object v0, v5, Lnet/flixster/android/ads/AdView;->dfpCustomTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    .line 173
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    iput-object v0, v5, Lnet/flixster/android/ads/AdView;->dfpCustomTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    .line 174
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mScrollAd:Lnet/flixster/android/ads/AdView;

    iput-object v0, v5, Lnet/flixster/android/ads/AdView;->dfpCustomTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    .line 176
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    iget-object v6, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v6}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lnet/flixster/android/ads/AdView;->mAdContextId:Ljava/lang/Long;

    .line 177
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    iget-object v6, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v6}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lnet/flixster/android/ads/AdView;->mAdContextId:Ljava/lang/Long;

    .line 178
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mScrollAd:Lnet/flixster/android/ads/AdView;

    iget-object v6, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v6}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lnet/flixster/android/ads/AdView;->mAdContextId:Ljava/lang/Long;

    .line 179
    return-void

    .line 159
    .end local v0           #dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;
    .restart local v2       #rightId:J
    :cond_3
    iget-wide v5, p0, Lnet/flixster/android/MovieDetails;->mMovieId:J

    invoke-static {v5, v6}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v5

    goto :goto_0

    .line 166
    .restart local v1       #p:Lnet/flixster/android/model/Property;
    .restart local v4       #user:Lnet/flixster/android/model/User;
    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected NetworkAttemptsCancelled()V
    .locals 2

    .prologue
    .line 1888
    const-string v0, "FlxMain"

    const-string v1, "MovieDetails.NetworkAttemptsCancelled()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1889
    return-void
.end method

.method protected createShareIntent()Landroid/content/Intent;
    .locals 7

    .prologue
    .line 1921
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1922
    .local v0, shareIntent:Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1923
    const-string v1, "android.intent.extra.TEXT"

    .line 1924
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->createMovieSummary(Lnet/flixster/android/model/Movie;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f0c01b1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->mobileUpsell()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lnet/flixster/android/MovieDetails;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1923
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1925
    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v4, 0x1

    .line 1565
    const/4 v3, -0x1

    if-ne p2, v3, :cond_2

    const/16 v3, 0x3f2

    if-ne p1, v3, :cond_2

    .line 1566
    iput-boolean v4, p0, Lnet/flixster/android/MovieDetails;->mFromRating:Z

    .line 1567
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1568
    .local v0, extras:Landroid/os/Bundle;
    iget-object v3, p0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    if-nez v3, :cond_0

    .line 1569
    new-instance v3, Lnet/flixster/android/model/Review;

    invoke-direct {v3}, Lnet/flixster/android/model/Review;-><init>()V

    iput-object v3, p0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    .line 1570
    iget-object v3, p0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    iput v4, v3, Lnet/flixster/android/model/Review;->type:I

    .line 1572
    :cond_0
    iget-object v3, p0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    const-string v4, "net.flixster.ReviewComment"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    .line 1573
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieDetails onActivityResult comment:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    iget-object v5, v5, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1574
    iget-object v3, p0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    const-string v4, "net.flixster.ReviewStars"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v3, Lnet/flixster/android/model/Review;->stars:D

    .line 1575
    const-string v3, "net.flixster.FeedUrl"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1576
    .local v1, feedUrl:Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 1577
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lnet/flixster/android/FeedPermissionPage;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1578
    .local v2, permissionIntent:Landroid/content/Intent;
    const-string v3, "net.flixster.FeedUrl"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1579
    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieDetails;->startActivity(Landroid/content/Intent;)V

    .line 1585
    .end local v0           #extras:Landroid/os/Bundle;
    .end local v1           #feedUrl:Ljava/lang/String;
    .end local v2           #permissionIntent:Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 1581
    :cond_2
    const/16 v3, 0x3e8

    if-ne p1, v3, :cond_1

    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->shouldShowPostitial()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1582
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->postitialShown()V

    .line 1583
    new-instance v3, Lcom/flixster/android/ads/DfpPostTrailerInterstitial;

    invoke-direct {v3}, Lcom/flixster/android/ads/DfpPostTrailerInterstitial;-><init>()V

    iget-object v4, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3, p0, v4}, Lcom/flixster/android/ads/DfpPostTrailerInterstitial;->loadAd(Landroid/app/Activity;Lnet/flixster/android/model/Movie;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 31
    .parameter "v"

    .prologue
    .line 1411
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-nez v2, :cond_1

    .line 1561
    :cond_0
    :goto_0
    return-void

    .line 1414
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 1418
    :sswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/criticreview"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1419
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Critic Review - "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "title"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1418
    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1421
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/Integer;

    .line 1422
    .local v29, reviewIndex:Ljava/lang/Integer;
    new-instance v27, Landroid/content/Intent;

    const-string v2, "REVIEW_PAGE"

    const/4 v3, 0x0

    const-class v4, Lnet/flixster/android/ReviewPage;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v2, v3, v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 1424
    .local v27, intent:Landroid/content/Intent;
    if-nez v29, :cond_2

    .line 1425
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    .line 1428
    :cond_2
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieDetails.onClick criticreview  reviewIndex:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1429
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mExtras:Landroid/os/Bundle;

    const-string v3, "REVIEW_INDEX"

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1430
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mExtras:Landroid/os/Bundle;

    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v8

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1431
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mExtras:Landroid/os/Bundle;

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/Intent;->replaceExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1433
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieDetails;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1437
    .end local v27           #intent:Landroid/content/Intent;
    .end local v29           #reviewIndex:Ljava/lang/Integer;
    :sswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/website/flixster"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1438
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "Info (Flixster) - "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "title"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1437
    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    new-instance v27, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "flixster"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1441
    .restart local v27       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieDetails;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1445
    .end local v27           #intent:Landroid/content/Intent;
    :sswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/website/rottentomatoes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1446
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "Website (RT) - "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "title"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1445
    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1448
    new-instance v27, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "rottentomatoes"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1449
    .restart local v27       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieDetails;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1453
    .end local v27           #intent:Landroid/content/Intent;
    :sswitch_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/website/imdb"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1454
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/MovieDetails;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "Info (IMDB) - "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "title"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1453
    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 1456
    new-instance v27, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "imdb"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1457
    .restart local v27       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieDetails;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1461
    .end local v27           #intent:Landroid/content/Intent;
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_0

    .line 1462
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const/16 v3, 0x3e8

    move-object/from16 v0, p0

    invoke-static {v2, v0, v3}, Lnet/flixster/android/Starter;->launchTrailerForResult(Lnet/flixster/android/model/Movie;Landroid/app/Activity;I)V

    goto/16 :goto_0

    .line 1466
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_0

    .line 1468
    new-instance v26, Landroid/content/Intent;

    const-string v2, "SHOWTIMES"

    const/4 v3, 0x0

    const-class v4, Lnet/flixster/android/ShowtimesPage;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v2, v3, v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 1469
    .local v26, i:Landroid/content/Intent;
    const-string v2, "net.flixster.android.EXTRA_MOVIE_ID"

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1470
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieDetails;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1474
    .end local v26           #i:Landroid/content/Intent;
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_0

    .line 1475
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/getglue"

    const/4 v4, 0x0

    const-string v8, "GetGlue"

    const-string v9, "Click"

    invoke-interface {v2, v3, v4, v8, v9}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1476
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-static {v2}, Lnet/flixster/android/data/ApiBuilder;->getGlue(Lnet/flixster/android/model/Movie;)Ljava/lang/String;

    move-result-object v30

    .line 1477
    .local v30, url:Ljava/lang/String;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieDetails.onClick GetGlue "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    invoke-static/range {v30 .. v31}, Lnet/flixster/android/Starter;->launchBrowser(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1482
    .end local v30           #url:Ljava/lang/String;
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_0

    .line 1483
    new-instance v28, Landroid/content/Intent;

    const-string v2, "MOVIE_ID"

    const/4 v3, 0x0

    const-class v4, Lnet/flixster/android/RateMoviePage;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v0, v2, v3, v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 1485
    .local v28, rateMovie:Landroid/content/Intent;
    const-string v2, "net.flixster.android.EXTRA_MOVIE_ID"

    move-object/from16 v0, p0

    iget-wide v3, v0, Lnet/flixster/android/MovieDetails;->mMovieId:J

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1486
    const-string v2, "title"

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "title"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1487
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    if-eqz v2, :cond_3

    .line 1488
    const-string v2, "net.flixster.ReviewComment"

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    iget-object v3, v3, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1489
    const-string v2, "net.flixster.ReviewStars"

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mReview:Lnet/flixster/android/model/Review;

    iget-wide v3, v3, Lnet/flixster/android/model/Review;->stars:D

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1491
    :cond_3
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1492
    const-string v2, "net.flixster.IsConnected"

    const/4 v3, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1495
    :cond_4
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieDetails.onClick() launch RateMoviePage mMovieId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v8, v0, Lnet/flixster/android/MovieDetails;->mMovieId:J

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1497
    const/16 v2, 0x3f2

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Lnet/flixster/android/MovieDetails;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1502
    .end local v28           #rateMovie:Landroid/content/Intent;
    :sswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_0

    .line 1503
    new-instance v25, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/MovieGalleryPage;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1504
    .local v25, galleryIntent:Landroid/content/Intent;
    const-string v2, "net.flixster.android.EXTRA_MOVIE_ID"

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v3

    move-object/from16 v0, v25

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1505
    const-string v2, "title"

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "title"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1506
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieDetails;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1510
    .end local v25           #galleryIntent:Landroid/content/Intent;
    :sswitch_9
    const v2, 0x7f07017d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 1511
    .local v6, criticPanel:Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getCriticReviewsList()Ljava/util/ArrayList;

    move-result-object v7

    .line 1512
    .local v7, criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeListSize()I

    move-result v2

    .line 1513
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsListSize()I

    move-result v3

    .line 1512
    add-int/2addr v2, v3

    .line 1513
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getUserReviewsList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1512
    add-int/2addr v2, v3

    add-int/lit8 v5, v2, 0x3

    .line 1515
    .local v5, moreCriticReviewStart:I
    const/4 v3, 0x3

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/MovieDetails;->loadCriticReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 1517
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1518
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1521
    .end local v5           #moreCriticReviewStart:I
    .end local v6           #criticPanel:Landroid/widget/LinearLayout;
    .end local v7           #criticReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    :sswitch_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeListSize()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsListSize()I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v11, v2, 0x3

    .line 1523
    .local v11, moreUserReviewStart:I
    const v2, 0x7f070186

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 1524
    .local v12, userPanel:Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getUserReviewsList()Ljava/util/ArrayList;

    move-result-object v13

    .line 1525
    .local v13, userReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    const/4 v9, 0x3

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v10

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v13}, Lnet/flixster/android/MovieDetails;->loadUserReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 1527
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1528
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1531
    .end local v11           #moreUserReviewStart:I
    .end local v12           #userPanel:Landroid/widget/LinearLayout;
    .end local v13           #userReviewList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    :sswitch_b
    const/16 v17, 0x3

    .line 1532
    .local v17, moreFriendWantToSeeStart:I
    const v2, 0x7f07016e

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    .line 1533
    .local v18, moreWantToSeeLayout:Landroid/widget/LinearLayout;
    const/4 v15, 0x3

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeListSize()I

    move-result v16

    .line 1534
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v14, p0

    .line 1533
    invoke-direct/range {v14 .. v19}, Lnet/flixster/android/MovieDetails;->loadFriendReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 1535
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1536
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1539
    .end local v17           #moreFriendWantToSeeStart:I
    .end local v18           #moreWantToSeeLayout:Landroid/widget/LinearLayout;
    :sswitch_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeListSize()I

    move-result v2

    add-int/lit8 v22, v2, 0x3

    .line 1540
    .local v22, moreFriendRatedStart:I
    const v2, 0x7f070174

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/LinearLayout;

    .line 1541
    .local v23, moreRatingsLayout:Landroid/widget/LinearLayout;
    const/16 v20, 0x3

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsListSize()I

    move-result v21

    .line 1542
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v19, p0

    .line 1541
    invoke-direct/range {v19 .. v24}, Lnet/flixster/android/MovieDetails;->loadFriendReviews(IIILandroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    .line 1543
    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1544
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1548
    .end local v22           #moreFriendRatedStart:I
    .end local v23           #moreRatingsLayout:Landroid/widget/LinearLayout;
    :sswitch_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I

    if-eqz v2, :cond_0

    .line 1549
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lnet/flixster/android/MovieDetails;->showDialog(I)V

    goto/16 :goto_0

    .line 1553
    :sswitch_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    if-nez v2, :cond_5

    .line 1554
    move-object/from16 v0, p0

    iget-wide v2, v0, Lnet/flixster/android/MovieDetails;->mMovieId:J

    move-object/from16 v0, p0

    invoke-static {v2, v3, v0}, Lnet/flixster/android/Starter;->launchEpisodes(JLandroid/content/Context;)V

    goto/16 :goto_0

    .line 1556
    :cond_5
    move-object/from16 v0, p0

    iget-wide v2, v0, Lnet/flixster/android/MovieDetails;->mMovieId:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    iget-wide v8, v4, Lnet/flixster/android/model/LockerRight;->rightId:J

    move-object/from16 v0, p0

    invoke-static {v2, v3, v8, v9, v0}, Lnet/flixster/android/Starter;->launchEpisodes(JJLandroid/content/Context;)V

    goto/16 :goto_0

    .line 1414
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f030022 -> :sswitch_0
        0x7f070151 -> :sswitch_e
        0x7f070152 -> :sswitch_4
        0x7f070153 -> :sswitch_5
        0x7f070154 -> :sswitch_8
        0x7f070156 -> :sswitch_7
        0x7f070158 -> :sswitch_6
        0x7f070159 -> :sswitch_d
        0x7f070162 -> :sswitch_8
        0x7f07016f -> :sswitch_b
        0x7f070175 -> :sswitch_c
        0x7f07017e -> :sswitch_9
        0x7f070187 -> :sswitch_a
        0x7f07018b -> :sswitch_1
        0x7f07018c -> :sswitch_2
        0x7f07018d -> :sswitch_3
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 137
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 138
    const v0, 0x7f03005b

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieDetails;->setContentView(I)V

    .line 139
    invoke-virtual {p0}, Lnet/flixster/android/MovieDetails;->createActionBar()V

    .line 141
    const v0, 0x7f070141

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/AdView;

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    .line 142
    const v0, 0x7f07018e

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/AdView;

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    .line 143
    const v0, 0x7f07002a

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/AdView;

    iput-object v0, p0, Lnet/flixster/android/MovieDetails;->mScrollAd:Lnet/flixster/android/ads/AdView;

    .line 145
    iput-object p0, p0, Lnet/flixster/android/MovieDetails;->mMovieDetails:Lnet/flixster/android/MovieDetails;

    .line 146
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 8
    .parameter "dialog"

    .prologue
    const/4 v5, 0x0

    .line 1793
    packed-switch p1, :pswitch_data_0

    .line 1884
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v5

    :cond_0
    :goto_0
    return-object v5

    .line 1795
    :pswitch_0
    invoke-virtual {p0}, Lnet/flixster/android/MovieDetails;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e000b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 1796
    .local v4, netflixMenuReference:[Ljava/lang/String;
    iget-object v6, p0, Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I

    if-eqz v6, :cond_0

    .line 1799
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I

    array-length v1, v5

    .line 1800
    .local v1, menuLength:I
    new-array v3, v1, [Ljava/lang/String;

    .line 1801
    .local v3, netflixMenuArray:[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 1805
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0c00ce

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 1806
    new-instance v6, Lnet/flixster/android/MovieDetails$27;

    invoke-direct {v6, p0}, Lnet/flixster/android/MovieDetails$27;-><init>(Lnet/flixster/android/MovieDetails;)V

    invoke-virtual {v5, v3, v6}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 1874
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto :goto_0

    .line 1802
    :cond_1
    iget-object v5, p0, Lnet/flixster/android/MovieDetails;->mNetflixMenuToAction:[I

    aget v5, v5, v0

    aget-object v5, v4, v5

    aput-object v5, v3, v0

    .line 1801
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1876
    .end local v0           #i:I
    .end local v1           #menuLength:I
    .end local v3           #netflixMenuArray:[Ljava/lang/String;
    .end local v4           #netflixMenuReference:[Ljava/lang/String;
    :pswitch_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1877
    .local v2, netflixAlertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v6, "Netflix Error"

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1878
    const-string v6, "You can\'t add movies to your Netflix Queue because it is full."

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1879
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1880
    const-string v6, "OK"

    invoke-virtual {v2, v6, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1882
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto :goto_0

    .line 1793
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mScrollAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 220
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onDestroy()V

    .line 221
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keycode"
    .parameter "event"

    .prologue
    .line 1393
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 1394
    const-string v0, "FlxMain"

    const-string v1, "MovieDetails.onKeyDown - call finish"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1395
    invoke-virtual {p0}, Lnet/flixster/android/MovieDetails;->finish()V

    .line 1397
    :cond_0
    invoke-super {p0, p1, p2}, Lnet/flixster/android/FlixsterActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 5
    .parameter "item"

    .prologue
    .line 1909
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1916
    :goto_0
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    return v0

    .line 1911
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/info"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1912
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "Info - "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "ActionBar"

    .line 1913
    const-string v4, "Share"

    .line 1911
    invoke-interface {v1, v2, v0, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1912
    :cond_0
    const-string v0, ""

    goto :goto_1

    .line 1909
    nop

    :pswitch_data_0
    .packed-switch 0x7f070303
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 183
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 184
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-nez v1, :cond_0

    .line 185
    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->readExtras()V

    .line 188
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v1, :cond_3

    .line 189
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v2, "title"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, title:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieDetails;->setActionBarTitle(Ljava/lang/String;)V

    .line 191
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "Info - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-boolean v1, p0, Lnet/flixster/android/MovieDetails;->showGetGlue:Z

    if-eqz v1, :cond_1

    .line 194
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/getglue"

    const/4 v3, 0x0

    const-string v4, "GetGlue"

    const-string v5, "Impression"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    .end local v0           #title:Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lnet/flixster/android/MovieDetails;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/flixster/android/widget/WidgetProvider;->isOriginatedFromWidget(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 202
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/info"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "Info - "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v1}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "WidgetEntrance"

    .line 204
    const-string v5, "Movie"

    .line 202
    invoke-interface {v2, v3, v1, v4, v5}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_2
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->right:Lnet/flixster/android/model/LockerRight;

    if-nez v1, :cond_5

    .line 208
    invoke-virtual {p0}, Lnet/flixster/android/MovieDetails;->refreshSticky()V

    .line 209
    iget-object v1, p0, Lnet/flixster/android/MovieDetails;->mScrollAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v1}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 214
    :goto_2
    const-wide/16 v1, 0x64

    invoke-direct {p0, v1, v2}, Lnet/flixster/android/MovieDetails;->ScheduleLoadMovieTask(J)V

    .line 215
    return-void

    .line 198
    :cond_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->getGaTagPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lnet/flixster/android/MovieDetails;->getGaTitlePrefix()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "Info - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 203
    :cond_4
    const-string v1, ""

    goto :goto_1

    .line 211
    :cond_5
    const-string v1, "FlxMain"

    const-string v2, "MovieDetails.onResume movie in user\'s collection, ads disabled"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "outState"

    .prologue
    .line 1787
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1788
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lnet/flixster/android/MovieDetails;->removeDialog(I)V

    .line 1789
    return-void
.end method

.method protected retryAction()V
    .locals 3

    .prologue
    .line 1331
    iget-object v0, p0, Lnet/flixster/android/MovieDetails;->mMovieDetails:Lnet/flixster/android/MovieDetails;

    const-wide/16 v1, 0x3e8

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/MovieDetails;->ScheduleLoadMovieTask(J)V

    .line 1332
    return-void
.end method
