.class Lnet/flixster/android/MovieGalleryPage$3;
.super Ljava/lang/Object;
.source "MovieGalleryPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieGalleryPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieGalleryPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieGalleryPage$3;->this$0:Lnet/flixster/android/MovieGalleryPage;

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "view"

    .prologue
    .line 165
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Photo;

    .line 166
    .local v2, photo:Lnet/flixster/android/model/Photo;
    if-eqz v2, :cond_1

    .line 167
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage$3;->this$0:Lnet/flixster/android/MovieGalleryPage;

    #getter for: Lnet/flixster/android/MovieGalleryPage;->photos:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/flixster/android/MovieGalleryPage;->access$2(Lnet/flixster/android/MovieGalleryPage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 168
    .local v0, index:I
    if-gez v0, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 171
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "PHOTO"

    const/4 v4, 0x0

    iget-object v5, p0, Lnet/flixster/android/MovieGalleryPage$3;->this$0:Lnet/flixster/android/MovieGalleryPage;

    invoke-virtual {v5}, Lnet/flixster/android/MovieGalleryPage;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lnet/flixster/android/ScrollGalleryPage;

    invoke-direct {v1, v3, v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 172
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    iget-object v4, p0, Lnet/flixster/android/MovieGalleryPage$3;->this$0:Lnet/flixster/android/MovieGalleryPage;

    #getter for: Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lnet/flixster/android/MovieGalleryPage;->access$0(Lnet/flixster/android/MovieGalleryPage;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 173
    const-string v3, "PHOTO_INDEX"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    const-string v3, "PHOTO_COUNT"

    iget-object v4, p0, Lnet/flixster/android/MovieGalleryPage$3;->this$0:Lnet/flixster/android/MovieGalleryPage;

    #getter for: Lnet/flixster/android/MovieGalleryPage;->photos:Ljava/util/ArrayList;
    invoke-static {v4}, Lnet/flixster/android/MovieGalleryPage;->access$2(Lnet/flixster/android/MovieGalleryPage;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 175
    const-string v3, "title"

    iget-object v4, p0, Lnet/flixster/android/MovieGalleryPage$3;->this$0:Lnet/flixster/android/MovieGalleryPage;

    #getter for: Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v4}, Lnet/flixster/android/MovieGalleryPage;->access$0(Lnet/flixster/android/MovieGalleryPage;)Lnet/flixster/android/model/Movie;

    move-result-object v4

    const-string v5, "title"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage$3;->this$0:Lnet/flixster/android/MovieGalleryPage;

    invoke-virtual {v3, v1}, Lnet/flixster/android/MovieGalleryPage;->startActivity(Landroid/content/Intent;)V

    .line 178
    .end local v0           #index:I
    .end local v1           #intent:Landroid/content/Intent;
    :cond_1
    return-void
.end method
