.class Lnet/flixster/android/MovieGalleryPage$5;
.super Ljava/util/TimerTask;
.source "MovieGalleryPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/MovieGalleryPage;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieGalleryPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieGalleryPage$5;->this$0:Lnet/flixster/android/MovieGalleryPage;

    .line 92
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 95
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage$5;->this$0:Lnet/flixster/android/MovieGalleryPage;

    #getter for: Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v3}, Lnet/flixster/android/MovieGalleryPage;->access$0(Lnet/flixster/android/MovieGalleryPage;)Lnet/flixster/android/model/Movie;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v1

    .line 97
    .local v1, movieId:J
    :try_start_0
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage$5;->this$0:Lnet/flixster/android/MovieGalleryPage;

    #getter for: Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v3}, Lnet/flixster/android/MovieGalleryPage;->access$0(Lnet/flixster/android/MovieGalleryPage;)Lnet/flixster/android/model/Movie;

    move-result-object v3

    invoke-static {v1, v2}, Lnet/flixster/android/data/PhotoDao;->getMoviePhotos(J)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v3, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage$5;->this$0:Lnet/flixster/android/MovieGalleryPage;

    #getter for: Lnet/flixster/android/MovieGalleryPage;->updateHandler:Landroid/os/Handler;
    invoke-static {v3}, Lnet/flixster/android/MovieGalleryPage;->access$4(Lnet/flixster/android/MovieGalleryPage;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 104
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v3, "FlxMain"

    const-string v4, "MovieGalleryPage.scheduleUpdatePageTask:failed to get photos"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 100
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage$5;->this$0:Lnet/flixster/android/MovieGalleryPage;

    #getter for: Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v3}, Lnet/flixster/android/MovieGalleryPage;->access$0(Lnet/flixster/android/MovieGalleryPage;)Lnet/flixster/android/model/Movie;

    move-result-object v3

    const/4 v4, 0x0

    iput-object v4, v3, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    goto :goto_0
.end method
