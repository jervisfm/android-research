.class public Lnet/flixster/android/MovieGalleryPage;
.super Lnet/flixster/android/FlixsterActivity;
.source "MovieGalleryPage.java"


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field private static final MAX_PHOTOS:I = 0x32


# instance fields
.field private mDefaultAd:Lnet/flixster/android/ads/AdView;

.field private mMovie:Lnet/flixster/android/model/Movie;

.field private mPhotosListAdapter:Lnet/flixster/android/PhotosListAdapter;

.field private photoClickListener:Landroid/view/View$OnClickListener;

.field private photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private photos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 111
    new-instance v0, Lnet/flixster/android/MovieGalleryPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieGalleryPage$1;-><init>(Lnet/flixster/android/MovieGalleryPage;)V

    iput-object v0, p0, Lnet/flixster/android/MovieGalleryPage;->updateHandler:Landroid/os/Handler;

    .line 157
    new-instance v0, Lnet/flixster/android/MovieGalleryPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieGalleryPage$2;-><init>(Lnet/flixster/android/MovieGalleryPage;)V

    iput-object v0, p0, Lnet/flixster/android/MovieGalleryPage;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 163
    new-instance v0, Lnet/flixster/android/MovieGalleryPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieGalleryPage$3;-><init>(Lnet/flixster/android/MovieGalleryPage;)V

    iput-object v0, p0, Lnet/flixster/android/MovieGalleryPage;->photoClickListener:Landroid/view/View$OnClickListener;

    .line 27
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/MovieGalleryPage;)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/MovieGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 126
    invoke-direct {p0}, Lnet/flixster/android/MovieGalleryPage;->updatePage()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/MovieGalleryPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lnet/flixster/android/MovieGalleryPage;->photos:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/MovieGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    invoke-direct {p0}, Lnet/flixster/android/MovieGalleryPage;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/MovieGalleryPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lnet/flixster/android/MovieGalleryPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 92
    new-instance v0, Lnet/flixster/android/MovieGalleryPage$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieGalleryPage$5;-><init>(Lnet/flixster/android/MovieGalleryPage;)V

    .line 106
    .local v0, updatePageTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/MovieGalleryPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lnet/flixster/android/MovieGalleryPage;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 109
    :cond_0
    return-void
.end method

.method private updatePage()V
    .locals 6

    .prologue
    .line 127
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v3, :cond_2

    .line 128
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 129
    new-instance v3, Ljava/lang/StringBuilder;

    const v4, 0x7f0c003d

    invoke-virtual {p0, v4}, Lnet/flixster/android/MovieGalleryPage;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lnet/flixster/android/MovieGalleryPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 132
    :cond_0
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v3, v3, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 133
    const v3, 0x7f070138

    invoke-virtual {p0, v3}, Lnet/flixster/android/MovieGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 134
    .local v0, galleryView:Landroid/widget/GridView;
    invoke-virtual {v0}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    if-nez v3, :cond_2

    .line 135
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->photos:Ljava/util/ArrayList;

    .line 136
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v3, v3, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    const/16 v3, 0x32

    if-lt v1, v3, :cond_3

    .line 140
    :cond_1
    new-instance v3, Lnet/flixster/android/PhotosListAdapter;

    iget-object v4, p0, Lnet/flixster/android/MovieGalleryPage;->photos:Ljava/util/ArrayList;

    iget-object v5, p0, Lnet/flixster/android/MovieGalleryPage;->photoClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v3, p0, v4, v5}, Lnet/flixster/android/PhotosListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;)V

    iput-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->mPhotosListAdapter:Lnet/flixster/android/PhotosListAdapter;

    .line 141
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->mPhotosListAdapter:Lnet/flixster/android/PhotosListAdapter;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 142
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 143
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setClickable(Z)V

    .line 155
    .end local v0           #galleryView:Landroid/widget/GridView;
    .end local v1           #i:I
    :cond_2
    return-void

    .line 137
    .restart local v0       #galleryView:Landroid/widget/GridView;
    .restart local v1       #i:I
    :cond_3
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v3, v3, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 138
    .local v2, photo:Ljava/lang/Object;
    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedState"

    .prologue
    .line 39
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v4, 0x7f030058

    invoke-virtual {p0, v4}, Lnet/flixster/android/MovieGalleryPage;->setContentView(I)V

    .line 41
    invoke-virtual {p0}, Lnet/flixster/android/MovieGalleryPage;->createActionBar()V

    .line 43
    const v4, 0x7f07002a

    invoke-virtual {p0, v4}, Lnet/flixster/android/MovieGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/ads/AdView;

    iput-object v4, p0, Lnet/flixster/android/MovieGalleryPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    .line 44
    invoke-virtual {p0}, Lnet/flixster/android/MovieGalleryPage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 45
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 46
    const-string v4, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 47
    .local v1, movieId:J
    invoke-static {v1, v2}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v4

    iput-object v4, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    .line 48
    iget-object v4, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v5, "title"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 49
    .local v3, movieTitle:Ljava/lang/String;
    if-nez v3, :cond_0

    .line 50
    const-string v4, "title"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 51
    iget-object v4, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v5, "title"

    invoke-virtual {v4, v5, v3}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .end local v1           #movieId:J
    .end local v3           #movieTitle:Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "dialogId"

    .prologue
    .line 72
    packed-switch p1, :pswitch_data_0

    .line 87
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    .line 74
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 75
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v1, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 76
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 77
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 78
    const-string v1, "Retry"

    new-instance v2, Lnet/flixster/android/MovieGalleryPage$4;

    invoke-direct {v2, p0}, Lnet/flixster/android/MovieGalleryPage$4;-><init>(Lnet/flixster/android/MovieGalleryPage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 83
    invoke-virtual {p0}, Lnet/flixster/android/MovieGalleryPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lnet/flixster/android/MovieGalleryPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 67
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onDestroy()V

    .line 68
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 58
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 59
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/photo/gallery/movie"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Photo Gallery - Movie - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/MovieGalleryPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lnet/flixster/android/MovieGalleryPage;->scheduleUpdatePageTask()V

    .line 61
    iget-object v0, p0, Lnet/flixster/android/MovieGalleryPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 62
    return-void
.end method
