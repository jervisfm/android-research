.class public Lnet/flixster/android/MovieGridViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "MovieGridViewAdapter.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final movieType:I

.field private final movies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/List;I)V
    .locals 0
    .parameter "context"
    .parameter
    .parameter "movieType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p2, movies:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 17
    iput-object p1, p0, Lnet/flixster/android/MovieGridViewAdapter;->context:Landroid/content/Context;

    .line 18
    iput-object p2, p0, Lnet/flixster/android/MovieGridViewAdapter;->movies:Ljava/util/List;

    .line 19
    iput p3, p0, Lnet/flixster/android/MovieGridViewAdapter;->movieType:I

    .line 20
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lnet/flixster/android/MovieGridViewAdapter;->movies:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 38
    iget-object v0, p0, Lnet/flixster/android/MovieGridViewAdapter;->movies:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 42
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 23
    if-eqz p2, :cond_0

    instance-of v0, p2, Lnet/flixster/android/MovieCollectionItem;

    if-nez v0, :cond_1

    .line 24
    :cond_0
    new-instance p2, Lnet/flixster/android/MovieCollectionItem;

    .end local p2
    iget-object v0, p0, Lnet/flixster/android/MovieGridViewAdapter;->context:Landroid/content/Context;

    invoke-direct {p2, v0}, Lnet/flixster/android/MovieCollectionItem;-><init>(Landroid/content/Context;)V

    .restart local p2
    :goto_0
    move-object v0, p2

    .line 29
    check-cast v0, Lnet/flixster/android/MovieCollectionItem;

    iget-object v1, p0, Lnet/flixster/android/MovieGridViewAdapter;->movies:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    iget v2, p0, Lnet/flixster/android/MovieGridViewAdapter;->movieType:I

    invoke-virtual {v0, v1, v2}, Lnet/flixster/android/MovieCollectionItem;->load(Lnet/flixster/android/model/Movie;I)V

    .line 30
    return-object p2

    :cond_1
    move-object v0, p2

    .line 26
    check-cast v0, Lnet/flixster/android/MovieCollectionItem;

    invoke-virtual {v0}, Lnet/flixster/android/MovieCollectionItem;->reset()V

    goto :goto_0
.end method
