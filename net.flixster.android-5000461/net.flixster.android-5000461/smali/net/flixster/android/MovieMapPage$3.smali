.class Lnet/flixster/android/MovieMapPage$3;
.super Landroid/os/Handler;
.source "MovieMapPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieMapPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieMapPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieMapPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieMapPage$3;->this$0:Lnet/flixster/android/MovieMapPage;

    .line 238
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "message"

    .prologue
    .line 242
    const-string v0, "FlxMain"

    const-string v1, "MyWantToSeePage.updateHandler"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage$3;->this$0:Lnet/flixster/android/MovieMapPage;

    #getter for: Lnet/flixster/android/MovieMapPage;->theaters:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/MovieMapPage;->access$1(Lnet/flixster/android/MovieMapPage;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage$3;->this$0:Lnet/flixster/android/MovieMapPage;

    #calls: Lnet/flixster/android/MovieMapPage;->updatePage()V
    invoke-static {v0}, Lnet/flixster/android/MovieMapPage;->access$2(Lnet/flixster/android/MovieMapPage;)V

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage$3;->this$0:Lnet/flixster/android/MovieMapPage;

    invoke-virtual {v0}, Lnet/flixster/android/MovieMapPage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage$3;->this$0:Lnet/flixster/android/MovieMapPage;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieMapPage;->showDialog(I)V

    goto :goto_0
.end method
