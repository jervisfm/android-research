.class Lnet/flixster/android/MovieMapPage$7;
.super Ljava/lang/Object;
.source "MovieMapPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/MovieMapPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieMapPage;

.field private final synthetic val$zipEntryView:Landroid/view/View;


# direct methods
.method constructor <init>(Lnet/flixster/android/MovieMapPage;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MovieMapPage$7;->this$0:Lnet/flixster/android/MovieMapPage;

    iput-object p2, p0, Lnet/flixster/android/MovieMapPage$7;->val$zipEntryView:Landroid/view/View;

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter "dialog"
    .parameter "whichButton"

    .prologue
    .line 161
    iget-object v2, p0, Lnet/flixster/android/MovieMapPage$7;->val$zipEntryView:Landroid/view/View;

    const v3, 0x7f0702f9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 162
    .local v1, editText:Landroid/widget/EditText;
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/util/PostalCodeUtils;->parseZipcodeForShowtimes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, cleanZip:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 164
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setUserLocation(Ljava/lang/String;)V

    .line 165
    const/4 v2, 0x0

    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->setUseLocationServiceFlag(Z)V

    .line 166
    iget-object v2, p0, Lnet/flixster/android/MovieMapPage$7;->this$0:Lnet/flixster/android/MovieMapPage;

    #calls: Lnet/flixster/android/MovieMapPage;->scheduleUpdatePageTask()V
    invoke-static {v2}, Lnet/flixster/android/MovieMapPage;->access$4(Lnet/flixster/android/MovieMapPage;)V

    .line 171
    :goto_0
    return-void

    .line 169
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/MovieMapPage$7;->this$0:Lnet/flixster/android/MovieMapPage;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lnet/flixster/android/MovieMapPage;->showDialog(I)V

    goto :goto_0
.end method
