.class public Lnet/flixster/android/MovieMapPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;
.source "MovieMapPage.java"


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x2

.field private static final DIALOG_THEATERS:I = 0x1

.field private static final DIALOG_THEATERS_NONE:I = 0x3

.field private static final DIALOG_ZIP:I = 0x4

.field private static final DIALOG_ZIP_ERROR:I = 0x5

.field private static final DIALOG_ZIP_NONE:I = 0x6

.field private static final MAP_ZOOM_DEFAULT:I = 0xd


# instance fields
.field private dismissProgressHandler:Landroid/os/Handler;

.field private mapController:Lcom/google/android/maps/MapController;

.field private mapView:Lcom/google/android/maps/MapView;

.field private movie:Lnet/flixster/android/model/Movie;

.field private progressDialog:Landroid/app/ProgressDialog;

.field private settingsClickListener:Landroid/view/View$OnClickListener;

.field private showProgressHandler:Landroid/os/Handler;

.field private theaters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation
.end field

.field private timer:Ljava/util/Timer;

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;-><init>()V

    .line 204
    new-instance v0, Lnet/flixster/android/MovieMapPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieMapPage$1;-><init>(Lnet/flixster/android/MovieMapPage;)V

    iput-object v0, p0, Lnet/flixster/android/MovieMapPage;->showProgressHandler:Landroid/os/Handler;

    .line 216
    new-instance v0, Lnet/flixster/android/MovieMapPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieMapPage$2;-><init>(Lnet/flixster/android/MovieMapPage;)V

    iput-object v0, p0, Lnet/flixster/android/MovieMapPage;->dismissProgressHandler:Landroid/os/Handler;

    .line 238
    new-instance v0, Lnet/flixster/android/MovieMapPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieMapPage$3;-><init>(Lnet/flixster/android/MovieMapPage;)V

    iput-object v0, p0, Lnet/flixster/android/MovieMapPage;->updateHandler:Landroid/os/Handler;

    .line 335
    new-instance v0, Lnet/flixster/android/MovieMapPage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieMapPage$4;-><init>(Lnet/flixster/android/MovieMapPage;)V

    iput-object v0, p0, Lnet/flixster/android/MovieMapPage;->settingsClickListener:Landroid/view/View$OnClickListener;

    .line 40
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/MovieMapPage;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/MovieMapPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage;->theaters:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/MovieMapPage;)V
    .locals 0
    .parameter

    .prologue
    .line 278
    invoke-direct {p0}, Lnet/flixster/android/MovieMapPage;->updatePage()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/MovieMapPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 204
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage;->showProgressHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/MovieMapPage;)V
    .locals 0
    .parameter

    .prologue
    .line 228
    invoke-direct {p0}, Lnet/flixster/android/MovieMapPage;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$5(Lnet/flixster/android/MovieMapPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 216
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage;->dismissProgressHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/MovieMapPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 253
    invoke-direct {p0}, Lnet/flixster/android/MovieMapPage;->getTheaters()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7(Lnet/flixster/android/MovieMapPage;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lnet/flixster/android/MovieMapPage;->theaters:Ljava/util/List;

    return-void
.end method

.method static synthetic access$8(Lnet/flixster/android/MovieMapPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 238
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getTheaters()Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    const/16 v18, 0x0

    .line 255
    .local v18, theaters:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v8

    .line 257
    .local v8, favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :try_start_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 258
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v1

    .line 259
    .local v1, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v3

    .line 261
    .local v3, longitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v6}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v6

    .line 260
    invoke-static/range {v1 .. v8}, Lnet/flixster/android/data/TheaterDao;->findTheatersByMovieLocation(DDLjava/util/Date;JLjava/util/HashMap;)Ljava/util/List;

    move-result-object v18

    .line 274
    .end local v1           #latitude:D
    .end local v3           #longitude:D
    :goto_0
    return-object v18

    .line 262
    :cond_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v5

    const-wide/16 v9, 0x0

    cmpl-double v5, v5, v9

    if-eqz v5, :cond_1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v5

    const-wide/16 v9, 0x0

    cmpl-double v5, v5, v9

    if-eqz v5, :cond_1

    .line 263
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v9

    .line 264
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v11

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v14

    move-object/from16 v16, v8

    .line 263
    invoke-static/range {v9 .. v16}, Lnet/flixster/android/data/TheaterDao;->findTheatersByMovieLocation(DDLjava/util/Date;JLjava/util/HashMap;)Ljava/util/List;

    move-result-object v18

    goto :goto_0

    .line 267
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v9

    .line 268
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v11

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v14

    move-object/from16 v16, v8

    .line 267
    invoke-static/range {v9 .. v16}, Lnet/flixster/android/data/TheaterDao;->findTheatersByMovieLocation(DDLjava/util/Date;JLjava/util/HashMap;)Ljava/util/List;
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v18

    goto :goto_0

    .line 271
    :catch_0
    move-exception v17

    .line 272
    .local v17, de:Lnet/flixster/android/data/DaoException;
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "problem loading theaters: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Lnet/flixster/android/data/DaoException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 229
    new-instance v0, Lnet/flixster/android/MovieMapPage$10;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieMapPage$10;-><init>(Lnet/flixster/android/MovieMapPage;)V

    .line 235
    .local v0, updatePageTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/MovieMapPage;->timer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 236
    return-void
.end method

.method private updatePage()V
    .locals 27

    .prologue
    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/MovieMapPage;->theaters:Ljava/util/List;

    move-object/from16 v23, v0

    if-eqz v23, :cond_6

    .line 282
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v23

    if-eqz v23, :cond_0

    .line 283
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v6

    .line 284
    .local v6, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v10

    .line 289
    .local v10, longitude:D
    :goto_0
    const-wide/16 v23, 0x0

    cmpl-double v23, v6, v23

    if-nez v23, :cond_1

    const-wide/16 v23, 0x0

    cmpl-double v23, v10, v23

    if-nez v23, :cond_1

    .line 290
    const v23, 0x7f030085

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieMapPage;->setContentView(I)V

    .line 291
    const v23, 0x7f070268

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    .line 292
    .local v14, settingsButton:Landroid/widget/Button;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/MovieMapPage;->settingsClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    const v23, 0x7f070267

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 294
    .local v16, settingsText:Landroid/widget/TextView;
    const-string v23, "No theaters found"

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 332
    .end local v6           #latitude:D
    .end local v10           #longitude:D
    .end local v14           #settingsButton:Landroid/widget/Button;
    .end local v16           #settingsText:Landroid/widget/TextView;
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/MovieMapPage;->dismissProgressHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 333
    return-void

    .line 286
    :cond_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v6

    .line 287
    .restart local v6       #latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v10

    .restart local v10       #longitude:D
    goto :goto_0

    .line 296
    :cond_1
    const v23, 0x7f070264

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/LinearLayout;

    .line 297
    .local v15, settingsLayout:Landroid/widget/LinearLayout;
    if-eqz v15, :cond_2

    .line 298
    const/16 v23, 0x4

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 300
    :cond_2
    new-instance v4, Lcom/google/android/maps/GeoPoint;

    const-wide v23, 0x412e848000000000L

    mul-double v23, v23, v6

    move-wide/from16 v0, v23

    double-to-int v0, v0

    move/from16 v23, v0

    const-wide v24, 0x412e848000000000L

    mul-double v24, v24, v10

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v4, v0, v1}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 301
    .local v4, geoPoint:Lcom/google/android/maps/GeoPoint;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/MovieMapPage;->mapController:Lcom/google/android/maps/MapController;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Lcom/google/android/maps/MapController;->setCenter(Lcom/google/android/maps/GeoPoint;)V

    .line 302
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/MovieMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 303
    .local v13, resources:Landroid/content/res/Resources;
    new-instance v9, Lnet/flixster/android/TheaterItemizedOverlay;

    .line 304
    const v23, 0x7f0200ad

    move/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .line 303
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v9, v0, v1}, Lnet/flixster/android/TheaterItemizedOverlay;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 305
    .local v9, locationOverlays:Lnet/flixster/android/TheaterItemizedOverlay;
    new-instance v8, Lcom/google/android/maps/OverlayItem;

    const-string v23, "Current Location"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v8, v4, v0, v1}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    .local v8, locationOverlay:Lcom/google/android/maps/OverlayItem;
    invoke-virtual {v9, v8}, Lnet/flixster/android/TheaterItemizedOverlay;->addOverlay(Lcom/google/android/maps/OverlayItem;)V

    .line 307
    new-instance v21, Lnet/flixster/android/TheaterItemizedOverlay;

    .line 308
    const v23, 0x7f020152

    move/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .line 307
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/TheaterItemizedOverlay;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 309
    .local v21, theaterOverlays:Lnet/flixster/android/TheaterItemizedOverlay;
    const/4 v5, 0x0

    .local v5, i:I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/MovieMapPage;->theaters:Ljava/util/List;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v23

    move/from16 v0, v23

    if-lt v5, v0, :cond_4

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/MovieMapPage;->mapView:Lcom/google/android/maps/MapView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v12

    .line 322
    .local v12, overlays:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 323
    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    invoke-virtual/range {v21 .. v21}, Lnet/flixster/android/TheaterItemizedOverlay;->size()I

    move-result v23

    if-lez v23, :cond_3

    .line 325
    move-object/from16 v0, v21

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/MovieMapPage;->mapView:Lcom/google/android/maps/MapView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/maps/MapView;->invalidate()V

    goto/16 :goto_1

    .line 310
    .end local v12           #overlays:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/MovieMapPage;->theaters:Ljava/util/List;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lnet/flixster/android/model/Theater;

    .line 311
    .local v17, theater:Lnet/flixster/android/model/Theater;
    if-eqz v17, :cond_5

    .line 312
    move-object/from16 v0, v17

    iget-wide v0, v0, Lnet/flixster/android/model/Theater;->latitude:D

    move-wide/from16 v23, v0

    const-wide v25, 0x412e848000000000L

    mul-double v23, v23, v25

    move-wide/from16 v0, v23

    double-to-int v0, v0

    move/from16 v18, v0

    .line 313
    .local v18, theaterLatitude:I
    move-object/from16 v0, v17

    iget-wide v0, v0, Lnet/flixster/android/model/Theater;->longitude:D

    move-wide/from16 v23, v0

    const-wide v25, 0x412e848000000000L

    mul-double v23, v23, v25

    move-wide/from16 v0, v23

    double-to-int v0, v0

    move/from16 v19, v0

    .line 314
    .local v19, theaterLongitude:I
    new-instance v22, Lcom/google/android/maps/GeoPoint;

    move-object/from16 v0, v22

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 315
    .local v22, theaterPoint:Lcom/google/android/maps/GeoPoint;
    new-instance v20, Lcom/google/android/maps/OverlayItem;

    .line 316
    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "name"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v24, "\n"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    .line 317
    const-string v24, "address"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    .line 316
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 317
    invoke-virtual/range {v17 .. v17}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v24

    .line 315
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    move-object/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    .local v20, theaterOverlay:Lcom/google/android/maps/OverlayItem;
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lnet/flixster/android/TheaterItemizedOverlay;->addOverlay(Lcom/google/android/maps/OverlayItem;)V

    .line 309
    .end local v18           #theaterLatitude:I
    .end local v19           #theaterLongitude:I
    .end local v20           #theaterOverlay:Lcom/google/android/maps/OverlayItem;
    .end local v22           #theaterPoint:Lcom/google/android/maps/GeoPoint;
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 330
    .end local v4           #geoPoint:Lcom/google/android/maps/GeoPoint;
    .end local v5           #i:I
    .end local v6           #latitude:D
    .end local v8           #locationOverlay:Lcom/google/android/maps/OverlayItem;
    .end local v9           #locationOverlays:Lnet/flixster/android/TheaterItemizedOverlay;
    .end local v10           #longitude:D
    .end local v13           #resources:Landroid/content/res/Resources;
    .end local v15           #settingsLayout:Landroid/widget/LinearLayout;
    .end local v17           #theater:Lnet/flixster/android/model/Theater;
    .end local v21           #theaterOverlays:Lnet/flixster/android/TheaterItemizedOverlay;
    :cond_6
    const/16 v23, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lnet/flixster/android/MovieMapPage;->showDialog(I)V

    goto/16 :goto_1
.end method


# virtual methods
.method public isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    const-string v2, "FlxMain"

    const-string v3, "MovieMapPage.onCreate()"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const v2, 0x7f030057

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieMapPage;->setContentView(I)V

    .line 66
    invoke-virtual {p0}, Lnet/flixster/android/MovieMapPage;->createActionBar()V

    .line 68
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    iput-object v2, p0, Lnet/flixster/android/MovieMapPage;->timer:Ljava/util/Timer;

    .line 69
    const v2, 0x7f0700e4

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/maps/MapView;

    iput-object v2, p0, Lnet/flixster/android/MovieMapPage;->mapView:Lcom/google/android/maps/MapView;

    .line 70
    const v2, 0x7f0700e5

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 71
    .local v0, zoomLayout:Landroid/widget/LinearLayout;
    iget-object v2, p0, Lnet/flixster/android/MovieMapPage;->mapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getZoomControls()Landroid/view/View;

    move-result-object v1

    .line 72
    .local v1, zoomView:Landroid/view/View;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 73
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 12
    .parameter "dialogId"

    .prologue
    const/4 v9, 0x0

    const v11, 0x7f0c004a

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 118
    packed-switch p1, :pswitch_data_0

    .line 201
    :goto_0
    return-object v7

    .line 120
    :pswitch_0
    new-instance v7, Landroid/app/ProgressDialog;

    invoke-direct {v7, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lnet/flixster/android/MovieMapPage;->progressDialog:Landroid/app/ProgressDialog;

    .line 121
    iget-object v7, p0, Lnet/flixster/android/MovieMapPage;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lnet/flixster/android/MovieMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c0135

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v7, p0, Lnet/flixster/android/MovieMapPage;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v7, v10}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 123
    iget-object v7, p0, Lnet/flixster/android/MovieMapPage;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v7, v10}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 124
    iget-object v7, p0, Lnet/flixster/android/MovieMapPage;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v7, v10}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 125
    iget-object v7, p0, Lnet/flixster/android/MovieMapPage;->progressDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 128
    :pswitch_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 129
    .local v2, networkFailAlert:Landroid/app/AlertDialog$Builder;
    const-string v7, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 130
    const-string v7, "Network Error"

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 131
    invoke-virtual {v2, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 132
    const-string v7, "Retry"

    new-instance v8, Lnet/flixster/android/MovieMapPage$5;

    invoke-direct {v8, p0}, Lnet/flixster/android/MovieMapPage$5;-><init>(Lnet/flixster/android/MovieMapPage;)V

    invoke-virtual {v2, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 138
    invoke-virtual {p0}, Lnet/flixster/android/MovieMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 139
    new-instance v8, Lnet/flixster/android/MovieMapPage$6;

    invoke-direct {v8, p0}, Lnet/flixster/android/MovieMapPage$6;-><init>(Lnet/flixster/android/MovieMapPage;)V

    .line 138
    invoke-virtual {v2, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 144
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    goto :goto_0

    .line 146
    .end local v2           #networkFailAlert:Landroid/app/AlertDialog$Builder;
    :pswitch_2
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 147
    .local v3, noTheatersAlert:Landroid/app/AlertDialog$Builder;
    const-string v8, "No theaters were found for this postal code."

    invoke-virtual {v3, v8}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 148
    const-string v8, "No Theaters."

    invoke-virtual {v3, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 149
    invoke-virtual {v3, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 150
    const-string v8, "OK"

    invoke-virtual {v3, v8, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 151
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    goto :goto_0

    .line 153
    .end local v3           #noTheatersAlert:Landroid/app/AlertDialog$Builder;
    :pswitch_3
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 154
    .local v1, factory:Landroid/view/LayoutInflater;
    const v8, 0x7f030095

    invoke-virtual {v1, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 155
    .local v5, zipEntryView:Landroid/view/View;
    const/16 v8, 0x12c

    invoke-virtual {v5, v8}, Landroid/view/View;->setMinimumWidth(I)V

    .line 156
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 157
    .local v0, enterZipAlert:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 158
    const v8, 0x7f0c0049

    new-instance v9, Lnet/flixster/android/MovieMapPage$7;

    invoke-direct {v9, p0, v5}, Lnet/flixster/android/MovieMapPage$7;-><init>(Lnet/flixster/android/MovieMapPage;Landroid/view/View;)V

    invoke-virtual {v0, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 173
    invoke-virtual {v0, v11, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 174
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    goto/16 :goto_0

    .line 176
    .end local v0           #enterZipAlert:Landroid/app/AlertDialog$Builder;
    .end local v1           #factory:Landroid/view/LayoutInflater;
    .end local v5           #zipEntryView:Landroid/view/View;
    :pswitch_4
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 177
    .local v6, zipErrorAlert:Landroid/app/AlertDialog$Builder;
    const-string v8, "Invalid Postal Code"

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 178
    const-string v8, "Invalid Postal Code"

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 179
    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const-string v9, "OK"

    invoke-virtual {v8, v9, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 180
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    goto/16 :goto_0

    .line 182
    .end local v6           #zipErrorAlert:Landroid/app/AlertDialog$Builder;
    :pswitch_5
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 183
    .local v4, noZipAlert:Landroid/app/AlertDialog$Builder;
    const-string v8, "Postal Code Warning"

    invoke-virtual {v4, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 184
    const-string v8, "Can\'t detect postal code, please retry or manually enter postal code."

    invoke-virtual {v4, v8}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 185
    invoke-virtual {v4, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 186
    const-string v8, "Retry"

    new-instance v9, Lnet/flixster/android/MovieMapPage$8;

    invoke-direct {v9, p0}, Lnet/flixster/android/MovieMapPage$8;-><init>(Lnet/flixster/android/MovieMapPage;)V

    invoke-virtual {v4, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 192
    const-string v8, "Manual"

    new-instance v9, Lnet/flixster/android/MovieMapPage$9;

    invoke-direct {v9, p0}, Lnet/flixster/android/MovieMapPage$9;-><init>(Lnet/flixster/android/MovieMapPage;)V

    invoke-virtual {v4, v8, v9}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 198
    invoke-virtual {p0}, Lnet/flixster/android/MovieMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 199
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    goto/16 :goto_0

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onDestroy()V

    .line 109
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 111
    iget-object v0, p0, Lnet/flixster/android/MovieMapPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 113
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/MovieMapPage;->timer:Ljava/util/Timer;

    .line 114
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onPause()V

    .line 104
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    .line 77
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onResume()V

    .line 78
    const-string v3, "FlxMain"

    const-string v4, "MovieMapPage.onResume()"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lnet/flixster/android/MovieMapPage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 80
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 81
    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 82
    .local v1, movieId:J
    new-instance v3, Lnet/flixster/android/model/Movie;

    invoke-direct {v3, v1, v2}, Lnet/flixster/android/model/Movie;-><init>(J)V

    iput-object v3, p0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    .line 83
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    const-string v4, "title"

    const-string v5, "title"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    const-string v4, "runningTime"

    const-string v5, "runningTime"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    const-string v4, "MOVIE_ACTORS_SHORT"

    const-string v5, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    const-string v4, "mpaa"

    const-string v5, "mpaa"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    const-string v4, "thumbnail"

    const-string v5, "thumbnail"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    const-string v4, "popcornScore"

    const-string v5, "popcornScore"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lnet/flixster/android/model/Movie;->setIntProperty(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 89
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    const-string v4, "rottenTomatoes"

    const-string v5, "rottenTomatoes"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lnet/flixster/android/model/Movie;->setIntProperty(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 90
    new-instance v3, Ljava/lang/StringBuilder;

    const v4, 0x7f0c0110

    invoke-virtual {p0, v4}, Lnet/flixster/android/MovieMapPage;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lnet/flixster/android/MovieMapPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 92
    .end local v1           #movieId:J
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/movie/showtimes/map"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Movie Info - "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lnet/flixster/android/MovieMapPage;->movie:Lnet/flixster/android/model/Movie;

    const-string v7, "title"

    invoke-virtual {v6, v7}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->mapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v3

    iput-object v3, p0, Lnet/flixster/android/MovieMapPage;->mapController:Lcom/google/android/maps/MapController;

    .line 95
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->mapController:Lcom/google/android/maps/MapController;

    const/16 v4, 0xd

    invoke-virtual {v3, v4}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 96
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->mapView:Lcom/google/android/maps/MapView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/maps/MapView;->displayZoomControls(Z)V

    .line 97
    iget-object v3, p0, Lnet/flixster/android/MovieMapPage;->showProgressHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 98
    invoke-direct {p0}, Lnet/flixster/android/MovieMapPage;->scheduleUpdatePageTask()V

    .line 99
    return-void
.end method
