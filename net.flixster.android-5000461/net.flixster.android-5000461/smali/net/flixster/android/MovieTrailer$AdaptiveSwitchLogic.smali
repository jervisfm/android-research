.class Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;
.super Ljava/lang/Object;
.source "MovieTrailer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieTrailer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AdaptiveSwitchLogic"
.end annotation


# instance fields
.field private bufferEndedTime:J

.field private bufferStartedTime:J

.field private hasAdaptiveSwitched:Z

.field private ignoreBufferEnd:Z

.field private isBufferTimeTooLong:Z

.field private lastPlaybackPosition:I

.field private urlLow:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "contentUrlLow"

    .prologue
    .line 529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->shouldUseLowBitrateForTrailer()Z

    move-result v0

    iput-boolean v0, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->hasAdaptiveSwitched:Z

    .line 531
    iput-object p1, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->urlLow:Ljava/lang/String;

    .line 532
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 529
    invoke-direct {p0, p1}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 534
    invoke-direct {p0, p1}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->videoUrlSet(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;)V
    .locals 0
    .parameter

    .prologue
    .line 539
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->videoPrepared()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;)Z
    .locals 1
    .parameter

    .prologue
    .line 569
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->shouldSwitch()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 543
    invoke-direct {p0, p1}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferStarted(I)V

    return-void
.end method

.method static synthetic access$5(Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;)V
    .locals 0
    .parameter

    .prologue
    .line 555
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferEnded()V

    return-void
.end method

.method private bufferEnded()V
    .locals 6

    .prologue
    .line 556
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferEndedTime:J

    .line 557
    iget-boolean v2, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->ignoreBufferEnd:Z

    if-eqz v2, :cond_0

    .line 558
    const-string v2, "FlxMain"

    const-string v3, "MovieTrailer.AdaptiveSwitchLogic.bufferEnded ignored"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :goto_0
    return-void

    .line 559
    :cond_0
    iget-boolean v2, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->hasAdaptiveSwitched:Z

    if-eqz v2, :cond_1

    .line 560
    const-string v2, "FlxMain"

    const-string v3, "MovieTrailer.AdaptiveSwitchLogic.bufferEnded already switched or low"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 562
    :cond_1
    iget-wide v2, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferEndedTime:J

    iget-wide v4, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferStartedTime:J

    sub-long v0, v2, v4

    .line 563
    .local v0, bufferTime:J
    const-wide/16 v2, 0x1388

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->isBufferTimeTooLong:Z

    .line 564
    const-string v3, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "MovieTrailer.AdaptiveSwitchLogic.bufferEnded took "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 565
    iget-boolean v2, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->isBufferTimeTooLong:Z

    if-eqz v2, :cond_3

    const-string v2, " too long"

    :goto_2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 564
    invoke-static {v3, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 563
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 565
    :cond_3
    const-string v2, ""

    goto :goto_2
.end method

.method private bufferStarted(I)V
    .locals 8
    .parameter "playbackPosition"

    .prologue
    const/high16 v7, 0x3f80

    .line 544
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferStartedTime:J

    .line 545
    iget-wide v3, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferStartedTime:J

    iget-wide v5, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferEndedTime:J

    sub-long/2addr v3, v5

    long-to-int v1, v3

    .line 546
    .local v1, elapsedTime:I
    iget v3, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->lastPlaybackPosition:I

    sub-int v0, p1, v3

    .line 547
    .local v0, elapsedPlaybackTime:I
    iput p1, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->lastPlaybackPosition:I

    .line 548
    int-to-float v3, v0

    int-to-float v4, v1

    add-float/2addr v4, v7

    div-float v2, v3, v4

    .line 549
    .local v2, ratio:F
    cmpl-float v3, v2, v7

    if-gtz v3, :cond_0

    const/4 v3, 0x0

    cmpg-float v3, v2, v3

    if-ltz v3, :cond_0

    if-eqz v0, :cond_0

    const/4 v3, 0x0

    :goto_0
    iput-boolean v3, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->ignoreBufferEnd:Z

    .line 551
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieTrailer.AdaptiveSwitchLogic.bufferStarted "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferStartedTime:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 552
    iget v5, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->lastPlaybackPosition:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 551
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    return-void

    .line 549
    :cond_0
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private shouldSwitch()Z
    .locals 4

    .prologue
    .line 570
    iget-boolean v1, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->hasAdaptiveSwitched:Z

    if-eqz v1, :cond_0

    .line 571
    const-string v1, "FlxMain"

    const-string v2, "MovieTrailer.AdaptiveSwitchLogic.shouldSwitch already switched or low"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    const/4 v0, 0x0

    .line 580
    :goto_0
    return v0

    .line 574
    :cond_0
    const/4 v0, 0x0

    .line 575
    .local v0, result:Z
    iget-boolean v1, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->isBufferTimeTooLong:Z

    if-eqz v1, :cond_1

    .line 576
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->hasAdaptiveSwitched:Z

    .line 577
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->setUseLowBitrateForTrailer()V

    .line 579
    :cond_1
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MovieTrailer.AdaptiveSwitchLogic.shouldSwitch "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private videoPrepared()V
    .locals 2

    .prologue
    .line 540
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferEndedTime:J

    iput-wide v0, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferStartedTime:J

    .line 541
    return-void
.end method

.method private videoUrlSet(Ljava/lang/String;)V
    .locals 3
    .parameter "url"

    .prologue
    .line 535
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MovieTrailer.AdaptiveSwitchLogic.videoUrlSet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->urlLow:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->hasAdaptiveSwitched:Z

    .line 537
    return-void
.end method
