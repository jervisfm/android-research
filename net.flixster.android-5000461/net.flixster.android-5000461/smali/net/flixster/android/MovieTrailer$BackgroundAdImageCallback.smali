.class final Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;
.super Ljava/lang/Object;
.source "MovieTrailer.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieTrailer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BackgroundAdImageCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieTrailer;


# direct methods
.method private constructor <init>(Lnet/flixster/android/MovieTrailer;)V
    .locals 0
    .parameter

    .prologue
    .line 493
    iput-object p1, p0, Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lnet/flixster/android/MovieTrailer;Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 493
    invoke-direct {p0, p1}, Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;-><init>(Lnet/flixster/android/MovieTrailer;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .parameter "msg"

    .prologue
    .line 496
    const-string v0, "FlxMain"

    const-string v1, "MovieTrailer.BackgroundAdImageCallback"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    #getter for: Lnet/flixster/android/MovieTrailer;->backgroundImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lnet/flixster/android/MovieTrailer;->access$3(Lnet/flixster/android/MovieTrailer;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    #getter for: Lnet/flixster/android/MovieTrailer;->backgroundAd:Lnet/flixster/android/ads/model/FlixsterAd;
    invoke-static {v1}, Lnet/flixster/android/MovieTrailer;->access$4(Lnet/flixster/android/MovieTrailer;)Lnet/flixster/android/ads/model/FlixsterAd;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 498
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    #getter for: Lnet/flixster/android/MovieTrailer;->backgroundAd:Lnet/flixster/android/ads/model/FlixsterAd;
    invoke-static {v1}, Lnet/flixster/android/MovieTrailer;->access$4(Lnet/flixster/android/MovieTrailer;)Lnet/flixster/android/ads/model/FlixsterAd;

    move-result-object v1

    const-string v2, "Impression"

    invoke-virtual {v0, v1, v2}, Lnet/flixster/android/ads/AdManager;->trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V

    .line 499
    const/4 v0, 0x1

    return v0
.end method
