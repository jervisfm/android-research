.class Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;
.super Ljava/lang/Object;
.source "MovieTrailer.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieTrailer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemainingTimeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieTrailer;


# direct methods
.method private constructor <init>(Lnet/flixster/android/MovieTrailer;)V
    .locals 0
    .parameter

    .prologue
    .line 226
    iput-object p1, p0, Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lnet/flixster/android/MovieTrailer;Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 226
    invoke-direct {p0, p1}, Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;-><init>(Lnet/flixster/android/MovieTrailer;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 2
    .parameter "msg"

    .prologue
    .line 229
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    invoke-virtual {v0}, Lnet/flixster/android/MovieTrailer;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    #getter for: Lnet/flixster/android/MovieTrailer;->adLabel:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/MovieTrailer;->access$1(Lnet/flixster/android/MovieTrailer;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    #calls: Lnet/flixster/android/MovieTrailer;->getRemainingTimeAdLabel()Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/MovieTrailer;->access$2(Lnet/flixster/android/MovieTrailer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
