.class Lnet/flixster/android/MovieTrailer$SkipButtonCallback;
.super Ljava/lang/Object;
.source "MovieTrailer.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MovieTrailer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SkipButtonCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MovieTrailer;


# direct methods
.method private constructor <init>(Lnet/flixster/android/MovieTrailer;)V
    .locals 0
    .parameter

    .prologue
    .line 214
    iput-object p1, p0, Lnet/flixster/android/MovieTrailer$SkipButtonCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lnet/flixster/android/MovieTrailer;Lnet/flixster/android/MovieTrailer$SkipButtonCallback;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 214
    invoke-direct {p0, p1}, Lnet/flixster/android/MovieTrailer$SkipButtonCallback;-><init>(Lnet/flixster/android/MovieTrailer;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .parameter "msg"

    .prologue
    .line 217
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer$SkipButtonCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    invoke-virtual {v0}, Lnet/flixster/android/MovieTrailer;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer$SkipButtonCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    #getter for: Lnet/flixster/android/MovieTrailer;->adSkipButton:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/MovieTrailer;->access$0(Lnet/flixster/android/MovieTrailer;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer$SkipButtonCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    #getter for: Lnet/flixster/android/MovieTrailer;->adSkipButton:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/MovieTrailer;->access$0(Lnet/flixster/android/MovieTrailer;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MovieTrailer$SkipButtonCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer$SkipButtonCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    #getter for: Lnet/flixster/android/MovieTrailer;->adSkipButton:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/MovieTrailer;->access$0(Lnet/flixster/android/MovieTrailer;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MovieTrailer$SkipButtonCallback;->this$0:Lnet/flixster/android/MovieTrailer;

    const v2, 0x7f04000e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 222
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
