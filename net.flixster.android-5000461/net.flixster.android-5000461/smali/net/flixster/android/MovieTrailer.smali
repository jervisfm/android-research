.class public Lnet/flixster/android/MovieTrailer;
.super Lcom/flixster/android/activity/common/DecoratedActivity;
.source "MovieTrailer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;
.implements Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;,
        Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;,
        Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;,
        Lnet/flixster/android/MovieTrailer$SkipButtonCallback;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$flixster$android$utils$OsInfo$NetworkSpeed:[I = null

.field private static synthetic $SWITCH_TABLE$com$google$ads$interactivemedia$api$AdsManager$AdEventType:[I = null

.field private static final SKIP_BUTTON_DELAY:I = 0xbb8


# instance fields
.field private adLabel:Landroid/widget/TextView;

.field private adSkipButton:Landroid/widget/TextView;

.field private adVisitButton:Landroid/widget/TextView;

.field private adaptiveSwitchLogic:Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;

.field private backgroundAd:Lnet/flixster/android/ads/model/FlixsterAd;

.field private backgroundImage:Landroid/widget/ImageView;

.field private contentUrlHigh:Ljava/lang/String;

.field private contentUrlLow:Ljava/lang/String;

.field private contentUrlMed:Ljava/lang/String;

.field private controls:Landroid/widget/MediaController;

.field private dfpVideoAdRunner:Lcom/flixster/android/ads/DfpVideoAdRunner;

.field private isAdStarted:Z

.field private isBackPressed:Z

.field private isDfpVideoAvailable:Z

.field private movieId:J

.field private savedContentPosition:I

.field private throbber:Landroid/widget/ProgressBar;

.field private timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

.field private title:Ljava/lang/String;

.field private video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

.field private videoPosition:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$flixster$android$utils$OsInfo$NetworkSpeed()[I
    .locals 3

    .prologue
    .line 52
    sget-object v0, Lnet/flixster/android/MovieTrailer;->$SWITCH_TABLE$com$flixster$android$utils$OsInfo$NetworkSpeed:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->values()[Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->BASIC_LIKE_3G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    invoke-virtual {v1}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FASTER_LIKE_WIFI:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    invoke-virtual {v1}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->FAST_LIKE_4G:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    invoke-virtual {v1}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->SLOW_LIKE_EDGE:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    invoke-virtual {v1}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->UNKNOWN:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    invoke-virtual {v1}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lnet/flixster/android/MovieTrailer;->$SWITCH_TABLE$com$flixster$android$utils$OsInfo$NetworkSpeed:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$google$ads$interactivemedia$api$AdsManager$AdEventType()[I
    .locals 3

    .prologue
    .line 52
    sget-object v0, Lnet/flixster/android/MovieTrailer;->$SWITCH_TABLE$com$google$ads$interactivemedia$api$AdsManager$AdEventType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->values()[Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->CLICK:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_1
    :try_start_1
    sget-object v1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->COMPLETE:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    :try_start_2
    sget-object v1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->CONTENT_PAUSE_REQUESTED:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_3
    :try_start_3
    sget-object v1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->CONTENT_RESUME_REQUESTED:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_4
    :try_start_4
    sget-object v1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->FIRST_QUARTILE:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    :try_start_5
    sget-object v1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->MIDPOINT:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_6
    :try_start_6
    sget-object v1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->PAUSED:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_7
    :try_start_7
    sget-object v1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->STARTED:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_8
    :try_start_8
    sget-object v1, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->THIRD_QUARTILE:Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_9
    sput-object v0, Lnet/flixster/android/MovieTrailer;->$SWITCH_TABLE$com$google$ads$interactivemedia$api$AdsManager$AdEventType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/MovieTrailer;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adSkipButton:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/MovieTrailer;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/MovieTrailer;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 236
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getRemainingTimeAdLabel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/MovieTrailer;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->backgroundImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/MovieTrailer;)Lnet/flixster/android/ads/model/FlixsterAd;
    .locals 1
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->backgroundAd:Lnet/flixster/android/ads/model/FlixsterAd;

    return-object v0
.end method

.method private getContentUrlHigh()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->contentUrlHigh:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getContentUrlMed()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->contentUrlHigh:Ljava/lang/String;

    goto :goto_0
.end method

.method private getContentUrlMed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->contentUrlMed:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->contentUrlMed:Ljava/lang/String;

    goto :goto_0
.end method

.method private getRemainingTime()I
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->getDuration()I

    move-result v0

    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->getCurrentPosition()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method private getRemainingTimeAdLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0c01b6

    invoke-virtual {p0, v1}, Lnet/flixster/android/MovieTrailer;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getRemainingTime()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getVideoUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 294
    invoke-static {}, Lcom/flixster/android/utils/OsInfo;->instance()Lcom/flixster/android/utils/OsInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/OsInfo;->getNetworkSpeed()Lcom/flixster/android/utils/OsInfo$NetworkSpeed;

    move-result-object v0

    .line 295
    .local v0, speed:Lcom/flixster/android/utils/OsInfo$NetworkSpeed;
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->shouldUseLowBitrateForTrailer()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 296
    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    .line 324
    :goto_0
    return-object v1

    .line 298
    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 299
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getContentUrlHigh()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 300
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 301
    invoke-static {}, Lnet/flixster/android/MovieTrailer;->$SWITCH_TABLE$com$flixster$android$utils$OsInfo$NetworkSpeed()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 309
    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    goto :goto_0

    .line 303
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isScreenXlarge()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getContentUrlHigh()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getContentUrlMed()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 305
    :pswitch_1
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getContentUrlMed()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 311
    :cond_3
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isAtLeastHdpi()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isScreenLarge()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 312
    :cond_4
    invoke-static {}, Lnet/flixster/android/MovieTrailer;->$SWITCH_TABLE$com$flixster$android$utils$OsInfo$NetworkSpeed()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/flixster/android/utils/OsInfo$NetworkSpeed;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 321
    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    goto :goto_0

    .line 314
    :pswitch_2
    sget v1, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_5

    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getContentUrlHigh()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 315
    :cond_5
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getContentUrlMed()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 318
    :pswitch_3
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getContentUrlMed()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 324
    :cond_6
    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    goto :goto_0

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 312
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_3
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private loadContent()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v5, -0x2

    const/4 v1, 0x0

    .line 247
    const-string v0, "FlxMain"

    const-string v2, "MovieTrailer.loadContent"

    invoke-static {v0, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v0

    const-string v2, "TrailerBG"

    iget-wide v3, p0, Lnet/flixster/android/MovieTrailer;->movieId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v10, v3}, Lnet/flixster/android/ads/AdManager;->getAd(Ljava/lang/String;Landroid/os/Handler;Ljava/lang/Long;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/FlixsterAd;

    iput-object v0, p0, Lnet/flixster/android/MovieTrailer;->backgroundAd:Lnet/flixster/android/ads/model/FlixsterAd;

    .line 250
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->backgroundAd:Lnet/flixster/android/ads/model/FlixsterAd;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->shouldShowSponsor()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->backgroundAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd;->imageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 252
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 254
    .local v6, layoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xc

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 255
    const/16 v0, 0xe

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 256
    const/16 v0, 0xf

    invoke-virtual {v6, v1, v1, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 257
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->throbber:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 258
    invoke-static {}, Lnet/flixster/android/ads/ImageManager;->getInstance()Lnet/flixster/android/ads/ImageManager;

    move-result-object v8

    .line 259
    new-instance v0, Lnet/flixster/android/model/ImageOrder;

    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->backgroundAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v3, p0, Lnet/flixster/android/MovieTrailer;->backgroundAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v3, v3, Lnet/flixster/android/ads/model/FlixsterAd;->imageUrl:Ljava/lang/String;

    iget-object v4, p0, Lnet/flixster/android/MovieTrailer;->backgroundImage:Landroid/widget/ImageView;

    new-instance v5, Landroid/os/Handler;

    .line 260
    new-instance v9, Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;

    invoke-direct {v9, p0, v10}, Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;-><init>(Lnet/flixster/android/MovieTrailer;Lnet/flixster/android/MovieTrailer$BackgroundAdImageCallback;)V

    invoke-direct {v5, v9}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    .line 259
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 258
    invoke-virtual {v8, v0}, Lnet/flixster/android/ads/ImageManager;->enqueImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 264
    .end local v6           #layoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isAdStarted:Z

    if-nez v0, :cond_1

    .line 265
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->controls:Landroid/widget/MediaController;

    invoke-virtual {v0, v2}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 267
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0, v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setVisibility(I)V

    .line 268
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getVideoUrl()Ljava/lang/String;

    move-result-object v7

    .line 269
    .local v7, url:Ljava/lang/String;
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adaptiveSwitchLogic:Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;

    #calls: Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->videoUrlSet(Ljava/lang/String;)V
    invoke-static {v0, v7}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->access$1(Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0, v7}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setVideoPath(Ljava/lang/String;)V

    .line 271
    iget v0, p0, Lnet/flixster/android/MovieTrailer;->savedContentPosition:I

    if-lez v0, :cond_2

    .line 272
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    iget v1, p0, Lnet/flixster/android/MovieTrailer;->savedContentPosition:I

    invoke-virtual {v0, v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->seekTo(I)V

    .line 274
    :cond_2
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->showLoading()V

    .line 275
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/trailer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trailer - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/MovieTrailer;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method private pauseContent()V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lnet/flixster/android/MovieTrailer;->savedContentPosition:I

    .line 391
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->stopPlayback()V

    .line 392
    return-void
.end method

.method private postTrailer()V
    .locals 0

    .prologue
    .line 467
    invoke-virtual {p0}, Lnet/flixster/android/MovieTrailer;->finish()V

    .line 468
    return-void
.end method

.method private removeLoading()V
    .locals 2

    .prologue
    .line 475
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->throbber:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 476
    return-void
.end method

.method private resumeContent()V
    .locals 0

    .prologue
    .line 395
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->loadContent()V

    .line 396
    return-void
.end method

.method private scheduleRemainingTime()V
    .locals 6

    .prologue
    .line 211
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

    new-instance v1, Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;-><init>(Lnet/flixster/android/MovieTrailer;Lnet/flixster/android/MovieTrailer$RemainingTimeCallback;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Lcom/flixster/android/activity/decorator/TimerDecorator;->scheduleTask(Landroid/os/Handler$Callback;JJ)V

    .line 212
    return-void
.end method

.method private scheduleSkipButton()V
    .locals 4

    .prologue
    .line 207
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

    new-instance v1, Lnet/flixster/android/MovieTrailer$SkipButtonCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lnet/flixster/android/MovieTrailer$SkipButtonCallback;-><init>(Lnet/flixster/android/MovieTrailer;Lnet/flixster/android/MovieTrailer$SkipButtonCallback;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Lcom/flixster/android/activity/decorator/TimerDecorator;->scheduleTask(Landroid/os/Handler$Callback;J)V

    .line 208
    return-void
.end method

.method private showLoading()V
    .locals 2

    .prologue
    .line 471
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->throbber:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 472
    return-void
.end method

.method private trackEventPrerollStart()V
    .locals 5

    .prologue
    .line 479
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/trailer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trailer - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/MovieTrailer;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TrailerWithPreroll"

    const-string v4, "PrerollStart"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method private trackEventTrailerEnd()V
    .locals 5

    .prologue
    .line 489
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/trailer"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Trailer - "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/MovieTrailer;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 490
    iget-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isDfpVideoAvailable:Z

    if-eqz v0, :cond_0

    const-string v0, "TrailerWithPreroll"

    :goto_0
    const-string v4, "TrailerEnd"

    .line 489
    invoke-interface {v1, v2, v3, v0, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    return-void

    .line 490
    :cond_0
    const-string v0, "TrailerWithoutPreroll"

    goto :goto_0
.end method

.method private trackEventTrailerStart()V
    .locals 5

    .prologue
    .line 483
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/trailer"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Trailer - "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/MovieTrailer;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 484
    iget-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isDfpVideoAvailable:Z

    if-eqz v0, :cond_0

    const-string v0, "TrailerWithPreroll"

    :goto_0
    const-string v4, "TrailerStart"

    .line 483
    invoke-interface {v1, v2, v3, v0, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    return-void

    .line 484
    :cond_0
    const-string v0, "TrailerWithoutPreroll"

    goto :goto_0
.end method


# virtual methods
.method public addCallback(Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V
    .locals 2
    .parameter "callback"

    .prologue
    .line 167
    const-string v0, "FlxAd"

    const-string v1, "MovieTrailer.addCallback DFP"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0, p1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->addCallback(Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V

    .line 169
    return-void
.end method

.method public onAdEvent(Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;)V
    .locals 4
    .parameter "event"

    .prologue
    const/16 v3, 0x8

    .line 179
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MovieTrailer.onAdEvent DFP "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;->getEventType()Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-static {}, Lnet/flixster/android/MovieTrailer;->$SWITCH_TABLE$com$google$ads$interactivemedia$api$AdsManager$AdEventType()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEvent;->getEventType()Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/api/AdsManager$AdEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 204
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 182
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->pauseContent()V

    goto :goto_0

    .line 187
    :pswitch_2
    iget-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isBackPressed:Z

    if-nez v0, :cond_0

    .line 190
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 191
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adVisitButton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adSkipButton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 193
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

    invoke-virtual {v0}, Lcom/flixster/android/activity/decorator/TimerDecorator;->cancelTimer()V

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isAdStarted:Z

    .line 197
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->resumeContent()V

    goto :goto_0

    .line 201
    :pswitch_3
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->dfpVideoAdRunner:Lcom/flixster/android/ads/DfpVideoAdRunner;

    invoke-virtual {v0}, Lcom/flixster/android/ads/DfpVideoAdRunner;->unloadAd()V

    goto :goto_0

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 506
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 516
    :goto_0
    return-void

    .line 509
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->dfpVideoAdRunner:Lcom/flixster/android/ads/DfpVideoAdRunner;

    invoke-virtual {v0}, Lcom/flixster/android/ads/DfpVideoAdRunner;->unloadAd()V

    goto :goto_0

    .line 513
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->onClick()V

    goto :goto_0

    .line 506
    :pswitch_data_0
    .packed-switch 0x7f0702e0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .parameter "mp"

    .prologue
    .line 399
    const-string v0, "FlxMain"

    const-string v1, "MovieTrailer.onCompletion"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    iget-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isAdStarted:Z

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->onCompletion()V

    .line 409
    :goto_0
    return-void

    .line 403
    :cond_0
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->trackEventTrailerEnd()V

    .line 404
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->stopPlayback()V

    .line 405
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 406
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setVisibility(I)V

    .line 407
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->postTrailer()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x0

    .line 77
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    new-instance v2, Lcom/flixster/android/activity/decorator/TimerDecorator;

    invoke-direct {v2, p0}, Lcom/flixster/android/activity/decorator/TimerDecorator;-><init>(Landroid/app/Activity;)V

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieTrailer;->addDecorator(Lcom/flixster/android/activity/decorator/ActivityDecorator;)V

    .line 79
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

    invoke-virtual {v2}, Lcom/flixster/android/activity/decorator/TimerDecorator;->onCreate()V

    .line 81
    sget v2, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v4, 0x9

    if-ge v2, v4, :cond_1

    .line 82
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieTrailer;->setRequestedOrientation(I)V

    .line 86
    :goto_0
    invoke-static {p0}, Lcom/flixster/android/utils/VersionedViewHelper;->dimSystemNavigation(Landroid/app/Activity;)V

    .line 88
    invoke-virtual {p0}, Lnet/flixster/android/MovieTrailer;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 89
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 90
    const-string v2, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lnet/flixster/android/MovieTrailer;->movieId:J

    .line 91
    const-string v2, "title"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->title:Ljava/lang/String;

    .line 92
    const-string v2, "high"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->contentUrlHigh:Ljava/lang/String;

    .line 93
    const-string v2, "med"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->contentUrlMed:Ljava/lang/String;

    .line 94
    const-string v2, "low"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    .line 95
    const-string v2, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieTrailer.onCreate high: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/MovieTrailer;->contentUrlHigh:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v2, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieTrailer.onCreate med: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/MovieTrailer;->contentUrlMed:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v2, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieTrailer.onCreate low: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 100
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->contentUrlMed:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->contentUrlMed:Ljava/lang/String;

    :goto_1
    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    .line 101
    const-string v2, "FlxMain"

    const-string v4, "MovieTrailer.onCreate substitue for null contentUrlLow"

    invoke-static {v2, v4}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v2, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieTrailer.onCreate high: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/MovieTrailer;->contentUrlHigh:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v2, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieTrailer.onCreate med: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/MovieTrailer;->contentUrlMed:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v2, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MovieTrailer.onCreate low: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    const v2, 0x7f030090

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieTrailer;->setContentView(I)V

    .line 109
    const v2, 0x7f0702de

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieTrailer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->backgroundImage:Landroid/widget/ImageView;

    .line 110
    const v2, 0x7f070039

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieTrailer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->throbber:Landroid/widget/ProgressBar;

    .line 111
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieTrailer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->adLabel:Landroid/widget/TextView;

    .line 112
    const v2, 0x7f0702e1

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieTrailer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->adSkipButton:Landroid/widget/TextView;

    .line 113
    const v2, 0x7f0702e0

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieTrailer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->adVisitButton:Landroid/widget/TextView;

    .line 115
    const v2, 0x7f0702dd

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieTrailer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    .line 116
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v2, p0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 117
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v2, p0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 118
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v2, p0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 119
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v2}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->requestFocus()Z

    .line 120
    new-instance v2, Landroid/widget/MediaController;

    invoke-direct {v2, p0}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->controls:Landroid/widget/MediaController;

    .line 121
    new-instance v2, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;

    iget-object v4, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    invoke-direct {v2, v4, v3}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;-><init>(Ljava/lang/String;Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;)V

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->adaptiveSwitchLogic:Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;

    .line 125
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->shouldShowPreroll()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 126
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v2

    const-string v3, "Preroll"

    invoke-virtual {v2, v3}, Lnet/flixster/android/ads/AdManager;->getAd(Ljava/lang/String;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v1

    .local v1, preroll:Lnet/flixster/android/ads/model/Ad;
    if-eqz v1, :cond_5

    .line 127
    instance-of v2, v1, Lnet/flixster/android/ads/model/DfpVideoAd;

    if-eqz v2, :cond_4

    .line 128
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v2

    check-cast v1, Lnet/flixster/android/ads/model/DfpVideoAd;

    .end local v1           #preroll:Lnet/flixster/android/ads/model/Ad;
    const-string v3, "Impression"

    invoke-virtual {v2, v1, v3}, Lnet/flixster/android/ads/AdManager;->trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V

    .line 129
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->showLoading()V

    .line 130
    new-instance v2, Lcom/flixster/android/ads/DfpVideoAdRunner;

    iget-wide v3, p0, Lnet/flixster/android/MovieTrailer;->movieId:J

    invoke-static {v3, v4}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v3

    invoke-direct {v2, p0, p0, p0, v3}, Lcom/flixster/android/ads/DfpVideoAdRunner;-><init>(Landroid/app/Activity;Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer;Lcom/google/ads/interactivemedia/api/AdsManager$AdEventListener;Lnet/flixster/android/model/Movie;)V

    iput-object v2, p0, Lnet/flixster/android/MovieTrailer;->dfpVideoAdRunner:Lcom/flixster/android/ads/DfpVideoAdRunner;

    invoke-virtual {v2}, Lcom/flixster/android/ads/DfpVideoAdRunner;->showAd()V

    .line 140
    :goto_2
    return-void

    .line 84
    .end local v0           #extras:Landroid/os/Bundle;
    :cond_1
    const/4 v2, 0x6

    invoke-virtual {p0, v2}, Lnet/flixster/android/MovieTrailer;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 100
    .restart local v0       #extras:Landroid/os/Bundle;
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->contentUrlHigh:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->contentUrlHigh:Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    move-object v2, v3

    goto/16 :goto_1

    .line 132
    .restart local v1       #preroll:Lnet/flixster/android/ads/model/Ad;
    :cond_4
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MovieTrailer.onCreate unknown preroll type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->trackEventTrailerStart()V

    .line 134
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->loadContent()V

    goto :goto_2

    .line 137
    .end local v1           #preroll:Lnet/flixster/android/ads/model/Ad;
    :cond_5
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->trackEventTrailerStart()V

    .line 138
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->loadContent()V

    goto :goto_2
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    .prologue
    .line 462
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->removeLoading()V

    .line 463
    const/4 v0, 0x0

    return v0
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    .prologue
    const/4 v1, 0x1

    .line 364
    packed-switch p2, :pswitch_data_0

    .line 386
    const/4 v1, 0x0

    :cond_0
    :goto_0
    return v1

    .line 366
    :pswitch_0
    const-string v2, "FlxMain"

    const-string v3, "MovieTrailer.onInfo buffering started"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    iget-boolean v2, p0, Lnet/flixster/android/MovieTrailer;->isAdStarted:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 368
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->adaptiveSwitchLogic:Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;

    #calls: Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->shouldSwitch()Z
    invoke-static {v2}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->access$3(Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 369
    const-string v2, "FlxMain"

    const-string v3, "MovieTrailer.onInfo adaptive switching"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->showLoading()V

    .line 371
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v2}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->getCurrentPosition()I

    move-result v0

    .line 372
    .local v0, pos:I
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v2}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->stopPlayback()V

    .line 373
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 374
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    iget-object v3, p0, Lnet/flixster/android/MovieTrailer;->contentUrlLow:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setVideoPath(Ljava/lang/String;)V

    .line 375
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v2, v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->seekTo(I)V

    goto :goto_0

    .line 377
    .end local v0           #pos:I
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->adaptiveSwitchLogic:Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;

    iget-object v3, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v3}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->getCurrentPosition()I

    move-result v3

    #calls: Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferStarted(I)V
    invoke-static {v2, v3}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->access$4(Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;I)V

    goto :goto_0

    .line 382
    :pswitch_1
    const-string v2, "FlxMain"

    const-string v3, "MovieTrailer.onInfo buffering ended"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v2, p0, Lnet/flixster/android/MovieTrailer;->adaptiveSwitchLogic:Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;

    #calls: Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->bufferEnded()V
    invoke-static {v2}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->access$5(Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;)V

    goto :goto_0

    .line 364
    :pswitch_data_0
    .packed-switch 0x2bd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "keycode"
    .parameter "event"

    .prologue
    const/4 v2, 0x4

    .line 447
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    if-ne p1, v2, :cond_1

    .line 448
    const-string v0, "FlxMain"

    const-string v1, "MovieDetails.onKeyDown back"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isBackPressed:Z

    .line 450
    iget-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isAdStarted:Z

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->dfpVideoAdRunner:Lcom/flixster/android/ads/DfpVideoAdRunner;

    invoke-virtual {v0}, Lcom/flixster/android/ads/DfpVideoAdRunner;->unloadAd()V

    .line 453
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->stopPlayback()V

    .line 454
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 455
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0, v2}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setVisibility(I)V

    .line 456
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->postTrailer()V

    .line 458
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/flixster/android/activity/common/DecoratedActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 436
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedActivity;->onPause()V

    .line 437
    const-string v0, "FlxMain"

    const-string v1, "MovieTrailer.onPause"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lnet/flixster/android/MovieTrailer;->videoPosition:I

    .line 441
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->pause()V

    .line 443
    :cond_0
    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 6
    .parameter "mediaplayer"

    .prologue
    const/4 v5, 0x0

    .line 339
    const-string v0, "FlxMain"

    const-string v1, "MovieTrailer.onPrepared"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->backgroundImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 341
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->removeLoading()V

    .line 342
    iget-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isAdStarted:Z

    if-eqz v0, :cond_0

    .line 343
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isDfpVideoAvailable:Z

    .line 344
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->prerollShown()V

    .line 345
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/preroll/dfp"

    const-string v2, "Preroll - DFP"

    const-string v3, "Preroll"

    const-string v4, "Impression"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->trackEventPrerollStart()V

    .line 347
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adLabel:Landroid/widget/TextView;

    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->getRemainingTimeAdLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adVisitButton:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adVisitButton:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->scheduleSkipButton()V

    .line 352
    invoke-direct {p0}, Lnet/flixster/android/MovieTrailer;->scheduleRemainingTime()V

    .line 353
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 359
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->start()V

    .line 360
    return-void

    .line 355
    :cond_0
    invoke-static {p0}, Lcom/flixster/android/utils/VersionedViewHelper;->hideSystemNavigation(Landroid/app/Activity;)V

    .line 356
    invoke-virtual {p1, p0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 357
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adaptiveSwitchLogic:Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;

    #calls: Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->videoPrepared()V
    invoke-static {v0}, Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;->access$2(Lnet/flixster/android/MovieTrailer$AdaptiveSwitchLogic;)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 4
    .parameter "hasFocus"

    .prologue
    .line 412
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedActivity;->onWindowFocusChanged(Z)V

    .line 413
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MovieTrailer.onWindowFocusChanged hasFocus:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isPlaying:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v3}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->isPlaying()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 414
    const-string v3, " mCurrentPosition:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lnet/flixster/android/MovieTrailer;->videoPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 413
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    if-eqz p1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417
    invoke-static {p0}, Lcom/flixster/android/utils/VersionedViewHelper;->hideSystemNavigation(Landroid/app/Activity;)V

    .line 421
    :cond_0
    if-eqz p1, :cond_1

    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lnet/flixster/android/MovieTrailer;->videoPosition:I

    if-lez v1, :cond_1

    .line 422
    const-string v1, "FlxMain"

    const-string v2, "MovieTrailer.onWindowFocusChanged start"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->start()V

    .line 424
    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->getCurrentPosition()I

    move-result v1

    iget v2, p0, Lnet/flixster/android/MovieTrailer;->videoPosition:I

    if-ge v1, v2, :cond_1

    .line 425
    iget-object v1, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    iget v2, p0, Lnet/flixster/android/MovieTrailer;->videoPosition:I

    invoke-virtual {v1, v2}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->seekTo(I)V

    .line 426
    const/4 v1, 0x0

    iput v1, p0, Lnet/flixster/android/MovieTrailer;->videoPosition:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 432
    :cond_1
    :goto_0
    return-void

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "FlxMain"

    const-string v2, "MovieTrailer.onWindowFocusChanged exception"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public playAd(Ljava/lang/String;)V
    .locals 2
    .parameter "url"

    .prologue
    .line 146
    const-string v0, "FlxAd"

    const-string v1, "MovieTrailer.playAd DFP"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isAdStarted:Z

    .line 148
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->enableTracking()V

    .line 149
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0, p1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setVideoPath(Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method public removeCallback(Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V
    .locals 2
    .parameter "callback"

    .prologue
    .line 173
    const-string v0, "FlxAd"

    const-string v1, "MovieTrailer.removeCallback DFP"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0, p1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->removeCallback(Lcom/google/ads/interactivemedia/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V

    .line 175
    return-void
.end method

.method public stopAd()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 154
    const-string v0, "FlxAd"

    const-string v1, "MovieTrailer.stopAd DFP"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->stopPlayback()V

    .line 156
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->disableTracking()V

    .line 157
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->video:Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/ads/interactivemedia/demoapp/player/TrackingVideoView;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adVisitButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->adSkipButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lnet/flixster/android/MovieTrailer;->timerDecorator:Lcom/flixster/android/activity/decorator/TimerDecorator;

    invoke-virtual {v0}, Lcom/flixster/android/activity/decorator/TimerDecorator;->cancelTimer()V

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/flixster/android/MovieTrailer;->isAdStarted:Z

    .line 163
    return-void
.end method
