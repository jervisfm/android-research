.class Lnet/flixster/android/MoviesIWantToSeePage$1;
.super Ljava/lang/Object;
.source "MoviesIWantToSeePage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MoviesIWantToSeePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MoviesIWantToSeePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MoviesIWantToSeePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MoviesIWantToSeePage$1;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "view"

    .prologue
    .line 158
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;

    .line 159
    .local v0, holder:Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "MoviesIWantToSee.mUserReviewClickListener holder:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " review:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 160
    iget-object v6, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieReview:Lnet/flixster/android/model/Review;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mUser:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lnet/flixster/android/MoviesIWantToSeePage$1;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v6}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 159
    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    if-eqz v0, :cond_1

    iget-object v4, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieReview:Lnet/flixster/android/model/Review;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lnet/flixster/android/MoviesIWantToSeePage$1;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v4}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 164
    iget-object v4, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieReview:Lnet/flixster/android/model/Review;

    invoke-virtual {v4}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v3

    .line 165
    .local v3, movie:Lnet/flixster/android/model/Movie;
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v4

    const-string v5, "/mymovies/wanttosee"

    .line 166
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "User Review - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "title"

    invoke-virtual {v3, v7}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 165
    invoke-interface {v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v4, p0, Lnet/flixster/android/MoviesIWantToSeePage$1;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v4}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    iget-object v5, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieReview:Lnet/flixster/android/model/Review;

    invoke-interface {v4, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 169
    .local v1, index:I
    if-gez v1, :cond_0

    .line 170
    const/4 v1, 0x0

    .line 172
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v4, "USER_REVIEW_PAGE"

    const/4 v5, 0x0

    iget-object v6, p0, Lnet/flixster/android/MoviesIWantToSeePage$1;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    const-class v7, Lnet/flixster/android/UserReviewPage;

    invoke-direct {v2, v4, v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 173
    .local v2, intent:Landroid/content/Intent;
    const-string v4, "REVIEW_INDEX"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    const-string v4, "REVIEW_TYPE"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 175
    iget-object v4, p0, Lnet/flixster/android/MoviesIWantToSeePage$1;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    invoke-virtual {v4, v2}, Lnet/flixster/android/MoviesIWantToSeePage;->startActivity(Landroid/content/Intent;)V

    .line 178
    .end local v1           #index:I
    .end local v2           #intent:Landroid/content/Intent;
    .end local v3           #movie:Lnet/flixster/android/model/Movie;
    :cond_1
    return-void
.end method
