.class Lnet/flixster/android/MoviesIWantToSeePage$2;
.super Ljava/util/TimerTask;
.source "MoviesIWantToSeePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/MoviesIWantToSeePage;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MoviesIWantToSeePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MoviesIWantToSeePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    .line 75
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 81
    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 82
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget v2, v2, Lnet/flixster/android/model/User;->wtsCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-lez v2, :cond_1

    .line 84
    :cond_0
    :try_start_1
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;

    move-result-object v3

    #setter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2, v3}, Lnet/flixster/android/MoviesIWantToSeePage;->access$1(Lnet/flixster/android/MoviesIWantToSeePage;Lnet/flixster/android/model/User;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 91
    :cond_1
    :goto_0
    :try_start_2
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 92
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget v2, v2, Lnet/flixster/android/model/User;->wtsCount:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    if-lez v2, :cond_3

    .line 94
    :cond_2
    :try_start_3
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #getter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lnet/flixster/android/MoviesIWantToSeePage;->access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v4, 0x32

    invoke-static {v3, v4}, Lnet/flixster/android/data/ProfileDao;->getWantToSeeReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 102
    :cond_3
    :goto_1
    :try_start_4
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    #calls: Lnet/flixster/android/MoviesIWantToSeePage;->setWtsLviList()V
    invoke-static {v2}, Lnet/flixster/android/MoviesIWantToSeePage;->access$2(Lnet/flixster/android/MoviesIWantToSeePage;)V

    .line 103
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    iget-object v2, v2, Lnet/flixster/android/MoviesIWantToSeePage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 117
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    iget-object v2, v2, Lnet/flixster/android/MoviesIWantToSeePage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 119
    :goto_2
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_5
    const-string v2, "FlxMain"

    const-string v3, "MyWantToSeePage.scheduleUpdatePageTask (failed to get user data)"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 87
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    const/4 v3, 0x0

    #setter for: Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;
    invoke-static {v2, v3}, Lnet/flixster/android/MoviesIWantToSeePage;->access$1(Lnet/flixster/android/MoviesIWantToSeePage;Lnet/flixster/android/model/User;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 104
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catch_1
    move-exception v1

    .line 106
    .local v1, e:Ljava/lang/Exception;
    :try_start_6
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MoviesIWantToSeePage.ScheduleLoadItemsTask.run() mRetryCount:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    iget v4, v4, Lnet/flixster/android/MoviesIWantToSeePage;->mRetryCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 108
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    iget v3, v2, Lnet/flixster/android/MoviesIWantToSeePage;->mRetryCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lnet/flixster/android/MoviesIWantToSeePage;->mRetryCount:I

    .line 109
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    iget v2, v2, Lnet/flixster/android/MoviesIWantToSeePage;->mRetryCount:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_4

    .line 110
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    const-wide/16 v3, 0x3e8

    #calls: Lnet/flixster/android/MoviesIWantToSeePage;->ScheduleLoadItemsTask(J)V
    invoke-static {v2, v3, v4}, Lnet/flixster/android/MoviesIWantToSeePage;->access$3(Lnet/flixster/android/MoviesIWantToSeePage;J)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 117
    :goto_3
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    iget-object v2, v2, Lnet/flixster/android/MoviesIWantToSeePage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 95
    .end local v1           #e:Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 96
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    :try_start_7
    const-string v2, "FlxMain"

    const-string v3, "MyWantToSeePage.scheduleUpdatePageTask (failed to get user reviews)"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_1

    .line 116
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catchall_0
    move-exception v2

    .line 117
    iget-object v3, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    iget-object v3, v3, Lnet/flixster/android/MoviesIWantToSeePage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v3, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 118
    throw v2

    .line 112
    .restart local v1       #e:Ljava/lang/Exception;
    :cond_4
    :try_start_8
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    const/4 v3, 0x0

    iput v3, v2, Lnet/flixster/android/MoviesIWantToSeePage;->mRetryCount:I

    .line 113
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage$2;->this$0:Lnet/flixster/android/MoviesIWantToSeePage;

    iget-object v2, v2, Lnet/flixster/android/MoviesIWantToSeePage;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3
.end method
