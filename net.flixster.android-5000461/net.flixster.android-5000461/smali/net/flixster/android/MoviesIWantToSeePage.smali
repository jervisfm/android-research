.class public Lnet/flixster/android/MoviesIWantToSeePage;
.super Lnet/flixster/android/LviActivity;
.source "MoviesIWantToSeePage.java"


# instance fields
.field private isConnected:Ljava/lang/Boolean;

.field private mUser:Lnet/flixster/android/model/User;

.field private mUserReviewClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lnet/flixster/android/LviActivity;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/MoviesIWantToSeePage;->isConnected:Ljava/lang/Boolean;

    .line 156
    new-instance v0, Lnet/flixster/android/MoviesIWantToSeePage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/MoviesIWantToSeePage$1;-><init>(Lnet/flixster/android/MoviesIWantToSeePage;)V

    iput-object v0, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mUserReviewClickListener:Landroid/view/View$OnClickListener;

    .line 27
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 3
    .parameter "delay"

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/MoviesIWantToSeePage;->throbberHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 75
    new-instance v0, Lnet/flixster/android/MoviesIWantToSeePage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/MoviesIWantToSeePage$2;-><init>(Lnet/flixster/android/MoviesIWantToSeePage;)V

    .line 122
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    const-string v1, "FlxMain"

    const-string v2, "MoviesIWantToSeePage.ScheduleLoadItemsTask() loading item"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v1, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v1, v0, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :cond_0
    monitor-exit p0

    return-void

    .line 73
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method static synthetic access$0(Lnet/flixster/android/MoviesIWantToSeePage;)Lnet/flixster/android/model/User;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/MoviesIWantToSeePage;Lnet/flixster/android/model/User;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28
    iput-object p1, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/MoviesIWantToSeePage;)V
    .locals 0
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, Lnet/flixster/android/MoviesIWantToSeePage;->setWtsLviList()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/MoviesIWantToSeePage;J)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/MoviesIWantToSeePage;->ScheduleLoadItemsTask(J)V

    return-void
.end method

.method private setWtsLviList()V
    .locals 5

    .prologue
    .line 129
    const-string v3, "FlxMain"

    const-string v4, "MoviesIWantToSeePage.setPopularLviList "

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v3, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 133
    invoke-virtual {p0}, Lnet/flixster/android/MoviesIWantToSeePage;->destroyExistingLviAd()V

    .line 134
    new-instance v3, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v3, p0, Lnet/flixster/android/MoviesIWantToSeePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 135
    iget-object v3, p0, Lnet/flixster/android/MoviesIWantToSeePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v4, "MoviesIWantToSee"

    iput-object v4, v3, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 137
    iget-object v3, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v4, p0, Lnet/flixster/android/MoviesIWantToSeePage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    new-instance v1, Lnet/flixster/android/lvi/LviUserProfile;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviUserProfile;-><init>()V

    .line 140
    .local v1, lviUserProfile:Lnet/flixster/android/lvi/LviUserProfile;
    iget-object v3, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;

    iput-object v3, v1, Lnet/flixster/android/lvi/LviUserProfile;->mUser:Lnet/flixster/android/model/User;

    .line 141
    iget-object v3, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v3, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mUser:Lnet/flixster/android/model/User;

    iget-object v3, v3, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 154
    return-void

    .line 143
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Review;

    .line 144
    .local v2, review:Lnet/flixster/android/model/Review;
    new-instance v0, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 145
    .local v0, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    invoke-virtual {v2}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v4

    iput-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 147
    iput-object v2, v0, Lnet/flixster/android/lvi/LviMovie;->mReview:Lnet/flixster/android/model/Review;

    .line 148
    iget-object v4, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mUserReviewClickListener:Landroid/view/View$OnClickListener;

    iput-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->mDetailsClick:Landroid/view/View$OnClickListener;

    .line 149
    iget-object v4, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 62
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 63
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/MoviesIWantToSeePage;->isConnected:Ljava/lang/Boolean;

    .line 64
    invoke-virtual {p0}, Lnet/flixster/android/MoviesIWantToSeePage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 65
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "net.flixster.IsConnected"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 70
    .end local v0           #intent:Landroid/content/Intent;
    :goto_0
    return-void

    .line 67
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lnet/flixster/android/MoviesIWantToSeePage;->setResult(I)V

    .line 68
    invoke-virtual {p0}, Lnet/flixster/android/MoviesIWantToSeePage;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedState"

    .prologue
    const/4 v4, 0x3

    .line 33
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lnet/flixster/android/MoviesIWantToSeePage;->createActionBar()V

    .line 35
    const v2, 0x7f0c006d

    invoke-virtual {p0, v2}, Lnet/flixster/android/MoviesIWantToSeePage;->setActionBarTitle(I)V

    .line 37
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    const-string v3, "MoviesIWantToSeeStickyTop"

    invoke-virtual {v2, v3}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 38
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    const-string v3, "MoviesIWantToSeeStickyBottom"

    invoke-virtual {v2, v3}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lnet/flixster/android/MoviesIWantToSeePage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 41
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_1

    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage;->isConnected:Ljava/lang/Boolean;

    if-nez v2, :cond_1

    .line 42
    const-string v2, "FlxMain"

    const-string v3, "MyWantToSeePage.onCreate "

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v2, "net.flixster.IsConnected"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage;->isConnected:Ljava/lang/Boolean;

    .line 44
    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage;->isConnected:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnet/flixster/android/MoviesIWantToSeePage;->isConnected:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 45
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/ConnectRatePage;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 46
    .local v0, connectRate:Landroid/content/Intent;
    const-string v2, "net.flixster.RequestCode"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 47
    invoke-virtual {p0, v0, v4}, Lnet/flixster/android/MoviesIWantToSeePage;->startActivityForResult(Landroid/content/Intent;I)V

    .line 50
    .end local v0           #connectRate:Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 183
    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 54
    invoke-super {p0}, Lnet/flixster/android/LviActivity;->onResume()V

    .line 56
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/wanttosee"

    const-string v2, "My Movies - Want To See"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lnet/flixster/android/MoviesIWantToSeePage;->ScheduleLoadItemsTask(J)V

    .line 58
    return-void
.end method
