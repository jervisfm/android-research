.class Lnet/flixster/android/MyMoviesPage$1;
.super Landroid/os/Handler;
.source "MyMoviesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 370
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 15
    .parameter "msg"

    .prologue
    .line 372
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsHeader:Landroid/widget/TextView;
    invoke-static {v10}, Lnet/flixster/android/MyMoviesPage;->access$0(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 373
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsLayout:Landroid/widget/LinearLayout;
    invoke-static {v10}, Lnet/flixster/android/MyMoviesPage;->access$1(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 374
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsLayout:Landroid/widget/LinearLayout;
    invoke-static {v10}, Lnet/flixster/android/MyMoviesPage;->access$1(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 375
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsLayout:Landroid/widget/LinearLayout;
    invoke-static {v10}, Lnet/flixster/android/MyMoviesPage;->access$1(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v10

    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->myMoviesOnClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v11}, Lnet/flixster/android/MyMoviesPage;->access$2(Lnet/flixster/android/MyMoviesPage;)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 376
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsLayout:Landroid/widget/LinearLayout;
    invoke-static {v10}, Lnet/flixster/android/MyMoviesPage;->access$1(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 378
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v7

    .line 379
    .local v7, user:Lnet/flixster/android/model/User;
    iget-object v2, v7, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    .line 380
    .local v2, friends:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/User;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 381
    .local v3, friendsSize:I
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsHeader:Landroid/widget/TextView;
    invoke-static {v10}, Lnet/flixster/android/MyMoviesPage;->access$0(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v10

    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v11}, Lnet/flixster/android/MyMoviesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c013f

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    const/4 v9, 0x0

    .line 384
    .local v9, viewCount:I
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v10}, Lnet/flixster/android/MyMoviesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0045

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 385
    .local v5, imageDimensions:I
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 386
    .local v6, params:Landroid/widget/LinearLayout$LayoutParams;
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v10}, Lnet/flixster/android/MyMoviesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a002c

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iput v10, v6, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 387
    const/4 v4, 0x0

    .local v4, i:I
    :goto_0
    if-lt v4, v3, :cond_1

    .line 404
    :cond_0
    return-void

    .line 388
    :cond_1
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/User;

    .line 389
    .local v1, friend:Lnet/flixster/android/model/User;
    const/4 v10, 0x6

    if-ge v9, v10, :cond_0

    .line 391
    iget-object v10, v1, Lnet/flixster/android/model/User;->profileImage:Ljava/lang/String;

    const-string v11, "user.none"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 392
    new-instance v8, Landroid/widget/ImageView;

    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-direct {v8, v10}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 393
    .local v8, userIcon:Landroid/widget/ImageView;
    invoke-virtual {v8, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 394
    sget-object v10, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 396
    invoke-virtual {v1, v8}, Lnet/flixster/android/model/User;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 397
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 398
    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 400
    :cond_2
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage$1;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsLayout:Landroid/widget/LinearLayout;
    invoke-static {v10}, Lnet/flixster/android/MyMoviesPage;->access$1(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v10

    invoke-virtual {v10, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 401
    add-int/lit8 v9, v9, 0x1

    .line 387
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v8           #userIcon:Landroid/widget/ImageView;
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
