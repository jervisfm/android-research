.class Lnet/flixster/android/MyMoviesPage$10;
.super Ljava/lang/Object;
.source "MyMoviesPage.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$10;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter "v"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 801
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage$10;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsGridView:Landroid/widget/GridView;
    invoke-static {v1}, Lnet/flixster/android/MyMoviesPage;->access$23(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 802
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage$10;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsMovies:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/MyMoviesPage;->access$22(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .line 807
    .local v0, movie:Lnet/flixster/android/model/Movie;
    :goto_0
    instance-of v1, v0, Lnet/flixster/android/model/Season;

    if-eqz v1, :cond_1

    .line 808
    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v1

    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage$10;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-static {v1, v2, v3}, Lnet/flixster/android/Starter;->launchSeasonDetail(JLandroid/content/Context;)V

    .line 812
    :goto_1
    return-void

    .line 804
    .end local v0           #movie:Lnet/flixster/android/model/Movie;
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage$10;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedMovies:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/MyMoviesPage;->access$27(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .restart local v0       #movie:Lnet/flixster/android/model/Movie;
    goto :goto_0

    .line 810
    :cond_1
    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v1

    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage$10;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-static {v1, v2, v3}, Lnet/flixster/android/Starter;->launchMovieDetail(JLandroid/content/Context;)V

    goto :goto_1
.end method
