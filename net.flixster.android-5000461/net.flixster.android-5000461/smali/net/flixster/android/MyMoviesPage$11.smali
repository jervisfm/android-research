.class Lnet/flixster/android/MyMoviesPage$11;
.super Ljava/lang/Object;
.source "MyMoviesPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .parameter "v"

    .prologue
    const/4 v6, 0x0

    .line 820
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v8, 0x1

    .line 821
    .local v8, isLoggedIn:Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 863
    :goto_1
    return-void

    .end local v8           #isLoggedIn:Z
    :cond_0
    move v8, v6

    .line 820
    goto :goto_0

    .line 823
    .restart local v8       #isLoggedIn:Z
    :sswitch_0
    new-instance v7, Landroid/content/Intent;

    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    const-class v1, Lnet/flixster/android/FriendsPage;

    invoke-direct {v7, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 824
    .local v7, intent:Landroid/content/Intent;
    const-string v0, "net.flixster.IsConnected"

    invoke-virtual {v7, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 825
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v0, v7}, Lnet/flixster/android/MyMoviesPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 828
    .end local v7           #intent:Landroid/content/Intent;
    :sswitch_1
    new-instance v7, Landroid/content/Intent;

    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    const-class v1, Lnet/flixster/android/FriendsRatedPage;

    invoke-direct {v7, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 829
    .restart local v7       #intent:Landroid/content/Intent;
    const-string v0, "net.flixster.IsConnected"

    invoke-virtual {v7, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 830
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v0, v7}, Lnet/flixster/android/MyMoviesPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 834
    .end local v7           #intent:Landroid/content/Intent;
    :sswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/msk/promo"

    const-string v2, "MSK Promo"

    const-string v3, "MskPromo"

    const-string v4, "Click"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-static {v0}, Lnet/flixster/android/Starter;->launchMsk(Landroid/content/Context;)V

    goto :goto_1

    .line 839
    :sswitch_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/rewards"

    const-string v2, "Flixster Rewards"

    const-string v3, "FlixsterRewards"

    const-string v4, "RewardsPromo"

    .line 840
    const-string v5, "Click"

    .line 839
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 841
    new-instance v7, Landroid/content/Intent;

    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    const-class v1, Lnet/flixster/android/RewardsPage;

    invoke-direct {v7, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 842
    .restart local v7       #intent:Landroid/content/Intent;
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v0, v7}, Lnet/flixster/android/MyMoviesPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 846
    .end local v7           #intent:Landroid/content/Intent;
    :sswitch_4
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/facebook/login"

    const-string v2, "My Movies - Facebook Login"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    new-instance v7, Landroid/content/Intent;

    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    const-class v1, Lnet/flixster/android/FacebookAuth;

    invoke-direct {v7, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 848
    .restart local v7       #intent:Landroid/content/Intent;
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v0, v7}, Lnet/flixster/android/MyMoviesPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 852
    .end local v7           #intent:Landroid/content/Intent;
    :sswitch_5
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/flixster/login"

    const-string v2, "My Movies - Flixster Login"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    new-instance v7, Landroid/content/Intent;

    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    const-class v1, Lnet/flixster/android/FlixsterLoginPage;

    invoke-direct {v7, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 854
    .restart local v7       #intent:Landroid/content/Intent;
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v0, v7}, Lnet/flixster/android/MyMoviesPage;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 858
    .end local v7           #intent:Landroid/content/Intent;
    :sswitch_6
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/flixster/register"

    const-string v2, "My Movies - Flixster Register"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->registerAccount()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    const v2, 0x7f0c01ba

    invoke-virtual {v1, v2}, Lnet/flixster/android/MyMoviesPage;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 860
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$11;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 859
    invoke-static {v0, v1, v2}, Lnet/flixster/android/Starter;->launchFlxHtmlPage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 821
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0700dc -> :sswitch_1
        0x7f070198 -> :sswitch_2
        0x7f07019d -> :sswitch_3
        0x7f0701a3 -> :sswitch_4
        0x7f0701a4 -> :sswitch_5
        0x7f0701a5 -> :sswitch_6
        0x7f0701b7 -> :sswitch_0
    .end sparse-switch
.end method
