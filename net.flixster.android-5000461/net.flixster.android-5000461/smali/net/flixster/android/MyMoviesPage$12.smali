.class Lnet/flixster/android/MyMoviesPage$12;
.super Ljava/lang/Object;
.source "MyMoviesPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    const/4 v4, 0x1

    .line 875
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v1

    .line 876
    .local v1, user:Lnet/flixster/android/model/User;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 904
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    #calls: Lnet/flixster/android/MyMoviesPage;->buttonShade()V
    invoke-static {v2}, Lnet/flixster/android/MyMoviesPage;->access$40(Lnet/flixster/android/MyMoviesPage;)V

    .line 905
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    #calls: Lnet/flixster/android/MyMoviesPage;->trackPage()V
    invoke-static {v2}, Lnet/flixster/android/MyMoviesPage;->access$41(Lnet/flixster/android/MyMoviesPage;)V

    .line 906
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    #calls: Lnet/flixster/android/MyMoviesPage;->initializeStaticViews()V
    invoke-static {v2}, Lnet/flixster/android/MyMoviesPage;->access$42(Lnet/flixster/android/MyMoviesPage;)V

    .line 907
    return-void

    .line 878
    :pswitch_1
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    #setter for: Lnet/flixster/android/MyMoviesPage;->mNavSelect:I
    invoke-static {v2, v4}, Lnet/flixster/android/MyMoviesPage;->access$37(Lnet/flixster/android/MyMoviesPage;I)V

    .line 879
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    #calls: Lnet/flixster/android/MyMoviesPage;->trackPromoImpressions()V
    invoke-static {v2}, Lnet/flixster/android/MyMoviesPage;->access$38(Lnet/flixster/android/MyMoviesPage;)V

    goto :goto_0

    .line 882
    :pswitch_2
    iget v2, v1, Lnet/flixster/android/model/User;->wtsCount:I

    if-nez v2, :cond_0

    .line 883
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    const-class v3, Lnet/flixster/android/QuickRatePage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 884
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "KEY_IS_WTS"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 885
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v2, v0}, Lnet/flixster/android/MyMoviesPage;->startActivity(Landroid/content/Intent;)V

    .line 889
    .end local v0           #intent:Landroid/content/Intent;
    :goto_1
    const/4 v2, 0x0

    invoke-static {v2}, Lnet/flixster/android/MyMoviesPage;->access$39(Ljava/lang/Thread;)V

    goto :goto_0

    .line 887
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    const/4 v3, 0x2

    #setter for: Lnet/flixster/android/MyMoviesPage;->mNavSelect:I
    invoke-static {v2, v3}, Lnet/flixster/android/MyMoviesPage;->access$37(Lnet/flixster/android/MyMoviesPage;I)V

    goto :goto_1

    .line 892
    :pswitch_3
    iget v2, v1, Lnet/flixster/android/model/User;->ratingCount:I

    if-nez v2, :cond_1

    .line 893
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    const-class v3, Lnet/flixster/android/QuickRatePage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 894
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v2, "KEY_IS_WTS"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 895
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v2, v0}, Lnet/flixster/android/MyMoviesPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 897
    .end local v0           #intent:Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    const/4 v3, 0x3

    #setter for: Lnet/flixster/android/MyMoviesPage;->mNavSelect:I
    invoke-static {v2, v3}, Lnet/flixster/android/MyMoviesPage;->access$37(Lnet/flixster/android/MyMoviesPage;I)V

    goto :goto_0

    .line 901
    :pswitch_4
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$12;->this$0:Lnet/flixster/android/MyMoviesPage;

    const/4 v3, 0x4

    #setter for: Lnet/flixster/android/MyMoviesPage;->mNavSelect:I
    invoke-static {v2, v3}, Lnet/flixster/android/MyMoviesPage;->access$37(Lnet/flixster/android/MyMoviesPage;I)V

    goto :goto_0

    .line 876
    nop

    :pswitch_data_0
    .packed-switch 0x7f0701e1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
