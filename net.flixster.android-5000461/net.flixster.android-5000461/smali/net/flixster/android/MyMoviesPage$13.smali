.class Lnet/flixster/android/MyMoviesPage$13;
.super Ljava/lang/Object;
.source "MyMoviesPage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/MyMoviesPage;->initializeProgressMonitorThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$13;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 550
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 551
    .local v0, currentThread:Ljava/lang/Thread;
    :cond_0
    :goto_0
    invoke-static {}, Lnet/flixster/android/MyMoviesPage;->access$43()Ljava/lang/Thread;

    move-result-object v7

    if-eq v7, v0, :cond_1

    .line 578
    return-void

    .line 553
    :cond_1
    const-wide/16 v7, 0x7d0

    :try_start_0
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V

    .line 554
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    iget-object v7, p0, Lnet/flixster/android/MyMoviesPage$13;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;
    invoke-static {v7}, Lnet/flixster/android/MyMoviesPage;->access$44(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    if-lt v2, v7, :cond_2

    .line 564
    const/4 v2, 0x0

    :goto_2
    iget-object v7, p0, Lnet/flixster/android/MyMoviesPage$13;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->myMoviesCollectionLayout:Landroid/widget/LinearLayout;
    invoke-static {v7}, Lnet/flixster/android/MyMoviesPage;->access$45(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 565
    iget-object v7, p0, Lnet/flixster/android/MyMoviesPage$13;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->myMoviesCollectionLayout:Landroid/widget/LinearLayout;
    invoke-static {v7}, Lnet/flixster/android/MyMoviesPage;->access$45(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 566
    .local v4, ll:Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    .local v3, j:I
    :goto_3
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    if-lt v3, v7, :cond_5

    .line 564
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 555
    .end local v3           #j:I
    .end local v4           #ll:Landroid/widget/LinearLayout;
    :cond_2
    iget-object v7, p0, Lnet/flixster/android/MyMoviesPage$13;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;
    invoke-static {v7}, Lnet/flixster/android/MyMoviesPage;->access$44(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 556
    .restart local v4       #ll:Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    .restart local v3       #j:I
    :goto_4
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    if-lt v3, v7, :cond_3

    .line 554
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 557
    :cond_3
    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 558
    .local v5, posterView:Landroid/view/View;
    instance-of v7, v5, Lnet/flixster/android/MovieCollectionItem;

    if-eqz v7, :cond_4

    .line 559
    check-cast v5, Lnet/flixster/android/MovieCollectionItem;

    .end local v5           #posterView:Landroid/view/View;
    iget-object v6, v5, Lnet/flixster/android/MovieCollectionItem;->progressHandler:Landroid/os/Handler;

    .line 560
    .local v6, progressHandler:Landroid/os/Handler;
    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 556
    .end local v6           #progressHandler:Landroid/os/Handler;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 567
    :cond_5
    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 568
    .restart local v5       #posterView:Landroid/view/View;
    instance-of v7, v5, Lnet/flixster/android/MovieCollectionItem;

    if-eqz v7, :cond_6

    .line 569
    check-cast v5, Lnet/flixster/android/MovieCollectionItem;

    .end local v5           #posterView:Landroid/view/View;
    iget-object v6, v5, Lnet/flixster/android/MovieCollectionItem;->progressHandler:Landroid/os/Handler;

    .line 570
    .restart local v6       #progressHandler:Landroid/os/Handler;
    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    .end local v6           #progressHandler:Landroid/os/Handler;
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 574
    .end local v2           #i:I
    .end local v3           #j:I
    .end local v4           #ll:Landroid/widget/LinearLayout;
    :catch_0
    move-exception v1

    .line 575
    .local v1, e:Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0
.end method
