.class Lnet/flixster/android/MyMoviesPage$2;
.super Landroid/os/Handler;
.source "MyMoviesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 407
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .parameter "msg"

    .prologue
    const/4 v10, 0x3

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 409
    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsActivityHeader:Landroid/widget/TextView;
    invoke-static {v9}, Lnet/flixster/android/MyMoviesPage;->access$3(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 410
    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsActivityLayout:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lnet/flixster/android/MyMoviesPage;->access$4(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 411
    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsActivityLayout:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lnet/flixster/android/MyMoviesPage;->access$4(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 412
    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsActivityLayout:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lnet/flixster/android/MyMoviesPage;->access$4(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 414
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v8

    .line 415
    .local v8, user:Lnet/flixster/android/model/User;
    iget-object v6, v8, Lnet/flixster/android/model/User;->friendsActivity:Ljava/util/List;

    .line 416
    .local v6, reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 417
    .local v7, reviewsSize:I
    invoke-static {v7, v10}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 418
    .local v1, displaySize:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-lt v2, v1, :cond_2

    .line 425
    if-le v7, v10, :cond_0

    .line 426
    new-instance v3, Lcom/flixster/android/view/LoadMore;

    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-direct {v3, v9}, Lcom/flixster/android/view/LoadMore;-><init>(Landroid/content/Context;)V

    .line 427
    .local v3, more:Lcom/flixster/android/view/LoadMore;
    const v9, 0x7f0700dc

    invoke-virtual {v3, v9}, Lcom/flixster/android/view/LoadMore;->setId(I)V

    .line 428
    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsActivityLayout:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lnet/flixster/android/MyMoviesPage;->access$4(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 429
    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v9}, Lnet/flixster/android/MyMoviesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c007f

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Lcom/flixster/android/view/LoadMore;->load(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    invoke-virtual {v3, v12}, Lcom/flixster/android/view/LoadMore;->setFocusable(Z)V

    .line 431
    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->myMoviesOnClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v9}, Lnet/flixster/android/MyMoviesPage;->access$2(Lnet/flixster/android/MyMoviesPage;)Landroid/view/View$OnClickListener;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/flixster/android/view/LoadMore;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 433
    .end local v3           #more:Lcom/flixster/android/view/LoadMore;
    :cond_0
    if-nez v7, :cond_1

    .line 434
    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    const v10, 0x7f0701ba

    invoke-virtual {v9, v10}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 435
    .local v4, placeholder:Landroid/widget/TextView;
    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 437
    .end local v4           #placeholder:Landroid/widget/TextView;
    :cond_1
    return-void

    .line 419
    :cond_2
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Review;

    .line 420
    .local v5, review:Lnet/flixster/android/model/Review;
    new-instance v0, Lcom/flixster/android/view/FriendActivity;

    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-direct {v0, v9}, Lcom/flixster/android/view/FriendActivity;-><init>(Landroid/content/Context;)V

    .line 421
    .local v0, activity:Lcom/flixster/android/view/FriendActivity;
    invoke-virtual {v0, v12}, Lcom/flixster/android/view/FriendActivity;->setFocusable(Z)V

    .line 422
    iget-object v9, p0, Lnet/flixster/android/MyMoviesPage$2;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->friendsActivityLayout:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lnet/flixster/android/MyMoviesPage;->access$4(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;

    move-result-object v9

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 423
    invoke-virtual {v0, v5}, Lcom/flixster/android/view/FriendActivity;->load(Lnet/flixster/android/model/Review;)V

    .line 418
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
