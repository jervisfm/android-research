.class Lnet/flixster/android/MyMoviesPage$3;
.super Landroid/os/Handler;
.source "MyMoviesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 440
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .parameter "msg"

    .prologue
    const/16 v7, 0x8

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 442
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->collectionThrobber:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$5(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ProgressBar;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 445
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v4

    .line 446
    .local v4, user:Lnet/flixster/android/model/User;
    if-nez v4, :cond_0

    .line 513
    :goto_0
    return-void

    .line 449
    :cond_0
    invoke-virtual {v4}, Lnet/flixster/android/model/User;->getLockerNonEpisodesCount()I

    move-result v5

    iput v5, v4, Lnet/flixster/android/model/User;->collectionsCount:I

    .line 450
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->collectedCountView:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$6(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v5

    iget v6, v4, Lnet/flixster/android/model/User;->collectionsCount:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 452
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->lastIsRewardsEligibleState:Z
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$7(Lnet/flixster/android/MyMoviesPage;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 453
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #calls: Lnet/flixster/android/MyMoviesPage;->updateRewardsLayout()V
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$8(Lnet/flixster/android/MyMoviesPage;)V

    .line 456
    :cond_1
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->rentalRights:Ljava/util/List;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$9(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 457
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->movieAndSeasonRights:Ljava/util/List;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$10(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 458
    const/4 v1, 0x0

    .local v1, insertionIndex:I
    const/4 v0, 0x0

    .local v0, downloadInsertionIndex:I
    const/4 v3, 0x0

    .line 459
    .local v3, unfulfillableCount:I
    invoke-virtual {v4}, Lnet/flixster/android/model/User;->getLockerRights()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_5

    .line 478
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #calls: Lnet/flixster/android/MyMoviesPage;->populateRentalRights()V
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$11(Lnet/flixster/android/MyMoviesPage;)V

    .line 479
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #calls: Lnet/flixster/android/MyMoviesPage;->populateMovieAndSeasonRights()V
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$12(Lnet/flixster/android/MyMoviesPage;)V

    .line 481
    if-lez v3, :cond_c

    .line 482
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->unfulfillablePanel:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$13(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v6

    .line 483
    if-ne v3, v11, :cond_b

    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    const v7, 0x7f0c0073

    invoke-virtual {v5, v7}, Lnet/flixster/android/MyMoviesPage;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_2
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 485
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->unfulfillablePanel:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$13(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v5

    iget-object v6, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->unfulfillablePanelClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v6}, Lnet/flixster/android/MyMoviesPage;->access$14(Lnet/flixster/android/MyMoviesPage;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 486
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->unfulfillablePanel:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$13(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 487
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->unfulfillableDivider:Landroid/widget/ImageView;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$15(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 493
    :goto_3
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #calls: Lnet/flixster/android/MyMoviesPage;->initializeProgressMonitorThread()V
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$16(Lnet/flixster/android/MyMoviesPage;)V

    .line 496
    invoke-static {}, Lnet/flixster/android/MyMoviesPage;->access$17()Z

    move-result v5

    if-nez v5, :cond_3

    .line 497
    invoke-static {v11}, Lnet/flixster/android/MyMoviesPage;->access$18(Z)V

    .line 498
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v5

    invoke-interface {v5}, Lcom/flixster/android/drm/PlaybackManager;->isRooted()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 499
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-static {v5}, Lcom/flixster/android/view/DialogBuilder;->showStreamUnsupportedOnRootedDevices(Landroid/app/Activity;)V

    .line 508
    :cond_3
    :goto_4
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->hasMigratedToRights()Z

    move-result v5

    if-nez v5, :cond_4

    .line 509
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    const v6, 0x3b9acacf

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lnet/flixster/android/MyMoviesPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 512
    :cond_4
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #calls: Lnet/flixster/android/MyMoviesPage;->invalidActionBarItems()V
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$19(Lnet/flixster/android/MyMoviesPage;)V

    goto/16 :goto_0

    .line 459
    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/LockerRight;

    .line 460
    .local v2, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isMovie()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-boolean v6, v2, Lnet/flixster/android/model/LockerRight;->isStreamingSupported:Z

    if-nez v6, :cond_7

    iget-boolean v6, v2, Lnet/flixster/android/model/LockerRight;->isDownloadSupported:Z

    if-nez v6, :cond_7

    :cond_6
    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 461
    :cond_7
    iget-boolean v6, v2, Lnet/flixster/android/model/LockerRight;->isRental:Z

    if-eqz v6, :cond_8

    .line 462
    iget-object v6, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->rentalRights:Ljava/util/List;
    invoke-static {v6}, Lnet/flixster/android/MyMoviesPage;->access$9(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 464
    :cond_8
    invoke-static {v2}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(Lnet/flixster/android/model/LockerRight;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 465
    iget-object v6, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->movieAndSeasonRights:Ljava/util/List;
    invoke-static {v6}, Lnet/flixster/android/MyMoviesPage;->access$10(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v0, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 466
    add-int/lit8 v0, v0, 0x1

    .line 467
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 469
    :cond_9
    iget-object v6, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->movieAndSeasonRights:Ljava/util/List;
    invoke-static {v6}, Lnet/flixster/android/MyMoviesPage;->access$10(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 470
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 473
    :cond_a
    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isUnfulfillable()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 474
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 484
    .end local v2           #right:Lnet/flixster/android/model/LockerRight;
    :cond_b
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    const v7, 0x7f0c0074

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v5, v7, v8}, Lnet/flixster/android/MyMoviesPage;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 489
    :cond_c
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->unfulfillablePanel:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$13(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 490
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->unfulfillableDivider:Landroid/widget/ImageView;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$15(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 500
    :cond_d
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v5

    invoke-interface {v5}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceNonWifiStreamBlacklisted()Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isWifi()Z

    move-result v5

    if-nez v5, :cond_e

    .line 501
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-static {v5}, Lcom/flixster/android/view/DialogBuilder;->showNonWifiStreamUnsupported(Landroid/app/Activity;)V

    goto/16 :goto_4

    .line 502
    :cond_e
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v5

    invoke-interface {v5}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceStreamBlacklisted()Z

    move-result v5

    if-nez v5, :cond_f

    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v5

    invoke-interface {v5}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceStreamingCompatible()Z

    move-result v5

    if-nez v5, :cond_3

    .line 503
    :cond_f
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$3;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-static {v5}, Lcom/flixster/android/view/DialogBuilder;->showStreamUnsupported(Landroid/app/Activity;)V

    goto/16 :goto_4
.end method
