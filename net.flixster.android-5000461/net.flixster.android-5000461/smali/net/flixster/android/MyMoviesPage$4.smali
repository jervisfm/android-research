.class Lnet/flixster/android/MyMoviesPage$4;
.super Landroid/os/Handler;
.source "MyMoviesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 686
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .parameter "msg"

    .prologue
    const/4 v8, 0x1

    .line 688
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsThrobber:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$20(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ProgressBar;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 689
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v2

    .line 690
    .local v2, user:Lnet/flixster/android/model/User;
    if-nez v2, :cond_0

    .line 710
    :goto_0
    return-void

    .line 693
    :cond_0
    iget-object v3, v2, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    .line 694
    .local v3, wtsReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    iget v4, v2, Lnet/flixster/android/model/User;->wtsCount:I

    const/16 v5, 0x32

    if-gt v4, v5, :cond_1

    .line 695
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v2, Lnet/flixster/android/model/User;->wtsCount:I

    .line 696
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsCountView:Landroid/widget/TextView;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$21(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v4

    iget v5, v2, Lnet/flixster/android/model/User;->wtsCount:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 699
    :cond_1
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsMovies:Ljava/util/List;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$22(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 700
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 705
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsGridView:Landroid/widget/GridView;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$23(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;

    move-result-object v4

    new-instance v5, Lnet/flixster/android/MovieGridViewAdapter;

    iget-object v6, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    iget-object v7, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsMovies:Ljava/util/List;
    invoke-static {v7}, Lnet/flixster/android/MyMoviesPage;->access$22(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v7

    .line 706
    invoke-direct {v5, v6, v7, v8}, Lnet/flixster/android/MovieGridViewAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    .line 705
    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 707
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsGridView:Landroid/widget/GridView;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$23(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/GridView;->setClickable(Z)V

    .line 708
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsGridView:Landroid/widget/GridView;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$23(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;

    move-result-object v4

    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->movieDetailClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$24(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 709
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsGridView:Landroid/widget/GridView;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$23(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setVisibility(I)V

    goto :goto_0

    .line 700
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Review;

    .line 701
    .local v1, review:Lnet/flixster/android/model/Review;
    invoke-virtual {v1}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v0

    .line 702
    .local v0, movie:Lnet/flixster/android/model/Movie;
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$4;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsMovies:Ljava/util/List;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$22(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
