.class Lnet/flixster/android/MyMoviesPage$5;
.super Landroid/os/Handler;
.source "MyMoviesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 713
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .parameter "msg"

    .prologue
    .line 715
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedThrobber:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$25(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ProgressBar;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 716
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v3

    .line 717
    .local v3, user:Lnet/flixster/android/model/User;
    if-nez v3, :cond_0

    .line 737
    :goto_0
    return-void

    .line 720
    :cond_0
    iget-object v1, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    .line 721
    .local v1, ratedReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    iget v4, v3, Lnet/flixster/android/model/User;->ratingCount:I

    const/16 v5, 0x32

    if-gt v4, v5, :cond_1

    .line 722
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v3, Lnet/flixster/android/model/User;->ratingCount:I

    .line 723
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratingsCountView:Landroid/widget/TextView;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$26(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v4

    iget v5, v3, Lnet/flixster/android/model/User;->ratingCount:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 726
    :cond_1
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedMovies:Ljava/util/List;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$27(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 727
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 732
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedGridView:Landroid/widget/GridView;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$28(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;

    move-result-object v4

    new-instance v5, Lnet/flixster/android/MovieGridViewAdapter;

    iget-object v6, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    iget-object v7, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedMovies:Ljava/util/List;
    invoke-static {v7}, Lnet/flixster/android/MyMoviesPage;->access$27(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v7

    .line 733
    const/4 v8, 0x2

    invoke-direct {v5, v6, v7, v8}, Lnet/flixster/android/MovieGridViewAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    .line 732
    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 734
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedGridView:Landroid/widget/GridView;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$28(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setClickable(Z)V

    .line 735
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedGridView:Landroid/widget/GridView;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$28(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;

    move-result-object v4

    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->movieDetailClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$24(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 736
    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedGridView:Landroid/widget/GridView;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$28(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setVisibility(I)V

    goto :goto_0

    .line 727
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Review;

    .line 728
    .local v2, review:Lnet/flixster/android/model/Review;
    invoke-virtual {v2}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v0

    .line 729
    .local v0, movie:Lnet/flixster/android/model/Movie;
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$5;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedMovies:Ljava/util/List;
    invoke-static {v5}, Lnet/flixster/android/MyMoviesPage;->access$27(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
