.class Lnet/flixster/android/MyMoviesPage$6;
.super Landroid/os/Handler;
.source "MyMoviesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$6;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 740
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 742
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v0

    .line 743
    .local v0, currSocialUser:Lnet/flixster/android/model/User;
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUserId()Ljava/lang/String;

    move-result-object v1

    .line 744
    .local v1, currSocialUserId:Ljava/lang/String;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MyMoviesPage.getUserSuccessHandler curr="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " last="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage$6;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->lastSocialUserId:Ljava/lang/String;
    invoke-static {v4}, Lnet/flixster/android/MyMoviesPage;->access$29(Lnet/flixster/android/MyMoviesPage;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    if-eqz v1, :cond_0

    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$6;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->lastSocialUserId:Ljava/lang/String;
    invoke-static {v2}, Lnet/flixster/android/MyMoviesPage;->access$29(Lnet/flixster/android/MyMoviesPage;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 746
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$6;->this$0:Lnet/flixster/android/MyMoviesPage;

    #setter for: Lnet/flixster/android/MyMoviesPage;->lastSocialUserId:Ljava/lang/String;
    invoke-static {v2, v1}, Lnet/flixster/android/MyMoviesPage;->access$30(Lnet/flixster/android/MyMoviesPage;Ljava/lang/String;)V

    .line 747
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$6;->this$0:Lnet/flixster/android/MyMoviesPage;

    iget-boolean v3, v0, Lnet/flixster/android/model/User;->isMskEligible:Z

    #setter for: Lnet/flixster/android/MyMoviesPage;->lastIsMskEligibleState:Z
    invoke-static {v2, v3}, Lnet/flixster/android/MyMoviesPage;->access$31(Lnet/flixster/android/MyMoviesPage;Z)V

    .line 748
    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage$6;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-virtual {v0}, Lnet/flixster/android/model/User;->isRewardsEligible()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, v0, Lnet/flixster/android/model/User;->isMskEligible:Z

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    #setter for: Lnet/flixster/android/MyMoviesPage;->lastIsRewardsEligibleState:Z
    invoke-static {v3, v2}, Lnet/flixster/android/MyMoviesPage;->access$32(Lnet/flixster/android/MyMoviesPage;Z)V

    .line 749
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$6;->this$0:Lnet/flixster/android/MyMoviesPage;

    #calls: Lnet/flixster/android/MyMoviesPage;->initializeSocialViews()V
    invoke-static {v2}, Lnet/flixster/android/MyMoviesPage;->access$33(Lnet/flixster/android/MyMoviesPage;)V

    .line 751
    :cond_0
    return-void

    .line 748
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
