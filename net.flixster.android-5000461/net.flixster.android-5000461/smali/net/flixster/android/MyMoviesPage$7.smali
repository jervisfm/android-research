.class Lnet/flixster/android/MyMoviesPage$7;
.super Landroid/os/Handler;
.source "MyMoviesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$7;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 754
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    const/16 v1, 0x8

    .line 756
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 772
    :goto_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lnet/flixster/android/data/DaoException;

    if-eqz v0, :cond_0

    .line 773
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lnet/flixster/android/data/DaoException;

    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage$7;->this$0:Lnet/flixster/android/MyMoviesPage;

    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage$7;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;
    invoke-static {v2}, Lnet/flixster/android/MyMoviesPage;->access$36(Lnet/flixster/android/MyMoviesPage;)Lcom/flixster/android/activity/decorator/DialogDecorator;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;Lcom/flixster/android/activity/decorator/DialogDecorator;)V

    .line 775
    :cond_0
    return-void

    .line 758
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$7;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->collectionThrobber:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lnet/flixster/android/MyMoviesPage;->access$5(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 761
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$7;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsHeader:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/MyMoviesPage;->access$34(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 762
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$7;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->wtsThrobber:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lnet/flixster/android/MyMoviesPage;->access$20(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 765
    :pswitch_2
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$7;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedHeader:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/MyMoviesPage;->access$35(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 766
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage$7;->this$0:Lnet/flixster/android/MyMoviesPage;

    #getter for: Lnet/flixster/android/MyMoviesPage;->ratedThrobber:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lnet/flixster/android/MyMoviesPage;->access$25(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 756
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
