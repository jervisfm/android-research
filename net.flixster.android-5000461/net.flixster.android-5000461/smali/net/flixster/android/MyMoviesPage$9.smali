.class Lnet/flixster/android/MyMoviesPage$9;
.super Ljava/lang/Object;
.source "MyMoviesPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyMoviesPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyMoviesPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage$9;->this$0:Lnet/flixster/android/MyMoviesPage;

    .line 786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "view"

    .prologue
    .line 789
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 790
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 791
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->assetId:J

    iget-wide v3, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$9;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-static {v1, v2, v3, v4, v5}, Lnet/flixster/android/Starter;->launchSeasonDetail(JJLandroid/content/Context;)V

    .line 795
    :cond_0
    :goto_0
    return-void

    .line 792
    :cond_1
    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight;->isMovie()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 793
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->assetId:J

    iget-wide v3, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage$9;->this$0:Lnet/flixster/android/MyMoviesPage;

    invoke-static {v1, v2, v3, v4, v5}, Lnet/flixster/android/Starter;->launchMovieDetail(JJLandroid/content/Context;)V

    goto :goto_0
.end method
