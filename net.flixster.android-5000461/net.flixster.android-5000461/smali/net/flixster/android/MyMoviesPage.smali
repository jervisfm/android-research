.class public Lnet/flixster/android/MyMoviesPage;
.super Lcom/flixster/android/activity/TabbedActivity;
.source "MyMoviesPage.java"


# static fields
.field private static final MAX_FRIENDS_DISPLAYED:I = 0x6

.field private static final MAX_FRIENDS_REVIEWS:I = 0x3

.field private static final MAX_MOVIES_PER_ROW_LANDSCAPE:I = 0x4

.field private static final MAX_MOVIES_PER_ROW_PORTRAIT:I = 0x3

.field private static final MAX_REVIEWS_DISPLAYED:I = 0x32

.field private static final NAV_COLLECTED:I = 0x1

.field private static final NAV_FRIENDS:I = 0x4

.field private static final NAV_RATINGS:I = 0x3

.field private static final NAV_WTS:I = 0x2

.field private static isNoticeShown:Z

.field private static volatile sProgressMonitorThread:Ljava/lang/Thread;


# instance fields
.field private collectedCountView:Landroid/widget/TextView;

.field private final collectedMoviesSuccessHandler:Landroid/os/Handler;

.field private collectionHeader:Landroid/widget/TextView;

.field private collectionThrobber:Landroid/widget/ProgressBar;

.field private final errorHandler:Landroid/os/Handler;

.field private facebookLogin:Landroid/widget/ImageView;

.field private flixsterLogin:Landroid/widget/ImageView;

.field private flixsterLoginDescription:Landroid/widget/TextView;

.field private flixsterLoginHeader:Landroid/widget/TextView;

.field private friendsActivityHeader:Landroid/widget/TextView;

.field private friendsActivityLayout:Landroid/widget/LinearLayout;

.field private final friendsActivitySuccessHandler:Landroid/os/Handler;

.field private friendsCountView:Landroid/widget/TextView;

.field private friendsHeader:Landroid/widget/TextView;

.field private friendsLayout:Landroid/widget/LinearLayout;

.field private final friendsSuccessHandler:Landroid/os/Handler;

.field private friendsView:Landroid/widget/ScrollView;

.field private final getUserSuccessHandler:Landroid/os/Handler;

.field private lastIsConnectedState:Z

.field private lastIsMskEligibleState:Z

.field private lastIsMskEnabledState:Z

.field private lastIsRewardsEligibleState:Z

.field private lastSocialUserId:Ljava/lang/String;

.field private final lockerRightClickListener:Landroid/view/View$OnClickListener;

.field private mNavListener:Landroid/view/View$OnClickListener;

.field private mNavSelect:I

.field private mNavbar:Landroid/widget/LinearLayout;

.field private mNavbarHolder:Landroid/widget/LinearLayout;

.field private movieAndSeasonRights:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;"
        }
    .end annotation
.end field

.field private final movieDetailClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private myMoviesCollectionLayout:Landroid/widget/LinearLayout;

.field private final myMoviesOnClickListener:Landroid/view/View$OnClickListener;

.field private myMoviesRatingsLayout:Landroid/widget/LinearLayout;

.field private myMoviesRentalsLayout:Landroid/widget/LinearLayout;

.field private myMoviesView:Landroid/widget/ScrollView;

.field private myMoviesWtsLayout:Landroid/widget/LinearLayout;

.field private offlineAlert:Landroid/widget/TextView;

.field private pickYourMovie:Landroid/widget/ImageView;

.field private ratedGridView:Landroid/widget/GridView;

.field private ratedHeader:Landroid/widget/TextView;

.field private ratedMovies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final ratedMoviesSuccessHandler:Landroid/os/Handler;

.field private ratedThrobber:Landroid/widget/ProgressBar;

.field private ratingsCountView:Landroid/widget/TextView;

.field private rentalRights:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;"
        }
    .end annotation
.end field

.field private rentalsHeader:Landroid/widget/TextView;

.field private rewardsEarned:Landroid/widget/TextView;

.field private rewardsLayout:Landroid/widget/RelativeLayout;

.field private rewardsMore:Landroid/widget/TextView;

.field private startCollectionHeader:Landroid/widget/TextView;

.field private subnavFriends:Landroid/view/View;

.field private subnavOwn:Landroid/view/View;

.field private subnavRate:Landroid/view/View;

.field private subnavWts:Landroid/view/View;

.field private unfulfillableDivider:Landroid/widget/ImageView;

.field private unfulfillablePanel:Landroid/widget/TextView;

.field private final unfulfillablePanelClickListener:Landroid/view/View$OnClickListener;

.field private uvFooter:Lcom/flixster/android/view/UvFooter;

.field private uvFooterDivider:Landroid/widget/ImageView;

.field private wtsCountView:Landroid/widget/TextView;

.field private wtsGridView:Landroid/widget/GridView;

.field private wtsHeader:Landroid/widget/TextView;

.field private wtsMovies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final wtsMoviesSuccessHandler:Landroid/os/Handler;

.field private wtsThrobber:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 53
    invoke-direct {p0}, Lcom/flixster/android/activity/TabbedActivity;-><init>()V

    .line 64
    iput v0, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    .line 88
    iput-boolean v0, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEligibleState:Z

    .line 370
    new-instance v0, Lnet/flixster/android/MyMoviesPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$1;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->friendsSuccessHandler:Landroid/os/Handler;

    .line 407
    new-instance v0, Lnet/flixster/android/MyMoviesPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$2;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->friendsActivitySuccessHandler:Landroid/os/Handler;

    .line 440
    new-instance v0, Lnet/flixster/android/MyMoviesPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$3;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->collectedMoviesSuccessHandler:Landroid/os/Handler;

    .line 686
    new-instance v0, Lnet/flixster/android/MyMoviesPage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$4;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->wtsMoviesSuccessHandler:Landroid/os/Handler;

    .line 713
    new-instance v0, Lnet/flixster/android/MyMoviesPage$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$5;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->ratedMoviesSuccessHandler:Landroid/os/Handler;

    .line 740
    new-instance v0, Lnet/flixster/android/MyMoviesPage$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$6;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->getUserSuccessHandler:Landroid/os/Handler;

    .line 754
    new-instance v0, Lnet/flixster/android/MyMoviesPage$7;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$7;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->errorHandler:Landroid/os/Handler;

    .line 778
    new-instance v0, Lnet/flixster/android/MyMoviesPage$8;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$8;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->unfulfillablePanelClickListener:Landroid/view/View$OnClickListener;

    .line 786
    new-instance v0, Lnet/flixster/android/MyMoviesPage$9;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$9;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->lockerRightClickListener:Landroid/view/View$OnClickListener;

    .line 798
    new-instance v0, Lnet/flixster/android/MyMoviesPage$10;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$10;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->movieDetailClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 815
    new-instance v0, Lnet/flixster/android/MyMoviesPage$11;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$11;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesOnClickListener:Landroid/view/View$OnClickListener;

    .line 873
    new-instance v0, Lnet/flixster/android/MyMoviesPage$12;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyMoviesPage$12;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyMoviesPage;->mNavListener:Landroid/view/View$OnClickListener;

    .line 53
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->friendsHeader:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->friendsLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$10(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->movieAndSeasonRights:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$11(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 584
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->populateRentalRights()V

    return-void
.end method

.method static synthetic access$12(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 641
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->populateMovieAndSeasonRights()V

    return-void
.end method

.method static synthetic access$13(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->unfulfillablePanel:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$14(Lnet/flixster/android/MyMoviesPage;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 778
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->unfulfillablePanelClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$15(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->unfulfillableDivider:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$16(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 546
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->initializeProgressMonitorThread()V

    return-void
.end method

.method static synthetic access$17()Z
    .locals 1

    .prologue
    .line 92
    sget-boolean v0, Lnet/flixster/android/MyMoviesPage;->isNoticeShown:Z

    return v0
.end method

.method static synthetic access$18(Z)V
    .locals 0
    .parameter

    .prologue
    .line 92
    sput-boolean p0, Lnet/flixster/android/MyMoviesPage;->isNoticeShown:Z

    return-void
.end method

.method static synthetic access$19(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lnet/flixster/android/MyMoviesPage;->invalidActionBarItems()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/MyMoviesPage;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 815
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$20(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->wtsThrobber:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$21(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->wtsCountView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$22(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->wtsMovies:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$23(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;
    .locals 1
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->wtsGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$24(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .parameter

    .prologue
    .line 798
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->movieDetailClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$25(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->ratedThrobber:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$26(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->ratingsCountView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$27(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->ratedMovies:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$28(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/GridView;
    .locals 1
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->ratedGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$29(Lnet/flixster/android/MyMoviesPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 87
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->lastSocialUserId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->friendsActivityHeader:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$30(Lnet/flixster/android/MyMoviesPage;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lnet/flixster/android/MyMoviesPage;->lastSocialUserId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$31(Lnet/flixster/android/MyMoviesPage;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 88
    iput-boolean p1, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEligibleState:Z

    return-void
.end method

.method static synthetic access$32(Lnet/flixster/android/MyMoviesPage;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 89
    iput-boolean p1, p0, Lnet/flixster/android/MyMoviesPage;->lastIsRewardsEligibleState:Z

    return-void
.end method

.method static synthetic access$33(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 261
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->initializeSocialViews()V

    return-void
.end method

.method static synthetic access$34(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->wtsHeader:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$35(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->ratedHeader:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$36(Lnet/flixster/android/MyMoviesPage;)Lcom/flixster/android/activity/decorator/DialogDecorator;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    return-object v0
.end method

.method static synthetic access$37(Lnet/flixster/android/MyMoviesPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64
    iput p1, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    return-void
.end method

.method static synthetic access$38(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 910
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->trackPromoImpressions()V

    return-void
.end method

.method static synthetic access$39(Ljava/lang/Thread;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    sput-object p0, Lnet/flixster/android/MyMoviesPage;->sProgressMonitorThread:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->friendsActivityLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$40(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 866
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->buttonShade()V

    return-void
.end method

.method static synthetic access$41(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lnet/flixster/android/MyMoviesPage;->trackPage()V

    return-void
.end method

.method static synthetic access$42(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 244
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->initializeStaticViews()V

    return-void
.end method

.method static synthetic access$43()Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lnet/flixster/android/MyMoviesPage;->sProgressMonitorThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$44(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$45(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesCollectionLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->collectionThrobber:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/MyMoviesPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->collectedCountView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7(Lnet/flixster/android/MyMoviesPage;)Z
    .locals 1
    .parameter

    .prologue
    .line 89
    iget-boolean v0, p0, Lnet/flixster/android/MyMoviesPage;->lastIsRewardsEligibleState:Z

    return v0
.end method

.method static synthetic access$8(Lnet/flixster/android/MyMoviesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 516
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->updateRewardsLayout()V

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/MyMoviesPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->rentalRights:Ljava/util/List;

    return-object v0
.end method

.method private buttonShade()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 867
    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->subnavOwn:Landroid/view/View;

    iget v0, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setSelected(Z)V

    .line 868
    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->subnavWts:Landroid/view/View;

    iget v0, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setSelected(Z)V

    .line 869
    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->subnavRate:Landroid/view/View;

    iget v0, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setSelected(Z)V

    .line 870
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->subnavFriends:Landroid/view/View;

    iget v3, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 871
    return-void

    :cond_0
    move v0, v2

    .line 867
    goto :goto_0

    :cond_1
    move v0, v2

    .line 868
    goto :goto_1

    :cond_2
    move v0, v2

    .line 869
    goto :goto_2

    :cond_3
    move v1, v2

    .line 870
    goto :goto_3
.end method

.method private initializeProgressMonitorThread()V
    .locals 2

    .prologue
    .line 547
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v0

    invoke-interface {v0}, Lcom/flixster/android/drm/PlaybackLogic;->isDeviceDownloadCompatible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lnet/flixster/android/MyMoviesPage$13;

    invoke-direct {v1, p0}, Lnet/flixster/android/MyMoviesPage$13;-><init>(Lnet/flixster/android/MyMoviesPage;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lnet/flixster/android/MyMoviesPage;->sProgressMonitorThread:Ljava/lang/Thread;

    .line 580
    sget-object v0, Lnet/flixster/android/MyMoviesPage;->sProgressMonitorThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 582
    :cond_0
    return-void
.end method

.method private initializeSocialViews()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 262
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->rentalsHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 263
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->collectionHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 265
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->wtsGridView:Landroid/widget/GridView;

    invoke-virtual {v2, v4}, Landroid/widget/GridView;->setVisibility(I)V

    .line 266
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->ratedGridView:Landroid/widget/GridView;

    invoke-virtual {v2, v4}, Landroid/widget/GridView;->setVisibility(I)V

    .line 268
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 269
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 270
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesCollectionLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 271
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->unfulfillableDivider:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 272
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->unfulfillablePanel:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 273
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->uvFooterDivider:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 274
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->uvFooter:Lcom/flixster/android/view/UvFooter;

    invoke-virtual {v2, v4}, Lcom/flixster/android/view/UvFooter;->setVisibility(I)V

    .line 276
    const v2, 0x7f0701a5

    invoke-virtual {p0, v2}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 277
    .local v0, flixsterRegister:Landroid/view/View;
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v1

    .line 280
    .local v1, user:Lnet/flixster/android/model/User;
    if-nez v1, :cond_1

    .line 281
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->mNavbarHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 282
    const/4 v2, 0x1

    iput v2, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    .line 283
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->initializeStaticViews()V

    .line 285
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLoginHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 286
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLoginDescription:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 287
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLogin:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 288
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->facebookLogin:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 289
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 291
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->wtsHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->ratedHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 294
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->collectionThrobber:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 295
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->wtsThrobber:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 296
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->ratedThrobber:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 298
    iget-boolean v2, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEnabledState:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->isMskFinishedDeepLinkInitiated()Z

    move-result v2

    if-nez v2, :cond_0

    .line 299
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->updateMskPromo()V

    .line 305
    :goto_0
    invoke-virtual {p0}, Lnet/flixster/android/MyMoviesPage;->invalidActionBarItems()V

    .line 368
    :goto_1
    return-void

    .line 301
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->startCollectionHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 302
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->pickYourMovie:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 307
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->mNavbarHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 309
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLoginHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 310
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLoginDescription:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLogin:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 312
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->facebookLogin:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 313
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 315
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->collectedCountView:Landroid/widget/TextView;

    iget v3, v1, Lnet/flixster/android/model/User;->collectionsCount:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 316
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->wtsCountView:Landroid/widget/TextView;

    iget v3, v1, Lnet/flixster/android/model/User;->wtsCount:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->ratingsCountView:Landroid/widget/TextView;

    iget v3, v1, Lnet/flixster/android/model/User;->ratingCount:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->friendsCountView:Landroid/widget/TextView;

    iget v3, v1, Lnet/flixster/android/model/User;->friendCount:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLoginHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 321
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLoginDescription:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 322
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLogin:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 323
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->facebookLogin:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 326
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->collectionThrobber:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 327
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->collectedMoviesSuccessHandler:Landroid/os/Handler;

    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->errorHandler:Landroid/os/Handler;

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getUserLockerRights(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 329
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v1, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    if-eqz v2, :cond_4

    .line 331
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->wtsHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 332
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->wtsThrobber:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 333
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->wtsMoviesSuccessHandler:Landroid/os/Handler;

    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->errorHandler:Landroid/os/Handler;

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getWantToSeeReviews(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 339
    :goto_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v1, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    if-eqz v2, :cond_5

    .line 341
    :cond_3
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->ratedHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 342
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->ratedThrobber:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 343
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->ratedMoviesSuccessHandler:Landroid/os/Handler;

    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->errorHandler:Landroid/os/Handler;

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getUserRatedReviews(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 350
    :goto_3
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEnabledState:Z

    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEligibleState:Z

    if-eqz v2, :cond_7

    .line 351
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->isMskFinishedDeepLinkInitiated()Z

    move-result v2

    if-nez v2, :cond_7

    .line 352
    iget v2, v1, Lnet/flixster/android/model/User;->collectionsCount:I

    if-nez v2, :cond_6

    .line 353
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->updateMskPromo()V

    .line 363
    :goto_4
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->friendsSuccessHandler:Landroid/os/Handler;

    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->errorHandler:Landroid/os/Handler;

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getFriends(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 366
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->friendsActivitySuccessHandler:Landroid/os/Handler;

    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->errorHandler:Landroid/os/Handler;

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getFriendsActivity(Landroid/os/Handler;Landroid/os/Handler;)V

    goto/16 :goto_1

    .line 335
    :cond_4
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->wtsHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 336
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->wtsThrobber:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_2

    .line 345
    :cond_5
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->ratedHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 346
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->ratedThrobber:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_3

    .line 355
    :cond_6
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->updateRewardsLayout()V

    goto :goto_4

    .line 358
    :cond_7
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->startCollectionHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 359
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->pickYourMovie:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4
.end method

.method private initializeStaticViews()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 245
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 246
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesWtsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRatingsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->friendsView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 250
    iget v0, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 251
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 259
    :goto_0
    return-void

    .line 252
    :cond_0
    iget v0, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 253
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesWtsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 254
    :cond_1
    iget v0, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 255
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRatingsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 257
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->friendsView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto :goto_0
.end method

.method private populateMovieAndSeasonRights()V
    .locals 13

    .prologue
    .line 642
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->movieAndSeasonRights:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_2

    .line 643
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesCollectionLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 644
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 645
    .local v2, ll:Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lnet/flixster/android/MyMoviesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    const/4 v3, 0x3

    .line 647
    .local v3, maxMoviesPerRow:I
    :goto_0
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->movieAndSeasonRights:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lt v1, v10, :cond_4

    .line 664
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    .line 665
    .local v7, rowChildCount:I
    if-lez v7, :cond_1

    .line 666
    mul-int/lit8 v10, v3, 0x2

    add-int/lit8 v10, v10, -0x1

    if-ge v7, v10, :cond_0

    .line 667
    move v5, v7

    .local v5, pos:I
    :goto_2
    mul-int/lit8 v10, v3, 0x2

    add-int/lit8 v10, v10, -0x1

    if-lt v5, v10, :cond_7

    .line 677
    .end local v5           #pos:I
    :cond_0
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesCollectionLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 679
    :cond_1
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->collectionHeader:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 680
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesCollectionLayout:Landroid/widget/LinearLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 681
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->uvFooterDivider:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 682
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->uvFooter:Lcom/flixster/android/view/UvFooter;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/flixster/android/view/UvFooter;->setVisibility(I)V

    .line 684
    .end local v1           #i:I
    .end local v2           #ll:Landroid/widget/LinearLayout;
    .end local v3           #maxMoviesPerRow:I
    .end local v7           #rowChildCount:I
    :cond_2
    return-void

    .line 646
    .restart local v2       #ll:Landroid/widget/LinearLayout;
    :cond_3
    const/4 v3, 0x4

    goto :goto_0

    .line 648
    .restart local v1       #i:I
    .restart local v3       #maxMoviesPerRow:I
    :cond_4
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->movieAndSeasonRights:Ljava/util/List;

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lnet/flixster/android/model/LockerRight;

    .line 649
    .local v6, right:Lnet/flixster/android/model/LockerRight;
    new-instance v4, Lnet/flixster/android/MovieCollectionItem;

    invoke-direct {v4, p0}, Lnet/flixster/android/MovieCollectionItem;-><init>(Landroid/content/Context;)V

    .local v4, movieView:Landroid/view/View;
    move-object v10, v4

    .line 650
    check-cast v10, Lnet/flixster/android/MovieCollectionItem;

    invoke-virtual {v10, v6}, Lnet/flixster/android/MovieCollectionItem;->load(Lnet/flixster/android/model/LockerRight;)V

    .line 651
    invoke-virtual {v4, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 652
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->lockerRightClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 653
    rem-int v10, v1, v3

    if-eqz v10, :cond_6

    .line 654
    new-instance v9, Landroid/view/View;

    invoke-direct {v9, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 655
    .local v9, spacingView:Landroid/view/View;
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, 0x0

    const/4 v11, -0x2

    const/high16 v12, 0x3f80

    invoke-direct {v8, v10, v11, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 656
    .local v8, spacingParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v9, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 657
    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 662
    .end local v8           #spacingParams:Landroid/widget/LinearLayout$LayoutParams;
    .end local v9           #spacingView:Landroid/view/View;
    :cond_5
    :goto_3
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 647
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 658
    :cond_6
    if-lez v1, :cond_5

    .line 659
    iget-object v10, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesCollectionLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 660
    new-instance v2, Landroid/widget/LinearLayout;

    .end local v2           #ll:Landroid/widget/LinearLayout;
    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .restart local v2       #ll:Landroid/widget/LinearLayout;
    goto :goto_3

    .line 668
    .end local v4           #movieView:Landroid/view/View;
    .end local v6           #right:Lnet/flixster/android/model/LockerRight;
    .restart local v5       #pos:I
    .restart local v7       #rowChildCount:I
    :cond_7
    new-instance v9, Landroid/view/View;

    invoke-direct {v9, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 669
    .restart local v9       #spacingView:Landroid/view/View;
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, 0x0

    const/4 v11, -0x2

    const/high16 v12, 0x3f80

    invoke-direct {v8, v10, v11, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 670
    .restart local v8       #spacingParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v9, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 671
    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 672
    new-instance v0, Lnet/flixster/android/MovieCollectionItem;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieCollectionItem;-><init>(Landroid/content/Context;)V

    .line 673
    .local v0, dummyView:Landroid/view/View;
    const/4 v10, 0x4

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 674
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 667
    add-int/lit8 v5, v5, 0x2

    goto/16 :goto_2
.end method

.method private populateRentalRights()V
    .locals 15

    .prologue
    .line 585
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->rentalRights:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 586
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 587
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 588
    .local v2, ll:Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lnet/flixster/android/MyMoviesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_3

    const/4 v3, 0x3

    .line 590
    .local v3, maxMoviesPerRow:I
    :goto_0
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->rentalRights:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-lt v1, v11, :cond_4

    .line 616
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    .line 617
    .local v7, rowChildCount:I
    if-lez v7, :cond_1

    .line 618
    mul-int/lit8 v11, v3, 0x2

    add-int/lit8 v11, v11, -0x1

    if-ge v7, v11, :cond_0

    .line 619
    move v5, v7

    .local v5, pos:I
    :goto_2
    mul-int/lit8 v11, v3, 0x2

    add-int/lit8 v11, v11, -0x1

    if-lt v5, v11, :cond_9

    .line 629
    .end local v5           #pos:I
    :cond_0
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v11, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 631
    :cond_1
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v11

    if-lez v11, :cond_a

    .line 632
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->rentalsHeader:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 633
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 639
    .end local v1           #i:I
    .end local v2           #ll:Landroid/widget/LinearLayout;
    .end local v3           #maxMoviesPerRow:I
    .end local v7           #rowChildCount:I
    :cond_2
    :goto_3
    return-void

    .line 589
    .restart local v2       #ll:Landroid/widget/LinearLayout;
    :cond_3
    const/4 v3, 0x4

    goto :goto_0

    .line 591
    .restart local v1       #i:I
    .restart local v3       #maxMoviesPerRow:I
    :cond_4
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->rentalRights:Ljava/util/List;

    invoke-interface {v11, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lnet/flixster/android/model/LockerRight;

    .line 592
    .local v6, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v6}, Lnet/flixster/android/model/LockerRight;->getViewingExpirationString()Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_5

    const/4 v11, 0x1

    invoke-virtual {v6, v11}, Lnet/flixster/android/model/LockerRight;->getRentalExpirationString(Z)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_8

    .line 593
    :cond_5
    new-instance v4, Lnet/flixster/android/MovieCollectionItem;

    invoke-direct {v4, p0}, Lnet/flixster/android/MovieCollectionItem;-><init>(Landroid/content/Context;)V

    .local v4, movieView:Landroid/view/View;
    move-object v11, v4

    .line 594
    check-cast v11, Lnet/flixster/android/MovieCollectionItem;

    invoke-virtual {v11, v6}, Lnet/flixster/android/MovieCollectionItem;->load(Lnet/flixster/android/model/LockerRight;)V

    .line 595
    invoke-virtual {v4, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 596
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->lockerRightClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597
    rem-int v11, v1, v3

    if-eqz v11, :cond_7

    .line 598
    new-instance v9, Landroid/view/View;

    invoke-direct {v9, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 599
    .local v9, spacingView:Landroid/view/View;
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v11, 0x0

    const/4 v12, -0x2

    const/high16 v13, 0x3f80

    invoke-direct {v8, v11, v12, v13}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 600
    .local v8, spacingParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v9, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 601
    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 606
    .end local v8           #spacingParams:Landroid/widget/LinearLayout$LayoutParams;
    .end local v9           #spacingView:Landroid/view/View;
    :cond_6
    :goto_4
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 590
    .end local v4           #movieView:Landroid/view/View;
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 602
    .restart local v4       #movieView:Landroid/view/View;
    :cond_7
    if-lez v1, :cond_6

    .line 603
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v11, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 604
    new-instance v2, Landroid/widget/LinearLayout;

    .end local v2           #ll:Landroid/widget/LinearLayout;
    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .restart local v2       #ll:Landroid/widget/LinearLayout;
    goto :goto_4

    .line 608
    .end local v4           #movieView:Landroid/view/View;
    :cond_8
    const v11, 0x7f0c0182

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v6}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {p0, v11, v12}, Lnet/flixster/android/MyMoviesPage;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 609
    const/4 v12, 0x1

    .line 608
    invoke-static {p0, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v11

    .line 609
    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    .line 610
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v10

    .line 611
    .local v10, user:Lnet/flixster/android/model/User;
    invoke-virtual {v10, v6}, Lnet/flixster/android/model/User;->removeLockerRight(Lnet/flixster/android/model/LockerRight;)V

    .line 612
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->rentalRights:Ljava/util/List;

    invoke-interface {v11, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 613
    add-int/lit8 v1, v1, -0x1

    goto :goto_5

    .line 620
    .end local v6           #right:Lnet/flixster/android/model/LockerRight;
    .end local v10           #user:Lnet/flixster/android/model/User;
    .restart local v5       #pos:I
    .restart local v7       #rowChildCount:I
    :cond_9
    new-instance v9, Landroid/view/View;

    invoke-direct {v9, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 621
    .restart local v9       #spacingView:Landroid/view/View;
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v11, 0x0

    const/4 v12, -0x2

    const/high16 v13, 0x3f80

    invoke-direct {v8, v11, v12, v13}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 622
    .restart local v8       #spacingParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v9, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 623
    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 624
    new-instance v0, Lnet/flixster/android/MovieCollectionItem;

    invoke-direct {v0, p0}, Lnet/flixster/android/MovieCollectionItem;-><init>(Landroid/content/Context;)V

    .line 625
    .local v0, dummyView:Landroid/view/View;
    const/4 v11, 0x4

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 626
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 619
    add-int/lit8 v5, v5, 0x2

    goto/16 :goto_2

    .line 635
    .end local v0           #dummyView:Landroid/view/View;
    .end local v5           #pos:I
    .end local v8           #spacingParams:Landroid/widget/LinearLayout$LayoutParams;
    .end local v9           #spacingView:Landroid/view/View;
    :cond_a
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->rentalsHeader:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 636
    iget-object v11, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method private trackPromoImpressions()V
    .locals 7

    .prologue
    .line 911
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->pickYourMovie:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 912
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/msk/promo"

    const-string v2, "MSK Promo"

    const-string v3, "MskPromo"

    const-string v4, "Impression"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    :cond_0
    :goto_0
    return-void

    .line 913
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 914
    iget-boolean v0, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEligibleState:Z

    if-eqz v0, :cond_2

    .line 915
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/msk/promo"

    const-string v2, "MSK Promo"

    const-string v3, "MskPromo"

    const-string v4, "Impression"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 916
    :cond_2
    iget-boolean v0, p0, Lnet/flixster/android/MyMoviesPage;->lastIsRewardsEligibleState:Z

    if-eqz v0, :cond_0

    .line 917
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/rewards"

    const-string v2, "Flixster Rewards"

    const-string v3, "FlixsterRewards"

    const-string v4, "RewardsPromo"

    .line 918
    const-string v5, "Impression"

    const/4 v6, 0x0

    .line 917
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private updateMskPromo()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->startCollectionHeader:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->pickYourMovie:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 182
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->pickYourMovie:Landroid/widget/ImageView;

    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v1

    .line 185
    .local v1, user:Lnet/flixster/android/model/User;
    const/4 v0, 0x0

    .line 186
    .local v0, mskPromoUrl:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, v1, Lnet/flixster/android/model/User;->collectionsCount:I

    if-eqz v2, :cond_1

    .line 187
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->pickYourMovie:Landroid/widget/ImageView;

    const v3, 0x7f020147

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 188
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMskUserPromoUrl()Ljava/lang/String;

    move-result-object v0

    .line 193
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 194
    new-instance v2, Lcom/flixster/android/model/Image;

    invoke-direct {v2, v0}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->pickYourMovie:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    .line 196
    :cond_0
    return-void

    .line 190
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->pickYourMovie:Landroid/widget/ImageView;

    const v3, 0x7f020146

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 191
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMskAnonPromoUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private updateRewardsLayout()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v2, 0x8

    .line 517
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->startCollectionHeader:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 518
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->pickYourMovie:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 520
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v0

    .line 521
    .local v0, user:Lnet/flixster/android/model/User;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 522
    iget-boolean v1, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEligibleState:Z

    if-eqz v1, :cond_0

    .line 523
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsEarned:Landroid/widget/TextView;

    const v2, 0x7f0c0144

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 524
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsMore:Landroid/widget/TextView;

    const v2, 0x7f0c0145

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 525
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f070198

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 526
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 527
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 543
    :goto_0
    return-void

    .line 528
    :cond_0
    iget-boolean v1, p0, Lnet/flixster/android/MyMoviesPage;->lastIsRewardsEligibleState:Z

    if-eqz v1, :cond_1

    .line 529
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->trackPromoImpressions()V

    .line 530
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsEarned:Landroid/widget/TextView;

    invoke-virtual {p0}, Lnet/flixster/android/MyMoviesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0142

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 531
    invoke-virtual {v0}, Lnet/flixster/android/model/User;->getGiftMoviesEarnedCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x1

    .line 532
    invoke-virtual {v0}, Lnet/flixster/android/model/User;->getGiftMoviesEarnedCount()I

    move-result v6

    invoke-virtual {v0}, Lnet/flixster/android/model/User;->getRewardsEligibleCount()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 530
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsMore:Landroid/widget/TextView;

    const v2, 0x7f0c0143

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 534
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f07019d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 535
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 536
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 538
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 541
    :cond_2
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 3

    .prologue
    .line 927
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v0

    .line 928
    .local v0, user:Lnet/flixster/android/model/User;
    if-nez v0, :cond_0

    const-string v1, "/mymovies/anon"

    :goto_0
    return-object v1

    :cond_0
    iget v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const-string v1, "/mymovies/collected"

    goto :goto_0

    .line 929
    :cond_1
    iget v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const-string v1, "/mymovies/wts"

    goto :goto_0

    :cond_2
    iget v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    const-string v1, "/mymovies/ratings"

    goto :goto_0

    .line 930
    :cond_3
    const-string v1, "/mymovies/friends"

    goto :goto_0
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 935
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v0

    .line 936
    .local v0, user:Lnet/flixster/android/model/User;
    if-nez v0, :cond_0

    const-string v1, "My Movies - Anon"

    :goto_0
    return-object v1

    :cond_0
    iget v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const-string v1, "My Movies - Collected"

    goto :goto_0

    .line 937
    :cond_1
    iget v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const-string v1, "My Movies - Want To See"

    goto :goto_0

    .line 938
    :cond_2
    iget v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavSelect:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    const-string v1, "My Movies - Ratings"

    goto :goto_0

    :cond_3
    const-string v1, "My Movies - Friends"

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 96
    invoke-super {p0, p1}, Lcom/flixster/android/activity/TabbedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 97
    const v1, 0x7f03005e

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->setContentView(I)V

    .line 99
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 100
    .local v0, li:Landroid/view/LayoutInflater;
    const v1, 0x7f030063

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbar:Landroid/widget/LinearLayout;

    .line 101
    const v1, 0x7f07019a

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbarHolder:Landroid/widget/LinearLayout;

    .line 102
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbarHolder:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage;->mNavbar:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 104
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbar:Landroid/widget/LinearLayout;

    const v4, 0x7f0701e1

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->subnavOwn:Landroid/view/View;

    .line 105
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->subnavOwn:Landroid/view/View;

    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage;->mNavListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbar:Landroid/widget/LinearLayout;

    const v4, 0x7f0701e3

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->subnavWts:Landroid/view/View;

    .line 107
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->subnavWts:Landroid/view/View;

    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage;->mNavListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbar:Landroid/widget/LinearLayout;

    const v4, 0x7f0701e5

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->subnavRate:Landroid/view/View;

    .line 109
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->subnavRate:Landroid/view/View;

    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage;->mNavListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbar:Landroid/widget/LinearLayout;

    const v4, 0x7f0701e7

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->subnavFriends:Landroid/view/View;

    .line 111
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->subnavFriends:Landroid/view/View;

    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage;->mNavListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    const v1, 0x7f0700b7

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesView:Landroid/widget/ScrollView;

    .line 114
    const v1, 0x7f0701b5

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->friendsView:Landroid/widget/ScrollView;

    .line 116
    const v1, 0x7f0701a8

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRentalsLayout:Landroid/widget/LinearLayout;

    .line 117
    const v1, 0x7f0701aa

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesCollectionLayout:Landroid/widget/LinearLayout;

    .line 118
    const v1, 0x7f0701ad

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesWtsLayout:Landroid/widget/LinearLayout;

    .line 119
    const v1, 0x7f0701b1

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesRatingsLayout:Landroid/widget/LinearLayout;

    .line 120
    const v1, 0x7f0701b7

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->friendsLayout:Landroid/widget/LinearLayout;

    .line 121
    const v1, 0x7f0701b9

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->friendsActivityLayout:Landroid/widget/LinearLayout;

    .line 123
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbar:Landroid/widget/LinearLayout;

    const v4, 0x7f0701e2

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->collectedCountView:Landroid/widget/TextView;

    .line 124
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbar:Landroid/widget/LinearLayout;

    const v4, 0x7f0701e4

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->wtsCountView:Landroid/widget/TextView;

    .line 125
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbar:Landroid/widget/LinearLayout;

    const v4, 0x7f0701e6

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->ratingsCountView:Landroid/widget/TextView;

    .line 126
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->mNavbar:Landroid/widget/LinearLayout;

    const v4, 0x7f0701e8

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->friendsCountView:Landroid/widget/TextView;

    .line 128
    const v1, 0x7f0701a1

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLoginHeader:Landroid/widget/TextView;

    .line 129
    const v1, 0x7f0701a2

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLoginDescription:Landroid/widget/TextView;

    .line 130
    const v1, 0x7f0701a4

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLogin:Landroid/widget/ImageView;

    .line 131
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->flixsterLogin:Landroid/widget/ImageView;

    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    const v1, 0x7f0701a3

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->facebookLogin:Landroid/widget/ImageView;

    .line 133
    iget-object v1, p0, Lnet/flixster/android/MyMoviesPage;->facebookLogin:Landroid/widget/ImageView;

    iget-object v4, p0, Lnet/flixster/android/MyMoviesPage;->myMoviesOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    const v1, 0x7f07019b

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->offlineAlert:Landroid/widget/TextView;

    .line 136
    const v1, 0x7f07019c

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->startCollectionHeader:Landroid/widget/TextView;

    .line 137
    const v1, 0x7f0701a9

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->collectionHeader:Landroid/widget/TextView;

    .line 138
    const v1, 0x7f0701a7

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rentalsHeader:Landroid/widget/TextView;

    .line 140
    const v1, 0x7f0701ae

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->wtsHeader:Landroid/widget/TextView;

    .line 141
    const v1, 0x7f0701b2

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->ratedHeader:Landroid/widget/TextView;

    .line 142
    const v1, 0x7f0701b6

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->friendsHeader:Landroid/widget/TextView;

    .line 143
    const v1, 0x7f0701b8

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->friendsActivityHeader:Landroid/widget/TextView;

    .line 145
    const v1, 0x7f070198

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->pickYourMovie:Landroid/widget/ImageView;

    .line 147
    const v1, 0x7f0701a6

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->collectionThrobber:Landroid/widget/ProgressBar;

    .line 148
    const v1, 0x7f0701af

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->wtsThrobber:Landroid/widget/ProgressBar;

    .line 149
    const v1, 0x7f0701b3

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->ratedThrobber:Landroid/widget/ProgressBar;

    .line 151
    const v1, 0x7f0701b0

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->wtsGridView:Landroid/widget/GridView;

    .line 152
    const v1, 0x7f0701b4

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->ratedGridView:Landroid/widget/GridView;

    .line 154
    const v1, 0x7f07019d

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsLayout:Landroid/widget/RelativeLayout;

    .line 155
    const v1, 0x7f07019f

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsEarned:Landroid/widget/TextView;

    .line 156
    const v1, 0x7f0701a0

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rewardsMore:Landroid/widget/TextView;

    .line 158
    const v1, 0x7f070111

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->unfulfillableDivider:Landroid/widget/ImageView;

    .line 159
    const v1, 0x7f070110

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->unfulfillablePanel:Landroid/widget/TextView;

    .line 160
    const v1, 0x7f0701ab

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->uvFooterDivider:Landroid/widget/ImageView;

    .line 161
    const v1, 0x7f0701ac

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyMoviesPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/flixster/android/view/UvFooter;

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->uvFooter:Lcom/flixster/android/view/UvFooter;

    .line 163
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->rentalRights:Ljava/util/List;

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->movieAndSeasonRights:Ljava/util/List;

    .line 165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->wtsMovies:Ljava/util/List;

    .line 166
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnet/flixster/android/MyMoviesPage;->ratedMovies:Ljava/util/List;

    .line 168
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->initializeStaticViews()V

    .line 169
    sget-boolean v4, Lnet/flixster/android/MyMoviesPage;->isNoticeShown:Z

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    or-int/2addr v1, v4

    sput-boolean v1, Lnet/flixster/android/MyMoviesPage;->isNoticeShown:Z

    .line 170
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_1
    iput-boolean v2, p0, Lnet/flixster/android/MyMoviesPage;->lastIsConnectedState:Z

    .line 172
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isMskEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->hasMskPromptShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 173
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->setMskPromptShown()V

    .line 175
    :cond_0
    invoke-virtual {p0}, Lnet/flixster/android/MyMoviesPage;->checkAndShowLaunchAd()V

    .line 176
    return-void

    :cond_1
    move v1, v3

    .line 169
    goto :goto_0

    :cond_2
    move v2, v3

    .line 170
    goto :goto_1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 240
    invoke-super {p0}, Lcom/flixster/android/activity/TabbedActivity;->onPause()V

    .line 241
    const/4 v0, 0x0

    sput-object v0, Lnet/flixster/android/MyMoviesPage;->sProgressMonitorThread:Ljava/lang/Thread;

    .line 242
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 200
    invoke-super {p0}, Lcom/flixster/android/activity/TabbedActivity;->onResume()V

    .line 202
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->buttonShade()V

    .line 205
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v0

    .line 210
    .local v0, currIsConnectedState:Z
    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage;->getUserSuccessHandler:Landroid/os/Handler;

    iget-object v6, p0, Lnet/flixster/android/MyMoviesPage;->errorHandler:Landroid/os/Handler;

    invoke-static {v5, v6}, Lcom/flixster/android/data/AccountFacade;->fetchSocialUserId(Landroid/os/Handler;Landroid/os/Handler;)Ljava/lang/String;

    move-result-object v2

    .line 211
    .local v2, currSocialUserId:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v1

    .line 212
    .local v1, currSocialUser:Lnet/flixster/android/model/User;
    if-nez v2, :cond_0

    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage;->lastSocialUserId:Ljava/lang/String;

    if-nez v5, :cond_5

    .line 213
    :cond_0
    if-eqz v2, :cond_1

    iget-object v5, p0, Lnet/flixster/android/MyMoviesPage;->lastSocialUserId:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 214
    :cond_1
    iget-boolean v5, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEnabledState:Z

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v6

    invoke-virtual {v6}, Lcom/flixster/android/utils/Properties;->isMskEnabled()Z

    move-result v6

    if-ne v5, v6, :cond_5

    .line 215
    if-eqz v2, :cond_2

    iget-boolean v5, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEligibleState:Z

    iget-boolean v6, v1, Lnet/flixster/android/model/User;->isMskEligible:Z

    if-ne v5, v6, :cond_5

    .line 216
    :cond_2
    if-eqz v2, :cond_3

    iget-boolean v6, p0, Lnet/flixster/android/MyMoviesPage;->lastIsRewardsEligibleState:Z

    invoke-virtual {v1}, Lnet/flixster/android/model/User;->isRewardsEligible()Z

    move-result v5

    if-eqz v5, :cond_a

    iget-boolean v5, v1, Lnet/flixster/android/model/User;->isMskEligible:Z

    if-nez v5, :cond_a

    move v5, v3

    :goto_0
    if-ne v6, v5, :cond_5

    .line 217
    :cond_3
    if-eqz v2, :cond_4

    iget-boolean v5, v1, Lnet/flixster/android/model/User;->refreshRequired:Z

    if-nez v5, :cond_5

    iget-object v5, v1, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    if-eqz v5, :cond_5

    iget-object v5, v1, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    if-eqz v5, :cond_5

    .line 218
    :cond_4
    iget-boolean v5, p0, Lnet/flixster/android/MyMoviesPage;->lastIsConnectedState:Z

    if-eq v5, v0, :cond_c

    .line 219
    :cond_5
    iput-object v2, p0, Lnet/flixster/android/MyMoviesPage;->lastSocialUserId:Ljava/lang/String;

    .line 220
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v5

    invoke-virtual {v5}, Lcom/flixster/android/utils/Properties;->isMskEnabled()Z

    move-result v5

    iput-boolean v5, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEnabledState:Z

    .line 221
    if-eqz v2, :cond_6

    .line 222
    iget-boolean v5, v1, Lnet/flixster/android/model/User;->isMskEligible:Z

    iput-boolean v5, p0, Lnet/flixster/android/MyMoviesPage;->lastIsMskEligibleState:Z

    .line 223
    invoke-virtual {v1}, Lnet/flixster/android/model/User;->isRewardsEligible()Z

    move-result v5

    if-eqz v5, :cond_b

    iget-boolean v5, v1, Lnet/flixster/android/model/User;->isMskEligible:Z

    if-nez v5, :cond_b

    :goto_1
    iput-boolean v3, p0, Lnet/flixster/android/MyMoviesPage;->lastIsRewardsEligibleState:Z

    .line 224
    iput-boolean v4, v1, Lnet/flixster/android/model/User;->refreshRequired:Z

    .line 226
    :cond_6
    iput-boolean v0, p0, Lnet/flixster/android/MyMoviesPage;->lastIsConnectedState:Z

    .line 227
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->initializeSocialViews()V

    .line 231
    :cond_7
    :goto_2
    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->offlineAlert:Landroid/widget/TextView;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v5

    if-nez v5, :cond_8

    if-nez v2, :cond_9

    :cond_8
    const/16 v4, 0x8

    :cond_9
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 233
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->initializeProgressMonitorThread()V

    .line 234
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->trackPromoImpressions()V

    .line 235
    invoke-virtual {p0}, Lnet/flixster/android/MyMoviesPage;->trackPage()V

    .line 236
    return-void

    :cond_a
    move v5, v4

    .line 216
    goto :goto_0

    :cond_b
    move v3, v4

    .line 223
    goto :goto_1

    .line 228
    :cond_c
    if-eqz v2, :cond_7

    iget-object v3, p0, Lnet/flixster/android/MyMoviesPage;->rentalRights:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 229
    invoke-direct {p0}, Lnet/flixster/android/MyMoviesPage;->populateRentalRights()V

    goto :goto_2
.end method
