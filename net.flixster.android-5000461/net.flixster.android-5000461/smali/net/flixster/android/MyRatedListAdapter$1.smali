.class Lnet/flixster/android/MyRatedListAdapter$1;
.super Ljava/lang/Object;
.source "MyRatedListAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyRatedListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyRatedListAdapter;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyRatedListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyRatedListAdapter$1;->this$0:Lnet/flixster/android/MyRatedListAdapter;

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lnet/flixster/android/MyRatedListAdapter$1;->this$0:Lnet/flixster/android/MyRatedListAdapter;

    invoke-virtual {v2, p3}, Lnet/flixster/android/MyRatedListAdapter;->getItemViewType(I)I

    move-result v1

    .line 76
    .local v1, viewType:I
    const/4 v2, 0x5

    if-ne v2, v1, :cond_0

    .line 77
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;

    .line 78
    .local v0, reviewView:Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;
    iget-object v2, v0, Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;->reviewLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->performClick()Z

    .line 80
    .end local v0           #reviewView:Lnet/flixster/android/FlixsterListAdapter$ReviewViewHolder;
    :cond_0
    return-void
.end method
