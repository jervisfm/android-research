.class Lnet/flixster/android/MyRatedListAdapter$2;
.super Ljava/lang/Object;
.source "MyRatedListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyRatedListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyRatedListAdapter;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyRatedListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyRatedListAdapter$2;->this$0:Lnet/flixster/android/MyRatedListAdapter;

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "view"

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Review;

    .line 86
    .local v3, review:Lnet/flixster/android/model/Review;
    if-eqz v3, :cond_1

    iget-object v4, p0, Lnet/flixster/android/MyRatedListAdapter$2;->this$0:Lnet/flixster/android/MyRatedListAdapter;

    #getter for: Lnet/flixster/android/MyRatedListAdapter;->user:Lnet/flixster/android/model/User;
    invoke-static {v4}, Lnet/flixster/android/MyRatedListAdapter;->access$0(Lnet/flixster/android/MyRatedListAdapter;)Lnet/flixster/android/model/User;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 87
    invoke-virtual {v3}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v2

    .line 88
    .local v2, movie:Lnet/flixster/android/model/Movie;
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v4

    const-string v5, "/mymovies/rated"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "User Review - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "title"

    invoke-virtual {v2, v7}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v4, p0, Lnet/flixster/android/MyRatedListAdapter$2;->this$0:Lnet/flixster/android/MyRatedListAdapter;

    #getter for: Lnet/flixster/android/MyRatedListAdapter;->user:Lnet/flixster/android/model/User;
    invoke-static {v4}, Lnet/flixster/android/MyRatedListAdapter;->access$0(Lnet/flixster/android/MyRatedListAdapter;)Lnet/flixster/android/model/User;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 90
    .local v0, index:I
    if-gez v0, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 93
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v4, "USER_REVIEW_PAGE"

    const/4 v5, 0x0

    iget-object v6, p0, Lnet/flixster/android/MyRatedListAdapter$2;->this$0:Lnet/flixster/android/MyRatedListAdapter;

    iget-object v6, v6, Lnet/flixster/android/MyRatedListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    const-class v7, Lnet/flixster/android/UserReviewPage;

    invoke-direct {v1, v4, v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 94
    .local v1, intent:Landroid/content/Intent;
    const-string v4, "REVIEW_INDEX"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 95
    const-string v4, "REVIEW_TYPE"

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 96
    iget-object v4, p0, Lnet/flixster/android/MyRatedListAdapter$2;->this$0:Lnet/flixster/android/MyRatedListAdapter;

    iget-object v4, v4, Lnet/flixster/android/MyRatedListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    invoke-virtual {v4, v1}, Lnet/flixster/android/FlixsterListActivity;->startActivity(Landroid/content/Intent;)V

    .line 98
    .end local v0           #index:I
    .end local v1           #intent:Landroid/content/Intent;
    .end local v2           #movie:Lnet/flixster/android/model/Movie;
    :cond_1
    return-void
.end method
