.class public Lnet/flixster/android/MyRatedListAdapter;
.super Lnet/flixster/android/FlixsterListAdapter;
.source "MyRatedListAdapter.java"


# instance fields
.field private mRefreshAd:Z

.field private reviewClickListener:Landroid/view/View$OnClickListener;

.field private reviewItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private user:Lnet/flixster/android/model/User;


# direct methods
.method public constructor <init>(Lnet/flixster/android/FlixsterListActivity;Lnet/flixster/android/model/User;Ljava/util/ArrayList;)V
    .locals 2
    .parameter "context"
    .parameter "user"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/flixster/android/FlixsterListActivity;",
            "Lnet/flixster/android/model/User;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p3, data:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct {p0, p1, p3}, Lnet/flixster/android/FlixsterListAdapter;-><init>(Lnet/flixster/android/FlixsterListActivity;Ljava/util/ArrayList;)V

    .line 73
    new-instance v0, Lnet/flixster/android/MyRatedListAdapter$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyRatedListAdapter$1;-><init>(Lnet/flixster/android/MyRatedListAdapter;)V

    iput-object v0, p0, Lnet/flixster/android/MyRatedListAdapter;->reviewItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 83
    new-instance v0, Lnet/flixster/android/MyRatedListAdapter$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyRatedListAdapter$2;-><init>(Lnet/flixster/android/MyRatedListAdapter;)V

    iput-object v0, p0, Lnet/flixster/android/MyRatedListAdapter;->reviewClickListener:Landroid/view/View$OnClickListener;

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/MyRatedListAdapter;->mRefreshAd:Z

    .line 27
    iput-object p2, p0, Lnet/flixster/android/MyRatedListAdapter;->user:Lnet/flixster/android/model/User;

    .line 28
    invoke-virtual {p1}, Lnet/flixster/android/FlixsterListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/MyRatedListAdapter;->reviewItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 29
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/MyRatedListAdapter;)Lnet/flixster/android/model/User;
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lnet/flixster/android/MyRatedListAdapter;->user:Lnet/flixster/android/model/User;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-virtual {p0, p1}, Lnet/flixster/android/MyRatedListAdapter;->getItemViewType(I)I

    move-result v11

    .line 37
    .local v11, viewType:I
    sget-object v0, Lnet/flixster/android/MyRatedListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    .line 38
    .local v6, dataObject:Ljava/lang/Object;
    packed-switch v11, :pswitch_data_0

    :goto_0
    :pswitch_0
    move-object v10, p2

    .line 70
    :cond_0
    :goto_1
    return-object v10

    :pswitch_1
    move-object v10, p2

    .line 40
    check-cast v10, Lnet/flixster/android/ads/AdView;

    .line 41
    .local v10, view:Lnet/flixster/android/ads/AdView;
    if-nez v10, :cond_1

    .line 42
    new-instance v10, Lnet/flixster/android/ads/AdView;

    .end local v10           #view:Lnet/flixster/android/ads/AdView;
    iget-object v0, p0, Lnet/flixster/android/MyRatedListAdapter;->context:Lnet/flixster/android/FlixsterListActivity;

    const-string v3, "MoviesIveRated"

    invoke-direct {v10, v0, v3}, Lnet/flixster/android/ads/AdView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 43
    .restart local v10       #view:Lnet/flixster/android/ads/AdView;
    const/16 v0, 0x11

    invoke-virtual {v10, v0}, Lnet/flixster/android/ads/AdView;->setGravity(I)V

    .line 44
    const v0, -0x444445

    invoke-virtual {v10, v0}, Lnet/flixster/android/ads/AdView;->setBackgroundColor(I)V

    .line 46
    :cond_1
    const-class v0, Lnet/flixster/android/ads/AdManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getView: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    iget-boolean v0, p0, Lnet/flixster/android/MyRatedListAdapter;->mRefreshAd:Z

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {v10}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 49
    iput-boolean v2, p0, Lnet/flixster/android/MyRatedListAdapter;->mRefreshAd:Z

    .line 50
    const-class v0, Lnet/flixster/android/ads/AdManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "refresh: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .end local v10           #view:Lnet/flixster/android/ads/AdView;
    :pswitch_2
    move-object v8, v6

    .line 54
    check-cast v8, Ljava/lang/String;

    .line 55
    .local v8, title:Ljava/lang/String;
    invoke-virtual {p0, v8, p2}, Lnet/flixster/android/MyRatedListAdapter;->getTitleView(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;

    move-result-object p2

    .line 56
    goto :goto_0

    .end local v8           #title:Ljava/lang/String;
    :pswitch_3
    move-object v9, v6

    .line 58
    check-cast v9, Lnet/flixster/android/model/User;

    .line 59
    .local v9, user:Lnet/flixster/android/model/User;
    invoke-virtual {p0, v9, p2, p3}, Lnet/flixster/android/MyRatedListAdapter;->getProfileView(Lnet/flixster/android/model/User;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 60
    goto :goto_0

    .end local v9           #user:Lnet/flixster/android/model/User;
    :pswitch_4
    move-object v1, v6

    .line 62
    check-cast v1, Lnet/flixster/android/model/Review;

    .line 63
    .local v1, review:Lnet/flixster/android/model/Review;
    iget-object v5, p0, Lnet/flixster/android/MyRatedListAdapter;->reviewClickListener:Landroid/view/View$OnClickListener;

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lnet/flixster/android/MyRatedListAdapter;->getReviewView(Lnet/flixster/android/model/Review;ZLandroid/view/View;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object p2

    .line 64
    goto :goto_0

    .end local v1           #review:Lnet/flixster/android/model/Review;
    :pswitch_5
    move-object v7, v6

    .line 66
    check-cast v7, Ljava/lang/String;

    .line 67
    .local v7, text:Ljava/lang/String;
    invoke-virtual {p0, v7, p2}, Lnet/flixster/android/MyRatedListAdapter;->getTextView(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setAdRefresh(Z)V
    .locals 0
    .parameter "bool"

    .prologue
    .line 32
    iput-boolean p1, p0, Lnet/flixster/android/MyRatedListAdapter;->mRefreshAd:Z

    .line 33
    return-void
.end method
