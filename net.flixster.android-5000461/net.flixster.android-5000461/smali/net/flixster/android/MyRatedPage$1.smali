.class Lnet/flixster/android/MyRatedPage$1;
.super Landroid/os/Handler;
.source "MyRatedPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/MyRatedPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyRatedPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyRatedPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyRatedPage$1;->this$0:Lnet/flixster/android/MyRatedPage;

    .line 173
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "message"

    .prologue
    .line 177
    const-string v0, "FlxMain"

    const-string v1, "MyRatedPage.updateHandler"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage$1;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lnet/flixster/android/MyRatedPage$1;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v0

    iget-object v0, v0, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lnet/flixster/android/MyRatedPage$1;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v0

    iget-object v0, v0, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/MyRatedPage$1;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v0

    iget v0, v0, Lnet/flixster/android/model/User;->ratingCount:I

    if-gtz v0, :cond_2

    .line 179
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage$1;->this$0:Lnet/flixster/android/MyRatedPage;

    #calls: Lnet/flixster/android/MyRatedPage;->updatePage()V
    invoke-static {v0}, Lnet/flixster/android/MyRatedPage;->access$1(Lnet/flixster/android/MyRatedPage;)V

    .line 185
    :cond_1
    :goto_0
    return-void

    .line 181
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage$1;->this$0:Lnet/flixster/android/MyRatedPage;

    invoke-virtual {v0}, Lnet/flixster/android/MyRatedPage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage$1;->this$0:Lnet/flixster/android/MyRatedPage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/MyRatedPage;->showDialog(I)V

    goto :goto_0
.end method
