.class Lnet/flixster/android/MyRatedPage$3;
.super Ljava/util/TimerTask;
.source "MyRatedPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/MyRatedPage;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/MyRatedPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/MyRatedPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    .line 145
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 147
    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget v1, v1, Lnet/flixster/android/model/User;->ratingCount:I

    if-lez v1, :cond_1

    .line 149
    :cond_0
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;

    move-result-object v2

    #setter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1, v2}, Lnet/flixster/android/MyRatedPage;->access$3(Lnet/flixster/android/MyRatedPage;Lnet/flixster/android/model/User;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :cond_1
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 157
    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget v1, v1, Lnet/flixster/android/model/User;->ratingCount:I

    if-lez v1, :cond_3

    .line 159
    :cond_2
    :try_start_1
    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/MyRatedPage;->access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v3, 0x32

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getUserRatedReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    iput-object v2, v1, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_1

    .line 165
    :cond_3
    :goto_1
    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    #getter for: Lnet/flixster/android/MyRatedPage;->updateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/MyRatedPage;->access$4(Lnet/flixster/android/MyRatedPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 166
    return-void

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "MyRatedPage.scheduleUpdatePageTask (failed to get user data)"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 152
    iget-object v1, p0, Lnet/flixster/android/MyRatedPage$3;->this$0:Lnet/flixster/android/MyRatedPage;

    const/4 v2, 0x0

    #setter for: Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1, v2}, Lnet/flixster/android/MyRatedPage;->access$3(Lnet/flixster/android/MyRatedPage;Lnet/flixster/android/model/User;)V

    goto :goto_0

    .line 160
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catch_1
    move-exception v0

    .line 161
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "MyRatedPage.scheduleUpdatePageTask (failed to get review data)"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
