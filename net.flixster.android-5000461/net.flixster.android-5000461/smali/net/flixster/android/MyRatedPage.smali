.class public Lnet/flixster/android/MyRatedPage;
.super Lnet/flixster/android/FlixsterListActivity;
.source "MyRatedPage.java"


# static fields
.field private static final DIALOG_MOVIES:I = 0x2

.field private static final DIALOG_NETWORK_FAIL:I = 0x1


# instance fields
.field private isConnected:Ljava/lang/Boolean;

.field private timer:Ljava/util/Timer;

.field private updateHandler:Landroid/os/Handler;

.field private user:Lnet/flixster/android/model/User;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lnet/flixster/android/FlixsterListActivity;-><init>()V

    .line 173
    new-instance v0, Lnet/flixster/android/MyRatedPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyRatedPage$1;-><init>(Lnet/flixster/android/MyRatedPage;)V

    iput-object v0, p0, Lnet/flixster/android/MyRatedPage;->updateHandler:Landroid/os/Handler;

    .line 29
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/MyRatedPage;)Lnet/flixster/android/model/User;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/MyRatedPage;)V
    .locals 0
    .parameter

    .prologue
    .line 189
    invoke-direct {p0}, Lnet/flixster/android/MyRatedPage;->updatePage()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/MyRatedPage;)V
    .locals 0
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Lnet/flixster/android/MyRatedPage;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/MyRatedPage;Lnet/flixster/android/model/User;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 31
    iput-object p1, p0, Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/MyRatedPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 145
    new-instance v0, Lnet/flixster/android/MyRatedPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/MyRatedPage$3;-><init>(Lnet/flixster/android/MyRatedPage;)V

    .line 168
    .local v0, updatePageTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/MyRatedPage;->timer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 169
    iget-object v1, p0, Lnet/flixster/android/MyRatedPage;->timer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 171
    :cond_0
    return-void
.end method

.method private updatePage()V
    .locals 7

    .prologue
    .line 190
    iget-object v3, p0, Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;

    if-eqz v3, :cond_1

    .line 191
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v1, data:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v3, Lnet/flixster/android/ads/AdView;

    const-string v4, "MoviesIveRated"

    invoke-direct {v3, p0, v4}, Lnet/flixster/android/ads/AdView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v3, p0, Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    iget-object v3, p0, Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;

    iget-object v3, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;

    iget-object v3, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 196
    iget-object v3, p0, Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;

    iget-object v3, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 200
    :goto_0
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 201
    .local v0, adapter:Landroid/widget/ListAdapter;
    if-nez v0, :cond_0

    .line 202
    const v3, 0x7f0701bb

    invoke-virtual {p0, v3}, Lnet/flixster/android/MyRatedPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 203
    .local v2, titleView:Landroid/view/View;
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 204
    new-instance v0, Lnet/flixster/android/MyRatedListAdapter;

    .end local v0           #adapter:Landroid/widget/ListAdapter;
    iget-object v3, p0, Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;

    invoke-direct {v0, p0, v3, v1}, Lnet/flixster/android/MyRatedListAdapter;-><init>(Lnet/flixster/android/FlixsterListActivity;Lnet/flixster/android/model/User;Ljava/util/ArrayList;)V

    .line 205
    .restart local v0       #adapter:Landroid/widget/ListAdapter;
    invoke-virtual {p0, v0}, Lnet/flixster/android/MyRatedPage;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 207
    .end local v2           #titleView:Landroid/view/View;
    :cond_0
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lnet/flixster/android/MyRatedPage;->removeDialog(I)V

    .line 208
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/mymovies/rated"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "My Rated Page for user:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lnet/flixster/android/MyRatedPage;->user:Lnet/flixster/android/model/User;

    iget-object v6, v6, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    .end local v0           #adapter:Landroid/widget/ListAdapter;
    .end local v1           #data:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1
    return-void

    .line 198
    .restart local v1       #data:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_2
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c008a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v2, 0x1

    .line 71
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    if-ne p1, v2, :cond_0

    .line 72
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/MyRatedPage;->isConnected:Ljava/lang/Boolean;

    .line 73
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 74
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "net.flixster.IsConnected"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 79
    .end local v0           #intent:Landroid/content/Intent;
    :goto_0
    return-void

    .line 76
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lnet/flixster/android/MyRatedPage;->setResult(I)V

    .line 77
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .parameter "savedState"

    .prologue
    const/4 v6, 0x1

    .line 40
    const-string v4, "FlxMain"

    const-string v5, "MyRatedPage.onCreate"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v4, 0x7f03005f

    invoke-virtual {p0, v4}, Lnet/flixster/android/MyRatedPage;->setContentView(I)V

    .line 43
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->createActionBar()V

    .line 44
    const v4, 0x7f0c006e

    invoke-virtual {p0, v4}, Lnet/flixster/android/MyRatedPage;->setActionBarTitle(I)V

    .line 46
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getListView()Landroid/widget/ListView;

    move-result-object v3

    .line 47
    .local v3, lv:Landroid/widget/ListView;
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09000b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 48
    .local v0, color:I
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 49
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 51
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0200c2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 52
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0025

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 54
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 55
    .local v2, extras:Landroid/os/Bundle;
    if-eqz v2, :cond_2

    iget-object v4, p0, Lnet/flixster/android/MyRatedPage;->isConnected:Ljava/lang/Boolean;

    if-nez v4, :cond_2

    .line 56
    const-string v4, "FlxMain"

    const-string v5, "MyRatedPage.onCreate "

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v4, "net.flixster.IsConnected"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lnet/flixster/android/MyRatedPage;->isConnected:Ljava/lang/Boolean;

    .line 58
    iget-object v4, p0, Lnet/flixster/android/MyRatedPage;->isConnected:Ljava/lang/Boolean;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lnet/flixster/android/MyRatedPage;->isConnected:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    .line 59
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lnet/flixster/android/ConnectRatePage;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v1, connectRate:Landroid/content/Intent;
    const-string v4, "net.flixster.RequestCode"

    invoke-virtual {v1, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 61
    invoke-virtual {p0, v1, v6}, Lnet/flixster/android/MyRatedPage;->startActivityForResult(Landroid/content/Intent;I)V

    .line 67
    .end local v1           #connectRate:Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 64
    :cond_2
    invoke-direct {p0}, Lnet/flixster/android/MyRatedPage;->updatePage()V

    goto :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "dialogId"

    .prologue
    const v5, 0x7f0c0135

    const/4 v4, 0x1

    .line 99
    packed-switch p1, :pswitch_data_0

    .line 123
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 124
    .local v1, defaultDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 125
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 126
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 127
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    move-object v2, v1

    .line 128
    .end local v1           #defaultDialog:Landroid/app/ProgressDialog;
    :goto_0
    return-object v2

    .line 102
    :pswitch_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 103
    .local v2, ratingsDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 104
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 105
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 106
    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 109
    .end local v2           #ratingsDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 110
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v3, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 111
    const-string v3, "Network Error"

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 112
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 113
    const-string v3, "Retry"

    new-instance v4, Lnet/flixster/android/MyRatedPage$2;

    invoke-direct {v4, p0}, Lnet/flixster/android/MyRatedPage$2;-><init>(Lnet/flixster/android/MyRatedPage;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 120
    invoke-virtual {p0}, Lnet/flixster/android/MyRatedPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 121
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 134
    const-string v0, "FlxMain"

    const-string v1, "MyRatedPage.onDestroy"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-super {p0}, Lnet/flixster/android/FlixsterListActivity;->onDestroy()V

    .line 137
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 139
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 141
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/MyRatedPage;->timer:Ljava/util/Timer;

    .line 142
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 84
    const-string v0, "FlxMain"

    const-string v1, "MyRatedPage.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-super {p0}, Lnet/flixster/android/FlixsterListActivity;->onResume()V

    .line 87
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage;->isConnected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/MyRatedPage;->timer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 91
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/MyRatedPage;->timer:Ljava/util/Timer;

    .line 93
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lnet/flixster/android/MyRatedPage;->showDialog(I)V

    .line 94
    invoke-direct {p0}, Lnet/flixster/android/MyRatedPage;->scheduleUpdatePageTask()V

    goto :goto_0
.end method
