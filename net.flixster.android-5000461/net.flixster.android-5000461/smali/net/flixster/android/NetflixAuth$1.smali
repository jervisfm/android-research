.class Lnet/flixster/android/NetflixAuth$1;
.super Ljava/lang/Object;
.source "NetflixAuth.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/NetflixAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/NetflixAuth;


# direct methods
.method constructor <init>(Lnet/flixster/android/NetflixAuth;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/NetflixAuth$1;->this$0:Lnet/flixster/android/NetflixAuth;

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 95
    :try_start_0
    iget-object v3, p0, Lnet/flixster/android/NetflixAuth$1;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v3}, Lnet/flixster/android/NetflixAuth;->access$0(Lnet/flixster/android/NetflixAuth;)Loauth/signpost/OAuthProvider;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/NetflixAuth$1;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v4}, Lnet/flixster/android/NetflixAuth;->access$1(Lnet/flixster/android/NetflixAuth;)Loauth/signpost/OAuthConsumer;

    move-result-object v4

    const-string v5, "flixster://netflix/queue"

    invoke-interface {v3, v4, v5}, Loauth/signpost/OAuthProvider;->retrieveRequestToken(Loauth/signpost/OAuthConsumer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, baseAuth:Ljava/lang/String;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 97
    .local v2, msg:Landroid/os/Message;
    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 98
    iget-object v3, p0, Lnet/flixster/android/NetflixAuth$1;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->authUrlHandler:Landroid/os/Handler;
    invoke-static {v3}, Lnet/flixster/android/NetflixAuth;->access$2(Lnet/flixster/android/NetflixAuth;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_3

    .line 108
    .end local v0           #baseAuth:Ljava/lang/String;
    .end local v2           #msg:Landroid/os/Message;
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v1

    .line 100
    .local v1, e:Loauth/signpost/exception/OAuthMessageSignerException;
    const-string v3, "FlxMain"

    const-string v4, "NetflixAuth.onCreate() OAuthMessageSignerException"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 101
    .end local v1           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v1

    .line 102
    .local v1, e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    const-string v3, "FlxMain"

    const-string v4, "NetflixAuth.onCreate() OAuthNotAuthorizedException"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 103
    .end local v1           #e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_2
    move-exception v1

    .line 104
    .local v1, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    const-string v3, "FlxMain"

    const-string v4, "NetflixAuth.onCreate() OAuthExpectationFailedException"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 105
    .end local v1           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_3
    move-exception v1

    .line 106
    .local v1, e:Loauth/signpost/exception/OAuthCommunicationException;
    const-string v3, "FlxMain"

    const-string v4, "NetflixAuth.onCreate() OAuthCommunicationException"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
