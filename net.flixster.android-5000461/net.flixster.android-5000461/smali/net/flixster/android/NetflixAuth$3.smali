.class Lnet/flixster/android/NetflixAuth$3;
.super Ljava/lang/Object;
.source "NetflixAuth.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/NetflixAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/NetflixAuth;


# direct methods
.method constructor <init>(Lnet/flixster/android/NetflixAuth;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/NetflixAuth$3;->this$0:Lnet/flixster/android/NetflixAuth;

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 211
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/NetflixAuth$3;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v1}, Lnet/flixster/android/NetflixAuth;->access$0(Lnet/flixster/android/NetflixAuth;)Loauth/signpost/OAuthProvider;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/NetflixAuth$3;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v2}, Lnet/flixster/android/NetflixAuth;->access$1(Lnet/flixster/android/NetflixAuth;)Loauth/signpost/OAuthConsumer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Loauth/signpost/OAuthProvider;->retrieveAccessToken(Loauth/signpost/OAuthConsumer;Ljava/lang/String;)V

    .line 213
    iget-object v1, p0, Lnet/flixster/android/NetflixAuth$3;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->netflixPageFinishedHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/NetflixAuth;->access$4(Lnet/flixster/android/NetflixAuth;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_3

    .line 226
    :goto_0
    return-void

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    const-string v1, "FlxMain"

    const-string v2, "NetflixAuth.onPageFinished OAuthMessageSignerException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 217
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v0

    .line 218
    .local v0, e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    const-string v1, "FlxMain"

    .line 219
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixAuth.onPageFinished OAuthNotAuthorizedException responseBody:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Loauth/signpost/exception/OAuthNotAuthorizedException;->getResponseBody()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 218
    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 221
    .end local v0           #e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_2
    move-exception v0

    .line 222
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    const-string v1, "FlxMain"

    const-string v2, "NetflixAuth.onPageFinished OAuthExpectationFailedException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 223
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_3
    move-exception v0

    .line 224
    .local v0, e:Loauth/signpost/exception/OAuthCommunicationException;
    const-string v1, "FlxMain"

    const-string v2, "NetflixAuth.onPageFinished OAuthCommunicationException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
