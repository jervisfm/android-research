.class Lnet/flixster/android/NetflixAuth$4;
.super Landroid/os/Handler;
.source "NetflixAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/NetflixAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/NetflixAuth;


# direct methods
.method constructor <init>(Lnet/flixster/android/NetflixAuth;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/NetflixAuth$4;->this$0:Lnet/flixster/android/NetflixAuth;

    .line 229
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 233
    iget-object v1, p0, Lnet/flixster/android/NetflixAuth$4;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v1}, Lnet/flixster/android/NetflixAuth;->access$0(Lnet/flixster/android/NetflixAuth;)Loauth/signpost/OAuthProvider;

    move-result-object v1

    invoke-interface {v1}, Loauth/signpost/OAuthProvider;->getResponseParameters()Loauth/signpost/http/HttpParameters;

    move-result-object v0

    .line 234
    .local v0, params:Loauth/signpost/http/HttpParameters;
    const-string v1, "FlxMain"

    .line 235
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixAuth.FlixsterWebViewClient mConsumer.getTokenSecret():"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/NetflixAuth$4;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v3}, Lnet/flixster/android/NetflixAuth;->access$1(Lnet/flixster/android/NetflixAuth;)Loauth/signpost/OAuthConsumer;

    move-result-object v3

    invoke-interface {v3}, Loauth/signpost/OAuthConsumer;->getTokenSecret()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 236
    const-string v3, " Map:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 235
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 234
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v1, p0, Lnet/flixster/android/NetflixAuth$4;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v1}, Lnet/flixster/android/NetflixAuth;->access$1(Lnet/flixster/android/NetflixAuth;)Loauth/signpost/OAuthConsumer;

    move-result-object v1

    invoke-interface {v1}, Loauth/signpost/OAuthConsumer;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setNetflixOAuthToken(Ljava/lang/String;)V

    .line 238
    iget-object v1, p0, Lnet/flixster/android/NetflixAuth$4;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v1}, Lnet/flixster/android/NetflixAuth;->access$1(Lnet/flixster/android/NetflixAuth;)Loauth/signpost/OAuthConsumer;

    move-result-object v1

    invoke-interface {v1}, Loauth/signpost/OAuthConsumer;->getTokenSecret()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setNetflixOAuthTokenSecret(Ljava/lang/String;)V

    .line 239
    const-string v1, "user_id"

    invoke-virtual {v0, v1}, Loauth/signpost/http/HttpParameters;->getFirst(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setNetflixUserId(Ljava/lang/String;)V

    .line 241
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/netflix/login/success"

    const-string v3, "Netflix Login Success"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v1, p0, Lnet/flixster/android/NetflixAuth$4;->this$0:Lnet/flixster/android/NetflixAuth;

    #getter for: Lnet/flixster/android/NetflixAuth;->mNetflixAuth:Lnet/flixster/android/NetflixAuth;
    invoke-static {v1}, Lnet/flixster/android/NetflixAuth;->access$5(Lnet/flixster/android/NetflixAuth;)Lnet/flixster/android/NetflixAuth;

    move-result-object v1

    invoke-virtual {v1}, Lnet/flixster/android/NetflixAuth;->finish()V

    .line 244
    return-void
.end method
