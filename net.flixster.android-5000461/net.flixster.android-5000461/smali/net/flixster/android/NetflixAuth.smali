.class public Lnet/flixster/android/NetflixAuth;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "NetflixAuth.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/NetflixAuth$NetflixWebViewClient;
    }
.end annotation


# static fields
.field public static final LAYOUT_SPLASH:I = 0x0

.field public static final LAYOUT_STATE:Ljava/lang/String; = "LayoutState"

.field public static final LAYOUT_WEB:I = 0x1


# instance fields
.field private final authUrlHandler:Landroid/os/Handler;

.field private volatile mConsumer:Loauth/signpost/OAuthConsumer;

.field private mLayoutState:I

.field private mLoginButton:Landroid/widget/Button;

.field private mNetflixAuth:Lnet/flixster/android/NetflixAuth;

.field private mNewAccountButton:Landroid/widget/Button;

.field private volatile mProvider:Loauth/signpost/OAuthProvider;

.field private mSelectLayout:Landroid/widget/RelativeLayout;

.field private mWebView:Landroid/webkit/WebView;

.field private final netflixPageFinishedHandler:Landroid/os/Handler;

.field private final retrieveTokenRunnable:Ljava/lang/Runnable;

.field private final tokenRequestRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/NetflixAuth;->mLayoutState:I

    .line 90
    new-instance v0, Lnet/flixster/android/NetflixAuth$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixAuth$1;-><init>(Lnet/flixster/android/NetflixAuth;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixAuth;->tokenRequestRunnable:Ljava/lang/Runnable;

    .line 111
    new-instance v0, Lnet/flixster/android/NetflixAuth$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixAuth$2;-><init>(Lnet/flixster/android/NetflixAuth;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixAuth;->authUrlHandler:Landroid/os/Handler;

    .line 207
    new-instance v0, Lnet/flixster/android/NetflixAuth$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixAuth$3;-><init>(Lnet/flixster/android/NetflixAuth;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixAuth;->retrieveTokenRunnable:Ljava/lang/Runnable;

    .line 229
    new-instance v0, Lnet/flixster/android/NetflixAuth$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixAuth$4;-><init>(Lnet/flixster/android/NetflixAuth;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixAuth;->netflixPageFinishedHandler:Landroid/os/Handler;

    .line 33
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/NetflixAuth;)Loauth/signpost/OAuthProvider;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lnet/flixster/android/NetflixAuth;->mProvider:Loauth/signpost/OAuthProvider;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/NetflixAuth;)Loauth/signpost/OAuthConsumer;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lnet/flixster/android/NetflixAuth;->mConsumer:Loauth/signpost/OAuthConsumer;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/NetflixAuth;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lnet/flixster/android/NetflixAuth;->authUrlHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/NetflixAuth;)Landroid/webkit/WebView;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lnet/flixster/android/NetflixAuth;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/NetflixAuth;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 229
    iget-object v0, p0, Lnet/flixster/android/NetflixAuth;->netflixPageFinishedHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/NetflixAuth;)Lnet/flixster/android/NetflixAuth;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lnet/flixster/android/NetflixAuth;->mNetflixAuth:Lnet/flixster/android/NetflixAuth;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/NetflixAuth;)Ljava/lang/Runnable;
    .locals 1
    .parameter

    .prologue
    .line 207
    iget-object v0, p0, Lnet/flixster/android/NetflixAuth;->retrieveTokenRunnable:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 248
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 264
    :goto_0
    return-void

    .line 250
    :pswitch_0
    const/4 v1, 0x1

    iput v1, p0, Lnet/flixster/android/NetflixAuth;->mLayoutState:I

    .line 251
    iget-object v1, p0, Lnet/flixster/android/NetflixAuth;->mSelectLayout:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 252
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/netflix/login/dialog"

    const-string v3, "Netflix Login"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 257
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/netflix/newaccnt"

    const-string v3, "Start New Account"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 259
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lnet/flixster/android/NetflixAuth;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 248
    :pswitch_data_0
    .packed-switch 0x7f0701f4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    iput-object p0, p0, Lnet/flixster/android/NetflixAuth;->mNetflixAuth:Lnet/flixster/android/NetflixAuth;

    .line 55
    const v2, 0x7f030066

    invoke-virtual {p0, v2}, Lnet/flixster/android/NetflixAuth;->setContentView(I)V

    .line 56
    invoke-virtual {p0}, Lnet/flixster/android/NetflixAuth;->createActionBar()V

    .line 57
    const v2, 0x7f0c00cd

    invoke-virtual {p0, v2}, Lnet/flixster/android/NetflixAuth;->setActionBarTitle(I)V

    .line 59
    const v2, 0x7f0701f1

    invoke-virtual {p0, v2}, Lnet/flixster/android/NetflixAuth;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lnet/flixster/android/NetflixAuth;->mWebView:Landroid/webkit/WebView;

    .line 60
    iget-object v2, p0, Lnet/flixster/android/NetflixAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 61
    .local v0, webSettings:Landroid/webkit/WebSettings;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 62
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 63
    new-instance v1, Lnet/flixster/android/NetflixAuth$NetflixWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lnet/flixster/android/NetflixAuth$NetflixWebViewClient;-><init>(Lnet/flixster/android/NetflixAuth;Lnet/flixster/android/NetflixAuth$NetflixWebViewClient;)V

    .line 64
    .local v1, wv:Landroid/webkit/WebViewClient;
    iget-object v2, p0, Lnet/flixster/android/NetflixAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 67
    const v2, 0x7f0701f2

    invoke-virtual {p0, v2}, Lnet/flixster/android/NetflixAuth;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lnet/flixster/android/NetflixAuth;->mSelectLayout:Landroid/widget/RelativeLayout;

    .line 68
    const v2, 0x7f0701f4

    invoke-virtual {p0, v2}, Lnet/flixster/android/NetflixAuth;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lnet/flixster/android/NetflixAuth;->mLoginButton:Landroid/widget/Button;

    .line 69
    iget-object v2, p0, Lnet/flixster/android/NetflixAuth;->mLoginButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v2, 0x7f0701f5

    invoke-virtual {p0, v2}, Lnet/flixster/android/NetflixAuth;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lnet/flixster/android/NetflixAuth;->mNewAccountButton:Landroid/widget/Button;

    .line 71
    iget-object v2, p0, Lnet/flixster/android/NetflixAuth;->mNewAccountButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->trackNewAccountImpression()V

    .line 79
    sget v2, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/4 v3, 0x7

    if-lt v2, v3, :cond_0

    .line 80
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->getNewConsumer()Loauth/signpost/OAuthConsumer;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/NetflixAuth;->mConsumer:Loauth/signpost/OAuthConsumer;

    .line 81
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->getNewProvider()Loauth/signpost/OAuthProvider;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/NetflixAuth;->mProvider:Loauth/signpost/OAuthProvider;

    .line 87
    :goto_0
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/NetflixAuth;->tokenRequestRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 88
    return-void

    .line 83
    :cond_0
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->getCommonsHttpConsumer()Loauth/signpost/OAuthConsumer;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/NetflixAuth;->mConsumer:Loauth/signpost/OAuthConsumer;

    .line 84
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->getCommonsHttpProvider()Loauth/signpost/OAuthProvider;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/NetflixAuth;->mProvider:Loauth/signpost/OAuthProvider;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 279
    const/4 v0, 0x1

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 147
    const-string v0, "FlxMain"

    const-string v1, "NetflixAuth.onRestoreInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v0, "LayoutState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/NetflixAuth;->mLayoutState:I

    .line 149
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 130
    iget v0, p0, Lnet/flixster/android/NetflixAuth;->mLayoutState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 131
    iget-object v0, p0, Lnet/flixster/android/NetflixAuth;->mSelectLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 136
    :goto_0
    return-void

    .line 133
    :cond_0
    const-string v0, "FlxMain"

    const-string v1, "NetflixAuth onResume() NetflixLinkSynergy()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->trackNewAccountReferral()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 140
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 141
    const-string v0, "FlxMain"

    const-string v1, "NetflixAuth.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v0, "LayoutState"

    iget v1, p0, Lnet/flixster/android/NetflixAuth;->mLayoutState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    return-void
.end method
