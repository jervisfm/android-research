.class Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;
.super Ljava/lang/Object;
.source "NetflixQueueAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/NetflixQueueAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NetflixViewItemHolder"
.end annotation


# instance fields
.field friendScore:Landroid/widget/TextView;

.field movieActors:Landroid/widget/TextView;

.field movieDetailsLayout:Landroid/widget/LinearLayout;

.field movieHeader:Landroid/widget/TextView;

.field movieItem:Landroid/widget/RelativeLayout;

.field movieMeta:Landroid/widget/TextView;

.field moviePlayIcon:Landroid/widget/ImageView;

.field movieRelease:Landroid/widget/TextView;

.field movieScore:Landroid/widget/TextView;

.field movieThumbnail:Landroid/widget/ImageView;

.field movieTitle:Landroid/widget/TextView;

.field movielistGrip:Landroid/widget/ImageView;

.field netflixCheckBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
