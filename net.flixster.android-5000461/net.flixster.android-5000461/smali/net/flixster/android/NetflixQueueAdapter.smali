.class public Lnet/flixster/android/NetflixQueueAdapter;
.super Landroid/widget/BaseAdapter;
.source "NetflixQueueAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;
    }
.end annotation


# static fields
.field public static final VIEWTYPE_FOOTER:I = 0x4

.field public static final VIEWTYPE_GETMORE:I = 0x1

.field public static final VIEWTYPE_ITEM:I = 0x0

.field public static final VIEWTYPE_NETFLIXLOGO:I = 0x2

.field public static final VIEWTYPE_SUBHEADER:I = 0x3


# instance fields
.field context:Landroid/content/Context;

.field public data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field public mNetflixEditAdapter:Lnet/flixster/android/NetflixQueueAdapter;

.field public refreshHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V
    .locals 1
    .parameter "context"
    .parameter
    .parameter "resource"
    .parameter "from"
    .parameter "to"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;I[",
            "Ljava/lang/String;",
            "[I)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p2, data:Ljava/util/List;,"Ljava/util/List<+Ljava/util/Map<Ljava/lang/String;*>;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 384
    new-instance v0, Lnet/flixster/android/NetflixQueueAdapter$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixQueueAdapter$1;-><init>(Lnet/flixster/android/NetflixQueueAdapter;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueueAdapter;->refreshHandler:Landroid/os/Handler;

    .line 54
    iput-object p2, p0, Lnet/flixster/android/NetflixQueueAdapter;->data:Ljava/util/List;

    .line 55
    iput-object p1, p0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    .line 57
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/NetflixQueueAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 58
    iput-object p0, p0, Lnet/flixster/android/NetflixQueueAdapter;->mNetflixEditAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    .line 59
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lnet/flixster/android/NetflixQueueAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 400
    iget-object v0, p0, Lnet/flixster/android/NetflixQueueAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 405
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .parameter "position"

    .prologue
    .line 77
    iget-object v0, p0, Lnet/flixster/android/NetflixQueueAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v1, "type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 35
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 82
    invoke-virtual/range {p0 .. p1}, Lnet/flixster/android/NetflixQueueAdapter;->getItemViewType(I)I

    move-result v33

    .line 83
    .local v33, type:I
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v23

    .line 84
    .local v23, movieRatingType:I
    packed-switch v33, :pswitch_data_0

    .line 348
    :goto_0
    :pswitch_0
    const-string v2, "FlxMain"

    const-string v3, "MovieListAdapter.getView(.) Bad stuff happened! (didn\'t catch the view type"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v15, p2

    .line 349
    .end local p2
    .local v15, convertView:Landroid/view/View;
    :goto_1
    return-object v15

    .line 86
    .end local v15           #convertView:Landroid/view/View;
    .restart local p2
    :pswitch_1
    if-nez p2, :cond_0

    .line 87
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030068

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 89
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;

    .line 90
    .local v34, viewHolder:Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;
    if-nez v34, :cond_1

    .line 91
    new-instance v34, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;

    .end local v34           #viewHolder:Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;
    invoke-direct/range {v34 .. v34}, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;-><init>()V

    .line 92
    .restart local v34       #viewHolder:Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;
    const v2, 0x7f07012e

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    .line 93
    const v2, 0x7f07012f

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->moviePlayIcon:Landroid/widget/ImageView;

    .line 94
    const v2, 0x7f070108

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieTitle:Landroid/widget/TextView;

    .line 95
    const v2, 0x7f07010c

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieMeta:Landroid/widget/TextView;

    .line 96
    const v2, 0x7f07010d

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieRelease:Landroid/widget/TextView;

    .line 97
    const v2, 0x7f07010b

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieActors:Landroid/widget/TextView;

    .line 98
    const v2, 0x7f070132

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    .line 99
    const v2, 0x7f070133

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->friendScore:Landroid/widget/TextView;

    .line 100
    const v2, 0x7f070130

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieDetailsLayout:Landroid/widget/LinearLayout;

    .line 101
    const v2, 0x7f070105

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieItem:Landroid/widget/RelativeLayout;

    .line 102
    const v2, 0x7f0701fb

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    .line 103
    const v2, 0x7f0701fd

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, v34

    iput-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movielistGrip:Landroid/widget/ImageView;

    .line 104
    move-object/from16 v0, p2

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 109
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->data:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/HashMap;

    .line 110
    .local v17, hashMapItem:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "netflixQueueItem"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lnet/flixster/android/model/NetflixQueueItem;

    .line 113
    .local v8, netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    const-string v2, "id"

    invoke-virtual {v8, v2}, Lnet/flixster/android/model/NetflixQueueItem;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 115
    .local v25, netflixId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/NetflixQueuePage;

    iget v2, v2, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    packed-switch v2, :pswitch_data_1

    .line 128
    move-object/from16 v0, v34

    iget-object v3, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, v2, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    .line 129
    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    .line 128
    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 130
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 131
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 132
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movielistGrip:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 141
    :goto_2
    const-string v2, "title"

    invoke-virtual {v8, v2}, Lnet/flixster/android/model/NetflixQueueItem;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 142
    .local v32, title:Ljava/lang/String;
    if-eqz v32, :cond_2

    .line 143
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieTitle:Landroid/widget/TextView;

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    :cond_2
    iget-object v2, v8, Lnet/flixster/android/model/NetflixQueueItem;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v2, :cond_13

    .line 147
    iget-object v4, v8, Lnet/flixster/android/model/NetflixQueueItem;->mMovie:Lnet/flixster/android/model/Movie;

    .line 149
    .local v4, movie:Lnet/flixster/android/model/Movie;
    move-object/from16 v0, v34

    iget-object v3, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/NetflixQueuePage;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 152
    move-object/from16 v0, v34

    iget-object v3, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieDetailsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/NetflixQueuePage;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieDetailsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 157
    move-object/from16 v0, v34

    iget-object v3, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/NetflixQueuePage;

    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 160
    sget-object v2, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-nez v2, :cond_7

    const/16 v19, 0x0

    .line 163
    .local v19, isReleased:Z
    :goto_3
    const-string v2, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 164
    .local v12, actors:Ljava/lang/String;
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_8

    .line 165
    :cond_3
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieActors:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 172
    :goto_4
    iget-object v2, v4, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/Bitmap;

    .line 173
    .local v13, b:Landroid/graphics/Bitmap;
    if-eqz v13, :cond_9

    .line 174
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 202
    :cond_4
    :goto_5
    const/4 v14, 0x0

    .line 204
    .local v14, bNoScore:Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    .line 205
    .local v26, resources:Landroid/content/res/Resources;
    packed-switch v23, :pswitch_data_2

    .line 255
    :goto_6
    if-eqz v14, :cond_5

    .line 256
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 260
    :cond_5
    const-string v2, "meta"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 261
    .local v21, meta:Ljava/lang/String;
    if-eqz v21, :cond_12

    .line 262
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieMeta:Landroid/widget/TextView;

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieMeta:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .end local v4           #movie:Lnet/flixster/android/model/Movie;
    .end local v12           #actors:Ljava/lang/String;
    .end local v14           #bNoScore:Z
    .end local v19           #isReleased:Z
    .end local v21           #meta:Ljava/lang/String;
    .end local v26           #resources:Landroid/content/res/Resources;
    :cond_6
    :goto_7
    move-object/from16 v15, p2

    .line 306
    .end local p2
    .restart local v15       #convertView:Landroid/view/View;
    goto/16 :goto_1

    .line 117
    .end local v13           #b:Landroid/graphics/Bitmap;
    .end local v15           #convertView:Landroid/view/View;
    .end local v32           #title:Ljava/lang/String;
    .restart local p2
    :pswitch_2
    move-object/from16 v0, v34

    iget-object v3, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, v2, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    .line 118
    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    .line 117
    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 119
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 120
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 121
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movielistGrip:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 124
    :pswitch_3
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->netflixCheckBox:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 125
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movielistGrip:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 160
    .restart local v4       #movie:Lnet/flixster/android/model/Movie;
    .restart local v32       #title:Ljava/lang/String;
    :cond_7
    const/16 v19, 0x1

    goto/16 :goto_3

    .line 167
    .restart local v12       #actors:Ljava/lang/String;
    .restart local v19       #isReleased:Z
    :cond_8
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieActors:Landroid/widget/TextView;

    invoke-virtual {v2, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieActors:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 176
    .restart local v13       #b:Landroid/graphics/Bitmap;
    :cond_9
    const-string v2, "thumbnail"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 177
    .local v5, thumbnailUrl:Ljava/lang/String;
    if-eqz v5, :cond_a

    const-string v2, "http"

    invoke-virtual {v5, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 179
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f020154

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 180
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v30

    .line 181
    .local v30, timeStamp:J
    move-wide/from16 v0, v30

    long-to-double v2, v0

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getThumbOrderStamp()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide v9, 0x41e65a0bc0000000L

    add-double/2addr v6, v9

    cmpl-double v2, v2, v6

    if-lez v2, :cond_4

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    move-object v9, v2

    check-cast v9, Lnet/flixster/android/FlixsterListActivity;

    new-instance v2, Lnet/flixster/android/model/ImageOrder;

    const/4 v3, 0x0

    .line 183
    move-object/from16 v0, v34

    iget-object v6, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lnet/flixster/android/NetflixQueueAdapter;->refreshHandler:Landroid/os/Handler;

    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 182
    invoke-virtual {v9, v2}, Lnet/flixster/android/FlixsterListActivity;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 184
    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Lnet/flixster/android/model/Movie;->setThumbOrderStamp(J)V

    goto/16 :goto_5

    .line 187
    .end local v30           #timeStamp:J
    :cond_a
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f02014f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    .line 209
    .end local v5           #thumbnailUrl:Ljava/lang/String;
    .restart local v14       #bNoScore:Z
    .restart local v26       #resources:Landroid/content/res/Resources;
    :pswitch_4
    const-string v2, "popcornScore"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 210
    const-string v2, "popcornScore"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 211
    .local v16, flixsterScore:I
    const/16 v2, 0x3c

    move/from16 v0, v16

    if-ge v0, v2, :cond_c

    const/16 v20, 0x1

    .line 213
    .local v20, isSpilled:Z
    :goto_8
    new-instance v2, Ljava/lang/StringBuilder;

    move/from16 v0, v16

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    .line 214
    .local v28, scoreBuilder:Ljava/lang/StringBuilder;
    if-nez v19, :cond_b

    .line 215
    const-string v2, " "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0c0092

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :cond_b
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    if-nez v19, :cond_d

    const v18, 0x7f0200f6

    .line 221
    .local v18, iconId:I
    :goto_9
    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v29

    .line 223
    .local v29, scoreIcon:Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 224
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v2, v0, v3, v6, v7}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 225
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 211
    .end local v18           #iconId:I
    .end local v20           #isSpilled:Z
    .end local v28           #scoreBuilder:Ljava/lang/StringBuilder;
    .end local v29           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_c
    const/16 v20, 0x0

    goto :goto_8

    .line 220
    .restart local v20       #isSpilled:Z
    .restart local v28       #scoreBuilder:Ljava/lang/StringBuilder;
    :cond_d
    if-eqz v20, :cond_e

    const v18, 0x7f0200ef

    goto :goto_9

    :cond_e
    const v18, 0x7f0200ea

    goto :goto_9

    .line 227
    .end local v16           #flixsterScore:I
    .end local v20           #isSpilled:Z
    .end local v28           #scoreBuilder:Ljava/lang/StringBuilder;
    :cond_f
    const/4 v14, 0x1

    .line 229
    goto/16 :goto_6

    .line 233
    :pswitch_5
    const-string v2, "rottenTomatoes"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 234
    const-string v2, "rottenTomatoes"

    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v27

    .line 235
    .local v27, rottenScore:I
    const/16 v2, 0x3c

    move/from16 v0, v27

    if-ge v0, v2, :cond_10

    .line 236
    const v2, 0x7f0200ed

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v29

    .line 241
    .restart local v29       #scoreIcon:Landroid/graphics/drawable/Drawable;
    :goto_a
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 242
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v2, v0, v3, v6, v7}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 243
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "%"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 238
    .end local v29           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_10
    const v2, 0x7f0200de

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v29

    .restart local v29       #scoreIcon:Landroid/graphics/drawable/Drawable;
    goto :goto_a

    .line 246
    .end local v27           #rottenScore:I
    .end local v29           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_11
    const/4 v14, 0x1

    .line 248
    goto/16 :goto_6

    .line 252
    :pswitch_6
    const/4 v14, 0x1

    goto/16 :goto_6

    .line 265
    .restart local v21       #meta:Ljava/lang/String;
    :cond_12
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieMeta:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 271
    .end local v4           #movie:Lnet/flixster/android/model/Movie;
    .end local v12           #actors:Ljava/lang/String;
    .end local v13           #b:Landroid/graphics/Bitmap;
    .end local v14           #bNoScore:Z
    .end local v19           #isReleased:Z
    .end local v21           #meta:Ljava/lang/String;
    .end local v26           #resources:Landroid/content/res/Resources;
    :cond_13
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieScore:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieActors:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 273
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieMeta:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 274
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f02014f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 276
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieDetailsLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    iget-object v13, v8, Lnet/flixster/android/model/NetflixQueueItem;->thumbnailBitmap:Landroid/graphics/Bitmap;

    .line 282
    .restart local v13       #b:Landroid/graphics/Bitmap;
    if-eqz v13, :cond_14

    .line 283
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_7

    .line 285
    :cond_14
    const-string v2, "box_art"

    invoke-virtual {v8, v2}, Lnet/flixster/android/model/NetflixQueueItem;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 289
    .restart local v5       #thumbnailUrl:Ljava/lang/String;
    if-eqz v5, :cond_15

    const-string v2, "http"

    invoke-virtual {v5, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 291
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f020154

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 292
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v30

    .line 293
    .restart local v30       #timeStamp:J
    move-wide/from16 v0, v30

    long-to-double v2, v0

    invoke-virtual {v8}, Lnet/flixster/android/model/NetflixQueueItem;->getThumbOrderStamp()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide v9, 0x41e65a0bc0000000L

    add-double/2addr v6, v9

    cmpl-double v2, v2, v6

    if-lez v2, :cond_6

    .line 294
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/FlixsterListActivity;

    new-instance v6, Lnet/flixster/android/model/ImageOrder;

    .line 295
    const/4 v7, 0x7

    .line 296
    move-object/from16 v0, v34

    iget-object v10, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lnet/flixster/android/NetflixQueueAdapter;->refreshHandler:Landroid/os/Handler;

    move-object v9, v5

    invoke-direct/range {v6 .. v11}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 294
    invoke-virtual {v2, v6}, Lnet/flixster/android/FlixsterListActivity;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 297
    move-wide/from16 v0, v30

    invoke-virtual {v8, v0, v1}, Lnet/flixster/android/model/NetflixQueueItem;->setThumbOrderStamp(J)V

    goto/16 :goto_7

    .line 300
    .end local v30           #timeStamp:J
    :cond_15
    move-object/from16 v0, v34

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;->movieThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f02014f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    .line 308
    .end local v5           #thumbnailUrl:Ljava/lang/String;
    .end local v8           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    .end local v13           #b:Landroid/graphics/Bitmap;
    .end local v17           #hashMapItem:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v25           #netflixId:Ljava/lang/String;
    .end local v32           #title:Ljava/lang/String;
    .end local v34           #viewHolder:Lnet/flixster/android/NetflixQueueAdapter$NetflixViewItemHolder;
    :pswitch_7
    if-nez p2, :cond_16

    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03006a

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 311
    :cond_16
    const v2, 0x7f03006a

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    .line 312
    const v2, 0x7f070201

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 313
    .local v22, moreMessage:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/NetflixQueuePage;

    iget v0, v2, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    move/from16 v24, v0

    .line 314
    .local v24, navSelect:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, v2, Lnet/flixster/android/NetflixQueuePage;->mMoreStateSelect:[I

    aget v2, v2, v24

    packed-switch v2, :pswitch_data_3

    :goto_b
    move-object/from16 v15, p2

    .line 334
    .end local p2
    .restart local v15       #convertView:Landroid/view/View;
    goto/16 :goto_1

    .line 316
    .end local v15           #convertView:Landroid/view/View;
    .restart local p2
    :pswitch_8
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 317
    const v2, 0x7f0c00d8

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 318
    const v2, -0x777778

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_b

    .line 321
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/NetflixQueuePage;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 322
    const v2, 0x7f0c00d9

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_b

    .line 325
    :pswitch_a
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    const/16 v2, 0x8

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_b

    .line 336
    .end local v22           #moreMessage:Landroid/widget/TextView;
    .end local v24           #navSelect:I
    :pswitch_b
    if-nez p2, :cond_17

    .line 337
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030069

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 339
    :cond_17
    const v2, 0x7f030069

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    .line 340
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/NetflixQueuePage;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v15, p2

    .line 341
    .end local p2
    .restart local v15       #convertView:Landroid/view/View;
    goto/16 :goto_1

    .line 343
    .end local v15           #convertView:Landroid/view/View;
    .restart local p2
    :pswitch_c
    new-instance p2, Landroid/widget/TextView;

    .end local p2
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/NetflixQueueAdapter;->context:Landroid/content/Context;

    move-object/from16 v0, p2

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .restart local p2
    move-object/from16 v2, p2

    .line 344
    check-cast v2, Landroid/widget/TextView;

    const-string v3, "footer"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_b
        :pswitch_0
        :pswitch_c
    .end packed-switch

    .line 115
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 205
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 314
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x5

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .parameter "position"

    .prologue
    .line 67
    iget-object v0, p0, Lnet/flixster/android/NetflixQueueAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v1, "type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
