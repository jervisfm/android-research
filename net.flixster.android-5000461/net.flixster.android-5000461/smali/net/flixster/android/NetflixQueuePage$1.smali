.class Lnet/flixster/android/NetflixQueuePage$1;
.super Ljava/lang/Object;
.source "NetflixQueuePage.java"

# interfaces
.implements Lnet/flixster/android/model/TouchInterceptor$DropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/NetflixQueuePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/NetflixQueuePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/NetflixQueuePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage$1;->this$0:Lnet/flixster/android/NetflixQueuePage;

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public drop(II)V
    .locals 7
    .parameter "from"
    .parameter "to"

    .prologue
    .line 239
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixQueuePage.mDropListener.drop(from:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", to:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 240
    const-string v5, ") mOffsetSelect[mNavSelect]:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lnet/flixster/android/NetflixQueuePage$1;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v5}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v5

    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage$1;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget v6, v6, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 239
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$1;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v3}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x3

    .line 255
    .local v0, lastMovieIndex:I
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixQueuePage.mDropListener.drop(from:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", to:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") lastMovieIndex:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 256
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 255
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    if-le p2, v0, :cond_0

    .line 258
    move p2, v0

    .line 261
    :cond_0
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixQueuePage.mDropListener.drop(from:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", to:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    if-eq p2, p1, :cond_1

    .line 266
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$1;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v3}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 268
    .local v2, swapMapItem:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$1;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v3}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 269
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$1;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v3}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p2, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 271
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$1;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v3, v3, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    invoke-virtual {v3}, Lnet/flixster/android/NetflixQueueAdapter;->notifyDataSetChanged()V

    .line 273
    const-string v3, "netflixQueueItem"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/NetflixQueueItem;

    .line 276
    .local v1, netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    const-string v3, "FlxMain"

    const-string v4, "NetflixQueuePage.mDropListener.drop calling ScheduleDiscMove"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$1;->this$0:Lnet/flixster/android/NetflixQueuePage;

    const-string v4, "id"

    invoke-virtual {v1, v4}, Lnet/flixster/android/model/NetflixQueueItem;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v5, p2, 0x1

    #calls: Lnet/flixster/android/NetflixQueuePage;->ScheduleDiscMove(Ljava/lang/String;I)V
    invoke-static {v3, v4, v5}, Lnet/flixster/android/NetflixQueuePage;->access$2(Lnet/flixster/android/NetflixQueuePage;Ljava/lang/String;I)V

    .line 280
    .end local v1           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    .end local v2           #swapMapItem:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_1
    return-void
.end method
