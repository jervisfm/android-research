.class Lnet/flixster/android/NetflixQueuePage$10;
.super Ljava/util/TimerTask;
.source "NetflixQueuePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/NetflixQueuePage;->ScheduleDiscDelete(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/NetflixQueuePage;

.field private final synthetic val$netflixDeleteId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lnet/flixster/android/NetflixQueuePage;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iput-object p2, p0, Lnet/flixster/android/NetflixQueuePage$10;->val$netflixDeleteId:Ljava/lang/String;

    .line 350
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 358
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, v2, Lnet/flixster/android/NetflixQueuePage;->mDiscAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    if-ne v1, v2, :cond_1

    .line 359
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$10;->val$netflixDeleteId:Ljava/lang/String;

    const-string v2, "/queues/disc/available"

    invoke-static {v1, v2}, Lnet/flixster/android/data/NetflixDao;->deleteQueueItem(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_0
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$10;->val$netflixDeleteId:Ljava/lang/String;

    #calls: Lnet/flixster/android/NetflixQueuePage;->deleteItem(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lnet/flixster/android/NetflixQueuePage;->access$15(Lnet/flixster/android/NetflixQueuePage;Ljava/lang/String;)V

    .line 366
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget v2, v2, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    aget v3, v1, v2

    add-int/lit8 v3, v3, -0x1

    aput v3, v1, v2

    .line 367
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget v2, v2, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    aget v3, v1, v2

    add-int/lit8 v3, v3, -0x1

    aput v3, v1, v2

    .line 388
    :goto_1
    return-void

    .line 360
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, v2, Lnet/flixster/android/NetflixQueuePage;->mInstantAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    if-ne v1, v2, :cond_2

    .line 361
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$10;->val$netflixDeleteId:Ljava/lang/String;

    const-string v2, "/queues/instant/available"

    invoke-static {v1, v2}, Lnet/flixster/android/data/NetflixDao;->deleteQueueItem(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 368
    :catch_0
    move-exception v0

    .line 370
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthMessageSignerException;->printStackTrace()V

    goto :goto_1

    .line 362
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$10;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, v2, Lnet/flixster/android/NetflixQueuePage;->mSavedAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    if-ne v1, v2, :cond_0

    .line 363
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$10;->val$netflixDeleteId:Ljava/lang/String;

    const-string v2, "/queues/disc/saved"

    invoke-static {v1, v2}, Lnet/flixster/android/data/NetflixDao;->deleteQueueItem(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_0

    .line 371
    :catch_1
    move-exception v0

    .line 373
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthExpectationFailedException;->printStackTrace()V

    goto :goto_1

    .line 374
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_2
    move-exception v0

    .line 376
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_1

    .line 377
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_3
    move-exception v0

    .line 379
    .local v0, e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthNotAuthorizedException;->printStackTrace()V

    goto :goto_1

    .line 380
    .end local v0           #e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_4
    move-exception v0

    .line 382
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 383
    .end local v0           #e:Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 385
    .local v0, e:Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_1
.end method
