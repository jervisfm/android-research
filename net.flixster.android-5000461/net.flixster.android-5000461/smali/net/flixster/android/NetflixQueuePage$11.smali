.class Lnet/flixster/android/NetflixQueuePage$11;
.super Ljava/util/TimerTask;
.source "NetflixQueuePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/NetflixQueuePage;->ScheduleLoadMoviesTask(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/NetflixQueuePage;

.field private final synthetic val$currNavSelect:I


# direct methods
.method constructor <init>(Lnet/flixster/android/NetflixQueuePage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iput p2, p0, Lnet/flixster/android/NetflixQueuePage$11;->val$currNavSelect:I

    .line 418
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 420
    const-string v1, "FlxMain"

    const-string v2, "NetflixQueuePage.ScheduleLoadMoviesTask()"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mMoreStateSelect:[I

    iget v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->val$currNavSelect:I

    aget v1, v1, v2

    if-ne v1, v5, :cond_0

    .line 425
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    iget v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->val$currNavSelect:I

    aput v4, v1, v2

    .line 427
    :cond_0
    iget v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->val$currNavSelect:I

    packed-switch v1, :pswitch_data_0

    .line 495
    :cond_1
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 496
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mNetflixQueueItemList.size():"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v3}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :cond_2
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$5(Lnet/flixster/android/NetflixQueuePage;)Lnet/flixster/android/NetflixQueuePage;

    move-result-object v1

    #getter for: Lnet/flixster/android/NetflixQueuePage;->postMovieChangeHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$25(Lnet/flixster/android/NetflixQueuePage;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 499
    return-void

    .line 429
    :pswitch_0
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v2

    aget v2, v2, v6

    const-string v3, "/queues/disc/available"

    #calls: Lnet/flixster/android/NetflixQueuePage;->getDvdQueue(ILjava/lang/String;I)Ljava/util/List;
    invoke-static {v1, v2, v3, v6}, Lnet/flixster/android/NetflixQueuePage;->access$16(Lnet/flixster/android/NetflixQueuePage;ILjava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 430
    .local v0, tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    if-eqz v0, :cond_1

    .line 431
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 432
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$7(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 433
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$7(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v2

    aget v2, v2, v6

    invoke-interface {v1, v2, v0}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 437
    :goto_1
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$7(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v2

    .line 438
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;
    invoke-static {v3}, Lnet/flixster/android/NetflixQueuePage;->access$18(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 437
    aput v2, v1, v6

    .line 440
    :cond_3
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v6

    goto/16 :goto_0

    .line 435
    :cond_4
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #setter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v1, v0}, Lnet/flixster/android/NetflixQueuePage;->access$17(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V

    goto :goto_1

    .line 444
    .end local v0           #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :pswitch_1
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v2

    aget v2, v2, v7

    const-string v3, "/queues/instant/available"

    #calls: Lnet/flixster/android/NetflixQueuePage;->getDvdQueue(ILjava/lang/String;I)Ljava/util/List;
    invoke-static {v1, v2, v3, v7}, Lnet/flixster/android/NetflixQueuePage;->access$16(Lnet/flixster/android/NetflixQueuePage;ILjava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 445
    .restart local v0       #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    if-eqz v0, :cond_1

    .line 446
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 447
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$12(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 448
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$12(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    .line 449
    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v2

    aget v2, v2, v7

    invoke-interface {v1, v2, v0}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 453
    :goto_2
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$12(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v2

    .line 454
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;
    invoke-static {v3}, Lnet/flixster/android/NetflixQueuePage;->access$20(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 453
    aput v2, v1, v7

    .line 456
    :cond_5
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v7

    goto/16 :goto_0

    .line 451
    :cond_6
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #setter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v1, v0}, Lnet/flixster/android/NetflixQueuePage;->access$19(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V

    goto :goto_2

    .line 460
    .end local v0           #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :pswitch_2
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v2

    aget v2, v2, v5

    const-string v3, "/queues/disc/saved"

    #calls: Lnet/flixster/android/NetflixQueuePage;->getDvdQueue(ILjava/lang/String;I)Ljava/util/List;
    invoke-static {v1, v2, v3, v5}, Lnet/flixster/android/NetflixQueuePage;->access$16(Lnet/flixster/android/NetflixQueuePage;ILjava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 461
    .restart local v0       #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    if-eqz v0, :cond_1

    .line 462
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_7

    .line 463
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$13(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 464
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$13(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v2

    aget v2, v2, v5

    invoke-interface {v1, v2, v0}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 468
    :goto_3
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$13(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v2

    .line 469
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;
    invoke-static {v3}, Lnet/flixster/android/NetflixQueuePage;->access$22(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 468
    aput v2, v1, v5

    .line 471
    :cond_7
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v5

    goto/16 :goto_0

    .line 466
    :cond_8
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #setter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v1, v0}, Lnet/flixster/android/NetflixQueuePage;->access$21(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V

    goto :goto_3

    .line 475
    .end local v0           #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :pswitch_3
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v2

    aget v2, v2, v8

    const-string v3, "/at_home"

    #calls: Lnet/flixster/android/NetflixQueuePage;->getDvdQueue(ILjava/lang/String;I)Ljava/util/List;
    invoke-static {v1, v2, v3, v8}, Lnet/flixster/android/NetflixQueuePage;->access$16(Lnet/flixster/android/NetflixQueuePage;ILjava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 476
    .restart local v0       #tempItemList:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    if-eqz v0, :cond_1

    .line 477
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_9

    .line 478
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$14(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 479
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$14(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v2

    aget v2, v2, v8

    invoke-interface {v1, v2, v0}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 483
    :goto_4
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v2}, Lnet/flixster/android/NetflixQueuePage;->access$14(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v2

    .line 484
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;
    invoke-static {v3}, Lnet/flixster/android/NetflixQueuePage;->access$24(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 483
    aput v2, v1, v8

    .line 486
    :cond_9
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v4, v1, v8

    goto/16 :goto_0

    .line 481
    :cond_a
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$11;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #setter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v1, v0}, Lnet/flixster/android/NetflixQueuePage;->access$23(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V

    goto :goto_4

    .line 427
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
