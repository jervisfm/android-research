.class Lnet/flixster/android/NetflixQueuePage$3;
.super Landroid/os/Handler;
.source "NetflixQueuePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/NetflixQueuePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/NetflixQueuePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/NetflixQueuePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage$3;->this$0:Lnet/flixster/android/NetflixQueuePage;

    .line 608
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    const/4 v2, 0x0

    .line 611
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.postMovieChangeHandler"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$3;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->removeLoadingDialog:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$3(Lnet/flixster/android/NetflixQueuePage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 613
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$3;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$4(Lnet/flixster/android/NetflixQueuePage;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 614
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$3;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$5(Lnet/flixster/android/NetflixQueuePage;)Lnet/flixster/android/NetflixQueuePage;

    move-result-object v0

    iget-object v0, v0, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 615
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$3;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$5(Lnet/flixster/android/NetflixQueuePage;)Lnet/flixster/android/NetflixQueuePage;

    move-result-object v0

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixContextMenu:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$6(Lnet/flixster/android/NetflixQueuePage;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 619
    :goto_0
    return-void

    .line 617
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$3;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$5(Lnet/flixster/android/NetflixQueuePage;)Lnet/flixster/android/NetflixQueuePage;

    move-result-object v0

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixContextMenu:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$6(Lnet/flixster/android/NetflixQueuePage;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method
