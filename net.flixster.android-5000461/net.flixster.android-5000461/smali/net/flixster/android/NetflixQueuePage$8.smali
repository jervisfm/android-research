.class Lnet/flixster/android/NetflixQueuePage$8;
.super Ljava/lang/Object;
.source "NetflixQueuePage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/NetflixQueuePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/NetflixQueuePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/NetflixQueuePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    .line 811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "view"

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 813
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 895
    :cond_0
    :goto_0
    return-void

    .line 815
    :pswitch_0
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.navListener - disc"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/dvd"

    const-string v2, "Netflix DVDs"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iput v4, v0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    .line 818
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v0, v0, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 819
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$7(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    #setter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0, v1}, Lnet/flixster/android/NetflixQueuePage;->access$8(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V

    .line 820
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mDiscAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    iput-object v1, v0, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    .line 821
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$4(Lnet/flixster/android/NetflixQueuePage;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 823
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aget-boolean v0, v0, v4

    if-eqz v0, :cond_1

    .line 824
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 825
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v0

    aput v3, v0, v4

    .line 826
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v0

    aput v3, v0, v4

    .line 828
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 829
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->showLoadingDialog:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$10(Lnet/flixster/android/NetflixQueuePage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 830
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    aget v1, v1, v4

    #calls: Lnet/flixster/android/NetflixQueuePage;->ScheduleLoadMoviesTask(I)V
    invoke-static {v0, v1}, Lnet/flixster/android/NetflixQueuePage;->access$11(Lnet/flixster/android/NetflixQueuePage;I)V

    goto :goto_0

    .line 834
    :pswitch_1
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.navListener - instant"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/instant"

    const-string v2, "Netflix Instant"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iput v5, v0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    .line 837
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v0, v0, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 838
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$12(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    #setter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0, v1}, Lnet/flixster/android/NetflixQueuePage;->access$8(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V

    .line 840
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mInstantAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    iput-object v1, v0, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    .line 841
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$4(Lnet/flixster/android/NetflixQueuePage;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 843
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aget-boolean v0, v0, v5

    if-eqz v0, :cond_2

    .line 844
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 845
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v0

    aput v3, v0, v5

    .line 846
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v0

    aput v3, v0, v5

    .line 848
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 849
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->showLoadingDialog:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$10(Lnet/flixster/android/NetflixQueuePage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 850
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    aget v1, v1, v5

    #calls: Lnet/flixster/android/NetflixQueuePage;->ScheduleLoadMoviesTask(I)V
    invoke-static {v0, v1}, Lnet/flixster/android/NetflixQueuePage;->access$11(Lnet/flixster/android/NetflixQueuePage;I)V

    goto/16 :goto_0

    .line 854
    :pswitch_2
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.navListener - saved"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/saved"

    const-string v2, "Netflix Saved"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iput v6, v0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    .line 857
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v0, v0, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 858
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$13(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    #setter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0, v1}, Lnet/flixster/android/NetflixQueuePage;->access$8(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V

    .line 860
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mSavedAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    iput-object v1, v0, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    .line 861
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$4(Lnet/flixster/android/NetflixQueuePage;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 863
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aget-boolean v0, v0, v6

    if-eqz v0, :cond_3

    .line 864
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 865
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v0

    aput v3, v0, v6

    .line 866
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v0

    aput v3, v0, v6

    .line 868
    :cond_3
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 869
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->showLoadingDialog:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$10(Lnet/flixster/android/NetflixQueuePage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 870
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    aget v1, v1, v6

    #calls: Lnet/flixster/android/NetflixQueuePage;->ScheduleLoadMoviesTask(I)V
    invoke-static {v0, v1}, Lnet/flixster/android/NetflixQueuePage;->access$11(Lnet/flixster/android/NetflixQueuePage;I)V

    goto/16 :goto_0

    .line 875
    :pswitch_3
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.navListener - saved"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/netflix/queue/athome"

    const-string v2, "Netflix AtHome"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iput v7, v0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    .line 878
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v0, v0, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 879
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$14(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v1

    #setter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0, v1}, Lnet/flixster/android/NetflixQueuePage;->access$8(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V

    .line 881
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mAtHomeAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    iput-object v1, v0, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    .line 882
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$4(Lnet/flixster/android/NetflixQueuePage;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 883
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aget-boolean v0, v0, v7

    if-eqz v0, :cond_4

    .line 884
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 885
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$9(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v0

    aput v3, v0, v7

    .line 886
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v0

    aput v3, v0, v7

    .line 888
    :cond_4
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 889
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->showLoadingDialog:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/NetflixQueuePage;->access$10(Lnet/flixster/android/NetflixQueuePage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 890
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$8;->this$0:Lnet/flixster/android/NetflixQueuePage;

    #getter for: Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I
    invoke-static {v1}, Lnet/flixster/android/NetflixQueuePage;->access$0(Lnet/flixster/android/NetflixQueuePage;)[I

    move-result-object v1

    aget v1, v1, v7

    #calls: Lnet/flixster/android/NetflixQueuePage;->ScheduleLoadMoviesTask(I)V
    invoke-static {v0, v1}, Lnet/flixster/android/NetflixQueuePage;->access$11(Lnet/flixster/android/NetflixQueuePage;I)V

    goto/16 :goto_0

    .line 813
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
