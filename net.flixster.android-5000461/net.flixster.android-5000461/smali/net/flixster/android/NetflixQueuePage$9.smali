.class Lnet/flixster/android/NetflixQueuePage$9;
.super Ljava/util/TimerTask;
.source "NetflixQueuePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/NetflixQueuePage;->ScheduleDiscMove(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/NetflixQueuePage;

.field private final synthetic val$netflixMovieId:Ljava/lang/String;

.field private final synthetic val$to:I


# direct methods
.method constructor <init>(Lnet/flixster/android/NetflixQueuePage;Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage$9;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iput-object p2, p0, Lnet/flixster/android/NetflixQueuePage$9;->val$netflixMovieId:Ljava/lang/String;

    iput p3, p0, Lnet/flixster/android/NetflixQueuePage$9;->val$to:I

    .line 306
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 312
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$9;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$9;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, v2, Lnet/flixster/android/NetflixQueuePage;->mDiscAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    if-ne v1, v2, :cond_1

    .line 313
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://api-public.netflix.com/catalog/titles/movies/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 314
    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$9;->val$netflixMovieId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 313
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 314
    iget v2, p0, Lnet/flixster/android/NetflixQueuePage$9;->val$to:I

    const-string v3, "/queues/disc/available"

    .line 313
    invoke-static {v1, v2, v3}, Lnet/flixster/android/data/NetflixDao;->postQueueItem(Ljava/lang/String;ILjava/lang/String;)V

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage$9;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$9;->this$0:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, v2, Lnet/flixster/android/NetflixQueuePage;->mInstantAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    if-ne v1, v2, :cond_0

    .line 316
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://api-public.netflix.com/catalog/titles/movies/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 317
    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage$9;->val$netflixMovieId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 316
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 317
    iget v2, p0, Lnet/flixster/android/NetflixQueuePage$9;->val$to:I

    const-string v3, "/queues/instant/available"

    .line 316
    invoke-static {v1, v2, v3}, Lnet/flixster/android/data/NetflixDao;->postQueueItem(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_6

    goto :goto_0

    .line 319
    :catch_0
    move-exception v0

    .line 321
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthMessageSignerException;->printStackTrace()V

    goto :goto_0

    .line 322
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v0

    .line 324
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthExpectationFailedException;->printStackTrace()V

    goto :goto_0

    .line 325
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_2
    move-exception v0

    .line 327
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 328
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_3
    move-exception v0

    .line 330
    .local v0, e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthNotAuthorizedException;->printStackTrace()V

    goto :goto_0

    .line 331
    .end local v0           #e:Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_4
    move-exception v0

    .line 333
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 334
    .end local v0           #e:Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 336
    .local v0, e:Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_0

    .line 337
    .end local v0           #e:Loauth/signpost/exception/OAuthCommunicationException;
    :catch_6
    move-exception v0

    .line 339
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
