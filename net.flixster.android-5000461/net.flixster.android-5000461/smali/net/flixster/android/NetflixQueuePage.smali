.class public Lnet/flixster/android/NetflixQueuePage;
.super Lnet/flixster/android/FlixsterListActivity;
.source "NetflixQueuePage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DIALOG_LOADING_MOVIES:I = 0x1

.field private static final DIALOG_NETWORKFAIL:I = 0x2

.field public static final MORE_CLICKFORMORE:I = 0x2

.field public static final MORE_EMPTYQUEUE:I = 0x1

.field public static final MORE_ENDOFQUEUE:I = 0x3

.field public static final MORE_UNKNOWN:I = 0x0

.field public static final NAV_ATHOME:I = 0x4

.field public static final NAV_DVD:I = 0x1

.field public static final NAV_INSTANT:I = 0x2

.field public static final NAV_NONE:I = 0x0

.field public static final NAV_SAVED:I = 0x3


# instance fields
.field mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

.field mAtHomeAdapter:Lnet/flixster/android/NetflixQueueAdapter;

.field protected mCheckedMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field mDiscAdapter:Lnet/flixster/android/NetflixQueueAdapter;

.field private mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

.field private mEditMode:Z

.field mInstantAdapter:Lnet/flixster/android/NetflixQueueAdapter;

.field public mIsMoreVisible:Z

.field private mLogoNetflixItemHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mMoreIndexSelect:[I

.field private mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mMoreNetflixQueueDiscItemHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mMoreNetflixQueueInstantItemHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mMoreNetflixQueueSavedItemHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public mMoreStateSelect:[I

.field private mNavListener:Landroid/view/View$OnClickListener;

.field public mNavSelect:I

.field private mNetflixContextMenu:Landroid/widget/RelativeLayout;

.field private mNetflixList:Landroid/widget/ListView;

.field private mNetflixQueueAtHomeItemHashList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNetflixQueueDiscItemHashList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNetflixQueueInstantItemHashList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

.field private mNetflixQueueSavedItemHashList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNetflixQueueSelectedItemHashList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOffsetSelect:[I

.field private mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

.field private mRemoveQueueButton:Landroid/widget/Button;

.field mSavedAdapter:Lnet/flixster/android/NetflixQueueAdapter;

.field private mTimer:Ljava/util/Timer;

.field private navBar:Lcom/flixster/android/view/SubNavBar;

.field private postMovieChangeHandler:Landroid/os/Handler;

.field private removeLoadingDialog:Landroid/os/Handler;

.field private removeNetworkFailDialog:Landroid/os/Handler;

.field private showLoadingDialog:Landroid/os/Handler;

.field private showNetworkFailDialog:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lnet/flixster/android/FlixsterListActivity;-><init>()V

    .line 54
    iput v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    .line 63
    iput-boolean v0, p0, Lnet/flixster/android/NetflixQueuePage;->mEditMode:Z

    .line 64
    iput-boolean v0, p0, Lnet/flixster/android/NetflixQueuePage;->mIsMoreVisible:Z

    .line 76
    iput-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    .line 77
    iput-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    .line 78
    iput-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    .line 79
    iput-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    .line 80
    iput-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;

    .line 83
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreStateSelect:[I

    .line 84
    iput-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    .line 85
    iput-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    .line 234
    new-instance v0, Lnet/flixster/android/NetflixQueuePage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixQueuePage$1;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

    .line 407
    new-instance v0, Lnet/flixster/android/NetflixQueuePage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixQueuePage$2;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

    .line 608
    new-instance v0, Lnet/flixster/android/NetflixQueuePage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixQueuePage$3;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->postMovieChangeHandler:Landroid/os/Handler;

    .line 622
    new-instance v0, Lnet/flixster/android/NetflixQueuePage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixQueuePage$4;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->showLoadingDialog:Landroid/os/Handler;

    .line 632
    new-instance v0, Lnet/flixster/android/NetflixQueuePage$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixQueuePage$5;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->removeLoadingDialog:Landroid/os/Handler;

    .line 642
    new-instance v0, Lnet/flixster/android/NetflixQueuePage$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixQueuePage$6;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->showNetworkFailDialog:Landroid/os/Handler;

    .line 652
    new-instance v0, Lnet/flixster/android/NetflixQueuePage$7;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixQueuePage$7;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->removeNetworkFailDialog:Landroid/os/Handler;

    .line 811
    new-instance v0, Lnet/flixster/android/NetflixQueuePage$8;

    invoke-direct {v0, p0}, Lnet/flixster/android/NetflixQueuePage$8;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNavListener:Landroid/view/View$OnClickListener;

    .line 45
    return-void
.end method

.method private ScheduleDiscDelete(Ljava/lang/String;)V
    .locals 4
    .parameter "netflixDeleteId"

    .prologue
    .line 350
    new-instance v0, Lnet/flixster/android/NetflixQueuePage$10;

    invoke-direct {v0, p0, p1}, Lnet/flixster/android/NetflixQueuePage$10;-><init>(Lnet/flixster/android/NetflixQueuePage;Ljava/lang/String;)V

    .line 391
    .local v0, discDelete:Ljava/util/TimerTask;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixQueuePage mTimer:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 393
    return-void
.end method

.method private ScheduleDiscMove(Ljava/lang/String;I)V
    .locals 4
    .parameter "netflixMovieId"
    .parameter "to"

    .prologue
    .line 306
    new-instance v0, Lnet/flixster/android/NetflixQueuePage$9;

    invoke-direct {v0, p0, p1, p2}, Lnet/flixster/android/NetflixQueuePage$9;-><init>(Lnet/flixster/android/NetflixQueuePage;Ljava/lang/String;I)V

    .line 345
    .local v0, discMove:Ljava/util/TimerTask;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixQueuePage mTimer:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 347
    return-void
.end method

.method private ScheduleLoadMoviesTask(I)V
    .locals 5
    .parameter "offset"

    .prologue
    .line 417
    iget v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    .line 418
    .local v0, currNavSelect:I
    new-instance v1, Lnet/flixster/android/NetflixQueuePage$11;

    invoke-direct {v1, p0, v0}, Lnet/flixster/android/NetflixQueuePage$11;-><init>(Lnet/flixster/android/NetflixQueuePage;I)V

    .line 502
    .local v1, loadNetflixMovies:Ljava/util/TimerTask;
    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    const-wide/16 v3, 0x64

    invoke-virtual {v2, v1, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 503
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/NetflixQueuePage;)[I
    .locals 1
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$10(Lnet/flixster/android/NetflixQueuePage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 622
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->showLoadingDialog:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$11(Lnet/flixster/android/NetflixQueuePage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 416
    invoke-direct {p0, p1}, Lnet/flixster/android/NetflixQueuePage;->ScheduleLoadMoviesTask(I)V

    return-void
.end method

.method static synthetic access$12(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$13(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$14(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$15(Lnet/flixster/android/NetflixQueuePage;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 395
    invoke-direct {p0, p1}, Lnet/flixster/android/NetflixQueuePage;->deleteItem(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$16(Lnet/flixster/android/NetflixQueuePage;ILjava/lang/String;I)Ljava/util/List;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 505
    invoke-direct {p0, p1, p2, p3}, Lnet/flixster/android/NetflixQueuePage;->getDvdQueue(ILjava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$17(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 77
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$18(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/Map;
    .locals 1
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$19(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 78
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/NetflixQueuePage;Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 305
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/NetflixQueuePage;->ScheduleDiscMove(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$20(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/Map;
    .locals 1
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$21(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$22(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/Map;
    .locals 1
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$23(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 80
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$24(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/Map;
    .locals 1
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$25(Lnet/flixster/android/NetflixQueuePage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 608
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->postMovieChangeHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$26(Lnet/flixster/android/NetflixQueuePage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 652
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->removeNetworkFailDialog:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/NetflixQueuePage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 632
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->removeLoadingDialog:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/NetflixQueuePage;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/NetflixQueuePage;)Lnet/flixster/android/NetflixQueuePage;
    .locals 1
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/NetflixQueuePage;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixContextMenu:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$7(Lnet/flixster/android/NetflixQueuePage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/NetflixQueuePage;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/NetflixQueuePage;)[I
    .locals 1
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    return-object v0
.end method

.method private declared-synchronized deleteItem(Ljava/lang/String;)V
    .locals 4
    .parameter "netflixDeleteId"

    .prologue
    .line 396
    monitor-enter p0

    :try_start_0
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixQueuePage.deleteItem() pre netflixDeleteId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 397
    const-string v3, " mNetflixQueueItemHashList.size():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 396
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 399
    .local v0, netflixDeleteMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 402
    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    iget-object v1, v1, Lnet/flixster/android/NetflixQueuePage;->postMovieChangeHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 403
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NetflixQueuePage.deleteItem() post mNetflixQueueItemHashList.size():"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 404
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 403
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    monitor-exit p0

    return-void

    .line 396
    .end local v0           #netflixDeleteMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized getDvdQueue(ILjava/lang/String;I)Ljava/util/List;
    .locals 9
    .parameter "offset"
    .parameter "qtype"
    .parameter "currNavSelect"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 506
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 509
    .local v1, list:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 514
    .local v3, netflixQueueItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NetflixQueueItem;>;"
    :try_start_1
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() about to fetch "

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    invoke-static {p1, p2}, Lnet/flixster/android/data/NetflixDao;->fetchQueue(ILjava/lang/String;)Lnet/flixster/android/model/NetflixQueue;

    move-result-object v5

    .line 516
    .local v5, queue:Lnet/flixster/android/model/NetflixQueue;
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() about to post fetch "

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    invoke-virtual {v5}, Lnet/flixster/android/model/NetflixQueue;->getNetflixQueueItemList()Ljava/util/ArrayList;

    move-result-object v3

    .line 518
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 528
    if-nez p1, :cond_0

    .line 530
    packed-switch p3, :pswitch_data_0

    .line 579
    :cond_0
    :goto_1
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aget v7, v7, p3

    add-int/lit8 v7, v7, 0x19

    aput v7, v6, p3

    .line 583
    invoke-virtual {v5}, Lnet/flixster/android/model/NetflixQueue;->getNumberOfResults()I

    move-result v6

    if-nez v6, :cond_2

    .line 584
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreStateSelect:[I

    const/4 v7, 0x1

    aput v7, v6, p3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 605
    .end local v1           #list:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v5           #queue:Lnet/flixster/android/model/NetflixQueue;
    :goto_2
    monitor-exit p0

    return-object v1

    .line 518
    .restart local v1       #list:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .restart local v5       #queue:Lnet/flixster/android/model/NetflixQueue;
    :cond_1
    :try_start_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/NetflixQueueItem;

    .line 520
    .local v4, nqi:Lnet/flixster/android/model/NetflixQueueItem;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 521
    .local v2, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v7, "type"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    const-string v7, "netflixQueueItem"

    invoke-interface {v2, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 591
    .end local v2           #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4           #nqi:Lnet/flixster/android/model/NetflixQueueItem;
    .end local v5           #queue:Lnet/flixster/android/model/NetflixQueue;
    :catch_0
    move-exception v0

    .line 592
    .local v0, e:Loauth/signpost/exception/OAuthMessageSignerException;
    :try_start_3
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() OAuthMessageSignerException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 604
    .end local v0           #e:Loauth/signpost/exception/OAuthMessageSignerException;
    :goto_3
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue - ioex"

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 605
    const/4 v1, 0x0

    goto :goto_2

    .line 532
    .restart local v5       #queue:Lnet/flixster/android/model/NetflixQueue;
    :pswitch_0
    :try_start_4
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;

    .line 533
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;

    const-string v7, "offset"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueDiscItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 536
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    .line 537
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_1

    .line 593
    .end local v5           #queue:Lnet/flixster/android/model/NetflixQueue;
    :catch_1
    move-exception v0

    .line 594
    .local v0, e:Loauth/signpost/exception/OAuthExpectationFailedException;
    :try_start_5
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() OAuthExpectationFailedException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 506
    .end local v0           #e:Loauth/signpost/exception/OAuthExpectationFailedException;
    .end local v1           #list:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v3           #netflixQueueItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NetflixQueueItem;>;"
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 541
    .restart local v1       #list:Ljava/util/List;,"Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .restart local v3       #netflixQueueItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NetflixQueueItem;>;"
    .restart local v5       #queue:Lnet/flixster/android/model/NetflixQueue;
    :pswitch_1
    :try_start_6
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;

    .line 542
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;

    const-string v7, "offset"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueInstantItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    .line 546
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_1

    .line 595
    .end local v5           #queue:Lnet/flixster/android/model/NetflixQueue;
    :catch_2
    move-exception v0

    .line 596
    .local v0, e:Ljava/io/IOException;
    :try_start_7
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() IOException network error"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 597
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->removeLoadingDialog:Landroid/os/Handler;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 598
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->showNetworkFailDialog:Landroid/os/Handler;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    .line 550
    .end local v0           #e:Ljava/io/IOException;
    .restart local v5       #queue:Lnet/flixster/android/model/NetflixQueue;
    :pswitch_2
    :try_start_8
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;

    .line 551
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;

    const-string v7, "offset"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueSavedItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 554
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    .line 555
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_1

    .line 600
    .end local v5           #queue:Lnet/flixster/android/model/NetflixQueue;
    :catch_3
    move-exception v0

    .line 601
    .local v0, e:Ljava/lang/Exception;
    :try_start_9
    const-string v6, "FlxMain"

    const-string v7, "NetflixQueuePage.getDvdQueue() Exception"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_3

    .line 560
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v5       #queue:Lnet/flixster/android/model/NetflixQueue;
    :pswitch_3
    :try_start_a
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;

    .line 561
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;

    const-string v7, "offset"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreNetflixQueueAtHomeItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 564
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    .line 565
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    const-string v7, "type"

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mLogoNetflixItemHash:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 585
    :cond_2
    invoke-virtual {v5}, Lnet/flixster/android/model/NetflixQueue;->getNumberOfResults()I

    move-result v6

    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aget v7, v7, p3

    if-gt v6, v7, :cond_3

    .line 586
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreStateSelect:[I

    const/4 v7, 0x3

    aput v7, v6, p3

    goto/16 :goto_2

    .line 588
    :cond_3
    iget-object v6, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreStateSelect:[I

    const/4 v7, 0x2

    aput v7, v6, p3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_a .. :try_end_a} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    goto/16 :goto_2

    .line 530
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .parameter "view"

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 721
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 805
    const-string v7, "FlxMain"

    const-string v8, "NetflixQueuePage.onClick (default)"

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    :cond_0
    :goto_0
    return-void

    .line 724
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 725
    .local v0, hashMapItem:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v7, "netflixQueueItem"

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/NetflixQueueItem;

    .line 726
    .local v5, netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    const-string v7, "id"

    invoke-virtual {v5, v7}, Lnet/flixster/android/model/NetflixQueueItem;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 728
    .local v6, urlId:Ljava/lang/String;
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    iget-object v7, v7, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    .line 730
    .local v2, isChecked:Z
    if-eqz v6, :cond_1

    .line 731
    if-eqz v2, :cond_2

    .line 732
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    iget-object v7, v7, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 738
    :cond_1
    :goto_1
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    iget-object v7, v7, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    if-nez v7, :cond_3

    .line 739
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixContextMenu:Landroid/widget/RelativeLayout;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 734
    :cond_2
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    iget-object v7, v7, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 741
    :cond_3
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixContextMenu:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 746
    .end local v0           #hashMapItem:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2           #isChecked:Z
    .end local v5           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    .end local v6           #urlId:Ljava/lang/String;
    :sswitch_1
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    iget-object v7, v7, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 748
    .local v4, netflixId:Ljava/lang/String;
    invoke-direct {p0, v4}, Lnet/flixster/android/NetflixQueuePage;->ScheduleDiscDelete(Ljava/lang/String;)V

    goto :goto_2

    .line 754
    .end local v4           #netflixId:Ljava/lang/String;
    :sswitch_2
    const-string v7, "FlxMain"

    const-string v8, "Fire up movie details intent"

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Movie;

    .line 756
    .local v3, movie:Lnet/flixster/android/model/Movie;
    new-instance v1, Landroid/content/Intent;

    const-string v7, "DETAILS"

    const/4 v8, 0x0

    const-class v9, Lnet/flixster/android/MovieDetails;

    invoke-direct {v1, v7, v8, p0, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 757
    .local v1, i:Landroid/content/Intent;
    const-string v7, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 759
    const-string v7, "title"

    const-string v8, "title"

    invoke-virtual {v3, v8}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 762
    iget v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    if-ne v7, v11, :cond_5

    .line 763
    sget-object v7, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v11, v7, v12

    .line 764
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 765
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    aput v10, v7, v12

    .line 766
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aput v10, v7, v12

    .line 767
    sget-object v7, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v11, v7, v13

    .line 768
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 769
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    aput v10, v7, v13

    .line 770
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aput v10, v7, v13

    .line 791
    :cond_4
    :goto_3
    invoke-virtual {p0, v1}, Lnet/flixster/android/NetflixQueuePage;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 771
    :cond_5
    iget v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    if-ne v7, v12, :cond_6

    .line 772
    sget-object v7, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v11, v7, v11

    .line 773
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 774
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    aput v10, v7, v11

    .line 775
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aput v10, v7, v11

    .line 776
    sget-object v7, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v11, v7, v13

    .line 777
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 778
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    aput v10, v7, v13

    .line 779
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aput v10, v7, v13

    goto :goto_3

    .line 780
    :cond_6
    iget v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    if-ne v7, v13, :cond_4

    .line 781
    sget-object v7, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v11, v7, v12

    .line 782
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 783
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    aput v10, v7, v12

    .line 784
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aput v10, v7, v12

    .line 785
    sget-object v7, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v11, v7, v11

    .line 786
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 787
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    aput v10, v7, v11

    .line 788
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aput v10, v7, v11

    goto :goto_3

    .line 794
    .end local v1           #i:Landroid/content/Intent;
    .end local v3           #movie:Lnet/flixster/android/model/Movie;
    :sswitch_3
    const-string v7, "FlxMain"

    const-string v8, "NetflixQueuePage.onClick (MORE)"

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->showLoadingDialog:Landroid/os/Handler;

    invoke-virtual {v7, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 796
    iget-object v7, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    iget v8, p0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    aget v7, v7, v8

    invoke-direct {p0, v7}, Lnet/flixster/android/NetflixQueuePage;->ScheduleLoadMoviesTask(I)V

    goto/16 :goto_0

    .line 799
    :sswitch_4
    const-string v7, "FlxMain"

    const-string v8, "NetflixQueuePage.onClick (LOGO)"

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    new-instance v1, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    const-string v8, "http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v1, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 801
    .restart local v1       #i:Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lnet/flixster/android/NetflixQueuePage;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 721
    :sswitch_data_0
    .sparse-switch
        0x7f030069 -> :sswitch_4
        0x7f03006a -> :sswitch_3
        0x7f07012e -> :sswitch_2
        0x7f070130 -> :sswitch_2
        0x7f0701f9 -> :sswitch_1
        0x7f0701fb -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .parameter "savedInstanceState"

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 104
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    iput-object p0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    .line 106
    const v0, 0x7f030067

    invoke-virtual {p0, v0}, Lnet/flixster/android/NetflixQueuePage;->setContentView(I)V

    .line 107
    invoke-virtual {p0}, Lnet/flixster/android/NetflixQueuePage;->createActionBar()V

    .line 108
    const v0, 0x7f0c00ce

    invoke-virtual {p0, v0}, Lnet/flixster/android/NetflixQueuePage;->setActionBarTitle(I)V

    .line 110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mCheckedMap:Ljava/util/HashMap;

    .line 111
    invoke-virtual {p0}, Lnet/flixster/android/NetflixQueuePage;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    .line 112
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 113
    iget-boolean v0, p0, Lnet/flixster/android/NetflixQueuePage;->mEditMode:Z

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    check-cast v0, Lnet/flixster/android/model/TouchInterceptor;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

    invoke-virtual {v0, v1}, Lnet/flixster/android/model/TouchInterceptor;->setDropListener(Lnet/flixster/android/model/TouchInterceptor$DropListener;)V

    .line 115
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    check-cast v0, Lnet/flixster/android/model/TouchInterceptor;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

    invoke-virtual {v0, v1}, Lnet/flixster/android/model/TouchInterceptor;->setRemoveListener(Lnet/flixster/android/model/TouchInterceptor$RemoveListener;)V

    .line 116
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 121
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/NetflixQueuePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 122
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/NetflixQueuePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 123
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    const v1, 0x7f09000b

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 124
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/NetflixQueuePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 127
    const v0, 0x7f0701f9

    invoke-virtual {p0, v0}, Lnet/flixster/android/NetflixQueuePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mRemoveQueueButton:Landroid/widget/Button;

    .line 128
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mRemoveQueueButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    const v0, 0x7f0701f8

    invoke-virtual {p0, v0}, Lnet/flixster/android/NetflixQueuePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixContextMenu:Landroid/widget/RelativeLayout;

    .line 130
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 131
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    .line 134
    :cond_1
    new-array v0, v9, [I

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    .line 135
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    aput v7, v0, v8

    .line 136
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    aput v7, v0, v10

    .line 137
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    aput v7, v0, v11

    .line 138
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    const/4 v1, 0x4

    aput v7, v0, v1

    .line 142
    new-array v0, v9, [I

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    .line 143
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aput v7, v0, v8

    .line 144
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aput v7, v0, v10

    .line 145
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aput v7, v0, v11

    .line 146
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    const/4 v1, 0x4

    aput v7, v0, v1

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;

    .line 153
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    .line 157
    new-instance v0, Lnet/flixster/android/NetflixQueueAdapter;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueDiscItemHashList:Ljava/util/List;

    .line 158
    const v3, 0x7f03005c

    new-array v4, v9, [Ljava/lang/String;

    const-string v5, "movieTitle"

    aput-object v5, v4, v7

    const-string v5, "movieActors"

    aput-object v5, v4, v8

    const-string v5, "movieMeta"

    aput-object v5, v4, v10

    const-string v5, "movieRelease"

    aput-object v5, v4, v11

    const/4 v5, 0x4

    .line 159
    const-string v6, "movieThumbnail"

    aput-object v6, v4, v5

    new-array v5, v9, [I

    fill-array-data v5, :array_0

    .line 160
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/NetflixQueueAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 157
    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mDiscAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    .line 161
    new-instance v0, Lnet/flixster/android/NetflixQueueAdapter;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueInstantItemHashList:Ljava/util/List;

    .line 162
    const v3, 0x7f03005c

    new-array v4, v9, [Ljava/lang/String;

    const-string v5, "movieTitle"

    aput-object v5, v4, v7

    const-string v5, "movieActors"

    aput-object v5, v4, v8

    const-string v5, "movieMeta"

    aput-object v5, v4, v10

    const-string v5, "movieRelease"

    aput-object v5, v4, v11

    const/4 v5, 0x4

    .line 163
    const-string v6, "movieThumbnail"

    aput-object v6, v4, v5

    new-array v5, v9, [I

    fill-array-data v5, :array_1

    .line 164
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/NetflixQueueAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 161
    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mInstantAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    .line 165
    new-instance v0, Lnet/flixster/android/NetflixQueueAdapter;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSavedItemHashList:Ljava/util/List;

    .line 166
    const v3, 0x7f03005c

    new-array v4, v9, [Ljava/lang/String;

    const-string v5, "movieTitle"

    aput-object v5, v4, v7

    const-string v5, "movieActors"

    aput-object v5, v4, v8

    const-string v5, "movieMeta"

    aput-object v5, v4, v10

    const-string v5, "movieRelease"

    aput-object v5, v4, v11

    const/4 v5, 0x4

    .line 167
    const-string v6, "movieThumbnail"

    aput-object v6, v4, v5

    new-array v5, v9, [I

    fill-array-data v5, :array_2

    .line 168
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/NetflixQueueAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 165
    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mSavedAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    .line 169
    new-instance v0, Lnet/flixster/android/NetflixQueueAdapter;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueuePage:Lnet/flixster/android/NetflixQueuePage;

    iget-object v2, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueAtHomeItemHashList:Ljava/util/List;

    .line 170
    const v3, 0x7f03005c

    new-array v4, v9, [Ljava/lang/String;

    const-string v5, "movieTitle"

    aput-object v5, v4, v7

    const-string v5, "movieActors"

    aput-object v5, v4, v8

    const-string v5, "movieMeta"

    aput-object v5, v4, v10

    const-string v5, "movieRelease"

    aput-object v5, v4, v11

    const/4 v5, 0x4

    .line 171
    const-string v6, "movieThumbnail"

    aput-object v6, v4, v5

    new-array v5, v9, [I

    fill-array-data v5, :array_3

    .line 172
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/NetflixQueueAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 169
    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mAtHomeAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    .line 174
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mDiscAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    if-eqz v0, :cond_2

    .line 176
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mDiscAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 177
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mDiscAdapter:Lnet/flixster/android/NetflixQueueAdapter;

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mAdapterSelected:Lnet/flixster/android/NetflixQueueAdapter;

    .line 181
    :cond_2
    new-instance v0, Lcom/flixster/android/view/SubNavBar;

    invoke-direct {v0, p0}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->navBar:Lcom/flixster/android/view/SubNavBar;

    .line 182
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->mNavListener:Landroid/view/View$OnClickListener;

    const v2, 0x7f0c00e1

    const v3, 0x7f0c00d6

    const v4, 0x7f0c00d5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;III)V

    .line 183
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->navBar:Lcom/flixster/android/view/SubNavBar;

    const v1, 0x7f070258

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 184
    const v0, 0x7f0701f7

    invoke-virtual {p0, v0}, Lnet/flixster/android/NetflixQueuePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lnet/flixster/android/NetflixQueuePage;->navBar:Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {v0, v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 186
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v8, v0, v8

    .line 187
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v8, v0, v10

    .line 188
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aput-boolean v8, v0, v11

    .line 189
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    const/4 v1, 0x4

    aput-boolean v8, v0, v1

    .line 191
    return-void

    .line 159
    :array_0
    .array-data 0x4
        0x8t 0x1t 0x7t 0x7ft
        0xbt 0x1t 0x7t 0x7ft
        0xct 0x1t 0x7t 0x7ft
        0xdt 0x1t 0x7t 0x7ft
        0x2et 0x1t 0x7t 0x7ft
    .end array-data

    .line 163
    :array_1
    .array-data 0x4
        0x8t 0x1t 0x7t 0x7ft
        0xbt 0x1t 0x7t 0x7ft
        0xct 0x1t 0x7t 0x7ft
        0xdt 0x1t 0x7t 0x7ft
        0x2et 0x1t 0x7t 0x7ft
    .end array-data

    .line 167
    :array_2
    .array-data 0x4
        0x8t 0x1t 0x7t 0x7ft
        0xbt 0x1t 0x7t 0x7ft
        0xct 0x1t 0x7t 0x7ft
        0xdt 0x1t 0x7t 0x7ft
        0x2et 0x1t 0x7t 0x7ft
    .end array-data

    .line 171
    :array_3
    .array-data 0x4
        0x8t 0x1t 0x7t 0x7ft
        0xbt 0x1t 0x7t 0x7ft
        0xct 0x1t 0x7t 0x7ft
        0xdt 0x1t 0x7t 0x7ft
        0x2et 0x1t 0x7t 0x7ft
    .end array-data
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "dialog"

    .prologue
    const/4 v3, 0x1

    .line 671
    packed-switch p1, :pswitch_data_0

    .line 714
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 673
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 674
    .local v0, progressDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lnet/flixster/android/NetflixQueuePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 675
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 676
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 677
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 678
    new-instance v1, Lnet/flixster/android/NetflixQueuePage$12;

    invoke-direct {v1, p0}, Lnet/flixster/android/NetflixQueuePage$12;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 687
    .end local v0           #progressDialog:Landroid/app/ProgressDialog;
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 688
    const-string v2, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 689
    const-string v2, "Network Error"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 690
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 691
    const-string v2, "Retry"

    new-instance v3, Lnet/flixster/android/NetflixQueuePage$13;

    invoke-direct {v3, p0}, Lnet/flixster/android/NetflixQueuePage$13;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 701
    invoke-virtual {p0}, Lnet/flixster/android/NetflixQueuePage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 702
    new-instance v3, Lnet/flixster/android/NetflixQueuePage$14;

    invoke-direct {v3, p0}, Lnet/flixster/android/NetflixQueuePage$14;-><init>(Lnet/flixster/android/NetflixQueuePage;)V

    .line 701
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 712
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 671
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 297
    invoke-super {p0}, Lnet/flixster/android/FlixsterListActivity;->onPause()V

    .line 298
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 300
    iget-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 302
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    .line 303
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 292
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.onRestoreInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 195
    invoke-super {p0}, Lnet/flixster/android/FlixsterListActivity;->onResume()V

    .line 196
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    if-nez v3, :cond_0

    .line 197
    new-instance v3, Ljava/util/Timer;

    invoke-direct {v3}, Ljava/util/Timer;-><init>()V

    iput-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mTimer:Ljava/util/Timer;

    .line 199
    :cond_0
    const-string v3, "FlxMain"

    const-string v4, "NetflixQueue.onResume()"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    invoke-virtual {p0}, Lnet/flixster/android/NetflixQueuePage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 201
    .local v2, uri:Landroid/net/Uri;
    if-eqz v2, :cond_1

    .line 202
    const-string v3, "oauth_token"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 203
    .local v1, oauth_token:Ljava/lang/String;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetflixQueue.onResume() oauth_token:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " uri:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->setNetflixOAuthToken(Ljava/lang/String;)V

    .line 207
    .end local v1           #oauth_token:Ljava/lang/String;
    :cond_1
    iget v0, p0, Lnet/flixster/android/NetflixQueuePage;->mNavSelect:I

    .line 208
    .local v0, currNavSelect:I
    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sNetflixListIsDirty:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_2

    .line 209
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->showLoadingDialog:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 210
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixQueueSelectedItemHashList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 211
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mMoreIndexSelect:[I

    aput v6, v3, v0

    .line 212
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aput v6, v3, v0

    .line 213
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mNetflixList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->invalidateViews()V

    .line 215
    iget-object v3, p0, Lnet/flixster/android/NetflixQueuePage;->mOffsetSelect:[I

    aget v3, v3, v0

    invoke-direct {p0, v3}, Lnet/flixster/android/NetflixQueuePage;->ScheduleLoadMoviesTask(I)V

    .line 218
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 232
    :goto_0
    return-void

    .line 220
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/netflix/queue/dvd"

    const-string v5, "Netflix DVDs"

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 223
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/netflix/queue/instant"

    const-string v5, "Netflix Instant"

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 226
    :pswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/netflix/queue/saved"

    const-string v5, "Netflix Saved"

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 229
    :pswitch_3
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/netflix/queue/athome"

    const-string v5, "Netflix AtHome"

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 285
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 286
    const-string v0, "FlxMain"

    const-string v1, "NetflixQueuePage.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    return-void
.end method
