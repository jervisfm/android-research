.class Lnet/flixster/android/NewsListPage$3;
.super Ljava/util/TimerTask;
.source "NewsListPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/NewsListPage;->ScheduleLoadNewsItemsTask(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/NewsListPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/NewsListPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    .line 57
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 61
    :try_start_0
    invoke-static {}, Lnet/flixster/android/data/NewsDao;->getNewsItemList()Ljava/util/ArrayList;

    move-result-object v3

    .line 63
    .local v3, newsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    new-instance v1, Lnet/flixster/android/lvi/LviPageHeader;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviPageHeader;-><init>()V

    .line 64
    .local v1, lviPageHeader:Lnet/flixster/android/lvi/LviPageHeader;
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    const v6, 0x7f0c00e0

    invoke-virtual {v5, v6}, Lnet/flixster/android/NewsListPage;->getResourceString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lnet/flixster/android/lvi/LviPageHeader;->mTitle:Ljava/lang/String;

    .line 65
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v5, v5, Lnet/flixster/android/NewsListPage;->mData:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    invoke-virtual {v5}, Lnet/flixster/android/NewsListPage;->destroyExistingLviAd()V

    .line 68
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    new-instance v6, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v6, v5, Lnet/flixster/android/NewsListPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 69
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v5, v5, Lnet/flixster/android/NewsListPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v6, "TopNews"

    iput-object v6, v5, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 71
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v5, v5, Lnet/flixster/android/NewsListPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 72
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v5, v5, Lnet/flixster/android/NewsListPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v5, v5, Lnet/flixster/android/NewsListPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v6, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v6, v6, Lnet/flixster/android/NewsListPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 82
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    #getter for: Lnet/flixster/android/NewsListPage;->mUpdateHandler:Landroid/os/Handler;
    invoke-static {v5}, Lnet/flixster/android/NewsListPage;->access$0(Lnet/flixster/android/NewsListPage;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v5, v5, Lnet/flixster/android/NewsListPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v5, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 96
    .end local v1           #lviPageHeader:Lnet/flixster/android/lvi/LviPageHeader;
    .end local v3           #newsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    :goto_1
    return-void

    .line 76
    .restart local v1       #lviPageHeader:Lnet/flixster/android/lvi/LviPageHeader;
    .restart local v3       #newsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    :cond_0
    :try_start_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/NewsStory;

    .line 77
    .local v4, story:Lnet/flixster/android/model/NewsStory;
    new-instance v2, Lnet/flixster/android/lvi/LviNewsStory;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviNewsStory;-><init>()V

    .line 78
    .local v2, lviStory:Lnet/flixster/android/lvi/LviNewsStory;
    iput-object v4, v2, Lnet/flixster/android/lvi/LviNewsStory;->mNewsStory:Lnet/flixster/android/model/NewsStory;

    .line 79
    iget-object v6, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v6, v6, Lnet/flixster/android/NewsListPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 83
    .end local v1           #lviPageHeader:Lnet/flixster/android/lvi/LviPageHeader;
    .end local v2           #lviStory:Lnet/flixster/android/lvi/LviNewsStory;
    .end local v3           #newsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    .end local v4           #story:Lnet/flixster/android/model/NewsStory;
    :catch_0
    move-exception v0

    .line 84
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->printStackTrace()V

    .line 86
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget v6, v5, Lnet/flixster/android/NewsListPage;->mRetryCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v5, Lnet/flixster/android/NewsListPage;->mRetryCount:I

    .line 87
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget v5, v5, Lnet/flixster/android/NewsListPage;->mRetryCount:I

    const/4 v6, 0x3

    if-ge v5, v6, :cond_1

    .line 88
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    const/16 v6, 0x3e8

    #calls: Lnet/flixster/android/NewsListPage;->ScheduleLoadNewsItemsTask(I)V
    invoke-static {v5, v6}, Lnet/flixster/android/NewsListPage;->access$1(Lnet/flixster/android/NewsListPage;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 94
    :goto_2
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v5, v5, Lnet/flixster/android/NewsListPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v5, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 90
    :cond_1
    :try_start_3
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    const/4 v6, 0x0

    iput v6, v5, Lnet/flixster/android/NewsListPage;->mRetryCount:I

    .line 91
    iget-object v5, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v5, v5, Lnet/flixster/android/NewsListPage;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 93
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catchall_0
    move-exception v5

    .line 94
    iget-object v6, p0, Lnet/flixster/android/NewsListPage$3;->this$0:Lnet/flixster/android/NewsListPage;

    iget-object v6, v6, Lnet/flixster/android/NewsListPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 95
    throw v5
.end method
