.class public Lnet/flixster/android/NewsListPage;
.super Lnet/flixster/android/LviActivity;
.source "NewsListPage.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private mNewsStoryClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mUpdateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lnet/flixster/android/LviActivity;-><init>()V

    .line 104
    new-instance v0, Lnet/flixster/android/NewsListPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/NewsListPage$1;-><init>(Lnet/flixster/android/NewsListPage;)V

    iput-object v0, p0, Lnet/flixster/android/NewsListPage;->mUpdateHandler:Landroid/os/Handler;

    .line 114
    new-instance v0, Lnet/flixster/android/NewsListPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/NewsListPage$2;-><init>(Lnet/flixster/android/NewsListPage;)V

    iput-object v0, p0, Lnet/flixster/android/NewsListPage;->mNewsStoryClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 27
    return-void
.end method

.method private ScheduleLoadNewsItemsTask(I)V
    .locals 4
    .parameter "delay"

    .prologue
    .line 55
    iget-object v1, p0, Lnet/flixster/android/NewsListPage;->throbberHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 57
    new-instance v0, Lnet/flixster/android/NewsListPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/NewsListPage$3;-><init>(Lnet/flixster/android/NewsListPage;)V

    .line 99
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/NewsListPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lnet/flixster/android/NewsListPage;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 102
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/NewsListPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lnet/flixster/android/NewsListPage;->mUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/NewsListPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lnet/flixster/android/NewsListPage;->ScheduleLoadNewsItemsTask(I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedState"

    .prologue
    .line 31
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    iget-object v0, p0, Lnet/flixster/android/NewsListPage;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lnet/flixster/android/NewsListPage;->mNewsStoryClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 34
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/topnews"

    const-string v2, "top news"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 51
    invoke-super {p0}, Lnet/flixster/android/LviActivity;->onPause()V

    .line 52
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lnet/flixster/android/LviActivity;->onResume()V

    .line 42
    iget-object v0, p0, Lnet/flixster/android/NewsListPage;->mPageTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/NewsListPage;->mPageTimer:Ljava/util/Timer;

    .line 46
    :cond_0
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lnet/flixster/android/NewsListPage;->ScheduleLoadNewsItemsTask(I)V

    .line 47
    return-void
.end method
