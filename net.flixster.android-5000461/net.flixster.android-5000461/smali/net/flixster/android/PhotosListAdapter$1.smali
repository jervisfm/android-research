.class Lnet/flixster/android/PhotosListAdapter$1;
.super Landroid/os/Handler;
.source "PhotosListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/PhotosListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/PhotosListAdapter;


# direct methods
.method constructor <init>(Lnet/flixster/android/PhotosListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/PhotosListAdapter$1;->this$0:Lnet/flixster/android/PhotosListAdapter;

    .line 79
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 82
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/widget/ImageView;

    .line 83
    .local v0, imageView:Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Photo;

    .line 85
    .local v1, photo:Lnet/flixster/android/model/Photo;
    if-eqz v1, :cond_0

    .line 86
    iget-object v2, v1, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 87
    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 90
    .end local v1           #photo:Lnet/flixster/android/model/Photo;
    :cond_0
    return-void
.end method
