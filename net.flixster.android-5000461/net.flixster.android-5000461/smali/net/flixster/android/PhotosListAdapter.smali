.class public Lnet/flixster/android/PhotosListAdapter;
.super Landroid/widget/BaseAdapter;
.source "PhotosListAdapter.java"


# instance fields
.field private clickListener:Landroid/view/View$OnClickListener;

.field private context:Landroid/content/Context;

.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mPhotoHeight:I

.field private photoHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;)V
    .locals 2
    .parameter "context"
    .parameter
    .parameter "clickListener"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    .local p2, data:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/PhotosListAdapter;->data:Ljava/util/List;

    .line 79
    new-instance v0, Lnet/flixster/android/PhotosListAdapter$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/PhotosListAdapter$1;-><init>(Lnet/flixster/android/PhotosListAdapter;)V

    iput-object v0, p0, Lnet/flixster/android/PhotosListAdapter;->photoHandler:Landroid/os/Handler;

    .line 26
    iput-object p1, p0, Lnet/flixster/android/PhotosListAdapter;->context:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lnet/flixster/android/PhotosListAdapter;->data:Ljava/util/List;

    .line 28
    iput-object p3, p0, Lnet/flixster/android/PhotosListAdapter;->clickListener:Landroid/view/View$OnClickListener;

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/PhotosListAdapter;->mPhotoHeight:I

    .line 31
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lnet/flixster/android/PhotosListAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 72
    iget-object v0, p0, Lnet/flixster/android/PhotosListAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 76
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v6, 0x1

    .line 36
    if-nez p2, :cond_2

    .line 37
    new-instance v4, Landroid/widget/ImageView;

    iget-object v1, p0, Lnet/flixster/android/PhotosListAdapter;->context:Landroid/content/Context;

    invoke-direct {v4, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 42
    .local v4, imageView:Landroid/widget/ImageView;
    :goto_0
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 44
    iget-object v1, p0, Lnet/flixster/android/PhotosListAdapter;->data:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnet/flixster/android/PhotosListAdapter;->data:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p1, :cond_1

    .line 45
    iget-object v1, p0, Lnet/flixster/android/PhotosListAdapter;->data:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Photo;

    .line 46
    .local v2, photo:Lnet/flixster/android/model/Photo;
    if-eqz v2, :cond_1

    .line 47
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 48
    iget-object v1, v2, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    .line 49
    iget-object v1, v2, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 58
    :cond_0
    :goto_1
    iget v1, p0, Lnet/flixster/android/PhotosListAdapter;->mPhotoHeight:I

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 59
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 60
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 61
    iget-object v1, p0, Lnet/flixster/android/PhotosListAdapter;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    .end local v2           #photo:Lnet/flixster/android/model/Photo;
    :cond_1
    return-object v4

    .end local v4           #imageView:Landroid/widget/ImageView;
    :cond_2
    move-object v4, p2

    .line 40
    check-cast v4, Landroid/widget/ImageView;

    .restart local v4       #imageView:Landroid/widget/ImageView;
    goto :goto_0

    .line 50
    .restart local v2       #photo:Lnet/flixster/android/model/Photo;
    :cond_3
    iget-object v1, v2, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 51
    const v1, 0x7f020151

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 52
    new-instance v0, Lnet/flixster/android/model/ImageOrder;

    const/4 v1, 0x3

    iget-object v3, v2, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    .line 53
    iget-object v5, p0, Lnet/flixster/android/PhotosListAdapter;->photoHandler:Landroid/os/Handler;

    .line 52
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 54
    .local v0, imageOrder:Lnet/flixster/android/model/ImageOrder;
    iget-object v1, p0, Lnet/flixster/android/PhotosListAdapter;->context:Landroid/content/Context;

    instance-of v1, v1, Lcom/flixster/android/utils/ImageTask;

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lnet/flixster/android/PhotosListAdapter;->context:Landroid/content/Context;

    check-cast v1, Lcom/flixster/android/utils/ImageTask;

    invoke-interface {v1, v0}, Lcom/flixster/android/utils/ImageTask;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_1
.end method
