.class Lnet/flixster/android/ProfileReviewPage$1;
.super Landroid/os/Handler;
.source "ProfileReviewPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ProfileReviewPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ProfileReviewPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ProfileReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ProfileReviewPage$1;->this$0:Lnet/flixster/android/ProfileReviewPage;

    .line 208
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 212
    iget-object v0, p0, Lnet/flixster/android/ProfileReviewPage$1;->this$0:Lnet/flixster/android/ProfileReviewPage;

    invoke-virtual {v0}, Lnet/flixster/android/ProfileReviewPage;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ProfileReviewPage.updateHandler - mReviewsList:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/flixster/android/ProfileReviewPage$1;->this$0:Lnet/flixster/android/ProfileReviewPage;

    #getter for: Lnet/flixster/android/ProfileReviewPage;->reviews:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/ProfileReviewPage;->access$0(Lnet/flixster/android/ProfileReviewPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lnet/flixster/android/ProfileReviewPage$1;->this$0:Lnet/flixster/android/ProfileReviewPage;

    #calls: Lnet/flixster/android/ProfileReviewPage;->hideLoading()V
    invoke-static {v0}, Lnet/flixster/android/ProfileReviewPage;->access$1(Lnet/flixster/android/ProfileReviewPage;)V

    .line 218
    iget-object v0, p0, Lnet/flixster/android/ProfileReviewPage$1;->this$0:Lnet/flixster/android/ProfileReviewPage;

    #getter for: Lnet/flixster/android/ProfileReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/ProfileReviewPage;->access$2(Lnet/flixster/android/ProfileReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lnet/flixster/android/ProfileReviewPage$1;->this$0:Lnet/flixster/android/ProfileReviewPage;

    #getter for: Lnet/flixster/android/ProfileReviewPage;->reviews:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/ProfileReviewPage;->access$0(Lnet/flixster/android/ProfileReviewPage;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lnet/flixster/android/ProfileReviewPage$1;->this$0:Lnet/flixster/android/ProfileReviewPage;

    #getter for: Lnet/flixster/android/ProfileReviewPage;->reviews:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/ProfileReviewPage;->access$0(Lnet/flixster/android/ProfileReviewPage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 219
    iget-object v0, p0, Lnet/flixster/android/ProfileReviewPage$1;->this$0:Lnet/flixster/android/ProfileReviewPage;

    #calls: Lnet/flixster/android/ProfileReviewPage;->updatePage()V
    invoke-static {v0}, Lnet/flixster/android/ProfileReviewPage;->access$3(Lnet/flixster/android/ProfileReviewPage;)V

    goto :goto_0

    .line 221
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/ProfileReviewPage$1;->this$0:Lnet/flixster/android/ProfileReviewPage;

    invoke-virtual {v0}, Lnet/flixster/android/ProfileReviewPage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lnet/flixster/android/ProfileReviewPage$1;->this$0:Lnet/flixster/android/ProfileReviewPage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/ProfileReviewPage;->showDialog(I)V

    goto :goto_0
.end method
