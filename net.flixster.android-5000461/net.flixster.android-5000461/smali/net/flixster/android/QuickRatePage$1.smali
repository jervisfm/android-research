.class Lnet/flixster/android/QuickRatePage$1;
.super Landroid/os/Handler;
.source "QuickRatePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/QuickRatePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/QuickRatePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/QuickRatePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/QuickRatePage$1;->this$0:Lnet/flixster/android/QuickRatePage;

    .line 66
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    .line 69
    iget-object v3, p0, Lnet/flixster/android/QuickRatePage$1;->this$0:Lnet/flixster/android/QuickRatePage;

    #getter for: Lnet/flixster/android/QuickRatePage;->quickRateList:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lnet/flixster/android/QuickRatePage;->access$0(Lnet/flixster/android/QuickRatePage;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 70
    iget-object v3, p0, Lnet/flixster/android/QuickRatePage$1;->this$0:Lnet/flixster/android/QuickRatePage;

    #getter for: Lnet/flixster/android/QuickRatePage;->throbber:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lnet/flixster/android/QuickRatePage;->access$1(Lnet/flixster/android/QuickRatePage;)Landroid/widget/ProgressBar;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 71
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    .line 72
    .local v2, quickRateMovies:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 77
    return-void

    .line 72
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .line 73
    .local v0, movie:Lnet/flixster/android/model/Movie;
    new-instance v1, Lcom/flixster/android/view/QuickRateView;

    iget-object v4, p0, Lnet/flixster/android/QuickRatePage$1;->this$0:Lnet/flixster/android/QuickRatePage;

    iget-object v5, p0, Lnet/flixster/android/QuickRatePage$1;->this$0:Lnet/flixster/android/QuickRatePage;

    #getter for: Lnet/flixster/android/QuickRatePage;->isWts:Z
    invoke-static {v5}, Lnet/flixster/android/QuickRatePage;->access$2(Lnet/flixster/android/QuickRatePage;)Z

    move-result v5

    iget-object v6, p0, Lnet/flixster/android/QuickRatePage$1;->this$0:Lnet/flixster/android/QuickRatePage;

    #getter for: Lnet/flixster/android/QuickRatePage;->quickRateActionHandler:Landroid/os/Handler;
    invoke-static {v6}, Lnet/flixster/android/QuickRatePage;->access$3(Lnet/flixster/android/QuickRatePage;)Landroid/os/Handler;

    move-result-object v6

    invoke-direct {v1, v4, v5, v6}, Lcom/flixster/android/view/QuickRateView;-><init>(Landroid/content/Context;ZLandroid/os/Handler;)V

    .line 74
    .local v1, qrView:Lcom/flixster/android/view/QuickRateView;
    iget-object v4, p0, Lnet/flixster/android/QuickRatePage$1;->this$0:Lnet/flixster/android/QuickRatePage;

    #getter for: Lnet/flixster/android/QuickRatePage;->quickRateList:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lnet/flixster/android/QuickRatePage;->access$0(Lnet/flixster/android/QuickRatePage;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 75
    invoke-virtual {v1, v0}, Lcom/flixster/android/view/QuickRateView;->load(Lnet/flixster/android/model/Movie;)V

    goto :goto_0
.end method
