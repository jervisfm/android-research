.class public Lnet/flixster/android/QuickRatePage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "QuickRatePage.java"


# static fields
.field public static final KEY_IS_WTS:Ljava/lang/String; = "KEY_IS_WTS"

.field public static final REWARDS_QUICKRATE_DEFAULT:I = 0x19


# instance fields
.field private final errorHandler:Landroid/os/Handler;

.field private isWts:Z

.field private final movieEarnedDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

.field private progressCompleted:I

.field private final quickRateActionHandler:Landroid/os/Handler;

.field private quickRateList:Landroid/widget/LinearLayout;

.field private quickRateProgress:Landroid/widget/TextView;

.field private rewardThreshold:I

.field private final successHandler:Landroid/os/Handler;

.field private throbber:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 66
    new-instance v0, Lnet/flixster/android/QuickRatePage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/QuickRatePage$1;-><init>(Lnet/flixster/android/QuickRatePage;)V

    iput-object v0, p0, Lnet/flixster/android/QuickRatePage;->successHandler:Landroid/os/Handler;

    .line 80
    new-instance v0, Lnet/flixster/android/QuickRatePage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/QuickRatePage$2;-><init>(Lnet/flixster/android/QuickRatePage;)V

    iput-object v0, p0, Lnet/flixster/android/QuickRatePage;->errorHandler:Landroid/os/Handler;

    .line 89
    new-instance v0, Lnet/flixster/android/QuickRatePage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/QuickRatePage$3;-><init>(Lnet/flixster/android/QuickRatePage;)V

    iput-object v0, p0, Lnet/flixster/android/QuickRatePage;->quickRateActionHandler:Landroid/os/Handler;

    .line 128
    new-instance v0, Lnet/flixster/android/QuickRatePage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/QuickRatePage$4;-><init>(Lnet/flixster/android/QuickRatePage;)V

    iput-object v0, p0, Lnet/flixster/android/QuickRatePage;->movieEarnedDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 27
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/QuickRatePage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lnet/flixster/android/QuickRatePage;->quickRateList:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/QuickRatePage;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lnet/flixster/android/QuickRatePage;->throbber:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/QuickRatePage;)Z
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-boolean v0, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    return v0
.end method

.method static synthetic access$3(Lnet/flixster/android/QuickRatePage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lnet/flixster/android/QuickRatePage;->quickRateActionHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/QuickRatePage;)I
    .locals 1
    .parameter

    .prologue
    .line 33
    iget v0, p0, Lnet/flixster/android/QuickRatePage;->progressCompleted:I

    return v0
.end method

.method static synthetic access$5(Lnet/flixster/android/QuickRatePage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33
    iput p1, p0, Lnet/flixster/android/QuickRatePage;->progressCompleted:I

    return-void
.end method

.method static synthetic access$6(Lnet/flixster/android/QuickRatePage;)V
    .locals 0
    .parameter

    .prologue
    .line 96
    invoke-direct {p0}, Lnet/flixster/android/QuickRatePage;->updateQuickRateProgress()V

    return-void
.end method

.method private updateQuickRateProgress()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 97
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v8

    .line 98
    .local v8, user:Lnet/flixster/android/model/User;
    iget-boolean v0, v8, Lnet/flixster/android/model/User;->isMskEligible:Z

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lnet/flixster/android/QuickRatePage;->quickRateProgress:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/QuickRatePage;->quickRateProgress:Landroid/widget/TextView;

    const v1, 0x7f0c0155

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lnet/flixster/android/QuickRatePage;->progressCompleted:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    .line 102
    iget v4, p0, Lnet/flixster/android/QuickRatePage;->rewardThreshold:I

    iget v5, p0, Lnet/flixster/android/QuickRatePage;->progressCompleted:I

    sub-int/2addr v4, v5

    invoke-static {v4, v9}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    .line 101
    invoke-virtual {p0, v1, v3}, Lnet/flixster/android/QuickRatePage;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lnet/flixster/android/QuickRatePage;->quickRateProgress:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    iget v0, p0, Lnet/flixster/android/QuickRatePage;->progressCompleted:I

    iget v1, p0, Lnet/flixster/android/QuickRatePage;->rewardThreshold:I

    if-lt v0, v1, :cond_3

    .line 106
    iget-boolean v0, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    if-eqz v0, :cond_1

    .line 107
    const-string v7, "MKW"

    .line 108
    .local v7, purchaseType:Ljava/lang/String;
    iput-boolean v9, v8, Lnet/flixster/android/model/User;->isMskWtsEligible:Z

    .line 113
    :goto_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/rewards"

    const-string v3, "FlixsterRewards"

    const-string v4, "CompletedReward"

    .line 114
    iget-boolean v5, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    if-eqz v5, :cond_2

    const-string v5, "QuickWts"

    .line 113
    :goto_2
    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 116
    invoke-static {v2, v2, v7}, Lnet/flixster/android/data/ProfileDao;->insertLockerRight(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    .line 117
    const v0, 0x3b9acb91

    iget-object v1, p0, Lnet/flixster/android/QuickRatePage;->movieEarnedDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    invoke-virtual {p0, v0, v1}, Lnet/flixster/android/QuickRatePage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    goto :goto_0

    .line 110
    .end local v7           #purchaseType:Ljava/lang/String;
    :cond_1
    const-string v7, "MKR"

    .line 111
    .restart local v7       #purchaseType:Ljava/lang/String;
    iput-boolean v9, v8, Lnet/flixster/android/model/User;->isMskRateEligible:Z

    goto :goto_1

    .line 114
    :cond_2
    const-string v5, "QuickRate"

    goto :goto_2

    .line 119
    .end local v7           #purchaseType:Ljava/lang/String;
    :cond_3
    iget-boolean v0, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    if-eqz v0, :cond_4

    .line 120
    iget v0, p0, Lnet/flixster/android/QuickRatePage;->progressCompleted:I

    iput v0, v8, Lnet/flixster/android/model/User;->wtsCount:I

    goto :goto_0

    .line 122
    :cond_4
    iget v0, p0, Lnet/flixster/android/QuickRatePage;->progressCompleted:I

    iput v0, v8, Lnet/flixster/android/model/User;->ratingCount:I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, 0x0

    .line 41
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v2, 0x7f03006f

    invoke-virtual {p0, v2}, Lnet/flixster/android/QuickRatePage;->setContentView(I)V

    .line 43
    invoke-virtual {p0}, Lnet/flixster/android/QuickRatePage;->createActionBar()V

    .line 45
    invoke-virtual {p0}, Lnet/flixster/android/QuickRatePage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "KEY_IS_WTS"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    .line 46
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 47
    .local v0, p:Lnet/flixster/android/model/Property;
    if-eqz v0, :cond_0

    iget-boolean v2, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    if-eqz v2, :cond_1

    iget v2, v0, Lnet/flixster/android/model/Property;->rewardsQuickWts:I

    :goto_0
    iput v2, p0, Lnet/flixster/android/QuickRatePage;->rewardThreshold:I

    if-nez v2, :cond_2

    :cond_0
    const/16 v2, 0x19

    :goto_1
    iput v2, p0, Lnet/flixster/android/QuickRatePage;->rewardThreshold:I

    .line 49
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v1

    .line 50
    .local v1, user:Lnet/flixster/android/model/User;
    iget-boolean v2, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    if-eqz v2, :cond_3

    iget v2, v1, Lnet/flixster/android/model/User;->wtsCount:I

    :goto_2
    iput v2, p0, Lnet/flixster/android/QuickRatePage;->progressCompleted:I

    .line 52
    const v2, 0x7f07020d

    invoke-virtual {p0, v2}, Lnet/flixster/android/QuickRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lnet/flixster/android/QuickRatePage;->quickRateProgress:Landroid/widget/TextView;

    .line 53
    const v2, 0x7f07020e

    invoke-virtual {p0, v2}, Lnet/flixster/android/QuickRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lnet/flixster/android/QuickRatePage;->quickRateList:Landroid/widget/LinearLayout;

    .line 54
    const v2, 0x7f070039

    invoke-virtual {p0, v2}, Lnet/flixster/android/QuickRatePage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lnet/flixster/android/QuickRatePage;->throbber:Landroid/widget/ProgressBar;

    .line 56
    iget-boolean v2, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    if-eqz v2, :cond_4

    const v2, 0x7f0c0154

    :goto_3
    invoke-virtual {p0, v2}, Lnet/flixster/android/QuickRatePage;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lnet/flixster/android/QuickRatePage;->setActionBarTitle(Ljava/lang/String;)V

    .line 57
    iget-object v3, p0, Lnet/flixster/android/QuickRatePage;->quickRateProgress:Landroid/widget/TextView;

    iget-boolean v2, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    if-eqz v2, :cond_5

    const v2, 0x7f020163

    :goto_4
    invoke-virtual {v3, v2, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 59
    invoke-direct {p0}, Lnet/flixster/android/QuickRatePage;->updateQuickRateProgress()V

    .line 60
    iget-object v2, p0, Lnet/flixster/android/QuickRatePage;->throbber:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 61
    iget-object v2, p0, Lnet/flixster/android/QuickRatePage;->successHandler:Landroid/os/Handler;

    iget-object v3, p0, Lnet/flixster/android/QuickRatePage;->errorHandler:Landroid/os/Handler;

    iget-boolean v4, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    invoke-static {v2, v3, v4}, Lnet/flixster/android/data/ProfileDao;->getQuickRateMovies(Landroid/os/Handler;Landroid/os/Handler;Z)V

    .line 62
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v4

    iget-boolean v2, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    if-eqz v2, :cond_6

    const-string v2, "/rewards/quick-wts"

    .line 63
    :goto_5
    iget-boolean v3, p0, Lnet/flixster/android/QuickRatePage;->isWts:Z

    if-eqz v3, :cond_7

    const-string v3, "Rewards - QuickWts"

    .line 62
    :goto_6
    invoke-interface {v4, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void

    .line 47
    .end local v1           #user:Lnet/flixster/android/model/User;
    :cond_1
    iget v2, v0, Lnet/flixster/android/model/Property;->rewardsQuickRate:I

    goto :goto_0

    .line 48
    :cond_2
    iget v2, p0, Lnet/flixster/android/QuickRatePage;->rewardThreshold:I

    goto :goto_1

    .line 50
    .restart local v1       #user:Lnet/flixster/android/model/User;
    :cond_3
    iget v2, v1, Lnet/flixster/android/model/User;->ratingCount:I

    goto :goto_2

    .line 56
    :cond_4
    const v2, 0x7f0c0153

    goto :goto_3

    .line 58
    :cond_5
    const v2, 0x7f02015f

    goto :goto_4

    .line 62
    :cond_6
    const-string v2, "/rewards/quick-rate"

    goto :goto_5

    .line 63
    :cond_7
    const-string v3, "Rewards - QuickRate"

    goto :goto_6
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 145
    const/4 v0, 0x1

    return v0
.end method
