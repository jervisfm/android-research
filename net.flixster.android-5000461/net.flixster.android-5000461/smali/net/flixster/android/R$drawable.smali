.class public final Lnet/flixster/android/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_bottom_solid_flxabv44:I = 0x7f020000

.field public static final ab_flixsterlogo:I = 0x7f020001

.field public static final ab_popcorn:I = 0x7f020002

.field public static final ab_solid_flxabv44:I = 0x7f020003

.field public static final ab_stacked_solid_flxabv44:I = 0x7f020004

.field public static final ab_transparent_flxabv44:I = 0x7f020005

.field public static final abs__ab_bottom_solid_dark_holo:I = 0x7f020006

.field public static final abs__ab_bottom_solid_inverse_holo:I = 0x7f020007

.field public static final abs__ab_bottom_solid_light_holo:I = 0x7f020008

.field public static final abs__ab_bottom_transparent_dark_holo:I = 0x7f020009

.field public static final abs__ab_bottom_transparent_light_holo:I = 0x7f02000a

.field public static final abs__ab_share_pack_holo_dark:I = 0x7f02000b

.field public static final abs__ab_share_pack_holo_light:I = 0x7f02000c

.field public static final abs__ab_solid_dark_holo:I = 0x7f02000d

.field public static final abs__ab_solid_light_holo:I = 0x7f02000e

.field public static final abs__ab_solid_shadow_holo:I = 0x7f02000f

.field public static final abs__ab_stacked_solid_dark_holo:I = 0x7f020010

.field public static final abs__ab_stacked_solid_light_holo:I = 0x7f020011

.field public static final abs__ab_stacked_transparent_dark_holo:I = 0x7f020012

.field public static final abs__ab_stacked_transparent_light_holo:I = 0x7f020013

.field public static final abs__ab_transparent_dark_holo:I = 0x7f020014

.field public static final abs__ab_transparent_light_holo:I = 0x7f020015

.field public static final abs__activated_background_holo_dark:I = 0x7f020016

.field public static final abs__activated_background_holo_light:I = 0x7f020017

.field public static final abs__btn_cab_done_default_holo_dark:I = 0x7f020018

.field public static final abs__btn_cab_done_default_holo_light:I = 0x7f020019

.field public static final abs__btn_cab_done_focused_holo_dark:I = 0x7f02001a

.field public static final abs__btn_cab_done_focused_holo_light:I = 0x7f02001b

.field public static final abs__btn_cab_done_holo_dark:I = 0x7f02001c

.field public static final abs__btn_cab_done_holo_light:I = 0x7f02001d

.field public static final abs__btn_cab_done_pressed_holo_dark:I = 0x7f02001e

.field public static final abs__btn_cab_done_pressed_holo_light:I = 0x7f02001f

.field public static final abs__cab_background_bottom_holo_dark:I = 0x7f020020

.field public static final abs__cab_background_bottom_holo_light:I = 0x7f020021

.field public static final abs__cab_background_top_holo_dark:I = 0x7f020022

.field public static final abs__cab_background_top_holo_light:I = 0x7f020023

.field public static final abs__dialog_full_holo_dark:I = 0x7f020024

.field public static final abs__dialog_full_holo_light:I = 0x7f020025

.field public static final abs__ic_ab_back_holo_dark:I = 0x7f020026

.field public static final abs__ic_ab_back_holo_light:I = 0x7f020027

.field public static final abs__ic_cab_done_holo_dark:I = 0x7f020028

.field public static final abs__ic_cab_done_holo_light:I = 0x7f020029

.field public static final abs__ic_menu_moreoverflow_holo_dark:I = 0x7f02002a

.field public static final abs__ic_menu_moreoverflow_holo_light:I = 0x7f02002b

.field public static final abs__ic_menu_moreoverflow_normal_holo_dark:I = 0x7f02002c

.field public static final abs__ic_menu_moreoverflow_normal_holo_light:I = 0x7f02002d

.field public static final abs__ic_menu_share_holo_dark:I = 0x7f02002e

.field public static final abs__ic_menu_share_holo_light:I = 0x7f02002f

.field public static final abs__item_background_holo_dark:I = 0x7f020030

.field public static final abs__item_background_holo_light:I = 0x7f020031

.field public static final abs__list_activated_holo:I = 0x7f020032

.field public static final abs__list_divider_holo_dark:I = 0x7f020033

.field public static final abs__list_divider_holo_light:I = 0x7f020034

.field public static final abs__list_focused_holo:I = 0x7f020035

.field public static final abs__list_longpressed_holo:I = 0x7f020036

.field public static final abs__list_pressed_holo_dark:I = 0x7f020037

.field public static final abs__list_pressed_holo_light:I = 0x7f020038

.field public static final abs__list_selector_background_transition_holo_dark:I = 0x7f020039

.field public static final abs__list_selector_background_transition_holo_light:I = 0x7f02003a

.field public static final abs__list_selector_disabled_holo_dark:I = 0x7f02003b

.field public static final abs__list_selector_disabled_holo_light:I = 0x7f02003c

.field public static final abs__list_selector_holo_dark:I = 0x7f02003d

.field public static final abs__list_selector_holo_light:I = 0x7f02003e

.field public static final abs__menu_dropdown_panel_holo_dark:I = 0x7f02003f

.field public static final abs__menu_dropdown_panel_holo_light:I = 0x7f020040

.field public static final abs__progress_bg_holo_dark:I = 0x7f020041

.field public static final abs__progress_bg_holo_light:I = 0x7f020042

.field public static final abs__progress_horizontal_holo_dark:I = 0x7f020043

.field public static final abs__progress_horizontal_holo_light:I = 0x7f020044

.field public static final abs__progress_medium_holo:I = 0x7f020045

.field public static final abs__progress_primary_holo_dark:I = 0x7f020046

.field public static final abs__progress_primary_holo_light:I = 0x7f020047

.field public static final abs__progress_secondary_holo_dark:I = 0x7f020048

.field public static final abs__progress_secondary_holo_light:I = 0x7f020049

.field public static final abs__spinner_48_inner_holo:I = 0x7f02004a

.field public static final abs__spinner_48_outer_holo:I = 0x7f02004b

.field public static final abs__spinner_ab_default_holo_dark:I = 0x7f02004c

.field public static final abs__spinner_ab_default_holo_light:I = 0x7f02004d

.field public static final abs__spinner_ab_disabled_holo_dark:I = 0x7f02004e

.field public static final abs__spinner_ab_disabled_holo_light:I = 0x7f02004f

.field public static final abs__spinner_ab_focused_holo_dark:I = 0x7f020050

.field public static final abs__spinner_ab_focused_holo_light:I = 0x7f020051

.field public static final abs__spinner_ab_holo_dark:I = 0x7f020052

.field public static final abs__spinner_ab_holo_light:I = 0x7f020053

.field public static final abs__spinner_ab_pressed_holo_dark:I = 0x7f020054

.field public static final abs__spinner_ab_pressed_holo_light:I = 0x7f020055

.field public static final abs__tab_indicator_ab_holo:I = 0x7f020056

.field public static final abs__tab_selected_focused_holo:I = 0x7f020057

.field public static final abs__tab_selected_holo:I = 0x7f020058

.field public static final abs__tab_selected_pressed_holo:I = 0x7f020059

.field public static final abs__tab_unselected_pressed_holo:I = 0x7f02005a

.field public static final actionbar_bg:I = 0x7f02005b

.field public static final actionbar_bg_grainy:I = 0x7f02005c

.field public static final actionbartab_bg_selector:I = 0x7f02005d

.field public static final ad_label_bg:I = 0x7f02005e

.field public static final ad_skip_button_bg:I = 0x7f02005f

.field public static final alert_icon_check:I = 0x7f020060

.field public static final alert_icon_error:I = 0x7f020061

.field public static final arrow_down:I = 0x7f020062

.field public static final arrow_left_purple:I = 0x7f020063

.field public static final arrow_right_blue:I = 0x7f020064

.field public static final arrow_up:I = 0x7f020065

.field public static final backarrow_black:I = 0x7f020066

.field public static final backarrow_selector:I = 0x7f020067

.field public static final backarrow_white:I = 0x7f020068

.field public static final bg_active_topdiv:I = 0x7f020069

.field public static final bg_connectrate:I = 0x7f02006a

.field public static final bg_facebookbuttongradient:I = 0x7f02006b

.field public static final bg_inactive:I = 0x7f02006c

.field public static final bg_inactive_topdiv:I = 0x7f02006d

.field public static final bg_selected_leftarrow:I = 0x7f02006e

.field public static final bg_selected_topdiv:I = 0x7f02006f

.field public static final bg_selected_topdiv_arrow:I = 0x7f020070

.field public static final bg_starbullet_purple:I = 0x7f020071

.field public static final bg_subheader:I = 0x7f020072

.field public static final bgborder:I = 0x7f020073

.field public static final bgline:I = 0x7f020074

.field public static final bgpanel_active_topdiv:I = 0x7f020075

.field public static final bgpanel_inactive_topdiv:I = 0x7f020076

.field public static final bgpanel_location:I = 0x7f020077

.field public static final box1:I = 0x7f020078

.field public static final box2:I = 0x7f020079

.field public static final box3:I = 0x7f02007a

.field public static final box_white:I = 0x7f02007b

.field public static final box_white_topdiv_layer:I = 0x7f02007c

.field public static final boxoffice_active:I = 0x7f02007d

.field public static final boxoffice_selected:I = 0x7f02007e

.field public static final boxoffice_selector:I = 0x7f02007f

.field public static final boxoffice_standard:I = 0x7f020080

.field public static final bullet_1:I = 0x7f020081

.field public static final bullet_2:I = 0x7f020082

.field public static final bullet_3:I = 0x7f020083

.field public static final button_active:I = 0x7f020084

.field public static final button_buy:I = 0x7f020085

.field public static final button_dec:I = 0x7f020086

.field public static final button_dec_pressed:I = 0x7f020087

.field public static final button_dec_selected:I = 0x7f020088

.field public static final button_delete:I = 0x7f020089

.field public static final button_fblogin:I = 0x7f02008a

.field public static final button_fblogin_pressed:I = 0x7f02008b

.field public static final button_fblogin_unpressed:I = 0x7f02008c

.field public static final button_fbloginfacebook:I = 0x7f02008d

.field public static final button_fbloginfacebook_pressed:I = 0x7f02008e

.field public static final button_fbloginfacebook_unpressed:I = 0x7f02008f

.field public static final button_fblogout:I = 0x7f020090

.field public static final button_fblogout_pressed:I = 0x7f020091

.field public static final button_fblogout_unpressed:I = 0x7f020092

.field public static final button_focused_selector:I = 0x7f020093

.field public static final button_green:I = 0x7f020094

.field public static final button_inc:I = 0x7f020095

.field public static final button_inc_pressed:I = 0x7f020096

.field public static final button_inc_selected:I = 0x7f020097

.field public static final button_leftarrow:I = 0x7f020098

.field public static final button_leftarrow_dis:I = 0x7f020099

.field public static final button_leftarrow_ena:I = 0x7f02009a

.field public static final button_nav_focus:I = 0x7f02009b

.field public static final button_nav_selected:I = 0x7f02009c

.field public static final button_nav_unselected:I = 0x7f02009d

.field public static final button_rightarrow:I = 0x7f02009e

.field public static final button_rightarrow_dis:I = 0x7f02009f

.field public static final button_rightarrow_ena:I = 0x7f0200a0

.field public static final button_selector:I = 0x7f0200a1

.field public static final button_standard:I = 0x7f0200a2

.field public static final button_subnav:I = 0x7f0200a3

.field public static final button_tickets_dec:I = 0x7f0200a4

.field public static final button_tickets_inc:I = 0x7f0200a5

.field public static final check_off:I = 0x7f0200a6

.field public static final check_on:I = 0x7f0200a7

.field public static final close:I = 0x7f0200a8

.field public static final close_button:I = 0x7f0200a9

.field public static final close_button_gray:I = 0x7f0200aa

.field public static final close_button_gray_touchable:I = 0x7f0200ab

.field public static final connect_rating_screen:I = 0x7f0200ac

.field public static final current_location:I = 0x7f0200ad

.field public static final dcrpromo:I = 0x7f0200ae

.field public static final dcrpromo_hdpi:I = 0x7f0200af

.field public static final divider_box:I = 0x7f0200b0

.field public static final dlarrow:I = 0x7f0200b1

.field public static final dvd_active:I = 0x7f0200b2

.field public static final dvd_selected:I = 0x7f0200b3

.field public static final dvd_selector:I = 0x7f0200b4

.field public static final dvd_standard:I = 0x7f0200b5

.field public static final error_bubble:I = 0x7f0200b6

.field public static final error_bubble_left:I = 0x7f0200b7

.field public static final errorpage:I = 0x7f0200b8

.field public static final expire:I = 0x7f0200b9

.field public static final fbinvite:I = 0x7f0200ba

.field public static final fbinvitedone:I = 0x7f0200bb

.field public static final flixster_frame:I = 0x7f0200bc

.field public static final flixster_sm_padded:I = 0x7f0200bd

.field public static final flixsterlogo:I = 0x7f0200be

.field public static final giftbox:I = 0x7f0200bf

.field public static final global:I = 0x7f0200c0

.field public static final goarrow:I = 0x7f0200c1

.field public static final gray_list_selector_background:I = 0x7f0200c2

.field public static final gridview_selector:I = 0x7f0200c3

.field public static final grip:I = 0x7f0200c4

.field public static final header_starbox:I = 0x7f0200c5

.field public static final ic_ab_addfave:I = 0x7f0200c6

.field public static final ic_ab_back_holo_dark:I = 0x7f0200c7

.field public static final ic_ab_removefave:I = 0x7f0200c8

.field public static final ic_ab_save:I = 0x7f0200c9

.field public static final ic_action_next:I = 0x7f0200ca

.field public static final ic_action_overflow:I = 0x7f0200cb

.field public static final ic_action_previous:I = 0x7f0200cc

.field public static final ic_action_refresh:I = 0x7f0200cd

.field public static final ic_action_refresh_dark:I = 0x7f0200ce

.field public static final ic_action_search:I = 0x7f0200cf

.field public static final ic_action_share:I = 0x7f0200d0

.field public static final ic_menu_actor:I = 0x7f0200d1

.field public static final ic_menu_back:I = 0x7f0200d2

.field public static final ic_menu_home:I = 0x7f0200d3

.field public static final ic_menu_invite:I = 0x7f0200d4

.field public static final ic_menu_movie:I = 0x7f0200d5

.field public static final ic_menu_preferences:I = 0x7f0200d6

.field public static final ic_menu_search:I = 0x7f0200d7

.field public static final icon_boxoffice_selected:I = 0x7f0200d8

.field public static final icon_boxoffice_unselected:I = 0x7f0200d9

.field public static final icon_collections:I = 0x7f0200da

.field public static final icon_dvd_selected:I = 0x7f0200db

.field public static final icon_dvd_unselected:I = 0x7f0200dc

.field public static final icon_fresh_md:I = 0x7f0200dd

.field public static final icon_fresh_sm:I = 0x7f0200de

.field public static final icon_friends:I = 0x7f0200df

.field public static final icon_home_selected:I = 0x7f0200e0

.field public static final icon_home_unselected:I = 0x7f0200e1

.field public static final icon_launcher:I = 0x7f0200e2

.field public static final icon_launcher_alt:I = 0x7f0200e3

.field public static final icon_more_arrow:I = 0x7f0200e4

.field public static final icon_mymovies_selected:I = 0x7f0200e5

.field public static final icon_mymovies_unselected:I = 0x7f0200e6

.field public static final icon_offline_alert:I = 0x7f0200e7

.field public static final icon_playbutton:I = 0x7f0200e8

.field public static final icon_popcorn_md:I = 0x7f0200e9

.field public static final icon_popcorn_sm:I = 0x7f0200ea

.field public static final icon_review:I = 0x7f0200eb

.field public static final icon_rotten_md:I = 0x7f0200ec

.field public static final icon_rotten_sm:I = 0x7f0200ed

.field public static final icon_spilled_md:I = 0x7f0200ee

.field public static final icon_spilled_sm:I = 0x7f0200ef

.field public static final icon_theaters_selected:I = 0x7f0200f0

.field public static final icon_theaters_unselected:I = 0x7f0200f1

.field public static final icon_ticket:I = 0x7f0200f2

.field public static final icon_upcoming_selected:I = 0x7f0200f3

.field public static final icon_upcoming_unselected:I = 0x7f0200f4

.field public static final icon_wts_md:I = 0x7f0200f5

.field public static final icon_wts_sm:I = 0x7f0200f6

.field public static final icon_wts_social:I = 0x7f0200f7

.field public static final iconbox_7eleven:I = 0x7f0200f8

.field public static final iconbox_actor:I = 0x7f0200f9

.field public static final iconbox_blockbuster:I = 0x7f0200fa

.field public static final iconbox_collection:I = 0x7f0200fb

.field public static final iconbox_date:I = 0x7f0200fc

.field public static final iconbox_download:I = 0x7f0200fd

.field public static final iconbox_facebook:I = 0x7f0200fe

.field public static final iconbox_faq:I = 0x7f0200ff

.field public static final iconbox_flixster:I = 0x7f020100

.field public static final iconbox_friendactivity:I = 0x7f020101

.field public static final iconbox_friends:I = 0x7f020102

.field public static final iconbox_getglue:I = 0x7f020103

.field public static final iconbox_imdb:I = 0x7f020104

.field public static final iconbox_location:I = 0x7f020105

.field public static final iconbox_mail:I = 0x7f020106

.field public static final iconbox_map:I = 0x7f020107

.field public static final iconbox_netflix:I = 0x7f020108

.field public static final iconbox_news:I = 0x7f020109

.field public static final iconbox_phone:I = 0x7f02010a

.field public static final iconbox_photo:I = 0x7f02010b

.field public static final iconbox_privacy:I = 0x7f02010c

.field public static final iconbox_rating:I = 0x7f02010d

.field public static final iconbox_recommend:I = 0x7f02010e

.field public static final iconbox_rt:I = 0x7f02010f

.field public static final iconbox_settings:I = 0x7f020110

.field public static final iconbox_tickets:I = 0x7f020111

.field public static final iconbox_trailer:I = 0x7f020112

.field public static final iconbox_wts:I = 0x7f020113

.field public static final iconbox_yelp:I = 0x7f020114

.field public static final image_highlight_background:I = 0x7f020115

.field public static final image_highlight_selector:I = 0x7f020116

.field public static final leftnav_bar_background_light:I = 0x7f020117

.field public static final leftnav_bar_home_up:I = 0x7f020118

.field public static final leftnav_bar_item_background_dark:I = 0x7f020119

.field public static final leftnav_bar_item_background_focused_dark:I = 0x7f02011a

.field public static final leftnav_bar_item_background_focused_light:I = 0x7f02011b

.field public static final leftnav_bar_item_background_light:I = 0x7f02011c

.field public static final leftnav_bar_item_background_pressed_dark:I = 0x7f02011d

.field public static final leftnav_bar_item_background_pressed_light:I = 0x7f02011e

.field public static final leftnav_bar_item_background_selected_dark:I = 0x7f02011f

.field public static final leftnav_bar_item_background_selected_focused_dark:I = 0x7f020120

.field public static final leftnav_bar_item_background_selected_focused_light:I = 0x7f020121

.field public static final leftnav_bar_item_background_selected_light:I = 0x7f020122

.field public static final leftnav_bar_menu_background_bottom_dark:I = 0x7f020123

.field public static final leftnav_bar_menu_background_bottom_light:I = 0x7f020124

.field public static final leftnav_bar_menu_background_top_dark:I = 0x7f020125

.field public static final leftnav_bar_menu_background_top_light:I = 0x7f020126

.field public static final leftnav_bar_option_icon_dark:I = 0x7f020127

.field public static final leftnav_bar_option_icon_focused_dark:I = 0x7f020128

.field public static final leftnav_bar_option_icon_focused_light:I = 0x7f020129

.field public static final leftnav_bar_option_icon_light:I = 0x7f02012a

.field public static final leftnav_bar_option_icon_normal_dark:I = 0x7f02012b

.field public static final leftnav_bar_option_icon_normal_light:I = 0x7f02012c

.field public static final leftnav_bar_tab_separator_light:I = 0x7f02012d

.field public static final leftnav_bar_tab_separator_transparent:I = 0x7f02012e

.field public static final list_focused_flxabv44:I = 0x7f02012f

.field public static final list_starbox:I = 0x7f020130

.field public static final listdivider:I = 0x7f020131

.field public static final listitem_bg:I = 0x7f020132

.field public static final listpanel_white_topdiv:I = 0x7f020133

.field public static final loading_static:I = 0x7f020134

.field public static final login_facebook:I = 0x7f020135

.field public static final login_facebook_alt:I = 0x7f020136

.field public static final login_flixster:I = 0x7f020137

.field public static final login_flixster_new:I = 0x7f020138

.field public static final login_netflix:I = 0x7f020139

.field public static final logout_flixster:I = 0x7f02013a

.field public static final macbar:I = 0x7f02013b

.field public static final menu_dropdown_panel_flxabv44:I = 0x7f02013c

.field public static final menu_hardkey_panel_flxabv44:I = 0x7f02013d

.field public static final mmfriends:I = 0x7f02013e

.field public static final mmown:I = 0x7f02013f

.field public static final mmratings:I = 0x7f020140

.field public static final mmselected:I = 0x7f020141

.field public static final mmwts:I = 0x7f020142

.field public static final movie_rating_bar:I = 0x7f020143

.field public static final movie_tickets:I = 0x7f020144

.field public static final moviedetail_bar_bg:I = 0x7f020145

.field public static final mskpromoanon_b:I = 0x7f020146

.field public static final mskpromologged_b:I = 0x7f020147

.field public static final mymovies_active:I = 0x7f020148

.field public static final mymovies_image_frame:I = 0x7f020149

.field public static final mymovies_selected:I = 0x7f02014a

.field public static final mymovies_selector:I = 0x7f02014b

.field public static final mymovies_standard:I = 0x7f02014c

.field public static final netflix_checkbox:I = 0x7f02014d

.field public static final netflixlogo:I = 0x7f02014e

.field public static final noposter:I = 0x7f02014f

.field public static final nuke:I = 0x7f020150

.field public static final photo_loading:I = 0x7f020151

.field public static final pin:I = 0x7f020152

.field public static final poster_frame:I = 0x7f020153

.field public static final poster_loading_small:I = 0x7f020154

.field public static final posterpromo:I = 0x7f020155

.field public static final pressed_background_flxabv44:I = 0x7f020156

.field public static final preview:I = 0x7f020157

.field public static final progress_bg_flxabv44:I = 0x7f020158

.field public static final progress_horizontal:I = 0x7f020159

.field public static final progress_horizontal_flxabv44:I = 0x7f02015a

.field public static final progress_primary_flxabv44:I = 0x7f02015b

.field public static final progress_secondary_flxabv44:I = 0x7f02015c

.field public static final promogiftbox:I = 0x7f02015d

.field public static final prompt:I = 0x7f02015e

.field public static final quickrate_ratingicon:I = 0x7f02015f

.field public static final quickrate_starempty:I = 0x7f020160

.field public static final quickrate_starfull:I = 0x7f020161

.field public static final quickrate_starhalf:I = 0x7f020162

.field public static final quickrate_wtsicon:I = 0x7f020163

.field public static final rate_ni_lg:I = 0x7f020164

.field public static final rate_wts_lg:I = 0x7f020165

.field public static final rating_ni:I = 0x7f020166

.field public static final rating_ni_focused_lg:I = 0x7f020167

.field public static final rating_ni_off_lg:I = 0x7f020168

.field public static final rating_ni_on_lg:I = 0x7f020169

.field public static final rating_wts:I = 0x7f02016a

.field public static final rating_wts_focused_lg:I = 0x7f02016b

.field public static final rating_wts_off_lg:I = 0x7f02016c

.field public static final rating_wts_on_lg:I = 0x7f02016d

.field public static final rect_black:I = 0x7f02016e

.field public static final rect_black_semitransparent:I = 0x7f02016f

.field public static final rewardapp:I = 0x7f020170

.field public static final rewardappdone:I = 0x7f020171

.field public static final rewardrate:I = 0x7f020172

.field public static final rewardratedone:I = 0x7f020173

.field public static final rewardsms:I = 0x7f020174

.field public static final rewardsmsdone:I = 0x7f020175

.field public static final rewarduv:I = 0x7f020176

.field public static final rewarduvdone:I = 0x7f020177

.field public static final rewardwts:I = 0x7f020178

.field public static final rewardwtsdone:I = 0x7f020179

.field public static final round_edges:I = 0x7f02017a

.field public static final screen_promo_friendrev:I = 0x7f02017b

.field public static final screen_promo_wts:I = 0x7f02017c

.field public static final scrollbar:I = 0x7f02017d

.field public static final search_active:I = 0x7f02017e

.field public static final search_padded:I = 0x7f02017f

.field public static final search_selected:I = 0x7f020180

.field public static final search_selector:I = 0x7f020181

.field public static final search_standard:I = 0x7f020182

.field public static final searchbox:I = 0x7f020183

.field public static final searchbutton:I = 0x7f020184

.field public static final searchbutton_focused:I = 0x7f020185

.field public static final searchbutton_nobg:I = 0x7f020186

.field public static final searchbutton_pressed:I = 0x7f020187

.field public static final searchbutton_selector:I = 0x7f020188

.field public static final selectable_background_flxabv44:I = 0x7f020189

.field public static final sendfbinvites:I = 0x7f02018a

.field public static final sendinvites:I = 0x7f02018b

.field public static final seperatortop_bg:I = 0x7f02018c

.field public static final skip_arrow:I = 0x7f02018d

.field public static final spinner_ab_default_flxabv44:I = 0x7f02018e

.field public static final spinner_ab_disabled_flxabv44:I = 0x7f02018f

.field public static final spinner_ab_focused_flxabv44:I = 0x7f020190

.field public static final spinner_ab_pressed_flxabv44:I = 0x7f020191

.field public static final spinner_background_ab_flxabv44:I = 0x7f020192

.field public static final splash:I = 0x7f020193

.field public static final splash_experimental:I = 0x7f020194

.field public static final splash_gradient:I = 0x7f020195

.field public static final star_large_empty:I = 0x7f020196

.field public static final star_large_full:I = 0x7f020197

.field public static final star_large_half:I = 0x7f020198

.field public static final star_selected:I = 0x7f020199

.field public static final star_selected_sm:I = 0x7f02019a

.field public static final star_unselected:I = 0x7f02019b

.field public static final star_unselected_sm:I = 0x7f02019c

.field public static final stars_large_0_0:I = 0x7f02019d

.field public static final stars_large_0_5:I = 0x7f02019e

.field public static final stars_large_1_0:I = 0x7f02019f

.field public static final stars_large_1_5:I = 0x7f0201a0

.field public static final stars_large_2_0:I = 0x7f0201a1

.field public static final stars_large_2_5:I = 0x7f0201a2

.field public static final stars_large_3_0:I = 0x7f0201a3

.field public static final stars_large_3_5:I = 0x7f0201a4

.field public static final stars_large_4_0:I = 0x7f0201a5

.field public static final stars_large_4_5:I = 0x7f0201a6

.field public static final stars_large_5_0:I = 0x7f0201a7

.field public static final stars_small_0_0:I = 0x7f0201a8

.field public static final stars_small_0_5:I = 0x7f0201a9

.field public static final stars_small_1_0:I = 0x7f0201aa

.field public static final stars_small_1_5:I = 0x7f0201ab

.field public static final stars_small_2_0:I = 0x7f0201ac

.field public static final stars_small_2_5:I = 0x7f0201ad

.field public static final stars_small_3_0:I = 0x7f0201ae

.field public static final stars_small_3_5:I = 0x7f0201af

.field public static final stars_small_4_0:I = 0x7f0201b0

.field public static final stars_small_4_5:I = 0x7f0201b1

.field public static final stars_small_5_0:I = 0x7f0201b2

.field public static final startwatch:I = 0x7f0201b3

.field public static final subheader:I = 0x7f0201b4

.field public static final subheader_bg:I = 0x7f0201b5

.field public static final subnav_ics_button_selected:I = 0x7f0201b6

.field public static final subnav_ics_button_selector:I = 0x7f0201b7

.field public static final subnav_stripe:I = 0x7f0201b8

.field public static final tab_drawable:I = 0x7f0201b9

.field public static final tab_focused:I = 0x7f0201ba

.field public static final tab_ic_boxoffice:I = 0x7f0201bb

.field public static final tab_ic_dvd:I = 0x7f0201bc

.field public static final tab_ic_home:I = 0x7f0201bd

.field public static final tab_ic_mymovies:I = 0x7f0201be

.field public static final tab_ic_theaters:I = 0x7f0201bf

.field public static final tab_ics_bg:I = 0x7f0201c0

.field public static final tab_ics_bg_pressed:I = 0x7f0201c1

.field public static final tab_ics_bg_sans_divider_selector:I = 0x7f0201c2

.field public static final tab_ics_bg_selected:I = 0x7f0201c3

.field public static final tab_ics_bg_selector:I = 0x7f0201c4

.field public static final tab_ics_indicator_selector:I = 0x7f0201c5

.field public static final tab_indicator_ab_flxabv44:I = 0x7f0201c6

.field public static final tab_pressed:I = 0x7f0201c7

.field public static final tab_selected:I = 0x7f0201c8

.field public static final tab_selected_flxabv44:I = 0x7f0201c9

.field public static final tab_selected_focused_flxabv44:I = 0x7f0201ca

.field public static final tab_selected_pressed_flxabv44:I = 0x7f0201cb

.field public static final tab_unselected:I = 0x7f0201cc

.field public static final tab_unselected_focused_flxabv44:I = 0x7f0201cd

.field public static final tab_unselected_pressed_flxabv44:I = 0x7f0201ce

.field public static final tabicon_dvd:I = 0x7f0201cf

.field public static final tabicon_home:I = 0x7f0201d0

.field public static final tabicon_movies:I = 0x7f0201d1

.field public static final tabicon_mymovies:I = 0x7f0201d2

.field public static final tabicon_theaters:I = 0x7f0201d3

.field public static final topbar:I = 0x7f0201d4

.field public static final topbar_gradient:I = 0x7f0201d5

.field public static final upcoming_active:I = 0x7f0201d6

.field public static final upcoming_selected:I = 0x7f0201d7

.field public static final upcoming_selector:I = 0x7f0201d8

.field public static final upcoming_standard:I = 0x7f0201d9

.field public static final user:I = 0x7f0201da

.field public static final uv:I = 0x7f0201db

.field public static final viewallposter:I = 0x7f0201dc

.field public static final widget_portrait:I = 0x7f0201dd

.field public static final wtsdown:I = 0x7f0201de

.field public static final wtsup:I = 0x7f0201df


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
