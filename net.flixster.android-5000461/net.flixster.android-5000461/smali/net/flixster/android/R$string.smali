.class public final Lnet/flixster/android/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Adult:I = 0x7f0c0099

.field public static final At_Home:I = 0x7f0c00d7

.field public static final Card_Number:I = 0x7f0c009d

.field public static final Complete_Purchase:I = 0x7f0c00ae

.field public static final Confirmation_Number:I = 0x7f0c00af

.field public static final Congratulations:I = 0x7f0c00b2

.field public static final Contact_Us:I = 0x7f0c00bb

.field public static final Continue:I = 0x7f0c00a5

.field public static final Convenience_charge:I = 0x7f0c0102

.field public static final Credit_Card:I = 0x7f0c009c

.field public static final DVD:I = 0x7f0c00e1

.field public static final Done:I = 0x7f0c00c6

.field public static final Email:I = 0x7f0c00a3

.field public static final Email_:I = 0x7f0c00bd

.field public static final Email_Address:I = 0x7f0c00a2

.field public static final Expiration_:I = 0x7f0c00bc

.field public static final Expiry_Date:I = 0x7f0c009f

.field public static final Favorites:I = 0x7f0c001e

.field public static final Featured_Movies:I = 0x7f0c00be

.field public static final Fee_:I = 0x7f0c00b9

.field public static final Flixster_Movie_News:I = 0x7f0c00e0

.field public static final Go:I = 0x7f0c001f

.field public static final Instant:I = 0x7f0c00d5

.field public static final Instructions:I = 0x7f0c00b0

.field public static final Invalid_Card_Number:I = 0x7f0c009e

.field public static final Invalid_Code:I = 0x7f0c00a6

.field public static final Invalid_Date:I = 0x7f0c00a7

.field public static final Invalid_Email:I = 0x7f0c00ab

.field public static final Max_Tickets:I = 0x7f0c00c8

.field public static final Month:I = 0x7f0c00ac

.field public static final Movie_:I = 0x7f0c00b5

.field public static final No_Tickets_Selected:I = 0x7f0c00c7

.field public static final Opening_This_Week:I = 0x7f0c00bf

.field public static final Others:I = 0x7f0c00c1

.field public static final Payment_Method:I = 0x7f0c009b

.field public static final Postal_Code:I = 0x7f0c00da

.field public static final Read_full_review:I = 0x7f0c00e2

.field public static final Saved:I = 0x7f0c00d6

.field public static final Search:I = 0x7f0c0048

.field public static final Sec_Code:I = 0x7f0c00a1

.field public static final Showtime_:I = 0x7f0c00b7

.field public static final Theater_:I = 0x7f0c00b6

.field public static final Ticket_Information:I = 0x7f0c00b4

.field public static final Tickets_:I = 0x7f0c00b8

.field public static final Top_Box_Office:I = 0x7f0c00c0

.field public static final Top_This_Week:I = 0x7f0c001d

.field public static final Total:I = 0x7f0c009a

.field public static final Total_:I = 0x7f0c00ba

.field public static final Year:I = 0x7f0c00ad

.field public static final abs__action_bar_home_description:I = 0x7f0c0000

.field public static final abs__action_bar_up_description:I = 0x7f0c0001

.field public static final abs__action_menu_overflow_description:I = 0x7f0c0002

.field public static final abs__action_mode_done:I = 0x7f0c0003

.field public static final abs__activity_chooser_view_dialog_title_default:I = 0x7f0c0005

.field public static final abs__activity_chooser_view_see_all:I = 0x7f0c0004

.field public static final abs__activitychooserview_choose_application:I = 0x7f0c0007

.field public static final abs__share_action_provider_share_with:I = 0x7f0c0006

.field public static final abs__shareactionprovider_share_with:I = 0x7f0c0008

.field public static final abs__shareactionprovider_share_with_application:I = 0x7f0c0009

.field public static final accounts:I = 0x7f0c000c

.field public static final add_favorite:I = 0x7f0c0173

.field public static final add_rating:I = 0x7f0c003e

.field public static final add_to_netflix_queue:I = 0x7f0c00d1

.field public static final admin_mode:I = 0x7f0c01bb

.field public static final advertisement:I = 0x7f0c01b6

.field public static final app_name:I = 0x7f0c000a

.field public static final apprater_dontask:I = 0x7f0c01af

.field public static final apprater_prompt:I = 0x7f0c01ac

.field public static final apprater_prompt_amazon:I = 0x7f0c01ad

.field public static final apprater_rateit:I = 0x7f0c01ae

.field public static final apprater_remind:I = 0x7f0c01b0

.field public static final apprater_title:I = 0x7f0c01ab

.field public static final box_office_combined:I = 0x7f0c01b3

.field public static final buffering:I = 0x7f0c0136

.field public static final button_7eleven:I = 0x7f0c00de

.field public static final check_out_flixster:I = 0x7f0c01b2

.field public static final checking_netflix_queue:I = 0x7f0c00d4

.field public static final collection:I = 0x7f0c000f

.field public static final collection_login_promo_header:I = 0x7f0c01a3

.field public static final collection_login_promo_text:I = 0x7f0c01a5

.field public static final collection_new:I = 0x7f0c0010

.field public static final collection_promo_header:I = 0x7f0c01a2

.field public static final collection_promo_text:I = 0x7f0c01a4

.field public static final connect_flixster:I = 0x7f0c0066

.field public static final connect_header:I = 0x7f0c0065

.field public static final connect_rate:I = 0x7f0c0067

.field public static final connect_rate_post:I = 0x7f0c006a

.field public static final connect_rate_save:I = 0x7f0c0068

.field public static final connect_rate_share:I = 0x7f0c0069

.field public static final connect_your_account:I = 0x7f0c00cf

.field public static final critic_footer_title:I = 0x7f0c004e

.field public static final critic_footer_title_flixster:I = 0x7f0c004f

.field public static final current_location:I = 0x7f0c0056

.field public static final current_location_message:I = 0x7f0c0057

.field public static final details_UserReviews:I = 0x7f0c012a

.field public static final details_criticReviews:I = 0x7f0c0128

.field public static final details_movieWebsites:I = 0x7f0c0129

.field public static final details_showAllCast:I = 0x7f0c0127

.field public static final details_showAllDirectors:I = 0x7f0c0126

.field public static final diagnostic_mode:I = 0x7f0c01bc

.field public static final diagnostics_report:I = 0x7f0c012c

.field public static final diagnostics_title:I = 0x7f0c012b

.field public static final dialog_sortselect_movies:I = 0x7f0c0020

.field public static final download_cancel_msg:I = 0x7f0c019c

.field public static final download_cancel_title:I = 0x7f0c0194

.field public static final download_completed:I = 0x7f0c0190

.field public static final download_confirm_msg:I = 0x7f0c019a

.field public static final download_confirm_msg_mobile:I = 0x7f0c019b

.field public static final download_confirm_title:I = 0x7f0c0193

.field public static final download_delete_msg:I = 0x7f0c019d

.field public static final download_delete_title:I = 0x7f0c0195

.field public static final download_exceeded_limit_msg:I = 0x7f0c01a0

.field public static final download_exceeded_limit_title:I = 0x7f0c0198

.field public static final download_exceeded_space_msg:I = 0x7f0c019f

.field public static final download_exceeded_space_title:I = 0x7f0c0197

.field public static final download_in_progress:I = 0x7f0c018f

.field public static final download_license_refetch_needed_msg:I = 0x7f0c01a1

.field public static final download_size:I = 0x7f0c018e

.field public static final download_storage_msg:I = 0x7f0c019e

.field public static final download_storage_title:I = 0x7f0c0196

.field public static final download_toast:I = 0x7f0c0191

.field public static final episode_number:I = 0x7f0c01a7

.field public static final episode_offlineAlert:I = 0x7f0c007b

.field public static final episode_right_missing:I = 0x7f0c01aa

.field public static final episodes:I = 0x7f0c01a6

.field public static final fb_login_text:I = 0x7f0c016f

.field public static final fbinvite_earn:I = 0x7f0c015b

.field public static final fbinvite_earn1movie:I = 0x7f0c015e

.field public static final fbinvite_earn2movies:I = 0x7f0c015f

.field public static final fbinvite_earn3movies:I = 0x7f0c0160

.field public static final fbinvite_invite:I = 0x7f0c015a

.field public static final fbinvite_signupfriend:I = 0x7f0c015c

.field public static final fbinvite_signupfriends:I = 0x7f0c015d

.field public static final flixster_login_description:I = 0x7f0c00ff

.field public static final flixster_registration:I = 0x7f0c0100

.field public static final format_dialog_distances:I = 0x7f0c00e5

.field public static final format_theaterlist_distance_subheader:I = 0x7f0c00e3

.field public static final format_theaterlist_distance_subheader_final:I = 0x7f0c00e4

.field public static final format_theaterlist_radiustext_place:I = 0x7f0c00e6

.field public static final fresh:I = 0x7f0c005c

.field public static final friend:I = 0x7f0c008d

.field public static final friend_activity_none:I = 0x7f0c0172

.field public static final friend_activity_rated:I = 0x7f0c0171

.field public static final friend_activity_wts:I = 0x7f0c0170

.field public static final friend_rating:I = 0x7f0c008f

.field public static final friend_ratings:I = 0x7f0c0090

.field public static final friend_ratings_header:I = 0x7f0c0091

.field public static final friend_want_to_see:I = 0x7f0c0093

.field public static final friends:I = 0x7f0c008e

.field public static final friends_want_to_see:I = 0x7f0c0094

.field public static final friends_want_to_see_header:I = 0x7f0c0095

.field public static final fun_stuff:I = 0x7f0c000d

.field public static final get_showtimes:I = 0x7f0c003c

.field public static final google_maps_key:I = 0x7f0c000b

.field public static final homepage_featured:I = 0x7f0c016a

.field public static final homepage_friend:I = 0x7f0c016c

.field public static final homepage_hot:I = 0x7f0c016b

.field public static final homepage_search:I = 0x7f0c016e

.field public static final homepage_user_greeting:I = 0x7f0c016d

.field public static final initializing:I = 0x7f0c0134

.field public static final label_7eleven:I = 0x7f0c00dd

.field public static final label_Cast:I = 0x7f0c003a

.field public static final label_Director:I = 0x7f0c0033

.field public static final label_biograpy:I = 0x7f0c00c9

.field public static final label_browse:I = 0x7f0c0029

.field public static final label_cachepolicy:I = 0x7f0c005a

.field public static final label_cancel:I = 0x7f0c004a

.field public static final label_collected:I = 0x7f0c013b

.field public static final label_commingsoon:I = 0x7f0c002d

.field public static final label_date:I = 0x7f0c0026

.field public static final label_dvd_releasedate:I = 0x7f0c0028

.field public static final label_fbsso_canceled_dialog_message:I = 0x7f0c00e7

.field public static final label_fbsso_connected_dialog_message:I = 0x7f0c00e8

.field public static final label_fbsso_denied_dialog_message:I = 0x7f0c00ea

.field public static final label_fbsso_denied_dialog_title:I = 0x7f0c00e9

.field public static final label_fbsso_error_dialog_message:I = 0x7f0c00ec

.field public static final label_fbsso_error_dialog_title:I = 0x7f0c00eb

.field public static final label_filmograpy:I = 0x7f0c00ca

.field public static final label_flixster:I = 0x7f0c004d

.field public static final label_friends:I = 0x7f0c013e

.field public static final label_friendsmovies:I = 0x7f0c01be

.field public static final label_genre:I = 0x7f0c0041

.field public static final label_imdb:I = 0x7f0c004b

.field public static final label_intheaters:I = 0x7f0c0138

.field public static final label_load_friend_activity:I = 0x7f0c007f

.field public static final label_load_more_reviews:I = 0x7f0c002a

.field public static final label_load_more_want_to_see:I = 0x7f0c002c

.field public static final label_load_movies:I = 0x7f0c007e

.field public static final label_login:I = 0x7f0c0132

.field public static final label_logout:I = 0x7f0c0133

.field public static final label_moviedetails:I = 0x7f0c0045

.field public static final label_moviereviews:I = 0x7f0c005b

.field public static final label_mymovies:I = 0x7f0c013a

.field public static final label_new_releases:I = 0x7f0c002e

.field public static final label_no_showtimes_found:I = 0x7f0c0058

.field public static final label_no_theaters_found:I = 0x7f0c0059

.field public static final label_ok:I = 0x7f0c0049

.field public static final label_password:I = 0x7f0c0064

.field public static final label_promo2:I = 0x7f0c00a9

.field public static final label_promo3:I = 0x7f0c00aa

.field public static final label_random:I = 0x7f0c0032

.field public static final label_rated:I = 0x7f0c0022

.field public static final label_ratings:I = 0x7f0c013d

.field public static final label_rottentomatoes:I = 0x7f0c004c

.field public static final label_runningtime:I = 0x7f0c0023

.field public static final label_show_all_reviews:I = 0x7f0c002b

.field public static final label_showtimes:I = 0x7f0c0046

.field public static final label_synopsis:I = 0x7f0c0021

.field public static final label_theater_releasedate:I = 0x7f0c0024

.field public static final label_ticket_email_message:I = 0x7f0c00a4

.field public static final label_ticket_error:I = 0x7f0c00a0

.field public static final label_top_actor_photos:I = 0x7f0c0030

.field public static final label_top_actress_photos:I = 0x7f0c002f

.field public static final label_top_movie_photos:I = 0x7f0c0031

.field public static final label_total:I = 0x7f0c010d

.field public static final label_total_movies:I = 0x7f0c0080

.field public static final label_total_reviews:I = 0x7f0c0081

.field public static final label_undefined_location:I = 0x7f0c00db

.field public static final label_upcoming:I = 0x7f0c0139

.field public static final label_username:I = 0x7f0c0063

.field public static final label_wts:I = 0x7f0c013c

.field public static final label_yelp:I = 0x7f0c00dc

.field public static final label_zip:I = 0x7f0c0047

.field public static final leftnav_bar_option_label:I = 0x7f0c01bd

.field public static final loading:I = 0x7f0c0135

.field public static final location:I = 0x7f0c0025

.field public static final location_field_hint:I = 0x7f0c0130

.field public static final logged_in_as:I = 0x7f0c0131

.field public static final login_choice_title:I = 0x7f0c01b9

.field public static final login_error:I = 0x7f0c0161

.field public static final login_flixster:I = 0x7f0c0061

.field public static final login_flixster_text:I = 0x7f0c0062

.field public static final logout_of_netflix:I = 0x7f0c00d0

.field public static final map_theaters:I = 0x7f0c006b

.field public static final menu_actor:I = 0x7f0c011b

.field public static final menu_home:I = 0x7f0c0115

.field public static final menu_info:I = 0x7f0c011c

.field public static final menu_main:I = 0x7f0c011f

.field public static final menu_movie:I = 0x7f0c011a

.field public static final menu_next:I = 0x7f0c0121

.field public static final menu_previous:I = 0x7f0c0120

.field public static final menu_refresh:I = 0x7f0c0118

.field public static final menu_save:I = 0x7f0c011e

.field public static final menu_search:I = 0x7f0c0117

.field public static final menu_settings:I = 0x7f0c0116

.field public static final menu_share:I = 0x7f0c011d

.field public static final menu_skip:I = 0x7f0c0122

.field public static final menu_sort:I = 0x7f0c0119

.field public static final movie_lists:I = 0x7f0c000e

.field public static final movieticketscomsurcharge:I = 0x7f0c0103

.field public static final msk_gift:I = 0x7f0c00f2

.field public static final msk_ineligible_msg:I = 0x7f0c00f5

.field public static final msk_ineligible_title:I = 0x7f0c00f4

.field public static final msk_not_ready:I = 0x7f0c00f3

.field public static final msk_success_anon:I = 0x7f0c00f1

.field public static final my_collection_offlineAlert:I = 0x7f0c0072

.field public static final my_collection_title:I = 0x7f0c0070

.field public static final my_collection_title_count:I = 0x7f0c0071

.field public static final my_friends_title:I = 0x7f0c006f

.field public static final my_rated_title:I = 0x7f0c006e

.field public static final my_rating:I = 0x7f0c003f

.field public static final my_ratings_title:I = 0x7f0c006c

.field public static final my_wts_title:I = 0x7f0c006d

.field public static final mymovies_login:I = 0x7f0c00ed

.field public static final mymovies_logout_dialog_message:I = 0x7f0c00ef

.field public static final mymovies_logout_dialog_title:I = 0x7f0c00ee

.field public static final mymovies_mskpromo:I = 0x7f0c00f0

.field public static final navbar_theater_ByName:I = 0x7f0c0124

.field public static final navbar_theater_map:I = 0x7f0c0125

.field public static final navbar_theater_nearby:I = 0x7f0c0123

.field public static final netflix_dvd:I = 0x7f0c0015

.field public static final netflix_instant:I = 0x7f0c0016

.field public static final netflix_login_description:I = 0x7f0c0101

.field public static final netflix_queue:I = 0x7f0c00ce

.field public static final newsfeed_facebook:I = 0x7f0c00a8

.field public static final no:I = 0x7f0c012e

.field public static final no_friends:I = 0x7f0c008c

.field public static final no_ratings:I = 0x7f0c008a

.field public static final no_user_ratings:I = 0x7f0c008b

.field public static final num_friend_wts:I = 0x7f0c0165

.field public static final num_friends:I = 0x7f0c013f

.field public static final num_friends_wts:I = 0x7f0c0166

.field public static final num_ratings:I = 0x7f0c01b5

.field public static final num_reviews:I = 0x7f0c01b4

.field public static final percent_critic_liked:I = 0x7f0c0162

.field public static final percent_user_liked:I = 0x7f0c0163

.field public static final percent_user_wts:I = 0x7f0c0164

.field public static final photos:I = 0x7f0c003d

.field public static final popcorn_like:I = 0x7f0c005f

.field public static final popcorn_spilled:I = 0x7f0c0060

.field public static final popcorn_wts:I = 0x7f0c005e

.field public static final preferences:I = 0x7f0c0019

.field public static final privacy_policy:I = 0x7f0c018a

.field public static final profile_collected:I = 0x7f0c0085

.field public static final profile_friends:I = 0x7f0c0082

.field public static final profile_ratings:I = 0x7f0c0083

.field public static final profile_wts:I = 0x7f0c0084

.field public static final promo_friends_left:I = 0x7f0c0086

.field public static final promo_wts_1:I = 0x7f0c0087

.field public static final promo_wts_2:I = 0x7f0c0088

.field public static final promo_wts_3:I = 0x7f0c0089

.field public static final quickrate_completed_text:I = 0x7f0c0157

.field public static final quickrate_completed_title:I = 0x7f0c0156

.field public static final quickrate_progress:I = 0x7f0c0155

.field public static final quickrate_ratemovies:I = 0x7f0c0153

.field public static final quickrate_right_inserted:I = 0x7f0c0158

.field public static final quickrate_right_not_inserted:I = 0x7f0c0159

.field public static final quickrate_wtsmovies:I = 0x7f0c0154

.field public static final rate_reviewhint:I = 0x7f0c007d

.field public static final rated:I = 0x7f0c0014

.field public static final recent_friend_ratings:I = 0x7f0c0017

.field public static final remove_favorite:I = 0x7f0c0174

.field public static final rental_expiration:I = 0x7f0c0180

.field public static final rental_expiration_short:I = 0x7f0c0181

.field public static final rental_expired:I = 0x7f0c0182

.field public static final rentals:I = 0x7f0c0011

.field public static final rewards_available:I = 0x7f0c0147

.field public static final rewards_center:I = 0x7f0c0146

.field public static final rewards_completed:I = 0x7f0c0148

.field public static final rewards_earned:I = 0x7f0c0142

.field public static final rewards_earnedmovie:I = 0x7f0c014c

.field public static final rewards_earnmoremovies:I = 0x7f0c014b

.field public static final rewards_earnmovie:I = 0x7f0c0149

.field public static final rewards_earnmovies:I = 0x7f0c014a

.field public static final rewards_fbinvite:I = 0x7f0c0150

.field public static final rewards_flixster:I = 0x7f0c0140

.field public static final rewards_installmobile:I = 0x7f0c0152

.field public static final rewards_more:I = 0x7f0c0143

.field public static final rewards_msk:I = 0x7f0c0144

.field public static final rewards_msk_more:I = 0x7f0c0145

.field public static final rewards_rate:I = 0x7f0c014d

.field public static final rewards_sms:I = 0x7f0c014f

.field public static final rewards_uvcreate:I = 0x7f0c0151

.field public static final rewards_viewall:I = 0x7f0c0141

.field public static final rewards_wts:I = 0x7f0c014e

.field public static final rotten:I = 0x7f0c005d

.field public static final save_to_netflix_queue:I = 0x7f0c00d2

.field public static final saved_in_queue:I = 0x7f0c00d3

.field public static final search_hint:I = 0x7f0c0108

.field public static final searchpage_actor:I = 0x7f0c0105

.field public static final searchpage_actor_hint:I = 0x7f0c0107

.field public static final searchpage_movie:I = 0x7f0c0104

.field public static final searchpage_movie_hint:I = 0x7f0c0106

.field public static final searchpage_noresults:I = 0x7f0c010a

.field public static final searchpage_results:I = 0x7f0c0109

.field public static final season_info:I = 0x7f0c01a9

.field public static final select_date:I = 0x7f0c012f

.field public static final select_friends_description:I = 0x7f0c00f7

.field public static final select_friends_friend:I = 0x7f0c00f9

.field public static final select_friends_friends:I = 0x7f0c00f8

.field public static final select_friends_more:I = 0x7f0c00fc

.field public static final select_friends_movie:I = 0x7f0c00fb

.field public static final select_friends_movies:I = 0x7f0c00fa

.field public static final select_friends_title:I = 0x7f0c00f6

.field public static final select_friends_toast_add_more:I = 0x7f0c00fe

.field public static final select_friends_toast_invalid_contact:I = 0x7f0c00fd

.field public static final select_showtime:I = 0x7f0c0027

.field public static final select_tickets:I = 0x7f0c0098

.field public static final sending:I = 0x7f0c0137

.field public static final sent_from_flixster:I = 0x7f0c01b1

.field public static final set_location:I = 0x7f0c010e

.field public static final settings:I = 0x7f0c0018

.field public static final settings_and_more:I = 0x7f0c001a

.field public static final settings_cache_header:I = 0x7f0c0037

.field public static final settings_captions_header:I = 0x7f0c0038

.field public static final settings_captions_long:I = 0x7f0c0039

.field public static final settings_distance:I = 0x7f0c0043

.field public static final settings_facebook:I = 0x7f0c0034

.field public static final settings_flixster:I = 0x7f0c0035

.field public static final settings_footer:I = 0x7f0c0055

.field public static final settings_header_faq:I = 0x7f0c0052

.field public static final settings_header_feedback:I = 0x7f0c0053

.field public static final settings_header_ratethisapp:I = 0x7f0c0051

.field public static final settings_header_tellafriend:I = 0x7f0c0050

.field public static final settings_location_header:I = 0x7f0c0042

.field public static final settings_movieratings_header:I = 0x7f0c0044

.field public static final settings_netflix:I = 0x7f0c0036

.field public static final show_all_actor_results:I = 0x7f0c010c

.field public static final show_all_movie_results:I = 0x7f0c010b

.field public static final show_elapsed_showtimes:I = 0x7f0c0097

.field public static final sign_up:I = 0x7f0c01ba

.field public static final skip:I = 0x7f0c01b8

.field public static final stream_unsupported:I = 0x7f0c0177

.field public static final stream_unsupported_non_wifi:I = 0x7f0c0178

.field public static final stream_unsupported_rooted:I = 0x7f0c0179

.field public static final stream_warning:I = 0x7f0c0176

.field public static final tabtext_boxoffice:I = 0x7f0c010f

.field public static final tabtext_dvd:I = 0x7f0c0112

.field public static final tabtext_home:I = 0x7f0c0114

.field public static final tabtext_mymovies:I = 0x7f0c0113

.field public static final tabtext_theaters:I = 0x7f0c0110

.field public static final tabtext_upcoming:I = 0x7f0c0111

.field public static final tag_netflix_clickformore:I = 0x7f0c00d9

.field public static final tag_netflix_emptyqueue:I = 0x7f0c00d8

.field public static final tag_netflix_select_login_button:I = 0x7f0c00cd

.field public static final tag_netflix_select_text:I = 0x7f0c00cc

.field public static final tag_netflix_signupbutton:I = 0x7f0c00cb

.field public static final tag_privacy_policy:I = 0x7f0c018b

.field public static final tag_ticketconfirm_congratulations:I = 0x7f0c00b3

.field public static final tag_ticketconfirm_error:I = 0x7f0c00c5

.field public static final tag_ticketconfirm_instructions:I = 0x7f0c00b1

.field public static final tag_ticketconfirm_message:I = 0x7f0c00c2

.field public static final tag_tickets_mtcom_message:I = 0x7f0c00c3

.field public static final tag_tickets_mtcom_short:I = 0x7f0c00c4

.field public static final terms_of_service:I = 0x7f0c0189

.field public static final ticket_info:I = 0x7f0c0096

.field public static final today:I = 0x7f0c0167

.field public static final top_actors:I = 0x7f0c001b

.field public static final top_photos:I = 0x7f0c001c

.field public static final tos:I = 0x7f0c0184

.field public static final tos_accept:I = 0x7f0c0185

.field public static final tos_decline:I = 0x7f0c0186

.field public static final tos_title:I = 0x7f0c0183

.field public static final unfulfillable_description_plural:I = 0x7f0c0074

.field public static final unfulfillable_description_singular:I = 0x7f0c0073

.field public static final unfulfillable_footer:I = 0x7f0c018c

.field public static final unfulfillable_learn_more:I = 0x7f0c0077

.field public static final unfulfillable_not_available:I = 0x7f0c0076

.field public static final unfulfillable_statement_text:I = 0x7f0c007a

.field public static final unfulfillable_statement_title:I = 0x7f0c0079

.field public static final unfulfillable_tap_below:I = 0x7f0c0078

.field public static final unfulfillable_title:I = 0x7f0c0075

.field public static final unknown:I = 0x7f0c00df

.field public static final url_critic:I = 0x7f0c0054

.field public static final url_pp:I = 0x7f0c0188

.field public static final url_tos:I = 0x7f0c0187

.field public static final user_profile_title:I = 0x7f0c007c

.field public static final uv_footer:I = 0x7f0c018d

.field public static final view_episodes:I = 0x7f0c01a8

.field public static final view_photos:I = 0x7f0c0040

.field public static final viewing_expiration:I = 0x7f0c017f

.field public static final visit:I = 0x7f0c01b7

.field public static final want_to_see:I = 0x7f0c0092

.field public static final watch_movies:I = 0x7f0c0012

.field public static final watch_now:I = 0x7f0c0175

.field public static final watch_trailer:I = 0x7f0c003b

.field public static final watchnow_confirm_msg:I = 0x7f0c0199

.field public static final watchnow_confirm_title:I = 0x7f0c0192

.field public static final widget_network_error:I = 0x7f0c0169

.field public static final widget_network_unavailable:I = 0x7f0c0168

.field public static final wts:I = 0x7f0c0013

.field public static final wv_error_title:I = 0x7f0c017a

.field public static final wv_network_error:I = 0x7f0c017c

.field public static final wv_notlicensed_error:I = 0x7f0c017e

.field public static final wv_security_error:I = 0x7f0c017d

.field public static final wv_unknown_error:I = 0x7f0c017b

.field public static final yes:I = 0x7f0c012d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
