.class Lnet/flixster/android/RateMoviePage$3;
.super Landroid/os/Handler;
.source "RateMoviePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/RateMoviePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/RateMoviePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/RateMoviePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    .line 377
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "message"

    .prologue
    .line 380
    iget-object v3, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mIntent:Landroid/content/Intent;
    invoke-static {v3}, Lnet/flixster/android/RateMoviePage;->access$3(Lnet/flixster/android/RateMoviePage;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "net.flixster.ReviewComment"

    iget-object v5, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;
    invoke-static {v5}, Lnet/flixster/android/RateMoviePage;->access$4(Lnet/flixster/android/RateMoviePage;)Lnet/flixster/android/model/Review;

    move-result-object v5

    iget-object v5, v5, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 381
    iget-object v3, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mIntent:Landroid/content/Intent;
    invoke-static {v3}, Lnet/flixster/android/RateMoviePage;->access$3(Lnet/flixster/android/RateMoviePage;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "net.flixster.ReviewStars"

    iget-object v5, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;
    invoke-static {v5}, Lnet/flixster/android/RateMoviePage;->access$4(Lnet/flixster/android/RateMoviePage;)Lnet/flixster/android/model/Review;

    move-result-object v5

    iget-wide v5, v5, Lnet/flixster/android/model/Review;->stars:D

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 382
    iget-object v3, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mMovieTitle:Ljava/lang/String;
    invoke-static {v3}, Lnet/flixster/android/RateMoviePage;->access$5(Lnet/flixster/android/RateMoviePage;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 383
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/movie/addrating"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Add Rating - "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mMovieTitle:Ljava/lang/String;
    invoke-static {v6}, Lnet/flixster/android/RateMoviePage;->access$5(Lnet/flixster/android/RateMoviePage;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->postNewsFeed:Z
    invoke-static {v3}, Lnet/flixster/android/RateMoviePage;->access$6(Lnet/flixster/android/RateMoviePage;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v3, :cond_0

    .line 389
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lorg/json/JSONObject;

    .line 390
    .local v2, response:Lorg/json/JSONObject;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isWifi()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "newsfeedUrl"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 392
    :try_start_0
    const-string v3, "newsfeedUrl"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 393
    .local v1, feedUrl:Ljava/lang/String;
    const-string v3, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 394
    iget-object v3, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mIntent:Landroid/content/Intent;
    invoke-static {v3}, Lnet/flixster/android/RateMoviePage;->access$3(Lnet/flixster/android/RateMoviePage;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "net.flixster.FeedUrl"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    .end local v1           #feedUrl:Ljava/lang/String;
    .end local v2           #response:Lorg/json/JSONObject;
    :cond_0
    :goto_1
    iget-object v3, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    const/4 v4, -0x1

    iget-object v5, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mIntent:Landroid/content/Intent;
    invoke-static {v5}, Lnet/flixster/android/RateMoviePage;->access$3(Lnet/flixster/android/RateMoviePage;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lnet/flixster/android/RateMoviePage;->setResult(ILandroid/content/Intent;)V

    .line 402
    iget-object v3, p0, Lnet/flixster/android/RateMoviePage$3;->this$0:Lnet/flixster/android/RateMoviePage;

    invoke-virtual {v3}, Lnet/flixster/android/RateMoviePage;->finish()V

    .line 403
    return-void

    .line 385
    :cond_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/movie/addrating"

    const-string v5, "Add Rating - <unknown>"

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 396
    .restart local v2       #response:Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 397
    .local v0, e:Lorg/json/JSONException;
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "problem parsing response "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
