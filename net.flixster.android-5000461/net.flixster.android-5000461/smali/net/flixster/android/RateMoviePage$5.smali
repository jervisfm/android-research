.class Lnet/flixster/android/RateMoviePage$5;
.super Ljava/util/TimerTask;
.source "RateMoviePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/RateMoviePage;->ScheduleLoadReviewTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/RateMoviePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/RateMoviePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/RateMoviePage$5;->this$0:Lnet/flixster/android/RateMoviePage;

    .line 248
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 250
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterId()Ljava/lang/String;

    move-result-object v1

    .line 252
    .local v1, flixsterId:Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage$5;->this$0:Lnet/flixster/android/RateMoviePage;

    iget-object v3, p0, Lnet/flixster/android/RateMoviePage$5;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mMovieId:Ljava/lang/Long;
    invoke-static {v3}, Lnet/flixster/android/RateMoviePage;->access$7(Lnet/flixster/android/RateMoviePage;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lnet/flixster/android/data/ProfileDao;->getUserMovieReview(Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/Review;

    move-result-object v3

    #setter for: Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;
    invoke-static {v2, v3}, Lnet/flixster/android/RateMoviePage;->access$8(Lnet/flixster/android/RateMoviePage;Lnet/flixster/android/model/Review;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    :goto_0
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage$5;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mRateMoviePage:Lnet/flixster/android/RateMoviePage;
    invoke-static {v2}, Lnet/flixster/android/RateMoviePage;->access$1(Lnet/flixster/android/RateMoviePage;)Lnet/flixster/android/RateMoviePage;

    move-result-object v2

    #getter for: Lnet/flixster/android/RateMoviePage;->refreshHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/RateMoviePage;->access$9(Lnet/flixster/android/RateMoviePage;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 258
    return-void

    .line 253
    :catch_0
    move-exception v0

    .line 254
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    const-string v3, "Error from review"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
