.class Lnet/flixster/android/RateMoviePage$6;
.super Ljava/util/TimerTask;
.source "RateMoviePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/RateMoviePage;->saveRating()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/RateMoviePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/RateMoviePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    .line 350
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 353
    :try_start_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterId()Ljava/lang/String;

    move-result-object v1

    .line 354
    .local v1, flixsterId:Ljava/lang/String;
    iget-object v6, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    iget-object v4, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->postFacebookCheckbox:Landroid/widget/CheckBox;
    invoke-static {v4}, Lnet/flixster/android/RateMoviePage;->access$10(Lnet/flixster/android/RateMoviePage;)Landroid/widget/CheckBox;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->postFacebookCheckbox:Landroid/widget/CheckBox;
    invoke-static {v4}, Lnet/flixster/android/RateMoviePage;->access$10(Lnet/flixster/android/RateMoviePage;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 355
    iget-object v4, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->postFacebookCheckbox:Landroid/widget/CheckBox;
    invoke-static {v4}, Lnet/flixster/android/RateMoviePage;->access$10(Lnet/flixster/android/RateMoviePage;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    .line 354
    :goto_0
    #setter for: Lnet/flixster/android/RateMoviePage;->postNewsFeed:Z
    invoke-static {v6, v4}, Lnet/flixster/android/RateMoviePage;->access$11(Lnet/flixster/android/RateMoviePage;Z)V

    .line 356
    iget-object v4, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mMovieId:Ljava/lang/Long;
    invoke-static {v4}, Lnet/flixster/android/RateMoviePage;->access$7(Lnet/flixster/android/RateMoviePage;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;
    invoke-static {v6}, Lnet/flixster/android/RateMoviePage;->access$4(Lnet/flixster/android/RateMoviePage;)Lnet/flixster/android/model/Review;

    move-result-object v6

    .line 357
    iget-object v7, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->postNewsFeed:Z
    invoke-static {v7}, Lnet/flixster/android/RateMoviePage;->access$6(Lnet/flixster/android/RateMoviePage;)Z

    move-result v7

    .line 356
    invoke-static {v1, v4, v6, v7}, Lnet/flixster/android/data/ProfileDao;->postUserMovieReview(Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/model/Review;Z)Lorg/json/JSONObject;

    move-result-object v2

    .line 360
    .local v2, response:Lorg/json/JSONObject;
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v3

    .line 361
    .local v3, user:Lnet/flixster/android/model/User;
    const/4 v4, 0x0

    iput-object v4, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    .line 362
    const/4 v4, 0x0

    iput-object v4, v3, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    .line 366
    iget-object v4, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->handlePostSuccess:Landroid/os/Handler;
    invoke-static {v4}, Lnet/flixster/android/RateMoviePage;->access$12(Lnet/flixster/android/RateMoviePage;)Landroid/os/Handler;

    move-result-object v4

    iget-object v6, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->handlePostSuccess:Landroid/os/Handler;
    invoke-static {v6}, Lnet/flixster/android/RateMoviePage;->access$12(Lnet/flixster/android/RateMoviePage;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    .end local v1           #flixsterId:Ljava/lang/String;
    .end local v2           #response:Lorg/json/JSONObject;
    .end local v3           #user:Lnet/flixster/android/model/User;
    :goto_1
    return-void

    .restart local v1       #flixsterId:Ljava/lang/String;
    :cond_0
    move v4, v5

    .line 355
    goto :goto_0

    .line 367
    .end local v1           #flixsterId:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 368
    .local v0, e:Ljava/lang/Exception;
    iget-object v4, p0, Lnet/flixster/android/RateMoviePage$6;->this$0:Lnet/flixster/android/RateMoviePage;

    #getter for: Lnet/flixster/android/RateMoviePage;->handlePostFail:Landroid/os/Handler;
    invoke-static {v4}, Lnet/flixster/android/RateMoviePage;->access$13(Lnet/flixster/android/RateMoviePage;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
