.class public Lnet/flixster/android/RateMoviePage;
.super Lnet/flixster/android/FlixsterActivity;
.source "RateMoviePage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RatingBar$OnRatingBarChangeListener;


# static fields
.field private static final DIALOGKEY_NETWORKFAIL:I = 0x2

.field private static final DIALOGKEY_SAVING:I = 0x1

.field public static final KEY_FEED_URL:Ljava/lang/String; = "net.flixster.FeedUrl"

.field static final REQUESTCODE_REVIEW:I = 0x1

.field static final WIDGET_NI:I = 0x2

.field static final WIDGET_NONE:I = 0x0

.field static final WIDGET_RATINGBAR:I = 0x3

.field static final WIDGET_WTS:I = 0x1


# instance fields
.field private handlePostFail:Landroid/os/Handler;

.field private handlePostSuccess:Landroid/os/Handler;

.field private mComment:Landroid/widget/EditText;

.field private mExtras:Landroid/os/Bundle;

.field private mIntent:Landroid/content/Intent;

.field private mIsConnected:Ljava/lang/Boolean;

.field private mLoadReviewTask:Ljava/util/TimerTask;

.field private mMovieId:Ljava/lang/Long;

.field private mMovieTitle:Ljava/lang/String;

.field private mNiButton:Landroid/view/View;

.field private mPromoHandler:Landroid/os/Handler;

.field private mRateMoviePage:Lnet/flixster/android/RateMoviePage;

.field private mRatingBar:Landroid/widget/RatingBar;

.field private mReview:Lnet/flixster/android/model/Review;

.field private mSavingDialog:Landroid/app/ProgressDialog;

.field private mSelectedWidget:I

.field private mTimer:Ljava/util/Timer;

.field private mWtsButton:Landroid/view/View;

.field private postFacebookCheckbox:Landroid/widget/CheckBox;

.field private postNewsFeed:Z

.field private refreshHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 51
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mMovieId:Ljava/lang/Long;

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mIsConnected:Ljava/lang/Boolean;

    .line 157
    new-instance v0, Lnet/flixster/android/RateMoviePage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/RateMoviePage$1;-><init>(Lnet/flixster/android/RateMoviePage;)V

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mPromoHandler:Landroid/os/Handler;

    .line 266
    new-instance v0, Lnet/flixster/android/RateMoviePage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/RateMoviePage$2;-><init>(Lnet/flixster/android/RateMoviePage;)V

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->refreshHandler:Landroid/os/Handler;

    .line 377
    new-instance v0, Lnet/flixster/android/RateMoviePage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/RateMoviePage$3;-><init>(Lnet/flixster/android/RateMoviePage;)V

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->handlePostSuccess:Landroid/os/Handler;

    .line 406
    new-instance v0, Lnet/flixster/android/RateMoviePage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/RateMoviePage$4;-><init>(Lnet/flixster/android/RateMoviePage;)V

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->handlePostFail:Landroid/os/Handler;

    .line 39
    return-void
.end method

.method private ScheduleLoadReviewTask()V
    .locals 4

    .prologue
    .line 248
    new-instance v0, Lnet/flixster/android/RateMoviePage$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/RateMoviePage$5;-><init>(Lnet/flixster/android/RateMoviePage;)V

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mLoadReviewTask:Ljava/util/TimerTask;

    .line 261
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mLoadReviewTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 264
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/RateMoviePage;)Ljava/lang/Boolean;
    .locals 1
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mIsConnected:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/RateMoviePage;)Lnet/flixster/android/RateMoviePage;
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mRateMoviePage:Lnet/flixster/android/RateMoviePage;

    return-object v0
.end method

.method static synthetic access$10(Lnet/flixster/android/RateMoviePage;)Landroid/widget/CheckBox;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->postFacebookCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$11(Lnet/flixster/android/RateMoviePage;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 72
    iput-boolean p1, p0, Lnet/flixster/android/RateMoviePage;->postNewsFeed:Z

    return-void
.end method

.method static synthetic access$12(Lnet/flixster/android/RateMoviePage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 377
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->handlePostSuccess:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$13(Lnet/flixster/android/RateMoviePage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 406
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->handlePostFail:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$14(Lnet/flixster/android/RateMoviePage;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mSavingDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/RateMoviePage;)V
    .locals 0
    .parameter

    .prologue
    .line 228
    invoke-direct {p0}, Lnet/flixster/android/RateMoviePage;->populatePage()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/RateMoviePage;)Landroid/content/Intent;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/RateMoviePage;)Lnet/flixster/android/model/Review;
    .locals 1
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/RateMoviePage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mMovieTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/RateMoviePage;)Z
    .locals 1
    .parameter

    .prologue
    .line 72
    iget-boolean v0, p0, Lnet/flixster/android/RateMoviePage;->postNewsFeed:Z

    return v0
.end method

.method static synthetic access$7(Lnet/flixster/android/RateMoviePage;)Ljava/lang/Long;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mMovieId:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/RateMoviePage;Lnet/flixster/android/model/Review;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/RateMoviePage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 266
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->refreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getExtras()V
    .locals 6

    .prologue
    .line 99
    const-string v2, "FlxMain"

    const-string v3, "RateMoviePage.getExtras()"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lnet/flixster/android/RateMoviePage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/RateMoviePage;->mExtras:Landroid/os/Bundle;

    .line 101
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mExtras:Landroid/os/Bundle;

    if-eqz v2, :cond_3

    .line 103
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mExtras:Landroid/os/Bundle;

    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/RateMoviePage;->mMovieId:Ljava/lang/Long;

    .line 105
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mExtras:Landroid/os/Bundle;

    const-string v3, "title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mExtras:Landroid/os/Bundle;

    const-string v3, "title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/RateMoviePage;->mMovieTitle:Ljava/lang/String;

    .line 109
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mExtras:Landroid/os/Bundle;

    const-string v3, "net.flixster.ReviewStars"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 110
    .local v1, stars:Ljava/lang/Double;
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mExtras:Landroid/os/Bundle;

    const-string v3, "net.flixster.ReviewComment"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, comment:Ljava/lang/String;
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mExtras:Landroid/os/Bundle;

    const-string v3, "net.flixster.IsConnected"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/RateMoviePage;->mIsConnected:Ljava/lang/Boolean;

    .line 113
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mIsConnected:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mIsConnected:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 115
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    if-nez v2, :cond_2

    .line 116
    if-nez v1, :cond_1

    if-eqz v0, :cond_2

    .line 117
    :cond_1
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RateMoviePage.getExtras() new Review() comment:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stars:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    new-instance v2, Lnet/flixster/android/model/Review;

    invoke-direct {v2}, Lnet/flixster/android/model/Review;-><init>()V

    iput-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    .line 119
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    const/4 v3, 0x1

    iput v3, v2, Lnet/flixster/android/model/Review;->type:I

    .line 120
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iput-object v0, v2, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    .line 121
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    iput-wide v3, v2, Lnet/flixster/android/model/Review;->stars:D

    .line 126
    :cond_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/movie/info/myRating"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Add Rating - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/RateMoviePage;->mMovieTitle:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .end local v0           #comment:Ljava/lang/String;
    .end local v1           #stars:Ljava/lang/Double;
    :goto_0
    return-void

    .line 134
    :cond_3
    const-string v2, "FlxMain"

    const-string v3, "Missing the bundle extras... "

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private populatePage()V
    .locals 4

    .prologue
    .line 229
    const-string v0, "FlxMain"

    const-string v1, "RateMoviePage.populatePage()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iget-wide v0, v0, Lnet/flixster/android/model/Review;->stars:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 233
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    .line 242
    :goto_0
    invoke-direct {p0}, Lnet/flixster/android/RateMoviePage;->selectionSet()V

    .line 243
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mComment:Landroid/widget/EditText;

    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iget-object v1, v1, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 245
    :cond_0
    return-void

    .line 234
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    invoke-virtual {v0}, Lnet/flixster/android/model/Review;->isWantToSee()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 235
    const/4 v0, 0x1

    iput v0, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    goto :goto_0

    .line 236
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    invoke-virtual {v0}, Lnet/flixster/android/model/Review;->isNotInterested()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 237
    const/4 v0, 0x2

    iput v0, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    goto :goto_0

    .line 239
    :cond_3
    const/4 v0, 0x3

    iput v0, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    goto :goto_0
.end method

.method private saveRating()V
    .locals 4

    .prologue
    .line 336
    iget v1, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    packed-switch v1, :pswitch_data_0

    .line 348
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mComment:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    .line 349
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lnet/flixster/android/RateMoviePage;->showDialog(I)V

    .line 350
    new-instance v0, Lnet/flixster/android/RateMoviePage$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/RateMoviePage$6;-><init>(Lnet/flixster/android/RateMoviePage;)V

    .line 372
    .local v0, postReview:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 373
    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0xa

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 375
    :cond_0
    return-void

    .line 338
    .end local v0           #postReview:Ljava/util/TimerTask;
    :pswitch_0
    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v2}, Landroid/widget/RatingBar;->getRating()F

    move-result v2

    float-to-double v2, v2

    iput-wide v2, v1, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_0

    .line 341
    :pswitch_1
    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    const-wide/high16 v2, 0x4016

    iput-wide v2, v1, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_0

    .line 344
    :pswitch_2
    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    const-wide/high16 v2, 0x4018

    iput-wide v2, v1, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_0

    .line 336
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private selectionSet()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 168
    iget-object v3, p0, Lnet/flixster/android/RateMoviePage;->mWtsButton:Landroid/view/View;

    iget v0, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setSelected(Z)V

    .line 169
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mNiButton:Landroid/view/View;

    iget v3, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 170
    iget v0, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    if-eqz v0, :cond_2

    .line 171
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mRatingBar:Landroid/widget/RatingBar;

    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iget-wide v1, v1, Lnet/flixster/android/model/Review;->stars:D

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    .line 175
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 168
    goto :goto_0

    :cond_1
    move v1, v2

    .line 169
    goto :goto_1

    .line 173
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mRatingBar:Landroid/widget/RatingBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    goto :goto_2
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v3, 0x1

    .line 460
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    if-ne p1, v3, :cond_0

    .line 461
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/RateMoviePage;->mIsConnected:Ljava/lang/Boolean;

    .line 462
    invoke-virtual {p0}, Lnet/flixster/android/RateMoviePage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 463
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.IsConnected"

    new-instance v2, Ljava/lang/Boolean;

    invoke-direct {v2, v3}, Ljava/lang/Boolean;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 468
    .end local v0           #i:Landroid/content/Intent;
    :goto_0
    return-void

    .line 465
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lnet/flixster/android/RateMoviePage;->setResult(I)V

    .line 466
    invoke-virtual {p0}, Lnet/flixster/android/RateMoviePage;->finish()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "v"

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-wide/16 v3, 0x0

    .line 300
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 331
    :goto_0
    invoke-direct {p0}, Lnet/flixster/android/RateMoviePage;->selectionSet()V

    .line 332
    return-void

    .line 303
    :sswitch_0
    iget v2, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    if-ne v2, v6, :cond_0

    .line 304
    iput v5, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    .line 305
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iput-wide v3, v2, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_0

    .line 307
    :cond_0
    iput v6, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    .line 308
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    const-wide/high16 v3, 0x4016

    iput-wide v3, v2, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_0

    .line 313
    :sswitch_1
    iget v2, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    if-ne v2, v7, :cond_1

    .line 314
    iput v5, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    .line 315
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iput-wide v3, v2, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_0

    .line 317
    :cond_1
    iput v7, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    .line 318
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    const-wide/high16 v3, 0x4018

    iput-wide v3, v2, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_0

    .line 323
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/FacebookAuth;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 324
    .local v0, i:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lnet/flixster/android/RateMoviePage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 327
    .end local v0           #i:Landroid/content/Intent;
    :sswitch_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lnet/flixster/android/FlixsterLoginPage;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 328
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lnet/flixster/android/RateMoviePage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 300
    :sswitch_data_0
    .sparse-switch
        0x7f070058 -> :sswitch_2
        0x7f070059 -> :sswitch_3
        0x7f070212 -> :sswitch_0
        0x7f070213 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 76
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-virtual {p0}, Lnet/flixster/android/RateMoviePage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mIntent:Landroid/content/Intent;

    .line 79
    const v0, 0x7f030071

    invoke-virtual {p0, v0}, Lnet/flixster/android/RateMoviePage;->setContentView(I)V

    .line 80
    invoke-virtual {p0}, Lnet/flixster/android/RateMoviePage;->createActionBar()V

    .line 81
    const v0, 0x7f0c003f

    invoke-virtual {p0, v0}, Lnet/flixster/android/RateMoviePage;->setActionBarTitle(I)V

    .line 83
    iput-object p0, p0, Lnet/flixster/android/RateMoviePage;->mRateMoviePage:Lnet/flixster/android/RateMoviePage;

    .line 85
    const v0, 0x7f070212

    invoke-virtual {p0, v0}, Lnet/flixster/android/RateMoviePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mWtsButton:Landroid/view/View;

    .line 86
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mWtsButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const v0, 0x7f070213

    invoke-virtual {p0, v0}, Lnet/flixster/android/RateMoviePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mNiButton:Landroid/view/View;

    .line 88
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mNiButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    const v0, 0x7f070214

    invoke-virtual {p0, v0}, Lnet/flixster/android/RateMoviePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mRatingBar:Landroid/widget/RatingBar;

    .line 90
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v0, p0}, Landroid/widget/RatingBar;->setOnRatingBarChangeListener(Landroid/widget/RatingBar$OnRatingBarChangeListener;)V

    .line 92
    const v0, 0x7f070218

    invoke-virtual {p0, v0}, Lnet/flixster/android/RateMoviePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mComment:Landroid/widget/EditText;

    .line 93
    const v0, 0x7f070216

    invoke-virtual {p0, v0}, Lnet/flixster/android/RateMoviePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->postFacebookCheckbox:Landroid/widget/CheckBox;

    .line 94
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->postFacebookCheckbox:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->postFacebookCheckbox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 96
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v3, 0x1

    .line 434
    packed-switch p1, :pswitch_data_0

    .line 455
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 436
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mSavingDialog:Landroid/app/ProgressDialog;

    .line 437
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mSavingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lnet/flixster/android/RateMoviePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 438
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mSavingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 439
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mSavingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 440
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mSavingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 441
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mSavingDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 443
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "The network connection failed."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 444
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 445
    const-string v1, "OK"

    new-instance v2, Lnet/flixster/android/RateMoviePage$7;

    invoke-direct {v2, p0}, Lnet/flixster/android/RateMoviePage$7;-><init>(Lnet/flixster/android/RateMoviePage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 452
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 434
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 472
    invoke-virtual {p0}, Lnet/flixster/android/RateMoviePage;->getSupportMenuInflater()Lcom/actionbarsherlock/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f000d

    invoke-virtual {v0, v1, p1}, Lcom/actionbarsherlock/view/MenuInflater;->inflate(ILcom/actionbarsherlock/view/Menu;)V

    .line 473
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 286
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onDestroy()V

    .line 290
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 292
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 295
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    .line 296
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keycode"
    .parameter "event"

    .prologue
    .line 276
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 277
    const-string v0, "FlxMain"

    const-string v1, "RateMoviePage.onKeyDown - call finish"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x0

    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lnet/flixster/android/RateMoviePage;->setResult(ILandroid/content/Intent;)V

    .line 279
    invoke-virtual {p0}, Lnet/flixster/android/RateMoviePage;->finish()V

    .line 281
    :cond_0
    invoke-super {p0, p1, p2}, Lnet/flixster/android/FlixsterActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 478
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 483
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 480
    :pswitch_0
    invoke-direct {p0}, Lnet/flixster/android/RateMoviePage;->saveRating()V

    .line 481
    const/4 v0, 0x1

    goto :goto_0

    .line 478
    nop

    :pswitch_data_0
    .packed-switch 0x7f07030a
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 179
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onPause()V

    .line 181
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 183
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 185
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    .line 186
    return-void
.end method

.method public onRatingChanged(Landroid/widget/RatingBar;FZ)V
    .locals 3
    .parameter "ratingBar"
    .parameter "rating"
    .parameter "fromUser"

    .prologue
    .line 418
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RateMoviePage.onRatingChanged rating:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_0

    .line 421
    const/4 v0, 0x3

    iput v0, p0, Lnet/flixster/android/RateMoviePage;->mSelectedWidget:I

    .line 423
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mRatingBar:Landroid/widget/RatingBar;

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    .line 424
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    if-nez v0, :cond_1

    .line 425
    new-instance v0, Lnet/flixster/android/model/Review;

    invoke-direct {v0}, Lnet/flixster/android/model/Review;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    .line 427
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v1}, Landroid/widget/RatingBar;->getRating()F

    move-result v1

    float-to-double v1, v1

    iput-wide v1, v0, Lnet/flixster/android/model/Review;->stars:D

    .line 428
    invoke-direct {p0}, Lnet/flixster/android/RateMoviePage;->selectionSet()V

    .line 431
    :cond_2
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 209
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 210
    const-string v0, "FlxMain"

    const-string v1, "RateMoviePage.onRestoreInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    if-nez v0, :cond_0

    .line 216
    new-instance v0, Lnet/flixster/android/model/Review;

    invoke-direct {v0}, Lnet/flixster/android/model/Review;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    .line 219
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    const-string v1, "net.flixster.RateMoviePage.mReview.comment"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    .line 220
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mComment:Landroid/widget/EditText;

    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iget-object v1, v1, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    const-string v1, "net.flixster.RateMoviePage.mReview.stars"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    iput-wide v1, v0, Lnet/flixster/android/model/Review;->stars:D

    .line 223
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RateMoviePage.onRestoreInstanceState() mComment:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mComment:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 224
    const-string v2, " mRatingBar:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v2}, Landroid/widget/RatingBar;->getRating()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mReview.comment:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iget-object v2, v2, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mReview.stars:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 225
    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iget-wide v2, v2, Lnet/flixster/android/model/Review;->stars:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 223
    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 140
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 144
    invoke-direct {p0}, Lnet/flixster/android/RateMoviePage;->getExtras()V

    .line 147
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/RateMoviePage;->mTimer:Ljava/util/Timer;

    .line 150
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    if-nez v0, :cond_1

    .line 151
    invoke-direct {p0}, Lnet/flixster/android/RateMoviePage;->ScheduleLoadReviewTask()V

    .line 153
    :cond_1
    invoke-direct {p0}, Lnet/flixster/android/RateMoviePage;->populatePage()V

    .line 154
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mPromoHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 155
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .parameter "outState"

    .prologue
    .line 190
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 191
    const-string v0, "FlxMain"

    const-string v1, "RateMoviePage.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v0, "net.flixster.RateMoviePage.mComment.text"

    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mComment:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v0, "net.flixster.RateMoviePage.mRatingBar.rating"

    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v1}, Landroid/widget/RatingBar;->getRating()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 194
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RateMoviePage.onSaveInstanceState() mRatingBar.getRating():"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/flixster/android/RateMoviePage;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v2}, Landroid/widget/RatingBar;->getRating()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    if-eqz v0, :cond_0

    .line 196
    const-string v0, "net.flixster.RateMoviePage.mReview.stars"

    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mReview:Lnet/flixster/android/model/Review;

    iget-wide v1, v1, Lnet/flixster/android/model/Review;->stars:D

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 199
    const-string v0, "net.flixster.RateMoviePage.mReview.comment"

    iget-object v1, p0, Lnet/flixster/android/RateMoviePage;->mComment:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :cond_0
    const-string v0, "FlxMain"

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RateMoviePage.onSaveInstanceState() outState.getFloat:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 204
    const-string v2, "net.flixster.RateMoviePage.mRatingBar.rating"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 203
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 201
    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    return-void
.end method
