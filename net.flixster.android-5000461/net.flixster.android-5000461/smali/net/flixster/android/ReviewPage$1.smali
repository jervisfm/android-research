.class Lnet/flixster/android/ReviewPage$1;
.super Landroid/os/Handler;
.source "ReviewPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ReviewPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ReviewPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ReviewPage$1;->this$0:Lnet/flixster/android/ReviewPage;

    .line 272
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 276
    iget-object v0, p0, Lnet/flixster/android/ReviewPage$1;->this$0:Lnet/flixster/android/ReviewPage;

    invoke-virtual {v0}, Lnet/flixster/android/ReviewPage;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 280
    :cond_1
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ReviewPage.postMovieLoadHandler - mReviewsList:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/flixster/android/ReviewPage$1;->this$0:Lnet/flixster/android/ReviewPage;

    #getter for: Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/ReviewPage;->access$0(Lnet/flixster/android/ReviewPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lnet/flixster/android/ReviewPage$1;->this$0:Lnet/flixster/android/ReviewPage;

    #calls: Lnet/flixster/android/ReviewPage;->hideLoading()V
    invoke-static {v0}, Lnet/flixster/android/ReviewPage;->access$1(Lnet/flixster/android/ReviewPage;)V

    .line 282
    iget-object v0, p0, Lnet/flixster/android/ReviewPage$1;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v0, v0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lnet/flixster/android/ReviewPage$1;->this$0:Lnet/flixster/android/ReviewPage;

    #getter for: Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/ReviewPage;->access$0(Lnet/flixster/android/ReviewPage;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 283
    iget-object v0, p0, Lnet/flixster/android/ReviewPage$1;->this$0:Lnet/flixster/android/ReviewPage;

    #calls: Lnet/flixster/android/ReviewPage;->populatePage()V
    invoke-static {v0}, Lnet/flixster/android/ReviewPage;->access$2(Lnet/flixster/android/ReviewPage;)V

    goto :goto_0

    .line 285
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/ReviewPage$1;->this$0:Lnet/flixster/android/ReviewPage;

    invoke-virtual {v0}, Lnet/flixster/android/ReviewPage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    iget-object v0, p0, Lnet/flixster/android/ReviewPage$1;->this$0:Lnet/flixster/android/ReviewPage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/ReviewPage;->showDialog(I)V

    goto :goto_0
.end method
