.class Lnet/flixster/android/ReviewPage$2;
.super Landroid/os/Handler;
.source "ReviewPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ReviewPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ReviewPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ReviewPage$2;->this$0:Lnet/flixster/android/ReviewPage;

    .line 292
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 295
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/widget/ImageView;

    .line 296
    .local v1, view:Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 297
    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .line 298
    .local v0, movie:Lnet/flixster/android/model/Movie;
    if-eqz v0, :cond_0

    .line 299
    iget-object v2, v0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 300
    iget-object v2, v0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 301
    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 305
    .end local v0           #movie:Lnet/flixster/android/model/Movie;
    :cond_0
    return-void
.end method
