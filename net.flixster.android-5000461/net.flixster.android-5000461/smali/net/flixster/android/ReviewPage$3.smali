.class Lnet/flixster/android/ReviewPage$3;
.super Ljava/util/TimerTask;
.source "ReviewPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/ReviewPage;->ScheduleLoadMoviesTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ReviewPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    .line 106
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 109
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    #getter for: Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/ReviewPage;->access$0(Lnet/flixster/android/ReviewPage;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    #getter for: Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/ReviewPage;->access$0(Lnet/flixster/android/ReviewPage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 112
    :cond_0
    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-wide v2, v2, Lnet/flixster/android/ReviewPage;->mMovieId:J

    invoke-static {v2, v3}, Lnet/flixster/android/data/MovieDao;->getMovieDetail(J)Lnet/flixster/android/model/Movie;

    move-result-object v1

    .line 114
    .local v1, tempMovie:Lnet/flixster/android/model/Movie;
    if-eqz v1, :cond_1

    .line 115
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v2, v2, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Movie;->merge(Lnet/flixster/android/model/Movie;)V

    .line 116
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iput-object v1, v2, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    .line 123
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lnet/flixster/android/ReviewPage;->access$3(Lnet/flixster/android/ReviewPage;Ljava/util/ArrayList;)V

    .line 124
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v2, v2, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 125
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    #getter for: Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/ReviewPage;->access$0(Lnet/flixster/android/ReviewPage;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v3, v3, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getFriendWantToSeeList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 127
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v2, v2, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 128
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    #getter for: Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/ReviewPage;->access$0(Lnet/flixster/android/ReviewPage;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v3, v3, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getFriendRatedReviewsList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 130
    :cond_3
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v2, v2, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getUserReviewsList()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 131
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    #getter for: Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/ReviewPage;->access$0(Lnet/flixster/android/ReviewPage;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v3, v3, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getUserReviewsList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 133
    :cond_4
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v2, v2, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->getCriticReviewsList()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 134
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    #getter for: Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lnet/flixster/android/ReviewPage;->access$0(Lnet/flixster/android/ReviewPage;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v3, v3, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getCriticReviewsList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 136
    :cond_5
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    #getter for: Lnet/flixster/android/ReviewPage;->postMovieLoadHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/ReviewPage;->access$4(Lnet/flixster/android/ReviewPage;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    .end local v1           #tempMovie:Lnet/flixster/android/model/Movie;
    :cond_6
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, de:Lnet/flixster/android/data/DaoException;
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget v3, v2, Lnet/flixster/android/ReviewPage;->mRetryCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lnet/flixster/android/ReviewPage;->mRetryCount:I

    .line 139
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget v2, v2, Lnet/flixster/android/ReviewPage;->mRetryCount:I

    const/4 v3, 0x4

    if-le v2, v3, :cond_7

    .line 140
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iput v4, v2, Lnet/flixster/android/ReviewPage;->mRetryCount:I

    .line 141
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    iget-object v2, v2, Lnet/flixster/android/ReviewPage;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 144
    :cond_7
    iget-object v2, p0, Lnet/flixster/android/ReviewPage$3;->this$0:Lnet/flixster/android/ReviewPage;

    #calls: Lnet/flixster/android/ReviewPage;->ScheduleLoadMoviesTask()V
    invoke-static {v2}, Lnet/flixster/android/ReviewPage;->access$5(Lnet/flixster/android/ReviewPage;)V

    goto :goto_0
.end method
