.class public Lnet/flixster/android/ReviewPage;
.super Lnet/flixster/android/FlixsterActivity;
.source "ReviewPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1


# instance fields
.field mMovie:Lnet/flixster/android/model/Movie;

.field mMovieId:J

.field public mMovieType:I

.field private mReviewPage:Lnet/flixster/android/ReviewPage;

.field private mReviewsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field private mStartReviewIndex:I

.field private mTimer:Ljava/util/Timer;

.field private mViewFlipper:Landroid/widget/ViewFlipper;

.field private final movieImageHandler:Landroid/os/Handler;

.field private final postMovieLoadHandler:Landroid/os/Handler;

.field private throbber:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 272
    new-instance v0, Lnet/flixster/android/ReviewPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/ReviewPage$1;-><init>(Lnet/flixster/android/ReviewPage;)V

    iput-object v0, p0, Lnet/flixster/android/ReviewPage;->postMovieLoadHandler:Landroid/os/Handler;

    .line 292
    new-instance v0, Lnet/flixster/android/ReviewPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/ReviewPage$2;-><init>(Lnet/flixster/android/ReviewPage;)V

    iput-object v0, p0, Lnet/flixster/android/ReviewPage;->movieImageHandler:Landroid/os/Handler;

    .line 39
    return-void
.end method

.method private ScheduleLoadMoviesTask()V
    .locals 4

    .prologue
    .line 106
    new-instance v0, Lnet/flixster/android/ReviewPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/ReviewPage$3;-><init>(Lnet/flixster/android/ReviewPage;)V

    .line 151
    .local v0, mLoadDetailsTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/ReviewPage;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lnet/flixster/android/ReviewPage;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 154
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/ReviewPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/ReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 268
    invoke-direct {p0}, Lnet/flixster/android/ReviewPage;->hideLoading()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/ReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 156
    invoke-direct {p0}, Lnet/flixster/android/ReviewPage;->populatePage()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/ReviewPage;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/ReviewPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 272
    iget-object v0, p0, Lnet/flixster/android/ReviewPage;->postMovieLoadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/ReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 105
    invoke-direct {p0}, Lnet/flixster/android/ReviewPage;->ScheduleLoadMoviesTask()V

    return-void
.end method

.method private getPageTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f0c005b

    invoke-virtual {p0, v1}, Lnet/flixster/android/ReviewPage;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 361
    iget-object v1, p0, Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 360
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private hideLoading()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lnet/flixster/android/ReviewPage;->throbber:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 270
    return-void
.end method

.method private populatePage()V
    .locals 29

    .prologue
    .line 157
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/4 v3, 0x0

    const-string v4, "title"

    aput-object v4, v16, v3

    const/4 v3, 0x1

    const-string v4, "MOVIE_ACTORS_SHORT"

    aput-object v4, v16, v3

    const/4 v3, 0x2

    const-string v4, "meta"

    aput-object v4, v16, v3

    .line 158
    .local v16, propertyList:[Ljava/lang/String;
    const/4 v3, 0x3

    new-array v0, v3, [I

    move-object/from16 v23, v0

    fill-array-data v23, :array_0

    .line 160
    .local v23, textViewIdArray:[I
    move-object/from16 v0, v16

    array-length v12, v0

    .line 161
    .local v12, length:I
    const/4 v10, 0x0

    .local v10, i:I
    :goto_0
    if-lt v10, v12, :cond_1

    .line 171
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mReviewPage:Lnet/flixster/android/ReviewPage;

    const v4, 0x7f07021d

    invoke-virtual {v3, v4}, Lnet/flixster/android/ReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 172
    .local v6, iv:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 173
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v3, v3, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 174
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v3, v3, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 192
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 194
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->removeAllViews()V

    .line 195
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mReviewPage:Lnet/flixster/android/ReviewPage;

    invoke-virtual {v3}, Lnet/flixster/android/ReviewPage;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v13

    .line 196
    .local v13, li:Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_5

    .line 258
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ReviewPage.populatePage() mStartReviewIndex:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v5, v0, Lnet/flixster/android/ReviewPage;->mStartReviewIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    move-object/from16 v0, p0

    iget v4, v0, Lnet/flixster/android/ReviewPage;->mStartReviewIndex:I

    invoke-virtual {v3, v4}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 260
    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/ReviewPage;->getPageTitle()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lnet/flixster/android/ReviewPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 262
    .end local v13           #li:Landroid/view/LayoutInflater;
    :cond_0
    return-void

    .line 162
    .end local v6           #iv:Landroid/widget/ImageView;
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mReviewPage:Lnet/flixster/android/ReviewPage;

    aget v4, v23, v10

    invoke-virtual {v3, v4}, Lnet/flixster/android/ReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 163
    .local v22, textView:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    aget-object v4, v16, v10

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 164
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    aget-object v4, v16, v10

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 167
    :cond_2
    const/16 v3, 0x8

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 176
    .end local v22           #textView:Landroid/widget/TextView;
    .restart local v6       #iv:Landroid/widget/ImageView;
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "thumbnail"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 177
    .local v15, prop:Ljava/lang/String;
    if-eqz v15, :cond_4

    const-string v3, "http"

    invoke-virtual {v15, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 180
    new-instance v2, Lnet/flixster/android/model/ImageOrder;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    .line 181
    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v7, "thumbnail"

    invoke-virtual {v5, v7}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lnet/flixster/android/ReviewPage;->movieImageHandler:Landroid/os/Handler;

    .line 180
    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 182
    .local v2, io:Lnet/flixster/android/model/ImageOrder;
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lnet/flixster/android/ReviewPage;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    goto/16 :goto_1

    .line 184
    .end local v2           #io:Lnet/flixster/android/model/ImageOrder;
    :cond_4
    const v3, 0x7f02014f

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 196
    .end local v15           #prop:Ljava/lang/String;
    .restart local v13       #li:Landroid/view/LayoutInflater;
    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lnet/flixster/android/model/Review;

    .line 198
    .local v17, r:Lnet/flixster/android/model/Review;
    const v4, 0x7f030073

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    const/4 v7, 0x0

    invoke-virtual {v13, v4, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ScrollView;

    .line 199
    .local v11, item:Landroid/widget/ScrollView;
    const v4, 0x7f070226

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    .line 200
    .local v24, tv:Landroid/widget/TextView;
    move-object/from16 v0, v17

    iget-object v4, v0, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    const v4, 0x7f070225

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    .line 203
    .local v14, mugImageView:Landroid/widget/ImageView;
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lnet/flixster/android/model/Review;->getReviewerBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 204
    .local v8, bitmap:Landroid/graphics/Bitmap;
    if-eqz v8, :cond_6

    .line 205
    invoke-virtual {v14, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 208
    :cond_6
    const v4, 0x7f070228

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 209
    .local v9, critic_icon:Landroid/widget/ImageView;
    const v4, 0x7f070229

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageView;

    .line 210
    .local v20, ri:Landroid/widget/ImageView;
    const v4, 0x7f070224

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/RelativeLayout;

    .line 211
    .local v18, reviewHeader:Landroid/widget/RelativeLayout;
    const v4, 0x7f07022a

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/RelativeLayout;

    .line 213
    .local v19, reviewItemBody:Landroid/widget/RelativeLayout;
    move-object/from16 v0, v17

    iget v4, v0, Lnet/flixster/android/model/Review;->type:I

    packed-switch v4, :pswitch_data_0

    .line 250
    :cond_7
    :goto_4
    move-object/from16 v0, v17

    iget-wide v4, v0, Lnet/flixster/android/model/Review;->userId:J

    const-wide/16 v25, 0x0

    cmp-long v4, v4, v25

    if-lez v4, :cond_8

    .line 251
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 252
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v4, v11}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 215
    :pswitch_0
    const v4, 0x7f07022b

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .end local v24           #tv:Landroid/widget/TextView;
    check-cast v24, Landroid/widget/TextView;

    .line 216
    .restart local v24       #tv:Landroid/widget/TextView;
    move-object/from16 v0, v17

    iget-object v4, v0, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    const/16 v4, 0x8

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 218
    const v4, 0x7f070227

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .end local v24           #tv:Landroid/widget/TextView;
    check-cast v24, Landroid/widget/TextView;

    .line 219
    .restart local v24       #tv:Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    iget-object v5, v0, Lnet/flixster/android/model/Review;->source:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    move-object/from16 v0, v17

    iget-object v4, v0, Lnet/flixster/android/model/Review;->url:Ljava/lang/String;

    if-eqz v4, :cond_9

    .line 221
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 222
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    :cond_9
    move-object/from16 v0, v17

    iget v4, v0, Lnet/flixster/android/model/Review;->score:I

    const/16 v5, 0x3c

    if-ge v4, v5, :cond_7

    .line 225
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ReviewPage;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0200ec

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v21

    .line 226
    .local v21, scoreIcon:Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 230
    .end local v21           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :pswitch_1
    const v4, 0x7f07022b

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .end local v24           #tv:Landroid/widget/TextView;
    check-cast v24, Landroid/widget/TextView;

    .line 231
    .restart local v24       #tv:Landroid/widget/TextView;
    move-object/from16 v0, v17

    iget-object v4, v0, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    const/16 v4, 0x8

    invoke-virtual {v9, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 233
    const v4, 0x7f070227

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .end local v24           #tv:Landroid/widget/TextView;
    check-cast v24, Landroid/widget/TextView;

    .line 234
    .restart local v24       #tv:Landroid/widget/TextView;
    const/16 v4, 0x8

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 235
    sget-object v4, Lnet/flixster/android/Flixster;->RATING_LARGE_R:[I

    move-object/from16 v0, v17

    iget-wide v0, v0, Lnet/flixster/android/model/Review;->stars:D

    move-wide/from16 v25, v0

    const-wide/high16 v27, 0x4000

    mul-double v25, v25, v27

    move-wide/from16 v0, v25

    double-to-int v5, v0

    aget v4, v4, v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 236
    const v4, 0x7f07022c

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .end local v24           #tv:Landroid/widget/TextView;
    check-cast v24, Landroid/widget/TextView;

    .line 237
    .restart local v24       #tv:Landroid/widget/TextView;
    const/16 v4, 0x8

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 240
    :pswitch_2
    const v4, 0x7f07022b

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .end local v24           #tv:Landroid/widget/TextView;
    check-cast v24, Landroid/widget/TextView;

    .line 241
    .restart local v24       #tv:Landroid/widget/TextView;
    move-object/from16 v0, v17

    iget-object v4, v0, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    const/16 v4, 0x8

    invoke-virtual {v9, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 243
    const v4, 0x7f070227

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .end local v24           #tv:Landroid/widget/TextView;
    check-cast v24, Landroid/widget/TextView;

    .line 244
    .restart local v24       #tv:Landroid/widget/TextView;
    const/16 v4, 0x8

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 245
    sget-object v4, Lnet/flixster/android/Flixster;->RATING_LARGE_R:[I

    move-object/from16 v0, v17

    iget-wide v0, v0, Lnet/flixster/android/model/Review;->stars:D

    move-wide/from16 v25, v0

    const-wide/high16 v27, 0x4000

    mul-double v25, v25, v27

    move-wide/from16 v0, v25

    double-to-int v5, v0

    aget v4, v4, v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 246
    const v4, 0x7f07022c

    invoke-virtual {v11, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .end local v24           #tv:Landroid/widget/TextView;
    check-cast v24, Landroid/widget/TextView;

    .line 247
    .restart local v24       #tv:Landroid/widget/TextView;
    const/16 v4, 0x8

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 158
    nop

    :array_0
    .array-data 0x4
        0x1et 0x2t 0x7t 0x7ft
        0x1ft 0x2t 0x7t 0x7ft
        0x20t 0x2t 0x7t 0x7ft
    .end array-data

    .line 213
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private showLoading()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lnet/flixster/android/ReviewPage;->throbber:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 266
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .parameter "v"

    .prologue
    .line 365
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 383
    :cond_0
    :goto_0
    return-void

    .line 367
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Review;

    .line 368
    .local v1, r:Lnet/flixster/android/model/Review;
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v4

    const-string v5, "/movie/criticreview"

    .line 369
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Critic Review - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v8, "title"

    invoke-virtual {v7, v8}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 368
    invoke-interface {v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    if-eqz v1, :cond_0

    iget-object v4, v1, Lnet/flixster/android/model/Review;->url:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, v1, Lnet/flixster/android/model/Review;->url:Ljava/lang/String;

    const-string v5, "http://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 371
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    iget-object v5, v1, Lnet/flixster/android/model/Review;->url:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 372
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lnet/flixster/android/ReviewPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 376
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #r:Lnet/flixster/android/model/Review;
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Review;

    .line 377
    .local v2, review:Lnet/flixster/android/model/Review;
    iget-wide v4, v2, Lnet/flixster/android/model/Review;->userId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 378
    .local v3, userId:Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-class v4, Lnet/flixster/android/UserProfilePage;

    invoke-direct {v0, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 379
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v4, "PLATFORM_ID"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 380
    invoke-virtual {p0, v0}, Lnet/flixster/android/ReviewPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 365
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f070224 -> :sswitch_1
        0x7f07022a -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    const/16 v3, 0x8

    .line 54
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v2, 0x7f030072

    invoke-virtual {p0, v2}, Lnet/flixster/android/ReviewPage;->setContentView(I)V

    .line 56
    const v2, 0x7f070219

    invoke-virtual {p0, v2}, Lnet/flixster/android/ReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 57
    const v2, 0x7f07021b

    invoke-virtual {p0, v2}, Lnet/flixster/android/ReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 58
    const v2, 0x7f07021c

    invoke-virtual {p0, v2}, Lnet/flixster/android/ReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 59
    const v2, 0x7f07021a

    invoke-virtual {p0, v2}, Lnet/flixster/android/ReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 60
    invoke-virtual {p0}, Lnet/flixster/android/ReviewPage;->createActionBar()V

    .line 61
    const v2, 0x7f0c005b

    invoke-virtual {p0, v2}, Lnet/flixster/android/ReviewPage;->setActionBarTitle(I)V

    .line 63
    invoke-virtual {p0}, Lnet/flixster/android/ReviewPage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 64
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 65
    const-string v2, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lnet/flixster/android/ReviewPage;->mMovieId:J

    .line 66
    iget-wide v2, p0, Lnet/flixster/android/ReviewPage;->mMovieId:J

    invoke-static {v2, v3}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    .line 67
    const-string v2, "REVIEW_INDEX"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/ReviewPage;->mStartReviewIndex:I

    .line 68
    const-string v2, "MOVIE_THUMBNAIL"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 69
    .local v0, b:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 70
    iget-object v2, p0, Lnet/flixster/android/ReviewPage;->mMovie:Lnet/flixster/android/model/Movie;

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, v2, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    .line 74
    .end local v0           #b:Landroid/graphics/Bitmap;
    :cond_0
    iput-object p0, p0, Lnet/flixster/android/ReviewPage;->mReviewPage:Lnet/flixster/android/ReviewPage;

    .line 75
    const v2, 0x7f070222

    invoke-virtual {p0, v2}, Lnet/flixster/android/ReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ViewFlipper;

    iput-object v2, p0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    .line 76
    const v2, 0x7f070039

    invoke-virtual {p0, v2}, Lnet/flixster/android/ReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/ReviewPage;->throbber:Landroid/view/View;

    .line 77
    invoke-direct {p0}, Lnet/flixster/android/ReviewPage;->populatePage()V

    .line 78
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "dialogId"

    .prologue
    .line 310
    packed-switch p1, :pswitch_data_0

    .line 329
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    .line 312
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 313
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v1, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 314
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 315
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 316
    const-string v1, "Retry"

    new-instance v2, Lnet/flixster/android/ReviewPage$4;

    invoke-direct {v2, p0}, Lnet/flixster/android/ReviewPage$4;-><init>(Lnet/flixster/android/ReviewPage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 321
    invoke-virtual {p0}, Lnet/flixster/android/ReviewPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 322
    new-instance v2, Lnet/flixster/android/ReviewPage$5;

    invoke-direct {v2, p0}, Lnet/flixster/android/ReviewPage$5;-><init>(Lnet/flixster/android/ReviewPage;)V

    .line 321
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 327
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 310
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 335
    invoke-virtual {p0}, Lnet/flixster/android/ReviewPage;->getSupportMenuInflater()Lcom/actionbarsherlock/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f000e

    invoke-virtual {v0, v1, p1}, Lcom/actionbarsherlock/view/MenuInflater;->inflate(ILcom/actionbarsherlock/view/Menu;)V

    .line 336
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onDestroy()V

    .line 96
    const-string v0, "FlxMain"

    const-string v1, "ReviewPage.onDestroy()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lnet/flixster/android/ReviewPage;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lnet/flixster/android/ReviewPage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 100
    iget-object v0, p0, Lnet/flixster/android/ReviewPage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 102
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/ReviewPage;->mTimer:Ljava/util/Timer;

    .line 103
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 3
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 341
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 355
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 343
    :pswitch_0
    iget-object v1, p0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    const v2, 0x7f040008

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 344
    iget-object v1, p0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    const v2, 0x7f040009

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 345
    iget-object v1, p0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->showPrevious()V

    .line 346
    invoke-direct {p0}, Lnet/flixster/android/ReviewPage;->getPageTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lnet/flixster/android/ReviewPage;->setActionBarTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 349
    :pswitch_1
    iget-object v1, p0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    const v2, 0x7f040006

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 350
    iget-object v1, p0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    const v2, 0x7f040007

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 351
    iget-object v1, p0, Lnet/flixster/android/ReviewPage;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->showNext()V

    .line 352
    invoke-direct {p0}, Lnet/flixster/android/ReviewPage;->getPageTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lnet/flixster/android/ReviewPage;->setActionBarTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 341
    nop

    :pswitch_data_0
    .packed-switch 0x7f07030b
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 83
    iget-object v0, p0, Lnet/flixster/android/ReviewPage;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/ReviewPage;->mTimer:Ljava/util/Timer;

    .line 87
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lnet/flixster/android/ReviewPage;->mReviewsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    :cond_1
    invoke-direct {p0}, Lnet/flixster/android/ReviewPage;->showLoading()V

    .line 89
    invoke-direct {p0}, Lnet/flixster/android/ReviewPage;->ScheduleLoadMoviesTask()V

    .line 91
    :cond_2
    return-void
.end method
