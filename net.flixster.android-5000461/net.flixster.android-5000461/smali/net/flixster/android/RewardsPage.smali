.class public Lnet/flixster/android/RewardsPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "RewardsPage.java"


# static fields
.field private static synthetic $SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType:[I


# instance fields
.field private availableRewardsHeader:Landroid/widget/TextView;

.field private availableRewardsLayout:Landroid/widget/LinearLayout;

.field private completedRewardsHeader:Landroid/widget/TextView;

.field private completedRewardsLayout:Landroid/widget/LinearLayout;

.field private final movieEarnedDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;


# direct methods
.method static synthetic $SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType()[I
    .locals 3

    .prologue
    .line 23
    sget-object v0, Lnet/flixster/android/RewardsPage;->$SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lnet/flixster/android/model/LockerRight$PurchaseType;->values()[Lnet/flixster/android/model/LockerRight$PurchaseType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->FB_INVITE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->INSTALL_MOBILE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->RATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->SMS:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->UNKNOWN:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->UV_CREATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->WTS:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lnet/flixster/android/RewardsPage;->$SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 168
    new-instance v0, Lnet/flixster/android/RewardsPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/RewardsPage$1;-><init>(Lnet/flixster/android/RewardsPage;)V

    iput-object v0, p0, Lnet/flixster/android/RewardsPage;->movieEarnedDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    .line 23
    return-void
.end method

.method private initializeRewardViews()V
    .locals 20

    .prologue
    .line 48
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->availableRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 49
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->completedRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 51
    const/4 v4, 0x0

    .local v4, earnedMoviesRate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v7, 0x0

    .local v7, earnedMoviesWts:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v5, 0x0

    .local v5, earnedMoviesSms:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v2, 0x0

    .local v2, earnedMoviesFbInvite:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v6, 0x0

    .local v6, earnedMoviesUvCreate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v3, 0x0

    .line 52
    .local v3, earnedMoviesInstallMobile:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v16

    .line 53
    .local v16, user:Lnet/flixster/android/model/User;
    invoke-virtual/range {v16 .. v16}, Lnet/flixster/android/model/User;->getLockerRights()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_7

    .line 94
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v8

    .line 96
    .local v8, p:Lnet/flixster/android/model/Property;
    new-instance v12, Lcom/flixster/android/view/RewardView;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/flixster/android/view/RewardView;-><init>(Landroid/content/Context;)V

    .line 97
    .local v12, rvRate:Lcom/flixster/android/view/RewardView;
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lnet/flixster/android/model/User;->isMskRateEligible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_e

    .line 98
    if-eqz v8, :cond_0

    iget-boolean v0, v8, Lnet/flixster/android/model/Property;->isQuickRateRewardEnabled:Z

    move/from16 v17, v0

    if-eqz v17, :cond_0

    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->availableRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 104
    :cond_0
    :goto_1
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0, v4}, Lcom/flixster/android/view/RewardView;->load(ILjava/util/List;)V

    .line 106
    new-instance v15, Lcom/flixster/android/view/RewardView;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/flixster/android/view/RewardView;-><init>(Landroid/content/Context;)V

    .line 107
    .local v15, rvWts:Lcom/flixster/android/view/RewardView;
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lnet/flixster/android/model/User;->isMskWtsEligible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_f

    .line 108
    if-eqz v8, :cond_1

    iget-boolean v0, v8, Lnet/flixster/android/model/Property;->isQuickWtsRewardEnabled:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->availableRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 114
    :cond_1
    :goto_2
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v15, v0, v7}, Lcom/flixster/android/view/RewardView;->load(ILjava/util/List;)V

    .line 116
    new-instance v13, Lcom/flixster/android/view/RewardView;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/flixster/android/view/RewardView;-><init>(Landroid/content/Context;)V

    .line 117
    .local v13, rvSms:Lcom/flixster/android/view/RewardView;
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lnet/flixster/android/model/User;->isMskSmsEligible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_10

    .line 118
    if-eqz v8, :cond_2

    iget-boolean v0, v8, Lnet/flixster/android/model/Property;->isSmsRewardEnabled:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    .line 119
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->availableRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 124
    :cond_2
    :goto_3
    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-virtual {v13, v0, v5}, Lcom/flixster/android/view/RewardView;->load(ILjava/util/List;)V

    .line 126
    new-instance v10, Lcom/flixster/android/view/RewardView;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/flixster/android/view/RewardView;-><init>(Landroid/content/Context;)V

    .line 127
    .local v10, rvFbInvite:Lcom/flixster/android/view/RewardView;
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lnet/flixster/android/model/User;->isMskFbInviteEligible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_11

    .line 128
    if-eqz v8, :cond_3

    iget-boolean v0, v8, Lnet/flixster/android/model/Property;->isFbInviteRewardEnabled:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->availableRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 134
    :cond_3
    :goto_4
    const/16 v17, 0x6

    move/from16 v0, v17

    invoke-virtual {v10, v0, v2}, Lcom/flixster/android/view/RewardView;->load(ILjava/util/List;)V

    .line 136
    new-instance v14, Lcom/flixster/android/view/RewardView;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/flixster/android/view/RewardView;-><init>(Landroid/content/Context;)V

    .line 137
    .local v14, rvUvCreate:Lcom/flixster/android/view/RewardView;
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lnet/flixster/android/model/User;->isMskUvCreateEligible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_12

    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->availableRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 142
    :cond_4
    :goto_5
    const/16 v17, 0x4

    move/from16 v0, v17

    invoke-virtual {v14, v0, v6}, Lcom/flixster/android/view/RewardView;->load(ILjava/util/List;)V

    .line 144
    new-instance v11, Lcom/flixster/android/view/RewardView;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/flixster/android/view/RewardView;-><init>(Landroid/content/Context;)V

    .line 145
    .local v11, rvInstallMobile:Lcom/flixster/android/view/RewardView;
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lnet/flixster/android/model/User;->isMskInstallMobileEligible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_13

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->availableRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 150
    :cond_5
    :goto_6
    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-virtual {v11, v0, v3}, Lcom/flixster/android/view/RewardView;->load(ILjava/util/List;)V

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->availableRewardsHeader:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->availableRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v17

    if-lez v17, :cond_14

    const/16 v17, 0x0

    :goto_7
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->completedRewardsHeader:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->completedRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v17

    if-lez v17, :cond_15

    const/16 v17, 0x0

    :goto_8
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 155
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lnet/flixster/android/model/User;->isMskInstallMobileEligible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_6

    .line 156
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lnet/flixster/android/model/User;->isMskInstallMobileEligible:Z

    .line 157
    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/RewardsPage;->insertInstallMobileLockerRight()V

    .line 159
    :cond_6
    return-void

    .line 53
    .end local v8           #p:Lnet/flixster/android/model/Property;
    .end local v10           #rvFbInvite:Lcom/flixster/android/view/RewardView;
    .end local v11           #rvInstallMobile:Lcom/flixster/android/view/RewardView;
    .end local v12           #rvRate:Lcom/flixster/android/view/RewardView;
    .end local v13           #rvSms:Lcom/flixster/android/view/RewardView;
    .end local v14           #rvUvCreate:Lcom/flixster/android/view/RewardView;
    .end local v15           #rvWts:Lcom/flixster/android/view/RewardView;
    :cond_7
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lnet/flixster/android/model/LockerRight;

    .line 54
    .local v9, r:Lnet/flixster/android/model/LockerRight;
    invoke-static {}, Lnet/flixster/android/RewardsPage;->$SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType()[I

    move-result-object v18

    iget-object v0, v9, Lnet/flixster/android/model/LockerRight;->purchaseType:Lnet/flixster/android/model/LockerRight$PurchaseType;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_0

    goto/16 :goto_0

    .line 56
    :pswitch_0
    if-nez v4, :cond_8

    .line 57
    new-instance v4, Ljava/util/ArrayList;

    .end local v4           #earnedMoviesRate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .restart local v4       #earnedMoviesRate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_8
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 62
    :pswitch_1
    if-nez v7, :cond_9

    .line 63
    new-instance v7, Ljava/util/ArrayList;

    .end local v7           #earnedMoviesWts:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .restart local v7       #earnedMoviesWts:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_9
    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 68
    :pswitch_2
    if-nez v5, :cond_a

    .line 69
    new-instance v5, Ljava/util/ArrayList;

    .end local v5           #earnedMoviesSms:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .restart local v5       #earnedMoviesSms:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_a
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 74
    :pswitch_3
    if-nez v2, :cond_b

    .line 75
    new-instance v2, Ljava/util/ArrayList;

    .end local v2           #earnedMoviesFbInvite:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 77
    .restart local v2       #earnedMoviesFbInvite:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_b
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 80
    :pswitch_4
    if-nez v6, :cond_c

    .line 81
    new-instance v6, Ljava/util/ArrayList;

    .end local v6           #earnedMoviesUvCreate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .restart local v6       #earnedMoviesUvCreate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_c
    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 86
    :pswitch_5
    if-nez v3, :cond_d

    .line 87
    new-instance v3, Ljava/util/ArrayList;

    .end local v3           #earnedMoviesInstallMobile:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .restart local v3       #earnedMoviesInstallMobile:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_d
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 101
    .end local v9           #r:Lnet/flixster/android/model/LockerRight;
    .restart local v8       #p:Lnet/flixster/android/model/Property;
    .restart local v12       #rvRate:Lcom/flixster/android/view/RewardView;
    :cond_e
    if-eqz v4, :cond_0

    .line 102
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->completedRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 111
    .restart local v15       #rvWts:Lcom/flixster/android/view/RewardView;
    :cond_f
    if-eqz v7, :cond_1

    .line 112
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->completedRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 121
    .restart local v13       #rvSms:Lcom/flixster/android/view/RewardView;
    :cond_10
    if-eqz v5, :cond_2

    .line 122
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->completedRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 131
    .restart local v10       #rvFbInvite:Lcom/flixster/android/view/RewardView;
    :cond_11
    if-eqz v2, :cond_3

    .line 132
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->completedRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 139
    .restart local v14       #rvUvCreate:Lcom/flixster/android/view/RewardView;
    :cond_12
    if-eqz v6, :cond_4

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->completedRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_5

    .line 147
    .restart local v11       #rvInstallMobile:Lcom/flixster/android/view/RewardView;
    :cond_13
    if-eqz v3, :cond_5

    .line 148
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/RewardsPage;->completedRewardsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 152
    :cond_14
    const/16 v17, 0x8

    goto/16 :goto_7

    .line 153
    :cond_15
    const/16 v17, 0x8

    goto/16 :goto_8

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private insertInstallMobileLockerRight()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 162
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/rewards"

    const-string v3, "FlixsterRewards"

    const-string v4, "CompletedReward"

    const-string v5, "MobileAppInstall"

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 163
    const-string v7, "MPN"

    .line 164
    .local v7, purchaseType:Ljava/lang/String;
    invoke-static {v2, v2, v7}, Lnet/flixster/android/data/ProfileDao;->insertLockerRight(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    .line 165
    const v0, 0x3b9acb91

    iget-object v1, p0, Lnet/flixster/android/RewardsPage;->movieEarnedDialogListener:Lcom/flixster/android/view/DialogBuilder$DialogListener;

    invoke-virtual {p0, v0, v1}, Lnet/flixster/android/RewardsPage;->showDialog(ILcom/flixster/android/view/DialogBuilder$DialogListener;)V

    .line 166
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const v0, 0x7f030075

    invoke-virtual {p0, v0}, Lnet/flixster/android/RewardsPage;->setContentView(I)V

    .line 31
    invoke-virtual {p0}, Lnet/flixster/android/RewardsPage;->createActionBar()V

    .line 32
    const v0, 0x7f0c0146

    invoke-virtual {p0, v0}, Lnet/flixster/android/RewardsPage;->setActionBarTitle(I)V

    .line 34
    const v0, 0x7f07022f

    invoke-virtual {p0, v0}, Lnet/flixster/android/RewardsPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/RewardsPage;->availableRewardsHeader:Landroid/widget/TextView;

    .line 35
    const v0, 0x7f070231

    invoke-virtual {p0, v0}, Lnet/flixster/android/RewardsPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/RewardsPage;->completedRewardsHeader:Landroid/widget/TextView;

    .line 36
    const v0, 0x7f070230

    invoke-virtual {p0, v0}, Lnet/flixster/android/RewardsPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lnet/flixster/android/RewardsPage;->availableRewardsLayout:Landroid/widget/LinearLayout;

    .line 37
    const v0, 0x7f070232

    invoke-virtual {p0, v0}, Lnet/flixster/android/RewardsPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lnet/flixster/android/RewardsPage;->completedRewardsLayout:Landroid/widget/LinearLayout;

    .line 38
    return-void
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 185
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 43
    invoke-direct {p0}, Lnet/flixster/android/RewardsPage;->initializeRewardViews()V

    .line 44
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/rewards/rewards-center"

    const-string v2, "Rewards - RewardsCenter"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    return-void
.end method
