.class Lnet/flixster/android/ScrollGalleryPage$1;
.super Landroid/os/Handler;
.source "ScrollGalleryPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ScrollGalleryPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ScrollGalleryPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ScrollGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ScrollGalleryPage$1;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    .line 280
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "message"

    .prologue
    .line 283
    const-string v0, "FlxMain"

    const-string v1, "MovieGalleryPage.updateHandler"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$1;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/ScrollGalleryPage;->access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 285
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$1;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #calls: Lnet/flixster/android/ScrollGalleryPage;->updatePage()V
    invoke-static {v0}, Lnet/flixster/android/ScrollGalleryPage;->access$1(Lnet/flixster/android/ScrollGalleryPage;)V

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$1;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    invoke-virtual {v0}, Lnet/flixster/android/ScrollGalleryPage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$1;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/ScrollGalleryPage;->showDialog(I)V

    goto :goto_0
.end method
