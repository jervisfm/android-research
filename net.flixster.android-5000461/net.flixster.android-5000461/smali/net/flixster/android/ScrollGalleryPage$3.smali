.class Lnet/flixster/android/ScrollGalleryPage$3;
.super Ljava/util/TimerTask;
.source "ScrollGalleryPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/ScrollGalleryPage;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ScrollGalleryPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ScrollGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    .line 251
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 254
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mType:I
    invoke-static {v1}, Lnet/flixster/android/ScrollGalleryPage;->access$4(Lnet/flixster/android/ScrollGalleryPage;)I
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 272
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->updateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/ScrollGalleryPage;->access$5(Lnet/flixster/android/ScrollGalleryPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 273
    return-void

    .line 256
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/ScrollGalleryPage;->access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 257
    iget-object v1, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/ScrollGalleryPage;->access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v2, v2, Lnet/flixster/android/ScrollGalleryPage;->mFilter:Ljava/lang/String;

    invoke-static {v2}, Lnet/flixster/android/data/PhotoDao;->getTopPhotos(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "ScrollGalleryPage.scheduleUpdatePageTask:failed to get photos"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 260
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_1
    :try_start_2
    iget-object v1, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/ScrollGalleryPage;->access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 261
    iget-object v1, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/ScrollGalleryPage;->access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-wide v2, v2, Lnet/flixster/android/ScrollGalleryPage;->mMovieId:J

    invoke-static {v2, v3}, Lnet/flixster/android/data/PhotoDao;->getMoviePhotos(J)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 264
    :pswitch_2
    iget-object v1, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/ScrollGalleryPage;->access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 265
    iget-object v1, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/ScrollGalleryPage;->access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$3;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-wide v2, v2, Lnet/flixster/android/ScrollGalleryPage;->mActorId:J

    invoke-static {v2, v3}, Lnet/flixster/android/data/PhotoDao;->getActorPhotos(J)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_2
    .catch Lnet/flixster/android/data/DaoException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 254
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
