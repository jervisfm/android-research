.class Lnet/flixster/android/ScrollGalleryPage$4;
.super Ljava/lang/Object;
.source "ScrollGalleryPage.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/ScrollGalleryPage;->updatePage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ScrollGalleryPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ScrollGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    .line 306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .parameter "parent"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 308
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v0, v0, Lnet/flixster/android/ScrollGalleryPage;->mTopLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 309
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v0, v0, Lnet/flixster/android/ScrollGalleryPage;->mTopLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 310
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v0, v0, Lnet/flixster/android/ScrollGalleryPage;->mCaption:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #calls: Lnet/flixster/android/ScrollGalleryPage;->hideActionBar()V
    invoke-static {v0}, Lnet/flixster/android/ScrollGalleryPage;->access$6(Lnet/flixster/android/ScrollGalleryPage;)V

    .line 312
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v0, v0, Lnet/flixster/android/ScrollGalleryPage;->mGallery:Landroid/widget/Gallery;

    invoke-virtual {v0}, Landroid/widget/Gallery;->invalidate()V

    .line 320
    :goto_0
    return-void

    .line 314
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v0, v0, Lnet/flixster/android/ScrollGalleryPage;->mTopLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v0, v0, Lnet/flixster/android/ScrollGalleryPage;->mCaption:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #calls: Lnet/flixster/android/ScrollGalleryPage;->showActionBar()V
    invoke-static {v0}, Lnet/flixster/android/ScrollGalleryPage;->access$7(Lnet/flixster/android/ScrollGalleryPage;)V

    .line 317
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v0, v0, Lnet/flixster/android/ScrollGalleryPage;->mGallery:Landroid/widget/Gallery;

    invoke-virtual {v0}, Landroid/widget/Gallery;->postInvalidate()V

    .line 318
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$4;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v0, v0, Lnet/flixster/android/ScrollGalleryPage;->mGallery:Landroid/widget/Gallery;

    invoke-virtual {v0}, Landroid/widget/Gallery;->invalidate()V

    goto :goto_0
.end method
