.class Lnet/flixster/android/ScrollGalleryPage$5;
.super Ljava/lang/Object;
.source "ScrollGalleryPage.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/ScrollGalleryPage;->updatePage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ScrollGalleryPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ScrollGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x1

    .line 327
    iget-object v4, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #setter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I
    invoke-static {v4, p3}, Lnet/flixster/android/ScrollGalleryPage;->access$8(Lnet/flixster/android/ScrollGalleryPage;I)V

    .line 328
    iget-object v4, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;
    invoke-static {v4}, Lnet/flixster/android/ScrollGalleryPage;->access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, p3, :cond_4

    .line 329
    iget-object v4, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;
    invoke-static {v4}, Lnet/flixster/android/ScrollGalleryPage;->access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Photo;

    .line 330
    .local v3, photo:Lnet/flixster/android/model/Photo;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 331
    .local v0, descriptionBuilder:Ljava/lang/StringBuilder;
    iget-object v4, v3, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, v3, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v5, :cond_0

    .line 332
    iget-object v4, v3, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    :cond_0
    iget-object v4, v3, Lnet/flixster/android/model/Photo;->description:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, v3, Lnet/flixster/android/model/Photo;->description:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v5, :cond_2

    .line 335
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 336
    const-string v4, " - "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    :cond_1
    iget-object v4, v3, Lnet/flixster/android/model/Photo;->description:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_5

    .line 341
    iget-object v4, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v4, v4, Lnet/flixster/android/ScrollGalleryPage;->mCaption:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 345
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 346
    .local v1, nowTime:J
    iget-object v4, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mLastPhotoTime:J
    invoke-static {v4}, Lnet/flixster/android/ScrollGalleryPage;->access$9(Lnet/flixster/android/ScrollGalleryPage;)J

    move-result-wide v4

    sub-long v4, v1, v4

    const-wide/16 v6, 0x3e8

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 347
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v4

    const-string v5, "/photo/view"

    const-string v6, "Photo View "

    invoke-interface {v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_3
    iget-object v4, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #setter for: Lnet/flixster/android/ScrollGalleryPage;->mLastPhotoTime:J
    invoke-static {v4, v1, v2}, Lnet/flixster/android/ScrollGalleryPage;->access$10(Lnet/flixster/android/ScrollGalleryPage;J)V

    .line 350
    iget-object v4, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    const v7, 0x7f0c003d

    invoke-virtual {v6, v7}, Lnet/flixster/android/ScrollGalleryPage;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I
    invoke-static {v6}, Lnet/flixster/android/ScrollGalleryPage;->access$11(Lnet/flixster/android/ScrollGalleryPage;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 351
    iget-object v6, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;
    invoke-static {v6}, Lnet/flixster/android/ScrollGalleryPage;->access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 350
    #calls: Lnet/flixster/android/ScrollGalleryPage;->setActionBarTitle(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lnet/flixster/android/ScrollGalleryPage;->access$12(Lnet/flixster/android/ScrollGalleryPage;Ljava/lang/String;)V

    .line 353
    .end local v0           #descriptionBuilder:Ljava/lang/StringBuilder;
    .end local v1           #nowTime:J
    .end local v3           #photo:Lnet/flixster/android/model/Photo;
    :cond_4
    return-void

    .line 343
    .restart local v0       #descriptionBuilder:Ljava/lang/StringBuilder;
    .restart local v3       #photo:Lnet/flixster/android/model/Photo;
    :cond_5
    iget-object v4, p0, Lnet/flixster/android/ScrollGalleryPage$5;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    iget-object v4, v4, Lnet/flixster/android/ScrollGalleryPage;->mCaption:Landroid/widget/TextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 356
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    return-void
.end method
