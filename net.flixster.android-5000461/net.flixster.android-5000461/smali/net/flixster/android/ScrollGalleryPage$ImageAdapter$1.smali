.class Lnet/flixster/android/ScrollGalleryPage$ImageAdapter$1;
.super Landroid/os/Handler;
.source "ScrollGalleryPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;


# direct methods
.method constructor <init>(Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter$1;->this$1:Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;

    .line 589
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "message"

    .prologue
    .line 592
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/widget/ImageView;

    .line 593
    .local v1, imageView:Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 594
    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Photo;

    .line 595
    .local v2, photo:Lnet/flixster/android/model/Photo;
    if-eqz v2, :cond_0

    .line 596
    iget-object v3, v2, Lnet/flixster/android/model/Photo;->galleryBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 597
    .local v0, galleryBitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 598
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 599
    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 600
    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 604
    .end local v0           #galleryBitmap:Landroid/graphics/Bitmap;
    .end local v2           #photo:Lnet/flixster/android/model/Photo;
    :cond_0
    return-void
.end method
