.class public Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "ScrollGalleryPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ScrollGalleryPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImageAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field mDeviceHeight:I

.field mDeviceWidth:I

.field mGalleryItemBackground:I

.field mPhotos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation
.end field

.field private photoHandler:Landroid/os/Handler;

.field final synthetic this$0:Lnet/flixster/android/ScrollGalleryPage;


# direct methods
.method public constructor <init>(Lnet/flixster/android/ScrollGalleryPage;Landroid/content/Context;Ljava/util/List;)V
    .locals 15
    .parameter
    .parameter "c"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 503
    .local p3, photos:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Photo;>;"
    move-object/from16 v0, p1

    iput-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->this$0:Lnet/flixster/android/ScrollGalleryPage;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 589
    new-instance v2, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter$1;

    invoke-direct {v2, p0}, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter$1;-><init>(Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;)V

    iput-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->photoHandler:Landroid/os/Handler;

    .line 504
    move-object/from16 v0, p2

    iput-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mContext:Landroid/content/Context;

    .line 505
    move-object/from16 v0, p3

    iput-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mPhotos:Ljava/util/List;

    .line 506
    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/ScrollGalleryPage;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v14

    .line 507
    .local v14, w:Landroid/view/WindowManager;
    invoke-interface {v14}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    .line 508
    .local v8, d:Landroid/view/Display;
    invoke-virtual {v8}, Landroid/view/Display;->getWidth()I

    move-result v2

    iput v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mDeviceWidth:I

    .line 509
    invoke-virtual {v8}, Landroid/view/Display;->getHeight()I

    move-result v2

    iput v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mDeviceHeight:I

    .line 512
    sget-object v2, Lnet/flixster/android/R$styleable;->Gallery1:[I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lnet/flixster/android/ScrollGalleryPage;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v7

    .line 513
    .local v7, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v7, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mGalleryItemBackground:I

    .line 514
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    .line 517
    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mPhotos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v13

    .line 518
    .local v13, size:I
    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mStartPhotoIndex:I
    invoke-static/range {p1 .. p1}, Lnet/flixster/android/ScrollGalleryPage;->access$2(Lnet/flixster/android/ScrollGalleryPage;)I

    move-result v2

    add-int/lit8 v4, v13, -0x1

    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mStartPhotoIndex:I
    invoke-static/range {p1 .. p1}, Lnet/flixster/android/ScrollGalleryPage;->access$2(Lnet/flixster/android/ScrollGalleryPage;)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 522
    .local v9, distance:I
    move v11, v9

    .local v11, i:I
    :goto_0
    if-gez v11, :cond_0

    .line 539
    return-void

    .line 523
    :cond_0
    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mStartPhotoIndex:I
    invoke-static/range {p1 .. p1}, Lnet/flixster/android/ScrollGalleryPage;->access$2(Lnet/flixster/android/ScrollGalleryPage;)I

    move-result v2

    sub-int v12, v2, v11

    .line 524
    .local v12, low:I
    #getter for: Lnet/flixster/android/ScrollGalleryPage;->mStartPhotoIndex:I
    invoke-static/range {p1 .. p1}, Lnet/flixster/android/ScrollGalleryPage;->access$2(Lnet/flixster/android/ScrollGalleryPage;)I

    move-result v2

    add-int v10, v2, v11

    .line 525
    .local v10, high:I
    if-ltz v12, :cond_1

    if-eqz v11, :cond_1

    .line 526
    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mPhotos:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Photo;

    .line 527
    .local v3, photo:Lnet/flixster/android/model/Photo;
    new-instance v1, Lnet/flixster/android/model/ImageOrder;

    const/4 v2, 0x3

    iget-object v4, v3, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    .line 528
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 527
    invoke-direct/range {v1 .. v6}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 529
    .local v1, imageOrder:Lnet/flixster/android/model/ImageOrder;
    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mContext:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/FlixsterActivity;

    invoke-virtual {v2, v1}, Lnet/flixster/android/FlixsterActivity;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 532
    .end local v1           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .end local v3           #photo:Lnet/flixster/android/model/Photo;
    :cond_1
    if-ge v10, v13, :cond_2

    .line 533
    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mPhotos:Ljava/util/List;

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Photo;

    .line 534
    .restart local v3       #photo:Lnet/flixster/android/model/Photo;
    new-instance v1, Lnet/flixster/android/model/ImageOrder;

    const/4 v2, 0x3

    iget-object v4, v3, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    .line 535
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 534
    invoke-direct/range {v1 .. v6}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 536
    .restart local v1       #imageOrder:Lnet/flixster/android/model/ImageOrder;
    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mContext:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/FlixsterActivity;

    invoke-virtual {v2, v1}, Lnet/flixster/android/FlixsterActivity;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 522
    .end local v1           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .end local v3           #photo:Lnet/flixster/android/model/Photo;
    :cond_2
    add-int/lit8 v11, v11, -0x1

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mPhotos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 547
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 551
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v5, -0x1

    .line 555
    if-nez p2, :cond_0

    .line 556
    new-instance p2, Landroid/widget/ImageView;

    .end local p2
    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p2, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .restart local p2
    :cond_0
    move-object v2, p2

    .line 558
    check-cast v2, Landroid/widget/ImageView;

    new-instance v4, Landroid/widget/Gallery$LayoutParams;

    .line 559
    invoke-direct {v4, v5, v5}, Landroid/widget/Gallery$LayoutParams;-><init>(II)V

    .line 558
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 561
    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mPhotos:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Photo;

    .line 562
    .local v3, photo:Lnet/flixster/android/model/Photo;
    iget-object v2, v3, Lnet/flixster/android/model/Photo;->galleryBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;

    .line 563
    .local v8, galleryBitmap:Landroid/graphics/Bitmap;
    if-eqz v8, :cond_1

    .line 564
    move-object v0, p2

    check-cast v0, Landroid/widget/ImageView;

    move-object v2, v0

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 565
    move-object v0, p2

    check-cast v0, Landroid/widget/ImageView;

    move-object v2, v0

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 586
    .end local v3           #photo:Lnet/flixster/android/model/Photo;
    .end local v8           #galleryBitmap:Landroid/graphics/Bitmap;
    :goto_0
    return-object p2

    .line 567
    .restart local v3       #photo:Lnet/flixster/android/model/Photo;
    .restart local v8       #galleryBitmap:Landroid/graphics/Bitmap;
    :cond_1
    iget-object v2, v3, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 568
    move-object v0, p2

    check-cast v0, Landroid/widget/ImageView;

    move-object v2, v0

    iget-object v4, v3, Lnet/flixster/android/model/Photo;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 575
    :goto_1
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 576
    new-instance v1, Lnet/flixster/android/model/ImageOrder;

    const/16 v2, 0x8

    iget-object v4, v3, Lnet/flixster/android/model/Photo;->galleryUrl:Ljava/lang/String;

    .line 577
    iget-object v6, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->photoHandler:Landroid/os/Handler;

    move-object v5, p2

    .line 576
    invoke-direct/range {v1 .. v6}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 578
    .local v1, imageOrder:Lnet/flixster/android/model/ImageOrder;
    const-string v2, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ScrollPhotoGalleryPage url:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v3, Lnet/flixster/android/model/Photo;->galleryUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;->mContext:Landroid/content/Context;

    check-cast v2, Lnet/flixster/android/FlixsterActivity;

    invoke-virtual {v2, v1}, Lnet/flixster/android/FlixsterActivity;->orderImage(Lnet/flixster/android/model/ImageOrder;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 583
    .end local v1           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .end local v3           #photo:Lnet/flixster/android/model/Photo;
    .end local v8           #galleryBitmap:Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 584
    .local v7, e:Ljava/lang/IndexOutOfBoundsException;
    const-string v2, "FlxMain"

    const-string v4, "ScrollGalleryPage.getView (mPhotos was cleared)"

    invoke-static {v2, v4}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 571
    .end local v7           #e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v3       #photo:Lnet/flixster/android/model/Photo;
    .restart local v8       #galleryBitmap:Landroid/graphics/Bitmap;
    :cond_2
    :try_start_1
    move-object v0, p2

    check-cast v0, Landroid/widget/ImageView;

    move-object v2, v0

    const v4, 0x7f020134

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 572
    move-object v0, p2

    check-cast v0, Landroid/widget/ImageView;

    move-object v2, v0

    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
