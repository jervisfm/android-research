.class public Lnet/flixster/android/ScrollGalleryPage;
.super Lnet/flixster/android/FlixsterActivity;
.source "ScrollGalleryPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;
    }
.end annotation


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field public static final KEY_GENERIC_GALLERY:Ljava/lang/String; = "KEY_GENERIC_GALLERY"

.field public static final KEY_GENERIC_PHOTOS:Ljava/lang/String; = "KEY_GENERIC_PHOTOS"

.field public static final KEY_PHOTO_COUNT:Ljava/lang/String; = "PHOTO_COUNT"

.field public static final KEY_PHOTO_INDEX:Ljava/lang/String; = "PHOTO_INDEX"

.field public static final KEY_TITLE:Ljava/lang/String; = "KEY_TITLE"

.field public static final PHOTO_VIEWTIME:J = 0x3e8L

.field private static final SAVE_PHOTO:I = 0x3

.field private static final SAVE_PHOTO_FAILED:I = 0x4

.field public static final TYPE_ACTOR:I = 0x2

.field public static final TYPE_GENERIC:I = 0x3

.field public static final TYPE_MOVIE:I = 0x1

.field public static final TYPE_TOP:I


# instance fields
.field mActorId:J

.field mCaption:Landroid/widget/TextView;

.field mContext:Landroid/content/Context;

.field private mDefaultAd:Lnet/flixster/android/ads/AdView;

.field mFilter:Ljava/lang/String;

.field mGallery:Landroid/widget/Gallery;

.field private mLastPhotoTime:J

.field mLoadingImage:Landroid/widget/ImageView;

.field mMovieId:J

.field private mPhotoIndex:I

.field mPhotoLayout:Landroid/widget/RelativeLayout;

.field private mPhotos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation
.end field

.field private mStartPhotoIndex:I

.field mTopLayout:Landroid/widget/RelativeLayout;

.field private mType:I

.field private originalHeaderText:Ljava/lang/String;

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mType:I

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mLastPhotoTime:J

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I

    .line 280
    new-instance v0, Lnet/flixster/android/ScrollGalleryPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/ScrollGalleryPage$1;-><init>(Lnet/flixster/android/ScrollGalleryPage;)V

    iput-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->updateHandler:Landroid/os/Handler;

    .line 48
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/ScrollGalleryPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/ScrollGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 294
    invoke-direct {p0}, Lnet/flixster/android/ScrollGalleryPage;->updatePage()V

    return-void
.end method

.method static synthetic access$10(Lnet/flixster/android/ScrollGalleryPage;J)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 67
    iput-wide p1, p0, Lnet/flixster/android/ScrollGalleryPage;->mLastPhotoTime:J

    return-void
.end method

.method static synthetic access$11(Lnet/flixster/android/ScrollGalleryPage;)I
    .locals 1
    .parameter

    .prologue
    .line 69
    iget v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I

    return v0
.end method

.method static synthetic access$12(Lnet/flixster/android/ScrollGalleryPage;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lnet/flixster/android/ScrollGalleryPage;->setActionBarTitle(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/ScrollGalleryPage;)I
    .locals 1
    .parameter

    .prologue
    .line 70
    iget v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mStartPhotoIndex:I

    return v0
.end method

.method static synthetic access$3(Lnet/flixster/android/ScrollGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 250
    invoke-direct {p0}, Lnet/flixster/android/ScrollGalleryPage;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/ScrollGalleryPage;)I
    .locals 1
    .parameter

    .prologue
    .line 60
    iget v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mType:I

    return v0
.end method

.method static synthetic access$5(Lnet/flixster/android/ScrollGalleryPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 280
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/ScrollGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lnet/flixster/android/ScrollGalleryPage;->hideActionBar()V

    return-void
.end method

.method static synthetic access$7(Lnet/flixster/android/ScrollGalleryPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lnet/flixster/android/ScrollGalleryPage;->showActionBar()V

    return-void
.end method

.method static synthetic access$8(Lnet/flixster/android/ScrollGalleryPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 69
    iput p1, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/ScrollGalleryPage;)J
    .locals 2
    .parameter

    .prologue
    .line 67
    iget-wide v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mLastPhotoTime:J

    return-wide v0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 251
    new-instance v0, Lnet/flixster/android/ScrollGalleryPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/ScrollGalleryPage$3;-><init>(Lnet/flixster/android/ScrollGalleryPage;)V

    .line 275
    .local v0, updatePageTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/ScrollGalleryPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 276
    iget-object v1, p0, Lnet/flixster/android/ScrollGalleryPage;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 278
    :cond_0
    return-void
.end method

.method private updatePage()V
    .locals 3

    .prologue
    .line 296
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 298
    iget v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 299
    iget v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mStartPhotoIndex:I

    iput v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I

    .line 303
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mGallery:Landroid/widget/Gallery;

    new-instance v1, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;

    iget-object v2, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;

    invoke-direct {v1, p0, p0, v2}, Lnet/flixster/android/ScrollGalleryPage$ImageAdapter;-><init>(Lnet/flixster/android/ScrollGalleryPage;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 306
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mGallery:Landroid/widget/Gallery;

    new-instance v1, Lnet/flixster/android/ScrollGalleryPage$4;

    invoke-direct {v1, p0}, Lnet/flixster/android/ScrollGalleryPage$4;-><init>(Lnet/flixster/android/ScrollGalleryPage;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 323
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mGallery:Landroid/widget/Gallery;

    new-instance v1, Lnet/flixster/android/ScrollGalleryPage$5;

    invoke-direct {v1, p0}, Lnet/flixster/android/ScrollGalleryPage$5;-><init>(Lnet/flixster/android/ScrollGalleryPage;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 359
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mGallery:Landroid/widget/Gallery;

    iget v1, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setSelection(I)V

    .line 361
    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .parameter "savedState"

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x8

    .line 95
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 96
    iput-object p0, p0, Lnet/flixster/android/ScrollGalleryPage;->mContext:Landroid/content/Context;

    .line 97
    const v6, 0x7f030032

    invoke-virtual {p0, v6}, Lnet/flixster/android/ScrollGalleryPage;->setContentView(I)V

    .line 98
    const v6, 0x7f0700ab

    invoke-virtual {p0, v6}, Lnet/flixster/android/ScrollGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 99
    const v6, 0x7f0700ac

    invoke-virtual {p0, v6}, Lnet/flixster/android/ScrollGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 100
    invoke-virtual {p0}, Lnet/flixster/android/ScrollGalleryPage;->createActionBar()V

    .line 101
    const v6, 0x7f0c003d

    invoke-virtual {p0, v6}, Lnet/flixster/android/ScrollGalleryPage;->setActionBarTitle(I)V

    .line 103
    const v6, 0x7f0700af

    invoke-virtual {p0, v6}, Lnet/flixster/android/ScrollGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mCaption:Landroid/widget/TextView;

    .line 104
    const v6, 0x7f0700aa

    invoke-virtual {p0, v6}, Lnet/flixster/android/ScrollGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mTopLayout:Landroid/widget/RelativeLayout;

    .line 105
    const v6, 0x7f0700ad

    invoke-virtual {p0, v6}, Lnet/flixster/android/ScrollGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoLayout:Landroid/widget/RelativeLayout;

    .line 106
    const v6, 0x7f0700ae

    invoke-virtual {p0, v6}, Lnet/flixster/android/ScrollGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Gallery;

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mGallery:Landroid/widget/Gallery;

    .line 107
    const v6, 0x7f07002a

    invoke-virtual {p0, v6}, Lnet/flixster/android/ScrollGalleryPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lnet/flixster/android/ads/AdView;

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    .line 109
    iget-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mTopLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 110
    iget-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mCaption:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    invoke-virtual {p0}, Lnet/flixster/android/ScrollGalleryPage;->hideActionBar()V

    .line 113
    invoke-virtual {p0}, Lnet/flixster/android/ScrollGalleryPage;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 114
    .local v1, extras:Landroid/os/Bundle;
    const-string v6, "FlxMain"

    const-string v8, "ScrollGalleryPage.onCreate"

    invoke-static {v6, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    if-eqz v1, :cond_2

    .line 116
    const-string v6, "PHOTO_INDEX"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mStartPhotoIndex:I

    .line 117
    iget v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mStartPhotoIndex:I

    if-gez v6, :cond_0

    .line 118
    iput v7, p0, Lnet/flixster/android/ScrollGalleryPage;->mStartPhotoIndex:I

    .line 121
    :cond_0
    const-string v6, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 122
    iput v10, p0, Lnet/flixster/android/ScrollGalleryPage;->mType:I

    .line 123
    const-string v6, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lnet/flixster/android/ScrollGalleryPage;->mMovieId:J

    .line 124
    iget-wide v8, p0, Lnet/flixster/android/ScrollGalleryPage;->mMovieId:J

    invoke-static {v8, v9}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v3

    .line 126
    .local v3, movie:Lnet/flixster/android/model/Movie;
    new-array v4, v10, [Ljava/lang/String;

    const-string v6, "title"

    aput-object v6, v4, v7

    .line 128
    .local v4, movieProperties:[Ljava/lang/String;
    array-length v8, v4

    move v6, v7

    :goto_0
    if-lt v6, v8, :cond_3

    .line 134
    const-string v6, "title"

    invoke-virtual {v3, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->originalHeaderText:Ljava/lang/String;

    .line 135
    iget-object v6, v3, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    if-nez v6, :cond_1

    .line 136
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v3, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    .line 138
    :cond_1
    iget-object v6, v3, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;

    .line 163
    .end local v3           #movie:Lnet/flixster/android/model/Movie;
    .end local v4           #movieProperties:[Ljava/lang/String;
    :goto_1
    const-string v6, "FlxMain"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ScrollGalleryPage.onCreate mType:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lnet/flixster/android/ScrollGalleryPage;->mType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_2
    return-void

    .line 128
    .restart local v3       #movie:Lnet/flixster/android/model/Movie;
    .restart local v4       #movieProperties:[Ljava/lang/String;
    :cond_3
    aget-object v2, v4, v6

    .line 129
    .local v2, key:Ljava/lang/String;
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 130
    .local v5, value:Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 131
    invoke-virtual {v3, v2, v5}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 139
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #movie:Lnet/flixster/android/model/Movie;
    .end local v4           #movieProperties:[Ljava/lang/String;
    .end local v5           #value:Ljava/lang/String;
    :cond_5
    const-string v6, "ACTOR_ID"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 140
    const/4 v6, 0x2

    iput v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mType:I

    .line 142
    new-instance v0, Lnet/flixster/android/model/Actor;

    invoke-direct {v0}, Lnet/flixster/android/model/Actor;-><init>()V

    .line 143
    .local v0, actor:Lnet/flixster/android/model/Actor;
    const-string v6, "ACTOR_ID"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mActorId:J

    .line 144
    iget-wide v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mActorId:J

    iput-wide v6, v0, Lnet/flixster/android/model/Actor;->id:J

    .line 145
    const-string v6, "ACTOR_NAME"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    .line 146
    iget-object v6, v0, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->originalHeaderText:Ljava/lang/String;

    .line 147
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v0, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    .line 148
    iget-object v6, v0, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;

    goto :goto_1

    .line 149
    .end local v0           #actor:Lnet/flixster/android/model/Actor;
    :cond_6
    const-string v6, "KEY_GENERIC_GALLERY"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 151
    const/4 v6, 0x3

    iput v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mType:I

    .line 152
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v6

    const-string v7, "KEY_PHOTOS"

    invoke-virtual {v6, v7}, Lcom/flixster/android/utils/ObjectHolder;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;

    .line 153
    const-string v6, "KEY_TITLE"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->originalHeaderText:Ljava/lang/String;

    goto :goto_1

    .line 156
    :cond_7
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;

    .line 157
    const-string v6, "KEY_PHOTO_FILTER"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mFilter:Ljava/lang/String;

    .line 158
    iget-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mFilter:Ljava/lang/String;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mFilter:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_9

    .line 159
    :cond_8
    const-string v6, "random"

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->mFilter:Ljava/lang/String;

    .line 161
    :cond_9
    const-string v6, "Top Photos"

    iput-object v6, p0, Lnet/flixster/android/ScrollGalleryPage;->originalHeaderText:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "dialogId"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 216
    packed-switch p1, :pswitch_data_0

    .line 246
    :pswitch_0
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v3

    :goto_0
    return-object v3

    .line 219
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 220
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v3, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 221
    const-string v3, "Network Error"

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 222
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 223
    const-string v3, "Retry"

    new-instance v4, Lnet/flixster/android/ScrollGalleryPage$2;

    invoke-direct {v4, p0}, Lnet/flixster/android/ScrollGalleryPage$2;-><init>(Lnet/flixster/android/ScrollGalleryPage;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 228
    invoke-virtual {p0}, Lnet/flixster/android/ScrollGalleryPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 229
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    .line 232
    .end local v0           #alertBuilder:Landroid/app/AlertDialog$Builder;
    :pswitch_2
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 233
    .local v2, alertBuilderSavePhoto:Landroid/app/AlertDialog$Builder;
    const-string v3, "The image was saved to your gallery."

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 234
    const-string v3, "Image Saved"

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 235
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 236
    const-string v3, "OK"

    invoke-virtual {v2, v3, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 237
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    .line 239
    .end local v2           #alertBuilderSavePhoto:Landroid/app/AlertDialog$Builder;
    :pswitch_3
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 240
    .local v1, alertBuilderSaveError:Landroid/app/AlertDialog$Builder;
    const-string v3, "We\'re sorry, we were unable to save the image."

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 241
    const-string v3, "Save Image Error"

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 242
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 243
    const-string v3, "OK"

    invoke-virtual {v1, v3, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 244
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 378
    invoke-virtual {p0}, Lnet/flixster/android/ScrollGalleryPage;->getSupportMenuInflater()Lcom/actionbarsherlock/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f000f

    invoke-virtual {v0, v1, p1}, Lcom/actionbarsherlock/view/MenuInflater;->inflate(ILcom/actionbarsherlock/view/Menu;)V

    .line 379
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 206
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onDestroy()V

    .line 207
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 8
    .parameter "item"

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 413
    iget-object v4, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget v5, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I

    if-le v4, v5, :cond_1

    .line 414
    iget-object v4, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;

    iget v5, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Photo;

    .line 416
    .local v1, photo:Lnet/flixster/android/model/Photo;
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 475
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v3

    .line 480
    .end local v1           #photo:Lnet/flixster/android/model/Photo;
    :goto_0
    return v3

    .line 419
    .restart local v1       #photo:Lnet/flixster/android/model/Photo;
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v4, "BOXOFFICE"

    const-class v5, Lnet/flixster/android/Flixster;

    invoke-direct {v0, v4, v7, p0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 420
    .local v0, intent:Landroid/content/Intent;
    const/high16 v4, 0x400

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 421
    invoke-virtual {p0, v0}, Lnet/flixster/android/ScrollGalleryPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 432
    .end local v0           #intent:Landroid/content/Intent;
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v4, "DETAILS"

    invoke-virtual {p0}, Lnet/flixster/android/ScrollGalleryPage;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lnet/flixster/android/MovieDetails;

    invoke-direct {v0, v4, v7, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 433
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v4, "net.flixster.android.EXTRA_MOVIE_ID"

    iget-wide v5, v1, Lnet/flixster/android/model/Photo;->movieId:J

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 434
    const-string v4, "title"

    iget-object v5, v1, Lnet/flixster/android/model/Photo;->movieTitle:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 435
    invoke-virtual {p0, v0}, Lnet/flixster/android/ScrollGalleryPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 465
    .end local v0           #intent:Landroid/content/Intent;
    :sswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v4

    const-string v5, "/photo/save"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "galleryUrl:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v1, Lnet/flixster/android/model/Photo;->galleryUrl:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    invoke-static {v1, p0}, Lnet/flixster/android/util/MediaHelper;->savePhoto(Lnet/flixster/android/model/Photo;Landroid/content/Context;)Z

    move-result v2

    .line 467
    .local v2, wasSucccessfull:Z
    if-eqz v2, :cond_0

    .line 468
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lnet/flixster/android/ScrollGalleryPage;->showDialog(I)V

    goto :goto_0

    .line 470
    :cond_0
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lnet/flixster/android/ScrollGalleryPage;->showDialog(I)V

    goto :goto_0

    .line 478
    .end local v1           #photo:Lnet/flixster/android/model/Photo;
    .end local v2           #wasSucccessfull:Z
    :cond_1
    invoke-virtual {p0, v3}, Lnet/flixster/android/ScrollGalleryPage;->showDialog(I)V

    goto :goto_0

    .line 416
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0702fe -> :sswitch_0
        0x7f070309 -> :sswitch_1
        0x7f07030a -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 197
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onPause()V

    .line 198
    iget v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mType:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 199
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 201
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 372
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 373
    const-string v0, "PHOTO_INDEX"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I

    .line 374
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 169
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 170
    const-string v0, "FlxMain"

    const-string v1, "ScrollGalleryPage.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mType:I

    packed-switch v0, :pswitch_data_0

    .line 179
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/photo/fullscreen/top_photos"

    const-string v2, "ScrollGalleryPage"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :goto_0
    invoke-direct {p0}, Lnet/flixster/android/ScrollGalleryPage;->scheduleUpdatePageTask()V

    .line 189
    iget-object v0, p0, Lnet/flixster/android/ScrollGalleryPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 193
    return-void

    .line 173
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/photo/fullscreen/movie"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ScrollGalleryPage:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/ScrollGalleryPage;->originalHeaderText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/photo/fullscreen/actor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ScrollGalleryPage:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/ScrollGalleryPage;->originalHeaderText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 365
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 366
    const-string v0, "PHOTO_INDEX"

    iget v1, p0, Lnet/flixster/android/ScrollGalleryPage;->mPhotoIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 368
    return-void
.end method
