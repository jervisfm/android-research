.class Lnet/flixster/android/SearchPage$1;
.super Ljava/lang/Object;
.source "SearchPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/SearchPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SearchPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/SearchPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SearchPage$1;->this$0:Lnet/flixster/android/SearchPage;

    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 270
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 278
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/SearchPage$1;->this$0:Lnet/flixster/android/SearchPage;

    #calls: Lnet/flixster/android/SearchPage;->updateSearchFieldHint()V
    invoke-static {v0}, Lnet/flixster/android/SearchPage;->access$1(Lnet/flixster/android/SearchPage;)V

    .line 280
    iget-object v0, p0, Lnet/flixster/android/SearchPage$1;->this$0:Lnet/flixster/android/SearchPage;

    const-wide/16 v1, 0xa

    #calls: Lnet/flixster/android/SearchPage;->ScheduleLoadItemsTask(J)V
    invoke-static {v0, v1, v2}, Lnet/flixster/android/SearchPage;->access$2(Lnet/flixster/android/SearchPage;J)V

    .line 281
    return-void

    .line 272
    :pswitch_0
    const/4 v0, 0x0

    invoke-static {v0}, Lnet/flixster/android/SearchPage;->access$0(I)V

    goto :goto_0

    .line 275
    :pswitch_1
    const/4 v0, 0x1

    invoke-static {v0}, Lnet/flixster/android/SearchPage;->access$0(I)V

    goto :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
