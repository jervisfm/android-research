.class Lnet/flixster/android/SearchPage$2;
.super Ljava/lang/Object;
.source "SearchPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/SearchPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SearchPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/SearchPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SearchPage$2;->this$0:Lnet/flixster/android/SearchPage;

    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "view"

    .prologue
    .line 287
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 288
    .local v0, actor:Lnet/flixster/android/model/Actor;
    if-eqz v0, :cond_0

    .line 289
    new-instance v1, Landroid/content/Intent;

    const-string v2, "TOP_ACTOR"

    const/4 v3, 0x0

    iget-object v4, p0, Lnet/flixster/android/SearchPage$2;->this$0:Lnet/flixster/android/SearchPage;

    const-class v5, Lnet/flixster/android/ActorPage;

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 290
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "ACTOR_ID"

    iget-wide v3, v0, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 291
    const-string v2, "ACTOR_NAME"

    iget-object v3, v0, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    iget-object v2, p0, Lnet/flixster/android/SearchPage$2;->this$0:Lnet/flixster/android/SearchPage;

    invoke-virtual {v2, v1}, Lnet/flixster/android/SearchPage;->startActivity(Landroid/content/Intent;)V

    .line 294
    .end local v1           #intent:Landroid/content/Intent;
    :cond_0
    return-void
.end method
