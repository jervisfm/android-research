.class Lnet/flixster/android/SearchPage$4;
.super Ljava/util/TimerTask;
.source "SearchPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/SearchPage;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SearchPage;

.field private final synthetic val$currResumeCtr:I


# direct methods
.method constructor <init>(Lnet/flixster/android/SearchPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iput p2, p0, Lnet/flixster/android/SearchPage$4;->val$currResumeCtr:I

    .line 105
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 109
    :try_start_0
    invoke-static {}, Lnet/flixster/android/SearchPage;->access$4()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 147
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget-object v1, v1, Lnet/flixster/android/SearchPage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget-object v1, v1, Lnet/flixster/android/SearchPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 162
    :goto_1
    return-void

    .line 111
    :pswitch_0
    :try_start_1
    invoke-static {}, Lnet/flixster/android/SearchPage;->access$5()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    #getter for: Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/SearchPage;->access$6(Lnet/flixster/android/SearchPage;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 112
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget-object v1, v1, Lnet/flixster/android/SearchPage;->throbberHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 113
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    #getter for: Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/SearchPage;->access$6(Lnet/flixster/android/SearchPage;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/SearchPage;->access$5()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, Lnet/flixster/android/data/MovieDao;->searchMovies(Ljava/lang/String;Ljava/util/List;)V

    .line 116
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget v2, p0, Lnet/flixster/android/SearchPage$4;->val$currResumeCtr:I

    invoke-virtual {v1, v2}, Lnet/flixster/android/SearchPage;->shouldSkipBackgroundTask(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    if-eqz v1, :cond_2

    .line 160
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget-object v1, v1, Lnet/flixster/android/SearchPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 120
    :cond_2
    :try_start_2
    invoke-static {}, Lnet/flixster/android/SearchPage;->access$5()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 121
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/search/movie/results"

    .line 122
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "Search Movie - "

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    #getter for: Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/SearchPage;->access$6(Lnet/flixster/android/SearchPage;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    #getter for: Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/SearchPage;->access$6(Lnet/flixster/android/SearchPage;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-interface {v2, v3, v1}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :goto_3
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    #calls: Lnet/flixster/android/SearchPage;->setMovieLviList()V
    invoke-static {v1}, Lnet/flixster/android/SearchPage;->access$7(Lnet/flixster/android/SearchPage;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 150
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_3
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SearchPage.ScheduleLoadItemsTask.run() mRetryCount:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget v3, v3, Lnet/flixster/android/SearchPage;->mRetryCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->printStackTrace()V

    .line 152
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget v2, v1, Lnet/flixster/android/SearchPage;->mRetryCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lnet/flixster/android/SearchPage;->mRetryCount:I

    .line 153
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget v1, v1, Lnet/flixster/android/SearchPage;->mRetryCount:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_8

    .line 154
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    const-wide/16 v2, 0x3e8

    #calls: Lnet/flixster/android/SearchPage;->ScheduleLoadItemsTask(J)V
    invoke-static {v1, v2, v3}, Lnet/flixster/android/SearchPage;->access$2(Lnet/flixster/android/SearchPage;J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 160
    :goto_4
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget-object v1, v1, Lnet/flixster/android/SearchPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 122
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_3
    :try_start_4
    const-string v1, "Results"

    goto :goto_2

    .line 124
    :cond_4
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/search"

    const-string v3, "Search"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    .line 159
    :catchall_0
    move-exception v1

    .line 160
    iget-object v2, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget-object v2, v2, Lnet/flixster/android/SearchPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 161
    throw v1

    .line 129
    :pswitch_1
    :try_start_5
    invoke-static {}, Lnet/flixster/android/SearchPage;->access$8()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    #getter for: Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/SearchPage;->access$6(Lnet/flixster/android/SearchPage;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 130
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget-object v1, v1, Lnet/flixster/android/SearchPage;->throbberHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 131
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    #getter for: Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/SearchPage;->access$6(Lnet/flixster/android/SearchPage;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/SearchPage;->access$8()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, Lnet/flixster/android/data/ActorDao;->searchActors(Ljava/lang/String;Ljava/util/List;)V

    .line 134
    :cond_5
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget v2, p0, Lnet/flixster/android/SearchPage$4;->val$currResumeCtr:I

    invoke-virtual {v1, v2}, Lnet/flixster/android/SearchPage;->shouldSkipBackgroundTask(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 138
    invoke-static {}, Lnet/flixster/android/SearchPage;->access$8()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 139
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/search/actor/results"

    .line 140
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "Search Actor - "

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    #getter for: Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/SearchPage;->access$6(Lnet/flixster/android/SearchPage;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    #getter for: Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/SearchPage;->access$6(Lnet/flixster/android/SearchPage;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 139
    invoke-interface {v2, v3, v1}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :goto_6
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    #calls: Lnet/flixster/android/SearchPage;->setActorResultsLviList()V
    invoke-static {v1}, Lnet/flixster/android/SearchPage;->access$9(Lnet/flixster/android/SearchPage;)V

    goto/16 :goto_0

    .line 140
    :cond_6
    const-string v1, "Results"

    goto :goto_5

    .line 142
    :cond_7
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/search"

    const-string v3, "Search"

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_6

    .line 156
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    :cond_8
    :try_start_6
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    const/4 v2, 0x0

    iput v2, v1, Lnet/flixster/android/SearchPage;->mRetryCount:I

    .line 157
    iget-object v1, p0, Lnet/flixster/android/SearchPage$4;->this$0:Lnet/flixster/android/SearchPage;

    iget-object v1, v1, Lnet/flixster/android/SearchPage;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_4

    .line 109
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
