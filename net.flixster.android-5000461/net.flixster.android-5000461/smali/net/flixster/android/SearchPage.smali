.class public Lnet/flixster/android/SearchPage;
.super Lnet/flixster/android/LviActivity;
.source "SearchPage.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final ACTOR_SEARCH:I = 0x1

.field private static final MOVIE_SEARCH:I

.field private static final sActorResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field

.field private static final sMovieResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private static sSearchType:I


# instance fields
.field private actorClickListener:Landroid/view/View$OnClickListener;

.field private mQueryString:Ljava/lang/String;

.field private navBar:Lcom/flixster/android/view/SubNavBar;

.field private searchField:Landroid/widget/EditText;

.field private searchTypeListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lnet/flixster/android/SearchPage;->sMovieResults:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lnet/flixster/android/SearchPage;->sActorResults:Ljava/util/ArrayList;

    .line 43
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lnet/flixster/android/LviActivity;-><init>()V

    .line 268
    new-instance v0, Lnet/flixster/android/SearchPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/SearchPage$1;-><init>(Lnet/flixster/android/SearchPage;)V

    iput-object v0, p0, Lnet/flixster/android/SearchPage;->searchTypeListener:Landroid/view/View$OnClickListener;

    .line 284
    new-instance v0, Lnet/flixster/android/SearchPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/SearchPage$2;-><init>(Lnet/flixster/android/SearchPage;)V

    iput-object v0, p0, Lnet/flixster/android/SearchPage;->actorClickListener:Landroid/view/View$OnClickListener;

    .line 43
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 4
    .parameter "delay"

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    .line 105
    .local v0, currResumeCtr:I
    new-instance v1, Lnet/flixster/android/SearchPage$4;

    invoke-direct {v1, p0, v0}, Lnet/flixster/android/SearchPage$4;-><init>(Lnet/flixster/android/SearchPage;I)V

    .line 165
    .local v1, loadMoviesTask:Ljava/util/TimerTask;
    const-string v2, "FlxMain"

    const-string v3, "SearchPage.ScheduleLoadItemsTask() loading item"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v2, p0, Lnet/flixster/android/SearchPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 167
    iget-object v2, p0, Lnet/flixster/android/SearchPage;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v1, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    :cond_0
    monitor-exit p0

    return-void

    .line 104
    .end local v0           #currResumeCtr:I
    .end local v1           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(I)V
    .locals 0
    .parameter

    .prologue
    .line 49
    sput p0, Lnet/flixster/android/SearchPage;->sSearchType:I

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/SearchPage;)V
    .locals 0
    .parameter

    .prologue
    .line 315
    invoke-direct {p0}, Lnet/flixster/android/SearchPage;->updateSearchFieldHint()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/SearchPage;J)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/SearchPage;->ScheduleLoadItemsTask(J)V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/SearchPage;)V
    .locals 0
    .parameter

    .prologue
    .line 297
    invoke-direct {p0}, Lnet/flixster/android/SearchPage;->showSoftKeyboard()V

    return-void
.end method

.method static synthetic access$4()I
    .locals 1

    .prologue
    .line 49
    sget v0, Lnet/flixster/android/SearchPage;->sSearchType:I

    return v0
.end method

.method static synthetic access$5()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lnet/flixster/android/SearchPage;->sMovieResults:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/SearchPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7(Lnet/flixster/android/SearchPage;)V
    .locals 0
    .parameter

    .prologue
    .line 171
    invoke-direct {p0}, Lnet/flixster/android/SearchPage;->setMovieLviList()V

    return-void
.end method

.method static synthetic access$8()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lnet/flixster/android/SearchPage;->sActorResults:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$9(Lnet/flixster/android/SearchPage;)V
    .locals 0
    .parameter

    .prologue
    .line 192
    invoke-direct {p0}, Lnet/flixster/android/SearchPage;->setActorResultsLviList()V

    return-void
.end method

.method private addAd()V
    .locals 2

    .prologue
    .line 212
    invoke-virtual {p0}, Lnet/flixster/android/SearchPage;->destroyExistingLviAd()V

    .line 213
    new-instance v0, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/SearchPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 214
    iget-object v0, p0, Lnet/flixster/android/SearchPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v1, "SearchResults"

    iput-object v1, v0, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 215
    iget-object v0, p0, Lnet/flixster/android/SearchPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v1, p0, Lnet/flixster/android/SearchPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    return-void
.end method

.method private addNoResults()V
    .locals 3

    .prologue
    .line 219
    new-instance v0, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 220
    .local v0, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    invoke-virtual {p0}, Lnet/flixster/android/SearchPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c010a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 221
    iget-object v1, p0, Lnet/flixster/android/SearchPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    return-void
.end method

.method private hideSoftKeyboard(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 302
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lnet/flixster/android/SearchPage;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 303
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 305
    return-void
.end method

.method private setActorResultsLviList()V
    .locals 5

    .prologue
    .line 193
    const-string v3, "FlxMain"

    const-string v4, "SearchPage.setActorResultsLviList "

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 195
    invoke-direct {p0}, Lnet/flixster/android/SearchPage;->addAd()V

    .line 197
    sget-object v3, Lnet/flixster/android/SearchPage;->sActorResults:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 199
    .local v2, sActorResultsCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Actor;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 205
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 206
    invoke-direct {p0}, Lnet/flixster/android/SearchPage;->addNoResults()V

    .line 208
    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;

    .line 209
    return-void

    .line 199
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 200
    .local v0, actor:Lnet/flixster/android/model/Actor;
    new-instance v1, Lnet/flixster/android/lvi/LviActor;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviActor;-><init>()V

    .line 201
    .local v1, lviActor:Lnet/flixster/android/lvi/LviActor;
    iput-object v0, v1, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    .line 202
    iget-object v4, p0, Lnet/flixster/android/SearchPage;->actorClickListener:Landroid/view/View$OnClickListener;

    iput-object v4, v1, Lnet/flixster/android/lvi/LviActor;->mActorClick:Landroid/view/View$OnClickListener;

    .line 203
    iget-object v4, p0, Lnet/flixster/android/SearchPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private setMovieLviList()V
    .locals 5

    .prologue
    .line 172
    const-string v3, "FlxMain"

    const-string v4, "SearchPage.setSearchLviList "

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 174
    invoke-direct {p0}, Lnet/flixster/android/SearchPage;->addAd()V

    .line 176
    sget-object v3, Lnet/flixster/android/SearchPage;->sMovieResults:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 177
    .local v2, sMovieResultsCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 179
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 185
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->mDataHolder:Ljava/util/ArrayList;

    new-instance v4, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_0
    :goto_1
    const/4 v3, 0x0

    iput-object v3, p0, Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;

    .line 190
    return-void

    .line 179
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Movie;

    .line 180
    .local v1, movie:Lnet/flixster/android/model/Movie;
    new-instance v0, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 181
    .local v0, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v1, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 182
    invoke-virtual {p0}, Lnet/flixster/android/SearchPage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    iput-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 183
    iget-object v4, p0, Lnet/flixster/android/SearchPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 186
    .end local v0           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v1           #movie:Lnet/flixster/android/model/Movie;
    :cond_2
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 187
    invoke-direct {p0}, Lnet/flixster/android/SearchPage;->addNoResults()V

    goto :goto_1
.end method

.method private shadeNavButtons()V
    .locals 2

    .prologue
    .line 308
    sget v0, Lnet/flixster/android/SearchPage;->sSearchType:I

    if-nez v0, :cond_1

    .line 309
    iget-object v0, p0, Lnet/flixster/android/SearchPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    const v1, 0x7f070258

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    sget v0, Lnet/flixster/android/SearchPage;->sSearchType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 311
    iget-object v0, p0, Lnet/flixster/android/SearchPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    const v1, 0x7f070259

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    goto :goto_0
.end method

.method private showSoftKeyboard()V
    .locals 2

    .prologue
    .line 298
    invoke-virtual {p0}, Lnet/flixster/android/SearchPage;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 299
    return-void
.end method

.method private updateSearchFieldHint()V
    .locals 2

    .prologue
    .line 316
    iget-object v1, p0, Lnet/flixster/android/SearchPage;->searchField:Landroid/widget/EditText;

    sget v0, Lnet/flixster/android/SearchPage;->sSearchType:I

    if-nez v0, :cond_0

    const v0, 0x7f0c0106

    :goto_0
    invoke-virtual {p0, v0}, Lnet/flixster/android/SearchPage;->getResourceString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 318
    return-void

    .line 317
    :cond_0
    const v0, 0x7f0c0107

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 225
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 232
    :goto_0
    return-void

    .line 227
    :pswitch_0
    const-string v1, "FlxMain"

    const-string v2, "SearchPage.onClick ok then"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 229
    .local v0, editText:Landroid/widget/EditText;
    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/16 v3, 0x42

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    goto :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x7f070101
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .parameter "savedInstanceState"

    .prologue
    .line 57
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lnet/flixster/android/SearchPage;->createActionBar()V

    .line 59
    const v3, 0x7f0c0048

    invoke-virtual {p0, v3}, Lnet/flixster/android/SearchPage;->setActionBarTitle(I)V

    .line 61
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/SearchPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 63
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    const-string v4, "SearchResultsStickyTop"

    invoke-virtual {v3, v4}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 64
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    const-string v4, "SearchResultsStickyBottom"

    invoke-virtual {v3, v4}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 66
    const v3, 0x7f0700f3

    invoke-virtual {p0, v3}, Lnet/flixster/android/SearchPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 67
    .local v1, mNavbarHolder:Landroid/widget/LinearLayout;
    new-instance v3, Lcom/flixster/android/view/SubNavBar;

    invoke-direct {v3, p0}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lnet/flixster/android/SearchPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    .line 68
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget-object v4, p0, Lnet/flixster/android/SearchPage;->searchTypeListener:Landroid/view/View$OnClickListener;

    const v5, 0x7f0c0104

    const v6, 0x7f0c0105

    invoke-virtual {v3, v4, v5, v6}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;II)V

    .line 69
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    const v4, 0x7f070258

    invoke-virtual {v3, v4}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 70
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 72
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 73
    const v4, 0x7f03004b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 72
    check-cast v2, Landroid/widget/RelativeLayout;

    .line 74
    .local v2, searchLayout:Landroid/widget/RelativeLayout;
    const v3, 0x7f070102

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lnet/flixster/android/SearchPage;->searchField:Landroid/widget/EditText;

    .line 75
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->searchField:Landroid/widget/EditText;

    invoke-virtual {v3, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 76
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->searchField:Landroid/widget/EditText;

    new-instance v4, Lnet/flixster/android/SearchPage$3;

    invoke-direct {v4, p0}, Lnet/flixster/android/SearchPage$3;-><init>(Lnet/flixster/android/SearchPage;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 84
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->searchField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 85
    invoke-direct {p0}, Lnet/flixster/android/SearchPage;->updateSearchFieldHint()V

    .line 86
    const v3, 0x7f070101

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 87
    .local v0, button:Landroid/widget/ImageButton;
    iget-object v3, p0, Lnet/flixster/android/SearchPage;->searchField:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 88
    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 91
    return-void
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 322
    const/4 v0, 0x1

    return v0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    const/4 v2, 0x0

    .line 235
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 236
    const-string v0, "FlxMain"

    const-string v1, "SearchPage.onEditorAction ok (from action)"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p1

    .line 237
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;

    .line 239
    invoke-direct {p0, p1}, Lnet/flixster/android/SearchPage;->hideSoftKeyboard(Landroid/view/View;)V

    .line 242
    const-string v0, "FLX"

    iget-object v1, p0, Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    const-string v0, "REINDEER FLOTILLA"

    iget-object v1, p0, Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    const-string v0, "UUDDLRLRBA"

    iget-object v1, p0, Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setAdminState(I)V

    .line 246
    const v0, 0x7f0c01bb

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 250
    :cond_1
    const-string v0, "DGNS"

    iget-object v1, p0, Lnet/flixster/android/SearchPage;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->enableDiagnosticMode()V

    .line 252
    const v0, 0x7f0c01bc

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 255
    :cond_2
    sget v0, Lnet/flixster/android/SearchPage;->sSearchType:I

    packed-switch v0, :pswitch_data_0

    .line 263
    :goto_0
    const-wide/16 v0, 0xa

    invoke-direct {p0, v0, v1}, Lnet/flixster/android/SearchPage;->ScheduleLoadItemsTask(J)V

    .line 265
    :cond_3
    return v2

    .line 257
    :pswitch_0
    sget-object v0, Lnet/flixster/android/SearchPage;->sMovieResults:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 260
    :pswitch_1
    sget-object v0, Lnet/flixster/android/SearchPage;->sActorResults:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 255
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 95
    invoke-super {p0}, Lnet/flixster/android/LviActivity;->onResume()V

    .line 96
    invoke-direct {p0}, Lnet/flixster/android/SearchPage;->shadeNavButtons()V

    .line 97
    invoke-virtual {p0}, Lnet/flixster/android/SearchPage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/flixster/android/widget/WidgetProvider;->isOriginatedFromWidget(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/search"

    const-string v2, "Search"

    const-string v3, "WidgetEntrance"

    const-string v4, "Search"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lnet/flixster/android/SearchPage;->ScheduleLoadItemsTask(J)V

    .line 101
    return-void
.end method
