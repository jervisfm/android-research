.class Lnet/flixster/android/SettingsPage$1;
.super Ljava/lang/Object;
.source "SettingsPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/SettingsPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SettingsPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/SettingsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SettingsPage$1;->this$0:Lnet/flixster/android/SettingsPage;

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 250
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 280
    :goto_0
    return-void

    .line 252
    :sswitch_0
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getNetflixUserId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 253
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lnet/flixster/android/SettingsPage$1;->this$0:Lnet/flixster/android/SettingsPage;

    const-class v2, Lnet/flixster/android/NetflixAuth;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 254
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lnet/flixster/android/SettingsPage$1;->this$0:Lnet/flixster/android/SettingsPage;

    invoke-virtual {v1, v0}, Lnet/flixster/android/SettingsPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 256
    .end local v0           #intent:Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/SettingsPage$1;->this$0:Lnet/flixster/android/SettingsPage;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lnet/flixster/android/SettingsPage;->showDialog(I)V

    goto :goto_0

    .line 262
    :sswitch_1
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUserId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 263
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lnet/flixster/android/SettingsPage$1;->this$0:Lnet/flixster/android/SettingsPage;

    const-class v2, Lnet/flixster/android/FacebookAuth;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 264
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v1, p0, Lnet/flixster/android/SettingsPage$1;->this$0:Lnet/flixster/android/SettingsPage;

    invoke-virtual {v1, v0}, Lnet/flixster/android/SettingsPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 266
    .end local v0           #intent:Landroid/content/Intent;
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/SettingsPage$1;->this$0:Lnet/flixster/android/SettingsPage;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lnet/flixster/android/SettingsPage;->showDialog(I)V

    goto :goto_0

    .line 272
    :sswitch_2
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUserId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 273
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lnet/flixster/android/SettingsPage$1;->this$0:Lnet/flixster/android/SettingsPage;

    const-class v2, Lnet/flixster/android/FlixsterLoginPage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 274
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v1, p0, Lnet/flixster/android/SettingsPage$1;->this$0:Lnet/flixster/android/SettingsPage;

    invoke-virtual {v1, v0}, Lnet/flixster/android/SettingsPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 276
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2
    iget-object v1, p0, Lnet/flixster/android/SettingsPage$1;->this$0:Lnet/flixster/android/SettingsPage;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lnet/flixster/android/SettingsPage;->showDialog(I)V

    goto :goto_0

    .line 250
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0701d5 -> :sswitch_1
        0x7f0701da -> :sswitch_2
        0x7f0701df -> :sswitch_0
    .end sparse-switch
.end method
