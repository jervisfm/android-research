.class Lnet/flixster/android/SettingsPage$11;
.super Ljava/lang/Object;
.source "SettingsPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/SettingsPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SettingsPage;

.field private final synthetic val$conv:D

.field private final synthetic val$distances:[D

.field private final synthetic val$radiusChoices:[Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lnet/flixster/android/SettingsPage;[DD[Ljava/lang/CharSequence;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SettingsPage$11;->this$0:Lnet/flixster/android/SettingsPage;

    iput-object p2, p0, Lnet/flixster/android/SettingsPage$11;->val$distances:[D

    iput-wide p3, p0, Lnet/flixster/android/SettingsPage$11;->val$conv:D

    iput-object p5, p0, Lnet/flixster/android/SettingsPage$11;->val$radiusChoices:[Ljava/lang/CharSequence;

    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 371
    iget-object v0, p0, Lnet/flixster/android/SettingsPage$11;->val$distances:[D

    aget-wide v0, v0, p2

    iget-wide v2, p0, Lnet/flixster/android/SettingsPage$11;->val$conv:D

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setTheaterDistance(I)V

    .line 372
    iget-object v0, p0, Lnet/flixster/android/SettingsPage$11;->this$0:Lnet/flixster/android/SettingsPage;

    #getter for: Lnet/flixster/android/SettingsPage;->theaterDistanceLabel:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/SettingsPage;->access$9(Lnet/flixster/android/SettingsPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/SettingsPage$11;->val$radiusChoices:[Ljava/lang/CharSequence;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    return-void
.end method
