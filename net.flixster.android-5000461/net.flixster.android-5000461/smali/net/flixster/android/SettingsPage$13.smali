.class Lnet/flixster/android/SettingsPage$13;
.super Ljava/lang/Object;
.source "SettingsPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/SettingsPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SettingsPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/SettingsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SettingsPage$13;->this$0:Lnet/flixster/android/SettingsPage;

    .line 389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 391
    if-eqz p2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->setCaptionsEnabled(Z)V

    .line 392
    iget-object v2, p0, Lnet/flixster/android/SettingsPage$13;->this$0:Lnet/flixster/android/SettingsPage;

    const v3, 0x7f070247

    invoke-virtual {v2, v3}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 393
    .local v1, captionsEnabled:Landroid/widget/TextView;
    iget-object v2, p0, Lnet/flixster/android/SettingsPage$13;->this$0:Lnet/flixster/android/SettingsPage;

    invoke-virtual {v2}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 394
    .local v0, arrayOptions:[Ljava/lang/String;
    aget-object v2, v0, p2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    return-void

    .line 391
    .end local v0           #arrayOptions:[Ljava/lang/String;
    .end local v1           #captionsEnabled:Landroid/widget/TextView;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
