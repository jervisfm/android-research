.class Lnet/flixster/android/SettingsPage$14;
.super Ljava/lang/Object;
.source "SettingsPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/SettingsPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SettingsPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/SettingsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SettingsPage$14;->this$0:Lnet/flixster/android/SettingsPage;

    .line 400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 402
    invoke-static {p2}, Lnet/flixster/android/FlixsterApplication;->setMovieRatingType(I)V

    .line 403
    iget-object v1, p0, Lnet/flixster/android/SettingsPage$14;->this$0:Lnet/flixster/android/SettingsPage;

    invoke-virtual {v1}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 404
    .local v0, arrayOptions:[Ljava/lang/String;
    iget-object v1, p0, Lnet/flixster/android/SettingsPage$14;->this$0:Lnet/flixster/android/SettingsPage;

    #getter for: Lnet/flixster/android/SettingsPage;->movieRatingsLabel:Landroid/widget/TextView;
    invoke-static {v1}, Lnet/flixster/android/SettingsPage;->access$11(Lnet/flixster/android/SettingsPage;)Landroid/widget/TextView;

    move-result-object v1

    aget-object v2, v0, p2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    return-void
.end method
