.class Lnet/flixster/android/SettingsPage$18;
.super Ljava/lang/Object;
.source "SettingsPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/SettingsPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SettingsPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/SettingsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SettingsPage$18;->this$0:Lnet/flixster/android/SettingsPage;

    .line 456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .parameter "dialog"
    .parameter "whichButton"

    .prologue
    const/4 v0, 0x0

    .line 458
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setNetflixOAuthToken(Ljava/lang/String;)V

    .line 459
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setNetflixOAuthTokenSecret(Ljava/lang/String;)V

    .line 460
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setNetflixUserId(Ljava/lang/String;)V

    .line 461
    iget-object v0, p0, Lnet/flixster/android/SettingsPage$18;->this$0:Lnet/flixster/android/SettingsPage;

    #calls: Lnet/flixster/android/SettingsPage;->initializeNetflixViews()V
    invoke-static {v0}, Lnet/flixster/android/SettingsPage;->access$13(Lnet/flixster/android/SettingsPage;)V

    .line 462
    return-void
.end method
