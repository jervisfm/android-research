.class Lnet/flixster/android/SettingsPage$19;
.super Ljava/util/TimerTask;
.source "SettingsPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/SettingsPage;->ScheduleLocationChange(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SettingsPage;

.field private final synthetic val$which:I


# direct methods
.method constructor <init>(Lnet/flixster/android/SettingsPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SettingsPage$19;->this$0:Lnet/flixster/android/SettingsPage;

    iput p2, p0, Lnet/flixster/android/SettingsPage$19;->val$which:I

    .line 471
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    .line 473
    const-string v4, "FlxMain"

    const-string v5, "SettingsPage.ScheduleLocationChange"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    iget v4, p0, Lnet/flixster/android/SettingsPage$19;->val$which:I

    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setLocationPolicy(I)V

    .line 475
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SettingsPage.ScheduleLocationChange post which:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lnet/flixster/android/SettingsPage$19;->val$which:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    iget v4, p0, Lnet/flixster/android/SettingsPage$19;->val$which:I

    packed-switch v4, :pswitch_data_0

    .line 502
    :cond_0
    :goto_0
    const-string v4, "FlxMain"

    const-string v5, "SettingsPage.ScheduleLocationChange call SetupLocationServices"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v4, p0, Lnet/flixster/android/SettingsPage$19;->this$0:Lnet/flixster/android/SettingsPage;

    #getter for: Lnet/flixster/android/SettingsPage;->mSetupLocationHandler:Landroid/os/Handler;
    invoke-static {v4}, Lnet/flixster/android/SettingsPage;->access$14(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 505
    const-string v4, "FlxMain"

    const-string v5, "SettingsPage.ScheduleLocationChange ta da..."

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    iget-object v4, p0, Lnet/flixster/android/SettingsPage$19;->this$0:Lnet/flixster/android/SettingsPage;

    #getter for: Lnet/flixster/android/SettingsPage;->mLocationDialogRemove:Landroid/os/Handler;
    invoke-static {v4}, Lnet/flixster/android/SettingsPage;->access$15(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 507
    iget-object v4, p0, Lnet/flixster/android/SettingsPage$19;->this$0:Lnet/flixster/android/SettingsPage;

    #getter for: Lnet/flixster/android/SettingsPage;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v4}, Lnet/flixster/android/SettingsPage;->access$7(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 508
    return-void

    .line 478
    :pswitch_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 479
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v0

    .line 480
    .local v0, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v2

    .line 481
    .local v2, longitude:D
    cmpl-double v4, v0, v8

    if-eqz v4, :cond_1

    cmpl-double v4, v2, v8

    if-eqz v4, :cond_1

    .line 482
    invoke-static {v0, v1}, Lnet/flixster/android/FlixsterApplication;->setUserLatitude(D)V

    .line 483
    invoke-static {v2, v3}, Lnet/flixster/android/FlixsterApplication;->setUserLongitude(D)V

    .line 484
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentZip()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setUserZip(Ljava/lang/String;)V

    .line 485
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentCity()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setUserCity(Ljava/lang/String;)V

    .line 486
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLocationDisplay()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setUserLocation(Ljava/lang/String;)V

    .line 488
    :cond_1
    invoke-static {v7}, Lnet/flixster/android/FlixsterApplication;->setUseLocationServiceFlag(Z)V

    goto :goto_0

    .line 492
    .end local v0           #latitude:D
    .end local v2           #longitude:D
    :pswitch_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v4

    if-nez v4, :cond_0

    .line 493
    invoke-static {v10}, Lnet/flixster/android/FlixsterApplication;->setUseLocationServiceFlag(Z)V

    goto :goto_0

    .line 497
    :pswitch_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v4

    if-nez v4, :cond_0

    .line 498
    invoke-static {v10}, Lnet/flixster/android/FlixsterApplication;->setUseLocationServiceFlag(Z)V

    goto :goto_0

    .line 476
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
