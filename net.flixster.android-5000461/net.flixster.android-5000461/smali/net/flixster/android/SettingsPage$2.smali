.class Lnet/flixster/android/SettingsPage$2;
.super Landroid/os/Handler;
.source "SettingsPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/SettingsPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SettingsPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/SettingsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SettingsPage$2;->this$0:Lnet/flixster/android/SettingsPage;

    .line 545
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 547
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 548
    .local v0, currentLocation:Ljava/lang/String;
    iget-object v2, p0, Lnet/flixster/android/SettingsPage$2;->this$0:Lnet/flixster/android/SettingsPage;

    const v3, 0x7f07023b

    invoke-virtual {v2, v3}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 549
    .local v1, locationText:Landroid/widget/TextView;
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 550
    iget-object v2, p0, Lnet/flixster/android/SettingsPage$2;->this$0:Lnet/flixster/android/SettingsPage;

    invoke-virtual {v2}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00db

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 551
    const/high16 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 555
    :goto_0
    return-void

    .line 553
    :cond_0
    const/high16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
