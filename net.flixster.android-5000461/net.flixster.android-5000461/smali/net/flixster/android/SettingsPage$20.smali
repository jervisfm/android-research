.class Lnet/flixster/android/SettingsPage$20;
.super Ljava/util/TimerTask;
.source "SettingsPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/SettingsPage;->ScheduleCacheChange(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SettingsPage;

.field private final synthetic val$which:I


# direct methods
.method constructor <init>(Lnet/flixster/android/SettingsPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SettingsPage$20;->this$0:Lnet/flixster/android/SettingsPage;

    iput p2, p0, Lnet/flixster/android/SettingsPage$20;->val$which:I

    .line 518
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 520
    iget v0, p0, Lnet/flixster/android/SettingsPage$20;->val$which:I

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setCachePolicy(I)V

    .line 521
    invoke-static {}, Lnet/flixster/android/FlixsterCacheManager;->SetCacheLimit()V

    .line 522
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lnet/flixster/android/FlixsterCacheManager;->TrimCache(J)V

    .line 523
    invoke-static {}, Lnet/flixster/android/SettingsPage;->access$6()Lnet/flixster/android/SettingsPage;

    move-result-object v0

    #getter for: Lnet/flixster/android/SettingsPage;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/SettingsPage;->access$7(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 524
    iget-object v0, p0, Lnet/flixster/android/SettingsPage$20;->this$0:Lnet/flixster/android/SettingsPage;

    #getter for: Lnet/flixster/android/SettingsPage;->mCacheDialogRemove:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/SettingsPage;->access$16(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 525
    return-void
.end method
