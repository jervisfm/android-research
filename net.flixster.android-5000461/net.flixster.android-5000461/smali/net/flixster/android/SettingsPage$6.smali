.class Lnet/flixster/android/SettingsPage$6;
.super Landroid/os/Handler;
.source "SettingsPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/SettingsPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SettingsPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/SettingsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SettingsPage$6;->this$0:Lnet/flixster/android/SettingsPage;

    .line 584
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 588
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SettingsPage.mCacheDialogRemove mCachePolicyDialog:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/flixster/android/SettingsPage$6;->this$0:Lnet/flixster/android/SettingsPage;

    #getter for: Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lnet/flixster/android/SettingsPage;->access$3(Lnet/flixster/android/SettingsPage;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    iget-object v0, p0, Lnet/flixster/android/SettingsPage$6;->this$0:Lnet/flixster/android/SettingsPage;

    #getter for: Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lnet/flixster/android/SettingsPage;->access$3(Lnet/flixster/android/SettingsPage;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/SettingsPage$6;->this$0:Lnet/flixster/android/SettingsPage;

    #getter for: Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lnet/flixster/android/SettingsPage;->access$3(Lnet/flixster/android/SettingsPage;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lnet/flixster/android/SettingsPage$6;->this$0:Lnet/flixster/android/SettingsPage;

    const/4 v1, 0x0

    #setter for: Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lnet/flixster/android/SettingsPage;->access$4(Lnet/flixster/android/SettingsPage;Landroid/app/ProgressDialog;)V

    .line 591
    iget-object v0, p0, Lnet/flixster/android/SettingsPage$6;->this$0:Lnet/flixster/android/SettingsPage;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lnet/flixster/android/SettingsPage;->removeDialog(I)V

    .line 593
    :cond_0
    return-void
.end method
