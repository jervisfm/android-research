.class Lnet/flixster/android/SettingsPage$8;
.super Ljava/lang/Object;
.source "SettingsPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/SettingsPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/SettingsPage;

.field private final synthetic val$zipEntryView:Landroid/view/View;


# direct methods
.method constructor <init>(Lnet/flixster/android/SettingsPage;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/SettingsPage$8;->this$0:Lnet/flixster/android/SettingsPage;

    iput-object p2, p0, Lnet/flixster/android/SettingsPage$8;->val$zipEntryView:Landroid/view/View;

    .line 292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter "dialog"
    .parameter "whichButton"

    .prologue
    const/4 v4, 0x0

    .line 295
    iget-object v2, p0, Lnet/flixster/android/SettingsPage$8;->val$zipEntryView:Landroid/view/View;

    const v3, 0x7f0702f9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 296
    .local v1, et:Landroid/widget/EditText;
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/util/PostalCodeUtils;->parseZipcodeForShowtimes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, cleanZip:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 298
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setUserLocation(Ljava/lang/String;)V

    .line 299
    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->setUseLocationServiceFlag(Z)V

    .line 300
    invoke-static {}, Lnet/flixster/android/SettingsPage;->access$6()Lnet/flixster/android/SettingsPage;

    move-result-object v2

    #getter for: Lnet/flixster/android/SettingsPage;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/SettingsPage;->access$7(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 304
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/SettingsPage$8;->this$0:Lnet/flixster/android/SettingsPage;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lnet/flixster/android/SettingsPage;->showDialog(I)V

    goto :goto_0
.end method
