.class public Lnet/flixster/android/SettingsPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "SettingsPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final ACTIVITY_RESULT_LOCATION:I = 0x0

.field private static final DIALOG_CACHEPOLICY:I = 0x7

.field private static final DIALOG_CACHE_POLICY_CHANGE:I = 0x9

.field private static final DIALOG_CAPTIONSENABLED:I = 0xd

.field private static final DIALOG_DISTANCE:I = 0x2

.field private static final DIALOG_LOCATION:I = 0x1

.field private static final DIALOG_LOCATION_POLICY_CHANGE:I = 0x8

.field private static final DIALOG_LOGOUT_FACEBOOK:I = 0xa

.field private static final DIALOG_LOGOUT_FLIXSTER:I = 0xb

.field private static final DIALOG_LOGOUT_NETFLIX:I = 0xc

.field private static final DIALOG_MOVIERATINGS:I = 0x3

.field private static final DIALOG_ZIP:I = 0x0

.field private static final DIALOG_ZIP_ERROR:I = 0x5

.field public static final IS_CURRENT_LOCATION_DISABLED:Z = false

.field public static final LISTITEM_FACEBOOK:I = 0x4

.field public static final LISTITEM_FAQ:I = 0x5

.field public static final LISTITEM_FEEDBACK:I = 0x6

.field public static final LISTITEM_FOOTER:I = 0x7

.field public static final LISTITEM_HEADER:I = 0x0

.field public static final LISTITEM_LOCATION:I = 0x1

.field public static final LISTITEM_MOVIERATINGS:I = 0x3

.field public static final LISTITEM_TRAILERZOOM:I = 0x2

.field private static final SEARCH_RADIUS_IMPERIAL:[D

.field private static final SEARCH_RADIUS_METRIC:[D

.field private static mSettingsPage:Lnet/flixster/android/SettingsPage;


# instance fields
.field private getCurrentLocationHandler:Landroid/os/Handler;

.field private final homepageOnClickListener:Landroid/view/View$OnClickListener;

.field mAsyncRunner:Lcom/facebook/android/AsyncFacebookRunner;

.field private mCacheDialogRemove:Landroid/os/Handler;

.field private mCachePolicyDialog:Landroid/app/ProgressDialog;

.field private mLocationDialogRemove:Landroid/os/Handler;

.field private mLocationPolicyDialog:Landroid/app/ProgressDialog;

.field private mRefreshHandler:Landroid/os/Handler;

.field private mSetupLocationHandler:Landroid/os/Handler;

.field private mTimer:Ljava/util/Timer;

.field private movieRatingsLabel:Landroid/widget/TextView;

.field private theaterDistanceLabel:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const/16 v0, 0x9

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, Lnet/flixster/android/SettingsPage;->SEARCH_RADIUS_IMPERIAL:[D

    .line 67
    const/16 v0, 0xa

    new-array v0, v0, [D

    fill-array-data v0, :array_1

    sput-object v0, Lnet/flixster/android/SettingsPage;->SEARCH_RADIUS_METRIC:[D

    .line 41
    return-void

    .line 66
    nop

    :array_0
    .array-data 0x8
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x8t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x14t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x24t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x39t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x80t 0x41t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x49t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0xc0t 0x52t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x59t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x69t 0x40t
    .end array-data

    .line 67
    :array_1
    .array-data 0x8
        0xd7t 0xa3t 0x70t 0x3dt 0xat 0xd7t 0xf3t 0x3ft
        0xe1t 0x7at 0x14t 0xaet 0x47t 0xe1t 0x8t 0x40t
        0xe1t 0x7at 0x14t 0xaet 0x47t 0xe1t 0x18t 0x40t
        0xa4t 0x70t 0x3dt 0xat 0xd7t 0xa3t 0x22t 0x40t
        0x5ct 0x8ft 0xc2t 0xf5t 0x28t 0xdct 0x38t 0x40t
        0xd7t 0xa3t 0x70t 0x3dt 0xat 0x17t 0x41t 0x40t
        0x5ct 0x8ft 0xc2t 0xf5t 0x28t 0xdct 0x48t 0x40t
        0x33t 0x33t 0x33t 0x33t 0x33t 0x13t 0x4ft 0x40t
        0x1ft 0x85t 0xebt 0x51t 0xb8t 0x4et 0x57t 0x40t
        0x66t 0x66t 0x66t 0x66t 0x66t 0x4et 0x67t 0x40t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 245
    new-instance v0, Lnet/flixster/android/SettingsPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/SettingsPage$1;-><init>(Lnet/flixster/android/SettingsPage;)V

    iput-object v0, p0, Lnet/flixster/android/SettingsPage;->homepageOnClickListener:Landroid/view/View$OnClickListener;

    .line 545
    new-instance v0, Lnet/flixster/android/SettingsPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/SettingsPage$2;-><init>(Lnet/flixster/android/SettingsPage;)V

    iput-object v0, p0, Lnet/flixster/android/SettingsPage;->getCurrentLocationHandler:Landroid/os/Handler;

    .line 558
    new-instance v0, Lnet/flixster/android/SettingsPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/SettingsPage$3;-><init>(Lnet/flixster/android/SettingsPage;)V

    iput-object v0, p0, Lnet/flixster/android/SettingsPage;->mRefreshHandler:Landroid/os/Handler;

    .line 565
    new-instance v0, Lnet/flixster/android/SettingsPage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/SettingsPage$4;-><init>(Lnet/flixster/android/SettingsPage;)V

    iput-object v0, p0, Lnet/flixster/android/SettingsPage;->mSetupLocationHandler:Landroid/os/Handler;

    .line 572
    new-instance v0, Lnet/flixster/android/SettingsPage$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/SettingsPage$5;-><init>(Lnet/flixster/android/SettingsPage;)V

    iput-object v0, p0, Lnet/flixster/android/SettingsPage;->mLocationDialogRemove:Landroid/os/Handler;

    .line 584
    new-instance v0, Lnet/flixster/android/SettingsPage$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/SettingsPage$6;-><init>(Lnet/flixster/android/SettingsPage;)V

    iput-object v0, p0, Lnet/flixster/android/SettingsPage;->mCacheDialogRemove:Landroid/os/Handler;

    .line 41
    return-void
.end method

.method private ScheduleCacheChange(I)V
    .locals 4
    .parameter "which"

    .prologue
    .line 518
    new-instance v0, Lnet/flixster/android/SettingsPage$20;

    invoke-direct {v0, p0, p1}, Lnet/flixster/android/SettingsPage$20;-><init>(Lnet/flixster/android/SettingsPage;I)V

    .line 527
    .local v0, locationChangeTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/SettingsPage;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 528
    iget-object v1, p0, Lnet/flixster/android/SettingsPage;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 530
    :cond_0
    return-void
.end method

.method private ScheduleLocationChange(I)V
    .locals 4
    .parameter "which"

    .prologue
    .line 471
    new-instance v0, Lnet/flixster/android/SettingsPage$19;

    invoke-direct {v0, p0, p1}, Lnet/flixster/android/SettingsPage$19;-><init>(Lnet/flixster/android/SettingsPage;I)V

    .line 511
    .local v0, locationChangeTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/SettingsPage;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 512
    iget-object v1, p0, Lnet/flixster/android/SettingsPage;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 515
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/SettingsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 105
    invoke-direct {p0}, Lnet/flixster/android/SettingsPage;->populatePage()V

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/SettingsPage;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mLocationPolicyDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$10(Lnet/flixster/android/SettingsPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 517
    invoke-direct {p0, p1}, Lnet/flixster/android/SettingsPage;->ScheduleCacheChange(I)V

    return-void
.end method

.method static synthetic access$11(Lnet/flixster/android/SettingsPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->movieRatingsLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$12(Lnet/flixster/android/SettingsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 209
    invoke-direct {p0}, Lnet/flixster/android/SettingsPage;->initializeSocialViews()V

    return-void
.end method

.method static synthetic access$13(Lnet/flixster/android/SettingsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 190
    invoke-direct {p0}, Lnet/flixster/android/SettingsPage;->initializeNetflixViews()V

    return-void
.end method

.method static synthetic access$14(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 565
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mSetupLocationHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$15(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 572
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mLocationDialogRemove:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$16(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 584
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mCacheDialogRemove:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/SettingsPage;Landroid/app/ProgressDialog;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, Lnet/flixster/android/SettingsPage;->mLocationPolicyDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/SettingsPage;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/SettingsPage;Landroid/app/ProgressDialog;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 80
    iput-object p1, p0, Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$5(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 545
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->getCurrentLocationHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6()Lnet/flixster/android/SettingsPage;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lnet/flixster/android/SettingsPage;->mSettingsPage:Lnet/flixster/android/SettingsPage;

    return-object v0
.end method

.method static synthetic access$7(Lnet/flixster/android/SettingsPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 558
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mRefreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/SettingsPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 470
    invoke-direct {p0, p1}, Lnet/flixster/android/SettingsPage;->ScheduleLocationChange(I)V

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/SettingsPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->theaterDistanceLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method private initializeNetflixViews()V
    .locals 5

    .prologue
    .line 191
    const v3, 0x7f0701df

    invoke-virtual {p0, v3}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 192
    .local v0, netflixLogin:Landroid/widget/RelativeLayout;
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isUsLocale()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isCanadaLocale()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 193
    :cond_0
    iget-object v3, p0, Lnet/flixster/android/SettingsPage;->homepageOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 195
    const v3, 0x7f0700c9

    invoke-virtual {p0, v3}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 196
    .local v2, netflixLoginMessage:Landroid/widget/TextView;
    const v3, 0x7f0700c8

    invoke-virtual {p0, v3}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 197
    .local v1, netflixLoginButton:Landroid/widget/ImageView;
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getNetflixUserId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 198
    invoke-virtual {p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00d0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    const v3, 0x7f02013a

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 207
    .end local v1           #netflixLoginButton:Landroid/widget/ImageView;
    .end local v2           #netflixLoginMessage:Landroid/widget/TextView;
    :goto_0
    return-void

    .line 201
    .restart local v1       #netflixLoginButton:Landroid/widget/ImageView;
    .restart local v2       #netflixLoginMessage:Landroid/widget/TextView;
    :cond_1
    invoke-virtual {p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00cf

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    const v3, 0x7f020137

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 205
    .end local v1           #netflixLoginButton:Landroid/widget/ImageView;
    .end local v2           #netflixLoginMessage:Landroid/widget/TextView;
    :cond_2
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private initializeSocialViews()V
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 210
    const v9, 0x7f0701d5

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 211
    .local v2, facebookLayout:Landroid/widget/RelativeLayout;
    iget-object v9, p0, Lnet/flixster/android/SettingsPage;->homepageOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    invoke-virtual {v2, v12}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 213
    const v9, 0x7f0701d8

    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 214
    .local v1, facebookIcon:Landroid/widget/ImageView;
    const v9, 0x7f0701d9

    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 216
    .local v3, facebookStatus:Landroid/widget/TextView;
    const v9, 0x7f0701da

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 217
    .local v5, flixsterLayout:Landroid/widget/RelativeLayout;
    iget-object v9, p0, Lnet/flixster/android/SettingsPage;->homepageOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    invoke-virtual {v5, v12}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 219
    const v9, 0x7f0701dd

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 220
    .local v4, flixsterIcon:Landroid/widget/ImageView;
    const v9, 0x7f0701de

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 222
    .local v6, flixsterStatus:Landroid/widget/TextView;
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v8

    .line 223
    .local v8, user:Lnet/flixster/android/model/User;
    if-nez v8, :cond_1

    .line 224
    invoke-virtual {p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c00cf

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, connectAccount:Ljava/lang/String;
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    const v9, 0x7f02008a

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 227
    invoke-virtual {v2, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 228
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    const v9, 0x7f020137

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 230
    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 243
    .end local v0           #connectAccount:Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    invoke-virtual {p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c0131

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 233
    .local v7, text:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/flixster/android/data/AccountManager;->isPlatformFacebook()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 234
    new-array v9, v12, [Ljava/lang/Object;

    iget-object v10, v8, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    aput-object v10, v9, v11

    invoke-static {v7, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    const v9, 0x7f020090

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 236
    invoke-virtual {v5, v13}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 237
    :cond_2
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/flixster/android/data/AccountManager;->isPlatformFlixster()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 238
    new-array v9, v12, [Ljava/lang/Object;

    iget-object v10, v8, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    aput-object v10, v9, v11

    invoke-static {v7, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    const v9, 0x7f02013a

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 240
    invoke-virtual {v2, v13}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private populatePage()V
    .locals 15

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 109
    const v9, 0x7f070237

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 110
    .local v0, adminButton:Landroid/widget/Button;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAdminState()I

    move-result v9

    if-nez v9, :cond_0

    .line 111
    const/16 v9, 0x8

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 117
    :goto_0
    invoke-direct {p0}, Lnet/flixster/android/SettingsPage;->initializeNetflixViews()V

    .line 118
    invoke-direct {p0}, Lnet/flixster/android/SettingsPage;->initializeSocialViews()V

    .line 120
    const v9, 0x7f07023b

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 121
    .local v7, locationText:Landroid/widget/TextView;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 122
    const-string v9, ""

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v9

    new-instance v12, Lnet/flixster/android/SettingsPage$7;

    invoke-direct {v12, p0}, Lnet/flixster/android/SettingsPage$7;-><init>(Lnet/flixster/android/SettingsPage;)V

    invoke-virtual {v9, v12}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 138
    :goto_1
    const v9, 0x7f07023a

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    .line 139
    .local v8, rl:Landroid/widget/RelativeLayout;
    invoke-virtual {v8, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    invoke-virtual {v8, v10}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 142
    const v9, 0x7f07023c

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 146
    .local v5, currentLocationLayout:Landroid/widget/RelativeLayout;
    const v9, 0x7f07023e

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 147
    .local v4, currentLocationLabel:Landroid/widget/TextView;
    invoke-virtual {p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v12, 0x7f0e0001

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 148
    .local v1, arrayOptions:[Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getLocationPolicy()I

    move-result v9

    aget-object v9, v1, v9

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {v5, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    invoke-virtual {v5, v10}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 153
    const v9, 0x7f07023f

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    .line 154
    .local v6, distanceView:Landroid/widget/RelativeLayout;
    invoke-virtual {v6, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    invoke-virtual {v6, v10}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 156
    const v9, 0x7f070241

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lnet/flixster/android/SettingsPage;->theaterDistanceLabel:Landroid/widget/TextView;

    .line 157
    iget-object v9, p0, Lnet/flixster/android/SettingsPage;->theaterDistanceLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0c00e5

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-array v13, v10, [Ljava/lang/Object;

    .line 158
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getTheaterDistance()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v13, v11

    .line 157
    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    const v9, 0x7f070243

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lnet/flixster/android/SettingsPage;->movieRatingsLabel:Landroid/widget/TextView;

    .line 161
    invoke-virtual {p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v12, 0x7f0e

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 162
    iget-object v9, p0, Lnet/flixster/android/SettingsPage;->movieRatingsLabel:Landroid/widget/TextView;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v12

    aget-object v12, v1, v12

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    const v9, 0x7f070242

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8           #rl:Landroid/widget/RelativeLayout;
    check-cast v8, Landroid/widget/RelativeLayout;

    .line 164
    .restart local v8       #rl:Landroid/widget/RelativeLayout;
    invoke-virtual {v8, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    invoke-virtual {v8, v10}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 168
    const v9, 0x7f070245

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 169
    .local v2, cachePolicy:Landroid/widget/TextView;
    const v9, 0x7f070244

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8           #rl:Landroid/widget/RelativeLayout;
    check-cast v8, Landroid/widget/RelativeLayout;

    .line 170
    .restart local v8       #rl:Landroid/widget/RelativeLayout;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v9

    const-string v12, "mounted"

    invoke-virtual {v9, v12}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 171
    invoke-virtual {p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v12, 0x7f0e0002

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCachePolicy()I

    move-result v9

    aget-object v9, v1, v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    invoke-virtual {v8, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    invoke-virtual {v8, v10}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 182
    :goto_2
    const v9, 0x7f070247

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 183
    .local v3, captionsEnabled:Landroid/widget/TextView;
    const v9, 0x7f070246

    invoke-virtual {p0, v9}, Lnet/flixster/android/SettingsPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8           #rl:Landroid/widget/RelativeLayout;
    check-cast v8, Landroid/widget/RelativeLayout;

    .line 184
    .restart local v8       #rl:Landroid/widget/RelativeLayout;
    invoke-virtual {p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v12, 0x7f0e0003

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 185
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCaptionsEnabled()Z

    move-result v9

    if-eqz v9, :cond_4

    move v9, v10

    :goto_3
    aget-object v9, v1, v9

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    invoke-virtual {v8, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    invoke-virtual {v8, v10}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 188
    return-void

    .line 113
    .end local v1           #arrayOptions:[Ljava/lang/String;
    .end local v2           #cachePolicy:Landroid/widget/TextView;
    .end local v3           #captionsEnabled:Landroid/widget/TextView;
    .end local v4           #currentLocationLabel:Landroid/widget/TextView;
    .end local v5           #currentLocationLayout:Landroid/widget/RelativeLayout;
    .end local v6           #distanceView:Landroid/widget/RelativeLayout;
    .end local v7           #locationText:Landroid/widget/TextView;
    .end local v8           #rl:Landroid/widget/RelativeLayout;
    :cond_0
    invoke-virtual {v0, v11}, Landroid/widget/Button;->setVisibility(I)V

    .line 114
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 130
    .restart local v7       #locationText:Landroid/widget/TextView;
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    const-string v9, ""

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 131
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    const/high16 v9, -0x100

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 134
    :cond_2
    const v9, 0x7f0c00db

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(I)V

    .line 135
    const/high16 v9, -0x1

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 176
    .restart local v1       #arrayOptions:[Ljava/lang/String;
    .restart local v2       #cachePolicy:Landroid/widget/TextView;
    .restart local v4       #currentLocationLabel:Landroid/widget/TextView;
    .restart local v5       #currentLocationLayout:Landroid/widget/RelativeLayout;
    .restart local v6       #distanceView:Landroid/widget/RelativeLayout;
    .restart local v8       #rl:Landroid/widget/RelativeLayout;
    :cond_3
    const-string v9, "No external storage detected."

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    invoke-virtual {v8, v11}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    goto :goto_2

    .restart local v3       #captionsEnabled:Landroid/widget/TextView;
    :cond_4
    move v9, v11

    .line 185
    goto :goto_3
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "view"

    .prologue
    .line 598
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 623
    :goto_0
    :pswitch_0
    return-void

    .line 600
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    .line 601
    invoke-static {}, Lcom/flixster/android/utils/GoogleApiDetector;->instance()Lcom/flixster/android/utils/GoogleApiDetector;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/GoogleApiDetector;->isVanillaAndroid()Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v1, Lnet/flixster/android/LocationPage;

    .line 600
    :goto_1
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 602
    .local v0, settingsLocationIntent:Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lnet/flixster/android/SettingsPage;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 601
    .end local v0           #settingsLocationIntent:Landroid/content/Intent;
    :cond_0
    const-class v1, Lnet/flixster/android/LocationMapPage;

    goto :goto_1

    .line 605
    :pswitch_2
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lnet/flixster/android/SettingsPage;->showDialog(I)V

    goto :goto_0

    .line 608
    :pswitch_3
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lnet/flixster/android/SettingsPage;->showDialog(I)V

    goto :goto_0

    .line 611
    :pswitch_4
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lnet/flixster/android/SettingsPage;->showDialog(I)V

    goto :goto_0

    .line 614
    :pswitch_5
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lnet/flixster/android/SettingsPage;->showDialog(I)V

    goto :goto_0

    .line 617
    :pswitch_6
    const/16 v1, 0xd

    invoke-virtual {p0, v1}, Lnet/flixster/android/SettingsPage;->showDialog(I)V

    goto :goto_0

    .line 620
    :pswitch_7
    invoke-static {p0}, Lnet/flixster/android/Starter;->launchAdminPage(Landroid/content/Context;)V

    goto :goto_0

    .line 598
    nop

    :pswitch_data_0
    .packed-switch 0x7f070237
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    const v0, 0x7f03007a

    invoke-virtual {p0, v0}, Lnet/flixster/android/SettingsPage;->setContentView(I)V

    .line 86
    invoke-virtual {p0}, Lnet/flixster/android/SettingsPage;->createActionBar()V

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Settings - Version "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lnet/flixster/android/SettingsPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 89
    const-string v0, "FlxMain"

    const-string v1, "SettingsPage.onCreate"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    sput-object p0, Lnet/flixster/android/SettingsPage;->mSettingsPage:Lnet/flixster/android/SettingsPage;

    .line 91
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 24
    .parameter "d"

    .prologue
    .line 286
    packed-switch p1, :pswitch_data_0

    .line 467
    :pswitch_0
    const/4 v3, 0x0

    :goto_0
    return-object v3

    .line 288
    :pswitch_1
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v13

    .line 289
    .local v13, factory:Landroid/view/LayoutInflater;
    const v3, 0x7f030095

    const/4 v4, 0x0

    invoke-virtual {v13, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v20

    .line 290
    .local v20, zipEntryView:Landroid/view/View;
    const/16 v3, 0x12c

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/view/View;->setMinimumWidth(I)V

    .line 291
    new-instance v3, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 292
    const v4, 0x7f0c0049

    new-instance v21, Lnet/flixster/android/SettingsPage$8;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/SettingsPage$8;-><init>(Lnet/flixster/android/SettingsPage;Landroid/view/View;)V

    move-object/from16 v0, v21

    invoke-virtual {v3, v4, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 305
    const v4, 0x7f0c004a

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v3, v4, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lnet/flixster/android/SettingsPage$9;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v4, v0, v1}, Lnet/flixster/android/SettingsPage$9;-><init>(Lnet/flixster/android/SettingsPage;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 321
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    .line 324
    .end local v13           #factory:Landroid/view/LayoutInflater;
    .end local v20           #zipEntryView:Landroid/view/View;
    :pswitch_2
    new-instance v3, Landroid/app/ProgressDialog;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lnet/flixster/android/SettingsPage;->mLocationPolicyDialog:Landroid/app/ProgressDialog;

    .line 325
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/SettingsPage;->mLocationPolicyDialog:Landroid/app/ProgressDialog;

    const-string v4, "Changing location policy...."

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 326
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/SettingsPage;->mLocationPolicyDialog:Landroid/app/ProgressDialog;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 327
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/SettingsPage;->mLocationPolicyDialog:Landroid/app/ProgressDialog;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 328
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/SettingsPage;->mLocationPolicyDialog:Landroid/app/ProgressDialog;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 329
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/SettingsPage;->mLocationPolicyDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 332
    :pswitch_3
    new-instance v3, Landroid/app/ProgressDialog;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;

    .line 333
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;

    const-string v4, "Changing cache policy...."

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 334
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 335
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 336
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 337
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/SettingsPage;->mCachePolicyDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 340
    :pswitch_4
    new-instance v16, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 341
    .local v16, locationDialogBuilder:Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0c0056

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 342
    const v3, 0x7f0e0001

    new-instance v4, Lnet/flixster/android/SettingsPage$10;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lnet/flixster/android/SettingsPage$10;-><init>(Lnet/flixster/android/SettingsPage;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 349
    invoke-virtual/range {v16 .. v16}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 351
    .end local v16           #locationDialogBuilder:Landroid/app/AlertDialog$Builder;
    :pswitch_5
    new-instance v11, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 352
    .local v11, distanceChoiceBuilder:Landroid/app/AlertDialog$Builder;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0043

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 355
    const-wide/high16 v9, 0x3ff0

    .line 356
    .local v9, conversion:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getDistanceType()I

    move-result v3

    if-nez v3, :cond_0

    .line 357
    sget-object v5, Lnet/flixster/android/SettingsPage;->SEARCH_RADIUS_IMPERIAL:[D

    .line 362
    .local v5, distances:[D
    :goto_1
    move-wide v6, v9

    .line 363
    .local v6, conv:D
    array-length v3, v5

    new-array v8, v3, [Ljava/lang/CharSequence;

    .line 364
    .local v8, radiusChoices:[Ljava/lang/CharSequence;
    const/4 v15, 0x0

    .local v15, i:I
    :goto_2
    array-length v3, v5

    if-lt v15, v3, :cond_1

    .line 369
    new-instance v3, Lnet/flixster/android/SettingsPage$11;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, Lnet/flixster/android/SettingsPage$11;-><init>(Lnet/flixster/android/SettingsPage;[DD[Ljava/lang/CharSequence;)V

    invoke-virtual {v11, v8, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 375
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v11, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 376
    invoke-virtual {v11}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 359
    .end local v5           #distances:[D
    .end local v6           #conv:D
    .end local v8           #radiusChoices:[Ljava/lang/CharSequence;
    .end local v15           #i:I
    :cond_0
    const-wide v9, 0x3ff9be76c0000000L

    .line 360
    sget-object v5, Lnet/flixster/android/SettingsPage;->SEARCH_RADIUS_METRIC:[D

    .restart local v5       #distances:[D
    goto :goto_1

    .line 365
    .restart local v6       #conv:D
    .restart local v8       #radiusChoices:[Ljava/lang/CharSequence;
    .restart local v15       #i:I
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00e5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v21, 0x0

    .line 366
    aget-wide v22, v5, v15

    mul-double v22, v22, v6

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->round(D)J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v4, v21

    .line 365
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v15

    .line 364
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 378
    .end local v5           #distances:[D
    .end local v6           #conv:D
    .end local v8           #radiusChoices:[Ljava/lang/CharSequence;
    .end local v9           #conversion:D
    .end local v11           #distanceChoiceBuilder:Landroid/app/AlertDialog$Builder;
    .end local v15           #i:I
    :pswitch_6
    new-instance v3, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0c005a

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 379
    const v4, 0x7f0e0002

    new-instance v21, Lnet/flixster/android/SettingsPage$12;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/flixster/android/SettingsPage$12;-><init>(Lnet/flixster/android/SettingsPage;)V

    move-object/from16 v0, v21

    invoke-virtual {v3, v4, v0}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 385
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 388
    :pswitch_7
    new-instance v3, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0c0038

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 389
    const v4, 0x7f0e0003

    new-instance v21, Lnet/flixster/android/SettingsPage$13;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/flixster/android/SettingsPage$13;-><init>(Lnet/flixster/android/SettingsPage;)V

    move-object/from16 v0, v21

    invoke-virtual {v3, v4, v0}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 396
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 399
    :pswitch_8
    new-instance v3, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0c0044

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 400
    const/high16 v4, 0x7f0e

    new-instance v21, Lnet/flixster/android/SettingsPage$14;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/flixster/android/SettingsPage$14;-><init>(Lnet/flixster/android/SettingsPage;)V

    move-object/from16 v0, v21

    invoke-virtual {v3, v4, v0}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 406
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 408
    :pswitch_9
    new-instance v3, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v4, "Invalid Postal Code"

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 409
    const-string v4, "Invalid Postal Code"

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 410
    const-string v4, "OK"

    new-instance v21, Lnet/flixster/android/SettingsPage$15;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/flixster/android/SettingsPage$15;-><init>(Lnet/flixster/android/SettingsPage;)V

    move-object/from16 v0, v21

    invoke-virtual {v3, v4, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 414
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 416
    :pswitch_a
    new-instance v12, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 417
    .local v12, facebookAlertBuilder:Landroid/app/AlertDialog$Builder;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00ee

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v21, 0x0

    const-string v22, "Facebook"

    aput-object v22, v4, v21

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 418
    .local v19, title:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00ef

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v21, 0x0

    const-string v22, "Facebook"

    aput-object v22, v4, v21

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 419
    .local v17, msg:Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 420
    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 421
    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 422
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0133

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 423
    new-instance v4, Lnet/flixster/android/SettingsPage$16;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lnet/flixster/android/SettingsPage$16;-><init>(Lnet/flixster/android/SettingsPage;)V

    .line 422
    invoke-virtual {v12, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 429
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v12, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 430
    invoke-virtual {v12}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 432
    .end local v12           #facebookAlertBuilder:Landroid/app/AlertDialog$Builder;
    .end local v17           #msg:Ljava/lang/String;
    .end local v19           #title:Ljava/lang/String;
    :pswitch_b
    new-instance v14, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 433
    .local v14, flixsterAlertBuilder:Landroid/app/AlertDialog$Builder;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00ee

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v21, 0x0

    const-string v22, "Flixster"

    aput-object v22, v4, v21

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 434
    .restart local v19       #title:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00ef

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v21, 0x0

    const-string v22, "Flixster"

    aput-object v22, v4, v21

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 435
    .restart local v17       #msg:Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 436
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 437
    const/4 v3, 0x1

    invoke-virtual {v14, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 438
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0133

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 439
    new-instance v4, Lnet/flixster/android/SettingsPage$17;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lnet/flixster/android/SettingsPage$17;-><init>(Lnet/flixster/android/SettingsPage;)V

    .line 438
    invoke-virtual {v14, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 446
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v14, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 447
    invoke-virtual {v14}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 449
    .end local v14           #flixsterAlertBuilder:Landroid/app/AlertDialog$Builder;
    .end local v17           #msg:Ljava/lang/String;
    .end local v19           #title:Ljava/lang/String;
    :pswitch_c
    new-instance v18, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 450
    .local v18, netflixAlertBuilder:Landroid/app/AlertDialog$Builder;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00ee

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v21, 0x0

    const-string v22, "Netflix"

    aput-object v22, v4, v21

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 451
    .restart local v19       #title:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00ef

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v21, 0x0

    const-string v22, "Netflix"

    aput-object v22, v4, v21

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 452
    .restart local v17       #msg:Ljava/lang/String;
    invoke-virtual/range {v18 .. v19}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 453
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 454
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 455
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0133

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 456
    new-instance v4, Lnet/flixster/android/SettingsPage$18;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lnet/flixster/android/SettingsPage$18;-><init>(Lnet/flixster/android/SettingsPage;)V

    .line 455
    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 464
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/SettingsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 465
    invoke-virtual/range {v18 .. v18}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 286
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_7
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 627
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 534
    const-string v0, "FlxMain"

    const-string v1, "SettingsPage.onDestroy"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onDestroy()V

    .line 537
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 539
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 541
    :cond_0
    iput-object v2, p0, Lnet/flixster/android/SettingsPage;->mTimer:Ljava/util/Timer;

    .line 542
    sput-object v2, Lnet/flixster/android/SettingsPage;->mSettingsPage:Lnet/flixster/android/SettingsPage;

    .line 543
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 96
    const-string v0, "FlxMain"

    const-string v1, "SettingsPage.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/SettingsPage;->mTimer:Ljava/util/Timer;

    .line 101
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/settings"

    const-string v2, "My Movies - Settings"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lnet/flixster/android/SettingsPage;->mRefreshHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 103
    return-void
.end method
