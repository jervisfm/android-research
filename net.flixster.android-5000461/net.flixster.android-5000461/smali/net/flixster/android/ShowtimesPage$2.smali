.class Lnet/flixster/android/ShowtimesPage$2;
.super Ljava/util/TimerTask;
.source "ShowtimesPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/ShowtimesPage;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ShowtimesPage;

.field private final synthetic val$currResumeCtr:I


# direct methods
.method constructor <init>(Lnet/flixster/android/ShowtimesPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    iput p2, p0, Lnet/flixster/android/ShowtimesPage$2;->val$currResumeCtr:I

    .line 92
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 95
    const-string v0, "FlxMain"

    const-string v1, "ShowtimesPage.ScheduleLoadItemsTask.run"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :try_start_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v7

    .line 98
    .local v7, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v9, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v0

    .line 101
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v2

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v4

    .line 102
    iget-object v5, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    #getter for: Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v5}, Lnet/flixster/android/ShowtimesPage;->access$0(Lnet/flixster/android/ShowtimesPage;)Lnet/flixster/android/model/Movie;

    move-result-object v5

    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v5

    .line 100
    invoke-static/range {v0 .. v7}, Lnet/flixster/android/data/TheaterDao;->findTheatersByMovieLocation(DDLjava/util/Date;JLjava/util/HashMap;)Ljava/util/List;

    move-result-object v0

    #setter for: Lnet/flixster/android/ShowtimesPage;->mTheaterList:Ljava/util/List;
    invoke-static {v9, v0}, Lnet/flixster/android/ShowtimesPage;->access$1(Lnet/flixster/android/ShowtimesPage;Ljava/util/List;)V

    .line 110
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    iget v1, p0, Lnet/flixster/android/ShowtimesPage$2;->val$currResumeCtr:I

    invoke-virtual {v0, v1}, Lnet/flixster/android/ShowtimesPage;->shouldSkipBackgroundTask(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 122
    .end local v7           #favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :goto_1
    return-void

    .line 105
    .restart local v7       #favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_0
    :try_start_1
    iget-object v9, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v0

    .line 106
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v2

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v4

    .line 107
    iget-object v5, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    #getter for: Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;
    invoke-static {v5}, Lnet/flixster/android/ShowtimesPage;->access$0(Lnet/flixster/android/ShowtimesPage;)Lnet/flixster/android/model/Movie;

    move-result-object v5

    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v5

    .line 105
    invoke-static/range {v0 .. v7}, Lnet/flixster/android/data/TheaterDao;->findTheatersByMovieLocation(DDLjava/util/Date;JLjava/util/HashMap;)Ljava/util/List;

    move-result-object v0

    #setter for: Lnet/flixster/android/ShowtimesPage;->mTheaterList:Ljava/util/List;
    invoke-static {v9, v0}, Lnet/flixster/android/ShowtimesPage;->access$1(Lnet/flixster/android/ShowtimesPage;Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 116
    .end local v7           #favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catch_0
    move-exception v8

    .line 117
    .local v8, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v0, "FlxMain"

    const-string v1, "ShowtimesPage.ScheduleLoadItemsTask.run DaoException"

    invoke-static {v0, v1, v8}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 118
    iget-object v0, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    invoke-virtual {v0, v8}, Lnet/flixster/android/ShowtimesPage;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 120
    iget-object v0, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 114
    .end local v8           #de:Lnet/flixster/android/data/DaoException;
    .restart local v7       #favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_1
    :try_start_3
    iget-object v0, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    #calls: Lnet/flixster/android/ShowtimesPage;->setShowtimesLviList()V
    invoke-static {v0}, Lnet/flixster/android/ShowtimesPage;->access$2(Lnet/flixster/android/ShowtimesPage;)V

    .line 115
    iget-object v0, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    .line 120
    iget-object v0, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 119
    .end local v7           #favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catchall_0
    move-exception v0

    .line 120
    iget-object v1, p0, Lnet/flixster/android/ShowtimesPage$2;->this$0:Lnet/flixster/android/ShowtimesPage;

    iget-object v1, v1, Lnet/flixster/android/ShowtimesPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 121
    throw v0
.end method
