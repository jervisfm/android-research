.class public Lnet/flixster/android/ShowtimesPage;
.super Lnet/flixster/android/LviActivity;
.source "ShowtimesPage.java"


# instance fields
.field private dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

.field private mMovie:Lnet/flixster/android/model/Movie;

.field private mTheaterList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation
.end field

.field private final mTheaterMapOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lnet/flixster/android/LviActivity;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/ShowtimesPage;->mTheaterList:Ljava/util/List;

    .line 219
    new-instance v0, Lnet/flixster/android/ShowtimesPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/ShowtimesPage$1;-><init>(Lnet/flixster/android/ShowtimesPage;)V

    iput-object v0, p0, Lnet/flixster/android/ShowtimesPage;->mTheaterMapOnClickListener:Landroid/view/View$OnClickListener;

    .line 43
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 4
    .parameter "delay"

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/ShowtimesPage;->throbberHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 91
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    .line 92
    .local v0, currResumeCtr:I
    new-instance v1, Lnet/flixster/android/ShowtimesPage$2;

    invoke-direct {v1, p0, v0}, Lnet/flixster/android/ShowtimesPage$2;-><init>(Lnet/flixster/android/ShowtimesPage;I)V

    .line 125
    .local v1, loadMoviesTask:Ljava/util/TimerTask;
    iget-object v2, p0, Lnet/flixster/android/ShowtimesPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 126
    iget-object v2, p0, Lnet/flixster/android/ShowtimesPage;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v1, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :cond_0
    monitor-exit p0

    return-void

    .line 89
    .end local v0           #currResumeCtr:I
    .end local v1           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lnet/flixster/android/ShowtimesPage;)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/ShowtimesPage;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lnet/flixster/android/ShowtimesPage;->mTheaterList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/ShowtimesPage;)V
    .locals 0
    .parameter

    .prologue
    .line 135
    invoke-direct {p0}, Lnet/flixster/android/ShowtimesPage;->setShowtimesLviList()V

    return-void
.end method

.method private createShowtimesSummary(Ljava/util/List;J)Ljava/lang/String;
    .locals 8
    .parameter
    .parameter "movieId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;J)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 248
    .local p1, theaters:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 249
    .local v1, sb:Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 250
    const v4, 0x7f0c0058

    invoke-virtual {p0, v4}, Lnet/flixster/android/ShowtimesPage;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    :cond_0
    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 252
    :cond_1
    const-string v4, "Showtimes for "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 253
    sget-object v5, Lcom/flixster/android/utils/DateTimeHelper;->DAY_IN_WEEK_FORMATTER:Ljava/text/SimpleDateFormat;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 254
    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Theater;

    .line 256
    .local v3, t:Lnet/flixster/android/model/Theater;
    invoke-virtual {v3}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;

    move-result-object v5

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 257
    .local v2, showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    if-eqz v2, :cond_2

    .line 260
    const-string v5, "name"

    invoke-virtual {v3, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 267
    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 261
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Showtimes;

    .line 262
    .local v0, s:Lnet/flixster/android/model/Showtimes;
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_4

    .line 263
    iget-object v6, v0, Lnet/flixster/android/model/Showtimes;->mShowtimesTitle:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    :cond_4
    invoke-virtual {v0}, Lnet/flixster/android/model/Showtimes;->getTimesString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private setShowtimesLviList()V
    .locals 20

    .prologue
    .line 136
    const-string v16, "FlxMain"

    const-string v17, "ShowtimesPage.setShowtimesLviList "

    invoke-static/range {v16 .. v17}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clear()V

    .line 148
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ShowtimesPage;->destroyExistingLviAd()V

    .line 149
    new-instance v16, Lnet/flixster/android/lvi/LviAd;

    invoke-direct/range {v16 .. v16}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/ShowtimesPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    move-object/from16 v16, v0

    const-string v17, "MovieShowtimes"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/lvi/LviAd;->dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v7, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v7}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 156
    .local v7, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v7, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 157
    const/16 v16, 0x1

    move/from16 v0, v16

    iput v0, v7, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    new-instance v5, Lnet/flixster/android/lvi/LviLocationPanel;

    invoke-direct {v5}, Lnet/flixster/android/lvi/LviLocationPanel;-><init>()V

    .line 161
    .local v5, lviLocationPanel:Lnet/flixster/android/lvi/LviLocationPanel;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    new-instance v3, Lnet/flixster/android/lvi/LviDatePanel;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviDatePanel;-><init>()V

    .line 164
    .local v3, lviDatePanel:Lnet/flixster/android/lvi/LviDatePanel;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ShowtimesPage;->getDateSelectOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v3, Lnet/flixster/android/lvi/LviDatePanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    invoke-static {}, Lcom/flixster/android/utils/GoogleApiDetector;->instance()Lcom/flixster/android/utils/GoogleApiDetector;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/flixster/android/utils/GoogleApiDetector;->isVanillaAndroid()Z

    move-result v16

    if-nez v16, :cond_0

    .line 168
    new-instance v4, Lnet/flixster/android/lvi/LviIconPanel;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviIconPanel;-><init>()V

    .line 169
    .local v4, lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ShowtimesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0c006b

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mTheaterMapOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v4, Lnet/flixster/android/lvi/LviIconPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    .end local v4           #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    :cond_0
    new-instance v14, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v14}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 175
    .local v14, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ShowtimesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0c0046

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v14, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mTheaterList:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_5

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mTheaterList:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_1
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_2

    .line 209
    new-instance v2, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 210
    .local v2, footer:Lnet/flixster/android/lvi/LviFooter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    .end local v2           #footer:Lnet/flixster/android/lvi/LviFooter;
    :goto_1
    return-void

    .line 179
    :cond_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lnet/flixster/android/model/Theater;

    .line 180
    .local v15, theater:Lnet/flixster/android/model/Theater;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    new-instance v18, Lcom/flixster/android/view/Divider;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/flixster/android/view/Divider;-><init>(Landroid/content/Context;)V

    sget v19, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_DIVIDER:I

    invoke-static/range {v18 .. v19}, Lnet/flixster/android/lvi/LviWrapper;->convertToLvi(Landroid/view/View;I)Lnet/flixster/android/lvi/Lvi;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    new-instance v9, Lnet/flixster/android/lvi/LviTheater;

    invoke-direct {v9}, Lnet/flixster/android/lvi/LviTheater;-><init>()V

    .line 183
    .local v9, lviTheater:Lnet/flixster/android/lvi/LviTheater;
    iput-object v15, v9, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    .line 184
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ShowtimesPage;->getStarTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v9, Lnet/flixster/android/lvi/LviTheater;->mStarTheaterClick:Landroid/view/View$OnClickListener;

    .line 185
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ShowtimesPage;->getTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v9, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    invoke-virtual {v15}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/ArrayList;

    .line 189
    .local v12, showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 190
    :cond_3
    new-instance v8, Lnet/flixster/android/lvi/LviShowtimes;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviShowtimes;-><init>()V

    .line 191
    .local v8, lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    iput-object v15, v8, Lnet/flixster/android/lvi/LviShowtimes;->mTheater:Lnet/flixster/android/model/Theater;

    .line 192
    const/16 v17, 0x0

    move-object/from16 v0, v17

    iput-object v0, v8, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 195
    .end local v8           #lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    :cond_4
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 196
    .local v13, size:I
    const/4 v10, 0x0

    .local v10, n:I
    :goto_2
    if-ge v10, v13, :cond_1

    .line 197
    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lnet/flixster/android/model/Showtimes;

    .line 198
    .local v11, showtimes:Lnet/flixster/android/model/Showtimes;
    new-instance v8, Lnet/flixster/android/lvi/LviShowtimes;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviShowtimes;-><init>()V

    .line 199
    .restart local v8       #lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    iput-object v15, v8, Lnet/flixster/android/lvi/LviShowtimes;->mTheater:Lnet/flixster/android/model/Theater;

    .line 200
    iput v13, v8, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimesListSize:I

    .line 201
    iput v10, v8, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimePosition:I

    .line 202
    iput-object v11, v8, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    .line 203
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ShowtimesPage;->getShowtimesClickListener()Landroid/view/View$OnClickListener;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v8, Lnet/flixster/android/lvi/LviShowtimes;->mBuyClick:Landroid/view/View$OnClickListener;

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 213
    .end local v8           #lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    .end local v9           #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    .end local v10           #n:I
    .end local v11           #showtimes:Lnet/flixster/android/model/Showtimes;
    .end local v12           #showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    .end local v13           #size:I
    .end local v15           #theater:Lnet/flixster/android/model/Theater;
    :cond_5
    new-instance v6, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 214
    .local v6, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ShowtimesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0c0058

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v6, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/ShowtimesPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method


# virtual methods
.method protected createShareIntent()Landroid/content/Intent;
    .locals 7

    .prologue
    .line 239
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 240
    .local v0, shareIntent:Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    const-string v1, "android.intent.extra.TEXT"

    .line 242
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-static {v3}, Lnet/flixster/android/MovieDetails;->createMovieSummary(Lnet/flixster/android/model/Movie;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/ShowtimesPage;->mTheaterList:Ljava/util/List;

    iget-object v4, p0, Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v4

    invoke-direct {p0, v3, v4, v5}, Lnet/flixster/android/ShowtimesPage;->createShowtimesSummary(Ljava/util/List;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 243
    const v3, 0x7f0c01b1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->mobileUpsell()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lnet/flixster/android/ShowtimesPage;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 242
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 241
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedState"

    .prologue
    .line 52
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lnet/flixster/android/ShowtimesPage;->createActionBar()V

    .line 55
    const v3, 0x7f0c0046

    invoke-virtual {p0, v3}, Lnet/flixster/android/ShowtimesPage;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lnet/flixster/android/ShowtimesPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 57
    iget-object v3, p0, Lnet/flixster/android/ShowtimesPage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    const-string v4, "MovieShowtimesStickyTop"

    invoke-virtual {v3, v4}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 58
    iget-object v3, p0, Lnet/flixster/android/ShowtimesPage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    const-string v4, "MovieShowtimesStickyBottom"

    invoke-virtual {v3, v4}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lnet/flixster/android/ShowtimesPage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 62
    const-string v3, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 63
    .local v1, movieId:J
    invoke-static {v1, v2}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v3

    iput-object v3, p0, Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;

    .line 76
    .end local v1           #movieId:J
    :cond_0
    new-instance v3, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    iget-object v4, p0, Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-direct {v3, v4}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;-><init>(Lnet/flixster/android/model/Movie;)V

    iput-object v3, p0, Lnet/flixster/android/ShowtimesPage;->dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    .line 77
    iget-object v3, p0, Lnet/flixster/android/ShowtimesPage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    iget-object v4, p0, Lnet/flixster/android/ShowtimesPage;->dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    iput-object v4, v3, Lnet/flixster/android/ads/AdView;->dfpCustomTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    .line 78
    iget-object v3, p0, Lnet/flixster/android/ShowtimesPage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    iget-object v4, p0, Lnet/flixster/android/ShowtimesPage;->dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    iput-object v4, v3, Lnet/flixster/android/ads/AdView;->dfpCustomTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    .line 79
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 5
    .parameter "item"

    .prologue
    .line 229
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 234
    :goto_0
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    return v0

    .line 231
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/showtimes"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Showtimes - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ActionBar"

    const-string v4, "Share"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 229
    nop

    :pswitch_data_0
    .packed-switch 0x7f070303
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 83
    invoke-super {p0}, Lnet/flixster/android/LviActivity;->onResume()V

    .line 84
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/showtimes"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Showtimes - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/ShowtimesPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lnet/flixster/android/ShowtimesPage;->ScheduleLoadItemsTask(J)V

    .line 86
    return-void
.end method

.method protected retryAction()V
    .locals 2

    .prologue
    .line 132
    const-wide/16 v0, 0x3e8

    invoke-direct {p0, v0, v1}, Lnet/flixster/android/ShowtimesPage;->ScheduleLoadItemsTask(J)V

    .line 133
    return-void
.end method
