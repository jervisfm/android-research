.class public Lnet/flixster/android/Starter;
.super Ljava/lang/Object;
.source "Starter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deepLinkMyMoviesTab(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    .line 324
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "flixster://mymovies"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 325
    return-void
.end method

.method public static forceClose()V
    .locals 1

    .prologue
    .line 330
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 331
    return-void
.end method

.method public static launchActor(JLjava/lang/String;Landroid/content/Context;)V
    .locals 2
    .parameter "actorId"
    .parameter "actorName"
    .parameter "context"

    .prologue
    .line 138
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/ActorPage;

    invoke-direct {v0, p3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    .local v0, i:Landroid/content/Intent;
    const-string v1, "ACTOR_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 140
    const-string v1, "ACTOR_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    invoke-virtual {p3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 142
    return-void
.end method

.method public static launchActorGallery(JLandroid/content/Context;)V
    .locals 2
    .parameter "actorId"
    .parameter "context"

    .prologue
    .line 145
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/ActorGalleryPage;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    .local v0, i:Landroid/content/Intent;
    const-string v1, "ACTOR_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 148
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 149
    return-void
.end method

.method public static launchAdFreeTrailer(JLandroid/content/Context;)V
    .locals 2
    .parameter "movieId"
    .parameter "context"

    .prologue
    .line 58
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/activity/common/LoadingActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 60
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 61
    return-void
.end method

.method public static launchAdFreeTrailer(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .parameter "url"
    .parameter "context"

    .prologue
    .line 75
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/MovieTrailer;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 76
    .local v0, i:Landroid/content/Intent;
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 77
    const-string v1, "low"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const-string v1, "title"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromAds()V

    .line 80
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 81
    return-void
.end method

.method public static launchAdFreeTrailer(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V
    .locals 4
    .parameter "m"
    .parameter "context"

    .prologue
    .line 64
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/MovieTrailer;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 66
    const-string v1, "high"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerHigh()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const-string v1, "med"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerMed()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    const-string v1, "low"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerLow()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string v1, "title"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromAds()V

    .line 71
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 72
    return-void
.end method

.method public static launchAdHtmlPage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .parameter "url"
    .parameter "title"
    .parameter "context"

    .prologue
    .line 246
    const/16 v0, 0x20

    .line 247
    .local v0, flags:I
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/flixster/android/ads/HtmlAdPage;

    invoke-direct {v1, p2, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 248
    .local v1, i:Landroid/content/Intent;
    const/high16 v2, 0x4000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 249
    const-string v2, "KEY_URL"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    const-string v2, "KEY_TITLE"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    const-string v2, "KEY_FLAGS"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 252
    invoke-virtual {p2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 253
    return-void
.end method

.method public static launchAdminPage(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 278
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/AdminPage;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 279
    .local v0, i:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 280
    return-void
.end method

.method public static launchBrowser(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .parameter "url"
    .parameter "context"

    .prologue
    .line 285
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 286
    .local v0, i:Landroid/content/Intent;
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 287
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 288
    return-void
.end method

.method public static launchDownloadManager(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 304
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW_DOWNLOADS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 305
    .local v0, i:Landroid/content/Intent;
    invoke-static {p0, v0}, Lnet/flixster/android/Starter;->tryLaunch(Landroid/content/Context;Landroid/content/Intent;)V

    .line 306
    return-void
.end method

.method public static launchEpisodes(JJLandroid/content/Context;)V
    .locals 2
    .parameter "seasonId"
    .parameter "rightId"
    .parameter "context"

    .prologue
    .line 118
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/activity/EpisodesPage;

    invoke-direct {v0, p4, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 119
    .local v0, i:Landroid/content/Intent;
    const-string v1, "KEY_SEASON_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 120
    const-string v1, "KEY_RIGHT_ID"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 121
    invoke-virtual {p4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 122
    return-void
.end method

.method public static launchEpisodes(JLandroid/content/Context;)V
    .locals 2
    .parameter "seasonId"
    .parameter "context"

    .prologue
    .line 112
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/activity/EpisodesPage;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 113
    .local v0, i:Landroid/content/Intent;
    const-string v1, "KEY_SEASON_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 114
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 115
    return-void
.end method

.method public static launchFbLogin(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 218
    return-void
.end method

.method public static launchFirstMsk(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    .line 235
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/flixster/android/msk/MskController;->setFirstLaunchMode(Z)V

    .line 236
    invoke-static {}, Lcom/flixster/android/utils/FirstLaunchMskHelper;->shouldShow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController;->isReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    invoke-static {}, Lcom/flixster/android/utils/FirstLaunchMskHelper;->shown()V

    .line 239
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/msk/MskEntryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 240
    .local v0, i:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 243
    .end local v0           #i:Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static launchFlxHtmlPage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .parameter "url"
    .parameter "title"
    .parameter "context"

    .prologue
    .line 256
    const/4 v0, 0x2

    .line 257
    .local v0, flags:I
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/flixster/android/ads/HtmlAdPage;

    invoke-direct {v1, p2, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 258
    .local v1, i:Landroid/content/Intent;
    const-string v2, "KEY_URL"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    const-string v2, "KEY_TITLE"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    const-string v2, "KEY_FLAGS"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 261
    invoke-virtual {p2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 262
    return-void
.end method

.method public static launchFlxLogin(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 222
    return-void
.end method

.method public static launchLoginPage(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .parameter "title"
    .parameter "context"

    .prologue
    .line 211
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/activity/LoginActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 212
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_TITLE"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 214
    return-void
.end method

.method public static launchMaps(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .parameter "query"
    .parameter "context"

    .prologue
    .line 291
    const-string v2, "+"

    invoke-static {p0, v2}, Lcom/flixster/android/utils/StringHelper;->replaceSpace(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 293
    invoke-static {}, Lcom/flixster/android/utils/GoogleApiDetector;->instance()Lcom/flixster/android/utils/GoogleApiDetector;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/GoogleApiDetector;->isVanillaAndroid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://maps.google.com/maps?q="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, uri:Ljava/lang/String;
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 299
    .local v0, i:Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 300
    return-void

    .line 296
    .end local v0           #i:Landroid/content/Intent;
    .end local v1           #uri:Ljava/lang/String;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "geo:0,0?q="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1       #uri:Ljava/lang/String;
    goto :goto_0
.end method

.method public static launchMovieDetail(JJLandroid/content/Context;)V
    .locals 2
    .parameter "movieId"
    .parameter "rightId"
    .parameter "context"

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/MovieDetails;

    invoke-direct {v0, p4, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 92
    const-string v1, "KEY_RIGHT_ID"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 93
    invoke-virtual {p4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 94
    return-void
.end method

.method public static launchMovieDetail(JLandroid/content/Context;)V
    .locals 2
    .parameter "movieId"
    .parameter "context"

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/MovieDetails;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 86
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 87
    return-void
.end method

.method public static launchMovieGallery(JLandroid/content/Context;)V
    .locals 2
    .parameter "movieId"
    .parameter "context"

    .prologue
    .line 131
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/MovieGalleryPage;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 134
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 135
    return-void
.end method

.method public static launchMovieShowtimes(JLandroid/content/Context;)V
    .locals 2
    .parameter "movieId"
    .parameter "context"

    .prologue
    .line 125
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/ShowtimesPage;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 127
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 128
    return-void
.end method

.method public static launchMsk(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    const/4 v2, 0x0

    .line 225
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/flixster/android/msk/MskController;->setFirstLaunchMode(Z)V

    .line 226
    invoke-static {}, Lcom/flixster/android/msk/MskController;->instance()Lcom/flixster/android/msk/MskController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/msk/MskController;->isReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/msk/MskEntryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 228
    .local v0, i:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 232
    .end local v0           #i:Landroid/content/Intent;
    :goto_0
    return-void

    .line 230
    :cond_0
    const v1, 0x7f0c00f3

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static launchMskHtmlPageForResult(Ljava/lang/String;ZLandroid/app/Activity;I)V
    .locals 4
    .parameter "url"
    .parameter "showSkip"
    .parameter "activity"
    .parameter "requestCode"

    .prologue
    .line 265
    const/16 v0, 0xa

    .line 266
    .local v0, flags:I
    if-eqz p1, :cond_0

    .line 267
    or-int/lit8 v0, v0, 0x40

    .line 269
    :cond_0
    const v3, 0x7f0c0135

    invoke-virtual {p2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 270
    .local v2, title:Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/flixster/android/ads/HtmlAdPage;

    invoke-direct {v1, p2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 271
    .local v1, i:Landroid/content/Intent;
    const-string v3, "KEY_URL"

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    const-string v3, "KEY_TITLE"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    const-string v3, "KEY_FLAGS"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 274
    invoke-virtual {p2, v1, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 275
    return-void
.end method

.method public static launchSearch(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .parameter "query"
    .parameter "context"

    .prologue
    .line 164
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/activity/phone/SearchFragmentActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_QUERY"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 168
    return-void
.end method

.method public static launchSeasonDetail(JJLandroid/content/Context;)V
    .locals 3
    .parameter "seasonId"
    .parameter "rightId"
    .parameter "context"

    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/MovieDetails;

    invoke-direct {v0, p4, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 106
    const-string v1, "KEY_IS_SEASON"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 107
    const-string v1, "KEY_RIGHT_ID"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 108
    invoke-virtual {p4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 109
    return-void
.end method

.method public static launchSeasonDetail(JLandroid/content/Context;)V
    .locals 3
    .parameter "seasonId"
    .parameter "context"

    .prologue
    .line 97
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/MovieDetails;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 98
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 99
    const-string v1, "KEY_IS_SEASON"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 100
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 101
    return-void
.end method

.method public static launchSettings(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 171
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/SettingsPage;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 172
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 173
    return-void
.end method

.method public static launchSimpleWebViewPage(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .parameter "url"
    .parameter "context"

    .prologue
    .line 205
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/activity/WebViewPage;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 206
    .local v0, i:Landroid/content/Intent;
    const-string v1, "KEY_URL"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 208
    return-void
.end method

.method public static launchSystemWirelessSettings(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 309
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIRELESS_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 310
    .local v0, i:Landroid/content/Intent;
    invoke-static {p0, v0}, Lnet/flixster/android/Starter;->tryLaunch(Landroid/content/Context;Landroid/content/Intent;)V

    .line 311
    return-void
.end method

.method public static launchTheatersMap(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 176
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/TheaterMapPage;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 177
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 178
    return-void
.end method

.method public static launchTheatersMapForMovie(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V
    .locals 4
    .parameter "m"
    .parameter "context"

    .prologue
    .line 181
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/MovieMapPage;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 182
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 183
    const-string v2, "MOVIE_THUMBNAIL"

    iget-object v1, p0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 184
    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 185
    const-string v1, "runningTime"

    const-string v2, "runningTime"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    const-string v1, "MOVIE_ACTORS_SHORT"

    const-string v2, "MOVIE_ACTORS_SHORT"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    const-string v1, "mpaa"

    const-string v2, "mpaa"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    const-string v1, "popcornScore"

    const-string v2, "popcornScore"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 189
    const-string v1, "rottenTomatoes"

    const-string v2, "rottenTomatoes"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 190
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 191
    return-void
.end method

.method public static launchTrailer(Lnet/flixster/android/model/Movie;Landroid/content/Context;)V
    .locals 4
    .parameter "m"
    .parameter "context"

    .prologue
    .line 36
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/MovieTrailer;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 38
    const-string v1, "high"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerHigh()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    const-string v1, "med"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerMed()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    const-string v1, "low"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerLow()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const-string v1, "title"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromNonAds()V

    .line 43
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method

.method public static launchTrailerForResult(Lnet/flixster/android/model/Movie;Landroid/app/Activity;I)V
    .locals 4
    .parameter "m"
    .parameter "activity"
    .parameter "requestCode"

    .prologue
    .line 47
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/MovieTrailer;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 48
    .local v0, i:Landroid/content/Intent;
    const-string v1, "net.flixster.android.EXTRA_MOVIE_ID"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 49
    const-string v1, "high"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerHigh()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    const-string v1, "med"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerMed()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const-string v1, "low"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerLow()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v1, "title"

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->trailer()Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/ads/AdsArbiter$TrailerAdsArbiter;->originatedFromNonAds()V

    .line 54
    invoke-virtual {p1, v0, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 55
    return-void
.end method

.method public static launchUserGallery(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .parameter "username"
    .parameter "context"

    .prologue
    .line 152
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/activity/PhotoGalleryPage;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 153
    .local v0, i:Landroid/content/Intent;
    const-string v1, "KEY_USERNAME"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 155
    return-void
.end method

.method public static launchUserProfile(JLandroid/content/Context;)V
    .locals 3
    .parameter "userId"
    .parameter "context"

    .prologue
    .line 158
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lnet/flixster/android/UserProfilePage;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 159
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "PLATFORM_ID"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 161
    return-void
.end method

.method public static launchVideo(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .parameter "url"
    .parameter "context"

    .prologue
    .line 194
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lnet/flixster/android/Starter;->launchVideo(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 195
    return-void
.end method

.method public static launchVideo(Ljava/lang/String;Landroid/content/Context;Z)V
    .locals 2
    .parameter "url"
    .parameter "context"
    .parameter "isLandscape"

    .prologue
    .line 198
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/flixster/android/activity/gtv/VideoPlayer;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 199
    .local v0, i:Landroid/content/Intent;
    const-string v1, "KEY_URL"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    const-string v1, "KEY_LANDSCAPE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 201
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 202
    return-void
.end method

.method public static tryLaunch(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 315
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :goto_0
    return-void

    .line 316
    :catch_0
    move-exception v0

    .line 317
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to start "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
