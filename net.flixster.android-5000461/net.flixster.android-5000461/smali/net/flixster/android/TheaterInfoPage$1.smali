.class Lnet/flixster/android/TheaterInfoPage$1;
.super Ljava/lang/Object;
.source "TheaterInfoPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TheaterInfoPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TheaterInfoPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TheaterInfoPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TheaterInfoPage$1;->this$0:Lnet/flixster/android/TheaterInfoPage;

    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "v"

    .prologue
    .line 268
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 270
    .local v0, callIntent:Landroid/content/Intent;
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lnet/flixster/android/TheaterInfoPage$1;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v4}, Lnet/flixster/android/TheaterInfoPage;->access$0(Lnet/flixster/android/TheaterInfoPage;)Lnet/flixster/android/model/Theater;

    move-result-object v4

    const-string v5, "street"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 271
    iget-object v4, p0, Lnet/flixster/android/TheaterInfoPage$1;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v4}, Lnet/flixster/android/TheaterInfoPage;->access$0(Lnet/flixster/android/TheaterInfoPage;)Lnet/flixster/android/model/Theater;

    move-result-object v4

    const-string v5, "city"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/TheaterInfoPage$1;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v4}, Lnet/flixster/android/TheaterInfoPage;->access$0(Lnet/flixster/android/TheaterInfoPage;)Lnet/flixster/android/model/Theater;

    move-result-object v4

    const-string v5, "state"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 272
    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/TheaterInfoPage$1;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v4}, Lnet/flixster/android/TheaterInfoPage;->access$0(Lnet/flixster/android/TheaterInfoPage;)Lnet/flixster/android/model/Theater;

    move-result-object v4

    const-string v5, "zip"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 270
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 273
    .local v2, query:Ljava/lang/String;
    const-string v3, " "

    const-string v4, "+"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 275
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "geo:0,0?q="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 277
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    const-string v4, "/theaters/info/map"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "query:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :try_start_0
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$1;->this$0:Lnet/flixster/android/TheaterInfoPage;

    invoke-virtual {v3, v0}, Lnet/flixster/android/TheaterInfoPage;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    :goto_0
    return-void

    .line 281
    :catch_0
    move-exception v1

    .line 282
    .local v1, e:Landroid/content/ActivityNotFoundException;
    const-string v3, "FlxMain"

    const-string v4, "Intent not found"

    invoke-static {v3, v4, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
