.class Lnet/flixster/android/TheaterInfoPage$4;
.super Ljava/lang/Object;
.source "TheaterInfoPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TheaterInfoPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TheaterInfoPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TheaterInfoPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TheaterInfoPage$4;->this$0:Lnet/flixster/android/TheaterInfoPage;

    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .parameter "v"

    .prologue
    .line 336
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/7eleven"

    const-string v2, "7-Eleven"

    const-string v3, "7Eleven"

    const-string v4, "Button"

    const-string v5, "Click"

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 338
    new-instance v7, Lnet/flixster/android/ads/model/FlixsterAd;

    invoke-direct {v7}, Lnet/flixster/android/ads/model/FlixsterAd;-><init>()V

    .line 339
    .local v7, ad:Lnet/flixster/android/ads/model/FlixsterAd;
    iget-object v0, p0, Lnet/flixster/android/TheaterInfoPage$4;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->elevenCreativeUrl:Ljava/lang/String;
    invoke-static {v0}, Lnet/flixster/android/TheaterInfoPage;->access$2(Lnet/flixster/android/TheaterInfoPage;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lnet/flixster/android/ads/model/FlixsterAd;->imageUrl:Ljava/lang/String;

    .line 341
    iget-object v0, p0, Lnet/flixster/android/TheaterInfoPage$4;->this$0:Lnet/flixster/android/TheaterInfoPage;

    const v1, 0x7f0c004a

    invoke-virtual {v0, v1}, Lnet/flixster/android/TheaterInfoPage;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lnet/flixster/android/ads/model/FlixsterAd;->button2:Ljava/lang/String;

    .line 342
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const-string v1, "CustomInterstitial"

    invoke-virtual {v0, v1, v7}, Lcom/flixster/android/utils/ObjectHolder;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 344
    new-instance v8, Landroid/content/Intent;

    iget-object v0, p0, Lnet/flixster/android/TheaterInfoPage$4;->this$0:Lnet/flixster/android/TheaterInfoPage;

    const-class v1, Lnet/flixster/android/ads/InterstitialPage;

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 345
    .local v8, intent:Landroid/content/Intent;
    const/high16 v0, 0x4000

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 346
    const-string v0, "SLOT_TYPE"

    const-string v1, "CustomInterstitial"

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    iget-object v0, p0, Lnet/flixster/android/TheaterInfoPage$4;->this$0:Lnet/flixster/android/TheaterInfoPage;

    invoke-virtual {v0, v8}, Lnet/flixster/android/TheaterInfoPage;->startActivity(Landroid/content/Intent;)V

    .line 348
    return-void
.end method
