.class Lnet/flixster/android/TheaterInfoPage$5;
.super Ljava/util/TimerTask;
.source "TheaterInfoPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TheaterInfoPage;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TheaterInfoPage;

.field private final synthetic val$currResumeCtr:I


# direct methods
.method constructor <init>(Lnet/flixster/android/TheaterInfoPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    iput p2, p0, Lnet/flixster/android/TheaterInfoPage$5;->val$currResumeCtr:I

    .line 102
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 105
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TheaterInfoPage.ScheduleLoadItemsTask.run mTheaterId:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mTheaterId:J
    invoke-static {v5}, Lnet/flixster/android/TheaterInfoPage;->access$3(Lnet/flixster/android/TheaterInfoPage;)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :try_start_0
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v3}, Lnet/flixster/android/TheaterInfoPage;->access$0(Lnet/flixster/android/TheaterInfoPage;)Lnet/flixster/android/model/Theater;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v3}, Lnet/flixster/android/TheaterInfoPage;->access$0(Lnet/flixster/android/TheaterInfoPage;)Lnet/flixster/android/model/Theater;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gtz v3, :cond_1

    .line 108
    :cond_0
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    invoke-virtual {v3}, Lnet/flixster/android/TheaterInfoPage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 109
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 110
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    iget-object v4, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mTheaterId:J
    invoke-static {v4}, Lnet/flixster/android/TheaterInfoPage;->access$3(Lnet/flixster/android/TheaterInfoPage;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lnet/flixster/android/data/TheaterDao;->getTheater(J)Lnet/flixster/android/model/Theater;

    move-result-object v4

    #setter for: Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v3, v4}, Lnet/flixster/android/TheaterInfoPage;->access$4(Lnet/flixster/android/TheaterInfoPage;Lnet/flixster/android/model/Theater;)V

    .line 111
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #calls: Lnet/flixster/android/TheaterInfoPage;->makeTheaterAddressString()V
    invoke-static {v3}, Lnet/flixster/android/TheaterInfoPage;->access$5(Lnet/flixster/android/TheaterInfoPage;)V

    .line 114
    .end local v1           #extras:Landroid/os/Bundle;
    :cond_1
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v4

    .line 115
    iget-object v5, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mTheaterId:J
    invoke-static {v5}, Lnet/flixster/android/TheaterInfoPage;->access$3(Lnet/flixster/android/TheaterInfoPage;)J

    move-result-wide v5

    .line 114
    invoke-static {v4, v5, v6}, Lnet/flixster/android/data/TheaterDao;->findShowtimesByTheater(Ljava/util/Date;J)Ljava/util/HashMap;

    move-result-object v4

    #setter for: Lnet/flixster/android/TheaterInfoPage;->mShowtimesByMovieHash:Ljava/util/HashMap;
    invoke-static {v3, v4}, Lnet/flixster/android/TheaterInfoPage;->access$6(Lnet/flixster/android/TheaterInfoPage;Ljava/util/HashMap;)V

    .line 117
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TheaterInfoPage.ScheduleLoadItemsTask.run() mShowtimesByMovieHash.size:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 118
    iget-object v5, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mShowtimesByMovieHash:Ljava/util/HashMap;
    invoke-static {v5}, Lnet/flixster/android/TheaterInfoPage;->access$7(Lnet/flixster/android/TheaterInfoPage;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 117
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lnet/flixster/android/TheaterInfoPage;->mMovieProtectionReference:Ljava/util/ArrayList;
    invoke-static {v3, v4}, Lnet/flixster/android/TheaterInfoPage;->access$8(Lnet/flixster/android/TheaterInfoPage;Ljava/util/ArrayList;)V

    .line 121
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mShowtimesByMovieHash:Ljava/util/HashMap;
    invoke-static {v3}, Lnet/flixster/android/TheaterInfoPage;->access$7(Lnet/flixster/android/TheaterInfoPage;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 125
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    iget v4, p0, Lnet/flixster/android/TheaterInfoPage$5;->val$currResumeCtr:I

    invoke-virtual {v3, v4}, Lnet/flixster/android/TheaterInfoPage;->shouldSkipBackgroundTask(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_3

    .line 135
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    iget-object v3, v3, Lnet/flixster/android/TheaterInfoPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 137
    :goto_1
    return-void

    .line 121
    :cond_2
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 122
    .local v2, l:Ljava/lang/Long;
    iget-object v4, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #getter for: Lnet/flixster/android/TheaterInfoPage;->mMovieProtectionReference:Ljava/util/ArrayList;
    invoke-static {v4}, Lnet/flixster/android/TheaterInfoPage;->access$9(Lnet/flixster/android/TheaterInfoPage;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 131
    .end local v2           #l:Ljava/lang/Long;
    :catch_0
    move-exception v0

    .line 132
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v3, "FlxMain"

    const-string v4, "TheaterInfoPage.ScheduleLoadItemsTask DaoException"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 133
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    invoke-virtual {v3, v0}, Lnet/flixster/android/TheaterInfoPage;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 135
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    iget-object v3, v3, Lnet/flixster/android/TheaterInfoPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 129
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_3
    :try_start_3
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    #calls: Lnet/flixster/android/TheaterInfoPage;->setTheaterInfoLviList()V
    invoke-static {v3}, Lnet/flixster/android/TheaterInfoPage;->access$10(Lnet/flixster/android/TheaterInfoPage;)V

    .line 130
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    iget-object v3, v3, Lnet/flixster/android/TheaterInfoPage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    .line 135
    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    iget-object v3, v3, Lnet/flixster/android/TheaterInfoPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 134
    :catchall_0
    move-exception v3

    .line 135
    iget-object v4, p0, Lnet/flixster/android/TheaterInfoPage$5;->this$0:Lnet/flixster/android/TheaterInfoPage;

    iget-object v4, v4, Lnet/flixster/android/TheaterInfoPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 136
    throw v3
.end method
