.class public Lnet/flixster/android/TheaterInfoPage;
.super Lnet/flixster/android/LviActivity;
.source "TheaterInfoPage.java"


# instance fields
.field private elevenCreativeUrl:Ljava/lang/String;

.field private final m7ElevenListener:Landroid/view/View$OnClickListener;

.field private final mCallTheaterListener:Landroid/view/View$OnClickListener;

.field private final mMapTheaterListener:Landroid/view/View$OnClickListener;

.field private mMovieProtectionReference:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private mShowtimesByMovieHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Showtimes;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTheater:Lnet/flixster/android/model/Theater;

.field private mTheaterAddressString:Ljava/lang/String;

.field private mTheaterId:J

.field private final mYelpTheaterListener:Landroid/view/View$OnClickListener;

.field private show7Eleven:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lnet/flixster/android/LviActivity;-><init>()V

    .line 264
    new-instance v0, Lnet/flixster/android/TheaterInfoPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterInfoPage$1;-><init>(Lnet/flixster/android/TheaterInfoPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterInfoPage;->mMapTheaterListener:Landroid/view/View$OnClickListener;

    .line 287
    new-instance v0, Lnet/flixster/android/TheaterInfoPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterInfoPage$2;-><init>(Lnet/flixster/android/TheaterInfoPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterInfoPage;->mCallTheaterListener:Landroid/view/View$OnClickListener;

    .line 312
    new-instance v0, Lnet/flixster/android/TheaterInfoPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterInfoPage$3;-><init>(Lnet/flixster/android/TheaterInfoPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterInfoPage;->mYelpTheaterListener:Landroid/view/View$OnClickListener;

    .line 333
    new-instance v0, Lnet/flixster/android/TheaterInfoPage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterInfoPage$4;-><init>(Lnet/flixster/android/TheaterInfoPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterInfoPage;->m7ElevenListener:Landroid/view/View$OnClickListener;

    .line 50
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 4
    .parameter "delay"

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/TheaterInfoPage;->throbberHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 101
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    .line 102
    .local v0, currResumeCtr:I
    new-instance v1, Lnet/flixster/android/TheaterInfoPage$5;

    invoke-direct {v1, p0, v0}, Lnet/flixster/android/TheaterInfoPage$5;-><init>(Lnet/flixster/android/TheaterInfoPage;I)V

    .line 140
    .local v1, loadMoviesTask:Ljava/util/TimerTask;
    iget-object v2, p0, Lnet/flixster/android/TheaterInfoPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 141
    iget-object v2, p0, Lnet/flixster/android/TheaterInfoPage;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v1, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :cond_0
    monitor-exit p0

    return-void

    .line 99
    .end local v0           #currResumeCtr:I
    .end local v1           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lnet/flixster/android/TheaterInfoPage;)Lnet/flixster/android/model/Theater;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/model/Theater;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 351
    invoke-static {p0}, Lnet/flixster/android/TheaterInfoPage;->getAddressForYelp(Lnet/flixster/android/model/Theater;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$10(Lnet/flixster/android/TheaterInfoPage;)V
    .locals 0
    .parameter

    .prologue
    .line 150
    invoke-direct {p0}, Lnet/flixster/android/TheaterInfoPage;->setTheaterInfoLviList()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/TheaterInfoPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lnet/flixster/android/TheaterInfoPage;->elevenCreativeUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/TheaterInfoPage;)J
    .locals 2
    .parameter

    .prologue
    .line 51
    iget-wide v0, p0, Lnet/flixster/android/TheaterInfoPage;->mTheaterId:J

    return-wide v0
.end method

.method static synthetic access$4(Lnet/flixster/android/TheaterInfoPage;Lnet/flixster/android/model/Theater;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    return-void
.end method

.method static synthetic access$5(Lnet/flixster/android/TheaterInfoPage;)V
    .locals 0
    .parameter

    .prologue
    .line 360
    invoke-direct {p0}, Lnet/flixster/android/TheaterInfoPage;->makeTheaterAddressString()V

    return-void
.end method

.method static synthetic access$6(Lnet/flixster/android/TheaterInfoPage;Ljava/util/HashMap;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lnet/flixster/android/TheaterInfoPage;->mShowtimesByMovieHash:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$7(Lnet/flixster/android/TheaterInfoPage;)Ljava/util/HashMap;
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lnet/flixster/android/TheaterInfoPage;->mShowtimesByMovieHash:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/TheaterInfoPage;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lnet/flixster/android/TheaterInfoPage;->mMovieProtectionReference:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/TheaterInfoPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lnet/flixster/android/TheaterInfoPage;->mMovieProtectionReference:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static createTheaterSummary(Lnet/flixster/android/model/Theater;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "t"
    .parameter "address"

    .prologue
    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 410
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "name"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getAddressForYelp(Lnet/flixster/android/model/Theater;)Ljava/lang/String;
    .locals 2
    .parameter "t"

    .prologue
    .line 352
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isFrenchLocale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "street"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "city"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", France"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 356
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "address"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private makeTheaterAddressString()V
    .locals 3

    .prologue
    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "street"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 362
    iget-object v1, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "city"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "state"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 363
    iget-object v1, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "zip"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 361
    iput-object v0, p0, Lnet/flixster/android/TheaterInfoPage;->mTheaterAddressString:Ljava/lang/String;

    .line 364
    return-void
.end method

.method private setTheaterInfoLviList()V
    .locals 22

    .prologue
    .line 151
    const-string v1, "FlxMain"

    const-string v2, "TheaterInfoPage.setPopularLviList "

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 159
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterInfoPage;->destroyExistingLviAd()V

    .line 160
    new-instance v1, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 161
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v2, "TheaterInfo"

    iput-object v2, v1, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 162
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/TheaterInfoPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    new-instance v10, Lnet/flixster/android/lvi/LviIconPanel;

    invoke-direct {v10}, Lnet/flixster/android/lvi/LviIconPanel;-><init>()V

    .line 171
    .local v10, lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    .line 172
    const v1, 0x7f02010a

    iput v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mDrawableResource:I

    .line 173
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mCallTheaterListener:Landroid/view/View$OnClickListener;

    iput-object v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 174
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    new-instance v10, Lnet/flixster/android/lvi/LviIconPanel;

    .end local v10           #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    invoke-direct {v10}, Lnet/flixster/android/lvi/LviIconPanel;-><init>()V

    .line 177
    .restart local v10       #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mTheaterAddressString:Ljava/lang/String;

    iput-object v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    .line 178
    const v1, 0x7f020107

    iput v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mDrawableResource:I

    .line 179
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mMapTheaterListener:Landroid/view/View$OnClickListener;

    iput-object v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 180
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isUsLocale()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isCanadaLocale()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isUkLocale()Z

    move-result v1

    if-nez v1, :cond_0

    .line 183
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->isFrenchLocale()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 184
    :cond_0
    new-instance v10, Lnet/flixster/android/lvi/LviIconPanel;

    .end local v10           #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    invoke-direct {v10}, Lnet/flixster/android/lvi/LviIconPanel;-><init>()V

    .line 185
    .restart local v10       #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterInfoPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00dc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    .line 186
    const v1, 0x7f020114

    iput v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mDrawableResource:I

    .line 187
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mYelpTheaterListener:Landroid/view/View$OnClickListener;

    iput-object v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 188
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lnet/flixster/android/TheaterInfoPage;->show7Eleven:Z

    if-eqz v1, :cond_2

    .line 192
    new-instance v10, Lnet/flixster/android/lvi/LviIconPanel;

    .end local v10           #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    invoke-direct {v10}, Lnet/flixster/android/lvi/LviIconPanel;-><init>()V

    .line 193
    .restart local v10       #lviIconPanel:Lnet/flixster/android/lvi/LviIconPanel;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterInfoPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00dd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    .line 194
    const v1, 0x7f0200f8

    iput v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mDrawableResource:I

    .line 195
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->m7ElevenListener:Landroid/view/View$OnClickListener;

    iput-object v1, v10, Lnet/flixster/android/lvi/LviIconPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 196
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/7eleven"

    const-string v3, "7-Eleven"

    const-string v4, "7Eleven"

    const-string v5, "Button"

    const-string v6, "Impression"

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 200
    :cond_2
    new-instance v9, Lnet/flixster/android/lvi/LviDatePanel;

    invoke-direct {v9}, Lnet/flixster/android/lvi/LviDatePanel;-><init>()V

    .line 201
    .local v9, lviDatePanel:Lnet/flixster/android/lvi/LviDatePanel;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterInfoPage;->getDateSelectOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    iput-object v1, v9, Lnet/flixster/android/lvi/LviDatePanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 202
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    new-instance v20, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct/range {v20 .. v20}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 205
    .local v20, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterInfoPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v20

    iput-object v1, v0, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 206
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v0, v20

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mShowtimesByMovieHash:Ljava/util/HashMap;

    if-eqz v1, :cond_7

    .line 215
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TheaterInfoList.LoadMoviesTask mShowtimesHash, size:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/TheaterInfoPage;->mShowtimesByMovieHash:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mShowtimesByMovieHash:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 218
    new-instance v11, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v11}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 219
    .local v11, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    const v1, 0x7f0c0058

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lnet/flixster/android/TheaterInfoPage;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 220
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    .end local v11           #lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    :goto_0
    return-void

    .line 224
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterInfoPage;->mShowtimesByMovieHash:Ljava/util/HashMap;

    move-object/from16 v21, v0

    .line 225
    .local v21, tempShowtimesByMovieHash:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;>;"
    if-eqz v21, :cond_5

    .line 227
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v18

    .line 228
    .local v18, showtimesMovieIdList:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_6

    .line 252
    .end local v18           #showtimesMovieIdList:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_5
    new-instance v8, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 253
    .local v8, footer:Lnet/flixster/android/lvi/LviFooter;
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 228
    .end local v8           #footer:Lnet/flixster/android/lvi/LviFooter;
    .restart local v18       #showtimesMovieIdList:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    .line 230
    .local v14, movieId:Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    new-instance v3, Lcom/flixster/android/view/Divider;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/flixster/android/view/Divider;-><init>(Landroid/content/Context;)V

    sget v4, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_DIVIDER:I

    invoke-static {v3, v4}, Lnet/flixster/android/lvi/LviWrapper;->convertToLvi(Landroid/view/View;I)Lnet/flixster/android/lvi/Lvi;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    new-instance v12, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v12}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 232
    .local v12, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v2

    iput-object v2, v12, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 233
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterInfoPage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    iput-object v2, v12, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 235
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/ArrayList;

    .line 238
    .local v17, showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 239
    .local v19, size:I
    const/4 v15, 0x0

    .local v15, n:I
    :goto_1
    move/from16 v0, v19

    if-ge v15, v0, :cond_4

    .line 240
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lnet/flixster/android/model/Showtimes;

    .line 241
    .local v16, showtimes:Lnet/flixster/android/model/Showtimes;
    new-instance v13, Lnet/flixster/android/lvi/LviShowtimes;

    invoke-direct {v13}, Lnet/flixster/android/lvi/LviShowtimes;-><init>()V

    .line 242
    .local v13, lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    iput-object v2, v13, Lnet/flixster/android/lvi/LviShowtimes;->mTheater:Lnet/flixster/android/model/Theater;

    .line 243
    move/from16 v0, v19

    iput v0, v13, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimesListSize:I

    .line 244
    iput v15, v13, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimePosition:I

    .line 245
    move-object/from16 v0, v16

    iput-object v0, v13, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    .line 246
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterInfoPage;->getShowtimesClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    iput-object v2, v13, Lnet/flixster/android/lvi/LviShowtimes;->mBuyClick:Landroid/view/View$OnClickListener;

    .line 247
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 257
    .end local v12           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v13           #lviShowtimes:Lnet/flixster/android/lvi/LviShowtimes;
    .end local v14           #movieId:Ljava/lang/Long;
    .end local v15           #n:I
    .end local v16           #showtimes:Lnet/flixster/android/model/Showtimes;
    .end local v17           #showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    .end local v18           #showtimesMovieIdList:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v19           #size:I
    .end local v21           #tempShowtimesByMovieHash:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;>;"
    :cond_7
    new-instance v11, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v11}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 258
    .restart local v11       #lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    const-string v1, "Network Error Occured"

    iput-object v1, v11, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 259
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/TheaterInfoPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method protected createShareIntent()Landroid/content/Intent;
    .locals 7

    .prologue
    .line 399
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 400
    .local v0, shareIntent:Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 402
    const-string v1, "android.intent.extra.TEXT"

    .line 403
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    iget-object v4, p0, Lnet/flixster/android/TheaterInfoPage;->mTheaterAddressString:Ljava/lang/String;

    invoke-static {v3, v4}, Lnet/flixster/android/TheaterInfoPage;->createTheaterSummary(Lnet/flixster/android/model/Theater;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 404
    const v3, 0x7f0c01b1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->mobileUpsell()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lnet/flixster/android/TheaterInfoPage;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 403
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 401
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedState"

    .prologue
    .line 62
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lnet/flixster/android/TheaterInfoPage;->createActionBar()V

    .line 65
    iget-object v2, p0, Lnet/flixster/android/TheaterInfoPage;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/TheaterInfoPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 67
    invoke-virtual {p0}, Lnet/flixster/android/TheaterInfoPage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 68
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 69
    const-string v2, "net.flixster.android.EXTRA_THEATER_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lnet/flixster/android/TheaterInfoPage;->mTheaterId:J

    .line 70
    iget-wide v2, p0, Lnet/flixster/android/TheaterInfoPage;->mTheaterId:J

    invoke-static {v2, v3}, Lnet/flixster/android/data/TheaterDao;->getTheater(J)Lnet/flixster/android/model/Theater;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    .line 71
    invoke-direct {p0}, Lnet/flixster/android/TheaterInfoPage;->makeTheaterAddressString()V

    .line 76
    :goto_0
    iget-object v2, p0, Lnet/flixster/android/TheaterInfoPage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    const-string v3, "TheaterInfoStickyTop"

    invoke-virtual {v2, v3}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 77
    iget-object v2, p0, Lnet/flixster/android/TheaterInfoPage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    const-string v3, "TheaterInfoStickyBottom"

    invoke-virtual {v2, v3}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 79
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v1

    .line 80
    .local v1, p:Lnet/flixster/android/model/Property;
    if-eqz v1, :cond_0

    .line 81
    iget-boolean v2, v1, Lnet/flixster/android/model/Property;->is7ElevenEnabled:Z

    iput-boolean v2, p0, Lnet/flixster/android/TheaterInfoPage;->show7Eleven:Z

    .line 82
    iget-object v2, v1, Lnet/flixster/android/model/Property;->elevenCreativeUrl:Ljava/lang/String;

    iput-object v2, p0, Lnet/flixster/android/TheaterInfoPage;->elevenCreativeUrl:Ljava/lang/String;

    .line 84
    :cond_0
    return-void

    .line 73
    .end local v1           #p:Lnet/flixster/android/model/Property;
    :cond_1
    const-string v2, "FlxMain"

    const-string v3, "Missing the bundle extras... "

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 368
    invoke-virtual {p0}, Lnet/flixster/android/TheaterInfoPage;->getSupportMenuInflater()Lcom/actionbarsherlock/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0012

    invoke-virtual {v0, v1, p1}, Lcom/actionbarsherlock/view/MenuInflater;->inflate(ILcom/actionbarsherlock/view/Menu;)V

    .line 369
    iget-object v0, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v0}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->favoriteTheaterFlag(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    const v0, 0x7f07030d

    invoke-interface {p1, v0}, Lcom/actionbarsherlock/view/Menu;->removeItem(I)V

    .line 374
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 372
    :cond_0
    const v0, 0x7f07030e

    invoke-interface {p1, v0}, Lcom/actionbarsherlock/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 5
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 379
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 393
    :goto_0
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_1
    return v0

    .line 381
    :sswitch_0
    iget-object v1, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v1}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->addFavoriteTheater(Ljava/lang/String;)V

    .line 382
    invoke-virtual {p0}, Lnet/flixster/android/TheaterInfoPage;->invalidateOptionsMenu()V

    goto :goto_1

    .line 385
    :sswitch_1
    iget-object v1, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v1}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->removeFavoriteTheater(Ljava/lang/String;)V

    .line 386
    invoke-virtual {p0}, Lnet/flixster/android/TheaterInfoPage;->invalidateOptionsMenu()V

    goto :goto_1

    .line 390
    :sswitch_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/theater/info"

    .line 391
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Theater Info - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v4, "name"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ActionBar"

    const-string v4, "Share"

    .line 390
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 379
    :sswitch_data_0
    .sparse-switch
        0x7f070303 -> :sswitch_2
        0x7f07030d -> :sswitch_0
        0x7f07030e -> :sswitch_1
    .end sparse-switch
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 88
    invoke-super {p0}, Lnet/flixster/android/LviActivity;->onResume()V

    .line 89
    const/4 v0, 0x0

    .line 90
    .local v0, theaterName:Ljava/lang/String;
    iget-object v1, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lnet/flixster/android/TheaterInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v2, "name"

    invoke-virtual {v1, v2}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    invoke-virtual {p0, v0}, Lnet/flixster/android/TheaterInfoPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 94
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v1

    const-string v2, "/theater/info"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Theater Info - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-wide/16 v1, 0x64

    invoke-direct {p0, v1, v2}, Lnet/flixster/android/TheaterInfoPage;->ScheduleLoadItemsTask(J)V

    .line 96
    return-void
.end method

.method protected retryAction()V
    .locals 2

    .prologue
    .line 147
    const-wide/16 v0, 0x3e8

    invoke-direct {p0, v0, v1}, Lnet/flixster/android/TheaterInfoPage;->ScheduleLoadItemsTask(J)V

    .line 148
    return-void
.end method
