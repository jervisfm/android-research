.class Lnet/flixster/android/TheaterItemizedOverlay$1;
.super Ljava/lang/Object;
.source "TheaterItemizedOverlay.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TheaterItemizedOverlay;->onTap(I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TheaterItemizedOverlay;

.field private final synthetic val$theaterId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lnet/flixster/android/TheaterItemizedOverlay;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TheaterItemizedOverlay$1;->this$0:Lnet/flixster/android/TheaterItemizedOverlay;

    iput-object p2, p0, Lnet/flixster/android/TheaterItemizedOverlay$1;->val$theaterId:Ljava/lang/String;

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter "dialog"
    .parameter "id"

    .prologue
    .line 47
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lnet/flixster/android/TheaterItemizedOverlay$1;->this$0:Lnet/flixster/android/TheaterItemizedOverlay;

    #getter for: Lnet/flixster/android/TheaterItemizedOverlay;->context:Landroid/content/Context;
    invoke-static {v2}, Lnet/flixster/android/TheaterItemizedOverlay;->access$0(Lnet/flixster/android/TheaterItemizedOverlay;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lnet/flixster/android/TheaterInfoPage;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 48
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Lnet/flixster/android/TheaterItemizedOverlay$1;->val$theaterId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lnet/flixster/android/data/TheaterDao;->getTheater(J)Lnet/flixster/android/model/Theater;

    move-result-object v1

    .line 49
    .local v1, theater:Lnet/flixster/android/model/Theater;
    const-string v2, "net.flixster.android.EXTRA_THEATER_ID"

    invoke-virtual {v1}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 50
    const-string v2, "name"

    const-string v3, "name"

    invoke-virtual {v1, v3}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const-string v2, "state"

    const-string v3, "state"

    invoke-virtual {v1, v3}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v2, "phone"

    const-string v3, "phone"

    invoke-virtual {v1, v3}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string v2, "street"

    const-string v3, "street"

    invoke-virtual {v1, v3}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    const-string v2, "zip"

    const-string v3, "zip"

    invoke-virtual {v1, v3}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const-string v2, "city"

    const-string v3, "city"

    invoke-virtual {v1, v3}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    iget-object v2, p0, Lnet/flixster/android/TheaterItemizedOverlay$1;->this$0:Lnet/flixster/android/TheaterItemizedOverlay;

    #getter for: Lnet/flixster/android/TheaterItemizedOverlay;->context:Landroid/content/Context;
    invoke-static {v2}, Lnet/flixster/android/TheaterItemizedOverlay;->access$0(Lnet/flixster/android/TheaterItemizedOverlay;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 57
    return-void
.end method
