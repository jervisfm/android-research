.class public Lnet/flixster/android/TheaterItemizedOverlay;
.super Lcom/google/android/maps/ItemizedOverlay;
.source "TheaterItemizedOverlay.java"


# instance fields
.field private context:Landroid/content/Context;

.field private overlayItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/maps/OverlayItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .parameter "context"
    .parameter "defaultMarker"

    .prologue
    .line 28
    invoke-static {p2}, Lnet/flixster/android/TheaterItemizedOverlay;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/ItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/TheaterItemizedOverlay;->overlayItems:Ljava/util/ArrayList;

    .line 29
    iput-object p1, p0, Lnet/flixster/android/TheaterItemizedOverlay;->context:Landroid/content/Context;

    .line 30
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/TheaterItemizedOverlay;)Landroid/content/Context;
    .locals 1
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lnet/flixster/android/TheaterItemizedOverlay;->context:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public addOverlay(Lcom/google/android/maps/OverlayItem;)V
    .locals 1
    .parameter "overlayItem"

    .prologue
    .line 33
    iget-object v0, p0, Lnet/flixster/android/TheaterItemizedOverlay;->overlayItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    invoke-virtual {p0}, Lnet/flixster/android/TheaterItemizedOverlay;->populate()V

    .line 35
    return-void
.end method

.method public createItem(I)Lcom/google/android/maps/OverlayItem;
    .locals 1
    .parameter "i"

    .prologue
    .line 76
    iget-object v0, p0, Lnet/flixster/android/TheaterItemizedOverlay;->overlayItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/OverlayItem;

    return-object v0
.end method

.method public onTap(I)Z
    .locals 8
    .parameter "i"

    .prologue
    .line 39
    iget-object v5, p0, Lnet/flixster/android/TheaterItemizedOverlay;->overlayItems:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/maps/OverlayItem;

    .line 40
    .local v2, item:Lcom/google/android/maps/OverlayItem;
    invoke-virtual {v2}, Lcom/google/android/maps/OverlayItem;->getSnippet()Ljava/lang/String;

    move-result-object v4

    .line 41
    .local v4, theaterId:Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 42
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lnet/flixster/android/TheaterItemizedOverlay;->context:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 43
    .local v1, alertBuilder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v2}, Lcom/google/android/maps/OverlayItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 44
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 45
    const-string v5, "GO"

    new-instance v6, Lnet/flixster/android/TheaterItemizedOverlay$1;

    invoke-direct {v6, p0, v4}, Lnet/flixster/android/TheaterItemizedOverlay$1;-><init>(Lnet/flixster/android/TheaterItemizedOverlay;Ljava/lang/String;)V

    invoke-virtual {v1, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 59
    iget-object v5, p0, Lnet/flixster/android/TheaterItemizedOverlay;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c004a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 60
    new-instance v6, Lnet/flixster/android/TheaterItemizedOverlay$2;

    invoke-direct {v6, p0}, Lnet/flixster/android/TheaterItemizedOverlay$2;-><init>(Lnet/flixster/android/TheaterItemizedOverlay;)V

    .line 59
    invoke-virtual {v1, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 65
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 66
    .local v0, alert:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 71
    .end local v0           #alert:Landroid/app/AlertDialog;
    .end local v1           #alertBuilder:Landroid/app/AlertDialog$Builder;
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/maps/ItemizedOverlay;->onTap(I)Z

    move-result v5

    return v5

    .line 68
    :cond_0
    iget-object v5, p0, Lnet/flixster/android/TheaterItemizedOverlay;->context:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/google/android/maps/OverlayItem;->getTitle()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 69
    .local v3, marker:Landroid/widget/Toast;
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lnet/flixster/android/TheaterItemizedOverlay;->overlayItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
