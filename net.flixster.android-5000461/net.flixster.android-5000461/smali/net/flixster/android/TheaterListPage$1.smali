.class Lnet/flixster/android/TheaterListPage$1;
.super Ljava/lang/Object;
.source "TheaterListPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TheaterListPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TheaterListPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TheaterListPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TheaterListPage$1;->this$0:Lnet/flixster/android/TheaterListPage;

    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    .line 336
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;

    .line 337
    .local v2, viewHolder:Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;
    if-eqz v2, :cond_0

    .line 338
    iget-object v1, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->theater:Lnet/flixster/android/model/Theater;

    .line 339
    .local v1, theater:Lnet/flixster/android/model/Theater;
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lnet/flixster/android/TheaterListPage$1;->this$0:Lnet/flixster/android/TheaterListPage;

    const-class v4, Lnet/flixster/android/TheaterInfoPage;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 340
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "net.flixster.android.EXTRA_THEATER_ID"

    invoke-virtual {v1}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 341
    iget-object v3, p0, Lnet/flixster/android/TheaterListPage$1;->this$0:Lnet/flixster/android/TheaterListPage;

    invoke-virtual {v3, v0}, Lnet/flixster/android/TheaterListPage;->startActivity(Landroid/content/Intent;)V

    .line 343
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #theater:Lnet/flixster/android/model/Theater;
    :cond_0
    return-void
.end method
