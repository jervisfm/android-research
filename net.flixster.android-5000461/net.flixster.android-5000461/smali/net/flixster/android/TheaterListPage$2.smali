.class Lnet/flixster/android/TheaterListPage$2;
.super Ljava/lang/Object;
.source "TheaterListPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TheaterListPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TheaterListPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TheaterListPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TheaterListPage$2;->this$0:Lnet/flixster/android/TheaterListPage;

    .line 346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 348
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 360
    :goto_0
    return-void

    .line 351
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$2;->this$0:Lnet/flixster/android/TheaterListPage;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    #setter for: Lnet/flixster/android/TheaterListPage;->mNavSelect:I
    invoke-static {v0, v1}, Lnet/flixster/android/TheaterListPage;->access$0(Lnet/flixster/android/TheaterListPage;I)V

    .line 352
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$2;->this$0:Lnet/flixster/android/TheaterListPage;

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lnet/flixster/android/TheaterListPage$2;->this$0:Lnet/flixster/android/TheaterListPage;

    invoke-virtual {v1}, Lnet/flixster/android/TheaterListPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 353
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->incrementResumeCtr()V

    .line 354
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$2;->this$0:Lnet/flixster/android/TheaterListPage;

    iget-object v1, p0, Lnet/flixster/android/TheaterListPage$2;->this$0:Lnet/flixster/android/TheaterListPage;

    #getter for: Lnet/flixster/android/TheaterListPage;->mNavSelect:I
    invoke-static {v1}, Lnet/flixster/android/TheaterListPage;->access$1(Lnet/flixster/android/TheaterListPage;)I

    move-result v1

    const-wide/16 v2, 0x64

    #calls: Lnet/flixster/android/TheaterListPage;->scheduleLoadItemsTask(IJ)V
    invoke-static {v0, v1, v2, v3}, Lnet/flixster/android/TheaterListPage;->access$2(Lnet/flixster/android/TheaterListPage;IJ)V

    goto :goto_0

    .line 357
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$2;->this$0:Lnet/flixster/android/TheaterListPage;

    invoke-static {v0}, Lnet/flixster/android/Starter;->launchTheatersMap(Landroid/content/Context;)V

    goto :goto_0

    .line 348
    nop

    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
