.class Lnet/flixster/android/TheaterListPage$3;
.super Ljava/lang/Object;
.source "TheaterListPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TheaterListPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TheaterListPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TheaterListPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TheaterListPage$3;->this$0:Lnet/flixster/android/TheaterListPage;

    .line 363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 365
    const-string v1, "FlxMain"

    const-string v2, "TheaterListPage.mSettingsClickListener"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lnet/flixster/android/TheaterListPage$3;->this$0:Lnet/flixster/android/TheaterListPage;

    const-class v2, Lnet/flixster/android/SettingsPage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 367
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lnet/flixster/android/TheaterListPage$3;->this$0:Lnet/flixster/android/TheaterListPage;

    invoke-virtual {v1, v0}, Lnet/flixster/android/TheaterListPage;->startActivity(Landroid/content/Intent;)V

    .line 368
    return-void
.end method
