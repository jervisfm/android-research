.class Lnet/flixster/android/TheaterListPage$4;
.super Ljava/util/TimerTask;
.source "TheaterListPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TheaterListPage;->scheduleLoadItemsTask(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TheaterListPage;

.field private final synthetic val$currResumeCtr:I

.field private final synthetic val$navSelection:I


# direct methods
.method constructor <init>(Lnet/flixster/android/TheaterListPage;II)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    iput p2, p0, Lnet/flixster/android/TheaterListPage$4;->val$navSelection:I

    iput p3, p0, Lnet/flixster/android/TheaterListPage$4;->val$currResumeCtr:I

    .line 109
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide/16 v8, 0x0

    const/4 v13, 0x0

    .line 112
    const-string v0, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "TheaterListPage.ScheduleLoadItemsTask.run navSelection:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lnet/flixster/android/TheaterListPage$4;->val$navSelection:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    #getter for: Lnet/flixster/android/TheaterListPage;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/TheaterListPage;->access$3(Lnet/flixster/android/TheaterListPage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v5

    .line 116
    .local v5, favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v1

    .line 118
    .local v1, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v3

    .line 119
    .local v3, longitude:D
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    #getter for: Lnet/flixster/android/TheaterListPage;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/TheaterListPage;->access$3(Lnet/flixster/android/TheaterListPage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static/range {v0 .. v5}, Lnet/flixster/android/data/TheaterDao;->findTheatersLocation(Ljava/util/List;DDLjava/util/HashMap;)V

    .line 129
    .end local v1           #latitude:D
    .end local v3           #longitude:D
    .end local v5           #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_0
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    iget v6, p0, Lnet/flixster/android/TheaterListPage$4;->val$currResumeCtr:I

    invoke-virtual {v0, v6}, Lnet/flixster/android/TheaterListPage;->shouldSkipBackgroundTask(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 151
    :goto_1
    return-void

    .line 120
    .restart local v5       #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_1
    :try_start_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v6

    cmpl-double v0, v6, v8

    if-eqz v0, :cond_2

    .line 121
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v6

    cmpl-double v0, v6, v8

    if-eqz v0, :cond_2

    .line 122
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    #getter for: Lnet/flixster/android/TheaterListPage;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/TheaterListPage;->access$3(Lnet/flixster/android/TheaterListPage;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v7

    .line 123
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v9

    move-object v11, v5

    .line 122
    invoke-static/range {v6 .. v11}, Lnet/flixster/android/data/TheaterDao;->findTheatersLocation(Ljava/util/List;DDLjava/util/HashMap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 145
    .end local v5           #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catch_0
    move-exception v12

    .line 146
    .local v12, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v0, "FlxMain"

    const-string v6, "TheaterListPage.ScheduleLoadItemsTask DaoException"

    invoke-static {v0, v6, v12}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 147
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    invoke-virtual {v0, v12}, Lnet/flixster/android/TheaterListPage;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 149
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 125
    .end local v12           #de:Lnet/flixster/android/data/DaoException;
    .restart local v5       #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_2
    :try_start_3
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    #getter for: Lnet/flixster/android/TheaterListPage;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/TheaterListPage;->access$3(Lnet/flixster/android/TheaterListPage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6, v5}, Lnet/flixster/android/data/TheaterDao;->findTheaters(Ljava/util/List;Ljava/lang/String;Ljava/util/HashMap;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 148
    .end local v5           #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catchall_0
    move-exception v0

    .line 149
    iget-object v6, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    iget-object v6, v6, Lnet/flixster/android/TheaterListPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v6, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 150
    throw v0

    .line 133
    :cond_3
    :try_start_4
    iget v0, p0, Lnet/flixster/android/TheaterListPage$4;->val$navSelection:I

    packed-switch v0, :pswitch_data_0

    .line 142
    :goto_2
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    invoke-virtual {v0}, Lnet/flixster/android/TheaterListPage;->trackPage()V

    .line 143
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 144
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    #calls: Lnet/flixster/android/TheaterListPage;->checkAndShowLaunchAd()V
    invoke-static {v0}, Lnet/flixster/android/TheaterListPage;->access$6(Lnet/flixster/android/TheaterListPage;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_4 .. :try_end_4} :catch_0

    .line 149
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 135
    :pswitch_0
    :try_start_5
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    #calls: Lnet/flixster/android/TheaterListPage;->setByDistanceLviList()V
    invoke-static {v0}, Lnet/flixster/android/TheaterListPage;->access$4(Lnet/flixster/android/TheaterListPage;)V

    goto :goto_2

    .line 138
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage$4;->this$0:Lnet/flixster/android/TheaterListPage;

    #calls: Lnet/flixster/android/TheaterListPage;->setByNameLviList()V
    invoke-static {v0}, Lnet/flixster/android/TheaterListPage;->access$5(Lnet/flixster/android/TheaterListPage;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_2

    .line 133
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
