.class public Lnet/flixster/android/TheaterListPage;
.super Lnet/flixster/android/LviActivityCopy;
.source "TheaterListPage.java"


# static fields
.field private static final DISTANCES_IMPERIAL:[D = null

.field private static final DISTANCES_METRIC:[D = null

.field public static final KEY_THEATERS_FILTER:Ljava/lang/String; = "KEY_THEATERS_FILTER"

.field public static mTheaterDistanceComparator:Lnet/flixster/android/model/TheaterDistanceComparator;

.field public static mTheaterNameComparator:Lnet/flixster/android/model/TheaterNameComparator;


# instance fields
.field private mDistanceString:Ljava/lang/String;

.field private final mFavorites:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/lvi/Lvi;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationStringTextView:Landroid/widget/TextView;

.field private mNavListener:Landroid/view/View$OnClickListener;

.field private mNavSelect:I

.field private mSettingsClickListener:Landroid/view/View$OnClickListener;

.field private mTheaterClickListener:Landroid/view/View$OnClickListener;

.field private final mTheaters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation
.end field

.field private navBar:Lcom/flixster/android/view/SubNavBar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x6

    .line 37
    new-array v0, v1, [D

    fill-array-data v0, :array_0

    sput-object v0, Lnet/flixster/android/TheaterListPage;->DISTANCES_IMPERIAL:[D

    .line 38
    new-array v0, v1, [D

    fill-array-data v0, :array_1

    sput-object v0, Lnet/flixster/android/TheaterListPage;->DISTANCES_METRIC:[D

    .line 40
    new-instance v0, Lnet/flixster/android/model/TheaterDistanceComparator;

    invoke-direct {v0}, Lnet/flixster/android/model/TheaterDistanceComparator;-><init>()V

    sput-object v0, Lnet/flixster/android/TheaterListPage;->mTheaterDistanceComparator:Lnet/flixster/android/model/TheaterDistanceComparator;

    .line 41
    new-instance v0, Lnet/flixster/android/model/TheaterNameComparator;

    invoke-direct {v0}, Lnet/flixster/android/model/TheaterNameComparator;-><init>()V

    sput-object v0, Lnet/flixster/android/TheaterListPage;->mTheaterNameComparator:Lnet/flixster/android/model/TheaterNameComparator;

    .line 35
    return-void

    .line 37
    :array_0
    .array-data 0x8
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0xf0t 0x3ft
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x8t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x14t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x24t 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x2et 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x34t 0x40t
    .end array-data

    .line 38
    :array_1
    .array-data 0x8
        0xd7t 0xa3t 0x70t 0x3dt 0xat 0xd7t 0xf3t 0x3ft
        0xe1t 0x7at 0x14t 0xaet 0x47t 0xe1t 0x8t 0x40t
        0xe1t 0x7at 0x14t 0xaet 0x47t 0xe1t 0x18t 0x40t
        0xa4t 0x70t 0x3dt 0xat 0xd7t 0xa3t 0x22t 0x40t
        0x14t 0xaet 0x47t 0xe1t 0x7at 0x14t 0x2ft 0x40t
        0x0t 0x0t 0x0t 0x0t 0x0t 0xc0t 0x35t 0x40t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lnet/flixster/android/LviActivityCopy;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/TheaterListPage;->mTheaters:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lnet/flixster/android/TheaterListPage;->mDistanceString:Ljava/lang/String;

    .line 333
    new-instance v0, Lnet/flixster/android/TheaterListPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterListPage$1;-><init>(Lnet/flixster/android/TheaterListPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterListPage;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    .line 346
    new-instance v0, Lnet/flixster/android/TheaterListPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterListPage$2;-><init>(Lnet/flixster/android/TheaterListPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterListPage;->mNavListener:Landroid/view/View$OnClickListener;

    .line 363
    new-instance v0, Lnet/flixster/android/TheaterListPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterListPage$3;-><init>(Lnet/flixster/android/TheaterListPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterListPage;->mSettingsClickListener:Landroid/view/View$OnClickListener;

    .line 35
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/TheaterListPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 46
    iput p1, p0, Lnet/flixster/android/TheaterListPage;->mNavSelect:I

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/TheaterListPage;)I
    .locals 1
    .parameter

    .prologue
    .line 46
    iget v0, p0, Lnet/flixster/android/TheaterListPage;->mNavSelect:I

    return v0
.end method

.method static synthetic access$2(Lnet/flixster/android/TheaterListPage;IJ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 105
    invoke-direct {p0, p1, p2, p3}, Lnet/flixster/android/TheaterListPage;->scheduleLoadItemsTask(IJ)V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/TheaterListPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage;->mTheaters:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/TheaterListPage;)V
    .locals 0
    .parameter

    .prologue
    .line 185
    invoke-direct {p0}, Lnet/flixster/android/TheaterListPage;->setByDistanceLviList()V

    return-void
.end method

.method static synthetic access$5(Lnet/flixster/android/TheaterListPage;)V
    .locals 0
    .parameter

    .prologue
    .line 274
    invoke-direct {p0}, Lnet/flixster/android/TheaterListPage;->setByNameLviList()V

    return-void
.end method

.method static synthetic access$6(Lnet/flixster/android/TheaterListPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lnet/flixster/android/TheaterListPage;->checkAndShowLaunchAd()V

    return-void
.end method

.method private declared-synchronized scheduleLoadItemsTask(IJ)V
    .locals 4
    .parameter "navSelection"
    .parameter "delay"

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/TheaterListPage;->throbberHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 108
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    .line 109
    .local v0, currResumeCtr:I
    new-instance v1, Lnet/flixster/android/TheaterListPage$4;

    invoke-direct {v1, p0, p1, v0}, Lnet/flixster/android/TheaterListPage$4;-><init>(Lnet/flixster/android/TheaterListPage;II)V

    .line 154
    .local v1, loadMoviesTask:Ljava/util/TimerTask;
    iget-object v2, p0, Lnet/flixster/android/TheaterListPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 155
    iget-object v2, p0, Lnet/flixster/android/TheaterListPage;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v1, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    :cond_0
    monitor-exit p0

    return-void

    .line 106
    .end local v0           #currResumeCtr:I
    .end local v1           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private setByDistanceLviList()V
    .locals 21

    .prologue
    .line 189
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v7

    .line 190
    .local v7, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 191
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 193
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TheaterListPage;->mTheaters:Ljava/util/ArrayList;

    invoke-static {v15}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v10

    .line 194
    .local v10, mTheatersCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    sget-object v15, Lnet/flixster/android/TheaterListPage;->mTheaterDistanceComparator:Lnet/flixster/android/model/TheaterDistanceComparator;

    invoke-static {v10, v15}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 197
    const-wide/high16 v1, 0x3ff0

    .line 198
    .local v1, conversion:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getDistanceType()I

    move-result v15

    if-nez v15, :cond_3

    .line 199
    sget-object v6, Lnet/flixster/android/TheaterListPage;->DISTANCES_IMPERIAL:[D

    .line 206
    .local v6, distances:[D
    :goto_0
    const-wide/16 v3, 0x0

    .line 207
    .local v3, currentDistance:D
    const/4 v5, 0x0

    .line 208
    .local v5, distanceIndex:I
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_0
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_4

    .line 250
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-lez v15, :cond_1

    .line 251
    const-string v15, "FlxMain"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "TheaterListPage mFavorites.size:"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " mDataHolder.size():"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 251
    invoke-static/range {v15 .. v16}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    new-instance v13, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v13}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 255
    .local v13, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterListPage;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0c001e

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v13, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 256
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v15, v0, v13}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v15 .. v17}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 258
    const-string v15, "FlxMain"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "TheaterListPage postadd mDataHolder.size():"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    .end local v13           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v15

    if-nez v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-nez v15, :cond_2

    .line 262
    new-instance v8, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 263
    .local v8, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterListPage;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0c0059

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v8, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 264
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    .end local v8           #lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterListPage;->destroyExistingLviAd()V

    .line 268
    new-instance v15, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v15}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lnet/flixster/android/TheaterListPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 269
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TheaterListPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v16, "TheatersTab"

    move-object/from16 v0, v16

    iput-object v0, v15, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 270
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    move-object/from16 v17, v0

    invoke-virtual/range {v15 .. v17}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 272
    return-void

    .line 202
    .end local v3           #currentDistance:D
    .end local v5           #distanceIndex:I
    .end local v6           #distances:[D
    :cond_3
    sget-object v6, Lnet/flixster/android/TheaterListPage;->DISTANCES_METRIC:[D

    .line 203
    .restart local v6       #distances:[D
    const-wide v1, 0x3ff9be76c0000000L

    goto/16 :goto_0

    .line 208
    .restart local v3       #currentDistance:D
    .restart local v5       #distanceIndex:I
    :cond_4
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lnet/flixster/android/model/Theater;

    .line 209
    .local v14, theater:Lnet/flixster/android/model/Theater;
    invoke-virtual {v14}, Lnet/flixster/android/model/Theater;->getMiles()D

    move-result-wide v11

    .line 211
    .local v11, miles:D
    cmpl-double v16, v11, v3

    if-lez v16, :cond_6

    .line 212
    :cond_5
    aget-wide v16, v6, v5

    cmpg-double v16, v16, v11

    if-ltz v16, :cond_7

    .line 218
    :goto_2
    new-instance v13, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v13}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 220
    .restart local v13       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    array-length v0, v6

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v5, v0, :cond_8

    .line 221
    const-wide v3, 0x412e848000000000L

    .line 223
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterListPage;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0c00e4

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    .line 224
    array-length v0, v6

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    aget-wide v19, v6, v19

    mul-double v19, v19, v1

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v19

    aput-object v19, v17, v18

    .line 222
    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v13, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 231
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    .end local v13           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_6
    new-instance v9, Lnet/flixster/android/lvi/LviTheater;

    invoke-direct {v9}, Lnet/flixster/android/lvi/LviTheater;-><init>()V

    .line 235
    .local v9, lviTheater:Lnet/flixster/android/lvi/LviTheater;
    iput-object v14, v9, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v9, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    .line 237
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterListPage;->getStarTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v9, Lnet/flixster/android/lvi/LviTheater;->mStarTheaterClick:Landroid/view/View$OnClickListener;

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    invoke-virtual {v14}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 242
    new-instance v9, Lnet/flixster/android/lvi/LviTheater;

    .end local v9           #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    invoke-direct {v9}, Lnet/flixster/android/lvi/LviTheater;-><init>()V

    .line 243
    .restart local v9       #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    iput-object v14, v9, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v9, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    .line 245
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterListPage;->getStarTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v9, Lnet/flixster/android/lvi/LviTheater;->mStarTheaterClick:Landroid/view/View$OnClickListener;

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 213
    .end local v9           #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    :cond_7
    add-int/lit8 v5, v5, 0x1

    .line 214
    array-length v0, v6

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v5, v0, :cond_5

    goto/16 :goto_2

    .line 226
    .restart local v13       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_8
    aget-wide v3, v6, v5

    .line 228
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterListPage;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0c00e3

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    .line 229
    aget-wide v19, v6, v5

    mul-double v19, v19, v1

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v19

    aput-object v19, v17, v18

    .line 227
    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v13, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    goto/16 :goto_3
.end method

.method private setByNameLviList()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 277
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v1

    .line 279
    .local v1, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v8, p0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 280
    iget-object v8, p0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 282
    iget-object v8, p0, Lnet/flixster/android/TheaterListPage;->mTheaters:Ljava/util/ArrayList;

    invoke-static {v8}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    .line 283
    .local v5, mTheatersCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    sget-object v8, Lnet/flixster/android/TheaterListPage;->mTheaterNameComparator:Lnet/flixster/android/model/TheaterNameComparator;

    invoke-static {v5, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 286
    const/4 v0, 0x0

    .line 287
    .local v0, currentLetter:C
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_3

    .line 314
    iget-object v8, p0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_1

    .line 315
    new-instance v6, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 316
    .local v6, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual {p0}, Lnet/flixster/android/TheaterListPage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c001e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 317
    iget-object v8, p0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v8, v11, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 318
    iget-object v8, p0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v9, p0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v8, v11, v9}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 321
    .end local v6           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, p0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_2

    .line 322
    new-instance v3, Lnet/flixster/android/lvi/LviMessagePanel;

    invoke-direct {v3}, Lnet/flixster/android/lvi/LviMessagePanel;-><init>()V

    .line 323
    .local v3, lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    invoke-virtual {p0}, Lnet/flixster/android/TheaterListPage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c0059

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lnet/flixster/android/lvi/LviMessagePanel;->mMessage:Ljava/lang/String;

    .line 324
    iget-object v8, p0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    .end local v3           #lviMessagePanel:Lnet/flixster/android/lvi/LviMessagePanel;
    :cond_2
    invoke-virtual {p0}, Lnet/flixster/android/TheaterListPage;->destroyExistingLviAd()V

    .line 328
    new-instance v8, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v8}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v8, p0, Lnet/flixster/android/TheaterListPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 329
    iget-object v8, p0, Lnet/flixster/android/TheaterListPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v9, "TheatersTab"

    iput-object v9, v8, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 330
    iget-object v8, p0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v9, p0, Lnet/flixster/android/TheaterListPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v8, v11, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 331
    return-void

    .line 287
    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/model/Theater;

    .line 288
    .local v7, theater:Lnet/flixster/android/model/Theater;
    const-string v9, "name"

    invoke-virtual {v7, v9}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 289
    .local v2, firstLetter:C
    invoke-static {v2}, Ljava/lang/Character;->isLetter(C)Z

    move-result v9

    if-nez v9, :cond_4

    .line 290
    const/16 v2, 0x23

    .line 292
    :cond_4
    if-eq v0, v2, :cond_5

    .line 293
    new-instance v6, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 294
    .restart local v6       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 295
    iget-object v9, p0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    move v0, v2

    .line 298
    .end local v6           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_5
    new-instance v4, Lnet/flixster/android/lvi/LviTheater;

    invoke-direct {v4}, Lnet/flixster/android/lvi/LviTheater;-><init>()V

    .line 299
    .local v4, lviTheater:Lnet/flixster/android/lvi/LviTheater;
    iput-object v7, v4, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    .line 300
    iget-object v9, p0, Lnet/flixster/android/TheaterListPage;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    iput-object v9, v4, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    .line 301
    invoke-virtual {p0}, Lnet/flixster/android/TheaterListPage;->getStarTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v9

    iput-object v9, v4, Lnet/flixster/android/lvi/LviTheater;->mStarTheaterClick:Landroid/view/View$OnClickListener;

    .line 302
    iget-object v9, p0, Lnet/flixster/android/TheaterListPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    invoke-virtual {v7}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 306
    new-instance v4, Lnet/flixster/android/lvi/LviTheater;

    .end local v4           #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    invoke-direct {v4}, Lnet/flixster/android/lvi/LviTheater;-><init>()V

    .line 307
    .restart local v4       #lviTheater:Lnet/flixster/android/lvi/LviTheater;
    iput-object v7, v4, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    .line 308
    iget-object v9, p0, Lnet/flixster/android/TheaterListPage;->mTheaterClickListener:Landroid/view/View$OnClickListener;

    iput-object v9, v4, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    .line 309
    invoke-virtual {p0}, Lnet/flixster/android/TheaterListPage;->getStarTheaterClickListener()Landroid/view/View$OnClickListener;

    move-result-object v9

    iput-object v9, v4, Lnet/flixster/android/lvi/LviTheater;->mStarTheaterClick:Landroid/view/View$OnClickListener;

    .line 310
    iget-object v9, p0, Lnet/flixster/android/TheaterListPage;->mFavorites:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private setLocationString()V
    .locals 6

    .prologue
    .line 165
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getTheaterDistance()I

    move-result v0

    .line 166
    .local v0, distance:I
    const/4 v1, 0x0

    .line 167
    .local v1, place:Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 168
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentZip()Ljava/lang/String;

    move-result-object v1

    .line 176
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    .line 177
    :cond_1
    invoke-virtual {p0}, Lnet/flixster/android/TheaterListPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c010e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/TheaterListPage;->mDistanceString:Ljava/lang/String;

    .line 182
    :goto_1
    iget-object v2, p0, Lnet/flixster/android/TheaterListPage;->mLocationStringTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lnet/flixster/android/TheaterListPage;->mDistanceString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    return-void

    .line 170
    :cond_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserZip()Ljava/lang/String;

    move-result-object v1

    .line 171
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 172
    :cond_3
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserCity()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 179
    :cond_4
    invoke-virtual {p0}, Lnet/flixster/android/TheaterListPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 180
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    .line 179
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/TheaterListPage;->mDistanceString:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method protected getAnalyticsAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 408
    const-string v0, "Logo"

    return-object v0
.end method

.method protected getAnalyticsCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 403
    const-string v0, "WidgetEntrance"

    return-object v0
.end method

.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 375
    const/4 v0, 0x0

    .line 376
    .local v0, tag:Ljava/lang/String;
    iget v1, p0, Lnet/flixster/android/TheaterListPage;->mNavSelect:I

    packed-switch v1, :pswitch_data_0

    .line 384
    :goto_0
    return-object v0

    .line 378
    :pswitch_0
    const-string v0, "/theaters/nearby"

    .line 379
    goto :goto_0

    .line 381
    :pswitch_1
    const-string v0, "/theaters/name"

    goto :goto_0

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 389
    const/4 v0, 0x0

    .line 390
    .local v0, title:Ljava/lang/String;
    iget v1, p0, Lnet/flixster/android/TheaterListPage;->mNavSelect:I

    packed-switch v1, :pswitch_data_0

    .line 398
    :goto_0
    return-object v0

    .line 392
    :pswitch_0
    const-string v0, "Theaters - Nearby"

    .line 393
    goto :goto_0

    .line 395
    :pswitch_1
    const-string v0, "Theaters - By Name"

    goto :goto_0

    .line 390
    nop

    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .parameter "savedState"

    .prologue
    const v8, 0x7f070258

    const/4 v3, 0x0

    .line 53
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onCreate(Landroid/os/Bundle;)V

    .line 54
    iget-object v2, p0, Lnet/flixster/android/TheaterListPage;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/TheaterListPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 55
    const v2, 0x7f0700f3

    invoke-virtual {p0, v2}, Lnet/flixster/android/TheaterListPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 57
    .local v1, mNavbarHolder:Landroid/widget/LinearLayout;
    new-instance v2, Lcom/flixster/android/view/SubNavBar;

    invoke-direct {v2, p0}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lnet/flixster/android/TheaterListPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    .line 58
    iget-object v4, p0, Lnet/flixster/android/TheaterListPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget-object v5, p0, Lnet/flixster/android/TheaterListPage;->mNavListener:Landroid/view/View$OnClickListener;

    const v6, 0x7f0c0123

    const v7, 0x7f0c0124

    .line 59
    invoke-static {}, Lcom/flixster/android/utils/GoogleApiDetector;->instance()Lcom/flixster/android/utils/GoogleApiDetector;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flixster/android/utils/GoogleApiDetector;->isVanillaAndroid()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    .line 58
    :goto_0
    invoke-virtual {v4, v5, v6, v7, v2}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;III)V

    .line 60
    iget-object v2, p0, Lnet/flixster/android/TheaterListPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {v2, v8}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 61
    iget-object v2, p0, Lnet/flixster/android/TheaterListPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 63
    const v2, 0x7f0700f5

    invoke-virtual {p0, v2}, Lnet/flixster/android/TheaterListPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1           #mNavbarHolder:Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 64
    .restart local v1       #mNavbarHolder:Landroid/widget/LinearLayout;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030086

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 65
    .local v0, distanceBar:Landroid/view/View;
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 67
    const v2, 0x7f070260

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lnet/flixster/android/TheaterListPage;->mLocationStringTextView:Landroid/widget/TextView;

    .line 68
    iget-object v2, p0, Lnet/flixster/android/TheaterListPage;->mLocationStringTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lnet/flixster/android/TheaterListPage;->mSettingsClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iput v8, p0, Lnet/flixster/android/TheaterListPage;->mNavSelect:I

    .line 72
    iget-object v2, p0, Lnet/flixster/android/TheaterListPage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    const-string v3, "TheatersTabStickyTop"

    invoke-virtual {v2, v3}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 73
    iget-object v2, p0, Lnet/flixster/android/TheaterListPage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    const-string v3, "TheatersTabStickyBottom"

    invoke-virtual {v2, v3}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 74
    return-void

    .line 59
    .end local v0           #distanceBar:Landroid/view/View;
    :cond_0
    const v2, 0x7f0c0125

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lnet/flixster/android/LviActivityCopy;->onPause()V

    .line 87
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage;->mTheaters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 88
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 99
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 100
    const-string v0, "FlxMain"

    const-string v1, "TheaterListPage.onRestoreInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v0, "LISTSTATE_NAV"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TheaterListPage;->mNavSelect:I

    .line 103
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 78
    invoke-super {p0}, Lnet/flixster/android/LviActivityCopy;->onResume()V

    .line 79
    invoke-direct {p0}, Lnet/flixster/android/TheaterListPage;->setLocationString()V

    .line 80
    iget-object v0, p0, Lnet/flixster/android/TheaterListPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget v1, p0, Lnet/flixster/android/TheaterListPage;->mNavSelect:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 81
    iget v0, p0, Lnet/flixster/android/TheaterListPage;->mNavSelect:I

    const-wide/16 v1, 0x64

    invoke-direct {p0, v0, v1, v2}, Lnet/flixster/android/TheaterListPage;->scheduleLoadItemsTask(IJ)V

    .line 82
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 92
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 93
    const-string v0, "FlxMain"

    const-string v1, "TheaterListPage.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v0, "LISTSTATE_NAV"

    iget v1, p0, Lnet/flixster/android/TheaterListPage;->mNavSelect:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 95
    return-void
.end method

.method protected retryAction()V
    .locals 3

    .prologue
    .line 161
    iget v0, p0, Lnet/flixster/android/TheaterListPage;->mNavSelect:I

    const-wide/16 v1, 0x3e8

    invoke-direct {p0, v0, v1, v2}, Lnet/flixster/android/TheaterListPage;->scheduleLoadItemsTask(IJ)V

    .line 162
    return-void
.end method
