.class Lnet/flixster/android/TheaterMapPage$4;
.super Ljava/lang/Object;
.source "TheaterMapPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TheaterMapPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TheaterMapPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TheaterMapPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TheaterMapPage$4;->this$0:Lnet/flixster/android/TheaterMapPage;

    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 245
    const-string v1, "FlxMain"

    const-string v2, "TheaterListPage.mSettingsClickListener"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lnet/flixster/android/TheaterMapPage$4;->this$0:Lnet/flixster/android/TheaterMapPage;

    const-class v2, Lnet/flixster/android/SettingsPage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 247
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lnet/flixster/android/TheaterMapPage$4;->this$0:Lnet/flixster/android/TheaterMapPage;

    invoke-virtual {v1, v0}, Lnet/flixster/android/TheaterMapPage;->startActivity(Landroid/content/Intent;)V

    .line 248
    return-void
.end method
