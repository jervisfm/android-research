.class Lnet/flixster/android/TheaterMapPage$5;
.super Ljava/lang/Object;
.source "TheaterMapPage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TheaterMapPage;->scheduleLoadItems()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TheaterMapPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TheaterMapPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide/16 v8, 0x0

    const/4 v13, 0x1

    .line 107
    const-string v0, "FlxMain"

    const-string v6, "TheaterMapPage.scheduleLoadItems.run"

    invoke-static {v0, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/TheaterMapPage;->access$2(Lnet/flixster/android/TheaterMapPage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFavoriteTheatersList()Ljava/util/HashMap;

    move-result-object v5

    .line 111
    .local v5, favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v1

    .line 113
    .local v1, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v3

    .line 114
    .local v3, longitude:D
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/TheaterMapPage;->access$2(Lnet/flixster/android/TheaterMapPage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static/range {v0 .. v5}, Lnet/flixster/android/data/TheaterDao;->findTheatersLocation(Ljava/util/List;DDLjava/util/HashMap;)V

    .line 123
    .end local v1           #latitude:D
    .end local v3           #longitude:D
    .end local v5           #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_0
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #calls: Lnet/flixster/android/TheaterMapPage;->updateMap()V
    invoke-static {v0}, Lnet/flixster/android/TheaterMapPage;->access$3(Lnet/flixster/android/TheaterMapPage;)V

    .line 124
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    iget-object v0, v0, Lnet/flixster/android/TheaterMapPage;->mRemoveDialogHandler:Landroid/os/Handler;

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lnet/flixster/android/TheaterMapPage;->access$1(Lnet/flixster/android/TheaterMapPage;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lnet/flixster/android/TheaterMapPage;->access$1(Lnet/flixster/android/TheaterMapPage;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    iget-object v0, v0, Lnet/flixster/android/TheaterMapPage;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 133
    :cond_1
    :goto_1
    return-void

    .line 115
    .restart local v5       #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_2
    :try_start_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v6

    cmpl-double v0, v6, v8

    if-eqz v0, :cond_3

    .line 116
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v6

    cmpl-double v0, v6, v8

    if-eqz v0, :cond_3

    .line 117
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/TheaterMapPage;->access$2(Lnet/flixster/android/TheaterMapPage;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v7

    .line 118
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v9

    move-object v11, v5

    .line 117
    invoke-static/range {v6 .. v11}, Lnet/flixster/android/data/TheaterDao;->findTheatersLocation(Ljava/util/List;DDLjava/util/HashMap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 125
    .end local v5           #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catch_0
    move-exception v12

    .line 126
    .local v12, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v0, "FlxMain"

    const-string v6, "TheaterMapPage.scheduleLoadItems DaoException"

    invoke-static {v0, v6, v12}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 127
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    iget-object v6, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;
    invoke-static {v6}, Lnet/flixster/android/TheaterMapPage;->access$4(Lnet/flixster/android/TheaterMapPage;)Lcom/flixster/android/activity/decorator/DialogDecorator;

    move-result-object v6

    invoke-static {v12, v0, v6}, Lcom/flixster/android/utils/ErrorDialog;->handleException(Lnet/flixster/android/data/DaoException;Landroid/app/Activity;Lcom/flixster/android/activity/decorator/DialogDecorator;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 129
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lnet/flixster/android/TheaterMapPage;->access$1(Lnet/flixster/android/TheaterMapPage;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lnet/flixster/android/TheaterMapPage;->access$1(Lnet/flixster/android/TheaterMapPage;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    iget-object v0, v0, Lnet/flixster/android/TheaterMapPage;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 120
    .end local v12           #de:Lnet/flixster/android/data/DaoException;
    .restart local v5       #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_3
    :try_start_3
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->mTheaters:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/TheaterMapPage;->access$2(Lnet/flixster/android/TheaterMapPage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6, v5}, Lnet/flixster/android/data/TheaterDao;->findTheaters(Ljava/util/List;Ljava/lang/String;Ljava/util/HashMap;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 128
    .end local v5           #favoriteTheaters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catchall_0
    move-exception v0

    .line 129
    iget-object v6, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;
    invoke-static {v6}, Lnet/flixster/android/TheaterMapPage;->access$1(Lnet/flixster/android/TheaterMapPage;)Landroid/app/Dialog;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    #getter for: Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;
    invoke-static {v6}, Lnet/flixster/android/TheaterMapPage;->access$1(Lnet/flixster/android/TheaterMapPage;)Landroid/app/Dialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Dialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 130
    iget-object v6, p0, Lnet/flixster/android/TheaterMapPage$5;->this$0:Lnet/flixster/android/TheaterMapPage;

    iget-object v6, v6, Lnet/flixster/android/TheaterMapPage;->mRemoveDialogHandler:Landroid/os/Handler;

    invoke-virtual {v6, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 132
    :cond_4
    throw v0
.end method
