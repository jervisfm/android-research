.class public Lnet/flixster/android/TheaterMapPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;
.source "TheaterMapPage.java"


# static fields
.field private static final DIALOGKEY_LOADING:I = 0x1

.field private static final MAP_ZOOM_DEFAULT:I = 0xd

.field private static final mDistanceZoomMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDistanceString:Ljava/lang/String;

.field private mLoadingDialog:Landroid/app/Dialog;

.field private mLocationStringTextView:Landroid/widget/TextView;

.field private mMapController:Lcom/google/android/maps/MapController;

.field private mMapInvalidateHandler:Landroid/os/Handler;

.field private mMapView:Lcom/google/android/maps/MapView;

.field private mMapViewControls:Landroid/widget/LinearLayout;

.field protected final mRemoveDialogHandler:Landroid/os/Handler;

.field private mSettingsClickListener:Landroid/view/View$OnClickListener;

.field protected final mShowDialogHandler:Landroid/os/Handler;

.field private final mTheaters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    .line 41
    sget-object v0, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    const/16 v1, 0x23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    const/16 v1, 0x4b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mTheaters:Ljava/util/ArrayList;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mDistanceString:Ljava/lang/String;

    .line 159
    new-instance v0, Lnet/flixster/android/TheaterMapPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterMapPage$1;-><init>(Lnet/flixster/android/TheaterMapPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mShowDialogHandler:Landroid/os/Handler;

    .line 169
    new-instance v0, Lnet/flixster/android/TheaterMapPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterMapPage$2;-><init>(Lnet/flixster/android/TheaterMapPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mRemoveDialogHandler:Landroid/os/Handler;

    .line 236
    new-instance v0, Lnet/flixster/android/TheaterMapPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterMapPage$3;-><init>(Lnet/flixster/android/TheaterMapPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mMapInvalidateHandler:Landroid/os/Handler;

    .line 243
    new-instance v0, Lnet/flixster/android/TheaterMapPage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/TheaterMapPage$4;-><init>(Lnet/flixster/android/TheaterMapPage;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mSettingsClickListener:Landroid/view/View$OnClickListener;

    .line 35
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/TheaterMapPage;)Lcom/google/android/maps/MapView;
    .locals 1
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mMapView:Lcom/google/android/maps/MapView;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/TheaterMapPage;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/TheaterMapPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mTheaters:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/TheaterMapPage;)V
    .locals 0
    .parameter

    .prologue
    .line 193
    invoke-direct {p0}, Lnet/flixster/android/TheaterMapPage;->updateMap()V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/TheaterMapPage;)Lcom/flixster/android/activity/decorator/DialogDecorator;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->dialogDecorator:Lcom/flixster/android/activity/decorator/DialogDecorator;

    return-object v0
.end method

.method private declared-synchronized scheduleLoadItems()V
    .locals 2

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 104
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/TheaterMapPage$5;

    invoke-direct {v1, p0}, Lnet/flixster/android/TheaterMapPage$5;-><init>(Lnet/flixster/android/TheaterMapPage;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setLocationString()V
    .locals 6

    .prologue
    .line 138
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getTheaterDistance()I

    move-result v0

    .line 139
    .local v0, distance:I
    const/4 v1, 0x0

    .line 140
    .local v1, place:Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 141
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentZip()Ljava/lang/String;

    move-result-object v1

    .line 149
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    .line 150
    :cond_1
    invoke-virtual {p0}, Lnet/flixster/android/TheaterMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c010e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mDistanceString:Ljava/lang/String;

    .line 156
    :goto_1
    iget-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mDistanceString:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lnet/flixster/android/TheaterMapPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 157
    return-void

    .line 143
    :cond_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserZip()Ljava/lang/String;

    move-result-object v1

    .line 144
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 145
    :cond_3
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserCity()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 152
    :cond_4
    invoke-virtual {p0}, Lnet/flixster/android/TheaterMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 153
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    .line 152
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mDistanceString:Ljava/lang/String;

    goto :goto_1
.end method

.method private updateMap()V
    .locals 25

    .prologue
    .line 196
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v20

    if-eqz v20, :cond_0

    .line 197
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLatitude()D

    move-result-wide v5

    .line 198
    .local v5, latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLongitude()D

    move-result-wide v9

    .line 203
    .local v9, longitude:D
    :goto_0
    const-wide/16 v20, 0x0

    cmpl-double v20, v5, v20

    if-nez v20, :cond_1

    const-wide/16 v20, 0x0

    cmpl-double v20, v9, v20

    if-nez v20, :cond_1

    .line 204
    const-string v20, "FlxMain"

    const-string v21, "TheaterMapPage.updateMap lat = long = 0"

    invoke-static/range {v20 .. v21}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :goto_1
    return-void

    .line 200
    .end local v5           #latitude:D
    .end local v9           #longitude:D
    :cond_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLatitude()D

    move-result-wide v5

    .line 201
    .restart local v5       #latitude:D
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLongitude()D

    move-result-wide v9

    .restart local v9       #longitude:D
    goto :goto_0

    .line 206
    :cond_1
    new-instance v4, Lcom/google/android/maps/GeoPoint;

    const-wide v20, 0x412e848000000000L

    mul-double v20, v20, v5

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v20, v0

    const-wide v21, 0x412e848000000000L

    mul-double v21, v21, v9

    move-wide/from16 v0, v21

    double-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v4, v0, v1}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 207
    .local v4, geoPoint:Lcom/google/android/maps/GeoPoint;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterMapPage;->mMapController:Lcom/google/android/maps/MapController;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/google/android/maps/MapController;->setCenter(Lcom/google/android/maps/GeoPoint;)V

    .line 208
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/TheaterMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 209
    .local v13, resources:Landroid/content/res/Resources;
    new-instance v8, Lnet/flixster/android/TheaterItemizedOverlay;

    .line 210
    const v20, 0x7f0200ad

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    .line 209
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v8, v0, v1}, Lnet/flixster/android/TheaterItemizedOverlay;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 211
    .local v8, locationOverlays:Lnet/flixster/android/TheaterItemizedOverlay;
    new-instance v7, Lcom/google/android/maps/OverlayItem;

    const-string v20, "Current Location"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v7, v4, v0, v1}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    .local v7, locationOverlay:Lcom/google/android/maps/OverlayItem;
    invoke-virtual {v8, v7}, Lnet/flixster/android/TheaterItemizedOverlay;->addOverlay(Lcom/google/android/maps/OverlayItem;)V

    .line 213
    new-instance v18, Lnet/flixster/android/TheaterItemizedOverlay;

    .line 214
    const v20, 0x7f020152

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    .line 213
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/TheaterItemizedOverlay;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 216
    .local v18, theaterOverlays:Lnet/flixster/android/TheaterItemizedOverlay;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterMapPage;->mTheaters:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v11

    .line 217
    .local v11, mTheatersCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_2
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-nez v21, :cond_3

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterMapPage;->mMapView:Lcom/google/android/maps/MapView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v12

    .line 227
    .local v12, overlays:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 228
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    invoke-virtual/range {v18 .. v18}, Lnet/flixster/android/TheaterItemizedOverlay;->size()I

    move-result v20

    if-lez v20, :cond_2

    .line 230
    move-object/from16 v0, v18

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TheaterMapPage;->mMapInvalidateHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 217
    .end local v12           #overlays:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    :cond_3
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lnet/flixster/android/model/Theater;

    .line 218
    .local v14, theater:Lnet/flixster/android/model/Theater;
    iget-wide v0, v14, Lnet/flixster/android/model/Theater;->latitude:D

    move-wide/from16 v21, v0

    const-wide v23, 0x412e848000000000L

    mul-double v21, v21, v23

    move-wide/from16 v0, v21

    double-to-int v15, v0

    .line 219
    .local v15, theaterLatitude:I
    iget-wide v0, v14, Lnet/flixster/android/model/Theater;->longitude:D

    move-wide/from16 v21, v0

    const-wide v23, 0x412e848000000000L

    mul-double v21, v21, v23

    move-wide/from16 v0, v21

    double-to-int v0, v0

    move/from16 v16, v0

    .line 220
    .local v16, theaterLongitude:I
    new-instance v19, Lcom/google/android/maps/GeoPoint;

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 221
    .local v19, theaterPoint:Lcom/google/android/maps/GeoPoint;
    new-instance v17, Lcom/google/android/maps/OverlayItem;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "name"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 222
    const-string v22, "\n"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "address"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v14}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    .line 221
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    .local v17, theaterOverlay:Lcom/google/android/maps/OverlayItem;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lnet/flixster/android/TheaterItemizedOverlay;->addOverlay(Lcom/google/android/maps/OverlayItem;)V

    goto/16 :goto_2
.end method


# virtual methods
.method protected isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedState"

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    const v2, 0x7f030084

    invoke-virtual {p0, v2}, Lnet/flixster/android/TheaterMapPage;->setContentView(I)V

    .line 66
    invoke-virtual {p0}, Lnet/flixster/android/TheaterMapPage;->createActionBar()V

    .line 68
    const v2, 0x7f070260

    invoke-virtual {p0, v2}, Lnet/flixster/android/TheaterMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mLocationStringTextView:Landroid/widget/TextView;

    .line 69
    iget-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mLocationStringTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lnet/flixster/android/TheaterMapPage;->mSettingsClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mLocationStringTextView:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 72
    const v2, 0x7f0700e4

    invoke-virtual {p0, v2}, Lnet/flixster/android/TheaterMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/maps/MapView;

    iput-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mMapView:Lcom/google/android/maps/MapView;

    .line 73
    iget-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mMapController:Lcom/google/android/maps/MapController;

    .line 74
    const v2, 0x7f070262

    invoke-virtual {p0, v2}, Lnet/flixster/android/TheaterMapPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mMapViewControls:Landroid/widget/LinearLayout;

    .line 75
    iget-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mMapViewControls:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lnet/flixster/android/TheaterMapPage;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->getZoomControls()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 76
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getTheaterDistance()I

    move-result v0

    .line 77
    .local v0, distance:I
    const/4 v1, -0x1

    .line 78
    .local v1, zoom:I
    if-lez v0, :cond_0

    .line 79
    sget-object v2, Lnet/flixster/android/TheaterMapPage;->mDistanceZoomMap:Ljava/util/HashMap;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getTheaterDistance()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 81
    :cond_0
    if-gtz v1, :cond_1

    .line 82
    const/16 v1, 0xd

    .line 84
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mMapController:Lcom/google/android/maps/MapController;

    invoke-virtual {v2, v1}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 85
    iget-object v2, p0, Lnet/flixster/android/TheaterMapPage;->mMapView:Lcom/google/android/maps/MapView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/maps/MapView;->displayZoomControls(Z)V

    .line 86
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v3, 0x1

    .line 181
    packed-switch p1, :pswitch_data_0

    .line 190
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 183
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;

    .line 184
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lnet/flixster/android/TheaterMapPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 186
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 187
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 188
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mLoadingDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onPause()V

    .line 99
    iget-object v0, p0, Lnet/flixster/android/TheaterMapPage;->mTheaters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 100
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 90
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockMapActivity;->onResume()V

    .line 91
    invoke-direct {p0}, Lnet/flixster/android/TheaterMapPage;->setLocationString()V

    .line 92
    invoke-direct {p0}, Lnet/flixster/android/TheaterMapPage;->scheduleLoadItems()V

    .line 93
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/theaters/map"

    const-string v2, "Theaters - By Name"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    return-void
.end method
