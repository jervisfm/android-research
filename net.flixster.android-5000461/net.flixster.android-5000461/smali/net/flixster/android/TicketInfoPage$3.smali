.class Lnet/flixster/android/TicketInfoPage$3;
.super Landroid/os/Handler;
.source "TicketInfoPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TicketInfoPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketInfoPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketInfoPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    .line 236
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .parameter "msg"

    .prologue
    .line 239
    const-string v9, "FlxMain"

    const-string v10, "TicketInfoPage.postShowtimesLoadHandler"

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v4, 0x0

    .line 241
    .local v4, showtimes:Lnet/flixster/android/model/Showtimes;
    iget-object v9, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v9}, Lnet/flixster/android/TicketInfoPage;->access$1(Lnet/flixster/android/TicketInfoPage;)Lnet/flixster/android/model/Theater;

    move-result-object v9

    invoke-virtual {v9}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;

    move-result-object v6

    .line 242
    .local v6, showtimesListByMovieHash:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;>;"
    iget-object v9, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mMovieId:J
    invoke-static {v9}, Lnet/flixster/android/TicketInfoPage;->access$2(Lnet/flixster/android/TicketInfoPage;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 243
    .local v5, showtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 248
    const-string v9, "FlxMain"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "TicketInfoPage.postShowtimesLoadHandler showtimes:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget-object v9, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mTicketInfoPage:Lnet/flixster/android/TicketInfoPage;
    invoke-static {v9}, Lnet/flixster/android/TicketInfoPage;->access$4(Lnet/flixster/android/TicketInfoPage;)Lnet/flixster/android/TicketInfoPage;

    move-result-object v9

    invoke-virtual {v9}, Lnet/flixster/android/TicketInfoPage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020085

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 252
    .local v0, buyIcon:Landroid/graphics/drawable/Drawable;
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v11

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v12

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 254
    iget-object v9, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mListingLayout:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lnet/flixster/android/TicketInfoPage;->access$5(Lnet/flixster/android/TicketInfoPage;)Landroid/widget/LinearLayout;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 255
    iget-object v9, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mListingPastLayout:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lnet/flixster/android/TicketInfoPage;->access$6(Lnet/flixster/android/TicketInfoPage;)Landroid/widget/LinearLayout;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 257
    iget-object v9, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    const v10, 0x7f070275

    invoke-virtual {v9, v10}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 258
    .local v8, totalShowtimes:Landroid/widget/TextView;
    new-instance v9, Ljava/lang/StringBuilder;

    iget-object v10, v4, Lnet/flixster/android/model/Showtimes;->mListings:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, " showtimes total"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    const-string v9, "FlxMain"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "TicketInfoPage.postShowtimesLoadHandler showtimes.mListings:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v4, Lnet/flixster/android/model/Showtimes;->mListings:Ljava/util/ArrayList;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    iget-object v9, v4, Lnet/flixster/android/model/Showtimes;->mListings:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 288
    iget-object v9, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mListingPastLayout:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lnet/flixster/android/TicketInfoPage;->access$6(Lnet/flixster/android/TicketInfoPage;)Landroid/widget/LinearLayout;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    if-nez v9, :cond_4

    .line 289
    iget-object v9, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lnet/flixster/android/TicketInfoPage;->access$7(Lnet/flixster/android/TicketInfoPage;)Landroid/widget/RelativeLayout;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 293
    :goto_2
    return-void

    .line 243
    .end local v0           #buyIcon:Landroid/graphics/drawable/Drawable;
    .end local v8           #totalShowtimes:Landroid/widget/TextView;
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/model/Showtimes;

    .line 244
    .local v7, tempShowtimes:Lnet/flixster/android/model/Showtimes;
    iget-object v10, v7, Lnet/flixster/android/model/Showtimes;->mShowtimesTitle:Ljava/lang/String;

    iget-object v11, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mShowtimesTitle:Ljava/lang/String;
    invoke-static {v11}, Lnet/flixster/android/TicketInfoPage;->access$3(Lnet/flixster/android/TicketInfoPage;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 245
    move-object v4, v7

    goto/16 :goto_0

    .line 261
    .end local v7           #tempShowtimes:Lnet/flixster/android/model/Showtimes;
    .restart local v0       #buyIcon:Landroid/graphics/drawable/Drawable;
    .restart local v8       #totalShowtimes:Landroid/widget/TextView;
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Listing;

    .line 262
    .local v1, listing:Lnet/flixster/android/model/Listing;
    new-instance v2, Landroid/widget/TextView;

    iget-object v10, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mTicketInfoPage:Lnet/flixster/android/TicketInfoPage;
    invoke-static {v10}, Lnet/flixster/android/TicketInfoPage;->access$4(Lnet/flixster/android/TicketInfoPage;)Lnet/flixster/android/TicketInfoPage;

    move-result-object v10

    invoke-direct {v2, v10}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 263
    .local v2, listingView:Landroid/widget/TextView;
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v10, -0x1

    .line 264
    const/4 v11, -0x2

    .line 263
    invoke-direct {v3, v10, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 266
    .local v3, lp:Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 267
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 268
    const/16 v10, 0x10

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setGravity(I)V

    .line 269
    invoke-virtual {v1}, Lnet/flixster/android/model/Listing;->getDisplayTimeDate()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v10, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    const v11, 0x7f0d0070

    invoke-virtual {v2, v10, v11}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 272
    invoke-virtual {v1}, Lnet/flixster/android/model/Listing;->hasElapsed()Z

    move-result v10

    if-nez v10, :cond_3

    .line 273
    const v10, 0x7f020075

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 274
    const/16 v10, 0xa

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 275
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v2, v10, v11, v0, v12}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 276
    const/16 v10, 0x1e

    const/16 v11, 0x14

    const/16 v12, 0x14

    const/16 v13, 0x14

    invoke-virtual {v2, v10, v11, v12, v13}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 277
    iget-object v10, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mTicketInfoPage:Lnet/flixster/android/TicketInfoPage;
    invoke-static {v10}, Lnet/flixster/android/TicketInfoPage;->access$4(Lnet/flixster/android/TicketInfoPage;)Lnet/flixster/android/TicketInfoPage;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    iget-object v10, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mListingLayout:Landroid/widget/LinearLayout;
    invoke-static {v10}, Lnet/flixster/android/TicketInfoPage;->access$5(Lnet/flixster/android/TicketInfoPage;)Landroid/widget/LinearLayout;

    move-result-object v10

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 281
    :cond_3
    const v10, 0x7f020133

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 282
    iget-object v10, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    invoke-virtual {v10}, Lnet/flixster/android/TicketInfoPage;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090021

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 283
    const/16 v10, 0x1e

    const/16 v11, 0x1e

    const/16 v12, 0x1e

    const/16 v13, 0x14

    invoke-virtual {v2, v10, v11, v12, v13}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 284
    iget-object v10, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mListingPastLayout:Landroid/widget/LinearLayout;
    invoke-static {v10}, Lnet/flixster/android/TicketInfoPage;->access$6(Lnet/flixster/android/TicketInfoPage;)Landroid/widget/LinearLayout;

    move-result-object v10

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 291
    .end local v1           #listing:Lnet/flixster/android/model/Listing;
    .end local v2           #listingView:Landroid/widget/TextView;
    .end local v3           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_4
    iget-object v9, p0, Lnet/flixster/android/TicketInfoPage$3;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lnet/flixster/android/TicketInfoPage;->access$7(Lnet/flixster/android/TicketInfoPage;)Landroid/widget/RelativeLayout;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_2
.end method
