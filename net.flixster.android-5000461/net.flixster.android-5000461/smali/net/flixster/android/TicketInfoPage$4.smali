.class Lnet/flixster/android/TicketInfoPage$4;
.super Ljava/util/TimerTask;
.source "TicketInfoPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TicketInfoPage;->ScheduleLoadShowtimesTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketInfoPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketInfoPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    .line 148
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 153
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mStartLoadingShowtimesDialogHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketInfoPage;->access$8(Lnet/flixster/android/TicketInfoPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 155
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TheaterDao.Schedule mMovie.getId():"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mMovieId:J
    invoke-static {v3}, Lnet/flixster/android/TicketInfoPage;->access$2(Lnet/flixster/android/TicketInfoPage;)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    iget-object v2, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mTheaterId:J
    invoke-static {v2}, Lnet/flixster/android/TicketInfoPage;->access$9(Lnet/flixster/android/TicketInfoPage;)J

    move-result-wide v2

    iget-object v4, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mMovieId:J
    invoke-static {v4}, Lnet/flixster/android/TicketInfoPage;->access$2(Lnet/flixster/android/TicketInfoPage;)J

    move-result-wide v4

    .line 157
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v6

    .line 156
    invoke-static {v2, v3, v4, v5, v6}, Lnet/flixster/android/data/TheaterDao;->findTheaterByTheaterMoviePair(JJLjava/util/Date;)Lnet/flixster/android/model/Theater;

    move-result-object v2

    #setter for: Lnet/flixster/android/TicketInfoPage;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v1, v2}, Lnet/flixster/android/TicketInfoPage;->access$10(Lnet/flixster/android/TicketInfoPage;Lnet/flixster/android/model/Theater;)V

    .line 158
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TicketInfoPage.ScheduleLoadShowtimesTask().run() mTheater.getShowtimesHash():"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 159
    iget-object v3, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mTheater:Lnet/flixster/android/model/Theater;
    invoke-static {v3}, Lnet/flixster/android/TicketInfoPage;->access$1(Lnet/flixster/android/TicketInfoPage;)Lnet/flixster/android/model/Theater;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 158
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mStopLoadingShowtimesDialogHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketInfoPage;->access$11(Lnet/flixster/android/TicketInfoPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 161
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->postShowtimesLoadHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketInfoPage;->access$12(Lnet/flixster/android/TicketInfoPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :goto_0
    return-void

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TicketInfoPage.ScheduleLoadShowtimesTask Exception mNetworkTrys:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mNetworkTrys:I
    invoke-static {v3}, Lnet/flixster/android/TicketInfoPage;->access$13(Lnet/flixster/android/TicketInfoPage;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 165
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mNetworkTrys:I
    invoke-static {v1}, Lnet/flixster/android/TicketInfoPage;->access$13(Lnet/flixster/android/TicketInfoPage;)I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    .line 166
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #calls: Lnet/flixster/android/TicketInfoPage;->ScheduleLoadShowtimesTask()V
    invoke-static {v1}, Lnet/flixster/android/TicketInfoPage;->access$14(Lnet/flixster/android/TicketInfoPage;)V

    .line 167
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mNetworkTrys:I
    invoke-static {v1}, Lnet/flixster/android/TicketInfoPage;->access$13(Lnet/flixster/android/TicketInfoPage;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    #setter for: Lnet/flixster/android/TicketInfoPage;->mNetworkTrys:I
    invoke-static {v1, v2}, Lnet/flixster/android/TicketInfoPage;->access$15(Lnet/flixster/android/TicketInfoPage;I)V

    goto :goto_0

    .line 169
    :cond_0
    const-string v1, "FlxMain"

    const-string v2, "TicketInfoPage.ScheduleLoadShowtimesTask SHOW NETWORK ERROR DIALOG"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage$4;->this$0:Lnet/flixster/android/TicketInfoPage;

    #getter for: Lnet/flixster/android/TicketInfoPage;->mStopLoadingShowtimesDialogHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketInfoPage;->access$11(Lnet/flixster/android/TicketInfoPage;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
